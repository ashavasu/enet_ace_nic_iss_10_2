/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *  $Id: ptptmr.c,v 1.7 2014/01/24 12:20:09 siva Exp $
 *
 * Description: This file contains PTP task timer related routines.     
 *********************************************************************/
#ifndef _PTPTMR_C_
#define _PTPTMR_C_

#include "ptpincs.h"

PRIVATE VOID        PtpTmrAlMstSyncMsgTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrCompDbTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrQulfnTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrAnncReceiptTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrPeerDelayReqTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrDelayReqTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrAnncTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrSyncTmrExp (VOID *pArg);
PRIVATE VOID        PtpTmrInitTmrDesc (VOID);

/*****************************************************************************/
/* Function                  : PtpTmrInit                                    */
/*                                                                           */
/* Description               : This routine intializes all the timer related */
/*                             PTP data.                                     */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.PtpTmrListId                   */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTmrInit (VOID)
{
    PTP_FN_ENTRY ();

    if (TmrCreateTimerList (PTP_TASK, PTP_TMR_EXP_EVENT, NULL,
                            &(gPtpGlobalInfo.PtpTmrListId)) == TMR_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "PtpTmrInit: Timer list creation FAILED !!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PtpTmrInitTmrDesc ();

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpTmrDeInit                                  */
/*                                                                           */
/* Description               : This routine de-initzes all the timer related */
/*                             PTP data.                                     */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.PtpTmrListId                   */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTmrDeInit (VOID)
{
    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.PtpTmrListId == 0)
    {
        /* Timer is not initialized */
        return OSIX_SUCCESS;
    }

    if (TmrDeleteTimerList (gPtpGlobalInfo.PtpTmrListId) == TMR_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "PtpTmrDeInit: Timer list deletion FAILED !!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (gPtpGlobalInfo.aTmrDesc, 0,
            (sizeof (tTmrDesc) * PTP_MAX_TMR_TYPES));

    gPtpGlobalInfo.PtpTmrListId = 0;
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpTmrInitTmrDesc                             */
/*                                                                           */
/* Description               : This routine intializes timer descriptors for */
/*                             all timers present in the PTP module.         */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.aTmrDesc                       */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrInitTmrDesc (VOID)
{
    PTP_FN_ENTRY ();

    /* Announce Timer */
    gPtpGlobalInfo.aTmrDesc[PTP_ANNC_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, AnnounceTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_ANNC_TMR].TmrExpFn = PtpTmrAnncTmrExp;

    /* Sync Timer */
    gPtpGlobalInfo.aTmrDesc[PTP_SYNC_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, SyncTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_SYNC_TMR].TmrExpFn = PtpTmrSyncTmrExp;

    /* Delay Request Tmr */
    gPtpGlobalInfo.aTmrDesc[PTP_DREQ_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, DelayReqTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_DREQ_TMR].TmrExpFn = PtpTmrDelayReqTmrExp;

    /* Peer Delay Request Tmr */
    gPtpGlobalInfo.aTmrDesc[PTP_PDREQ_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, PdelayReqTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_PDREQ_TMR].TmrExpFn = PtpTmrPeerDelayReqTmrExp;

    /* Annc Receipt Time out timer */
    gPtpGlobalInfo.aTmrDesc[PTP_ANNC_RECEIPT_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, AnnounceReciptTimeOutTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_ANNC_RECEIPT_TMR].TmrExpFn =
        PtpTmrAnncReceiptTmrExp;

    /* Qualification timer */
    gPtpGlobalInfo.aTmrDesc[PTP_QULFN_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, QualificationExpiryTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_QULFN_TMR].TmrExpFn = PtpTmrQulfnTmrExp;

    /* Compatability DB Table timer */
    gPtpGlobalInfo.aTmrDesc[PTP_COMPDB_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpCompDBEntry, CompDBTmrNode);
    gPtpGlobalInfo.aTmrDesc[PTP_COMPDB_TMR].TmrExpFn = PtpTmrCompDbTmrExp;

    /* Alternate Master Multi Sync Timer */
    gPtpGlobalInfo.aTmrDesc[PTP_MULT_SYNC_TMR].i2Offset =
        (INT4) FSAP_OFFSETOF (tPtpPort, MulSyncTimer);
    gPtpGlobalInfo.aTmrDesc[PTP_MULT_SYNC_TMR].TmrExpFn =
        PtpTmrAlMstSyncMsgTmrExp;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrInitTmrDesc                             */
/*                                                                           */
/* Description               : This function is called whenever a timer      */
/*                             expiry event is received by PTP. Appropriate  */
/*                             timer expiry handlers are called based on the */
/*                             timer type                                    */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.aTmrDesc,                      */
/*                             gPtpGlobalInfo.PtpTmrListId                   */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    PTP_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gPtpGlobalInfo.PtpTmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Timer expired Event occurred...\r\n"));

        if (u1TimerId < PTP_MAX_TMR_TYPES)
        {
            i2Offset = gPtpGlobalInfo.aTmrDesc[u1TimerId].i2Offset;

            /* Call the registered expired handler function */
            (*(gPtpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }                            /* End of while */
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrAnncTmrExp                              */
/*                                                                           */
/* Description               : This routine indicates the announce message   */
/*                             handler the need to transmit an announce msg  */
/*                             over the given interface. This will also      */
/*                             restart the Announce timer after indicating   */
/*                             the announce message handler.                 */
/*                                                                           */
/* Input                     : pArg - Pointer to the structure whose timer   */
/*                                    has expired.                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrAnncTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Announce timer expired for port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Transmitting Announce Message over interface "
              "%u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    if ((pPtpPort->PortDs.u1PortState == PTP_STATE_MASTER) ||
        ((PtpUtilGetAltMstFlagStatus (pPtpPort)) == OSIX_TRUE))
    {
        /* Announce timer will be started when we are in listening state.
         * Hence the PDU should be transmitted only when the port moves to
         * MASTER state
         * */
        if (PtpAnncTxAnncMsg (pPtpPort) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "PtpTmrAnncTmrExp: "
                      "Port %u Id %u PtpAnncTxAnncMsg returned Failure!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber, PTP_ANNC_TMR));

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Id %u Unable to transmit Announce message\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber, PTP_ANNC_TMR));
        }
    }

    /* Trigger BMC Calculation for this port */
    PtpBmcTriggerBmcCalc (pPtpPort->pPtpDomain);

    /* Trigger AltMst Calculation for this port */
    if ((PtpAlMstTriggerAltMstMsg (pPtpPort)) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpTmrAnncTmrExp : PtpAlMstTriggerAltMstMsg failed.\r\n"));
    }

    u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                            u1AnnounceInterval);
    if (PtpTmrStartTmr (&(pPtpPort->AnnounceTimer),
                        u4MilliSec, PTP_ANNC_TMR, OSIX_TRUE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port : %u "
                  "PtpTmrAnncTmrExp: "
                  "Timer %u Re start function returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_ANNC_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to restart announce timer"
                  "%u for port %u\r\n",
                  PTP_ANNC_TMR, pPtpPort->PortDs.u4PtpPortNumber));
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrSyncTmrExp                              */
/*                                                                           */
/* Description               : This routine indicates the sync     message   */
/*                             handler the need to transmit a sync message   */
/*                             over the given interface. This will also      */
/*                             restart the Sync timer after triggering the   */
/*                             Sync message handler.                         */
/*                                                                           */
/* Input                     : pArg - Pointer to the structure whose timer   */
/*                                    has expired.                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrSyncTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Sync timer expired for port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Transmitting Sync Message over interface "
              "\r\n"));

    if (PtpSyncTxSyncMsg (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u Id %u "
                  "PtpTmrSyncTmrExp: "
                  "PtpSyncTxSyncMsg returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_SYNC_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to transmit Sync message\r\n"));
    }

    u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                            i1SyncInterval);
    if (PtpTmrStartTmr (&(pPtpPort->SyncTimer),
                        u4MilliSec, PTP_SYNC_TMR, OSIX_TRUE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "PtpTmrSyncTmrExp: "
                  "PtpTmrStartTmr returned Failure!!!\r\n"));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to restart sync timer %u"
                  "for port %u\r\n", PTP_SYNC_TMR,
                  pPtpPort->PortDs.u4PtpPortNumber));
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrDelayReqTmrExp                          */
/*                                                                           */
/* Description               : This routine indicates the delay request      */
/*                             message handler the need to transmit a delay  */
/*                             request message. This will also restart the   */
/*                             delay request timer after triggering the      */
/*                             delay request message handler.                */
/*                                                                           */
/* Input                     : pArg - Pointer to the structure whose timer   */
/*                                    has expired.                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrDelayReqTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Delay Request timer expired for port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Transmitting Delay Req Message over interface " "\r\n"));

    if (PtpDreqTxDelayReqMsg (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u Id %u "
                  "PtpTmrDelayReqTmrExp: "
                  "PtpAnncTxAnncMsg returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_DREQ_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to transmit Delay Request "
                  "message\r\n"));
    }

    u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                            u1MinDelayReqInterval);
    if (PtpTmrStartTmr (&(pPtpPort->DelayReqTimer),
                        u4MilliSec, PTP_DREQ_TMR, OSIX_TRUE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u Id %u "
                  "PtpTmrDelayReqTmrExp: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_DREQ_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to restart Delay Request timer %u"
                  " for port %u\r\n", PTP_DREQ_TMR,
                  pPtpPort->PortDs.u4PtpPortNumber));
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrPeerDelayReqTmrExp                      */
/*                                                                           */
/* Description               : This routine indicates the peer delay request */
/*                             message handler the need to transmit a peer   */
/*                             delay request message. This will also restart */
/*                             peer delay request timer after triggering the */
/*                             peer delay request message handler.           */
/*                                                                           */
/* Input                     : pArg - Pointer to the structure whose timer   */
/*                                    has expired.                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrPeerDelayReqTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Peer Delay Request timer expired for port %u"
              "\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Transmitting Peer Delay Req Message over "
              "interface\r\n"));

    if (PtpPdreqTxPeerDelayReqMsg (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u Id %u "
                  "PtpTmrPeerDelayReqTmrExp: "
                  "PtpPdreqTxPeerDelayReqMsg returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_PDREQ_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to transmit Peer Delay Request "
                  "message over %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    }

    u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                            u1MinPdelayReqInterval);
    if (PtpTmrStartTmr (&(pPtpPort->PdelayReqTimer),
                        u4MilliSec, PTP_PDREQ_TMR, OSIX_TRUE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u Id %u"
                  "PtpTmrPeerDelayReqTmrExp: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_PDREQ_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to restart Peer delay "
                  "request timer for port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrAnncReceiptTmrExp                       */
/*                                                                           */
/* Description               : This routine indicates the PTP port State     */
/*                             Event machine the expiry of Announce receipt  */
/*                             Time out. This will not restart the timer.    */
/*                                                                           */
/* Input                     : pArg - Pointer to the structure whose timer   */
/*                                    has expired.                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrAnncReceiptTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Announce Receipt timer expired for "
              "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "PTP SEM indicated with Announce receipt "
              "time out event for port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PtpSemPortSemHandler (pPtpPort, PTP_ANNC_RECPT_TIMEOUT_EVENT);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrQulfnTmrExp                             */
/*                                                                           */
/* Description               : This routine indicates the PTP port State     */
/*                             Event machine the expiry of Qualification     */
/*                             Time out. This will not restart the timer.    */
/*                                                                           */
/* Input                     : pArg - Pointer to the structure whose timer   */
/*                                    has expired.                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrQulfnTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Qualification timer expired for port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "PTP SEM indicated with Qualification "
              "time out event for port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PtpSemPortSemHandler (pPtpPort, PTP_QUALIFY_TMR_EXP_EVENT);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrStartTmr                                */
/*                                                                           */
/* Description               : This routine starts the various timers prsent */
/*                             in the PTP module. The timer type should also */
/*                             provided along with the timer node. When this */
/*                             routine is called from Expiry handler, it is  */
/*                             assumed that the timer would have been removed*/
/*                             from the DLL and timer is stopped. When this  */
/*                             flag is set, Timer will be started without    */
/*                             validation.                                   */
/*                                                                           */
/* Input                     : pTmrBlk- Pointer to the structure whose timer */
/*                                      needs to be started                  */
/*                             u1TimeInterval - Duration for which the timer */
/*                                              needs to run.                */
/*                             u1TmrType      - Type of the timer.           */
/*                             u1IsExpired    - OSIX_TRUE indicates, that    */
/*                                              this function is invoked from*/
/*                                              timer expiry thread.         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.PtpTmrListId                   */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_FAILURE/OSIX_SUCCESS                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTmrStartTmr (tTmrBlk * pTmrBlk, UINT4 u4TimeInterval, UINT1 u1TmrType,
                UINT1 u1IsExpired)
{
    UINT4               u4RemainingTime = 0;

    PTP_FN_ENTRY ();

    /* This function will be called during configuration as well as during
     * timer expiry. Hence, if this function is invoked during timer expiry,
     * we will be sure that the timer has been removed from the expired list
     * and the timer is not running.
     * But, if it is invoked from some other place, we need to make sure that
     * the existing timer should be stopped and after that the node should be
     * removed from expired list. To achieve the same, following method is
     * followed.
     * 1) Get the remaining time for timer
     * 2) If the function call returns SUCCESS, then the timer is present
     *    either in expired list or in running list. Hence, first stop the 
     *    existing timer & then start a new timer,.i.e. TmrRestartTimer call
     * 3) If the function call returns FAILURE, then timer is not present
     *    within the Data structure. Hence invoke TmrStartTimer directly 
     * */

    if (u1IsExpired == OSIX_FALSE)
    {
        if (TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                 &(pTmrBlk->TimerNode), &u4RemainingTime)
            == TMR_SUCCESS)

        {
            if (TmrRestart (gPtpGlobalInfo.PtpTmrListId, pTmrBlk, u1TmrType,
                            0, u4TimeInterval) == TMR_FAILURE)

            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "Timer Id %u PtpTmrStartTmr: "
                          "TmrRestart function returned Failure!!!\r\n",
                          u1TmrType));

                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }

    if (TmrStart (gPtpGlobalInfo.PtpTmrListId, pTmrBlk, u1TmrType,
                  0, u4TimeInterval) == TMR_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, ALL_FAILURE_TRC |
                  OS_RESOURCE_TRC,
                  "Timer Identifer %u PtpTmrStartTmr: "
                  "TmrStart function returned Failure!!!\r\n", u1TmrType));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
              "Timer started for timer type %u Duration %u\r\n",
              u1TmrType, u4TimeInterval));

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpTmrStopPdelayReqTimer                      */
/*                                                                           */
/* Description               : This routine stops the peer delay request     */
/*                timer over the given interface.              */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port whose timer    */
/*                                        needs to be stopped.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTmrStopPdelayReqTimer (tPtpPort * pPtpPort)
{
    UINT4               u4RemainingTime = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4TmrRetVal = TMR_SUCCESS;

    PTP_FN_ENTRY ();

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Stopping pdelay req timer for peer to peer"
              "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    u4TmrRetVal = TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                       &(pPtpPort->PdelayReqTimer.TimerNode),
                                       &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->PdelayReqTimer))
            != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for PdelayReqTimer!!!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to stop PdelayReqTimer\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
        u4RemainingTime = 0;
        i4RetVal = OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpTmrStopAllTimersForPort                    */
/*                                                                           */
/* Description               : This routine stops all the running timers     */
/*                             over the given interface.                     */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port whose timer    */
/*                                        needs to be stopped.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTmrStopAllTimersForPort (tPtpPort * pPtpPort)
{
    UINT4               u4RemainingTime = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4TmrRetVal = TMR_SUCCESS;

    PTP_FN_ENTRY ();

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Stopping all the running timers for "
              "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    u4TmrRetVal = TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                       &(pPtpPort->AnnounceTimer.TimerNode),
                                       &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->AnnounceTimer))
            != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for Announce message!!!!!\\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to stop Announce timer\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            i4RetVal = OSIX_FAILURE;
        }
        u4RemainingTime = 0;
    }

    u4TmrRetVal = TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                       &(pPtpPort->SyncTimer.TimerNode),
                                       &u4RemainingTime);

    /* Irrespective of last timer stop success/failure, this timer should be
     * stopped.
     * */
    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->SyncTimer))
            != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for SyncTimer!!!!!\\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Port %u Unable to stop SyncTimer\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            i4RetVal = OSIX_FAILURE;
        }
        u4RemainingTime = 0;
    }

    u4TmrRetVal = TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                       &(pPtpPort->DelayReqTimer.TimerNode),
                                       &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->DelayReqTimer))
            != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for DelayReqTimer!!!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to stop DelayReqTimer\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            i4RetVal = OSIX_FAILURE;
        }
        u4RemainingTime = 0;
    }

    u4TmrRetVal = TmrGetRemainingTime
        (gPtpGlobalInfo.PtpTmrListId,
         &(pPtpPort->AnnounceReciptTimeOutTimer.TimerNode), &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId,
                     &(pPtpPort->AnnounceReciptTimeOutTimer)) != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for Announce Receipt Timer!!\\r\n.",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to stop Announce receipt"
                      "Timer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
            i4RetVal = OSIX_FAILURE;
        }

        u4RemainingTime = 0;
    }

    u4TmrRetVal = TmrGetRemainingTime
        (gPtpGlobalInfo.PtpTmrListId,
         &(pPtpPort->QualificationExpiryTimer.TimerNode), &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId,
                     &(pPtpPort->QualificationExpiryTimer)) != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for Qualification Timer!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Port %u Unable to stop Qualification"
                      "ReqTimer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
            i4RetVal = OSIX_FAILURE;
        }

        u4RemainingTime = 0;
    }

    u4TmrRetVal = TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                       &(pPtpPort->MulSyncTimer.TimerNode),
                                       &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->MulSyncTimer))
            != TMR_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u TmrStop: "
                      "Returned failure for MulSyncTimer!!!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to stop MulSyncTimer\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            i4RetVal = OSIX_FAILURE;
        }
        u4RemainingTime = 0;
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : PtpTmrCompDbTmrExp                                         */
/*                                                                           */
/* Description  : This Timer expiry indicates in PtpCompTable entry is       */
/*                staled , it not received the Sync message form the v1 Node */
/*                three times for Sync Interval call PtpCompDelCompDbEntry   */
/*                to delete the entry.                                       */
/*                                                                           */
/* Input        :  pArg - Pointer to the expired timer's Entry structure     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTmrCompDbTmrExp (VOID *pArg)
{
    tPtpCompDBEntry    *pPtpCompDB = NULL;

    PTP_FN_ENTRY ();

    pPtpCompDB = (tPtpCompDBEntry *) pArg;

    PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
              CONTROL_PLANE_TRC, "Compatibility DB timer expired\r\n"));

    if (PtpCompDelCompDbEntry (pPtpCompDB) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "PtpTmrCompDbTmrExp: "
                  "PtpCompDelCompDBEntry returned Failure!!!\r\n"));
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpTmrAlMstSyncMsgTmrExp                                   */
/*                                                                           */
/* Description  : This function is used to to handle the Alternate master    */
/*                Multi Sync timer expiry event.                             */
/*                This will send the Multicast Sync message with Alternate   */
/*                master flag set and start this timer again                 */
/*                                                                           */
/* Input        :  pArg - Pointer to the expired timer's Entry structure     */
/*                                                                           */
/* Output       : Do the following                                           */
/*                1. Multicast Sync message with Alternate master flag set   */
/*                2. start this timer again                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*****************************************************************************/
PRIVATE VOID
PtpTmrAlMstSyncMsgTmrExp (VOID *pArg)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    pPtpPort = (tPtpPort *) pArg;

    if ((pPtpPort->bIsEligibleAltMst == OSIX_TRUE) &&
        (pPtpPort->bAltMulcastSync == PTP_ENABLED))
    {
        /* 1.Send the sync message with alternateMasterFlag = TRUE */
        /* Sync Message Trigger */
        i4Status = PtpSyncTxSyncMsg (pPtpPort);

        if (i4Status == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "Port %u PtpTmrAlMstSyncMsgTmrExp : PtpSyncTxSyncMsg -"
                      " Multi Sync Timer failed!!.\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to send Multi Sync Msg\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }

        /* 2.Start the timer for next sync message. */
        u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->
                                                u1AltMulcastSynInterval);
        if (PtpTmrStartTmr (&(pPtpPort->MulSyncTimer),
                            u4MilliSec, PTP_MULT_SYNC_TMR, OSIX_TRUE)
            == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "Port %u PtpTmrAlMstSyncMsgTmrExp : PtpTmrStartTmr -"
                      " Multi Sync Timer failed!!.\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to start Multi Sync timer\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTmrStopTmrsOnStateChg                      */
/*                                                                           */
/* Description               : This routine should be invoked whenever there */
/*                             is a state change. This routine will stop     */
/*                             all the running timers that are ir-relevant   */
/*                             to the current state.                         */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the Port.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.PtpTmrListId                   */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTmrStopTmrsOnStateChg (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    /* Whenever a new state is elected for a port, then the timers associated
     * with the old state needs to be stopped. If new state is not different
     * from the existing one, the function should return from here itself
     * */

    if (pPtpPort->PortDs.u1RecommendedState == pPtpPort->PortDs.u1PortState)
    {
        /* Same state selected */
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    switch (pPtpPort->PortDs.u1RecommendedState)
    {
        case PTP_STATE_MASTER:

            switch (pPtpPort->PortDs.u1PortState)
            {
                case PTP_STATE_SLAVE:
                case PTP_STATE_UNCALIBRATED:
                    /* Intentional fall through */

                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              CONTROL_PLANE_TRC, "Stopping Announce Receipt "
                              "timer for port %u\r\n",
                              pPtpPort->PortDs.u4PtpPortNumber));
                    TmrStop (gPtpGlobalInfo.PtpTmrListId,
                             &(pPtpPort->AnnounceReciptTimeOutTimer));
                    break;

                case PTP_STATE_PREMASTER:

                    /* Stopping Delay Request timer */
                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              CONTROL_PLANE_TRC, "Stopping Delay Request"
                              "timer for port %u\r\n",
                              pPtpPort->PortDs.u4PtpPortNumber));
                    TmrStop (gPtpGlobalInfo.PtpTmrListId,
                             &(pPtpPort->DelayReqTimer));
                    break;

                default:
                    /* Exceptional case */
                    break;
            }                    /* Master State Selected */
            break;

        case PTP_STATE_SLAVE:
        case PTP_STATE_UNCALIBRATED:
            /* Intentional fall through */
            switch (pPtpPort->PortDs.u1PortState)
            {
                case PTP_STATE_MASTER:

                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              CONTROL_PLANE_TRC, "Stopping sync"
                              "timer for port %u\r\n",
                              pPtpPort->PortDs.u4PtpPortNumber));
                    TmrStop (gPtpGlobalInfo.PtpTmrListId,
                             &(pPtpPort->SyncTimer));

                    break;

                case PTP_STATE_PREMASTER:

                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              CONTROL_PLANE_TRC, "Stopping Qualification"
                              "timer for port %u\r\n",
                              pPtpPort->PortDs.u4PtpPortNumber));
                    TmrStop (gPtpGlobalInfo.PtpTmrListId,
                             &(pPtpPort->QualificationExpiryTimer));
                    break;

                default:
                    /* Exceptional case */
                    break;
            }                    /* Slave or Uncalibrated state */

        default:
            /* For other states, timers are not associated */
            break;
    }
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif /* _PTPTMR_C_ */
