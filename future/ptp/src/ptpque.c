/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpque.c,v 1.9 2014/07/03 10:21:33 siva Exp $
 * Description: This file contains PTP task queue related routines and 
 *              processing the received PDUs.
 *********************************************************************/
#ifndef _PTPQUE_C_
#define _PTPQUE_C_

#include "ptpincs.h"

PRIVATE VOID        PtpQueHandleFollowUpTrigger (tPtpQMsg * pPtpQMsg);
PRIVATE VOID        PtpQueProcessNpsimMsg (UINT4 u4ContextId,
                                           UINT1 *pu1NpsimPkt,
                                           UINT4 u4MediaHdrLen);

/*****************************************************************************/
/* Function                  : PtpQueMsgHandler                              */
/*                                                                           */
/* Description               : This routine receives the PTP messages that   */
/*                             have been posted into the PTP Queue. It       */
/*                             de-queues the PTP message and will invoke the */
/*                             corresponding message handlers based on the   */
/*                             received message.                             */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.PtpQId                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpQueMsgHandler (VOID)
{
    tPtpQMsg           *pPtpQMsg = NULL;

    PTP_FN_ENTRY ();

    /* Dequeue the PTP Queue and handle the message for processing */
    while (OsixQueRecv (gPtpGlobalInfo.PtpQId, (UINT1 *) &pPtpQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pPtpQMsg->u4MsgType)
        {
            case PTP_CTXT_DELETE_MSG:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC,
                          "PtpQueMsgHandler: Received Context Delete Msg\r\n"));
                PtpCxtDeleteContext (pPtpQMsg->u4ContextId);
                break;

            case PTP_PORT_DEL_MSG:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpQueMsgHandler: Received Port Delete "
                          "Message\r\n"));
                PtpIfDeleteInterface (pPtpQMsg->u4ContextId, pPtpQMsg->u4Port,
                                      pPtpQMsg->PtpDeviceType);
                break;

            case PTP_PORT_STATUS_MSG:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpQueMsgHandler: "
                          "Received Port Oper status Message.\r\n"));
                PtpIfHandleOperStatusChg (pPtpQMsg->u4ContextId,
                                          pPtpQMsg->u4Port,
                                          pPtpQMsg->PtpDeviceType,
                                          pPtpQMsg->u1PortOperStatus);
                break;

            case PTP_PDU_ENQ_MSG:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpQueMsgHandler: Received PTP Message\r\n"));
                PtpQueHandleRxPdu (pPtpQMsg);
                break;

            case PTP_FOLLOW_UP_TRIGGER:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpQueMsgHandler: Received Follow up trigger\r\n"));
                PtpQueHandleFollowUpTrigger (pPtpQMsg);
                break;

            case PTP_PRIMARY_PARAMS_CHG:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpQueMsgHandler: Received Primary Params "
                          "Change Message.\r\n"));
                PtpDmnUpdtPrimaryParams (pPtpQMsg);
                break;

            default:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "Unknown MessageType received\r\n"));
                break;
        }                        /* Message Type */

        PtpMainReleaseQueMemory (pPtpQMsg);
    }                            /* DeQueueing */

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpQueHandleRxPdu                             */
/*                                                                           */
/* Description               : This routine handles the received PTP message.*/
/*                             This routine copies the received PTP Message  */
/*                             into a linear buffer and decodes the PDU      */
/*                             and invokes the corresponding message handler */
/*                             routines. Validation should be done inside the*/
/*                             corresponding message handlers.               */
/*                                                                           */
/* Input                     : pPtpQMsg - Received PTP Message               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpQueHandleRxPdu (tPtpQMsg * pPtpQMsg)
{
    UINT1              *pu1RxPdu = NULL;
    tPtpPort           *pPtpPort = NULL;
    tPtpPktParams       PtpPktParams;
    tPtpPortAddress     PtpPortAddress;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpPktParams, 0, sizeof (tPtpPktParams));
    MEMSET (&PtpPortAddress, 0, sizeof (tPtpPortAddress));

    PtpPktParams.pPtpIngressTimeStamp = &(pPtpQMsg->PtpSysTimeInfo);

    /* Exception handling */
    if (pPtpQMsg->u4PktLen > PTP_MAX_PDU_LEN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS, PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "Invalid Packet length received over port %u\r\n"));

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS, PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "Length of the received PDU is %d...\r\n",
                  pPtpQMsg->u4PktLen));
        PTP_FN_EXIT ();
        return;
    }

    PtpPktParams.pu1RcvdPdu = MemBuddyAlloc ((UINT1)
                                             gPtpGlobalInfo.i4PktBuddyId,
                                             pPtpQMsg->u4PktLen);

    if (PtpPktParams.pu1RcvdPdu == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpQueMsgHandler: "
                  "Buddy Memory Allocation for PDU Failed!!!!\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return;
    }

    pu1RxPdu = PtpPktParams.pu1RcvdPdu;

    MEMSET ((PtpPktParams.pu1RcvdPdu), 0, pPtpQMsg->u4PktLen);

    CRU_BUF_Copy_FromBufChain (pPtpQMsg->uPtpMsgParam.pPtpPdu,
                               PtpPktParams.pu1RcvdPdu, 0, pPtpQMsg->u4PktLen);

    /* Get the Source Mac Address  */
    CRU_BUF_Copy_FromBufChain (pPtpQMsg->uPtpMsgParam.pPtpPdu,
                               (UINT1 *) &(PtpPortAddress.ai1Addr),
                               CFA_ENET_ADDR_LEN, CFA_ENET_ADDR_LEN);

    PtpPortAddress.u2NetworkProtocol = PTP_IFACE_IEEE_802_3;
    PtpPortAddress.u2AddrLength = CFA_ENET_ADDR_LEN;
    PtpPktParams.pPtpPortAddress = &PtpPortAddress;

    if ((PtpCmUtlGetDomainIdFromPTPv1Msg ((PtpPktParams.pu1RcvdPdu +
                                           PTP_LLC_HDR_SIZE),
                                          &u1DomainId)) == OSIX_FAILURE)
    {
        u1DomainId = PtpPktParams.pu1RcvdPdu[PTP_LLC_HDR_SIZE +
                                             PTP_MSG_DOMAIN_OFFSET];
    }
    /* Obtain the corresponding port entry. Get the PTP Port number from the
     * given port id. If a valid VLAN identifier is obtained, then this needs
     * to be done for the provided vlan identifier.
     * */
    if (pPtpQMsg->VlanId != 0)
    {
        /* A valid VLAN id is provided */
        pPtpQMsg->u4Port = (UINT4) pPtpQMsg->VlanId;
    }

    pPtpPort = PtpIfGetPortFromIfTypeAndIndex (pPtpQMsg->u4ContextId,
                                               u1DomainId,
                                               pPtpQMsg->u4Port,
                                               pPtpQMsg->PtpDeviceType);
    if (pPtpPort == NULL)
    {
        /* Exceptional handling & for klocworks warning */
        PTP_TRC ((pPtpQMsg->u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpQueHandleRxPdu:PTP Port NOT IN SERVICE.Packet discarded\r\n"));
        MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
        PTP_FN_EXIT ();
        return;
    }

    PtpPktParams.u4PktLen = pPtpQMsg->u4PktLen;
    PtpPktParams.pPtpPort = pPtpPort;

    if (PtpQueProcessPtpPdu (&PtpPktParams) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | BUFFER_TRC, "PtpQueProcessPtpPdu: "
                  "returned Failure!!!!!\r\n"));
    }

    /* Release buddy memory here */
    MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
    PTP_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function                  : PtpQueProcessPtpPdu                           */
/*                                                                           */
/* Description               : This routine will be invoked to process the  .*/
/*                             PTP messages received over the interface. This*/
/*                             routine can be invoked either directly from   */
/*                             Queue Processing or after reading data from   */
/*                             the socket.                                   */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpQueProcessPtpPdu (tPtpPktParams * pPtpPktParams)
{
    UINT4               u4MediaHdrLen = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1PtpMsgIndex = 0;

    PTP_FN_ENTRY ();

    /* If the port is NOT_IN_SERVICE, do not process the pdu
     * */
    if (pPtpPktParams->pPtpPort->u1RowStatus != ACTIVE)
    {
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpQueProcessPtpPdu: "
                  "PTP Port Is Not Active !!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pPtpPktParams->pPtpPort->PortDs.u1PtpEnabledStatus == PTP_DISABLED)
    {
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpQueProcessPtpPdu: "
                  "PTP Is Not Enabled In This Port!!!!!\r\n"));
        /* PTP is disabled over this interface */
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    /* If the version no is neither one nor two, drop the packet */
    if ((pPtpPktParams->pu1RcvdPdu[u4MediaHdrLen + PTP_MSG_VERSION_OFFSET] !=
         PTP_MESSAGE_VERSION_ONE) &&
        (pPtpPktParams->pu1RcvdPdu[u4MediaHdrLen + PTP_MSG_VERSION_OFFSET] !=
         PTP_MESSAGE_VERSION_TWO))
    {
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        /* PTP is disabled over this interface */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpQueProcessPtpPdu: "
                  "Invalid PTP Version!!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Message type will be encoded in the 4 bits of the first byte of the
     * header. */
    u1MsgType = pPtpPktParams->pu1RcvdPdu[PTP_MSG_TYPE_OFFSET + u4MediaHdrLen];
    u1MsgType = u1MsgType & PTP_MSG_TYPE_MASK;

    if (u1MsgType == PTP_NPSIM_MSG)
    {
        /* This is the message received from NPSIM regarding the origin time
         * information of sync / delay request / peer delay request message
         * This is used only for testing purposes
         */
        PtpQueProcessNpsimMsg (pPtpPktParams->pPtpPort->u4ContextId,
                               pPtpPktParams->pu1RcvdPdu, u4MediaHdrLen);
        return OSIX_SUCCESS;
    }

    /* Check the clock mode. If PTP port is operating on forward mode,
     * transmit the packet to all other ports in this domain
     */
    if (pPtpPktParams->pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE)
    {
        if (PtpTxForwardPktOnDmnPorts
            (pPtpPktParams->pu1RcvdPdu, pPtpPktParams->pPtpPort,
             pPtpPktParams->u4PktLen, u1MsgType, NULL) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "Unable to forward packets on all other "
                      "ports in the domain.\r\n"));
        }
        return OSIX_SUCCESS;
    }

    /* Check the incoming port version if the it is V1 and Clock mode is 
     * Boundary Clock Transform the V1 Packet to V2 Packet and Process further
     */
    if (pPtpPktParams->pPtpPort->PortDs.u4VersionNumber ==
        PTP_MESSAGE_VERSION_ONE)
    {
        /* Buffer "pu1RcvdPdu" will be released in the 
         * PtpComRxProcessV1Messages on both success and failure case */

        if ((PtpComRxProcessV1Messages (pPtpPktParams->pPtpPort,
                                        pPtpPktParams)) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC | BUFFER_TRC,
                      "PtpQueProcessPtpPdu: "
                      "PtpComRxProcessV1Messages returned Failure!!\r\n"));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    /* The packet needs to be validated before classifying and feeding to
     * appropriate engine. The following routine checks for Transmitted 
     * packet received condition, length etc.
     * */
    if (PtpUtilValidateRxPkt (pPtpPktParams->pu1RcvdPdu,
                              pPtpPktParams->pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Validation of received packet "
                  "Failed.\r\n"));
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpQueProcessPtpPdu: "
                  "PtpUtilValidateRxPkt returned Failure!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    switch (u1MsgType)
    {
        case PTP_ANNC_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Received Announce Message \r\n"));
            u1PtpMsgIndex = PTP_ANNC_FN_PTR;
            break;

        case PTP_SYNC_MSG:
        case PTP_FOLLOW_UP_MSG:
            /* Intentional Fall Through
             * Follow up messages are applicable only for Sync messages
             * */

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Received Sync Message \r\n"));
            u1PtpMsgIndex = PTP_SYNC_FN_PTR;
            break;

        case PTP_DELAY_REQ_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Received Delay Request Message\r\n"));
            u1PtpMsgIndex = PTP_DREQ_FN_PTR;
            break;

        case PTP_DELAY_RESP_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Received Delay Response Message\r\n"));
            u1PtpMsgIndex = PTP_DRES_FN_PTR;
            break;

        case PTP_PEER_DELAY_REQ_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Received Peer Delay Request Message \r\n"));
            u1PtpMsgIndex = PTP_PDREQ_FN_PTR;
            break;

        case PTP_PEER_DELAY_RESP_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Received Peer Delay Response Message \r\n"));
            u1PtpMsgIndex = PTP_PDRES_FN_PTR;
            break;

        case PTP_PDELAY_RESP_FOLLOWUP_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Received Peer Delay Response Follow up Message \r\n"));
            u1PtpMsgIndex = PTP_PDRES_FN_PTR;
            break;

        case PTP_SIGNALLING_MSG:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Received Signalling Message \r\n"));
            u1PtpMsgIndex = PTP_SIG_FN_PTR;
            break;

        default:

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "Unknown message type received\r\n"));
            u1PtpMsgIndex = PTP_MAX_MSG;
            break;
    }

    if (u1PtpMsgIndex >= PTP_MAX_MSG)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, ALL_FAILURE_TRC,
                  "Invalid Message Type decoded.....\r\n"));

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, ALL_FAILURE_TRC,
                  "Ignoring the message.....\r\n"));
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if (gPtpGlobalInfo.PtpMsgFn[u1PtpMsgIndex].pMsgRxHandler == NULL)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "Message processing is not available in this package.\r\n"));

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "Ignoring the received message.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if (gPtpGlobalInfo.PtpMsgFn[u1PtpMsgIndex].pMsgRxHandler (pPtpPktParams)
        == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC |
                  ALL_FAILURE_TRC,
                  "Message Handler returned Failure!!!!!\r\n"));
    }
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpQueEnqMsg                                  */
/*                                                                           */
/* Description               : This routine posts the given message to the   */
/*                             PTP task and triggers an event to the PTP     */
/*                             Queue. This utility should be used to post    */
/*                             the message to PTP.                           */
/*                                                                           */
/* Input                     : pPtpQMsg - Pointer to the message to be posted*/
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.PtpQId, gPtpGlobalInfo.TaskId  */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpQueEnqMsg (tPtpQMsg * pPtpQMsg)
{
    PTP_FN_ENTRY ();

    if (OsixQueSend (gPtpGlobalInfo.PtpQId, (UINT1 *) &pPtpQMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PtpMainReleaseQueMemory (pPtpQMsg);

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  BUFFER_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "PtpQueEnqMsg: " "Osix Queue Send failed!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpQuePostEventToPtpTask (PTP_CONF_MSG_ENQ_EVENT) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  BUFFER_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "PtpQuePostEventToPtpTask: "
                  "Osix Event send failed!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpQuePostEventToPtpTask                      */
/*                                                                           */
/* Description               : This routine posts the event to the PTP module*/
/*                                                                           */
/* Input                     : i4Event - Event to be posted to the module.   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.TaskId                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpQuePostEventToPtpTask (INT4 i4Event)
{
    PTP_FN_ENTRY ();

    if (OsixEvtSend (gPtpGlobalInfo.TaskId, i4Event) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "PtpQuePostEventToPtpTask: "
                  "Osix Event send failed!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpQueLoadMsgHandlers                         */
/*                                                                           */
/* Description               : This routine loads the corresponding message  */
/*                             handlers. This routine loads for all the type */
/*                             of messages, announce, sync, ...              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpQueLoadMsgHandlers (VOID)
{
    PTP_FN_ENTRY ();

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Loading Module Message " "Handlers....\r\n"));

    PtpAnncInitAnncHandler ();
    PtpSyncInitSyncHandler ();
    PtpDreqInitDreqHandler ();
    PtpDresInitDresHandler ();
    PtpPdreqInitPdreqHandler ();
    PtpPdresInitPeerDelayRespHandler ();

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpQueHandleFollowUpTrigger                   */
/*                                                                           */
/* Description               : This routine will be invoked when the hardware*/
/*                             provides the PHY level time stamp for the     */
/*                             transmitted packet. This routine will trigger */
/*                             the corresponding follow up handler to send   */
/*                             the follow up message.                        */
/*                                                                           */
/* Input                     : pPtpQMsg - Pointer to the received Q Msg.     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpQueHandleFollowUpTrigger (tPtpQMsg * pPtpQMsg)
{
    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();

    /* Check if Port entry is present for VLAN */
    pPtpPort = PtpIfGetPortFromIfTypeAndIndex (pPtpQMsg->u4ContextId,
                                               pPtpQMsg->u1DomainId,
                                               pPtpQMsg->u4Port,
                                               PTP_IFACE_IEEE_802_3);
    if (pPtpPort == NULL)
    {
        /* Get the Port entry with IfIndex of the interface */
        pPtpPort = PtpIfGetPortFromIfTypeAndIndex (pPtpQMsg->u4ContextId,
                                                   pPtpQMsg->u1DomainId,
                                                   pPtpQMsg->u4Port,
                                                   PTP_IFACE_UDP_IPV4);

        if (pPtpPort == NULL)
        {
            /* Get the Port entry with IfIndex of the interface */
            pPtpPort = PtpIfGetPortFromIfTypeAndIndex (pPtpQMsg->u4ContextId,
                                                       pPtpQMsg->u1DomainId,
                                                       pPtpQMsg->u4Port,
                                                       PTP_IFACE_UDP_IPV6);

            if (pPtpPort == NULL)
            {
                /* Exceptional handling & for klocworks warning */
                PTP_TRC ((pPtpQMsg->u4ContextId, pPtpQMsg->u1DomainId,
                          OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                          "FollowUp Trigger failed ...\r\n"));
                PTP_FN_EXIT ();
                return;
            }
        }
    }

    if ((pPtpQMsg->uPtpMsgParam.u1MsgType == PTP_SYNC_MSG) &&
        (pPtpPort->pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE))
    {
        /* This can happen when a two step transparent clock tries to forward
         * the follow up. Hardware may think that the Time stamp needs to be
         * provided to the software for follow up. But Transparent clock will
         * never originate Sync.
         * We cannot disable the Time Stamp option in hardware itself, as it
         * will be needed for Peer Delay response messages.
         * Hence exit safely here
         * */
        PTP_FN_EXIT ();
        return;
    }

    /* Obtain the port pointer from pPtpQMsg */
    PtpTxTriggerFollowUp (pPtpPort, &(pPtpQMsg->PtpSysTimeInfo),
                          pPtpQMsg->uPtpMsgParam.u1MsgType);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpQueProcessNpsimMsg                         */
/*                                                                           */
/* Description               : This routine will process the message from    */
/*                             NPSIM which contains the origin time stamp    */
/*                             of sync / delay request / peer delay request  */
/*                             message                                       */
/*                                                                           */
/* Input                     : u4ContextId  -  Context Id                    */
/*                             pu1NpsimPkt  -  NPSIM pkt                     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpQueProcessNpsimMsg (UINT4 u4ContextId, UINT1 *pu1NpsimPkt,
                       UINT4 u4MediaHdrLen)
{
    tClkSysTimeInfo     PtpSysTimeInfo;
    ePtpDeviceType      DeviceType = 0;
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;
    UINT1              *pu1TmpPkt = NULL;
    UINT1               u1MessageType = 0;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));

    /* Move the offset to message type size + version + message length size
     */
    pu1TmpPkt = pu1NpsimPkt + u4MediaHdrLen + sizeof (UINT1) +
        sizeof (UINT1) + sizeof (UINT2);

    u1DomainId = *pu1TmpPkt;
    pu1TmpPkt = pu1TmpPkt + 1;

    PTP_LBUF_GET_4_BYTES (pu1TmpPkt, DeviceType);
    PTP_LBUF_GET_4_BYTES (pu1TmpPkt, u4IfIndex);
    u1MessageType = *pu1TmpPkt;
    pu1TmpPkt = pu1TmpPkt + 1;

    PTP_LBUF_GET_4_BYTES (pu1TmpPkt, PtpSysTimeInfo.FsClkTimeVal.u8Sec.u4Lo);
    PTP_LBUF_GET_4_BYTES (pu1TmpPkt, PtpSysTimeInfo.FsClkTimeVal.u4Nsec);

    /* If the Device type is IPv4, get the IP port number */
    if (DeviceType == PTP_IFACE_UDP_IPV4)
    {
        PtpPortGetCfaIfIndexFromPort (u4IfIndex, &u4Port);
        u4IfIndex = u4Port;
    }

    pPtpPort = PtpIfGetPortFromIfTypeAndIndex (u4ContextId, u1DomainId,
                                               u4IfIndex, DeviceType);

    if (pPtpPort == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpQueProcessNpsimMsg: Port Not Present ...\r\n"));
        return;
    }

    switch (u1MessageType)
    {
        case PTP_DELAY_REQ_MSG:

            MEMCPY (&(pPtpPort->DelayReqOriginTime),
                    &(PtpSysTimeInfo.FsClkTimeVal), sizeof (tFsClkTimeVal));
            break;

        case PTP_PEER_DELAY_REQ_MSG:

            MEMCPY (&(pPtpPort->PeerDelayOriginTime),
                    &(PtpSysTimeInfo.FsClkTimeVal), sizeof (tFsClkTimeVal));
            break;

    }

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag
        == PTP_ENABLED)
    {
        PtpTxTriggerFollowUp (pPtpPort, &PtpSysTimeInfo, u1MessageType);
    }

    PTP_FN_EXIT ();
}
#endif /* _PTPQUE_C_ */
