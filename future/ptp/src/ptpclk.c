/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpclk.c,v 1.15 2014/05/29 13:19:58 siva Exp $
 *
 * Description: This file contains PTP clock servo sub module implementation.
 ******************************************************************************/
#ifndef _PTPCLK_C_
#define _PTPCLK_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpClkGetTimeStamp                            */
/*                                                                           */
/* Description               : This routine will be invoked to acquire the   */
/*                             time stamp of the packet. When software       */
/*                             time stamping is enabled, this routine gives  */
/*                             the current time stamp. If Hardware timestamp */
/*                             is enabled, this routine passes the packet to */
/*                             the hardware and acquires the timestamp.      */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                                                                           */
/* Output                    : pPtpSysTimeInfo - Time Stamp of the pkt.      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpClkGetTimeStamp (tPtpPort * pPtpPort, VOID *pu1Pdu,
                    tClkSysTimeInfo * pPtpSysTimeInfo, UINT4 u4IfIndex,
                    UINT1 u1Mode)
{
#ifdef NPAPI_WANTED
    tPtpHwLog           PtpHwLog;
    UINT2               u2TwoByteVal = 0;
    UINT4               u4MediaHdrLen = 0;

    MEMSET (&PtpHwLog, 0, sizeof (tPtpHwLog));
#endif

    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.TimeStamp == PTP_SOFTWARE_TIMESTAMPING)
    {
        if (PtpPortClkIwfGetClock (pPtpSysTimeInfo) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Unable to obtain Time stamp.\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
#ifdef NPAPI_WANTED
    else
    {
        /* Hardware Time Stamping */
        u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);
        CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) pu1Pdu,
                                   (UINT1 *) &u2TwoByteVal,
                                   (PTP_SEQ_OFFSET + u4MediaHdrLen), 2);
        u2TwoByteVal = OSIX_NTOHS (u2TwoByteVal);
        PtpHwLog.u4SeqId = (UINT4) u2TwoByteVal;
        PtpHwLog.u1Mode = u1Mode;
        PtpHwLog.u4PortNo = u4IfIndex;

        if (PtpNpWrGetPktLogTime (&PtpHwLog) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Call Back for packet ingress time Failed!!!!\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
        FSAP_U8_ASSIGN (&(pPtpSysTimeInfo->FsClkTimeVal.u8Sec),
                        &(PtpHwLog.FsTimeVal.u8Sec));
        pPtpSysTimeInfo->FsClkTimeVal.u4Nsec = PtpHwLog.FsTimeVal.u4Nsec;
    }
#else
    UNUSED_PARAM (pPtpPort);
    UNUSED_PARAM (pu1Pdu);
    UNUSED_PARAM (pPtpSysTimeInfo);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Mode);
#endif
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpClkSynchornize                             */
/*                                                                           */
/* Description               : This routine will be invoked to synchronize   */
/*                             the local clock with a MASTER in the network. */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpClkSynchornize (tPtpPort * pPtpPort)
{
    tPtpCxt            *pPtpCxt = NULL;
    tClkSysTimeInfo     SysTime;
    tPtpNpWrCfgHwClk    PtpNpWrCfgHwClk;
    FS_UINT8            u8Nsec;
    FS_UINT8            u8MeanDelay;
    FS_UINT8            u8NsecResult;
    FS_UINT8            u8NsecReminder;
    FS_UINT8            u8MeanPathDelay;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8Result;
    FS_UINT8            u8NanoSecValue;
    UINT4               u4Nsec = 0;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8MeanDelay);
    FSAP_U8_CLR (&u8MeanPathDelay);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8Result);
    FSAP_U8_CLR (&u8NanoSecValue);
    FSAP_U8_CLR (&u8Nsec);
    FSAP_U8_CLR (&u8NsecResult);
    FSAP_U8_CLR (&u8NsecReminder);
    MEMSET (&SysTime, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpNpWrCfgHwClk, 0, sizeof (tPtpNpWrCfgHwClk));

    /* ASSUMPTION
     * 1) This routine should be invoked after calculating the offsetFromMaster
     *    based on the message type.
     * 2) This routine will be responsible for synchronizing the local clock
     *    alone.
     * */
    UINT8_LO (&(u8NanoSecValue)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Setting the local clock time....\r\n"));

    pPtpCxt = PtpCxtGetNode (gPtpGlobalInfo.u4PrimaryContextId);

    if (pPtpCxt == NULL)
    {
        /* Invalid Context Identifier. Hence not updating the clock
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Invalid Primary Context Id....\r\n"));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Not Programming the System Clock...\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if ((pPtpPort->u4ContextId != gPtpGlobalInfo.u4PrimaryContextId) ||
        (pPtpPort->u1DomainId != pPtpCxt->u1PrimaryDomainId))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpClkSynchornize:Invalid Context Or Domain Information\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }
    FSAP_U8_ASSIGN (&u8MeanDelay,
                    &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                      u8OffsetFromMaster));
    if ((UINT8_LO (&u8MeanDelay) == 0) && (UINT8_HI (&u8MeanDelay) == 0))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpClkSynchornize:Mean Delay Is Zero\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    /* Offset From Master represents a value that is absolute difference
     * between the slave node clock and Master Node clock. Hence, if we
     * are programming the slave node, First get the current time of the
     * clock and do +/- Offset based on the calculated value and program
     * the clock
     * */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Calculated Value of OffsetFromMaster is %u%u\r\n",
              u8MeanDelay.u4Hi, u8MeanDelay.u4Lo));

    if (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_PEER_TO_PEER)
    {
        /* The value of MeanPathDelay will be interms of nanoseconds. 
         * change the same.
         * */
        FSAP_U8_ASSIGN (&(u8MeanPathDelay),
                        &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                          u8MeanPathDelay));

        if ((UINT8_LO (&u8MeanPathDelay) == 0) &&
            (UINT8_HI (&u8MeanPathDelay) == 0))
        {
            /* Before you obtain mean path delay, do not program the clock.
             * Here the Peer Delay value is not taken into account as the mean
             * path delay would have been calculated even before the port moves
             * to listening, & SLAVE state and receives this Sync message.
             * */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "PtpClkSynchornize:Mean Delay Is Zero\r\n"));
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
    PTP_U8_DIV (&u8Result, &u8Reminder, &(u8MeanDelay), &(u8NanoSecValue));

    SysTime.u4ContextId = pPtpPort->u4ContextId;
    SysTime.u1DomainId = pPtpPort->u1DomainId;
    if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TIMESTAMPING)
    {
#ifdef NPAPI_WANTED
        if (PtpNpWrGetHwClk (pPtpPort->u4IfIndex, &SysTime) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpClkSynchornize:PtpNpWrGetHwClk FAILED.\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
#endif
    }
    else
    {
        PtpPortClkIwfGetClock (&SysTime);
    }

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.i1OffsetSign ==
        PTP_NEGATIVE)
    {
        /* On adding calculated Offset Nanoseconds (u8Reminder) to system's
         * Nanoseconds ( SysTime.FsClkTimeVal.u4Nsec ) if the result
         * exceeds one second we add the resulted second to system seconds and 
         * resulted nanoseconds (u8NsecReminder) to system nanoseconds*/

        FSAP_U8_ADD (&(SysTime.FsClkTimeVal.u8Sec),
                     &(SysTime.FsClkTimeVal.u8Sec), &(u8Result));

        u4Nsec = SysTime.FsClkTimeVal.u4Nsec + u8Reminder.u4Lo;
        FSAP_U8_ASSIGN_LO (&u8Nsec, u4Nsec);
        PTP_U8_DIV (&(u8NsecResult), &u8NsecReminder,
                    &u8Nsec, &(u8NanoSecValue));
        FSAP_U8_ADD (&(SysTime.FsClkTimeVal.u8Sec),
                     &(SysTime.FsClkTimeVal.u8Sec), &(u8NsecResult));
        SysTime.FsClkTimeVal.u4Nsec = (UINT4) u8NsecReminder.u4Lo;
    }
    else
    {
        if (FSAP_U8_CMP (&(SysTime.FsClkTimeVal.u8Sec), &(u8Result)) == 1)
        {
            FSAP_U8_SUB (&(SysTime.FsClkTimeVal.u8Sec),
                         &(SysTime.FsClkTimeVal.u8Sec), &u8Result);
        }
        else
        {
            FSAP_U8_SUB (&(SysTime.FsClkTimeVal.u8Sec), &u8Result,
                         &(SysTime.FsClkTimeVal.u8Sec));
        }
        if ((SysTime.FsClkTimeVal.u4Nsec) > u8Reminder.u4Lo)
        {
            SysTime.FsClkTimeVal.u4Nsec =
                SysTime.FsClkTimeVal.u4Nsec - u8Reminder.u4Lo;
        }
        else
        {
            SysTime.FsClkTimeVal.u4Nsec = u8Reminder.u4Lo -
                SysTime.FsClkTimeVal.u4Nsec;
        }

    }

    if (gPtpGlobalInfo.TimeStamp == PTP_SOFTWARE_TIMESTAMPING)
    {
        if (PtpPortSetClk (&SysTime,
                           pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.
                           u1TimeSource) != OSIX_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "Unable to set system timing upon "
                      "receipt of sync message in Port : %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
    else
    {
        PtpNpWrCfgHwClk.u4IfIndex = pPtpPort->u4IfIndex;
        PtpNpWrCfgHwClk.pPtpSysTimeInfo = &SysTime;
        FSAP_U8_ASSIGN (&(PtpNpWrCfgHwClk.u8Offset), &u8MeanDelay);
        PtpNpWrCfgHwClk.i1OffsetSign =
            pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.i1OffsetSign;

        if (PtpNpWrConfigHwClk (&PtpNpWrCfgHwClk) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "Unable to set system timing upon "
                      "receipt of sync message in Port : %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
        /* Get the clock from hardware and set it in clkiwf. */
        MEMSET (&SysTime, 0, sizeof (SysTime));
        SysTime.u4ContextId = pPtpPort->u4ContextId;
        SysTime.u1DomainId = pPtpPort->u1DomainId;

        if (PtpNpWrGetHwClk (pPtpPort->u4IfIndex, &SysTime) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "PtpClkSynchornize:"
                      "PtpNpWrGetHwClk NP Wrapper Failed !!!\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
        if (PtpPortSetClk (&SysTime,
                           pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.
                           u1TimeSource) != OSIX_SUCCESS)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "Unable to set system timing upon "
                      "receipt of sync message in Port : %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "PtpClkSynchornize Returns Success\r\n"));
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpClkSyntonizeClk                            */
/*                                                                           */
/* Description               : This routine will be invoked to syntonize     */
/*                             the local clock with a MASTER in the network. */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpClkSyntonizeClk (tPtpPort * pPtpPort, FS_UINT8 * pu8FollowupCorrnField)
{
    tPtpCurrentDs      *pPtpCurrentDs = NULL;
    tPtpCxt            *pPtpCxt = NULL;
    tClkSysTimeInfo     PtpSysTimeInfo;
    FS_UINT8            u8CorrectedMasterTimeStamp;
    FS_UINT8            u8SyncDiff;
    FS_UINT8            u8MasterTimeStampDiff;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8Result;
    FS_UINT8            u8CurrentTime;
    FS_UINT8            u8NanoSecValue;
    FS_UINT8            u8OriginSyncTimeStamp;
    FS_UINT8            u8IngressSyncTimeStampNsec;
    FS_UINT8            u8TempIngressSyncTime;
    FS_UINT8            u8ResultIngressSyncTime;
    static FS_UINT8     u8NthResultIngressSyncTime;
    static FS_UINT8     u8NthCorrectedMasterTimeStamp;
    FS_UINT8            u8SyntRatioDiff;
    FS_UINT8            u8TempSyntCalc;
    INT1                i1SyncDiffSign = 0;
    INT1                i1MasterTSSign = 0;

    PTP_FN_ENTRY ();

    /* Refer Section 12.1.2 of IEEE Std 1588 2008 
     * */
    FSAP_U8_CLR (&u8CorrectedMasterTimeStamp);
    FSAP_U8_CLR (&u8SyncDiff);
    FSAP_U8_CLR (&u8MasterTimeStampDiff);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8Result);
    FSAP_U8_CLR (&u8CurrentTime);
    FSAP_U8_CLR (&u8NanoSecValue);
    FSAP_U8_CLR (&u8IngressSyncTimeStampNsec);
    FSAP_U8_CLR (&u8OriginSyncTimeStamp);
    FSAP_U8_CLR (&u8TempIngressSyncTime);
    FSAP_U8_CLR (&u8ResultIngressSyncTime);
    FSAP_U8_CLR (&u8SyntRatioDiff);
    FSAP_U8_CLR (&u8TempSyntCalc);

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    UINT8_LO (&(u8NanoSecValue)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;

    pPtpCxt = PtpCxtGetNode (gPtpGlobalInfo.u4PrimaryContextId);

    if (pPtpCxt == NULL)
    {
        /* Invalid Context Identifier. Hence not updating the clock
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Invalid Primary Context Id....\r\n"));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Not Performing Syntonization...\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if ((pPtpPort->u4ContextId != gPtpGlobalInfo.u4PrimaryContextId) &&
        (pPtpPort->u1DomainId != pPtpCxt->u1PrimaryDomainId))
    {
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Syntonizing the local clock....\r\n"));

    pPtpCurrentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs);

    /* The port is having <syncIngressEventTimeStamp> recorded for the current
     * Message. Use the following set of formulas to calculate the syntonization
     * ratio
     *        <syncEventIngressTimeStamp>N - <syncEventIngressTimeStamp>O
     * _________________________________________________________________________
     *     
     *     <correctedMasterEventTimeStamp>N - <correctedMasterEventTimeStamp>O
     *
     * where,
     * <correctedMasterEventTimeStamp> = <originTimeStamp> + <meanPathDelay> +
     *                                   <correctionField of Sync Message>   +
     *                                   <correctionField of Followup message>
     * */

    /* Initialize the static variables for the first iteration */
    if ((UINT8_LO (&(pPtpPort->NthIngressSyncTimeStamp.u8Sec)) == 0) &&
        (UINT8_HI (&(pPtpPort->NthIngressSyncTimeStamp.u8Sec)) == 0) &&
        (pPtpPort->NthIngressSyncTimeStamp.u4Nsec == 0))
    {
        FSAP_U8_CLR (&u8NthResultIngressSyncTime);
        FSAP_U8_CLR (&u8NthCorrectedMasterTimeStamp);
    }
    if (pPtpPort->PtpPortStats.u4RcvdSyncMsgCnt %
        PTP_SYTONIZE_CALCULATION_INTERVAL == 0)
    {
        /* The ratio needs to be calculated at every Nth interval as per 
         * section 12.2 of IEEE Std 1588 2008. Choosing 
         * PTP_SYTONIZE_CALCULATION_INTERVAL as Nth interval
         * */

        /* Adding Ingress time stamp and Calculated Mean path delay
         * */
        /* Converting the Seconds to Nano Seconds */

        /* Numerator part 0th sync receipt time */
        FSAP_U8_MUL (&u8TempIngressSyncTime,
                     &(pPtpPort->SyncReceptTime.u8Sec), &(u8NanoSecValue));
        FSAP_U8_ASSIGN_LO (&u8IngressSyncTimeStampNsec,
                           (pPtpPort->SyncReceptTime.u4Nsec));

        FSAP_U8_ADD (&u8ResultIngressSyncTime,
                     &u8TempIngressSyncTime, &(u8IngressSyncTimeStampNsec));
        FSAP_U8_CLR (&u8TempIngressSyncTime);

        /* Denominator part 0th corrected master time */

        FSAP_U8_MUL (&u8TempIngressSyncTime,
                     &(pPtpPort->IngressSyncTimeStamp.u8Sec),
                     &(u8NanoSecValue));
        FSAP_U8_ASSIGN_LO (&u8OriginSyncTimeStamp,
                           (pPtpPort->IngressSyncTimeStamp.u4Nsec));

        FSAP_U8_ADD (&u8CorrectedMasterTimeStamp,
                     &u8TempIngressSyncTime, &(u8OriginSyncTimeStamp));

        FSAP_U8_ADD (&u8CorrectedMasterTimeStamp,
                     &(u8CorrectedMasterTimeStamp),
                     &(pPtpCurrentDs->u8MeanPathDelay));

        /* Adding the sync correction field */
        FSAP_U8_ADD (&u8CorrectedMasterTimeStamp,
                     &(pPtpPort->u8CorrectionField),
                     &u8CorrectedMasterTimeStamp);

        if (pu8FollowupCorrnField != NULL)
        {
            /* This is being invoked from sync message transmitted by
             * Two Step Clock. Hence Add the follow up correction field
             * Also
             * */
            FSAP_U8_ADD (&u8CorrectedMasterTimeStamp,
                         pu8FollowupCorrnField, &u8CorrectedMasterTimeStamp);
        }

        /* Subtracting Sync differnce */
        PtpClkPerf8ByteSubtraction (&u8ResultIngressSyncTime,
                                    &u8NthResultIngressSyncTime,
                                    &u8SyncDiff, &i1SyncDiffSign);

        PtpClkPerf8ByteSubtraction (&u8CorrectedMasterTimeStamp,
                                    &u8NthCorrectedMasterTimeStamp,
                                    &u8MasterTimeStampDiff, &i1MasterTSSign);

        /* Update the correctedMasterTimeStamp and SyncIngressTimeStamp
         * in the static variables for later usage
         * */
        MEMCPY (&u8NthCorrectedMasterTimeStamp,
                &u8CorrectedMasterTimeStamp, sizeof (FS_UINT8));
        /* Nth SyncIngressEventTimeStamp */
        MEMCPY (&u8NthResultIngressSyncTime,
                &u8ResultIngressSyncTime, sizeof (FS_UINT8));

        MEMCPY (&(pPtpPort->NthIngressSyncTimeStamp),
                &pPtpPort->IngressSyncTimeStamp, sizeof (tFsClkTimeVal));

        if ((UINT8_LO (&u8MasterTimeStampDiff) != 0)
            || (UINT8_HI (&u8MasterTimeStampDiff) != 0))
        {
            /* Exception case handling to avoid Divide by zero problem */
            FSAP_U8_CLR (&(pPtpPort->PortDs.u8SyntRatio));
            if (OSIX_FALSE ==
                PtpUtilFixedPtDiv (&(pPtpPort->PortDs.u8SyntRatio), &u8SyncDiff,
                                   &u8MasterTimeStampDiff))
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "Unable to perform Fixed point Division\r\n"));
                return OSIX_SUCCESS;
            }
        }
        else
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Div By Zero Handling - SyncDiff Value is Zero\r\n"));
            return OSIX_SUCCESS;
        }

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Calculated Syntonization Ratio for port %d is... "
                  "%d%d.\r\n", pPtpPort->PortDs.u4PtpPortNumber,
                  pPtpPort->PortDs.u8SyntRatio.u4Hi,
                  pPtpPort->PortDs.u8SyntRatio.u4Lo));
        /* pPtpPort->PortDs.u8SyntRatio Contains the syntonization ratio
         * which is calculated per sync interval. Now, calculate Syntonizatio
         * ratio per second. The formula is 
         * SyntRatePerSecond = ((1 ~ SyntRatio)* (10^9) / SyncInterval) */
        FSAP_U8_CLR (&u8TempIngressSyncTime);

        u8TempIngressSyncTime.u4Hi = 0;
        u8TempIngressSyncTime.u4Lo = 1;
        PtpConvertToFixedPt (&u8TempIngressSyncTime, &u8TempIngressSyncTime);
        /* pPtpPort->PortDs.u8SyntRatio Contains the syntonization ratio. Now
         * determine the Sign of the syntonization ratio based on the value
         * pPtpPort->PortDs.i1SyntRatioSign. If u8SyntRatio is greater than 1,
         * i1SyntRatioSign shall be -ve. Otherwise, i1SyntRatioSign shall be 
         * +ve */

        PtpUtilFixedPtSub (&u8TempSyntCalc,
                           &(pPtpPort->PortDs.i1SyntRatioSign),
                           &u8TempIngressSyncTime,
                           &(pPtpPort->PortDs.u8SyntRatio));

        FSAP_U8_CLR (&u8TempIngressSyncTime);
        u8TempIngressSyncTime.u4Hi = 0;
        UINT8_LO (&(u8TempIngressSyncTime)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
        if (OSIX_FALSE ==
            PtpConvertToFixedPt (&u8TempIngressSyncTime,
                                 &u8TempIngressSyncTime))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Unable to perform Fixed point Conversion\r\n"));
            return OSIX_SUCCESS;
        }
        if (OSIX_FALSE ==
            PtpUtilFixedPtMul (&u8TempSyntCalc, &u8TempSyntCalc,
                               &u8TempIngressSyncTime))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Unable to perform Fixed point Multiplication\r\n"));
            return OSIX_SUCCESS;
        }
        if (OSIX_FALSE ==
            PtpConvertToFixedPt (&u8MasterTimeStampDiff,
                                 &u8MasterTimeStampDiff))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Unable to perform Fixed point Conversion\r\n"));
            return OSIX_SUCCESS;
        }
        if ((UINT8_LO (&u8MasterTimeStampDiff) != 0)
            || (UINT8_HI (&u8MasterTimeStampDiff) != 0))
        {
            if (OSIX_FALSE ==
                PtpUtilFixedPtDiv (&(pPtpPort->PortDs.u8RatioPerSec),
                                   &u8TempSyntCalc, &u8MasterTimeStampDiff))
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "Unable to perform Fixed point Division\r\n"));
                return OSIX_SUCCESS;
            }
        }
        else
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Div By Zero Handling - u8MasterTimeStampDiff Value is Zero\r\n"));
            return OSIX_SUCCESS;
        }

        /* Get current time */
        if (PtpPortClkIwfGetClock (&PtpSysTimeInfo) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Unable to obtain Time stamp.\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        /* Converting the Seconds into Nano Second */
        FSAP_U8_MUL (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec),
                     &(PtpSysTimeInfo.FsClkTimeVal.u8Sec), &(u8NanoSecValue));

        /* Subtracting the Nano Second */
        (UINT8_LO (&u8CurrentTime)) =
            ((UINT8_LO (&PtpSysTimeInfo.FsClkTimeVal.u8Sec)) +
             (PtpSysTimeInfo.FsClkTimeVal.u4Nsec));

        /* Calculate Rate Adjustment factor 
         * Rate = ((syntRatio/currentTime) * 100) */
        FSAP_U8_CLR (&u8Reminder);
        PTP_U8_DIV (&u8Result, &u8Reminder, &(pPtpPort->PortDs.u8SyntRatio),
                    &u8CurrentTime);
        pPtpPort->PortDs.u4RateAdj = (UINT8_LO (&u8Result)) * 100;

        if (pPtpPort->PortDs.i1SyntRatioSign == PTP_NEGATIVE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Slave Clock is leading the Master Clock.....\r\n"));
        }
        else
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Slave Clock is lagging the Master Clock.....\r\n"));
        }
    }

    /* Configure the hardware only if the port is in primary context and
     * primary domain
     * */
    PtpNpWrConfigRateAdj (pPtpPort);
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpClkUpdtMasterToSlaveDelay                  */
/*                                                                           */
/* Description               : This routine will be invoked to calculate the */
/*                             the master to Slave delay for the port.       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             pu8TimeDiff - Pointer to Seconds and Nano     */
/*                             difference.(In Nano Seconds)                  */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpClkUpdtMasterToSlaveDelay (tPtpPort * pPtpPort, FS_UINT8 * pu8TimeDiff)
{
    UINT4              *pu4CurrMasToSlaveDelay = NULL;

    PTP_FN_ENTRY ();

    pu4CurrMasToSlaveDelay = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                               CurrentDs.u4CurrentMasterToSlaveDelay);

    *pu4CurrMasToSlaveDelay = *pu4CurrMasToSlaveDelay + pu8TimeDiff->u4Lo;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpClkUpdtSlaveToMasterDelay                  */
/*                                                                           */
/* Description               : This routine will be invoked to calculate the */
/*                             the Slave to Master delay for the port.       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             pu8TimeDiff - Pointer to time diff in Nano    */
/*                             Seconds.                                      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpClkUpdtSlaveToMasterDelay (tPtpPort * pPtpPort, FS_UINT8 * pu8TimeDiff)
{
    UINT4              *pu4CurrSlaveToMasDelay = NULL;
    FS_UINT8            u8Result;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8Result);

    /* The function has Delay Response ingress time stamp. Delay request
     * origin time stamp is stored in the data base already. Use the same
     * for calculation and update the values of Slave to Master Delay
     * */
    FSAP_U8_SUB (&u8Result, pu8TimeDiff, &(pPtpPort->DelayReqOriginTime.u8Sec));

    pu4CurrSlaveToMasDelay = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                               CurrentDs.u4CurrentSlaveToMasterDelay);

    if ((UINT8_LO (&u8Result) == 0) && (UINT8_HI (&u8Result) == 0))
    {
        UINT8_LO (&u8Result) = PTP_MAX_U4_VAL;
        UINT8_HI (&u8Result) = PTP_MAX_U4_VAL;
        *pu4CurrSlaveToMasDelay = PTP_MAX_U4_VAL;
        PTP_FN_EXIT ();
        return;
    }
    *pu4CurrSlaveToMasDelay = *pu4CurrSlaveToMasDelay + pu8TimeDiff->u4Lo;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpClkPerf8ByteSubtraction                    */
/*                                                                           */
/* Description               : This routine performs the INT8 subtraction    */
/*                             for two UINT8 values. The value of pi1Sign if */
/*                             the resultant UINT8 value is negative will be */
/*                             -1. Otherwise, it will take the value of +1.  */
/*                                                                           */
/* Input                     : pu8Val1              -  Value A               */
/*                             pu8Val2              -  Value B               */
/*                                                     key id                */
/* Output                    : pi1Sign              -  Sign Indicator.       */
/*                             pu8Result            - Result of the operation*/
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpClkPerf8ByteSubtraction (FS_UINT8 * pu8Val1, FS_UINT8 * pu8Val2,
                            FS_UINT8 * pu8Result, INT1 *pi1Sign)
{
    PTP_FN_ENTRY ();

    *pi1Sign = PTP_POSITIVE;

    if (FSAP_U8_CMP (pu8Val1, pu8Val2) < 0)
    {
        /* Value 1 is lesser than Value 2. Perform Val2 - Val1 and set the sign
         * bit as true.
         * */
        FSAP_U8_SUB (pu8Result, pu8Val2, pu8Val1);
        *pi1Sign = PTP_NEGATIVE;
    }
    else if (FSAP_U8_CMP (pu8Val1, pu8Val2) > 0)
    {
        FSAP_U8_SUB (pu8Result, pu8Val1, pu8Val2);
    }
    else
    {
        /* This means equal values */
        FSAP_U8_SUB (pu8Result, pu8Val1, pu8Val2);
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpClkSubtractTimeStamps                      */
/*                                                                           */
/* Description               : This routine performs subtraction for two     */
/*                             different time stamps provided. This tries to */
/*                             subtract two different time stamps in the     */
/*                             following method. Secs:NanoSecs format.       */
/*                                                                           */
/* Input                     : pTimeStamp1 - Time Stamp 1.                   */
/*                             pTimeStamp2 - Time Stamp 2.                   */
/*                                                                           */
/* Output                    : pTimeStampOutput - Output Time Stamp.         */
/*                             pi1Sign          - Sign of the output.        */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpClkSubtractTimeStamps (tFsClkTimeVal * pTimeStamp1,
                          tFsClkTimeVal * pTimeStamp2,
                          tFsClkTimeVal * pTimeStampOutput, INT1 *pi1Sign)
{
    INT1                i1Sign = PTP_POSITIVE;

    *pi1Sign = PTP_POSITIVE;

    /* The time stamp subtraction is performed by the following method. 
     * First identify which of the seconds field is greater. 
     * If pTimeStamp1->seconds is greater than pTimeStamp2->seconds, try to
     * perform normal subtraction. But in this case, if nano seconds field of
     * pTimeStamp1 is lesser than pTimeStamp2, then first increment the MSB of
     * Most Significant byte of the u4Nanoseconds and decrement seconds field 
     * of pTimeStamp1 by 1
     * */

    PtpClkPerf8ByteSubtraction (&(pTimeStamp1->u8Sec), &(pTimeStamp2->u8Sec),
                                &(pTimeStampOutput->u8Sec), pi1Sign);

    if (pTimeStamp1->u4Nsec < pTimeStamp2->u4Nsec)
    {
        pTimeStampOutput->u4Nsec = pTimeStamp2->u4Nsec - pTimeStamp1->u4Nsec;
        i1Sign = PTP_NEGATIVE;
    }
    else
    {
        pTimeStampOutput->u4Nsec = pTimeStamp1->u4Nsec - pTimeStamp2->u4Nsec;
    }

    /* Normalize the output value */
    if ((UINT8_LO (&(pTimeStampOutput->u8Sec)) == 0) &&
        (UINT8_HI (&(pTimeStampOutput->u8Sec)) == 0))
    {
        /* Value of seconds is ZERO, cannot normalize */
        *pi1Sign = i1Sign;
        return;
    }

    if ((*pi1Sign == PTP_POSITIVE) && (i1Sign == PTP_NEGATIVE))
    {
        UINT8_LO (&(pTimeStampOutput->u8Sec)) =
            UINT8_LO (&(pTimeStampOutput->u8Sec)) - 1;
        pTimeStampOutput->u4Nsec =
            PTP_NANO_SEC_TO_SEC_CONV_FACTOR - pTimeStampOutput->u4Nsec;
    }
}

/*****************************************************************************/
/* Function                  : PtpClkCalcScalNanoSecs                        */
/*                                                                           */
/* Description               : This routine calculates the value of scaled   */
/*                             nano seconds for the given nano second value. */
/*                                                                           */
/* Input                     : u4NanoSecVal - Nanosecond value.              */
/*                                                                           */
/* Output                    : pu8CorrnField - Calculated Value.             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpClkCalcScalNanoSecs (FS_UINT8 u8NanoSecVal, FS_UINT8 * pu8CorrnField)
{
    FS_UINT8            u8DispConst;
    FSAP_U8_CLR (&u8DispConst);
    UINT8_LO (&u8DispConst) = PTP_DISPLAY_CONSTANT;
    FSAP_U8_MUL (pu8CorrnField, &u8NanoSecVal, &u8DispConst);
}

/*****************************************************************************/
/* Function                  : PtpClkExtractCorrnField                       */
/*                                                                           */
/* Description               : This routine calculates the value in seconds  */
/*                             given the value in scaled nano seconds.       */
/*                                                                           */
/* Input                     : pu8NanoSec - Scaled nano seconds.             */
/*                                                                           */
/* Output                    : pu8CorrnField - Calculated Value.             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpClkExtractCorrnField (FS_UINT8 * pu8NanoSec, FS_UINT8 * pu8CorrnField)
{
    FS_UINT8            u8Value;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8DispConst;

    FSAP_U8_CLR (&u8Value);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8DispConst);

    UINT8_LO (&u8DispConst) = PTP_DISPLAY_CONSTANT;

    PTP_U8_DIV (&u8Value, &u8Reminder, pu8NanoSec, &u8DispConst);
    /* The output will be in terms of nano seconds */
    FSAP_U8_ASSIGN (pu8CorrnField, &u8Value);
}
#endif /* _PTPCLK_C_ */
