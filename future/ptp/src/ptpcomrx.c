/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 * $Id: ptpcomrx.c,v 1.5 2014/02/03 12:27:07 siva Exp $
 *
 * Description: This file contains PTP Compatability Sub module functionality
 *              routines for Rx.
 *********************************************************************/
#ifndef _PTPCOMRX_C_
#define _PTPCOMRX_C_

#include "ptpincs.h"

PRIVATE INT4
 
 
 
 PtpComRxV1SyncToV2AnncSync (tPtpCompRxInfo * pPtpCompRxInfo,
                             tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info);
PRIVATE INT4
 
 
 
 PtpComRxV1DReqToV2DReq (tPtpCompRxInfo * pPtpCompRxInfo,
                         tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info);
PRIVATE INT4
 
 
 
 PtpComRxV1FolloUpToV2FollowUp (tPtpCompRxInfo * pPtpCompRxInfo,
                                tPtpv1Info * pPtpv1Info,
                                tPtpv2Info * pPtpv2Info);
PRIVATE INT4        PtpComRxV1DRespToV2DResp (tPtpCompRxInfo * pPtpCompRxInfo,
                                              tPtpv1Info * pPtpv1Info,
                                              tPtpv2Info * pPtpv2Info);
PRIVATE VOID        PtpComRxGetV1InfoFromPkt (UINT1 *pu1V1Pdu,
                                              tPtpv1Info * pPtpv1Info);
PRIVATE INT4        PtpComRxMapV1InfoToV2CommonInfo (tPtpv1Info * pPtpv1Info,
                                                     tPtpv2Info * pPtpv2Info);
PRIVATE INT4        PtpComRxMapV1InfoToV2Info (tPtpv1Info * pPtpv1Info,
                                               tPtpv2Info * pPtpv2Info);
PRIVATE VOID        PtpComRxPackV2PtpPkt (UINT2 u2PktType,
                                          tPtpv2Info * pPtpv2Info,
                                          UINT1 *pu1Ptpv2Msg);

/*****************************************************************************/
/* Function     : PtpComRxProcessV1Messages                                  */
/*                                                                           */
/* Description  : This function is triggered from the PTP Message Handler sub*/
/*                module whenever it receives the PTP version 1 Packet on its*/
/*                PTP Ports, only if the clock is boundary clock.            */
/*                It Verifies the port PTP protocol version is vresion1,     */
/*                if not same return failure else translate the Version1 of  */
/*                the incoming packet to Version2 of the type of received    */
/*                Message type. Transform the version1 sync message to       */
/*                version2 Announce and Sync message trigger respective      */
/*                Message handler of version2 Protocol handler. Transform the*/
/*                version1 Follow_up, delay request, delay response message  */
/*                to version2 Follow_up, delay request, delay response,      */
/*                respectively and call corresponding Message handler of     */
/*                version2 Protocol handler.                                 */
/*                                                                           */
/* Input        : pPtpPort    - PTP Port Pointer                             */
/*              : pPtpPktParams - Received Packet Info                       */
/*                                                                           */
/* Output       : Trigger the V2 Message handlers                            */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpComRxProcessV1Messages (tPtpPort * pPtpPort, tPtpPktParams * pPtpPktParams)
{
    tPtpCompRxInfo      PtpCompRxInfo;
    tPtpv1Info         *pPtpv1Info = NULL;
    tPtpv2Info         *pPtpv2Info = NULL;
    UINT1              *pu1TmpPtpPdu = NULL;
    UINT4               u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    INT4                i4Status = OSIX_SUCCESS;
    UINT2               u2PtpVersion = PTP_MESSAGE_VERSION_ONE;
    UINT1               u1PtpV1Control = PTP_MESSAGE_VERSION_ONE;

    PTP_FN_ENTRY ();

    pu1TmpPtpPdu = pPtpPktParams->pu1RcvdPdu + u4MediaHdrLen;

    MEMSET ((&PtpCompRxInfo), 0, sizeof (tPtpCompRxInfo));

    /* Assign the Globle V1 and V2 info */
    pPtpv1Info = &(gPtpGlobalInfo.Ptpv1Info);
    pPtpv2Info = &(gPtpGlobalInfo.Ptpv2Info);

    PtpCompRxInfo.Rx.pPtpPort = pPtpPort;
    PtpCompRxInfo.Rx.u4Ptpv1PduLen = pPtpPktParams->u4PktLen;
    PtpCompRxInfo.Rx.pu1Ptpv1Pdu = pPtpPktParams->pu1RcvdPdu;

    PTP_LBUF_GET_2_BYTES (pu1TmpPtpPdu, u2PtpVersion);
    if (u2PtpVersion != PTP_MESSAGE_VERSION_ONE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxProcessV1Messages : Packet is not PTP "
                  "v1 Message.\r\n"));
        /* Release the v1 Msg Buffer */
        PtpCompFreeBuddy (&PtpCompRxInfo);
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* translate the Version1 of the incoming packet to Version2
     * of the type of received Message type.
     */
    pu1TmpPtpPdu = (pPtpPktParams->pu1RcvdPdu + u4MediaHdrLen +
                    PTP_V1_CONTROL_FIELD_OFFSET);
    PTP_LBUF_GET_1_BYTE (pu1TmpPtpPdu, u1PtpV1Control);

    /* Allocate the Buddy Memory for the new packets (v2 Packets)  */
    i4Status = PtpCompAllocBuddy (&PtpCompRxInfo, u1PtpV1Control);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpComRxProcessV1Messages : PtpCompAllocBuddy failed.\r\n"));
        /* Release the v1 Msg Buffer */
        PtpCompFreeBuddy (&PtpCompRxInfo);
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    if (u4MediaHdrLen != 0)
    {
        /* Copy the LL header */
        MEMCPY (PtpCompRxInfo.Rx.pu1Ptpv2Pdu, PtpCompRxInfo.Rx.pu1Ptpv1Pdu,
                u4MediaHdrLen);

        if (u1PtpV1Control == PTP_V1_SYNC_MESSAGE)
        {
            MEMCPY (PtpCompRxInfo.pu1Ptpv2AnncPdu, PtpCompRxInfo.Rx.pu1Ptpv1Pdu,
                    u4MediaHdrLen);
        }
    }

    switch (u1PtpV1Control)
    {
        case PTP_V1_SYNC_MESSAGE:
            i4Status =
                PtpComRxV1SyncToV2AnncSync (&PtpCompRxInfo, pPtpv1Info,
                                            pPtpv2Info);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Trigger v2 announce message handler */
                pPtpPktParams->u4PktLen = PtpCompRxInfo.u4Ptpv2AnncPduLen;
                pPtpPktParams->pu1RcvdPdu = PtpCompRxInfo.pu1Ptpv2AnncPdu;

                i4Status = (gPtpGlobalInfo.PtpMsgFn[PTP_FP_ANNC_MSG_TYPE].
                            pMsgRxHandler (pPtpPktParams));
                if (i4Status == OSIX_FAILURE)
                {
                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              ALL_FAILURE_TRC, "PtpComRxProcessV1Messages : "
                              "Announce Message returned Failure!!\r\n"));
                }
                /* Trigger v2 sync message handler */
                pPtpPktParams->u4PktLen = PtpCompRxInfo.Rx.u4Ptpv2PduLen;
                pPtpPktParams->pu1RcvdPdu = PtpCompRxInfo.Rx.pu1Ptpv2Pdu;

                i4Status = (gPtpGlobalInfo.PtpMsgFn[PTP_FP_SYNC_MSG_TYPE].
                            pMsgRxHandler (pPtpPktParams));
                if (i4Status == OSIX_FAILURE)
                {
                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              ALL_FAILURE_TRC, "PtpComRxProcessV1Messages : "
                              "Announce Message returned Failure!!\r\n"));
                }
            }
            break;

        case PTP_V1_DELAY_REQ_MESSAGE:
            i4Status = PtpComRxV1DReqToV2DReq (&PtpCompRxInfo, pPtpv1Info,
                                               pPtpv2Info);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Trigger v2 Delay Request message handler */
                pPtpPktParams->u4PktLen = PtpCompRxInfo.Rx.u4Ptpv2PduLen;
                pPtpPktParams->pu1RcvdPdu = PtpCompRxInfo.Rx.pu1Ptpv2Pdu;

                i4Status = (gPtpGlobalInfo.PtpMsgFn[PTP_FP_DREQ_MSG_TYPE].
                            pMsgRxHandler (pPtpPktParams));
                if (i4Status == OSIX_FAILURE)
                {
                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              ALL_FAILURE_TRC,
                              "PtpComRxProcessV1Messages : "
                              "Delay Request Message returned Failure!!\r\n"));
                }
            }
            break;

        case PTP_V1_FOLLOWUP_MESSAGE:
            i4Status = PtpComRxV1FolloUpToV2FollowUp (&PtpCompRxInfo,
                                                      pPtpv1Info, pPtpv2Info);

            if (i4Status == OSIX_SUCCESS)
            {
                /* Trigger v2 followup message handler */
                pPtpPktParams->u4PktLen = PtpCompRxInfo.Rx.u4Ptpv2PduLen;
                pPtpPktParams->pu1RcvdPdu = PtpCompRxInfo.Rx.pu1Ptpv2Pdu;

                i4Status = (gPtpGlobalInfo.PtpMsgFn[PTP_FP_SYNC_MSG_TYPE].
                            pMsgRxHandler (pPtpPktParams));
                if (i4Status == OSIX_FAILURE)
                {
                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              ALL_FAILURE_TRC,
                              "PtpComRxProcessV1Messages : "
                              "Follow up Message returned Failure!!\r\n"));
                }
            }
            break;

        case PTP_V1_DELAY_RESP_MESSAGE:
            i4Status = PtpComRxV1DRespToV2DResp (&PtpCompRxInfo, pPtpv1Info,
                                                 pPtpv2Info);
            if (i4Status == OSIX_SUCCESS)
            {
                /* Trigger v2 Delay Response message handler */
                pPtpPktParams->u4PktLen = PtpCompRxInfo.Rx.u4Ptpv2PduLen;
                pPtpPktParams->pu1RcvdPdu = PtpCompRxInfo.Rx.pu1Ptpv2Pdu;

                i4Status = (gPtpGlobalInfo.PtpMsgFn[PTP_FP_DRESP_MSG_TYPE].
                            pMsgRxHandler (pPtpPktParams));
                if (i4Status == OSIX_FAILURE)
                {
                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              ALL_FAILURE_TRC, "PtpComRxProcessV1Messages : "
                              "Delay Response Message returned Failure!!\r\n"));
                }
            }
            break;

        case PTP_V1_MANAGEMENT_MESSAGE:
        case PTP_V1_SYNC_MESSAGE_BURST:
        case PTP_V1_DELAY_REQ_MESSAGE_BURST:
        default:
            i4Status = OSIX_FAILURE;
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "PtpComRxProcessV1Messages: InvalidMsgType.\r\n"));
            break;
    }

    /* Release the Buddy Memory for the new packets (v2 Packets) and 
     * v1 packtet Received  */
    PtpCompFreeBuddy (&PtpCompRxInfo);

    PTP_FN_EXIT ();
    return (i4Status);
}

/*****************************************************************************/
/* Function     : PtpComRxV1SyncToV2AnncSync                                 */
/*                                                                           */
/* Description  : This function is used to form v2 announce message and      */
/*                v2 sync message form the v1 Sync message.                  */
/*                                                                           */
/* Input        : pPtpCompRxInfo - V1 and v2 Message informantion.           */
/*              : pPtpv2Info     - V2 info structure to get the v2 Pkt info. */
/*              : pPtpv1Info     - V1 info structure to corresponding v2 info*/
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComRxGetV1InfoFromPkt                                 */
/*              : 2.PtpComRxMapV1InfoToV2Info                                */
/*              : 3.PtpComRxPackV2PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComRxV1SyncToV2AnncSync (tPtpCompRxInfo * pPtpCompRxInfo,
                            tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info)
{
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompRxInfo->Rx.pPtpPort);
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    /* Get the v1 info to from v2 Msg Header */
    PtpComRxGetV1InfoFromPkt (pPtpCompRxInfo->Rx.pu1Ptpv1Pdu + u4MediaHdrLen,
                              pPtpv1Info);

    /* Update the shadow info from the v1 message */
    i4Status =
        PtpCompUpdateCompDbFromV1Info (pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                                       pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                                       pPtpCompRxInfo->Rx.pPtpPort->PortDs.
                                       u4PtpPortNumber, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1SyncToV2AnncSync: Compatibility table Entry"
                  " updatation failed. \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Convert the v1 info to v2 info */
    i4Status = PtpComRxMapV1InfoToV2Info (pPtpv1Info, pPtpv2Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1SyncToV2AnncSync: v1Info to v2Info Mapping "
                  "failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* message specific changes */
    pPtpv2Info->ComHdr.u2MsgLen = PTP_V2_ANNC_MSG_LEN;
    pPtpv2Info->ComHdr.u1MsgType = PTP_ANNC_MSG;

    /* form the v2 Packet from v2 info */
    PtpComRxPackV2PtpPkt (PTP_ANNC_MSG, pPtpv2Info,
                          pPtpCompRxInfo->pu1Ptpv2AnncPdu + u4MediaHdrLen);
    pPtpCompRxInfo->u4Ptpv2AnncPduLen = u4MediaHdrLen + PTP_V2_ANNC_MSG_LEN;

    /* message specific changes */
    pPtpv2Info->ComHdr.u2MsgLen = PTP_V2_SYNC_MSG_LEN;
    pPtpv2Info->ComHdr.u1MsgType = PTP_SYNC_MSG;
    /* form the v2 Packet from v2 info */
    PtpComRxPackV2PtpPkt (PTP_SYNC_MSG, pPtpv2Info,
                          pPtpCompRxInfo->Rx.pu1Ptpv2Pdu + u4MediaHdrLen);

    pPtpCompRxInfo->Rx.u4Ptpv2PduLen = u4MediaHdrLen + PTP_V2_SYNC_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComRxV1DReqToV2DReq                                     */
/*                                                                           */
/* Description  : This function is used to form v2 Delay Request message     */
/*                form the v1 Delay Request message.                         */
/*                                                                           */
/* Input        : pPtpCompRxInfo - V1 and v2 Message informantion.           */
/*              : pPtpv2Info     - V2 info structure to get the v2 Pkt info. */
/*              : pPtpv1Info     - V1 info structure to corresponding v2 info*/
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComRxGetV1InfoFromPkt                                 */
/*              : 2.PtpComRxMapV1InfoToV2Info                                */
/*              : 3.PtpComRxPackV2PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PtpComRxV1DReqToV2DReq (tPtpCompRxInfo * pPtpCompRxInfo,
                        tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info)
{
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompRxInfo->Rx.pPtpPort);
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    /* Get the v1 info to from v2 Msg Header */
    PtpComRxGetV1InfoFromPkt (pPtpCompRxInfo->Rx.pu1Ptpv1Pdu + u4MediaHdrLen,
                              pPtpv1Info);

    /* Update the shadow info from the DB */
    i4Status =
        PtpCompUpdateCompDbFromV1Info (pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                                       pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                                       pPtpCompRxInfo->Rx.pPtpPort->PortDs.
                                       u4PtpPortNumber, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1DReqToV2DReq : Compatibility table Entry"
                  " updatation failed. \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Convert the v1 info to v2 info */
    i4Status = PtpComRxMapV1InfoToV2Info (pPtpv1Info, pPtpv2Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1DReqToV2DReq : v1Info to v2Info Mapping "
                  "failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* message specific changes */
    pPtpv2Info->ComHdr.u2MsgLen = PTP_V2_DELAY_REQ_MSG_LEN;
    pPtpv2Info->ComHdr.i1LogMsgInter =
        PtpUtilSecLogBase2 ((INT1) (pPtpv1Info->SyncDReqHdr.i1SyncInterval +
                                    PTP_COMP_TRNS_INCR_TIME));

    /* form the v2 Packet from v2 info */
    PtpComRxPackV2PtpPkt (PTP_DELAY_REQ_MSG, pPtpv2Info,
                          pPtpCompRxInfo->Rx.pu1Ptpv2Pdu + u4MediaHdrLen);
    pPtpCompRxInfo->Rx.u4Ptpv2PduLen = u4MediaHdrLen + PTP_V2_DELAY_REQ_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComRxV1FolloUpToV2FollowUp                              */
/*                                                                           */
/* Description  : This function is used to form v2 Follow up message         */
/*                form the v1 Follow up message.                             */
/*                                                                           */
/* Input        : pPtpCompRxInfo - V1 and v2 Message informantion.           */
/*              : pPtpv2Info     - V2 info structure to get the v2 Pkt info. */
/*              : pPtpv1Info     - V1 info structure to corresponding v2 info*/
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComRxGetV1InfoFromPkt                                 */
/*              : 2.PtpComRxMapV1InfoToV2Info                                */
/*              : 3.PtpComRxPackV2PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComRxV1FolloUpToV2FollowUp (tPtpCompRxInfo * pPtpCompRxInfo,
                               tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info)
{
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompRxInfo->Rx.pPtpPort);
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    /* Get the v1 info to from v2 Msg Header */
    PtpComRxGetV1InfoFromPkt (pPtpCompRxInfo->Rx.pu1Ptpv1Pdu + u4MediaHdrLen,
                              pPtpv1Info);

    /* Update the shadow info from the DB */
    i4Status =
        PtpCompUpdateV1InfoFromCompDb (pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                                       pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                                       pPtpCompRxInfo->Rx.pPtpPort->PortDs.
                                       u4PtpPortNumber, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1FolloUpToV2FollowUp :Entry is not found in the"
                  " Compatibility table. \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Convert the v1 info to v2 info */
    i4Status = PtpComRxMapV1InfoToV2Info (pPtpv1Info, pPtpv2Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1FolloUpToV2FollowUp : v1Info to v2Info Mapping"
                  " failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* message specific changes */
    pPtpv2Info->ComHdr.u2MsgLen = PTP_V2_FOLLOW_UP_MSG_LEN;
    pPtpv2Info->ComHdr.u2SeqId = pPtpv1Info->FollowUpHdr.u2AssoSeqId;

    /* form the v2 Packet from v2 info */
    PtpComRxPackV2PtpPkt (PTP_FOLLOW_UP_MSG, pPtpv2Info,
                          pPtpCompRxInfo->Rx.pu1Ptpv2Pdu + u4MediaHdrLen);
    pPtpCompRxInfo->Rx.u4Ptpv2PduLen = u4MediaHdrLen + PTP_V2_FOLLOW_UP_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComRxV1DRespToV2DResp                                   */
/*                                                                           */
/* Description  : This function is used to form v2 Delay Response message    */
/*                form the v1 Delay Response message.                        */
/*                                                                           */
/* Input        : pPtpCompRxInfo - V1 and v2 Message informantion.           */
/*              : pPtpv2Info     - V2 info structure to get the v2 Pkt info. */
/*              : pPtpv1Info     - V1 info structure to corresponding v2 info*/
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComRxGetV1InfoFromPkt                                 */
/*              : 2.PtpComRxMapV1InfoToV2Info                                */
/*              : 3.PtpComRxPackV2PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComRxV1DRespToV2DResp (tPtpCompRxInfo * pPtpCompRxInfo,
                          tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info)
{
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompRxInfo->Rx.pPtpPort);
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    /* Get the v1 info to from v2 Msg Header */
    PtpComRxGetV1InfoFromPkt (pPtpCompRxInfo->Rx.pu1Ptpv1Pdu + u4MediaHdrLen,
                              pPtpv1Info);

    /* Update the shadow info from the DB */
    i4Status =
        PtpCompUpdateV1InfoFromCompDb (pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                                       pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                                       pPtpCompRxInfo->Rx.pPtpPort->PortDs.
                                       u4PtpPortNumber, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1DRespToV2DResp :Entry is not found in the "
                  "Compatibility table. \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Convert the v1 info to v2 info */
    i4Status = PtpComRxMapV1InfoToV2Info (pPtpv1Info, pPtpv2Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpCompRxInfo->Rx.pPtpPort->u4ContextId,
                  pPtpCompRxInfo->Rx.pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpComRxV1DRespToV2DResp : v1Info to v2Info Mapping"
                  "failed . \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* message specific changes */
    pPtpv2Info->ComHdr.i1LogMsgInter =
        PtpUtilSecLogBase2 ((INT1) (pPtpv1Info->SyncDReqHdr.i1SyncInterval +
                                    PTP_COMP_TRNS_INCR_TIME));
    pPtpv2Info->ComHdr.u2MsgLen = PTP_V2_DELAY_RES_MSG_LEN;
    pPtpv2Info->ComHdr.u2SeqId = pPtpv1Info->DRespHdr.u2ReqSrcSeqId;

    /* form the v2 Packet from v2 info */
    PtpComRxPackV2PtpPkt (PTP_DELAY_RESP_MSG, pPtpv2Info,
                          pPtpCompRxInfo->Rx.pu1Ptpv2Pdu + u4MediaHdrLen);
    pPtpCompRxInfo->Rx.u4Ptpv2PduLen = u4MediaHdrLen + PTP_V2_DELAY_RES_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComRxGetV1InfoFromPkt                                   */
/*                                                                           */
/* Description  : This function is used to update the v1 informantion from   */
/*                the Compatibility table info.                              */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Port Identifier.                         */
/*              : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpComRxGetV1InfoFromPkt (UINT1 *pu1V1Pdu, tPtpv1Info * pPtpv1Info)
{
    UINT1              *pTmpPtpv1Msg = NULL;
    UINT2               u2Resvd = 0;
    UINT1               u1Resvd = 0;

    PTP_FN_ENTRY ();

    pTmpPtpv1Msg = pu1V1Pdu;
    /* Collect the V1 Common header fields */
    PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u2ver);
    PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u2verNetwork);
    PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.ai1SubDomain,
                          PTP_SUBDOMAIN_NAME_LENGTH);
    PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u1MsgType);
    PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u1SrcComTech);
    PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.ai1SourceUuid,
                          PTP_UUID_LENGTH);
    PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u2SrcPortId);
    PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u2SeqId);
    PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->ComHdr.u1Control);
    PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);
    PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->ComHdr.ai1Flags,
                          PTP_V1_FLAG_FIELD_LEN);

    switch (pPtpv1Info->ComHdr.u1Control)
    {
        case PTP_V1_SYNC_MESSAGE:
        case PTP_V1_DELAY_REQ_MESSAGE:
            /* Collect the V1 Sync/Delay Request header fields */
            pTmpPtpv1Msg = pTmpPtpv1Msg + PTP_V1_COMM_SYNC_HEADER_PAD;

            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  OrigTimestamp.u4Sec);
            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  OrigTimestamp.i4NanoSec);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2EpochNum);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2CurrUTCOffset);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1GMCommTech);
            PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1GMClkUuid, PTP_UUID_LENGTH);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2GMPortId);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2GMSequenceId);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1GMClkStratum);
            PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1GMClkId, PTP_CODE_STRING_LENGTH);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2GMClkVar);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 bGMPreferred);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 bGMIsBoundaryClk);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 i1SyncInterval);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2LocalClkVar);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2LocalStepsRemoved);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1LocalClkStratum);
            PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1LocalClkId, PTP_CODE_STRING_LENGTH);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1ParentCommTech);
            PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1ParentUuid, PTP_UUID_LENGTH);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2ParentPortField);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2EstMstVar);
            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i4EstMstDrift);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 bUtcReasonable);
            break;

        case PTP_V1_FOLLOWUP_MESSAGE:
            /* Collect the V1 Delay Response header fields */
            pTmpPtpv1Msg = pTmpPtpv1Msg + PTP_V1_COMM_FOLLOW_UP_HEADER_PAD;

            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->FollowUpHdr.
                                  u2AssoSeqId);
            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->FollowUpHdr.
                                  PreOrigTimestamp.u4Sec);
            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->FollowUpHdr.
                                  PreOrigTimestamp.i4NanoSec);
            break;

        case PTP_V1_DELAY_RESP_MESSAGE:
            /* Collect the V1 Delay Response header fields */
            pTmpPtpv1Msg = pTmpPtpv1Msg + PTP_V1_COMM_D_RES_HEADER_PAD;

            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->DRespHdr.
                                  DRecTimestamp.u4Sec);
            PTP_LBUF_GET_4_BYTES (pTmpPtpv1Msg, pPtpv1Info->DRespHdr.
                                  DRecTimestamp.i4NanoSec);
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, u1Resvd);    /* Reserved Byte */
            PTP_LBUF_GET_1_BYTE (pTmpPtpv1Msg, pPtpv1Info->DRespHdr.
                                 u1ReqSrcCommTech);
            PTP_LBUF_GET_N_BYTES (pTmpPtpv1Msg, pPtpv1Info->DRespHdr.
                                  ai1ReqSrcUuid, PTP_UUID_LENGTH);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->DRespHdr.
                                  u2ReqSrcPortId);
            PTP_LBUF_GET_2_BYTES (pTmpPtpv1Msg, pPtpv1Info->DRespHdr.
                                  u2ReqSrcSeqId);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComRxGetV1InfoFromPkt : Invalid Message Type.\r\n"));
            break;
    }
    UNUSED_PARAM (u1Resvd);
    UNUSED_PARAM (u2Resvd);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpComRxMapV1InfoToV2CommonInfo                            */
/*                                                                           */
/* Description  : This function is used to map the v1 informantion to        */
/*                corressponding v2 informantion which are used to form      */
/*                comman header for the v2 Messages                          */
/*                                                                           */
/* Input        : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*              : pPtpv2Info  - Pointer to tPtpv2Info Type                   */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComRxMapV1InfoToV2CommonInfo (tPtpv1Info * pPtpv1Info,
                                 tPtpv2Info * pPtpv2Info)
{
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    pPtpv2Info->ComHdr.u1PtpVersion = PTP_MESSAGE_VERSION_TWO;

    i4Status =
        PtpCmUtlv1ContMsgTypeTov2MsgType (pPtpv1Info->ComHdr.u1Control,
                                          pPtpv1Info->ComHdr.u1MsgType,
                                          &(pPtpv2Info->ComHdr.u1MsgType));
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpCmUtlv1ContMsgTypeTov2MsgType failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    i4Status =
        PtpCmUtlv1SubDomainTov2Domain (pPtpv1Info->ComHdr.ai1SubDomain,
                                       &(pPtpv2Info->ComHdr.u1DomainId));
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpCmUtlv1SubDomainTov2Domain failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PtpCmUtlv1FlagTov2Flag (pPtpv1Info->ComHdr.ai1Flags,
                            pPtpv1Info->SyncDReqHdr.u1LocalClkStratum,
                            pPtpv1Info->SyncDReqHdr.ai1LocalClkId,
                            pPtpv2Info->ComHdr.ai1FlagField);
    i4Status =
        PtpCmUtlv1SrcPortIdTov2SrcPortId (pPtpv1Info->ComHdr.u1SrcComTech,
                                          pPtpv1Info->ComHdr.ai1SourceUuid,
                                          pPtpv1Info->ComHdr.u2SrcPortId,
                                          &(pPtpv2Info->ComHdr.SrcPortId.ClkId),
                                          &(pPtpv2Info->ComHdr.SrcPortId.
                                            u2PortNumber));
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpCmUtlv1SrcPortIdTov2SrcPortId failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    pPtpv2Info->ComHdr.u2SeqId = pPtpv1Info->ComHdr.u2SeqId;
    pPtpv2Info->ComHdr.u1ContField = pPtpv1Info->ComHdr.u1Control;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComRxMapV1InfoToV2Info                                  */
/*                                                                           */
/* Description  : This function is used to map the v1 informantion to        */
/*                corressponding v2 informantion.                            */
/*                                                                           */
/* Input        : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*              : pPtpv2Info  - Pointer to tPtpv2Info Type                   */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComRxMapV1InfoToV2Info (tPtpv1Info * pPtpv1Info, tPtpv2Info * pPtpv2Info)
{
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    /* Common */
    i4Status = PtpComRxMapV1InfoToV2CommonInfo (pPtpv1Info, pPtpv2Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComRxMapV1InfoToV2Info : PtpComRxMapV1InfoToV2CommonInfo"
                  " failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    switch (pPtpv1Info->ComHdr.u1Control)
    {
        case PTP_V1_SYNC_MESSAGE:
        case PTP_V1_DELAY_REQ_MESSAGE:
            /* Sync / Delay Req */
            pPtpv2Info->AnncHdr.u1GMPri1 =
                ((pPtpv1Info->SyncDReqHdr.bGMPreferred ==
                  PTP_V1_GM_IS_PREFERRED_FALSE) ?
                 PTP_V2_PRIORITY1_VAL_128 : PTP_V2_PRIORITY1_VAL_127);

            i4Status = PtpCmUtlv1StratumTov2ClkCls (pPtpv1Info->SyncDReqHdr.
                                                    u1GMClkStratum,
                                                    &(pPtpv2Info->AnncHdr.
                                                      GMClkQuality.u1Class));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1StratumTov2ClkCls failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            i4Status =
                PtpCmUtlv1ClkIdTov2ClkAcTimSrc (pPtpv1Info->SyncDReqHdr.
                                                ai1GMClkId,
                                                &(pPtpv2Info->AnncHdr.
                                                  GMClkQuality.u1Accuracy),
                                                &(pPtpv2Info->AnncHdr.
                                                  u1TimeSrc));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1ClkIdTov2ClkAcTimSrc failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            i4Status =
                PtpCmUtlv1ClkIdTov2ClkAcTimSrc (pPtpv1Info->SyncDReqHdr.
                                                ai1LocalClkId,
                                                &(pPtpv2Info->LocalClkQuality.
                                                  u1Accuracy),
                                                &(pPtpv2Info->u1LocalTimeSrc));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1ClkIdTov2ClkAcTimSrc failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            pPtpv2Info->AnncHdr.GMClkQuality.u2OffsetScaledLogVariance =
                pPtpv1Info->SyncDReqHdr.i2GMClkVar;

            pPtpv2Info->AnncHdr.u1GMPri2 =
                ((pPtpv1Info->SyncDReqHdr.bGMIsBoundaryClk ==
                  PTP_V1_IS_BOUNDARY_CLK_FALSE) ?
                 PTP_V2_PRIORITY2_VAL_128 : PTP_V2_PRIORITY2_VAL_127);

            i4Status =
                PtpCmUtlv1SrcPortIdTov2SrcPortId (pPtpv1Info->SyncDReqHdr.
                                                  u1GMCommTech,
                                                  pPtpv1Info->SyncDReqHdr.
                                                  ai1GMClkUuid,
                                                  pPtpv1Info->SyncDReqHdr.
                                                  u2GMPortId,
                                                  &(pPtpv2Info->AnncHdr.
                                                    GMClkId),
                                                  &(pPtpv2Info->u2GMPortId));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1SrcPortIdTov2SrcPortId failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            pPtpv2Info->ComHdr.i1LogMsgInter =
                pPtpv1Info->SyncDReqHdr.i1SyncInterval;

            pPtpv2Info->AnncHdr.u2StepsRemoved =
                pPtpv1Info->SyncDReqHdr.u2LocalStepsRemoved;

            pPtpv2Info->AnncHdr.i2CurrUTCOffset =
                pPtpv1Info->SyncDReqHdr.i2CurrUTCOffset;

            i4Status = PtpCmUtlv1TimStpTov2TimStp (&(pPtpv1Info->SyncDReqHdr.
                                                     OrigTimestamp),
                                                   pPtpv1Info->SyncDReqHdr.
                                                   u2EpochNum,
                                                   &(pPtpv2Info->SyncDReqHdr.
                                                     OrigTimestamp));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1TimStpTov2TimStp failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }
            MEMCPY (&(pPtpv2Info->AnncHdr.OrigTimestamp),
                    &(pPtpv2Info->SyncDReqHdr.OrigTimestamp),
                    sizeof (tPtpTimeStamp));
            break;

        case PTP_V1_DELAY_RESP_MESSAGE:
            /* Delay Resp */
            i4Status = PtpCmUtlv1TimStpTov2TimStp (&(pPtpv1Info->DRespHdr.
                                                     DRecTimestamp),
                                                   pPtpv1Info->SyncDReqHdr.
                                                   u2EpochNum,
                                                   &(pPtpv2Info->DRespHdr.
                                                     RecTimestamp));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1TimStpTov2TimStp failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            i4Status =
                PtpCmUtlv1SrcPortIdTov2SrcPortId (pPtpv1Info->DRespHdr.
                                                  u1ReqSrcCommTech,
                                                  pPtpv1Info->DRespHdr.
                                                  ai1ReqSrcUuid,
                                                  pPtpv1Info->DRespHdr.
                                                  u2ReqSrcPortId,
                                                  &(pPtpv2Info->DRespHdr.
                                                    ReqPortId.ClkId),
                                                  &(pPtpv2Info->DRespHdr.
                                                    ReqPortId.u2PortNumber));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1SrcPortIdTov2SrcPortId failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }
            break;

        case PTP_V1_FOLLOWUP_MESSAGE:
            /* Form V2 Follow up */
            i4Status = PtpCmUtlv1TimStpTov2TimStp (&(pPtpv1Info->FollowUpHdr.
                                                     PreOrigTimestamp),
                                                   pPtpv1Info->SyncDReqHdr.
                                                   u2EpochNum,
                                                   &(pPtpv2Info->FollowUpHdr.
                                                     PreOrigTimestamp));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComRxMapV1InfoToV2Info : "
                          "PtpCmUtlv1TimStpTov2TimStp failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComRxMapV1InfoToV2Info: Invalid Message Type. \r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
            break;
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComRxPackV2PtpPkt                                       */
/*                                                                           */
/* Description  : This function is used to form the v2 Message form the      */
/*                corressponding v2 informantion.                            */
/*                                                                           */
/* Input        : u2PktType   - V2 Message Type                              */
/*              : pPtpv2Info  - Pointer to tPtpv2Info Type                   */
/*              : pu1Ptpv2Msg - Pointer to V2 Message                        */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpComRxPackV2PtpPkt (UINT2 u2PktType, tPtpv2Info * pPtpv2Info,
                      UINT1 *pu1Ptpv2Msg)
{
    UINT1              *pu1Tmpv2Msg = NULL;
    UINT4               u4Resvd = 0;
    UINT2               u2Val = 0;
    UINT1               u1Resvd = 0;
    UINT1               u1Val = 0;

    PTP_FN_ENTRY ();

    pu1Tmpv2Msg = pu1Ptpv2Msg;
    /* Update the V2 Common header fields */
     /*DIAB*/
        u1Val = (UINT1)
        ((((pPtpv2Info->ComHdr.u1TranSpc) << PTP_TRANS_SPC_BITS_SHIFT)) |
         (pPtpv2Info->ComHdr.u1MsgType));

    PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, u1Val);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->ComHdr.u1PtpVersion);
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, pPtpv2Info->ComHdr.u2MsgLen);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->ComHdr.u1DomainId);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, 0);
    PTP_LBUF_PUT_N_BYTES (pu1Tmpv2Msg, pPtpv2Info->ComHdr.ai1FlagField,
                          PTP_V2_FLAG_FIELD_LEN);
    PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg,
                          (pPtpv2Info->ComHdr.u8CorrectionField.u4Hi));
    PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg,
                          (pPtpv2Info->ComHdr.u8CorrectionField.u4Lo));

    PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, u4Resvd);
    PTP_LBUF_PUT_N_BYTES (pu1Tmpv2Msg, pPtpv2Info->ComHdr.SrcPortId.ClkId,
                          PTP_MAX_CLOCK_ID_LEN);
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg,
                          pPtpv2Info->ComHdr.SrcPortId.u2PortNumber);
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, pPtpv2Info->ComHdr.u2SeqId);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->ComHdr.u1ContField);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->ComHdr.i1LogMsgInter);

    switch (u2PktType)
    {
        case PTP_SYNC_MSG:
        case PTP_DELAY_REQ_MSG:
            /* Form V2 SYNC / DELAY_REQ */
            u2Val = (UINT2) pPtpv2Info->SyncDReqHdr.OrigTimestamp.u8Sec.u4Hi;
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, u2Val);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, (pPtpv2Info->SyncDReqHdr.
                                                OrigTimestamp.u8Sec.u4Lo));

            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, pPtpv2Info->SyncDReqHdr.
                                  OrigTimestamp.u4NanoSec);
            break;

        case PTP_DELAY_RESP_MSG:
            /* Form V2 Delay Resp */
            u2Val = (UINT2) pPtpv2Info->DRespHdr.RecTimestamp.u8Sec.u4Hi;
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, u2Val);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, (pPtpv2Info->DRespHdr.
                                                RecTimestamp.u8Sec.u4Lo));

            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, pPtpv2Info->DRespHdr.
                                  RecTimestamp.u4NanoSec);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv2Msg, pPtpv2Info->DRespHdr.
                                  ReqPortId.ClkId, PTP_MAX_CLOCK_ID_LEN);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, pPtpv2Info->DRespHdr.
                                  ReqPortId.u2PortNumber);

            break;

        case PTP_FOLLOW_UP_MSG:
            /* Form V2 Follow up */
            u2Val = (UINT2) pPtpv2Info->FollowUpHdr.PreOrigTimestamp.u8Sec.u4Hi;
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, u2Val);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, (pPtpv2Info->FollowUpHdr.
                                                PreOrigTimestamp.u8Sec.u4Lo));

            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, pPtpv2Info->FollowUpHdr.
                                  PreOrigTimestamp.u4NanoSec);
            break;

        case PTP_ANNC_MSG:
            /* Form V2 Announce message */
            u2Val = (UINT2) pPtpv2Info->AnncHdr.OrigTimestamp.u8Sec.u4Hi;
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, u2Val);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, (pPtpv2Info->AnncHdr.
                                                OrigTimestamp.u8Sec.u4Lo));
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.
                                  OrigTimestamp.u4NanoSec);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.
                                  i2CurrUTCOffset);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, u1Resvd);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.u1GMPri1);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.
                                 GMClkQuality.u1Class);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.
                                 GMClkQuality.u1Accuracy);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.
                                  GMClkQuality.u2OffsetScaledLogVariance);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.u1GMPri2);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.GMClkId,
                                  PTP_MAX_CLOCK_ID_LEN);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.
                                  u2StepsRemoved);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv2Msg, pPtpv2Info->AnncHdr.u1TimeSrc);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComRxPackV2PtpPkt : Invalid Message Type. \r\n"));
            break;
    }
    PTP_FN_EXIT ();
}
#endif /*  _PTPCOMRX_C_ */

/***************************************************************************
 *                         END OF FILE ptpcomrx.c                          *
 ***************************************************************************/
