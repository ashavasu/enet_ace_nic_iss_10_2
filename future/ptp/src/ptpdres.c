/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpdres.c,v 1.8 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP task delay response message handler 
 *              related routines.
 *********************************************************************/
#ifndef _PTPDRESP_C_
#define _PTPDRESP_C_

#include "ptpincs.h"

PRIVATE INT4        PtpDrespValidateDelayRespMsg (tPtpPort * pPtpPort,
                                                  tPtpHdrInfo * pPtpHdrInfo,
                                                  UINT1 *pu1RxDelayResp);

PRIVATE INT4        PtpDrespHandleDelayRespMsg (UINT1 *pu1Pdu,
                                                tPtpPort * pPtpPort,
                                                tPtpHdrInfo * pPtpHdrInfo,
                                                tClkSysTimeInfo *
                                                pPtpSysTimeInfo);

PRIVATE INT4        PtpDrespUpdtDelayRequestTmr (tPtpPort * pPtpPort,
                                                 tPtpHdrInfo * pPtpHdrInfo);

/*****************************************************************************/
/* Function                  : PtpDrespHandleRxDelayRespMsg                  */
/*                                                                           */
/* Description               : This routine will be invoked to handle the    */
/*                             delay respons messages. This routine validates*/
/*                             and processes the received delay response     */
/*                             message.                                      */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             Parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpDrespHandleRxDelayRespMsg (tPtpPktParams * pPtpPktParams)
{
    tPtpHdrInfo         PtpHdrInfo;

    PTP_FN_ENTRY ();

    if (pPtpPktParams->pPtpPort->pPtpDomain->ClkMode ==
        PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (pPtpPktParams->pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType
            != PTP_E2E_TRANSPARENT_CLOCK)
        {
            /* The device is operating as P2P Transparent clock. Hence
             * drop the received Delay request and response messages.
             * Refer Section 10.3 of IEEE Std 1588-2008
             * */
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "Clock is not operating in End to End "
                      "Transparent Mode...\r\n"));

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "Discarding the packet.....\r\n"));
            /* Incrementing the dropped message count */
            pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
        /* Clock is operating in transparent mode. Do not consume the 
         * packet. Give it to transparent clock handler for forwarding
         * Since, Delay response message is an event message, generate 
         * the Ingress event time stamp.  
         * */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Clock is operating in Transparent "
                  "mode.\r\n"));

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "TimeStamping for ingress event "
                  "message.\r\n"));

        /* The timestamping for ingress and egress events along with correction
         * field updation should happen only when software time stamping is 
         * enabled in the system.
         * The packet should be handled at the hardware level itself,if hardware
         * time stamping is enabled. 
         * */
        if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TRANS_TIMESTAMPING)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Forwarding the received delay response"
                  "message.\r\n"));

        PtpTransHandleRcvdPtpPkt (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort,
                                  pPtpPktParams->u4PktLen,
                                  PTP_DELAY_RESP_MSG,
                                  &(pPtpPktParams->pPtpIngressTimeStamp->
                                    FsClkTimeVal));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    pPtpPktParams->pu1RcvdPdu =
        pPtpPktParams->pu1RcvdPdu +
        PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    PtpUtilExtractHdrFromMsg (pPtpPktParams->pu1RcvdPdu, &PtpHdrInfo);

    if (PtpDrespValidateDelayRespMsg (pPtpPktParams->pPtpPort, &PtpHdrInfo,
                                      pPtpPktParams->pu1RcvdPdu)
        != OSIX_SUCCESS)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpDrespHandleRxDelayRespMsg: "
                  "Delay response Message Validation returned Failure!!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpDrespHandleDelayRespMsg (pPtpPktParams->pu1RcvdPdu,
                                    pPtpPktParams->pPtpPort, &PtpHdrInfo,
                                    pPtpPktParams->pPtpIngressTimeStamp)
        != OSIX_SUCCESS)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpDrespHandleRxDelayRespMsg: "
                  "PtpDrespHandleDelayRespMsg returned Failure!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Incrementing the delay request message received count */
    pPtpPktParams->pPtpPort->PtpPortStats.u4RcvdDelayRespMsgCnt++;
    pPtpPktParams->pPtpPort->bDelayReqInProgress = OSIX_FALSE;

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDrespValidateDelayRespMsg                  */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             delay response message.                       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to header structure     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpDrespValidateDelayRespMsg (tPtpPort * pPtpPort, tPtpHdrInfo * pPtpHdrInfo,
                              UINT1 *pu1RxDelayResp)
{
    PTP_FN_ENTRY ();

    if (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_END_TO_END)
    {
        /* The Delay Mechanism of the port is Peer to Peer so
         * discarding the Delay Response message */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port delay mechanism is not End-to-" "End.\r\n"));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Hence discarding the received "
                  "Delay response message.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* The received delay response message should be discarded on any one of 
     * the following criterias
     *
     * 1) Port is in INITIALIZING or DISABLED state
     * 2) Port is in FAULTY state
     * 3) Port is not in SLAVE/UNCALIBRATED state
     * 4) Message is not from current Master clock
     * 5) Delay response message is not associated with a delay request message
     * */
    if ((pPtpPort->PortDs.u1PortState == PTP_STATE_INITIALIZING) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_DISABLED) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_FAULTY))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port %u is in INITIALIZING/DISABLED/FAULTY" " State\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the received Delay Request "
                  "Message in Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if ((pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE) &&
        (pPtpPort->PortDs.u1PortState != PTP_STATE_UNCALIBRATED))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port %u is not in SLAVE/UNCALIBRATED " "State\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the received Delay Request "
                  "Message in Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUtilIsMsgFromCurrentParent (pPtpPort, pPtpHdrInfo->ClkId,
                                       pPtpHdrInfo->u2Port) != OSIX_TRUE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Delay Response not received from "
                  "current master in Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the message in Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUtilIsDelayRespFromDelayReq (pPtpPort, pPtpHdrInfo, pu1RxDelayResp)
        != OSIX_TRUE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Delay Response not associated with"
                  "delay request message in Port\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the message in Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDrespHandleDelayRespMsg                    */
/*                                                                           */
/* Description               : This routine handles the received delay resp  */
/*                             messages. The PTP messages should be validated*/
/*                             earlier itself before invoking this routine.  */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to header structure     */
/*                             pIngressDelayRespTime - Pointer to ingress    */
/*                                                     Delay Response time.  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpDrespHandleDelayRespMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                            tPtpHdrInfo * pPtpHdrInfo,
                            tClkSysTimeInfo * pIngressDelayRespTime)
{
    tFsClkTimeVal       FsClkDiff;
    tFsClkTimeVal       receiveTimeStampDelayResp;
    FS_UINT8            u8CorrectionField;
    FS_UINT8            u8TotalCorrectionField;
    FS_UINT8            u8Mean;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8SyncDelayDiff;
    FS_UINT8            u8TempAdd;
    FS_UINT8            u8NanoSecValue;
    FS_UINT8            u8Result;
    FS_UINT8            u8NsecDiff;
    FS_UINT8            u8DelayDiff1;
    FS_UINT8            u8DelayDiff2;
    UINT1              *pu1Tmp = NULL;
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;
    INT1                i1CorrectionDelaySign = 0;
    INT1                i1CorrectionDelaySign1 = 0;
    INT1                i1CorrectionDelaySign2 = 0;
    INT1                i1SyncDelaySign = 0;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8CorrectionField);
    FSAP_U8_CLR (&u8TotalCorrectionField);
    FSAP_U8_CLR (&u8Mean);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8SyncDelayDiff);
    FSAP_U8_CLR (&u8TempAdd);
    FSAP_U8_CLR (&u8NanoSecValue);
    FSAP_U8_CLR (&u8Result);
    FSAP_U8_CLR (&u8NsecDiff);
    FSAP_U8_CLR (&u8DelayDiff1);
    FSAP_U8_CLR (&u8DelayDiff2);
    UNUSED_PARAM (pIngressDelayRespTime);
    UNUSED_PARAM (i1SyncDelaySign);

    MEMSET (&FsClkDiff, 0, sizeof (tFsClkTimeVal));
    MEMSET (&receiveTimeStampDelayResp, 0, sizeof (tFsClkTimeVal));

    /* Refer Section 11.3 of IEEE Std 1588 2008 for processing of delay
     * response message.
     * */

    UINT8_LO (&u8Mean) = PTP_MEAN_LINK_DELAY_VALUE;
    UINT8_LO (&(u8NanoSecValue)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Processing received delay response "
              "message \r\n"));

    /* Clearing the calculated value of the mean path delay */
    FSAP_U8_CLR (&pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                 u8MeanPathDelay);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Delay response message received from "
              "one step clock.\r\n"));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Calculating the Mean path delay...\r\n"));

    /* Obtaining Receive Time stamp Of Delay Rsp */
    pu1Tmp = pu1Pdu + PTP_SYNC_TIME_STAMP_OFFSET;
    PTP_LBUF_GET_2_BYTES (pu1Tmp, u2Val);
    UINT8_HI (&(receiveTimeStampDelayResp.u8Sec)) = (UINT4) u2Val;

    PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
    UINT8_LO (&(receiveTimeStampDelayResp.u8Sec)) = u4Val;
    u4Val = 0;
    PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
    receiveTimeStampDelayResp.u4Nsec = u4Val;

    /* mean path delay = [(t2-t3)+(t4-t1)- corrn field sync - corrn filed of follow_up - corrn field of delay_res]/2 */
    /* t2-t3 (receiveTimestamp of Sync message - originTimestamp of Delay request message) */
    PtpClkSubtractTimeStamps (&(pPtpPort->SyncReceptTime),
                              &(pPtpPort->DelayReqOriginTime),
                              &(FsClkDiff), &(i1CorrectionDelaySign));

    FSAP_U8_MUL (&(u8DelayDiff1), &(FsClkDiff.u8Sec), &(u8NanoSecValue));
    FSAP_U8_CLR (&u8NsecDiff);
    FSAP_U8_ASSIGN_LO (&u8NsecDiff, FsClkDiff.u4Nsec);
    FSAP_U8_ADD (&(u8DelayDiff1), &(u8DelayDiff1), &(u8NsecDiff));
    MEMSET (&FsClkDiff, 0, sizeof (tFsClkTimeVal));

    /* t4-t1 (receiveTimestamp of Delay_Resp message - originTimestamp of Sync message) */
    PtpClkSubtractTimeStamps (&(receiveTimeStampDelayResp),
                              &(pPtpPort->IngressSyncTimeStamp),
                              &(FsClkDiff), &(i1CorrectionDelaySign1));

    FSAP_U8_MUL (&(u8DelayDiff2), &(FsClkDiff.u8Sec), &(u8NanoSecValue));
    FSAP_U8_CLR (&u8NsecDiff);
    FSAP_U8_ASSIGN_LO (&u8NsecDiff, FsClkDiff.u4Nsec);
    FSAP_U8_ADD (&(u8DelayDiff2), &(u8DelayDiff2), &(u8NsecDiff));

    /*Adding the result of (t2-t3) and (t4-t1) */
    PtpUtil8BytesAddition (&(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                             CurrentDs.u8MeanPathDelay), u8DelayDiff1,
                           u8DelayDiff2, i1CorrectionDelaySign,
                           i1CorrectionDelaySign1,
                           &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                             CurrentDs.i1DelaySign));

    /* Adding Sync , Sync_follow_up and Delay response correction fields,
       For two step clock Correction field will be sent in follow up msg
       Hence Sync msg correction field will be Zero */

    FSAP_U8_ADD (&(u8TotalCorrectionField), &(pPtpPort->u8CorrectionField),
                 &(pPtpHdrInfo->u8CorrectionField));

    /* Subtracting [(t2-t3)+(t4-t1)- corrn field(sync+follow_up+delay_res)] */
    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.i1DelaySign ==
        PTP_POSITIVE)
    {
        PtpClkPerf8ByteSubtraction (&
                                    (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                                     CurrentDs.u8MeanPathDelay),
                                    &(u8TotalCorrectionField),
                                    &(pPtpPort->pPtpDomain->ClockDs.
                                      PtpOcBcClkDs.CurrentDs.u8MeanPathDelay),
                                    &(pPtpPort->pPtpDomain->ClockDs.
                                      PtpOcBcClkDs.CurrentDs.i1DelaySign));
    }
    else
    {
        FSAP_U8_ADD (&(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                       CurrentDs.u8MeanPathDelay),
                     &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                       CurrentDs.u8MeanPathDelay), &(u8TotalCorrectionField));
    }

    /* Before dividing by two, change the value of seconds to nano seconds */
    FSAP_U8_ASSIGN (&u8Result,
                    &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                      u8MeanPathDelay));

    /* Calculation oneway delay .i.e. dividing by 2 */

    PTP_U8_DIV (&u8Result, &u8Reminder, &u8Result, &u8Mean);
    FSAP_U8_ASSIGN (&(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                      u8MeanPathDelay), &u8Result);

    /* (receiveTimeStamp of Delay Response Message ) -
       Origin Time Stamp of Delay Request Message) */

    MEMSET (&FsClkDiff, 0, sizeof (tFsClkTimeVal));
    PtpClkSubtractTimeStamps (&(receiveTimeStampDelayResp),
                              &(pPtpPort->DelayReqOriginTime),
                              &(FsClkDiff), &(i1CorrectionDelaySign2));

    FSAP_U8_MUL (&(u8CorrectionField), &(FsClkDiff.u8Sec), &(u8NanoSecValue));
    FSAP_U8_CLR (&u8NsecDiff);
    FSAP_U8_ASSIGN_LO (&u8NsecDiff, FsClkDiff.u4Nsec);
    FSAP_U8_ADD (&(u8CorrectionField), &(u8CorrectionField), &(u8NsecDiff));

    /* Update Slave to Master Delay */
    PtpClkUpdtSlaveToMasterDelay (pPtpPort, &(u8CorrectionField));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Updated the mean path " "delay...\r\n"));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Computed mean path delay from Master "
              "is %d%d\r\n",
              pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
              u8MeanPathDelay.u4Hi,
              pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
              u8MeanPathDelay.u4Lo));

    if (PtpDrespUpdtDelayRequestTmr (pPtpPort, pPtpHdrInfo) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpDrespHandleDelayRespMsg: "
                  "PtpDrespUpdtDelayRequestTmr returned Failure!!!\r\n",
                  pPtpPort));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to update "
                  "logMinDelayReqInterval.....\r\n", pPtpPort));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDrespTxDelayResp                           */
/*                                                                           */
/* Description               : This routine transmits the Delay response     */
/*                             over the given interface.                     */
/*                                                                           */
/* Input                     : pu1RxPdu - Pointer to the received PDU        */
/*                             pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpDrespTxDelayResp (UINT1 *pu1RxPdu, tPtpPort * pPtpPort)
{
    UINT1              *pu1PtpPdu = NULL;
    UINT1              *pu1TmpPdu = NULL;
    UINT4               u4MsgLen = PTP_MSG_DELAY_RESP_LEN;
    UINT2               u2TwoByteVal = 0;
    tPtpTxParams        PtpTxParams;

    PTP_FN_ENTRY ();

    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

#ifdef L2RED_WANTED
    /*Only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            /* Returning  because in standby node
             * only slave mode will work*/
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                               PTP_MAX_PDU_LEN);
    if (pu1PtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1PtpPdu, 0, PTP_MAX_PDU_LEN);
    pu1TmpPdu = pu1PtpPdu;

    PtpHdrAddMediaHdrForIntf (&pu1TmpPdu, pPtpPort, PTP_DELAY_RESP_MSG,
                              &u4MsgLen);

    /* Refer Table 28 of IEEE 1588-2008 for Delay Response message
     * */
    PtpHdrFillPtpHeader (&pu1TmpPdu, pPtpPort, PTP_DELAY_RESP_MSG);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Filling PTP header for the delay resp "
              "message to be sent through Port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PtpDresCopyHdrFieldFrmDelayReq (pu1RxPdu, pu1PtpPdu, pPtpPort,
                                    PTP_DELAY_RESP_MSG);

    /* Time stamp will be added here with t4 not in Tx module */
    u2TwoByteVal = (UINT2) UINT8_HI (&(pPtpPort->DelayReqIngressTime.u8Sec));
    PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2TwoByteVal);
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu,
                          UINT8_LO (&(pPtpPort->DelayReqIngressTime.u8Sec)));
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, (pPtpPort->DelayReqIngressTime.u4Nsec));

    /* Embed the requesting port identity. */
    MEMCPY (pu1TmpPdu, pu1RxPdu + PTP_SRC_PORT_ID_OFFSET, PTP_SRC_PORT_ID_LEN);

    PtpTxParams.pu1PtpPdu = pu1PtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = u4MsgLen;
    PtpTxParams.u1MsgType = (UINT1) PTP_DELAY_RESP_MSG;

    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpTxTransmitPtpMessage returned "
                  "Failure!!!!!\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDrespUpdtDelayRequestTmr                   */
/*                                                                           */
/* Description               : This routine updates the value of the delay   */
/*                             request timer as provided by the master and   */
/*                             restarts the timer whenever necessary.        */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to header structure     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpDrespUpdtDelayRequestTmr (tPtpPort * pPtpPort, tPtpHdrInfo * pPtpHdrInfo)
{
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    /* The log message interval represented in the delay response message
     * corresponds to the value of portDs.logMinDelayReqInterval.
     * Refer Table 24 - Values of logMessageInterval field of IEEE Std 
     * 1588 2008.
     * Updating the same.
     * */

    if (pPtpHdrInfo->u1LogMsgInterval == PTP_UNICAST_LOG_MSG_INTERVAL)
    {
        /* For unicast messages, the value transmitted will always be
         * 0x7F. Hence ignore such messages.
         * */
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if (pPtpPort->PortDs.u1MinDelayReqInterval == pPtpHdrInfo->u1LogMsgInterval)
    {
        /* Already delay request timer is running for the same value. No 
         * need to do a restart.
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Delay Request timer already in sync "
                  "with Master for port %d.\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Hence not updating the "
                  "portMinDelayReqInterval.....\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Updating the logMessageInterval in "
              "Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    /*The Delay Request Interval value shall be an integer with the minimum value being
     * portDS.logSyncInterval, i.e., at the same rate as Sync messages, and a maximum value of
     * logSyncInterval+5, i.e., 1 Delay_Req message every 32 Sync messages.
     * Refer to 7.7.2.4 of IEEE Std 1588 - 2008 */

    pPtpPort->PortDs.u1MinDelayReqInterval = pPtpHdrInfo->u1LogMsgInterval
        + PTP_DELAY_SYNC_DIFFERENCE;

    /* Restarting the delay request timer */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Restarting the Delay Request timer.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1)
                                   pPtpPort->PortDs.u1MinDelayReqInterval);

    if (PtpTmrStartTmr (&(pPtpPort->DelayReqTimer),
                        u4MilliSec, PTP_DREQ_TMR, OSIX_FALSE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpDrespHandleDelayRespMsg: Port :%u "
                  "Timer Id : %u PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_DREQ_TMR));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Unable to restart Delay Request timer "
                  "Port : %u Timer Id : %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber, PTP_DREQ_TMR));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDresInitDresHandler                        */
/*                                                                           */
/* Description               : This routine initialses the Delay Response    */
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDresInitDresHandler (VOID)
{
    PTP_FN_ENTRY ();

    /* Initialize the message handler function pointer */
    gPtpGlobalInfo.PtpMsgFn[PTP_DRES_FN_PTR].pMsgRxHandler =
        PtpDrespHandleRxDelayRespMsg;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDresCopyHdrFieldFrmDelayReq                */
/*                                                                           */
/* Description               : This routine copies the necessary parameters  */
/*                             from the received delay request message into  */
/*                             delay response message that needs to be       */
/*                             transmitted.                                  */
/*                                                                           */
/* Input                     : pu1DelayReq - Received Delay Request Message  */
/*                             pu1DelayResp - Formed Delay Response message. */
/*                             pPtpPort - Pointer to Port                    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDresCopyHdrFieldFrmDelayReq (UINT1 *pu1DelayReq, UINT1 *pu1DelayResp,
                                tPtpPort * pPtpPort, UINT1 u1MsgType)
{
    FS_UINT8            u8CorrnField;
    UINT4               u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    UINT1              *pu1TmpPdu = pu1DelayResp;
    UINT1              *pu1TmpDelayReq = NULL;
    UINT2               u2SequenceId = 0;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8CorrnField);

    /* Refer Section 11.3 of IEEE Std 1588-2008
     * The following fields need to be copied from the received delay request
     * message.
     * 1) Sequence Identifier.
     * 2) Source Port Identifier.
     * 3) Domain Number
     * 4) Correction Field
     * */

    /* Domain Identifier */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Copying the domain identifier from Delay request message "
              "to the delay response message....\r\n"));

    pu1TmpPdu = pu1DelayResp + PTP_MSG_DOMAIN_OFFSET + u4MediaHdrLen;
    PTP_GET_1_BYTE_FROM_DELAY_REQ (u1DomainId, pu1DelayReq,
                                   PTP_MSG_DOMAIN_OFFSET);
    PTP_LBUF_PUT_1_BYTE (pu1TmpPdu, u1DomainId);

    /* Sequence identifier */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Copying the Sequence identifier from Delay request message "
              "to the delay response message....\r\n"));

    pu1TmpPdu = pu1DelayResp + PTP_SEQ_OFFSET + u4MediaHdrLen;
    pu1TmpDelayReq = pu1DelayReq + PTP_SEQ_OFFSET;

    PTP_LBUF_GET_2_BYTES (pu1TmpDelayReq, u2SequenceId);
    PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2SequenceId);

    /* Correction Field. There is no need to move offset of Media header for
     * the received message, the message would have been stripped of these
     * header at ingress of PTP by ptpque.c 
     * */
    pu1TmpDelayReq = pu1DelayReq + PTP_MSG_CORRECTION_FIELD_OFFSET;

    MEMCPY (&(u8CorrnField.u4Hi), pu1TmpDelayReq, sizeof (UINT4));
    u8CorrnField.u4Hi = (UINT4) OSIX_NTOHL (u8CorrnField.u4Hi);

    pu1TmpDelayReq = pu1TmpDelayReq + sizeof (UINT4);

    MEMCPY (&(u8CorrnField.u4Lo), pu1TmpDelayReq, sizeof (UINT4));
    u8CorrnField.u4Lo = (UINT4) OSIX_NTOHL (u8CorrnField.u4Lo);

    /* Copying into the Delay response message */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Copying the Correction Field from Delay request message "
              "to the delay response message....\r\n"));

    pu1TmpPdu = pu1DelayResp + PTP_MSG_CORRECTION_FIELD_OFFSET + u4MediaHdrLen;
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrnField.u4Hi);
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrnField.u4Lo);

    if (u1MsgType == PTP_PEER_DELAY_RESP_MSG)
    {
        /* update the value in the data base for Follow up Tx.
         * */
        UINT8_LO (&(pPtpPort->u8PDelCorrectionField)) =
            UINT8_LO (&u8CorrnField);
        UINT8_HI (&(pPtpPort->u8PDelCorrectionField)) =
            UINT8_HI (&u8CorrnField);
    }

    PTP_FN_EXIT ();
}
#endif /* _PTPDRESP_C_ */
