/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpptrc.c,v 1.3 2014/01/24 12:20:09 siva Exp $
 *
 * Description: This file contains PTP Path Trace Sub module functionality
 *              routines.
 *********************************************************************/
#ifndef _PTPPTRC_C_
#define _PTPPTRC_C_

#include "ptpincs.h"

PRIVATE UINT4
      PtpPTrcGetLLMTU (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId);

/*****************************************************************************/
/* Function     : PtpPTrcIsPathTraceEnabled                                  */
/*                                                                           */
/* Description  : This is the used to check the path trace option for the    */
/*                given context, domain is enabled or not.                   */
/*                                                                           */
/* Input        : pPtpDomain  - Domain Pointer                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpPTrcIsPathTraceEnabled (tPtpDomain * pPtpDomain)
{
    PTP_FN_ENTRY ();

    if (pPtpDomain->bPathTraceOption == OSIX_TRUE)
    {
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpPTrcIsPathTraceEnabled : Enabled.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_TRUE);
    }

    PTP_FN_EXIT ();
    return (OSIX_FALSE);
}

/*****************************************************************************/
/* Function     : PtpPTrcInitPTrcList                                        */
/*                                                                           */
/* Description  : This is the used to empty the path trace list for the      */
/*                ginve context, domain .                                    */
/*                                                                           */
/* Input        : pPtpPort    - PTP Port Pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPTrcInitPTrcList (tPtpDomain * pPtpDomain)
{
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    if (pPtpDomain->pu1PathTraceTLV != NULL)
    {
        /* Free the Memory */
        i4Status = MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PTrcBuddyId,
                                 pPtpDomain->pu1PathTraceTLV);
        if (i4Status == BUDDY_FAILURE)
        {
            PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpPTrcInitPTrcList : PT TLV BuddyFree Failed.\r\n"));
            PTP_FN_EXIT ();
            return;
        }
        pPtpDomain->pu1PathTraceTLV = NULL;
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpPTrcIsLoopedAnnounceMsg                                 */
/*                                                                           */
/* Description  : This function is called only if the path trace is enabled  */
/*                in the given domain from the announce message handler      */
/*                to check the received announce message packet is looped    */
/*                packet or not.                                             */
/*                                                                           */
/* Input        : pPtpDomain  - Domain Pointer                               */
/*              : pMsgSrcClkId- Source Clock Identifier.                     */
/*              : pu1PTrcTlv  - Pointer to the Path Trace TLV.               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpPTrcIsLoopedAnnounceMsg (tPtpDomain * pPtpDomain, tClkId * pMsgSrcClkId,
                            UINT1 *pu1PTrcTlv)
{
    UINT1              *pu1TmpPTrcTlv = NULL;
    INT4                i4Status = 0;
    UINT2               u2TlvLen = 0;
    UINT2               u2TotTlvLen = 0;
    UINT2               u2OffSet = 0;

    PTP_FN_ENTRY ();

    /* Check the Clock Type is boundary clock and 
     * Check Announce messages from the current parent.
     */
    if ((pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE) ||
        (MEMCMP (pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.ClkId, pMsgSrcClkId,
                 PTP_MAX_CLOCK_ID_LEN) != 0))
    {
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpPTrcIsLoopedAnnounceMsg : Not form Master Clock "
                  "or Current Clock Mode is not BoundaryClock.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FALSE);
    }
    pu1TmpPTrcTlv = pu1PTrcTlv;
    pu1TmpPTrcTlv = pu1TmpPTrcTlv + PTP_TLV_LEN_FIELD_OFFSET;
    PTP_LBUF_GET_2_BYTES (pu1TmpPTrcTlv, u2TlvLen);

    u2TotTlvLen = (UINT2) (PTP_TLV_DATA_FIELD_OFFSET + u2TlvLen);
    /* Check the Clock own ClockId is in the Path trace TVL */
    for (u2OffSet = (UINT2) PTP_TLV_DATA_FIELD_OFFSET; u2OffSet < u2TotTlvLen;
         u2OffSet = (UINT2) (u2OffSet + PTP_MAX_CLOCK_ID_LEN))
    {
        if (MEMCMP (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId,
                    &(pu1PTrcTlv[u2OffSet]), PTP_MAX_CLOCK_ID_LEN) == 0)
        {
            /* Looped Annc Msg */
            PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                      PTP_CRITICAL_TRC,
                      "PtpPTrcIsLoopedAnnounceMsg : Looped Announce Msg.\r\n"));

            PTP_FN_EXIT ();

            return (OSIX_TRUE);
        }
    }

    /* Update the PathTraceTLV list with pu1PTrcTLV */
    if (pPtpDomain->pu1PathTraceTLV != NULL)
    {
        if (MEMCMP (pPtpDomain->pu1PathTraceTLV, pu1PTrcTlv, u2TotTlvLen) == 0)
        {
            PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpPTrcIsLoopedAnnounceMsg : Same Path Trace TLV.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FALSE);
        }
        /* Not Same TLV, update the PathTraceTlvDB */
        i4Status = MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PTrcBuddyId,
                                 pPtpDomain->pu1PathTraceTLV);
        if (i4Status == BUDDY_FAILURE)
        {
            PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpPTrcIsLoopedAnnounceMsg : PT TLV BuddyFree "
                      "Failed.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FALSE);
        }
        pPtpDomain->pu1PathTraceTLV = NULL;
    }

    pPtpDomain->pu1PathTraceTLV =
        MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PTrcBuddyId, u2TotTlvLen);
    if (pPtpDomain->pu1PathTraceTLV != NULL)
    {
        MEMCPY (pPtpDomain->pu1PathTraceTLV, pu1PTrcTlv, u2TotTlvLen);
    }
    else
    {
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpPTrcIsLoopedAnnounceMsg : PT TLV MemBuddyAlloc "
                  "Failed.\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
    }
    PTP_FN_EXIT ();
    return (OSIX_FALSE);
}

/*****************************************************************************/
/* Function     : PtpPTrcAddPTrcTLV                                          */
/*                                                                           */
/* Description  : This function is called only if the path trace is enabled  */
/*                in the given domain from the announce message handler      */
/*                to add the Path trace TLV on transmit announce             */
/*                message.                                                   */
/*                                                                           */
/* Input        : pPtpPort    - PTP Port Pointer                             */
/*              : pu1PtpPdu   - Pointer to the Announce message PDU.         */
/*              : pu4MsgLen   - Pointer to the Announce message PDU length   */
/*                                                                           */
/* Output       : pu4MsgLen   - Updated for the added path trace tlv         */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPTrcAddPTrcTLV (tPtpPort * pPtpPort, UINT1 *pu1PtpPdu, UINT4 *pu4MsgLen)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT1              *pu1TmpPtpPdu = NULL;
    UINT1              *pu1TmpPTrcTlv = NULL;
    UINT4               u4MTU = 0;
    UINT2               u2AuthTlvLen = 0;
    UINT2               u2TotalMsgLen = 0;
    UINT2               u2TlvLen = 0;
    UINT2               u2TotTlvLen = 0;
    UINT2               u2Val = 0;

    PTP_FN_ENTRY ();

    /* PATH_TRACE TLV shall be the <pathTraceList> + clock's clockId
     * If the resulting Announce message size exceeds the maximum
     * frame size permitted by the network technology, the PATH_TRACE TLV 
     * shall not be appended.*/

    pPtpDomain = pPtpPort->pPtpDomain;

    pu1TmpPtpPdu = pu1PtpPdu;

    if (pPtpDomain->bSecurityEnabled == OSIX_TRUE)
    {
        /* AUTH Tlv Max Len (30) */
        u2AuthTlvLen = PTP_MAX_AUTH_TLV_SIZE;
    }

    u4MTU = PtpPTrcGetLLMTU (pPtpPort->u4ContextId,
                             pPtpPort->u1DomainId,
                             pPtpPort->PortDs.u4PtpPortNumber);

    if (pPtpDomain->pu1PathTraceTLV != NULL)
    {
        /* Get the Length */
        pu1TmpPTrcTlv = pPtpDomain->pu1PathTraceTLV;
        pu1TmpPTrcTlv = pu1TmpPTrcTlv + PTP_TLV_LEN_FIELD_OFFSET;
        PTP_LBUF_GET_2_BYTES (pu1TmpPTrcTlv, u2TlvLen);
    }

    u2TotalMsgLen = (UINT2) (*pu4MsgLen + u2TlvLen +
                             PTP_TLV_DATA_FIELD_OFFSET +
                             PTP_MAX_CLOCK_ID_LEN + u2AuthTlvLen);

    /* u4MTU < (u4MsgLen + u2PTTlvLen + OwnClkId (8) + AUTH Tlv Len) */
    if (u4MTU < u2TotalMsgLen)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpPTrcAddPTrcTLV : "
                  "Packet size > Lower Layer MTU. \r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Add the PathTraceTLV + ClkId  */
    pu1TmpPtpPdu = pu1PtpPdu + (*pu4MsgLen);

    if (pPtpDomain->pu1PathTraceTLV != NULL)
    {
        u2TlvLen = (UINT2) (u2TlvLen + PTP_TLV_DATA_FIELD_OFFSET);
        PTP_LBUF_PUT_N_BYTES (pu1TmpPtpPdu, pPtpDomain->pu1PathTraceTLV,
                              u2TlvLen);
        u2TotTlvLen = u2TlvLen;
    }
    else
    {
        /* Added the Tlv Type and Tlv length */
        u2TotTlvLen = (UINT2) (u2TlvLen + PTP_TLV_DATA_FIELD_OFFSET);
        u2Val = (UINT2) PTP_TLV_TYPE_PATH_TRACE;
        PTP_LBUF_PUT_2_BYTES (pu1TmpPtpPdu, u2Val);
        PTP_LBUF_PUT_2_BYTES (pu1TmpPtpPdu, u2TotTlvLen);
    }

    PTP_LBUF_PUT_N_BYTES (pu1TmpPtpPdu,
                          pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId,
                          PTP_MAX_CLOCK_ID_LEN);
    u2TotTlvLen += PTP_MAX_CLOCK_ID_LEN;

    /* Update the Path trace TLV Length */
    pu1TmpPtpPdu = pu1PtpPdu + (*pu4MsgLen) + PTP_TLV_LEN_FIELD_OFFSET;
    u2Val = (UINT2) (u2TotTlvLen - PTP_TLV_DATA_FIELD_OFFSET);
    PTP_LBUF_PUT_2_BYTES (pu1TmpPtpPdu, u2Val);

    /* Update the u4MsgLen */
    *pu4MsgLen += u2TotTlvLen;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpPTrcGetLLMTU                                            */
/*                                                                           */
/* Description  : This function used to get the PTP Port's MTU Value         */
/*                It will get the lower layer MTU of the PTP Port            */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Port Identifier.                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : MTU of that given PTP Port                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
PtpPTrcGetLLMTU (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId)
{
    UINT4               u4MTU = PTP_PTRC_LINK_MTU;

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1DomainId);
    UNUSED_PARAM (u4PortId);

    return (u4MTU);
}

#endif /*  _PTPPTRC_C_ */

/***************************************************************************
 *                         END OF FILE ptpptrc.c                           *
 ***************************************************************************/
