/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains PTP task stub routines.              
 *********************************************************************/
#ifndef _PTPSTUBS_C
#define _PTPSTUBS_C

#include "ptpincs.h"

#ifndef ANNC_MSG_HANDLER
/*****************************************************************************/
/* Function                  : PtpAnncInitAnncHandler                        */
/*                                                                           */
/* Description               : This routine initialses the announce handler  */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpAnncInitAnncHandler (VOID)
{
    gPtpGlobalInfo.PtpMsgFn[PTP_ANNC_FN_PTR].pMsgRxHandler = NULL;
}
#endif

#ifndef DELAY_REQ_MSG_HANDLER
/*****************************************************************************/
/* Function                  : PtpDreqInitDreqHandler                        */
/*                                                                           */
/* Description               : This routine initialses the Delay Request     */
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDreqInitDreqHandler (VOID)
{
    gPtpGlobalInfo.PtpMsgFn[PTP_DREQ_FN_PTR].pMsgRxHandler = NULL;
}
#endif

#ifndef DELAY_RESP_MSG_HANDLER
/*****************************************************************************/
/* Function                  : PtpDresInitDresHandler                        */
/*                                                                           */
/* Description               : This routine initialses the Delay Response    */
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDresInitDresHandler (VOID)
{
    gPtpGlobalInfo.PtpMsgFn[PTP_DRES_FN_PTR].pMsgRxHandler = NULL;
}
#endif

#ifndef TRANSPARENT_CLK
/*****************************************************************************/
/* Function                  : PtpPdreqInitPdreqHandler                      */
/*                                                                           */
/* Description               : This routine initialses the Peer Delay Request*/
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPdreqInitPdreqHandler (VOID)
{
    gPtpGlobalInfo.PtpMsgFn[PTP_PDREQ_FN_PTR].pMsgRxHandler = NULL;
}

/*****************************************************************************/
/* Function                  : PtpPdresInitPeerDelayRespHandler              */
/*                                                                           */
/* Description               : This routine initialses the Peer Delay Resp   */
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPdresInitPeerDelayRespHandler (VOID)
{
    gPtpGlobalInfo.PtpMsgFn[PTP_PDRES_FN_PTR].pMsgRxHandler = NULL;
}
#endif
#endif /* _PTPSTUBS_C */
