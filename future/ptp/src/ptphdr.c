/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *$Id: ptphdr.c,v 1.3 2014/02/03 12:27:08 siva Exp $
 *
 * Description: This file contains PTP module header formation including
 *              PTP header as well as UDP/802.3 headers. 
 *********************************************************************/
#ifndef _PTPHDR_C_
#define _PTPHDR_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpHdrFillPtpHeader                           */
/*                                                                           */
/* Description               : This routine fills the PTP header for the     */
/*                             given message.                                */
/*                                                                           */
/* Input                     : pPtpPdu  - Pointer to the PDU.                */
/*                             pPtpPort - Pointer to the port structure.     */
/*                             u1MsgType- Type of the Message                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpHdrFillPtpHeader (UINT1 **ppPtpPdu, tPtpPort * pPtpPort, UINT1 u1MsgType)
{
    FS_UINT8            u8CorrectionField;
    FS_UINT8            u8Result;
    UINT1              *pPtpPdu = *ppPtpPdu;
    tClkSysTimeInfo     PtpSysTimeInfo;
    UINT4               u4Reserved = 0;
    UINT2               u2FlagField = 0;
    UINT2               u2SeqId = 0;
    UINT2               u2Val = 0;
    UINT2               u2TwoByteVal = 0;
    UINT1               u1OneByteVal = 0;
    UINT1               u1LogInterval = (UINT1) PTP_UNICAST_LOG_MSG_INTERVAL;
    UINT1               u1ControlField = 0;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8CorrectionField);
    FSAP_U8_CLR (&u8Result);

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    /* Refer Section 13 & Table 18 of IEEE Std 1588-2008 for PTP message
     * header format
     * */

    u1OneByteVal = PTP_TRANSPORT_SPECIFIC_VALUE;
    u1OneByteVal = (UINT1) (u1OneByteVal << PTP_TRANS_SPC_BITS_SHIFT);
    u1OneByteVal |= (UINT1) (u1MsgType & PTP_MSG_TYPE_MASK);

    PTP_LBUF_PUT_1_BYTE (pPtpPdu, u1OneByteVal);

    /* Reserved + Version */
    u1OneByteVal = 0;
    u1OneByteVal =
        (UINT1) (pPtpPort->PortDs.u4VersionNumber & PTP_VERSION_MASK);

    PTP_LBUF_PUT_1_BYTE (pPtpPdu, u1OneByteVal);

    /* MessageLength */
    if (u1MsgType == PTP_ANNC_MSG)
    {
        u2TwoByteVal = PTP_ANNC_MSG_SIZE;
    }
    else if ((u1MsgType == PTP_SYNC_MSG) || (u1MsgType == PTP_FOLLOW_UP_MSG) ||
             (u1MsgType == PTP_DELAY_REQ_MSG))
    {
        u2TwoByteVal = PTP_SYNC_MSG_SIZE;
    }
    else if ((u1MsgType == PTP_PEER_DELAY_REQ_MSG) ||
             (u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
             (u1MsgType == PTP_DELAY_RESP_MSG))
    {
        u2TwoByteVal = PTP_MSG_DELAY_RESP_LEN;
    }
    PTP_LBUF_PUT_2_BYTES (pPtpPdu, u2TwoByteVal);

    /* Domain number */
    PTP_LBUF_PUT_1_BYTE (pPtpPdu, pPtpPort->pPtpDomain->u1DomainId);

    /* Reserved */
    u1OneByteVal = 0;
    PTP_LBUF_PUT_1_BYTE (pPtpPdu, u1OneByteVal);

    /* FlagField will be updated based on the transmitted message */
    switch (u1MsgType)
    {
        case PTP_SYNC_MSG:

            u1LogInterval = pPtpPort->PortDs.i1SyncInterval;
            u2SeqId = pPtpPort->PtpPortSeqIdPool.u2SyncSeqId;
            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag
                == PTP_DISABLED)
            {
                /* For two step clocks, Follow up uses the same sequence
                 * Id. Hence increment while doing Follow up egress
                 * */
                pPtpPort->PtpPortSeqIdPool.u2SyncSeqId++;
            }

            if ((PtpUtilGetAltMstFlagStatus (pPtpPort)) == OSIX_TRUE)
            {
                PTP_SET_ALT_MST_FLAG (u2FlagField);
            }
            if ((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag)
                == PTP_ENABLED)
            {
                PTP_SET_TWO_STEP_FLAG (u2FlagField);
            }
            break;

        case PTP_PEER_DELAY_RESP_MSG:
            /* Following flags need to be updated
             * twoStepFlag */
            if ((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag)
                == PTP_ENABLED)
            {
                PTP_SET_TWO_STEP_FLAG (u2FlagField);
            }

            /* This is not a sync message and should be Peer Delay 
             * response. Fill 0x7F. Refer Table 24 of IEEE Std 1588-2008
             * */
            u1LogInterval = (UINT1) PTP_UNICAST_LOG_MSG_INTERVAL;
            break;

        case PTP_PEER_DELAY_REQ_MSG:

            u2SeqId = pPtpPort->PtpPortSeqIdPool.u2PDelayReqSeqId;
            pPtpPort->PtpPortSeqIdPool.u2PDelayReqSeqId++;

            break;

        case PTP_ANNC_MSG:
            /* Following flags need to be updated 
             * 1) Alternate Master
             * 2) leap61
             * 3) leap59
             * 4) currentUtcOffsetValid
             * 5) ptp Timescale
             * 6) time Traceable
             * 7) frequency Traceable
             * */

            if ((PtpUtilGetAltMstFlagStatus (pPtpPort)) == OSIX_TRUE)
            {
                PTP_SET_ALT_MST_FLAG (u2FlagField);
            }

            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bLeap61
                == OSIX_TRUE)
            {
                PTP_SET_LEAP61_FIELD (u2FlagField);
            }

            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bLeap59
                == OSIX_TRUE)
            {
                PTP_SET_LEAP59_FIELD (u2FlagField);
            }

            if ((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                 TimeDs.bCurrentUtcOffsetValid) == OSIX_TRUE)
            {
                PTP_SET_UTC_OFFSET_FIELD (u2FlagField);
            }

            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bPtpTimeScale
                == OSIX_TRUE)
            {
                PTP_SET_PTP_TIMESCALE_FIELD (u2FlagField);
            }

            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bTimeTraceable
                == OSIX_TRUE)
            {
                PTP_SET_TIME_TRC_FIELD (u2FlagField);
            }

            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bFreqTraceable
                == OSIX_TRUE)
            {
                PTP_SET_FREQ_TRC_FIELD (u2FlagField);
            }

            /* This should be Announce message. Fill the log Announce
             * interval. Refer Table 24 of IEEE Std 1588-2008
             * */
            u1LogInterval = pPtpPort->PortDs.u1AnnounceInterval;

            u2SeqId = pPtpPort->PtpPortSeqIdPool.u2AnncSeqId;
            pPtpPort->PtpPortSeqIdPool.u2AnncSeqId++;

            break;

        case PTP_DELAY_RESP_MSG:
            if ((PtpUtilGetAltMstFlagStatus (pPtpPort)) == OSIX_TRUE)
            {
                PTP_SET_ALT_MST_FLAG (u2FlagField);
            }

            u1LogInterval = pPtpPort->PortDs.u1MinDelayReqInterval;

            break;

        case PTP_DELAY_REQ_MSG:

            u2SeqId = pPtpPort->PtpPortSeqIdPool.u2DelayReqSeqId;
            pPtpPort->PtpPortSeqIdPool.u2DelayReqSeqId++;

            break;

        case PTP_FOLLOW_UP_MSG:

            if ((PtpUtilGetAltMstFlagStatus (pPtpPort)) == OSIX_TRUE)
            {
                PTP_SET_ALT_MST_FLAG (u2FlagField);
            }

            u1LogInterval = pPtpPort->PortDs.i1SyncInterval;
            u2SeqId = pPtpPort->PtpPortSeqIdPool.u2SyncSeqId;
            pPtpPort->PtpPortSeqIdPool.u2SyncSeqId++;

            break;

        default:
            /* Following flags need to be updated
             * 1) unicastFlag
             * 2) PTP profile Specific 1
             * 3) PTP profile Specific 
             * */
            u1LogInterval = (UINT1) PTP_UNICAST_LOG_MSG_INTERVAL;

            break;
    }

    PTP_LBUF_PUT_2_BYTES (pPtpPdu, u2FlagField);

    if ((u1MsgType == PTP_SYNC_MSG) &&
        (PtpCxtIsPrimaryCxtAndDmn (pPtpPort) == OSIX_FALSE))
    {
        /* Message is SYNC message and is not transmitted for primary context
         * and primary domain. Hence updating the rate difference calculated
         * using syntonization.
         * */
        PtpPortClkIwfGetClock (&PtpSysTimeInfo);
        UINT8_LO (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec)) =
            UINT8_LO (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec)) +
            PtpSysTimeInfo.FsClkTimeVal.u4Nsec /
            PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
        FSAP_U8_MUL (&u8Result, &(pPtpPort->PortDs.u8SyntRatio),
                     &(PtpSysTimeInfo.FsClkTimeVal.u8Sec));

        PTP_LBUF_PUT_4_BYTES (pPtpPdu, u8Result.u4Hi);
        PTP_LBUF_PUT_4_BYTES (pPtpPdu, u8Result.u4Lo);
    }
    else
    {
        /* Correction field needs to be updated based on the message types.
         * It can be left as 0 for Announce, signalling and management messages
         * */
        PTP_LBUF_PUT_4_BYTES (pPtpPdu, u8CorrectionField.u4Hi);
        PTP_LBUF_PUT_4_BYTES (pPtpPdu, u8CorrectionField.u4Lo);
    }
    /* Reserved for 4 bytes */
    PTP_LBUF_PUT_4_BYTES (pPtpPdu, u4Reserved);

    /* Port Identity */
    PTP_LBUF_PUT_N_BYTES (pPtpPdu, &(pPtpPort->PortDs.ClkId),
                          PTP_MAX_CLOCK_ID_LEN);

    u2Val = (UINT2) pPtpPort->PortDs.u4PtpPortNumber;
    PTP_LBUF_PUT_2_BYTES (pPtpPdu, u2Val);

    /* Sequence identifier */
    PTP_LBUF_PUT_2_BYTES (pPtpPdu, u2SeqId);

    PTP_GET_CONTROL_FIELD_FOR_MSG (u1MsgType, u1ControlField);

    PTP_LBUF_PUT_1_BYTE (pPtpPdu, u1ControlField);

    /* Log Message Interval */
    PTP_LBUF_PUT_1_BYTE (pPtpPdu, u1LogInterval);

    *ppPtpPdu = pPtpPdu;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpHdrAddMediaHdrForIntf                      */
/*                                                                           */
/* Description               : This routine fills the transport specific     */
/*                             header for the given message based on the     */
/*                             interface type.                               */
/*                                                                           */
/* Input                     : pPtpPdu  - Pointer to the PDU.                */
/*                             pPtpPort - Pointer to the port structure.     */
/*                             u1MsgType- Type of the Message                */
/*                                                                           */
/* Output                    : pu4MsgLen - Msg Length pointer                */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpHdrAddMediaHdrForIntf (UINT1 **ppPtpPdu, tPtpPort * pPtpPort,
                          UINT1 u1MsgType, UINT4 *pu4MsgLen)
{
    tCfaIfInfo          CfaIfInfo;
    tMacAddr            MacAddr;
    UINT1              *pPtpPdu = *ppPtpPdu;
    UINT2               u2Val = 0;

    PTP_FN_ENTRY ();

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));

    if ((pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV4) ||
        (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV6))
    {
        /* For UDP interfaces, the socket itself will be able to fill the
         * header. Hence, header is not added explicitly for UDP interfaces 
         * */
        PTP_FN_EXIT ();
        return;
    }

    if ((u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_REQ_MSG))
    {
        /* Peer delay mechanism will be having the destination MAC address
         * falling in the range of Reserved MAC Address, so that they are
         * not blocked by Spanning tree operation
         * */
        MEMCPY (pPtpPdu, gau1PtpP2PDestMacAddr, MAC_ADDR_LEN);
    }
    else
    {
        MEMCPY (pPtpPdu, gau1PtpDestMacAddr, MAC_ADDR_LEN);
    }
    pPtpPdu = pPtpPdu + MAC_ADDR_LEN;

    if (pPtpPort->PtpDeviceType == PTP_IFACE_IEEE_802_3)
    {
        /* Physical interface */
        PtpPortCfaGetIfInfo (pPtpPort->u4IfIndex, &CfaIfInfo);
        MEMCPY (pPtpPdu, &(CfaIfInfo.au1MacAddr), MAC_ADDR_LEN);
    }
    else
    {
        /* Get the Context MAC Address for sending over VLAN */
        PtpPortGetSysMacAddress (pPtpPort->u4ContextId, MacAddr);
        MEMCPY (pPtpPdu, MacAddr, MAC_ADDR_LEN);
    }

    pPtpPdu = pPtpPdu + MAC_ADDR_LEN;

    u2Val = CFA_ENET_PTP;
    PTP_LBUF_PUT_2_BYTES (pPtpPdu, u2Val);

    *pu4MsgLen = *pu4MsgLen + PTP_LLC_HDR_SIZE;

    *ppPtpPdu = pPtpPdu;

    PTP_FN_EXIT ();
}
#endif /* _PTPHDR_C_ */
