/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpsecsa.c,v 1.4 2014/01/30 12:20:35 siva Exp $
 *
 * Description: This file contains PTP context related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPSECSA_C_
#define _PTPSECSA_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpSecSACreate                                */
/*                                                                           */
/* Description               : This routine creates the security association */
/*                             data set                                      */
/*                                                                           */
/* Input                     : u4ContextId   -  Context identifier           */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4SAId        -  KeyId                        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.SAPoolId                       */
/*                             gPtpGlobalInfo.saList                         */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpSADs    *
PtpSecSACreate (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4SAId)
{
    tPtpSADs           *pPtpSADs = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    PTP_FN_ENTRY ();

    /* Check whether the domain exists */

    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);

    if (pPtpDomain == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Domain does not exists\r\n"));

        PTP_FN_EXIT ();
        return NULL;
    }

    pPtpSADs = (tPtpSADs *) MemAllocMemBlk (gPtpGlobalInfo.SAPoolId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Memory Allocation failed for security association\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    MEMSET (pPtpSADs, 0, sizeof (tPtpSADs));

    pPtpSADs->u4ContextId = u4ContextId;
    pPtpSADs->u1DomainId = u1DomainId;
    pPtpSADs->u4SAId = u4SAId;

    PtpDbAddNode ((VOID *) &(gPtpGlobalInfo.saList), pPtpSADs,
                  (UINT1) PTP_SA_DATA_SET);

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Security association node is created successfully\r\n"));

    PTP_FN_EXIT ();
    return pPtpSADs;
}

/*****************************************************************************/
/* Function                  : PtpSecSADelete                                */
/*                                                                           */
/* Description               : This routine deletes the security association */
/*                             data set                                      */
/*                                                                           */
/* Input                     : pPtpSADs              - pointer to node       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.SAPoolId                       */
/*                             gPtpGlobalInfo.saList                         */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpSecSADelete (tPtpSADs * pPtpSADs)
{
#ifdef TRACE_WANTED
    UINT4               u4ContextId = pPtpSADs->u4ContextId;
    UINT1               u1DomainId = pPtpSADs->u1DomainId;
#endif

    PTP_FN_ENTRY ();

    PtpDbDeleteNode ((VOID *) &(gPtpGlobalInfo.saList), pPtpSADs,
                     (UINT1) PTP_SA_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.SAPoolId, (UINT1 *) pPtpSADs);
    pPtpSADs = NULL;

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Security association node is deleted successfully\r\n"));
}

/*****************************************************************************/
/* Function                  : PtpSecSAGetSAEntry                            */
/*                                                                           */
/* Description               : This routine gets the SA Entry form           */
/*                             the corresponding data base                   */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                             u4SAId        - SA identifier                 */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.saList                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpSADs    *
PtpSecSAGetSAEntry (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4SAId)
{
    tPtpSADs           *pPtpSADs = NULL;
    tPtpSADs           *pTmpPtpSADs = NULL;

    PTP_FN_ENTRY ();

    /* Allocate Memory for the Index */
    pPtpSADs = (tPtpSADs *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpSecSAGetPortEntry : Memory Allocation failed "
                  "for security association !!!\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    /* Assign Index */
    pPtpSADs->u4ContextId = u4ContextId;
    pPtpSADs->u1DomainId = u1DomainId;
    pPtpSADs->u4SAId = u4SAId;

    /* Get the Node form the Index */
    pTmpPtpSADs = (tPtpSADs *) PtpDbGetNode ((VOID *) &(gPtpGlobalInfo.saList),
                                             pPtpSADs, (UINT1) PTP_SA_DATA_SET);

    /* Release Memory allocated for the Index */
    if ((MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                             (UINT1 *) pPtpSADs)) == MEM_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpSecSAGetPortEntry : MemReleaseMemBlock Failed \r\n"));
    }
    pPtpSADs = NULL;
    PTP_FN_EXIT ();
    return (pTmpPtpSADs);
}

/*****************************************************************************/
/* Function                  : PtpSecSAGetNextSAEntry                        */
/*                                                                           */
/* Description               : This routine gets next the SA Entry form      */
/*                             the corresponding data base                   */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                             u4SAId        - SA identifier                 */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.PortPoolId                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpSADs    *
PtpSecSAGetNextSAEntry (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4SAId)
{
    tPtpSADs           *pPtpSADs = NULL;
    tPtpSADs           *pTmpPtpSADs = NULL;

    PTP_FN_ENTRY ();

    /* Allocate Memory for the Index */
    pPtpSADs = (tPtpSADs *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpIfGetNextSAEntry : Memory Allocation failed "
                  "for security association !!!\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    /* Assign Index */
    pPtpSADs->u4ContextId = u4ContextId;
    pPtpSADs->u1DomainId = u1DomainId;
    pPtpSADs->u4SAId = u4SAId;

    /* Get the Node form the Index */
    pTmpPtpSADs = (tPtpSADs *) PtpDbGetNextNode
        ((VOID *) &(gPtpGlobalInfo.saList), pPtpSADs, (UINT1) PTP_SA_DATA_SET);

    /* Release Memory allocated for the Index */
    if ((MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                             (UINT1 *) pPtpSADs)) == MEM_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpIfGetNextSAEntry : MemReleaseMemBlock Failed \r\n"));
    }
    pPtpSADs = NULL;
    PTP_FN_EXIT ();
    return (pTmpPtpSADs);
}
#endif /*_PTPSECSA_C_*/
