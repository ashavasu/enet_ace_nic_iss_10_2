/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpbmc.c,v 1.5 2014/01/24 12:20:08 siva Exp $
 *
 *
 * Description: This file contains PTP Best Master Clock Algorithm 
 *              Implementation. 
 *********************************************************************/
#ifndef _PTPBMC_C_
#define _PTPBMC_C_

#include "ptpincs.h"

PRIVATE INT4        PtpBmcCalcErBest (tPtpPort * pPtpPort);

PRIVATE INT4        PtpBmcCompareDataSets (tPtpForeignMasterDs *
                                           pPtpForeignMaster1,
                                           tPtpForeignMasterDs *
                                           pPtpForeignMaster2,
                                           UINT4 *pu4DiffDataSet);

PRIVATE INT4        PtpBmcDetermineDataSetOrder (INT4 i4DsOrder,
                                                 UINT4 *pu4DiffDataSet);

PRIVATE INT4        PtpBmcCmpDataSetForEqualGm (tPtpForeignMasterDs *
                                                pPtpForeignMasterDsA,
                                                tPtpForeignMasterDs *
                                                pPtpForeignMasterDsB,
                                                UINT4 *pu4DiffDataSet);

PRIVATE INT4        PtpBmcCalcEBest (tPtpDomain * pPtpDomain);

PRIVATE INT4        PtpBmcTriggStateDecisionForDmn (tPtpDomain * pPtpDomain,
                                                    tPtpForeignMasterDs *
                                                    pPtpEbestMsg);
/*****************************************************************************/
/* Function                  : PtpBmcTriggerBmcCalc                          */
/*                                                                           */
/* Description               : This routine starts the BMC calculation for   */
/*                             the node. This compares the data sets received*/
/*                             within the ports and calculates the best msg  */
/*                             received over the port. Then this calculates  */
/*                             the best message amongst the best received    */
/*                             over each interface to find the current master*/
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to the domain.           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpBmcTriggerBmcCalc (tPtpDomain * pPtpDomain)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* BMC is applicable only for ordinary and boundary clocks */
    if ((pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE) &&
        (pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE))
    {
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC, "Clock mode is not in boundary or "
                  "ordinary clock\r\n", pPtpDomain));
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC, "Not triggering BMC.\r\n"));

        PTP_FN_EXIT ();
        return;
    }

    /* For each port present in the domain, calculate ErBest 
     * */
    for (u4Port = 1; u4Port <= PTP_MAX_PORTS; u4Port++)
    {
        OSIX_BITLIST_IS_BIT_SET (pPtpDomain->DomainPortList, u4Port,
                                 PTP_PORT_LIST_SIZE, bResult);

        if (bResult != OSIX_TRUE)
        {
            /* Port is not part of the domain */
            continue;
        }

        /* DATA Base Access */
        pPtpPort = PtpIfGetPortEntry (pPtpDomain->u4ContextId,
                                      pPtpDomain->u1DomainId, u4Port);
        if ((pPtpPort == NULL) ||
            (pPtpPort->PortDs.u1PortState == PTP_STATE_DISABLED))
        {
            /* Invalid port */
            continue;
        }
        PtpBmcCalcErBest (pPtpPort);

        /* Reinitializing */
        bResult = OSIX_FALSE;
    }

    PtpBmcCalcEBest (pPtpDomain);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpBmcCalcErBest                              */
/*                                                                           */
/* Description               : This routine performs the data set comparision*/
/*                             data set algorithm as per Section 9.3.4 of    */
/*                             IEEE 1588 2008 standard. This routine uses the*/
/*                             algorithm to calculate the best message       */
/*                             received over the port.                       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpBmcCalcErBest (tPtpPort * pPtpPort)
{
    tPtpForeignMasterDs PtpForeignMasterDs2;
    tPtpForeignMasterDs *pPtpForeignMasterDs1 = NULL;
    tPtpForeignMasterDs *pPtpForeignMasterDs2 = NULL;
    tPtpForeignMasterDs *pPtpFreeForenMaster = NULL;
    INT4                i4BmcOutPut = 0;

    PTP_FN_ENTRY ();

    /* Following procedure is used to calculate the Erbest per port
     * 1) Walk through the foreign master table for the given port and compare 
     *    the same with each other to find the Erbest for the port.
     * 2) Then update this Erbest into the port data structure.
     * */
    MEMSET (&PtpForeignMasterDs2, 0, sizeof (tPtpForeignMasterDs));

    pPtpForeignMasterDs1 = PtpMastGetFirstFornMasterOnPort (pPtpPort);
    if (pPtpForeignMasterDs1 == NULL)
    {
        pPtpPort->pPtpErBestMsg = NULL;
        PTP_FN_EXIT ();
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpBmcCalcErBest:Port Has No Foreign Master !!!\r\n"));
        return (OSIX_FAILURE);
    }

    if (pPtpPort->pPtpErBestMsg == NULL)
    {
        /* Port is now only executing its first BMC calculation. Hence,
         * the value of Erbest is NULL */
        pPtpPort->pPtpErBestMsg = pPtpForeignMasterDs1;
    }

    pPtpForeignMasterDs2 = pPtpForeignMasterDs1;

    do
    {
        if (pPtpFreeForenMaster != NULL)
        {
            /* Some invalid announce message has been identified in the previous
             * iteration
             * */
            if (pPtpFreeForenMaster == pPtpPort->pPtpErBestMsg)
            {
                pPtpPort->pPtpErBestMsg = NULL;
            }
            PtpDbDeleteNode (gPtpGlobalInfo.FMTree, pPtpFreeForenMaster,
                             (UINT1) PTP_FOREIGN_MASTER_DATA_SET);
            MemReleaseMemBlock (gPtpGlobalInfo.ForeignMasterPoolId,
                                (UINT1 *) pPtpFreeForenMaster);
            pPtpFreeForenMaster = NULL;
        }

        if (PtpAnncValidateForeignMaster (pPtpForeignMasterDs2) == OSIX_FAILURE)
        {
            /* The Foreign Master that is considered for BMC is not valid. So,
             * the entry can be freed and destroyed straight away. But this
             * pointer pPtpForeignMasterDs2 only is being used to walk the
             * RBTree. Hence copy this pointer in to be freed list. That value
             * will be freed in the next iteration.
             * */
            pPtpFreeForenMaster = pPtpForeignMasterDs2;

            PtpForeignMasterDs2.u4ContextId = pPtpForeignMasterDs2->u4ContextId;
            PtpForeignMasterDs2.u1DomainId = pPtpForeignMasterDs2->u1DomainId;
            PtpForeignMasterDs2.u4PortIndex = pPtpForeignMasterDs2->u4PortIndex;
            MEMCPY (PtpForeignMasterDs2.ClkId, pPtpForeignMasterDs2->ClkId,
                    PTP_MAX_CLOCK_ID_LEN);
            PtpForeignMasterDs2.u4SrcPortIndex =
                pPtpForeignMasterDs2->u4SrcPortIndex;
            continue;
        }

        i4BmcOutPut = PtpBmcDataSetCmpAlgo (pPtpPort->pPtpErBestMsg,
                                            pPtpForeignMasterDs2);
        if ((i4BmcOutPut == PTP_LESSER) ||
            (i4BmcOutPut == PTP_LESSER_BY_TOPOLOGY))
        {
            pPtpPort->pPtpErBestMsg = pPtpForeignMasterDs2;
        }

        pPtpForeignMasterDs2->u1BmcCnt++;

        PtpForeignMasterDs2.u4ContextId = pPtpForeignMasterDs2->u4ContextId;
        PtpForeignMasterDs2.u1DomainId = pPtpForeignMasterDs2->u1DomainId;
        PtpForeignMasterDs2.u4PortIndex = pPtpForeignMasterDs2->u4PortIndex;
        MEMCPY (PtpForeignMasterDs2.ClkId, pPtpForeignMasterDs2->ClkId,
                PTP_MAX_CLOCK_ID_LEN);
        PtpForeignMasterDs2.u4SrcPortIndex =
            pPtpForeignMasterDs2->u4SrcPortIndex;
    }
    while (((pPtpForeignMasterDs2 = (tPtpForeignMasterDs *)
             PtpDbGetNextNode (gPtpGlobalInfo.FMTree, &PtpForeignMasterDs2,
                               (UINT1) PTP_FOREIGN_MASTER_DATA_SET)) != NULL)
           && (pPtpForeignMasterDs2->u4PortIndex ==
               pPtpPort->PortDs.u4PtpPortNumber) &&
           (pPtpForeignMasterDs2->u4ContextId == pPtpPort->u4ContextId) &&
           (pPtpForeignMasterDs2->u1DomainId == pPtpPort->u1DomainId));

    if (pPtpFreeForenMaster != NULL)
    {
        /* Some invalid announce message has been identified in the last 
         * iteration
         * */
        if (pPtpFreeForenMaster == pPtpPort->pPtpErBestMsg)
        {
            pPtpPort->pPtpErBestMsg = NULL;
        }
        PtpDbDeleteNode (gPtpGlobalInfo.FMTree, pPtpFreeForenMaster,
                         (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

        MemReleaseMemBlock (gPtpGlobalInfo.ForeignMasterPoolId,
                            (UINT1 *) pPtpFreeForenMaster);
        pPtpFreeForenMaster = NULL;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpBmcCalcEBest                               */
/*                                                                           */
/* Description               : This routine performs the data set comparision*/
/*                             data set algorithm as per Section 9.3.4 of    */
/*                             IEEE 1588 2008 standard. This routine uses the*/
/*                             algorithm to calculate the best message       */
/*                             received over all the ports associated with   */
/*                             the domain.                                   */
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to the Domain.           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpBmcCalcEBest (tPtpDomain * pPtpDomain)
{
    tPtpPort           *pPtpPort1 = NULL;
    tPtpPort           *pPtpPort2 = NULL;
    tPtpForeignMasterDs *pPtpEbestMsg = NULL;
    INT4                i4RetVal = 0;

    PTP_FN_ENTRY ();

    /* Walk through all the ports associated with this context and this
     * domain specified. Then Compare each messages Erbest to form Ebest
     * for this domain.*/
    pPtpPort1 = PtpIfGetNextPortEntry (pPtpDomain->u4ContextId,
                                       pPtpDomain->u1DomainId, 0);
    if (pPtpPort1 == NULL)
    {
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    if ((pPtpPort1->u4ContextId != pPtpDomain->u4ContextId) ||
        (pPtpPort1->u1DomainId != pPtpDomain->u1DomainId))
    {
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpBmcCalcEBest:Does not have valid Ports...\r\n"));
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpBmcCalcEBest:Exiting BMC....\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    pPtpEbestMsg = pPtpPort1->pPtpErBestMsg;

    pPtpPort2 = PtpIfGetNextPortEntry (pPtpDomain->u4ContextId,
                                       pPtpDomain->u1DomainId,
                                       pPtpPort1->PortDs.u4PtpPortNumber);

    if ((pPtpPort2 != NULL) &&
        ((pPtpPort1->u4ContextId != pPtpPort2->u4ContextId) ||
         (pPtpPort1->u1DomainId != pPtpPort2->u1DomainId)))
    {
        /* Obtained a port entry that is not in the given context and domain
         * Do not include in Ebest calculation
         * */
        pPtpPort2 = NULL;
    }

    if (pPtpPort2 == NULL)
    {
        /* Only one port present in the domain. Select the port's message
         * as Ebest */
        if (pPtpEbestMsg != NULL)
        {
            PtpBmcTriggStateDecisionForDmn (pPtpDomain, pPtpEbestMsg);
            PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "PtpBmcCalcEBest:Selecting The Only Available Port As Ebest!!!\r\n"));
        }
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    do
    {
        if ((pPtpEbestMsg == NULL) && (pPtpPort2->pPtpErBestMsg != NULL))
        {
            /* Update the value of pPtpEbestMsg. Since pPtpEbestMsg & 
             * pPtpPort2->pPtpErBestMsg are equal, Nothing needs to be done 
             * here.
             * */
            pPtpEbestMsg = pPtpPort2->pPtpErBestMsg;
            pPtpPort1 = pPtpPort2;
            continue;
        }
        else if (((pPtpEbestMsg == NULL) &&
                  (pPtpPort2->pPtpErBestMsg == NULL)) ||
                 ((pPtpEbestMsg != NULL) && (pPtpPort2->pPtpErBestMsg == NULL)))
        {
            /* Both Ebest Messsage and Erbest messsage for Port2 is NULL.
             * Search for the remaining ports
             * */
            pPtpPort1 = pPtpPort2;
            continue;
        }

        i4RetVal = PtpBmcDataSetCmpAlgo (pPtpEbestMsg,
                                         pPtpPort2->pPtpErBestMsg);

        if ((i4RetVal == PTP_LESSER_BY_TOPOLOGY) || (i4RetVal == PTP_LESSER))
        {
            /* Current Ebest is not better than the one received over the
             * interface
             * */
            pPtpEbestMsg = pPtpPort2->pPtpErBestMsg;
        }

        pPtpPort1 = pPtpPort2;
    }
    while (((pPtpPort2 = (tPtpPort *)
             PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                               pPtpPort1,
                               (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET)) !=
            NULL) && (pPtpPort2->u1DomainId == pPtpDomain->u1DomainId)
           && (pPtpPort2->u4ContextId == pPtpDomain->u4ContextId));

    if (pPtpEbestMsg == NULL)
    {
        /* No EBEST message calculated.
         * added for Klockworks warning
         * */
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "EBest Message not computed.....\r\n"));

        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Aborting State Decision Calculation....\r\n"));

        return OSIX_FAILURE;
    }

    PtpBmcTriggStateDecisionForDmn (pPtpDomain, pPtpEbestMsg);

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpBmcDataSetCmpAlgo                          */
/*                                                                           */
/* Description               : This routine performs the data set comparision*/
/*                             algorithm as specified in figure 27 & 28 of   */
/*                             IEEE 1588 2008 standard.                      */
/*                                                                           */
/* Input                     : pPtpForeignMaster1 - Data Set 1               */
/*                             pPtpForeignMaster2 - Data Set 2               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpBmcDataSetCmpAlgo (tPtpForeignMasterDs * pPtpForeignMaster1,
                      tPtpForeignMasterDs * pPtpForeignMaster2)
{
    INT4                i4BmcOutput = 0;
    INT4                i4RetVal = 0;
    UINT4               u4DiffDataSet = 0;

    PTP_FN_ENTRY ();

    if (pPtpForeignMaster1 == NULL && pPtpForeignMaster2 != NULL)
    {
        PTP_FN_EXIT ();
        return PTP_LESSER;
    }
    else if (pPtpForeignMaster1 != NULL && pPtpForeignMaster2 == NULL)
    {
        PTP_FN_EXIT ();
        return PTP_GREATER;
    }
    else if (pPtpForeignMaster1 == NULL && pPtpForeignMaster2 == NULL)
    {
        /* Exceptional case. This condition will not be met */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                  "Foreign master2 cant be NULL\r\n"));
        PTP_FN_EXIT ();
        return PTP_DS_CMP_ALGO_ERROR1;
    }

    i4BmcOutput =
        PtpBmcCompareDataSets (pPtpForeignMaster1,
                               pPtpForeignMaster2, &u4DiffDataSet);

    switch (i4BmcOutput)
    {
        case PTP_LESSER:

            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data Set is better than curren ErBest "
                      "message on port\r\n"));
            PtpTrcDisplayBetterParam (pPtpForeignMaster1, u4DiffDataSet);
            break;

        case PTP_GREATER:

            /* If the utility returns pPtpPort->pPtpErBestMsg is the greater 
             * one, then there is no need to update that variable, because this
             * is already being stored as the best one.
             * */
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId, CONTROL_PLANE_TRC,
                      "ErBest message is the better one\r\n"));
            PtpTrcDisplayBetterParam (pPtpForeignMaster1, u4DiffDataSet);
            break;

        case PTP_EQUAL:

            /* Grand Master clock identity of the both the cases are equal */
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      CONTROL_PLANE_TRC, "Data Sets are having equal Grand "
                      "Master Identities.\r\n"));
            i4RetVal = PtpBmcCmpDataSetForEqualGm (pPtpForeignMaster1,
                                                   pPtpForeignMaster2,
                                                   &u4DiffDataSet);

            break;

        default:

            /* Cannot come here & is exceptional case handling */
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      ALL_FAILURE_TRC, "PtpBmcCompareDataSets returned"
                      "invalid value.\r\n"));
            break;
    }

    if (i4BmcOutput != PTP_EQUAL)
    {
        /* No need to execute the other switch statements. BMC calculation
         * has already ended */
        PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                  pPtpForeignMaster1->u1DomainId, CONTROL_PLANE_TRC,
                  "PtpBmcDataSetCmpAlgo:BMC Ended, Master Is Already Selected!!!\r\n"));
        PTP_FN_EXIT ();
        return i4BmcOutput;
    }

    switch (i4RetVal)
    {
        case PTP_GREATER_BY_TOPOLOGY:
        case PTP_GREATER:

            /* Intentional Fall Through */
            /* It indicates that the existing ErBest for the port is better
             * than the received Foreign Master */
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      CONTROL_PLANE_TRC, "Existing Erbest is better : %u\r\n",
                      i4RetVal));
            break;

        case PTP_LESSER_BY_TOPOLOGY:
        case PTP_LESSER:

            pPtpForeignMaster2->pPtpPort->pPtpErBestMsg = pPtpForeignMaster2;
            /* Intentional Fall Through */
            /* It indicates that received Foreign Master is better than the
             * ErBest for the port 
             * */
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Foreign Master Ds is better : %d\r\n", i4RetVal));
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      CONTROL_PLANE_TRC, "Updating the Erbest\r\n"));
            break;

        case PTP_DS_CMP_ALGO_ERROR1:

            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Two DataSets are equal for port : %u\r\n"));
            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId, CONTROL_PLANE_TRC,
                      "Two Equal DS received from different nodes\r\n"));
            break;

        case PTP_DS_CMP_ALGO_ERROR2:

            PTP_TRC ((pPtpForeignMaster1->u4ContextId,
                      pPtpForeignMaster1->u1DomainId, CONTROL_PLANE_TRC,
                      "Two Equal DS received from same node\r\n"));
            break;

        default:
            /* Calculation itself is not involved as the data set comparison
             * would have broken by function PtpBmcCompareDataSets (did not
             * return that the data sets were equal)
             * */
            break;
    }

    PTP_FN_EXIT ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpBmcCompareDataSets                         */
/*                                                                           */
/* Description               : This routine performs the data set comparision*/
/*                             data set algorithm as per Figure 27 of IEEE   */
/*                             1588 2008 standard.                           */
/*                                                                           */
/* Input                     : pPtpForeignMaster1 - Data Set 1 to be compared*/
/*                             pPtpForeignMaster2 - Data Set 2 to be compared*/
/*                                                                           */
/* Output                    : pu4DiffDataSet   - Contains the value that    */
/*                                                that caused this algo      */
/*                                                to return which one is     */
/*                                                greater.                   */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : PTP_GREATER - If Data Set 1 is > Data Set 2   */
/*                             PTP_LESSER  - If Data Set 1 is < Data Set 2   */
/*                             PTP_EQUAL   - If Data Set 1  =   Data Set 2   */
/*****************************************************************************/
PRIVATE INT4
PtpBmcCompareDataSets (tPtpForeignMasterDs * pPtpForeignMaster1,
                       tPtpForeignMasterDs * pPtpForeignMaster2,
                       UINT4 *pu4DiffDataSet)
{
    tPtpAnnounceMsg    *pPtpDataSet1 = NULL;
    tPtpAnnounceMsg    *pPtpDataSet2 = NULL;
    INT4                i4CmpVal = 0;
    UINT1               u1Ds1GMPriority1 = 0;
    UINT1               u1Ds2GMPriority1 = 0;
    /* Refer Flow chart Figure 27 of IEEE 1588-2008 */

    PTP_FN_ENTRY ();

    pPtpDataSet1 = &(pPtpForeignMaster1->AnnounceMsg);
    pPtpDataSet2 = &(pPtpForeignMaster2->AnnounceMsg);

    i4CmpVal = MEMCMP (&(pPtpDataSet1->GMClkId), &(pPtpDataSet2->GMClkId),
                       PTP_MAX_CLOCK_ID_LEN);

    if (i4CmpVal != 0)
    {
        /* If the acceptable master is enabled for this Port  
         * AltPri1 will be updated in AnnounceMsg handler 
         *   If the Configured AltPri1 for this Announce message is zero 
         *     use the received message Priority1 as it is 
         *   else 
         *     use the Configured AltPri1 as a Priority1 for 
         *    this announce message
         */
        u1Ds1GMPriority1 = ((pPtpForeignMaster1->u1AltPri1 != 0) ?
                            pPtpForeignMaster1->u1AltPri1 :
                            pPtpDataSet1->u1GMPriority1);

        u1Ds2GMPriority1 = ((pPtpForeignMaster2->u1AltPri1 != 0) ?
                            pPtpForeignMaster2->u1AltPri1 :
                            pPtpDataSet2->u1GMPriority1);

        *pu4DiffDataSet = PTP_GM_PRIO_1;

        if (u1Ds1GMPriority1 < u1Ds2GMPriority1)
        {
            PTP_FN_EXIT ();
            return PTP_GREATER;
        }
        else if (u1Ds1GMPriority1 > u1Ds2GMPriority1)
        {
            PTP_FN_EXIT ();
            return PTP_LESSER;
        }
        /* Priority1 values are equal */

        *pu4DiffDataSet = PTP_GM_CLASS;

        if (pPtpDataSet1->GMClkQuality.u1Class <
            pPtpDataSet2->GMClkQuality.u1Class)
        {
            PTP_FN_EXIT ();
            return PTP_GREATER;
        }
        else if (pPtpDataSet1->GMClkQuality.u1Class >
                 pPtpDataSet2->GMClkQuality.u1Class)
        {
            PTP_FN_EXIT ();
            return PTP_LESSER;
        }
        /* Class Values are equal */

        *pu4DiffDataSet = PTP_GM_ACCURACY;

        if (pPtpDataSet1->GMClkQuality.u1Accuracy <
            pPtpDataSet2->GMClkQuality.u1Accuracy)
        {
            PTP_FN_EXIT ();
            return PTP_GREATER;
        }
        else if (pPtpDataSet1->GMClkQuality.u1Accuracy >
                 pPtpDataSet2->GMClkQuality.u1Accuracy)
        {
            PTP_FN_EXIT ();
            return PTP_LESSER;
        }
        /* Accuracy values are equal */

        *pu4DiffDataSet = PTP_GM_VARIANCE;
        if (pPtpDataSet1->GMClkQuality.u2OffsetScaledLogVariance <
            pPtpDataSet2->GMClkQuality.u2OffsetScaledLogVariance)
        {
            PTP_FN_EXIT ();
            return PTP_GREATER;
        }
        else if (pPtpDataSet1->GMClkQuality.u2OffsetScaledLogVariance >
                 pPtpDataSet2->GMClkQuality.u2OffsetScaledLogVariance)
        {
            PTP_FN_EXIT ();
            return PTP_LESSER;
        }
        /* Offset Scaled Log Variance values are also equal */

        *pu4DiffDataSet = PTP_GM_PRIO_2;
        if (pPtpDataSet1->u1GMPriority2 < pPtpDataSet2->u1GMPriority2)
        {
            PTP_FN_EXIT ();
            return PTP_GREATER;
        }
        else if (pPtpDataSet1->u1GMPriority2 > pPtpDataSet2->u1GMPriority2)
        {
            PTP_FN_EXIT ();
            return PTP_LESSER;
        }
        /* Priority2 Values are equal */

        *pu4DiffDataSet = PTP_GM_CLK_ID;
        /* Now we need to compare the Grand master identity. It has been 
         * already compared at the start of this data set comparision 
         * algorithm (i4CmpVal). We are exploiting the same variable here.
         * */
        if (i4CmpVal < 0)
        {
            PTP_FN_EXIT ();
            return PTP_GREATER;
        }
        else
        {
            /* No need to do "else-if" here, as the code would have entered
             * here only when the value of i4CmpVal != 0. And the condition
             * (i4CmpVal < 0) has already been checked. This means that the 
             * code can come here only when i4CmpVal > 0
             * */
            PTP_FN_EXIT ();
            return PTP_LESSER;
        }
    }

    PTP_FN_EXIT ();
    return PTP_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpBmcCmpDataSetForEqualGm                    */
/*                                                                           */
/* Description               : This routine performs the data set comparision*/
/*                             data set algorithm as per Figure 28 of IEEE   */
/*                             1588 2008 standard. This needs to invoked only*/
/*                             data sets that are having the same grand      */
/*                             master identifier.                            */
/*                                                                           */
/* Input                     : pPtpForeignMasterDsA - Foreign master whose   */
/*                                                    data set needs to be   */
/*                                                    compared.              */
/*                             pPtpForeignMasterDsB - Foreign master whose   */
/*                                                    data set needs to be   */
/*                                                    compared.              */
/*                                                                           */
/* Output                    : pu4DiffDataSet   - Contains the value that    */
/*                                                that caused this algo      */
/*                                                to return which one is     */
/*                                                greater.                   */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : PTP_GREATER - If Data Set 1 is > Data Set 2   */
/*                             PTP_LESSER  - If Data Set 1 is < Data Set 2   */
/*                             PTP_EQUAL   - If Data Set 1  =   Data Set 2   */
/*****************************************************************************/
PRIVATE INT4
PtpBmcCmpDataSetForEqualGm (tPtpForeignMasterDs * pPtpForeignMasterDsA,
                            tPtpForeignMasterDs * pPtpForeignMasterDsB,
                            UINT4 *pu4DiffDataSet)
{
    tPtpAnnounceMsg    *pPtpDataSet1 = &(pPtpForeignMasterDsA->AnnounceMsg);
    tPtpAnnounceMsg    *pPtpDataSet2 = &(pPtpForeignMasterDsB->AnnounceMsg);
    INT4                i4RetVal = 0;
    INT4                i4DataSetOrder = 0;

    PTP_FN_ENTRY ();

    *pu4DiffDataSet = PTP_STEPS_REMOVED;
    if (pPtpDataSet1->u4StepsRemoved + 1 < pPtpDataSet2->u4StepsRemoved)
    {
        PTP_FN_EXIT ();
        return PTP_GREATER;
    }
    else if (pPtpDataSet1->u4StepsRemoved > pPtpDataSet2->u4StepsRemoved + 1)
    {
        PTP_FN_EXIT ();
        return PTP_LESSER;
    }
    else
    {
        /* A within 1 of B Figure 28 Data Set Comparision Algorithm of 
         * IEEE std 1588-2008.
         * */
        if (pPtpDataSet1->u4StepsRemoved > pPtpDataSet2->u4StepsRemoved)
        {
            /* Comparing identities of receiver of A and Sender of A */
            /* Since it has come to this point, the value returned can only
             * be either B is Better or B is better by topology
             * */
            *pu4DiffDataSet = PTP_B_BETTER;
            i4DataSetOrder = PtpUtilCmpIdentities (pPtpForeignMasterDsA->ClkId,
                                                   pPtpForeignMasterDsA->
                                                   pPtpPort->PortDs.ClkId);
        }
        else if (pPtpDataSet1->u4StepsRemoved < pPtpDataSet2->u4StepsRemoved)
        {
            /* Comparing identities of receiver of B and Sender of B . Since
             * it has come to this point, the value returned can either be
             * A is better or A better by Topology. Refer Figure 28 of
             * IEEE Std 1588 2008
             * */
            *pu4DiffDataSet = PTP_A_BETTER;
            i4DataSetOrder = PtpUtilCmpIdentities (pPtpForeignMasterDsB->ClkId,
                                                   pPtpForeignMasterDsB->
                                                   pPtpPort->PortDs.ClkId);
        }
        else if (pPtpDataSet1->u4StepsRemoved == pPtpDataSet2->u4StepsRemoved)
        {
            *pu4DiffDataSet = PTP_RCV_SEND_IDENTITIES;

            /* Compare identities of Senders A & B */
            i4DataSetOrder = PtpUtilCmpIdentities (pPtpForeignMasterDsA->ClkId,
                                                   pPtpForeignMasterDsB->ClkId);

            if (i4DataSetOrder != PTP_DS_EQUAL_SENDER_RECEIVER)
            {
                /* Some decision has been taken at the clock identifier
                 * level itself
                 * */
                i4RetVal = PtpBmcDetermineDataSetOrder (i4DataSetOrder,
                                                        pu4DiffDataSet);
                PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC,
                          "PtpBmcCmpDataSetForEqualGm:Clocks Dont Have Equal Sender/Receiver!!!\r\n"));
                PTP_FN_EXIT ();
                return i4RetVal;
            }

            /* This means Clock identities are also equal
             * */
            if (pPtpForeignMasterDsA->u4SrcPortIndex ==
                pPtpForeignMasterDsB->u4SrcPortIndex)
            {
                i4DataSetOrder = PTP_DS_EQUAL_SENDER_RECEIVER;
            }
            else if (pPtpForeignMasterDsA->u4SrcPortIndex <
                     pPtpForeignMasterDsB->u4SrcPortIndex)
            {
                i4DataSetOrder = PTP_DS_BETTER_SENDER;
            }
            else
            {
                i4DataSetOrder = PTP_DS_BETTER_RECEIVER;
            }
        }

        i4RetVal = PtpBmcDetermineDataSetOrder (i4DataSetOrder, pu4DiffDataSet);
    }                            /* A within 1 of B */

    PTP_FN_EXIT ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpBmcDetermineDataSetOrder                   */
/*                                                                           */
/* Description               : This routine determines the value that needs  */
/*                             to be returned based on provisioned value     */
/*                             i4Value.                                      */
/*                                                                           */
/* Input                     : i4DsOrder - Data Set order that determines the*/
/*                                         value to be returned.             */
/*                             pu4DiffDataSet - Diff Data set.               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : PTP_GREATER - If Data Set 1 is > Data Set 2   */
/*                             PTP_LESSER  - If Data Set 1 is < Data Set 2   */
/*                             PTP_EQUAL   - If Data Set 1  =   Data Set 2   */
/*****************************************************************************/
PRIVATE INT4
PtpBmcDetermineDataSetOrder (INT4 i4DsOrder, UINT4 *pu4DiffDataSet)
{
    INT4                i4RetVal = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* This function should be invoked as part of Data Set Algorithm part 2
     * mentioned in Figure 28. This function should be invoked either after
     * any one of the following
     * 1) Comparing identities of senders of A & B 
     *    [pu4DiffDataSet = PTP_B_BETTER]
     * 2) Comparing identities of receiver of B & sender of B
     *    [pu4DiffDataSet = PTP_A_BETTER]
     * 3) Comparing port numbers of receivers of A & B
     *    [pu4DiffDataSet = PTP_RCV_SEND_IDENTITIES]
     * */
    switch (i4DsOrder)
    {
        case PTP_DS_BETTER_RECEIVER:

            if (*pu4DiffDataSet == PTP_B_BETTER)
            {
                /* PTP_LESSER_BY_TOPOLOGY is equivalent to B Better by
                 * topology
                 * */
                i4RetVal = PTP_LESSER;
            }
            else if (*pu4DiffDataSet == PTP_A_BETTER)
            {
                /* PTP_GREATER_BY_TOPOLOGY is equivalent to A Better by
                 * topology
                 * */
                i4RetVal = PTP_GREATER;
            }
            else if (*pu4DiffDataSet == PTP_RCV_SEND_IDENTITIES)
            {
                /* Condition where Steps removed field of A & B are equal */
                i4RetVal = PTP_LESSER_BY_TOPOLOGY;
            }
            break;

        case PTP_DS_BETTER_SENDER:

            if (*pu4DiffDataSet == PTP_B_BETTER)
            {
                /* PTP_LESSER_BY_TOPOLOGY is equivalent to B Better by
                 * topology
                 * */
                i4RetVal = PTP_LESSER_BY_TOPOLOGY;
            }
            else if (*pu4DiffDataSet == PTP_A_BETTER)
            {
                /* PTP_GREATER_BY_TOPOLOGY is equivalent to A Better by
                 * topology
                 * */
                i4RetVal = PTP_GREATER_BY_TOPOLOGY;
            }
            else if (*pu4DiffDataSet == PTP_RCV_SEND_IDENTITIES)
            {
                /* Condition where Steps removed field of A & B are equal */
                i4RetVal = PTP_GREATER_BY_TOPOLOGY;
            }
            break;

        case PTP_DS_EQUAL_SENDER_RECEIVER:

            if (*pu4DiffDataSet == PTP_RCV_SEND_IDENTITIES)
            {
                i4RetVal = PTP_DS_CMP_ALGO_ERROR2;
            }
            else
            {
                i4RetVal = PTP_DS_CMP_ALGO_ERROR1;
            }
            break;

        default:

            /* Exceptional case. This condition will not be met */
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                      "Invalid Data Set order passed to utility\r\n"));
            break;
    }                            /* switch statement */

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpBmcTriggStateDecisionForDmn                */
/*                                                                           */
/* Description               : This routine triggers the state decision for  */
/*                             all the ports associated with the given domain*/
/*                                                                           */
/* Input                     : pPtpDomain- Pointer to the domain.            */
/*                             pPtpEbestMsg - Pointer to EBEST Message.      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*****************************************************************************/
PRIVATE INT4
PtpBmcTriggStateDecisionForDmn (tPtpDomain * pPtpDomain,
                                tPtpForeignMasterDs * pPtpEbestMsg)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* Run State decision event for all the ports in this domain */
    for (u4Port = 1; u4Port <= PTP_MAX_PORTS; u4Port++)
    {
        OSIX_BITLIST_IS_BIT_SET (pPtpDomain->DomainPortList, u4Port,
                                 PTP_PORT_LIST_SIZE, bResult);

        if (bResult != OSIX_TRUE)
        {
            continue;
        }
        /* Port is member of the domain. */
        pPtpPort = PtpIfGetPortEntry (pPtpDomain->u4ContextId,
                                      pPtpDomain->u1DomainId, u4Port);
        if (pPtpPort == NULL)
        {
            continue;
        }

        PtpStateExecDecisionAlgorithm (pPtpPort, pPtpEbestMsg);
    }

    PTP_FN_EXIT ();

    return OSIX_SUCCESS;
}
#endif /* _PTPBMC_C_ */
