/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpnpwr.c,v 1.9 2014/05/29 13:19:55 siva Exp $
 *
 * Description: This file contains the wrappers for the NPAPIS that will be
 *              invoked for programming the hardware.
 *****************************************************************************/
#ifndef _PTPNPWR_C_
#define _PTPNPWR_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpNpWrConfigPtpStatus                        */
/*                                                                           */
/* Description               : This function programs the PTP status into the*/
/*                             NP if NPAPI is defined in the system. Else,   */
/*                             this will return SUCCESS.                     */
/*                                                                           */
/* Input(s)                  : pPtpPort - Pointer to Interface Index.        */
/*                             u4Status - Status of PTP over this domain.    */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrConfigPtpStatus (tPtpPort * pPtpPort, UINT4 u4Status)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    if (pPtpPort == NULL)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrConfigPtpStatus:Port Entry Not Present\r\n"));
        return OSIX_FAILURE;
    }
    if (gPtpGlobalInfo.TimeStamp != PTP_HARDWARE_TIMESTAMPING)
    {
        /* Hardware does not support PTP. Just return from here
         * */
        return i4RetVal;
    }

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.pSlotInfo = NULL;
    PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.u4ContextId = pPtpPort->u4ContextId;
    PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.u4IfIndex = pPtpPort->u4IfIndex;
    PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.u1DomainId = pPtpPort->u1DomainId;
    PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.DeviceType = pPtpPort->PtpDeviceType;

    if (u4Status == PTP_ENABLED)
    {
        PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.u4Status = FNP_TRUE;
    }
    else
    {
        PtpHwInfo.unHwParam.PtpHwPtpStatusInfo.u4Status = FNP_FALSE;
    }

    /* This function is obsoleted */
    FsNpHwSetPtpStatus (&(PtpHwInfo.unHwParam.PtpHwPtpStatusInfo));

    PtpHwInfo.u4InfoType = PTP_HW_SET_STATUS;

    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pPtpPort);
    UNUSED_PARAM (u4Status);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrInitPortParams                         */
/*                                                                           */
/* Description               : This function initializes the port parameters */
/*                             that are required to operate on the hardware. */
/*                                                                           */
/* Input(s)                  : pPtpPort - Pointer to Interface Index.        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrInitPortParams (tPtpPort * pPtpPort)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwTransmitTsOpt PtpHwTransmitTsOpt;
    tPtpHwRecvTsOpt     PtpHwRecvTsOpt;
    tNetIpv4IfInfo      NetIpIfInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tPtpHwInfo          PtpHwInfo;

    if (pPtpPort == NULL)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrInitPortParams:Port Entry Not Present\r\n"));
        return OSIX_FAILURE;
    }

    if (gPtpGlobalInfo.TimeStamp != PTP_HARDWARE_TIMESTAMPING)
    {
        /* Hardware does not support PTP. Just return from here
         * */
        return i4RetVal;
    }

    MEMSET (&PtpHwTransmitTsOpt, 0, sizeof (tPtpHwTransmitTsOpt));
    MEMSET (&PtpHwRecvTsOpt, 0, sizeof (tPtpHwRecvTsOpt));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    /* Configuring the Transmit Time Stamp Options */
    PtpHwTransmitTsOpt.pSlotInfo = NULL;
    PtpHwTransmitTsOpt.u4IfIndex = pPtpPort->u4IfIndex;
    PtpHwTransmitTsOpt.u4ContextId = pPtpPort->u4ContextId;
    PtpHwTransmitTsOpt.u1DomainId = pPtpPort->u1DomainId;
    PtpHwTransmitTsOpt.u1DelayMech = pPtpPort->PortDs.u1DelayMechanism;
    PtpHwTransmitTsOpt.DeviceType = pPtpPort->PtpDeviceType;

    /* Time stamp generation */
    PtpHwTransmitTsOpt.bIanaIpTsReq = FNP_TRUE;
    if (pPtpPort->pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        /* In case of transparent clocks, the packet will be modified
         * for the correction field updations. Hence it will be desirable to
         * recalculate the checksum for those packets. Hence setting as
         * FNP_TRUE
         * */
        PtpHwTransmitTsOpt.bRecalcChkSum = FNP_TRUE;
        PtpHwTransmitTsOpt.bRecalcCrc = FNP_TRUE;
    }
    else
    {
        /* Make Checksum recalculation to FALSE */
        PtpHwTransmitTsOpt.bRecalcChkSum = FNP_FALSE;
        PtpHwTransmitTsOpt.bRecalcCrc = FNP_FALSE;
    }
    /* Make Transmit Delay Request message as FALSE */
    PtpHwTransmitTsOpt.bGetDelayReq = FNP_FALSE;
    /* Making Time Stamp generation enable */
    PtpHwTransmitTsOpt.bTsEnable = FNP_TRUE;

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag ==
        PTP_ENABLED)
    {
        PtpHwTransmitTsOpt.bIsTwoStep = FNP_TRUE;
        PtpHwTransmitTsOpt.bSyncOneStep = FNP_FALSE;
    }
    else
    {
        /* Replace FNP_FALSE & FNP_TRUE */
        PtpHwTransmitTsOpt.bIsTwoStep = FNP_FALSE;
        PtpHwTransmitTsOpt.bSyncOneStep = FNP_TRUE;
    }

    /* Configuring Receive Options */
    PtpHwRecvTsOpt.u4IfIndex = pPtpPort->u4IfIndex;
    PtpHwRecvTsOpt.u4ContextId = pPtpPort->u4ContextId;
    PtpHwRecvTsOpt.u1DomainId = pPtpPort->u1DomainId;
    PtpHwRecvTsOpt.DeviceType = pPtpPort->PtpDeviceType;

    PtpHwRecvTsOpt.u1Version = (UINT1) pPtpPort->PortDs.u4VersionNumber;
    PtpHwRecvTsOpt.bDomainmatchEnable = FNP_TRUE;

    if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV4)
    {
        PtpHwTransmitTsOpt.bIpV4PtpDetEnable = FNP_TRUE;
        PtpHwRecvTsOpt.bIpV4PtpDetEnable = FNP_TRUE;
        PtpHwRecvTsOpt.bUserIpEnable = FNP_TRUE;
        PtpPortNetIpv4GetIfInfo (pPtpPort->u4IfIndex, &NetIpIfInfo);
        PtpHwRecvTsOpt.u4SrcIPAddr = NetIpIfInfo.u4Addr;
    }
    else if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV6)
    {
        PtpHwTransmitTsOpt.bIpV6PtpDetEnable = FNP_TRUE;
        PtpHwRecvTsOpt.bIpV6PtpDetEnable = FNP_TRUE;
        PtpHwRecvTsOpt.bUserIpEnable = FNP_FALSE;
        PtpPortNetIpv6GetIfInfo (pPtpPort->u4IfIndex, &NetIpv6IfInfo);
        /* Copy the address into the Source Address field */
        Ip6AddrCopy (&(PtpHwRecvTsOpt.Ip6Addr), &(NetIpv6IfInfo.Ip6Addr));
    }
    else if (pPtpPort->PtpDeviceType == PTP_IFACE_IEEE_802_3)
    {
        PtpHwTransmitTsOpt.bL2PtpDetEnable = FNP_TRUE;
        PtpHwRecvTsOpt.bL2PtpDetEnable = FNP_TRUE;
    }
    else if (pPtpPort->PtpDeviceType == PTP_IFACE_VLAN)
    {
        PtpHwTransmitTsOpt.bL2PtpDetEnable = FNP_TRUE;
        PtpHwRecvTsOpt.bL2PtpDetEnable = FNP_TRUE;
    }

    PtpHwRecvTsOpt.bRecvTSEnable = FNP_TRUE;
    PtpHwRecvTsOpt.bPtpTsIanaEnable = FNP_TRUE;
    PtpHwRecvTsOpt.bIsSlaveOnly =
        pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly;
    PtpHwRecvTsOpt.bSrcIdHash = FNP_FALSE;
    PtpHwRecvTsOpt.bUdpChkSumErr = FNP_TRUE;
    PtpHwRecvTsOpt.bCrcErrAcc = FNP_TRUE;
    PtpHwRecvTsOpt.bPtpRxTSInsertEnable = FNP_FALSE;
    PtpHwRecvTsOpt.bPtpTSAppendEnable = FNP_TRUE;
    PtpHwRecvTsOpt.u4SecondsOff = 0;
    PtpHwRecvTsOpt.u4NanoSecondsOff = 0;

    /* This function is obsoleted */
    FsNpHwSetPtpTransmitTSOption (&PtpHwTransmitTsOpt);

    MEMCPY (&PtpHwInfo.unHwParam.PtpHwTransmitTsOpt, &PtpHwTransmitTsOpt,
            sizeof (tPtpHwTransmitTsOpt));
    PtpHwInfo.u4InfoType = PTP_HW_SET_TRANSMIT_TS_OPTION;
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Unable to configure Transmit Config Parameters for "
                  "port.\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    /* This function is obsolete */
    FsNpHwSetPtpReceiveTSOption (&PtpHwRecvTsOpt);

    MEMCPY (&PtpHwInfo.unHwParam.PtpHwRecvTsOpt, &PtpHwRecvTsOpt,
            sizeof (tPtpHwRecvTsOpt));
    PtpHwInfo.u4InfoType = PTP_HW_SET_RECEIVE_TS_OPTION;
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Unable to configure Receive Config Parameters for "
                  "port.\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    if (pPtpPort->PtpHwSrcIdHash.bSrcIdHash == OSIX_TRUE)
    {
        /* Copy the source port identity from the given port info
         * */
        PtpHwConfigSrcIdHash (&(pPtpPort->PtpHwSrcIdHash));
    }
#else
    UNUSED_PARAM (pPtpPort);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrTimeStampCallBack                      */
/*                                                                           */
/* Description               : This function invokes the NPAPI to obtain the */
/*                             corresponding time stamp for the received     */
/*                             packet.                                       */
/*                                                                           */
/* Input(s)                  : pu1Pdu - Pointer to PDU.                      */
/*                             pPtpSysTimeInfo - Pointer to system info      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrTimeStampCallBack (tPtpHwPtpCbParams * pPtpHwPtpCbParams)
{
#ifdef NPAPI_WANTED
    if (FsNpPtpHwTimeStampCallBack (pPtpHwPtpCbParams) == FNP_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrTimeStampCallBack: FsNpPtpHwTimeStampCallBack:"
                  "FsNpPtpHwTimeStampCallBack FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pPtpHwPtpCbParams);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpNpWrConfigDomainMode                       */
/*                                                                           */
/* Description               : This function invokes the NPAPI to configure  */
/*                             the value of the clock mode for the given     */
/*                             context and domain.                           */
/*                                                                           */
/* Input(s)                  : pPtpDomain - Pointer to Domain.               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrConfigDomainMode (tPtpDomain * pPtpDomain)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    PtpHwInfo.u4InfoType = PTP_HW_SET_DOMAIN_MODE;
    PtpHwInfo.unHwParam.PtpHwDmnModeParams.pSlotInfo = NULL;
    PtpHwInfo.unHwParam.PtpHwDmnModeParams.u4ContextId =
        pPtpDomain->u4ContextId;
    PtpHwInfo.unHwParam.PtpHwDmnModeParams.u1DomainId = pPtpDomain->u1DomainId;
    PtpHwInfo.unHwParam.PtpHwDmnModeParams.ClkMode = pPtpDomain->ClkMode;

    /* This function is obsoleted and the unified NPAPI, FsNpHwConfigPtpInfo is
     * used for this purpose */
    FsNpPtpSetDomainMode (&PtpHwInfo.unHwParam.PtpHwDmnModeParams);

    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_SET_DOMAIN_MODE;

    /* Calling the unified API with information type as DomainMode */
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Unable to configure Domain mode...\r\n"));
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "NP Call returned failure!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pPtpDomain);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrConfigRateAdj                          */
/*                                                                           */
/* Description               : This function invokes the NPAPI to configure  */
/*                             the rate adjustment for the clock. This       */
/*                             should be invoked only for primary context &  */
/*                             primary Domain.                               */
/*                                                                           */
/* Input(s)                  : pPtpDomain - Pointer to Port.                 */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrConfigRateAdj (tPtpPort * pPtpPort)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.pSlotInfo = NULL;
    PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u4ContextId = pPtpPort->u4ContextId;
    PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u4IfIndex = pPtpPort->u4IfIndex;
    PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.DeviceType = pPtpPort->PtpDeviceType;

    if (pPtpPort->PortDs.i1SyntRatioSign == PTP_NEGATIVE)
    {
        /* Clock is in LEAD compared to Master */
        PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u1Direction = FNP_FALSE;
    }
    else
    {
        PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u1Direction = FNP_TRUE;
    }

    PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u4NanoSeconds =
        pPtpPort->PortDs.u8SyntRatio.u4Lo * PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
    PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u4RateAdj =
        pPtpPort->PortDs.u4RateAdj;
    FSAP_U8_ASSIGN (&(PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj.u8RatioPerSec),
                    &(pPtpPort->PortDs.u8RatioPerSec));

    /* This function is obsoleted and the unified NPAI is used for the purpose */
    FsNpPtpHwSetClkAdj (&(PtpHwInfo.unHwParam.PtpHwPtpClkRateAdj));

    /* Calling the unified API with information type as RateAdjustmentFactor */
    PtpHwInfo.u4InfoType = PTP_HW_SET_CLOCK_ADJUSTMENT_FACTOR;
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Unable to configure Rate Adjustment...\r\n"));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "NP Call returned failure!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pPtpPort);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrConfigHwClk                            */
/*                                                                           */
/* Description               : This function invokes the NPAPI to configure  */
/*                             the system clock for the given interface.     */
/*                                                                           */
/* Input(s)                  : pPtpDomain - Pointer to Port.                 */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrConfigHwClk (tPtpNpWrCfgHwClk * pPtpNpWrCfgHwClk)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    PtpHwInfo.unHwParam.PtpHwPtpClk.pSlotInfo = NULL;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u4ContextId =
        pPtpNpWrCfgHwClk->pPtpSysTimeInfo->u4ContextId;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u4IfIndex = pPtpNpWrCfgHwClk->u4IfIndex;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u1DomainId =
        pPtpNpWrCfgHwClk->pPtpSysTimeInfo->u1DomainId;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u4Seconds =
        pPtpNpWrCfgHwClk->pPtpSysTimeInfo->FsClkTimeVal.u8Sec.u4Lo;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u4NanoSeconds =
        pPtpNpWrCfgHwClk->pPtpSysTimeInfo->FsClkTimeVal.u4Nsec;
    /* This function is obsoleted and the unified NPAPI, FsNpHwConfigPtpInfo is
     * used for this purpose */

    FSAP_U8_ASSIGN (&(PtpHwInfo.unHwParam.PtpHwPtpClk.u8Offset),
                    &(pPtpNpWrCfgHwClk->u8Offset));
    PtpHwInfo.unHwParam.PtpHwPtpClk.i1OffsetSign =
        pPtpNpWrCfgHwClk->i1OffsetSign;
    FsNpPtpHwSetClk (&(PtpHwInfo.unHwParam.PtpHwPtpClk));

    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_SET_CLOCK;

    /* Calling the unified API with information type as SetHwClock */
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC ((pPtpNpWrCfgHwClk->pPtpSysTimeInfo->u4ContextId,
                  pPtpNpWrCfgHwClk->pPtpSysTimeInfo->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Unable to configure System Clock...\r\n"));
        PTP_TRC ((pPtpNpWrCfgHwClk->pPtpSysTimeInfo->u4ContextId,
                  pPtpNpWrCfgHwClk->pPtpSysTimeInfo->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "NP Call returned failure!!!!!\r\n"));

        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pPtpNpWrCfgHwClk);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpHwConfigSrcIdHash                          */
/*                                                                           */
/* Description               : This function invokes the NPAPI to configure  */
/*                             the Source Identity hash value. This function */
/*                             will invoke the NPAPI only when the source    */
/*                             hash identity is defined in the system. Other */
/*                             wise, it will return without configuring the  */
/*                             NPAPI.                                        */
/*                                                                           */
/* Input(s)                  : pSrcIdentity - Source Identity.               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpHwConfigSrcIdHash (tPtpHwSrcIdHash * pPtpHwSrcIdHash)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    /* This function is obsoleted and the unified NPAPI, FsNpHwConfigPtpInfo is
     * used for this purpose */
    FsNpPtpHwSetSrcPortIdHash (pPtpHwSrcIdHash);

    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_SET_SRC_ID_HASH;

    /* Copy the parameters */
    MEMCPY (&(PtpHwInfo.unHwParam.PtpHwSrcIdHash), pPtpHwSrcIdHash,
            sizeof (tPtpHwSrcIdHash));

    /* Calling the unified API with information type as SourceIdHash */
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Unable to configure Source Port Id...\r\n"));
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "NP Call returned failure!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

#else
    UNUSED_PARAM (pPtpHwSrcIdHash);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrInit                                   */
/*                                                                           */
/* Description               : This function initialises PTP in the hardware */
/*                                                                           */
/* Input(s)                  : None                                          */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrInit (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;
    tPtpHwLog           PtpHwLog;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));
    MEMSET (&PtpHwLog, 0, sizeof (tPtpHwLog));

    /* By default the time stamp option is append */
    gPtpGlobalInfo.u1HardwareTSOpt = PTP_TS_APPEND;

    /* This function is obsoleted and the unified NPAPI, FsNpHwConfigPtpInfo 
     * is used for this purpose */
    PtpHwLog.u2TimeStamp = gPtpGlobalInfo.TimeStamp;
    FsNpPtpHwInit (&PtpHwLog);

    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_INIT;
    PtpHwInfo.u2PtpTimeStamp = gPtpGlobalInfo.TimeStamp;

    /* Calling the unified API with information type as HwInit */
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrInit: NP Call Failed...\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrDeInit                                 */
/*                                                                           */
/* Description               : This function de-initialises PTP in the       */
/*                             hardware                                      */
/*                                                                           */
/* Input(s)                  : None                                          */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpNpWrDeInit (VOID)
{
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    /* This function is obsoleted and the unified NPAPI, FsNpHwConfigPtpInfo is
     * used for this purpose */
    FsNpPtpHwDeInit ();

    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_DEINIT;

    /* Calling the unified API with information type as HwDeInit */
    FsNpHwConfigPtpInfo (&PtpHwInfo);
#endif
}

/*****************************************************************************/
/* Function                  : PtpNpWrGetHwPktLogTime                        */
/*                                                                           */
/* Description               : This function is invoked to get the hardware  */
/*                             logged time when the packet was sent or       */
/*                             received from PTP, if NPAPI is enabled. Else, */
/*                             this will return SUCCESS.                     */
/*                                                                           */
/* Input(s)                  : pPtpHwLog - Pointer to Hardware LogInfo.      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrGetPktLogTime (tPtpHwLog * pPtpHwLog)
{
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    if (pPtpHwLog == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrGetPktLogTime:Log Entry Not Present\r\n"));
        return OSIX_FAILURE;
    }
    if (gPtpGlobalInfo.TimeStamp != PTP_HARDWARE_TIMESTAMPING)
    {
        /* Hardware does not support PTP. Just return from here
         * */
        return OSIX_FAILURE;
    }

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));
    PtpHwInfo.unHwParam.PtpHwLog.u4PortNo = pPtpHwLog->u4PortNo;
    PtpHwInfo.unHwParam.PtpHwLog.u4SeqId = pPtpHwLog->u4SeqId;
    PtpHwInfo.unHwParam.PtpHwLog.u1Mode = pPtpHwLog->u1Mode;
    PtpHwInfo.u4InfoType = PTP_HW_GET_PKT_LOG_TIMESTAMP;

    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrGetPktLogTime: NP Call Failed...\r\n"));
        return OSIX_FAILURE;
    }
    pPtpHwLog->FsTimeVal.u8Sec.u4Lo =
        PtpHwInfo.unHwParam.PtpHwLog.FsTimeVal.u8Sec.u4Lo;
    pPtpHwLog->FsTimeVal.u8Sec.u4Hi =
        PtpHwInfo.unHwParam.PtpHwLog.FsTimeVal.u8Sec.u4Hi;
    pPtpHwLog->FsTimeVal.u4Nsec = PtpHwInfo.unHwParam.PtpHwLog.FsTimeVal.u4Nsec;
#else
    UNUSED_PARAM (pPtpHwLog);
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpNpWrConfigPeerDelay                        */
/*                                                                           */
/* Description               : This function is to configure the peer mean   */
/*                             path delay for the particular index.          */
/*                                                                           */
/* Input(s)                  : pPtpNpWrPeerDelay - pointer to Peer delay Info*/
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrConfigPeerDelay (tPtpNpWrPeerDelay * pPtpNpWrPeerDelay)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    if (gPtpGlobalInfo.TimeStamp != PTP_HARDWARE_TIMESTAMPING)
    {
        /* Hardware does not support PTP. Just return from here
         * */
        return OSIX_FAILURE;
    }

    PtpHwInfo.unHwParam.PtpHwPeerDelay.u1DomainId =
        pPtpNpWrPeerDelay->u1DomainId;
    PtpHwInfo.unHwParam.PtpHwPeerDelay.u4ContextId =
        pPtpNpWrPeerDelay->u4ContextId;
    PtpHwInfo.unHwParam.PtpHwPeerDelay.u4IfIndex = pPtpNpWrPeerDelay->u4IfIndex;
    FSAP_U8_ASSIGN (&(PtpHwInfo.unHwParam.PtpHwPeerDelay.u8PeerMeanDelay),
                    &(pPtpNpWrPeerDelay->u8PeerMeanPathDelay));

    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_CONFIG_PEER_DELAY;

    /* Calling the unified API with information type as ConfigPeerDelay */
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrConfigPeerDelay: NP Call Failed...\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pPtpNpWrPeerDelay);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpNpWrGetHwClk                               */
/*                                                                           */
/* Description               : This function invokes the NPAPI to configure  */
/*                             the system clock for the given interface.     */
/*                                                                           */
/* Input(s)                  : u4IfIndex . Interface index.                  */
/*                                                                           */
/* Output                    : pPtpSysTimeInfo . System Time from hardware.  */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpNpWrGetHwClk (UINT4 u4IfIndex, tClkSysTimeInfo * pPtpSysTimeInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tPtpHwInfo          PtpHwInfo;

    MEMSET (&PtpHwInfo, 0, sizeof (tPtpHwInfo));

    PtpHwInfo.unHwParam.PtpHwPtpClk.pSlotInfo = NULL;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u4ContextId = pPtpSysTimeInfo->u4ContextId;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u4IfIndex = u4IfIndex;
    PtpHwInfo.unHwParam.PtpHwPtpClk.u1DomainId = pPtpSysTimeInfo->u1DomainId;
    /* Set the information type */
    PtpHwInfo.u4InfoType = PTP_HW_GET_CLOCK;

    /* Calling the unified API with information type as GetHwClock */
    if (FsNpHwConfigPtpInfo (&PtpHwInfo) == FNP_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpNpWrGetHwClk: NP Call Failed...\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
    pPtpSysTimeInfo->FsClkTimeVal.u8Sec.u4Lo =
        PtpHwInfo.unHwParam.PtpHwPtpClk.u4Seconds;
    pPtpSysTimeInfo->FsClkTimeVal.u4Nsec =
        PtpHwInfo.unHwParam.PtpHwPtpClk.u4NanoSeconds;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pPtpSysTimeInfo);
#endif

    return i4RetVal;
}
#endif /*_PTPNPWR_C_*/
