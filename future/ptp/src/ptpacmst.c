/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpacmst.c,v 1.3 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP Acceptable master Sub module 
 *              functionality routines.
 *********************************************************************/
#ifndef _PTPACMST_C_
#define _PTPACMST_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function     : PtpAcMstIsAccMasterEnabled                                 */
/*                                                                           */
/* Description  : This function is used to get the status of the Acceptable  */
/*                master option on given PTP port.                           */
/*                                                                           */
/* Input        : pPtpPort    - PTP Port Pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAcMstIsAccMasterEnabled (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    if (pPtpPort->bAccMastEnabled == OSIX_TRUE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpAcMstIsAccMasterEnabled : Enabled for port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();

        return (OSIX_TRUE);
    }

    PTP_FN_EXIT ();

    return (OSIX_FALSE);
}

/*****************************************************************************/
/* Function     : PtpAcMstValidateMaster                                     */
/*                                                                           */
/* Description  : This function is used to Valid the Acceptable master       */
/*                entry and if the given master is present it will give the  */
/*                configured alternate Priority1 value.                      */
/*                                                                           */
/* Input        : u4ContextId    - Context Identifier.                       */
/*              : u1DomainId     - Domain Identifier.                        */
/*              : pPortAddress   - Port Address table                        */
/*              : pu1AltPriority1- Pointer to the configured Alternate       */
/*                                 Priority1value.                           */
/*                                                                           */
/* Output       : Success case the   *pu1AltPriority1 have the configured    */
/*                Alternate Priority1value.                                  */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAcMstValidateMaster (UINT4 u4ContextId, UINT1 u1DomainId,
                        tPtpPortAddress * pPortAddress, UINT1 *pu1AltPriority1)
{
    tPtpAccMasterTable  AccMstTable;
    tPtpAccMasterTable *pAccMstEntry = NULL;

    PTP_FN_ENTRY ();

    MEMSET (&AccMstTable, 0, sizeof (tPtpAccMasterTable));

    AccMstTable.u4ContextId = u4ContextId;
    AccMstTable.u1DomainId = u1DomainId;

    AccMstTable.AccMstAddr.u2NetworkProtocol = pPortAddress->u2NetworkProtocol;
    MEMCPY (AccMstTable.AccMstAddr.ai1Addr,
            pPortAddress->ai1Addr, pPortAddress->u2AddrLength);
    AccMstTable.AccMstAddr.u2AddrLength = pPortAddress->u2AddrLength;

    pAccMstEntry = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(AccMstTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pAccMstEntry == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId, ALL_FAILURE_TRC,
                  "PtpAcMstValidateMaster : Not Valid Acceptable Master.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    *pu1AltPriority1 = pAccMstEntry->u1AltPri1;

    PTP_TRC ((u4ContextId, u1DomainId, CONTROL_PLANE_TRC,
              "PtpAcMstValidateMaster : Valid Acceptable Master.\r\n"));
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

#endif /*  _PTPACMST_C_ */

/***************************************************************************
 *                         END OF FILE ptpacmst.c                          *
 ***************************************************************************/
