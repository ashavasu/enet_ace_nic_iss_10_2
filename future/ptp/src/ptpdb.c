/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *$Id: ptpdb.c,v 1.7 2014/02/21 13:44:07 siva Exp $
 *
 * Description: This file contains PTP Data base Access routines. All
 *              the data base access needs to be done through the 
 *              routines mentioned in this file only.
 *********************************************************************/
#ifndef _PTPDB_C_
#define _PTPDB_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpDbAddNode                                  */
/*                                                                           */
/* Description               : This routine adds the node to the             */
/*                             corresponding data base                       */
/*                                                                           */
/* Input                     : pvPtpTable    -  Root pointer                 */
/*                             pvPtpNode     -  Node pointer to be added     */
/*                             u1PtpDbId     -  Macro to identify the DB     */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpDbAddNode (VOID *pvPtpTable, VOID *pvPtpNode, UINT1 u1PtpDbId)
{
    VOID               *pvNode = NULL;
    VOID               *pvTmpNode = NULL;
    UINT4               u4HashValue = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    if (u1PtpDbId == PTP_SA_DATA_SET)
    {
        UTL_SLL_OFFSET_SCAN ((tTMO_SLL *) pvPtpTable, pvNode, pvTmpNode,
                             tPtpSADs *)
        {
            if ((((tPtpSADs *) pvNode)->u4ContextId >
                 ((tPtpSADs *) pvPtpNode)->u4ContextId) ||
                (((tPtpSADs *) pvNode)->u1DomainId >
                 ((tPtpSADs *) pvPtpNode)->u1DomainId) ||
                (((tPtpSADs *) pvNode)->u4SAId >
                 ((tPtpSADs *) pvPtpNode)->u4SAId))
            {
                break;
            }
        }

        if (pvNode != NULL)
        {
            TMO_SLL_Insert
                ((tTMO_SLL *) pvPtpTable,
                 TMO_SLL_Previous ((tTMO_SLL *) pvPtpTable,
                                   &(((tPtpSADs *) pvNode)->nextSANode)),
                 &(((tPtpSADs *) pvPtpNode)->nextSANode));
            i4RetVal = OSIX_SUCCESS;
        }
        else
        {
            TMO_SLL_Add ((tTMO_SLL *) pvPtpTable,
                         &(((tPtpSADs *) pvPtpNode)->nextSANode));
            i4RetVal = OSIX_SUCCESS;
        }
    }
    else if (u1PtpDbId == PTP_PORT_CONFIG_HASH_DATA_SET)
    {
        u4HashValue = PtpUtilGetHashValue
            (((tPtpPort *) pvPtpNode)->PtpDeviceType,
             ((tPtpPort *) pvPtpNode)->u4IfIndex);

        TMO_HASH_Add_Node ((tTMO_HASH_TABLE *) pvPtpTable,
                           &(((tPtpPort *) pvPtpNode)->nextHashNode),
                           u4HashValue, NULL);
        i4RetVal = OSIX_SUCCESS;
    }
    else
    {
        if (RBTreeAdd ((tRBTree) pvPtpTable, (tRBElem *) pvPtpNode)
            != RB_FAILURE)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpDbAddNode: Node Addition Failed\r\n"));
    }
    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpDbDeleteNode                               */
/*                                                                           */
/* Description               : This routine deletes the node from the        */
/*                             corresponding data base                       */
/*                                                                           */
/* Input                     : pvPtpTable    - Root pointer                  */
/*                             pvPtpNode     - Node pointer containing index */
/*                             u1PtpDbId     - Macro to identify the DB      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID        *
PtpDbDeleteNode (VOID *pvPtpTable, VOID *pvPtpNode, UINT1 u1PtpDbId)
{
    VOID               *pvNode = NULL;
    VOID               *pvTmpNode = NULL;
    UINT4               u4HashValue = 0;

    PTP_FN_ENTRY ();

    if (u1PtpDbId == PTP_SA_DATA_SET)
    {
        UTL_SLL_OFFSET_SCAN ((tTMO_SLL *) pvPtpTable, pvNode, pvTmpNode,
                             tPtpSADs *)
        {
            if ((((tPtpSADs *) pvNode)->u4ContextId ==
                 ((tPtpSADs *) pvPtpNode)->u4ContextId) ||
                (((tPtpSADs *) pvNode)->u1DomainId ==
                 ((tPtpSADs *) pvPtpNode)->u1DomainId) ||
                (((tPtpSADs *) pvNode)->u4SAId ==
                 ((tPtpSADs *) pvPtpNode)->u4SAId))
            {
                TMO_SLL_Delete ((tTMO_SLL *) pvPtpTable,
                                &(((tPtpSADs *) pvNode)->nextSANode));
                break;
            }
        }
    }
    else if (u1PtpDbId == PTP_PORT_CONFIG_HASH_DATA_SET)
    {
        /* While deleting the node, context id, domain id and port number are
         * must
         */
        u4HashValue = PtpUtilGetHashValue
            (((tPtpPort *) pvPtpNode)->PtpDeviceType,
             ((tPtpPort *) pvPtpNode)->u4IfIndex);

        UTL_HASH_Scan_Bucket ((tTMO_HASH_TABLE *) pvPtpTable,
                              u4HashValue, pvNode, pvTmpNode, tPtpPort *)
        {
            if ((((tPtpPort *) pvPtpNode)->PortDs.u4PtpPortNumber ==
                 ((tPtpPort *) pvNode)->PortDs.u4PtpPortNumber) &&
                (((tPtpPort *) pvPtpNode)->u4ContextId ==
                 ((tPtpPort *) pvNode)->u4ContextId) &&
                ((((tPtpPort *) pvPtpNode)->u1DomainId ==
                  ((tPtpPort *) pvNode)->u1DomainId)))
            {
                TMO_HASH_Delete_Node
                    ((tTMO_HASH_TABLE *) pvPtpTable,
                     &(((tPtpPort *) pvNode)->nextHashNode), u4HashValue);
                break;
            }
        }
    }
    else
    {
        pvNode = RBTreeRem ((tRBTree) pvPtpTable, (tRBElem *) pvPtpNode);
    }

    PTP_FN_EXIT ();
    return pvNode;
}

/*****************************************************************************/
/* Function                  : PtpDbGetNode                                  */
/*                                                                           */
/* Description               : This routine gets the node from the           */
/*                             corresponding data base                       */
/*                                                                           */
/* Input                     : pvPtpTable    - Root pointer                  */
/*                             pvPtpNode     - Node pointer containing index */
/*                             u1PtpDbId     - Macro to identify the DB      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID        *
PtpDbGetNode (VOID *pvPtpTable, VOID *pvPtpNode, UINT1 u1PtpDbId)
{
    VOID               *pvNode = NULL;
    VOID               *pvTmpNode = NULL;
    UINT4               u4HashValue = 0;

    PTP_FN_ENTRY ();

    if (u1PtpDbId == PTP_SA_DATA_SET)
    {
        UTL_SLL_OFFSET_SCAN ((tTMO_SLL *) pvPtpTable, pvNode, pvTmpNode,
                             tPtpSADs *)
        {
            if ((((tPtpSADs *) pvNode)->u4ContextId ==
                 ((tPtpSADs *) pvPtpNode)->u4ContextId) ||
                (((tPtpSADs *) pvNode)->u1DomainId ==
                 ((tPtpSADs *) pvPtpNode)->u1DomainId) ||
                (((tPtpSADs *) pvNode)->u4SAId ==
                 ((tPtpSADs *) pvPtpNode)->u4SAId))
            {
                break;
            }
        }
    }
    else if (u1PtpDbId == PTP_PORT_CONFIG_HASH_DATA_SET)
    {
        /* While searching the node, context id and domain id are must since,
         * the node matching the context id and domain id will be deleted
         */
        u4HashValue = PtpUtilGetHashValue
            (((tPtpPort *) pvPtpNode)->PtpDeviceType,
             ((tPtpPort *) pvPtpNode)->u4IfIndex);

        UTL_HASH_Scan_Bucket ((tTMO_HASH_TABLE *) pvPtpTable,
                              u4HashValue, pvNode, pvTmpNode, tPtpPort *)
        {
            if ((((tPtpPort *) pvPtpNode)->PtpDeviceType ==
                 ((tPtpPort *) pvNode)->PtpDeviceType) &&
                (((tPtpPort *) pvPtpNode)->u4IfIndex ==
                 ((tPtpPort *) pvNode)->u4IfIndex) &&
                (((tPtpPort *) pvPtpNode)->u4ContextId ==
                 ((tPtpPort *) pvNode)->u4ContextId) &&
                ((((tPtpPort *) pvPtpNode)->u1DomainId ==
                  ((tPtpPort *) pvNode)->u1DomainId)))
            {
                break;
            }
        }
    }
    else
    {
        pvNode = RBTreeGet ((tRBTree) pvPtpTable, (tRBElem *) pvPtpNode);
    }

    PTP_FN_EXIT ();
    return pvNode;
}

/*****************************************************************************/
/* Function                  : PtpDbGetFirstNode                             */
/*                                                                           */
/* Description               : This routine gets the first node from the     */
/*                             corresponding data base                       */
/*                                                                           */
/* Input                     : pvPtpTable    - Root pointer                  */
/*                             u1PtpDbId     - Macro to identify the DB      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID        *
PtpDbGetFirstNode (VOID *pvPtpTable, UINT1 u1PtpDbId)
{
    VOID               *pvNode = NULL;
    tTMO_SLL_NODE      *pTmpNode = NULL;

    PTP_FN_ENTRY ();

    if (u1PtpDbId == PTP_SA_DATA_SET)
    {
        pTmpNode = TMO_SLL_First ((tTMO_SLL *) pvPtpTable);

        if (pTmpNode != NULL)
        {
            pvNode = pTmpNode -
                (UINT4) (FS_ULONG) ((UINT1 *)
                                    FSAP_OFFSETOF (tPtpSADs, nextSANode));
        }
    }
    else
    {
        pvNode = RBTreeGetFirst ((tRBTree) pvPtpTable);
    }

    PTP_FN_EXIT ();
    return pvNode;
}

/*****************************************************************************/
/* Function                  : PtpDbGetNextNode                              */
/*                                                                           */
/* Description               : This routine gets the next node from the      */
/*                             corresponding data base                       */
/*                                                                           */
/* Input                     : pvPtpTable    - Root pointer                  */
/*                             pvPtpNode     - Node pointer containing index */
/*                             u1PtpDbId     - Macro to identify the DB      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID        *
PtpDbGetNextNode (VOID *pvPtpTable, VOID *pvPtpNode, UINT1 u1PtpDbId)
{
    VOID               *pvNode = NULL;
    VOID               *pvTmpNode = NULL;

    PTP_FN_ENTRY ();

    if (u1PtpDbId == PTP_SA_DATA_SET)
    {
        UTL_SLL_OFFSET_SCAN ((tTMO_SLL *) pvPtpTable, pvNode, pvTmpNode,
                             tPtpSADs *)
        {
            if ((((tPtpSADs *) pvNode)->u4ContextId >
                 ((tPtpSADs *) pvPtpNode)->u4ContextId) ||
                (((tPtpSADs *) pvNode)->u1DomainId >
                 ((tPtpSADs *) pvPtpNode)->u1DomainId) ||
                (((tPtpSADs *) pvNode)->u4SAId >
                 ((tPtpSADs *) pvPtpNode)->u4SAId))
            {
                break;
            }
        }
    }
    else
    {
        pvNode = RBTreeGetNext ((tRBTree) pvPtpTable, (tRBElem *) pvPtpNode,
                                NULL);
    }

    PTP_FN_EXIT ();
    return pvNode;
}

/*****************************************************************************/
/* Function                  : PtpDbDispTimeInterval                         */
/*                                                                           */
/* Description               : This routine displays the object as the time  */
/*                             interval as Specified in Section 5.3.2 of     */
/*                             IEEE 1588 2008 Standard.                      */
/*                                                                           */
/* Input                     : i4DataType    - Indicates the type of the data*/
/*                                             to be displayed.              */
/*                             pvPtrVal      - Pointer value used to derive  */
/*                                             the output.                   */
/*                                                                           */
/* Output                    : pu1OctetList  - Output Value to be displayed. */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDbDispTimeInterval (INT4 i4DataType, VOID *pvPtrVal,
                       tSNMP_OCTET_STRING_TYPE * pDisplay)
{
    FS_UINT8            u8DisplayConst;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8Value;
    FS_UINT8            u8Result;
    tPtpDomain         *pPtpDomain = NULL;
    tPtpPort           *pPtpPort = NULL;

    FSAP_U8_CLR (&u8DisplayConst);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8Value);
    FSAP_U8_CLR (&u8Result);

    UINT8_LO (&u8DisplayConst) = PTP_DISPLAY_CONSTANT;
    switch (i4DataType)
    {
        case PTP_OFFSET_FROM_MASTER:

            pPtpDomain = (tPtpDomain *) pvPtrVal;
            FSAP_U8_ASSIGN (&u8Value,
                            &(pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                              u8OffsetFromMaster));

            break;

        case PTP_MEAN_PATH_DELAY:

            pPtpDomain = (tPtpDomain *) pvPtrVal;
            FSAP_U8_ASSIGN (&u8Value,
                            &(pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                              u8MeanPathDelay));

            break;

        case PTP_PEER_MEAN_PATH_DELAY:

            pPtpPort = (tPtpPort *) pvPtrVal;
            FSAP_U8_ASSIGN (&u8Value, &(pPtpPort->PortDs.u8PeerMeanPathDelay));
            break;

        default:
            /* This cannot happen. Exceptional case handling. Breaking here
             * will execute the subsequent portions of code that may result
             * in Divide by ZERO exception. Hence returning from here
             * */
            return;
    }

    FSAP_U8_MUL (&u8Result, &u8Value, &u8DisplayConst);

    /* Here we assume that pDisplay->pu1_OctetList is good enough to hold values
     * of maximum of 8 bytes (HI + LO)
     * */

    FSAP_U8_2STR (&u8Result, (CHR1 *) pDisplay->pu1_OctetList);
    pDisplay->i4_Length = STRLEN (pDisplay->pu1_OctetList);
}
#endif /* _PTPDB_C_ */
