/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 * $Id: ptputil.c,v 1.15 2018/01/09 11:00:59 siva Exp $
 *
 * Description: This file contains PTP task main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _PTPUTIL_C_
#define _PTPUTIL_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpUtilExtractHdrFromMsg                      */
/*                                                                           */
/* Description               : This routine determines whether the given     */
/*                             clock parameters belong to that of current    */
/*                             Master. It returns OSIX_TRUE, if the params   */
/*                             belong to that of current master & OSIX_FALSE */
/*                             otherwise.                                    */
/*                                                                           */
/* Input                     : pu1Pdu  - Pointer to the received PDU         */
/*                                                                           */
/* Output                    : pPtpHdrInfo - Pointer to the header struct    */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUtilExtractHdrFromMsg (UINT1 *pu1Pdu, tPtpHdrInfo * pPtpHdrInfo)
{
    UINT1              *pu1TmpPdu = pu1Pdu;
    FS_UINT8            u8CorrnField;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8CorrnField);
    /* Message type */
    pPtpHdrInfo->u1MsgType = pu1Pdu[PTP_MSG_TYPE_OFFSET];
    pPtpHdrInfo->u1MsgType = pPtpHdrInfo->u1MsgType & PTP_MSG_TYPE_MASK;

    pu1TmpPdu = pu1Pdu + PTP_MSG_VERSION_OFFSET;
    PTP_LBUF_GET_1_BYTE (pu1TmpPdu, pPtpHdrInfo->u1PtpVersion);
    pPtpHdrInfo->u1PtpVersion = pPtpHdrInfo->u1PtpVersion & PTP_VERSION_MASK;

    /* Received PDU consists of port identifier embedded as CLKID:PORTID 
     * 10 bytes at offset 20 */
    MEMCPY (&(pPtpHdrInfo->ClkId), pu1Pdu + PTP_MSG_PORTID_OFFSET,
            PTP_MAX_CLOCK_ID_LEN);

    MEMCPY (&(pPtpHdrInfo->u2Port),
            pu1Pdu + PTP_MSG_PORTID_OFFSET + PTP_MAX_CLOCK_ID_LEN,
            sizeof (UINT2));
    pPtpHdrInfo->u2Port = (UINT2) OSIX_NTOHS (pPtpHdrInfo->u2Port);

    /* Flags */
    MEMCPY (&(pPtpHdrInfo->u2Flags),
            pu1Pdu + PTP_MSG_FLAGS_OFFSET, sizeof (UINT2));
    pPtpHdrInfo->u2Flags = (UINT2) OSIX_NTOHS (pPtpHdrInfo->u2Flags);

    /* Correction Field */
    MEMCPY (&(pPtpHdrInfo->u8CorrectionField.u4Hi),
            pu1Pdu + PTP_MSG_CORRECTION_FIELD_OFFSET, sizeof (UINT4));
    pPtpHdrInfo->u8CorrectionField.u4Hi =
        (UINT4) OSIX_NTOHL (pPtpHdrInfo->u8CorrectionField.u4Hi);

    MEMCPY (&(pPtpHdrInfo->u8CorrectionField.u4Lo),
            pu1Pdu + PTP_MSG_CORRECTION_FIELD_OFFSET +
            PTP_MSG_CORRECTION_FIELD_LOWER_BYTE_OFFSET, sizeof (UINT4));
    pPtpHdrInfo->u8CorrectionField.u4Lo =
        (UINT4) OSIX_NTOHL (pPtpHdrInfo->u8CorrectionField.u4Lo);

    PtpClkExtractCorrnField (&(pPtpHdrInfo->u8CorrectionField), &u8CorrnField);
    FSAP_U8_ASSIGN (&(pPtpHdrInfo->u8CorrectionField), &u8CorrnField);

    /* Sequence Identifier */
    pu1TmpPdu = pu1Pdu + PTP_SEQ_OFFSET;
    PTP_LBUF_GET_2_BYTES (pu1TmpPdu, (pPtpHdrInfo->u2SeqId));

    pu1TmpPdu = pu1Pdu + PTP_LOG_MSG_INTERVAL_OFFSET;
    pPtpHdrInfo->u1LogMsgInterval = *pu1TmpPdu;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpUtilIsMsgFromCurrentParent                 */
/*                                                                           */
/* Description               : This routine determines whether the given     */
/*                             clock parameters belong to that of current    */
/*                             Master. It returns OSIX_TRUE, if the params   */
/*                             belong to that of current master & OSIX_FALSE */
/*                             otherwise.                                    */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             RxClkId  - Clock Idenity to be compared       */
/*                             u2Port   - Port identity to be compared       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilIsMsgFromCurrentParent (tPtpPort * pPtpPort, tClkId RxClkId,
                               UINT2 u2Port)
{
    tClkId              ClkId;
    INT4                i4RetVal = OSIX_FALSE;
    UINT2               u2PortId = 0;

    PTP_FN_ENTRY ();

    MEMSET (ClkId, 0, sizeof (tClkId));

    switch (pPtpPort->PortDs.u1PortState)
    {
        case PTP_STATE_PASSIVE:

            /* This utility will be invoked to restart the announce receipt 
             * timer on receiving an announce message. For ports in PASSIVE 
             * state the current Parent identity will not be updated in the
             * ParentDs. Hence, the comparision should be done only with the
             * data Set that was transmitted earlier which made this port to
             * move to PASSIVE state.
             * Refer case (d) of Section 9.2.6.11 of IEEE Std 1588-2008
             * */

            /* ASSUMPTION: pPtpPort->pPtpErBestMsg cannot be NULL for a port in
             * PASSIVE State. But for exceptional handling */
            if (pPtpPort->pPtpErBestMsg == NULL)
            {
                return i4RetVal;
            }
            MEMCPY (ClkId, pPtpPort->pPtpErBestMsg->ClkId, sizeof (tClkId));
            u2PortId = (UINT2) (pPtpPort->pPtpErBestMsg->u4SrcPortIndex);
            break;

        default:

            /* For all other states */
            MEMCPY (ClkId, pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
                    ClkId, sizeof (tClkId));
            u2PortId = (UINT2) (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                                ParentDs.u4PortIndex);
            break;
    }

    if ((MEMCMP (RxClkId, ClkId, sizeof (tClkId)) == 0) && (u2Port == u2PortId))
    {
        i4RetVal = OSIX_TRUE;
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpUtilValidateRxPkt                          */
/*                                                                           */
/* Description               : This routine validates the packet. Message    */
/*                             specific validation will not be done here.    */
/*                                                                           */
/* Input                     : pu1Pdu  - Pointer to the received PDU         */
/*                             pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilValidateRxPkt (UINT1 *pu1Pdu, tPtpPort * pPtpPort)
{
    tClkId              ClkId;
    UINT1              *pu1PTrcTlv = NULL;
    UINT1              *pu1TmpPtpPdu = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2Port = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2TlvLen = 0;
    UINT2               u2PktLen = 0;
    UINT1               u1MsgType = 0;

    PTP_FN_ENTRY ();

    MEMSET (ClkId, 0, PTP_MAX_CLOCK_ID_LEN);

    pu1Pdu = pu1Pdu + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    /* Received PDU consists of port identifier embedded as CLKID:PORTID 
     * 10 bytes at offset 20 */

    u1MsgType = pu1Pdu[PTP_MSG_TYPE_OFFSET];
    u1MsgType = u1MsgType & PTP_MSG_TYPE_MASK;

    pu1TmpPtpPdu = pu1Pdu + PTP_MSG_PORTID_OFFSET;
    PTP_LBUF_GET_N_BYTES (pu1TmpPtpPdu, ClkId, PTP_MAX_CLOCK_ID_LEN);

    PTP_LBUF_GET_2_BYTES (pu1TmpPtpPdu, u2Port);
    /* Get Msg Len Offset */
    pu1TmpPtpPdu = pu1Pdu + PTP_MSG_LEN_OFFSET;
    PTP_LBUF_GET_2_BYTES (pu1TmpPtpPdu, u2PktLen);
#ifdef L2RED_WANTED
    /*In case of active node CLKID should contain 0xFF and 0xFE in 4 and 5 byte
     * respectively but in Standby CLKID should contain 0xFF and 0xFF in 4 and 5 byte
     * respectively.So in delay response if CLKID is not correct than drop the packet.*/
    if (((gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
         (gPtpRedGlobalInfo.u1NumOfStandbyNodesUp != 0)) &&
        ((u1MsgType == PTP_DELAY_RESP_MSG) ||
         (u1MsgType == PTP_PEER_DELAY_RESP_MSG)))
    {
        if (PTP_RED_CLK_IDENTITY_CHECK (pu1Pdu) !=
            PTP_RED_CLK_ID_ACTIVE_FIFTH_VAL)
        {
            /*Dropping packet because its not valid for active node. */
            return (OSIX_FAILURE);
        }
    }
    else if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY &&
             ((u1MsgType == PTP_DELAY_RESP_MSG) ||
              (u1MsgType == PTP_PEER_DELAY_RESP_MSG)))
    {
        if (PTP_RED_CLK_IDENTITY_CHECK (pu1Pdu) !=
            PTP_RED_CLK_ID_STANDBY_FIFTH_VAL)
        {
            /*Dropping packet because packet is not valid for stand node. */
            return (OSIX_FAILURE);
        }
        else if (PTP_RED_CLK_IDENTITY_CHECK (pu1Pdu) ==
                 PTP_RED_CLK_ID_STANDBY_FIFTH_VAL)
        {
            /*Replacing clock identity because it receive 
             *correct packet from active node.*/
            PTP_RED_CLK_IDENTITY_REPLACE (pu1Pdu);
        }
    }
#endif
    /* checking the invalid message length packets */
    switch (u1MsgType)
    {
        case PTP_SYNC_MSG:
        case PTP_DELAY_REQ_MSG:
        case PTP_FOLLOW_UP_MSG:
        case PTP_PDELAY_RESP_FOLLOWUP_MSG:
            if (u2PktLen < PTP_SYNC_MSG_SIZE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "Discarding the invalid message length PTP packets \r\n"));
                pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }
            break;

        case PTP_PEER_DELAY_REQ_MSG:
        case PTP_PEER_DELAY_RESP_MSG:
        case PTP_DELAY_RESP_MSG:
            if (u2PktLen < PTP_MSG_DELAY_RESP_LEN)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "Discarding the invalid message length PTP packets \r\n"));
                pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }
            break;

        case PTP_ANNC_MSG:
            if (u2PktLen < PTP_ANNC_MSG_SIZE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "Discarding the invalid message length PTP packets \r\n"));
                pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }
            /* Check the Path Trace Option for this domain and processes Packet */
            if ((PtpPTrcIsPathTraceEnabled (pPtpPort->pPtpDomain)) == OSIX_TRUE)
            {
                /* Search from the start of the header */
                pu1TmpPtpPdu = pu1Pdu;
                u2OffSet = 0;
                if ((PtpUtilFindTlv (pu1TmpPtpPdu, u2PktLen, &u2OffSet,
                                     PTP_TLV_TYPE_PATH_TRACE, &pu1PTrcTlv,
                                     &u2TlvLen)) == OSIX_SUCCESS)
                {
                    if ((PtpPTrcIsLoopedAnnounceMsg (pPtpPort->pPtpDomain,
                                                     &ClkId, pu1PTrcTlv)) ==
                        OSIX_TRUE)
                    {
                        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                  "Discarding the received "
                                  "Looped Announce Message\r\n"));

                        pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;

                        PTP_FN_EXIT ();
                        return (OSIX_FAILURE);
                    }
                }
            }
    }

    if (MEMCMP (ClkId, pPtpPort->PortDs.ClkId, PTP_MAX_CLOCK_ID_LEN) == 0)
    {
        /* Clock identities are same */
        if (u2Port == (UINT2) (pPtpPort->PortDs.u4PtpPortNumber))
        {
            /* Same Message received */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Transmitted message received over port %d ....\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Ignoring the message.......\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        }
        else
        {
            /* Same clock identifiers but different ports. If the domain is
             * operating as Boundary clock, move the non best port number to
             * PASSIVE state
             * */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Transmitted message over some other port in the clock "
                      "received on port %d for same clock ....\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            /* PASSIVE state when an Announce message is received from 
             * the clock that transmitted the Announce message*/
            if (u1MsgType == PTP_ANNC_MSG)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "Moving non best port %d to PASSIVE State....\r\n",
                          pPtpPort->PortDs.u4PtpPortNumber));

                PtpSemMakePassive (pPtpPort);
            }
        }

        i4RetVal = OSIX_FAILURE;
    }

    PTP_FN_EXIT ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpUtilIsDelayRespFromDelayReq                */
/*                                                                           */
/* Description               : This routine determines whether the given     */
/*                             clock parameters belong to that of given      */
/*                             Port.   It returns OSIX_TRUE, if the params   */
/*                             belong to that of current Port   & OSIX_FALSE */
/*                             otherwise.                                    */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Extracted Header Information.   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilIsDelayRespFromDelayReq (tPtpPort * pPtpPort, tPtpHdrInfo * pPtpHdrInfo,
                                UINT1 *pu1RxDelayResp)
{
    INT4                i4RetVal = OSIX_FALSE;
#define PTP_PORT_ID_LEN 10
    UINT1               au1SrcPortId[PTP_PORT_ID_LEN] /* [PTP_PORT_ID_LEN] */ ;
    UINT1              *pu1Temp = NULL;
    UINT2               u2PortVal = 0;

    PTP_FN_ENTRY ();

    MEMSET (au1SrcPortId, 0, PTP_PORT_ID_LEN);
    MEMCPY (au1SrcPortId, pu1RxDelayResp + PTP_REQUESTING_PORT_ID_OFFSET,
            PTP_PORT_ID_LEN);

    pu1Temp = au1SrcPortId + PTP_MAX_CLOCK_ID_LEN;
    MEMCPY (&u2PortVal, pu1Temp, sizeof (UINT2));
    u2PortVal = (UINT2) (OSIX_NTOHS (u2PortVal));

    /* To determine whether the message is associated with a delay request
     * message, following method is used.
     * Get the requesting port identity field of the received message.
     * Compare the same with the received port identity. Both should be same 
     * for processing the message.
     * */
    if (MEMCMP (pPtpPort->PortDs.ClkId, au1SrcPortId, sizeof (tClkId)) == 0)
    {
        /* Now compare the port numbers and sequence identities. For Sequence
         * Identity, we would have transmitted the identity and incremented the
         * same by 1, so that it can be used as such in the next iteration. 
         * Hence comparision is done as follows (RxSeqId == SeqIdPool - 1)*/
        if ((u2PortVal == (UINT2) (pPtpPort->PortDs.u4PtpPortNumber))
            && (pPtpHdrInfo->u2SeqId ==
                (pPtpPort->PtpPortSeqIdPool.u2DelayReqSeqId - 1)))
        {
            i4RetVal = OSIX_TRUE;
        }
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : PtpUtilFindNextTlv                                         */
/*                                                                           */
/* Description  : This is the used to find the Tlv in the given PTP message  */
/*                if the offset value is 0 then it means find the first tlv  */
/*                from the given message or it will find the next tlv form   */
/*                the cuurent tlv if the offset is not 0  than pu1PtpPdu     */
/*                should point the valid TLV and offset should be valid      */
/*                                                                           */
/* Input        : pu1PtpPdu   - PTP Message Pointer.                         */
/*              : u2PduLen    - PTP Message Length.                          */
/*              : pu2OffSet   - Offset from the PTP Message Header.          */
/*              : ppu1Tlv     - Poiner to the Next Tlv.                      */
/*              : pu2TlvType  - Poiner to the Next Tlv Type                  */
/*              : pu2TlvLen   - Poiner to the Next Tlv Length.               */
/*                                                                           */
/* Output       : Next Tlv Position, Length, Type, Offset                    */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilFindNextTlv (UINT1 *pu1PtpPdu, UINT2 u2PduLen, UINT2 *pu2OffSet,
                    UINT1 **ppu1Tlv, UINT2 *pu2TlvType, UINT2 *pu2TlvLen)
{
    UINT1              *pu1TmpPtpPdu = NULL;
    UINT2               u2TmpOffSet = 0;
    UINT2               u2TlvLen = 0;
    UINT2               u2TlvType = 0;
    UINT1               u1PduType = 0;

    PTP_FN_ENTRY ();

    pu1TmpPtpPdu = pu1PtpPdu;
    /*  Announce / Sig Message  */
    if (*pu2OffSet == 0)
    {
        u1PduType = (*pu1TmpPtpPdu) & PTP_MSG_TYPE_MASK;

        switch (u1PduType)
        {
            case PTP_ANNC_MSG:
                u2TmpOffSet = PTP_ANNC_HDR_TLV_OFFSET;
                break;

            default:
                /* Invalid Ptp Msg Type */
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpUtilFindNextTlv : Invalid Ptp Msg Type. \r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
                break;
        }

        if (u2TmpOffSet >= u2PduLen)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpUtilFindNextTlv : End of Pkt. \r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
        pu1TmpPtpPdu = pu1PtpPdu + u2TmpOffSet;
    }
    else
    {
        if (*pu2OffSet >= u2PduLen)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpUtilFindNextTlv : "
                      "OffSet >= u2PduLen End of Pkt.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
        /* Get Given Tlv Length find next Tlv */
        pu1TmpPtpPdu += PTP_TLV_LEN_FIELD_OFFSET;
        PTP_LBUF_GET_2_BYTES (pu1TmpPtpPdu, u2TlvLen);

        pu1TmpPtpPdu = (pu1PtpPdu + (PTP_TLV_DATA_FIELD_OFFSET + u2TlvLen));
        u2TmpOffSet = (UINT2)
            (*pu2OffSet + (UINT2) PTP_TLV_DATA_FIELD_OFFSET + u2TlvLen);
    }

    *pu2OffSet = u2TmpOffSet;

    *ppu1Tlv = pu1TmpPtpPdu;

    PTP_LBUF_GET_2_BYTES (pu1TmpPtpPdu, u2TlvType);
    PTP_LBUF_GET_2_BYTES (pu1TmpPtpPdu, u2TlvLen);

    *pu2TlvType = u2TlvType;
    *pu2TlvLen = u2TlvLen;

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
              "PtpUtilFindNextTlv : u2OffSet %d pu1Tlv %p u2TlvType %d "
              "u2TlvLen %d \r\n", u2TmpOffSet, pu1TmpPtpPdu, u2TlvType,
              u2TlvLen));

    if (*pu2OffSet >= u2PduLen)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                  "PtpUtilFindNextTlv : "
                  "OffSet >= u2PduLen End of Pkt.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpUtilFindTlv                                             */
/*                                                                           */
/* Description  : This is the used to find given Tlv in the given PTP message*/
/*                if the offset value is 0 then it means find from the first */
/*                tlv of the given message or it will find from the cuurent  */
/*                tlv if the offset is not 0  than pu1PtpPdu should point    */
/*                the valid TLV.                                             */
/*                                                                           */
/* Input        : pu1PtpPdu   - PTP Message Pointer.                         */
/*              : u2PduLen    - PTP Message Length.                          */
/*              : pu2OffSet   - Pointer Offset from the PTP Message Header.  */
/*              : u2TlvType   - TLV Type needs to be find.                   */
/*              : ppu1Tlv     - Poiner to that Tlv.                          */
/*              : pu2TlvLen   - Poiner to that Tlv Length.                   */
/*                                                                           */
/* Output       : Next Tlv Position, Length, Type, Offset                    */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilFindTlv (UINT1 *pu1PtpPdu, UINT2 u2PduLen, UINT2 *pu2OffSet,
                UINT2 u2TlvType, UINT1 **ppu1Tlv, UINT2 *pu2TlvLen)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT2               u2TmpTlvType = 0;
    UINT2               u2TmpTlvLen = 0;
    UINT2               u2TmpOffSet = *pu2OffSet;

    PTP_FN_ENTRY ();

    i4Status = PtpUtilFindNextTlv (pu1PtpPdu, u2PduLen, &u2TmpOffSet,
                                   ppu1Tlv, &u2TmpTlvType, &u2TmpTlvLen);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpUtilFindTlv : Given TLV is not found. \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    do
    {
        if (u2TlvType == u2TmpTlvType)
        {
            *pu2TlvLen = u2TmpTlvLen;
            *pu2OffSet = u2TmpOffSet;
            PTP_FN_EXIT ();
            return (OSIX_SUCCESS);
        }
    }
    while ((PtpUtilFindNextTlv (pu1PtpPdu, u2PduLen, &u2TmpOffSet, ppu1Tlv,
                                &u2TmpTlvType, &u2TmpTlvLen)) == OSIX_SUCCESS);

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              ALL_FAILURE_TRC,
              "PtpUtilFindTlv : Given TLV is not found. \r\n"));
    PTP_FN_EXIT ();
    return (OSIX_FAILURE);
}

/*****************************************************************************/
/* Function                  : PtpUtilIsSlavePortInBoundaryClk               */
/*                                                                           */
/* Description               : This routine finds whether the boundary clock */
/*                             contains any slave port other than the given  */
/*                             Port.                                         */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilIsSlavePortInBoundaryClk (tPtpPort * pPtpPort)
{
    tPtpPort           *pPtpTmpPort = NULL;
    INT4                i4RetVal = OSIX_FALSE;
    UINT4               u4Port = 1;
    BOOL1               bResult = OSIX_FALSE;

    PTP_FN_ENTRY ();

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              INIT_SHUT_TRC,
              "Entering PtpUtilIsSlavePortInBoundaryClk...\r\n"));

    if (pPtpPort->pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE)
    {
        PTP_FN_EXIT ();
        return OSIX_FALSE;
    }

    for (u4Port = 1; u4Port <= PTP_MAX_PORTS; u4Port++)
    {
        if (u4Port == pPtpPort->PortDs.u4PtpPortNumber)
        {
            continue;
        }

        OSIX_BITLIST_IS_BIT_SET (pPtpPort->pPtpDomain->DomainPortList, u4Port,
                                 PTP_PORT_LIST_SIZE, bResult);

        if (bResult != OSIX_TRUE)
        {
            continue;
        }

        pPtpTmpPort = PtpIfGetPortEntry (pPtpPort->u4ContextId,
                                         pPtpPort->u1DomainId, u4Port);

        if (pPtpTmpPort == NULL)
        {
            continue;
        }

        /* Port is member of the domain. */
        if (pPtpTmpPort->PortDs.u1PortState == PTP_STATE_SLAVE)
        {
            /* One of the ports is in SLAVE state, update the variable and
             * break from the loop */
            i4RetVal = OSIX_TRUE;
            break;
        }
        /* Reinitializing */
        bResult = OSIX_FALSE;
    }

    PTP_FN_EXIT ();
    return (i4RetVal);
}

/*****************************************************************************/
/* Function                  : PtpUtilInitPortsInDmn                         */
/*                                                                           */
/* Description               : This routine scans through all ports in this  */
/*                             domain context and initialises the port       */
/*                                                                           */
/* Input                     : u4ContextId   -  Context identifier           */
/*                             u1DomainId    -  Domain identifier            */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUtilInitPortsInDmn (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpDomain         *pPtpDomain = NULL;
    UINT4               u4ProtId = 0;

    PTP_FN_ENTRY ();

    while ((pPtpPort =
            PtpIfGetNextPortEntry (u4ContextId, u1DomainId, u4ProtId)) != NULL)
    {
        if ((pPtpPort->u4ContextId != u4ContextId) ||
            (pPtpPort->u1DomainId != u1DomainId))
        {
            break;
        }

        pPtpDomain = pPtpPort->pPtpDomain;
        pPtpDomain->u1InitInProgress = OSIX_TRUE;

        u4ProtId = pPtpPort->PortDs.u4PtpPortNumber;

        /* This function will be called when the domain row status is
         * set to active. Move the port state to initialize state if the port
         * is not in disabled state
         */
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_DISABLED)
        {
            PtpSemPortSemHandler (pPtpPort, PTP_INIT_EVENT);
        }
        PtpIfEnablePort (pPtpPort);
    }

    if (pPtpDomain != NULL)
    {
        pPtpDomain->u1InitInProgress = OSIX_FALSE;
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpUtilGetAltMstFlagStatus                    */
/*                                                                           */
/* Description               : This routine used to find the Alternate flag  */
/*                             status for the given port                     */
/*                                                                           */
/* Input                     : pPtpPort      -  PtpPort information struct   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpUtilGetAltMstFlagStatus (tPtpPort * pPtpPort)
{
    tPtpAltMstInfo      PtpAltMstInfo;
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    /* if the portstate is master return FALSE 
     * else u1NumberOfAltMaster is >  0 return TRUE  */
    if (pPtpPort->PortDs.u1PortState == PTP_STATE_SLAVE)
    {
        i4Status = PtpAlMstGetAltMstInfo (pPtpPort->u4ContextId,
                                          pPtpPort->u1DomainId,
                                          pPtpPort->PortDs.u4PtpPortNumber,
                                          &PtpAltMstInfo);
        if ((i4Status == OSIX_SUCCESS) &&
            (PtpAltMstInfo.u1NumberOfAltMaster != 0) &&
            (pPtpPort->bIsEligibleAltMst == OSIX_TRUE))
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "PtpUtilGetAltMstFlagStatus : True. \r\n"));
            PTP_FN_EXIT ();
            return (OSIX_TRUE);
        }
    }
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
              "PtpUtilGetAltMstFlagStatus : False. \r\n"));
    PTP_FN_EXIT ();
    return (OSIX_FALSE);
}

/*****************************************************************************/
/* Function                  : PtpUtilGetV2AnncInfo                          */
/*                                                                           */
/* Description               : This routine used to get the Announce message */
/*                             related values for the Compatibility module   */
/*                                                                           */
/* Input                     : pPtpPort      -  PtpPort information struct   */
/*                             pPtpv2Info    -  Ptpv2Info structure          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUtilGetV2AnncInfo (tPtpPort * pPtpPort, tPtpv2Info * pPtpv2Info)
{
    PTP_FN_ENTRY ();

    pPtpv2Info->AnncHdr.i2CurrUTCOffset =
        (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u2CurrentUtcOffset);

    MEMCPY (pPtpv2Info->AnncHdr.GMClkId,
            (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkId),
            PTP_MAX_CLOCK_ID_LEN);

    pPtpv2Info->u2GMPortId = (UINT2)
        pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4PortIndex;

    pPtpv2Info->u2GMSeqId = 0;

    MEMCPY (&(pPtpv2Info->AnncHdr.GMClkQuality),
            &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
              GMClkQuality), sizeof (tPtpClkQuality));

    pPtpv2Info->AnncHdr.u1GMPri1 = (pPtpPort->pPtpDomain->ClockDs.
                                    PtpOcBcClkDs.ParentDs.u1GMPriority1);
    pPtpv2Info->AnncHdr.u1GMPri2 = (pPtpPort->pPtpDomain->ClockDs.
                                    PtpOcBcClkDs.ParentDs.u1GMPriority2);

    MEMCPY (&(pPtpv2Info->LocalClkQuality),
            &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality),
            sizeof (tPtpClkQuality));

    pPtpv2Info->AnncHdr.u2StepsRemoved = (UINT2)
        (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u4StepsRemoved);

    pPtpv2Info->ParentPortId.u2PortNumber = (UINT2)
        pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4PortIndex;

    MEMCPY (pPtpv2Info->ParentPortId.ClkId,
            pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.ClkId,
            PTP_MAX_CLOCK_ID_LEN);

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
        bParentStats == TRUE)
    {
        pPtpv2Info->u2ObsParentOffsetScaledLogVar =
            pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
            u2ObservedOffsetScaledLogVariance;

        pPtpv2Info->i4ObsParentClkPhaseCngRate = (UINT4)
            pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
            u4ObservedClkPhaseChangeRate;
    }
    else
    {
        pPtpv2Info->u2ObsParentOffsetScaledLogVar = 0;
        pPtpv2Info->i4ObsParentClkPhaseCngRate = 0;
    }

    pPtpv2Info->bCurrUtcOffsetValid =
        (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.
         bCurrentUtcOffsetValid);

    pPtpv2Info->ComHdr.i1LogMsgInter =
        (INT1) pPtpPort->PortDs.u1AnnounceInterval;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpUtilIsGrandMasterClk                       */
/*                                                                           */
/* Description               : This routine determines whether the given     */
/*                             clock is the grandmaster clock or not.        */
/*                             It returns OSIX_TRUE, if grandmaster          */
/*                             & OSIX_FALSE otherwise.                       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpUtilIsGrandMasterClk (tPtpDomain * pPtpDomain)
{
    PTP_FN_ENTRY ();

    if ((MEMCMP (&(pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkId),
                 &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),
                 sizeof (tClkId))) == 0)
    {
        PTP_FN_EXIT ();
        return (OSIX_TRUE);
    }
    PTP_FN_EXIT ();
    return (OSIX_FALSE);
}

/*****************************************************************************/
/* Function                  : PtpUtilIsPtpStarted                           */
/*                                                                           */
/* Description               : This routine determines whether PTP is started*/
/*                             in the context. If PTP is started, it returns */
/*                             OSIX_TRUE, and OSIX_FALSE, otherwise.         */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpUtilIsPtpStarted (UINT4 u4ContextId)
{
    BOOL1               bRetVal = OSIX_TRUE;

    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        /* PTP module itself is not initialized. */
        bRetVal = OSIX_FALSE;
    }

    if ((bRetVal == OSIX_TRUE) && (PtpCxtGetNode (u4ContextId) == NULL))
    {
        /* Given context is not present in the system */
        bRetVal = OSIX_FALSE;
    }

    PTP_FN_EXIT ();
    return bRetVal;
}

/*****************************************************************************/
/* Function                  : PtpUtilLogBase2ToMilliSec                     */
/*                                                                           */
/* Description               : This routine used to convert the log base 2   */
/*                             value to Mill Second                          */
/*                                                                           */
/* Input                     : i4LogBase2Val - log base 2 value              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Mill Second vaule                             */
/*                                                                           */
/*****************************************************************************/
UINT4
PtpUtilLogBase2ToMilliSec (INT1 i1LogBase2Val)
{
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    /*  Min and Max Range Check */
    if ((i1LogBase2Val < PTP_MIN_LOG_RANGE) ||
        (i1LogBase2Val > PTP_MAX_LOG_RANGE))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "LogBase2Val %d is out of range.....\r\n", i1LogBase2Val));
        PTP_FN_EXIT ();
        return (u4MilliSec);
    }

    /* This utility finds the antilog (base 2) for the provisioned logrthamic
     * values. The Antilog for a value is computed as follows 2^x. 
     * This is equivalent to (1 << x).
     * 
     * Now using the above logic the following is calculated.
     * Antilog (x) = (1 << x)
     * Antilog (-x) = 2^-x = 1/(2^x) = 1/(1 << x)
     *
     * Since, this routine always returns in terms of milliseconds, the value
     * is always multiplied by 1000
     * */
    if (i1LogBase2Val < 0)
    {
        /* for negative values  */
        u4MilliSec = (UINT4)
            (PTP_SEC_TO_MIILI_SEC_CONV_FACTOR / (1 << -(i1LogBase2Val)));
    }
    else
    {
        /* for positive values  */
        u4MilliSec = (PTP_SEC_TO_MIILI_SEC_CONV_FACTOR * (1 << i1LogBase2Val));
    }

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
              "LogBase2Val %d is MilliSec %d\r\n", i1LogBase2Val, u4MilliSec));

    PTP_FN_EXIT ();
    return (u4MilliSec);
}

/*****************************************************************************/
/* Function                  : PtpUtilCmpIdentities                          */
/*                                                                           */
/* Description               : This routine                                  */
/*                                                                           */
/* Input                     : ClkIdA - Clock Identity of A.                 */
/*                             ClkIdB - Clock Identity of B.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUtilCmpIdentities (tClkId ClkIdA, tClkId ClkIdB)
{
    INT4                i4RetVal = PTP_DS_EQUAL_SENDER_RECEIVER;
    INT4                i4RetVal1 = 0;

    PTP_FN_ENTRY ();

    i4RetVal1 = MEMCMP (ClkIdA, ClkIdB, PTP_MAX_CLOCK_ID_LEN);

    if (i4RetVal1 < 0)
    {
        i4RetVal = PTP_DS_BETTER_SENDER;
    }
    else if (i4RetVal1 > 0)
    {
        i4RetVal = PTP_DS_BETTER_RECEIVER;
    }
    /* Equal Clock identities */

    PTP_FN_EXIT ();

    return i4RetVal;
}

/******************************************************************************/
/*  Function Name   : PtpUtilGetTraceOptionValue                              */
/*                                                                            */
/*  Description     : This function used to get the trace types configured in */
/*                    the given trace input string.                           */
/*                                                                            */
/*                    Given input trace string will be compared with the      */
/*                    valid trace strings supported by the module. If the     */
/*                    valid trace string is given, then the bit number        */
/*                    corresponding to the TraceID will be set in the output  */
/*                    Trace option variable.                                  */
/*                                                                            */
/*                    If any one of the given string is not supported, then   */
/*                    the output trace option will be set to zero and         */
/*                    returned.                                               */
/*                                                                            */
/*  Input(s)        : pPtpUtlValidTraces - Structure containing valid traces  */
/*                                     supported by the particular module.    */
/*                                     It contains two values, pu1TraceStrings*/
/*                                     containing array of valid trace strings*/
/*                                     supported and u2MaxTrcTypes contains   */
/*                                     maximum trace types supported by the   */
/*                                     particular module.                     */
/*                    pu1TraceInput  - Input trace string, that needs to be   */
/*                                     parsed                                 */
/*                    u2TrcLen       - Length of the input trace string       */
/*                                                                            */
/*  Output(s)       : pu4TraceOption - Variable having the information of     */
/*                                     trace types configured. Bit number     */
/*                                     corresponding to the trace ID will     */
/*                                     be set in this variable.               */
/*                    pu1TraceStatus - Flag indicates whether trace is        */
/*                                     "enable" trace or "disable" trace      */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PtpUtilGetTraceOptionValue (tPtpUtlValidTraces * pPtpUtlValidTraces,
                            UINT1 *pu1TraceInput,
                            UINT2 u2TrcLen, UINT4 *pu4TraceOption,
                            UINT1 *pu1TraceStatus)
{
    UINT2               u2TokenSize = 0;
    UINT2               u2TrcId;
    UINT1               u1ValidToken = 0;
    UINT1               u1Delimiter = (UINT1) PTP_UTL_TRACE_TOKEN_DELIMITER;

    PTP_FN_ENTRY ();

    *pu4TraceOption = 0;
    *pu1TraceStatus = PTP_UTL_TRACE_DISABLE;

    if ((!pu1TraceInput) || (!pPtpUtlValidTraces->pu1TraceStrings))
    {
        return;
    }

    /* Check for enable token */
    if ((STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable "))) == 0)
    {
        pu1TraceInput += STRLEN ("enable ");
        u2TrcLen -= STRLEN ("enable ");
        *pu1TraceStatus = PTP_UTL_TRACE_ENABLE;
    }
    /* Check for disable token */
    else if ((STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable "))) == 0)
    {
        pu1TraceInput += STRLEN ("disable ");
        u2TrcLen -= STRLEN ("disable ");
    }
    else
    {
        /* First  token has to be either enable or disable */
        return;
    }

    while (u2TrcLen > 0)
    {
        /* scan for delimiter and find the trace string */

        while ((u2TrcLen > 0)
               && (*(pu1TraceInput + u2TokenSize) != u1Delimiter))
        {
            u2TrcLen--;
            u2TokenSize++;
        }

        /* A single trace string is parsed, now check it with the valid
         * trace type supported. If the given string is not a valid trace
         * type, then trace option will be resetted to 0 and returned */

        for (u2TrcId = 0; u2TrcId < pPtpUtlValidTraces->u2MaxTrcTypes;
             u2TrcId++)
        {
            if ((STRNCMP (((pPtpUtlValidTraces->pu1TraceStrings)
                           + (u2TrcId * PTP_UTL_MAX_TRC_LEN)),
                          pu1TraceInput, u2TokenSize)) == 0)
            {
                *pu4TraceOption |= (1 << u2TrcId);
                u1ValidToken = 1;
                break;
            }
        }

        /* Given input trace string matched with any valid trace supported,
         * output trace option will be set to zero and return. */

        if (!u1ValidToken)
        {
            *pu4TraceOption = 0;
            PTP_FN_EXIT ();
            return;
        }

        if (u2TrcLen > 0)
        {
            /* Trace length has to be subtracted by 1 for space delimiter.
             * u2TrcLen will be zero, when the end of the input string is
             * reached */
            u2TrcLen--;
        }
        pu1TraceInput += (u2TokenSize + 1);    /* Plus for space delimiter */
        u2TokenSize = 0;        /* Reinitialize Token size with zero */
    }

    PTP_FN_EXIT ();
    return;
}

/******************************************************************************/
/*  Function Name   : PtpUtilGetTraceOptionString                             */
/*                                                                            */
/*  Description     : This function used to get the trace string from the     */
/*                    given trace option value.                               */
/*                                                                            */
/*                    Given input trace option value will parsed bit by bit.  */
/*                    If the particular bit is set, then the bit numberth     */
/*                    trace string will be appended to the output trace       */
/*                    string.                                                 */
/*                                                                            */
/*                    If the trace option value given is zero, then the       */
/*                    function will be returned without further processing.   */
/*                                                                            */
/*  Input(s)        : pPtpUtlValidTraces - Structure containing valid traces  */
/*                                     supported by the particular module.    */
/*                                     It contains two values, pu1TraceStrings*/
/*                                     containing array of valid trace strings*/
/*                                     supported and u2MaxTrcTypes contains   */
/*                                     maximum trace types supported by the   */
/*                                     particular module.                     */
/*                    u4TrcOption    - Variable having the information of     */
/*                                     trace types configured. Bit number     */
/*                                     corresponding to the trace ID will     */
/*                                     be returned in the output trace string */
/*                                                                            */
/*  Output(s)       : pu1RetTrcString- Output trace string, will have the     */
/*                                     list of traces enabled separated by    */
/*                                     a space as a delimited.                */
/*                    pu2TrcLen      - Returns the length of the trace string */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PtpUtilGetTraceOptionString (tPtpUtlValidTraces * pPtpUtlValidTraces,
                             UINT4 u4TrcOption,
                             UINT1 *pu1RetTrcString, UINT2 *pu2TrcLen)
{
    UINT2               u2BitNo = 0;

    PTP_FN_ENTRY ();

    /* Verifying input strings for NULL */
    if ((!pu1RetTrcString) || (!pPtpUtlValidTraces->pu1TraceStrings))
    {
        PTP_FN_EXIT ();
        return;
    }

    /* No trace is enabled */
    if (u4TrcOption == 0)
    {
        PTP_FN_EXIT ();
        return;
    }

    for (; u2BitNo < pPtpUtlValidTraces->u2MaxTrcTypes; u2BitNo++)
    {
        if ((u4TrcOption >> u2BitNo) & OSIX_TRUE)
        {
            STRNCAT (pu1RetTrcString, (pPtpUtlValidTraces->pu1TraceStrings
                                       + (u2BitNo * PTP_UTL_MAX_TRC_LEN)),
                     STRLEN ((pPtpUtlValidTraces->pu1TraceStrings
                              + (u2BitNo * PTP_UTL_MAX_TRC_LEN))));

            *pu2TrcLen += STRLEN (pPtpUtlValidTraces->pu1TraceStrings +
                                  (u2BitNo * PTP_UTL_MAX_TRC_LEN));

            /* Adding one space as a delimiter between the trace strings */
            STRNCAT (pu1RetTrcString, " ", STRLEN (" "));
            (*pu2TrcLen)++;
        }
    }

    /* Remove the last space delimiter */
    (*pu2TrcLen)--;
    MEMSET ((pu1RetTrcString + (*pu2TrcLen)), 0, 1);

    PTP_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function                  : PtpUtilGetHashValue                           */
/*                                                                           */
/* Description               : This routine calculates the Hash Value with   */
/*                             the given interface type and interface index  */
/*                                                                           */
/* Input                     : PtpDeviceType   -   Device type               */
/*                             u4IfIndex       -   Interface index / vlan id */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : Hash Value                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
PtpUtilGetHashValue (ePtpDeviceType PtpDeviceType, UINT4 u4IfIndex)
{
    UINT4               u4HashValue = 0;

    PTP_FN_ENTRY ();

    u4HashValue = (u4IfIndex * PtpDeviceType) % PTP_HASH_TABLE_SIZE;

    PTP_FN_EXIT ();

    return u4HashValue;
}

/*****************************************************************************/
/* Function                  : PtpUtilFindStartIndex                         */
/*                                                                           */
/* Description               : This routine used to find the Starting index  */
/*                             for a table which is using the context and    */
/*                             domain as a 1st and 2nd Index and the third   */
/*                             index is starts wiht 0. The bIsFirst indicates*/
/*                             the retrun Indices are first or not.          */
/*                                                                           */
/* Input                     : u4ContextId  - Context Identifier.            */
/*                           : pu4TmpContextId - Pointer to Starting         */
/*                           :                   Context Identifier.         */
/*                           : u1DomainId   - Domain Identifier.             */
/*                           : pu1TmpDomainId  - Pointer to Starting         */
/*                           :                   Domain Identifier.          */
/*                           : bIsFirst     - Is the First Index or Not      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
PtpUtilFindStartIndex (UINT4 u4ContextId, UINT4 *pu4IdxContextId,
                       UINT1 u1DomainId, UINT1 *pu1IdxDomainId,
                       BOOL1 * bIsFirst)
{
    if (u4ContextId == 0)
    {
        *pu4IdxContextId = u4ContextId;

        if (u1DomainId == 0)
        {
            *pu1IdxDomainId = u1DomainId;
            *bIsFirst = OSIX_TRUE;
        }
        else
        {
            *pu1IdxDomainId = (UINT1) (u1DomainId - 1);
            *bIsFirst = OSIX_FALSE;
        }
    }
    else
    {
        *bIsFirst = OSIX_FALSE;

        if (u1DomainId == 0)
        {
            *pu4IdxContextId = u4ContextId - 1;
            *pu1IdxDomainId = 0xFF;
        }
        else
        {
            *pu4IdxContextId = u4ContextId;
            *pu1IdxDomainId = (UINT1) (u1DomainId - 1);
        }
    }
}

/*****************************************************************************/
/* Function                  : PtpUtilSecLogBase2                            */
/*                                                                           */
/* Description               : This routine used to convert the Second value */
/*                             to log base 2 value                           */
/*                                                                           */
/* Input                     : i1Sec - Second value                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : log base 2 value                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
PtpUtilSecLogBase2 (INT1 i1Sec)
{
    INT1                i1LogBase2Val = 0;

    PTP_FN_ENTRY ();

    /* This utility finds the log (base 2) for given value 
     * To compute log (base 2) X, we need to divide X by 2 until we get 1.
     * Now using the above logic the following is calculated.
     * For negative values it will be 0 
     */
    if (i1Sec > 0)
    {
        do
        {
            i1Sec = (INT1) ((i1Sec / PTP_LOG_BASE2_DIV_VAL));
            i1LogBase2Val++;
        }
        while (i1Sec != 0);
    }

    PTP_TRC ((PTP_MAX_CONTEXTS, PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
              "LogBase2Val %d is Sec %d\r\n", i1LogBase2Val, i1Sec));

    PTP_FN_EXIT ();
    return (i1LogBase2Val);
}

/*****************************************************************************/
/* Function                  : PtpUtilGetDebugLevel                          */
/*                                                                           */
/* Description               : This routine used to get                      */
/*                                debug level as int                         */
/*                                                                           */
/* Input                     : u4ContextId context                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : log base 2 value                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
PtpUtilGetDebugLevel (UINT4 u4ContextId, UINT4 *u4DbgLevel)
{
    tPtpCxt            *pContextInfo = NULL;

    pContextInfo = PtpCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "Context Not Present\r\n"));
        return OSIX_FAILURE;
    }

    if (PtpUtilIsPtpStarted (u4ContextId) == OSIX_FALSE)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "Module Not Started\r\n"));
        return OSIX_FAILURE;
    }

    *u4DbgLevel = (UINT4) pContextInfo->u2Trace;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUtlU8Div                                   */
/*                                                                           */
/* Description               : This routine used to perform 8 Byte Division  */
/*                                                                           */
/* Input                     : pu8Val1 - Numerator                           */
/*                           : pu8Val2 - Denominator                         */
/*                                                                           */
/* Output                    : pu8Result                                     */
/*                           : pu8Reminder                                   */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : log base 2 value                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUtlU8Div (FS_UINT8 * pu8Result, FS_UINT8 * pu8Reminder, FS_UINT8 * pu8Val1,
             FS_UINT8 * pu8Val2)
{
    UtlU8Div (pu8Result, pu8Reminder, pu8Val1, pu8Val2);
}

/*****************************************************************************/
/* Function                  : PtpUtil8BytesSubtraction                      */
/*                                                                           */
/* Description               : This routine used to perform 8 Byte           */
/*                             Subtraction depending on the sign values      */
/*                             Formula is A - B. Here A & B can have any sign*/
/*                                                                           */
/* Input                     : tPtpCurrentDs - pPtpCurrentDs                 */
/*                                                                           */
/* Output                    :                                               */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   :                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUtil8BytesSubtraction (tPtpCurrentDs * pPtpCurrentDs)
{
    INT1                i1Sign = 0;

    /* Forumula is: Result = A - B 
     * If A is +ve and B is +ve do A - B. Result sign is +ve if A is big else -ve.
     * If A is +ve and B is -ve do A + B. Result sign is +ve.
     * If A is -ve ans B is +ve do A + B. Result sign is -ve.
     * If A is -ve and B is -ve do B - A. Result sign is +ve if B is big else -ve.
     */
    if ((pPtpCurrentDs->i1OffsetSign == PTP_NEGATIVE) &
        (pPtpCurrentDs->i1DelaySign == PTP_NEGATIVE))
    {
        PtpClkPerf8ByteSubtraction (&(pPtpCurrentDs->u8MeanPathDelay),
                                    &(pPtpCurrentDs->u8OffsetFromMaster),
                                    &(pPtpCurrentDs->u8OffsetFromMaster),
                                    &i1Sign);
        pPtpCurrentDs->i1OffsetSign = i1Sign;
    }

    else if ((pPtpCurrentDs->i1OffsetSign == PTP_POSITIVE) &
             (pPtpCurrentDs->i1DelaySign == PTP_POSITIVE))
    {
        PtpClkPerf8ByteSubtraction (&(pPtpCurrentDs->u8OffsetFromMaster),
                                    &(pPtpCurrentDs->u8MeanPathDelay),
                                    &(pPtpCurrentDs->u8OffsetFromMaster),
                                    &i1Sign);
        pPtpCurrentDs->i1OffsetSign = i1Sign;
    }

    else if ((pPtpCurrentDs->i1OffsetSign == PTP_NEGATIVE) &
             (pPtpCurrentDs->i1DelaySign == PTP_POSITIVE))
    {
        FSAP_U8_ADD (&(pPtpCurrentDs->u8OffsetFromMaster),
                     &(pPtpCurrentDs->u8OffsetFromMaster),
                     &(pPtpCurrentDs->u8MeanPathDelay));
        pPtpCurrentDs->i1OffsetSign = PTP_NEGATIVE;
    }

    else
    {
        FSAP_U8_ADD (&(pPtpCurrentDs->u8OffsetFromMaster),
                     &(pPtpCurrentDs->u8OffsetFromMaster),
                     &(pPtpCurrentDs->u8MeanPathDelay));
        pPtpCurrentDs->i1OffsetSign = PTP_POSITIVE;
    }
}

/*****************************************************************************/
/* Function                  : PtpUtil8BytesAddition                         */
/*                                                                           */
/* Description               : This routine used to perform 8 Byte           */
/*                             Addition depending on the sign values         */
/*                                                                           */
/* Input                     : tPtpCurrentDs - pPtpCurrentDs                 */
/*                                                                           */
/* Output                    :                                               */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   :                                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
PtpUtil8BytesAddition (FS_UINT8 * pu8Result, FS_UINT8 pu8Val1, FS_UINT8 pu8Val2,
                       INT1 i1Sign1, INT1 i1Sign2, INT1 *i1DelaySign)
{

    /* Forumula is: Result = A + B
     * If A is +ve and B is +ve do A + B. Result sign is +ve.
     * If A is +ve and B is -ve do A - B. Result sign is +ve if A is big else -ve.
     * If A is -ve ans B is +ve do B - A. Result sign is +ve if B is big else -ve.
     * If A is -ve and B is -ve do A + B. Result sign is -ve.
     * */

    if ((i1Sign1 == PTP_POSITIVE) & (i1Sign2 == PTP_POSITIVE))
    {
        FSAP_U8_ADD (pu8Result, &pu8Val1, &pu8Val2);
        i1DelaySign = (INT1 *) PTP_POSITIVE;
    }

    else if ((i1Sign1 == PTP_NEGATIVE) & (i1Sign2 == PTP_NEGATIVE))
    {
        FSAP_U8_ADD ((pu8Result), &pu8Val1, &pu8Val2);
        i1DelaySign = (INT1 *) PTP_NEGATIVE;
    }

    else if ((i1Sign1 == PTP_POSITIVE) & (i1Sign2 == PTP_NEGATIVE))
    {
        PtpClkPerf8ByteSubtraction (&(pu8Val1), &(pu8Val2),
                                    (pu8Result), i1DelaySign);
    }
    else
    {
        PtpClkPerf8ByteSubtraction (&(pu8Val2), &(pu8Val1),
                                    (pu8Result), i1DelaySign);
    }
}

/************************************************************************
 *  Function Name   : PtpUtilFixedPtAdd
 *  Description     : Adds two fixedpt numbers.
 *  Input           : pu8Val1 , pu8Val2 - Numbers to be added.
 *                    It is assumed that the decimal value is converted 
 *                    into fixed point numbers and stored the whole number 
 *                    in first 48 bits and fractional part in the least
 *                    16 bits.
 *  Output          : pu8Result - The result shall also be a fixed point
 *                    number and whole and fractional part is stored as same 
 *                    as inputs.
 *  Returns         : None.
 ************************************************************************/

PUBLIC VOID
PtpUtilFixedPtAdd (FS_UINT8 * pu8Result, FS_UINT8 * pu8Val1, FS_UINT8 * pu8Val2)
{
    FP_UINT8            fpu8Val1 = 0;
    FP_UINT8            fpu8Val2 = 0;
    FP_UINT8            fpu8Result = 0;

    fpu8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    fpu8Val1 = fpu8Val1 << 32;
    fpu8Val1 = fpu8Val1 | pu8Val1->u4Lo;

    fpu8Val2 = 0x00000000000000000000000000000000 | pu8Val2->u4Hi;
    fpu8Val2 = fpu8Val2 << 32;
    fpu8Val2 = fpu8Val2 | pu8Val2->u4Lo;

    fpu8Result = fpu8Val1 + fpu8Val2;

    /* Converting the FP_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) fpu8Result;
    pu8Result->u4Hi = fpu8Result >> 32;
}

/************************************************************************
 *  Function Name   : PtpUtilFixedPtSub
 *  Description     : Subracts two fixedpt numbers.
 *  Input           : pu8Val1 , pu8Val2 - Numbers to be subracted.
 *                    It is assumed that the decimal value is converted 
 *                    into fixed point numbers and stored the whole number 
 *                    in first 48 bits and fractional part in the least
 *                    16 bits.
 *  Output          : pu8Result - The result shall also be a fixed point
 *                    number and whole and fractional part is stored as same 
 *                    as inputs.
 *  Returns         : None.
 ************************************************************************/

PUBLIC VOID
PtpUtilFixedPtSub (FS_UINT8 * pu8Result, INT1 *pi1Sign, FS_UINT8 * pu8Val1,
                   FS_UINT8 * pu8Val2)
{
    FP_UINT8            fpu8Val1 = 0;
    FP_UINT8            fpu8Val2 = 0;
    FP_UINT8            fpu8Result = 0;

    fpu8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    fpu8Val1 = fpu8Val1 << 32;
    fpu8Val1 = fpu8Val1 | pu8Val1->u4Lo;

    fpu8Val2 = 0x00000000000000000000000000000000 | pu8Val2->u4Hi;
    fpu8Val2 = fpu8Val2 << 32;
    fpu8Val2 = fpu8Val2 | pu8Val2->u4Lo;

    if (fpu8Val1 >= fpu8Val2)
    {
        fpu8Result = fpu8Val1 - fpu8Val2;
        *pi1Sign = PTP_POSITIVE;
    }
    else
    {
        fpu8Result = fpu8Val2 - fpu8Val1;
        *pi1Sign = PTP_NEGATIVE;
    }

    /* Converting the FP_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) fpu8Result;
    pu8Result->u4Hi = fpu8Result >> 32;
}

/************************************************************************
 *  Function Name   : PtpUtilFixedPtMul
 *  Description     : Multiplies two fixedpt numbers.
 *  Input           : pu8Val1 , pu8Val2 - Multiplicand and multiplier.
 *                    It is assumed that the decimal value is converted 
 *                    into fixed point numbers and stored the whole number 
 *                    in first 48 bits and fractional part in the least
 *                    16 bits.
 *  Output          : pu8Result - The result shall also be a fixed point
 *                    number and whole and fractional part is stored as same 
 *                    as inputs.
 *  Returns         : None.
 ************************************************************************/

PUBLIC              BOOL1
PtpUtilFixedPtMul (FS_UINT8 * pu8Result, FS_UINT8 * pu8Val1, FS_UINT8 * pu8Val2)
{
    FP_UINT8            fpu8Val1 = 0;
    FP_UINT8            fpu8Val2 = 0;
    FP_UINT8            fpu8Result = 0;

    fpu8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    fpu8Val1 = fpu8Val1 << 32;
    fpu8Val1 = fpu8Val1 | pu8Val1->u4Lo;

    fpu8Val2 = 0x00000000000000000000000000000000 | pu8Val2->u4Hi;
    fpu8Val2 = fpu8Val2 << 32;
    fpu8Val2 = fpu8Val2 | pu8Val2->u4Lo;

    if ((fpu8Val1 >= ((FP_UINT8) pow (2, PTP_FIXEDPT_WBITS))) ||
        (fpu8Val2 >= ((FP_UINT8) pow (2, PTP_FIXEDPT_WBITS))))
    {
        return OSIX_FALSE;
    }
    fpu8Result = ((fpu8Val1 * fpu8Val2) >> PTP_FIXEDPT_FBITS);

    /* Converting the AR_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) fpu8Result;
    pu8Result->u4Hi = fpu8Result >> 32;
    return OSIX_TRUE;
}

/************************************************************************
 *  Function Name   : PtpUtilFixedPtDiv
 *  Description     : Divide two fixedpt numbers.
 *  Input           : pu8Val1 , pu8Val2 - Dividend and divider.
 *                    It is assumed that the decimal value is converted 
 *                    into fixed point numbers and stored the whole number 
 *                    in first 48 bits and fractional part in the least
 *                    16 bits.
 *  Output          : pu8Result - The result shall also be a fixed point
 *                    number and whole and fractional part is stored as same 
 *                    as inputs.
 *  Returns         : None.
 ************************************************************************/

PUBLIC              BOOL1
PtpUtilFixedPtDiv (FS_UINT8 * pu8Result, FS_UINT8 * pu8Val1, FS_UINT8 * pu8Val2)
{
    FP_UINT8            fpu8Val1 = 0;
    FP_UINT8            fpu8Val2 = 0;
    FP_UINT8            fpu8Result = 0;

    fpu8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    fpu8Val1 = fpu8Val1 << 32;
    fpu8Val1 = fpu8Val1 | pu8Val1->u4Lo;

    fpu8Val2 = 0x00000000000000000000000000000000 | pu8Val2->u4Hi;
    fpu8Val2 = fpu8Val2 << 32;
    fpu8Val2 = fpu8Val2 | pu8Val2->u4Lo;

    if ((fpu8Val1 >= ((FP_UINT8) pow (2, PTP_FIXEDPT_WBITS))) ||
        (fpu8Val2 >= ((FP_UINT8) pow (2, PTP_FIXEDPT_WBITS))))
    {
        return OSIX_FALSE;
    }
    fpu8Result = ((fpu8Val1 << PTP_FIXEDPT_FBITS) / fpu8Val2);

    /* Converting the FP_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) fpu8Result;
    pu8Result->u4Hi = fpu8Result >> 32;
    return OSIX_TRUE;
}

/************************************************************************
 *  Function Name   : PtpConvertToFixedPt
 *  Description     : Converts the whole number to fixed point number.
 *  Input           : pu8Val1 - Number to be converted.
 *  Output          : pu8Result - Fixed point number.
 *  Returns         : None.
 ************************************************************************/
PUBLIC              BOOL1
PtpConvertToFixedPt (FS_UINT8 * pu8Result, FS_UINT8 * pu8Val1)
{
    FP_UINT8            fpu8Val1 = 0;
    FP_UINT8            fpu8Result = 0;

    fpu8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    fpu8Val1 = fpu8Val1 << 32;
    fpu8Val1 = fpu8Val1 | pu8Val1->u4Lo;

    if (fpu8Val1 >= ((FP_UINT8) pow (2, PTP_FIXEDPT_WBITS)))
    {
        return OSIX_FALSE;
    }
    fpu8Result = fpu8Val1 << PTP_FIXEDPT_FBITS;

    /* Converting the FP_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) fpu8Result;
    pu8Result->u4Hi = fpu8Result >> 32;
    return OSIX_TRUE;
}

/************************************************************************
 *  Function Name   : PtpConvertToFixedPt
 *  Description     : Converts the whole number to fixed point number.
 *  Input           : pu8Val1 - Number to be converted.
 *  Output          : pu8Result - Fixed point number.
 *  Returns         : None.
 ************************************************************************/
PUBLIC VOID
PtpConvertFixedPtToInt (FS_UINT8 * pu8Result, FS_UINT8 * pu8Val1)
{
    FP_UINT8            fpu8Val1 = 0;
    FP_UINT8            fpu8Result = 0;

    fpu8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    fpu8Val1 = fpu8Val1 << 32;
    fpu8Val1 = fpu8Val1 | pu8Val1->u4Lo;

    fpu8Result = fpu8Val1 >> PTP_FIXEDPT_FBITS;

    /* Converting the FP_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) fpu8Result;
    pu8Result->u4Hi = fpu8Result >> 32;
}

/*************************************************************************
 *  Function Name   : PtpGetDeviceTypeFromIfIndex
 *  Description     : This function return the device type for PW.
 *  Input           : pBuf - pointer of chain buffer
 *                    u4ContextId - Context Id
 *                    u4IfIndex - Interface Index
 *                    VlanId - Vlan Id
 *  Output          : tPtpPort - ptp port
 *                    pu1Flag - device type
 *  Returns         : None.
 ************************************************************************/
PUBLIC tPtpPort    *
PtpGetDeviceTypeFromIfIndex (tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4ContextId, UINT4 u4IfIndex,
                             tVlanId VlanId, UINT1 *pu1Flag)
{
    UINT1               u1DomainId = 0;
    INT1                ai1SubDomainId[PTP_SUBDOMAIN_NAME_LENGTH];
    UINT4               u4ptpVersion = 0;
    tPtpPort           *pPtpPort = NULL;

    *pu1Flag = PTP_IFACE_VLAN;

    MEMSET (ai1SubDomainId, 0, PTP_SUBDOMAIN_NAME_LENGTH);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4ptpVersion,
                               (PTP_LLC_HDR_SIZE + PTP_MSG_VERSION_OFFSET),
                               PTP_VERSION_TWO);
    if (u4ptpVersion == PTP_VERSION_ONE)
    {

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1SubDomainId,
                                   (PTP_LLC_HDR_SIZE + PTP_MSG_DOMAIN_OFFSET),
                                   PTP_SUBDOMAIN_NAME_LENGTH);
        /* Find the SubDomain and Get v2 Domain Mapping */
        if ((PtpCmUtlv1SubDomainTov2Domain (ai1SubDomainId, &(u1DomainId)))
            != OSIX_SUCCESS)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpGetDeviceTypeFromIfIndex : "
                      "PtpCmUtlv1SubDomainTov2Domain failed.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_SUCCESS);
        }
    }
    else
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1DomainId,
                                   (PTP_LLC_HDR_SIZE + PTP_MSG_DOMAIN_OFFSET),
                                   PTP_DOMAIN_LEN_SIZE);
    }

    pPtpPort = PtpIfGetPortFromIfTypeAndIndex (u4ContextId, u1DomainId,
                                               VlanId, PTP_IFACE_VLAN);
    if (pPtpPort == NULL)
    {
        pPtpPort = PtpIfGetPortFromIfTypeAndIndex (u4ContextId,
                                                   u1DomainId,
                                                   u4IfIndex,
                                                   PTP_IFACE_IEEE_802_3);
        *pu1Flag = PTP_IFACE_IEEE_802_3;
    }

    return pPtpPort;
}

#ifdef L2RED_WANTED
/*************************************************************************
 *  Function Name   : PtpProcessValidPtpPktForRm
 *  Description     : This fuction process the packet and retrun success if
 *                    packet need to forword to RM else returm failure.
 *  Input           : pBuf - pointer of chain buffer
 *             
 *  Output          : NONE
 *                    
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
PtpProcessValidPtpPktForRm (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4OffSet = PTP_LLC_HDR_SIZE;
    UINT1               u1MsgType = 0;
    UINT1               u1ClkIdentity = 0;

    PTP_RM_GET_1_BYTE (pBuf, &u4OffSet, u1MsgType);

    if ((u1MsgType == PTP_SYNC_MSG) ||
        (u1MsgType == PTP_ANNC_MSG) ||
        (u1MsgType == PTP_FOLLOW_UP_MSG) ||
        (u1MsgType == PTP_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_REQ_MSG))
    {
        return OSIX_SUCCESS;
    }

    if ((u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG) ||
        (u1MsgType == PTP_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_RESP_MSG))
    {

        u4OffSet = PTP_RED_CLK_ID_FIFTH_LOC_CHECK_AT_ACTIVE;
        PTP_RM_GET_1_BYTE (pBuf, &u4OffSet, u1ClkIdentity);
        /*Need to check clock identity and send the packet */
        if (u1ClkIdentity == PTP_RED_CLK_ID_STANDBY_FIFTH_VAL)
        {
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}
#endif /*_L2RED_WANTED_*/
#endif /* _PTPUTIL_C_ */
