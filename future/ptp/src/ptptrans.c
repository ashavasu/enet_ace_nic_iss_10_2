/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptptrans.c,v 1.10 2014/02/24 11:39:34 siva Exp $
 *
 * Description: This file contains PTP Transparent clock implementation.     
 *********************************************************************/
#ifndef _PTPTRANS_C_
#define _PTPTRANS_C_

#include "ptpincs.h"

PRIVATE VOID        PtpTransSendFollowUp (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                          FS_UINT8 * pu8Latency,
                                          UINT4 u4PktLen, UINT1 u1MsgType);

PRIVATE VOID        PtpTransUpdtTwoStepFlag (UINT1 *pu1Pdu,
                                             tPtpPort * pPtpPort);

PRIVATE VOID        PtpTransHandleSyncMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                           tClkSysTimeInfo * pIngressSyncTime);
#define PTP_PACKET_OFFSET 14
/*****************************************************************************/
/* Function                  : PtpTransHandleRcvdPtpPkt                      */
/*                                                                           */
/* Description               : This routine handles the received PDUs when   */
/*                             the clock is operating as transparent clock.  */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             u4PktLen - Length of the received PDU         */
/*                             u1MsgType- Type of the message.               */
/*                             pEventTimeStamp - Ingress time stamp of the   */
/*                                               message.                    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpTransHandleRcvdPtpPkt (UINT1 *pu1Pdu, tPtpPort * pPtpPort, UINT4 u4PktLen,
                          UINT1 u1MsgType, tFsClkTimeVal * pEventTimeStamp)
{
    UINT1              *pu1TmpPdu = 0;
    UINT1              *pu1PtpPdu = pu1Pdu + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    tClkSysTimeInfo     PtpSysTimeInfo;
    FS_UINT8            u8CorrectionField;
    FS_UINT8            u8Latency;
    UINT4               u4Val = 0;
    tPtpHdrInfo         PtpHdrInfo;
    PTP_FN_ENTRY ();

    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));
    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    FSAP_U8_CLR (&u8CorrectionField);
    FSAP_U8_CLR (&u8Latency);

    PtpUtilExtractHdrFromMsg (pu1PtpPdu, &PtpHdrInfo);

    if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TRANS_TIMESTAMPING)
    {
        /* Hardware will take care of residence time calculation
         * */
        PTP_FN_EXIT ();
        return;
    }

    switch (PtpHdrInfo.u1MsgType)
    {
        case PTP_SYNC_MSG:
            pPtpPort->PtpPortStats.u4RcvdSyncMsgCnt++;
            break;
        case PTP_DELAY_REQ_MSG:
            pPtpPort->PtpPortStats.u4RcvdDelayReqMsgCnt++;
            break;
        case PTP_PEER_DELAY_REQ_MSG:
            pPtpPort->PtpPortStats.u4RcvdPeerDelayReqMsgCnt++;
            break;
        case PTP_PEER_DELAY_RESP_MSG:
            pPtpPort->PtpPortStats.u4RcvdPeerDelayRespMsgCnt++;
            break;
        case PTP_FOLLOW_UP_MSG:
            pPtpPort->PtpPortStats.u4RcvdFollowUpMsgCnt++;
            break;
        case PTP_DELAY_RESP_MSG:
            pPtpPort->PtpPortStats.u4RcvdDelayRespMsgCnt++;
            break;
        case PTP_PDELAY_RESP_FOLLOWUP_MSG:
            pPtpPort->PtpPortStats.u4RcvdPeerDelayRespFollowUpMsgCnt++;
            break;
        case PTP_ANNC_MSG:
            pPtpPort->PtpPortStats.u4RcvdAnnounceMsgCnt++;
            break;
        default:
            break;
    }

    if ((PtpHdrInfo.u1MsgType == PTP_SYNC_MSG) || (PtpHdrInfo.u1MsgType ==
                                                   PTP_FOLLOW_UP_MSG))
    {
        MEMCPY (&(PtpSysTimeInfo.FsClkTimeVal), pEventTimeStamp,
                sizeof (tFsClkTimeVal));
        PtpSysTimeInfo.u4ContextId = pPtpPort->u4ContextId;
        PtpSysTimeInfo.u1DomainId = pPtpPort->u1DomainId;
        PtpTransHandleSyncMsg (pu1Pdu, pPtpPort, &PtpSysTimeInfo);
    }
    /* When a clock is operating in transparent mode, it has to update the
     * correction field for the event messages alone. And after that it
     * has to forward the received message to all other ports present in
     * the domain 
     * */
    if (pPtpPort->pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag == PTP_ENABLED)
    {
        /* The clock is Two step clock. Hence set the flag as two step for
         * sync and peer delay response messages. Follow up will be transmitted
         * after processing this PDU. 
         * Refer Section 11.5 of IEEE Std 1588 2008
         * */
        if ((PtpHdrInfo.u1MsgType == PTP_SYNC_MSG)
            || (PtpHdrInfo.u1MsgType == PTP_PEER_DELAY_RESP_MSG))
        {
            PtpTransUpdtTwoStepFlag (pu1Pdu, pPtpPort);
        }
    }

    switch (PtpHdrInfo.u1MsgType)
    {
        case PTP_DELAY_RESP_MSG:
            if (pPtpPort->pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag !=
                PTP_ENABLED)
            {
                /* Clock is one-step clock and message is delay response no
                 * change should be done in the message
                 * */
                break;
            }
            /* Intentional Fall through */
        case PTP_SYNC_MSG:
        case PTP_DELAY_REQ_MSG:
        case PTP_PEER_DELAY_REQ_MSG:
        case PTP_PEER_DELAY_RESP_MSG:
            /* Intentional fall through for all the Event messages supported
             * by PTP. Has to operate upon the time stamp */

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Received Event message through "
                      "Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
            /* Egress Event time stamp will be added when we actually 
             * transmit the packet out as part of PTP TX Sub Module.
             * Hence Subtracting the time stamp here. Addition of
             * Egress time stamp will happed in PTP TX Sub module.
             * */
            pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET;
            PTP_LBUF_GET_4_BYTES (pu1TmpPdu, u4Val);
            UINT8_HI (&(u8CorrectionField)) = u4Val;

            u4Val = 0;
            PTP_LBUF_GET_4_BYTES (pu1TmpPdu, u4Val);
            UINT8_LO (&(u8CorrectionField)) = u4Val;

            /* In case the port follows Peer Delay mechanism, update the
             * Link Delay */
            if (pPtpPort->PortDs.u1DelayMechanism ==
                PTP_PORT_DELAY_MECH_PEER_TO_PEER)
            {
                /* The peer mean path delay will be in nanoseconds. */
                PtpClkCalcScalNanoSecs (pPtpPort->PortDs.
                                        u8PeerMeanPathDelay, &u8Latency);
                FSAP_U8_ADD (&u8CorrectionField, &u8CorrectionField,
                             &u8Latency);

                FSAP_U8_CLR (&u8Latency);
            }
            /* Update the correction field in the packet */
            pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET;
            PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrectionField.u4Hi);

            pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET +
                PTP_MSG_CORRECTION_FIELD_LOWER_BYTE_OFFSET;
            PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrectionField.u4Lo);
            break;

        default:
            /* Nothing needs to be done */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Message is a general message "
                      "or a Follow up message...\r\n"));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "No Action performed in Transparent clock.....\r\n"));
            break;
    }

    /* Transmitting packet over all other member ports of the domain
     * */
    if (PtpTxForwardPktOnDmnPorts (pu1Pdu, pPtpPort, u4PktLen,
                                   PtpHdrInfo.u1MsgType,
                                   pEventTimeStamp) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Unable to forward packets on all other "
                  "ports in the domain.\r\n"));
    }

    if ((PtpHdrInfo.u1MsgType != PTP_SYNC_MSG) && (PtpHdrInfo.u1MsgType !=
                                                   PTP_PEER_DELAY_RESP_MSG))
    {
        /* Follow up needs to be transmitted for these two messages only.
         * */
        PTP_FN_EXIT ();
        return;
    }
    if ((PtpHdrInfo.u2Flags & PTP_TWO_STEP_CLK_MASK) != PTP_TWO_STEP_CLK_MASK)
    {
        /* When a transparent clock is configured as Two Step clock and receives
         * one step sync message or a Peer Delay response message, then this 
         * clock has to generate the follow up message. Refer Section 11.5 of
         * IEEE Std 1588 2008 for more details 
         * */
        PtpTransSendFollowUp (pu1Pdu, pPtpPort, &u8Latency, u4PktLen,
                              PtpHdrInfo.u1MsgType);
    }
    UNUSED_PARAM (u1MsgType);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTransUpdtTwoStepFlag                       */
/*                                                                           */
/* Description               : This routine updates the Two step flag as true*/
/*                             in the PDU. This PDU will be forwarded by the */
/*                             Transparent clock. Then transparent clock will*/
/*                             generate Follow up for the same.              */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTransUpdtTwoStepFlag (UINT1 *pu1Pdu, tPtpPort * pPtpPort)
{
    tPtpHdrInfo         PtpHdrInfo;
    UINT1              *pu1Tmp = pu1Pdu + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    UINT2               u2Flags = 0;

    PTP_FN_ENTRY ();
    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));

    PtpUtilExtractHdrFromMsg (pu1Tmp, &PtpHdrInfo);

    u2Flags = PtpHdrInfo.u2Flags & PTP_TWO_STEP_CLK_MASK;

    if (u2Flags != 0)
    {
        /* The received message itself indicates that a follow up will be 
         * received. Hence nothing needs to be done
         * */
        PTP_FN_EXIT ();
        return;
    }

    /* Two step flag is not set in the received message. Update the same
     * */
    u2Flags = PtpHdrInfo.u2Flags;
    PTP_SET_TWO_STEP_FLAG (u2Flags);

    pu1Tmp = pu1Pdu + PTP_MSG_FLAGS_OFFSET + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    PTP_LBUF_PUT_2_BYTES (pu1Tmp, u2Flags);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTransSendFollowUp                          */
/*                                                                           */
/* Description               : This routine triggers the Follow up for the   */
/*                             Sync and peer delay response messages when the*/
/*                             Transparent clock is configured as a two step */
/*                             clock.                                        */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointe to the received port        */
/*                             pu8Latency - Observed Latency.                */
/*                             u4PktLen - Length of the packet.              */
/*                             u1MsgType- Message Type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTransSendFollowUp (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                      FS_UINT8 * pu8Latency, UINT4 u4PktLen, UINT1 u1MsgType)
{
    UINT1              *pu1Tmp = pu1Pdu;
    UINT1               u1Val = 0;
    FS_UINT8            u8CorrectionField;
    FS_UINT8            u8CorrnFieldInNanoSec;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8CorrectionField);
    FSAP_U8_CLR (&u8CorrnFieldInNanoSec);

    if (pPtpPort->pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag == PTP_DISABLED)
    {
        PTP_FN_EXIT ();
        return;
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Sync or Peer Delay response received on a two step "
              "Transparent clock....\r\n"));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Generating Follow up for the message....\r\n"));

    if (u1MsgType == PTP_SYNC_MSG)
    {
        /* Configuring control field as Follow up */
        u1Val = PTP_CONTROL_FIELD_FOLLOW_UP;
        pu1Tmp = pu1Pdu + PTP_CONTROL_FIELD_OFFSET +
            PTP_GET_MEDIA_HDR_LEN (pPtpPort);
        PTP_LBUF_PUT_1_BYTE (pu1Tmp, u1Val);
    }
    else
    {
        /* Configuring control field as Follow up */
        u1Val = PTP_CONTROL_FIELD_OTHERS;
        pu1Tmp = pu1Pdu + PTP_CONTROL_FIELD_OFFSET +
            PTP_GET_MEDIA_HDR_LEN (pPtpPort);
        PTP_LBUF_PUT_1_BYTE (pu1Tmp, u1Val);
    }

    pu1Tmp = pu1Pdu + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    MEMCPY (&(u8CorrectionField.u4Hi),
            pu1Tmp + PTP_MSG_CORRECTION_FIELD_OFFSET, sizeof (UINT4));

    u8CorrectionField.u4Hi = (UINT4) OSIX_NTOHL (u8CorrectionField.u4Hi);

    MEMCPY (&(u8CorrectionField.u4Lo),
            pu1Tmp + PTP_MSG_CORRECTION_FIELD_OFFSET +
            PTP_MSG_CORRECTION_FIELD_LOWER_BYTE_OFFSET, sizeof (UINT4));

    u8CorrectionField.u4Lo = (UINT4) OSIX_NTOHL (u8CorrectionField.u4Lo);

    /* Update the correction field */
    FSAP_U8_ADD (&u8CorrectionField, &u8CorrectionField, pu8Latency);

    /* Update the correction field in the packet */
    pu1Tmp = pu1Pdu + PTP_MSG_CORRECTION_FIELD_OFFSET +
        PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    PTP_LBUF_PUT_4_BYTES (pu1Tmp, u8CorrectionField.u4Hi);

    pu1Tmp = pu1Pdu + PTP_MSG_CORRECTION_FIELD_OFFSET +
        PTP_MSG_CORRECTION_FIELD_LOWER_BYTE_OFFSET +
        PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    PTP_LBUF_PUT_4_BYTES (pu1Tmp, u8CorrectionField.u4Lo);

    /* Transmitting packet over all other member ports of the domain
     * */
    if (u1MsgType == PTP_SYNC_MSG)
    {
        u1MsgType = (UINT1) PTP_FOLLOW_UP_MSG;
    }
    else if (u1MsgType == PTP_PEER_DELAY_RESP_MSG)
    {
        u1MsgType = (UINT1) PTP_PDELAY_RESP_FOLLOWUP_MSG;
    }
    pu1Tmp = pu1Pdu + PTP_MSG_TYPE_OFFSET + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    PTP_LBUF_PUT_1_BYTE (pu1Tmp, u1MsgType);

    if (PtpTxForwardPktOnDmnPorts (pu1Pdu, pPtpPort, u4PktLen,
                                   u1MsgType, NULL) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Unable to forward packets on all other "
                  "ports in the domain.\r\n"));
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTransHandleSyncMsg                         */
/*                                                                           */
/* Description               : This routine is used to handle the sync msg   */
/*                             received over the transparent clock. For      */
/*                             transparent clocks, it is necessary to calc   */
/*                             the syntonization ratio.                      */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointe to the received port        */
/*                             pIngressSyncTime - PDU ingress Time           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTransHandleSyncMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                       tClkSysTimeInfo * pIngressSyncTime)
{
    tPtpHdrInfo         PtpHdrInfo;
    tFsClkTimeVal       OriginTimeStamp;
    tFsClkTimeVal       FsClkDiff;
    tPtpCurrentDs      *pPtpCurrentDs = NULL;
    FS_UINT8           *pu8CorrnField = NULL;
    FS_UINT8            u8NanoSecValue;
    UINT1              *pu1Tmp = NULL;
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));
    MEMSET (&OriginTimeStamp, 0, sizeof (tFsClkTimeVal));
    MEMSET (&FsClkDiff, 0, sizeof (tFsClkTimeVal));
    FSAP_U8_CLR (&u8NanoSecValue);

    UINT8_LO (&(u8NanoSecValue)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
    pPtpCurrentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs);

    pu1Tmp = pu1Pdu + PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    PtpUtilExtractHdrFromMsg (pu1Tmp, &PtpHdrInfo);
    if ((PtpHdrInfo.u2Flags & PTP_TWO_STEP_CLK_MASK) == PTP_TWO_STEP_CLK_MASK)
    {
        /* This is a sync message that indicates FollowUp will be received
         * Nothing needs to be done here
         * */
        PTP_FN_EXIT ();
        return;
    }

    /* <offsetFromMaster> = <syncEventIngressTimeStamp> - <originTimeStamp>
     *                      - <meanPathDelay> - correctionField
     * */
    /* Update the sync ingress time */
    if (PtpHdrInfo.u1MsgType == PTP_SYNC_MSG)
    {
        MEMCPY (&(pPtpPort->SyncReceptTime),
                &(pIngressSyncTime->FsClkTimeVal), sizeof (tFsClkTimeVal));

    }
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Sync message received from "
              "one step clock.\r\n"));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Calculating Offset From Master...\r\n"));

    /* Obtaining Sync Origin Time stamp */
    pu1Tmp = pu1Pdu + PTP_SYNC_TIME_STAMP_OFFSET +
        PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    PTP_LBUF_GET_2_BYTES (pu1Tmp, u2Val);
    UINT8_HI (&(OriginTimeStamp.u8Sec)) = (UINT4) u2Val;

    PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
    UINT8_LO (&(OriginTimeStamp.u8Sec)) = u4Val;

    u4Val = 0;
    PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
    OriginTimeStamp.u4Nsec = u4Val;

    FSAP_U8_ASSIGN (&(pPtpPort->IngressSyncTimeStamp.u8Sec),
                    &(OriginTimeStamp.u8Sec));
    pPtpPort->IngressSyncTimeStamp.u4Nsec = OriginTimeStamp.u4Nsec;

    /* <syncEventIngressTimeStamp> - <originTimeStamp> */
    PtpClkSubtractTimeStamps (&(pPtpPort->SyncReceptTime),
                              &(OriginTimeStamp),
                              &(FsClkDiff), &(pPtpCurrentDs->i1OffsetSign));
    FSAP_U8_MUL (&(pPtpCurrentDs->u8OffsetFromMaster), &(FsClkDiff.u8Sec),
                 &(u8NanoSecValue));
    UINT8_LO (&(pPtpCurrentDs->u8OffsetFromMaster)) =
        UINT8_LO (&(pPtpCurrentDs->u8OffsetFromMaster)) + (FsClkDiff.u4Nsec);

    /* Subtracting the correction field */
    if (pPtpCurrentDs->i1OffsetSign == PTP_NEGATIVE)
    {
        FSAP_U8_ADD (&(pPtpCurrentDs->u8OffsetFromMaster),
                     &(pPtpCurrentDs->u8OffsetFromMaster),
                     &(PtpHdrInfo.u8CorrectionField));
    }
    else
    {
        PtpClkPerf8ByteSubtraction (&(pPtpCurrentDs->u8OffsetFromMaster),
                                    &(PtpHdrInfo.u8CorrectionField),
                                    &(pPtpCurrentDs->u8OffsetFromMaster),
                                    &(pPtpCurrentDs->i1OffsetSign));
    }

    /* Subtracting the mean path delay. Delay Request mechanism is not
     * applicable in case of Transparent clocks. Hence use only the 
     * P2P delay value. */
    if (pPtpPort->PortDs.u1DelayMechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER)
    {
        /* Clock supports Peer to Peer delay mechanism. Use Peer mean
         * Path delay for calculation.
         * Refer (c) of Section 11.2 of IEEE 1588 2008
         * */
        if (pPtpCurrentDs->i1OffsetSign == PTP_NEGATIVE)
        {
            FSAP_U8_ADD (&(pPtpCurrentDs->u8OffsetFromMaster),
                         &(pPtpCurrentDs->u8OffsetFromMaster),
                         &(pPtpPort->PortDs.u8PeerMeanPathDelay));
        }
        else
        {
            pPtpCurrentDs->i1DelaySign = pPtpPort->PortDs.i1PeerDelaySign;
            FSAP_U8_ASSIGN (&(pPtpCurrentDs->u8MeanPathDelay),
                            &(pPtpPort->PortDs.u8PeerMeanPathDelay));
            PtpUtil8BytesSubtraction (pPtpCurrentDs);
        }

    }

    PtpClkSyntonizeClk (pPtpPort, pu8CorrnField);

    PTP_FN_EXIT ();
}
#endif /* _PTPTRANS_C_ */
