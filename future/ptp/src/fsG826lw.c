/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsG826lw.c,v 1.3 2014/01/24 12:20:08 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include "ptpincs.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsG8261TsModuleStatus
 Input       :  The Indices

                The Object 
                retValFsG8261TsModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsModuleStatus (INT4 *pi4RetValFsG8261TsModuleStatus)
{
    *pi4RetValFsG8261TsModuleStatus = gDelaySystem.u4G8261ModuleStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsG8261TsModuleStatus
 Input       :  The Indices

                The Object 
                setValFsG8261TsModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsModuleStatus (INT4 i4SetValFsG8261TsModuleStatus)
{
    gDelaySystem.u4G8261ModuleStatus = i4SetValFsG8261TsModuleStatus;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsModuleStatus
 Input       :  The Indices

                The Object 
                testValFsG8261TsModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsModuleStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsG8261TsModuleStatus)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsG8261TsModuleStatus != PTP_ENABLED) &&
        (i4TestValFsG8261TsModuleStatus != PTP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsModuleStatus: "
                  "Invalid Value for Module Status!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsG8261TsModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsG8261TsModuleStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsG8261TsParamsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsG8261TsParamsTable
 Input       :  The Indices
                FsG8261TsDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsG8261TsParamsTable (INT4 i4FsG8261TsDirection)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }

    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "nmhValidateIndexInstanceFsG8261TsParamsTable: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsG8261TsParamsTable
 Input       :  The Indices
                FsG8261TsDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsG8261TsParamsTable (INT4 *pi4FsG8261TsDirection)
{
    *pi4FsG8261TsDirection = G8261TS_FORWARD;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsG8261TsParamsTable
 Input       :  The Indices
                FsG8261TsDirection
                nextFsG8261TsDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsG8261TsParamsTable (INT4 i4FsG8261TsDirection,
                                     INT4 *pi4NextFsG8261TsDirection)
{
    if (i4FsG8261TsDirection == G8261TS_REVERSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsG8261TsDirection = i4FsG8261TsDirection + 1;
        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsG8261TsSwitchCount
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsSwitchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsSwitchCount (INT4 i4FsG8261TsDirection,
                            UINT4 *pu4RetValFsG8261TsSwitchCount)
{
    *pu4RetValFsG8261TsSwitchCount =
        gDelayParams[(i4FsG8261TsDirection - 1)].u4SwitchCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsPacketSize
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsPacketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsPacketSize (INT4 i4FsG8261TsDirection,
                           UINT4 *pu4RetValFsG8261TsPacketSize)
{
    *pu4RetValFsG8261TsPacketSize =
        gDelayParams[(i4FsG8261TsDirection - 1)].u4PacketSize;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsLoad
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsLoad
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsLoad (INT4 i4FsG8261TsDirection, UINT4 *pu4RetValFsG8261TsLoad)
{
    *pu4RetValFsG8261TsLoad = gDelayParams[(i4FsG8261TsDirection - 1)].u4Load;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsNetworkDisturbance
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsNetworkDisturbance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsNetworkDisturbance (INT4 i4FsG8261TsDirection,
                                   UINT4 *pu4RetValFsG8261TsNetworkDisturbance)
{
    *pu4RetValFsG8261TsNetworkDisturbance =
        gDelayParams[(i4FsG8261TsDirection - 1)].u4NwDisturbance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsDuration
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsDuration (INT4 i4FsG8261TsDirection,
                         UINT4 *pu4RetValFsG8261TsDuration)
{
    *pu4RetValFsG8261TsDuration =
        gDelayParams[(i4FsG8261TsDirection - 1)].u4DelayDuration;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsFlowVariationFactor
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsFlowVariationFactor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsFlowVariationFactor (INT4 i4FsG8261TsDirection,
                                    INT4 *pi4RetValFsG8261TsFlowVariationFactor)
{
    *pi4RetValFsG8261TsFlowVariationFactor =
        gDelayParams[(i4FsG8261TsDirection - 1)].u1FlowVariation;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsFlowInterval
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsFlowInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsFlowInterval (INT4 i4FsG8261TsDirection,
                             UINT4 *pu4RetValFsG8261TsFlowInterval)
{
    *pu4RetValFsG8261TsFlowInterval =
        gDelayParams[(i4FsG8261TsDirection - 1)].u4FlowInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsFrequency
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                retValFsG8261TsFrequency
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsFrequency (INT4 i4FsG8261TsDirection,
                          UINT4 *pu4RetValFsG8261TsFrequency)
{
    *pu4RetValFsG8261TsFrequency =
        gDelayParams[(i4FsG8261TsDirection - 1)].u4Frequency;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsG8261TsSwitchCount
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsSwitchCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsSwitchCount (INT4 i4FsG8261TsDirection,
                            UINT4 u4SetValFsG8261TsSwitchCount)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4SwitchCount =
        u4SetValFsG8261TsSwitchCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsPacketSize
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsPacketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsPacketSize (INT4 i4FsG8261TsDirection,
                           UINT4 u4SetValFsG8261TsPacketSize)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4PacketSize =
        u4SetValFsG8261TsPacketSize;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsLoad
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsLoad
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsLoad (INT4 i4FsG8261TsDirection, UINT4 u4SetValFsG8261TsLoad)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4Load = u4SetValFsG8261TsLoad;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsNetworkDisturbance
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsNetworkDisturbance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsNetworkDisturbance (INT4 i4FsG8261TsDirection,
                                   UINT4 u4SetValFsG8261TsNetworkDisturbance)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4NwDisturbance =
        u4SetValFsG8261TsNetworkDisturbance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsDuration
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsDuration (INT4 i4FsG8261TsDirection,
                         UINT4 u4SetValFsG8261TsDuration)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4DelayDuration =
        u4SetValFsG8261TsDuration;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsFlowVariationFactor
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsFlowVariationFactor
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsFlowVariationFactor (INT4 i4FsG8261TsDirection,
                                    INT4 i4SetValFsG8261TsFlowVariationFactor)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u1FlowVariation =
        (UINT1) i4SetValFsG8261TsFlowVariationFactor;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsFlowInterval
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsFlowInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsFlowInterval (INT4 i4FsG8261TsDirection,
                             UINT4 u4SetValFsG8261TsFlowInterval)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4FlowInterval =
        u4SetValFsG8261TsFlowInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsG8261TsFrequency
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                setValFsG8261TsFrequency
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsG8261TsFrequency (INT4 i4FsG8261TsDirection,
                          UINT4 u4SetValFsG8261TsFrequency)
{
    gDelayParams[(i4FsG8261TsDirection - 1)].u4Frequency =
        u4SetValFsG8261TsFrequency;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsSwitchCount
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsSwitchCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsSwitchCount (UINT4 *pu4ErrorCode,
                               INT4 i4FsG8261TsDirection,
                               UINT4 u4TestValFsG8261TsSwitchCount)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }

    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsSwitchCount: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4TestValFsG8261TsSwitchCount > G8261TS_MAX_SWITCHCOUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_SWITCH_COUNT);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsSwitchCount: "
                  "Wrong value for switch count!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsPacketSize
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsPacketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsPacketSize (UINT4 *pu4ErrorCode,
                              INT4 i4FsG8261TsDirection,
                              UINT4 u4TestValFsG8261TsPacketSize)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsPacketSize: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4TestValFsG8261TsPacketSize < G8261TS_MIN_PKTSIZE) ||
        (u4TestValFsG8261TsPacketSize > G8261TS_MAX_PKTSIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_PKTSIZE);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsPacketSize: "
                  "Wrong value for packet size!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsLoad
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsLoad
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsLoad (UINT4 *pu4ErrorCode,
                        INT4 i4FsG8261TsDirection, UINT4 u4TestValFsG8261TsLoad)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsLoad: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4TestValFsG8261TsLoad > G8261TS_MAX_LOAD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_LOAD);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsLoad: "
                  "Wrong value for load!!!\r\n"));
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsNetworkDisturbance
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsNetworkDisturbance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsNetworkDisturbance (UINT4 *pu4ErrorCode,
                                      INT4 i4FsG8261TsDirection,
                                      UINT4
                                      u4TestValFsG8261TsNetworkDisturbance)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsNetworkDisturbance: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4TestValFsG8261TsNetworkDisturbance > G8261TS_MAX_NWDIST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_NWDIST);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsNetworkDisturbance: "
                  "Wrong value for network disturbance!!!\r\n"));
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsDuration
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsDuration (UINT4 *pu4ErrorCode,
                            INT4 i4FsG8261TsDirection,
                            UINT4 u4TestValFsG8261TsDuration)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsDuration: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if (u4TestValFsG8261TsDuration > G8261TS_MAX_DURATION)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DURATION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsDuration: "
                  "Wrong value for delay duration!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsFlowVariationFactor
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsFlowVariationFactor
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsFlowVariationFactor (UINT4 *pu4ErrorCode,
                                       INT4 i4FsG8261TsDirection,
                                       INT4
                                       i4TestValFsG8261TsFlowVariationFactor)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsFlowVariationFactor: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsG8261TsFlowVariationFactor != G8261TS_CONSTANT_FLOW) &&
        (i4TestValFsG8261TsFlowVariationFactor != G8261TS_INCREMENT) &&
        (i4TestValFsG8261TsFlowVariationFactor != G8261TS_DECREMENT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_FLOW_VARIATION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsFlowVariationFactor: "
                  "Wrong value for traffic flow variation factor!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsFlowInterval
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsFlowInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsFlowInterval (UINT4 *pu4ErrorCode,
                                INT4 i4FsG8261TsDirection,
                                UINT4 u4TestValFsG8261TsFlowInterval)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsFlowInterval: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4TestValFsG8261TsFlowInterval < G8261TS_MIN_FLOWINTERVAL) ||
        (u4TestValFsG8261TsFlowInterval > G8261TS_MAX_FLOWINTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_FLOWINTERVAL);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsFlowInterval: "
                  "Wrong value for FlowInterval!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsG8261TsFrequency
 Input       :  The Indices
                FsG8261TsDirection

                The Object 
                testValFsG8261TsFrequency
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsG8261TsFrequency (UINT4 *pu4ErrorCode,
                             INT4 i4FsG8261TsDirection,
                             UINT4 u4TestValFsG8261TsFrequency)
{
    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsG8261TsDirection != G8261TS_FORWARD) &&
        (i4FsG8261TsDirection != G8261TS_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_DIRECTION);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsFrequency: "
                  "Invalid direction specified!!!\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4TestValFsG8261TsFrequency < G8261TS_MIN_FREQUENCY) ||
        (u4TestValFsG8261TsFrequency > G8261TS_MAX_FREQUENCY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_G8261TS_INVALID_FREQUENCY);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "nmhTestv2FsG8261TsFrequency: "
                  "Wrong value for Frequency!!!\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsG8261TsParamsTable
 Input       :  The Indices
                FsG8261TsDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsG8261TsParamsTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsG8261TsMeanVariance
 Input       :  The Indices

                The Object 
                retValFsG8261TsMeanVariance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsG8261TsMeanVariance
    (tSNMP_OCTET_STRING_TYPE * pRetValFsG8261TsMeanVariance)
{
    if ((pRetValFsG8261TsMeanVariance == NULL) ||
        (pRetValFsG8261TsMeanVariance->pu1_OctetList == NULL))
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsG8261TsMeanVariance->pu1_OctetList,
            &(gDelayOutput.u8MeanVariance), sizeof (FS_UINT8));
    pRetValFsG8261TsMeanVariance->i4_Length = sizeof (FS_UINT8);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsAccuracy
 Input       :  The Indices

                The Object 
                retValFsG8261TsAccuracy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsAccuracy (INT4 *pi4RetValFsG8261TsAccuracy)
{
    *pi4RetValFsG8261TsAccuracy = gDelayOutput.u4Offset;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsG8261TsDelay
 Input       :  The Indices

                The Object 
                retValFsG8261TsDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsG8261TsDelay (tSNMP_OCTET_STRING_TYPE * pRetValFsG8261TsDelay)
{
    if ((pRetValFsG8261TsDelay == NULL) ||
        (pRetValFsG8261TsDelay->pu1_OctetList == NULL))
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsG8261TsDelay->pu1_OctetList,
            &(gDelayOutput.u8Delay), sizeof (FS_UINT8));
    pRetValFsG8261TsDelay->i4_Length = sizeof (FS_UINT8);
    return SNMP_SUCCESS;
}
