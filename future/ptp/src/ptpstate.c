/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpstate.c,v 1.3 2014/01/24 12:20:09 siva Exp $
 *
 * Description: This file contains PTP State Decision Event Algorithm
 *              Implementation. 
 *********************************************************************/
#ifndef _PTPSTATE_C_
#define _PTPSTATE_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpStateExecDecisionAlgorithm                 */
/*                                                                           */
/* Description               : This routine performs the state decision event*/
/*                             algorithm as specified in figure 26 of IEEE   */
/*                             1588 2008 standard.                           */
/*                                                                           */
/* Input                     : pPtpPort           - Pointer to Port.         */
/*                             pPtpEbestMsg       - Pointer to the selected  */
/*                                                  Ebest message.           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpStateExecDecisionAlgorithm (tPtpPort * pPtpPort,
                               tPtpForeignMasterDs * pPtpEbestMsg)
{
    tPtpDomain         *pPtpDomain = pPtpPort->pPtpDomain;
    tPtpForeignMasterDs *pEbestMsg = pPtpEbestMsg;
    tPtpForeignMasterDs PtpDefaultDs;
    INT4                i4BmcOutPut = 0;
    BOOL1               bD0Class = OSIX_FALSE;

    PTP_FN_ENTRY ();

    MEMSET (&PtpDefaultDs, 0, sizeof (tPtpForeignMasterDs));

    if ((pPtpPort->pPtpErBestMsg == NULL) &&
        (pPtpPort->PortDs.u1PortState == PTP_STATE_LISTENING))
    {
        /* Refer Figure 26 of IEEE Std 1588 2008 */
        /* A port is in LISTENING state and this port has not yet received a
         * valid announce message. Hence remain in LISTENING state waiting for
         * announce messages in the network. Here invoking SEM is avoided as 
         * this may result in re-initializing of attributes belonging to 
         * LISTENING state
         * */
        PTP_FN_EXIT ();
        return;
    }

    if (pPtpPort->pPtpErBestMsg == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port %d does not have a valid Best Message....\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port %d does not have executed BMC Algorithm...\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Not including Port %d for "
                  "running State Decision Algorithm.\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return;
    }

    if (pPtpDomain == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpStateExecDecisionAlgorithm:" "Domain Not Present.\r\n"));
        /* A port cannot have invalid back pointer */
        PTP_FN_EXIT ();
        return;
    }

    PTP_IS_CLK_CLASS_1_127 (pPtpDomain, bD0Class);
    if (bD0Class == OSIX_TRUE)
    {
        pEbestMsg = pPtpPort->pPtpErBestMsg;
    }

    /* Forming default Ds for comparision.
     * Refer Table 12 of IEEE Std 1588-2008
     * */
    PTP_FORM_DEFAULT_DS (pPtpDomain, PtpDefaultDs);

    PtpDefaultDs.pPtpPort = pPtpPort;
    /* Now compare the Default Data Set with the Ebest message */
    i4BmcOutPut = PtpBmcDataSetCmpAlgo (&PtpDefaultDs, pEbestMsg);

    switch (i4BmcOutPut)
    {
        case PTP_GREATER:
        case PTP_GREATER_BY_TOPOLOGY:

            /* Intentional fall through */
            pPtpPort->PortDs.u1RecommendedState = PTP_STATE_MASTER;
            if (bD0Class == OSIX_TRUE)
            {
                pPtpPort->PortDs.DecisionCode = M1;
            }
            else
            {
                pPtpPort->PortDs.DecisionCode = M2;
            }
            PtpBmcDsUpdtDataSetForM1andM2 (pPtpPort);
            PtpSemPortSemHandler (pPtpPort, PTP_STATE_DECISION_EVENT);
            return;

        default:

            if (bD0Class == OSIX_TRUE)
            {
                /* Default data set falls in the range of 1>D0>127
                 * */
                pPtpPort->PortDs.u1RecommendedState = PTP_STATE_PASSIVE;
                pPtpPort->PortDs.DecisionCode = P1;
                PtpBmcDsUpdtDataSetForP1AndP2 (pPtpPort);
                PtpSemPortSemHandler (pPtpPort, PTP_STATE_DECISION_EVENT);
                return;
            }
            break;
    }

    /* Default Data set class does not fall in the range of 1 to 127
     * and DefaultDS is not better than Ebest Message*/

    if (pEbestMsg == pPtpPort->pPtpErBestMsg)
    {
        /* Ebest received on this port */
        pPtpPort->PortDs.u1RecommendedState = PTP_STATE_SLAVE;
        pPtpPort->PortDs.DecisionCode = S1;
        PtpBmcDsUpdtDataSetForS1 (pPtpPort, pEbestMsg);
        PtpSemPortSemHandler (pPtpPort, PTP_STATE_DECISION_EVENT);
        PTP_FN_EXIT ();
        return;
    }

    /* Now compare the Ebest message with Erbest received over the 
     * port */
    i4BmcOutPut = PtpBmcDataSetCmpAlgo (pEbestMsg, pPtpPort->pPtpErBestMsg);
    switch (i4BmcOutPut)
    {
        case PTP_GREATER_BY_TOPOLOGY:

            /* EBest is better by topology than ErBest. This indicates that 
             * same message being received through some other port
             * */
            pPtpPort->PortDs.u1RecommendedState = PTP_STATE_PASSIVE;
            pPtpPort->PortDs.DecisionCode = P2;
            PtpBmcDsUpdtDataSetForP1AndP2 (pPtpPort);
            break;

        default:

            pPtpPort->PortDs.u1RecommendedState = PTP_STATE_MASTER;
            pPtpPort->PortDs.DecisionCode = M3;
            PtpBmcDsUpdtDataSetForM3 (pPtpPort);
            break;
    }

    PtpSemPortSemHandler (pPtpPort, PTP_STATE_DECISION_EVENT);
    PTP_FN_EXIT ();
}
#endif
