#!/bin/csh
# (C) 2006 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :Aricent					     |	
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10 May 2006                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

ECFM_BASE_DIR = ${BASE_DIR}/ecfm
ECFM_SRC_DIR  = ${ECFM_BASE_DIR}/src
ECFM_INC_DIR  = ${ECFM_BASE_DIR}/inc
ECFM_OBJ_DIR  = ${ECFM_BASE_DIR}/obj
ECFM_TST_DIR  = ${ECFM_BASE_DIR}/test

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${ECFM_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
