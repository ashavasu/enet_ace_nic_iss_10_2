/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmtstpr.c,v 1.10 2014/06/29 11:01:27 siva Exp $
 *
 * Description: This file contains the Functionality of 
 *              TST Control Sub Module.
 *******************************************************************/
#include "cfminc.h"

/*******************************************************************************
 * Function Name      : EcfmLbLtClntParseTst
 *
 * Description        : This is called to parse the received TST PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntParseTst (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRxTstPduInfo *pTstPduInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pTstPduInfo = &(pPduSmInfo->uPduInfo.Tst);
    pu1Pdu = pu1Pdu + ECFM_TST_SEQ_NUM_FIELD_SIZE + ECFM_CFM_HDR_SIZE;

    /* Copy the Test TLV */
    ECFM_GET_1BYTE (pTstPduInfo->u1TstTlvType, pu1Pdu);
    ECFM_GET_2BYTE (pTstPduInfo->u2TstTlvLength, pu1Pdu);
    ECFM_GET_1BYTE (pTstPduInfo->u1TstTlvPatternType, pu1Pdu);

    if ((pTstPduInfo->u1TstTlvPatternType ==
         ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC)
        || (pTstPduInfo->u1TstTlvPatternType ==
            ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC))
    {
        pTstPduInfo->pu1TstTlvData = pu1Pdu;
        pu1Pdu = pu1Pdu + (pTstPduInfo->u2TstTlvLength -
                           ECFM_TST_PATTERN_TYPE_FIELD_SIZE -
                           ECFM_LBLT_TEST_TLV_CRC_SIZE);

        /* Copy the CRC */
        ECFM_GET_4BYTE (pTstPduInfo->u4Checksum, pu1Pdu);
    }
    else
    {
        pTstPduInfo->pu1TstTlvData = pu1Pdu;
        pu1Pdu = pu1Pdu + (pTstPduInfo->u2TstTlvLength);
    }

    /* Validate the First TLV Offset value received in the TST PDU */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_TST_FIRST_TLV_OFFSET)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParserTst: TST frame is discarded as the"
                       " first tlv offset if wrong is\r\n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbltClntProcessTst
 *
 * Description        : This routine processes the received TST PDU
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwdTst - Pointer to Boolean indicating whether the 
 *                      TST needs to be forwarded in case the Receving entity 
 *                      is a MHF.
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbltClntProcessTst (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdTst)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    tEcfmMacAddr        MepMacAddr = { 0 };
    UINT4               u4CalChksum = ECFM_INIT_VAL;
    UINT2               u2TlvLen = ECFM_INIT_VAL;
    UINT1              *pu1StartBuf = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pMepInfo);

    pStackInfo = pPduSmInfo->pStackInfo;
    if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbltClntProcessTst: " "No Port Information \r\n");
        return ECFM_FAILURE;
    }
    /* Get the mac-addres of the MP */
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pStackInfo->u2PortNum)->u4IfIndex, MepMacAddr);

    /* Check that Tst capability should be enabled for MEP 
     * to process the TST PDU */
    if ((ECFM_LBLT_IS_MEP (pStackInfo))
        && (ECFM_LBLT_IS_TST_DISABLED (pMepInfo)))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbltClntProcessTst: "
                       "Tst is disabled, So MEP cannot process "
                       "the TST PDU further\r\n");
        return ECFM_FAILURE;
    }
    /* Check that destination_address parameter contains neither MAC address of 
     * the receiving MP nor CCM gp address then no further processing of the TST
     * is performed */
    if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                               MepMacAddr) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtClntProcessTst: "
                       "Destination address of received TST not equal to "
                       "MEPs Mac Addr, so MEP cannot process the TST PDU "
                       "further\r\n");

        /* MIP should forward the TST PDU if the MAC address doesnot match */
        if (ECFM_LBLT_IS_MHF (pStackInfo))
        {
            /* Set the Flag to indicate the Receiver to forward the TST */
            *pbFrwdTst = ECFM_TRUE;
            return ECFM_FAILURE;
        }

        /* If the destination address of the TST is not CCM Group Address app to 
         * the receving MP's MD level then no further processing is performed */
        if (ECFM_IS_MULTICAST_CLASS1_ADDR (pPduSmInfo->RxDestMacAddr,
                                           pStackInfo->u1MdLevel) != ECFM_TRUE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtClntProcessTst: "
                           "Destination address of received TST not equal "
                           "to CCM Group Addr app to rcv MEP's Md Level, "
                           "MEP cannot process the TST PDU further\r\n");
            return ECFM_FAILURE;
        }
        else
        {
            /* Y.1731 : Check that for processing Multicast TST PDU, MEPs
             * Multicast TST reception capability should be enabled */
            if (ECFM_LBLT_IS_MCAST_TST_RX_ENABLED (pMepInfo) == ECFM_FALSE)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntProcessTst: "
                               "Multicast Loopback reception capability is "
                               "disabled, So MEP cannot process the TST PDU "
                               "further\r\n");
                return ECFM_FAILURE;
            }
        }
    }
    else
    {
        /* MIP should Discard the TST PDU if the MAC address matches */
        if (ECFM_LBLT_IS_MHF (pStackInfo))
        {
            return ECFM_FAILURE;
        }
    }

    /* Check if Checksum is present then Is it correct */
    if ((pPduSmInfo->uPduInfo.Tst.u1TstTlvPatternType ==
         ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC)
        || (pPduSmInfo->uPduInfo.Tst.u1TstTlvPatternType ==
            ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC))
    {
        pu1StartBuf = gau1EcfmLbltTstPdu;

        /* Form Buffer to calculate CRC */
        ECFM_PUT_1BYTE (pu1StartBuf, pPduSmInfo->uPduInfo.Tst.u1TstTlvType);
        ECFM_PUT_2BYTE (pu1StartBuf, pPduSmInfo->uPduInfo.Tst.u2TstTlvLength);
        ECFM_PUT_1BYTE (pu1StartBuf,
                        pPduSmInfo->uPduInfo.Tst.u1TstTlvPatternType);

        u2TlvLen =
            pPduSmInfo->uPduInfo.Tst.u2TstTlvLength -
            ECFM_LBLT_TEST_TLV_CRC_SIZE;
        MEMCPY (pu1StartBuf, pPduSmInfo->uPduInfo.Tst.pu1TstTlvData, u2TlvLen);

        /* Calculate Checksum for the TST data received */
        pu1StartBuf = pu1StartBuf - ECFM_CFM_HDR_SIZE;
        u2TlvLen = (pPduSmInfo->uPduInfo.Tst.u2TstTlvLength) -
            ECFM_LBLT_TEST_TLV_CRC_SIZE + ECFM_TLV_LENGTH_FIELD_SIZE +
            ECFM_TLV_TYPE_FIELD_SIZE;
        u4CalChksum =
            EcfmLbLtUtilCalculateCrc32 (pu1StartBuf, (UINT4) u2TlvLen);
        if (pPduSmInfo->uPduInfo.Tst.u4Checksum == u4CalChksum)
        {
            /* Increment the counter of the TST PDU received */
            ECFM_LBLT_INCR_TST_IN (pTstInfo);
        }
        else
        {
            /* Increment the counter of the Bit Error received */
            ECFM_TST_INCR_BIT_ERROR (pTstInfo);
            /* Generate the SNMP trap for the fault */
            Y1731_LBLT_GENERATE_TRAP (pMepInfo, Y1731_TST_RCVD_ERR_TRAP_VAL);

            MepInfo.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;
            MepInfo.u4IfIndex =
                ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
            /* Notify the registered applications that Bit Error is Set */
            ECFM_NOTIFY_PROTOCOLS (ECFM_CHECKSUM_COMPARISON_FAILED,
                                   &MepInfo, NULL,
                                   ECFM_CHECKSUM_COMPARISON_FAILED,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                   ECFM_LBLT_TASK_ID);
        }
        pu1StartBuf = NULL;
    }
    else
    {
        /* Increment the counter of the TST PDU received */
        ECFM_LBLT_INCR_TST_IN (pTstInfo);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
  End of File cfmtstpr.c
 ******************************************************************************/
