/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmmalw.c,v 1.46 2012/02/10 10:14:54 siva Exp $
 *
 * Description: This file contains the Protocol Low Level Routines 
 *              for MA Table of standard ECFM MIB.
 *******************************************************************/
#include "cfminc.h"
#include "fscfmmcli.h"
#include "fsmiy1cli.h"

/* LOW LEVEL Routines for Table : Dot1agCfmMaNetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmMaNetTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmMaNetTable (UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex)
{
    return (EcfmUtlValIndexInstMaNetTable (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmMaNetTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmMaNetTable (UINT4 *pu4Dot1agCfmMdIndex,
                                     UINT4 *pu4Dot1agCfmMaIndex)
{
    return (nmhGetNextIndexDot1agCfmMaNetTable (0, pu4Dot1agCfmMdIndex,
                                                0, pu4Dot1agCfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmMaNetTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmMaNetTable (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 *pu4NextDot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 *pu4NextDot1agCfmMaIndex)
{
    return (EcfmUtlGetNextIndexMaNetTable (u4Dot1agCfmMdIndex,
                                           pu4NextDot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           pu4NextDot1agCfmMaIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaNetFormat
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaNetFormat (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                            INT4 *pi4RetValDot1agCfmMaNetFormat)
{
    return (EcfmUtlGetMaNetFormat (u4Dot1agCfmMdIndex,
                                   u4Dot1agCfmMaIndex,
                                   pi4RetValDot1agCfmMaNetFormat));
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaNetName
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaNetName (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValDot1agCfmMaNetName)
{
    return (EcfmUtlGetMaNetName (u4Dot1agCfmMdIndex,
                                 u4Dot1agCfmMaIndex,
                                 pRetValDot1agCfmMaNetName));
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaNetCcmInterval
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetCcmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaNetCcmInterval (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 INT4 *pi4RetValDot1agCfmMaNetCcmInterval)
{
    return (EcfmUtlGetMaNetCcmInterval (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        pi4RetValDot1agCfmMaNetCcmInterval));
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaNetRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaNetRowStatus (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               INT4 *pi4RetValDot1agCfmMaNetRowStatus)
{
    return (EcfmUtlGetMaNetRowStatus (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      pi4RetValDot1agCfmMaNetRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaNetFormat
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaNetFormat (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                            INT4 i4SetValDot1agCfmMaNetFormat)
{
    return (EcfmUtlSetMaNetFormat (u4Dot1agCfmMdIndex,
                                   u4Dot1agCfmMaIndex,
                                   i4SetValDot1agCfmMaNetFormat));
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaNetName
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaNetName (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValDot1agCfmMaNetName)
{
    return (EcfmUtlSetMaNetName (u4Dot1agCfmMdIndex,
                                 u4Dot1agCfmMaIndex,
                                 pSetValDot1agCfmMaNetName));
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaNetCcmInterval
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetCcmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaNetCcmInterval (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 INT4 i4SetValDot1agCfmMaNetCcmInterval)
{
    return (EcfmUtlSetMaNetCcmInterval (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        i4SetValDot1agCfmMaNetCcmInterval));
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaNetRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaNetRowStatus (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               INT4 i4SetValDot1agCfmMaNetRowStatus)
{

    return (EcfmUtlSetMaNetRowStatus (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      i4SetValDot1agCfmMaNetRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaNetFormat
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaNetFormat (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               INT4 i4TestValDot1agCfmMaNetFormat)
{
    return (EcfmUtlTestv2CfmMaNetFormat (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         i4TestValDot1agCfmMaNetFormat));
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaNetName
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaNetName (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValDot1agCfmMaNetName)
{
    return (EcfmUtlTestv2MaNetName (pu4ErrorCode,
                                    u4Dot1agCfmMdIndex,
                                    u4Dot1agCfmMaIndex,
                                    pTestValDot1agCfmMaNetName));
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaNetCcmInterval
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetCcmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaNetCcmInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 i4TestValDot1agCfmMaNetCcmInterval)
{
    return (EcfmUtlTestv2MaNetCcmInterval (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           i4TestValDot1agCfmMaNetCcmInterval));
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaNetRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaNetRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 i4TestValDot1agCfmMaNetRowStatus)
{
    return (EcfmUtlTestv2MaNetRowStatus (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         i4TestValDot1agCfmMaNetRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmMaNetTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmMaNetTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CfmMaCompTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmMaCompTable (UINT4
                                                u4Ieee8021CfmMaComponentId,
                                                UINT4 u4Dot1agCfmMdIndex,
                                                UINT4 u4Dot1agCfmMaIndex)
{
    return (EcfmUtlValIndexInstMaCompTable (u4Ieee8021CfmMaComponentId,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmMaCompTable
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmMaCompTable (UINT4 *pu4Ieee8021CfmMaComponentId,
                                        UINT4 *pu4Dot1agCfmMdIndex,
                                        UINT4 *pu4Dot1agCfmMaIndex)
{
    return (nmhGetNextIndexIeee8021CfmMaCompTable
            (0, pu4Ieee8021CfmMaComponentId, 0, pu4Dot1agCfmMdIndex, 0,
             pu4Dot1agCfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmMaCompTable
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                nextIeee8021CfmMaComponentId
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmMaCompTable (UINT4 u4Ieee8021CfmMaComponentId,
                                       UINT4 *pu4NextIeee8021CfmMaComponentId,
                                       UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 *pu4NextDot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 *pu4NextDot1agCfmMaIndex)
{
    return (EcfmUtlGetNextIndexMaCompTable (u4Ieee8021CfmMaComponentId,
                                            pu4NextIeee8021CfmMaComponentId,
                                            u4Dot1agCfmMdIndex,
                                            pu4NextDot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            pu4NextDot1agCfmMaIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompPrimarySelectorType
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompPrimarySelectorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompPrimarySelectorType (UINT4 u4Ieee8021CfmMaComponentId,
                                            UINT4 u4Dot1agCfmMdIndex,
                                            UINT4 u4Dot1agCfmMaIndex,
                                            INT4
                                            *pi4RetValIeee8021CfmMaCompPrimarySelectorType)
{
    return EcfmUtlGetMaCompPriSelType (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       pi4RetValIeee8021CfmMaCompPrimarySelectorType);
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompPrimarySelectorOrNone
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompPrimarySelectorOrNone
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (UINT4 u4Ieee8021CfmMaComponentId,
                                              UINT4 u4Dot1agCfmMdIndex,
                                              UINT4 u4Dot1agCfmMaIndex,
                                              UINT4
                                              *pu4RetValIeee8021CfmMaCompPrimarySelectorOrNone)
{
    return (EcfmUtlGetMaCompPriSelectOrNone (u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             pu4RetValIeee8021CfmMaCompPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompMhfCreation
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompMhfCreation (UINT4 u4Ieee8021CfmMaComponentId,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 *pi4RetValIeee8021CfmMaCompMhfCreation)
{
    return (EcfmUtlGetMaCompMhfCreation (u4Ieee8021CfmMaComponentId,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         pi4RetValIeee8021CfmMaCompMhfCreation));
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompIdPermission
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompIdPermission (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4
                                     *pi4RetValIeee8021CfmMaCompIdPermission)
{
    return (EcfmUtlGetMaCompIdPermission (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          pi4RetValIeee8021CfmMaCompIdPermission));

}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompNumberOfVids
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompNumberOfVids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompNumberOfVids (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4
                                     *pu4RetValIeee8021CfmMaCompNumberOfVids)
{
    return (EcfmUtlGetMaCompNumberOfVids (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          pu4RetValIeee8021CfmMaCompNumberOfVids));
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompRowStatus
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompRowStatus (UINT4 u4Ieee8021CfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 *pi4RetValIeee8021CfmMaCompRowStatus)
{
    return (EcfmUtlGetMaCompRowStatus (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       pi4RetValIeee8021CfmMaCompRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompPrimarySelectorType
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompPrimarySelectorType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompPrimarySelectorType (UINT4 u4Ieee8021CfmMaComponentId,
                                            UINT4 u4Dot1agCfmMdIndex,
                                            UINT4 u4Dot1agCfmMaIndex,
                                            INT4
                                            i4SetValIeee8021CfmMaCompPrimarySelectorType)
{
    return EcfmUtlSetMaCompPriSelType (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       i4SetValIeee8021CfmMaCompPrimarySelectorType);
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompPrimarySelectorOrNone
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompPrimarySelectorOrNone
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompPrimarySelectorOrNone (UINT4 u4Ieee8021CfmMaComponentId,
                                              UINT4 u4Dot1agCfmMdIndex,
                                              UINT4 u4Dot1agCfmMaIndex,
                                              UINT4
                                              u4SetValIeee8021CfmMaCompPrimarySelectorOrNone)
{
    return (EcfmUtlSetMaCompPriSelorNone (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4SetValIeee8021CfmMaCompPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompMhfCreation
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompMhfCreation (UINT4 u4Ieee8021CfmMaComponentId,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 i4SetValIeee8021CfmMaCompMhfCreation)
{
    return (EcfmUtlSetMaCompMhfCreation (u4Ieee8021CfmMaComponentId,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         i4SetValIeee8021CfmMaCompMhfCreation));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompIdPermission
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompIdPermission (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4 i4SetValIeee8021CfmMaCompIdPermission)
{
    return (EcfmUtlSetMaCompIdPermission (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          i4SetValIeee8021CfmMaCompIdPermission));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompNumberOfVids
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompNumberOfVids
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompNumberOfVids (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4
                                     u4SetValIeee8021CfmMaCompNumberOfVids)
{
    return (EcfmUtlSetMaCompNumberOfVids (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4SetValIeee8021CfmMaCompNumberOfVids));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompRowStatus
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompRowStatus (UINT4 u4Ieee8021CfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 i4SetValIeee8021CfmMaCompRowStatus)
{
    return (EcfmUtlSetMaCompRowStatus (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       i4SetValIeee8021CfmMaCompRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompPrimarySelectorType
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompPrimarySelectorType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompPrimarySelectorType (UINT4 *pu4ErrorCode,
                                               UINT4 u4Ieee8021CfmMaComponentId,
                                               UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               INT4
                                               i4TestValIeee8021CfmMaCompPrimarySelectorType)
{
    return EcfmUtlTestMaCompPriSelType (pu4ErrorCode,
                                        u4Ieee8021CfmMaComponentId,
                                        u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        i4TestValIeee8021CfmMaCompPrimarySelectorType);
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompPrimarySelectorOrNone
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021CfmMaComponentId,
                                                 UINT4 u4Dot1agCfmMdIndex,
                                                 UINT4 u4Dot1agCfmMaIndex,
                                                 UINT4
                                                 u4TestValIeee8021CfmMaCompPrimarySelectorOrNone)
{
    return (EcfmUtlTestv2MaCompPriSelOrNone (pu4ErrorCode,
                                             u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4TestValIeee8021CfmMaCompPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompMhfCreation
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompMhfCreation (UINT4 *pu4ErrorCode,
                                       UINT4 u4Ieee8021CfmMaComponentId,
                                       UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       INT4
                                       i4TestValIeee8021CfmMaCompMhfCreation)
{
    return (EcfmUtlTstv2CfmMaCompMhfCreation (pu4ErrorCode,
                                              u4Ieee8021CfmMaComponentId,
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              i4TestValIeee8021CfmMaCompMhfCreation));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompIdPermission
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompIdPermission (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021CfmMaComponentId,
                                        UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        INT4
                                        i4TestValIeee8021CfmMaCompIdPermission)
{
    return (EcfmUtlTestv2MaCompIdPermission (pu4ErrorCode,
                                             u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             i4TestValIeee8021CfmMaCompIdPermission));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompNumberOfVids
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompNumberOfVids
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompNumberOfVids (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021CfmMaComponentId,
                                        UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4
                                        u4TestValIeee8021CfmMaCompNumberOfVids)
{
    return (EcfmUtlTestv2MaCompNumberOfVids (pu4ErrorCode,
                                             u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4TestValIeee8021CfmMaCompNumberOfVids));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompRowStatus
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4 i4TestValIeee8021CfmMaCompRowStatus)
{

    return (EcfmUtlTestv2MaCompRowStatus (pu4ErrorCode,
                                          u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          i4TestValIeee8021CfmMaCompRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CfmMaCompTable
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CfmMaCompTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1agCfmMaCompTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmMaCompTable (UINT4 u4Dot1agCfmMaComponentId,
                                              UINT4 u4Dot1agCfmMdIndex,
                                              UINT4 u4Dot1agCfmMaIndex)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmMaCompTable (UINT4 *pu4Dot1agCfmMaComponentId,
                                      UINT4 *pu4Dot1agCfmMdIndex,
                                      UINT4 *pu4Dot1agCfmMaIndex)
{
    UNUSED_PARAM (pu4Dot1agCfmMaComponentId);
    UNUSED_PARAM (pu4Dot1agCfmMdIndex);
    UNUSED_PARAM (pu4Dot1agCfmMaIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                nextDot1agCfmMaComponentId
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmMaCompTable (UINT4 u4Dot1agCfmMaComponentId,
                                     UINT4 *pu4NextDot1agCfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 *pu4NextDot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 *pu4NextDot1agCfmMaIndex)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (pu4NextDot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (pu4NextDot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (pu4NextDot1agCfmMaIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaCompPrimaryVlanId
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompPrimaryVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaCompPrimaryVlanId (UINT4 u4Dot1agCfmMaComponentId,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 *pi4RetValDot1agCfmMaCompPrimaryVlanId)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (pi4RetValDot1agCfmMaCompPrimaryVlanId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaCompMhfCreation
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaCompMhfCreation (UINT4 u4Dot1agCfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 *pi4RetValDot1agCfmMaCompMhfCreation)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (pi4RetValDot1agCfmMaCompMhfCreation);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaCompIdPermission
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaCompIdPermission (UINT4 u4Dot1agCfmMaComponentId,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   INT4 *pi4RetValDot1agCfmMaCompIdPermission)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (pi4RetValDot1agCfmMaCompIdPermission);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaCompNumberOfVids
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompNumberOfVids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaCompNumberOfVids (UINT4 u4Dot1agCfmMaComponentId,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 *pu4RetValDot1agCfmMaCompNumberOfVids)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (pu4RetValDot1agCfmMaCompNumberOfVids);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaCompRowStatus
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaCompRowStatus (UINT4 u4Dot1agCfmMaComponentId,
                                UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                INT4 *pi4RetValDot1agCfmMaCompRowStatus)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (pi4RetValDot1agCfmMaCompRowStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaCompPrimaryVlanId
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompPrimaryVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaCompPrimaryVlanId (UINT4 u4Dot1agCfmMaComponentId,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 i4SetValDot1agCfmMaCompPrimaryVlanId)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4SetValDot1agCfmMaCompPrimaryVlanId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaCompMhfCreation
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaCompMhfCreation (UINT4 u4Dot1agCfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 i4SetValDot1agCfmMaCompMhfCreation)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4SetValDot1agCfmMaCompMhfCreation);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaCompIdPermission
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaCompIdPermission (UINT4 u4Dot1agCfmMaComponentId,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   INT4 i4SetValDot1agCfmMaCompIdPermission)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4SetValDot1agCfmMaCompIdPermission);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaCompNumberOfVids
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompNumberOfVids
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaCompNumberOfVids (UINT4 u4Dot1agCfmMaComponentId,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4SetValDot1agCfmMaCompNumberOfVids)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (u4SetValDot1agCfmMaCompNumberOfVids);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaCompRowStatus
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaCompRowStatus (UINT4 u4Dot1agCfmMaComponentId,
                                UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                INT4 i4SetValDot1agCfmMaCompRowStatus)
{
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4SetValDot1agCfmMaCompRowStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaCompPrimaryVlanId
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompPrimaryVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaCompPrimaryVlanId (UINT4 *pu4ErrorCode,
                                       UINT4 u4Dot1agCfmMaComponentId,
                                       UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       INT4
                                       i4TestValDot1agCfmMaCompPrimaryVlanId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4TestValDot1agCfmMaCompPrimaryVlanId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaCompMhfCreation
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaCompMhfCreation (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1agCfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4 i4TestValDot1agCfmMaCompMhfCreation)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4TestValDot1agCfmMaCompMhfCreation);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaCompIdPermission
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaCompIdPermission (UINT4 *pu4ErrorCode,
                                      UINT4 u4Dot1agCfmMaComponentId,
                                      UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      INT4 i4TestValDot1agCfmMaCompIdPermission)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4TestValDot1agCfmMaCompIdPermission);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaCompNumberOfVids
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompNumberOfVids
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaCompNumberOfVids (UINT4 *pu4ErrorCode,
                                      UINT4 u4Dot1agCfmMaComponentId,
                                      UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4
                                      u4TestValDot1agCfmMaCompNumberOfVids)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (u4TestValDot1agCfmMaCompNumberOfVids);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaCompRowStatus
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaCompRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMaComponentId,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   INT4 i4TestValDot1agCfmMaCompRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmMaComponentId);
    UNUSED_PARAM (u4Dot1agCfmMdIndex);
    UNUSED_PARAM (u4Dot1agCfmMaIndex);
    UNUSED_PARAM (i4TestValDot1agCfmMaCompRowStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmMaCompTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Dot1agCfmMaMepListTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmMaMepListTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmMaMepListTable (UINT4 u4Dot1agCfmMdIndex,
                                                 UINT4 u4Dot1agCfmMaIndex,
                                                 UINT4
                                                 u4Dot1agCfmMaMepListIdentifier)
{
    return (EcfmUtlValdateMaMepListTable (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMaMepListIdentifier));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmMaMepListTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmMaMepListTable (UINT4 *pu4Dot1agCfmMdIndex,
                                         UINT4 *pu4Dot1agCfmMaIndex,
                                         UINT4 *pu4Dot1agCfmMaMepListIdentifier)
{
    return (nmhGetNextIndexDot1agCfmMaMepListTable (0, pu4Dot1agCfmMdIndex, 0,
                                                    pu4Dot1agCfmMaIndex, 0,
                                                    pu4Dot1agCfmMaMepListIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmMaMepListTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier
                nextDot1agCfmMaMepListIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmMaMepListTable (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 *pu4NextDot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 *pu4NextDot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMaMepListIdentifier,
                                        UINT4
                                        *pu4NextDot1agCfmMaMepListIdentifier)
{
    return (EcfmUtlGetNxtIdxMaMepListTable (u4Dot1agCfmMdIndex,
                                            pu4NextDot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            pu4NextDot1agCfmMaIndex,
                                            u4Dot1agCfmMaMepListIdentifier,
                                            pu4NextDot1agCfmMaMepListIdentifier));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMaMepListRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier

                The Object 
                retValDot1agCfmMaMepListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMaMepListRowStatus (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMaMepListIdentifier,
                                   INT4 *pi4RetValDot1agCfmMaMepListRowStatus)
{
    return (EcfmUtlGetMaMepListRowStatus (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMaMepListIdentifier,
                                          pi4RetValDot1agCfmMaMepListRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmMaMepListRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier

                The Object 
                setValDot1agCfmMaMepListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMaMepListRowStatus (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMaMepListIdentifier,
                                   INT4 i4SetValDot1agCfmMaMepListRowStatus)
{
    return (EcfmUtlSetMaMepListRowStatus (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMaMepListIdentifier,
                                          i4SetValDot1agCfmMaMepListRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMaMepListRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier

                The Object 
                testValDot1agCfmMaMepListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMaMepListRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMaMepListIdentifier,
                                      INT4 i4TestValDot1agCfmMaMepListRowStatus)
{
    return (EcfmUtlTestMaMepListRowStatus (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMaMepListIdentifier,
                                           i4TestValDot1agCfmMaMepListRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmMaMepListTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmMaMepListTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/******************************************************************************/
/*                           End  of file cfmmalw.c                           */
/******************************************************************************/
