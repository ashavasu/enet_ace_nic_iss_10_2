/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccif.c,v 1.49 2016/05/25 10:13:04 siva Exp $
 *
 * Description: This file contains the routine for                  
 *              creating/deleting interface structure and change in 
 *              the interface operational status notification from  
 *              lower layer to ECFM Module's CC Task.               
 *******************************************************************/

#include "cfminc.h"

/* Prototypes for the PRIVATE routines used in this file */
PRIVATE INT4 EcfmStackCmpForPort PROTO ((tRBElem *, tRBElem *));
PRIVATE BOOL1 EcfmCcIsPortBlocked PROTO ((UINT1));

/****************************************************************************
 * FUNCTION NAME    : EcfmCcIfCreateAllPorts
 *
 * DESCRIPTION      : This function creates all valid ports in ECFM
 *                    module's CC Task .
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None
 *
 *****************************************************************************/

PUBLIC VOID
EcfmCcIfCreateAllPorts (VOID)
{
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PrevPort = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    while (EcfmL2IwfGetNextValidPortForContext
           (ECFM_CC_CURR_CONTEXT_ID (), u2PrevPort, &u2PortNum,
            &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (EcfmCcIfHandleCreatePort (ECFM_CC_CURR_CONTEXT_ID (), u4IfIndex,
                                      u2PortNum) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmCcIfCreateAllPorts: Port %u Entry creation"
                              "FAILED \r\n", u2PortNum);
        }
        u2PrevPort = u2PortNum;
    }

    /* Create one Dummy Port (Last Local Port# in the context reserved) for all 
     * MPLS-TP Meps in the context to point.
     */
#ifdef MPLS_WANTED
    if (EcfmCreateCcMplsPort (ECFM_CC_CURR_CONTEXT_ID (),
                              (CFA_MIN_IVR_IF_INDEX +
                               ECFM_CC_CURR_CONTEXT_ID ()),
                              (UINT2)ECFM_CC_MAX_PORT_INFO) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmCcIfCreateAllPorts: Dummy MPLS Port %u Entry creation"
                          "FAILED \r\n", ECFM_MAX_PORTS_PER_CONTEXT);
    }
#endif
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * 
 * FUNCTION NAME    : EcfmCcDeleteAllPortsInfo
 * 
 * DESCRIPTION      : Delete all Ports related info present in Global
 *                    structure for CC Task.
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 *****************************************************************************/
PUBLIC VOID
EcfmCcDeleteAllPortsInfo ()
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;

    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_CC_MAX_PORT_INFO; u2LocalPortNum++)
    {

        pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPortNum);
        if (pPortInfo != NULL)
        {
            EcfmCcIfHandleDeletePort (u2LocalPortNum);
        }
    }
    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmCcIfHandleCreatePort
 *
 * DESCRIPTION      : This function allocates mempools, intializes the pot info
 *                     in ECFM  module for Main Task
 * 
 * INPUT            : u4ContextId - Context Identifier
 *                    u4IfIndex - IfIndex of the Port to be created
 *                    u4LocalPort - Local Port Number
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcIfHandleCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPort)
{
    tEcfmCcPortInfo    *pPortEntry = NULL;
    tEcfmCfaIfInfo      CfaIfInfo;
    UINT4               u4PhyIfIndex = ECFM_INIT_VAL;
    UINT4               u4TempCxtId = 0;
    UINT2               u2PortChannel = 0;
    UINT2               u2ChannelPortNum = 0;
    UINT1               u1InterfaceType = 0;
    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcIfHandleCreatePort:Creating Port %u \r\n",
                      u4IfIndex);

    /* Check if the Port Entry already exists */
    pPortEntry = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    if (pPortEntry != NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcIfHandleCreatePort:Port Already exits \r\n");
        return ECFM_SUCCESS;
    }
    /* Get PortInfo from CFA about the Port */
    if ((EcfmUtilGetIfInfo (u4IfIndex, &CfaIfInfo)) != CFA_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcIfHandleCreatePort: IfInfo from CFA FAILED!!! \r\n");
        return ECFM_FAILURE;
    }
    /* Check if the Port Entry already exists */
    pPortEntry = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    if (pPortEntry == NULL)
    {
        /* Allocate memory for the new port entry */
        if (ECFM_ALLOC_MEM_BLOCK_CC_PORT_INFO (pPortEntry) == NULL)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "EcfmCcIfHandleCreatePort:Memory Allocation for Port FAILED"
                         "\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }

        ECFM_MEMSET (pPortEntry, ECFM_INIT_VAL, ECFM_CC_PORT_INFO_SIZE);

        /* Create Stack RBTree for each port created */
        pPortEntry->StackInfoTree = EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                                              (tEcfmCcStackInfo,
                                                               StackPortInfoNode),
                                                              EcfmStackCmpForPort);
        if (pPortEntry->StackInfoTree == NULL)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                         ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcIfHandleCreatePort: Stack Tree Creation Failed\n");

            /* Free the memory block assigned */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);
            return ECFM_FAILURE;
        }

    }
    /* Add the port entry to the port table */
    /* Update the interface specific paramaters obtained from lower layer. */
    pPortEntry->u4IfIndex = u4IfIndex;
    pPortEntry->u2PortNum = u2LocalPort;
    pPortEntry->u4ContextId = u4ContextId;
    pPortEntry->u1IfOperStatus = CfaIfInfo.u1IfOperStatus;
    pPortEntry->u1IfType = CfaIfInfo.u1IfType;
    pPortEntry->u1PortEcfmStatus = ECFM_MODULE_STATUS (u4ContextId);
    pPortEntry->u1PortY1731Status = ECFM_Y1731_STATUS (u4ContextId);
    pPortEntry->b1LLCEncapStatus = FALSE;
    if (CfaIfInfo.u1BrgPortType == L2IWF_CNP_STAGGED_PORT)
    {
        if (EcfmL2IwfGetInterfaceType (u4ContextId, &u1InterfaceType) ==
            L2IWF_FAILURE)
        {
            /* Free the memory block assigned */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);
            return ECFM_FAILURE;
        }
        if (u1InterfaceType == L2IWF_C_INTERFACE_TYPE)
        {
            CfaIfInfo.u1BrgPortType = L2IWF_CNP_CTAGGED_PORT;
        }
    }
    pPortEntry->u1PortType = CfaIfInfo.u1BrgPortType;
    pPortEntry->u1PortIdSubType = ECFM_PORTID_SUB_IF_ALIAS;
    EcfmCfaGetInterfaceNameFromIndex (u4IfIndex, pPortEntry->au1PortId);

    /* If this port is part of the port channel update the port channel
     * number in port info */

    if (EcfmL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        /* Get the Global portChannel number from Global interface index */
        if (EcfmL2IwfGetPortChannelForPort (u4IfIndex, &u2PortChannel)
            != L2IWF_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmCcIfHandleCreatePort: Port Channel retrieval"
                              "failed for port %u\n", u4IfIndex);
            ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);
            return ECFM_FAILURE;
        }

        /* Get the Local portChannel number from Global PortChannel Number */
        if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u2PortChannel, &u4TempCxtId,
                                                &u2ChannelPortNum)
            != VCM_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmCcIfHandleCreatePort: Getting the local"
                              "Port Channel failed for port %u\n", u4IfIndex);
            ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);
            return ECFM_FAILURE;
        }

        pPortEntry->u2ChannelPortNum = u2ChannelPortNum;
    }

    if ((u4IfIndex >= ECFM_MIN_SISP_INDEX) &&
        (u4IfIndex <= ECFM_MAX_SISP_INDEX))
    {
        /* The ifindex range falls under SISP logical interface index.
         * Hence, query VCM-SISP module to obtain the physical interface mapped
         * to this logical interface
         * */
        if (EcfmVcmSispGetPhysicalPortOfSispPort (u4IfIndex, &u4PhyIfIndex)
            != VCM_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "CcIfHandleCreatePort: Physical port retrieval from VCM-SISP failed for port %u\n",
                              u4IfIndex);
            RBTreeDestroy (pPortEntry->StackInfoTree,
                           (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                           ECFM_CC_STACK_ENTRY_IN_PORT);
            /* Free the memory block assigned */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);

            return ECFM_FAILURE;
        }
        pPortEntry->u4PhyPortNum = u4PhyIfIndex;
    }
    else
    {
        pPortEntry->u4PhyPortNum = u4IfIndex;
    }

    ECFM_CC_SET_PORT_INFO (u2LocalPort, pPortEntry);

    /* Add in global RB-Tree */
    if (RBTreeAdd (ECFM_CC_GLOBAL_PORT_TABLE, pPortEntry) == ECFM_RB_FAILURE)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcIfHandleCreatePort: Unable to add port in global"
                     " RB-Tree\n");
        RBTreeDestroy (pPortEntry->StackInfoTree,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_STACK_ENTRY_IN_PORT);
        ECFM_CC_SET_PORT_INFO (u2LocalPort, NULL);
        /* Free the memory block assigned */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmDeleteMpFromPort
 *
 * DESCRIPTION      : This function Deletes the MP nodes corresponding to a 
 *                    ifIndex from stack and Mip Table.
 * 
 * INPUT            : UINT2 u2PortNum - port for which Mp node is to be 
 *                    deleted. 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None. 
 *
 *****************************************************************************/
PRIVATE VOID
EcfmDeleteMpFromPort (UINT2 u2PortNum)
{
    tEcfmCcMipInfo      MipNode;
    tEcfmCcMipPreventInfo MipPreventNode;
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipInfo     *pMipNextNode = NULL;
    tEcfmCcMipPreventInfo *pMipPreventNode = NULL;
    tEcfmCcMipPreventInfo *pMipPreventNextNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepInfo     *pMepNextNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcRMepDbInfo  *pTempRMepNode = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
    tEcfmCcMepInfo     *pHwMepNode = NULL;
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL,
                 ECFM_HW_MEP_HANDLER_SIZE);
#endif

    ECFM_MEMSET (&MipNode, 0x00, sizeof (tEcfmCcMipInfo));
    ECFM_MEMSET (&MipPreventNode, 0x00, sizeof (tEcfmCcMipPreventInfo));
    ECFM_MEMSET (gpEcfmCcMepNode, 0x00, sizeof (tEcfmCcMepInfo));

    /* Delete Mep nodes corresponding to u2PortNum */
    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    pMepNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode);
    if (pMepNode == NULL)
    {
        pMepNode =
            RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);
    }
    while ((pMepNode != NULL) && (pMepNode->u2PortNum == u2PortNum))
    {
        /* Release Avilability Info Mempool If allocated */
        if (pMepNode->pAvlbltyInfo != NULL)
        {
            ECFM_FREE_MEM_BLOCK (ECFM_AVLBLTY_INFO_POOLID,
                                 (UINT1 *) pMepNode->pAvlbltyInfo);
            pMepNode->pAvlbltyInfo = NULL;
        }

        pMepNextNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, pMepNode, NULL);

        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmDeleteMpFromPort: Reverting back State Machine \n");
        EcfmCcUtilNotifySm (pMepNode, ECFM_IND_IF_DELETE);
        EcfmCcUtilNotifyY1731 (pMepNode, ECFM_IND_IF_DELETE);

#ifdef NPAPI_WANTED
        ECFM_MEMSET (gpEcfmCcMepNode, 0x00, sizeof (tEcfmCcMepInfo));

        MEMCPY (gpEcfmCcMepNode->au1HwMepHandler, pMepNode->au1HwMepHandler,
                ECFM_HW_MEP_HANDLER_SIZE);
        pHwMepNode = (tEcfmCcMepInfo *)
                    RBTreeGet (ECFM_CC_GLOBAL_HW_MEP_TABLE,
                               gpEcfmCcMepNode);

        if (pHwMepNode != NULL)
        {
            /* Remove the entry from global h/w MEP table */
            RBTreeRem (ECFM_CC_GLOBAL_HW_MEP_TABLE, pHwMepNode);
        }
        /* Offloaded module is informed to Stop CCM Tx and
         * Stop CCM Rx through EcfmCcUtilNotifySm. Now delete
         * the MEP from the hardware.
         */
        if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
            ECFM_HW_MEP_HANDLER_SIZE) != 0)
        {
            MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

            EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);
            EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepNode->u4IfIndex;

            if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_DELETE,
                                     &EcfmHwInfo) == FNP_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmMepUtlSetAgMepRowStatus :MEP deletion"
                             "indication to H/W failed. \r\n");
            }
        }
#endif

        /* If a port is delted in the ECFM then that indication 
         * should be given to external modules. B'cause some modules 
         * can expecting this indication like ELPS module. */
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepNode->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepNode->u2PortNum,
                                                  pTempPortInfo);

        MepInfo.u4MdIndex = pMepNode->u4MdIndex;
        MepInfo.u4MaIndex = pMepNode->u4MaIndex;
        MepInfo.u2MepId = pMepNode->u2MepId;
        MepInfo.u1MdLevel = pMepNode->u1MdLevel;
        MepInfo.u1Direction = pMepNode->u1Direction;

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                               &MepInfo, NULL,
                               ECFM_DEFECT_CONDITION_ENCOUNTERED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL,
                               NULL, ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

        /*Remove other references of MEP node */
        pMaNode = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);

        /* Destroy Remote Mep Db Info Tree */
        pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
        while (pRMepNode != NULL)
        {
            pTempRMepNode = pRMepNode;
            pRMepNode = (tEcfmCcRMepDbInfo *)
                TMO_DLL_Next (&(pMepNode->RMepDb),
                              &(pTempRMepNode->MepDbDllNode));

            EcfmDeleteRMepEntry (pMepNode, pTempRMepNode);
            pTempRMepNode = NULL;
        }

        /* From its associated MA 's MepTable */
        TMO_DLL_Delete (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));

        RBTreeRem (ECFM_CC_MEP_TABLE, (tRBElem *) pMepNode);
        RBTreeRem (ECFM_CC_PORT_MEP_TABLE, (tRBElem *) pMepNode);

        pMepNode->pPortInfo = NULL;
        /* Release mem block allocated to MEP node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL, (UINT1 *) (pMepNode));
        pMepNode = pMepNextNode;
    }

    /* Delete Mip nodes corresponding to u2PortNum */
    MipNode.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    MipNode.u2PortNum = u2PortNum;
    pMipNode = RBTreeGet (ECFM_CC_GLOBAL_MIP_TABLE, &MipNode);
    if (pMipNode == NULL)
    {
        pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE, &MipNode, NULL);
    }
    while ((pMipNode != NULL) &&
           (pMipNode->u4ContextId == MipNode.u4ContextId) &&
           (pMipNode->u2PortNum == MipNode.u2PortNum))
    {
        pMipNextNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE, pMipNode, NULL);
        /* Remove Mip node from Mip table */
        RBTreeRem (ECFM_CC_GLOBAL_MIP_TABLE, (tRBElem *) pMipNode);

        /* Release memory allocated to Mip node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL, (UINT1 *) (pMipNode));
        pMipNode = pMipNextNode;
    }

    /* Delete Mip prevent nodes corresponding to u2PortNum */
    MipPreventNode.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();

    /* Initialise the ptr to NULL */
    /*Reset the Port Info Ptr */
    pTempPortInfo = NULL;
    MipPreventNode.u4IfIndex = ECFM_CC_GET_PHY_PORT (u2PortNum, pTempPortInfo);
    pMipPreventNode =
        RBTreeGet (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE, &MipPreventNode);
    if (pMipPreventNode == NULL)
    {
        pMipPreventNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                                         &MipPreventNode, NULL);
    }

    /* Reset the Port info ptr */
    pTempPortInfo = NULL;

    while ((pMipPreventNode != NULL) &&
           (pMipPreventNode->u4IfIndex ==
            ECFM_CC_GET_PHY_PORT (u2PortNum, pTempPortInfo)))
    {
        pMipPreventNextNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                                             pMipPreventNode, NULL);
        /* Remove Mip node from Mip table */
        RBTreeRem (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                   (tRBElem *) pMipPreventNode);

        /* Release memory allocated to Mip node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_PREVENT_TABLE_POOL,
                             (UINT1 *) (pMipPreventNode));
        pMipPreventNode = pMipPreventNextNode;

        /* Reset the Port info ptr */
        pTempPortInfo = NULL;
    }

    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmCcIfHandleDeletePort
 *
 * DESCRIPTION      : This function Deletes the Port created on indication from
 *                    lower layer.
 * 
 * INPUT            : UINT2 u2PortNum - Port to be Deleted 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcIfHandleDeletePort (UINT2 u2PortNum)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);

    if (pPortInfo == NULL)
    {
        return;
    }

    /* If not MPLS Dummy Port */
    if (u2PortNum != ECFM_CC_MAX_PORT_INFO)
    {
        /* Deleting Stack Tree in a Port */
        RBTreeDestroy (pPortInfo->StackInfoTree,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_STACK_ENTRY_IN_PORT);
        /* Delete MP nodes corresponding to port */
        EcfmDeleteMpFromPort (u2PortNum);

        /* Delete the port for global tree */
        RBTreeRem (ECFM_CC_GLOBAL_PORT_TABLE, (tRBElem *) pPortInfo);
    }

    /* Freeing MEM Block for the Port Node and assiginig NULL  */
    ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortInfo);
    pPortInfo = NULL;
    ECFM_CC_SET_PORT_INFO (u2PortNum, NULL);

    /* If not MPLS Dummy Port */
    if (u2PortNum != ECFM_CC_MAX_PORT_INFO)
    {
        /* Reevaluate MIP creation for all ports and all VLANs */
        EcfmCcUtilEvaluateAndCreateMip (-1, -1, ECFM_TRUE);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmCcIfHandlePortOperChg
 *
 * DESCRIPTION      : This function changes the operational status of the Port.
 * 
 * INPUT            : u2PortNum - IfIndex of the Port whose status needs to be 
 *                                updated.
 *                    u1State  - New State of the Port to be updated with. 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcIfHandlePortOperChg (UINT2 u2PortNum, UINT1 u1State)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;

    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4Event = 0;
    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&PduSmInfo, 0, sizeof (tEcfmCcPduSmInfo));
    MEMSET (gpEcfmCcMepNode, 0, sizeof (tEcfmCcMepInfo));
    /* Getting PortInfo if exists */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);

    if (pPortInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcIfHandlePortOperChg:Port Doesnt Exist \r\n");
        return ECFM_FAILURE;
    }

    if (pPortInfo->u1IfOperStatus == u1State)
    {
        return ECFM_SUCCESS;
    }
    pPortInfo->u1IfOperStatus = u1State;

    if (u1State == CFA_IF_DOWN)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC, "EcfmCcIfHandlePortOperChg:"
                      "Port %d is down\r\n", pPortInfo->u4IfIndex);
    }
    else if (u1State == CFA_IF_UP)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC, "EcfmCcIfHandlePortOperChg:"
                      "Port %d is up\r\n", pPortInfo->u4IfIndex);
    }

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                 "EcfmCcIfHandlePortOperChg:Send Port OperStatus Change to"
                 "Standby Node also \r\n");
    EcfmRedSyncUpPortOperStatus (pPortInfo->u4IfIndex, u1State);

    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);

    do
    {
        if (pMepNode == NULL)
        {
            break;
        }

        if (pMepNode->u2PortNum != u2PortNum)
        {
            continue;
        }

        /* Give notification to other protocols (ERPS etc.,) , when
         * oper-status changes in case of DOWN MEP */

        if ((pMepNode->u1Direction == ECFM_MP_DIR_DOWN) &&
            (ECFM_TRUE == EcfmCcIsPortBlocked (pPortInfo->u1IfOperStatus)))
        {
            PduSmInfo.pMepInfo = pMepNode;
            EcfmCcClntNotifyRmep (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            u4Event = ECFM_EV_INT_PDU_IN_QUEUE |
                ECFM_EV_CFM_PDU_IN_QUEUE | ECFM_EV_CFG_MSG_IN_QUEUE;

            EcfmCcUtilRelinquishLoop (u4Event);
        }

        if ((pMepNode->b1MepCcmOffloadStatus ==
             ECFM_TRUE) && (pMepNode->u1Direction == ECFM_MP_DIR_DOWN))
        {
            if (EcfmUpdatePortStatusForDownMep (pMepNode) == ECFM_FAILURE)
            {
                continue;
            }
        }

        EcfmCcHandleMacStatusChange (pMepNode, ECFM_OPER_STATUS_CHG_MSG);
    }
    while ((pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                      (tRBElem *) pMepNode, NULL)) != NULL);

    if ((EcfmUpdatePortStatusForAllUpMep
         (pPortInfo->u4ContextId, pPortInfo->u4IfIndex, pPortInfo,
          u1State)) == ECFM_FAILURE)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      :  EcfmHandlePortStateChg
 *
 * Description        : This rotuine is used to handle Port State change Indication
 * 
 * Input(s)           : u2MstInst  - InstanceID for which the port state is to
 *                                   be updated
 *                      u4IfIndex -  Interface Index of the port, whose port
 *                                   state is to be updated .
 *                      u1PortState - Port state to be set 
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmHandlePortStateChg (UINT2 u2MstInst, UINT4 u4IfIndex, UINT1 u1PortState)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT2               u2PortNo = ECFM_INIT_VAL;
    UINT2               u2MepMstInst = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT1               u1ProtocolId = ECFM_INIT_VAL;
#ifdef NPAPI_WANTED
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL, ECFM_HW_MEP_HANDLER_SIZE);
#endif
    ECFM_MEMSET (gpEcfmCcMepNode, 0x00, sizeof (tEcfmCcMepInfo));
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex, &u4ContextId,
                                            &u2PortNo) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmPortStateChangeIndication:"
                     "ECFM Module is SHUTDOWN\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    /* Verify the index for which the Port State Change is received */
    if ((u2PortNo < (UINT2) ECFM_PORTS_PER_CONTEXT_MIN)
        || (u2PortNo > (UINT2) ECFM_CC_MAX_PORT_INFO))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    /* For This Actual Port get MEP Information */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

    if (pPortInfo == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    gpEcfmCcMepNode->u2PortNum = u2PortNo;
    pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);

    do
    {
        if (pMepInfo == NULL)
        {
            break;
        }

        if (pMepInfo->u2PortNum != u2PortNo)
        {
            continue;
        }

        EcfmCcUtilUpdatePortState (pMepInfo, u1PortState);

        if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
	{
	       /* In case of MSTP/ERPS, update state only in case of VLAN aware MEP, 
   		* VLAN of which is mapped to MST/Ring instance */
		if ((EcfmAstIsMstEnabledInContext (u4ContextId) == MST_ENABLED) ||
		     (EcfmErpsApiIsErpsStartedInContext(u4ContextId) == OSIX_TRUE))
		{
			if (pMepInfo->u4PrimaryVidIsid == 0)
			{
			    /* In case of MSTP/ERPS, update state only in case of 
 			     * VLAN aware MEP */
			     continue;
			}
			L2IwfGetPortStateCtrlOwner(pMepInfo->u2PortNum, &u1ProtocolId ,OSIX_TRUE);
		        if((u1ProtocolId == ERPS_MODULE) || (u1ProtocolId == STP_MODULE))
		        {
                    	/* Update the port status only in case of MEP's VLAN is
                     	 * mapped to MST/ERPS Instance */
                   	      u2MepMstInst = ECFM_INIT_VAL;
                      	      u2MepMstInst = L2IwfMiGetVlanInstMapping (u4ContextId,
                        	                       pMepInfo->u4PrimaryVidIsid);
                     	      if (u2MepMstInst == u2MstInst)
                     	      {
                         	    /* First Disable MEP transmission from this MEP */
                         	    /* Update Tx/Rx parameters for this MEP */
                    	 	    EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                    		    if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == 
							ECFM_FAILURE)
                    		    {
                        		continue;
                    		    }
                     	      }
			}/*End of Protocol Id check*/
			else
			{
	                      EcfmCcmOffDeleteTxRxForMep (pMepInfo);
         	              if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
                              {
	                            	continue;
        	              }
			}		
		}
		else
		{
			/* First Disable MEP transmission from this MEP */
			/* Update Tx/Rx parameters for this MEP */
			EcfmCcmOffDeleteTxRxForMep (pMepInfo);

			if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
			{
				continue;
			}
		}
	}

        EcfmCcHandleMacStatusChange (pMepInfo, ECFM_PORT_STATE_CHANGE);
    }
    while ((pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                      (tRBElem *) pMepInfo, NULL)) != NULL);

    if ((EcfmUpdatePortStatusForAllUpMep
         (pPortInfo->u4ContextId, u4IfIndex, pPortInfo,
          u1PortState)) == ECFM_FAILURE)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/***************************************************************************
*
* Function Name : EcfmHandlePbPortStateChg
*
* Description   : This function is to update the PEP Port state for UP MEP
*
* Input         : u4CepIfIndex- IfIndex
*                u4VlanId - svlan ID
*                u1PortState - Port state to be updated
*
* Output       : None
*
* Returns      : ECFM_SUCCESS/ECFM_FAILURE
***************************************************************************/
PUBLIC INT4
EcfmHandlePbPortStateChg (UINT4 u4IfIndex, UINT4 u4VlanId, UINT1 u1PortState)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT2               u2PortNo = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, 0x00, sizeof (tEcfmCcMepInfo));
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex, &u4ContextId,
                                            &u2PortNo) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmHandlePbPortStateChg:" "ECFM Module is SHUTDOWN\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    /* Verify the index for which the Port State Change is received */
    if ((u2PortNo < (UINT2) ECFM_PORTS_PER_CONTEXT_MIN)
        || (u2PortNo > (UINT2) ECFM_PORTS_PER_CONTEXT_MAX))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    /* For This Actual Port get MEP Information */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);
    if (pPortInfo == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    gpEcfmCcMepNode->u2PortNum = u2PortNo;
    pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);
    do
    {
        if (pMepInfo == NULL)
        {
            break;
        }

        if (pMepInfo->u2PortNum != u2PortNo)
        {
            continue;
        }
        if (pMepInfo->u4PrimaryVidIsid != u4VlanId)
        {
            continue;
        }
        if ((pMepInfo->pPortInfo->u1PortType == CFA_CUSTOMER_EDGE_PORT) &&
            (pMepInfo->u1Direction == ECFM_MP_DIR_UP))
        {
            if (u1PortState == AST_PORT_STATE_FORWARDING)
            {
                pMepInfo->u1PepPortState = ECFM_PORT_IS_UP;
            }
            else
            {
                pMepInfo->u1PepPortState = ECFM_PORT_IS_BLOCKED;
            }
        }

        EcfmCcHandleMacStatusChange (pMepInfo, ECFM_PORT_STATE_CHANGE);
    }
    while ((pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                      (tRBElem *) pMepInfo, NULL)) != NULL);
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmCcIfInterfaceTypeChg
 *
 * DESCRIPTION      : This function changes the operational status of the Port.
 * 
 * INPUT            : u2PortNum - IfIndex of the Port whose status needs to be 
 *                                updated.
 *                    u1State  - New State of the Port to be updated with. 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcIfInterfaceTypeChg (UINT1 u1IntfType)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT2               u2Port;
    UINT1               u1PortType;
    UINT1               u1NewPortType;

    ECFM_CC_TRC_FN_ENTRY ();

    if (u1IntfType == L2IWF_C_INTERFACE_TYPE)
    {
        u1PortType = L2IWF_CNP_STAGGED_PORT;
        u1NewPortType = L2IWF_CNP_CTAGGED_PORT;
    }
    else
    {
        u1PortType = L2IWF_CNP_CTAGGED_PORT;
        u1NewPortType = L2IWF_CNP_STAGGED_PORT;
    }

    for (u2Port = ECFM_PORTS_PER_CONTEXT_MIN;
         u2Port <= ECFM_CC_MAX_PORT_INFO; u2Port++)
    {
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2Port);
        if (pPortInfo != NULL)
        {
            if (pPortInfo->u1PortType == u1PortType)
            {
                pPortInfo->u1PortType = u1NewPortType;
            }
            pPortInfo = NULL;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmCcIsPortBlocked
 *
 *    DESCRIPTION      : This routine compares the OperStatus of the Port 
 *                       with the list of blocked Interface Oper Types.
 *
 *    INPUT            : u1OperStatus - Interface's OperStatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If input operstatus matches the list then ECFM_TRUE  
 *                       else ECFM_FALSE
 ****************************************************************************/

PRIVATE             BOOL1
EcfmCcIsPortBlocked (UINT1 u1OperStatus)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < ECFM_MAX_INTF_OPER_TYPES - 1; u1Index++)
    {
        if (au1BlockedOperType[u1Index] == u1OperStatus)
        {
            return ECFM_TRUE;
        }
    }

    return ECFM_FALSE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmStackCmpForPort
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Stack RBTree Nodes maintained per
 *                       Port
 *
 *    INPUT            : *pS1 - pointer to Stack Node1
 *                       *pS2 - pointer to Stack Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pS1 > pS2 
 *                       else returns -1
 *
 ****************************************************************************/

PRIVATE INT4
EcfmStackCmpForPort (tRBElem * pS1, tRBElem * pS2)
{
    tEcfmCcStackInfo   *pEcfmEntryA = NULL;
    tEcfmCcStackInfo   *pEcfmEntryB = NULL;

    pEcfmEntryA = (tEcfmCcStackInfo *) pS1;
    pEcfmEntryB = (tEcfmCcStackInfo *) pS2;

    if (pEcfmEntryA->u4VlanIdIsid != pEcfmEntryB->u4VlanIdIsid)
    {
        if (pEcfmEntryA->u4VlanIdIsid < pEcfmEntryB->u4VlanIdIsid)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)
    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1Direction != pEcfmEntryB->u1Direction)
    {
        if (pEcfmEntryA->u1Direction < pEcfmEntryB->u1Direction)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    return 0;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCreateCcMplsPort 
 *
 *    DESCRIPTION      : This function is used to create a dummy Port interface
 *                       which will be used for MPLS Networks. 
 *
 *    INPUT            : u4ContextId - Context Id 
 *                       u4IfIndex   - IfIndex
 *                       u2LocalPort - Local Port Number 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pS1 > pS2
 *                       else returns -1
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCreateCcMplsPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPort)
{
    tEcfmCcPortInfo    *pPortEntry = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCreateCcMplsPort:Creating Port %u \r\n", u4IfIndex);

    /* Check if the Port Entry already exists */
    pPortEntry = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    if (pPortEntry != NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCreateCcMplsPort: Port Already exits \r\n");
        return ECFM_SUCCESS;
    }
    /* Check if the Port Entry already exists */
    if (pPortEntry == NULL)
    {
        /* Allocate memory for the new port entry */
        if (ECFM_ALLOC_MEM_BLOCK_CC_PORT_INFO (pPortEntry) == NULL)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "EcfmCreateCcMplsPort: Memory Allocation for Port FAILED"
                         "\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }

        ECFM_MEMSET (pPortEntry, ECFM_INIT_VAL, ECFM_CC_PORT_INFO_SIZE);
    }

    /* Add the port entry to the port table */
    /* Update the interface specific paramaters obtained from lower layer. */
    pPortEntry->u4IfIndex = u4IfIndex;
    pPortEntry->u2PortNum = u2LocalPort;
    pPortEntry->u4ContextId = u4ContextId;
    pPortEntry->u1IfOperStatus = UP;
    pPortEntry->u1IfType = CFA_MPLS;
    /* Dummy Port takes the current ECFM module status */
    pPortEntry->u1PortEcfmStatus = ECFM_MODULE_STATUS (u4ContextId);
    pPortEntry->u1PortY1731Status = ECFM_ENABLE;
    pPortEntry->b1LLCEncapStatus = FALSE;
    pPortEntry->u1PortType = ECFM_MPLSTP_DEFAULT_PORT_TYPE;
    pPortEntry->u1PortIdSubType = ECFM_MPLSTP_PORTID_SUB_IF_ALIAS;
    MEMCPY (pPortEntry->au1PortId, "MPLSPORT", STRLEN ("MPLSPORT"));
    ECFM_CC_SET_PORT_INFO (u2LocalPort, pPortEntry);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmccif.c
 ****************************************************************************/
