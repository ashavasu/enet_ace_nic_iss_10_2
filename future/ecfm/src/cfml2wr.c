/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfml2wr.c,v 1.9 2014/02/26 12:12:54 siva Exp $
 *
 * Description: This file contains the wrapper function for all the
 *              external L2IWF APIs called by ECFM module.
 *******************************************************************/

#include "cfminc.h"
/*****************************************************************************/
/* Function Name      : EcfmL2IwfMiIsVlanMemberPort                          */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a member of the given Vlan in the given      */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : ContextId, VlanId, Global IfIndex                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
PUBLIC              BOOL1
EcfmL2IwfMiIsVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex)
{
    return (L2IwfMiIsVlanMemberPort (u4ContextId, VlanId, u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This routine checks whether the port is a member of  */
/*                      any Port Channel and if so returns SUCCESS.          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetPortChannelForPort                       */
/*                                                                           */
/* Description        : This routine returns the Port Channel of which the   */
/*                      given port is a member.                              */
/*                                                                           */
/* Input(s)           : u2IfIndex - Global IfIndex of the port whose         */
/*                                  PortChannel Id to be obtained.           */
/*                                                                           */
/* Output(s)          : u2AggId     - Port Channel Index                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

PUBLIC INT4
EcfmL2IwfGetPortChannelForPort (UINT2 u2IfIndex, UINT2 *pu2AggId)
{
    return (L2IwfGetPortChannelForPort (u2IfIndex, pu2AggId));
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetVlanPortState                            */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Vlan Id and the Port Index.                          */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the port state is to be    */
/*                               obtained.                                   */
/*                    : u2IfIndex - Global IfIndex of the port whose state   */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
PUBLIC UINT1
EcfmL2IwfGetVlanPortState (UINT4 u4VlanIdIsid, UINT4 u4IfIndex)
{
    if (u4VlanIdIsid >= ECFM_INTERNAL_ISID_MIN)
    {
        /*for ISID-aware ports */
        return AST_PORT_STATE_FORWARDING;
    }
    return L2IwfGetVlanPortState (u4VlanIdIsid, u4IfIndex);
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfMiIsVlanActive                               */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : ContextId, VlanId                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
PUBLIC              BOOL1
EcfmL2IwfMiIsVlanActive (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiIsVlanActive (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetBridgeMode                                */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      bridge mode.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode));
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfMiIsVlanUntagMemberPort                               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Port Membership status                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                    : u2VlanId - Vlan Identifier                           */
/*                    : u4IfIndex - Actual interface number                  */
/*                                                                           */
/* Output(s)          : ECFM_TRUE - Port is Untagged member of Vlan          */
/*                    : ECFM_FALSE - Port is Tagged member of Vlan           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfMiIsVlanUntagMemberPort (UINT4 u2ContextId, UINT2 u2VlanId,
                                  UINT4 u4IfIndex)
{

    return L2IwfMiIsVlanUntagMemberPort (u2ContextId, u2VlanId, u4IfIndex);
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfMiGetVlanEgressPorts                        */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Port List                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                    : u2VlanId - Vlan Identifier                           */
/*                    : PortList  - Port List                                */
/*                                                                           */
/* Output(s)          : ECFM_SUCCESS - Egress Port is Not NULL               */
/*                    : ECFM_FAIULURE - PortList is NULL                     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfMiGetVlanEgressPorts (UINT4 u2ContextId, UINT2 u2VlanId,
                               tLocalPortListExt PortList)
{
    /* Make sure that we return the local port list and not the if-index based
     * egress list*/
    return L2IwfMiGetVlanLocalEgressPorts (u2ContextId, u2VlanId, PortList);
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetInterfaceType                            */
/*                                                                           */
/* Description        : This function returns the Interface Type configured  */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu1InterfaceType - Interface Type configured in L2IWF*/
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
EcfmL2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType)
{
    return L2IwfGetInterfaceType (u4ContextId, pu1InterfaceType);
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetVlanPortPvid                             */
/*                                                                           */
/* Description        : This routine returns the port VLAN Id for the given  */
/*                      given Port Index. It accesses the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortPvid   - Port VLAN Id.                          */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE                           */
/*****************************************************************************/
INT4
EcfmL2IwfGetVlanPortPvid (UINT4 u4IfIndex, tVlanId * pVlanId)
{
    if (L2IwfGetVlanPortPvid (u4IfIndex, pVlanId) != L2IWF_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetPortVlanList                             */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Vlan list. If argument u1VlanPortType is Tagged then */
/*                      this function will return the List of vlans for which*/
/*                      this port is untagged Member.                        */
/*                      If the u1VlanPortType is untagged or member port then*/
/*                      this functiion will return the list of vlans for     */
/*                      which port is untagged memberor both                 */
/*                      tagged & untagged List                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                    : u2LocalPortId - Local Port Number                    */
/*                    : u1VlanIdPortType - Port Vlan Type                    */
/*                      u1VlanPortType values may be                         */
/*                                   1. UnTagged member port -0              */
/*                                   2. tagged member port - 1               */
/*                                   3. member port (tagged or untagged)-2   */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfGetPortVlanList (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE *
                          pVlanIdList, UINT2 u2LocalPortId,
                          UINT1 u1VlanPortType)
{
    if (L2IwfGetPortVlanList (u4ContextId, pVlanIdList, u2LocalPortId,
                              u1VlanPortType) != L2IWF_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfml2wr.c
 ****************************************************************************/
