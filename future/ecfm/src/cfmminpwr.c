/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmminpwr.c,v 1.1 2008/01/17 12:47:01 iss Exp $
 *
 * Description: Stubs for network processor functions given here
 *
 *******************************************************************/

#include "cfminc.h"

/*****************************************************************************
 * FsMiEcfmHwInit 
 *
 * DESCRIPTION:  Stub provided for backward compatibility with MI-unaware 
 *               hardware.
 *
 * INPUTS:       u4ContextId - Virtual Switch ID 
 * 
 * OUTPUTS:      None
 *
 * RETURNS:      None
 *
 *****************************************************************************/

INT4
FsMiEcfmHwInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsEcfmHwInit ();
}

/*****************************************************************************
 * FsMiEcfmHwDeInit 
 *
 * DESCRIPTION:   Stub provided for backward compatibility with MI-unaware 
 *                hardware.
 *
 * INPUTS:        u4ContextId - Virtual Switch ID 
 * 
 * OUTPUTS:       None
 *
 * RETURNS:       None
 *
 *****************************************************************************/

INT4
FsMiEcfmHwDeInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsEcfmHwDeInit ();
}
