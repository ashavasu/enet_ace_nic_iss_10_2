/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmmeplw.c,v 1.58 2012/02/10 10:14:54 siva Exp $
 *
 * Description: This file contains the Protocol Low Level Routines 
 *               for MEP Table of standard ECFM MIB.
 *******************************************************************/

#include "cfminc.h"
#include "fscfmmcli.h"
#include "fsmiy1cli.h"

/* LOW LEVEL Routines for Table : Dot1agCfmMepTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmMepTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmMepTable (UINT4 u4Dot1agCfmMdIndex,
                                           UINT4 u4Dot1agCfmMaIndex,
                                           UINT4 u4Dot1agCfmMepIdentifier)
{
    return EcfmMepUtlValAgMepTable (u4Dot1agCfmMdIndex,
                                    u4Dot1agCfmMaIndex,
                                    u4Dot1agCfmMepIdentifier);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmMepTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmMepTable (UINT4 *pu4Dot1agCfmMdIndex,
                                   UINT4 *pu4Dot1agCfmMaIndex,
                                   UINT4 *pu4Dot1agCfmMepIdentifier)
{
    return (nmhGetNextIndexDot1agCfmMepTable (0, pu4Dot1agCfmMdIndex, 0,
                                              pu4Dot1agCfmMaIndex, 0,
                                              pu4Dot1agCfmMepIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmMepTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                nextDot1agCfmMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmMepTable (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 *pu4NextDot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 *pu4NextDot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4NextDot1agCfmMepIdentifier)
{
    return EcfmMepUtlGetNextIndexAgMepTable (u4Dot1agCfmMdIndex,
                                             pu4NextDot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             pu4NextDot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4NextDot1agCfmMepIdentifier);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepIfIndex
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepIfIndex (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           INT4 *pi4RetValDot1agCfmMepIfIndex)
{
    return EcfmMepUtlGetAgMepIfIndex (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      pi4RetValDot1agCfmMepIfIndex);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDirection
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDirection (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 *pi4RetValDot1agCfmMepDirection)
{
    return EcfmMepUtlGetAgMepDirection (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        pi4RetValDot1agCfmMepDirection);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepPrimaryVid
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepPrimaryVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepPrimaryVid (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 *pu4RetValDot1agCfmMepPrimaryVid)
{
    return EcfmMepUtlGetAgMepPrimaryVid (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pu4RetValDot1agCfmMepPrimaryVid);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepActive
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepActive (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          INT4 *pi4RetValDot1agCfmMepActive)
{
    return EcfmMepUtlGetAgMepActive (u4Dot1agCfmMdIndex,
                                     u4Dot1agCfmMaIndex,
                                     u4Dot1agCfmMepIdentifier,
                                     pi4RetValDot1agCfmMepActive);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepFngState
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepFngState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepFngState (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            INT4 *pi4RetValDot1agCfmMepFngState)
{
    return EcfmMepUtlGetAgMepFngState (u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       u4Dot1agCfmMepIdentifier,
                                       pi4RetValDot1agCfmMepFngState);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepCciEnabled
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCciEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepCciEnabled (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 *pi4RetValDot1agCfmMepCciEnabled)
{
    return EcfmMepUtlGetAgMepCciEnabled (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pi4RetValDot1agCfmMepCciEnabled);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepCcmLtmPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCcmLtmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepCcmLtmPriority (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4RetValDot1agCfmMepCcmLtmPriority)
{

    return EcfmMepUtlGetAgMepCcmLtmPriority (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4RetValDot1agCfmMepCcmLtmPriority);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepMacAddress (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              tMacAddr * pRetValDot1agCfmMepMacAddress)
{
    return EcfmMepUtlGetAgMepMacAddress (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pRetValDot1agCfmMepMacAddress);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepLowPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLowPrDef
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepLowPrDef (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            INT4 *pi4RetValDot1agCfmMepLowPrDef)
{
    return EcfmMepUtlGetAgMepLowPrDef (u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       u4Dot1agCfmMepIdentifier,
                                       pi4RetValDot1agCfmMepLowPrDef);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepFngAlarmTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepFngAlarmTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepFngAlarmTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 *pi4RetValDot1agCfmMepFngAlarmTime)
{
    return EcfmMepUtlGetAgMepFngAlarmTime (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pi4RetValDot1agCfmMepFngAlarmTime);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepFngResetTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepFngResetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepFngResetTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 *pi4RetValDot1agCfmMepFngResetTime)
{
    return EcfmMepUtlGetAgMepFngResetTime (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pi4RetValDot1agCfmMepFngResetTime);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepHighestPrDefect
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepHighestPrDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepHighestPrDefect (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4 *pi4RetValDot1agCfmMepHighestPrDefect)
{
    return EcfmMepUtlGetAgMepHighestPrDef (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pi4RetValDot1agCfmMepHighestPrDefect);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDefects
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepDefects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDefects (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           tSNMP_OCTET_STRING_TYPE * pRetValDot1agCfmMepDefects)
{
    return EcfmMepUtlGetAgMepDefects (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      pRetValDot1agCfmMepDefects);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepErrorCcmLastFailure
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepErrorCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepErrorCcmLastFailure (UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 u4Dot1agCfmMepIdentifier,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValDot1agCfmMepErrorCcmLastFailure)
{
    return EcfmMepUtlGetAgMepErrCcmLastFail (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pRetValDot1agCfmMepErrorCcmLastFailure);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepXconCcmLastFailure
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepXconCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepXconCcmLastFailure (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValDot1agCfmMepXconCcmLastFailure)
{
    return EcfmMepUtlGetAgMepXconCcmLastFail (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pRetValDot1agCfmMepXconCcmLastFailure);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepCcmSequenceErrors
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCcmSequenceErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepCcmSequenceErrors (UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     UINT4
                                     *pu4RetValDot1agCfmMepCcmSequenceErrors)
{
    return EcfmMepUtlGetAgMepCcmSeqErrors (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pu4RetValDot1agCfmMepCcmSequenceErrors);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepCciSentCcms
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCciSentCcms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepCciSentCcms (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 *pu4RetValDot1agCfmMepCciSentCcms)
{
    return EcfmMepUtlGetAgMepCciSentCcms (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          pu4RetValDot1agCfmMepCciSentCcms);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepNextLbmTransId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepNextLbmTransId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepNextLbmTransId (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4RetValDot1agCfmMepNextLbmTransId)
{
    return EcfmMepUtlGetAgMepNextLbmTransId (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4RetValDot1agCfmMepNextLbmTransId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepLbrIn
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepLbrIn (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 *pu4RetValDot1agCfmMepLbrIn)
{
    return EcfmMepUtlGetAgMepLbrIn (u4Dot1agCfmMdIndex,
                                    u4Dot1agCfmMaIndex,
                                    u4Dot1agCfmMepIdentifier,
                                    pu4RetValDot1agCfmMepLbrIn);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepLbrInOutOfOrder
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrInOutOfOrder
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepLbrInOutOfOrder (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   UINT4 *pu4RetValDot1agCfmMepLbrInOutOfOrder)
{
    return EcfmMepUtlGetAgMepLbrInOutOfOrder (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pu4RetValDot1agCfmMepLbrInOutOfOrder);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepLbrBadMsdu
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrBadMsdu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepLbrBadMsdu (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 *pu4RetValDot1agCfmMepLbrBadMsdu)
{
    return EcfmMepUtlGetAgMepLbrBadMsdu (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pu4RetValDot1agCfmMepLbrBadMsdu);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepLtmNextSeqNumber
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLtmNextSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepLtmNextSeqNumber (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4
                                    *pu4RetValDot1agCfmMepLtmNextSeqNumber)
{
    return EcfmMepUtlGetAgMepLtmNextSeqNo (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pu4RetValDot1agCfmMepLtmNextSeqNumber);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepUnexpLtrIn
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepUnexpLtrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepUnexpLtrIn (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 *pu4RetValDot1agCfmMepUnexpLtrIn)
{
    return EcfmMepUtlGetAgMepUnexpLtrIn (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pu4RetValDot1agCfmMepUnexpLtrIn);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepLbrOut
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepLbrOut (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          UINT4 *pu4RetValDot1agCfmMepLbrOut)
{
    return EcfmMepUtlGetAgMepLbrOut (u4Dot1agCfmMdIndex,
                                     u4Dot1agCfmMaIndex,
                                     u4Dot1agCfmMepIdentifier,
                                     pu4RetValDot1agCfmMepLbrOut);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmStatus (UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     INT4
                                     *pi4RetValDot1agCfmMepTransmitLbmStatus)
{
    INT4                i4RetVal = ECFM_INIT_VAL;

    i4RetVal = EcfmMepUtlGetAgMepTxLbmStatus (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pi4RetValDot1agCfmMepTransmitLbmStatus);

    if (*pi4RetValDot1agCfmMepTransmitLbmStatus == ECFM_TX_STATUS_START)
    {
        *pi4RetValDot1agCfmMepTransmitLbmStatus = ECFM_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLbmStatus = ECFM_SNMP_FALSE;
    }
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmDestMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmDestMacAddress (UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             tMacAddr *
                                             pRetValDot1agCfmMepTransmitLbmDestMacAddress)
{
    return EcfmMepUtlGetAgMepTxLbmDstMacAddr (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pRetValDot1agCfmMepTransmitLbmDestMacAddress);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmDestMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmDestMepId (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4
                                        *pu4RetValDot1agCfmMepTransmitLbmDestMepId)
{
    return EcfmMepUtlGetAgMepTxLbmDestMepId (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4RetValDot1agCfmMepTransmitLbmDestMepId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmDestIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmDestIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                          UINT4 u4Dot1agCfmMaIndex,
                                          UINT4 u4Dot1agCfmMepIdentifier,
                                          INT4
                                          *pi4RetValDot1agCfmMepTransmitLbmDestIsMepId)
{
    return EcfmMepUtlGetAgMepTxLbmDstIsMepId (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pi4RetValDot1agCfmMepTransmitLbmDestIsMepId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmMessages
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmMessages (UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 u4Dot1agCfmMepIdentifier,
                                       INT4
                                       *pi4RetValDot1agCfmMepTransmitLbmMessages)
{
    return EcfmMepUtlGetAgMepTxLbmMessages (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            pi4RetValDot1agCfmMepTransmitLbmMessages);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmDataTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDataTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmDataTlv (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValDot1agCfmMepTransmitLbmDataTlv)
{
    return EcfmMepUtlGetAgMepTxLbmDataTlv (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pRetValDot1agCfmMepTransmitLbmDataTlv);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmVlanPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmVlanPriority (UINT4 u4Dot1agCfmMdIndex,
                                           UINT4 u4Dot1agCfmMaIndex,
                                           UINT4 u4Dot1agCfmMepIdentifier,
                                           INT4
                                           *pi4RetValDot1agCfmMepTransmitLbmVlanPriority)
{
    return EcfmMepUtlGetAgMepTxLbmVlanPri (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pi4RetValDot1agCfmMepTransmitLbmVlanPriority);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmVlanDropEnable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmVlanDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmVlanDropEnable (UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             INT4
                                             *pi4RetValDot1agCfmMepTransmitLbmVlanDropEnable)
{
    return EcfmMepUtlGetAgMepTxLbmVlanDropEna (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               pi4RetValDot1agCfmMepTransmitLbmVlanDropEnable);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmResultOK
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmResultOK (UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 u4Dot1agCfmMepIdentifier,
                                       INT4
                                       *pi4RetValDot1agCfmMepTransmitLbmResultOK)
{
    return EcfmMepUtlGetAgMepTxLbmResultOK (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            pi4RetValDot1agCfmMepTransmitLbmResultOK);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLbmSeqNumber
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLbmSeqNumber (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4
                                        *pu4RetValDot1agCfmMepTransmitLbmSeqNumber)
{
    return EcfmMepUtlGetAgMepTxLbmSeqNumber (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4RetValDot1agCfmMepTransmitLbmSeqNumber);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmStatus (UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     INT4
                                     *pi4RetValDot1agCfmMepTransmitLtmStatus)
{
    INT4                i4RetVal = ECFM_INIT_VAL;

    i4RetVal = EcfmMepUtlGetAgMepTxLtmStatus (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pi4RetValDot1agCfmMepTransmitLtmStatus);
    if (*pi4RetValDot1agCfmMepTransmitLtmStatus == ECFM_TX_STATUS_START)
    {
        *pi4RetValDot1agCfmMepTransmitLtmStatus = ECFM_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLtmStatus = ECFM_SNMP_FALSE;
    }
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmFlags
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmFlags (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValDot1agCfmMepTransmitLtmFlags)
{
    return EcfmMepUtlGetAgMepTxLtmFlags (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pRetValDot1agCfmMepTransmitLtmFlags);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTargetMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmTargetMacAddress (UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               UINT4 u4Dot1agCfmMepIdentifier,
                                               tMacAddr *
                                               pRetValDot1agCfmMepTransmitLtmTargetMacAddress)
{
    return EcfmMepUtlGetAgMepTxLtmTgtMacAddr (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pRetValDot1agCfmMepTransmitLtmTargetMacAddress);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmTargetMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTargetMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmTargetMepId (UINT4 u4Dot1agCfmMdIndex,
                                          UINT4 u4Dot1agCfmMaIndex,
                                          UINT4 u4Dot1agCfmMepIdentifier,
                                          UINT4
                                          *pu4RetValDot1agCfmMepTransmitLtmTargetMepId)
{
    return EcfmMepUtlGetAgMepTxLtmTgtMepId (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            pu4RetValDot1agCfmMepTransmitLtmTargetMepId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTargetIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmTargetIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                            UINT4 u4Dot1agCfmMaIndex,
                                            UINT4 u4Dot1agCfmMepIdentifier,
                                            INT4
                                            *pi4RetValDot1agCfmMepTransmitLtmTargetIsMepId)
{
    return EcfmMepUtlGetAgMepTxLtmTgtIsMepId (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              pi4RetValDot1agCfmMepTransmitLtmTargetIsMepId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmTtl (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4RetValDot1agCfmMepTransmitLtmTtl)
{
    return EcfmMepUtlGetAgMepTxLtmTtl (u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       u4Dot1agCfmMepIdentifier,
                                       pu4RetValDot1agCfmMepTransmitLtmTtl);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmResult
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmResult (UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     INT4
                                     *pi4RetValDot1agCfmMepTransmitLtmResult)
{
    return EcfmMepUtlGetAgMepTxLtmResult (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          pi4RetValDot1agCfmMepTransmitLtmResult);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmSeqNumber
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmSeqNumber (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4
                                        *pu4RetValDot1agCfmMepTransmitLtmSeqNumber)
{
    return EcfmMepUtlGetAgMepTxLtmSeqNumber (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4RetValDot1agCfmMepTransmitLtmSeqNumber);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepTransmitLtmEgressIdentifier (UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               UINT4 u4Dot1agCfmMepIdentifier,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValDot1agCfmMepTransmitLtmEgressIdentifier)
{
    return EcfmMepUtlGetAgMepTxLtmEgrId (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pRetValDot1agCfmMepTransmitLtmEgressIdentifier);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepRowStatus (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 *pi4RetValDot1agCfmMepRowStatus)
{
    return EcfmMepUtlGetAgMepRowStatus (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        pi4RetValDot1agCfmMepRowStatus);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepIfIndex
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepIfIndex (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           INT4 i4SetValDot1agCfmMepIfIndex)
{
    return EcfmMepUtlSetAgMepIfIndex (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      i4SetValDot1agCfmMepIfIndex);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepDirection
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepDirection (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 i4SetValDot1agCfmMepDirection)
{
    return EcfmMepUtlSetAgMepDirection (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        i4SetValDot1agCfmMepDirection);

}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepPrimaryVid
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepPrimaryVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepPrimaryVid (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 u4SetValDot1agCfmMepPrimaryVid)
{
    return EcfmMepUtlSetAgMepPrimaryVid (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         u4SetValDot1agCfmMepPrimaryVid);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepActive
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepActive (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          INT4 i4SetValDot1agCfmMepActive)
{
    return EcfmMepUtlSetAgMepActive (u4Dot1agCfmMdIndex,
                                     u4Dot1agCfmMaIndex,
                                     u4Dot1agCfmMepIdentifier,
                                     i4SetValDot1agCfmMepActive);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepCciEnabled
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepCciEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepCciEnabled (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 i4SetValDot1agCfmMepCciEnabled)
{
    return EcfmMepUtlSetAgMepCciEnabled (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         i4SetValDot1agCfmMepCciEnabled);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepCcmLtmPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepCcmLtmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepCcmLtmPriority (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 u4SetValDot1agCfmMepCcmLtmPriority)
{
    return EcfmMepUtlSetAgMepCcmLtmPriority (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             u4SetValDot1agCfmMepCcmLtmPriority);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepLowPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepLowPrDef
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepLowPrDef (UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            INT4 i4SetValDot1agCfmMepLowPrDef)
{
    return EcfmMepUtlSetAgMepLowPrDef (u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       u4Dot1agCfmMepIdentifier,
                                       i4SetValDot1agCfmMepLowPrDef);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepFngAlarmTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepFngAlarmTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepFngAlarmTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4SetValDot1agCfmMepFngAlarmTime)
{
    return EcfmMepUtlSetAgMepFngAlarmTime (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           i4SetValDot1agCfmMepFngAlarmTime);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepFngResetTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepFngResetTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepFngResetTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4SetValDot1agCfmMepFngResetTime)
{
    return EcfmMepUtlSetAgMepFngResetTime (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           i4SetValDot1agCfmMepFngResetTime);

}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmStatus (UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     INT4 i4SetValDot1agCfmMepTransmitLbmStatus)
{
    if (i4SetValDot1agCfmMepTransmitLbmStatus == ECFM_SNMP_TRUE)
    {
        i4SetValDot1agCfmMepTransmitLbmStatus = ECFM_TX_STATUS_START;
    }
    else
    {
        i4SetValDot1agCfmMepTransmitLbmStatus = ECFM_TX_STATUS_STOP;
    }

    return EcfmMepUtlSetAgMepTxLbmStatus (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          i4SetValDot1agCfmMepTransmitLbmStatus);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmDestMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmDestMacAddress (UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             tMacAddr
                                             SetValDot1agCfmMepTransmitLbmDestMacAddress)
{
    return EcfmMepUtlSetAgMepTxLbmDstMacAddr (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              SetValDot1agCfmMepTransmitLbmDestMacAddress);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmDestMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmDestMepId (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4
                                        u4SetValDot1agCfmMepTransmitLbmDestMepId)
{
    return EcfmMepUtlSetAgMepTxLbmDestMepId (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             u4SetValDot1agCfmMepTransmitLbmDestMepId);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmDestIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmDestIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                          UINT4 u4Dot1agCfmMaIndex,
                                          UINT4 u4Dot1agCfmMepIdentifier,
                                          INT4
                                          i4SetValDot1agCfmMepTransmitLbmDestIsMepId)
{
    return EcfmMepUtlSetAgMepTxLbmDstIsMepId (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4SetValDot1agCfmMepTransmitLbmDestIsMepId);

}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmMessages
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmMessages (UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 u4Dot1agCfmMepIdentifier,
                                       INT4
                                       i4SetValDot1agCfmMepTransmitLbmMessages)
{
    return EcfmMepUtlSetAgMepTxLbmMessages (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            i4SetValDot1agCfmMepTransmitLbmMessages);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmDataTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDataTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmDataTlv (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValDot1agCfmMepTransmitLbmDataTlv)
{
    return EcfmMepUtlSetAgMepTxLbmDataTlv (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pSetValDot1agCfmMepTransmitLbmDataTlv);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmVlanPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmVlanPriority (UINT4 u4Dot1agCfmMdIndex,
                                           UINT4 u4Dot1agCfmMaIndex,
                                           UINT4 u4Dot1agCfmMepIdentifier,
                                           INT4
                                           i4SetValDot1agCfmMepTransmitLbmVlanPriority)
{
    return EcfmMepUtlSetAgMepTxLbmVlanPri (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           i4SetValDot1agCfmMepTransmitLbmVlanPriority);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLbmVlanDropEnable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmVlanDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLbmVlanDropEnable (UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             INT4
                                             i4SetValDot1agCfmMepTransmitLbmVlanDropEnable)
{
    return EcfmMepUtlSetAgMepTxLbmVlanDropEna (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               i4SetValDot1agCfmMepTransmitLbmVlanDropEnable);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmStatus (UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     INT4 i4SetValDot1agCfmMepTransmitLtmStatus)
{
    if (i4SetValDot1agCfmMepTransmitLtmStatus == ECFM_SNMP_TRUE)
    {
        i4SetValDot1agCfmMepTransmitLtmStatus = ECFM_TX_STATUS_START;
    }
    else
    {
        i4SetValDot1agCfmMepTransmitLtmStatus = ECFM_TX_STATUS_STOP;
    }

    return EcfmMepUtlSetAgMepTxLtmStatus (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          i4SetValDot1agCfmMepTransmitLtmStatus);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmFlags
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmFlags (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValDot1agCfmMepTransmitLtmFlags)
{

    return EcfmMepUtlSetAgMepTxLtmFlags (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pSetValDot1agCfmMepTransmitLtmFlags);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTargetMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmTargetMacAddress (UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               UINT4 u4Dot1agCfmMepIdentifier,
                                               tMacAddr
                                               SetValDot1agCfmMepTransmitLtmTargetMacAddress)
{

    return EcfmMepUtlSetAgMepTxLtmTgtMacAddr (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              SetValDot1agCfmMepTransmitLtmTargetMacAddress);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmTargetMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTargetMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmTargetMepId (UINT4 u4Dot1agCfmMdIndex,
                                          UINT4 u4Dot1agCfmMaIndex,
                                          UINT4 u4Dot1agCfmMepIdentifier,
                                          UINT4
                                          u4SetValDot1agCfmMepTransmitLtmTargetMepId)
{

    return EcfmMepUtlSetAgMepTxLtmTgtMepId (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4SetValDot1agCfmMepTransmitLtmTargetMepId);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTargetIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmTargetIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                            UINT4 u4Dot1agCfmMaIndex,
                                            UINT4 u4Dot1agCfmMepIdentifier,
                                            INT4
                                            i4SetValDot1agCfmMepTransmitLtmTargetIsMepId)
{
    return EcfmMepUtlSetAgMepTxLtmTgtIsMepId (u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4SetValDot1agCfmMepTransmitLtmTargetIsMepId);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmTtl (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 u4SetValDot1agCfmMepTransmitLtmTtl)
{
    return EcfmMepUtlSetAgMepTxLtmTtl (u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       u4Dot1agCfmMepIdentifier,
                                       u4SetValDot1agCfmMepTransmitLtmTtl);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmEgressIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepTransmitLtmEgressIdentifier (UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               UINT4 u4Dot1agCfmMepIdentifier,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValDot1agCfmMepTransmitLtmEgressIdentifier)
{
    return EcfmMepUtlSetAgMepTxLtmEgrId (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pSetValDot1agCfmMepTransmitLtmEgressIdentifier);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMepRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMepRowStatus (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 i4SetValDot1agCfmMepRowStatus)
{
    return EcfmMepUtlSetAgMepRowStatus (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        i4SetValDot1agCfmMepRowStatus);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepIfIndex
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepIfIndex (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 i4TestValDot1agCfmMepIfIndex)
{
    return EcfmMepUtlTestv2AgMepIfIndex (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         i4TestValDot1agCfmMepIfIndex);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepDirection
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepDirection (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4TestValDot1agCfmMepDirection)
{
    return EcfmMepUtlTestv2AgMepDirection (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           i4TestValDot1agCfmMepDirection);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepPrimaryVid
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepPrimaryVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepPrimaryVid (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4 u4TestValDot1agCfmMepPrimaryVid)
{
    return EcfmMepUtlTestv2AgMepPrimaryVid (pu4ErrorCode,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4TestValDot1agCfmMepPrimaryVid);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepActive
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepActive (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 i4TestValDot1agCfmMepActive)
{
    return EcfmMepUtlTestv2AgMepActive (pu4ErrorCode,
                                        u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        i4TestValDot1agCfmMepActive);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepCciEnabled
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepCciEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepCciEnabled (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 INT4 i4TestValDot1agCfmMepCciEnabled)
{
    return EcfmMepUtlTestv2AgMepCciEnabled (pu4ErrorCode,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            i4TestValDot1agCfmMepCciEnabled);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepCcmLtmPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepCcmLtmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepCcmLtmPriority (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     UINT4 u4TestValDot1agCfmMepCcmLtmPriority)
{

    return EcfmMepUtlTstAgMepCcmLtmPri (pu4ErrorCode,
                                        u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4TestValDot1agCfmMepCcmLtmPriority);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepLowPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepLowPrDef
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepLowPrDef (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 i4TestValDot1agCfmMepLowPrDef)
{
    return EcfmMepUtlTestv2AgMepLowPrDef (pu4ErrorCode,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          i4TestValDot1agCfmMepLowPrDef);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepFngAlarmTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepFngAlarmTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepFngAlarmTime (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4 i4TestValDot1agCfmMepFngAlarmTime)
{
    return EcfmMepUtlTstAgMepFngAlmTime (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         i4TestValDot1agCfmMepFngAlarmTime);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepFngResetTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepFngResetTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepFngResetTime (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4 i4TestValDot1agCfmMepFngResetTime)
{
    return EcfmMepUtlTstAgMepFngRstTime (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         i4TestValDot1agCfmMepFngResetTime);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        INT4
                                        i4TestValDot1agCfmMepTransmitLbmStatus)
{
    if (i4TestValDot1agCfmMepTransmitLbmStatus == ECFM_SNMP_TRUE)
    {
        i4TestValDot1agCfmMepTransmitLbmStatus = ECFM_TX_STATUS_START;
    }
    else
    {
        i4TestValDot1agCfmMepTransmitLbmStatus = ECFM_TX_STATUS_STOP;
    }

    return EcfmMepUtlTestv2AgMepTxLbmStatus (pu4ErrorCode,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             i4TestValDot1agCfmMepTransmitLbmStatus);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmDestMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmDestMacAddress (UINT4 *pu4ErrorCode,
                                                UINT4 u4Dot1agCfmMdIndex,
                                                UINT4 u4Dot1agCfmMaIndex,
                                                UINT4 u4Dot1agCfmMepIdentifier,
                                                tMacAddr
                                                TestValDot1agCfmMepTransmitLbmDestMacAddress)
{
    return EcfmMepUtlTstAgMepTxLbmDstMacAddr (pu4ErrorCode,
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              TestValDot1agCfmMepTransmitLbmDestMacAddress);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmDestMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmDestMepId (UINT4 *pu4ErrorCode,
                                           UINT4 u4Dot1agCfmMdIndex,
                                           UINT4 u4Dot1agCfmMaIndex,
                                           UINT4 u4Dot1agCfmMepIdentifier,
                                           UINT4
                                           u4TestValDot1agCfmMepTransmitLbmDestMepId)
{
    return EcfmMepUtlTstAgMepTxLbmDestMepId (pu4ErrorCode,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             u4TestValDot1agCfmMepTransmitLbmDestMepId);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmDestIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmDestIsMepId (UINT4 *pu4ErrorCode,
                                             UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             INT4
                                             i4TestValDot1agCfmMepTransmitLbmDestIsMepId)
{
    return EcfmMepUtlTstAgMepTxLbmDstIsMepId (pu4ErrorCode,
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4TestValDot1agCfmMepTransmitLbmDestIsMepId);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmMessages
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmMessages (UINT4 *pu4ErrorCode,
                                          UINT4 u4Dot1agCfmMdIndex,
                                          UINT4 u4Dot1agCfmMaIndex,
                                          UINT4 u4Dot1agCfmMepIdentifier,
                                          INT4
                                          i4TestValDot1agCfmMepTransmitLbmMessages)
{
    return EcfmMepUtlTstAgMepTxLbmMessages (pu4ErrorCode,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            i4TestValDot1agCfmMepTransmitLbmMessages);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmDataTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDataTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmDataTlv (UINT4 *pu4ErrorCode,
                                         UINT4 u4Dot1agCfmMdIndex,
                                         UINT4 u4Dot1agCfmMaIndex,
                                         UINT4 u4Dot1agCfmMepIdentifier,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValDot1agCfmMepTransmitLbmDataTlv)
{
    return EcfmMepUtlTstAgMepTxLbmDataTlv (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           pTestValDot1agCfmMepTransmitLbmDataTlv);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmVlanPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmVlanPriority (UINT4 *pu4ErrorCode,
                                              UINT4 u4Dot1agCfmMdIndex,
                                              UINT4 u4Dot1agCfmMaIndex,
                                              UINT4 u4Dot1agCfmMepIdentifier,
                                              INT4
                                              i4TestValDot1agCfmMepTransmitLbmVlanPriority)
{
    return EcfmMepUtlTstAgMepTxLbmVlanPri (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           i4TestValDot1agCfmMepTransmitLbmVlanPriority);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLbmVlanDropEnable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmVlanDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLbmVlanDropEnable (UINT4 *pu4ErrorCode,
                                                UINT4 u4Dot1agCfmMdIndex,
                                                UINT4 u4Dot1agCfmMaIndex,
                                                UINT4 u4Dot1agCfmMepIdentifier,
                                                INT4
                                                i4TestValDot1agCfmMepTransmitLbmVlanDropEnable)
{
    return EcfmMepUtlTstAgMepTxLbmVlanDropEna (pu4ErrorCode,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               i4TestValDot1agCfmMepTransmitLbmVlanDropEnable);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        INT4
                                        i4TestValDot1agCfmMepTransmitLtmStatus)
{
    if (i4TestValDot1agCfmMepTransmitLtmStatus == ECFM_SNMP_TRUE)
    {
        i4TestValDot1agCfmMepTransmitLtmStatus = ECFM_TX_STATUS_START;
    }
    else
    {
        i4TestValDot1agCfmMepTransmitLtmStatus = ECFM_TX_STATUS_STOP;
    }

    return EcfmMepUtlTestv2AgMepTxLtmStatus (pu4ErrorCode,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             i4TestValDot1agCfmMepTransmitLtmStatus);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmFlags
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmFlags (UINT4 *pu4ErrorCode,
                                       UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 u4Dot1agCfmMepIdentifier,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValDot1agCfmMepTransmitLtmFlags)
{
    return EcfmMepUtlTestv2AgMepTxLtmFlags (pu4ErrorCode,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            pTestValDot1agCfmMepTransmitLtmFlags);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTargetMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmTargetMacAddress (UINT4 *pu4ErrorCode,
                                                  UINT4 u4Dot1agCfmMdIndex,
                                                  UINT4 u4Dot1agCfmMaIndex,
                                                  UINT4
                                                  u4Dot1agCfmMepIdentifier,
                                                  tMacAddr
                                                  TestValDot1agCfmMepTransmitLtmTargetMacAddress)
{
    return EcfmMepUtlTstAgMepTxLtmTgtMacAddr (pu4ErrorCode,
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              TestValDot1agCfmMepTransmitLtmTargetMacAddress);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmTargetMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTargetMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmTargetMepId (UINT4 *pu4ErrorCode,
                                             UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             UINT4
                                             u4TestValDot1agCfmMepTransmitLtmTargetMepId)
{
    return EcfmMepUtlTstAgMepTxLtmTgtMepId (pu4ErrorCode,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4TestValDot1agCfmMepTransmitLtmTargetMepId);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTargetIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmTargetIsMepId (UINT4 *pu4ErrorCode,
                                               UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               UINT4 u4Dot1agCfmMepIdentifier,
                                               INT4
                                               i4TestValDot1agCfmMepTransmitLtmTargetIsMepId)
{
    return EcfmMepUtlTstAgMepTxLtmTgtIsMepId (pu4ErrorCode,
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4TestValDot1agCfmMepTransmitLtmTargetIsMepId);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmTtl (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4 u4Dot1agCfmMepIdentifier,
                                     UINT4 u4TestValDot1agCfmMepTransmitLtmTtl)
{
    return EcfmMepUtlTestv2AgMepTxLtmTtl (pu4ErrorCode,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          u4TestValDot1agCfmMepTransmitLtmTtl);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmEgressIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepTransmitLtmEgressIdentifier (UINT4 *pu4ErrorCode,
                                                  UINT4 u4Dot1agCfmMdIndex,
                                                  UINT4 u4Dot1agCfmMaIndex,
                                                  UINT4
                                                  u4Dot1agCfmMepIdentifier,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValDot1agCfmMepTransmitLtmEgressIdentifier)
{
    return EcfmMepUtlTstAgMepTxLtmEgrId (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         pTestValDot1agCfmMepTransmitLtmEgressIdentifier);

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMepRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMepRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4TestValDot1agCfmMepRowStatus)
{
    return EcfmMepUtlTestv2AgMepRowStatus (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           i4TestValDot1agCfmMepRowStatus);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmMepTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmMepTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1agCfmLtrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmLtrTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmLtrTable (UINT4 u4Dot1agCfmMdIndex,
                                           UINT4 u4Dot1agCfmMaIndex,
                                           UINT4 u4Dot1agCfmMepIdentifier,
                                           UINT4 u4Dot1agCfmLtrSeqNumber,
                                           UINT4 u4Dot1agCfmLtrReceiveOrder)
{
    return EcfmMepUtlValAgLtrTable (u4Dot1agCfmMdIndex,
                                    u4Dot1agCfmMaIndex,
                                    u4Dot1agCfmMepIdentifier,
                                    u4Dot1agCfmLtrSeqNumber,
                                    u4Dot1agCfmLtrReceiveOrder);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmLtrTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmLtrTable (UINT4 *pu4Dot1agCfmMdIndex,
                                   UINT4 *pu4Dot1agCfmMaIndex,
                                   UINT4 *pu4Dot1agCfmMepIdentifier,
                                   UINT4 *pu4Dot1agCfmLtrSeqNumber,
                                   UINT4 *pu4Dot1agCfmLtrReceiveOrder)
{
    return (nmhGetNextIndexDot1agCfmLtrTable (0, pu4Dot1agCfmMdIndex, 0,
                                              pu4Dot1agCfmMaIndex, 0,
                                              pu4Dot1agCfmMepIdentifier, 0,
                                              pu4Dot1agCfmLtrSeqNumber, 0,
                                              pu4Dot1agCfmLtrReceiveOrder));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmLtrTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                nextDot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                nextDot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder
                nextDot1agCfmLtrReceiveOrder
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmLtrTable (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 *pu4NextDot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 *pu4NextDot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4NextDot1agCfmMepIdentifier,
                                  UINT4 u4Dot1agCfmLtrSeqNumber,
                                  UINT4 *pu4NextDot1agCfmLtrSeqNumber,
                                  UINT4 u4Dot1agCfmLtrReceiveOrder,
                                  UINT4 *pu4NextDot1agCfmLtrReceiveOrder)
{
    return EcfmMepUtlGetNextIndexAgLtrTable (u4Dot1agCfmMdIndex,
                                             pu4NextDot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             pu4NextDot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             pu4NextDot1agCfmMepIdentifier,
                                             u4Dot1agCfmLtrSeqNumber,
                                             pu4NextDot1agCfmLtrSeqNumber,
                                             u4Dot1agCfmLtrReceiveOrder,
                                             pu4NextDot1agCfmLtrReceiveOrder);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrTtl (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                       UINT4 u4Dot1agCfmMepIdentifier,
                       UINT4 u4Dot1agCfmLtrSeqNumber,
                       UINT4 u4Dot1agCfmLtrReceiveOrder,
                       UINT4 *pu4RetValDot1agCfmLtrTtl)
{
    return EcfmMepUtlGetAgLtrTtl (u4Dot1agCfmMdIndex,
                                  u4Dot1agCfmMaIndex,
                                  u4Dot1agCfmMepIdentifier,
                                  u4Dot1agCfmLtrSeqNumber,
                                  u4Dot1agCfmLtrReceiveOrder,
                                  pu4RetValDot1agCfmLtrTtl);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrForwarded
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrForwarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrForwarded (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             INT4 *pi4RetValDot1agCfmLtrForwarded)
{
    return EcfmMepUtlGetAgLtrForwarded (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmLtrSeqNumber,
                                        u4Dot1agCfmLtrReceiveOrder,
                                        pi4RetValDot1agCfmLtrForwarded);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrTerminalMep
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrTerminalMep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrTerminalMep (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4Dot1agCfmLtrSeqNumber,
                               UINT4 u4Dot1agCfmLtrReceiveOrder,
                               INT4 *pi4RetValDot1agCfmLtrTerminalMep)
{
    return EcfmMepUtlGetAgLtrTerminalMep (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          u4Dot1agCfmLtrSeqNumber,
                                          u4Dot1agCfmLtrReceiveOrder,
                                          pi4RetValDot1agCfmLtrTerminalMep);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrLastEgressIdentifier
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrLastEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrLastEgressIdentifier (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4 u4Dot1agCfmLtrSeqNumber,
                                        UINT4 u4Dot1agCfmLtrReceiveOrder,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValDot1agCfmLtrLastEgressIdentifier)
{
    return EcfmMepUtlGetAgLtrLastEgrId (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmLtrSeqNumber,
                                        u4Dot1agCfmLtrReceiveOrder,
                                        pRetValDot1agCfmLtrLastEgressIdentifier);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrNextEgressIdentifier
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrNextEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrNextEgressIdentifier (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4 u4Dot1agCfmLtrSeqNumber,
                                        UINT4 u4Dot1agCfmLtrReceiveOrder,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValDot1agCfmLtrNextEgressIdentifier)
{
    return EcfmMepUtlGetAgLtrNextEgrId (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmLtrSeqNumber,
                                        u4Dot1agCfmLtrReceiveOrder,
                                        pRetValDot1agCfmLtrNextEgressIdentifier);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrRelay
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrRelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrRelay (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 u4Dot1agCfmLtrSeqNumber,
                         UINT4 u4Dot1agCfmLtrReceiveOrder,
                         INT4 *pi4RetValDot1agCfmLtrRelay)
{
    return EcfmMepUtlGetAgLtrRelay (u4Dot1agCfmMdIndex,
                                    u4Dot1agCfmMaIndex,
                                    u4Dot1agCfmMepIdentifier,
                                    u4Dot1agCfmLtrSeqNumber,
                                    u4Dot1agCfmLtrReceiveOrder,
                                    pi4RetValDot1agCfmLtrRelay);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrChassisIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrChassisIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmLtrSeqNumber,
                                    UINT4 u4Dot1agCfmLtrReceiveOrder,
                                    INT4 *pi4RetValDot1agCfmLtrChassisIdSubtype)
{
    return EcfmMepUtlGetAgLtrChassisIdSubtype (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               u4Dot1agCfmLtrSeqNumber,
                                               u4Dot1agCfmLtrReceiveOrder,
                                               pi4RetValDot1agCfmLtrChassisIdSubtype);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrChassisId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrChassisId (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDot1agCfmLtrChassisId)
{
    return EcfmMepUtlGetAgLtrChassisId (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmLtrSeqNumber,
                                        u4Dot1agCfmLtrReceiveOrder,
                                        pRetValDot1agCfmLtrChassisId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrManAddressDomain
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrManAddressDomain (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmLtrSeqNumber,
                                    UINT4 u4Dot1agCfmLtrReceiveOrder,
                                    tSNMP_OID_TYPE *
                                    pRetValDot1agCfmLtrManAddressDomain)
{
    return EcfmMepUtlGetAgLtrManAddressDomain (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               u4Dot1agCfmLtrSeqNumber,
                                               u4Dot1agCfmLtrReceiveOrder,
                                               pRetValDot1agCfmLtrManAddressDomain);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrManAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrManAddress (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 u4Dot1agCfmLtrSeqNumber,
                              UINT4 u4Dot1agCfmLtrReceiveOrder,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDot1agCfmLtrManAddress)
{
    return EcfmMepUtlGetAgLtrManAddress (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         u4Dot1agCfmLtrSeqNumber,
                                         u4Dot1agCfmLtrReceiveOrder,
                                         pRetValDot1agCfmLtrManAddress);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrIngress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrIngress (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           UINT4 u4Dot1agCfmLtrSeqNumber,
                           UINT4 u4Dot1agCfmLtrReceiveOrder,
                           INT4 *pi4RetValDot1agCfmLtrIngress)
{
    return EcfmMepUtlGetAgLtrIngress (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder,
                                      pi4RetValDot1agCfmLtrIngress);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrIngressMac
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngressMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrIngressMac (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 u4Dot1agCfmLtrSeqNumber,
                              UINT4 u4Dot1agCfmLtrReceiveOrder,
                              tMacAddr * pRetValDot1agCfmLtrIngressMac)
{
    return EcfmMepUtlGetAgLtrIngressMac (u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier,
                                         u4Dot1agCfmLtrSeqNumber,
                                         u4Dot1agCfmLtrReceiveOrder,
                                         pRetValDot1agCfmLtrIngressMac);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrIngressPortIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngressPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrIngressPortIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4 u4Dot1agCfmLtrSeqNumber,
                                        UINT4 u4Dot1agCfmLtrReceiveOrder,
                                        INT4
                                        *pi4RetValDot1agCfmLtrIngressPortIdSubtype)
{
    return EcfmMepUtlGetAgLtrIngPortIdSubtype (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               u4Dot1agCfmLtrSeqNumber,
                                               u4Dot1agCfmLtrReceiveOrder,
                                               pi4RetValDot1agCfmLtrIngressPortIdSubtype);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrIngressPortId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngressPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrIngressPortId (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4 u4Dot1agCfmLtrSeqNumber,
                                 UINT4 u4Dot1agCfmLtrReceiveOrder,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValDot1agCfmLtrIngressPortId)
{
    return EcfmMepUtlGetAgLtrIngressPortId (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4Dot1agCfmLtrSeqNumber,
                                            u4Dot1agCfmLtrReceiveOrder,
                                            pRetValDot1agCfmLtrIngressPortId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrEgress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrEgress (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          UINT4 u4Dot1agCfmLtrSeqNumber,
                          UINT4 u4Dot1agCfmLtrReceiveOrder,
                          INT4 *pi4RetValDot1agCfmLtrEgress)
{
    return EcfmMepUtlGetAgLtrEgress (u4Dot1agCfmMdIndex,
                                     u4Dot1agCfmMaIndex,
                                     u4Dot1agCfmMepIdentifier,
                                     u4Dot1agCfmLtrSeqNumber,
                                     u4Dot1agCfmLtrReceiveOrder,
                                     pi4RetValDot1agCfmLtrEgress);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrEgressMac
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgressMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrEgressMac (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             tMacAddr * pRetValDot1agCfmLtrEgressMac)
{
    return EcfmMepUtlGetAgLtrEgressMac (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmLtrSeqNumber,
                                        u4Dot1agCfmLtrReceiveOrder,
                                        pRetValDot1agCfmLtrEgressMac);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrEgressPortIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgressPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrEgressPortIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 u4Dot1agCfmMepIdentifier,
                                       UINT4 u4Dot1agCfmLtrSeqNumber,
                                       UINT4 u4Dot1agCfmLtrReceiveOrder,
                                       INT4
                                       *pi4RetValDot1agCfmLtrEgressPortIdSubtype)
{
    return EcfmMepUtlGetAgLtrEgrPortIdSubtype (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               u4Dot1agCfmLtrSeqNumber,
                                               u4Dot1agCfmLtrReceiveOrder,
                                               pi4RetValDot1agCfmLtrEgressPortIdSubtype);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrEgressPortId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgressPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrEgressPortId (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmLtrSeqNumber,
                                UINT4 u4Dot1agCfmLtrReceiveOrder,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1agCfmLtrEgressPortId)
{
    return EcfmMepUtlGetAgLtrEgressPortId (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           u4Dot1agCfmLtrSeqNumber,
                                           u4Dot1agCfmLtrReceiveOrder,
                                           pRetValDot1agCfmLtrEgressPortId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmLtrOrganizationSpecificTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrOrganizationSpecificTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmLtrOrganizationSpecificTlv (UINT4 u4Dot1agCfmMdIndex,
                                           UINT4 u4Dot1agCfmMaIndex,
                                           UINT4 u4Dot1agCfmMepIdentifier,
                                           UINT4 u4Dot1agCfmLtrSeqNumber,
                                           UINT4 u4Dot1agCfmLtrReceiveOrder,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValDot1agCfmLtrOrganizationSpecificTlv)
{

    return EcfmMepUtlGetAgLtrOrgSpecificTlv (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier,
                                             u4Dot1agCfmLtrSeqNumber,
                                             u4Dot1agCfmLtrReceiveOrder,
                                             pRetValDot1agCfmLtrOrganizationSpecificTlv);
}

/* LOW LEVEL Routines for Table : Dot1agCfmMepDbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmMepDbTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmMepDbTable (UINT4 u4Dot1agCfmMdIndex,
                                             UINT4 u4Dot1agCfmMaIndex,
                                             UINT4 u4Dot1agCfmMepIdentifier,
                                             UINT4
                                             u4Dot1agCfmMepDbRMepIdentifier)
{
    return EcfmMepUtlValAgMepDbTable (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmMepDbRMepIdentifier);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmMepDbTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmMepDbTable (UINT4 *pu4Dot1agCfmMdIndex,
                                     UINT4 *pu4Dot1agCfmMaIndex,
                                     UINT4 *pu4Dot1agCfmMepIdentifier,
                                     UINT4 *pu4Dot1agCfmMepDbRMepIdentifier)
{
    return (nmhGetNextIndexDot1agCfmMepDbTable (0, pu4Dot1agCfmMdIndex, 0,
                                                pu4Dot1agCfmMaIndex, 0,
                                                pu4Dot1agCfmMepIdentifier, 0,
                                                pu4Dot1agCfmMepDbRMepIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmMepDbTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                nextDot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier
                nextDot1agCfmMepDbRMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmMepDbTable (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 *pu4NextDot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 *pu4NextDot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 *pu4NextDot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                    UINT4 *pu4NextDot1agCfmMepDbRMepIdentifier)
{
    return EcfmMepUtlGetNxtIdxAgMepDbTable (u4Dot1agCfmMdIndex,
                                            pu4NextDot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            pu4NextDot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            pu4NextDot1agCfmMepIdentifier,
                                            u4Dot1agCfmMepDbRMepIdentifier,
                                            pu4NextDot1agCfmMepDbRMepIdentifier);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbRMepState
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbRMepState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbRMepState (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                               INT4 *pi4RetValDot1agCfmMepDbRMepState)
{
    return EcfmMepUtlGetAgMepDbRMepState (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          u4Dot1agCfmMepDbRMepIdentifier,
                                          pi4RetValDot1agCfmMepDbRMepState);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbRMepFailedOkTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbRMepFailedOkTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbRMepFailedOkTime (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                      UINT4
                                      *pu4RetValDot1agCfmMepDbRMepFailedOkTime)
{
    return EcfmMepUtlGetAgMepDbRMepFailOkTime (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               u4Dot1agCfmMepDbRMepIdentifier,
                                               pu4RetValDot1agCfmMepDbRMepFailedOkTime);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbMacAddress (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                tMacAddr * pRetValDot1agCfmMepDbMacAddress)
{

    return EcfmMepUtlGetAgMepDbMacAddress (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           u4Dot1agCfmMepDbRMepIdentifier,
                                           pRetValDot1agCfmMepDbMacAddress);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbRdi
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbRdi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbRdi (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                         INT4 *pi4RetValDot1agCfmMepDbRdi)
{
    return EcfmMepUtlGetAgMepDbRdi (u4Dot1agCfmMdIndex,
                                    u4Dot1agCfmMaIndex,
                                    u4Dot1agCfmMepIdentifier,
                                    u4Dot1agCfmMepDbRMepIdentifier,
                                    pi4RetValDot1agCfmMepDbRdi);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbPortStatusTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbPortStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbPortStatusTlv (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                   INT4 *pi4RetValDot1agCfmMepDbPortStatusTlv)
{
    return EcfmMepUtlGetAgMepDbPortStatTlv (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4Dot1agCfmMepDbRMepIdentifier,
                                            pi4RetValDot1agCfmMepDbPortStatusTlv);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbInterfaceStatusTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbInterfaceStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbInterfaceStatusTlv (UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4 u4Dot1agCfmMepIdentifier,
                                        UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                        INT4
                                        *pi4RetValDot1agCfmMepDbInterfaceStatusTlv)
{
    return EcfmMepUtlGetAgMepDbInterfaceStatTlv (u4Dot1agCfmMdIndex,
                                                 u4Dot1agCfmMaIndex,
                                                 u4Dot1agCfmMepIdentifier,
                                                 u4Dot1agCfmMepDbRMepIdentifier,
                                                 pi4RetValDot1agCfmMepDbInterfaceStatusTlv);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbChassisIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbChassisIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                      INT4
                                      *pi4RetValDot1agCfmMepDbChassisIdSubtype)
{
    return EcfmMepUtlGetAgMepDbChassisIdSubtype (u4Dot1agCfmMdIndex,
                                                 u4Dot1agCfmMaIndex,
                                                 u4Dot1agCfmMepIdentifier,
                                                 u4Dot1agCfmMepDbRMepIdentifier,
                                                 pi4RetValDot1agCfmMepDbChassisIdSubtype);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbChassisId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbChassisId (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValDot1agCfmMepDbChassisId)
{
    return EcfmMepUtlGetAgMepDbChassisId (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          u4Dot1agCfmMepDbRMepIdentifier,
                                          pRetValDot1agCfmMepDbChassisId);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbManAddressDomain
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbManAddressDomain (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                      tSNMP_OID_TYPE *
                                      pRetValDot1agCfmMepDbManAddressDomain)
{
    return EcfmMepUtlGetAgMepDbManAddrDom (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           u4Dot1agCfmMepDbRMepIdentifier,
                                           pRetValDot1agCfmMepDbManAddressDomain);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMepDbManAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMepDbManAddress (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1agCfmMepDbManAddress)
{
    return EcfmMepUtlGetAgMepDbManAddress (u4Dot1agCfmMdIndex,
                                           u4Dot1agCfmMaIndex,
                                           u4Dot1agCfmMepIdentifier,
                                           u4Dot1agCfmMepDbRMepIdentifier,
                                           pRetValDot1agCfmMepDbManAddress);
}

/******************************************************************************/
/*                           End  of file cfmmeplw.c                          */
/******************************************************************************/
