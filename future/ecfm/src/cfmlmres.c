/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlmres.c,v 1.16 2015/04/16 06:29:36 siva Exp $
 *
 * Description: This file contains the Functionality of 1 way Loss 
 *              Measurement module.
 *******************************************************************/
#include "cfminc.h"
PRIVATE UINT4 EcfmLmResXmitLmrPdu PROTO ((tEcfmCcPduSmInfo *, tEcfmMacAddr));

/*******************************************************************************
 * Function Name      : EcfmCcClntProcessLmm
 *
 * Description        : This is called to parse the received LMM PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcClntProcessLmm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu,
                      BOOL1 * pbFrwdLmm)
{
    tEcfmCcRxLmmPduInfo *pLmmPduInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmMacAddr        MepMacAddr = {
        ECFM_INIT_VAL
    };
    UINT4               u4TxFCl = ECFM_INIT_VAL;
    UINT4               u4RxFCb = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();
    pLmmPduInfo = &(pPduSmInfo->uPduInfo.Lmm);
    pMepInfo = pPduSmInfo->pMepInfo;
    pStackInfo = pPduSmInfo->pStackInfo;

    /* Parse the PDU after the headers. This info has already been parsed */
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;
    if ((pMepInfo == NULL) || (pMepInfo->pEcfmMplsParams == NULL))
    {
        if (ECFM_CC_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
        {
            return ECFM_FAILURE;
        }
        /* Get the MEP's MAC Address */
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_CC_PORT_INFO
                                   (pStackInfo->u2PortNum)->u4IfIndex,
                                   MepMacAddr);

        /* Check if the Mac Address received in the LMM is same as that of the
         * receiving MEP
         */
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr) !=
            ECFM_SUCCESS)

        {

            /* Discard the DMM frame */
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntParserLmm: "
                         "discarding the received LMM frame\r\n");
            if (ECFM_CC_IS_MHF (pStackInfo))

            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntParserLmm:"
                             "Lmm forwarded in case of MIP\r\n");
                *pbFrwdLmm = ECFM_TRUE;
            }
            return ECFM_FAILURE;
        }
        if (ECFM_CC_IS_MHF (pStackInfo))

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntParserLmm: "
                         "discarding the received LMM frame\r\n");
            return ECFM_FAILURE;
        }
    }

    if (pMepInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParserLmm: "
                     "MEP is not found, discarding the received LMM frame\r\n");
        return ECFM_FAILURE;
    }

    /* Fetch the value of TxFCf from the PDU */
    ECFM_GET_4BYTE (pLmmPduInfo->u4TxFCf, pu1Pdu);

    /* Fetch the value of RxFCb from the PDU */
    ECFM_GET_4BYTE (u4RxFCb, pu1Pdu);
    if (u4RxFCb == ECFM_INIT_VAL)

    {
        if (pMepInfo->pEcfmMplsParams == NULL)
        {

            /* Hardware has not filled the value of the counters.
             * Fetch the same from VLAN module
             */
            EcfmGetPacketCounters (pMepInfo->pPortInfo->u4IfIndex,
                                   pMepInfo->u4PrimaryVidIsid, &u4TxFCl,
                                   &u4RxFCb);
        }
        else
        {
            if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
            {
                EcfmGetMplsPktCnt (pMepInfo, &u4TxFCl, &u4RxFCb);
            }
        }
    }

    pLmmPduInfo->u4RxFCf = u4RxFCb;

    /* Check that the First TLV offset value received in the LMM PDU should be 12 */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_LMM_FIRST_TLV_OFFSET)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParserLmm: LMM frame is discarded as the"
                     " first tlv offset is wrong\r\n");
        ECFM_CC_INCR_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->u2PortNum);
        ECFM_CC_INCR_CTX_RX_BAD_CFM_PDU_COUNT
            (pPduSmInfo->pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }

    /* Received PDU is correct, Transmit the LMR */
    if (EcfmLmResXmitLmrPdu (pPduSmInfo, MepMacAddr) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLbltClntParserLmm: "
                     "failure occurred while responding a LMR for LMM\r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLmResXmitLmrPdu
 *
 * Description        : This routine formats and transmits the LMR PDUs.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      MepMacAddr - Mac Address of the MEP receiving the LMM
 *                      frame.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE UINT4
EcfmLmResXmitLmrPdu (tEcfmCcPduSmInfo * pPduSmInfo, tEcfmMacAddr MepMacAddr)
{
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4RxFCf = ECFM_INIT_VAL;
    UINT4               u4TxFCb = ECFM_INIT_VAL;
    UINT4               u4RxFCl = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT1               u1Opcode = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    u1Opcode = ECFM_OPCODE_LMR;
    u4RxFCf = pPduSmInfo->uPduInfo.Lmm.u4RxFCf;

    if (pMepInfo->pEcfmMplsParams == NULL)
    {
        EcfmGetPacketCounters (pPduSmInfo->pPortInfo->u4IfIndex,
                               pMepInfo->u4PrimaryVidIsid, &u4TxFCb, &u4RxFCl);

    }
    else
    {
        if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
        {
            EcfmGetMplsPktCnt (pMepInfo, &u4TxFCb, &u4RxFCl);
        }
    }

    /* Duplicate the CRU Buffer */
    pDupCruBuf = ECFM_DUPLICATE_CRU_BUF (pPduSmInfo->pBuf);
    if (pDupCruBuf == NULL)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmResXmitLmrPdu: "
                     "failure occurred while duplicating the cru buffer\r\n");
        return ECFM_FAILURE;
    }

    do

    {

        if (pMepInfo->pEcfmMplsParams == NULL)
        {
            /* Changing the Destination addr field as the src address received in the DMM
             * frame*/
            if (ECFM_COPY_OVER_CRU_BUF
                (pDupCruBuf, pPduSmInfo->RxSrcMacAddr, ECFM_INIT_VAL,
                 ECFM_MAC_ADDR_LENGTH) != ECFM_CRU_SUCCESS)

            {
                ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                break;
            }

            /* Move offset by 6 bytes to point it to the Source address field */
            u2Offset = u2Offset + (UINT2) ECFM_MAC_ADDR_LENGTH;

            /* Changing the Source addr field */
            if (ECFM_COPY_OVER_CRU_BUF
                (pDupCruBuf, MepMacAddr, u2Offset,
                 ECFM_MAC_ADDR_LENGTH) != ECFM_CRU_SUCCESS)

            {
                ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                break;
            }
        }

        /* Move offset by 1 byte to point it to Opcode field */
        u2Offset =
            (UINT2) (pPduSmInfo->u1CfmPduOffset +
                     (UINT2) ECFM_MDLEVEL_VER_FIELD_SIZE);

        /*Change the OpCode to LMR */
        if (ECFM_COPY_OVER_CRU_BUF
            (pDupCruBuf, (UINT1 *) &u1Opcode, u2Offset,
             ECFM_OPCODE_FIELD_SIZE) != ECFM_CRU_SUCCESS)

        {
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            break;
        }

        /* The TxFCf field should remain the same as in LMM PDU.
         * Move the pointer to point after TxFCf field 
         */
        u2Offset = u2Offset + (UINT2) (ECFM_OPCODE_FIELD_SIZE) +
            (ECFM_FLAGS_FIELD_SIZE) +
            (ECFM_FIRST_TLV_OFFSET_FIELD_SIZE) + (ECFM_DATA_COUNTER_FIELD_SIZE);

        /* Fill the buffer with RxFCf */
        u4RxFCf = ECFM_HTONL (u4RxFCf);

        if (ECFM_COPY_OVER_CRU_BUF
            (pDupCruBuf, (UINT1 *) &u4RxFCf, u2Offset,
             ECFM_DATA_COUNTER_FIELD_SIZE) != ECFM_CRU_SUCCESS)

        {
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            break;
        }
        u4RxFCf = ECFM_NTOHL (u4RxFCf);

        /* Move the pointer and fill the TxFCb field */
        u2Offset = u2Offset + (UINT2) ECFM_DATA_COUNTER_FIELD_SIZE;
        u4TxFCb = ECFM_HTONL (u4TxFCb);
        if (ECFM_COPY_OVER_CRU_BUF
            (pDupCruBuf, (UINT1 *) &u4TxFCb, u2Offset,
             ECFM_DATA_COUNTER_FIELD_SIZE) != ECFM_CRU_SUCCESS)

        {
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            break;
        }
        u4TxFCb = ECFM_NTOHL (u4TxFCb);
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pDupCruBuf);
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pDupCruBuf, u4PduSize,
                          "EcfmLmResXmitLmrPdu: "
                          "Sending out LMR-PDU to lower layer...\r\n");

        if (pMepInfo->pEcfmMplsParams != NULL)
        {
            if (EcfmMplsTpCcTxPacket (pDupCruBuf, pMepInfo,
                                      ECFM_OPCODE_LMR) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCciSmXmitLmrPdu: Transmit CFMPDU failed\n");
                ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                i4RetVal = ECFM_FAILURE;
            }
        }
        else
        {
            pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
                (UINT2) pMepInfo->u4PrimaryVidIsid;

#ifdef NPAPI_WANTED
            if (ECFM_HW_LMM_SUPPORT () == ECFM_FALSE)
            {
                i4RetVal =
                    EcfmCcCtrlTxTransmitPkt (pDupCruBuf, pMepInfo->u2PortNum,
                                             pMepInfo->u4PrimaryVidIsid, 0, 0,
                                             pMepInfo->u1Direction,
                                             ECFM_OPCODE_LMR,
                                             &(pPduSmInfo->
                                               VlanClassificationInfo),
                                             &(pPduSmInfo->
                                               PbbClassificationInfo));
            }
            else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
            {
                UINT1              *pu1LmrPdu = ECFM_CC_PDU;

                /*Copy the CRU-BUFF into linear buffer */
                ECFM_COPY_FROM_CRU_BUF (pDupCruBuf, pu1LmrPdu, ECFM_INIT_VAL,
                                        u4PduSize);
                if (EcfmFsMiEcfmTransmitLmr
                    (ECFM_CC_CURR_CONTEXT_ID (), pMepInfo->pPortInfo->u4IfIndex,
                     pu1LmrPdu, (UINT2) u4PduSize,
                     &(pPduSmInfo->VlanClassificationInfo.OuterVlanTag),
                     pMepInfo->u1Direction) != FNP_SUCCESS)

                {
                    i4RetVal = ECFM_FAILURE;
                }
                else
                {
                    ECFM_LBLT_INCR_TX_COUNT (pMepInfo->u2PortNum,
                                             ECFM_OPCODE_LMR);
                    ECFM_LBLT_INCR_CTX_TX_COUNT (pMepInfo->u4ContextId,
                                                 ECFM_OPCODE_LMR);
                    ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                    pDupCruBuf = NULL;
                }
            }
#else /*  */
            i4RetVal =
                EcfmCcCtrlTxTransmitPkt (pDupCruBuf, pMepInfo->u2PortNum,
                                         pMepInfo->u4PrimaryVidIsid, 0, 0,
                                         pMepInfo->u1Direction, ECFM_OPCODE_LMR,
                                         &(pPduSmInfo->VlanClassificationInfo),
                                         &(pPduSmInfo->PbbClassificationInfo));
#endif /*  */
        }
    }
    while (0);

    /* Transmit the LMR frame */
    if (i4RetVal != ECFM_SUCCESS)

    {
        ECFM_CC_INCR_TX_FAILED_COUNT (pMepInfo->u2PortNum);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (pMepInfo->u4ContextId);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmResXmitLmrPdu: "
                     "failure occurred while formatting LMR frame\r\n");
        ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return i4RetVal;
}
