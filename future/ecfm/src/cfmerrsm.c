/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmerrsm.c,v 1.25 2015/12/31 11:25:34 siva Exp $
 *
 * Description: This file contains the Functionality of the Remote 
 *              MEP error State Machine.
 *******************************************************************/

#include "cfminc.h"
#include "cfmerrsm.h"

/****************************************************************************
 * Function Name      : EcfmCcClntRmepErrSm
 *
 * Description        : This is the Remote MEP Error State Machine that 
 *                      traverses the state event matrix depending event 
 *                      given to it. 
 *
 * Input(s)           : u1Event - Specifying the event received by this 
 *                                state machine.
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntRmepErrSm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcClntRmepErrSm: Called with Event: %d, and the "
                      "current State: %d\r\n",
                      u1Event, ECFM_CC_RMEP_ERR_GET_STATE (pMepInfo));

    if (ECFM_CC_RMEP_ERR_STATE_MACHINE (u1Event, pCcInfo->u1RMepErrState,
                                        pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmCcClntRmepErrSm:RMEP Error State machine for Mep"
                          "%u does not function Correctly\r\n",
                          pMepInfo->u2MepId);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepErrSmSetStateDefault
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      MepNotActive in which state machine changes its current 
 *                      state to DEFAULT state.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmRmepErrSmSetStateDefault (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    /* if the timer was running then stop the corrosponding timer */
    /* Stop the running Error CCM While timer */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmRmepErrSmSetStateDefault:Unable to stop timer"
                     "\r\n");
        return ECFM_FAILURE;
    }

    /* Errored CCM variable is set to false */
    pCcInfo->b1ErrorCcmDefect = ECFM_FALSE;
    /* Simulate the exit log in case the defect was already present */
    if (pCcInfo->b1UnExpectedMepDefect == ECFM_TRUE)
    {
        /* Add entry for Unexpected Mep Defect exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_MEP_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_MEP_EX_TRAP_VAL);
        pCcInfo->b1UnExpectedMepDefect = ECFM_FALSE;
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    }
    pCcInfo->b1UnExpectedMepDefect = ECFM_FALSE;

    if (pCcInfo->b1UnExpectedPeriodDefect == ECFM_TRUE)
    {
        /* Add entry for Unexpected Period Defect Exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_PERIOD_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_PERIOD_EX_TRAP_VAL);
        pCcInfo->b1UnExpectedPeriodDefect = ECFM_FALSE;
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
    }
    pCcInfo->b1UnExpectedPeriodDefect = ECFM_FALSE;

    EcfmRedCheckAndSyncSmData (&pMepInfo->CcInfo.u1RMepErrState,
                               ECFM_RMEP_ERR_STATE_DEFAULT, pPduSmInfo);

    /*State set to Default */
    ECFM_CC_RMEP_ERR_SET_STATE (pMepInfo, ECFM_RMEP_ERR_STATE_DEFAULT);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepErrSmSetStateDefault:RMEP ERR SEM Moved to Default"
                 "state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/****************************************************************************
 * Function Name      : EcfmRmepErrSmSetStateNoDefect
 *
 * Description        : This routine is used to set the SM to NO_DEFECT state
 *                      from transient idle state on begin event or from the 
 *                      defect state after timer expiry 
 *                      event .In this state Defect variable for Error
 *                      is reinitialize and Last saved wrong CCM is freed.
 * 
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmRmepErrSmSetStateNoDefect (tEcfmCcPduSmInfo * pPduSmInfo)
{
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
#endif
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
#ifdef NPAPI_WANTED
    MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
#endif

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
#ifdef NPAPI_WANTED
    if ((pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE) &&
        (pCcInfo->b1ErrorCcmDefect == ECFM_TRUE))
    {
        EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, NULL);

        /* Invoke hit-me-once so that defect will be raised again. */
        if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
                                 &EcfmHwInfo) != FNP_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcInterruptQueueHandler: NPAPI"
                         "for Hit-me-Once returned"
                         "FAILURE\r\n");
        }
    }
#endif
    /* Stop the errror ccm while timer if running */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmRmepErrSmSetStateNoDefect:Stop Timer FAILED\r\n");
        return ECFM_FAILURE;
    }

    /*Unexpected Mep Exit Trap is raised and Error Log Entry is done when 
     * Y.1731 is enabled */
    if (pCcInfo->b1UnExpectedMepDefect == ECFM_TRUE)
    {
        /* Add entry for Unexpected Mep Defect exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_MEP_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_MEP_EX_TRAP_VAL);
        pCcInfo->b1UnExpectedMepDefect = ECFM_FALSE;
    }

    /*Unexpected Period Exit Trap is raised and Error Log Entry is done when 
     * Y.1731 is enabled */
    if (pCcInfo->b1UnExpectedPeriodDefect == ECFM_TRUE)
    {
        /* Add entry for Unexpected Period Defect Exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_PERIOD_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_PERIOD_EX_TRAP_VAL);
        pCcInfo->b1UnExpectedPeriodDefect = ECFM_FALSE;
    }

    if (pCcInfo->b1ErrorCcmDefect == ECFM_TRUE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmRmepErrSmSetStateNoDefect: ErrorCcmDefect Cleared "
                     "\r\n");
        /* Errored CCM variable is set to false */
        pCcInfo->b1ErrorCcmDefect = ECFM_FALSE;
        pCcInfo->u2ErrorRMepId = ECFM_INIT_VAL;
        /* Send !MAdefectIndication to FNG */
        EcfmCcFngNoMaDefectIndication (pPduSmInfo);
    }

    /* Check if ErrorCcmLastFailure already has frame then free it */
    if (pCcInfo->ErrorCcmLastFailure.pu1Octets != NULL)
    {
        /* Free the variable */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_ERR_CCM_POOL,
                             pCcInfo->ErrorCcmLastFailure.pu1Octets);
        pCcInfo->ErrorCcmLastFailure.pu1Octets = NULL;
        pCcInfo->ErrorCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->CcInfo.u1RMepErrState,
                               ECFM_RMEP_ERR_STATE_NO_DEFFECT, pPduSmInfo);

    /* Set state to ECFM_RMEP_ERR_STATE_NO_DEFFECT */
    ECFM_CC_RMEP_ERR_SET_STATE (pMepInfo, ECFM_RMEP_ERR_STATE_NO_DEFFECT);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepErrSmSetStateNoDefect: ERR SEM Moved to No"
                 "DefectState\r\n");

    /* Required in case of OFFLOADING CCM */
    /* Check if RDI is to be set/reset in Offloaded module */
    if (FsEcfmOffloadSetRDI (pMepInfo) == ECFM_FAILURE)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepErrSmSetStateDefect
 *
 * Description        : This routine is called when invalid CCM is received
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmRmepErrSmSetStateDefect (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    UINT1               u1RecvdIntervalCode = ECFM_INIT_VAL;
    UINT4               u4CcmInterval = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT2               u2CrossCheckDelay = ECFM_INIT_VAL;
    UINT1               u1ByteCount = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    /* get the received CCI Interval */
    u1RecvdIntervalCode =
        (UINT1) (ECFM_CC_GET_CCM_INTERVAL (pPduSmInfo->u1RxFlags));
    ECFM_GET_CCM_INTERVAL (u1RecvdIntervalCode, u4CcmInterval);

    u2CrossCheckDelay = ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;
    if (u2CrossCheckDelay != 0)
    {
        u4CcmInterval = u4CcmInterval * u2CrossCheckDelay;
    }
    else
    {
        u4CcmInterval = u4CcmInterval * ECFM_XCHK_DELAY_DEF_VAL;
    }

    /* Get the remaing Time if timer is laready running */
    ECFM_GET_REMAINING_TIME (ECFM_CC_TMRLIST_ID, &(pCcInfo->
                                                   ErrCCMWhileTimer.
                                                   TimerNode),
                             &u4RemainingTime);

    if (u4RemainingTime != 0)
    {
        /* Convert Time Ticks into MilliSecs */
        u4RemainingTime = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4RemainingTime);
        /* if remaing time is greater than the 3.5 * CciInterval
         * received then start the timer with the remaining time else start timer
         * with the received interval */
        if (u4RemainingTime < u4CcmInterval)
        {
            /* Stop the running Error CCM While timer */
            if (EcfmCcTmrStopTimer (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmRmepErrSmSetStateDefect:Stop Timer FAILED\r\n");
                return ECFM_FAILURE;

            }
            /* start timer */
            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo,
                                     u4CcmInterval) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmRmepErrSmSetStateDefect:Start Timer FAILED\r\n");
                return ECFM_FAILURE;
            }
        }                        /* start the timer with the remaining time */
        else
        {
            /* Stop the running Error CCM While timer */
            if (EcfmCcTmrStopTimer (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmRmepErrSmSetStateDefect:Stop Timer FAILED\r\n");
                return ECFM_FAILURE;
            }
            /* Start timer with Remaining Time interval */
            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo,
                                     u4RemainingTime) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmRmepErrSmSetStateDefect:Start Timer FAILED\r\n");
                return ECFM_FAILURE;
            }
        }
    }                            /* check if timer can be stopped or not */
    else
    {

        /* start the timer */
        if (EcfmCcTmrStartTimer
            (ECFM_CC_TMR_ERR_CCM_WHILE, pPduSmInfo,
             u4CcmInterval) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmRmepErrSmSetStateDefect:Start Timer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }
    /* Copy the received errored frame in CC Info */
    u1ByteCount = (UINT1) ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pBuf);

    /* Allocate the memory to store the errored CCM */
    ECFM_ALLOC_MEM_BLOCK_CC_MEP_ERR_CCM
        (pCcInfo->ErrorCcmLastFailure.pu1Octets);

    if (pCcInfo->ErrorCcmLastFailure.pu1Octets == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmRmepErrSmSetStateDefect:Meme alloc failed FAILED\r\n");
        return ECFM_FAILURE;
    }
    ECFM_COPY_FROM_CRU_BUF (pPduSmInfo->pBuf,
                            pCcInfo->ErrorCcmLastFailure.pu1Octets,
                            ECFM_INIT_VAL, (UINT4) (u1ByteCount));

    /* Update the length of received Error CCM PDU */
    pCcInfo->ErrorCcmLastFailure.u4OctLen = u1ByteCount;
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepErrSmSetStateDefect: ErrorCcmDefect Occurred \r\n");
    /* Generate the SNMP trap for the fault 
     * Whether ECFM Trap will be genearted or not will be
     * checked in GENERATE TRAP FUNCTION */
    ECFM_CC_GENERATE_TRAP (pPduSmInfo, ECFM_ERROR_CCM_TRAP_VAL);

    /* Generate the fault fo the errored CCM */
    pCcInfo->b1ErrorCcmDefect = ECFM_TRUE;
    /* Store the MEP-ID which caused the Error CCM Defect */
    pCcInfo->u2ErrorRMepId = pPduSmInfo->uPduInfo.Ccm.u2MepId;
    /* Call Ma defect indiaction that generate the fault depending upon the
     * priority */
    EcfmCcFngMaDefectIndication (pPduSmInfo, ECFM_DEF_ERROR_CCM);
    /* Increment the defect counter */
    ECFM_CC_INCR_ERROR_CCM_DEFECT_COUNT ();

    EcfmRedCheckAndSyncSmData (&pMepInfo->CcInfo.u1RMepErrState,
                               ECFM_RMEP_ERR_STATE_DEFFECT, pPduSmInfo);

    /*Set state to Defect state */
    ECFM_CC_RMEP_ERR_SET_STATE (pMepInfo, ECFM_RMEP_ERR_STATE_DEFFECT);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepErrSmSetStateDefect: RMEP ERR SEM Moved to Defect "
                 "state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/****************************************************************************
 * Function Name      : EcfmRmepErrSmSetStateDefFrmDef
 *
 * Description        : Set state to DEFECT, Timer is started for keeping track
 *                      for the next invalid CCM and defect is generated after 
 *                      copying the wrong CCM received in the MIB and
 *                      setting the BOOL variable for error CCM.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmRmepErrSmSetStateDefFrmDef (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    if (pCcInfo->ErrorCcmLastFailure.pu1Octets != NULL)
    {
        /* Free the variable */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_ERR_CCM_POOL,
                             pCcInfo->ErrorCcmLastFailure.pu1Octets);
        pCcInfo->ErrorCcmLastFailure.pu1Octets = NULL;
        pCcInfo->ErrorCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }

    if (EcfmRmepErrSmSetStateDefect (pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmRmepErrSmSetStateDefFrmDef:Fault geneartion failed"
                     "\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepErrSmEvtImpossible
 *
 * Description        : This rotuine is used to handle the occurence of event
 *                      which canmt be possible in the current state of the
 *                      state machine.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmRmepErrSmEvtImpossible (tEcfmCcPduSmInfo * pPduSmInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

    UNUSED_PARAM (pPduSmInfo);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmRmepErrSmEvtImpossible:"
                 "IMPOSSIBLE EVENT/STATE Combination Occurred in "
                 "Remote MEP ERR SEMr\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmerrsm.c
 *****************************************************************************/
