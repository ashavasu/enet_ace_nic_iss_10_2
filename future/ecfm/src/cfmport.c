/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmport.c,v 1.52 2016/03/31 10:50:41 siva Exp $
 *
 * Description: This file contains the wrapper function for all the
 *              external APIs called by ECFM module.
 *******************************************************************/

#include "cfminc.h"
#include "utilrand.h"
#ifdef LLDP_WANTED
#include "lldp.h"
#endif

#if defined (Y1564_WANTED) && defined (NPAPI_WANTED)
PRIVATE VOID EcfmGetUnAvailCnt (tEcfmCcMepInfo * pMepInfo, UINT4 * pu4UnAvailCnt);
#endif

/*****************************************************************************/
/* Function Name      : EcfmVcmGetAliasName                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName (u4ContextId, pu1Alias));
}

/*****************************************************************************/
/* Function Name      : EcfmVcmGetContextInfoFromIfIndex                     */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}

#ifdef L2RED_WANTED
#ifdef ELMI_WANTED
/*****************************************************************************/
/* Function Name      : EcfmElmRedRcvPktFromRm                               */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the ELMI*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RM_FAILURE/RM_SUCCESS                                */
/*****************************************************************************/
PUBLIC INT4
EcfmElmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (ElmRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif
#if (defined (IGS_WANTED)) || (defined (MLDS_WANTED))
/*****************************************************************************/
/* Function Name      : EcfmSnoopRedRcvPktFromRm                             */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to SNOOP*/
/*                      task. For other events this function enqueues the    */
/*                      given data to the queue and sends the corresponding  */
/*                      event to SNOOP task.                                 */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event is sent then return     */
/*                      SNOOP_SUCCESS otherwise return SNOOP_FAILURE         */
/*****************************************************************************/
PUBLIC INT4
EcfmSnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (SnoopRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif

#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : EcfmLldpRedRcvPktFromRm                              */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to LLDP */
/*                      task.                                                */
/*                                                                           */
/* Input(s)           : u1Event   - Event to be sent to LLDP                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
PUBLIC INT4
EcfmLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif
/*****************************************************************************/
/* Function Name      : EcfmRmEnqMsgToRmFromAppl                             */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
EcfmRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : EcfmRmGetNodeState                                   */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
EcfmRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : EcfmRmHandleProtocolEvent                         */
/*                                                                           */
/* Description        : This function calls the RM module.                   */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
EcfmRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : EcfmRmRegisterProtocols                              */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
EcfmRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : EcfmRmDegisterProtocols                              */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
EcfmRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_ECFM_APP_ID));
}

/*****************************************************************************/
/* Function Name      : EcfmRmSetBulkUpdatesStatus                           */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}

/*****************************************************************************/
/* Function Name      : EcfmRmReleaseMemoryForMsg                            */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
EcfmRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}
#endif
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanGetFwdPortList                           */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the list of ports to which the pkt is fwded. */
/*                                                                           */
/*    Input(s)            : pFrame- Incoming frame                           */
/*                          u4ContextId - Context Identifier                 */
/*                          u2LocalPort - Port Identifier                    */
/*                          SrcAddr - Source MacAddress to be learnt         */
/*                          DstMacAddr - Destination Mac Address for lookup  */
/*                          VlanId - VlanId of the frame                     */
/*                                                                           */
/*    Output(s)           : FwdPortList- list of port(s) to which the pkt    */
/*                          has to be forwarded                              */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
EcfmVlanGetFwdPortList (UINT4 u4ContextId, UINT2 u2LocalPort, tMacAddr SrcAddr,
                        tMacAddr DestAddr, tVlanId VlanId, tLocalPortListExt
                        FwdPortList)
{
    return (VlanGetFwdPortList (u4ContextId, u2LocalPort, SrcAddr,
                                DestAddr, VlanId, FwdPortList));
}

/*****************************************************************************
 *
 *    Function Name        : EcfmSetPortLockStatus
 *
 *    Description          : This function updates LCK status for Data 
 *                           corresponding to Vlan
 *
 *    Input(s)             : u4IfIndex: Interface Index 
 *                           u2VlanId : Vlan Id for which status to be updated
 *                           b1Status: Status 
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
PUBLIC VOID
EcfmSetPortLockStatus (UINT4 u4IfIndex, UINT4 u4IsidVlanId, BOOL1 b1Status)
{
    if (ECFM_IS_MEP_ISID_AWARE (u4IsidVlanId))
    {
        PbbConfPortIsidLckStatus (u4IfIndex,
                                  ECFM_ISID_INTERNAL_TO_ISID (u4IsidVlanId),
                                  b1Status);
    }
    else
    {
        VlanConfPortVlanLckStatus (u4IfIndex, u4IsidVlanId, b1Status);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanTransmitCfmFrame                         */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVlanTransmitCfmFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                          UINT2 u2Port, tVlanTag VlanTagInfo, UINT1 u1FrameType)
{
    return (VlanTransmitCfmFrame (pBuf, u4ContextId, u2Port, VlanTagInfo,
                                  u1FrameType));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanTransmitCfmFrame                         */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          verify the port is member port of ecfm 	     */
/*			    service vlan                               	     */
/*                                                                           */
/*    Input(s)            :         					     */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          						     */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVlanIsMemberPort (UINT4 u4ContextId, UINT2 u2Port,tVlanTag VlanTagInfo)
{
           return (VlanIsMemberPortForCfm (u4ContextId, u2Port, VlanTagInfo));
}
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanGetVlanInfoFromFrame                         */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get VlanId, priority and DE from the given frame.*/
/*                          In case of PEB, This function should not be      */
/*                          called by the ECFM module in case of the untagged*/
/*                          packet intended for Vlan unaware MEP existing on */
/*                          that port.                                       */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                                                                           */
/*    Output(s)           : *pVlanTagInfo- VlanId, Priority, DE for recvd    */
/*                           frame                                           */
/*                          *pu1IngressAction - VLAN_TRUE (If Port is not    */
/*                           the member port of VLAN and Ingress filtering is*/
/*                           enabled)                                        */
/*                           VLAN_FALSE (If port is member of VLAN)          */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVlanGetVlanInfoFromFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
                              tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                              UINT1 *pu1IngressAction, UINT4 *pu4TagOffSet)
{
    return VlanGetVlanInfoFromFrame (u4ContextId, u2LocalPort, pBuf, pVlanTag,
                                     pu1IngressAction, pu4TagOffSet);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  EcfmGetPacketCounters                           */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the transmission counters for a vlan         */
/*                          present on a particular port.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Index                           */
/*                          u2VlanId - Vlan Id                               */
/*                                                                           */
/*    Output(s)           : pu4TxFCl - Transmission Counter                  */
/*                          pu4RxFCl - Reception Counter                     */
/*                                                                           */
/*    Returns            : NONE                                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
EcfmGetPacketCounters (UINT4 u4IfIndex, UINT4 u4IsidVlanId,
                       UINT4 *pu4TxFCl, UINT4 *pu4RxFCl)
{
    if (ECFM_IS_MEP_ISID_AWARE (u4IsidVlanId))
    {
        PbbGetPortIsidStats (u4IfIndex,
                             ECFM_ISID_INTERNAL_TO_ISID (u4IsidVlanId),
                             pu4TxFCl, pu4RxFCl);
    }
    else
    {
        VlanGetPortStats (u4IfIndex, u4IsidVlanId, pu4TxFCl, pu4RxFCl);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanGetPortEtherType                         */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the VLAN ethertype for a port if configured  */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Port Index                        */
/*                          pu2EtherType - Ether Type configured             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : NONE                                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
EcfmVlanGetPortEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherType)
{
#ifdef VLAN_WANTED
    VlanGetPortEtherType (u4IfIndex, pu2EtherType);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu2EtherType);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmIsStpEnabledInContext                            */
/*                                                                           */
/* Description        : Called by ECFM module to know if STP is enabled      */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 - Yes STP is enabled in the given context          */
/*                      1 - No, STP is NOT enabled in the given context      */
/*****************************************************************************/
PUBLIC INT4
EcfmIsStpEnabledInContext (UINT4 u4ContextId)
{

    INT4                i4RetVal = ECFM_FAILURE;

    /* Check MSTP Enabled Status */
    if (AstIsMstEnabledInContext (u4ContextId))
    {
        i4RetVal = ECFM_SUCCESS;
    }
    /* Check RSTP Enabled Status */
    else if (AstIsRstEnabledInContext (u4ContextId))
    {
        i4RetVal = ECFM_SUCCESS;
    }
    /* Check PVRST Enabled Status */
    else
    {
        if (AstIsPvrstEnabledInContext (u4ContextId))
        {
            i4RetVal = ECFM_SUCCESS;
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : EcfmAstIsMstEnabledInContext                         */
/*                                                                           */
/* Description        : Called by ECFM module to know if MSTP is enabled     */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Mstp is enabled in the given context         */
/*                      0 - No, Mstp is NOT enabled in the given context     */
/*****************************************************************************/
PUBLIC INT4
EcfmAstIsMstEnabledInContext (UINT4 u4ContextId)
{
    return (AstIsMstEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : EcfmErpsApiIsErpsStartedInContext		     */
/*                                                                           */
/* Description        : Called by ECFM module to know if ERPS is enabled     */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes ERPS is enabled in the given context         */
/*                      0 - No, ERPS is NOT enabled in the given context     */
/*****************************************************************************/
PUBLIC INT4
EcfmErpsApiIsErpsStartedInContext (UINT4 u4ContextId)
{
#ifdef ERPS_WANTED
    return (ErpsApiIsErpsStartedInContext (u4ContextId));
#endif
    UNUSED_PARAM (u4ContextId); 
    return OSIX_FALSE;
}


/*****************************************************************************/
/* Function Name      : EcfmL2IwfGetNextValidPortForContext                  */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context as ordered by the HL Port ID. This is used   */
/*                      by the ECFM modules to know the active ports in the  */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                     UINT2 *pu2NextLocalPort,
                                     UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = u4ContextId;
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_ECFM;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : EcfmL2IwfHandleOutgoingPktOnPort           */
/*                                                                           */
/*    Description               : This API is used by ECFM module to tx      */
/*                                packets on a set of port through CFA.      */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u2IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                                  UINT4 u4PktSize, UINT2 u2Protocol,
                                  UINT1 u1EncapType)
{
    return (L2IwfHandleOutgoingPktOnPort (pBuf, u2IfIndex, u4PktSize,
                                          u2Protocol, u1EncapType));
}

/*****************************************************************************/
/* Function Name      : EcfmVcmGetSystemMode                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}
/*****************************************************************************/
/* Function Name      : EcfmVcmGetSystemModeExt                              */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}
/*****************************************************************************/
/* Function Name      : EcfmGenerateRandPseudoBytes                          */
/*                                                                           */
/* Description        : This function calls the opensource pseudo random     */
/*                      generator to get the random number.                  */
/*                                                                           */
/* Input(s)           : pu1Bytes - Random Number                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
EcfmGenerateRandPseudoBytes (UINT1 *pu1Bytes)
{
    UINT4               u4RandValue = ECFM_INIT_VAL;
    u4RandValue = OSIX_RAND (1, ECFM_UINT4_MAX);
    ECFM_PUT_4BYTE (pu1Bytes, u4RandValue);
    return ECFM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : EcfmCfaGetSysMacAddress                         */
/*                                                                          */
/*    Description         : This function returns the Switch's Base Mac     */
/*                          address                                         */
/*                          read from NVRAM                                 */
/*    Input(s)            : None.                                           */
/*                                                                          */
/*    Output(s)           : pSwitchMac - the Switch Mac address.            */
/*                                                                          */
/*    Global Variables Referred : gIssSysGroupInfo.                         */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/*                                                                          */
/* **************************************************************************/
PUBLIC VOID
EcfmCfaGetSysMacAddress (tMacAddr SwitchMac)
{
    CfaGetSysMacAddress (SwitchMac);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : EcfmCfaGetContextMacAddr                        */
/*                                                                          */
/*    Description         : This function returns the Context MAC           */
/*                          address                                         */
/*                          read from NVRAM                                 */
/*    Input(s)            : Context Mac Address                             */
/*                                                                          */
/*    Output(s)           : pSwitchMac - the Context Mac address.           */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/* **************************************************************************/
PUBLIC VOID
EcfmCfaGetContextMacAddr (UINT4 u4ContextId, tMacAddr SwitchMac)
{
    IssGetContextMacAddress (u4ContextId, SwitchMac);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanGetTagLenInFrame                         */
/*                                                                           */
/*    Description         : This function returns the VLAN tag offset from   */
/*                          the incoming frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame     - Incoming frame                      */
/*                                                                           */
/*    Output(s)           : pu4VlanOffset - VLAN tag offset in the incoming  */
/*                                          frame based on the number of     */
/*                                          tags present.                    */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVlanGetTagLenInFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                          UINT4 *pu4VlanOffset)
{
    return VlanGetTagLenInFrame (pFrame, u4IfIndex, pu4VlanOffset);
}

/*****************************************************************************
 *
 *    Function Name        : EcfmCfaGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
PUBLIC INT4
EcfmCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return CfaGetIfInfo (u4IfIndex, pIfInfo);
}

/*****************************************************************************
 *
 *    Function Name       : EcfmCfaCliConfGetIfName
 *
 *    Description         : This function returns Interface name for specified
 *                          interface
 *
 *    Input(s)            : u4IfIndex - Interface Index 
 *
 *    Output(s)           : pi1IfName - Pointer to buffer
 *
 *
 *    Returns            : CLI_SUCCESS if name assigned for pi1IfName
 *                         CLI_FAILURE if name is not assign pi1IfName
 *                         CFA_FAILURE if u4IfIndex is not valid interface
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
#endif
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetInterfaceNameFromIndex
 *    Description         : This function retrieves the interface name 
 *                          for the given interface index.
 *    Input(s)            : u4Index - Interface Index
 *                          pu1Alias - Pointer to interface alias name
 *
 *    Output(s)           : puAlias - Pointer to interface alias name
 *
 *    Returns             : OSIX_SUCCESS if interface is valid
 *                          otherwise OSIX_FAILURE.
 *****************************************************************************/
PUBLIC UINT4
EcfmCfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
    return CfaGetInterfaceNameFromIndex (u4Index, pu1Alias);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmPortRegisterWithIp
 *
 *    DESCRIPTION      : This function register with Ip module (Ipv4 and Ipv6)
 *                       the given interface index.
 *                       EcfmNotifyIpv4IfStatusChange and
 *                       EcfmNotifyIpv6IfStatusChange APIs are used for this
 *                       registration.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmPortRegisterWithIp (VOID)
{
    tNetIpRegInfo       NetRegInfo;
    UINT4               u4Mask = 0;

    /* Register with IPv4 */
    ECFM_MEMSET ((UINT1 *) &NetRegInfo, 0, sizeof (tNetIpRegInfo));
    NetRegInfo.pIfStChng = (VOID *) EcfmNotifyIpv4IfStatusChange;
    NetRegInfo.u2InfoMask = NETIPV4_IFCHG_REQ;
    NetRegInfo.u1ProtoId = ECFM_ID;    /* As ECFM module does not run
                                       above IP module. So there is no
                                       valid protocol Id present to 
                                       register with IP. here one
                                       unused protocol id is used */
    NetRegInfo.pRtChng = NULL;
    NetRegInfo.pProtoPktRecv = NULL;
    /* intialize the IP interface indexs to invalid values */
    ECFM_IPv4_L3IFINDEX = -1;
    ECFM_IPv6_L3IFINDEX = -1;
    if (NetIpv4RegisterHigherLayerProtocol (&NetRegInfo) == NETIPV4_FAILURE)
    {
        return ECFM_FAILURE;
    }
#ifdef IP6_WANTED
    u4Mask = NETIPV6_INTERFACE_PARAMETER_CHANGE;
    if (NetIpv6RegisterHigherLayerProtocol (ECFM_ID, u4Mask,
                                            EcfmNotifyIpv6IfStatusChange)
        == NETIPV6_FAILURE)
    {
        return ECFM_FAILURE;
    }
#else
    UNUSED_PARAM (u4Mask);
#endif /* for IP6_WANTED */
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmPortDeRegisterWithIp
 *
 *    DESCRIPTION      : This function de-register with Ip module (Ipv4 and Ipv6)
 *                       the given interface index.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmPortDeRegisterWithIp (VOID)
{
    /* intialize the IP interface indexs to invalid values */
    ECFM_IPv4_L3IFINDEX = -1;
    ECFM_IPv6_L3IFINDEX = -1;
    /* De-register with IPv4 */
    NetIpv4DeRegisterHigherLayerProtocol (ECFM_ID);

#ifdef IP6_WANTED
    /* De-register with IPv6 */
    if (NetIpv6DeRegisterHigherLayerProtocol (ECFM_ID) == NETIPV6_FAILURE)
    {
        return ECFM_FAILURE;
    }
#endif /* for IP6_WANTED */
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmL2IwfGetTagInfoFromFrame                     */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get VlanId, priority and DE from the given frame.*/
/*                          In case of PEB, This function should not be      */
/*                          called by the ECFM module in case of the untagged*/
/*                          packet intended for Vlan unaware MEP existing on */
/*                          that port.                                       */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                                                                           */
/*    Output(s)           : *pVlanTagInfo- VlanId, Priority, DE for recvd    */
/*                           frame                                           */
/*                          *pu1IngressAction - VLAN_TRUE (If Port is not    */
/*                           the member port of VLAN and Ingress filtering is*/
/*                           enabled)                                        */
/*                           VLAN_FALSE (If port is member of VLAN)          */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfGetTagInfoFromFrame (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                              tPbbTag * pPbbTag, UINT1 *pu1IngressAction,
                              UINT4 *pu4InTagOffSet, UINT4 *pu4TagOffSet,
                              UINT1 u1FrameType, UINT1 *pu1IsSend)
{
    return L2IwfGetTagInfoFromFrame (u4ContextId, u4IfIndex, pBuf, pVlanTag,
                                     pPbbTag, pu1IngressAction, pu4InTagOffSet,
                                     pu4TagOffSet, u1FrameType, pu1IsSend);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmL2IwfTransmitCfmFrame                        */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmL2IwfTransmitCfmFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           tVlanTag VlanTagInfo, tEcfmPbbTag PbbTagInfo,
                           UINT1 u1FrameType)
{
    return (L2IwfTransmitPbbFrame (pBuf, u4IfIndex, &VlanTagInfo,
                                   &PbbTagInfo, u1FrameType));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmPbbGetCbpPortListForIsid                     */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmPbbGetCbpFwdPortListForIsid (UINT4 u4ContextId,
                                 UINT2 u2Port, UINT4 u4Isid,
                                 tLocalPortListExt IfFwdPortList)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UNUSED_PARAM (u2Port);
    SnmpPorts.pu1_OctetList = IfFwdPortList;
    SnmpPorts.i4_Length = sizeof (tLocalPortListExt);
    /* fllowing function returns the CBP  port list */
    return (PbbGetCbpListForIsid (u4ContextId, u4Isid, &SnmpPorts));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmPbbGetBvidForIsid                            */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmPbbGetBvidForIsid (UINT4 u4ContextId,
                       UINT2 u2Port, UINT4 u4Isid, tVlanId * pBvid)
{
    return (PbbGetBvidForIsid (u4ContextId, u2Port, u4Isid, pBvid));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmPbbGetPipWithPortList                        */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmPbbGetPipWithPortList (UINT4 u4ContextId, UINT2 u2Port,
                           tLocalPortListExt InPortList,
                           tLocalPortListExt OutPortList)
{
    return (PbbGetPipWithPortList (u4ContextId, u2Port,
                                   InPortList, OutPortList));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmPbbGetMemberPortsForIsid                     */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmPbbGetMemberPortsForIsid (UINT4 u4ContextId,
                              UINT4 u4Isid, tLocalPortListExt IfFwdPortList)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    SnmpPorts.pu1_OctetList = IfFwdPortList;
    SnmpPorts.i4_Length = sizeof (tLocalPortListExt);
    /* fllowing function returns the member ports of the ISID 
     * based on the component */
    if (PbbGetMemberPortsForIsid (u4ContextId, u4Isid, &SnmpPorts) ==
        PBB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EcfmPbbGetVipIsidWithPortList                    */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
PUBLIC INT1
EcfmPbbGetVipIsidWithPortList (UINT4 u4ContextId, UINT2 u2LocalPort,
                               tLocalPortListExt InPortList, UINT2 *pu2Vip,
                               UINT2 *pu2Pip, UINT4 *pu4Isid,
                               tMacAddr * pBDaAddr)
{
    if (PbbGetVipIsidWithPortList (u4ContextId, u2LocalPort,
                                   InPortList, pu2Vip, pu2Pip, pu4Isid,
                                   pBDaAddr) == PBB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EcfmPbbGetPipVipWithIsid                         */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
PUBLIC INT1
EcfmPbbGetPipVipWithIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                          UINT4 u4Isid, UINT2 *pu2Vip, UINT2 *pu2Pip)
{
    if (PbbGetPipVipWithIsid (u4ContextId, u2LocalPort,
                              u4Isid, pu2Vip, pu2Pip) == PBB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EcfmVcmGetIfIndexFromLocalPort                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Interface Index      */
/*                        from the given Localport and Context-Id.           */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u2LocalPort    - LocalPort.                        */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex     - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : ECFM_SUCCESS / ECFM_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmVcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                                UINT4 *pu4IfIndex)
{
    if (VcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPort,
                                    pu4IfIndex) != VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmPbbGetCnpMemberPortsForIsid                  */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmPbbGetCnpMemberPortsForIsid (UINT4 u4ContextId,
                                 UINT4 u4Isid, tLocalPortListExt IfFwdPortList)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    SnmpPorts.pu1_OctetList = IfFwdPortList;
    SnmpPorts.i4_Length = sizeof (tLocalPortListExt);
    if (PbbGetCnpMemberPortsForIsid (u4ContextId, u4Isid,
                                     &SnmpPorts) == PBB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmPbbMiIsIsidMemberPort                            */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a member of the given ISID in the given      */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : ContextId, u4IsidIntr, Global IfIndex                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
PUBLIC              BOOL1
EcfmPbbMiIsIsidMemberPort (UINT4 u4ContextId, UINT4 u4IsidIntr, UINT2 u2Port)
{
    UINT4               u4Isid;
    u4Isid = ECFM_ISID_INTERNAL_TO_ISID (u4IsidIntr);
    return (PbbIsIsidMemberPort (u4ContextId, u4Isid, u2Port));
}

/*****************************************************************************/
/* Function Name      : EcfmVlanIsDeiBitSet                                  */
/*                                                                           */
/* Description        : This routine is called to check whether the Dei Bit  */
/*                      is set True of False.                                */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
PUBLIC              BOOL1
EcfmVlanIsDeiBitSet (UINT4 u4IfIndex)
{
    return (VlanIsDeiBitSet (u4IfIndex));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetBCompBDA                                   */
/*                                                                          */
/*    Description        : This function is used to get the BDA for the     */
/*                         PDU being generated from CBP for B-COMP          */
/*                                                                          */
/*    Input(s)           : u4ContextId - Context ID                         */
/*                         u2LocalPort - local port of the CBP              */
/*                         u4Isid - ISID                                    */
/*                         u1Dir - Direction of the frame                   */
/*                                 PBB_INGRESS - for frame coming from      */
/*                                               PIP                        */
/*                                 PBB_EGRESS  - for frame coming from      */
/*                                               PNP using the reply        */
/*                         b1UseDefaultBDA - Boolean to indicate whether    */
/*                                           configured BDA can be used     */
/*    Output(s)          : BDA - BDA mac address                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
EcfmPbbGetBCompBDA (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 u4Isid,
                    tMacAddr BDA, UINT1 u1Dir, BOOL1 b1UseDefaultBDA)
{
    PbbGetBCompBDA (u4ContextId, u2LocalPort, u4Isid, BDA, u1Dir,
                    b1UseDefaultBDA);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanProcessPktForCep                         */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          apply the PB rules for a CFM-PDU received        */
/*                          on a CEP.                                        */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u2LocalPort- Port on which CFM-PDU was received. */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : pBuf     - CRU Buffer recieved.                  */
/*                          pVlanTag - Classified Vlan Information.          */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS/                                     */
/*                         ECFM_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
EcfmVlanProcessPktForCep (UINT4 u4ContextId, UINT2 u2LocalPort,
                          tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag)
{
    if (VlanProcessPktForCep (u4ContextId, u2LocalPort, pBuf, pVlanTag) !=
        VLAN_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EcfmPbbGetfirstIsidForCBP                            */
/*                                                                          */
/*    Description        : This function is used to get ISID mapped to a CBP*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
EcfmPbbGetfirstIsidForCBP (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 *pu4Isid)
{
    if (PbbGetNextIsidForCBP (u4ContextId, u2LocalPort,
                              0, pu4Isid) == PBB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EcfmPbbGetNextIsidForCBP                            */
/*                                                                          */
/*    Description        : This function is used to get ISID mapped to a CBP*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
EcfmPbbGetNextIsidForCBP (UINT4 u4ContextId, UINT2 u2LocalPort,
                          UINT4 u4CurrIsid, UINT4 *pu4NextIsid)
{
    if (PbbGetNextIsidForCBP (u4ContextId, u2LocalPort,
                              u4CurrIsid, pu4NextIsid) == PBB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmL2IwfPbbTxFrameToIComp                       */
/*                                                                           */
/*    Description         : This function is called when an B-Tagged frame   */
/*                          arrives on PNP in an IB Bridge, and the frame    */
/*                          consists S-Tag also. Then the frame need to be   */
/*                          sent to I Component bridge                       */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                          u2LocalPort - Local Port                         */
/*                          pVlanTag - Vlan Tag Information                  */
/*                          pPbbTag - Pbb Tag Information                    */
/*                          pu4ITagOffset - offset of the protocol PDU in    */
/*                          buf                                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : L2IWF_SUCCESS/                                    */
/*                         L2IWF_FAILURE                                     */
/*                                                                           */
/*****************************************************************************/
INT4
EcfmL2IwfPbbTxFrameToIComp (UINT4 u4ContextId, UINT4 u4IfIndex,
                            tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                            tPbbTag * pPbbTag, UINT4 *pu4ITagOffset,
                            UINT1 u1FrameType, UINT2 u2LocalPort)
{
    if (L2IwfPbbTxFrameToIComp (u4ContextId, u4IfIndex, pBuf, pVlanTag,
                                pPbbTag, pu4ITagOffset, u1FrameType,
                                u2LocalPort) != L2IWF_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************/
/* Function Name      : L2IwfTransmitFrameOnVip                               */
/*                                                                            */
/* Description        : This routine is used to forward the CFM-PDU on VIP    */
/*                      port                                                  */
/*                                                                            */
/* Input(s)           :u4ContextId:- Context Id                               */
/*                    u4IfIndex :- VIP IfIndex                                */
/*                    pBuf:- Buffer to be transmitted                         */
/*                    pVlanTag:- Vlan Tag Info                                */
/*                    pPbbTag:- BVLAN and ISID tag information                */
/*                    u1FrameType:- Frame Type to be tranmitted               */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                           */
/**************************************************************************** */
INT4
EcfmL2IwfTransmitFrameOnVip (UINT4 u4ContextId, UINT4 u4IfIndex,
                             tCRU_BUF_CHAIN_DESC * pBuf,
                             tVlanTag * pVlanTag, tPbbTag * pPbbTag,
                             UINT1 u1FrameType)
{
    if (L2IwfTransmitFrameOnVip (u4ContextId, u4IfIndex,
                                 pBuf, pVlanTag, pPbbTag,
                                 u1FrameType) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EcfmVcmSispGetPhysicalPortOfSispPort             */
/*                                                                          */
/*    Description        : This function will provide the physical interface*/
/*                         index over which the given logical port runs.    */
/*                         This will be invoked by ECFM to obtain the       */
/*                         corresponding physical interface index over which*/
/*                         a logical port runs.                             */
/*                                                                          */
/*    Input(s)           : u4IfIndex   - Interface index.                   */
/*                                                                          */
/*    Output(s)          : pu4PhyIfIndex - Physical interface index.        */
/*                                                                          */
/*    Returns            : VCM_SUCCESS/VCM_FAILURE.                         */
/****************************************************************************/
INT4
EcfmVcmSispGetPhysicalPortOfSispPort (UINT4 u4IfIndex, UINT4 *pu4PhyIfIndex)
{
    return (VcmSispGetPhysicalPortOfSispPort (u4IfIndex, pu4PhyIfIndex));
}

/*****************************************************************************/
/* Function Name      : EcfmVcmSispGetSispPortsInfoOfPhysicalPort            */
/*                                                                           */
/* Description        : This API will get the set of logical interfaces      */
/*                      running over the given physical interface. This API  */
/*                      will return the logical ports in IfIndex or LocalPort*/
/*                      based on u1RetLocalPorts variable.                   */
/*                                                                           */
/*                      This API can be used to get the number of SISP ports */
/*                      existing for the particular physical port. When      */
/*                      paSispPorts is NULL, SISP port count alone will be   */
/*                      returned.                                            */
/*                                                                           */
/*                      This API will fill up the PortList continously for   */
/*                      number of Sisp Ports in IfIndex based retrieval. For */
/*                      LocalPort based retrieval, bit position corresponding*/
/*                      to context will be set.                              */
/*                      will be invoked by MSTP to obtain the corresponding  */
/*                      physical interface index over which a logical port   */
/*                      runs.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Logical interface index.                 */
/*                                                                           */
/* Output(s)          : pau4SispPorts-Physical interface index over which    */
/*                                    this logical runs.                     */
/*                      pu4PortCnt - No. of ports                            */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
EcfmVcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
                                           UINT1 u1RetLocalPorts,
                                           VOID *paSispPorts,
                                           UINT4 *pu4PortCount)
{
    return (VcmSispGetSispPortsInfoOfPhysicalPort
            (u4PhyIfIndex, u1RetLocalPorts, paSispPorts, pu4PortCount));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmVlanGetCVlanIdList                           */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in the CVID     */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS/ECFM_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
EcfmVlanGetCVlanIdList (UINT4 u4IfIndex, tVlanId SVlanId, tVlanListExt CVlanList)
{
    if (VlanGetCVlanIdList (u4IfIndex, SVlanId, CVlanList) != VLAN_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : EcfmVlanApiForwardOnPortList                    */
/*    Description         : This function transmits the provided packet to   */
/*                          the given port list. This function should also   */
/*                          apply the egress rules for the ports and         */
/*                          add/update/remove VLAN tag of the packet based on*/
/*                          the VLAN configuration before transmitting the   */
/*                          packet.                                          */
/*    Input (s)           : u4ContextId   - Context Identifier               */
/*                          pVlanTag      - Pointer to frame's VLAN tag      */
/*                                          information.                     */
/*                          pFrame        - Pointer to the frame to be       */
/*                                          transmitted.                     */
/*                          PortList      - Bit list containing list of pots */
/*                                          on which the frame will be       */
/*                                          transmitted.                     */
/*                          u4InPort      - Port from which the packet       */
/*                                          originated                       */
/*                          DestAddr      - Destination address of the frame.*/
/*    Output(s)           : None                                             */
/*    Returns             : ECFM_SUCCESS/ECFM_FAILURE                        */
/*****************************************************************************/
INT4
EcfmVlanApiForwardOnPortList (UINT4 u4ContextId,
                              UINT4 u4InPort,
                              tVlanTag * pVlanTag,
                              tCRU_BUF_CHAIN_HEADER * pFrame,
                              tLocalPortListExt PortList, tMacAddr DestAddr)
{
    tVlanFwdInfo        VlanFwdInfo;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;

    UNUSED_PARAM (u4InPort);

    ECFM_MEMSET (&VlanFwdInfo, 0x00, sizeof (tVlanFwdInfo));
    pDupBuf = ECFM_DUPLICATE_CRU_BUF (pFrame);
    if (pDupBuf == NULL)
    {
        return ECFM_FAILURE;
    }
    VlanFwdInfo.u4ContextId = u4ContextId;
    VlanFwdInfo.u4InPort = VLAN_INVALID_PORT;
    VlanFwdInfo.pPortList = PortList;

    VlanFwdInfo.u1Action = VLAN_FORWARD_SPECIFIC;
    VlanFwdInfo.u1IsPortListLocal = OSIX_TRUE;
    VlanFwdInfo.u1FrameType = VLAN_CFM_FRAME;

    ECFM_MEMCPY (&(VlanFwdInfo.VlanTag), pVlanTag, sizeof (tVlanTag));
    ECFM_MEMCPY (&(VlanFwdInfo.DestAddr), &DestAddr, sizeof (tMacAddr));
    VlanFwdInfo.u1OverrideSrcMac = OSIX_FALSE;
    VlanApiForwardOnPorts (pDupBuf, &VlanFwdInfo);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmMplsHandleExtInteraction                     */
/*                                                                           */
/*    Description         : This function is the exit point for interacting  */
/*                          with MPLS module.                                */
/*                                                                           */
/*    Input(s)            : u4ReqType - Type of interaction                  */
/*                          pInMplsApiInfo - Pointer to the Request params   */
/*                                                                           */
/*    Output(s)           : pOutMplsApiInfo - Pointer to the response params */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS or OSIX_FAILURE                     */
/*****************************************************************************/
INT4
EcfmMplsHandleExtInteraction (UINT4 u4ReqType,
                              tMplsApiInInfo * pInMplsApiInfo,
                              tMplsApiOutInfo * pOutMplsApiInfo)
{
#ifdef MPLS_WANTED
    return (MplsApiHandleExternalRequest (u4ReqType,
                                          pInMplsApiInfo, pOutMplsApiInfo));
#else
    UNUSED_PARAM (u4ReqType);
    UNUSED_PARAM (pInMplsApiInfo);
    UNUSED_PARAM (pOutMplsApiInfo);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmGetMplsPktCnt                                */
/*                                                                           */
/*    Description         : This function is used to get the Tx and Rx MPLS  */
/*                          Packet count from MPLS module.                   */
/*                                                                           */
/*    Input(s)            : u4ReqType - Type of interaction                  */
/*                          pInMplsApiInfo - Pointer to the Request params   */
/*                                                                           */
/*    Output(s)           : pOutMplsApiInfo - Pointer to the response params */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS or OSIX_FAILURE                     */
/*****************************************************************************/
INT4
EcfmGetMplsPktCnt (tEcfmCcMepInfo * pMepInfo, UINT4 *pu4TxFCf, UINT4 *pu4RxFCf)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    UINT4               u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_INPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                      "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Allocate memory for the Output structure from
     * MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_OUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                      "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->u4ContextId = u4ContextId;

    pMplsApiInInfo->unIntInfo.MplsPktCntInfo.u4PwId =
        pMepInfo->pEcfmMplsParams->MplsPathParams.u4PswId;
    pMplsApiInInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4TxCnt =
        pMepInfo->LmInfo.u4MplsTxCount;
    pMplsApiInInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4RxCnt =
        pMepInfo->LmInfo.u4MplsRxCount;
    ECFM_GET_LMM_INTERVAL (pMepInfo->LmInfo.u2TxLmmInterval,
                           pMplsApiInInfo->unIntInfo.MplsPktCntInfo.
                           u4LmmInterval);

    EcfmMplsHandleExtInteraction (MPLS_GET_PACKET_COUNT,
                                  pMplsApiInInfo, pMplsApiOutInfo);

    *pu4TxFCf = pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4TxCnt;
    *pu4RxFCf = pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4RxCnt;

    pMepInfo->LmInfo.u4MplsTxCount =
        pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsTotDayCnt.u4TxCnt;
    pMepInfo->LmInfo.u4MplsRxCount =
        pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsTotDayCnt.u4RxCnt;

    /* Release Allocated Memory */
    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                        (UINT1 *) pMplsApiOutInfo);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmIncrOutMplsPktCnt                            */
/*                                                                           */
/*    Description         : This function is used to increment the Txed MPLS */
/*                          Packets                                          */
/*                                                                           */
/*    Input(s)            : u4ReqType - Type of interaction                  */
/*                          pInMplsApiInfo - Pointer to the Request params   */
/*                                                                           */
/*    Output(s)           : pOutMplsApiInfo - Pointer to the response params */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS or OSIX_FAILURE                     */
/*****************************************************************************/
INT4
EcfmIncrOutMplsPktCnt (UINT4 u1MplsPathType, UINT4 u4PswId, UINT4 u4ContextId)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    if (u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        /* Allocate memory for the Input structure to
         * MPLS module */
        pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
            (ECFM_MPLSTP_INPARAMS_POOLID);
        if (pMplsApiInInfo == NULL)
        {
            ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                          "Mempool allocation failed\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }

        /* Allocate memory for the Output structure from
         * MPLS module */
        pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
            (ECFM_MPLSTP_OUTPARAMS_POOLID);
        if (pMplsApiOutInfo == NULL)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                          "Mempool allocation failed\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

        /* Fill the MPLS Input structure */
        pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->unIntInfo.MplsPktCntInfo.u4PwId = u4PswId;

        EcfmMplsHandleExtInteraction (MPLS_INCR_OUT_PACKET_COUNT,
                                      pMplsApiInInfo, pMplsApiOutInfo);

        /* Release Allocated Memory */
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    }
    return ECFM_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : EcfmUtilGetMcLagStatus                           */
/*  Description     : The function returns the MC-LAG status of the    */
/*                    node                                             */
/*  Input(s)        : None                                             */
/*  Output(s)       : None                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                        */
/* *********************************************************************/
INT4
EcfmUtilGetMcLagStatus (VOID)
{
#ifdef ICCH_WANTED
    if (LaGetMCLAGSystemStatus() == LA_ENABLED)
    {
         return OSIX_SUCCESS;
    }
#endif
    return OSIX_FAILURE;
}

/***********************************************************************/
/*  Function Name   : EcfmUtilCheckIfIcclInterface                     */
/*  Description     : The function checks if the given interface is an */
/*                    ICCL interface                                   */
/*  Input(s)        : u4Index                                          */
/*  Output(s)       : None                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                        */
/* *********************************************************************/
INT4
EcfmUtilCheckIfIcclInterface (UINT4 u4Index)
{
#ifdef ICCH_WANTED
    if (LaGetMCLAGSystemStatus() == LA_ENABLED)
    {
        if (IcchIsIcclInterface(u4Index) == OSIX_TRUE)
        {
             return OSIX_SUCCESS;
        }
    }
#endif
    UNUSED_PARAM (u4Index);
    return OSIX_FAILURE;
}

/***********************************************************************/
/*  Function Name   : EcfmUtilCheckIfMcLagInterface                    */
/*  Description     : The function checks if the given interface is an */
/*                    MC-LAG interface                                 */
/*  Input(s)        : u4Index                                          */
/*  Output(s)       : None                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                        */
/* *********************************************************************/
INT4
EcfmUtilCheckIfMcLagInterface (UINT4 u4Index)
{
#ifdef ICCH_WANTED
    UINT1        u1McLagInterface = OSIX_FALSE;
    if (LaGetMCLAGSystemStatus() == LA_ENABLED)
    {
        LaApiIsMclagInterface (u4Index, &u1McLagInterface);
        if (u1McLagInterface == OSIX_TRUE)
        {
            return OSIX_SUCCESS;
        }
    }
#endif
    UNUSED_PARAM (u4Index);
    return OSIX_FAILURE;
}


#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)

/*****************************************************************************
 *                                                                           *
 *    Function Name       : EcfmPortHandleExtInteraction                     *
 *                                                                           *
 *    Description         : This function is called by ECFM module to        *
 *                          transmit Ecmp request to the External Module.    *
 *                                                                           *
 *    Input(s)            : pEcfmLbLtMsg - Pointer to LBLT Struct            *
 *                                                                           *
 *    Output(s)           : pY1564ReqParams/pY1564RespParams - Modified      *
 *                                                                           *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE                        *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
EcfmPortHandleExtInteraction (tEcfmLbLtMsg *pEcfmLbLtMsg)
{
#ifdef Y1564_WANTED
    tSNMP_OCTET_STRING_TYPE *pPercentage =NULL;
    tSNMP_OCTET_STRING_TYPE Percentage;
#endif
    tSNMP_OCTET_STRING_TYPE RetDelayValue;
    tEcfmExtInParams        EcfmExtInParams;
    tEcfmExtOutParams       EcfmExtOutParams;
    UINT1                   au1Delay[ECFM_ARRAY_SIZE_8 + 1]; 
#ifdef Y1564_WANTED
    UINT1                   au1Percentage[ECFM_AVLBLTY_RATIO_LENGTH] = { 0 };
    tEcfmLbLtMepInfo        *pMepNode = NULL;
    UINT4                   u4UnAvailCnt;
#endif
    UINT4                   u4CurrentContextId = ECFM_INIT_VAL;
    UINT4                   u4MdIndex = ECFM_INIT_VAL;
    UINT4                   u4MaIndex = ECFM_INIT_VAL;
    UINT4                   u4MepIdentifier = ECFM_INIT_VAL;
    UINT4                   u4NextMdIndex = ECFM_INIT_VAL;
    UINT4                   u4NextMaIndex = ECFM_INIT_VAL;
    UINT4                   u4NextMepIdentifier = ECFM_INIT_VAL;
    UINT4                   u4NextContextId = ECFM_INIT_VAL;
    UINT4                   u4TransId = ECFM_INIT_VAL;
    UINT4                   u4NextTransId = ECFM_INIT_VAL;
    UINT4                   u4Sec = ECFM_INIT_VAL;
    UINT4                   u4NanoSec = ECFM_INIT_VAL;
#ifdef RFC2544_WANTED
	UINT4                   u4DmSent = ECFM_INIT_VAL;
    UINT4                   u4DmrRcvd = ECFM_INIT_VAL;
#endif

#ifdef Y1564_WANTED
    INT4                    i4RetVal = 0;
#endif    
    MEMSET (&RetDelayValue, ECFM_INIT_VAL, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));
    MEMSET (&EcfmExtInParams, ECFM_INIT_VAL, sizeof (tEcfmExtInParams));

    RetDelayValue.pu1_OctetList = au1Delay;
    RetDelayValue.i4_Length = sizeof (au1Delay);

    u4CurrentContextId = pEcfmLbLtMsg->ReqParams.u4ContextId;
    u4MdIndex = pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId;
    u4MaIndex = pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId;
    u4MepIdentifier = pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId;


    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrentContextId) != ECFM_SUCCESS)
    {
       return OSIX_FAILURE;
    }

#ifdef Y1564_WANTED
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex,
                                               (UINT2) u4MepIdentifier);

    if (pMepNode == NULL)

    {
        return OSIX_FAILURE;
    }
#endif

    switch (pEcfmLbLtMsg->MsgType)
    {
#ifdef Y1564_WANTED
        case ECFM_Y1564_REQ_CFM_GET_TEST_REPORT:
        case ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST:
        case ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST:
            u4TransId = pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4TransId; 
#ifdef NPAPI_WANTED
            if (EcfmSlaFetchSlaReport (pEcfmLbLtMsg->MsgType,
                                       &pEcfmLbLtMsg->ReqParams,
                                       &EcfmExtInParams)
                != ECFM_SUCCESS)
            {
                return OSIX_FAILURE;
            }
		
            if (nmhGetNextIndexFsMIY1731FdStatsTable (u4CurrentContextId,
                                                        &u4NextContextId,
                                                        u4MdIndex,
                                                        &u4NextMdIndex,
                                                        u4MaIndex,
                                                        &u4NextMaIndex,
                                                        u4MepIdentifier,
                                                        &u4NextMepIdentifier,
                                                        u4TransId,
                                                        &u4NextTransId)
                == SNMP_SUCCESS)
            {
                if ((u4MdIndex != u4NextMdIndex) || 
                    (u4MaIndex != u4NextMaIndex) ||
                    (u4MepIdentifier != u4NextMepIdentifier) || 
                    (u4CurrentContextId != u4NextContextId))
                {
                    break;
                }

                u4TransId = u4NextTransId;
            }
            else
            {
                return OSIX_FAILURE;
            }

            /* If some Trasaction ID is present Fetch report */
            if (u4TransId != ECFM_INIT_VAL)
            {
                EcfmExtInParams.Y1564ReqParams.u4ContextId = pEcfmLbLtMsg->ReqParams.u4ContextId;
                EcfmExtInParams.Y1564ReqParams.u4SlaId = pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4SlaId;
                EcfmExtInParams.Y1564ReqParams.u4TransId =  u4TransId;

                if ((pEcfmLbLtMsg->MsgType == ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST) ||
                    (pEcfmLbLtMsg->MsgType == ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST))
                {
                    EcfmExtInParams.Y1564ReqParams.u1SlaCurrentTestState = 
                        Y1564_SLA_TEST_ABORTED;
                }
                else
                {
                    EcfmExtInParams.Y1564ReqParams.u1SlaCurrentTestState = 
                        Y1564_SLA_TEST_COMPLETED;
                }

                EcfmExtInParams.Y1564ReqParams.u4ReqType = 
                    ECFM_Y1564_RESULT_RECEIVED;

                nmhGetFsMIY1731FdStatsDelayMin (u4CurrentContextId,
                                                u4MdIndex, u4MaIndex,
                                                u4MepIdentifier,u4TransId,
                                                &RetDelayValue);

                ECFM_Y1564_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                ECFM_Y1564_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                EcfmExtInParams.Y1564ReqParams.ResultInfo.u4StatsFrTxDelayMin =
                    (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));
                MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));

                u4Sec = ECFM_INIT_VAL;
                u4NanoSec = ECFM_INIT_VAL;

                nmhGetFsMIY1731FdStatsDelayAverage (u4CurrentContextId, 
                                                    u4MdIndex, u4MaIndex,
                                                    u4MepIdentifier, u4TransId,
                                                    &RetDelayValue);

                ECFM_Y1564_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                ECFM_Y1564_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                EcfmExtInParams.Y1564ReqParams.ResultInfo.u4StatsFrTxDelayMean =
                    (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));
                MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));

                u4Sec = ECFM_INIT_VAL;
                u4NanoSec = ECFM_INIT_VAL;

                nmhGetFsMIY1731FdStatsDelayMax (u4CurrentContextId,
                                                u4MdIndex,
                                                u4MaIndex,
                                                u4MepIdentifier,
                                                u4TransId,
                                                &RetDelayValue);
                ECFM_Y1564_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                ECFM_Y1564_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                EcfmExtInParams.Y1564ReqParams.ResultInfo.u4StatsFrTxDelayMax =
                    (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));
                MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));

                u4Sec = ECFM_INIT_VAL;
                u4NanoSec = ECFM_INIT_VAL;

                nmhGetFsMIY1731FdStatsFDVAverage (u4CurrentContextId,
                                                  u4MdIndex, u4MaIndex,
                                                  u4MepIdentifier, u4TransId,
                                                  &RetDelayValue);

                ECFM_Y1564_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                ECFM_Y1564_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                EcfmExtInParams.Y1564ReqParams.ResultInfo.u4StatsFrDelayVarMean =
                    (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));

            }

            if (pEcfmLbLtMsg->MsgType != ECFM_R2544_REQ_SLA_TEST_RESULT)
            {

                ECFM_MEMSET (&Percentage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                Percentage.pu1_OctetList = au1Percentage;

                ECFM_CC_LOCK ();
                if (nmhGetFsMIY1731MepAvailabilityPercentage
                    (u4CurrentContextId, u4MdIndex, u4MaIndex, u4MepIdentifier,
                     &Percentage) == SNMP_FAILURE)
                {
                    i4RetVal = OSIX_FAILURE;
                }
                else
                {
                    i4RetVal = OSIX_SUCCESS;
                }

                ECFM_CC_UNLOCK();

                if (i4RetVal == OSIX_SUCCESS)
                {
                    pPercentage = &Percentage;

                    if (ECFM_OCTETSTRING_TO_FLOAT
                        (pPercentage, 
                         EcfmExtInParams.Y1564ReqParams.ResultInfo.f4StatsAvailability)
                          == EOF)
                    {
                        return OSIX_FAILURE;
                    }
                }
                EcfmGetUnAvailCnt ((tEcfmCcMepInfo *) pMepNode->pCcMepInfo, &u4UnAvailCnt);
                EcfmExtInParams.Y1564ReqParams.ResultInfo.u4UnAvailCnt = u4UnAvailCnt;
            }
            return Y1564ApiHandleExtRequest (&EcfmExtInParams.Y1564ReqParams, 
                                             &EcfmExtOutParams.Y1564RespParams);
#else
            UNUSED_PARAM (i4RetVal);
            UNUSED_PARAM (pPercentage);
            UNUSED_PARAM (Percentage);
            UNUSED_PARAM (au1Percentage);
            UNUSED_PARAM (u4UnAvailCnt);
#endif
            break;
#endif
#ifdef RFC2544_WANTED
        case ECFM_R2544_REQ_SLA_TEST_RESULT:

            if ((pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u1R2544SubTest == RFC2544_THROUGHPUT_TEST) || 
                    (pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u1R2544SubTest == RFC2544_FRAMELOSS_TEST) || 
                    (pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u1R2544SubTest == RFC2544_BACKTOBACK_TEST))
            {

#ifdef NPAPI_WANTED
                if (EcfmSlaFetchSlaReport (pEcfmLbLtMsg->MsgType,
                                           &pEcfmLbLtMsg->ReqParams,
                                           &EcfmExtInParams)
                        == ECFM_FAILURE)
                {
                    ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                            "EcfmPortHandleExtInteraction: "
                            "Failed to Fetch statisticis information from NP\r\n");
                    return OSIX_FAILURE;
                }
#endif
            }
            else if (pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u1R2544SubTest == RFC2544_LATENCY_TEST)
            {

                EcfmExtInParams.R2544ReqParams.SlaInfo.u4SlaId =
                    pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4SlaId;
                EcfmExtInParams.R2544ReqParams.u4ContextId =
                    pEcfmLbLtMsg->ReqParams.u4ContextId;
                EcfmExtInParams.R2544ReqParams.u1SubTest =
                    pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u1R2544SubTest;
                EcfmExtInParams.R2544ReqParams.u4FrameSize =
                    pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u2FrameSize;
                EcfmExtInParams.R2544ReqParams.u4ReqType =
                    R2544_LATENCY_TEST_RESULT;
                EcfmExtInParams.R2544ReqParams.LatencyResult.u4TrialCount =
                    pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u4TrialCount;


                u4TransId =  pEcfmLbLtMsg->ReqParams.TrafficProfileParam.u4TrialCount;
                    
                if (nmhGetNextIndexFsMIY1731FdStatsTable (u4CurrentContextId,
                            &u4NextContextId,
                            u4MdIndex,
                            &u4NextMdIndex,
                            u4MaIndex,
                            &u4NextMaIndex,
                            u4MepIdentifier,
                            &u4NextMepIdentifier,
                            u4TransId,
                            &u4NextTransId)
                        == SNMP_SUCCESS)
                {
                    if ((u4MdIndex != u4NextMdIndex) || 
                            (u4MaIndex != u4NextMaIndex) ||
                            (u4MepIdentifier != u4NextMepIdentifier) || 
                            (u4CurrentContextId != u4NextContextId))
                    {
                        break;
                    }

                    u4TransId = u4NextTransId;
                }
                /* If some Trasaction ID is present Fetch report */
                if (u4TransId != ECFM_INIT_VAL)
                {
                        EcfmExtInParams.R2544ReqParams.u4ContextId = pEcfmLbLtMsg->u4ContextId;
                        EcfmExtInParams.R2544ReqParams.SlaInfo.u4SlaId = pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4SlaId;

                        EcfmExtInParams.R2544ReqParams.u4ReqType = 
                            ECFM_R2544_RESULT_RECEIVED;

                        nmhGetFsMIY1731FdStatsDelayMin (u4CurrentContextId,
                                u4MdIndex, u4MaIndex,
                                u4MepIdentifier,u4TransId,
                                &RetDelayValue);

                        ECFM_R2544_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                        ECFM_R2544_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                        EcfmExtInParams.R2544ReqParams.LatencyResult.u4LatencyMin =
                            (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));
                        MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));


                        u4Sec = ECFM_INIT_VAL;
                        u4NanoSec = ECFM_INIT_VAL;
                        nmhGetFsMIY1731FdStatsDelayAverage (u4CurrentContextId, 
                                u4MdIndex, u4MaIndex,
                                u4MepIdentifier, u4TransId,
                                &RetDelayValue);

                        ECFM_R2544_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                        ECFM_R2544_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                        EcfmExtInParams.R2544ReqParams.LatencyResult.u4LatencyMean =
                            (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));
                        MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));

                        u4Sec = ECFM_INIT_VAL;
                        u4NanoSec = ECFM_INIT_VAL;

                        nmhGetFsMIY1731FdStatsDelayMax (u4CurrentContextId,
                                u4MdIndex,
                                u4MaIndex,
                                u4MepIdentifier,
                                u4TransId,
                                &RetDelayValue);
                        ECFM_R2544_GET_4BYTE (u4Sec, RetDelayValue.pu1_OctetList);
                        ECFM_R2544_GET_4BYTE (u4NanoSec, (RetDelayValue.pu1_OctetList + 4));

                        EcfmExtInParams.R2544ReqParams.LatencyResult.u4LatencyMax =
                            (UINT4) ((((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000));
                        MEMSET (&au1Delay, ECFM_INIT_VAL, sizeof (au1Delay));

                        nmhGetFsMIY1731FdStatsDmmOut (u4CurrentContextId, u4MdIndex,
                                u4MaIndex, u4MepIdentifier,
                                u4TransId, &u4DmSent);
                         
                        EcfmExtInParams.R2544ReqParams.LatencyResult.u4TxCount = u4DmSent;

                        nmhGetFsMIY1731FdStatsDmrIn (u4CurrentContextId, u4MdIndex,
                                                     u4MaIndex, u4MepIdentifier,
                                                     u4TransId, &u4DmrRcvd);

                        EcfmExtInParams.R2544ReqParams.LatencyResult.u4RxCount = u4DmrRcvd;

                }
                EcfmExtInParams.R2544ReqParams.u4ReqType = R2544_LATENCY_TEST_RESULT;             
            }

            R2544ApiHandleExtRequest (&EcfmExtInParams.R2544ReqParams, 
                    &EcfmExtOutParams.R2544RespParams);

            break;
#endif

        default:

            /* Unexpected event occured */
            ECFM_GLB_TRC (pEcfmLbLtMsg->u4ContextId, ECFM_CONTROL_PLANE_TRC,
                          "EcfmPortHandleExtInteraction: Invalid event !!! \r\n");
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif

#if defined (Y1564_WANTED) && defined (NPAPI_WANTED)
/*****************************************************************************
 * Function Name      : EcfmGetUnAvailCnt                                    *
 *                                                                           *
 * Description        : The routine gets the UnAvailability count            *
 *                      fot y1564 performance test.                          *
 *                                                                           *
 * Input(s)           : pMepInfo - Ecfm MEP Structure                        *
 *                                                                           *
 * Output(s)          : pu4UnAvailCnt                                        *
 *                                                                           *
 * Returns            : None                                                 *
 *                                                                           *
 *****************************************************************************/
PRIVATE VOID
EcfmGetUnAvailCnt (tEcfmCcMepInfo * pMepInfo, UINT4 *pu4UnAvailCnt)
{
    tEcfmCcAvlbltyInfo *pAvlbltyInfo = NULL;

    pAvlbltyInfo = (tEcfmCcAvlbltyInfo *) pMepInfo->pAvlbltyInfo;

    if (pAvlbltyInfo != NULL)
    {
        *pu4UnAvailCnt = pAvlbltyInfo->u4UnAvailCnt;
    }
    else
    {
        *pu4UnAvailCnt = ECFM_INIT_VAL;
    }
}
#endif

/****************************************************************************
  End of File cfmport.c
 ****************************************************************************/
