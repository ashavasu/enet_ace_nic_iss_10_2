/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbmod.c,v 1.44 2016/02/18 08:32:43 siva Exp $
 *
 * Description: This file contains the module start/shutdown
 *              enable/disable routines
 *******************************************************************/

#include "cfminc.h"
#include "cfmlbext.h"
PRIVATE INT4 EcfmLbLtMdCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtMaCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtMepCmpInGlobal PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtMipCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtRMepDbCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtMipCcmDbCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE VOID EcfmLbLtDeleteGlobalRBTrees PROTO ((VOID));
PRIVATE INT4 EcfmLbLtCreateGlobalRBTrees PROTO ((VOID));
PRIVATE INT4 EcfmLbLtDefaultMdCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtPortMepCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtLbmCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbLtLtmCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmLbModPrimaryVlanCmp PROTO ((tRBElem * pE1, tRBElem * pE2));
PRIVATE INT4 EcfmLbModVlanCmp PROTO ((tRBElem * pE1, tRBElem * pE2));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtY1731Enable                                  
 *                                                                          
 *    DESCRIPTION      : This function is called to Y.1731 funtionality for LBLT
 *                       Task from disabled state.                           
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtY1731Enable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Scan the ECFM enabled ports for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)

    {
        EcfmLbLtY1731EnableForAPort (u2LocalPortNum);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtY1731Disable                                 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable Y.1731 funtionality 
 *                       for LBLT Task.   
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtY1731Disable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Scan the ECFM enabled ports for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)

    {
        if ((ECFM_LBLT_GET_PORT_INFO (u2LocalPortNum) != NULL) &&
            (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (u2LocalPortNum)))

        {
            EcfmLbLtY1731DisableForAPort (u2LocalPortNum);
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtY1731EnableForAPort                                  
 *                                                                          
 *    DESCRIPTION      : This function is called to Y.1731 funtionality for LBLT
 *                       Task from disabled state.                           
 *
 *    INPUT            : PortNum 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtY1731EnableForAPort (UINT2 u2PortNum)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfoNode = NULL;
    tEcfmLbLtMaInfo    *pMaNode = NULL;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    tEcfmLbLtMipInfo    MipNode;

    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);
    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_LBLT_MIP_INFO_SIZE);
    pPortInfoNode = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfoNode != NULL)

    {
        EcfmLbLtModuleDisableForAPort (u2PortNum);
        pPortInfoNode->u1PortEcfmStatus = ECFM_ENABLE;
        pPortInfoNode->u1PortY1731Status = ECFM_ENABLE;

        /* Set Y.1731 Status to enabled */
        ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_ENABLE;

        if (u2PortNum < ECFM_LBLT_MAX_MEP_INFO)
        {
            gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
            /* Set Default values according to Y.1731 */
            pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
            {
                pMepInfo->b1Active = ECFM_TRUE;
                EcfmLbLtUtilY1731EnableMep (pMepInfo);

                pMepInfo = RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                          (tRBElem *) pMepInfo, NULL);
            }
            MipNode.u2PortNum = u2PortNum;
            pMipNode = (tEcfmLbLtMipInfo *) RBTreeGetNext (ECFM_LBLT_MIP_TABLE,
                                                           &MipNode, NULL);
            while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
            {
                pMipNode->b1Active = ECFM_TRUE;
                pMipNode = RBTreeGetNext (ECFM_LBLT_MIP_TABLE,
                                          (tRBElem *) pMipNode, NULL);
            }
        }
        else
        {
            pMaNode = RBTreeGetFirst (ECFM_LBLT_MA_TABLE);
            while (pMaNode != NULL)
            {
                if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pMaNode->u1SelectorType))
                {
                    gpEcfmLbLtMepNode->u4MdIndex = pMaNode->u4MdIndex;
                    gpEcfmLbLtMepNode->u4MaIndex = pMaNode->u4MaIndex;
                    pMepInfo = RBTreeGetFirst (ECFM_LBLT_MEP_TABLE);

                    while ((pMepInfo != NULL) &&
                           ((pMepInfo->u4MdIndex == pMaNode->u4MdIndex) &&
                            (pMepInfo->u4MaIndex == pMaNode->u4MaIndex)))
                    {
                        pMepInfo->b1Active = ECFM_TRUE;
                        EcfmLbLtUtilY1731EnableMep (pMepInfo);
                        gpEcfmLbLtMepNode->u2MepId = pMepInfo->u2MepId;
                        pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                            (ECFM_LBLT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_LBLT_MA_TABLE, pMaNode, NULL);
            }
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtY1731DisableForAPort                                 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable Y.1731 funtionality 
 *                       for LBLT Task.   
 *
 *    INPUT            : PortNum
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtY1731DisableForAPort (UINT2 u2PortNum)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfoNode = NULL;
    tEcfmLbLtMaInfo    *pMaNode = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);
    pPortInfoNode = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfoNode != NULL)

    {
        if (u2PortNum < ECFM_LBLT_MAX_MEP_INFO)
        {
            gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
            pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
            {
                EcfmLbLtUtilY1731DisableMep (pMepInfo);
                pMepInfo = RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                          (tRBElem *) pMepInfo, NULL);
            }
            ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_DISABLE;
            EcfmLbLtModuleEnableForAPort (u2PortNum);
        }
        else
        {
            pMaNode = RBTreeGetFirst (ECFM_LBLT_MA_TABLE);
            while (pMaNode != NULL)
            {
                if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pMaNode->u1SelectorType))
                {
                    gpEcfmLbLtMepNode->u4MdIndex = pMaNode->u4MdIndex;
                    gpEcfmLbLtMepNode->u4MaIndex = pMaNode->u4MaIndex;
                    pMepInfo = RBTreeGetFirst (ECFM_LBLT_MEP_TABLE);

                    while ((pMepInfo != NULL) &&
                           ((pMepInfo->u4MdIndex == pMaNode->u4MdIndex) &&
                            (pMepInfo->u4MaIndex == pMaNode->u4MaIndex)))
                    {
                        EcfmLbLtUtilY1731DisableMep (pMepInfo);

                        gpEcfmLbLtMepNode->u2MepId = pMepInfo->u2MepId;
                        pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                            (ECFM_LBLT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_LBLT_MA_TABLE, pMaNode, NULL);
            }
        }

        /* Set Y.1731 Status to disabled */
        ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_DISABLE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtModuleEnableForAPort
 *
 *    DESCRIPTION      : This function is called to enable ECFM module at 
 *                       particular port.
 *
 *    INPUT            : PortNum
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtModuleEnableForAPort (UINT2 u2PortNum)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfoNode = NULL;
    tEcfmLbLtMaInfo    *pMaNode = NULL;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    tEcfmLbLtMipInfo    MipNode;

    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);
    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_LBLT_MIP_INFO_SIZE);
    pPortInfoNode = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfoNode != NULL)

    {
        pPortInfoNode->u1PortEcfmStatus = ECFM_ENABLE;
        if (u2PortNum < ECFM_LBLT_MAX_MEP_INFO)
        {
            gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
            pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
            {
                pMepInfo->b1Active = ECFM_TRUE;
                EcfmLbLtUtilModuleEnableForAMep (pMepInfo);

                pMepInfo = RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                          (tRBElem *) pMepInfo, NULL);
            }
            MipNode.u2PortNum = u2PortNum;
            pMipNode = (tEcfmLbLtMipInfo *) RBTreeGetNext (ECFM_LBLT_MIP_TABLE,
                                                           &MipNode, NULL);
            while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
            {
                pMipNode->b1Active = ECFM_TRUE;
                pMipNode = RBTreeGetNext (ECFM_LBLT_MIP_TABLE,
                                          (tRBElem *) pMipNode, NULL);
            }
        }
        else
        {
            pPortInfoNode->u1PortY1731Status = ECFM_ENABLE;

            pMaNode = RBTreeGetFirst (ECFM_LBLT_MA_TABLE);
            while (pMaNode != NULL)
            {

                if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pMaNode->u1SelectorType))
                {
                    gpEcfmLbLtMepNode->u4MdIndex = pMaNode->u4MdIndex;
                    gpEcfmLbLtMepNode->u4MaIndex = pMaNode->u4MaIndex;
                    pMepInfo = RBTreeGetFirst (ECFM_LBLT_MEP_TABLE);

                    while ((pMepInfo != NULL) &&
                           ((pMepInfo->u4MdIndex == pMaNode->u4MdIndex) &&
                            (pMepInfo->u4MaIndex == pMaNode->u4MaIndex)))
                    {
                        pMepInfo->b1Active = ECFM_TRUE;
                        EcfmLbLtUtilModuleEnableForAMep (pMepInfo);
                        EcfmLbLtUtilY1731EnableMep (pMepInfo);

                        gpEcfmLbLtMepNode->u2MepId = pMepInfo->u2MepId;
                        pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                            (ECFM_LBLT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_LBLT_MA_TABLE, pMaNode, NULL);
            }
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtModuleDisableForAPort
 *
 *    DESCRIPTION      : This function is called to disable ECFM module at 
 *                       particular port.
 *
 *    INPUT            : PortNum 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtModuleDisableForAPort (UINT2 u2PortNum)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfoNode = NULL;
    tEcfmLbLtMaInfo    *pMaNode = NULL;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    tEcfmLbLtMipInfo    MipNode;

    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);
    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_LBLT_MIP_INFO_SIZE);
    pPortInfoNode = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfoNode != NULL)
    {
        if (u2PortNum < ECFM_LBLT_MAX_MEP_INFO)
        {
            gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
            pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
            {
                EcfmLbLtUtilModuleDisableForAMep (pMepInfo);
                pMepInfo->b1Active = ECFM_FALSE;
                pMepInfo = RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                          (tRBElem *) pMepInfo, NULL);
            }
            MipNode.u2PortNum = u2PortNum;
            pMipNode = (tEcfmLbLtMipInfo *) RBTreeGetNext (ECFM_LBLT_MIP_TABLE,
                                                           &MipNode, NULL);
            while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
            {
                pMipNode->b1Active = ECFM_FALSE;
                pMipNode = RBTreeGetNext (ECFM_LBLT_MIP_TABLE,
                                          (tRBElem *) pMipNode, NULL);
            }
        }
        else
        {
            pMaNode = RBTreeGetFirst (ECFM_LBLT_MA_TABLE);
            while (pMaNode != NULL)
            {
                if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pMaNode->u1SelectorType))
                {
                    gpEcfmLbLtMepNode->u4MdIndex = pMaNode->u4MdIndex;
                    gpEcfmLbLtMepNode->u4MaIndex = pMaNode->u4MaIndex;
                    pMepInfo = RBTreeGetFirst (ECFM_LBLT_MEP_TABLE);

                    while ((pMepInfo != NULL) &&
                           ((pMepInfo->u4MdIndex == pMaNode->u4MdIndex) &&
                            (pMepInfo->u4MaIndex == pMaNode->u4MaIndex)))
                    {
                        EcfmLbLtUtilModuleDisableForAMep (pMepInfo);
                        pMepInfo->b1Active = ECFM_FALSE;
                        gpEcfmLbLtMepNode->u2MepId = pMepInfo->u2MepId;
                        pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                            (ECFM_LBLT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_LBLT_MA_TABLE, pMaNode, NULL);
            }
        }

        /* Update Port ModuleStatus in LBLT Task  */
        pPortInfoNode->u1PortEcfmStatus = ECFM_DISABLE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME    : EcfmLbLtModuleEnable                                
 *                                                                           
 *    DESCRIPTION      : This function is called to enable ECFM module for   
 *                       LBLT Task.                                          
 *                                                                           
 *    INPUT            : None                                                
 *                                                                           
 *    OUTPUT           : None                                                
 *                                                                           
 *    RETURNS          : None
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtModuleEnable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Scan the ECFM enabled ports state machine for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)

    {
        EcfmLbLtModuleEnableForAPort (u2LocalPortNum);
    }
    ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                   "EcfmLbLtModuleEnable: ECFM Module ENABLED \r\n");
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME    : EcfmLbLtModuleDisable                               
 *                                                                           
 *    DESCRIPTION      : This function is called to disable ECFM module for  
 *                       LBLT Task                                          
 *                                                                           
 *    INPUT            : None                                                
 *                                                                           
 *    OUTPUT           : None                                                
 *                                                                           
 *    RETURNS          : None
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtModuleDisable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Scan the ECFM enabled ports for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)

    {
        if ((ECFM_LBLT_GET_PORT_INFO (u2LocalPortNum) != NULL) &&
            (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (u2LocalPortNum)))

        {
            EcfmLbLtModuleDisableForAPort (u2LocalPortNum);
        }
    }
    ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                   "EcfmLbLtModuleDisable: ECFM Module's LBLT Task Disabled \r\n");
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtFrmDelayBuffCmp
 *
 * DESCRIPTION      : Compare function for creating RBTree Embedded.
 *                    in LBLT Global info for FD Buffer 
 * 
 * INPUT            : *pE1 - pointer to RB Node
 *                    *pE2 - Pointer to RB Node 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : -1 represents E1 is less than E2
 *                     1 represents E1 is greater than E2
 *                     0 represents E1 is equals to E2    
 * 
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtFrmDelayBuffCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtFrmDelayBuff *pEcfmEntryA = NULL;
    tEcfmLbLtFrmDelayBuff *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtFrmDelayBuff *) pE1;
    pEcfmEntryB = (tEcfmLbLtFrmDelayBuff *) pE2;

    /* First Index - MD Index of the MEP */
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Second Index - MA Index */
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Third Index - MEP Identifier */
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Fourth Index - Trsansaction Identifier */
    if (pEcfmEntryA->u4TransId != pEcfmEntryB->u4TransId)

    {
        if (pEcfmEntryA->u4TransId < pEcfmEntryB->u4TransId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Fifth Index - Sequence Identifier */
    if (pEcfmEntryA->u4SeqNum != pEcfmEntryB->u4SeqNum)

    {
        if (pEcfmEntryA->u4SeqNum < pEcfmEntryB->u4SeqNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtMepCmpInGlobal
 *
 * DESCRIPTION      : Compare function for creating RBTree Embedded.
 *                    in LBLT Global info indexed by MdIndex, MaIndex, MepId 
 * 
 * INPUT            : *pE1 - pointer to RB Node
 *                    *pE2 - Pointer to RB Node 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : -1 represents E1 is less than E2
 *                     1 represents E1 is greater than E2
 *                     0 represents E1 is equals to E2    
 * 
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtMepCmpInGlobal (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtMepInfo   *pEcfmEntryA = NULL;
    tEcfmLbLtMepInfo   *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtMepInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtMepInfo *) pE2;

    /* First Index - MD Index of the MEP */
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Second Index - MA Index */
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Third Index - MEP Identifier */
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtDefaultMdCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two default MD RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to default MD Node1
 *                       *pE2 - pointer to default MD Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtDefaultMdCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcDefaultMdTableInfo *pEcfmEntryA = NULL;
    tEcfmCcDefaultMdTableInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcDefaultMdTableInfo *) pE1;
    pEcfmEntryB = (tEcfmCcDefaultMdTableInfo *) pE2;
    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)

    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtLtrCmp
 *
 * DESCRIPTION      : Compare function for creating RBTree Embedded.
 *                    for LTR Nodes indexed by mdIndex, maIndex, mepId,
 *                    LtmSeqNumber, LtrReceiveOrder.
 * 
 * INPUT            : *pE1 - pointer to RB Node
 *                    *pE2 - Pointer to RB Node 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : -1 represents E1 is less than E2
 *                     1 represents E1 is greater than E2
 *                     0 represents E1 is equals to E2    
 * 
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtLtrCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtLtrInfo   *pEcfmEntryA = NULL;
    tEcfmLbLtLtrInfo   *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtLtrInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtLtrInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4SeqNum != pEcfmEntryB->u4SeqNum)

    {
        if (pEcfmEntryA->u4SeqNum < pEcfmEntryB->u4SeqNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4RcvOrder != pEcfmEntryB->u4RcvOrder)

    {
        if (pEcfmEntryA->u4RcvOrder < pEcfmEntryB->u4RcvOrder)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtLbmCmp
 *
 * DESCRIPTION      : Compare function for creating RBTree Embedded.
 *                    for LB Init Nodes indexed by mdIndex, maIndex, mepId,
 *                    TransId, SeqNumber.
 * 
 * INPUT            : *pE1 - pointer to RB Node
 *                    *pE2 - Pointer to RB Node 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : -1 represents E1 is less than E2
 *                     1 represents E1 is greater than E2
 *                     0 represents E1 is equals to E2    
 * 
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtLbmCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtLbmInfo   *pEcfmEntryA = NULL;
    tEcfmLbLtLbmInfo   *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtLbmInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtLbmInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4TransId != pEcfmEntryB->u4TransId)

    {
        if (pEcfmEntryA->u4TransId < pEcfmEntryB->u4TransId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4SeqNum != pEcfmEntryB->u4SeqNum)

    {
        if (pEcfmEntryA->u4SeqNum < pEcfmEntryB->u4SeqNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtLtmCmp
 *
 * DESCRIPTION      : Compare function for creating RBTree Embedded.
 *                    for LT Init Nodes indexed by mdIndex, maIndex, mepId,
 *                    SeqNumber.
 * 
 * INPUT            : *pE1 - pointer to RB Node
 *                    *pE2 - Pointer to RB Node 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : -1 represents E1 is less than E2
 *                     1 represents E1 is greater than E2
 *                     0 represents E1 is equals to E2    
 * 
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtLtmCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtLtmReplyListInfo *pEcfmEntryA = NULL;
    tEcfmLbLtLtmReplyListInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtLtmReplyListInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtLtmReplyListInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4LtmSeqNum != pEcfmEntryB->u4LtmSeqNum)

    {
        if (pEcfmEntryA->u4LtmSeqNum < pEcfmEntryB->u4LtmSeqNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtMipCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Mip RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Mip Node1
 *                       *pE2 - pointer to Mip Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtMipCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtMipInfo   *pEcfmEntryA = NULL;
    tEcfmLbLtMipInfo   *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtMipInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtMipInfo *) pE2;
    if (pEcfmEntryA->u2PortNum != pEcfmEntryB->u2PortNum)

    {
        if (pEcfmEntryA->u2PortNum < pEcfmEntryB->u2PortNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4VlanIdIsid != pEcfmEntryB->u4VlanIdIsid)

    {
        if (pEcfmEntryA->u4VlanIdIsid < pEcfmEntryB->u4VlanIdIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtRMepDbCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Remote MEP DB RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Remote MEP DB Node1
 *                       *pE2 - pointer to Remote MEP DB Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtRMepDbCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtRMepDbInfo *pEcfmEntryA = NULL;
    tEcfmLbLtRMepDbInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtRMepDbInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtRMepDbInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2RMepId != pEcfmEntryB->u2RMepId)

    {
        if (pEcfmEntryA->u2RMepId < pEcfmEntryB->u2RMepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtMdCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MD RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MD Node1
 *                       *pE2 - pointer to MD Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtMdCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtMdInfo    *pEcfmEntryA = NULL;
    tEcfmLbLtMdInfo    *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtMdInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtMdInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtMaCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MA RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MA Node1
 *                       *pE2 - pointer to MA Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtMaCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtMaInfo    *pEcfmEntryA = NULL;
    tEcfmLbLtMaInfo    *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtMaInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtMaInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtMipCcmDbCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MIP CCM DB RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MIP CCM DB Node1
 *                       *pE2 - pointer to MIP CCM DB Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtMipCcmDbCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtMipCcmDbInfo *pEcfmEntryA = NULL;
    tEcfmLbLtMipCcmDbInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtMipCcmDbInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtMipCcmDbInfo *) pE2;
    if (pEcfmEntryA->u2Fid != pEcfmEntryB->u2Fid)

    {
        if (pEcfmEntryA->u2Fid < pEcfmEntryB->u2Fid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (ECFM_MEMCMP
        (pEcfmEntryA->SrcMacAddr, pEcfmEntryB->SrcMacAddr,
         ECFM_MAC_ADDR_LENGTH) != 0)

    {
        if (ECFM_MEMCMP
            (pEcfmEntryA->SrcMacAddr, pEcfmEntryB->SrcMacAddr,
             ECFM_MAC_ADDR_LENGTH) < 0)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtPortMepCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two PortMep RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Stack Node1
 *                       *pE2 - pointer to Stack Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtPortMepCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtMepInfo   *pEcfmEntryA = NULL;
    tEcfmLbLtMepInfo   *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmLbLtMepInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtMepInfo *) pE2;

    if (pEcfmEntryA->u2PortNum != pEcfmEntryB->u2PortNum)

    {
        if (pEcfmEntryA->u2PortNum < pEcfmEntryB->u2PortNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)

    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1Direction != pEcfmEntryB->u1Direction)

    {
        if (pEcfmEntryA->u1Direction < pEcfmEntryB->u1Direction)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmLbModVlanCmp          
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Vlan RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Stack Node1
 *                       *pE2 - pointer to Stack Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PRIVATE INT4
EcfmLbModVlanCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtVlanInfo  *pEcfmEntryA = NULL;
    tEcfmLbLtVlanInfo  *pEcfmEntryB = NULL;

    pEcfmEntryA = (tEcfmLbLtVlanInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtVlanInfo *) pE2;

    if (pEcfmEntryA->u4VidIsid != pEcfmEntryB->u4VidIsid)
    {
        if (pEcfmEntryA->u4VidIsid < pEcfmEntryB->u4VidIsid)
        {
            return -1;
        }
        else

        {
            return 1;
        }
    }

    return 0;
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmLbModPrimaryVlanCmp 
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Vlan RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Stack Node1
 *                       *pE2 - pointer to Stack Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PRIVATE INT4
EcfmLbModPrimaryVlanCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmLbLtVlanInfo  *pEcfmEntryA = NULL;
    tEcfmLbLtVlanInfo  *pEcfmEntryB = NULL;

    pEcfmEntryA = (tEcfmLbLtVlanInfo *) pE1;
    pEcfmEntryB = (tEcfmLbLtVlanInfo *) pE2;

    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)
    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    if (pEcfmEntryA->u4VidIsid != pEcfmEntryB->u4VidIsid)
    {
        if (pEcfmEntryA->u4VidIsid < pEcfmEntryB->u4VidIsid)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtModuleStart
 *
 *    DESCRIPTION      : This function allocates memory pools for all tables
 *                       in LbLt Task. It also initalizes the LbLt global
 *                       structure.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtModuleStart ()
{
    if (EcfmL2IwfGetBridgeMode
        (ECFM_LBLT_CURR_CONTEXT_ID (),
         &(ECFM_LBLT_BRIDGE_MODE ())) != L2IWF_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtModuleStart: EcfmL2IwfGetBridgeMode returned failure \r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_LBLT_BRIDGE_MODE () == ECFM_INVALID_BRIDGE_MODE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtModuleStart: Invalid Bridge Mode \r\n");
        return ECFM_FAILURE;
    }

    /* Creating Ports, Updating PortInfo Structure and Creating 
     * MEPTree per Port */
    EcfmLbLtIfCreateAllPorts ();

    /* Create Global RBTrees */
    if (EcfmLbLtCreateGlobalRBTrees () != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtModuleStart: Protocol RBTree Creation "
                       "FAILED  !!!! \r\n");
        return ECFM_FAILURE;
    }

    /* LTR cache */
    ECFM_LBLT_LTR_CACHE_STATUS = ECFM_DISABLE;
    ECFM_LBLT_LTR_CACHE_HOLD_TIME = ECFM_LTR_CACHE_DEF_HOLD_TIME;
    ECFM_LBLT_LTR_CACHE_SIZE = ECFM_LTR_CACHE_DEF_SIZE;

    /* frame delay buffer */
    ECFM_LBLT_FD_BUFFER_SIZE = ECFM_FD_BUFFER_DEF_SIZE;
    if (ECFM_CREATE_MEM_POOL
        (ECFM_LBLT_FRM_DELAY_INFO_SIZE, ECFM_LBLT_FD_BUFFER_SIZE,
         MEM_DEFAULT_MEMORY_TYPE,
         &ECFM_LBLT_FD_BUFFER_POOL) == ECFM_MEM_FAILURE)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtModuleStart: Creation of Mem Pool for "
                       "Frame Delay Buffer FAILED!\n");
        return SNMP_FAILURE;
    }

    /* LBR cache */
    ECFM_LBLT_LBR_CACHE_STATUS = ECFM_ENABLE;
    ECFM_LBLT_CURR_LBR_CACHE_STATUS = ECFM_LBLT_LBR_CACHE_STATUS;
    ECFM_LBLT_LBR_CACHE_SIZE = ECFM_LBR_CACHE_DEF_SIZE;
    ECFM_LBLT_LBR_CACHE_HOLD_TIME = ECFM_LBR_CACHE_DEF_HOLD_TIME;
    if (ECFM_CREATE_MEM_POOL
        (ECFM_LBLT_LBR_INFO_SIZE, ECFM_LBLT_LBR_CACHE_SIZE,
         MEM_DEFAULT_MEMORY_TYPE,
         &ECFM_LBLT_LBR_TABLE_POOL) == ECFM_MEM_FAILURE)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtModuleStart: "
                       "LBR Pool creation failed  !!!! \r\n");
        return ECFM_FAILURE;
    }

    /* Start LBR Cache Hold Timer */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_LBR_HOLD, NULL,
         ECFM_NUM_OF_MSEC_IN_A_SEC * (ECFM_LBLT_LBR_CACHE_HOLD_TIME *
                                      ECFM_NUM_OF_SEC_IN_A_MIN)) !=
        ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtModuleStart: "
                       "LBR Cache Hold Timer Start FAILED\r\n");
        return ECFM_FAILURE;
    }

    /* Start the free running LTF while timer */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_DELAY_QUEUE, NULL,
         ECFM_NUM_OF_MSEC_IN_A_SEC * (ECFM_DELAY_QUEUE_INTERVAL)) !=
        ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtModuleStart: "
                       "LTF While Timer Start FAILED\r\n");
        return ECFM_FAILURE;
    }
    /* Initializing Global Delay Queue Timer */
    EcfmLbLtInitDelayQueue ();
    ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                   "EcfmLbLtModuleStart: ECFM Module's LBLT Task Successfully Started"
                   "!!! \r\n");
    return ECFM_SUCCESS;
}

/***************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtModuleShutDown 
 *                                                                          
 *    DESCRIPTION      : This is function is called during system shutdown or
 *                       when the manager wants to shut the ECFM module.
 *
 *    INPUT            : None.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtModuleShutDown ()
{
    ECFM_LBLT_TRC_FN_ENTRY ();

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        return;
    }
    /* Delete all the dynamic caches */
    /* LTR cache */
    if (EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtModuleShutDown: Stopping LTR Hold Timer Failed \r\n");
    }
    if (ECFM_LBLT_LTR_TABLE_POOL != 0)

    {

        /* Drain the LTR cache */
        RBTreeDrain (ECFM_LBLT_LTR_TABLE,
                     (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                     ECFM_LBLT_LTR_ENTRY);
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_TABLE_POOL);
        ECFM_LBLT_LTR_TABLE_POOL = 0;
    }
    EcfmSnmpLwDelMepLtmReplyList ();

    /* LBR cache */
    if (EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBR_HOLD, NULL) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtModuleShutDown: Stopping LBR Hold Timer Failed \r\n");
    }
    if (ECFM_LBLT_LBR_TABLE_POOL != 0)

    {
        RBTreeDrain (ECFM_LBLT_LBM_TABLE,
                     (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                     ECFM_LBLT_LBM_ENTRY);
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LBR_TABLE_POOL);
        ECFM_LBLT_LBR_TABLE_POOL = 0;
    }

    /* frame delay buffer */
    if (ECFM_LBLT_FD_BUFFER_POOL != 0)

    {
        RBTreeDrain (ECFM_LBLT_FD_BUFFER_TABLE,
                     (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                     ECFM_LBLT_FD_BUFFER_ENTRY);
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_FD_BUFFER_POOL);
        ECFM_LBLT_FD_BUFFER_POOL = ECFM_INIT_VAL;
    }

    /* stop the free running LTF While timer */
    if (EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_DELAY_QUEUE, NULL) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtModuleShutDown: Stopping LTF While Timer Failed \r\n");
    }
    if (ECFM_LBLT_MIP_CCM_DB_POOL != 0)

    {
        RBTreeDrain (ECFM_LBLT_MIP_CCM_DB_TABLE,
                     (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                     ECFM_LBLT_MIP_CCM_DB_ENTRY);
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_MIP_CCM_DB_POOL);
        ECFM_LBLT_MIP_CCM_DB_POOL = 0;
    }

    /* Clear Pending Delay Queue List */
    EcfmLbLtDeInitDelayQueue ();

    /* Remove all ports related info  */
    EcfmLbLtDeleteAllPorts ();

    /* Delete all Global RBTrees */
    EcfmLbLtDeleteGlobalRBTrees ();
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtCreateGlobalRBTrees
 *
 *    DESCRIPTION      : This function creates all the Protocol RBTrees for the 
 *                       LBLT Task.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLbLtCreateGlobalRBTrees ()
{
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Creating RBTree for MepTableIndex,indexed by MdIndex, MaIndex,  MepId */
    ECFM_LBLT_MEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMepInfo, MepGlobalIndexNode),
         EcfmLbLtMepCmpInGlobal);
    if (ECFM_LBLT_MEP_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       " Creation of MEP RBTree for LbLt Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Create RBTree for LTR Table indexed by MdIndex, MaIndex, MepId, LTM
     * Sequence Number, ReceiveOrder */
    ECFM_LBLT_LTR_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtLtrInfo, LtrTableGlobalNode), EcfmLbLtLtrCmp);
    if (ECFM_LBLT_LTR_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       " Creation of LTR RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /*Create RB Tree for LB Init Table */
    ECFM_LBLT_LBM_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtLbmInfo, LbmTableGlobalNode), EcfmLbLtLbmCmp);
    if (ECFM_LBLT_LBM_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       " Creation of LB Init RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /*Create RB Tree for LTM Init Table */
    ECFM_LBLT_LTM_REPLY_LIST = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtLtmReplyListInfo, LtmReplyListGlobalNode),
         EcfmLbLtLtmCmp);
    if (ECFM_LBLT_LTM_REPLY_LIST == NULL)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       " Creation of LT Init RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Create RB Tree for Frame Delay Buffer */
    ECFM_LBLT_FD_BUFFER_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtFrmDelayBuff, FrmDelayBuffGlobalNode),
         EcfmLbLtFrmDelayBuffCmp);
    if (ECFM_LBLT_FD_BUFFER_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       " Creation of FD Buffer RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for Mip Table indexed by IfIndex, level, Vid,
     * direction */
    ECFM_LBLT_MIP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMipInfo, MipGlobalNode), EcfmLbLtMipCmp);
    if (ECFM_LBLT_MIP_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of Mip RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for MepTableIndex,indexed by MdIndex, MaIndex,  MepId and
     * RmepId*/
    ECFM_LBLT_RMEP_DB_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtRMepDbInfo, MepDbGlobalNode),
         EcfmLbLtRMepDbCmp);
    if (ECFM_LBLT_RMEP_DB_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of RMep DB RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    /* Creating RBTree for Default MD Table indexed by Selector type and value */
    ECFM_LBLT_DEF_MD_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtDefaultMdTableInfo, DefaultMdGlobalNode),
         EcfmLbLtDefaultMdCmp);
    if (ECFM_LBLT_DEF_MD_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of Default Md Table FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for indexed by MdIndex, MaIndex
     */
    ECFM_LBLT_MA_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMaInfo, MaTableGlobalNode), EcfmLbLtMaCmp);
    if (ECFM_LBLT_MA_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of MA RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for indexed by MdIndex
     */
    ECFM_LBLT_MD_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMdInfo, MdTableGlobalNode), EcfmLbLtMdCmp);
    if (ECFM_LBLT_MD_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of MD RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_MIP_CCM_DB_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMipCcmDbInfo, MipCcmDbGlobalNode),
         EcfmLbLtMipCcmDbCmp);
    if (ECFM_LBLT_MIP_CCM_DB_TABLE == NULL)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of MIP-CCM RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    /* Creating RBTree for PortMepTable,indexed 
     * by u2PortNum, VlanId, MdLevel, Direction*/
    ECFM_LBLT_PORT_MEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMepInfo, MepPortTableNode),
         EcfmLbLtPortMepCmp);
    if (ECFM_LBLT_PORT_MEP_TABLE == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of PortMep RBTree for LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Create RB Tree for the Vlan Table. This table will be indexed by the
     * VLAN identifier and contains the primary vlan configured for the given
     * vlan identifier.
     * */
    (ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF (tEcfmLbLtVlanInfo,
                                                 VlanGlobalNode),
                                  EcfmLbModVlanCmp);

    if ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable == NULL)
    {
        /* Unable to create the RBTree */
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of Vlan RBTree in LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Create RB Tree for the Primary Vlan Table. This table will be indexed 
     * by the primary VLAN identifier and contains the set of secondary vlan
     * for this particular vlan.
     * */
    (ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF (tEcfmLbLtVlanInfo,
                                                 PrimaryVlanGlobalNode),
                                  EcfmLbModPrimaryVlanCmp);

    if ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable == NULL)
    {
        /* Unable to create the RBTree */
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtCreateGlobalRBTrees:"
                       "Creation of Primary Vlan RBTree in LBLT Task "
                       "FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Create RB-Tree for the MPLS-TP MEP Table. This table consists of MPLS-TP
     * based MEP entries and indexed by 4 tuple for LSP based MEPs and VC-ID 
     * for PW based MEPs
     */
    ECFM_LBLT_MPLSMEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmLbLtMepInfo, MepTableMplsNode),
         EcfmMpTpLbLtMepMplsPathCmp);
    if (ECFM_LBLT_MPLSMEP_TABLE == NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLbLtCreateGlobalRBTrees:"
                     "Creation of Mpls Path Tree for LBLT context FAILED \r\n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * 
 * FUNCTION NAME    : EcfmLbLtDeleteGlobalRBTrees
 *
 * DESCRIPTION      : Delete all RBTrees present in the LbltGlobalInfo
 *                    structure
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ****************************************************************************/
PRIVATE VOID
EcfmLbLtDeleteGlobalRBTrees ()
{
    /* Freeing Memory Assigned to DefaultMd Table */
    if (ECFM_LBLT_DEF_MD_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_LBLT_DEF_MD_TABLE,
                       (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                       ECFM_LBLT_DEF_MD_ENTRY);
        ECFM_LBLT_DEF_MD_TABLE = NULL;
    }
    /* Delete Global LB Init Table in LBLT Task GlobalInfo */
    if (ECFM_LBLT_LBM_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_LBM_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LBM_ENTRY);
        ECFM_LBLT_LBM_TABLE = NULL;
    }

    /* Delete Global LB Init Table in LBLT Task GlobalInfo */
    if (ECFM_LBLT_LTM_REPLY_LIST != NULL)
    {
        RBTreeDestroy (ECFM_LBLT_LTM_REPLY_LIST, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LTM_REPLY_LIST_ENTRY);
        ECFM_LBLT_LTM_REPLY_LIST = NULL;
    }

    if (ECFM_LBLT_FD_BUFFER_TABLE != NULL)

    {

        /* Clear out the Frame Delay Buffer */
        RBTreeDestroy (ECFM_LBLT_FD_BUFFER_TABLE,
                       (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                       ECFM_LBLT_FD_BUFFER_ENTRY);
        ECFM_LBLT_FD_BUFFER_TABLE = NULL;
    }

    /* Delete Global LTR Table in LBLT Tasks GlobalInfo */
    if (ECFM_LBLT_LTR_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_LTR_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LTR_ENTRY);
        ECFM_LBLT_LTR_TABLE = NULL;
    }

    /* Delete Global MIP Table in LBLT Tasks GlobalInfo */
    if (ECFM_LBLT_MIP_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_MIP_TABLE,
                       (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                       ECFM_LBLT_MIP_ENTRY);
        ECFM_LBLT_MIP_TABLE = NULL;
    }

    /* Delete Global MEP RBTree in LBLT Tasks GlobalInfo */
    if (ECFM_LBLT_MEP_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_MEP_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_MEP_ENTRY);
        ECFM_LBLT_MEP_TABLE = NULL;
    }

    /* Delete Global RMEP DB RBTree in LBLT Tasks GlobalInfo */
    if (ECFM_LBLT_RMEP_DB_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_RMEP_DB_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_RMEP_DB_ENTRY);
        ECFM_LBLT_RMEP_DB_TABLE = NULL;
    }

    /* Delete Global MA RBTree in LBLT Tasks GlobalInfo */
    if (ECFM_LBLT_MA_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_MA_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_MA_ENTRY);
        ECFM_LBLT_MA_TABLE = NULL;
    }

    /* Delete Global MD RBTree in LBLT Tasks GlobalInfo */
    if (ECFM_LBLT_MD_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_MD_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_MD_ENTRY);
        ECFM_LBLT_MD_TABLE = NULL;
    }
    /* Delete Context Port MEP RBTree in LBLT Tasks ContextInfo */
    if (ECFM_LBLT_PORT_MEP_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_LBLT_PORT_MEP_TABLE, (tRBKeyFreeFn)
                       EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_PORT_MEP_ENTRY);
        ECFM_LBLT_PORT_MEP_TABLE = NULL;
    }

    if ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable != NULL)
    {
        RBTreeDestroy ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable,
                       (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                       ECFM_LBLT_VLAN_ENTRY);
        (ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable = NULL;
    }

    if ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable != NULL)
    {
        RBTreeDestroy ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable,
                       NULL, 0);
        (ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable = NULL;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmLbLtSelectContext                            */
/*                                                                           */
/*    Description         : This function switches to given context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS / ECFM_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmLbLtSelectContext (UINT4 u4ContextId)
{
    if (u4ContextId >= LBLT_MAX_CONTEXTS)

    {
        return ECFM_FAILURE;
    }
    ECFM_LBLT_CURR_CONTEXT_INFO () = ECFM_LBLT_GET_CONTEXT_INFO (u4ContextId);
    if (ECFM_LBLT_CURR_CONTEXT_INFO () == NULL)

    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmLbLtReleaseContext                           */
/*                                                                           */
/*    Description         : This function makes the switched context to NULL */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmLbLtReleaseContext (VOID)
{
    ECFM_LBLT_CURR_CONTEXT_INFO () = NULL;
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmLbLtGetNextActiveContext                     */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS/ECFM_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmLbLtGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId;

    for (u4ContextId = u4CurrContextId + 1; u4ContextId < LBLT_MAX_CONTEXTS;
         u4ContextId++)

    {
        if (ECFM_LBLT_GET_CONTEXT_INFO (u4ContextId) != NULL)

        {
            *pu4NextContextId = u4ContextId;
            return ECFM_SUCCESS;
        }
    }
    return ECFM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmLbLtHandleCreateContext                      */
/*                                                                           */
/*    Description         : This function creates context, and initalises the*/
/*                          per context information                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS / ECFM_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmLbLtHandleCreateContext (UINT4 u4ContextId)
{
    tEcfmLbLtContextInfo *pContextInfo = NULL;
    if (ECFM_LBLT_GET_CONTEXT_INFO (u4ContextId) != NULL)

    {
        return ECFM_SUCCESS;
    }

    /* Allocate memory to the pContextInfo */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_CONTEXT (pContextInfo) == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtHandleCreateContext :MEM Block Allocation"
                      "Failed !!! \r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pContextInfo, 0x00, ECFM_LBLT_CONTEXT_INFO_SIZE);

    /* Select the newly allocated context info as current context */
    ECFM_LBLT_CURR_CONTEXT_INFO () = pContextInfo;
    ECFM_LBLT_CURR_CONTEXT_ID () = u4ContextId;
    ECFM_LBLT_GET_CONTEXT_INFO (u4ContextId) = ECFM_LBLT_CURR_CONTEXT_INFO ();
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmLbLtHandleDeleteContext                      */
/*                                                                           */
/*    Description         : This function deletes given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            :  ECFM_SUCCESS / ECFM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmLbLtHandleDeleteContext (UINT4 u4ContextId)
{
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtHandleDeleteContext :Invalid ContextId \r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (ECFM_LBLT_CURR_CONTEXT_INFO (), 0,
                 sizeof (tEcfmLbLtContextInfo));
    if (ECFM_LBLT_CONTEXT_POOL != 0)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_CONTEXT_POOL,
                             (UINT1 *) ECFM_LBLT_CURR_CONTEXT_INFO ());
    }
    ECFM_LBLT_CURR_CONTEXT_INFO () = NULL;
    ECFM_LBLT_GET_CONTEXT_INFO (u4ContextId) = ECFM_LBLT_CURR_CONTEXT_INFO ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmlbmod.c
 ****************************************************************************/
