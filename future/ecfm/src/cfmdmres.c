/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmdmres.c,v 1.19 2015/04/16 06:29:36 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way and 2 
 *              way Delay Mesaurement of Control Sub Module.
 *******************************************************************/
#include "cfminc.h"
PRIVATE UINT4 EcfmDmResXmitDmrPdu PROTO ((tEcfmLbLtPduSmInfo *, tEcfmMacAddr));

/*******************************************************************************
 * Function Name      : EcfmLbLtClntParserDmm
 *
 * Description        : This is called to parse the received DMM PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntParseDmm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRxDmmPduInfo *pDmmPduInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pDmmPduInfo = &(pPduSmInfo->uPduInfo.Dmm);
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Copy the TxTimeStampf */
    ECFM_GET_4BYTE (pDmmPduInfo->TxTimeStampf.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (pDmmPduInfo->TxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Copy the Locally filled RxTimeStampf */
    ECFM_GET_4BYTE (pDmmPduInfo->RxTimeStampf.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (pDmmPduInfo->RxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Check if RxTimeStampf was filled by the HW on reception */
    if ((pDmmPduInfo->RxTimeStampf.u4Seconds == ECFM_INIT_VAL) &&
        (pDmmPduInfo->RxTimeStampf.u4NanoSeconds == ECFM_INIT_VAL))

    {

        /* RxTimeStampf was not filled by the HW */
        /* Concider this time as the reception time */
        EcfmLbLtUtilDmGetCurrentTime (&(pDmmPduInfo->RxTimeStampf));
    }

    /* Check that First TLV Offset value received in the LBR PDU should be
     * greater or equal to 4 */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_DMM_FIRST_TLV_OFFSET)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParserDmm: DMM frame is discarded as the"
                       " first tlv offset if wrong is\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbltClntProcessDmm
 *
 * Description        : This routine processes the received DMM PDU, and
 *                      trasnmits the DMR in response.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbltClntProcessDmm (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdDmm)
{
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmMacAddr        MepMacAddr = {
        ECFM_INIT_VAL
    };
    ECFM_LBLT_TRC_FN_ENTRY ();
    pStackInfo = pPduSmInfo->pStackInfo;

    if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
    {
        if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
        {

            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessDmm: "
                           "No Port Information available\r\n");
            return ECFM_FAILURE;
        }
        /* Get the MEP's MAC Address */
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                   (pStackInfo->u2PortNum)->u4IfIndex,
                                   MepMacAddr);

        /* Check if the Mac Address received in the DMM is same as that of the
         * receiving MEP
         */
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr) !=
            ECFM_SUCCESS)

        {

            /* Discard the DMM frame */
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessDmm: "
                           "discarding the received DMM frame\r\n");
            if (ECFM_LBLT_IS_MHF (pStackInfo))

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbltClntProcessDmm: "
                               "dmm forwarded in case of MIP\r\n");
                *pbFrwdDmm = ECFM_TRUE;
            }
            return ECFM_FAILURE;
        }
        if (ECFM_LBLT_IS_MHF (pStackInfo))

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessDmm: "
                           "discarding the received DMM frame\r\n");
            return ECFM_FAILURE;
        }
    }

    /* Transmit the DMR */
    if (EcfmDmResXmitDmrPdu (pPduSmInfo, MepMacAddr) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessDmm: "
                       "failure occurred while responding a DMR for DMM\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmDmResXmitDmrPdu
 *
 * Description        : This routine formats and transmits the LBM PDUs.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      MepMacAddr - Mac Address of the MEP receiving the DMM
 *                      frame.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE UINT4
EcfmDmResXmitDmrPdu (tEcfmLbLtPduSmInfo * pPduSmInfo, tEcfmMacAddr MepMacAddr)
{
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    UINT1              *pu1TimeStamp = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               i4RetVal = ECFM_SUCCESS;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT1               u1Opcode = ECFM_INIT_VAL;
    UINT1               au1TimeStamp[ECFM_TIMESTAMP_FIELD_SIZE];
    BOOL1               b1MemFlag = ECFM_FAILURE;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pDmInfo = &(pMepInfo->DmInfo);
    u1Opcode = ECFM_OPCODE_DMR;

    /* Duplicate the CRU Buffer */
    pDupCruBuf = ECFM_DUPLICATE_CRU_BUF (pPduSmInfo->pBuf);
    if (pDupCruBuf == NULL)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessDmm: "
                       "failure occurred while duplicating the cru buffer\r\n");
        return ECFM_FAILURE;
    }

    do

    {
        if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
        {

            /* Changing the Destination addr field as the src address received in the DMM
             * frame*/
            if (ECFM_COPY_OVER_CRU_BUF
                (pDupCruBuf, pPduSmInfo->RxSrcMacAddr, ECFM_INIT_VAL,
                 ECFM_MAC_ADDR_LENGTH) != ECFM_CRU_SUCCESS)

            {
                b1MemFlag = ECFM_SUCCESS;
                break;
            }

            /* Move offset by 6 bytes to point it to the Source address field */
            u2Offset = u2Offset + (UINT2) ECFM_MAC_ADDR_LENGTH;

            /* Changing the Source addr field */
            if (ECFM_COPY_OVER_CRU_BUF
                (pDupCruBuf, MepMacAddr, u2Offset,
                 ECFM_MAC_ADDR_LENGTH) != ECFM_CRU_SUCCESS)

            {
                b1MemFlag = ECFM_SUCCESS;
                break;
            }
        }

        /* Move offset by 1 byte to point it to Opcode field */
        u2Offset =
            (UINT2) (pPduSmInfo->u1CfmPduOffset +
                     (UINT2) ECFM_MDLEVEL_VER_FIELD_SIZE);

        /*Change the OpCode to DMR */
        if (ECFM_COPY_OVER_CRU_BUF
            (pDupCruBuf, (UINT1 *) &u1Opcode, u2Offset,
             ECFM_OPCODE_FIELD_SIZE) != ECFM_CRU_SUCCESS)
        {
            b1MemFlag = ECFM_SUCCESS;
            break;
        }

        /* Check of Optional fields are to be included in the DMR frame */
        if (pDmInfo->b1TxDmrOpFields == ECFM_TRUE)
        {
            tEcfmLbLtRxDmmPduInfo *pDmmPduInfo = NULL;
            tEcfmTimeRepresentation TxTimeStampb;
            pDmmPduInfo = &(pPduSmInfo->uPduInfo.Dmm);

            /* Move to Offset to RxTimeStamf field */
            u2Offset = u2Offset + (UINT2) ((ECFM_OPCODE_FIELD_SIZE) +
                                           (ECFM_FLAGS_FIELD_SIZE) +
                                           (ECFM_FIRST_TLV_OFFSET_FIELD_SIZE)
                                           + (ECFM_TIMESTAMP_FIELD_SIZE));

            /* Copy the RxTimeStampf received from the HW or the one filled by the
             * DMM parser*/
            pu1TimeStamp = au1TimeStamp;
            ECFM_PUT_4BYTE (pu1TimeStamp, pDmmPduInfo->RxTimeStampf.u4Seconds);
            ECFM_PUT_4BYTE (pu1TimeStamp,
                            pDmmPduInfo->RxTimeStampf.u4NanoSeconds);
            if (ECFM_COPY_OVER_CRU_BUF
                (pDupCruBuf, au1TimeStamp, u2Offset,
                 ECFM_TIMESTAMP_FIELD_SIZE) != ECFM_CRU_SUCCESS)

            {
                b1MemFlag = ECFM_SUCCESS;
                break;
            }

            /* Generate the current timestamp as TxTimeStampb and fill it in DMR
             * frame*/
            EcfmLbLtUtilDmGetCurrentTime (&TxTimeStampb);

            /*Move the Offset to TxTimeStampb Field */
            u2Offset = u2Offset + ECFM_TIMESTAMP_FIELD_SIZE;
            pu1TimeStamp = au1TimeStamp;
            ECFM_PUT_4BYTE (pu1TimeStamp, TxTimeStampb.u4Seconds);
            ECFM_PUT_4BYTE (pu1TimeStamp, TxTimeStampb.u4NanoSeconds);
            if (ECFM_COPY_OVER_CRU_BUF
                (pDupCruBuf, au1TimeStamp, u2Offset,
                 ECFM_TIMESTAMP_FIELD_SIZE) != ECFM_CRU_SUCCESS)

            {
                b1MemFlag = ECFM_SUCCESS;
                break;
            }
        }
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pDupCruBuf);
        ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pDupCruBuf, u4PduSize,
                            "EcfmDmInitXmitDmmPdu: "
                            "Sending out DMR-PDU to lower layer...\r\n");

        /* Transmit the DMR over MPLS-TP path */
        if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
        {
            if (EcfmMplsTpLbLtTxPacket (pDupCruBuf, pMepInfo,
                                        ECFM_OPCODE_DMR) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbiTxXmitDmrPdu: Transmit DMR PDU failed\n");
                ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                i4RetVal = ECFM_FAILURE;
            }
        }
        else                    /* Transmit DMR to Ethernet */
        {
            pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
                (UINT2) pMepInfo->u4PrimaryVidIsid;
#ifdef NPAPI_WANTED
            if (ECFM_HW_DMM_SUPPORT () == ECFM_FALSE)
            {
                i4RetVal =
                    EcfmLbLtCtrlTxTransmitPkt (pDupCruBuf, pMepInfo->u2PortNum,
                                               pMepInfo->u4PrimaryVidIsid, 0, 0,
                                               pMepInfo->u1Direction,
                                               ECFM_OPCODE_DMR,
                                               &(pPduSmInfo->
                                                 VlanClassificationInfo),
                                               &(pPduSmInfo->
                                                 PbbClassificationInfo));
            }
            else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
            {
                UINT1              *pu1DmrPdu = ECFM_LBLT_PDU;

                /*Copy the CRU-BUFF into linear buffer */
                ECFM_COPY_FROM_CRU_BUF (pDupCruBuf, pu1DmrPdu, ECFM_INIT_VAL,
                                        u4PduSize);
                if (EcfmFsMiEcfmTransmitDmr
                    (ECFM_LBLT_CURR_CONTEXT_ID (),
                     ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum,
                                             pTempLbLtPortInfo),
                     pu1DmrPdu, u4PduSize, pPduSmInfo->VlanClassificationInfo,
                     pMepInfo->u1Direction) != FNP_SUCCESS)

                {
                    ECFM_LBLT_INCR_TX_FAILED_COUNT (pMepInfo->u2PortNum);
                    ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT
                        (ECFM_LBLT_CURR_CONTEXT_ID ());
                    i4RetVal = ECFM_FAILURE;
                }

                else

                {
                    ECFM_LBLT_INCR_TX_COUNT (pMepInfo->u2PortNum,
                                             ECFM_OPCODE_DMR);
                    ECFM_LBLT_INCR_CTX_TX_COUNT (ECFM_LBLT_CURR_CONTEXT_ID (),
                                                 ECFM_OPCODE_DMR);
                    ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                    pDupCruBuf = NULL;
                    i4RetVal = ECFM_SUCCESS;
                }
            }
#else /*  */
            i4RetVal =
                EcfmLbLtCtrlTxTransmitPkt (pDupCruBuf, pMepInfo->u2PortNum,
                                           pMepInfo->u4PrimaryVidIsid, 0, 0,
                                           pMepInfo->u1Direction,
                                           ECFM_OPCODE_DMR,
                                           &(pPduSmInfo->
                                             VlanClassificationInfo),
                                           &(pPduSmInfo->
                                             PbbClassificationInfo));
#endif /*  */
        }
    }
    while (0);
    if (i4RetVal != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessDmm: "
                       "failure occurred while formating and transmitting DMR frame\r\n");
        ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
    }
    else if (b1MemFlag == ECFM_SUCCESS)
    {
        ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
  End of File cfmdmres.c
 ******************************************************************************/
