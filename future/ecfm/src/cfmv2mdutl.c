/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *  
 * $Id: cfmv2mdutl.c,v 1.11 2014/12/09 12:45:07 siva Exp $
 * 
 * 
 * Description: This file contains common Util Low Level Routines
 *              for MD Table used by both IEEECFM MIB 
 *              verisons D8.0 and D8.1 .
 *******************************************************************/

#include "cfminc.h"
#include  "fscfmmcli.h"
#include "fsmiy1cli.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdTableNextIndex
 Input       :  The Indices

                The Object 
                retValDot1agCfmMdTableNextIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdTableNextIndex (UINT4 *pu4RetValDot1agCfmMdTableNextIndex)
{
    tEcfmCcMdInfo      *pMdCurrentNode = NULL;
    tEcfmCcMdInfo      *pMdStartNode = NULL;
    tEcfmCcMdInfo      *pMdNextNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_SUCCESS;
    }
    pMdCurrentNode = (tEcfmCcMdInfo *) RBTreeGetFirst (ECFM_CC_MD_TABLE);
    /* Check if no MD is configured yet */
    if (pMdCurrentNode == NULL)
    {
        *pu4RetValDot1agCfmMdTableNextIndex = ECFM_MD_INDEX_MIN;
        return SNMP_SUCCESS;
    }
    pMdStartNode = pMdCurrentNode;
    while (pMdCurrentNode != NULL)
    {
        pMdNextNode = RBTreeGetNext (ECFM_CC_MD_TABLE, pMdCurrentNode, NULL);
        if (pMdNextNode == NULL)
        {
            if (pMdCurrentNode->u4MdIndex != ECFM_MD_INDEX_MAX)
            {
                *pu4RetValDot1agCfmMdTableNextIndex
                    = pMdCurrentNode->u4MdIndex + 1;
                return SNMP_SUCCESS;
            }
            else
            {
                break;
            }
        }
        if (pMdNextNode->u4MdIndex == ECFM_MD_INDEX_MAX)
        {
            break;
        }
        pMdCurrentNode = pMdNextNode;
    }

    /* This means indexes are
       fully finished, figure out gaps if there.. */
    while (pMdStartNode != NULL)
    {
        if (pMdStartNode->u4MdIndex != ECFM_MD_INDEX_MIN)
        {
            *pu4RetValDot1agCfmMdTableNextIndex = ECFM_MD_INDEX_MIN;
            return SNMP_SUCCESS;
        }
        pMdNextNode = (tEcfmCcMdInfo *)
            RBTreeGetNext (ECFM_CC_MD_TABLE, pMdStartNode, NULL);
        if (pMdNextNode == NULL)
        {
            /*No free indexes */
            break;
        }
        if ((pMdStartNode->u4MdIndex + 1) != pMdNextNode->u4MdIndex)
        {
            *pu4RetValDot1agCfmMdTableNextIndex = pMdStartNode->u4MdIndex + 1;
            return SNMP_SUCCESS;
        }
        pMdStartNode = pMdNextNode;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Dot1agCfmMdTable. */

/****************************************************************************
 Function    :  EcfmMdUtlValAgMdTable
 Input       :  The Indices
                Dot1agCfmMdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
PUBLIC INT1
EcfmMdUtlValAgMdTable (UINT4 u4Dot1agCfmMdIndex)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /*Check whether MD entry exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlGetNextIndexAgMdTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
PUBLIC INT1
EcfmMdUtlGetNextIndexAgMdTable (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 *pu4NextDot1agCfmMdIndex)
{
    tEcfmCcMdInfo       MdInfo;
    tEcfmCcMdInfo      *pMdNextNode = NULL;

    /* First check System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MD entry next to index MdIndex */
    ECFM_MEMSET (&MdInfo, ECFM_INIT_VAL, ECFM_CC_MD_INFO_SIZE);
    MdInfo.u4MdIndex = u4Dot1agCfmMdIndex;
    pMdNextNode = (tEcfmCcMdInfo *) RBTreeGetNext
        (ECFM_CC_MD_TABLE, (tRBElem *) (&MdInfo), NULL);

    /* Check whether next MD entry exists or not */
    if (pMdNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* Next MD entry exists, return the next MD Index */
    *pu4NextDot1agCfmMdIndex = pMdNextNode->u4MdIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdFormat
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdFormat (UINT4 u4Dot1agCfmMdIndex,
                        INT4 *pi4RetValDot1agCfmMdFormat)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check whether MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set the Md name Format from corresponding MD entry */
    *pi4RetValDot1agCfmMdFormat = (INT4) (pMdNode->u1NameFormat);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdName
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdName (UINT4 u4Dot1agCfmMdIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValDot1agCfmMdName)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check whether MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists, */
    /* Set the MdName and its length from corresponding MD entry */
    ECFM_MEMCPY (pRetValDot1agCfmMdName->pu1_OctetList, pMdNode->au1Name,
                 pMdNode->u1NameLength);
    pRetValDot1agCfmMdName->i4_Length = (INT4) pMdNode->u1NameLength;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdMdLevel
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdMdLevel (UINT4 u4Dot1agCfmMdIndex,
                         INT4 *pi4RetValDot1agCfmMdMdLevel)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check whether MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set Md Level from corresponding MD entry */

    *pi4RetValDot1agCfmMdMdLevel = (INT4) (pMdNode->u1Level);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdMhfCreation
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdMhfCreation (UINT4 u4Dot1agCfmMdIndex,
                             INT4 *pi4RetValDot1agCfmMdMhfCreation)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get Md entry corresponding to u4Dot1agCfmMdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check whether MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set Mhf creation criteria from corresponding MD entry */
    *pi4RetValDot1agCfmMdMhfCreation = (INT4) (pMdNode->u1MhfCreation);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdMhfIdPermission
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMhfIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdMhfIdPermission (UINT4 u4Dot1agCfmMdIndex,
                                 INT4 *pi4RetValDot1agCfmMdMhfIdPermission)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get Md entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check whether MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set Sender Id permission from corresponding MD entry */
    *pi4RetValDot1agCfmMdMhfIdPermission = (INT4) (pMdNode->u1IdPermission);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdMaTableNextIndex
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMaTableNextIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdMaTableNextIndex (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 *pu4RetValDot1agCfmMdMaTableNextIndex)
{
    tEcfmCcMaInfo      *pMaCurrentNode = NULL;
    tEcfmCcMaInfo      *pMaStartNode = NULL;
    tEcfmCcMaInfo      *pMaNextNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MD entry corresponding to u4Dot1agCfmMdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check if MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        *pu4RetValDot1agCfmMdMaTableNextIndex = 0;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }

    pMaCurrentNode = (tEcfmCcMaInfo *) RBTreeGetFirst (pMdNode->MaTable);
    /* Check if no MA associated with MD with MdIndex exists */
    if (pMaCurrentNode == NULL)
    {
        *pu4RetValDot1agCfmMdMaTableNextIndex = ECFM_MA_INDEX_MIN;
        return SNMP_SUCCESS;
    }
    pMaStartNode = pMaCurrentNode;

    while (pMaCurrentNode != NULL)
    {
        pMaNextNode = RBTreeGetNext (pMdNode->MaTable, pMaCurrentNode, NULL);

        if (pMaNextNode == NULL)
        {
            if (pMaCurrentNode->u4MaIndex != ECFM_MA_INDEX_MAX)
            {
                *pu4RetValDot1agCfmMdMaTableNextIndex
                    = pMaCurrentNode->u4MaIndex + 1;

                return SNMP_SUCCESS;
            }
            else
            {
                break;
            }
        }
        if (pMaNextNode->u4MaIndex == ECFM_MA_INDEX_MAX)
        {
            break;
        }

        pMaCurrentNode = pMaNextNode;
    }

    /* This means indexes are
       fully finished, figure out gaps if there.. */
    while (pMaStartNode != NULL)
    {
        if (pMaStartNode->u4MaIndex != ECFM_MA_INDEX_MIN)
        {
            *pu4RetValDot1agCfmMdMaTableNextIndex = ECFM_MA_INDEX_MIN;
            return SNMP_SUCCESS;
        }
        pMaNextNode = (tEcfmCcMaInfo *)
            RBTreeGetNext (pMdNode->MaTable, pMaStartNode, NULL);

        if (pMaNextNode == NULL)
        {
            /*No free indexes */
            break;
        }

        if ((pMaStartNode->u4MaIndex + 1) != pMaNextNode->u4MaIndex)
        {
            *pu4RetValDot1agCfmMdMaTableNextIndex = pMaStartNode->u4MaIndex + 1;
            return SNMP_SUCCESS;
        }
        pMaStartNode = pMaNextNode;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  EcfmMdUtlGetAgMdRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlGetAgMdRowStatus (UINT4 u4Dot1agCfmMdIndex,
                           INT4 *pi4RetValDot1agCfmMdRowStatus)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

    /* Check whether MD entry corresponding to MdIndex exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set Row Status from corresponding MD entry */
    *pi4RetValDot1agCfmMdRowStatus = (INT4) (pMdNode->u1RowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMdUtlSetAgMdFormat
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlSetAgMdFormat (UINT4 u4Dot1agCfmMdIndex,
                        INT4 i4SetValDot1agCfmMdFormat)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set Md name format to corresponding MD entry */
    pMdNode->u1NameFormat = (UINT1) i4SetValDot1agCfmMdFormat;

    if (pMdNode->u1NameFormat == ECFM_DOMAIN_NAME_TYPE_NONE)
    {
        /* On setting the format to none(1), reset the old MD name.
         * If user desires to assign a MD name for CLI purpose for 
         * easy identification, it can be configured, though it is 
         * not needed for CFM PDU construction.
         */
        MEMSET (pMdNode->au1Name, ECFM_INIT_VAL, ECFM_MD_NAME_ARRAY_SIZE);
        pMdNode->u1NameLength = ECFM_INIT_VAL;
    }

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMdFormat, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, i4SetValDot1agCfmMdFormat));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlSetAgMdName
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlSetAgMdName (UINT4 u4Dot1agCfmMdIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValDot1agCfmMdName)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set MD name to corresponding MD entry */
    ECFM_MEMSET (pMdNode->au1Name, ECFM_INIT_VAL, ECFM_MD_NAME_MAX_LEN);
    ECFM_MEMCPY (pMdNode->au1Name, pSetValDot1agCfmMdName->pu1_OctetList,
                 pSetValDot1agCfmMdName->i4_Length);
    pMdNode->u1NameLength = (UINT1) pSetValDot1agCfmMdName->i4_Length;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMdName, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %s", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, pSetValDot1agCfmMdName));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlSetAgMdMdLevel
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlSetAgMdMdLevel (UINT4 u4Dot1agCfmMdIndex,
                         INT4 i4SetValDot1agCfmMdMdLevel)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set MD Level to corresponding MD entry */
    pMdNode->u1Level = (UINT1) i4SetValDot1agCfmMdMdLevel;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMdMdLevel, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, i4SetValDot1agCfmMdMdLevel));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlSetAgMdMhfCreation
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlSetAgMdMhfCreation (UINT4 u4Dot1agCfmMdIndex,
                             INT4 i4SetValDot1agCfmMdMhfCreation)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set MD Mhf Creation criteria to corresponding MD entry */
    pMdNode->u1MhfCreation = (UINT1) i4SetValDot1agCfmMdMhfCreation;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMdMhfCreation, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, i4SetValDot1agCfmMdMhfCreation));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlSetAgMdMhfIdPermission
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdMhfIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlSetAgMdMhfIdPermission (UINT4 u4Dot1agCfmMdIndex,
                                 INT4 i4SetValDot1agCfmMdMhfIdPermission)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Set MD Mhf Id Permission to corresponding MD entry */
    pMdNode->u1IdPermission = (UINT1) i4SetValDot1agCfmMdMhfIdPermission;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMdMhfIdPermission, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, i4SetValDot1agCfmMdMhfIdPermission));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlSetAgMdRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlSetAgMdRowStatus (UINT4 u4Dot1agCfmMdIndex,
                           INT4 i4SetValDot1agCfmMdRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMdInfo      *pMdNewNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4TimeTick = ECFM_INIT_VAL;
    UINT4               u4TimeInMsc = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    INT4                i4SavedRowStatus = 0;
    UINT1               au1DefaultMdName[] = "DEFAULT";

    ECFM_MEMSET (&PduSmInfo, 0x00, sizeof (tEcfmCcPduSmInfo));
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode != NULL)
    {
        /* MD entry corresponding to MdIndex exists, and MD row status is 
         * same as user wants to set */
        if (pMdNode->u1RowStatus == (UINT1) (i4SetValDot1agCfmMdRowStatus))
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }
    else if ((i4SetValDot1agCfmMdRowStatus != ECFM_ROW_STATUS_CREATE_AND_WAIT)
             && (i4SetValDot1agCfmMdRowStatus != ECFM_ROW_STATUS_CREATE_AND_GO))

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMdRowStatus, u4SeqNum,
                          TRUE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    /* User wants to set particular row status for MD */
    switch (i4SetValDot1agCfmMdRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
#ifdef L2RED_WANTED
            EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                           ECFM_RED_MD_ROW_STS_CMD,
                                           ECFM_INIT_VAL,
                                           u4Dot1agCfmMdIndex,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_TRUE);

            gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd = ECFM_TRUE;
            if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
            {
                gEcfmRedGlobalInfo.HwAuditInfo.bError = ECFM_FALSE;
            }
#endif
            pMdNode->u1RowStatus = (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE;
            EcfmSnmpLwUpdateAllMaRowStatus (pMdNode);
#ifdef L2RED_WANTED
            gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd = ECFM_FALSE;
#endif
            /*Stop Mep Archive Hold Timer */
            PduSmInfo.pMdInfo = pMdNode;
            EcfmCcTmrStopTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD, &PduSmInfo);
            break;

        case ECFM_ROW_STATUS_ACTIVE:
            if (pMdNode->u1RowStatus != (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: MD row status set to Active Failed\n");
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  i4SetValDot1agCfmMdRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
#ifdef L2RED_WANTED
            EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                           ECFM_RED_MD_ROW_STS_CMD,
                                           ECFM_INIT_VAL,
                                           u4Dot1agCfmMdIndex,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_TRUE);
            gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd = ECFM_TRUE;
            if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
            {
                gEcfmRedGlobalInfo.HwAuditInfo.bError = ECFM_FALSE;
            }
#endif
            pMdNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
            EcfmSnmpLwUpdateAllMaRowStatus (pMdNode);
#ifdef L2RED_WANTED
            gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd = ECFM_FALSE;
#endif
            /* Start Mep Archive Hold Timer */
            u4TimeTick =
                ECFM_CONVERT_SEC_TO_TIME_TICKS (pMdNode->u2MepArchiveHoldTime *
                                                60);
            u4TimeInMsc = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4TimeTick);
            PduSmInfo.pMdInfo = pMdNode;
            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD,
                                     &PduSmInfo, u4TimeInMsc) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "EcfmMdUtlSetAgMdRowStatus: Unable to Start"
                             "MEP-Archive Hold Timer \r\n");
            }

            /* Update MD at LBLT also */
            EcfmLbLtAddMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pMdNode);
            break;
        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
        case ECFM_ROW_STATUS_CREATE_AND_GO:
            /* User wants to create a new MD entry with MdIndex */
            /* Create  a node for new MD */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MD_TABLE (pMdNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "EcfmMdUtlSetAgMdRowStatus: Row Status Create and Go"
                             "Allocation for MD Node Failed \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  i4SetValDot1agCfmMdRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }

            ECFM_MEMSET (pMdNewNode, ECFM_INIT_VAL, ECFM_CC_MD_INFO_SIZE);

            /* Put MdIndex in new MD node */
            pMdNewNode->i1ClientLevel = ECFM_DEF_MD_LEVEL_DEF_VAL;
            pMdNewNode->b1CfmDropEligible = ECFM_FALSE;
            pMdNewNode->u1CfmVlanPriority = ECFM_VLAN_PRIORITY_MAX;
            pMdNewNode->u4MdIndex = u4Dot1agCfmMdIndex;
            /* Put other values as default one */
            pMdNewNode->u1Level = ECFM_INIT_VAL;
            pMdNewNode->u1MhfCreation = ECFM_MHF_CRITERIA_NONE;
            pMdNewNode->u1IdPermission = ECFM_SENDER_ID_NONE;
            pMdNewNode->u4MaTableNextIndex = 1;
            pMdNewNode->u2MepArchiveHoldTime = ECFM_MEP_ARCHIVE_DEF_HOLD_TIME;
            pMdNewNode->u1NameFormat = ECFM_DOMAIN_NAME_TYPE_CHAR_STRING;
            pMdNewNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();

            MEMSET (pMdNewNode->au1Name, 0x0, ECFM_MD_NAME_ARRAY_SIZE);
            ECFM_MEMCPY (pMdNewNode->au1Name, au1DefaultMdName,
                         sizeof (au1DefaultMdName));
            pMdNewNode->u1NameLength = ECFM_MD_DEFAULT_NAME_LEN;
            /* Create RBTree for MAs associated with MD */
            pMdNewNode->MaTable = EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                                            (tEcfmCcMaInfo,
                                                             MaTableMdNode),
                                                            EcfmMaCmpForMaTableInMd);
            if (pMdNewNode->MaTable == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Allocation to MA node Failed for this MD\n");
                /*Release memory allocated to new MD node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MD_TABLE_POOL,
                                     (UINT1 *) (pMdNewNode));
                pMdNewNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  i4SetValDot1agCfmMdRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (i4SetValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT)
            {
                pMdNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            }
            else
            {
                pMdNewNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
                /* Note: Workaround done to send create and wait and active instead of 
                 * create and go*/
                SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  ECFM_ROW_STATUS_CREATE_AND_WAIT));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                i4SavedRowStatus = i4SetValDot1agCfmMdRowStatus;
                i4SetValDot1agCfmMdRowStatus = ECFM_ROW_STATUS_ACTIVE;
            }
            /* Add this MD entry in RBTree MdTableIndex in Global structure */
            if (RBTreeAdd (ECFM_CC_MD_TABLE, pMdNewNode) == ECFM_RB_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Allocation to MD node Failed\n");
                RBTreeDestroy (pMdNewNode->MaTable,
                               (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                               ECFM_CC_MA_ENTRY_IN_MD);
                pMdNewNode->MaTable = NULL;
                /*Release memory allocated to new node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MD_TABLE_POOL,
                                     (UINT1 *) (pMdNewNode));
                pMdNewNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex, i4SavedRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* Start Mep Archive Hold Timer */
            if (i4SavedRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO)
            {
                u4TimeTick =
                    ECFM_CONVERT_SEC_TO_TIME_TICKS (pMdNewNode->
                                                    u2MepArchiveHoldTime * 60);
                u4TimeInMsc = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4TimeTick);
                PduSmInfo.pMdInfo = pMdNewNode;
                if (EcfmCcTmrStartTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD,
                                         &PduSmInfo,
                                         u4TimeInMsc) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                 ECFM_OS_RESOURCE_TRC,
                                 "EcfmMdUtlSetAgMdRowStatus: Unable to Start MEP-Archive Hold Timer \r\n");
                }
            }
            break;

        case ECFM_ROW_STATUS_DESTROY:
            /* User wants to Delete MD entry corresponding to MdIndex */
            /* Remove MD entry */
            /*Stop Mep Archive Hold Timer */
            PduSmInfo.pMdInfo = pMdNode;

            /* Update MD at LBLT also */
            EcfmLbLtRemoveMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pMdNode);
            EcfmCcTmrStopTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD, &PduSmInfo);
            RBTreeRem (ECFM_CC_MD_TABLE, (tRBElem *) pMdNode);

            /* Release memory block allocated to MD node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MD_TABLE_POOL, (UINT1 *) (pMdNode));
            pMdNode = NULL;
            break;
        default:
            break;
    }

    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, i4SetValDot1agCfmMdRowStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    /* workaround for incrememntal MSR */
    if (i4SetValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIY1731MegRowStatus, u4SeqNum,
                              TRUE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

        /* Send Destroy notification to Y.1731 MIB also */
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          i4SetValDot1agCfmMdRowStatus));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  EcfmMdUtlTestv2AgMdFormat
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlTestv2AgMdFormat (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                           INT4 i4TestValDot1agCfmMdFormat)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown \n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }
    /*Check for MD name format possible values */
    if ((i4TestValDot1agCfmMdFormat == ECFM_DOMAIN_NAME_TYPE_IEEE_RESERVED) ||
        (i4TestValDot1agCfmMdFormat > ECFM_DOMAIN_NAME_TYPE_CHAR_STRING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid value for MD Name format\n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists, */
    /* Check whether MD name format value is allowed to set */
    if (pMdNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:MD already exists for given indices \n");
        return SNMP_FAILURE;
    }

    /* Check MD name format can be modified */
    if (ECFM_ANY_MA_CONFIGURED_IN_MD (pMdNode) == ECFM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:MD Name cannot be modified\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlTestv2AgMdName
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlTestv2AgMdName (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValDot1agCfmMdName)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown \n");
        return SNMP_FAILURE;
    }
    /*  Validate MD name according to its MdFormat and its length */
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }

    /* MD entry corresponding to MdIndex exists */
    /* Check whether MD name is allowed to set */
    if (pMdNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Maintaince Domain already exists \n");
        return SNMP_FAILURE;
    }

    /* Check if MD name can be modified */
    if (ECFM_ANY_MA_CONFIGURED_IN_MD (pMdNode) == ECFM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:MD Name cannot be modified \n");
        return SNMP_FAILURE;
    }

    if ((EcfmValidateMdNameFormat
         (pTestValDot1agCfmMdName->pu1_OctetList,
          pTestValDot1agCfmMdName->i4_Length,
          pMdNode->u1NameFormat, pu4ErrorCode)) != ECFM_SUCCESS)
    {
        CLI_SET_ERR (CLI_ECFM_MD_NAME_FORMAT_ERR);
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid MD Name Format \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlTestv2AgMdMdLevel
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlTestv2AgMdMdLevel (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                            INT4 i4TestValDot1agCfmMdMdLevel)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }

    /*  Validate MdLevel value */
    if ((i4TestValDot1agCfmMdMdLevel < ECFM_MD_LEVEL_MIN) ||
        (i4TestValDot1agCfmMdMdLevel > ECFM_MD_LEVEL_MAX))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid value for MD Level\n");
        return SNMP_FAILURE;
    }
    /* MD entry corresponding to MdIndex exists */
    /* Check whether MdLevel is allowed to set */
    if (pMdNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Maintaince Domain already exists \n");
        return SNMP_FAILURE;
    }

    /* Check whether MdLevel can be modified */
    if (ECFM_ANY_MA_CONFIGURED_IN_MD (pMdNode) == ECFM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:MD Level already exists\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlTestv2AgMdMhfCreation
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlTestv2AgMdMhfCreation (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                INT4 i4TestValDot1agCfmMdMhfCreation)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown \n");
        return SNMP_FAILURE;
    }
    /*  Validate  Mhf creation value */
    if ((i4TestValDot1agCfmMdMhfCreation < ECFM_MHF_CRITERIA_NONE) ||
        (i4TestValDot1agCfmMdMhfCreation > ECFM_MHF_CRITERIA_EXPLICIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid MHF Creation criteria \n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }

    /* MD entry corresponding to MdIndex exists */
    /* Check whether Mhf creation criteria can be changed */
    if (pMdNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Maintenance Domain already Exists \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlTestAgMdMhfIdPermission
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdMhfIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlTestAgMdMhfIdPermission (UINT4 *pu4ErrorCode,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  INT4 i4TestValDot1agCfmMdMhfIdPermission)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /*  Validate Sender Id permission value */
    if ((i4TestValDot1agCfmMdMhfIdPermission < ECFM_SENDER_ID_NONE) ||
        (i4TestValDot1agCfmMdMhfIdPermission > ECFM_SENDER_ID_CHASSID_MANAGE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid Value for Sender Id Permission\n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Maintenance Domain Exists \n");
        return SNMP_FAILURE;
    }

    /* MD entry corresponding to MdIndex exists */
    /* Check whether SenderId permission is allowed to set */
    if (pMdNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Maintenance Domain already Exists \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMdUtlTestv2AgMdRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMdUtlTestv2AgMdRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                              INT4 i4TestValDot1agCfmMdRowStatus)
{
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMdInfo      *pMdTempNode = NULL;
    UINT4               u4EntryCount = 0;
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate Row status value */
    if ((i4TestValDot1agCfmMdRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValDot1agCfmMdRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid MD Row Status\n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        /* MD entry corresponding to MdIndex does not exists, 
         * but user wants to change MD row status */
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4TestValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }

        else if ((i4TestValDot1agCfmMdRowStatus !=
                  ECFM_ROW_STATUS_CREATE_AND_GO)
                 && (i4TestValDot1agCfmMdRowStatus !=
                     ECFM_ROW_STATUS_CREATE_AND_WAIT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:No Maintenance Domain Exists \n");
            return SNMP_FAILURE;
        }
        /* Whether there exists MD with "DEFAULT" as name */
        if (i4TestValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO)
        {
            pMdTempNode = (tEcfmCcMdInfo *) RBTreeGetFirst (ECFM_CC_MD_TABLE);
            while (pMdTempNode != NULL)
            {
                if (ECFM_STRCMP (pMdTempNode->au1Name, "DEFAULT") == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "\tSNMP:Maintenance Domain already Exists \n");
                    return SNMP_FAILURE;
                }
                pMdTempNode = (tEcfmCcMdInfo *) RBTreeGetNext (ECFM_CC_MD_TABLE,
                                                               (tRBElem *)
                                                               pMdTempNode,
                                                               NULL);
            }
        }
        /* Number of MD created count is fetched from RBTreeCount 
         * datastructure and it is compared with maximum Number of
         * MD that are allowed to create*/
        RBTreeCount (ECFM_CC_MD_TABLE, &u4EntryCount);
        if (u4EntryCount >= ECFM_MAX_MD)
        {
            CLI_SET_ERR (CLI_ECFM_MAX_MD_CREATION_REACHED);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Invalid MD Index\n");

            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }

    /* MD entry corresponding to MdIndex exists, 
     * and its row status is same as user wants to set */
    if (pMdNode->u1RowStatus == (UINT1) i4TestValDot1agCfmMdRowStatus)
    {
        return SNMP_SUCCESS;
    }
    /* MD entry corresponding to MdIndex exists and 
     * user wants to create new MD entry corresponding to MdIndex */
    if ((i4TestValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT) ||
        (i4TestValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Maintenance Domain already exists\n");
        return SNMP_FAILURE;
    }

    /* If user wants to make MD's row status ACTIVE */
    if ((i4TestValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_ACTIVE) &&
        (pMdNode->u1RowStatus == ECFM_ROW_STATUS_NOT_IN_SERVICE))
    {
        /* Check whether MD row status can be changed to ACTIVE */
        if (EcfmSnmpLwIsInfoConfiguredForMd (pMdNode) != ECFM_SUCCESS)
        {
            CLI_SET_ERR (CLI_ECFM_INVALID_MD_NAME_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Maintenance Domain cannot be created\n");
            return SNMP_FAILURE;
        }
    }
    /* If user wants to delete particular MD, check if it is possible */
    if (i4TestValDot1agCfmMdRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        if (ECFM_ANY_MA_CONFIGURED_IN_MD (pMdNode) == ECFM_TRUE)
        {
            CLI_SET_ERR (CLI_ECFM_DOM_DEL_MA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Maintenance Domain already exists\n");
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/******************************************************************************/
/*                           End  of file cfmmdutil.c                           */
/******************************************************************************/
