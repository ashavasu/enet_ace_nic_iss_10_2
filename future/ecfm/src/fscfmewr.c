/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved]
*
* $Id: fscfmewr.c,v 1.6 2011/09/24 06:53:36 siva Exp $
*
* Description: This file contains the wrapper routines for
*              proprietary mib.
*******************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fscfmewr.h"
# include  "fscfmedb.h"
# include  "cfminc.h"

INT4
GetNextIndexFsMIEcfmExtStackTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmExtStackTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmExtStackTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaCrosscheckStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaCrosscheckStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaCrosscheckStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaCrosscheckStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaCrosscheckStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaCrosscheckStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[3].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[4].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

VOID
RegisterFSCFME ()
{
    SNMPRegisterMib (&fscfmeOID, &fscfmeEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fscfmeOID, (const UINT1 *) "fscfmext");
}

VOID
UnRegisterFSCFME ()
{
    SNMPUnRegisterMib (&fscfmeOID, &fscfmeEntry);
    SNMPDelSysorEntry (&fscfmeOID, (const UINT1 *) "fscfmext");
}

INT4
FsMIEcfmExtStackMdIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtStackMdIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtStackMaIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtStackMaIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtStackMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtStackMepId (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtStackMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmExtStackMacAddress
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIEcfmExtDefaultMdTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmExtDefaultMdTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmExtDefaultMdTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtDefaultMdStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtDefaultMdLevel
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdMhfCreationGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtDefaultMdMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdIdPermissionGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtDefaultMdIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtDefaultMdLevel
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdMhfCreationSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtDefaultMdMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdIdPermissionSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtDefaultMdIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtDefaultMdLevel (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtDefaultMdMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtDefaultMdMhfCreation (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiData->
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtDefaultMdIdPermission (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiData->
                                                   i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtDefaultMdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmExtDefaultMdTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmExtConfigErrorListTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmExtConfigErrorListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmExtConfigErrorListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtConfigErrorListErrorTypeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtConfigErrorListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtConfigErrorListErrorType
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIEcfmExtMaTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmExtMaTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmExtMaTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].i4_SLongValue,
                                   pMultiIndex->pIndex[4].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaName (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].i4_SLongValue,
                                 pMultiIndex->pIndex[4].u4_ULongValue,
                                 pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {

        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaIdPermissionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaCcmIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaCcmInterval
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaNumberOfVidsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaNumberOfVids
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMaRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].i4_SLongValue,
                                   pMultiIndex->pIndex[4].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaName (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].i4_SLongValue,
                                 pMultiIndex->pIndex[4].u4_ULongValue,
                                 pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaIdPermissionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaCcmIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaCcmInterval
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMaRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaFormat (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaName (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].u4_ULongValue,
                                    pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaMhfCreation (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[2].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[3].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaIdPermission (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[4].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaCcmIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaCcmInterval (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[2].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[3].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMaRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[1].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[2].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[3].
                                         i4_SLongValue,
                                         pMultiIndex->pIndex[4].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmExtMaTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmExtMipTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmExtMipTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmExtMipTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMipTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMipActive (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMipTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMipRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipActiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMipActive (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMipRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipActiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMipActive (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMipRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].
                                          i4_SLongValue,
                                          pMultiIndex->pIndex[1].
                                          i4_SLongValue,
                                          pMultiIndex->pIndex[2].
                                          i4_SLongValue,
                                          pMultiIndex->pIndex[3].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMipTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmExtMipTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmExtMepTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmExtMepTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmExtMepTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepDirection (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepPrimaryVidOrIsidGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepPrimaryVidOrIsid
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepActive (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepFngStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepFngState (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepCciEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepCciEnabled
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepCcmLtmPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepCcmLtmPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmExtMepMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepLowPrDefGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepLowPrDef (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepFngAlarmTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepFngAlarmTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepFngResetTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepFngResetTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepHighestPrDefectGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepHighestPrDefect
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepDefectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepDefects (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepErrorCcmLastFailureGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepErrorCcmLastFailure
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepXconCcmLastFailureGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepXconCcmLastFailure
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepCcmSequenceErrorsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepCcmSequenceErrors
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepCciSentCcmsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepCciSentCcms (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepNextLbmTransIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepNextLbmTransId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepLbrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepLbrInOutOfOrderGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepLbrInOutOfOrder
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepLbrBadMsduGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepLbrBadMsdu (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepLtmNextSeqNumberGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepLtmNextSeqNumber
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepUnexpLtrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIEcfmExtMepUnexpLtrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepLbrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepLbrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == ECFM_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmDestMacAddressGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmExtMepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmDestMepIdGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmDestMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmDestIsMepIdGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmDestIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmMessagesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmMessages
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmDataTlvGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmDataTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmVlanIsidPriorityGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmResultOKGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmResultOK
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmSeqNumberGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLbmSeqNumber
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmFlagsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmFlags
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmTargetMacAddressGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmExtMepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmTargetMepIdGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmTargetIsMepIdGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmTtl
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmResultGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmResult
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmSeqNumberGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmSeqNumber
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLtmEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmExtMepTransmitLtmEgressIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepCcmOffloadGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmExtMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmExtMepCcmOffload
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepDirection (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepPrimaryVidOrIsidSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepPrimaryVidOrIsid
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepActiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepActive (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCciEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepCciEnabled
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCcmLtmPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepCcmLtmPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLowPrDefSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepLowPrDef (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepFngAlarmTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepFngAlarmTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepFngResetTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepFngResetTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCcmSequenceErrorsSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepCcmSequenceErrors
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCciSentCcmsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepCciSentCcms (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepLbrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrInOutOfOrderSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepLbrInOutOfOrder
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrBadMsduSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepLbrBadMsdu (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepUnexpLtrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepUnexpLtrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepLbrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmExtMepTransmitLbmDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDestMepIdSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmDestMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDestIsMepIdSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmDestIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmMessagesSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmMessages
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDataTlvSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmDataTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmVlanIsidPrioritySet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmFlagsSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmFlags
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTargetMacAddressSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTargetMepIdSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTargetIsMepIdSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTtlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmTtl
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmEgressIdentifierSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmExtMepTransmitLtmEgressIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCcmOffloadSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmExtMepCcmOffload
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepIfIndex (pu4Error,
                                        pMultiIndex->pIndex[0].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[1].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[2].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[3].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepDirection (pu4Error,
                                          pMultiIndex->pIndex[0].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[1].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[2].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[3].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepPrimaryVidOrIsidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepPrimaryVidOrIsid (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->
                                                 u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepActiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepActive (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCciEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepCciEnabled (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[2].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[3].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCcmLtmPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepCcmLtmPriority (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLowPrDefTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepLowPrDef (pu4Error,
                                         pMultiIndex->pIndex[0].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[1].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[2].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[3].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepFngAlarmTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepFngAlarmTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepFngResetTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepFngResetTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCcmSequenceErrorsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepCcmSequenceErrors (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCciSentCcmsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepCciSentCcms (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepLbrIn (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrInOutOfOrderTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepLbrInOutOfOrder (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[3].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrBadMsduTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepLbrBadMsdu (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepUnexpLtrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepUnexpLtrIn (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepLbrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepLbrOut (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDestMacAddressTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmDestMacAddress (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[3].
                                                          u4_ULongValue,
                                                          (*(tMacAddr *)
                                                           pMultiData->
                                                           pOctetStrValue->
                                                           pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDestMepIdTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmDestMepId (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDestIsMepIdTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmDestIsMepId (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[2].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[3].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmMessagesTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmMessages (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmDataTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmDataTlv (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->
                                                   pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmVlanIsidPriorityTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidPriority (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[2].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[3].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmFlagsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmFlags (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->
                                                 pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTargetMacAddressTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMacAddress (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            (*(tMacAddr *)
                                                             pMultiData->
                                                             pOctetStrValue->
                                                             pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTargetMepIdTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMepId (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[2].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[3].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTargetIsMepIdTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmTargetIsMepId (pu4Error,
                                                         pMultiIndex->
                                                         pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[3].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmTtlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmTtl (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTransmitLtmEgressIdentifierTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepTransmitLtmEgressIdentifier (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[1].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[2].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[3].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepCcmOffloadTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmExtMepCcmOffload (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[2].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[3].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmExtMepTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmExtMepTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
