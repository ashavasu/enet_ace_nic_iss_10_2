/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmtrap.c,v 1.25 2016/05/19 10:55:39 siva Exp $
 *
 * Description: This file contains snmp trap specific procedures.
 *************************************************************************/

#include "cfminc.h"
#include "fsecfm.h"
#include "fscfmmi.h"
#include "stdecfm.h"
#include "fsmiy173.h"

#ifdef SNMP_3_WANTED
PRIVATE INT4 EcfmParseSubIdNew PROTO ((INT1 **, UINT4 *));
PRIVATE tSNMP_OID_TYPE *EcfmMakeObjIdFromDotNew
PROTO ((INT1 *, struct MIB_OID *));
PRIVATE VOID        Y1731LbLtSnmpFrameDelayTrap
PROTO ((tEcfmLbLtFrmDelayBuff *, UINT1));
PRIVATE VOID Y1731CcSnmpFrameLossTrap PROTO ((tEcfmCcFrmLossBuff *, UINT1));
PRIVATE VOID Y1731LbLtSnmpTstErrRcvdTrap PROTO ((tEcfmLbLtMepInfo *, UINT1));

/******************************************************************************
 * Function : EcfmParseSubIdNew
 * Input    : ppi1TempPtr
 * Output   : value of ppu1TempPtr
 * Returns  : ECFM_SUCCESS or ECFM_FAILURE
 *******************************************************************************/
PRIVATE INT4
EcfmParseSubIdNew (INT1 **ppi1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    INT1               *pi1Tmp = NULL;
    INT4                i4RetVal = ECFM_SUCCESS;

    for (pi1Tmp = *ppi1TempPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                                 ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                                 ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F')));
         pi1Tmp++)
    {
        u4Value = (u4Value * ECFM_VAL_10) + (*pi1Tmp & ECFM_4BIT_MAX);
    }

    if (*ppi1TempPtr == pi1Tmp)
    {
        return ECFM_FAILURE;
    }
    *ppi1TempPtr = pi1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/******************************************************************************
 * Function : EcfmMakeObjIdFromDotNew                                         *
 * Input    : pi1TextStr
 * Output   : NONE
 * Returns  : pOidPtr or NULL
 *******************************************************************************/
PRIVATE tSNMP_OID_TYPE *
EcfmMakeObjIdFromDotNew (INT1 *pi1TextStr, struct MIB_OID * pmib_table)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = ECFM_INIT_VAL;
    UINT2               u2DotCount;
    static INT1         ai1TempBuffer[ECFM_ARRAY_SIZE_257];

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr) != ECFM_FALSE)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0;
             ((pi1TempPtr < pi1DotPtr) && (u2Index < ECFM_ARRAY_SIZE_256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; pmib_table[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP
                 (pmib_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (pmib_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pmib_table[u2Index].pNumber,
                         ECFM_ARRAY_SIZE_256);
                break;
            }
        }

        if (pmib_table[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 ECFM_ARRAY_SIZE_256);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 ECFM_ARRAY_SIZE_256);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         ((u2Index < ECFM_ARRAY_SIZE_257) && (ai1TempBuffer[u2Index] != '\0'));
         u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }

    if ((pOidPtr = alloc_oid (sizeof (tSNMP_OID_TYPE))) == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {

        if (u2Index > SNMP_MAX_OID_LENGTH)
        {
            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if ((EcfmParseSubIdNew
             ((INT1 **) (&(pi1TempPtr)),
              &(pOidPtr->pu4_OidList[u2Index]))) == ECFM_FAILURE)
        {
            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
 * Function : Y1731CcSnmpFrameLossTrap                                        
 * Description        : This function will prepare the TRAP PDU for Frame     
 *                      Delay                                                 
 * Input    : pFrmLossBuff, u1TrapId
 * Output   : NONE
 * Returns  : NONE
 *******************************************************************************/
PRIVATE VOID
Y1731CcSnmpFrameLossTrap (tEcfmCcFrmLossBuff * pFrmLossBuff, UINT1 u1TrapId)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT1              *pu1ContextName = NULL;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT1               au1TempContextName[ECFM_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];

    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));
    ECFM_MEMSET (au1TempContextName, ECFM_INIT_VAL,
                 (ECFM_SWITCH_ALIAS_LEN * sizeof (UINT1)));

   /*-----------------MI TRAP PDU y.1731 MIB trap
    * ECFM_FRAME_LOSS_TRAP"--------------------------------------------*
    * Enterprise_OID|context_name_OID|context_name|
    * fsMIY1731FlMeasurementTimeStamp|fsMIY1731FlMeasurementType
    * |fsMIY1731FlFarEndFrameLoss|sMIY1731FlNearEndFrameLoss
    *------------------------------------------------------------------*/
    /* Set Enterprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) Y1731_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    if (u1TrapId < Y1731_EX_TRAP_BASE_VAL)
    {
        u4SpecTrapType = u1TrapId;
    }
    else
    {
        u4SpecTrapType = u1TrapId / Y1731_EX_TRAP_INCR_VAL;
    }

    /* Fill Context Info */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_CONTEXT_NAME);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
    }
    if (EcfmVcmGetAliasName (ECFM_CC_CURR_CONTEXT_ID (), au1TempContextName)
        == VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (ECFM_SWITCH_ALIAS_LEN));
    pContextName = pOstring;
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    SnmpCnt64Type.msn = ECFM_INIT_VAL;
    SnmpCnt64Type.lsn = ECFM_INIT_VAL;

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pStartVb = pVbList;

    /* Fill trap info  with fsMIY1731FlMeasurementTimeStamp */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FL_MEASUR_TIMESTAMP);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  (UINT4) (pFrmLossBuff->
                                                           RxTimeStamp),
                                                  0, NULL, NULL, SnmpCnt64Type);

    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Fill trap info  with fsMIY1731FlMeasurementType */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FL_MEASUR_TYPE);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  (UINT4) (pFrmLossBuff->
                                                           u1LossMeasurementType),
                                                  0, NULL, NULL, SnmpCnt64Type);

    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Fill trap info  with fsMIY1731FdDelayValue */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FL_FAR_END_LOSS);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  pFrmLossBuff->u4FarEndLoss, 0,
                                                  NULL, NULL, SnmpCnt64Type);
    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    /* Fill Near End Loss */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FL_NEAR_END_LOSS);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmLossBuff->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  pFrmLossBuff->u4NearEndLoss,
                                                  0, NULL, NULL, SnmpCnt64Type);
    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (EcfmVcmGetSystemModeExt (ECFM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                                  u4SpecTrapType, pStartVb);
    }
    else
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                                  pStartVb, pContextName);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : Y1731LbLtSnmpFrameDelayTrap                          */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU for Frame    */
/*                      Delay                                                */
/*                                                                           */
/* Input(s)           : Info  Pointer to information required for TRAP  */
/*                    : pStartVb  Pointer to start of VarBind                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
Y1731LbLtSnmpFrameDelayTrap (tEcfmLbLtFrmDelayBuff * pFrmDelayBuffNode,
                             UINT1 u1TrapId)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pDelayValue = NULL;
    UINT1              *pu1ContextName = NULL;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT1               au1TempContextName[ECFM_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];
    UINT1              *pu1FdValue = NULL;

    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));
    ECFM_MEMSET (au1TempContextName, ECFM_INIT_VAL,
                 (ECFM_SWITCH_ALIAS_LEN * sizeof (UINT1)));

   /*-----------------MI TRAP PDU y.1731 MIB trap
    * ECFM_FRAME_LOSS_TRAP"--------------------------------------------*
    * Enterprise_OID|context_name_OID|context_name|
    * fsMIY1731FdMeasurementTimeStamp|fsMIY1731FdMeasurementType
    * |fsMIY1731FdDelayValue
    *------------------------------------------------------------------*/
    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) Y1731_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    if (u1TrapId < Y1731_EX_TRAP_BASE_VAL)
    {
        u4SpecTrapType = u1TrapId;
    }
    else
    {
        u4SpecTrapType = u1TrapId / Y1731_EX_TRAP_INCR_VAL;
    }

    /* Fill Context Info */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_CONTEXT_NAME);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_LBLT_CURR_CONTEXT_ID ();
    }
    if (EcfmVcmGetAliasName (ECFM_LBLT_CURR_CONTEXT_ID (), au1TempContextName)
        == VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (ECFM_SWITCH_ALIAS_LEN));
    pContextName = pOstring;
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    SnmpCnt64Type.msn = ECFM_INIT_VAL;
    SnmpCnt64Type.lsn = ECFM_INIT_VAL;

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pStartVb = pVbList;

    /* Fill trap info  with fsMIY1731FdMeasurementTimeStamp */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FD_MEASUR_TIMESTAMP);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_LBLT_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  (UINT4) (pFrmDelayBuffNode->
                                                           MeasurementTimeStamp),
                                                  0, NULL, NULL, SnmpCnt64Type);

    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Fill trap info  with fsMIY1731FdMeasurementType */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FD_MEASUR_TYPE);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_LBLT_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  (UINT4) (pFrmDelayBuffNode->
                                                           u1DmType), 0, NULL,
                                                  NULL, SnmpCnt64Type);

    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Fill trap info  with fsMIY1731FdDelayValue */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_FD_MEASUR_DELAY_VAL);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_LBLT_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4TransId;
        pOid->pu4_OidList[pOid->u4_Length++] = pFrmDelayBuffNode->u4SeqNum;
    }
    pu1FdValue = au1Buf;
    ECFM_PUT_4BYTE (pu1FdValue, pFrmDelayBuffNode->FrmDelayValue.u4Seconds);
    ECFM_PUT_4BYTE (pu1FdValue, pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds);
    pu1FdValue = au1Buf;
    pDelayValue =
        SNMP_AGT_FormOctetString (pu1FdValue,
                                  (INT4) (ECFM_TIMESTAMP_FIELD_SIZE));
    if (pDelayValue == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pDelayValue, NULL,
                                                  SnmpCnt64Type);
    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_AGT_FreeOctetString (pDelayValue);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (EcfmVcmGetSystemModeExt (ECFM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                                  u4SpecTrapType, pStartVb);
    }
    else
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                                  pStartVb, pContextName);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : Y1731LbLtSnmpTstErrRcvdTrap                          */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU for Tst Error*/
/*                      Received                                             */
/*                                                                           */
/* Input(s)           : pTrapInfo  Pointer to information required for TRAP  */
/*                    : pStartVb  Pointer to start of VarBind                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
Y1731LbLtSnmpTstErrRcvdTrap (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1TrapId)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT1              *pu1ContextName = NULL;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT1               au1TempContextName[ECFM_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];

    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));
    ECFM_MEMSET (au1TempContextName, ECFM_INIT_VAL,
                 (ECFM_SWITCH_ALIAS_LEN * sizeof (UINT1)));

   /*-----------------MI TRAP PDU y.1731 MIB trap
    * Y1731_TST_RCVD_ERR_TRAP_VAL"--------------------------------------------*
    * Enterprise_OID|context_name_OID|context_name|
    * fsMIY1731MepBitErroredTstIn
    *------------------------------------------------------------------*/
    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) Y1731_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    if (u1TrapId < Y1731_EX_TRAP_BASE_VAL)
    {
        u4SpecTrapType = u1TrapId;
    }
    else
    {
        u4SpecTrapType = u1TrapId / Y1731_EX_TRAP_INCR_VAL;
    }

    /* Fill Context Info */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_CONTEXT_NAME);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_LBLT_CURR_CONTEXT_ID ();
    }
    if (EcfmVcmGetAliasName (ECFM_LBLT_CURR_CONTEXT_ID (), au1TempContextName)
        == VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (ECFM_SWITCH_ALIAS_LEN));
    pContextName = pOstring;
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    SnmpCnt64Type.msn = ECFM_INIT_VAL;
    SnmpCnt64Type.lsn = ECFM_INIT_VAL;

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pStartVb = pVbList;

    /* Fill trap info  with BitError */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_BIT_ERR_TST_IN);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_LBLT_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pMepInfo->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pMepInfo->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pMepInfo->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pMepInfo->TstInfo.u4TstSeqNumber;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  (UINT4) (pMepInfo->TstInfo.
                                                           u4BitErroredTstIn),
                                                  0, NULL, NULL, SnmpCnt64Type);

    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (EcfmVcmGetSystemModeExt (ECFM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                                  u4SpecTrapType, pStartVb);
    }
    else
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                                  pStartVb, pContextName);
    }
    return;
}
#endif
/*****************************************************************************/
/* Function Name      : EcfmSnmpIfSendTrap                                   */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU and send it  */
/*                      to the administrator if particular trap is enabled   */
/*                                                                           */
/* Input(s)           : pPduSmInfo  Pointer to information required for TRAP */
/*                      u1TrapId - TrapIdentifier                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmSnmpIfSendTrap (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType;
    UINT4               u4NotificationType;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    struct MIB_OID     *pmib_table = NULL;
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];

    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pMepInfo->u2PortNum))
    {
        return;
    }
    /* Check if particular Trap is enabled */
    if (u1TrapId != ECFM_FAULT_ALARM_TRAP_VAL)
    {
        switch (u1TrapId)
        {
            case ECFM_XCONN_CCM_TRAP_VAL:
                if (ECFM_XCONN_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "Cross-connect defect occurred"));
                break;
            case ECFM_ERROR_CCM_TRAP_VAL:
                if (ECFM_ERR_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "Error CCM defect occurred"));
                break;
            case ECFM_REMOTE_CCM_TRAP_VAL:
                if (ECFM_REMOTE_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "Remote CCM defect occurred"));
                break;
            case ECFM_MAC_STATUS_TRAP_VAL:
                if (ECFM_MAC_STATUS_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "Mac Status defect occurred"));
                break;
            case ECFM_RDI_CCM_TRAP_VAL:
                if (ECFM_RDI_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "Rdi defect occurred"));
                break;
            default:
                return;
        }
    }
    else
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                      "FNG Fault Alarm Generated"));
    }
    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));

   /*----------------TRAP PDU(proprietary traps)-----------------------------------*/
    /* Enterprise_OID|trap_type_OID appended with indices|trap_type_val             */
   /*------------------------------------------------------------------------------*/

   /*----------------TRAP PDU(standard MIB trap"ECFM_FAULT_ALARM_TRAP"--------------*/
    /* Enterprise_OID|HighestDefectPri_OID appended with indices|HighestDefectPri_val */
   /*-------------------------------------------------------------------------------*/

    /* Set Entrprise OID */
    if (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL)
    {
        pEnterpriseOid =
            SNMP_AGT_GetOidFromString ((INT1 *) ECFM_STD_TRAPS_OID);
    }
    else
    {
        pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) ECFM_FS_TRAPS_OID);
    }

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    /* Notification value for fsEcfmMepDefectTrap and
     * dot1agCfmFaultAlarm are ONE */
    u4NotificationType = ECFM_SI_TRAP_NOTIFICATION;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Get OID corresponding to the trap type mib object */
    if (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL)
    {
        SPRINTF ((char *) au1Buf, ECFM_MIB_OBJ_MEP_HIGHEST_PRIORITY_DEF);
        pmib_table = orig_mib_oid_table;
    }
    else
    {
        SPRINTF ((char *) au1Buf, ECFM_MIB_OBJ_TRAP_TYPE);
        pmib_table = orig_mib_oid_table_si;
    }
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, pmib_table);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    /* The Object is tabular. Hence append the Index value */
    /* Append Indices according to the trap type OID, trap is specific
     * to either MEP or Remote MEP so indices are appended accordingly */

    /* Traps specific to MEP */
    if ((u1TrapId == ECFM_XCONN_CCM_TRAP_VAL) ||
        (u1TrapId == ECFM_ERROR_CCM_TRAP_VAL) ||
        (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL))
    {
        pMepNode = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
        /* Append MdIndex, MaIndex and MepId to the OID */
        if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
        {
            pOid->pu4_OidList[pOid->u4_Length++] = pMepNode->u4MdIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = pMepNode->u4MaIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = (UINT4) (pMepNode->u2MepId);
        }
    }
    /* Traps specific to remote MEP */
    else if ((u1TrapId == ECFM_REMOTE_CCM_TRAP_VAL) ||
             (u1TrapId == ECFM_MAC_STATUS_TRAP_VAL) ||
             (u1TrapId == ECFM_RDI_CCM_TRAP_VAL))
    {
        pRMepNode = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);
        /* Append MdIndex, MaIndex and MepId, Remote MepId to the OID */
        if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
        {
            pOid->pu4_OidList[pOid->u4_Length++] = pRMepNode->u4MdIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = pRMepNode->u4MaIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = (UINT4) (pRMepNode->u2MepId);
            pOid->pu4_OidList[pOid->u4_Length++] =
                (UINT4) (pRMepNode->u2RMepId);
        }
    }

    /* Bind corresponding value to the OID */
    if (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL)
    {
        pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_UNSIGNED32,
                                        (UINT4) (pMepNode->FngInfo.
                                                 u1HighestDefectPri), 0, NULL,
                                        NULL, SnmpCnt64Type);
    }
    else
    {
        pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_UNSIGNED32,
                                        (UINT4) (u1TrapId), 0, NULL, NULL,
                                        SnmpCnt64Type);

    }
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    /* The following API sends the Trap info to the SNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                              u4NotificationType, pVbList);
#else
    UNUSED_PARAM (pPduSmInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/*****************************************************************************/
/* Function Name      : EcfmMISnmpIfSendTrap                                 */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU and send it  */
/*                      to the administrator if particular trap is enabled   */
/*                                                                           */
/* Input(s)           : pPduSmInfo  Pointer to information required for TRAP */
/*                      u1TrapId - TrapIdentifier                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmMISnmpIfSendTrap (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType;
    UINT4               u4NotificationType;
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    UINT1              *pu1ContextName = NULL;
    UINT1               au1TempContextName[ECFM_SWITCH_ALIAS_LEN];
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pMepInfo->u2PortNum))
    {
        return;
    }
    /* Check if particular Trap is enabled */
    if (u1TrapId != ECFM_FAULT_ALARM_TRAP_VAL)
    {
        switch (u1TrapId)
        {
            case ECFM_XCONN_CCM_TRAP_VAL:
                if (ECFM_XCONN_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d Cross-connect defect occurred",
                              ECFM_CC_CURR_CONTEXT_ID ()));
                break;
            case ECFM_ERROR_CCM_TRAP_VAL:
                if (ECFM_ERR_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d Error CCM defect occurred",
                              ECFM_CC_CURR_CONTEXT_ID ()));

                break;
            case ECFM_REMOTE_CCM_TRAP_VAL:
                if (ECFM_REMOTE_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d Remote CCM defect occurred",
                              ECFM_CC_CURR_CONTEXT_ID ()));

                break;
            case ECFM_MAC_STATUS_TRAP_VAL:
                if (ECFM_MAC_STATUS_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d Mac Status defect occurred",
                              ECFM_CC_CURR_CONTEXT_ID ()));

                break;
            case ECFM_RDI_CCM_TRAP_VAL:
                if (ECFM_RDI_CCM_TRAP_DISABLED () == ECFM_TRUE)
                {
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d Rdi defect occurred",
                              ECFM_CC_CURR_CONTEXT_ID ()));

                break;

            default:
                return;
        }
    }
    else
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                      "context-id: %d FNG Fault Alarm Generated",
                      ECFM_CC_CURR_CONTEXT_ID ()));
    }

    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));
    ECFM_MEMSET (au1TempContextName, ECFM_INIT_VAL,
                 (ECFM_SWITCH_ALIAS_LEN * sizeof (UINT1)));

   /*-----------------MI TRAP PDU (proprietary traps)--------------------------------------------------------------*/
    /* Enterprise_OID|context_name_OID|context_name|trap_type_OID appended with indices|trap_type_val               */
   /*--------------------------------------------------------------------------------------------------------------*/

   /*-----------------MI TRAP PDU (standard MIB trap "ECFM_FAULT_ALARM_TRAP"---------------------------------------*/
    /* Enterprise_OID|context_name_OID|context_name|HighestDefectPri_OID appended with indices|HighestDefectPri_val */
   /*--------------------------------------------------------------------------------------------------------------*/

    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) ECFM_PROP_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    /* Notification value of fsMIEcfmFaultAlarm is ONE 
     * and fsEcfmMepDefectTrap is TWO */
    if (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL)
    {
        u4NotificationType = ECFM_FAULT_TRAP_NOTIFICATION;
    }
    else
    {
        u4NotificationType = ECFM_DEFECT_TRAP_NOTIFICATION;
    }
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Fill Context Info */
    SPRINTF ((char *) au1Buf, ECFM_MIB_OBJ_CONTEXT_NAME);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_mi);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    /* As VCM cant be compiled with SI so commented until VCM works fine in SI
     * mode, Incase of MI uncomment it */
    if (EcfmVcmGetAliasName (ECFM_CC_CURR_CONTEXT_ID (), au1TempContextName) ==
        VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (ECFM_SWITCH_ALIAS_LEN));
    pContextName = pOstring;
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pStartVb = pVbList;

    /* Fill trap info depending upon particular trap */
    /* Get OID corresponding to the trap_type mib object */
    if (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL)
    {
        SPRINTF ((char *) au1Buf, ECFM_MIB_OBJ_MEP_HIGHEST_PRIORITY_DEF);
    }
    else
    {
        SPRINTF ((char *) au1Buf, ECFM_MIB_OBJ_TRAP_TYPE);
    }

    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_mi);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    /* The Object is tabular. Hence append the Index value */
    /* Append Indices according to the trap type OID, trap is specific
     * to either MEP or Remote MEP so indices are appended accordingly */

    /* Traps specific to MEP */
    if ((u1TrapId == ECFM_XCONN_CCM_TRAP_VAL) ||
        (u1TrapId == ECFM_ERROR_CCM_TRAP_VAL) ||
        (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL))
    {
        pMepNode = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
        if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
        {
            pOid->pu4_OidList[pOid->u4_Length++] = pMepNode->u4MdIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = pMepNode->u4MaIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = (UINT4) (pMepNode->u2MepId);
        }
    }
    /* Traps specific to remote MEP */
    else if ((u1TrapId == ECFM_REMOTE_CCM_TRAP_VAL) ||
             (u1TrapId == ECFM_MAC_STATUS_TRAP_VAL) ||
             (u1TrapId == ECFM_RDI_CCM_TRAP_VAL))
    {
        pRMepNode = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);
        if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
        {
            pOid->pu4_OidList[pOid->u4_Length++] = pRMepNode->u4MdIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = pRMepNode->u4MaIndex;
            pOid->pu4_OidList[pOid->u4_Length++] = (UINT4) (pRMepNode->u2MepId);
            pOid->pu4_OidList[pOid->u4_Length++] =
                (UINT4) (pRMepNode->u2RMepId);
        }
    }

    /* Bind corresponding value to the OID */
    if (u1TrapId == ECFM_FAULT_ALARM_TRAP_VAL)
    {
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_UNSIGNED32,
                                                      (UINT4) (pMepNode->
                                                               FngInfo.
                                                               u1HighestDefectPri),
                                                      0, NULL, NULL,
                                                      SnmpCnt64Type);
    }
    else
    {
        pVbList->pNextVarBind =
            SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_UNSIGNED32,
                                  (UINT4) (u1TrapId), 0, NULL, NULL,
                                  SnmpCnt64Type);
    }
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    /* The following API sends the Trap info to the SNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4NotificationType,
                              pStartVb, pContextName);
#else
    UNUSED_PARAM (pPduSmInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/*****************************************************************************/
/* Function Name      : Y1731CcSnmpIfSendTrap                                */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU and send it  */
/*                      to the administrator if particular trap is enabled   */
/*                                                                           */
/* Input(s)           : pTrapInfo - Pointer  to the information required     */
/*                      for TRAP                                             */
/*                      u1TrapId - TrapIdentifier                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
Y1731CcSnmpIfSendTrap (VOID *pTrapInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcErrLogInfo  *pCcErrLogInfo = NULL;
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    UINT1              *pu1ContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT1               au1TempContextName[ECFM_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];

    if (u1TrapId == Y1731_FRM_LOSS_TRAP_VAL)
    {
        pFrmLossBuffNode = (tEcfmCcFrmLossBuff *) pTrapInfo;
        if (pFrmLossBuffNode == NULL)
        {
            return;
        }
        pMepNode = EcfmCcUtilGetMepEntryFrmGlob (pFrmLossBuffNode->u4MdIndex,
                                                 pFrmLossBuffNode->u4MaIndex,
                                                 pFrmLossBuffNode->u2MepId);
    }
    else
    {
        pCcErrLogInfo = (tEcfmCcErrLogInfo *) pTrapInfo;
        /*If error log is disabled, trap will not be generated */
        if (pCcErrLogInfo == NULL)
        {
            return;
        }

        /* Get MEP entry corresponding to MegIndex, MeIndex, MepId */
        pMepNode = EcfmCcUtilGetMepEntryFrmGlob (pCcErrLogInfo->u4MdIndex,
                                                 pCcErrLogInfo->u4MaIndex,
                                                 pCcErrLogInfo->u2MepId);
    }
    if (pMepNode == NULL)
    {
        return;
    }

    /* Check the Y.1731 Status */
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        return;
    }
    switch (u1TrapId)
    {
        case Y1731_FRM_LOSS_TRAP_VAL:
            if (Y1731_FRM_LOSS_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                          "context-id: %d frame loss exceeded configured threshold",
                          ECFM_CC_CURR_CONTEXT_ID ()));
            break;
        case Y1731_RDI_CCM_EN_TRAP_VAL:
        case Y1731_RDI_CCM_EX_TRAP_VAL:
            if (Y1731_RDI_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_RDI_CCM_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d remote defect indication defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {

                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d remote defect indication defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_LOC_TRAP_EN_VAL:
        case Y1731_LOC_TRAP_EX_VAL:
            if (Y1731_LOC_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_LOC_TRAP_EN_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d loss of continuity defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d loss of continuity defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_UNEXP_PERIOD_EN_TRAP_VAL:
        case Y1731_UNEXP_PERIOD_EX_TRAP_VAL:
            if (Y1731_UNEXP_PERIOD_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_UNEXP_PERIOD_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d unexpected ccm period defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d unexpected ccm period defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_UNEXP_MEP_EN_TRAP_VAL:
        case Y1731_UNEXP_MEP_EX_TRAP_VAL:
            if (Y1731_UNEXP_MEP_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_UNEXP_MEP_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d unexpected mep defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d unexpected mep defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_MISMERGE_EN_TRAP_VAL:
        case Y1731_MISMERGE_EX_TRAP_VAL:
            if (Y1731_MISMERGE_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_MISMERGE_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d mismerge defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d mismerge defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_UNEXP_LEVEL_EN_TRAP_VAL:
        case Y1731_UNEXP_LEVEL_EX_TRAP_VAL:
            if (Y1731_UNEXP_LEVEL_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_UNEXP_LEVEL_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d unexpected meg level defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d unexpected meg level defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_LLF_EN_TRAP_VAL:
        case Y1731_LLF_EX_TRAP_VAL:
            if (Y1731_LLF_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_LLF_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d local link failure defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d local link failure defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_INT_HW_FAIL_EN_TRAP_VAL:
        case Y1731_INT_HW_FAIL_EX_TRAP_VAL:
            if (Y1731_INT_HW_FAIL_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_INT_HW_FAIL_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d internal hardware failure defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d internal hardware failure defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_INT_SW_FAIL_EN_TRAP_VAL:
        case Y1731_INT_SW_FAIL_EX_TRAP_VAL:
            if (Y1731_INT_SW_FAIL_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_INT_SW_FAIL_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d internal software failure defect - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d internal hardware failure defect - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_AIS_COND_EN_TRAP_VAL:
        case Y1731_AIS_COND_EX_TRAP_VAL:
            if (Y1731_AIS_COND_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_AIS_COND_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d alarm indication signal condition - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d alarm indication signal condition - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;
        case Y1731_LCK_COND_EN_TRAP_VAL:
        case Y1731_LCK_COND_EX_TRAP_VAL:
            if (Y1731_LCK_COND_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            if (u1TrapId == Y1731_LCK_COND_EN_TRAP_VAL)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d locked signal condition - entry",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                              "context-id: %d locked signal condition - exit",
                              ECFM_CC_CURR_CONTEXT_ID ()));
            }
            break;

        default:
            return;
    }
    if (u1TrapId == Y1731_FRM_LOSS_TRAP_VAL)
    {
        Y1731CcSnmpFrameLossTrap (pFrmLossBuffNode, u1TrapId);
        return;
    }
    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));
    ECFM_MEMSET (au1TempContextName, ECFM_INIT_VAL,
                 (ECFM_SWITCH_ALIAS_LEN * sizeof (UINT1)));

   /*-----------------MI TRAP PDU y.1731 MIB trap
    * ECFM_DEFECT_CONDITION_TRAP---------------------------------------------*
    * Enterprise_OID|context_name_OID|context_name|fsMIY1731ErrorLogType     *
    *------------------------------------------------------------------------*/
    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) Y1731_TRAPS_OID);

    u1TrapId = Y1731_DEFECT_CONDN_TRAP_VAL;

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    if (u1TrapId < Y1731_EX_TRAP_BASE_VAL)
    {
        u4SpecTrapType = u1TrapId;
    }
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Fill Context Info */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_CONTEXT_NAME);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
    }
    if (EcfmVcmGetAliasName (ECFM_CC_CURR_CONTEXT_ID (), au1TempContextName) ==
        VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (ECFM_SWITCH_ALIAS_LEN));
    pContextName = pOstring;
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pStartVb = pVbList;
    /* Fill trap info  */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_ERROR_LOG_TYPE);

    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length++] = ECFM_CC_CURR_CONTEXT_ID ();
        pOid->pu4_OidList[pOid->u4_Length++] = pCcErrLogInfo->u4MdIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pCcErrLogInfo->u4MaIndex;
        pOid->pu4_OidList[pOid->u4_Length++] = pCcErrLogInfo->u2MepId;
        pOid->pu4_OidList[pOid->u4_Length++] = pCcErrLogInfo->u4SeqNum;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_UNSIGNED32,
                                                  (UINT4) (pCcErrLogInfo->
                                                           u2LogType), 0, NULL,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    if (EcfmVcmGetSystemModeExt (ECFM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                                  u4SpecTrapType, pStartVb);
    }
    else
    {
        /* The following API sends the Trap info to the SNMP Agent. */
        SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                                  pStartVb, pContextName);
    }

#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/*****************************************************************************/
/* Function Name      : Y1731LbLtSnmpIfSendTrap                              */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU and send it  */
/*                      to the administrator if particular trap is enabled   */
/*                                                                           */
/* Input(s)           : pTrapInfo  Pointer to information required for TRAP  */
/*                      u1TrapId - TrapIdentifier                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
Y1731LbLtSnmpIfSendTrap (VOID *pTrapInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tEcfmLbLtFrmDelayBuff *pFrmDelayNode = NULL;
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    if (pTrapInfo == NULL)
    {
        return;
    }
    if (u1TrapId == Y1731_FRM_DELAY_TRAP_VAL)
    {
        pFrmDelayNode = (tEcfmLbLtFrmDelayBuff *) pTrapInfo;
        pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (pFrmDelayNode->u4MdIndex,
                                                   pFrmDelayNode->u4MaIndex,
                                                   pFrmDelayNode->u2MepId);
    }
    else
    {
        pMepNode = (tEcfmLbLtMepInfo *) pTrapInfo;
    }

    if (pMepNode == NULL)
    {
        return;
    }

    /* Check the Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        return;
    }
    switch (u1TrapId)
    {
        case Y1731_TST_RCVD_ERR_TRAP_VAL:
            if (Y1731_TST_RCVD_ERR_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                          "context-id: %d TST-PDU recived with error",
                          ECFM_LBLT_CURR_CONTEXT_ID ()));
            Y1731LbLtSnmpTstErrRcvdTrap (pMepNode, u1TrapId);
            break;
        case Y1731_FRM_DELAY_TRAP_VAL:
            if (Y1731_FRM_DELAY_TRAP_DISABLED () == ECFM_TRUE)
            {
                return;
            }
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                          "context-id: %d frame delay exceeded configured threshold",
                          ECFM_LBLT_CURR_CONTEXT_ID ()));
            Y1731LbLtSnmpFrameDelayTrap (pFrmDelayNode, u1TrapId);
            break;
        default:
            return;
    }
#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/*****************************************************************************/
/* Function Name      : Y1731LbLtSnmpIfSendTxFailTrap                        */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU and send it  */
/*                      to the administrator if CFM TX Failure Occurred       */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number on which transmit failure    */
/*                                  occurred                                  */
/*                      u1OpCode  - OpCode of the cfm-pdu for which transmit */
/*                                  failure occurred.                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
Y1731LbLtSnmpIfSendTxFailTrap (UINT2 u2PortNum, UINT1 u1OpCode)
{
#ifdef SNMP_3_WANTED
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT1               au1Buf[ECFM_ARRAY_SIZE_256];
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return;
    }
    pPortInfo->u1LastCfmTxFailOpcode = u1OpCode;
    if (gpEcfmLbLtContextInfo != NULL)
    {
        gpEcfmLbLtContextInfo->LbLtStats.u1LastCfmTxFailOpcode = u1OpCode;
    }
    u4IfIndex = pPortInfo->u4IfIndex;
    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                  "Transmission failure occurred for OpCode %d on interface %d",
                  u1OpCode, u4IfIndex));
    ECFM_MEMSET (au1Buf, ECFM_INIT_VAL, (ECFM_ARRAY_SIZE_256 * sizeof (UINT1)));

   /*-----------------MI TRAP PDU y.1731 MIB trap
    * ECFM_CFM_TX_FAILURE_TRAP"--------------------------------------------*
    * Enterprise_OID|fsMIY1731LastTxFailOpcode_Oid|Port-Index|OPCODE Value
    *------------------------------------------------------------------*/
    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) Y1731_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = Y1731_LAST_CFM_TX_FAIL_TRAP_VAL;
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Fill Context Info */
    SPRINTF ((char *) au1Buf, Y1731_MIB_OBJ_LAST_CFM_FAIL_OPCODE);
    pOid = EcfmMakeObjIdFromDotNew ((INT1 *) au1Buf, orig_mib_oid_table_y1731);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        /*Append the Port Number */
        pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;
    }
    pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_UNSIGNED32, (UINT4)u1OpCode,
                                    0, NULL, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    pStartVb = pVbList;
    /* The following API sends the Trap info to the SNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                              u4SpecTrapType, pStartVb);
#else
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (u1OpCode);
#endif
}

/******************************************************************************/
/*                           End  of file cfmtrap.c                           */
/******************************************************************************/
