/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmltmsm.c,v 1.53 2015/04/23 12:03:48 siva Exp $
 *
 * Description: This file contains the Functionality of the 
 *              Link Trace message Receiver State Machine.
 *******************************************************************/

#include "cfminc.h"
PRIVATE INT4 EcfmLtmRxSmEnqueLtr PROTO ((tEcfmLbLtPduSmInfo *, UINT2, BOOL1));
PRIVATE INT4 EcfmLtmRxSmValidateLtm PROTO ((tEcfmLbLtPduSmInfo *));
PRIVATE VOID EcfmLtmRxSmPutLtrInfo PROTO ((tEcfmLbLtRxLtmPduInfo *, UINT1 **));
PRIVATE VOID        EcfmLtmRxSmPutLtrTlv
PROTO ((tEcfmLbLtPduSmInfo *, UINT2, UINT1 **, BOOL1));
PRIVATE INT4        EcfmLtmAHRxSmGetEgressPort
PROTO ((tEcfmLbLtPduSmInfo *, UINT2 *));
PRIVATE INT4 EcfmLtmRxSmForwardLtm PROTO ((tEcfmLbLtPduSmInfo *, UINT2));
PRIVATE INT4 EcfmMipProcessLtm PROTO ((tEcfmLbLtPduSmInfo *, UINT2 *, BOOL1
                                       *, BOOL1 *));
PRIVATE INT4        EcfmMepProcessLtm
PROTO ((tEcfmLbLtPduSmInfo *, UINT2 *, BOOL1 *, BOOL1 *));
PRIVATE INT4 EcfmLtmRxFwdLtmToFf PROTO ((tEcfmLbLtPduSmInfo *, BOOL1 *));
PRIVATE INT4        EcfmLtmRxFwdLtmToPort
PROTO ((tEcfmLbLtPduSmInfo *, BOOL1 *, UINT1));
PRIVATE INT4 EcfmGetMipDbEntry PROTO ((UINT2, UINT1 *, UINT2 *));
PRIVATE VOID EcfmGetIngressEgressAction PROTO ((UINT2, UINT2, UINT1 *, BOOL1));
PRIVATE VOID        EcfmLtmCopyUnknownTlvFromLtm
PROTO ((tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 **ppu1Pdu));
/*****************************************************************************
 * Function           : EcfmMipProcessLtm 
 *
 * Description        : This routine is used to process the received LTM, when 
 *                      the receiving entity is MIP.
 *                      
 * Input(s)           :  pPudSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other  information.
 *
 * Output(s)          : pu2EgressPort - EgressPort Identified for transmitting 
 *                                      LTR or forwarding LTM .
 *                      pb1FwdLtm     - Flag value for forwarding the LTM if 
 *                                      required
 *                      pb1NotEnqueLtr- Flag value to enque LTR if required
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmMipProcessLtm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT2 *pu2EgressPort,
                   BOOL1 * pb1LtmFwd, BOOL1 * pb1NotEnqueLtr)
{
    tEcfmLbLtRxLtmPduInfo *pRxLtm = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    UINT2               u2EgressPort = ECFM_INIT_VAL;
    UINT1               u1PortState = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;    /* Md level at which this 
                                                     * MP is configured 
                                                     */
    tEcfmMacAddr        MacAddr = {
        0
    };
    BOOL1               b1FwdLtm = ECFM_FALSE;
    BOOL1               b1RlyFdbflag = ECFM_TRUE;    /* Relay FDB flag is 
                                                     * TRUE when FDB Look up
                                                     * happens*/

    *pb1NotEnqueLtr = ECFM_FALSE;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get Received LTM info from the PduSmInfo */
    pRxLtm = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Store Index from where the LTR is to be transmitted */
    pRxLtm->u2LtrOutPortNum = pPduSmInfo->pStackInfo->u2PortNum;

    /* Store Direction where the LTR is to be transmitted */
    pRxLtm->u1LtrOutDirection = pPduSmInfo->pStackInfo->u1Direction;
    pRxLtm->pEgressStackInfo = NULL;
    switch (pPduSmInfo->pStackInfo->u1Direction)

    {
        case ECFM_MP_DIR_DOWN:

            /* Store Index where the LTM is Received */
            pRxLtm->u2LtmInPortNum = pPduSmInfo->pStackInfo->u2PortNum;

            /* Compares the Target address of LTM PDU with MIPs Mac 
             * Address */
            ECFM_GET_MAC_ADDR_OF_PORT (pPduSmInfo->pPortInfo->u4IfIndex,
                                       MacAddr);
            if (ECFM_COMPARE_MAC_ADDR (pRxLtm->TargetMacAddr, MacAddr)
                != ECFM_SUCCESS)

            {

                /* Get the Spanning tree state of bridge port and       
                 * Vid */
                u1PortState =
                    EcfmL2IwfGetVlanPortState ((UINT2) pPduSmInfo->
                                               u4RxVlanIdIsId,
                                               pPduSmInfo->pPortInfo->
                                               u4IfIndex);

                /* Check for the Port State is FORWARDING */
                if ((ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT
                     (pPduSmInfo->pStackInfo->u2PortNum)) &&
                    (u1PortState != AST_PORT_STATE_FORWARDING))

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMipProcessLtm:"
                                   "The spanning tree state of bridge port and"
                                   "vlan_id of LTM is not Forwarding \r\n");
                    return ECFM_FAILURE;
                }

                else

                {
                    if (ECFM_LBLT_GET_USE_FDB_ONLY (pPduSmInfo->u1RxFlags))
                    {
                        if ((ECFM_LBLT_802_1AH_BRIDGE ()?
                             EcfmLtmAHRxSmGetEgressPort (pPduSmInfo,
                                                         &u2EgressPort) :
                             EcfmLtmRxSmGetEgressPort (pPduSmInfo,
                                                       &u2EgressPort,
                                                       &b1RlyFdbflag)) ==
                            ECFM_SUCCESS)
                        {

                            /* Set Relay Action that will be filled in LTR as 
                               RLY_FDB */
                            if (b1RlyFdbflag == ECFM_TRUE)
                            {
                                pRxLtm->u1RelayAction = ECFM_LTR_RLY_FDB;
                            }
                        }

                        else

                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMipProcessLtm:"
                                           "Unique Egress Port not determined "
                                           "\r\n");

                            if (EcfmLtmRxFwdLtmToFf
                                (pPduSmInfo, pb1NotEnqueLtr) == ECFM_SUCCESS)

                            {

                                /*LTM Fowarded to all egress ports */
                                break;
                            }
                            return ECFM_FAILURE;
                        }
                    }
                    else
                        /*Search in MIP CCM DB */
                    {
                        /* Get entry for specified VLAN ID and Source 
                         * MAC Address */
                        if (EcfmGetMipDbEntry
                            ((INT2) pPduSmInfo->u4RxVlanIdIsId,
                             pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                             &u2EgressPort) == ECFM_SUCCESS)

                        {

                            /* Set Relay Action that will be filled in
                             * LTR as RLY_MPDB */
                            pRxLtm->u1RelayAction = ECFM_LTR_RLY_MPDB;
                        }

                        else

                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMipProcessLtm:"
                                           "Unique Egress Port not"
                                           "determined \r\n");
                            if (EcfmLtmRxFwdLtmToFf
                                (pPduSmInfo, pb1NotEnqueLtr) == ECFM_SUCCESS)

                            {

                                /*LTM Fowarded to all egress ports */
                                break;
                            }
                            return ECFM_FAILURE;
                        }
                    }            /* end of else for search in MIP CCM DB */

                    /*Y.1731: If Egress Port found is same as the ingress port 
                     * than Ltm is forwadred to all the Ports*/
                    if (u2EgressPort == pPduSmInfo->pStackInfo->u2PortNum)

                    {
                        if (EcfmLtmRxFwdLtmToFf (pPduSmInfo, pb1NotEnqueLtr)
                            == ECFM_SUCCESS)

                        {

                            /*LTM Fowarded to all egress ports */
                            break;
                        }
                        return ECFM_FAILURE;
                    }

                    /* Check if on found unique Egress Port, Up MHF is found, 
                     * then is enqueued and if the target MAC Address matches 
                     * then should be discarded.  If the TTL is not 0/1 then 
                     * only LTM is forwarded */
                    pStackInfo = EcfmLbLtUtilGetMp (u2EgressPort,
                                                    pPduSmInfo->u1RxMdLevel,
                                                    (UINT2) pPduSmInfo->
                                                    u4RxVlanIdIsId,
                                                    ECFM_MP_DIR_UP);

                    if (pStackInfo != NULL)
                    {
                        if (ECFM_LBLT_IS_MHF (pStackInfo))
                        {
                            pMipNode = pStackInfo->pLbLtMipInfo;

                            if (ECFM_LBLT_IS_MIP_ACTIVE (pMipNode) != ECFM_TRUE)
                            {
                                u2EgressPort = 0;
                                break;
                            }

                        }
                        else
                        {
                            pMepInfo = pStackInfo->pLbLtMepInfo;
                            if (ECFM_LBLT_IS_MEP_ACTIVE (pMepInfo) != ECFM_TRUE)
                            {
                                u2EgressPort = 0;
                                break;
                            }
                        }
                    }

                    if (pStackInfo != NULL)
                    {

                        pRxLtm->pEgressStackInfo = pStackInfo;

                        /* Fill adress of StackInfo in PduSmInfo */
                        pPduSmInfo->pStackInfo = pStackInfo;
                        pPduSmInfo->pPortInfo =
                            ECFM_LBLT_GET_PORT_INFO (u2EgressPort);
                        if (ECFM_LBLT_IS_MHF (pStackInfo))

                        {
                            if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum)
                                == NULL)
                            {
                                return ECFM_FAILURE;
                            }
                            ECFM_GET_MAC_ADDR_OF_PORT
                                (ECFM_LBLT_PORT_INFO (pStackInfo->u2PortNum)->
                                 u4IfIndex, MacAddr);
                            if (ECFM_COMPARE_MAC_ADDR
                                (pRxLtm->TargetMacAddr,
                                 MacAddr) != ECFM_SUCCESS)
                            {
                                /* IEEE 802.1ag Section 20.3.2
                                 * If the LTM TTL is 0 or 1, the LTM is not
                                 * forwarded to the next hop, and if 0, no LTR 
                                 * is generated
                                 */
                                if (pRxLtm->u1RxLtmTtl > 1)
                                {
                                    b1FwdLtm = ECFM_TRUE;
                                }
                                else
                                {
                                    ECFM_LBLT_TRC_ARG1 (ECFM_ALL_FAILURE_TRC |
                                                        ECFM_CONTROL_PLANE_TRC,
                                                        "EcfmMipProcessLtm:"
                                                        "LTM TTL is %d. LTM "
                                                        "cannot be "
                                                        "Forwarded \r\n",
                                                        pRxLtm->u1RxLtmTtl);
                                }
                            }
                            else
                            {

                                /* Set Relay Action that will be filled in LTR 
                                 * as RLY_HIT as Egress Port Matches the Target
                                 * MAC Address of the LTM */
                                pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;
                            }

                            /* Store Index from where the LTR is to be 
                             * transmitted */
                            pRxLtm->u2LtrOutPortNum =
                                (UINT4) (pPduSmInfo->pStackInfo->u2PortNum);

                            /* Store Direction where the LTR is to be 
                             * transmitted */
                            pRxLtm->u1LtrOutDirection =
                                pPduSmInfo->pStackInfo->u1Direction;
                        }        /* End of IS_MHF */
                        else
                        {

                            /* Get the MEP Info  from the Stack Info indexed by 
                             * Vlan Id and the MD Level */
                            pMepInfo = pStackInfo->pLbLtMepInfo;
                            /* Fill adress of MepInfo in PduSmInfo */
                            pPduSmInfo->pMepInfo = pMepInfo;

                            /*Up Mep at same MD-Level is found, Enqueue LTR
                             * and no further processing should be done */
                            /* Check if Egress port is Target Mac Address */
                            if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum)
                                == NULL)
                            {
                                return ECFM_FAILURE;
                            }
                            ECFM_GET_MAC_ADDR_OF_PORT
                                (ECFM_LBLT_PORT_INFO (pStackInfo->u2PortNum)->
                                 u4IfIndex, MacAddr);
                            if (ECFM_COMPARE_MAC_ADDR
                                (pRxLtm->TargetMacAddr,
                                 MacAddr) == ECFM_SUCCESS)

                            {

                                /* Set Relay Action that will be filled in LTR 
                                 * as RLY_HIT as Egress Port Matches the Target
                                 * MAC Address of the LTM */
                                pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;
                            }
                            break;
                        }
                    }
                    else        /* Check for higher level MP or Lower */
                    {
                        for (u1MdLevel = (UINT1) (pPduSmInfo->u1RxMdLevel + 1);
                             u1MdLevel <= ECFM_MD_LEVEL_MAX;
                             u1MdLevel = u1MdLevel + 1)

                        {
                            pStackInfo =
                                EcfmLbLtUtilGetMp (u2EgressPort, u1MdLevel,
                                                   (UINT2) pPduSmInfo->
                                                   u4RxVlanIdIsId,
                                                   ECFM_MP_DIR_UP);

                            /* If MP at Higer Level exists on Port */
                            if (pStackInfo != NULL)

                            {
                                if ((ECFM_LBLT_IS_MEP (pStackInfo))
                                    || (ECFM_LBLT_IS_MHF (pStackInfo)))

                                {

                                    /* LTM should be discarded and no further
                                     * processin should be done */
                                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                                   ECFM_CONTROL_PLANE_TRC,
                                                   "EcfmMipProcessLtm: Higher "
                                                   "Level MEP/MHF is found on "
                                                   "the Egresss Port."
                                                   "LTM is discarded \r\n");
                                    return ECFM_FAILURE;
                                }
                            }    /*end of else - StackInfo is NULL */
                        }        /* end of for loop for Higher Md Level */

                        /* IEEE 802.1ag Section 20.3.2
                         * If the LTM TTL is 0 or 1, the LTM is not
                         * forwarded to the next hop, and if 0, no LTR 
                         * is generated
                         */

                        /* Forward the CFM-PDU in case a No MP is found at 
                         * equal or higher level */
                        if (pRxLtm->u1RxLtmTtl > 1)
                        {
                            b1FwdLtm = ECFM_TRUE;
                        }
                        else
                        {
                            ECFM_LBLT_TRC_ARG1 (ECFM_ALL_FAILURE_TRC |
                                                ECFM_CONTROL_PLANE_TRC,
                                                "EcfmMipProcessLtm:"
                                                "LTM TTL is %d. LTM cannot be"
                                                "Forwarded \r\n",
                                                pRxLtm->u1RxLtmTtl);
                        }
                    }            /* end of else for higher Md level */
                }                /* End of Else for VLan Port State not 
                                   FORWARDING */
                break;
            }                    /* End of If - Comparing Target MAC Address */

            /* Set Relay Action that will be filled in LTR as RLY_HIT */
            pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;
            break;
        case ECFM_MP_DIR_UP:

            /* Compares the Target address of LTM PDU with MIPs Mac 
             * Address */
            ECFM_GET_MAC_ADDR_OF_PORT (pPduSmInfo->pPortInfo->u4IfIndex,
                                       MacAddr);
            if (ECFM_COMPARE_MAC_ADDR (pRxLtm->TargetMacAddr, MacAddr) !=
                ECFM_SUCCESS)

            {
                if ((ECFM_LBLT_802_1AH_BRIDGE ()?
                     EcfmLtmAHRxSmGetEgressPort (pPduSmInfo, &u2EgressPort) :
                     EcfmLtmRxSmGetEgressPort (pPduSmInfo,
                                               &u2EgressPort,
                                               &b1RlyFdbflag)) == ECFM_SUCCESS)
                {

                    /* Set Relay Action that will be filled in LTR as RLY_FDB  
                     */
                    if (b1RlyFdbflag == ECFM_TRUE)
                    {
                        pRxLtm->u1RelayAction = ECFM_LTR_RLY_FDB;
                    }
                }
                else
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMipProcessLtm:"
                                   "Unique Egress Port not determined \r\n");
                    if (ECFM_LBLT_GET_USE_FDB_ONLY (pPduSmInfo->u1RxFlags))
                    {
                        if (EcfmLtmRxFwdLtmToPort
                            (pPduSmInfo, pb1NotEnqueLtr,
                             pRxLtm->u1RxLtmTtl) == ECFM_SUCCESS)
                        {

                            /* Forwad Ltm to egress port */
                            break;
                        }
                        return ECFM_FAILURE;
                    }
                    else
                    {

                        /* Get entry for specified VLAN ID and Source 
                         * MAC Address */
                        if (EcfmGetMipDbEntry
                            ((UINT2) pPduSmInfo->u4RxVlanIdIsId,
                             pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                             &u2EgressPort) == ECFM_SUCCESS)
                        {

                            /* Set Relay Action that will be filled in LTR as   
                             * RLY_MPDB   */
                            pRxLtm->u1RelayAction = ECFM_LTR_RLY_MPDB;
                        }
                        else
                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMipProcessLtm:"
                                           "Unique Egress Port not"
                                           "determined \r\n");
                            if (EcfmLtmRxFwdLtmToPort
                                (pPduSmInfo, pb1NotEnqueLtr,
                                 pRxLtm->u1RxLtmTtl) == ECFM_SUCCESS)
                            {

                                /* Forwad Ltm to egress port */
                                break;
                            }
                            return ECFM_FAILURE;
                        }
                    }            /*HW Only bit not set */
                }

                if (u2EgressPort != pPduSmInfo->pStackInfo->u2PortNum)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMipProcessLtm:"
                                   "Unique Egress Port is not that which the"
                                   " MHF resides \r\n");
                    return ECFM_FAILURE;
                }

                /* IEEE 802.1ag Section 20.3.2
                 * If the LTM TTL is 0 or 1, the LTM is not
                 * forwarded to the next hop, and if 0, no LTR 
                 * is generated
                 */
                if (pRxLtm->u1RxLtmTtl > 1)
                {
                    b1FwdLtm = ECFM_TRUE;
                }

                /* Same UP-MHF is present on the egress port */
                pRxLtm->pEgressStackInfo = pPduSmInfo->pStackInfo;
                break;
            }

            /* Set Relay Action to be sent in LTR as RLY_HIT */
            pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;

            /* Same UP-MHF is present on the egress port */
            pRxLtm->pEgressStackInfo = pPduSmInfo->pStackInfo;
            u2EgressPort = pPduSmInfo->pStackInfo->u2PortNum;
            break;
        default:
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmMipProcessLtm: Invalid MP Direction" "\r\n");
            return ECFM_FAILURE;
    }
    *pu2EgressPort = u2EgressPort;
    *pb1LtmFwd = b1FwdLtm;
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmMepProcessLtm 
 *
 * Description        : This routine is used to process the received LTM, when 
 *                      the receiving entity is MEP.
 *                      
 * Input(s)           :  pPudSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other  information.
 *
 * Output(s)          : pu2EgressPort - EgressPort Identified for transmitting 
 *                                      LTR or forwarding LTM .
 *                      pb1FwdLtm     - Flag value for forwarding the LTM if 
 *                                      required 
 *                      pb1NotEnqueLtr- Flag value to enque LTR if required 
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmMepProcessLtm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT2 *pu2EgressPort,
                   BOOL1 * pb1LtmFwd, BOOL1 * pb1NotEnqueLtr)
{
    tEcfmLbLtRxLtmPduInfo *pRxLtm = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    UINT2               u2EgressPort = ECFM_INIT_VAL;
    UINT1               u1PortState = ECFM_INIT_VAL;
    tEcfmMacAddr        MepMacAddr = {
        0
    };
    BOOL1               b1RlyFdbflag = ECFM_TRUE;
    BOOL1               b1FwdLtm = ECFM_FALSE;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get Received LTM info from the PduSmInfo */
    pRxLtm = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Get Mep Info from pdusm Info received */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    pRxLtm->pEgressStackInfo = NULL;
    switch (pMepInfo->u1Direction)

    {
        case ECFM_MP_DIR_DOWN:

            /* Store Index from where the LTR is to be transmitted */
            pRxLtm->u2LtrOutPortNum = (UINT4) (pMepInfo->u2PortNum);

            /* Store Direction where the LTR is to be transmitted */
            pRxLtm->u1LtrOutDirection = pMepInfo->u1Direction;

            /* Store Index where the LTM is Received */
            pRxLtm->u2LtmInPortNum = pPduSmInfo->pStackInfo->u2PortNum;

            /* Compares the Target address of LTM PDU with Meps Mac 
             * Address */
            ECFM_LBLT_GET_MAC_ADDR_OF_MEP (pMepInfo, MepMacAddr);
            if (ECFM_COMPARE_MAC_ADDR (pRxLtm->TargetMacAddr, MepMacAddr) !=
                ECFM_SUCCESS)

            {

                /* Get the Spanning tree state of bridge port and       
                 * Vid */
                u1PortState = EcfmL2IwfGetVlanPortState ((UINT2) pPduSmInfo->
                                                         u4RxVlanIdIsId,
                                                         pPduSmInfo->pPortInfo->
                                                         u4IfIndex);

                /* Check for the Port State is FORWARDING */
                if ((ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT
                     (pPduSmInfo->pStackInfo->u2PortNum)) &&
                    (u1PortState != AST_PORT_STATE_FORWARDING))

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMepProcessLtm:"
                                   "The spanning tree state of bridge port and"
                                   "vlan_identifier of LTM is not Forwarding"
                                   "\r\n");
                    return ECFM_FAILURE;
                }

                else

                {

                    /* Check unique Egress port if the UseFdbOnly Bit is set 
                     * then check Bridge  Filtering DB otherwise check
                     *  MIP CCM DB*/
                    if ((ECFM_LBLT_802_1AH_BRIDGE ()?
                         EcfmLtmAHRxSmGetEgressPort (pPduSmInfo,
                                                     &u2EgressPort) :
                         EcfmLtmRxSmGetEgressPort (pPduSmInfo,
                                                   &u2EgressPort,
                                                   &b1RlyFdbflag)) ==
                        ECFM_SUCCESS)
                    {

                        /* Set Relay Action that will be filled in LTR as 
                         * RLY_FDB */
                        if (b1RlyFdbflag == ECFM_TRUE)
                        {
                            pRxLtm->u1RelayAction = ECFM_LTR_RLY_FDB;
                        }
                    }

                    else

                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmMepProcessLtm:"
                                       "Unique Egress Port not determined "
                                       "through FDB"
                                       "Searching in MIP CCM DB \r\n");
                        if (ECFM_LBLT_GET_USE_FDB_ONLY (pPduSmInfo->u1RxFlags))

                        {
                            return ECFM_FAILURE;
                        }

                        else

                        {

                            /* Get entry for specified VLAN ID and Source 
                             * MAC Address */
                            if (EcfmGetMipDbEntry ((UINT2) pPduSmInfo->
                                                   u4RxVlanIdIsId,
                                                   pPduSmInfo->uPduInfo.Ltm.
                                                   TargetMacAddr,
                                                   &u2EgressPort) ==
                                ECFM_SUCCESS)
                            {

                                /* Set Relay Action that will be filled in LTR 
                                 * as RLY_MPDB   */
                                pRxLtm->u1RelayAction = ECFM_LTR_RLY_MPDB;
                            }

                            else

                            {
                                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                               ECFM_CONTROL_PLANE_TRC,
                                               "EcfmMepProcessLtm:"
                                               "Unique Egress Port not"
                                               "determined in MIP CCM DB \r\n");
                                return ECFM_FAILURE;
                            }
                        }
                    }
                    if (pMepInfo->u2PortNum == pRxLtm->u2LtmInPortNum)
                    {
                      ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmMepProcessLtm:"
                                     "Found Egress Port is not same as on which"
                                     "the Down MEP that initiated the LTM, is "
                                     "configured \r\n");
                      return ECFM_FAILURE;
                    }
                }
                break;
            }
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmMepProcessLtm: Target Address in LTM PDU"
                           "equal to MEPs MacAdd \r\n");

            /* Set Relay Action to be filled in LTR as RLY_HIT as Target 
             * MAC Address in LTM is that of the receiveing MEP */
            pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;
            break;
        case ECFM_MP_DIR_UP:

            /* Compares the Target address of LTM PDU with Meps Mac 
             * Address */
            /* if the LTM is received through LTI SAP */
            if (ECFM_LBLT_IS_LTI_SAP_IND_PRESENT (pPduSmInfo->pBuf))

            {

                /* Check unique Egress port if the UseFdbOnly Bit is set 
                 * then check Bridge  Filtering DB otherwise check
                 *  MIP CCM DB*/
                /* Store Index from where the LTR is to be transmitted */
                pRxLtm->u2LtrOutPortNum =
                    (UINT4) (pPduSmInfo->pStackInfo->u2PortNum);

                /* Store Direction where the LTR is to be transmitted */
                pRxLtm->u1LtrOutDirection = pPduSmInfo->pStackInfo->u1Direction;

                /* Store Index where the LTM is Received */
                if ((ECFM_LBLT_802_1AH_BRIDGE ()?
                     EcfmLtmAHRxSmGetEgressPort (pPduSmInfo, &u2EgressPort) :
                     EcfmLtmRxSmGetEgressPort (pPduSmInfo,
                                               &u2EgressPort,
                                               &b1RlyFdbflag)) == ECFM_SUCCESS)
                {

                    /* Set Relay Action that will be filled in LTR as RLY_FDB    
                     */
                    if (b1RlyFdbflag == ECFM_TRUE)
                    {
                        pRxLtm->u1RelayAction = ECFM_LTR_RLY_FDB;
                    }
                }

                else

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMepProcessLtm:"
                                   "Unique Egress Port not determined in FDB."
                                   " Searching in MIP CCM DB \r\n");
                    if (ECFM_LBLT_GET_USE_FDB_ONLY (pPduSmInfo->u1RxFlags))

                    {
                        if (EcfmLtmRxFwdLtmToFf (pPduSmInfo, pb1NotEnqueLtr)
                            == ECFM_SUCCESS)

                        {

                            /*LTM Fowarded to all egress ports */
                            break;
                        }
                        return ECFM_FAILURE;
                    }

                    else

                    {

                        /* Get entry for specified VLAN ID and Source  
                         * MAC Address */
                        if (EcfmGetMipDbEntry
                            (pPduSmInfo->u4RxVlanIdIsId,
                             pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                             &u2EgressPort) == ECFM_SUCCESS)
                        {

                            /* Set Relay Action that will be filled in LTR as 
                             * RLY_MPDB   */
                            pRxLtm->u1RelayAction = ECFM_LTR_RLY_MPDB;
                        }

                        else

                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMepProcessLtm:"
                                           "Unique Egress Port not"
                                           "determined in MIP CCM DB\r\n");
                            if (EcfmLtmRxFwdLtmToFf
                                (pPduSmInfo, pb1NotEnqueLtr) == ECFM_SUCCESS)

                            {

                                /*LTM Fowarded to all egress ports */
                                break;
                            }
                            return ECFM_FAILURE;
                        }
                    }            /*End of Check for USE_FDB_ONLY not set */
                }                /*End of Check for Unique Egress Port not found */

                /* This is function is used to get entry from Stack table */
                pStackInfo = EcfmLbLtUtilGetMp (u2EgressPort,
                                                pPduSmInfo->u1RxMdLevel,
                                                (UINT2) pPduSmInfo->
                                                u4RxVlanIdIsId,
                                                pPduSmInfo->u1RxDirection);
                if ((pStackInfo == NULL)
                    &&
                    (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)))

                {

                    /*LTM Fwded to all egress port
                     */
                    *pb1NotEnqueLtr = ECFM_TRUE;
                    if (ECFM_LBLT_802_1AH_BRIDGE ())
                    {
                        if (EcfmLbLtAHCtrlTxFwdToFf (pPduSmInfo) !=
                            ECFM_SUCCESS)

                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMepProcessLtm:"
                                           "EcfmLbLtAHCtrlTxFwdToFf returned "
                                           "failure\r\n");
                            return ECFM_FAILURE;
                        }
                    }
                    else
                    {
                        if (EcfmLbLtADCtrlTxFwdToFf (pPduSmInfo) !=
                            ECFM_SUCCESS)

                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMepProcessLtm:"
                                           "EcfmLbLtADCtrlTxFwdToFf returned "
                                           "failure\r\n");
                            return ECFM_FAILURE;
                        }
                    }
                    break;
                }

                /* Store Index from where the LTR is to be transmitted */
                pRxLtm->u2LtrOutPortNum =
                    (UINT4) (pPduSmInfo->pStackInfo->u2PortNum);

                /* Store Direction where the LTR is to be transmitted */
                pRxLtm->u1LtrOutDirection = pPduSmInfo->pStackInfo->u1Direction;

                /* If MP exists at same Level exists */
                if (pStackInfo != NULL)

                {
                    pRxLtm->pEgressStackInfo = pStackInfo;
                    if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
                    {
                        return ECFM_FAILURE;
                    }
                    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                               (pStackInfo->u2PortNum)->
                                               u4IfIndex, MepMacAddr);
                    if (ECFM_COMPARE_MAC_ADDR
                        (MepMacAddr, pRxLtm->TargetMacAddr) == ECFM_SUCCESS)

                    {

                        /* Set Relay Action that will be filled in LTR as 
                         * RLY_HIT as Egress Port Matches the Target MAC 
                         * Address of the LTM */
                        pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;
                    }
                    else
                    {
                        /* if Mip of same level exists */
                        if ((pStackInfo->pLbLtMipInfo != NULL) &&
                            (pRxLtm->u1RxLtmTtl > 1))
                        {
                            b1FwdLtm = ECFM_TRUE;
                        }
                    }
                }

                else

                {
                    UINT1               u1MdLevel = ECFM_INIT_VAL;

                    /* Get next higher level mp if any from the port belongs 
                       to same Vid and
                       * same direction */
                    for (u1MdLevel = (UINT1) (pPduSmInfo->u1RxMdLevel + 1);
                         u1MdLevel <= ECFM_MD_LEVEL_MAX;
                         u1MdLevel = u1MdLevel + 1)

                    {
                        pStackInfo = EcfmLbLtUtilGetMp (u2EgressPort, u1MdLevel,
                                                        (UINT2) pPduSmInfo->
                                                        u4RxVlanIdIsId,
                                                        ECFM_MP_DIR_UP);

                        /* If MP at Higer Level exists on Port */
                        if (pStackInfo != NULL)

                        {

                            /* LTM should be discarded and no further
                             * processin should be done */
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMepProcessLtm: Higher Level"
                                           "MEP/MHF is found on the Egresss "
                                           "Port.LTM is discarded \r\n");
                            return ECFM_FAILURE;
                        }        /*end of else - StackInfo is NULL */
                    }            /* end of for loop for Higher Md Level */
                    if (pPduSmInfo->uPduInfo.Ltm.u1RxLtmTtl > 1)

                    {
                        b1FwdLtm = ECFM_TRUE;
                    }
                }
                break;
            }
            else
            {

                /* Store Index from where the LTR is to be transmitted */
                pRxLtm->u2LtrOutPortNum = (UINT4) (pMepInfo->u2PortNum);

                /* Store Direction where the LTR is to be transmitted */
                pRxLtm->u1LtrOutDirection = pMepInfo->u1Direction;
            }
            /* Check if the Target MAC Address Matches the Receiving MEPs MAC
             * Address */
            ECFM_LBLT_GET_MAC_ADDR_OF_MEP (pMepInfo, MepMacAddr);
            if (ECFM_COMPARE_MAC_ADDR (pRxLtm->TargetMacAddr, MepMacAddr)
                != ECFM_SUCCESS)

            {

                /* Check unique Egress port if the UseFdbOnly Bit is set 
                 * then check Bridge  Filtering DB otherwise check
                 *  MIP CCM DB*/
                if ((ECFM_LBLT_802_1AH_BRIDGE ()?
                     EcfmLtmAHRxSmGetEgressPort (pPduSmInfo, &u2EgressPort) :
                     EcfmLtmRxSmGetEgressPort (pPduSmInfo,
                                               &u2EgressPort,
                                               &b1RlyFdbflag)) == ECFM_SUCCESS)
                {

                    /*Set Relay Action that will be filled in LTR as RLY_FDB */
                    if (b1RlyFdbflag == ECFM_TRUE)
                    {
                        pRxLtm->u1RelayAction = ECFM_LTR_RLY_FDB;
                    }
                }

                else

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMepProcessLtm:"
                                   "Unique Egress Port not determined in FDB"
                                   " Searching in MIP CCM DB \r\n");
                    if (ECFM_LBLT_GET_USE_FDB_ONLY (pPduSmInfo->u1RxFlags))

                    {
                        return ECFM_FAILURE;
                    }

                    else

                    {

                        /* Get entry for specified VLAN ID and Source 
                         * MAC Address in MIP CCM DB */
                        if (EcfmGetMipDbEntry
                            ((UINT2) pPduSmInfo->u4RxVlanIdIsId,
                             pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                             &u2EgressPort) == ECFM_SUCCESS)

                        {

                            /* Set Relay Action that will be filled in LTR as 
                             * RLY_MPDB   */
                            pRxLtm->u1RelayAction = ECFM_LTR_RLY_MPDB;
                        }

                        else

                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmMepProcessLtm:"
                                           "Unique Egress Port not"
                                           "determined in MIP CCM DB \r\n");
                            return ECFM_FAILURE;
                        }
                    }
                }
                if (pMepInfo->u2PortNum != u2EgressPort)

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMepProcessLtm:"
                                   "Found Egress Port is not same as on which "
                                   "the Up MEP that initiated the LTM, is "
                                   "configured \r\n");
                    return ECFM_FAILURE;
                }
                /* Same UP-MEP is present on the egress port */
                pRxLtm->pEgressStackInfo = pPduSmInfo->pStackInfo;
                break;
            }
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmMepProcessLtm: Target Address in LTM PDU"
                           "equal to MEPs MacAdd \r\n");

            /* Set Relay Action to be filled in LTR as RLY_HIT as Target 
             * MAC Address in LTM is that of the receiveing MEP */
            pRxLtm->u1RelayAction = ECFM_LTR_RLY_HIT;
            u2EgressPort = pPduSmInfo->pStackInfo->u2PortNum;

            /* Same UP-MEP is present on the egress port */
            pRxLtm->pEgressStackInfo = pPduSmInfo->pStackInfo;
            break;
        default:
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmMepProcessLtm: Invalid MP Direction \r\n");
            return ECFM_FAILURE;
    }
    *pu2EgressPort = u2EgressPort;
    *pb1LtmFwd = b1FwdLtm;
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLtmRxSmProcessLtm 
 *
 * Description        : This routine is used to process the received LTM, and    
 *                      Enqueues an LTR for the same. If LTM is received by an   
 *                      MHF and if required LTM is forwarded.
 *                      
 * Input(s)           :  pPudSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other  information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / :ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLtmRxSmProcessLtm (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtRxLtmPduInfo *pRxLtm = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2EgressPort = ECFM_INIT_VAL;
    tEcfmMacAddr        MacAddr = {
        0
    };
    BOOL1               b1FwdLtm = ECFM_FALSE;
    BOOL1               b1NotEnqueLtr = ECFM_FALSE;
    UINT1               u1PortState = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    /* Get Received LTM info from the PduSmInfo */
    pRxLtm = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Get Mep Info from pdusm Info received */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Validate the received LTM */
    if (EcfmLtmRxSmValidateLtm (pPduSmInfo) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmProcessLtm: Could not validate Received LTM"
                       "\r\n");
        return ECFM_FAILURE;
    }

    /* Check the LTM TTL value is ZERO, then the received LTM should be        
     * discarded */
    if (pRxLtm->u1RxLtmTtl == 0)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmProcessLtm: "
                       "LTM PDU is received with TTL value ZERO. Thus LTM "
                       "received is invalid \r\n");
        return ECFM_FAILURE;
    }

    /* Checking the Destination Address in the received LTM PDU is appropriate 
     * to the receiveing MEPs MD Level */
    if (ECFM_IS_MULTICAST_CLASS2_ADDR
        (pPduSmInfo->RxDestMacAddr, pPduSmInfo->u1RxMdLevel) != ECFM_TRUE)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLtmRxSmProcessLtm: "
                       " Destination Multicast Address of the PDU"
                       "is not approprate to MD Level of received LTM ");

        /* Check LTM received by LinkTrace SAP and destination address is 
         * the Individual MAC Address of the MEP then LTM received is     
         * valid */
        if (ECFM_LBLT_IS_LTI_SAP_IND_PRESENT (pPduSmInfo->pBuf))

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtmRxSmProcessLtm: Received LTM is not received"
                           "through LinkTrace SAP \r\n");
            return ECFM_FAILURE;
        }

        /* If the destination address parameter of the LTM is individual  
         * MAC Address of the receiving MEP, received LTM is valid */
        if (pMepInfo != NULL)

        {
            ECFM_LBLT_GET_MAC_ADDR_OF_MEP (pMepInfo, MacAddr);
            if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MacAddr) !=
                ECFM_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmProcessLtm:"
                               "Destination Address not equal to MEPs MacAdd "
                               "\r\n");
                return ECFM_FAILURE;
            }
        }

        else

        {
            if (ECFM_LBLT_GET_PORT_INFO (pPduSmInfo->pStackInfo->u2PortNum) ==
                NULL)
            {
                return ECFM_FAILURE;
            }
            ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                       (pPduSmInfo->pStackInfo->u2PortNum)->
                                       u4IfIndex, MacAddr);
            if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MacAddr) !=
                ECFM_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmProcessLtm:"
                               "Destination Address not equal to MHFs MacAdd "
                               "\r\n");
                return ECFM_FAILURE;
            }
        }
    }

    /* Check whether on PDU reception MP found was MEP or MIP and based on 
     * that process received the LTM */
    if (pMepInfo != NULL)

    {
        if (EcfmMepProcessLtm
            (pPduSmInfo, &u2EgressPort, &b1FwdLtm,
             &b1NotEnqueLtr) != ECFM_SUCCESS)

        {
            return ECFM_FAILURE;
        }
    }

    else

    {
        if (EcfmMipProcessLtm
            (pPduSmInfo, &u2EgressPort, &b1FwdLtm,
             &b1NotEnqueLtr) != ECFM_SUCCESS)

        {
            return ECFM_FAILURE;
        }
    }

    /* When Y.1731 is enabled and If Address Learning of the N/W 
     * Element is disabled then MIP on this NE will not transmit LTR, 
     * So this condition can be checked
     * and b1NotEnqueLtr can be set to ECFM_TRUE*/
    if (pRxLtm->u2LtmInPortNum != ECFM_INIT_VAL)
    {
        /* Routine to get Port Info of ingress port */
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (pRxLtm->u2LtmInPortNum);

        /*Check the ingress port memebership of Vlan */
        if ((pPortInfo != NULL)
            && (!ECFM_IS_MEP_ISID_AWARE (pPduSmInfo->u4RxVlanIdIsId)))
        {
            if (EcfmL2IwfMiIsVlanMemberPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                             (UINT2) pPduSmInfo->u4RxVlanIdIsId,
                                             pPortInfo->u4IfIndex) != OSIX_TRUE)
            {
                b1FwdLtm = ECFM_FALSE;
                pPortInfo = NULL;
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmProcessLtm:"
                               "EcfmL2IwfMiIsVlanMemberPort returned failure\r\n");
            }
        }
    }
    /* Routine to get Port Info of egress port */
    if (u2EgressPort != ECFM_INIT_VAL)

    {
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2EgressPort);

        /*Check the egress port memebership of Vlan */
        if (pPortInfo != NULL)
        {
            if (!ECFM_IS_MEP_ISID_AWARE (pPduSmInfo->u4RxVlanIdIsId))
            {
                if (EcfmL2IwfMiIsVlanMemberPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                                 (UINT2) pPduSmInfo->
                                                 u4RxVlanIdIsId,
                                                 pPortInfo->u4IfIndex) !=
                    OSIX_TRUE)
                {
                    b1FwdLtm = ECFM_FALSE;
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLtmRxSmProcessLtm:"
                                   "EcfmL2IwfMiIsVlanMemberPort returned failure\r\n");
                }
            }
            /* Check if Spanning Tree is enabled in this Context */
            if (EcfmIsStpEnabledInContext
                (ECFM_LBLT_CURR_CONTEXT_ID () != ECFM_SUCCESS))
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmProcessLtm:"
                               "STP is disabled in this Context\r\n");

                u1PortState = AST_PORT_STATE_FORWARDING;
            }
            else
            {
                /* Get the Spanning tree state of bridge port and       
                 * Vid */
                u1PortState =
                    EcfmL2IwfGetVlanPortState ((UINT2) pPduSmInfo->
                                               u4RxVlanIdIsId,
                                               pPortInfo->u4IfIndex);
            }
            /* Check for the Port State is FORWARDING */
            if (u1PortState != AST_PORT_STATE_FORWARDING)

            {
                b1FwdLtm = ECFM_FALSE;
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmProcessLtm:"
                               "The spanning tree state of bridge port and"
                               "vlan_identifier of LTM is not Forwarding"
                               "\r\n");
            }
        }
    }
    if (b1NotEnqueLtr != ECFM_TRUE)

    {

        /* Enque LTR for the valid LTM received */
        if (EcfmLtmRxSmEnqueLtr (pPduSmInfo, u2EgressPort, b1FwdLtm) !=
            ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtmRxSmProcessLtm: LTR cannot be enqueued \r\n");
            i4RetVal = ECFM_FAILURE;
        }
    }
    if (b1FwdLtm)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmProcessLtm: Received LTM needs is Forwarded"
                       " \r\n");
        EcfmLtmRxSmForwardLtm (pPduSmInfo, u2EgressPort);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Function           : EcfmLtmRxSmEnqueLtr 
 *
 * Description        : This routine is used to format and enqueue the LTR for  
 *                      the received LTM.
 *
 * Input(s)           :  pPudSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other  information.
 *                       u2EgressPort - Egress Port found
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS /ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmLtmRxSmEnqueLtr (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT2 u2EgressPort,
                     BOOL1 b1FwdLtm)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmLbLtRxLtmPduInfo *pRxLtmInfo = NULL;
    tEcfmLbLtDelayQueueNode DelayQueueNode;
    UINT2               u2PduLen = ECFM_INIT_VAL;
    UINT2               u2TypeLength = ECFM_INIT_VAL;
    UINT2               u2EtherType = ECFM_INIT_VAL;
    UINT1               u1Flag = ECFM_INIT_VAL;
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT1              *pu1EthLlcHdr = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocates the CRU Buffer for formatting LTR */
    pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_PDU_SIZE, ECFM_INIT_VAL);
    if (pBuf == NULL)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmEnqueLtr:"
                       "CRU Buffer Allocation failed \r\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Get received LTM information from received PduSmInfo */
    pRxLtmInfo = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);
    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
    pu1PduStart = ECFM_LBLT_PDU;
    pu1PduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;
    ECFM_MEMSET (&DelayQueueNode, ECFM_INIT_VAL,
                 sizeof (tEcfmLbLtDelayQueueNode));
    ECFM_MEMCPY (&(DelayQueueNode.VlanClassificationInfo),
                 &(pPduSmInfo->VlanClassificationInfo), sizeof (tEcfmVlanTag));
    ECFM_MEMCPY (&(DelayQueueNode.PbbClassificationInfo),
                 &(pPduSmInfo->PbbClassificationInfo), sizeof (tEcfmPbbTag));

    if (pPduSmInfo->pStackInfo->u4VlanIdIsid <= ECFM_INTERNAL_ISID_MIN)
    {
        if (ECFM_LBLT_802_1AH_BRIDGE () != ECFM_TRUE)
        {
            DelayQueueNode.VlanClassificationInfo.OuterVlanTag.u2VlanId =
                (UINT2) pPduSmInfo->pStackInfo->u4VlanIdIsid;
            pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
                (UINT2) pPduSmInfo->pStackInfo->u4VlanIdIsid;
        }
        else
        {
            DelayQueueNode.PbbClassificationInfo.OuterVlanTag.u2VlanId =
                (UINT2) pPduSmInfo->pStackInfo->u4VlanIdIsid;
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId =
                (UINT2) pPduSmInfo->pStackInfo->u4VlanIdIsid;
        }
    }

    DelayQueueNode.u1Direction = pRxLtmInfo->u1LtrOutDirection;
    DelayQueueNode.u4RxVlanIdIsId = pPduSmInfo->u4RxVlanIdIsId;

    /* Fill the CFM PDU Header */
    /* Set MD Level and Version in the first byte of CFM PDU Header */
    ECFM_SET_MDLEVEL (u1MdLvlAndVer, pPduSmInfo->pStackInfo->u1MdLevel);
    ECFM_SET_VERSION (u1MdLvlAndVer);
    ECFM_PUT_1BYTE (pu1PduEnd, u1MdLvlAndVer);

    /* Set in the next 1 byte OpCode */
    ECFM_PUT_1BYTE (pu1PduEnd, ECFM_OPCODE_LTR);

    /* Fill 1 byte of Flag field */
    /* Copy the flag values received in LTM as it is */
    u1Flag = pPduSmInfo->u1RxFlags;

    /* Copy the FDB bit from received LTM Flags */
    if (ECFM_LBLT_GET_USE_FDB_ONLY (pPduSmInfo->u1RxFlags))

    {
        ECFM_LBLT_SET_USE_FDB_ONLY (u1Flag);
    }

    /*Y.1731: When is disabled then FwdYes and Terminal Bits are 
     * present in the Flags 
     * FwdYes bit      - not set as MEP cannot forward the LTM 
     * TerminalMep Bit - Set as the replying MP is a MEP */
    if (ECFM_LBLT_IS_MEP (pPduSmInfo->pStackInfo))

    {
        pStackInfo = pPduSmInfo->pStackInfo;
        pMepInfo = pStackInfo->pLbLtMepInfo;

        if (ECFM_LBLT_IS_MEP_ACTIVE (pMepInfo) == ECFM_TRUE)
        {
            if (!ECFM_LBLT_IS_LTI_SAP_IND_PRESENT (pPduSmInfo->pBuf))
            {
                ECFM_LBLT_SET_TERM_MEP (u1Flag);
            }
            else
            {
                if ((pRxLtmInfo->pEgressStackInfo != NULL) &&
                    (ECFM_LBLT_IS_MEP (pRxLtmInfo->pEgressStackInfo)))
                {
                    ECFM_LBLT_SET_TERM_MEP (u1Flag);
                }
            }
        }
    }
    if (b1FwdLtm == ECFM_TRUE)

    {
        ECFM_LBLT_SET_FWD_YES (u1Flag);
    }
    ECFM_PUT_1BYTE (pu1PduEnd, u1Flag);

    /* Fill 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1PduEnd, ECFM_LTR_FIRST_TLV_OFFSET);

    /* Fill the other fields of LTR PDU */
    EcfmLtmRxSmPutLtrInfo (pRxLtmInfo, &pu1PduEnd);

    /* Fill the LTR TLVs PDU */
    EcfmLtmRxSmPutLtrTlv (pPduSmInfo, u2EgressPort, &pu1PduEnd, b1FwdLtm);

    /* Calculate the length of the formatted PDU */
    u2PduLen = (UINT2) (pu1PduEnd - pu1PduStart);

    /* IEEE 802.1ag/D8.1, section 20.42.4, NOTE 2 and p802.1Q-REV/D1.5, 
     * section 20.47.4, NOTE 2
     * The resultant LTR can be larger than the maximum frame size for
     * the media-dependent sublayer on which the
     * LTR is to be transmitted, causing the LTR to be discarded.
     */
    if (u2PduLen > ECFM_MAX_LTR_PDU_SIZE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmEnqueLtr: PDU Length exceeds max frame size \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, FALSE);
        return ECFM_FAILURE;
    }

    /* copying the PDU in CRU Buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLen)) != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmEnqueLtr: Copying into Buffer failed \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, FALSE);
        return ECFM_FAILURE;
    }

    /* Copy the Ethernet Header from LTM to LTR, as we also need the same VLAN
     * information in LTR also
     */
    ECFM_COPY_FROM_CRU_BUF (pPduSmInfo->pBuf, pu1EthLlcHdr, ECFM_INIT_VAL,
                            pPduSmInfo->u1CfmPduOffset);

    /* Copy 6byte Destination address as Original Address
     * Received in the LTM */
    ECFM_MEMCPY (pu1EthLlcHdr, pRxLtmInfo->OriginalMacAddr,
                 ECFM_MAC_ADDR_LENGTH);
    pu1EthLlcHdr = pu1EthLlcHdr + ECFM_MAC_ADDR_LENGTH;

    /* Copy next 6byte Source address as Mac address of the Replying
     * MEP */
    if (ECFM_LBLT_GET_PORT_INFO (pRxLtmInfo->u2LtrOutPortNum) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "EcfmLtmRxSmEnqueLtr:No port info available " "\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pRxLtmInfo->u2LtrOutPortNum)->u4IfIndex,
                               pu1EthLlcHdr);

    /* Move the offset by 6 bytes and also increment its length */
    pu1EthLlcHdr = pu1EthLlcHdr + ECFM_MAC_ADDR_LENGTH;

    /* Check if Outer Vlan Tag is Present */
    if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)

    {
        pu1EthLlcHdr = pu1EthLlcHdr + ECFM_VLAN_HDR_SIZE;
    }

    /* Check if Inner Vlan Tag is Present */
    if (pPduSmInfo->VlanClassificationInfo.InnerVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        pu1EthLlcHdr = pu1EthLlcHdr + ECFM_VLAN_HDR_SIZE;
    }
    if (ECFM_LBLT_802_1AH_BRIDGE ())
    {
        if (pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            pu1EthLlcHdr = pu1EthLlcHdr + ECFM_VLAN_HDR_SIZE;
        }
        /* Check if Inner Vlan Tag is Present */
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            pu1EthLlcHdr = pu1EthLlcHdr + ECFM_ISID_HDR_SIZE;
        }
    }

    ECFM_GET_2BYTE (u2EtherType, pu1EthLlcHdr);
    if (u2EtherType == ECFM_VLAN_CUSTOMER_PROTOCOL_ID
        || u2EtherType == ECFM_VLAN_PROVIDER_PROTOCOL_ID)
    {
        pu1EthLlcHdr = pu1EthLlcHdr + ECFM_VLAN_HDR_SIZE;
    }

    /* Get TypeLength of CFM-PDU */
    ECFM_GET_2BYTE (u2TypeLength, pu1EthLlcHdr);
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)

    {
        pu1EthLlcHdr = pu1EthLlcHdr - ECFM_LLC_TYPE_LEN_FIELD_SIZE;

        /* Set LTR Pdu Length in LLC SNAP Header */
        ECFM_PUT_2BYTE (pu1EthLlcHdr, (u2PduLen + ECFM_LLC_SNAP_HDR_SIZE));
    }
    u2PduLen = pPduSmInfo->u1CfmPduOffset;
    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart, (UINT4) (u2PduLen))
        != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "EcfmLtmRxSmEnqueLtr:Prepending into Buffer failed "
                       "\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Check if the LTI Bit is set in the received LTM, set it in the Enqued LTR
     * also */
    if (ECFM_LBLT_IS_LTI_SAP_IND_PRESENT (pPduSmInfo->pBuf))
    {
        ECFM_LBLT_SET_LTI_SAP_INDICATION (pBuf);

        /* Set the IfIndex same as the receiving MEP as this will be the UP MEP
         * which initated the LTM
         */
        DelayQueueNode.u2PortNum = pPduSmInfo->pPortInfo->u2PortNum;
    }

    else

    {
        ECFM_LBLT_RESET_LTI_SAP_INDICATION (pBuf);
        DelayQueueNode.u2PortNum = (UINT2) (pRxLtmInfo->u2LtrOutPortNum);
    }

    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pRxLtmInfo->u2LtrOutPortNum))
    {
        /* Add LTR to Delay Queue for transmission after a random interval of 
         * time in case of 802.1ag. 
         */
        DelayQueueNode.u1OpCode = ECFM_OPCODE_LTR;
        DelayQueueNode.pBuf = pBuf;
        EcfmLbLtAddCfmPduToDelayQueue (&DelayQueueNode);
    }
    else
    {
        if (EcfmLbLtCtrlTxTransmitPkt (pBuf,
                                       pRxLtmInfo->u2LtrOutPortNum,
                                       pPduSmInfo->u4RxVlanIdIsId, 0, 0,
                                       pRxLtmInfo->u1LtrOutDirection,
                                       ECFM_OPCODE_LTR,
                                       &(pPduSmInfo->VlanClassificationInfo),
                                       &(pPduSmInfo->PbbClassificationInfo)) !=
            ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC |
                           ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlTxTransmitPkt:"
                           "LTR Transmission Failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLtmRxSmValidateLtm
 *
 * Description        : This routine is used to validate received LTM PDU.
 *
 * Input(s)           :  pPudSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other  information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / :ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmLtmRxSmValidateLtm (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtRxLtmPduInfo *pRxLtmInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get the received LTM information from PduSmInfo */
    pRxLtmInfo = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Checking MSB bit in Original MacAddr in received LTM for Group Address 
     */
    if (ECFM_IS_MULTICAST_ADDR (pRxLtmInfo->OriginalMacAddr) == ECFM_TRUE)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmValidateLtm:"
                       "Original Mac address is a group address. Thus received "
                       "LTM PDU is invalid \r\n");
        return ECFM_FAILURE;
    }

    /* Checking Target MacAddress received in LTM is a Group Address */
    if (ECFM_IS_MULTICAST_ADDR (pRxLtmInfo->TargetMacAddr) == ECFM_TRUE)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmValidateLtm:"
                       "Target Mac address is group address. Thus received "
                       "LTM PDU is invalid \r\n ");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtClntParseLtm 
 *
 * DESCRIPTION      : This is the LTM Parser which extracts the TLVs and other  
 *                    fields from the LTM and places then into the              
 *                    tEcfmLbLtPduSmInfo Structure.
 *
 * Input(s)         : pPduSmInfo - Pointer to the structure that stores the 
 *                                 information regarding MP info, PDU rcvd 
 *                                 and other information.
 *                    pu1Pdu     - Pointer to the received PDU
 *
 * OUTPUT           : None.
 *
 * RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtClntParseLtm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRxLtmPduInfo *pLtmInfo = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    UINT2               u2TlvLength = ECFM_INIT_VAL;
    UINT1               u1TlvType = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get Ltm Pdu Information from PduSmInfo */
    pLtmInfo = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Validate the Value of FirstTLVOffset in LTM PDU. If the received LTM PDU 
     * is invalid, received LTM should be discarded */
    if (pPduSmInfo->u1RxFirstTlvOffset < ECFM_LTM_FIRST_TLV_OFFSET)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtClntParseLtm: FirstTLVOffset in LTM PDU less "
                       "than Required value of FirstTLVOffset for LTM. "
                       "Thus received LTM PDU is invalid \r\n");
        return ECFM_FAILURE;
    }

    /* Move pointer 4 bytes (CFM Header) ahead to read CFM PDU Fields */
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Read the LTM Transaction Identifier */
    ECFM_GET_4BYTE (pLtmInfo->u4TransId, pu1Pdu);

    /* Read LTM TTL from the received LTM */
    ECFM_GET_1BYTE (pLtmInfo->u1RxLtmTtl, pu1Pdu);

    /* Original MAC Address from the received LTM */
    ECFM_MEMCPY (pLtmInfo->OriginalMacAddr, pu1Pdu, ECFM_MAC_ADDR_LENGTH);

    /* Move the */
    pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

    /* Read Target MAC Addres from the received LTM */
    ECFM_MEMCPY (pLtmInfo->TargetMacAddr, pu1Pdu, ECFM_MAC_ADDR_LENGTH);

    /* Move the pointer forward with MAC Address Length */
    pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

    /* Read the received LTM till End TLV, TLVs that can be received are: 
     * 1. LTM Egress Identifier TLV, 
     * 2. Sender Id TLV
     * 3. Organizational Specific TLV 
     * 4. End TLV */

    /* Get the TLV Type from the rebuffer */
    ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    while (u1TlvType != ECFM_END_TLV_TYPE)

    {
        switch (u1TlvType)

        {
            case ECFM_LTM_EGRESS_ID_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtm:"
                               "Received Ltm Egress Id TLV \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                if (u2TlvLength != ECFM_EGRESS_ID_LENGTH)

                {
                    return ECFM_FAILURE;
                }

                /* Read Egress Identifier */
                ECFM_MEMCPY (pLtmInfo->au1EgressId, pu1Pdu,
                             ECFM_EGRESS_ID_LENGTH);

                /* Move Pointer to PDU by ECFM_EGRESS_ID_LENGTH for Next Egress 
                   Id */
                pu1Pdu = pu1Pdu + ECFM_EGRESS_ID_LENGTH;
                break;
            case ECFM_SENDER_ID_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtm:"
                               "Received Sender TLV \r\n");
                pSenderId = &(pLtmInfo->SenderId);

                /* Get Length in the Length field of the SenderId TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Read 1 byte Chasis ID Length from Sender ID Tlv */
                ECFM_GET_1BYTE (pSenderId->ChassisId.u4OctLen, pu1Pdu);
                u2TlvLength = u2TlvLength -
                    (UINT2) ECFM_CHASSIS_ID_LENGTH_FIELD_SIZE;
                if (pSenderId->ChassisId.u4OctLen != 0)

                {

                    /* Read 1 byte Chasis Id Subtype */
                    ECFM_GET_1BYTE (pSenderId->u1ChassisIdSubType, pu1Pdu);
                    u2TlvLength = u2TlvLength -
                        (UINT2) ECFM_CHASSIS_ID_LENGTH_FIELD_SIZE;

                    /* Extract Chassis ID */
                    pSenderId->ChassisId.pu1Octets = pu1Pdu;

                    /* Move the pointer for the Chassis ID Length */
                    pu1Pdu = pu1Pdu + pSenderId->ChassisId.u4OctLen;
                    u2TlvLength = u2TlvLength -
                        (UINT2) pSenderId->ChassisId.u4OctLen;
                }

                /* Check if the remaining Length in the TLV field is nonzero 
                 * If it is non-zero then Management Address Domain field 
                 * exists */
                if (u2TlvLength != 0)

                {

                    /* Read 1 byte Management Address Domain Length */
                    ECFM_GET_1BYTE (pSenderId->MgmtAddressDomain.u4OctLen,
                                    pu1Pdu);
                    u2TlvLength =
                        u2TlvLength -
                        (UINT2) ECFM_MGMT_ADDR_DOMAIN_LENGTH_FIELD_SIZE;

                    /* Read the Management Address Domain from Sender ID TLV */
                    pSenderId->MgmtAddressDomain.pu1Octets = pu1Pdu;

                    /* Move the pointer by Management Address Domain length */
                    pu1Pdu = pu1Pdu + pSenderId->MgmtAddressDomain.u4OctLen;
                    u2TlvLength = u2TlvLength - (UINT2)
                        (pSenderId->MgmtAddressDomain.u4OctLen);

                    /* Check if the Management Address Field exists, if the 
                     * remaining length from the SenderId Length is nonzero */
                    if (u2TlvLength != 0)

                    {

                        /* Read 1 byte Management Address Length */
                        ECFM_GET_1BYTE (pSenderId->MgmtAddress.u4OctLen,
                                        pu1Pdu);
                        u2TlvLength =
                            u2TlvLength -
                            (UINT2) ECFM_MGMT_ADDR_LENGTH_FIELD_SIZE;

                        /* Read the Management Address from the Sender ID TLV */
                        pSenderId->MgmtAddress.pu1Octets = pu1Pdu;

                        /* Move the pointer by Management Address lenght */
                        pu1Pdu =
                            pu1Pdu + (UINT1) pSenderId->MgmtAddress.u4OctLen;
                    }
                }
                break;
            case ECFM_ORG_SPEC_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtm:"
                               "Received Organization Specific TLV \r\n");

                /* Get 2 byte lenght field from the TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Read OUI from the TLV */
                ECFM_MEMSET (pLtmInfo->OrgSpecific.au1Oui, ECFM_INIT_VAL,
                             ECFM_OUI_FIELD_SIZE);
                ECFM_MEMCPY (pLtmInfo->OrgSpecific.au1Oui, pu1Pdu,
                             ECFM_OUI_LENGTH);

                /* Move the pointer by OUI Length */
                pu1Pdu = pu1Pdu + ECFM_OUI_LENGTH;

                /* Read 1 byte Sub type from the TLV */
                ECFM_GET_1BYTE (pLtmInfo->OrgSpecific.u1SubType, pu1Pdu);

                /* Lenght for the Value field in the Org Specific TLV is 
                   calculated */
                u2TlvLength = u2TlvLength - (UINT2) (ECFM_OUI_FIELD_SIZE + 1);

                /* Store the Length of the Value field in SenderID TLV */
                pLtmInfo->OrgSpecific.Value.u4OctLen = (UINT4) (u2TlvLength);
                if (pLtmInfo->OrgSpecific.Value.u4OctLen != 0)

                {

                    /* Read the value from the OrgSpecific TLV */
                    pLtmInfo->OrgSpecific.Value.pu1Octets = pu1Pdu;

                    /* Move the PDU with u2TlvLength */
                    pu1Pdu = pu1Pdu + (UINT1) u2TlvLength;
                }
                break;
            default:
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtm: TLV NOT SUPPORTED \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Check if the TLV Length is present or not */
                if (u2TlvLength != 0)

                {
                    pu1Pdu = pu1Pdu + u2TlvLength;
                }

                else

                {
                    pu1Pdu = pu1Pdu + ECFM_TLV_LENGTH_FIELD_SIZE;
                }
        }                        /* End of Switch */
        ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    }                            /* End of While */
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmLtmRxSmPutLtrInfo 
 *
 * Description        : This rotuine is used to fill the LTR fields i.e. LTR     
 *                      Transaction Id, Reply TTL, Relay Action
 *
 * Input(s)           : pRxLtmInfo - pointer to strucuter containing LTM Pdu 
 *                      info 
 *                      ppu1PduEnd  - Pointer where Information needs to be 
 *                      filled
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmLtmRxSmPutLtrInfo (tEcfmLbLtRxLtmPduInfo * pRxLtmInfo, UINT1 **ppu1PduEnd)
{
    UINT1              *pu1Pdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *(ppu1PduEnd);

    /* Fill the 4byte as LTR Transaction Identifier, same as the LTM           
     * Identifier received in the LTM PDU*/
    ECFM_PUT_4BYTE (pu1Pdu, pRxLtmInfo->u4TransId);

    /* Fill next 1 byte as Reply TTL, one less than Received in LTM PDU */
    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pRxLtmInfo->u1RxLtmTtl - 1));

    /* Copy next 1 byte Relay Action based on received LTM */
    ECFM_PUT_1BYTE (pu1Pdu, pRxLtmInfo->u1RelayAction);
    *(ppu1PduEnd) = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmLtmRxSmPutLtrTlv
 *
 * Description        : This rotuine is used to fill the LTR TLVs in the PDU
 *
 *
 * Input(s)           : ppu1PduEnd   - pointer to the Pdu
 *                      pPduSmInfo   - Pointer  the PduSm structure
 *                      u2EgressPort - Egress Port found 
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmLtmRxSmPutLtrTlv (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT2 u2EgressPort,
                      UINT1 **ppu1PduEnd, BOOL1 b1FwdLtm)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    tEcfmLbLtRxLtmPduInfo *pRxLtmInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    UINT1              *pu1ReplyTlvLen = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1Action = ECFM_INIT_VAL;
    BOOL1               b1Ingress = ECFM_FALSE;
    BOOL1               b1MepMipActiveStatus;
    UINT1               u1PortIdLen = 0;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *ppu1PduEnd;

    /* Get receivced LTM Pdu information from pdusm Info */
    pRxLtmInfo = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Get MEP info from received pduSm Info */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get LT Info maintained per MEP */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pPduSmInfo->pStackInfo->u2PortNum))
    {
        /* Format LTR Egress Identifier TLVs */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_LTR_EGRESS_ID_TLV_TYPE);

        /* Fill LTR Egress Identifier TLV Length */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_LTR_EGRESS_ID_VALUE_SIZE);

        /* Fill LTR Egress Identifier TLV Value 
         * Last Egress Identifier same as Egress Identifier received in LTM */
        ECFM_MEMCPY (pu1Pdu, pRxLtmInfo->au1EgressId, ECFM_EGRESS_ID_LENGTH);
        pu1Pdu = pu1Pdu + ECFM_EGRESS_ID_LENGTH;

        /* Next Egress Identifier in LTR Egress Identifier TLV to be filled 
         * only if the LTM is not to be forwarded */
        if (b1FwdLtm != ECFM_TRUE)
        {
            if (ECFM_LBLT_IS_MEP (pPduSmInfo->pStackInfo))
            {
                UINT1               au1NullEgressId[ECFM_EGRESS_ID_LENGTH];
                ECFM_MEMSET (au1NullEgressId, ECFM_INIT_VAL,
                             ECFM_EGRESS_ID_LENGTH);
                if (ECFM_MEMCMP
                    (pLtInfo->au1TxLtmEgressId, au1NullEgressId,
                     ECFM_EGRESS_ID_LENGTH) == 0)
                {
                    if (ECFM_LBLT_GET_PORT_INFO
                        (pPduSmInfo->pStackInfo->u2PortNum) == NULL)
                    {
                        return;
                    }
                    ECFM_PUT_2BYTE (pu1Pdu,
                                    ECFM_LBLT_PORT_INFO (pPduSmInfo->
                                                         pStackInfo->
                                                         u2PortNum)->u4IfIndex);
                    EcfmCfaGetContextMacAddr (ECFM_LBLT_CURR_CONTEXT_ID (),
                                              pu1Pdu);
                    pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;
                }
                else
                {

                    /* Next Egress Identifier in LTR Egress Identifier TLV */
                    ECFM_MEMCPY (pu1Pdu, pLtInfo->au1TxLtmEgressId,
                                 ECFM_EGRESS_ID_LENGTH);
                    pu1Pdu = pu1Pdu + ECFM_EGRESS_ID_LENGTH;
                }
            }
            else
            {
                UINT1               au1EgressId[ECFM_EGRESS_ID_LENGTH];
                ECFM_MEMSET (au1EgressId, ECFM_INIT_VAL, ECFM_EGRESS_ID_LENGTH);
                ECFM_SET_LTM_EGRESS_ID (pPduSmInfo->pStackInfo, au1EgressId);
                ECFM_MEMCPY (pu1Pdu, au1EgressId, ECFM_EGRESS_ID_LENGTH);
                pu1Pdu = pu1Pdu + ECFM_EGRESS_ID_LENGTH;
            }
        }
        else
        {
            UINT1               au1EgressId[ECFM_EGRESS_ID_LENGTH];
            ECFM_MEMSET (au1EgressId, ECFM_INIT_VAL, ECFM_EGRESS_ID_LENGTH);

            ECFM_SET_LTM_EGRESS_ID (pPduSmInfo->pStackInfo, au1EgressId);
            ECFM_MEMCPY (pu1Pdu, au1EgressId, ECFM_EGRESS_ID_LENGTH);
            pu1Pdu = pu1Pdu + ECFM_EGRESS_ID_LENGTH;
        }
    }

    if (ECFM_LBLT_IS_MEP (pPduSmInfo->pStackInfo))
    {
        pMepNode = pPduSmInfo->pStackInfo->pLbLtMepInfo;
        b1MepMipActiveStatus = ECFM_TRUE;
    }
    else
    {
        pMipNode = pPduSmInfo->pStackInfo->pLbLtMipInfo;
        b1MepMipActiveStatus = ECFM_FALSE;

    }

    /* This condition should be for both MEP and MHF and is to be updated with 
     * the MHF implementation in further release */
    /* If the LTM was not received by a Down MEP or Down MHF, does not place a
     * Reply Ingress TLV in  the LTR; otherwise:
     * 1) Fills the Ingress Action field of a Reply Ingress TLV (21.9.8) with
     *    the appropriate value according to Table 21-30 on page 194;
     * 2) Places the receiving MP.s MAC address in the Ingress MAC Address field
     *    of the Reply IngressTLV; and
     * 3) Optionally, fills the remainder of the Reply Ingress TLV with the
     *    receiving MP.s Port ID information;
     */
    if (pRxLtmInfo->u2LtmInPortNum != ECFM_INIT_VAL)
    {
        /* Check for the Ingress Action field */
        b1Ingress = ECFM_TRUE;

        /* Set IngressAction */
        if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
        {
            u1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_OK;
            if ((pTempLbLtPortInfo =
                 ECFM_LBLT_GET_PORT_INFO (pRxLtmInfo->u2LtmInPortNum)) == NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmPutLtrTlv:"
                               "No Port information available \r\n");
                return;
            }

            if (EcfmAHUtilChkPortFiltering (pRxLtmInfo->u2LtmInPortNum,
                                            pTempLbLtPortInfo->u1IfOperStatus,
                                            pPduSmInfo->u4RxVlanIdIsId,
                                            pPduSmInfo->u1RxPbbPortType,
                                            &pPduSmInfo->PbbClassificationInfo,
                                            pPduSmInfo->u1RxDirection) !=
                ECFM_SUCCESS)
            {
                u1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_BLOCKED;
            }
        }
        else
        {
            EcfmGetIngressEgressAction (pRxLtmInfo->u2LtmInPortNum,
                                        (UINT2) pPduSmInfo->u4RxVlanIdIsId,
                                        &u1Action, b1Ingress);
        }

        /* 802.1ag clauses 12.14.7.5.3:g, 20.36.2.6, 21.9.8.1 
         * An value of 0 for u1Action field indicates
         * that no Reply Ingress TLV was returned in the LTR.
         */
        if (u1Action != ECFM_LBLT_PORT_FILTERING_ACTION_ING_NOTLV)
        {
            /* Fill ReplyIngress TLV only when the receiving MP is Up MEP/MHF */
            ECFM_PUT_1BYTE (pu1Pdu, ECFM_REPLY_INGRESS_TLV_TYPE);

            /* Fill ReplyIngress TLV Length */
            pu1ReplyTlvLen = pu1Pdu;
            pu1Pdu += ECFM_TLV_LENGTH_FIELD_SIZE;

            /* Fill the Value in TLV that includes 
             * 1.IngressAction, 2.IngressMacAddress, 3.IngressPortIdLength,       
             * 4.IngressPortIdSubType, 5.IngressPortId */

            ECFM_PUT_1BYTE (pu1Pdu, u1Action);

            /* Set Ingress MAC Address as the MAC Address of the receiving MP */
            if (ECFM_LBLT_GET_PORT_INFO (pRxLtmInfo->u2LtmInPortNum) == NULL)
            {
                return;
            }

            ECFM_GET_MAC_ADDR_OF_PORT
                (ECFM_LBLT_PORT_INFO (pRxLtmInfo->u2LtmInPortNum)->u4IfIndex,
                 pu1Pdu);
            pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

            if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT
                (pPduSmInfo->pStackInfo->u2PortNum))
            {
                u1PortIdLen =
                    ECFM_STRLEN (ECFM_LBLT_PORT_INFO
                                 (pRxLtmInfo->u2LtmInPortNum)->au1PortId);
                if ((u1PortIdLen > 0) && (u1PortIdLen < ECFM_MAX_PORTID_LEN))

                {
                    ECFM_PUT_1BYTE (pu1Pdu, u1PortIdLen);
                    ECFM_PUT_1BYTE (pu1Pdu,
                                    ECFM_LBLT_PORT_INFO (pRxLtmInfo->
                                                         u2LtmInPortNum)->
                                    u1PortIdSubType);
                    ECFM_MEMCPY (pu1Pdu,
                                 ECFM_LBLT_PORT_INFO (pRxLtmInfo->
                                                      u2LtmInPortNum)->
                                 au1PortId, u1PortIdLen);
                    pu1Pdu += u1PortIdLen;
                }
            }
            /* Fill the TLV length */
            ECFM_PUT_2BYTE (pu1ReplyTlvLen,
                            ((pu1Pdu - pu1ReplyTlvLen) -
                             ECFM_TLV_LENGTH_FIELD_SIZE));
        }
    }

    /* If TargetMacAddress in the LTM PDU matches, then EgressPort will be 
     * ZERO and EgressTLV should not be transmitted */
    /* If the LTM was received by a Down MEP, or if no Egress Port was
     * identified, or if no Up MEP nor Up MHF belonging to the LTM.s 
     * MA is configured on the Egress Port, does not place a Reply
     * Egress TLV in the LTR; otherwise:
     * 1) Fills the Egress Action field of a Reply Egress TLV (21.9.9) with the
     *    appropriate value according to Table 21-32 on page 195;
     * 2) Places the Egress Port.s Up MP.s MAC address in the Egress MAC Address
     *    field of a Reply Egress TLV in the LTM; and
     * 3) Optionally, fills the remainder of the Reply Egress TLV with Egress
     *    Port.s Port ID information
     */
    if (u2EgressPort != 0
        && (b1MepMipActiveStatus ? ECFM_LBLT_IS_MEP_ACTIVE (pMepNode) :
            ECFM_LBLT_IS_MIP_ACTIVE (pMipNode))
        && (pRxLtmInfo->pEgressStackInfo != NULL))
    {
        b1Ingress = ECFM_FALSE;

        /* Set EgressAction */
        if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
        {
            u1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_OK;

            /* Reset the temp port info ptr */
            pTempLbLtPortInfo = NULL;
            if ((pTempLbLtPortInfo =
                 ECFM_LBLT_GET_PORT_INFO (u2EgressPort)) == NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmPutLtrTlv:"
                               "No Port information available \r\n");
                return;
            }

            if (EcfmAHUtilChkPortFiltering
                (pTempLbLtPortInfo->u4IfIndex,
                 pTempLbLtPortInfo->u1IfOperStatus,
                 pPduSmInfo->u4RxVlanIdIsId, pPduSmInfo->u1RxPbbPortType,
                 &pPduSmInfo->PbbClassificationInfo,
                 pPduSmInfo->u1RxDirection) != ECFM_SUCCESS)
            {
                u1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_BLOCKED;
            }
        }
        else
        {
            EcfmGetIngressEgressAction (u2EgressPort,
                                        pPduSmInfo->u4RxVlanIdIsId, &u1Action,
                                        b1Ingress);
        }

        /* 802.1ag clauses 12.14.7.5.3:o, 20.36.2.10, 21.9.9.1 
         * An value of 0 for u1Action field indicates
         * that no Reply Egress TLV was returned in the LTR.
         */
        if (u1Action != ECFM_LBLT_PORT_FILTERING_ACTION_EGR_NOTLV)
        {
            /* Fill ReplyEgress TLV only when the receiving MP is Down 
             * MEP/MHF */
            ECFM_PUT_1BYTE (pu1Pdu, ECFM_REPLY_EGRESS_TLV_TYPE);

            /* Fill ReplyEgress TLV Length */
            pu1ReplyTlvLen = pu1Pdu;
            pu1Pdu += ECFM_TLV_LENGTH_FIELD_SIZE;

            /* Fill the Value in TLV that includes 
             * 1.EgressAction, 2.EgressMacAddress, 3.EgressPortIdLength,        
             * 4.EgressPortIdSubType, 5.EgressPortId */

            /* Set EgressAction */
            ECFM_PUT_1BYTE (pu1Pdu, u1Action);

            /* Get MAC Address of the Egress Port corresponding to the 
             * Egress Port found and set Egress MAC Address */
            if (ECFM_LBLT_GET_PORT_INFO (u2EgressPort) == NULL)
            {
                return;
            }
            ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                       (u2EgressPort)->u4IfIndex, pu1Pdu);
            pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

            if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT
                (pPduSmInfo->pStackInfo->u2PortNum))
            {

                u1PortIdLen =
                    ECFM_STRLEN (ECFM_LBLT_PORT_INFO (u2EgressPort)->au1PortId);

                if ((u1PortIdLen > 0) && (u1PortIdLen < ECFM_MAX_PORTID_LEN))
                {
                    ECFM_PUT_1BYTE (pu1Pdu, u1PortIdLen);
                    ECFM_PUT_1BYTE (pu1Pdu,
                                    ECFM_LBLT_PORT_INFO (u2EgressPort)->
                                    u1PortIdSubType);
                    ECFM_MEMCPY (pu1Pdu,
                                 ECFM_LBLT_PORT_INFO (u2EgressPort)->au1PortId,
                                 u1PortIdLen);
                    pu1Pdu += u1PortIdLen;
                }
            }
            /* Fill the TLV length */
            ECFM_PUT_2BYTE (pu1ReplyTlvLen, ((pu1Pdu - pu1ReplyTlvLen) -
                                             ECFM_TLV_LENGTH_FIELD_SIZE));
        }
    }

    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pPduSmInfo->pStackInfo->u2PortNum))
    {

        /* Format Sender ID TLV If the MA to which this MEP belongs allows to 
         * transmit Sender ID TLV then add Sender ID TLV in the LTM */
        if (ECFM_LBLT_IS_MEP (pPduSmInfo->pStackInfo))
        {
            ECFM_LBLT_SET_SENDER_ID_TLV (pMepInfo, pu1Pdu);
        }
        else
        {
            ECFM_LBLT_SET_SENDER_ID_TLV_FOR_MHF (pPduSmInfo->pStackInfo,
                                                 pu1Pdu);
        }

        /* Fill Organizational Specific TLV Type */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_TLV_TYPE);

        /* Fill Organizational Specific TLV Length */
        ECFM_PUT_2BYTE (pu1Pdu,
                        (ECFM_ORG_SPEC_MIN_LENGTH +
                         pRxLtmInfo->OrgSpecific.Value.u4OctLen));

        /* Fill OUI */
        ECFM_PUT_NBYTE (pu1Pdu, pRxLtmInfo->OrgSpecific.au1Oui,
                        ECFM_OUI_LENGTH);

        /* Fill SubType in OrgSpecTLV */
        ECFM_PUT_1BYTE (pu1Pdu, pRxLtmInfo->OrgSpecific.u1SubType);

        /* Fill the Optional Field "VALUE" in the Organization Specific TLV */
        if (pRxLtmInfo->OrgSpecific.Value.u4OctLen != 0)
        {
            ECFM_PUT_NBYTE (pu1Pdu, pRxLtmInfo->OrgSpecific.Value.pu1Octets,
                            pRxLtmInfo->OrgSpecific.Value.u4OctLen);
        }

        /* Copy the unknown TLV's received from the LTM to LTR. */
        EcfmLtmCopyUnknownTlvFromLtm (pPduSmInfo, &pu1Pdu);

    }

    /* Fill end TLV to mark the end of the LTR PDU */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_END_TLV_TYPE);
    *ppu1PduEnd = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmLtmRxSmGetEgressPort
 *
 * Description        : This routine is used to get the unique Egress Port.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                                   structure holds the information about the 
 *                                   MP and the so far parsed CFM-PDU.
 *
 * Output(s)          : pu2EgressPort     - Egress Port
 *                      pb1RlyFdbflag      - Flag for Relay FDB Table
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLtmRxSmGetEgressPort (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT2 *pu2EgressPort,
                          BOOL1 * pb1RlyFdbflag)
{
    UINT1              *pIfFwdPortList = NULL;
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    tMacAddr            NullMacAddr = {
        ECFM_INIT_VAL
    };
    UINT2               u2PortCount = ECFM_INIT_VAL;
    UINT1               u1Direction = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    u2LocalPort = pPduSmInfo->pPortInfo->u2PortNum;
    u1Direction = pPduSmInfo->pStackInfo->u1Direction;
    pIfFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pIfFwdPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmLtmRxSmGetEgressPort: Error in allocating memory "
                     "for pIfFwdPortList\r\n");
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pIfFwdPortList, 0, sizeof (tLocalPortListExt));

    /* Get the Egress Port */
    /* No vlan learning is required in this case */
    if (EcfmVlanGetFwdPortList (ECFM_LBLT_CURR_CONTEXT_ID (), u2LocalPort,
                                NullMacAddr,
                                pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                                (UINT2) pPduSmInfo->u4RxVlanIdIsId,
                                pIfFwdPortList) != VLAN_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmGetEgressPort: Unable to get Egress Ports"
                       "\r\n");

        /* Check if the destination mac-address is unicast and is of some port
         * on the same bridge*/
        if (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->uPduInfo.Ltm.TargetMacAddr)
            != ECFM_TRUE)

        {
            tEcfmLbLtPortInfo  *pPortInfo = NULL;
            tEcfmMacAddr        PortMacAddr = { 0 };

            /* Scan through the port-list to check if the destination 
             * mac-address is that of some port on the same bridge 
             */
            for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
                 u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)

            {
                pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPortNum);
                if (pPortInfo != NULL)

                {

                    /* Get the mac-address of the port */
                    ECFM_GET_MAC_ADDR_OF_PORT (pPortInfo->u4IfIndex,
                                               PortMacAddr);

                    /* Compare the port mac-address with the destination
                     * mac-address received*/
                    if (ECFM_COMPARE_MAC_ADDR
                        (PortMacAddr,
                         pPduSmInfo->uPduInfo.Ltm.TargetMacAddr) ==
                        ECFM_SUCCESS)

                    {
                        *pb1RlyFdbflag = ECFM_FALSE;
                        *pu2EgressPort = u2LocalPortNum;
                        UtilPlstReleaseLocalPortList (pIfFwdPortList);
                        return ECFM_SUCCESS;
                    }
                    ECFM_MEMSET (PortMacAddr, ECFM_INIT_VAL,
                                 ECFM_MAC_ADDR_LENGTH);
                }
            }
        }

        /*To get the  Member Ports of the VLAN ID in receiving PDU */
        if (EcfmL2IwfMiGetVlanEgressPorts
            (ECFM_LBLT_CURR_CONTEXT_ID (),
             (UINT2) pPduSmInfo->u4RxVlanIdIsId,
             pIfFwdPortList) == L2IWF_SUCCESS)
        {
            u2PortCount =
                EcfmGetFwdPortCount ((tLocalPortListExt *) pIfFwdPortList);
        }
        /* As per IEEE 802.1ag section:20.42.1.2:
         * If there are only two Bridge Ports that are members of the VLAN.s 
         * member set, the Ingress Port was one of those two Bridge Ports, 
         * and the Ingress Port is not connected to a shared medium, 
         * then this step can identify the Egress Port even if the 
         * Filtering Database is not used for this vlan_identifier. 
         */
        if (u2PortCount == ECFM_TWO_PORT_BRIDGE)
        {
            /* Disable the port on which the packet was received */
            *pb1RlyFdbflag = ECFM_FALSE;
            if ((u1Direction == ECFM_MP_DIR_UP)&&(pPduSmInfo->pMepInfo == NULL))
            {
                /*If the direction is UP, Keep the port as Egress Port */
                *pu2EgressPort = pPduSmInfo->pPortInfo->u2PortNum;
                UtilPlstReleaseLocalPortList (pIfFwdPortList);
                return ECFM_SUCCESS;
            }
            else
            {
                /* If the direction is DOWN, Disable the port on which the packet was received */
                ECFM_RESET_MEMBER_PORT (pIfFwdPortList, u2LocalPort);
            }
        }
    }

    /*Get the egress Port and forward the packet */
    for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
         u2ByteInd++)

    {
        if (pIfFwdPortList[u2ByteInd] == 0)

        {
            continue;
        }
        u1PortFlag = pIfFwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {
            if (u2Port == ECFM_INIT_VAL)

            {
                u2Port =
                    (u2ByteInd * BITS_PER_BYTE) +
                    EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
                *pu2EgressPort = u2Port;
            }

            else

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                               ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxSmGetEgressPort: More than One "
                               "Egress Ports" "found \r\n");
                UtilPlstReleaseLocalPortList (pIfFwdPortList);
                return ECFM_FAILURE;
            }
        }
    }
    UtilPlstReleaseLocalPortList (pIfFwdPortList);

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLtmAHRxSmGetEgressPort
 *
 * Description        : This routine is used to get the unique Egress Port.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                                   structure holds the information about the 
 *                                   MP and the so far parsed CFM-PDU.
 *
 * Output(s)          : pu2Port     - Egress Port
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLtmAHRxSmGetEgressPort (tEcfmLbLtPduSmInfo * pPduSmInfo,
                            UINT2 *pu2EgressPort)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4RetVal = ECFM_INIT_VAL;
    UINT1              *pu1FwdPortList = NULL;
    UINT1              *pu1PortList = NULL;
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    tEcfmMacAddr        NullMacAddr = { ECFM_INIT_VAL };
    UINT2               u2Vid = ECFM_INIT_VAL;
    UINT2               u2PipPort = ECFM_INIT_VAL;
    UINT2               u2VipPort = ECFM_INIT_VAL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocating Memory for FwdPortList */
    pu1FwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1FwdPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmLtmAHRxSmGetEgressPort: Error in allocating memory "
                     "for FwdPortList\r\n");
        return ECFM_FAILURE;
    }
    /* Allocating memory for PortList */
    pu1PortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1PortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmLtmAHRxSmGetEgressPort: Error in allocating memory "
                     "for PortList\r\n");
        /* Releasing Memory for FwdPortList */
        UtilPlstReleaseLocalPortList (pu1FwdPortList);
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pu1FwdPortList, 0, sizeof (tLocalPortListExt));
    ECFM_MEMSET (pu1PortList, 0, sizeof (tLocalPortListExt));

    u2LocalPort = pPduSmInfo->pPortInfo->u2PortNum;
    /* Get the VID of the packet */
    u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLtmAHRxSmGetEgressPort: local-port = %d vid = %d\r\n",
                        u2LocalPort, u2Vid);
    if (pPduSmInfo->u1RxPbbPortType == ECFM_PROVIDER_NETWORK_PORT)
    {
        /* get the port list associated with B-vid this list includes 
         * the CBP ports and PNP port also */
        i4RetVal = EcfmVlanGetFwdPortList (ECFM_LBLT_CURR_CONTEXT_ID (),
                                           u2LocalPort, NullMacAddr,
                                           pPduSmInfo->uPduInfo.Ltm.
                                           TargetMacAddr,
                                           pPduSmInfo->PbbClassificationInfo.
                                           OuterVlanTag.u2VlanId,
                                           pu1FwdPortList);
    }
    else if (pPduSmInfo->u1RxPbbPortType == ECFM_CUSTOMER_BACKBONE_PORT)
    {
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0)
        {
            if (EcfmPbbGetBvidForIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                       u2LocalPort,
                                       pPduSmInfo->PbbClassificationInfo.
                                       InnerIsidTag.u4Isid,
                                       &u2Vid) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmAHRxSmGetEgressPort: Unable to B-vid for the Isid\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_FAILURE;
            }
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId = u2Vid;
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_UNTAGGED;

        }
        else
        {
            u2Vid = pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId;
        }
        /* CBP information is lost at the PNP so we need to fill the BDA, and fill the
         * BSA here only.
         */
        /* for the PDU comming from PIP BDA is swapped at the ingress only */
        if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType !=
             VLAN_TAGGED) &&
            (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0))
        {
            if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1UcaBitValue ==
                 OSIX_TRUE)
                &&
                (ECFM_IS_MULTICAST_ADDR
                 (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.CDAMacAddr) ==
                 ECFM_TRUE))
            {
                EcfmPbbGetBCompBDA (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    u2LocalPort,
                                    pPduSmInfo->PbbClassificationInfo.
                                    InnerIsidTag.u4Isid,
                                    pPduSmInfo->PbbClassificationInfo.
                                    BDAMacAddr, L2IWF_INGRESS, OSIX_TRUE);
            }
            else
            {
                ECFM_MEMCPY (pPduSmInfo->PbbClassificationInfo.BDAMacAddr,
                             pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                             CDAMacAddr, ECFM_MAC_ADDR_LENGTH);
            }
        }
        i4RetVal =
            EcfmVlanGetFwdPortList (ECFM_LBLT_CURR_CONTEXT_ID (), u2LocalPort,
                                    NullMacAddr,
                                    pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                                    u2Vid, pu1FwdPortList);
    }
    else if (pPduSmInfo->u1RxPbbPortType == ECFM_PROVIDER_INSTANCE_PORT)
    {
        if (EcfmPbbGetPipVipWithIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->PbbClassificationInfo.
                                      InnerIsidTag.u4Isid, &u2VipPort,
                                      &u2PipPort) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtmAHRxSmGetEgressPort: "
                           "Unable to get VIP information for the ISID\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            /* Releasing Memory for PortList */
            UtilPlstReleaseLocalPortList (pu1PortList);
            return ECFM_FAILURE;
        }
        /* Get the VIP Corresponding to ISID 
         * remove this VIP from the port list associated with the Vid
         * received from Vlan */
        u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
            VLAN_TAGGED)
        {
            /* get the forward port list for this S/c vlan id */
            i4RetVal =
                EcfmVlanGetFwdPortList (ECFM_LBLT_CURR_CONTEXT_ID (),
                                        u2VipPort, NullMacAddr,
                                        pPduSmInfo->uPduInfo.Ltm.TargetMacAddr,
                                        u2Vid, pu1FwdPortList);
        }
        else
        {
            /* get the forward port list for this S/c vlan id */
            if (EcfmPbbGetCnpMemberPortsForIsid
                (ECFM_LBLT_CURR_CONTEXT_ID (),
                 pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid,
                 pu1FwdPortList) != ECFM_SUCCESS)
            {

                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmAHRxSmGetEgressPort: Unable to get "
                               "Egress Ports\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_FAILURE;
            }
        }
    }
    else
    {
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0)
        {
            /* CFM pdu generated local */
            if (EcfmPbbGetPipVipWithIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                          u2LocalPort,
                                          pPduSmInfo->PbbClassificationInfo.
                                          InnerIsidTag.u4Isid, &u2VipPort,
                                          pu2EgressPort) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmAHRxSmGetEgressPort: Unable to get "
                               "VIP info with ISID\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_FAILURE;
            }
            else
            {
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_SUCCESS;
            }
        }
        else
        {
            u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
            /* get the forward port list for this S/c vlan id */
            if (EcfmL2IwfMiGetVlanEgressPorts
                (ECFM_LBLT_CURR_CONTEXT_ID (), u2Vid,
                 pu1PortList) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmAHRxSmGetEgressPort: Unable to get Egress Ports\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_FAILURE;
            }
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1Priority =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1Priority;
            /* PbbGetPipAndIsidWithVip */
            if (EcfmPbbGetVipIsidWithPortList (ECFM_LBLT_CURR_CONTEXT_ID (),
                                               u2LocalPort, pu1PortList,
                                               &u2VipPort, pu2EgressPort,
                                               &(pPduSmInfo->
                                                 PbbClassificationInfo.
                                                 InnerIsidTag.u4Isid),
                                               &(pPduSmInfo->
                                                 PbbClassificationInfo.
                                                 BDAMacAddr)) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmAHRxSmGetEgressPort: Unable to get "
                               "VIP ISID with PortList\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_FAILURE;
            }
            else
            {
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_SUCCESS;
            }
        }
    }
    /* No vlan learning is required in this case */
    if (i4RetVal != ECFM_SUCCESS ||
        (FsUtilBitListIsAllZeros (pu1FwdPortList, ECFM_PORT_LIST_SIZE)
         == OSIX_TRUE))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmAHRxSmGetEgressPort: Unable to get Egress Ports"
                       "\r\n");
        /* Check if the destination mac-address is unicast and is of some port
         * on the same bridge*/
        if (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->uPduInfo.Ltm.TargetMacAddr) !=
            ECFM_TRUE)
        {
            tEcfmLbLtPortInfo  *pPortInfo = NULL;
            tEcfmMacAddr        PortMacAddr = { 0 };
            /* Scan through the port-list to check if the destination 
             * mac-address is that of some port on the same bridge 
             */
            for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
                 u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)
            {
                pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPortNum);
                if (pPortInfo != NULL)
                {
                    /* Get the mac-address of the port */
                    ECFM_GET_MAC_ADDR_OF_PORTID (pPortInfo->u4IfIndex,
                                                 PortMacAddr, CfaIfInfo);
                    /* Compare the port mac-address with the destination
                     * mac-address received*/
                    if (ECFM_COMPARE_MAC_ADDR (PortMacAddr,
                                               pPduSmInfo->uPduInfo.Ltm.
                                               TargetMacAddr) == ECFM_SUCCESS)
                    {
                        *pu2EgressPort = u2LocalPortNum;
                        /* Releasing Memory for FwdPortList */
                        UtilPlstReleaseLocalPortList (pu1FwdPortList);
                        /* Releasing Memory for PortList */
                        UtilPlstReleaseLocalPortList (pu1PortList);
                        return ECFM_SUCCESS;
                    }
                    ECFM_MEMSET (PortMacAddr, ECFM_INIT_VAL,
                                 ECFM_MAC_ADDR_LENGTH);
                }
            }
        }
        /* Releasing Memory for FwdPortList */
        UtilPlstReleaseLocalPortList (pu1FwdPortList);
        /* Releasing Memory for PortList */
        UtilPlstReleaseLocalPortList (pu1PortList);
        return ECFM_FAILURE;
    }
    /*Get the egress Port and forward the packet */
    for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
         u2ByteInd++)
    {
        if (pu1FwdPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = pu1FwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {

            if (u2Port == ECFM_INIT_VAL)
            {
                u2Port =
                    (u2ByteInd * BITS_PER_BYTE) +
                    EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
                *pu2EgressPort = u2Port;

                pTempLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u2Port);
                if (pTempLbLtPortInfo != NULL)
                {
                    ECFM_GET_MAC_ADDR_OF_PORTID (pTempLbLtPortInfo->u4IfIndex,
                                                 pPduSmInfo->
                                                 PbbClassificationInfo.
                                                 BSAMacAddr, CfaIfInfo);
                }
            }
            else
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                               ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmAHRxSmGetEgressPort: More than One "
                               "Egress Ports" "found \r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                /* Releasing Memory for PortList */
                UtilPlstReleaseLocalPortList (pu1PortList);
                return ECFM_FAILURE;
            }
        }
    }
    /* Releasing Memory for FwdPortList */
    UtilPlstReleaseLocalPortList (pu1FwdPortList);
    /* Releasing Memory for PortList */
    UtilPlstReleaseLocalPortList (pu1PortList);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLtmRxSmForwardLtm
 *
 * Description        : This routine is used to Forward the received LTM.
 *
 * Input(s)           : pPduSmInfo    - Pointer to pdusm structure, this 
 *                                      structure holds the information about 
 the 
 *                                      MP and the so far parsed CFM-PDU.
 *
 * Output(s)          : u2EgressPort - Egress Port
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLtmRxSmForwardLtm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT2 u2EgressPort)
{
    tEcfmBufChainHeader *pFwdLtmBuf = NULL;
    tEcfmLbLtRxLtmPduInfo *pRxLtmInfo = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmMacAddr        MacAddr;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    UINT2               u2PduLen = ECFM_INIT_VAL;
    UINT1              *pu1LtmPduStart = NULL;
    UINT1              *pu1LtmPduEnd = NULL;
    UINT1               au1EgressId[ECFM_EGRESS_ID_LENGTH];


    ECFM_LBLT_TRC_FN_ENTRY ();
    pStackInfo = EcfmLbLtUtilGetMp (u2EgressPort,
                                    pPduSmInfo->u1RxMdLevel,
                                    pPduSmInfo->u4RxVlanIdIsId,
                                    pPduSmInfo->u1RxDirection);
    if (pStackInfo != NULL)
    {
       pPduSmInfo->pStackInfo = pStackInfo;
    }
    ECFM_MEMSET (au1EgressId, ECFM_INIT_VAL, ECFM_EGRESS_ID_LENGTH);
    /*Allocates the CRU Buffer */
    pFwdLtmBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_LTM_PDU_SIZE, ECFM_INIT_VAL);
    if (pFwdLtmBuf == NULL)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_ALL_FAILURE_TRC,
                       "EcfmLtmRxSmForwardLtm:Buffer Allocation failed\r\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pBuf);
    UNUSED_PARAM (u4ByteCount);
    pu1LtmPduEnd = ECFM_LBLT_PDU;
    pu1LtmPduStart = pu1LtmPduEnd;
    ECFM_MEMSET (pu1LtmPduEnd, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
    if (ECFM_COPY_FROM_CRU_BUF
        (pPduSmInfo->pBuf, pu1LtmPduEnd, ECFM_INIT_VAL,
         (pPduSmInfo->u1CfmPduOffset + ECFM_CFM_HDR_SIZE +
          pPduSmInfo->u1RxFirstTlvOffset)) == ECFM_CRU_FAILURE)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmForwardLtm:Copy From CRU Buffer FAILED\r\n");
        ECFM_RELEASE_CRU_BUF (pFwdLtmBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Move the pointer & offset by 6 bytes to point it to the Source 
     * address field */
    pu1LtmPduEnd = pu1LtmPduEnd + (UINT2) ECFM_MAC_ADDR_LENGTH;
    ECFM_MEMSET (MacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* Get MAC Address of the Egress Port */
    if ((pTempLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u2EgressPort)) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmForwardLtm:No Port Information\r\n");
        ECFM_RELEASE_CRU_BUF (pFwdLtmBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (pTempLbLtPortInfo->u4IfIndex, MacAddr);
    ECFM_MEMCPY (pu1LtmPduEnd, MacAddr, ECFM_MAC_ADDR_LENGTH);

    /* Move pointer to LTM TTL value */
    pu1LtmPduEnd = pu1LtmPduStart + pPduSmInfo->u1CfmPduOffset +
        (UINT1) (ECFM_CFM_HDR_SIZE + ECFM_LTM_TRANS_ID_FIELD_SIZE);

    /* Get received LTM information from received PduSmInfo */
    pRxLtmInfo = ECFM_LBLT_GET_LTM_FROM_PDUSM (pPduSmInfo);

    /* Get decremented value in the Forwarded LTM */
    ECFM_PUT_1BYTE (pu1LtmPduEnd, (UINT1) (pRxLtmInfo->u1RxLtmTtl - 1));

    /* Move the pointer and offset to the First TLV Field of the LTM PDU */
    /* Increment the pointer to Original MAC ADDRESS LEngth */
    pu1LtmPduEnd = pu1LtmPduEnd + ECFM_MAC_ADDR_LENGTH;

    /* Increment the pointer to Target MAC ADDRESS LEngth */
    pu1LtmPduEnd = pu1LtmPduEnd + ECFM_MAC_ADDR_LENGTH;
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pPduSmInfo->pStackInfo->u2PortNum))
    {
        /* Fill LTM Egress Identifier TLV Type */
        ECFM_PUT_1BYTE (pu1LtmPduEnd, ECFM_LTM_EGRESS_ID_TLV_TYPE);

        /* Fill LTM Egress Identifier TLV Length */
        ECFM_PUT_2BYTE (pu1LtmPduEnd, ECFM_EGRESS_ID_LENGTH);

        /* Fill LTM Egress Identifier TLV Value */
        ECFM_SET_LTM_EGRESS_ID (pPduSmInfo->pStackInfo, au1EgressId);
        ECFM_MEMCPY (pu1LtmPduEnd, au1EgressId, ECFM_EGRESS_ID_LENGTH);
        pu1LtmPduEnd = pu1LtmPduEnd + ECFM_EGRESS_ID_LENGTH;

        /* Format Sender ID TLV If the MA to which this MEP belongs allows to 
         * transmit Sender ID TLV then add Sender ID TLV in the LTM */
        ECFM_LBLT_SET_SENDER_ID_TLV_FOR_MHF (pPduSmInfo->pStackInfo,
                                             pu1LtmPduEnd);
        /* Fill Organizational Specific TLV Type */
        ECFM_PUT_1BYTE (pu1LtmPduEnd, ECFM_ORG_SPEC_TLV_TYPE);

        /* Fill Organizational Specific TLV Length */
        ECFM_PUT_2BYTE (pu1LtmPduEnd,
                        (ECFM_ORG_SPEC_MIN_LENGTH +
                         pRxLtmInfo->OrgSpecific.Value.u4OctLen));

        /* Fill OUI */
        ECFM_PUT_NBYTE (pu1LtmPduEnd, pRxLtmInfo->OrgSpecific.au1Oui,
                        ECFM_OUI_LENGTH);

        /* Fill SubType in OrgSpecTLV */
        ECFM_PUT_1BYTE (pu1LtmPduEnd, pRxLtmInfo->OrgSpecific.u1SubType);

        /* Fill the Optional Field "VALUE" in the Organization Specific TLV */
        if (pRxLtmInfo->OrgSpecific.Value.u4OctLen != 0)
        {
            ECFM_PUT_NBYTE (pu1LtmPduEnd,
                            pRxLtmInfo->OrgSpecific.Value.pu1Octets,
                            pRxLtmInfo->OrgSpecific.Value.u4OctLen);
        }

        /* Copy the unknown TLV's received from LTM to LTM to be forwarded. */
        EcfmLtmCopyUnknownTlvFromLtm (pPduSmInfo, &pu1LtmPduEnd);
    }

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1LtmPduEnd, ECFM_END_TLV_TYPE);
    u2PduLen = (UINT2) (pu1LtmPduEnd - pu1LtmPduStart);

    /*Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pFwdLtmBuf, pu1LtmPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLen)) != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "EcfmLtmRxSmForwardLtm:Copying into Buffer failed \r\n");
        ECFM_RELEASE_CRU_BUF (pFwdLtmBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    if (EcfmLbLtCtrlTxTransmitPkt
        (pFwdLtmBuf, u2EgressPort, pPduSmInfo->u4RxVlanIdIsId, 0, 0,
         ECFM_MP_DIR_DOWN, ECFM_OPCODE_LTM,
         &(pPduSmInfo->VlanClassificationInfo),
         &(pPduSmInfo->PbbClassificationInfo)) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtmRxSmForwardLtm: LTM PDU Could not be transmitted"
                       "\r\n");
        ECFM_RELEASE_CRU_BUF (pFwdLtmBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLtmRxFwdLtmToFf
 *
 * Description        : This routine is used to Forward the received LTM to 
 *                      Frame Filtering if the Y1731 is Enabled 
 *
 * Input(s)           : pPduSmInfo    - Pointer to pdusm structure, this 
 *                                      structure holds the information about 
 *                                      the MP and the so far parsed CFM-PDU.
 *                      pb1NotEnqueLtr- Flag value to enque LTR
 *                                      if required
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLtmRxFwdLtmToFf (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pb1NotEnqueLtr)
{

    /*Y.1731: When Y1731 enabled and no egress port
     * is found then LTM is forwrded to all egress 
     * ports*/
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pPortInfo->u2PortNum))

    {
        if (ECFM_LBLT_802_1AH_BRIDGE ())
        {
            if (EcfmLbLtAHCtrlTxFwdToFf (pPduSmInfo) != ECFM_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxFwdLtmToFf:" "EcfmLbLtAHCtrlTxFwdToFf "
                               "returned failure\r\n");
                return ECFM_FAILURE;
            }

        }
        else
        {
            if (EcfmLbLtADCtrlTxFwdToFf (pPduSmInfo) != ECFM_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtmRxFwdLtmToFf:" "EcfmLbLtADCtrlTxFwdToFf "
                               "returned failure\r\n");
                return ECFM_FAILURE;
            }
        }
        *pb1NotEnqueLtr = ECFM_TRUE;    /* No LTR is sent */
        return ECFM_SUCCESS;
    }
    return ECFM_FAILURE;
}

/******************************************************************************
 * Function Name      : EcfmLtmRxFwdLtmToPort
 *
 * Description        : This routine is used to Forward the received LTM to 
 *                      Frame Filtering if the Y1731 is Enabled 
 *
 * Input(s)           : pPduSmInfo    - Pointer to pdusm structure, this 
 *                                      structure holds the information about 
 *                                      the MP and the so far parsed CFM-PDU.
 *                      pb1NotEnqueLtr- Flag value to enque LTR
 *                                      if required
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLtmRxFwdLtmToPort (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pb1NotEnqueLtr,
                       UINT1 u1LtmTtl)
{
    UINT2               u2EgressPort = ECFM_INIT_VAL;

    /*Y.1731: When Y1731 enabled then LTM is forwarded to 
     * egress port*/
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pPortInfo->u2PortNum))

    {
        *pb1NotEnqueLtr = ECFM_TRUE;
        u2EgressPort = pPduSmInfo->pStackInfo->u2PortNum;
        if (u1LtmTtl > 1)

        {
            if (ECFM_LBLT_802_1AH_BRIDGE ())
            {
                if ((EcfmLbLtAHCtrlTxFwdToPort
                     (pPduSmInfo->pBuf, (UINT4) (u2EgressPort),
                      &(pPduSmInfo->VlanClassificationInfo),
                      &(pPduSmInfo->PbbClassificationInfo))) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLtmRxFwdLtmToPort:"
                                   "EcfmLbLtAHCtrlTxFwdToPort "
                                   "returned failure\r\n");
                    return ECFM_FAILURE;
                }

            }
            else
            {
                if ((EcfmLbLtADCtrlTxFwdToPort
                     (pPduSmInfo->pBuf, (UINT4) (u2EgressPort),
                      &(pPduSmInfo->VlanClassificationInfo))) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLtmRxFwdLtmToPort:"
                                   "EcfmLbLtADCtrlTxFwdToPort "
                                   "returned failure\r\n");
                    return ECFM_FAILURE;
                }
            }
        }
        return ECFM_SUCCESS;
    }
    return ECFM_FAILURE;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmGetMipDbEntry
 * 
 * DESCRIPTION      : API Function to Get Entry from the MIP CCM DB.
 *
 * INPUT            : SrcMacAddr - Source MAC Address for which the entry is
 *                                 required.
 *                    u2VlanId   - Vlan Id for which the of the received PDU
 *
 * OUTPUT           : u2EgressPOrt
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
EcfmGetMipDbEntry (UINT2 u2VlanId, UINT1 *pSrcMacAddr, UINT2 *pu2EgressPort)
{
    tEcfmLbLtMipCcmDbInfo *pMipCcmDbNode = NULL;
    pMipCcmDbNode = EcfmLbLtUtilGetMipCcmDbEntry (u2VlanId, pSrcMacAddr);
    if (pMipCcmDbNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfMipDbEntry: Egress Port not found \r\n");
        return ECFM_FAILURE;
    }
    *pu2EgressPort = pMipCcmDbNode->u2PortNum;
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmGetIngressEgressAction
 * 
 * DESCRIPTION      : Function to Get Ingress Action based on VLAN membership
 *                    STP Port state and OperStatus of the port.
 *
 * Input(s)           : u2LocalPort - Interface Index
 *                      u4Vid -  VLAN-ID
 *                      pu1Action - Ingress/Egress action
 *                      b1Ingress - Flag value for specifying
 *                                  Ingress Filtering(if TRUE) else
 *                                  Egress Filtering if required
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PRIVATE VOID
EcfmGetIngressEgressAction (UINT2 u2LocalPort, UINT2 u2Vid,
                            UINT1 *pu1Action, BOOL1 b1Ingress)
{
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;

    if ((pTempLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort)) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmGetIngressEgressAction:"
                       "No Port Information present\r\n");
        return;
    }
    u4IfIndex = pTempLbLtPortInfo->u4IfIndex;

    /*Check the port operational Status */
    if (pTempLbLtPortInfo->u1IfOperStatus != CFA_IF_UP)

    {
        if (b1Ingress == ECFM_TRUE)

        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_BLOCKED;
        }

        else

        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_DOWN;
        }
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmGetIngressEgressAction:"
                       "Operational State of the port is not UP\r\n");
        return;
    }

    /*Check the port memebership of Vlan */
    if (EcfmL2IwfMiIsVlanMemberPort
        (ECFM_LBLT_CURR_CONTEXT_ID (), u2Vid, u4IfIndex) != OSIX_TRUE)

    {
        if (b1Ingress == ECFM_TRUE)

        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_VID;
        }

        else

        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_VID;
        }
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmGetIngressEgressAction:"
                       "EcfmL2IwfMiIsVlanMemberPort returned failure\r\n");
        return;
    }

    /* Check if Spanning Tree is enabled in this Context */
    if (EcfmIsStpEnabledInContext
        (ECFM_LBLT_CURR_CONTEXT_ID () != ECFM_SUCCESS))
    {
        if (b1Ingress == ECFM_TRUE)

        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_OK;
        }
        else
        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_OK;
        }
        return;
    }
    else
    {
        /*check for the port state */
        if (EcfmL2IwfGetVlanPortState (u2Vid, u4IfIndex) !=
            AST_PORT_STATE_FORWARDING)

        {
            if (b1Ingress == ECFM_TRUE)

            {
                *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_BLOCKED;
            }

            else

            {
                *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_BLOCKED;
            }
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmGetIngressEgressAction:"
                           "EcfmL2IwfGetVlanPortState returned port state !forwarding\r\n");
            return;
        }
        if (b1Ingress == ECFM_TRUE)
        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_ING_OK;
        }
        else
        {
            *pu1Action = ECFM_LBLT_PORT_FILTERING_ACTION_EGR_OK;
        }
        return;
    }
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLtmCopyUnknownTlvFromLtm
 * 
 * DESCRIPTION      : This function copies Unknown Tlv's received from
 *                    LTM into LTR or LTM.
 *
 * Input(s)           : pPduSmInfo   - Pointer  the PduSm structure
 *                      ppu1Pdu      - pointer to the Pdu
 *
 *                    Note: This function uses a static array. So, please
 *                          ensure that this function is called only from
 *                          LBLT Thread and not from any other thread/task.
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PRIVATE VOID
EcfmLtmCopyUnknownTlvFromLtm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 **ppu1Pdu)
{
    tEcfmBufChainHeader *pBuf = NULL;
    UINT4               u4ByteCount = 0;
    UINT2               u2TlvLength = 0;
    UINT1               u1TlvType = 0;
    UINT1              *pu1EcfmPdu = NULL;
    UINT1              *pu1Pdu = NULL;
    static UINT1        au1Pdu[ECFM_MAX_LTM_PDU_SIZE];

    pu1Pdu = *ppu1Pdu;
    pBuf = pPduSmInfo->pBuf;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /*Remove the ethernet header size from the pdu length */
    u4ByteCount = u4ByteCount - pPduSmInfo->u1CfmPduOffset;
    pu1EcfmPdu = ECFM_GET_DATA_PTR_IF_LINEAR (pBuf,
                                              pPduSmInfo->u1CfmPduOffset,
                                              u4ByteCount);
    if (pu1EcfmPdu == NULL)
    {
        ECFM_MEMSET (au1Pdu, 0, ECFM_MAX_LTM_PDU_SIZE);
        pu1EcfmPdu = au1Pdu;
        if (ECFM_COPY_FROM_CRU_BUF (pBuf, pu1EcfmPdu,
                                    pPduSmInfo->u1CfmPduOffset, u4ByteCount)
            == ECFM_CRU_FAILURE)
        {
            return;
        }
    }

    /* Move pointer to First Additional LTM TLV Offset */
    pu1EcfmPdu = pu1EcfmPdu + ECFM_CFM_HDR_SIZE + sizeof (UINT4) +
        sizeof (UINT1) + ECFM_MAC_ADDR_LENGTH + ECFM_MAC_ADDR_LENGTH;

    ECFM_GET_1BYTE (u1TlvType, pu1EcfmPdu);
    while (u1TlvType != ECFM_END_TLV_TYPE)
    {
        ECFM_GET_2BYTE (u2TlvLength, pu1EcfmPdu);
        if ((u1TlvType == ECFM_LTM_EGRESS_ID_TLV_TYPE) ||
            (u1TlvType == ECFM_SENDER_ID_TLV_TYPE) ||
            (u1TlvType == ECFM_ORG_SPEC_TLV_TYPE))
        {
            pu1EcfmPdu += u2TlvLength;
        }
        else
        {
            ECFM_PUT_1BYTE (pu1Pdu, u1TlvType);
            ECFM_PUT_2BYTE (pu1Pdu, u2TlvLength);

            ECFM_PUT_NBYTE (pu1Pdu, pu1EcfmPdu, u2TlvLength);

            /* Increment the offset in the received PDU. */
            pu1EcfmPdu += u2TlvLength;
        }
        ECFM_GET_1BYTE (u1TlvType, pu1EcfmPdu);
    }

    *ppu1Pdu = pu1Pdu;
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmGetFwdPortCount
 * 
 * DESCRIPTION      : This function counts number of ports in a particular VLAN 
 *               from the forward Port list                    
 *      
 * Input(s)         : pIfFwdPortList - Pointer to the Array IfFwdPortList
 *         
 * OUTPUT           : Port Count of that VLAN
 *              
 * RETURNS          : u2PortCount
 **************************************************************************/

PUBLIC INT2
EcfmGetFwdPortCount (tLocalPortListExt * pIfFwdPortList)
{
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2PortCount = ECFM_INIT_VAL;    /*No of Ports in a
                                                         *particular VLAN*/
    for (u2ByteInd = ECFM_INIT_VAL;
         u2ByteInd < ECFM_PORT_LIST_SIZE; u2ByteInd++)
    {
        for (u2BitIndex = ECFM_INIT_VAL;
             u2BitIndex < BITS_PER_BYTE; u2BitIndex++)
        {
            if (((*pIfFwdPortList)[u2ByteInd] &
                 gau1BitMaskMap[u2BitIndex]) != ECFM_INIT_VAL)
            {
                u2PortCount++;
            }
        }                        /*End for Bit Index */
    }                            /*End for Byte Index */
    return u2PortCount;
}

/******************************************************************************/
/*                           End  of file cfmltmsm.c                          */
/******************************************************************************/
