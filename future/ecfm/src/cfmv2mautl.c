/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmv2mautl.c,v 1.43 2017/02/09 13:51:25 siva Exp $
 *
 * Description:Common Util Low Level Routines for MA 
 * Table and Mep List Table used by both IEEECFM MIB 
 * verisons D8.0 and D8.1.
 *******************************************************************/
#include "cfminc.h"
#include "fscfmmcli.h"
#include "fsmiy1cli.h"
#include "cfmprot.h"

/* Util Routines for Table : Dot1agCfmMaNetTable. */
/****************************************************************************
 Function    :  EcfmUtlValIndexInstMaNetTable 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
EcfmUtlValIndexInstMaNetTable (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Check whether ECFM Module is enabled or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices, MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintaince Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetNextIndexMaNetTable 
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
EcfmUtlGetNextIndexMaNetTable (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 *pu4NextDot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 *pu4NextDot1agCfmMaIndex)
{
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNextNode = NULL;

    /* Check whether ECFM Module is enabled or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices next to MdIndex,
     * MaIndex */
    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
    MaInfo.u4MdIndex = u4Dot1agCfmMdIndex;
    MaInfo.u4MaIndex = u4Dot1agCfmMaIndex;

    pMaNextNode = (tEcfmCcMaInfo *) RBTreeGetNext
        (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo, NULL);
    if (pMaNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintaince Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices next to MdIndex, MaIndex exists */
    /* Set the indices corresponding to MA  entry */
    *pu4NextDot1agCfmMdIndex = pMaNextNode->u4MdIndex;
    *pu4NextDot1agCfmMaIndex = pMaNextNode->u4MaIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaNetFormat 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaNetFormat (UINT4 u4Dot1agCfmMdIndex,
                       UINT4 u4Dot1agCfmMaIndex,
                       INT4 *pi4RetValDot1agCfmMaNetFormat)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Ma name Format from corresponding MA entry */
    *pi4RetValDot1agCfmMaNetFormat = (INT4) (pMaNode->u1NameFormat);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaNetName 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaNetName (UINT4 u4Dot1agCfmMdIndex,
                     UINT4 u4Dot1agCfmMaIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValDot1agCfmMaNetName)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MA entry corresponding to indices MdIndex, MaIndex exists */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    ECFM_MEMCPY (pRetValDot1agCfmMaNetName->pu1_OctetList, pMaNode->au1Name,
                 pMaNode->u1NameLength);
    pRetValDot1agCfmMaNetName->i4_Length = (INT4) pMaNode->u1NameLength;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaNetCcmInterval 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetCcmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaNetCcmInterval (UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            INT4 *pi4RetValDot1agCfmMaNetCcmInterval)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the CcmInterval from corresponding MA entry */
    *pi4RetValDot1agCfmMaNetCcmInterval = (INT4) (pMaNode->u1CcmInterval);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaNetRowStatus 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaNetRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
EcfmUtlGetMaNetRowStatus (UINT4 u4Dot1agCfmMdIndex,
                          UINT4 u4Dot1agCfmMaIndex,
                          INT4 *pi4RetValDot1agCfmMaNetRowStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Row status from corresponding MA entry */
    *pi4RetValDot1agCfmMaNetRowStatus = (INT4) (pMaNode->u1RowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaNetFormat 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
EcfmUtlSetMaNetFormat (UINT4 u4Dot1agCfmMdIndex,
                       UINT4 u4Dot1agCfmMaIndex,
                       INT4 i4SetValDot1agCfmMaNetFormat)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Ma name format to corresponding MA entry */
    pMaNode->u1NameFormat = (UINT1) i4SetValDot1agCfmMaNetFormat;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaFormat, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                      i4SetValDot1agCfmMaNetFormat));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaNetName 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaNetName (UINT4 u4Dot1agCfmMdIndex,
                     UINT4 u4Dot1agCfmMaIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValDot1agCfmMaNetName)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Ma name to corresponding MA entry */
    ECFM_MEMSET (pMaNode->au1Name, ECFM_INIT_VAL, ECFM_MA_NAME_MAX_LEN);
    ECFM_MEMCPY (pMaNode->au1Name, pSetValDot1agCfmMaNetName->pu1_OctetList,
                 pSetValDot1agCfmMaNetName->i4_Length);
    pMaNode->u1NameLength = (UINT1) pSetValDot1agCfmMaNetName->i4_Length;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaName, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %s", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                      pSetValDot1agCfmMaNetName));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaNetCcmInterval 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetCcmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaNetCcmInterval (UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            INT4 i4SetValDot1agCfmMaNetCcmInterval)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    tEcfmCcMepInfo     *pMepTempNode = NULL;
    tEcfmCcMepInfo     *pMepHwnode = NULL;
#endif

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    /* Delete all the MEPs created in hardware.
     * CCM Tx and Rx stop is already indicated when 
     * RowStatus was set to NIS.
     */
    /* Get all MEPs associated with pMaNode */
    pMepTempNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));

    while (pMepTempNode != NULL)
    {
        MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

        EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepTempNode, NULL);
        EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepTempNode->u4IfIndex;

        if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_DELETE,
                                     &EcfmHwInfo) == FNP_FAILURE)
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmUtlSetMaNetCcmInterval :MEP deletion"
                         "indication to H/W failed. \r\n");
            return SNMP_FAILURE;
        }

        ECFM_MEMSET (gpEcfmCcMepNode, 0x00, sizeof (tEcfmCcMepInfo));

        MEMCPY (gpEcfmCcMepNode->au1HwMepHandler, pMepTempNode->au1HwMepHandler,
                ECFM_HW_MEP_HANDLER_SIZE);

        pMepHwnode = (tEcfmCcMepInfo *)
                     RBTreeGet (ECFM_CC_GLOBAL_HW_MEP_TABLE,
                                gpEcfmCcMepNode);
        if (pMepHwnode != NULL)
        {
            /* Remove the entry from global h/w MEP table */
            RBTreeRem (ECFM_CC_GLOBAL_HW_MEP_TABLE, pMepHwnode);
        } 

        /* Reset the hardware handler so that when MEP RowStatus is
         * made active, MEP create indication to hardware will be done
         * with the updated CC interval.
         */
        MEMSET (pMepTempNode->au1HwMepHandler, ECFM_INIT_VAL,
                                        ECFM_HW_MEP_HANDLER_SIZE);
        pMepTempNode =
            (tEcfmCcMepInfo *) TMO_DLL_Next (&(pMaNode->MepTable),
                                             &(pMepTempNode->MepTableDllNode));
    }
#endif
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the CcmInterval value */
    pMaNode->u1CcmInterval = (UINT1) i4SetValDot1agCfmMaNetCcmInterval;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaCcmInterval, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                      i4SetValDot1agCfmMaNetCcmInterval));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaNetRowStatus 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaNetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaNetRowStatus (UINT4 u4Dot1agCfmMdIndex,
                          UINT4 u4Dot1agCfmMaIndex,
                          INT4 i4SetValDot1agCfmMaNetRowStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaNewNode = NULL;
    UINT1               au1DefaultMaName[] = "DEFAULT";
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               u1Type = ECFM_INIT_VAL;
#endif

    /* Get MD Entry Entry for the given Index */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4SetValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }

        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode != NULL)
    {
        /* MA entry corresponding to indices MdIndex, MaIndex exists,
         * and MA's row status is same as user wants to set */
        if (pMaNode->u1RowStatus == (UINT1) i4SetValDot1agCfmMaNetRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }
    else if ((i4SetValDot1agCfmMaNetRowStatus !=
              ECFM_ROW_STATUS_CREATE_AND_WAIT)
             && (i4SetValDot1agCfmMaNetRowStatus !=
                 ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invlaid Row Status value for "
                     "Maintenance Association \n");
        return SNMP_FAILURE;
    }

    if (i4SetValDot1agCfmMaNetRowStatus != ECFM_ROW_STATUS_NOT_IN_SERVICE)
    {
        pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
        if (pMdNode == NULL)
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No MD Entry for the given Index \r\n");
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValDot1agCfmMaNetRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
#ifdef L2RED_WANTED
            if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                               ECFM_RED_MA_ROW_STS_CMD,
                                               ECFM_INIT_VAL,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_TRUE);
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_TRUE;
            }
#endif
            pMaNode->u1RowStatus = (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE;
            /* Update all MEPs row status, associated with MA */
            EcfmSnmpLwUpdateAllMepRowStatus (pMaNode);
#ifdef L2RED_WANTED
            gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_FALSE;
#endif
            break;
        case ECFM_ROW_STATUS_ACTIVE:
            if (pMaNode->u1RowStatus != (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: MA rowstatus not set to Active\n");
                return SNMP_FAILURE;
            }
            /* Set MA row status ACTIVE */
            pMaNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
#ifdef L2RED_WANTED
            if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                               ECFM_RED_MA_ROW_STS_CMD,
                                               ECFM_INIT_VAL,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_TRUE);
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_TRUE;
            }
#endif
            /* Setting all associated MEPs row status to ACTIVE */
            EcfmSnmpLwUpdateAllMepRowStatus (pMaNode);
#ifdef L2RED_WANTED
            gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_FALSE;
#endif
            EcfmLbLtAddMaEntry (ECFM_CC_CURR_CONTEXT_ID (), pMaNode);
            break;
        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
        case ECFM_ROW_STATUS_CREATE_AND_GO:
            /* User wants to create a new MA entry with MdIndex and MaIndex */
            /* Create a node for MA entry that needs to be created */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MA_TABLE (pMaNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "Allocation for MA Node Failed \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return SNMP_FAILURE;
            }

            ECFM_MEMSET (pMaNewNode, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
            /* Put MA  index in new MA node */
            pMaNewNode->u4MaIndex = u4Dot1agCfmMaIndex;
            /* Put MD  index in new MA node */
            pMaNewNode->u4MdIndex = u4Dot1agCfmMdIndex;
            pMaNewNode->pMdInfo = pMdNode;
            /* put other default values */
            pMaNewNode->u1NameFormat = ECFM_ASSOC_NAME_CHAR_STRING;
            ECFM_MEMCPY (pMaNewNode->au1Name, au1DefaultMaName,
                         sizeof (au1DefaultMaName));
            pMaNewNode->u1NameLength = ECFM_MA_DEFAULT_NAME_LEN;
            pMaNewNode->u1CcmInterval = ECFM_CCM_INTERVAL_1_S;
            pMaNewNode->i4PrimarySelType = ECFM_INIT_VAL;

            /* Cross check status should be enabled to trigger the
             * Remote MEP state machine.
             * When MaNet Table is created,Comp Entries will not present,
             * Cross Check status also will be in disabled state
             * and State machine will not triggered. To avoid that 
             * Enable Cross here itself */
            pMaNewNode->u1CrossCheckStatus = ECFM_ENABLE;

            /* Set MA rowstatus */
            if (i4SetValDot1agCfmMaNetRowStatus ==
                ECFM_ROW_STATUS_CREATE_AND_WAIT)
            {
                pMaNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            }
            else
            {
                pMaNewNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
            }

            /* Add new MA node in MaTableIndex in global info and in its 
             * associated MD's MaTable */
            if (EcfmSnmpLwAddMaEntry (pMaNewNode) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Allocation for MA Node Failed\n");
                /* Delete MepList associated with MA */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_TABLE_POOL,
                                     (UINT1 *) (pMaNewNode));
                pMaNewNode = NULL;
                return SNMP_FAILURE;
            }

            if (i4SetValDot1agCfmMaNetRowStatus ==
                ECFM_ROW_STATUS_CREATE_AND_GO)
            {
                i4SetValDot1agCfmMaNetRowStatus = ECFM_ROW_STATUS_ACTIVE;
#ifdef L2RED_WANTED
                if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE)
                {
                    EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                                   ECFM_RED_MA_ROW_STS_CMD,
                                                   ECFM_INIT_VAL,
                                                   u4Dot1agCfmMdIndex,
                                                   u4Dot1agCfmMaIndex,
                                                   ECFM_INIT_VAL,
                                                   ECFM_INIT_VAL, ECFM_TRUE);
                    gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_TRUE;
                }
#endif
                /* Setting all associated MEPs row status to ACTIVE */
                EcfmSnmpLwUpdateAllMepRowStatus (pMaNewNode);
#ifdef L2RED_WANTED
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_FALSE;
#endif
                EcfmLbLtAddMaEntry (ECFM_CC_CURR_CONTEXT_ID (), pMaNewNode);
            }

            break;
        case ECFM_ROW_STATUS_DESTROY:
#ifdef NPAPI_WANTED

            /* Delete MA in HW */
            /* This is placed here in case of MA deletion because 
             * EcfmUtlSetMaCompRowStatus is not called in MA delete.
             */
            MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
            EcfmHwInfo.u1EcfmOffStatus =
                ECFM_CC_GET_CONTEXT_INFO (pMaNode->pMdInfo->u4ContextId)->
                b1GlobalCcmOffload;
            EcfmHwInfo.EcfmHwMepParams.u1MdLevel = pMaNode->pMdInfo->u1Level;

            u1Type = ECFM_NP_MA_DELETION;

            MEMCPY (EcfmHwInfo.EcfmHwMaParams.au1HwHandler,
                    pMaNode->au1HwMaHandler, ECFM_HW_MA_HANDLER_SIZE);

            if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
            MEMSET (pMaNode->au1HwMaHandler, 0, ECFM_HW_MA_HANDLER_SIZE);
#endif
            /* Remove MA node from MD's MaTable and from MaTableIndex * in Global info */
            /* Delete MA from LBLT task also */
            EcfmLbLtRemoveMaEntry (ECFM_CC_CURR_CONTEXT_ID (), pMaNode);
            EcfmSnmpLwDeleteMaEntry (pMaNode);
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_TABLE_POOL, (UINT1 *) (pMaNode));
            pMaNode = NULL;

            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaCompRowStatus
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaCompRowStatus (UINT4 u4Ieee8021CfmMaComponentId,
                           UINT4 u4Dot1agCfmMdIndex,
                           UINT4 u4Dot1agCfmMaIndex,
                           INT4 i4SetValIeee8021CfmMaCompRowStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
#endif
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    INT4                i4RetPrimaryVid = 0;
    UINT2               u2NumberOfVids = ECFM_INIT_VAL;
    UINT2               u2VlanId = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
#ifdef NPAPI_WANTED
    UINT1               u1Type = ECFM_INIT_VAL;
#endif
    BOOL1               b1VlanAwareMa = ECFM_FALSE;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MD Entry Entry for the given Index */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4SetValIeee8021CfmMaCompRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4SetValIeee8021CfmMaCompRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex
     * MaIndex exists, and MA's row status is same as user wants to set */
    if (pMaNode->u1CompRowStatus == (UINT1) i4SetValIeee8021CfmMaCompRowStatus)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValIeee8021CfmMaCompRowStatus != ECFM_ROW_STATUS_NOT_IN_SERVICE)
    {
        pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
        if (pMdNode == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    EcfmUtlGetMaCompPriSelectOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                     u4Dot1agCfmMdIndex,
                                     u4Dot1agCfmMaIndex,
                                     (UINT4 *) &i4RetPrimaryVid);

    if (!(ECFM_IS_MEP_ISID_AWARE (i4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaRowStatus, u4SeqNum,
                              TRUE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    }

    switch (i4SetValIeee8021CfmMaCompRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
#ifdef L2RED_WANTED
            if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                               ECFM_RED_MA_ROW_STS_CMD,
                                               ECFM_INIT_VAL,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_TRUE);
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_TRUE;
            }
#endif
            pMaNode->u1CompRowStatus = (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE;
            /* Update all MEPs row status, associated with MA */
            EcfmSnmpLwUpdateAllMepRowStatus (pMaNode);
#ifdef L2RED_WANTED
            gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_FALSE;
#endif
            break;
        case ECFM_ROW_STATUS_ACTIVE:
            if (pMaNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: MA rowstatus not set to Active\n");
                if (!(ECFM_IS_MEP_ISID_AWARE (i4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      i4SetValIeee8021CfmMaCompRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }

            if (pMaNode->u1CompRowStatus !=
                (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: MA rowstatus not set to Active\n");
                if (!(ECFM_IS_MEP_ISID_AWARE (i4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      i4SetValIeee8021CfmMaCompRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }

                return SNMP_FAILURE;
            }

            if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaNode) != ECFM_TRUE)
            {
                /* Set number of VlanIds as depending upon no. of entries
                 * found in VLAN table with MA's PrimaryVid */
                if (pMaNode->u4PrimaryVidIsid == 0)
                {
                    pMaNode->u2NumberOfVids = 0;
                }
                else
                {
                    u2NumberOfVids =
                        EcfmSnmpLwGetNoOfEntriesForPVid (pMaNode->
                                                         u4PrimaryVidIsid);
                    if (u2NumberOfVids == 0)
                    {
                        pMaNode->u2NumberOfVids = 1;
                    }
                    else
                    {
                        pMaNode->u2NumberOfVids = u2NumberOfVids +
                            (UINT2) ECFM_INCR_VAL;
                    }
                }
            }
            /* Set MA row status ACTIVE */
            pMaNode->u1CompRowStatus = ECFM_ROW_STATUS_ACTIVE;
#ifdef L2RED_WANTED
            if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                               ECFM_RED_MA_ROW_STS_CMD,
                                               ECFM_INIT_VAL,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_TRUE);
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_TRUE;
            }
#endif
            /* Setting all associated MEPs row status to ACTIVE */
            EcfmSnmpLwUpdateAllMepRowStatus (pMaNode);
#ifdef L2RED_WANTED
            gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd = ECFM_FALSE;
#endif
            /* Update DefaultMd status corresponding to MA primaryVid and 
             * level and Evaluate for MIP creation for a change of
             * MA, VlanId */
            if (pMaNode->u4PrimaryVidIsid != 0)
            {
                /* If UP MEP associated with this MA exists then only 
                 * update default MdStatus
                 */
                if (EcfmIsMepAssocWithMa (pMaNode->u4MdIndex,
                                          pMaNode->u4MaIndex, -1,
                                          ECFM_MP_DIR_UP) == ECFM_TRUE)
                {
                    EcfmSnmpLwUpdateDefaultMdStatus (pMdNode->u1Level,
                                                     pMaNode->u4PrimaryVidIsid,
                                                     ECFM_FALSE);
                }

                EcfmCcUtilEvaluateAndCreateMip (-1, pMaNode->u4PrimaryVidIsid,
                                                ECFM_TRUE);
            }

            /* Update the MA at LBLT task also */
            EcfmLbLtAddMaEntry (ECFM_CC_CURR_CONTEXT_ID (), pMaNode);
            break;
        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
        case ECFM_ROW_STATUS_CREATE_AND_GO:

            pMaNode->u1MhfCreation = ECFM_MHF_CRITERIA_DEFER;
            pMaNode->u1IdPermission = ECFM_SENDER_ID_DEFER;

            /*Initialise DLL for MEPs and RBTree for MepList */
            TMO_DLL_Init (&(pMaNode->MepTable));
            /* Set MA rowstatus */
            if (i4SetValIeee8021CfmMaCompRowStatus ==
                ECFM_ROW_STATUS_CREATE_AND_WAIT)
            {
                pMaNode->u1CompRowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            }
            else
            {
                pMaNode->u1CompRowStatus = ECFM_ROW_STATUS_ACTIVE;
                /* Note: Workaround done to send create and
                 * wait and active instead of create and go*/
                if (!(ECFM_IS_MEP_ISID_AWARE (i4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      ECFM_ROW_STATUS_CREATE_AND_WAIT));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                    i4SetValIeee8021CfmMaCompRowStatus = ECFM_ROW_STATUS_ACTIVE;
                }
            }
            break;
        case ECFM_ROW_STATUS_DESTROY:

            /* Update DefaultMd status corresponding to MA primaryVid and 
             * level and Evaluate for MIP creation for a change of MA
             */
            if (pMaNode->u4PrimaryVidIsid != 0)
            {
                b1VlanAwareMa = ECFM_TRUE;
                u1MdLevel = pMdNode->u1Level;
                u2VlanId = (UINT2) pMaNode->u4PrimaryVidIsid;
            }

            if (b1VlanAwareMa == ECFM_TRUE)
            {
                /* Update default MdStatus, as all MEPs associated                       
                 * would have been deleted */
                EcfmSnmpLwUpdateDefaultMdStatus (u1MdLevel, u2VlanId,
                                                 ECFM_TRUE);
                EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanId, ECFM_TRUE);
            }
            /* When MaComp Row Status set as Destroy, we are resetting 
             * all the structure variables used in MaComp Table as Zero*/
            pMaNode->u4PrimaryVidIsid = ECFM_INIT_VAL;
            pMaNode->i4PrimarySelType = ECFM_INIT_VAL;
            pMaNode->u2NumberOfVids = ECFM_INIT_VAL;
            pMaNode->u1MhfCreation = ECFM_INIT_VAL;
            pMaNode->u1IdPermission = ECFM_INIT_VAL;
            pMaNode->u1CompRowStatus = ECFM_INIT_VAL;

            break;
        default:
            break;
    }

#ifdef NPAPI_WANTED
    MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

    EcfmHwInfo.u1EcfmOffStatus =
        ECFM_CC_GET_CONTEXT_INFO (pMaNode->pMdInfo->u4ContextId)->
        b1GlobalCcmOffload;
    EcfmHwInfo.EcfmHwMepParams.u1MdLevel = pMaNode->pMdInfo->u1Level;

    if (i4SetValIeee8021CfmMaCompRowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        /* Create MA in HW */
        u1Type = ECFM_NP_MA_CREATION;

        if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }
    else if (i4SetValIeee8021CfmMaCompRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        /* Delete MA in HW */
        u1Type = ECFM_NP_MA_DELETION;

        MEMCPY (EcfmHwInfo.EcfmHwMaParams.au1HwHandler, pMaNode->au1HwMaHandler,
                ECFM_HW_MA_HANDLER_SIZE);

        if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
        {
            return ECFM_FAILURE;
        }
        MEMSET (pMaNode->au1HwMaHandler, 0, ECFM_HW_MA_HANDLER_SIZE);
    }
#endif

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (i4RetPrimaryVid)))
    {
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex,
                          i4SetValIeee8021CfmMaCompRowStatus));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2CfmMaNetFormat 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTestv2CfmMaNetFormat (UINT4 *pu4ErrorCode,
                             UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             INT4 i4TestValDot1agCfmMaNetFormat)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /* Validate MA name format value */
    if ((i4TestValDot1agCfmMaNetFormat <= ECFM_ASSOC_NAME_IEEE_RESERVED) ||
        ((i4TestValDot1agCfmMaNetFormat > ECFM_ASSOC_NAME_RFC2865_VPN_ID) &&
         (i4TestValDot1agCfmMaNetFormat != ECFM_ASSOC_NAME_ICC)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MA Name Format\n");
        return SNMP_FAILURE;
    }

    /* Get MD configured for the level */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Domain for given Index\n");
        return SNMP_FAILURE;
    }

    /* Get MD configured for the level */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Domain for given Index\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1agCfmMaNetFormat == ECFM_ASSOC_NAME_ICC) &&
        (pMdNode->u1NameFormat != ECFM_DOMAIN_NAME_TYPE_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ECFM_MA_FORMAT_ERR);
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MA Name Format\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether Ma name format can be set */
    if (pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Cannot set Name for this MA, "
                     " Row Status already ACTIVE\n");
        return SNMP_FAILURE;
    }

    /* Check whether Ma name format can be modified */
    /* If any MEP associated with this MA entry, this value can't be modified */
    if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaNode) == ECFM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP is associated with this MA, "
                     " Cannot set MA Name\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaNetName  
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTestv2MaNetName (UINT4 *pu4ErrorCode,
                        UINT4 u4Dot1agCfmMdIndex,
                        UINT4 u4Dot1agCfmMaIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValDot1agCfmMaNetName)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    INT4                i4MaIdLength = ECFM_INIT_VAL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /* Get MD configured for the level */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Domain for " " given Index\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     " given Indices\n");
        return SNMP_FAILURE;
    }

    /* Check whether Ma name value can be set */
    if (pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Status ACTIVE, " " cannot set MA Name\n");
        return SNMP_FAILURE;
    }

    /* Check whether Ma name value can be modified */
    /* If any MEP associated with this MA, this value can't be modified */
    if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaNode) == ECFM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP associated with this MA, "
                     " Cannot set MA Name\n");
        return SNMP_FAILURE;
    }
    if (STRCMP (pTestValDot1agCfmMaNetName->pu1_OctetList, "0") == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Name cannot be Zero\n");
        return SNMP_FAILURE;
    }

    /* Check whether Ma name is corresponding to its format */
    if ((EcfmValidateMaNameFormat
         (pTestValDot1agCfmMaNetName->pu1_OctetList,
          pTestValDot1agCfmMaNetName->i4_Length,
          pMaNode->u1NameFormat, pMdNode, pu4ErrorCode)) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Name is not according to MA Name format\n");
        return SNMP_FAILURE;
    }

    /* MA Name should not overrun 48 Bytes Octet Length of MAID
     * i.e. MA Name can be (MAID Length(48) - (MD Name format(1
     * byte) + MD Name Length(1 byte) + MD Name (as per MDlen) +
     * MA Name Format (1 byte) + MA Name Length (1 byte) +
     * MA Name (as per MA Name len))
     */
    /* 1. If the MDName format is NONE then the below entries will not
     * be present
     * i) ECFM_MD_NAME_LEN_FIELD_SIZE
     * ii) ECFM_MA_NAME_FRMT_FIELD_SIZE
     * iii) pMdNode->u1NameLength as per the section 21.6.5.1 Table
     *      21-18 in IEEE 802.1ag standard.
     */

    if (pMdNode->u1NameFormat == ECFM_DOMAIN_NAME_TYPE_NONE)
    {
        i4MaIdLength = ECFM_MD_NAME_FRMT_FIELD_SIZE +
            ECFM_MA_NAME_FRMT_FIELD_SIZE + ECFM_MA_NAME_LEN_FIELD_SIZE;
    }
    else
    {
        i4MaIdLength = ECFM_MD_NAME_FRMT_FIELD_SIZE +
            ECFM_MD_NAME_LEN_FIELD_SIZE +
            ECFM_MA_NAME_FRMT_FIELD_SIZE +
            pMdNode->u1NameLength + ECFM_MA_NAME_LEN_FIELD_SIZE;

    }

    if ((ECFM_MAID_FIELD_SIZE - i4MaIdLength) <
        pTestValDot1agCfmMaNetName->i4_Length)
    {
        CLI_SET_ERR (CLI_ECFM_MA_NAME_LEN_EXCEED_MAID_LEN);
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Name cannot override 48 byte MAID length"
                     " which includes MD Name and MA Name\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaNetCcmInterval 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetCcmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTestv2MaNetCcmInterval (UINT4 *pu4ErrorCode,
                               UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               INT4 i4TestValDot1agCfmMaNetCcmInterval)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4IntervalInMsec = ECFM_INIT_VAL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /* Validating CcmInterval value */
    /* IEEE 802.1ag, Table 21-16 - CCM Interval field */
    if ((i4TestValDot1agCfmMaNetCcmInterval < ECFM_CCM_INTERVAL_INVALID) ||
        (i4TestValDot1agCfmMaNetCcmInterval > ECFM_CCM_INTERVAL_10_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for CCM Interval\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association with given"
                     " Indices\n");
        return SNMP_FAILURE;
    }

    pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);
    
    while (pMepNode != NULL)
    {
        if ((pMepNode->pMaInfo->u4MaIndex == pMaNode->u4MaIndex) &&
           (pMepNode->pMaInfo->u4MdIndex == pMaNode->u4MdIndex))
        {
            if ((pMepNode->b1MepCcmOffloadStatus != ECFM_TRUE) &&
               ((i4TestValDot1agCfmMaNetCcmInterval == ECFM_CCM_INTERVAL_300Hz) || 
               (i4TestValDot1agCfmMaNetCcmInterval == ECFM_CCM_INTERVAL_10_Ms))) 
            {
               *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
               ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                            "\tSNMP: Timer value not supported\n");
               return SNMP_FAILURE;
            }
         }
        pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
    }
            
    /* Check for system timer minumum granuality */
    switch (i4TestValDot1agCfmMaNetCcmInterval)
    {
        case ECFM_CCM_INTERVAL_INVALID:
            break;
        case ECFM_CCM_INTERVAL_300Hz:
            break;
        default:
            ECFM_GET_CCM_INTERVAL (i4TestValDot1agCfmMaNetCcmInterval,
                                   u4IntervalInMsec);
            if (ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT > u4IntervalInMsec)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
    }


    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether CcmInterval value can be set */
    if (pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Satus is ACTIVE, "
                     " Cannot set MA CCM Interval\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaNetRowStatus 
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaNetRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
EcfmUtlTestv2MaNetRowStatus (UINT4 *pu4ErrorCode,
                             UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             INT4 i4TestValDot1agCfmMaNetRowStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdTempNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    UINT4               u4NextDot1agCfmMdIndex = ECFM_INIT_VAL;
    UINT4               u4NextDot1agCfmMaIndex = ECFM_INIT_VAL;
    UINT4               u4NextDot1agCfmMaMepListIdentifier = ECFM_INIT_VAL;
    UINT1               au1TempMaName[] = "DEFAULT";
    UINT4               u4EntryCount = 0;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /* Validate row status value */
    if ((i4TestValDot1agCfmMaNetRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValDot1agCfmMaNetRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for MA Row Status\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        /* MA entry corresponding to indices MdIndex, MaIndex does not exists
         * user wants to change MA entry's row status */

        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4TestValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }

        else if ((i4TestValDot1agCfmMaNetRowStatus !=
                  ECFM_ROW_STATUS_CREATE_AND_GO)
                 && (i4TestValDot1agCfmMaNetRowStatus !=
                     ECFM_ROW_STATUS_CREATE_AND_WAIT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Value for MA Row Status\n");
            return SNMP_FAILURE;
        }

        /* Check MA's associated MD entry's row status */
        pMdTempNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
        if (pMdTempNode == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No Maintenance domain with given"
                         " Indices\n");
            return SNMP_FAILURE;
        }

        if (pMdTempNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
        {
            /* MA's associated MD entry's row status is not ACTIVE */
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: MA's associated MD entry's row "
                         " status is not ACTIVE\n");
            return SNMP_FAILURE;
        }

        /* check if MA with create and go allowed */
        if (i4TestValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO)
        {
            if (EcfmSnmpLwDefaultMaConfigAllowed (pMdTempNode) == ECFM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP:Insufficient Information MA Creation \n");
                return SNMP_FAILURE;
            }
        }
        /* Number of MA created count is fetched from RBTree structure and it 
         * is compared with maximum Number of MA that are allowed to create */ 
         RBTreeCount (ECFM_CC_MA_TABLE, &u4EntryCount); 
         if (u4EntryCount >= ECFM_MAX_MA_IN_SYSTEM) 
         { 
             CLI_SET_ERR (CLI_ECFM_MAX_MA_CREATION_REACHED); 
             *pu4ErrorCode = SNMP_ERR_WRONG_VALUE; 
             ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC, 
                          "\tSNMP:Reaches maximum MA creation\n"); 
             return SNMP_FAILURE; 
         } 

        return SNMP_SUCCESS;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists and its row
     * status is same as user wants to set */
    if (pMaNode->u1RowStatus == (UINT1) i4TestValDot1agCfmMaNetRowStatus)
    {
        return SNMP_SUCCESS;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists and
     * user wants to create MA entry */
    /* If row is already created and user wants to create it */
    if ((i4TestValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT) ||
        (i4TestValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid MA Row Status Value\n");
        return SNMP_FAILURE;
    }
    /* If user wants to make MA's row status to ACTIVE */
    if ((i4TestValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_ACTIVE) &&
        (pMaNode->u1RowStatus == ECFM_ROW_STATUS_NOT_IN_SERVICE))
    {
        /* If user doesnot provide MA name and Format is Primary-vid
         * then set MA name equal to MA primary vlan id, this is
         * applicable only for vlan-aware MA */
        if ((ECFM_MEMCMP (pMaNode->au1Name, au1TempMaName,
                          sizeof (au1TempMaName)) == 0)
            && (pMaNode->u1NameFormat == ECFM_ASSOC_NAME_PRIMARY_VID)
            && (pMaNode->u4PrimaryVidIsid != 0))
        {
            UINT1               u1NameLsb = ECFM_INIT_VAL;
            UINT1               u1NameMsb = ECFM_INIT_VAL;
            ECFM_MEMSET (pMaNode->au1Name, ECFM_INIT_VAL,
                         ECFM_MA_NAME_ARRAY_SIZE);
            u1NameLsb = pMaNode->u4PrimaryVidIsid & ECFM_MASK_WITH_VAL_255;
            pMaNode->au1Name[1] = u1NameLsb;
            u1NameMsb = pMaNode->u4PrimaryVidIsid >> ECFM_SHIFT_8BITS;
            pMaNode->au1Name[0] = u1NameMsb;
            pMaNode->u1NameLength = ECFM_ASSOC_NAME_PRIMARY_VID_LENGTH;
        }

        if (EcfmSnmpLwIsInfoConfiguredForMa (pMaNode) != ECFM_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Incomplete information for MA creation\n");
            return SNMP_FAILURE;
        }
    }

    /* Configured ICC and UMC should not exceed more than
     * ECFM_MEG_ID_LEN(13) */
    if ((pMaNode->u1IccCodeLength + pMaNode->u1UmcCodeLength) > ECFM_MEG_ID_LEN)
    {
        CLI_SET_ERR (CLI_ECFM_ICC_UMC_LEN_EXCEED_MEGID_LEN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid length for ICC and UMC Code\n");
        return SNMP_FAILURE;
    }

    /* if MaFormat is ICC and no ICC and UMC values are configured then 
     * should not allow MA Row status to active */ 
    if ((pMaNode->u1NameFormat == ECFM_ASSOC_NAME_ICC) && 
        (pMaNode->u1IccCodeLength == ECFM_INIT_VAL) && 
        (pMaNode->u1UmcCodeLength == ECFM_INIT_VAL)) 
    { 
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE; 
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,"\tSNMP:When Ma name                                      format is ICC, both ICC and UMC is not configured\n"); 
        return SNMP_FAILURE; 
    } 

    /* SNMP: Check either ICC or UMC is not configured */
    if (((pMaNode->u1IccCodeLength == ECFM_INIT_VAL) ^
         (pMaNode->u1UmcCodeLength == ECFM_INIT_VAL)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Either ICC or UMC is not configured\n");
        return SNMP_FAILURE;
    }
    /* Check if user wants to delete MA entry */
    /* whether MA's deletion is possible or not */
    if (i4TestValDot1agCfmMaNetRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaNode) == ECFM_TRUE)
        {
            /* Its not possible to delete MA directly,first all
             * MEPs belongs to this MA should be deleted */
            CLI_SET_ERR (CLI_ECFM_MA_DEL_MEP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:MEP Associated with MA, cannot delete MA\n");
            return SNMP_FAILURE;
        }
          /*Test if any MIP is associated with MA*/
        pMipNode = (tEcfmCcMipInfo *) RBTreeGetFirst (ECFM_CC_GLOBAL_MIP_TABLE);
        while (pMipNode != NULL)
        {
            if ((pMipNode->u4MaIndex == pMaNode->u4MaIndex) && (pMipNode->u4MdIndex ==  pMaNode->u4MdIndex))
            {
                if ((pMipNode->u4VlanIdIsid == pMaNode->u4PrimaryVidIsid) && (pMipNode->u1RowStatus == ACTIVE))
                {
                    CLI_SET_ERR (CLI_ECFM_DEL_MIP_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                  "\tSNMP:MIP Associated with MA, cannot delete MA\n");
                    return SNMP_FAILURE;
                }
            }
            pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                      (tRBElem *) pMipNode, NULL);
        } 

        /* Get first MaMepList entry with provided Md and Ma index */

        if (nmhGetNextIndexDot1agCfmMaMepListTable
            (u4Dot1agCfmMdIndex, &u4NextDot1agCfmMdIndex, u4Dot1agCfmMaIndex,
             &u4NextDot1agCfmMaIndex, 0,
             &u4NextDot1agCfmMaMepListIdentifier) == SNMP_SUCCESS)
        {
            if ((u4NextDot1agCfmMdIndex == u4Dot1agCfmMdIndex) &&
                (u4NextDot1agCfmMaIndex == u4Dot1agCfmMaIndex))
            {
                /* Its not possible to delete MA, first all MaMepList entries
                 * belonging to this MA should be deleted. */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP:MEPLIST Associated with MA, cannot delete MA\n");
                return SNMP_FAILURE;
            }
        }

    }

    return SNMP_SUCCESS;
}

/***********************************************************************/
/*         LOW LEVEL Routines for Table : Ieee8021CfmMaCompTable.      */
/***********************************************************************/

/****************************************************************************
 Function    :  EcfmUtlValIndexInstMaCompTable 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
EcfmUtlValIndexInstMaCompTable (UINT4 u4Ieee8021CfmMaComponentId,
                                UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);
    /* Check whether ECFM Module is enabled or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices, MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* When MaCompTable entries are not created or destroyed,
     * it should not return Zero values*/
    if (pMaNode->u1CompRowStatus == ECFM_INIT_VAL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetNextIndexMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                nextDot1agCfmMaComponentId
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
EcfmUtlGetNextIndexMaCompTable (UINT4 u4Ieee8021CfmMaComponentId,
                                UINT4 *pu4NextIeee8021CfmMaComponentId,
                                UINT4 u4Dot1agCfmMdIndex,
                                UINT4 *pu4NextDot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 *pu4NextDot1agCfmMaIndex)
{
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNextNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);
    *pu4NextIeee8021CfmMaComponentId = ECFM_CC_CURR_CONTEXT_ID ();
    ECFM_CONVERT_CTXT_ID_TO_COMP_ID (*pu4NextIeee8021CfmMaComponentId);
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices next to MdIndex, MaIndex */
    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
    MaInfo.u4MdIndex = u4Dot1agCfmMdIndex;
    MaInfo.u4MaIndex = u4Dot1agCfmMaIndex;
    pMaNextNode = (tEcfmCcMaInfo *) RBTreeGetNext
        (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo, NULL);
    if (pMaNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices next to MdIndex, MaIndex exists */
    /* Set the indices corresponding to MA entry */
    *pu4NextDot1agCfmMdIndex = pMaNextNode->u4MdIndex;
    *pu4NextDot1agCfmMaIndex = pMaNextNode->u4MaIndex;

    /* When MaCompTable is not created or destroyed for particular
     * service,we sholud not give the Null value instead we should 
     * try to get the next active entry by passing the next index*/

    if (pMaNextNode->u1CompRowStatus == ECFM_INIT_VAL)
    {
        u4Ieee8021CfmMaComponentId = (UINT4) *pu4NextIeee8021CfmMaComponentId;
        u4Dot1agCfmMdIndex = (UINT4) *pu4NextDot1agCfmMdIndex;
        u4Dot1agCfmMaIndex = (UINT4) *pu4NextDot1agCfmMaIndex;

        return (EcfmUtlGetNextIndexMaCompTable (u4Ieee8021CfmMaComponentId,
                                                pu4NextIeee8021CfmMaComponentId,
                                                u4Dot1agCfmMdIndex,
                                                pu4NextDot1agCfmMdIndex,
                                                u4Dot1agCfmMaIndex,
                                                pu4NextDot1agCfmMaIndex));

    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmUtlGetMaCompPriSelType 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                i4RetValIeee8021CfmMaCompPrimarySelectorType 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlGetMaCompPriSelType (UINT4 u4MaComponentId,
                            UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            INT4 *pi4RetValIeee8021CfmMaCompPrimarySelectorType)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4MaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Primary Selector Type value from corresponding MA entry */
    *pi4RetValIeee8021CfmMaCompPrimarySelectorType = pMaNode->i4PrimarySelType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaCompPrimaryVlanId 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompPrimaryVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaCompPriSelectOrNone (UINT4 u4MaComponentId,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 *pu4RetValMaCompPrimarySelectorOrNone)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4MaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the MhfCreation value from corresponding MA entry */
    if (pMaNode->i4PrimarySelType == ECFM_SERVICE_SELECTION_ISID)
    {
        *pu4RetValMaCompPrimarySelectorOrNone =
            ECFM_ISID_INTERNAL_TO_ISID (pMaNode->u4PrimaryVidIsid);
    }
    else
    {
        *pu4RetValMaCompPrimarySelectorOrNone = pMaNode->u4PrimaryVidIsid;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaCompMhfCreation 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaCompMhfCreation (UINT4 u4Ieee8021CfmMaComponentId,
                             UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             INT4 *pi4RetValMaCompMhfCreation)
{

    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the MhfCreation value from corresponding MA entry */
    *pi4RetValMaCompMhfCreation = (INT4) (pMaNode->u1MhfCreation);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaCompIdPermission 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaCompIdPermission (UINT4 u4Ieee8021CfmMaComponentId,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              INT4 *pi4RetValMaCompIdPermission)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the SenderId Permission from corresponding MA entry */
    *pi4RetValMaCompIdPermission = (INT4) (pMaNode->u1IdPermission);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaCompNumberOfVids 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompNumberOfVids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaCompNumberOfVids (UINT4 u4Ieee8021CfmMaComponentId,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 *pu4RetValMaCompNumberOfVids)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the SenderId Permission from corresponding MA entry */
    *pu4RetValMaCompNumberOfVids = (UINT4) (pMaNode->u2NumberOfVids);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetMaCompRowStatus 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValDot1agCfmMaCompRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlGetMaCompRowStatus (UINT4 u4Ieee8021CfmMaComponentId,
                           UINT4 u4Dot1agCfmMdIndex,
                           UINT4 u4Dot1agCfmMaIndex,
                           INT4 *pi4RetValMaCompRowStatus)
{

    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Row status from corresponding MA entry */
    *pi4RetValMaCompRowStatus = (INT4) pMaNode->u1CompRowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmUtlSetMaCompPriSelType 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompPrimaryServiceSelectorType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlSetMaCompPriSelType (UINT4 u4MaComponentId,
                            UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            INT4 i4SetValMaCompPrimarySelectorType)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4MaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Primary SelectorType  value to corresponding MA entry */
    pMaNode->i4PrimarySelType = i4SetValMaCompPrimarySelectorType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaCompPrimaryVlanId 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompPrimaryVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaCompPriSelorNone (UINT4 u4MaComponentId,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              INT4 i4SetValMaCompPrimaryVlanId)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4MaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set the Mhf Creation criteria value to corresponding MA entry */
    if (pMaNode->i4PrimarySelType == ECFM_SERVICE_SELECTION_ISID)
    {
        pMaNode->u4PrimaryVidIsid =
            ECFM_ISID_TO_ISID_INTERNAL ((UINT4) (i4SetValMaCompPrimaryVlanId));
    }
    else
    {
        pMaNode->u4PrimaryVidIsid = (UINT4) i4SetValMaCompPrimaryVlanId;
    }

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaPrimaryVlanId, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                      i4SetValMaCompPrimaryVlanId));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaCompMhfCreation 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaCompMhfCreation (UINT4 u4Ieee8021CfmMaComponentId,
                             UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             INT4 i4SetValMaCompMhfCreation)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /*Set the Mhf Creation criteria value to corresponding MA entry */
    pMaNode->u1MhfCreation = (UINT1) i4SetValMaCompMhfCreation;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaMhfCreation, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                      i4SetValMaCompMhfCreation));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaCompIdPermission 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaCompIdPermission (UINT4 u4Ieee8021CfmMaComponentId,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              INT4 i4SetValMaCompIdPermission)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set SenderId permission to corresponding MA entry */
    pMaNode->u1IdPermission = (UINT1) i4SetValMaCompIdPermission;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaIdPermission, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                      i4SetValMaCompIdPermission));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaCompNumberOfVids 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValDot1agCfmMaCompNumberOfVids
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlSetMaCompNumberOfVids (UINT4 u4Ieee8021CfmMaComponentId,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4SetValIeee8021CfmMaCompNumberOfVids)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association for "
                     "given Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Set Number of Vids to corresponding MA entry */
    pMaNode->u2NumberOfVids = (UINT2) u4SetValIeee8021CfmMaCompNumberOfVids;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  EcfmUtlTestMaCompPriSelType 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompPrimarySelectorType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlTestMaCompPriSelType (UINT4 *pu4ErrorCode,
                             UINT4 u4Dot1agCfmMaComponentId,
                             UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex, INT4 i4MaCompPriSelType)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Dot1agCfmMaComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    if ((i4MaCompPriSelType != ECFM_SERVICE_SELECTION_ISID) &&
        (i4MaCompPriSelType != ECFM_SERVICE_SELECTION_VLAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Primary Selector Type for this MA\n");
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association with given"
                     " Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether Mhf creation criteria can be set */
    if (pMaNode->u1CompRowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Status already ACTIVE, "
                     " Cannot set MHF Criteria\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaCompPriSelOrNone 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompPrimaryVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTestv2MaCompPriSelOrNone (UINT4 *pu4ErrorCode,
                                 UINT4 u4Dot1agCfmMaComponentId,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 INT4 i4TestValDot1agCfmMaCompPrimaryVlanId)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Dot1agCfmMaComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    if (i4TestValDot1agCfmMaCompPrimaryVlanId < ECFM_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MHF Criteria for this MA\n");
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association with given"
                     " Indices\n");
        return SNMP_FAILURE;
    }

    if (pMaNode->i4PrimarySelType != ECFM_SERVICE_SELECTION_ISID)
    {
       if((i4TestValDot1agCfmMaCompPrimaryVlanId < ECFM_INIT_VAL) ||
                       (i4TestValDot1agCfmMaCompPrimaryVlanId > ECFM_VLANID_MAX))
       {
               *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
               ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MHF Criteria for this MA\n");
               return SNMP_FAILURE;
       }
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether Mhf creation criteria can be set */
    if (pMaNode->u1CompRowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Status already ACTIVE, "
                     " Cannot set MHF Criteria\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmUtlTstv2CfmMaCompMhfCreation 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTstv2CfmMaCompMhfCreation (UINT4 *pu4ErrorCode,
                                  UINT4 u4Ieee8021CfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 i4TestValIeee8021CfmMaCompMhfCreation)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /*  Validating Mhf creation criteria value */
    if ((i4TestValIeee8021CfmMaCompMhfCreation < ECFM_MHF_CRITERIA_NONE) ||
        (i4TestValIeee8021CfmMaCompMhfCreation > ECFM_MHF_CRITERIA_DEFER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MHF Criteria for this MA\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association with given"
                     " Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether Mhf creation criteria can be set */
    if (pMaNode->u1CompRowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Status already ACTIVE, "
                     " Cannot set MHF Criteria\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaCompIdPermission 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTestv2MaCompIdPermission (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021CfmMaComponentId,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 INT4 i4TestValIeee8021CfmMaCompIdPermission)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /*  validating SenderId permission value */
    if ((i4TestValIeee8021CfmMaCompIdPermission < ECFM_SENDER_ID_NONE) ||
        (i4TestValIeee8021CfmMaCompIdPermission > ECFM_SENDER_ID_DEFER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for sender Id Permission\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association with these"
                     "Indices\n");
        return SNMP_FAILURE;
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether SenderId permission value can be set */
    if (pMaNode->u1CompRowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Status is ACTIVE, "
                     " Cannot set Sender ID Permission\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaCompNumberOfVids 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompNumberOfVids
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
EcfmUtlTestv2MaCompNumberOfVids (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021CfmMaComponentId,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4TestValMaCompNumberOfVids)
{

    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    if (u4TestValMaCompNumberOfVids == ECFM_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for Number of Vids\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Maintenance Association with these"
                     "Indices\n");
        return SNMP_FAILURE;
    }

    /* MA entry corresponding to indices MdIndex, MaIndex exists */
    /* Check whether SenderId permission value can be set */
    if (pMaNode->u1CompRowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Row Status is ACTIVE, "
                     " Cannot set Number of VIDs\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestv2MaCompRowStatus 
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValDot1agCfmMaCompRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
EcfmUtlTestv2MaCompRowStatus (UINT4 *pu4ErrorCode,
                              UINT4 u4Ieee8021CfmMaComponentId,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              INT4 i4TestValIeee8021CfmMaCompRowStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmMaComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* validate index range */
    if ((u4Dot1agCfmMdIndex < ECFM_MD_INDEX_MIN) ||
        (u4Dot1agCfmMaIndex < ECFM_MA_INDEX_MIN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indices for MA\n");
        return SNMP_FAILURE;
    }

    /* Validate row status value */
    if ((i4TestValIeee8021CfmMaCompRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValIeee8021CfmMaCompRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for MA Row Status\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        /* MA entry corresponding to indices MdIndex, 
         * MaIndex does not exists user wants to change MA
         * entry's row status */
        if ((i4TestValIeee8021CfmMaCompRowStatus ==
             ECFM_ROW_STATUS_CREATE_AND_GO) ||
            (i4TestValIeee8021CfmMaCompRowStatus ==
             ECFM_ROW_STATUS_CREATE_AND_WAIT) ||
            (i4TestValIeee8021CfmMaCompRowStatus == ECFM_ROW_STATUS_DESTROY))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Value for MA Row Status\n");
            return SNMP_FAILURE;
        }
    }
    /* MA entry corresponding to indices MdIndex, MaIndex exists and its row
     * status is same as user wants to set */
    if (pMaNode->u1CompRowStatus == (UINT1) i4TestValIeee8021CfmMaCompRowStatus)
    {
        return SNMP_SUCCESS;
    }

    /* Check if user wants to delete MA entry */
    /* whether MA's deletion is possible or not */
    if (i4TestValIeee8021CfmMaCompRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaNode) == ECFM_TRUE)
        {
            /* Its not possible to delete MA directly,first all MEPs
             * belongs to this MA should be deleted
             */
            CLI_SET_ERR (CLI_ECFM_MA_DEL_MEP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:MEP Associated with MA, cannot delete MA\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/*************************************************************/
/*       Util Routines for Table: Dot1agCfmMaMepListTable.   */
/*************************************************************/

/****************************************************************************
 Function    :  EcfmUtlValAgMaMepListTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
PUBLIC INT1
EcfmUtlValdateMaMepListTable (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMaMepListIdentifier)
{
    tEcfmCcMepListInfo *pMaMepListNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MaMepList entry corresponding to MdIndex, MaIndex, MepId */
    pMaMepListNode =
        EcfmSnmpLwGetMaMepListEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                     u4Dot1agCfmMaMepListIdentifier);

    /* Check if MaMepList entry corresponding to MdIndex, MaIndex, 
       MepId exists or not */
    if (pMaMepListNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No MaMepList with given Indices\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetNxtIdxAgMaMepListTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier
                nextDot1agCfmMaMepListIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlGetNxtIdxMaMepListTable (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 *pu4NextDot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 *pu4NextDot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMaMepListIdentifier,
                                UINT4 *pu4NextDot1agCfmMaMepListIdentifier)
{
    tEcfmCcMepListInfo  MepListInfo;
    tEcfmCcMepListInfo *pMepListNextNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is ShutDown\n");
        return SNMP_FAILURE;
    }

    /* Get MaMepList entry corresponding to indices next to
     *  MdIndex, MaIndex, MepId */
    ECFM_MEMSET (&MepListInfo, ECFM_INIT_VAL, ECFM_CC_MEP_LIST_SIZE);
    MepListInfo.u4MdIndex = u4Dot1agCfmMdIndex;
    MepListInfo.u4MaIndex = u4Dot1agCfmMaIndex;
    MepListInfo.u2MepId = (UINT2) u4Dot1agCfmMaMepListIdentifier;
    pMepListNextNode = (tEcfmCcMepListInfo *) RBTreeGetNext
        (ECFM_CC_MA_MEP_LIST_TABLE, (tRBElem *) & MepListInfo, NULL);
    if (pMepListNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No MaMepList with given Indices\n");
        return SNMP_FAILURE;
    }
    /* MaMepList entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set indices from corresponding MA next entry */
    *pu4NextDot1agCfmMdIndex = pMepListNextNode->u4MdIndex;
    *pu4NextDot1agCfmMaIndex = pMepListNextNode->u4MaIndex;
    *pu4NextDot1agCfmMaMepListIdentifier = (UINT4) (pMepListNextNode->u2MepId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlGetAgMaMepListRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier

                The Object 
                retValDot1agCfmMaMepListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlGetMaMepListRowStatus (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMaMepListIdentifier,
                              INT4 *pi4RetValDot1agCfmMaMepListRowStatus)
{
    tEcfmCcMepListInfo *pMaMepListNode = NULL;

    /* Get MaMepList entry corresponding to MdIndex, MaIndex, MepId */
    pMaMepListNode = EcfmSnmpLwGetMaMepListEntry (u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  u4Dot1agCfmMaMepListIdentifier);
    if (pMaMepListNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No MaMepList with given Indices\n");
        return SNMP_FAILURE;
    }
    /* MaMepList entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the row status from corresponding MepList entry */
    *pi4RetValDot1agCfmMaMepListRowStatus =
        (INT4) (pMaMepListNode->u1RowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlSetMaMepListRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier

                The Object 
                setValDot1agCfmMaMepListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlSetMaMepListRowStatus (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMaMepListIdentifier,
                              INT4 i4SetValDot1agCfmMaMepListRowStatus)
{
    tEcfmCcMepListInfo *pMepListNode = NULL;
    tEcfmCcMepListInfo *pMepListNewNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MaMepList entry corresponding to MdIndex, MaIndex, MepId */
    pMepListNode = EcfmSnmpLwGetMaMepListEntry (u4Dot1agCfmMdIndex,
                                                u4Dot1agCfmMaIndex,
                                                (UINT2)
                                                u4Dot1agCfmMaMepListIdentifier);
    if (pMepListNode != NULL)
    {
        /* MaMepList entry corresponding to MdIndex, MaIndex, MepId exists and
         * its row status is same as user wants to set */
        if (pMepListNode->u1RowStatus ==
            (UINT1) i4SetValDot1agCfmMaMepListRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValDot1agCfmMaMepListRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MAMEPList Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }
    else if ((i4SetValDot1agCfmMaMepListRowStatus !=
              ECFM_ROW_STATUS_CREATE_AND_GO)
             && (i4SetValDot1agCfmMaMepListRowStatus !=
                 ECFM_ROW_STATUS_CREATE_AND_WAIT))

    {
        return SNMP_FAILURE;
    }
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaMepListRowStatus, u4SeqNum,
                          TRUE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    switch (i4SetValDot1agCfmMaMepListRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
            /* Check if this MEP-ID is a local MEP, if yes then we will have to
             * make the local MEP as notInService*/
            pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                                     u4Dot1agCfmMaIndex,
                                                     (UINT2)
                                                     u4Dot1agCfmMaMepListIdentifier);
            if (pMepNode != NULL)
            {
                if (nmhSetDot1agCfmMepRowStatus
                    (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                     u4Dot1agCfmMaMepListIdentifier,
                     ECFM_ROW_STATUS_NOT_IN_SERVICE) != SNMP_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "\tSNMP:Cannot Set row status "
                                 "NOT_IN_SERVICE for MEP \n");
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* Delete the entry of this Remote MEP from all the local MEPs */
                EcfmSnmpLwDeleteRMepInAllMeps (pMepListNode);
            }
            pMepListNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            break;
        case ECFM_ROW_STATUS_CREATE_AND_GO:
            /* createAndGo is same createAndWait and then Active */
            if (EcfmUtlSetMaMepListRowStatus
                (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                 u4Dot1agCfmMaMepListIdentifier,
                 ECFM_ROW_STATUS_CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP:Cannot set row status Create and Go for this "
                             "Ma-MepList\n");
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  u4Dot1agCfmMaIndex,
                                  u4Dot1agCfmMaMepListIdentifier,
                                  i4SetValDot1agCfmMaMepListRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

                return SNMP_FAILURE;
            }
            if (EcfmUtlSetMaMepListRowStatus
                (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                 u4Dot1agCfmMaMepListIdentifier,
                 ECFM_ROW_STATUS_ACTIVE) != SNMP_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP:Cannot set row status Create and "
                             "Go for this Ma-MepList\n");
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  u4Dot1agCfmMaIndex,
                                  u4Dot1agCfmMaMepListIdentifier,
                                  i4SetValDot1agCfmMaMepListRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

                return SNMP_FAILURE;
            }
            break;
        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
            /* Create node for MaMepList */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MA_MEP_LIST_TABLE (pMepListNewNode)
                == NULL)

            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "\tSNMP:Cannot allocate new node for this "
                             "MA-MepList\n");
                CLI_SET_ERR(CLI_ECFM_MAX_MEP_LIST_CREATION_REACHED);
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return SNMP_FAILURE;
            }
            ECFM_MEMSET (pMepListNewNode, ECFM_INIT_VAL, ECFM_CC_MEP_LIST_SIZE);
            /* put values in MaMepList new node */
            pMepListNewNode->u4MdIndex = u4Dot1agCfmMdIndex;
            pMepListNewNode->u4MaIndex = u4Dot1agCfmMaIndex;
            pMepListNewNode->u2MepId = (UINT2) u4Dot1agCfmMaMepListIdentifier;
            /* Put back pointer to its associated MA */
            pMepListNewNode->pMaInfo = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex,
                                                             u4Dot1agCfmMaIndex);

            if (pMepListNewNode->pMaInfo == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "\t MA information is not present while "
                             "Creating List Information\n");
                /*Deallocate memory assigned to new node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_MEP_LIST_TABLE_POOL,
                                     (UINT1 *) (pMepListNewNode));
                pMepListNewNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  u4Dot1agCfmMaIndex,
                                  u4Dot1agCfmMaMepListIdentifier,
                                  i4SetValDot1agCfmMaMepListRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* Add MepList new node in global MaMepList Table and in 
             * its associated MA */
            if ((EcfmSnmpLwAddMaMepListEntry (pMepListNewNode)) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "\tSNMP:Cannot allocate new node for this "
                             "MA-MepList\n");
                /*Deallocate memory assigned to new node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_MEP_LIST_TABLE_POOL,
                                     (UINT1 *) (pMepListNewNode));
                pMepListNewNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Dot1agCfmMdIndex,
                                  u4Dot1agCfmMaIndex,
                                  u4Dot1agCfmMaMepListIdentifier,
                                  i4SetValDot1agCfmMaMepListRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pMepListNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            break;
        case ECFM_ROW_STATUS_ACTIVE:
            /* Check if this MEP-ID is a local MEP, if yes then we will have to
             * make the local MEP as active*/
            pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                                     u4Dot1agCfmMaIndex,
                                                     (UINT2)
                                                     u4Dot1agCfmMaMepListIdentifier);
            if (pMepNode != NULL)
            {
                if (nmhSetDot1agCfmMepRowStatus
                    (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                     u4Dot1agCfmMaMepListIdentifier,
                     ECFM_ROW_STATUS_ACTIVE) != SNMP_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "\tSNMP:Cannot set Row Status ACTIVE for this "
                                 "MA-MepList\n");
                    return SNMP_FAILURE;
                }
            }
            /* check if it is remote MEP then create it */
            if (EcfmSnmpLwCreateRMepInAllMeps (pMepListNode) == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Creation of Remote MEP Failed\n");
                return SNMP_FAILURE;
            }
            /* Set MepList new node rowstatus */
            pMepListNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
            break;

        case ECFM_ROW_STATUS_DESTROY:
            /* Remove MepList node from MA's MepList and 
             * from MaMepListTable in Global info */
            EcfmSnmpLwDeleteMaMepListEntry (pMepListNode);
            /* Release memory allocated to MepList node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_MEP_LIST_TABLE_POOL,
                                 (UINT1 *) (pMepListNode));
            pMepListNode = NULL;
            break;
        default:
            break;
    }

    if (i4SetValDot1agCfmMaMepListRowStatus != ECFM_ROW_STATUS_CREATE_AND_GO)
    {
        /* Sending Trigger to MSR */

        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaMepListRowStatus,
                              u4SeqNum, TRUE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMaMepListIdentifier,
                          i4SetValDot1agCfmMaMepListRowStatus));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmUtlTestMaMepListRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMaMepListIdentifier

                The Object 
                testValDot1agCfmMaMepListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmUtlTestMaMepListRowStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMaMepListIdentifier,
                               INT4 i4TestValDot1agCfmMaMepListRowStatus)
{
    tEcfmCcMepListInfo *pMepListNode = NULL;
    tEcfmCcMdInfo      *pMdInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4NextDot1agCfmMdIndex = ECFM_INIT_VAL;
    UINT4               u4NextDot1agCfmMaIndex = ECFM_INIT_VAL;
    UINT4               u4NextDot1agCfmMaMepListIdentifier = ECFM_INIT_VAL;
    UINT4               u4NextDot1agCfmLocalMepIdentifier = ECFM_INIT_VAL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate index range */
    if (ECFM_VALIDATE_MA_MEPLIST_TABLE_INDICES (u4Dot1agCfmMdIndex,
                                                u4Dot1agCfmMaIndex,
                                                u4Dot1agCfmMaMepListIdentifier))
    {
        CLI_SET_ERR (CLI_ECFM_RMEP_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid Indices for MaMepList\n");
        return SNMP_FAILURE;
    }
    /* Check row status possible values */
    if ((i4TestValDot1agCfmMaMepListRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValDot1agCfmMaMepListRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Wrong value for Ma-MepList's Row Status\n");
        return SNMP_FAILURE;
    }
    /* Get MepList entry corresponding to MdIndex, MaIndex, MepId */
    pMepListNode = EcfmSnmpLwGetMaMepListEntry (u4Dot1agCfmMdIndex,
                                                u4Dot1agCfmMaIndex,
                                                (UINT2)
                                                u4Dot1agCfmMaMepListIdentifier);
    if (pMepListNode == NULL)
    {
        /* MepList entry corresponding to MdIndex, MaIndex, MepId does not exists 
         * user wants to change its row status */

        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEPList Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4TestValDot1agCfmMaMepListRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }

        else if ((i4TestValDot1agCfmMaMepListRowStatus !=
                  ECFM_ROW_STATUS_CREATE_AND_GO)
                 && (i4TestValDot1agCfmMaMepListRowStatus !=
                     ECFM_ROW_STATUS_CREATE_AND_WAIT))

        {
            CLI_SET_ERR (CLI_ECFM_RMEP_NOT_PRESENT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:No MA-MepList exists with given Indices\n");
            return SNMP_FAILURE;
        }
        /* MepList entry corresponding to MdIndex, MaIndex, MepId does not exists 
         * user wants to create new entry */
        pMaInfo = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
        pMdInfo = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

        /* Check for its associated MD and MA's row status should be ACTIVE */
        if ((pMdInfo == NULL) || (pMaInfo == NULL))
        {
            CLI_SET_ERR (CLI_ECFM_RMEP_NOT_PRESENT_ERR);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Associated MD, MA row status is not ACTIVE\n");
            return SNMP_FAILURE;
        }
        if ((pMdInfo->u1RowStatus != ECFM_ROW_STATUS_ACTIVE) ||
            (pMaInfo->u1RowStatus != ECFM_ROW_STATUS_ACTIVE) ||
            (pMaInfo->u1CompRowStatus != ECFM_ROW_STATUS_ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Cannot set Row Status ACTIVE for this MA-MepList\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    /* MepList entry corresponding to MdIndex, MaIndex, MepId already exists 
     * user wants to create it */
    if ((i4TestValDot1agCfmMaMepListRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO)
        || (i4TestValDot1agCfmMaMepListRowStatus ==
            ECFM_ROW_STATUS_CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_ECFM_MEPLIST_CONFIG_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:MA-MepList with given indices already exists\n");
        return SNMP_FAILURE;
    }
    /* MepList entry corresponding to MdIndex, MaIndex, MepId  exists 
     * its row status is same as user wants to set */
    if (pMepListNode->u1RowStatus ==
        (UINT1) i4TestValDot1agCfmMaMepListRowStatus)

    {
        return SNMP_SUCCESS;
    }
    /* If user wants to delete it */
    if (i4TestValDot1agCfmMaMepListRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        /* In Mplstp oam do not allow to delete the Mep from MaMepList if it is 
         * associated with a local Mep.
         */
        pMaInfo = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
        if (pMaInfo != NULL)
        {
            if ((pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
                (pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
            {
                if (EcfmSnmpLwGetNextIndexFsY1731MplstpExtRemoteMepTable
                    (u4Dot1agCfmMdIndex, &u4NextDot1agCfmMdIndex,
                     u4Dot1agCfmMaIndex, &u4NextDot1agCfmMaIndex,
                     ECFM_INIT_VAL, &u4NextDot1agCfmLocalMepIdentifier,
                     u4Dot1agCfmMaMepListIdentifier,
                     &u4NextDot1agCfmMaMepListIdentifier) == ECFM_SUCCESS)
                {
                    if ((u4Dot1agCfmMdIndex == u4NextDot1agCfmMdIndex) &&
                        (u4Dot1agCfmMaIndex == u4NextDot1agCfmMaIndex) &&
                        (u4Dot1agCfmMaMepListIdentifier ==
                         u4NextDot1agCfmMaMepListIdentifier) &&
                        (u4NextDot1agCfmLocalMepIdentifier != ECFM_INIT_VAL))
                    {
                        CLI_SET_ERR (CLI_ECFM_RMEP_IS_ASSOCIATED_TO_MEP);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "\tSNMP: Mep is associated as a Remote "
                                     "Mep for another Local Mep\n");
                        return SNMP_FAILURE;
                    }
                }
            }
        }
        /* Check if it locally configured MEP, then it cannot be deleted */
        pMepNode =
            EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          (UINT2)
                                          u4Dot1agCfmMaMepListIdentifier);
        if ((pMepNode != NULL) && (pMepNode->pPortInfo != NULL))
        {
            CLI_SET_ERR (CLI_ECFM_MEPLIST_DEL_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Mep ID in MA-MepList is configured Locally, "
                         "Cannot delete MA-MepList Entry\n");
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/******************************************************************************/
/*                           End  of file cfmmautil.c                           */
/******************************************************************************/
