/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlminr.c,v 1.13 2016/07/25 07:25:12 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way
 *              Loss Measurement module 
 *******************************************************************/
#include "cfminc.h"

/*******************************************************************************
 * Function Name      : EcfmCcClntParserLmr
 *
 * Description        : This is called to parse the received LMR PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC VOID
EcfmCcClntParseLmr (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmCcRxLmrPduInfo *pLmrPduInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pLmrPduInfo = &(pPduSmInfo->uPduInfo.Lmr);
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;
    /* Copy the Counter values:
     * TxFCf
     * RxFCf
     * TxFCb
     */
    ECFM_GET_4BYTE (pLmrPduInfo->u4TxFCf, pu1Pdu);
    ECFM_GET_4BYTE (pLmrPduInfo->u4RxFCf, pu1Pdu);
    ECFM_GET_4BYTE (pLmrPduInfo->u4TxFCb, pu1Pdu);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcClntProcessLmr
 *
 * Description        : This is called to process the received LMR PDU 
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcClntProcessLmr (tEcfmCcPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdLmr)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };

    ECFM_CC_TRC_FN_ENTRY ();

    /* Validate the recieved PDU */

    /* Check that First TLV Offset value received in the LBR PDU should be
     * greater or equal to 4 
     */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_LMR_FIRST_TLV_OFFSET)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParserLmr: LMR frame is discarded as the"
                     " first tlv offset is wrong.\r\n");
        ECFM_CC_INCR_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->u2PortNum);
        ECFM_CC_INCR_CTX_RX_BAD_CFM_PDU_COUNT
            (pPduSmInfo->pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }

    if ((pPduSmInfo->pMepInfo == NULL) ||
        (pPduSmInfo->pMepInfo->pEcfmMplsParams == NULL))
    {
        /* Get the MEP's MAC Address */
        ECFM_GET_MAC_ADDR_OF_PORT (pPduSmInfo->pPortInfo->u4IfIndex,
                                   MepMacAddr);
        /* Check if the Mac Address received in the LMR is same as that of the
         * receiving MEP
         */
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                                   MepMacAddr) != ECFM_SUCCESS)
        {
            /* Discard the LMR frame */
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessLmr: "
                         "discarding the received LMR frame\r\n");
            if (ECFM_CC_IS_MHF (pPduSmInfo->pStackInfo))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntProcessLmr:"
                             "Lmr forwarded in case of MIP\r\n");
                *pbFrwdLmr = ECFM_TRUE;
            }

            return ECFM_FAILURE;
        }
        if (ECFM_CC_IS_MHF (pPduSmInfo->pStackInfo))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessLmr:"
                         "Lmr discarded in case destined to  MIP\r\n");
            return ECFM_FAILURE;
        }
    }
    else
    {
        if (ECFM_CC_IS_MEP_ACTIVE (pPduSmInfo->pMepInfo) == ECFM_FALSE)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessLmr:"
                         "Lmr discarded in case MEP is not active\r\n");
            return ECFM_FAILURE;
        }
    }

    pMepInfo = pPduSmInfo->pMepInfo;
    if (pMepInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessLmr:"
                     "Lmr discarded in case MEP is not found\r\n");
        return ECFM_FAILURE;
    }

    /* Check the state of LM Transaction */
    if (pMepInfo->LmInfo.u1TxLmmStatus != ECFM_TX_STATUS_NOT_READY)
    {
        /* Discard the LMR frame */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParserLmr: "
                     "discarding the received LMR frame, there is no on going"
                     " LM transaction for this MEP \r\n");
        return ECFM_FAILURE;
    }
    /* Calculate the Frame Loss based upon the received PDU */
    if (EcfmCcClntCalcFrameLoss (pPduSmInfo, ECFM_CC_LM_TYPE_1LM) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParserLmr: "
                     "Error in calculating Frame Loss\r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcClntCalcFrameLoss
 *
 * Description        : This routine is called to calculate frame loss and store 
 *                      the result in Frame Loss Buffer.
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      u1LossMeasurementType - Type of Loss Measurement
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC UINT4
EcfmCcClntCalcFrameLoss (tEcfmCcPduSmInfo * pPduSmInfo,
                         UINT1 u1LossMeasurementType)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcRxCcmPduInfo *pCcPduInfo = NULL;
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    tEcfmCcRxLmrPduInfo *pLmrPduInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmMepInfoParams  MepInfo;
    UINT4               u4RxFCl = ECFM_INIT_VAL;
    UINT4               u4TxFCl = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT4               u4RemainingTicks = ECFM_INIT_VAL;
    UINT4               u4TimeLeft = ECFM_INIT_VAL;
    UINT4               u4LmmInterval = ECFM_INIT_VAL;
    UINT4               u4TotalTxCnt = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    tUtlSysPreciseTime  SysPreciseTime;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    pMepInfo = pPduSmInfo->pMepInfo;
    pLmrPduInfo = &(pPduSmInfo->uPduInfo.Lmr);
    pCcPduInfo = &(pPduSmInfo->uPduInfo.Ccm);
    pLmInfo = &(pMepInfo->LmInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pMepInfo->LmInfo.u1LossMeasurementType = u1LossMeasurementType;
    if (u1LossMeasurementType == ECFM_CC_LM_TYPE_1LM)
    {
        ECFM_MEMCPY (pMepInfo->LmInfo.TxLmmDestMacAddr,
                     pPduSmInfo->RxSrcMacAddr, ECFM_MAC_ADDR_LENGTH);
    }
    else
    {
        ECFM_MEMCPY (pMepInfo->CcInfo.UnicastCcmMacAddr,
                     pPduSmInfo->RxSrcMacAddr, ECFM_MAC_ADDR_LENGTH);
    }
    ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4Interval);
    ECFM_GET_LMM_INTERVAL (pLmInfo->u2TxLmmInterval, u4LmmInterval);
    if (pMaInfo->u4SeqNum == ECFM_INIT_VAL)
    {
        pLmInfo->u4PreTxFCf = pLmrPduInfo->u4TxFCf;
        pLmInfo->u4PreRxFCf = pLmrPduInfo->u4RxFCf;
        pLmInfo->u4PreTxFCb = pLmrPduInfo->u4TxFCb;
        /* This is the first LMR received for this transaction.
         * Form a node and save the information in the Frame Loss Buffer
         */
        pFrmLossBuffNode = EcfmLmInitAddLmEntry (pMepInfo);
        if (pFrmLossBuffNode == NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntCalcFrameLoss: "
                         "Failure adding node in the frame loss buffer\r\n");
        }
        else
        {
            UtlGetPreciseSysTime (&SysPreciseTime);
            pFrmLossBuffNode->RxTimeStamp = SysPreciseTime.u4Sec;
            pFrmLossBuffNode->u4NearEndLoss = ECFM_INIT_VAL;
            pFrmLossBuffNode->u4FarEndLoss = ECFM_INIT_VAL;
            /* Store the value of counters for frame loss calculation */
            if (u1LossMeasurementType == ECFM_CC_LM_TYPE_1LM)
            {
                /* The measurement time is the difference of time between the 
                 * two received messages for Frame Loss 
                 */
                if (ECFM_GET_REMAINING_TIME (ECFM_CC_TMRLIST_ID,
                                             &(pLmInfo->LmmInitWhileTimer.
                                               TimerNode),
                                             &u4RemainingTicks) != ECFM_SUCCESS)
                {
                    u4RemainingTicks = ECFM_INIT_VAL;
                }
                u4TimeLeft = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4RemainingTicks);
                pFrmLossBuffNode->u4MeasurementInterval =
                    u4LmmInterval - u4TimeLeft;

            }
            else
            {
                /* The measurement time is the difference of time between the
                 * two received messages for Frame Loss
                 */
                if (ECFM_GET_REMAINING_TIME (ECFM_CC_TMRLIST_ID,
                                             &(pMepInfo->CcInfo.
                                               CciWhileTimer.TimerNode),
                                             &u4RemainingTicks) != ECFM_SUCCESS)
                {
                    u4RemainingTicks = ECFM_INIT_VAL;
                }
                u4TimeLeft = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4RemainingTicks);
                pFrmLossBuffNode->u4MeasurementInterval =
                    u4Interval - u4TimeLeft;

            }
        }
        if (pMepInfo->pEcfmMplsParams != NULL)
        {
            if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
            {
                EcfmGetMplsPktCnt (pMepInfo, &u4TxFCl, &u4RxFCl);
            }
        }
        else
        {
            EcfmGetPacketCounters (pMepInfo->pPortInfo->u4IfIndex,
                                   pMepInfo->u4PrimaryVidIsid, &u4TxFCl,
                                   &u4RxFCl);

        }
        pLmInfo->u4PreRxFCl = u4RxFCl;
    }
    else
    {
        /* Allocate a node in the Frame Loss Buffer */
        pFrmLossBuffNode = EcfmLmInitAddLmEntry (pMepInfo);
        if (pMepInfo->pEcfmMplsParams != NULL)
        {
            if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
            {
                EcfmGetMplsPktCnt (pMepInfo, &u4TxFCl, &u4RxFCl);
            }
        }
        else
        {
            EcfmGetPacketCounters (pMepInfo->pPortInfo->u4IfIndex,
                                   pMepInfo->u4PrimaryVidIsid, &u4TxFCl,
                                   &u4RxFCl);

        }
        if (pFrmLossBuffNode == NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntCalcFrameLoss: "
                         "Failure adding node in the frame loss buffer\r\n");
        }
        else
        {
            UtlGetPreciseSysTime (&SysPreciseTime);
            pFrmLossBuffNode->RxTimeStamp = SysPreciseTime.u4Sec;
            /* Calculate Far end loss and near end loss */
            if (u1LossMeasurementType == ECFM_CC_LM_TYPE_1LM)
            {
                ECFM_CALCULATE_FRAME_LOSS (pLmrPduInfo->u4TxFCf,
                                           pLmInfo->u4PreTxFCf,
                                           pLmrPduInfo->u4RxFCf,
                                           pLmInfo->u4PreRxFCf,
                                           pFrmLossBuffNode->u4FarEndLoss);
                ECFM_CALCULATE_FRAME_LOSS (pLmrPduInfo->u4TxFCb,
                                           pLmInfo->u4PreTxFCb, u4RxFCl,
                                           pLmInfo->u4PreRxFCl,
                                           pFrmLossBuffNode->u4NearEndLoss);
                /* If LM is initiated by Availability,
                 * Perform Availability calculation 
                 */
                if (pLmInfo->b1TxLmByAvlbility == ECFM_TRUE)
                {

                    if (pLmrPduInfo->u4TxFCf > pLmInfo->u4PreTxFCf)
                    {
                        u4TotalTxCnt =
                            pLmrPduInfo->u4TxFCf - pLmInfo->u4PreTxFCf;
                    }
                    else
                    {
                        u4TotalTxCnt =
                            pLmInfo->u4PreTxFCf - pLmrPduInfo->u4TxFCf;
                    }

                    pFrmLossBuffNode->f4FarEndFrmLossRatio = ECFM_INIT_VAL;
                    if (u4TotalTxCnt != 0)
                    {
                        pFrmLossBuffNode->f4FarEndFrmLossRatio =
                            (pFrmLossBuffNode->u4FarEndLoss *
                             ECFM_FLT_VAL_100) / u4TotalTxCnt;
                    }
                    EcfmCcMeasureAvlblty (pPduSmInfo,
                                          pFrmLossBuffNode->
                                          f4FarEndFrmLossRatio);

                }
                /* The frame loss info has been updated. 
                 * Update the local values so that Frame Loss can be calculated 
                 * the next time. 
                 */
                pLmInfo->u4PreTxFCf = pLmrPduInfo->u4TxFCf;
                pLmInfo->u4PreRxFCf = pLmrPduInfo->u4RxFCf;
                pLmInfo->u4PreTxFCb = pLmrPduInfo->u4TxFCb;
                pLmInfo->u4PreRxFCl = u4RxFCl;
            }
            else
            {
                ECFM_CALCULATE_FRAME_LOSS (pCcPduInfo->u4TxFCb,
                                           pLmInfo->u4PreTxFCb,
                                           pCcPduInfo->u4RxFCb,
                                           pLmInfo->u4PreRxFCb,
                                           pFrmLossBuffNode->u4FarEndLoss);
                ECFM_CALCULATE_FRAME_LOSS (pCcPduInfo->u4TxFCf,
                                           pLmInfo->u4PreTxFCf, u4RxFCl,
                                           pLmInfo->u4PreRxFCl,
                                           pFrmLossBuffNode->u4NearEndLoss);
                /* The frame loss info has been updated. 
                 * Update the local values so that Frame Loss can be calculated 
                 * the next time. 
                 */
                pLmInfo->u4PreTxFCb = pCcPduInfo->u4TxFCb;
                pLmInfo->u4PreRxFCb = pCcPduInfo->u4RxFCb;
                pLmInfo->u4PreTxFCf = pCcPduInfo->u4TxFCf;
                pLmInfo->u4PreRxFCl = u4RxFCl;
            }

            if (pLmInfo->u1LossMeasurementType == ECFM_CC_LM_TYPE_1LM)
            {
                /* The measurement time is the difference of time between the 
                 * two received messages for Frame Loss 
                 */
                if (ECFM_GET_REMAINING_TIME (ECFM_CC_TMRLIST_ID,
                                             &(pLmInfo->LmmInitWhileTimer.
                                               TimerNode),
                                             &u4RemainingTicks) != ECFM_SUCCESS)
                {
                    u4RemainingTicks = ECFM_INIT_VAL;
                }
                u4TimeLeft = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4RemainingTicks);
                pFrmLossBuffNode->u4MeasurementInterval =
                    u4LmmInterval - u4TimeLeft;
            }
            else
            {
                /* The measurement time is the difference of time between the
                 * two received messages for Frame Loss
                 */
                if (ECFM_GET_REMAINING_TIME (ECFM_CC_TMRLIST_ID,
                                             &(pMepInfo->CcInfo.
                                               CciWhileTimer.TimerNode),
                                             &u4RemainingTicks) != ECFM_SUCCESS)
                {
                    u4RemainingTicks = ECFM_INIT_VAL;
                }
                u4TimeLeft = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4RemainingTicks);
                pFrmLossBuffNode->u4MeasurementInterval =
                    u4Interval - u4TimeLeft;
            }

            if (pLmInfo->b1TxLmByAvlbility != ECFM_TRUE)
            {
                /* If either of Near end or far end loss exceeds the configured 
                 * threshold, send notification to the registered applications 
                 */
                if ((pFrmLossBuffNode->u4NearEndLoss >
                     pLmInfo->u4NearEndFrmLossThreshold) ||
                    (pFrmLossBuffNode->u4FarEndLoss >
                     pLmInfo->u4FarEndFrmLossThreshold))
                {

                    Y1731_CC_GENERATE_TRAP (pFrmLossBuffNode,
                                            Y1731_FRM_LOSS_TRAP_VAL);
                    MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
                    MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;
                    MepInfo.u4IfIndex =
                        ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                              pTempPortInfo);
                    MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
                    MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
                    MepInfo.u2MepId = pMepInfo->u2MepId;
                    MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
                    MepInfo.u1Direction = pMepInfo->u1Direction;

                    ECFM_NOTIFY_PROTOCOLS (ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD,
                                           &MepInfo, NULL,
                                           ECFM_CC_MAX_VAL (pFrmLossBuffNode->
                                                            u4FarEndLoss,
                                                            pFrmLossBuffNode->
                                                            u4NearEndLoss),
                                           ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                           ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                           ECFM_CC_TASK_ID);

                    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
                }
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLmInitAddLmEntry
 *
 * Description        : This routine is used to add the entry of the transmitted
 *                      LMM frame in case of 100% frame loss and of received 
 *                      LMR frame otherwise.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *                        
 * Output(s)          : None
 *
 * Returns            : tEcfmCcFrmLossBuff * - Pointer to the node added
 ******************************************************************************/
PUBLIC tEcfmCcFrmLossBuff *
EcfmLmInitAddLmEntry (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    tEcfmCcFrmLossBuff *pFrmLossBuffNodeTmp = NULL;
    tEcfmCcFrmLossBuff *pFirstFrmLossBuffEntry = NULL;
    ECFM_CC_TRC_FN_ENTRY ();
    /* Allocate a memory block for frame loss buffer node */
    ECFM_ALLOC_MEM_BLOCK_CC_FL_BUFF_TABLE (pFrmLossBuffNode);

    /* Increment the locally maintained Sequence number */
    ECFM_CC_INCR_LM_SEQ_NUM (pMepInfo);
    while (pFrmLossBuffNode == NULL)
    {
        /* Memory allocation failed, so now check if the memory pool is 
         * exhausted
         */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmInitAddLmEntry: "
                     "failure allocating memory for frame loss buffer node\r\n");
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_CC_FRM_LOSS_POOL) != ECFM_INIT_VAL)
        {
            /* memory failure failure has occured even when we have free 
             * memory in the pool
             */
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmLmInitAddLmEntry: "
                         "Frame Loss memory pool corruption\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return NULL;
        }
        /* Memory pool is exhausted for sure, now we can delete the first entry
         * in the pool to have some free memory
         */
        pFirstFrmLossBuffEntry = (tEcfmCcFrmLossBuff *)
            RBTreeGetFirst (ECFM_CC_FL_BUFFER_TABLE);
        if (pFirstFrmLossBuffEntry == NULL)
        {
            /* Memory pool is exhausted and we dont have anyting in the frame
             * loss table to free.*/
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmLmInitAddLmEntry: "
                         "Frame loss buffer table corruption \r\n");
            return NULL;
        }
        /* Remove the first node from the table */
        RBTreeRem (ECFM_CC_FL_BUFFER_TABLE, pFirstFrmLossBuffEntry);
        /* Free the memory for the node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_FRM_LOSS_POOL,
                             (UINT1 *) pFirstFrmLossBuffEntry);
        pFirstFrmLossBuffEntry = NULL;
        /* Now we can again try for allocating memory */
        ECFM_ALLOC_MEM_BLOCK_CC_FL_BUFF_TABLE (pFrmLossBuffNode);
    }
    ECFM_MEMSET (pFrmLossBuffNode, ECFM_INIT_VAL, sizeof (tEcfmCcFrmLossBuff));
    /* Fill up the index in the frame loss buffer node */
    pFrmLossBuffNode->u4MdIndex = pMepInfo->u4MdIndex;
    pFrmLossBuffNode->u4MaIndex = pMepInfo->u4MaIndex;
    pFrmLossBuffNode->u2MepId = pMepInfo->u2MepId;
    pFrmLossBuffNode->u1LossMeasurementType =
        pMepInfo->LmInfo.u1LossMeasurementType;
    if (pFrmLossBuffNode->u1LossMeasurementType == ECFM_CC_LM_TYPE_1LM)
    {
        ECFM_MEMCPY (pFrmLossBuffNode->PeerMepMacAddress,
                     pMepInfo->LmInfo.TxLmmDestMacAddr, ECFM_MAC_ADDR_LENGTH);
    }
    else
    {
        ECFM_MEMCPY (pFrmLossBuffNode->PeerMepMacAddress,
                     pMepInfo->CcInfo.UnicastCcmMacAddr, ECFM_MAC_ADDR_LENGTH);
    }

    pFrmLossBuffNode->u4TransId = pMepInfo->pMaInfo->u4TransId;
    pFrmLossBuffNode->u4SeqNum = pMepInfo->pMaInfo->u4SeqNum;

    /* Update the Number of LMM sent counter for Single Ended Transaction */
    if (pMepInfo->LmInfo.u1LossMeasurementType == ECFM_CC_LM_TYPE_1LM)
    {
        /* Copy Sequence no to LMM Sent counter */
        pFrmLossBuffNode->u2NoOfLMMSent = pFrmLossBuffNode->u4SeqNum;

        if (pFrmLossBuffNode->u4SeqNum == 1)
        {
            /* Initializing LmrIn for the new transaction */
            pMepInfo->LmInfo.u2NoOfLmrIn = ECFM_INIT_VAL;
        }
    }

    /* Update the Number of LMR received counter */
    if (EcfmCcGetLocDefectFromRmep (pMepInfo, pMepInfo->LmInfo.u2TxLmDestMepId)
        != ECFM_SUCCESS)
    {
        ECFM_CC_INCR_LMR_RCVD_COUNT (pMepInfo);
    }
    pFrmLossBuffNodeTmp = pFrmLossBuffNode;
    /* Traverse all the nodes after this node in the current transaction & 
     * update the number of LMR received counter. 
     * This is done so to handle the case of Wrapping */
    do
    {
        pFrmLossBuffNodeTmp->u2NoOfLMRRcvd = pMepInfo->LmInfo.u2NoOfLmrIn;
    }
    while ((pFrmLossBuffNodeTmp =
            EcfmGetNextFrmLossNodeForATrans (pFrmLossBuffNodeTmp)) != NULL);

    /* Add the node into the table */
    if (RBTreeAdd (ECFM_CC_FL_BUFFER_TABLE, pFrmLossBuffNode) !=
        ECFM_RB_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmInitAddLmEntry: "
                     "failure adding node into the frame loss table \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_FRM_LOSS_POOL, (UINT1 *) pFrmLossBuffNode);
        return NULL;
    }
    /* Store the current LM type (1LM/2LM) into the frame loss buffer */
    ECFM_CC_TRC_FN_EXIT ();
    return pFrmLossBuffNode;
}
