/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccoff.c,v 1.87 2016/01/13 11:45:04 siva Exp $
 *
 * Description: This file contains the functionality of the routine
 *                        which is the entry point to the CC Task
 *******************************************************************/

#include "cfminc.h"

#ifndef NPAPI_WANTED
#include "cfmoffnp.h"
#endif
/* This is used for having a unique TX/RX Handle for MEPs and RMEPs. 
 * It is assumed that this variable will never reach to maximum (UINT4 MAX). 
 * So its wrap around is not handled. */
PRIVATE UINT4       gu4MepTxRxHandle = ECFM_INIT_VAL;
 /******************************************************************************
 * Function Name      : EcfmCcmOffStatus   
 *
 * Description        : This Routine will enable or disable the offload
 *                      engine in hardware.
 *                      
 * Input(s)           : i4EcfmOffloadStatus - The offloading status to be
 *                         set
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffStatus (INT4 i4EcfmOffloadStatus)
{
    tEcfmHwParams       EcfmHwInfo;
    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));

    EcfmHwInfo.u1EcfmOffStatus = i4EcfmOffloadStatus;

#ifdef NPAPI_WANTED
    if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_OFFLOAD_STATE,
                                 &EcfmHwInfo) != FNP_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "OFFLOAD: Function "
                     "EcfmCcmOffStatus returns FAILURE" "\r\n");

        return ECFM_FAILURE;
    }
#endif
    return ECFM_SUCCESS;
}

 /******************************************************************************
 * Function Name      : EcfmCcmOffCreateTxRxForMep   
 *
 * Description        : This Routine will Pass the Information for CCM 
 *                      Reception & Transmittion for the MEP to Offloaded Module. 
 *                      
 * Input(s)           : pMepNode   - Pinter to Structure For Mep Info 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS.
 *                      ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffCreateTxRxForMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;

    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    if (pPortInfo == NULL)
    {
        /*MEP is not yet configured on any port */
        return ECFM_SUCCESS;
    }
#ifdef NPAPI_WANTED
    if ((pMepNode->u1Direction == ECFM_MP_DIR_UP) &&
        (ECFM_HW_UPMEP_OFFLOAD_SUPPORT () == ECFM_FALSE))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "OFFLOAD: Function "
                     "UP MEP Offloading is Not Supported in target" "\r\n");
        pMepNode->b1MepCcmOffloadStatus = ECFM_FALSE;
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
        return ECFM_FAILURE;
    }
#endif

    if (EcfmCcmOffCreateTxMep (pMepNode) == ECFM_FAILURE)
    {
        ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC,
                          "\n OFFLOAD: Function EcfmCcmOffCreateTxMep has"
                          "Returned Failure For MEPID %d on Port %d\r\n",
                          pMepNode->u2MepId, pMepNode->pPortInfo->u4IfIndex);
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
        pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
        return ECFM_FAILURE;
    }
    if (EcfmCcmOffCreateRxForAllRMep (pMepNode) == ECFM_FAILURE)
    {
        ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC,
                          "\n OFFLOAD: Function "
                          "EcfmCcmOffCreateRxForAllRMep has"
                          "Returned Failure For MEPID %d on"
                          "Port %d\r\n",
                          pMepNode->u2MepId, pMepNode->pPortInfo->u4IfIndex);
        EcfmCcmOffDeleteTxRxForMep (pMepNode);
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
        pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
        return ECFM_FAILURE;
    }
    if (pMepNode->u2OffloadMepTxHandle != ECFM_INIT_VAL)
    {
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_TRUE;
    }
    else
    {
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
    }
    pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffDeleteTxRxForMep   
 *
 * Description        : This Routine will Delete the Reception & Transmittion
 *                      Information from the Offloaded Module for the MEP. 
 *                      
 * Input(s)           : pMepNode   - Pinter to Structure For Mep Info 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcmOffDeleteTxRxForMep (tEcfmCcMepInfo * pMepNode)
{
    UINT1              *pPortList = NULL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcOffPortStats EcfmCcOffPortStats;

    ECFM_MEMSET (&EcfmCcOffPortStats, ECFM_INIT_VAL,
                 sizeof (tEcfmCcOffPortStats));
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcmOffDeleteTxRxForMep: Error in allocating memory "
                     "for pPortList\r\n");
        return;
    }

    ECFM_MEMSET (pPortList, ECFM_INIT_VAL, sizeof (tLocalPortListExt));

    if (EcfmCcmOffDeleteTxMep (pMepNode) == ECFM_FAILURE)
    {
        ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC,
                          "\n OFFLOAD: Function EcfmCcmOffDeleteTxMep has"
                          "Returned Failure For MEPID %d on Port %d\r\n",
                          pMepNode->u2MepId, pMepNode->pPortInfo->u4IfIndex);
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
    }

    EcfmCcmOffDeleteRxForAllRMep (pMepNode);

    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    u4ContextId = pPortInfo->u4ContextId;

    if (pPortInfo->u4IfIndex < ECFM_CC_MAX_MEP_INFO)
    {
        if (pMepNode->u1Direction == ECFM_MP_DIR_DOWN)
        {
            if (EcfmGetPortCcStats
                (u4ContextId, pPortInfo->u4IfIndex,
                 &EcfmCcOffPortStats) == ECFM_FAILURE)
            {
                UtilPlstReleaseLocalPortList (pPortList);
                return;
            }
            pPortInfo->u4TxCfmPduCount =
                pPortInfo->u4TxCfmPduCount + EcfmCcOffPortStats.u4NoOfCcTx;
            pPortInfo->u4TxCcmCount =
                pPortInfo->u4TxCcmCount + EcfmCcOffPortStats.u4NoOfCcTx;
            pPortInfo->u4RxCcmCount =
                pPortInfo->u4RxCcmCount + EcfmCcOffPortStats.u4NoOfCcRx;
            pPortInfo->u4RxCfmPduCount =
                pPortInfo->u4RxCfmPduCount + EcfmCcOffPortStats.u4NoOfCcRx;
            pPortInfo->u4FrwdCfmPduCount =
                pPortInfo->u4FrwdCfmPduCount + EcfmCcOffPortStats.u4NoOfCcFw;
        }
        else
        {
            if (EcfmL2IwfMiGetVlanEgressPorts (ECFM_CC_CURR_CONTEXT_ID (),
                                               pMepNode->u4PrimaryVidIsid,
                                               pPortList) == ECFM_SUCCESS)
            {
                for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
                     u2ByteInd++)
                {
                    if (pPortList[u2ByteInd] == 0)
                    {
                        continue;
                    }
                    u1PortFlag = pPortList[u2ByteInd];
                    for (u2BitIndex = 0;
                         ((u2BitIndex < BITS_PER_BYTE)
                          && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex)
                              != 0)); u2BitIndex++)
                    {
                        u2Port =
                            (u2ByteInd * BITS_PER_BYTE) +
                            EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);

                        pPortInfo = NULL;
                        pPortInfo = ECFM_CC_GET_PORT_INFO (u2Port);
                        if (pPortInfo != NULL)
                        {
                            ECFM_MEMSET (&EcfmCcOffPortStats, ECFM_INIT_VAL,
                                         sizeof (tEcfmCcOffPortStats));
                            if (EcfmGetPortCcStats
                                (u4ContextId, pPortInfo->u4PhyPortNum,
                                 &EcfmCcOffPortStats) == ECFM_FAILURE)
                            {
                                UtilPlstReleaseLocalPortList (pPortList);
                                return;
                            }
                            pPortInfo->u4TxCfmPduCount =
                                pPortInfo->u4TxCfmPduCount +
                                EcfmCcOffPortStats.u4NoOfCcTx;
                            pPortInfo->u4TxCcmCount =
                                pPortInfo->u4TxCcmCount +
                                EcfmCcOffPortStats.u4NoOfCcTx;
                            pPortInfo->u4RxCcmCount =
                                pPortInfo->u4RxCcmCount +
                                EcfmCcOffPortStats.u4NoOfCcRx;
                            pPortInfo->u4RxCfmPduCount =
                                pPortInfo->u4RxCfmPduCount +
                                EcfmCcOffPortStats.u4NoOfCcRx;
                            pPortInfo->u4FrwdCfmPduCount =
                                pPortInfo->u4FrwdCfmPduCount +
                                EcfmCcOffPortStats.u4NoOfCcFw;
                        }
                    }
                }
            }
        }
    }
    pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
    UtilPlstReleaseLocalPortList (pPortList);
    return;

}

/******************************************************************************
 * Function Name      : EcfmCcmOffCreateTxMep   
 *
 * Description        : This Routine will Create the CCM PDU pass the Information 
 *                      to offloaded Module for Transmittion. 
 *                      
 * Input(s)           : pMepNode   - Pinter to Structure For Mep Info 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS.
 *                      ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffCreateTxMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pCcMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;    /* pointer to CRU buffer */
    tVlanTagInfo        VlanInfo;
    tIsidTagInfo        IsidInfo;
    tEcfmTagType        EcfmTagType = ECFM_VLAN_TAG_INFO;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmMplstpCcOffTxInfo MplsCcOffTxParams;

    UINT2               u2PduSize = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT1              *pu1EthLlcHdr = NULL;    /* Pointer for filling the LLC Hdr */
    UINT1              *pu1PduStart = NULL;    /* pointer containing Start of PDU */
    UINT1              *pu1PduEnd = NULL;    /* pointer containing End of PDU */
    UINT1               u1PduLength = ECFM_INIT_VAL;
    UINT1               au1MplsDestMac[MAC_ADDR_LEN];
    UINT4               u4MplsOutIfIndex = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    if (pMepNode == NULL)
    {
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (&VlanInfo, ECFM_INIT_VAL, sizeof (tVlanTagInfo));
    ECFM_MEMSET (&IsidInfo, ECFM_INIT_VAL, sizeof (tIsidTagInfo));
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    pCcMepInfo = pMepNode;
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pCcMepInfo);
    PduSmInfo.pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepNode);
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    PduSmInfo.pMepInfo = pMepNode;
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    u2LocalPort = pPortInfo->u2PortNum;

    /* Stop the CCIWhile Timer If running */
    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "OFFLOAD: EcfmCcmOffCreateTxMep: CCI While Timer is"
                      " running for MEPID %d on Port %d  "
                      " \r\n", pMepNode->u2MepId, pPortInfo->u4IfIndex);
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_CCI_WHILE, &PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                          ECFM_OS_RESOURCE_TRC,
                          "OFFLOAD: EcfmCcmOffCreateTxMep: Unable to stop the "
                          "CCI WHILE Timer for MEPID %d on Port %d \r\n",
                          (pMepNode->u2MepId), (pPortInfo->u4IfIndex));
        return ECFM_FAILURE;
    }

    pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
    while (pRMepNode != NULL)
    {
        PduSmInfo.pRMepInfo = pRMepNode;

        if (EcfmCcTmrStopTimer (ECFM_CC_TMR_RMEP_WHILE, &PduSmInfo) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                              ECFM_OS_RESOURCE_TRC,
                              "OFFLOAD: EcfmCcmOffCreateTxMep: Unable to stop "
                              "the RMEP WHILE Timer for MEPID %d on Port %d \r\n",
                              (pMepNode->u2MepId), (pPortInfo->u4IfIndex));
            return ECFM_FAILURE;
        }

        /* Get the next node form the tree */
        pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                        &(pRMepNode->
                                                          MepDbDllNode));
    }

    if (pCcInfo->b1CciEnabled != ECFM_TRUE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC,
                          "OFFLOAD: EcfmCcmOffCreateTxMep: "
                          "CCI is not enabled on Port %d \r\n",
                          pPortInfo->u4IfIndex);
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
        return ECFM_SUCCESS;
    }

    /* Constructing MPLS PDU for MPLS-TP OAM Packet */
    if (pCcMepInfo->pEcfmMplsParams != NULL)
    {
        /*Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_PDU_SIZE,
                                   ECFM_MPLSTP_CTRL_PKT_OFFSET);
        if (pBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "\n OFFLOAD: EcfmCcmOffCreateTxMep: "
                         "Buffer Allocation failed\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }

    else
    {
        /*Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_CCM_PDU_SIZE, ECFM_INIT_VAL);

        if (pBuf == NULL)
        {
            ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC,
                              "\n OFFLOAD: EcfmCcmOffCreateTxMep: Buffer "
                              "Allocation failed for MEPID %d on Port %d\r\n",
                              pMepNode->u2MepId, pPortInfo->u4IfIndex);
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    ECFM_MEMSET (ECFM_CC_PDU, ECFM_INIT_VAL, ECFM_MAX_CCM_PDU_SIZE);
    pu1PduStart = ECFM_CC_PDU;
    pu1EthLlcHdr = ECFM_CC_PDU;
    pu1PduEnd = ECFM_CC_PDU;

    /* Format the  CCM PDU header */
    EcfmCciCcmOffFormatCcmPduHdr (pCcMepInfo, &pu1PduEnd);

    /* Fill in the CCM PDU info like MEP ID, seq number, MAID and other 
     * optional tlvs.
     */
    EcfmCciSmPutInfo (&PduSmInfo, &pu1PduEnd);

    u1PduLength = (UINT1) (pu1PduEnd - pu1PduStart);

    /* check for the Pdu length if it is less than equal to 128 octets then
     * only write to CRU bufffer else retuen failure */
    if (u1PduLength > ECFM_MAX_CCM_PDU_SIZE)
    {
        ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                          ALL_FAILURE_TRC,
                          "\n OFFLOAD: EcfmCcmOffCreateTxMep: PDU"
                          " length is greater than MAX PDU Size for MEPID %d "
                          "on Port %d\r\n", pMepNode->u2MepId,
                          pPortInfo->u4IfIndex);
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /*Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                (UINT4) (u1PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                          ALL_FAILURE_TRC,
                          "\n OFFLOAD: EcfmCcmOffCreateTxMep: Copying into "
                          "Buffer failed for MEPID %d on Port %d\r\n",
                          pMepNode->u2MepId, pPortInfo->u4IfIndex);

        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    if (pCcMepInfo->pEcfmMplsParams == NULL)
    {
        if (pCcMepInfo->u4PrimaryVidIsid < ECFM_INTERNAL_ISID_MIN)
        {
            /* Setting the value in Vlan Info structure */
            VlanInfo.u2VlanId = pCcMepInfo->u4PrimaryVidIsid;

            /* Y.1731: Ccm Priority and Drop Eligibility are configured 
             * According to Y1731 Module enable/disable status*/

            if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPortInfo->u2PortNum))
            {
                VlanInfo.u1Priority = pCcInfo->u1CcmPriority;
                VlanInfo.u1DropEligible = pCcInfo->b1CcmDropEligible;
            }
            else
            {
                VlanInfo.u1Priority = pCcMepInfo->u1CcmLtmPriority;
                VlanInfo.u1DropEligible = ECFM_FALSE;
            }
            VlanInfo.u1TagType = ECFM_VLAN_UNTAGGED;
            EcfmCcmOffFormatEthHdr (pCcMepInfo, &pu1EthLlcHdr,
                                    u1PduLength, VlanInfo);
        }
        else
        {
            EcfmTagType = ECFM_ISID_TAG_INFO;
            IsidInfo.u4Isid = pCcMepInfo->u4PrimaryVidIsid;
            IsidInfo.u1Priority = pCcMepInfo->u1CcmLtmPriority;
            IsidInfo.u1DropEligible = pCcInfo->b1CcmDropEligible;
            IsidInfo.u1TagType = ECFM_VLAN_TAGGED;

            /* Format the Ether net and the LLC header */
            EcfmCcmOffFormatIsidEthHdr (pCcMepInfo, &pu1EthLlcHdr,
                                        u1PduLength, IsidInfo);

        }
        /* Prepend the Ethernet and the LLC header in the CRU buffer */
        u1PduLength = (UINT1) (pu1EthLlcHdr - pu1PduStart);

        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart,
                                  (UINT4) (u1PduLength)) != ECFM_CRU_SUCCESS)
        {
            ECFM_CC_TRC_ARG2 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                              ALL_FAILURE_TRC,
                              "\n OFFLOAD: EcfmCcmOffCreateTxMep: Prepending "
                              "into Buffer failed for MEPID %d on Port %d\r\n",
                              pMepNode->u2MepId, pPortInfo->u4IfIndex);

            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
    }
    else
    {
        if (EcfmCcContructOffloadMplsTpTxPkt (pBuf, pCcMepInfo,
                                              au1MplsDestMac, &u4MplsOutIfIndex)
            != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                              ALL_FAILURE_TRC,
                              "\n OFFLOAD: EcfmCcmOffCreateTxMep: Constructing "
                              "of MPLS Packet Failed for MEPID %d \r\n",
                              pMepNode->u2MepId);

            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

    }

    u2PduSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);

    ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u2PduSize,
                      "OFFLOAD: EcfmCcmOffCreateTxMep: Sending "
                      "out CFMPDU to lower layer...\n");

    if (!(ECFM_CC_IS_PORT_MODULE_STS_ENABLED (u2LocalPort)))
    {
        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "OFFLOAD: ECFcmOffCreateTxMep: "
                          "ECFM Status not enabled on Port: %d \r\n",
                          pPortInfo->u4IfIndex);
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_SUCCESS;
    }
    if (pMepNode->b1Active == ECFM_FALSE)
    {
        ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC,
                          "OFFLOAD: EcfmCcmOffCreateTxMep: MEP %d is not "
                          "active On Port: %d r\n",
                          pMepNode->u2MepId, pPortInfo->u4IfIndex);
        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_SUCCESS;
    }

    if (pMepNode->pEcfmMplsParams != NULL)
    {
        /* The MEP resides on the MPLS TP transport network. Invoke the 
         * appropriate NPAPI for the transmission of the CC PDUs
         */

        /* Check is not made to verify the status of the Tunnel  
         * assuming that if the tunnel is down the packet will not 
         * get transmitted.
         */
        ECFM_MEMSET (&MplsCcOffTxParams, 0x0, sizeof (tEcfmMplstpCcOffTxInfo));
        MplsCcOffTxParams.u4ContextId = pMepNode->u4ContextId;
        MplsCcOffTxParams.pSlotInfo = NULL;    /* Currently Unused */
        MplsCcOffTxParams.pBuf = pBuf;
        MplsCcOffTxParams.u2PduSize = u2PduSize;
        MplsCcOffTxParams.u4CcInterval = pMepNode->pMaInfo->u1CcmInterval;
        MplsCcOffTxParams.pEcfmMplsParams = pMepNode->pEcfmMplsParams;
        ECFM_MEMCPY (MplsCcOffTxParams.au1NHopMac, au1MplsDestMac,
                     MAC_ADDR_LEN);
        MplsCcOffTxParams.u4OutIfIndex = u4MplsOutIfIndex;

        if (pMepNode->u2OffloadMepTxHandle == ECFM_INIT_VAL)
        {
            gu4MepTxRxHandle += 1;
        }
        MplsCcOffTxParams.u2TxFilterHandle = (UINT2) gu4MepTxRxHandle;

        if (EcfmMplstpCcOffCreateTxMep (&MplsCcOffTxParams) == ECFM_FAILURE)
        {
            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: EcfmCcOffCreateTxMepForTransPath "
                              "returned failure for MEPID %d\r\n",
                              pMepNode->u2MepId);
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

        if (pMepNode->u2OffloadMepTxHandle == ECFM_INIT_VAL)
        {
            pCcMepInfo->u2OffloadMepTxHandle =
                MplsCcOffTxParams.u2TxFilterHandle;

            EcfmRedSynchOffTxFilterId (ECFM_CC_CURR_CONTEXT_ID (),
                                       pCcMepInfo->u4MdIndex,
                                       pCcMepInfo->u4MaIndex,
                                       pCcMepInfo->u2MepId,
                                       pCcMepInfo->u2OffloadMepTxHandle);
            ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: ECFM-MPLS-TP MEPId %d Tx Handle Value is updated "
                              "%d\r\n", pCcMepInfo->u2MepId,
                              pCcMepInfo->u2OffloadMepTxHandle);
        }
    }
    else
    {
        if (EcfmTagType == ECFM_VLAN_TAG_INFO)
        {
            if (EcfmCcCtrlTxTransmitPktToOffLoad (pBuf, pCcMepInfo,
                                                  &VlanInfo,
                                                  pCcInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD: Function "
                                  "EcfmCcCtrlTxTransmitPktToOffLoad Returned failed"
                                  "for MEPID %d on Port %d\r\n",
                                  pMepNode->u2MepId, pPortInfo->u4IfIndex);

                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                ECFM_CC_INCR_TX_FAILED_COUNT (pCcMepInfo->u2PortNum);
                ECFM_CC_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
                return ECFM_FAILURE;
            }
        }
        else
        {
            if (EcfmCcCtrlTxTransmitIsidPktToOffLoad (pBuf, pCcMepInfo,
                                                      &IsidInfo,
                                                      pCcInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD: Function EcfmCcCtrlTxTransmitPktToOffLoad Returned failed"
                                  "for MEPID %d on Port %d\r\n",
                                  pMepNode->u2MepId, pPortInfo->u4IfIndex);

                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                ECFM_CC_INCR_TX_FAILED_COUNT (pCcMepInfo->u2PortNum);
                ECFM_CC_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
                return ECFM_FAILURE;
            }

        }
    }
    ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
    pBuf = NULL;

    if (RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode) == NULL)
    {
        /* Add MEP node in offload global RBTree offloadMepPortNode */
        if (RBTreeAdd (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode) ==
            ECFM_RB_FAILURE)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmOffCreateTxMep: RBTree addition is failed \r\n");
            return ECFM_FAILURE;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciCcmOffFormatCcmPduHdr
 *
 * Description        : This rotuine is used to fill the CFM PDU Header
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       pu1CcmPdu - Pointer to the Pdu     
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCciCcmOffFormatCcmPduHdr (tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu)
{
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1Flag = ECFM_INIT_VAL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);
    pu1Pdu = *pu1CcmPdu;

    /* Fill in the 4 byte as CFM header */

    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1Pdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_OPCODE_CCM);

    /* Fill in the Next 1 byte of FLag(RDI(MSB),
     * Reserve(4Bit),CCIInterval(3Bit) */

    /* Set the MSB as the RDI field */
    /* RDI bit is set if any of the of variable someRMEPCCMDefec,
       someMACstatusDefect,errorCCMDefect or xconCCMDefect is true,
       means check is made if the  vector variable Defect is non zero in 
       MepInfo then RDI BIT is set */
    if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_TRUE) ||
        (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_TRUE) ||
        (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE) ||
        (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_TRUE))
    {
        if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)))
        {
            if (ECFM_CC_IS_RDI_ENABLED (pMepInfo))
            {
                /* Set the MSB in the FLAGS of the CCM-PDU */
                ECFM_SET_U1BIT (u1Flag, ECFM_CC_RDI_BIT);
                /* set presentRDI to True */
                pMepInfo->b1PresentRdi = ECFM_TRUE;
            }
            else
            {
                pMepInfo->b1PresentRdi = ECFM_FALSE;
            }
        }
        else
        {
            /* Set the MSB in the FLAGS of the CCM-PDU */
            ECFM_SET_U1BIT (u1Flag, ECFM_CC_RDI_BIT);
            pMepInfo->b1PresentRdi = ECFM_TRUE;
        }
    }
    else
    {
        /* set presentRDI to False */
        pMepInfo->b1PresentRdi = ECFM_FALSE;
    }

    pMepInfo->b1CcmOffloadLastRdi = pMepInfo->b1PresentRdi;
    /* next 4bit are reserved */
    /* Fill in the next 3 bits of CCI Interval */
    ECFM_CC_SET_CCM_INTERVAL (u1Flag, pMaInfo->u1CcmInterval);
    ECFM_PUT_1BYTE (pu1Pdu, u1Flag);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_CCM_FIRST_TLV_OFFSET);

    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffFormatEthHdr   
 *
 * Description        : This Routine will Fill the Ethernet Hedder. 
 *                      
 *                      
 * Input(s)           : pMepInfo           - Pointer to the MEP structure
 *                      pu1CcmPdu           - Pointer to the Pdu
 *                      u1Length         -Length of the CFM header formated so
 *                      far
 *                      VlanInfo         - Vlan Information 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *                      
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcmOffFormatEthHdr (tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu, UINT1
                        u1Length, tVlanTagInfo VlanInfo)
{
    tMacAddr            MacAddr;
    UINT4               u4BridgeMode = ECFM_INIT_VAL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2VlanProtoId = ECFM_INIT_VAL;
    UINT2               u2VlanId = ECFM_INIT_VAL;
    BOOL1               b1LlcEncapSts = ECFM_TRUE;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1DeiBitValue = (UINT1) ECFM_INIT_VAL;
    tMacAddr            NullMacAddr = { ECFM_INIT_VAL };

    ECFM_CC_TRC_FN_ENTRY ();

    pu1Pdu = *pu1CcmPdu;

    /*Get the LLC Encapsulation status from the port Info */
    b1LlcEncapSts = pMepInfo->pPortInfo->b1LLCEncapStatus;

    /* Y.1731:
     * Fill in the 6byte of Destination Address
     * Destination Address is unicast if CC Role is Performance Monitoring
     * and Y.1731 is enabled otherwise DA is multicast
     */
    if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
        (ECFM_MEMCMP (pMepInfo->CcInfo.UnicastCcmMacAddr, NullMacAddr,
                      ECFM_MAC_ADDR_LENGTH) != ECFM_SUCCESS))
    {
        ECFM_MEMCPY (MacAddr, pMepInfo->CcInfo.UnicastCcmMacAddr,
                     ECFM_MAC_ADDR_LENGTH);
    }
    else
    {
        ECFM_GEN_MULTICAST_CLASS1_ADDR (MacAddr, pMepInfo->u1MdLevel);
    }

    /*Finally copying the Mac address to the pdu */
    ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);
    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 6byte with source address as the address of the Sending
     * MEP*/
    if (ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        return;
    }
    ECFM_GET_MAC_ADDR_OF_PORT ((ECFM_CC_PORT_INFO (pMepInfo->u2PortNum)->
                                u4IfIndex), pu1Pdu);

    u4IfIndex = ECFM_CC_PORT_INFO (pMepInfo->u2PortNum)->u4IfIndex;
    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 2 byte as Length of the CMF PDU if the LLC encap has to be
     * added else fill in as the CFM type */
    /* LLC Snap header not to be filled */
    u1DeiBitValue = EcfmVlanIsDeiBitSet (u4IfIndex);
    if (ECFM_IS_MEP_VLAN_AWARE (VlanInfo.u2VlanId))
    {
        EcfmFormVlanTagFrame (&u2VlanId, VlanInfo, u1DeiBitValue);
        if (ECFM_BRIDGE_MODE (ECFM_CC_CURR_CONTEXT_ID (), &u4BridgeMode) !=
            L2IWF_SUCCESS)
        {
            return;
        }

        /* Fill VLAN Ethertype configured */
        EcfmVlanGetPortEtherType (u4IfIndex, &u2VlanProtoId);
        if (pMepInfo->u1Direction == ECFM_MP_DIR_DOWN)
        {
            if (EcfmL2IwfMiIsVlanUntagMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                                  VlanInfo.u2VlanId,
                                                  u4IfIndex) != ECFM_TRUE)
            {
                ECFM_PUT_2BYTE (pu1Pdu, u2VlanProtoId);
                ECFM_PUT_2BYTE (pu1Pdu, u2VlanId);
            }
        }
        else
        {
            ECFM_PUT_2BYTE (pu1Pdu, u2VlanProtoId);
            ECFM_PUT_2BYTE (pu1Pdu, u2VlanId);
        }

    }
    if (b1LlcEncapSts != ECFM_TRUE)
    {
        /* Fill in the next 2 byte as the CFM type */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_PDU_TYPE_CFM);
    }
    else
    {
        /* Filling LLC Snap header */
        ECFM_CC_SET_LLC_SNAP_HDR (pu1Pdu, (u1Length + ECFM_LLC_SNAP_HDR_SIZE));
    }

    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmFormVlanTagFrame   
 *
 * Description        : This Routine will Generate the VLAN ID to be added in
 *                      CCM PDU . 
 *                      
 *                      
 * Input(s)           : pu2VlanTagId     - Pointer to the MEP structure
 *                      VlanInfo         - Vlan Information 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *                      
 *
 *****************************************************************************/

PUBLIC VOID
EcfmFormVlanTagFrame (UINT2 *pu2VlanTagId, tVlanTagInfo VlanInfo,
                      UINT1 u1DeiBitValue)
{

    UINT1               u1Priority = ECFM_FALSE;

    ECFM_CC_TRC_FN_ENTRY ();

    u1Priority = ((VlanInfo.u1Priority) << 1);
    if (ECFM_CC_802_1AD_BRIDGE () == ECFM_TRUE)
    {
        if ((u1DeiBitValue == ECFM_TRUE)
            && (VlanInfo.u1DropEligible == ECFM_TRUE))
        {
            u1Priority = (UINT1) (u1Priority | 0x0001);
        }
    }
    *pu2VlanTagId = (UINT2) u1Priority;
    *pu2VlanTagId = (UINT2) (*pu2VlanTagId << ECFM_VLAN_TAG_PRIORITY_DEI_SHIFT);
    *pu2VlanTagId = (UINT2) (*pu2VlanTagId | VlanInfo.u2VlanId);

    ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Function EcfmFormVlanTagFrame Returned Vlan"
                      "Id %d\r\n", *pu2VlanTagId);

    ECFM_CC_TRC_FN_EXIT ();
}

/******************************************************************************
 * Function Name      : EcfmCcCtrlTxTransmitPktToOffLoad   
 *
 * Description        : This Routine will Forwared the Packet to Hardware
 *                      Depending upon the Direction of MEP. 
 *                      
 * Input(s)           : pBuf     - CRU Buffer Pointer 
 *                     pCcMepInfo - Pointer to MEP Structure Info 
 *                     pVlanInfo  - Pointer to VLAN Info 
 *                     pCcMepCcInfo - Pointer to CC Mep Info Structure
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS.
 *                      ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcCtrlTxTransmitPktToOffLoad (tEcfmBufChainHeader * pBuf,
                                  tEcfmCcMepInfo * pCcMepInfo,
                                  tEcfmVlanTagInfo * pVlanInfo,
                                  tEcfmCcMepCcInfo * pCcMepCcInfo)
{

    UINT1               u1MpDirection = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    u1MpDirection = pCcMepInfo->u1Direction;

    if (ECFM_IS_MEP_VLAN_AWARE (pVlanInfo->u2VlanId))
    {
        /*Check the port memebership of Vlan */
        if (EcfmL2IwfMiIsVlanMemberPort
            (ECFM_CC_CURR_CONTEXT_ID (),
             pCcMepInfo->u4PrimaryVidIsid,
             pCcMepInfo->pPortInfo->u4IfIndex) != OSIX_TRUE)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "OFFLOAD: Function "
                         "EcfmL2IwfMiIsVlanMemberPort has returned" "\r\n");
            pCcMepInfo->b1MepCcmOffloadHwStatus = ECFM_FALSE;
            pCcMepInfo->u2OffloadMepTxHandle = ECFM_INIT_VAL;
            return ECFM_SUCCESS;
        }

        if (EcfmL2IwfMiIsVlanActive (ECFM_CC_CURR_CONTEXT_ID (),
                                     pCcMepInfo->u4PrimaryVidIsid) != OSIX_TRUE)
        {
            ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: Function "
                              "EcfmL2IwfMiIsVlanActive has returned"
                              "Failure For MEP %d On Port %d Active \r\n",
                              pCcMepInfo->u2MepId,
                              pCcMepInfo->pPortInfo->u4IfIndex);

            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: VLAN %d is Not Active Active \r\n",
                              pVlanInfo->u2VlanId);
            pCcMepInfo->b1MepCcmOffloadHwStatus = ECFM_FALSE;
            pCcMepInfo->u2OffloadMepTxHandle = ECFM_INIT_VAL;
            return ECFM_SUCCESS;
        }

        /* If MEP Dirextion is down */
        if (u1MpDirection == ECFM_MP_DIR_DOWN)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: MEP %d is Vlan "
                              "Aware MEP with Direction Down "
                              "VLAN ID is  %d\r\n",
                              pCcMepInfo->u2MepId, pVlanInfo->u2VlanId);
            /*Check the port operational Status */
            if (pCcMepInfo->pPortInfo->u1IfOperStatus != CFA_IF_UP)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "Operational State of the port is not UP\r\n");
                pCcMepInfo->b1MepCcmOffloadHwStatus = ECFM_FALSE;
                pCcMepInfo->u2OffloadMepTxHandle = ECFM_INIT_VAL;
                return ECFM_SUCCESS;
            }

            if (EcfmCcCtrlTxFwdToHwForDown (pBuf, pCcMepInfo, pCcMepCcInfo) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD:Function EcfmCcCtrlTxFwdToHwForDown:"
                                  "Returned Failure for MEP %d\r\n",
                                  pCcMepInfo->u2MepId);
                return ECFM_FAILURE;
            }
        }
        else
        {
            /* If Up MP is Vlan aware then transmit the the CFM-PDU to the
             * frame-filtering 
             */
            tEcfmCcPduSmInfo    PduSmInfo;
            /* Clearing values of PduSmInfo structure */
            ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: MEP %d is Vlan Aware "
                              "MEP with Direction Up "
                              "VLAN ID is  %d\r\n",
                              pCcMepInfo->u2MepId, pVlanInfo->u2VlanId);

            pCcMepInfo->pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pCcMepInfo);
            PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO
                (pCcMepInfo->pPortInfo->u2PortNum);
            PduSmInfo.pMepInfo = pCcMepInfo;
            PduSmInfo.pBuf = pBuf;
            PduSmInfo.EcfmTagType = ECFM_VLAN_TAG_INFO;
            PduSmInfo.u4RxVlanIdIsId = pVlanInfo->u2VlanId;
            PduSmInfo.u1RxDirection = ECFM_MP_DIR_UP;

            ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo.OuterVlanTag),
                         pVlanInfo, sizeof (tEcfmVlanTagInfo));

            EcfmCcOffCtrlTxParsePduHdrs (&PduSmInfo);

            if (EcfmCcCtrlTxFwdToHdForUp (&PduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD:Function EcfmCcCtrlTxFwdToHdForUp "
                                  "has Returned failure For MEPID %d \r\n",
                                  pCcMepInfo->u2MepId);
                return ECFM_FAILURE;
            }
        }                        /* end of If up/Down MP is Vlan aware */
    }
    else
    {
        if (EcfmCcCtrlTxFwdToHwForDown (pBuf, pCcMepInfo, pCcMepCcInfo)
            != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: Function EcfmCcCtrlTxFwdToHwdForDown"
                              "has returned failure for MEPID %d \r\n",
                              pCcMepInfo->u2MepId);
            return ECFM_FAILURE;
        }

    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmCcOffCtrlTxParsePduHdrs 
 *
 * Description        : This is called to parse the received CFM PDU by 
 *                      extracting the Header values from that and places then 
 *                      into the tEcfmCcPduSmInfo Structure.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                                   structure holds the information about the MP 
 *                                   and the so far parsed CFM-PDU
 * Output(s)          : None
 *
 * Return Value(s)    : NONE
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcOffCtrlTxParsePduHdrs (tEcfmCcPduSmInfo * pPduSmInfo)
{
    /* Interface Index of the port */
    UINT4               u4Offset = ECFM_INIT_VAL;    /* Stores offset */
    UINT2               u2TypeLength = ECFM_INIT_VAL;
    /* Store CFM PDU Type */
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;
    /* Stores first byte of CFM HDR */

    ECFM_CC_TRC_FN_ENTRY ();

    /* Copy the 6byte destination address from the recived PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxDestMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    /* Copy the 6byte Received source address from the LBM PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxSrcMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    /* Get Type Length */
    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset, u2TypeLength);

    /* Move the pointer by the 2 byte to get the MD LEVEL */
    u4Offset = u4Offset + ECFM_VLAN_TYPE_LEN_FIELD_SIZE;

    /* Check if TypeLength value contains ECFM_PDU_TYPE_CFM type then LLC Snap  
     * Header is present then move offset by 8 bytes
     */
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)
    {
        u4Offset = u4Offset + ECFM_LLC_SNAP_HDR_SIZE;
    }

    pPduSmInfo->u1CfmPduOffset = (UINT1) u4Offset;
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1MdLvlAndVer);

    /* Read the 3MSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxMdLevel = (UINT1) (ECFM_GET_MDLEVEL (u1MdLvlAndVer));

    /* Read the 5LSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxVersion = (UINT1) (ECFM_GET_VERSION (u1MdLvlAndVer));
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flag from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset,
                         pPduSmInfo->u1RxFirstTlvOffset);
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcCtrlTxFwdToHdForUp  
 *
 * Description        : This Routine will Create the PortList & Pass tp Hardware
 *                      for the Tx of CCM PDU for UP MEP 
 *                       
 *                      
 * Input(s)           : pPduSmInfo  - Pointer to structure of tEcfmCcPduSmInfo 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS.
 *                      ECFM_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
EcfmCcCtrlTxFwdToHdForUp (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmBufChainHeader *pCruBuf = NULL;
    UINT1              *pIfFwdPortList = NULL;
    tPortListExt       *pIfFwdActualPortList = NULL;
    tPortListExt       *pUntagIfFwdActualPortList = NULL;
    tEcfmMacAddr        DestAddr;
    tEcfmMacAddr        SrcAddr;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;

    UINT4               u4PduLength = ECFM_INIT_VAL;
    UINT4               u4VidIsid = ECFM_INIT_VAL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2Vid = ECFM_INIT_VAL;
    UINT2               u2OffloadMepTxHandle = ECFM_INIT_VAL;
    UINT2               u2MepTxVlanId = ECFM_INIT_VAL;

    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT4               u4CcmInterval = ECFM_INIT_VAL;
    UINT1               u1PortState = ECFM_INIT_VAL;
    UINT1              *pu1Buf = NULL;
    UINT1               au1UniqueTxHandle[ECFM_TX_HANDLE_NAME_LENGTH];
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pPortInfo = pPduSmInfo->pPortInfo;
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    u2LocalPort = pPortInfo->u2PortNum;
    u4IfIndex = pPortInfo->u4IfIndex;
    pCruBuf = pPduSmInfo->pBuf;
    pIfFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pIfFwdPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlTxFwdToHdForUp: Error in allocating memory "
                     "for pIfFwdPortList\r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pIfFwdPortList, ECFM_INIT_VAL, sizeof (tLocalPortListExt));
    ECFM_MEMSET (au1UniqueTxHandle, ECFM_INIT_VAL, ECFM_TX_UNIQUE_HANDLE_VAL);
    ECFM_MEMSET (SrcAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (DestAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));

    ECFM_COPY_FROM_CRU_BUF (pCruBuf, DestAddr, ECFM_INIT_VAL,
                            ECFM_MAC_ADDR_LENGTH);
    ECFM_COPY_FROM_CRU_BUF (pCruBuf, SrcAddr, ECFM_MAC_ADDR_LENGTH,
                            ECFM_MAC_ADDR_LENGTH);
    u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
    if (pPduSmInfo->EcfmTagType == ECFM_VLAN_TAG_INFO)
    {
        u4VidIsid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        if (EcfmVlanGetFwdPortList
            (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort, SrcAddr, DestAddr, u2Vid,
             pIfFwdPortList) != VLAN_SUCCESS)
        {
            ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort, ECFM_OPCODE_CCM);
            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD:Function EcfmVlanGetFwdPortList has "
                              "returned failure for MEPID %d \r\n",
                              pPduSmInfo->pMepInfo->u2MepId);
            UtilPlstReleaseLocalPortList (pIfFwdPortList);
            return ECFM_FAILURE;
        }
    }
    else
    {
        u4VidIsid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
    }

    pIfFwdActualPortList =
        (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));

    if (pIfFwdActualPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_CRITICAL_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCtrlTxFwdToHdForUp: Error in Allocating memory"
                     " for bitlist \r\n");
        UtilPlstReleaseLocalPortList (pIfFwdPortList);
        return ECFM_FAILURE;
    }

    pUntagIfFwdActualPortList =
        (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));

    if (pUntagIfFwdActualPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_CRITICAL_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCtrlTxFwdToHdForUp: Error in Allocating memory"
                     " for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
        UtilPlstReleaseLocalPortList (pIfFwdPortList);
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (*pIfFwdActualPortList, ECFM_INIT_VAL, sizeof (tPortListExt));
    ECFM_MEMSET (*pUntagIfFwdActualPortList, ECFM_INIT_VAL,
                 sizeof (tPortListExt));

    /* Disable the port on which the packet was received */
    ECFM_RESET_MEMBER_PORT (pIfFwdPortList, u2LocalPort);
    /* Remove the Port From the PortList If Port State is Discarding */
    /*Get the egress Port and forward the packet */
    for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
         u2ByteInd++)
    {
        if (pIfFwdPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = pIfFwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {
            u2Port =
                (u2ByteInd * BITS_PER_BYTE) +
                EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
            /*Reset the port info */
            pPortInfo = NULL;
            /*Get the port info */
            pPortInfo = ECFM_CC_GET_PORT_INFO (u2Port);
            if (pPortInfo != NULL)
            {
                /* Check if Spanning Tree is enabled in this Context */
                if (EcfmIsStpEnabledInContext
                    (ECFM_CC_CURR_CONTEXT_ID () != ECFM_SUCCESS))
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                 ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcCtrlTxFwdToHdForUp:"
                                 "STP is disabled in this Context\r\n");

                    u1PortState = AST_PORT_STATE_FORWARDING;
                }
                else
                {
                    u1PortState =
                        EcfmL2IwfGetVlanPortState (u4VidIsid,
                                                   pPortInfo->u4IfIndex);
                }
                /* Port status is not forwarding */
                if (u1PortState != AST_PORT_STATE_FORWARDING)
                {
                    if (EcfmL2IwfMiIsVlanUntagMemberPort
                        (pPortInfo->u4ContextId, u4VidIsid,
                         pPortInfo->u4IfIndex) == ECFM_TRUE)
                    {
                        ECFM_RESET_MEMBER_PORT
                            ((*pUntagIfFwdActualPortList),
                             pPortInfo->u4IfIndex);
                    }
                    else
                    {
                        ECFM_RESET_MEMBER_PORT ((*pIfFwdActualPortList),
                                                pPortInfo->u4IfIndex);
                    }
                }
                else
                {
                    if (EcfmL2IwfMiIsVlanUntagMemberPort
                        (pPortInfo->u4ContextId, u4VidIsid,
                         pPortInfo->u4IfIndex) == ECFM_TRUE)
                    {
                        /*Reset the Port Info Ptr */
                        pTempPortInfo = NULL;
                        if ((ECFM_CC_ISPORT_SISP_LOG (u2Port,
                                                      pTempPortInfo)) ==
                            ECFM_FALSE)
                        {
                            ECFM_SET_MEMBER_PORT
                                ((*pUntagIfFwdActualPortList),
                                 pPortInfo->u4IfIndex);
                        }
                        else
                        {
                            ECFM_SET_MEMBER_PORT
                                ((*pUntagIfFwdActualPortList),
                                 pPortInfo->u4PhyPortNum);
                        }
                    }
                    else
                    {
                        /*Reset the Port Info Ptr */
                        pTempPortInfo = NULL;
                        if ((ECFM_CC_ISPORT_SISP_LOG (u2Port,
                                                      pTempPortInfo)) ==
                            ECFM_FALSE)
                        {
                            ECFM_SET_MEMBER_PORT ((*pIfFwdActualPortList),
                                                  pPortInfo->u4IfIndex);
                        }
                        else
                        {
                            ECFM_SET_MEMBER_PORT ((*pIfFwdActualPortList),
                                                  pPortInfo->u4PhyPortNum);
                        }
                    }
                }
            }
        }
    }

    /* Call the NPAPI fot Tx of Up Mep */

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pPduSmInfo->pMepInfo);

    ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4CcmInterval);
    /* Timer Interval Changed to Micro Seconds 
     * */
    u4CcmInterval = u4CcmInterval * ECFM_NUM_OF_USEC_IN_A_MSEC;
    u4PduLength = ECFM_GET_CRU_VALID_BYTE_COUNT (pCruBuf);

    u2MepTxVlanId = pPduSmInfo->pMepInfo->u4PrimaryVidIsid;

    if (pPduSmInfo->pMepInfo->u2OffloadMepTxHandle == ECFM_INIT_VAL)
    {
        gu4MepTxRxHandle = gu4MepTxRxHandle + 1;
        u2OffloadMepTxHandle = gu4MepTxRxHandle;
    }
    else
    {
        u2OffloadMepTxHandle = pPduSmInfo->pMepInfo->u2OffloadMepTxHandle;
    }

    ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: UP MEPId %d Passing The PDU For Tx "
                      "To Hardware For Port No %d\r\n",
                      pPduSmInfo->pMepInfo->u2MepId, u4IfIndex);

    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: MEPId %d CCM PduLength %d"
                      "Time Interval Value is %d\r\n",
                      pPduSmInfo->pMepInfo->u2MepId, u4PduLength,
                      u4CcmInterval);

    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: UP MEPId %d Length of CCM PUD is %d "
                      "Time Interval is %d\r\n",
                      pPduSmInfo->pMepInfo->u2MepId, u4PduLength,
                      u4CcmInterval);

    ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pCruBuf, u4PduLength,
                      "OFFLOAD: CCM PDU Passing to Hardware for Tx\n");

    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: UP MEPId %d Tx Handle Value is %d "
                      "on interface  %d\r\n",
                      pPduSmInfo->pMepInfo->u2MepId, u2OffloadMepTxHandle,
                      u4IfIndex);
    if (ECFM_COPY_FROM_CRU_BUF (pCruBuf, ECFM_CC_PDU, 0, u4PduLength) ==
        ECFM_CRU_FAILURE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "CCMOFFLOAD: "
                     "Received Bpdu Copy From CRU Buffer FAILED\r\n");
        FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
        FsUtilReleaseBitList ((UINT1 *) pUntagIfFwdActualPortList);
        UtilPlstReleaseLocalPortList (pIfFwdPortList);
        return ECFM_FAILURE;
    }

    pu1Buf = ECFM_CC_PDU;

    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                      "OFFLOAD: CCM Start Tx NPAPI is called for MEP %d Valn"
                      "%d on Port %d  \r\n",
                      pPduSmInfo->pMepInfo->u2MepId, u2MepTxVlanId, u4IfIndex);

    if (EcfmCcmOffCreateMepTxOnPortList (*pIfFwdActualPortList,
                                         *pUntagIfFwdActualPortList,
                                         pu1Buf,
                                         u4PduLength,
                                         u4CcmInterval,
                                         ECFM_CC_CURR_CONTEXT_ID (),
                                         u2OffloadMepTxHandle,
                                         pPduSmInfo->pMepInfo,
                                         pPduSmInfo->pRMepInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort, ECFM_OPCODE_CCM);
        ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC |
                          ECFM_CONTROL_PLANE_TRC,
                          "OFFLOAD: UP MEPId %d Tx Handle "
                          "Value Received from"
                          "Offloaded Module is is %d"
                          "on interface  %d\r\n",
                          pPduSmInfo->pMepInfo->u2MepId,
                          u2OffloadMepTxHandle, u4IfIndex);

        ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                          "OFFLOAD: Function EcfmCcmOffCreateMepTxOnPortList"
                          "returned failure for MEPID %d on Interface %d \r\n",
                          pPduSmInfo->pMepInfo->u2MepId, u4IfIndex);
        FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
        FsUtilReleaseBitList ((UINT1 *) pUntagIfFwdActualPortList);
        UtilPlstReleaseLocalPortList (pIfFwdPortList);
        return ECFM_FAILURE;
    }

    pPduSmInfo->pMepInfo->u2OffloadMepTxHandle = u2OffloadMepTxHandle;

    EcfmRedSynchOffTxFilterId (ECFM_CC_CURR_CONTEXT_ID (),
                               pPduSmInfo->pMepInfo->u4MdIndex,
                               pPduSmInfo->pMepInfo->u4MaIndex,
                               pPduSmInfo->pMepInfo->u2MepId,
                               u2OffloadMepTxHandle);
    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: UP MEPId %d Tx Handle Value is updated %d "
                      "on interface  %d\r\n", pPduSmInfo->pMepInfo->u2MepId,
                      u2OffloadMepTxHandle, u4IfIndex);

    FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
    FsUtilReleaseBitList ((UINT1 *) pUntagIfFwdActualPortList);
    UtilPlstReleaseLocalPortList (pIfFwdPortList);
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcCtrlTxFwdToHwForDown
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      pCcMepInfo - Pointer of Structue of Mep Info  
 *                    
 *                      pCcMepCcInfo  - Pointer to Structure of CCMEP Info 
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcCtrlTxFwdToHwForDown (tEcfmBufChainHeader * pBuf,
                            tEcfmCcMepInfo * pCcMepInfo,
                            tEcfmCcMepCcInfo * pCcMepCcInfo)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcPduSmInfo    EcfmCcPduSmInfo;

    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PduSize = ECFM_INIT_VAL;
    UINT2               u2OffloadMepTxHandle = ECFM_INIT_VAL;
    UINT1               au1UniqueTxHandle[ECFM_TX_HANDLE_NAME_LENGTH];
    UINT1               u1PduLength = ECFM_INIT_VAL;
    UINT4               u4CcmInterval = ECFM_INIT_VAL;
    UINT1              *pu1Buf = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    UNUSED_PARAM (pCcMepCcInfo);
    ECFM_MEMSET (&EcfmCcPduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    u1PduLength = (UINT1) ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);
    UNUSED_PARAM (u1PduLength);
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pCcMepInfo);

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pCcMepInfo);
    ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4CcmInterval);
/* Time Interval Changed to MicroSeconds 
 * */
    u4CcmInterval = u4CcmInterval * ECFM_NUM_OF_USEC_IN_A_MSEC;
    u2PduSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);
    u4IfIndex = pPortInfo->u4IfIndex;
    ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Down MEPId %d Passing The PDU For Tx "
                      "To Hardware For Port No %d\r\n",
                      pCcMepInfo->u2MepId, u4IfIndex);

    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Down MEPId %d Length of CCM PUD is %d "
                      "Time Interval is %d\r\n",
                      pCcMepInfo->u2MepId, u2PduSize, u4CcmInterval);

    ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u2PduSize,
                      "OFFLOAD: CCM PDU Passing to Hardware for Tx\n");

    ECFM_MEMSET (au1UniqueTxHandle, 0, ECFM_TX_UNIQUE_HANDLE_VAL);

    if (pCcMepInfo->u2OffloadMepTxHandle == ECFM_INIT_VAL)
    {
        gu4MepTxRxHandle = gu4MepTxRxHandle + 1;
        u2OffloadMepTxHandle = gu4MepTxRxHandle;
    }
    else
    {
        u2OffloadMepTxHandle = pCcMepInfo->u2OffloadMepTxHandle;
    }

    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Down MEPId %d Tx Handle Value Generated is %d "
                      "on interface  %d\r\n",
                      pCcMepInfo->u2MepId, u2OffloadMepTxHandle, u4IfIndex);

    if (ECFM_COPY_FROM_CRU_BUF (pBuf, ECFM_CC_PDU, 0, u2PduSize) ==
        ECFM_CRU_FAILURE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "CCMOFFLOAD: "
                     "Received BPDU Copy From CRU Buffer FAILED\r\n");
        return ECFM_FAILURE;
    }
    pu1Buf = ECFM_CC_PDU;

    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                      "OFFLOAD: CCM Start Tx NPAPI is called for MEP %d Vlan"
                      "%d on Port %d  \r\n",
                      pCcMepInfo->u2MepId, pCcMepInfo->u4PrimaryVidIsid,
                      u4IfIndex);

    if ((EcfmCcmOffCreateMepTx
         (u4IfIndex, pu1Buf, u2PduSize, u4CcmInterval,
          u2OffloadMepTxHandle, pCcMepInfo) == ECFM_FAILURE))

    {
        ECFM_CC_HANDLE_TRANSMIT_FAILURE (pPortInfo->u2PortNum, ECFM_OPCODE_CCM);
        ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                          "OFFLOAD: Down MEPId %d Tx Handle Value Received from"
                          "Offloaded Module is is %d on interface  %d\r\n",
                          pCcMepInfo->u2MepId, u2OffloadMepTxHandle, u4IfIndex);

        ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                          "OFFLOAD: Function EcfmCcmOffCreateMepTx"
                          "returned failure for MEPID %d on Interface %d \r\n",
                          pCcMepInfo->u2MepId, u4IfIndex);

        return ECFM_FAILURE;
    }

    pCcMepInfo->u2OffloadMepTxHandle = u2OffloadMepTxHandle;
    EcfmRedSynchOffTxFilterId (ECFM_CC_CURR_CONTEXT_ID (),
                               pCcMepInfo->u4MdIndex, pCcMepInfo->u4MaIndex,
                               pCcMepInfo->u2MepId, u2OffloadMepTxHandle);
    ECFM_CC_TRC_ARG3 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Down MEPId %d Tx Handle Value is updated %d "
                      "on interface  %d\r\n", pCcMepInfo->u2MepId,
                      u2OffloadMepTxHandle, u4IfIndex);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffDeleteTxMep  
 *
 * Description        : This Routine will Delete the Mep Information From the
 *                      Offloaded Module, & start the CCM Tx in
 *                      Software for the MEP .
 *                      
 * Input(s)           : pMepNode   - Pointer to structure of Mep Info
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    :  - ECFM_SUCCESS.
 *                       - ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffDeleteTxMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcOffMepTxStats EcfmCcOffMepTxStats;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmMplstpCcOffTxInfo MplsCcOffTxParams;

    UINT4               u4ContextId = ECFM_INIT_VAL;
#ifdef TRACE_WANTED
    UINT4               u4NoOfCcmTxFailure = ECFM_INIT_VAL;
    UINT4               u4CcmTx = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
#endif
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT2               u2CrossCheckDelay = ECFM_INIT_VAL;

    UINT2               u2OffloadMepTxHandle = (UINT2) ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);

    u2OffloadMepTxHandle = pMepNode->u2OffloadMepTxHandle;

    u4ContextId = pMepNode->pPortInfo->u4ContextId;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    ECFM_MEMSET (&EcfmCcOffMepTxStats, ECFM_INIT_VAL,
                 sizeof (tEcfmCcOffMepTxStats));

    PduSmInfo.pMepInfo = pMepNode;
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);

    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);

    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: TxHandle value For MEPID %d is %d \r\n",
                      pMepNode->u2MepId, pMepNode->u2OffloadMepTxHandle);

    if (RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode) != NULL)
    {
        RBTreeRem (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode);
    }

    /* Update Tx Stats from the Offloaded Module */
    if (u2OffloadMepTxHandle != ECFM_INIT_VAL)
    {
        if (EcfmGetCcmOffTxStatistics (u4ContextId, u2OffloadMepTxHandle,
                                       &EcfmCcOffMepTxStats) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG1 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: Function EcfmGetCcmOffTxStatistics has"
                              " Returned Failure For MEPID %d \r\n",
                              pMepNode->u2MepId);
            return ECFM_FAILURE;
        }
    }

    if (!ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPortInfo->u2PortNum))
    {
        pCcInfo->u4CciSentCcms = EcfmCcOffMepTxStats.u4LastSeqNum;
    }

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Tx CCM Count "
                      "in Offloaded Module is %d for .\r\n"
                      "MEPID %d on Port %d", u4CcmTx,
                      pMepNode->u2MepId, pPortInfo->u4IfIndex);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Sequence Number Received from "
                      "Offloaded Module for "
                      "MEPID %d on Port %d is %d .\r\n",
                      pMepNode->u2MepId, pPortInfo->u4IfIndex, u4SeqNum);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Tx CCM Failure Count "
                      "in Offloaded Module is %d for .\r\n"
                      "MEPID %d on Port %d", u4NoOfCcmTxFailure,
                      pMepNode->u2MepId, pPortInfo->u4IfIndex);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Transmitted CCM %d Count"
                      "is Updated in Software from Offloaded"
                      "Module For MEPID %d on Port %d \r\n",
                      pPortInfo->u4TxCcmCount, pMepNode->u2MepId,
                      pPortInfo->u4IfIndex);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Transmittion CCM Failure %d"
                      "Count is Updated in Software from"
                      "Offloaded Module For MEPID %d on Port %d \r\n",
                      pPortInfo->u4TxFailedCount, pMepNode->u2MepId,
                      pPortInfo->u4IfIndex);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Sequence Number is %d updated"
                      " in Software for MEPID %d"
                      "on Port %d \r\n",
                      pCcInfo->u4CciSentCcms, pMepNode->u2MepId,
                      pPortInfo->u4IfIndex);

    if (pMepNode->u4PrimaryVidIsid > ECFM_VLANID_MAX)
    {
        ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                          "OFFLOAD: CCM Stop Tx NPAPI is called for MEP %d Vlan"
                          "%d on Port %d  \r\n",
                          pMepNode->u2MepId, pMepNode->u4PrimaryVidIsid,
                          pMepNode->pPortInfo->u4IfIndex);
    }
    else
    {
        ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                          "OFFLOAD: CCM Stop Tx NPAPI is "
                          "called for MEP %d Vlan"
                          "%d on Port %d  \r\n",
                          pMepNode->u2MepId, pMepNode->u4PrimaryVidIsid,
                          pMepNode->pPortInfo->u4IfIndex);
    }

    if (u2OffloadMepTxHandle != ECFM_INIT_VAL)
    {
        if (pMepNode->pEcfmMplsParams == NULL)
        {
            EcfmCcmOffDeleteMepTx (u4ContextId, u2OffloadMepTxHandle, pMepNode);
        }
        else
        {
            ECFM_MEMSET (&MplsCcOffTxParams, 0x0,
                         sizeof (tEcfmMplstpCcOffTxInfo));
            MplsCcOffTxParams.u4ContextId = pMepNode->u4ContextId;
            MplsCcOffTxParams.u2TxFilterHandle = pMepNode->u2OffloadMepTxHandle;
            MplsCcOffTxParams.pSlotInfo = NULL;    /* Currently Unused */
            MplsCcOffTxParams.u4CcInterval = pMepNode->pMaInfo->u1CcmInterval;
            MplsCcOffTxParams.pEcfmMplsParams = pMepNode->pEcfmMplsParams;

            if (EcfmMplstpCcOffDeleteTxMep (&MplsCcOffTxParams) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD: EcfmMplstpCcOffDeleteTxMep: Returned "
                                  "failure for MEPID [%u]\r\n",
                                  pMepNode->u2MepId);
                /* Fall through for other cleanup */
            }
        }
        pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;
    }

    ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC,
                      "OFFLOAD: Function FsMiEcfmHwStopCcmTx has "
                      "Returned for MEPID %d with TxHandle Value %d\r\n",
                      pMepNode->u2MepId, u2OffloadMepTxHandle);

    ECFM_CC_TRC_ARG1 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: For MEPId %d Info is Deleted from"
                      "Offloaded Module\r\n", pMepNode->u2MepId);

    pMepNode->u2OffloadMepTxHandle = ECFM_INIT_VAL;

    /* Send the ECFM_SM_EV_BEGIN EVENT TO CCI State Machine  */

    if (pMepNode->pMaInfo->u1CcmInterval == ECFM_CCM_INTERVAL_300Hz)
    {

        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                      "CC while Timer will not be started with"
                      "interval %d in Mep %d\r\n",
                      u4Interval, pMepNode->u2MepId));

        return ECFM_FAILURE;
    }

    ECFM_GET_CCM_INTERVAL (pMepNode->pMaInfo->u1CcmInterval, u4Interval);
    if ((pCcInfo->b1CciEnabled == ECFM_TRUE) &&
        (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
        (pMepNode->b1Active == ECFM_TRUE))
    {
        if (EcfmCcTmrStartTimer (ECFM_CC_TMR_CCI_WHILE, &PduSmInfo,
                                 u4Interval) != ECFM_SUCCESS)
        {
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ECFM_SYSLOG_ID,
                          "CC while Timer will not be started with"
                          "interval %d in Mep %d\r\n",
                          u4Interval, pMepNode->u2MepId));

            return ECFM_FAILURE;
        }

    }

    u2CrossCheckDelay = (UINT1) ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;

    if (u2CrossCheckDelay != 0)
    {
        u4Interval = u4Interval * u2CrossCheckDelay;
    }
    else
    {
        u4Interval = u4Interval * ECFM_XCHK_DELAY_DEF_VAL;
    }

    if ((pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
        (pMepNode->b1Active == ECFM_TRUE))
    {
        pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
        while (pRMepNode != NULL)
        {
            PduSmInfo.pRMepInfo = pRMepNode;

            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_RMEP_WHILE, &PduSmInfo,
                                     u4Interval) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC
                                  | ECFM_OS_RESOURCE_TRC,
                                  "OFFLOAD: Function "
                                  "EcfmCcTmrStartTimer has Returned "
                                  "Failure For MEPID %d on Port %d \r\n",
                                  (pMepNode->u2MepId), (pPortInfo->u4IfIndex));
                return ECFM_FAILURE;
            }

            /* Get the next node form the tree */
            pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                            &(pRMepNode->
                                                              MepDbDllNode));
        }
    }

    ECFM_CC_TRC_ARG1 (ECFM_OS_RESOURCE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: CCI Timer is started in "
                      "Software for MEPID %d\r\n", pMepNode->u2MepId);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      :EcfmCcmOffDeleteRxForAllRMep   
 *
 * Description        : This Routine will call the EcfmCcmOffDeleteRxRMep
 *                      Function for Deletion of REM Info From the OFFloaded
 *                      Module.
 *                      
 * Input(s)           : pMepNode   - Pointer to structure of Mep Info
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : - ECFM_SUCCESS.
 *                      - ECFM_FAILURE
 *
 *****************************************************************************/

PUBLIC VOID
EcfmCcmOffDeleteRxForAllRMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&PduSmInfo, 0, sizeof (tEcfmCcPduSmInfo));
    /* Get the first node from the RBtree of RMepDbTable in
     * MepInfo */
    pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
    while (pRMepNode != NULL)
    {
        /* Assign MepInfo and Remote MEP Info
         * in the PduSmInfo for calling the SM */
        PduSmInfo.pRMepInfo = pRMepNode;
        PduSmInfo.pMepInfo = pRMepNode->pMepInfo;

        /* Call the Remote MEP State Machine for TIME_OUT event */
        EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);

        /* Delete RX for the MEP  */
        if (EcfmCcmOffDeleteRxRMep (pMepNode, pRMepNode) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: EcfmCcmOffDeleteRxRMep function has "
                              "Returned Failure for"
                              " MEPID %d & RMEPID %d \r\n",
                              pMepNode->u2MepId, pRMepNode->u2RMepId);
            pRMepNode->u2OffloadRMepRxHandle = ECFM_INIT_VAL;
        }

        /* Get the next node form the tree */
        pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                        &(pRMepNode->
                                                          MepDbDllNode));
    }
    ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "OFFLOAD:EcfmCcmOffDeleteRxForAllRMep "
                      "Returned Success For MEPID %d Info is"
                      "passed to Hardware for All RMEP \r\n",
                      pMepNode->u2MepId);

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffDeleteRxRMep   
 *
 * Description        : This Routine will Call the NPAPI for the Deletion of
 *                      Information from the OFFLOADED Module for RMEP.
 *                      
 *                      
 * Input(s)           : pMepNode   - Pointer to structure of Mep Info
 *                      pRMepNode -  Pointer to structure of RMepNode 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : - ECFM_SUCCESS.
 *                      - ECFM_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
EcfmCcmOffDeleteRxRMep (tEcfmCcMepInfo * pMepNode,
                        tEcfmCcRMepDbInfo * pRMepNode)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcOffMepRxStats EcfmCcOffMepRxStats;
    tEcfmMplstpCcOffRxInfo MplsCcOffRxParams;

    UINT4               u4ContextId = ECFM_INIT_VAL;
#ifdef TRACE_WANTED
    UINT4               u4NumOfCcmRx = ECFM_INIT_VAL;
#endif
    UINT2               u2OffloadRMepRxHandle = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    ECFM_MEMSET (&EcfmCcOffMepRxStats, ECFM_INIT_VAL,
                 sizeof (tEcfmCcOffMepRxStats));

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);
    UNUSED_PARAM (pMaInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    UNUSED_PARAM (pCcInfo);
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);

    u4ContextId = pPortInfo->u4ContextId;

    PduSmInfo.pMepInfo = pMepNode;
    PduSmInfo.pRMepInfo = pRMepNode;
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    PduSmInfo.pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepNode);

    u2OffloadRMepRxHandle = pRMepNode->u2OffloadRMepRxHandle;

    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "OFFLOAD: RxHandle value For MEPID %d"
                      " with REMID %d is %d \r\n",
                      pMepNode->u2MepId, pRMepNode->u2RMepId,
                      pRMepNode->u2OffloadRMepRxHandle);

    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: RxHandle value For MEPID %d"
                      " with REMID %d is %d \r\n",
                      pMepNode->u2MepId, pRMepNode->u2RMepId,
                      pRMepNode->u2OffloadRMepRxHandle);
    if (u2OffloadRMepRxHandle != ECFM_INIT_VAL)
    {
        if (EcfmGetCcmOffRxStatistics
            (u4ContextId, u2OffloadRMepRxHandle,
             &EcfmCcOffMepRxStats) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG2 (ECFM_OS_RESOURCE_TRC |
                              ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: Function EcfmGetCcmOffRxStatistics has"
                              " Returned Failure For MEPID %d with REMID %d "
                              "\r\n", pMepNode->u2MepId, pRMepNode->u2RMepId);
            return ECFM_FAILURE;
        }
    }
    pRMepNode->u4SeqNum = EcfmCcOffMepRxStats.u4ExpectedSeqNum;

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Received CCM in Offloaded Module are"
                      " MEPID %d with REMID %d is %d \r\n",
                      u4NumOfCcmRx, pMepNode->u2MepId, pRMepNode->u2RMepId);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: Sequence Number Received from"
                      " Offloaded Module for MEPID %d with REMID"
                      " %d is %d .\r\n", u4NumOfCcmRx,
                      pMepNode->u2MepId, pRMepNode->u2RMepId);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Received CCM is %d "
                      "updated in Software for MEPID %d"
                      "with REMID %d \r\n",
                      pPortInfo->u4RxCcmCount, pMepNode->u2MepId,
                      pRMepNode->u2RMepId);

    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Of Received ECFM PDU is %d"
                      "updated in Software from "
                      " Offloaded Module for MEPID %d"
                      "with REMID %d \r\n",
                      pPortInfo->u4RxCcmCount, pMepNode->u2MepId,
                      pRMepNode->u2RMepId);
    ECFM_CC_TRC_ARG3 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: No Sequence Number is %d "
                      " updated in Software for MEPID %d"
                      "with REMID %d is %d.\r\n", pRMepNode->u4SeqNum,
                      pMepNode->u2MepId, pRMepNode->u2RMepId);

    /* Call the NPAPI for Deletion of Information in Offloaded Module */

    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                      "OFFLOAD: CCM Stop Rx NPAPI is called "
                      " for MEP %d Vlan %d on Port %d  \r\n",
                      pMepNode->u2MepId, pMepNode->u4PrimaryVidIsid,
                      pPortInfo->u4IfIndex);

    if (u2OffloadRMepRxHandle != ECFM_INIT_VAL)
    {
        if (pMepNode->pEcfmMplsParams == NULL)
        {
            EcfmCcmOffDeleteMepRx (u4ContextId, u2OffloadRMepRxHandle, pMepNode,
                                   pRMepNode);
        }
        else
        {
            ECFM_MEMSET (&MplsCcOffRxParams, 0x0,
                         sizeof (tEcfmMplstpCcOffRxInfo));
            MplsCcOffRxParams.u4ContextId = pMepNode->u4ContextId;
            MplsCcOffRxParams.u2RxFilterHandle =
                pRMepNode->u2OffloadRMepRxHandle;
            MplsCcOffRxParams.pSlotInfo = NULL;    /* Currently Unused */
            if (EcfmMplstpCcOffDeleteRxMep (&MplsCcOffRxParams) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD: EcfmCcmOffDeleteRxRMep Returned"
                                  "failure for MEPID [%u]\r\n",
                                  pRMepNode->u2RMepId);

                /* Fall through and proceed with other cleanup */
            }
        }

        RBTreeRem (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE, pRMepNode);
        pRMepNode->u2OffloadRMepRxHandle = ECFM_INIT_VAL;
    }

    ECFM_CC_TRC_ARG2 (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "OFFLOAD: MEP Info is Deleted from Offloaded Module for "
                      "MEPID %d with RMEPID %d \r\n", pMepNode->u2MepId,
                      pRMepNode->u2RMepId);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffCreateRxForAllRMep   
 *
 * Description        : This Routine will Create Reception call for each Remote
 *                      MEP.
 *                      
 * Input(s)           : pMepNode   - Pointer to structure of Mep Info
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : - ECFM_SUCCESS.
 *                      - ECFM_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
EcfmCcmOffCreateRxForAllRMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    if (pMepNode->u2OffloadMepTxHandle == ECFM_INIT_VAL)
    {
        return ECFM_SUCCESS;
    }
    /* Get the first node from the RBtree of RMepDbTable in
     * MepInfo */
    pRMepInfo = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
    while (pRMepInfo != NULL)
    {
        /* Create Transmit for the MEP  */
        if (EcfmCcmOffCreateRxRMep (pMepNode, pRMepInfo) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: EcfmCcmOffCreateRxRMep Function has "
                              "Returned Failure for"
                              " MEPID %d with RMEPID %d \r\n",
                              pMepNode->u2MepId, pRMepInfo->u2RMepId);
            pRMepInfo->u2OffloadRMepRxHandle = ECFM_INIT_VAL;
            return ECFM_FAILURE;
        }

        /* Get the next node form the tree */
        pRMepInfo = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                        &(pRMepInfo->
                                                          MepDbDllNode));
    }
    ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "OFFLOAD: EcfmCcmOffCreateRxForAllRMep "
                      "Returned Success For MEPID %d Info is "
                      "passed to Hardware for All RMEP \r\n",
                      pMepNode->u2MepId);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffCreateRxRMep   
 *
 * Description        : This Routine is called by EcfmCcmOffCreateRxForAllRMep
 *                      Function & pass the Reception Information 
 *                      to offloaded Module.
 *                      
 * Input(s)           : pMepNode   - Pointer to structure of Mep Info
 *                      pRmep - Pointer to Structure of RMepNode 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : - ECFM_SUCCESS.
 *                      - ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffCreateRxRMep (tEcfmCcMepInfo * pMepNode, tEcfmCcRMepDbInfo * pRmep)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmMacAddr        DestAddr;
    tEcfmMacAddr        SrcAddr;
    UINT1              *pu1IfFwdPortList = NULL;
    tPortListExt       *pIfFwdActualPortList = NULL;
    tPortListExt       *pUntagIfFwdActualPortList = NULL;
    tEcfmCcOffRxInfo    EcfmCcOffRxInfo;
    tEcfmMplstpCcOffRxInfo MplsCcOffRxParams;

    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT4               u4SeqNo = ECFM_INIT_VAL;
    UINT4               u4RxInterval = ECFM_INIT_VAL;
    UINT4               u4VlanIDIsid = ECFM_INIT_VAL;
    UINT2               u2OffloadRMepRxHandle = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2RemMepId = ECFM_INIT_VAL;
    UINT2               u2Handle = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2MepRxVlanId = ECFM_INIT_VAL;

    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
    UINT1               u1PortState = ECFM_INIT_VAL;
    UINT1               au1Pdu[ECFM_MAC_ADDR_LENGTH + 1];
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    PduSmInfo.pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepNode);
    PduSmInfo.pMepInfo = pMepNode;
    PduSmInfo.pRMepInfo = pRmep;
    PduSmInfo.pPortInfo = pPortInfo;

    u4IfIndex = pPortInfo->u4IfIndex;
    u1MdLevel = pMepNode->u1MdLevel;

    if (pMepNode->pEcfmMplsParams == NULL)
    {
        if (pMepNode->u4PrimaryVidIsid < ECFM_INTERNAL_ISID_MIN)
        {
            u4VlanIDIsid = pMepNode->u4PrimaryVidIsid;
        }
        else
        {
            u4VlanIDIsid = pMepNode->u4PrimaryVidIsid - ECFM_INTERNAL_ISID_MIN;
        }
    }
    u2RemMepId = pRmep->u2RMepId;
    u2MepId = pMepNode->u2MepId;
    u4SeqNo = pRmep->u4SeqNum;
    u2Handle = pRmep->u2OffloadRMepRxHandle;
    u2LocalPort = pMepNode->u2PortNum;

    ECFM_MEMSET (SrcAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (DestAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (au1Pdu, ECFM_INIT_VAL, (ECFM_MAC_ADDR_LENGTH + 1));

    /* Allocating Memory for FwdPortList */
    pu1IfFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1IfFwdPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Error in allocating memory "
                     "for FwdPortList\r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pu1IfFwdPortList, 0, sizeof (tLocalPortListExt));

    /* Get The Destination address */
    ECFM_GEN_MULTICAST_CLASS1_ADDR (DestAddr, pMepNode->u1MdLevel);

    if ((pTempPortInfo = ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum)) == NULL)
    {
        /* Releasing Memory for FwdPortList */
        UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
        return ECFM_SUCCESS;
    }
    if (pMepNode->pEcfmMplsParams == NULL)
    {
        /* Get The Source address */
        ECFM_GET_MAC_ADDR_OF_PORT (pTempPortInfo->u4IfIndex, au1Pdu);

        ECFM_MEMCPY (SrcAddr, au1Pdu, ECFM_MAC_ADDR_LENGTH);
    }
    ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4RxInterval);

    if (pCcInfo->b1CciEnabled != ECFM_TRUE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC,
                          "OFFLOAD: CCI is Not Enabled On Port %d \r\n",
                          pPortInfo->u4IfIndex);
        /* Releasing Memory for FwdPortList */
        UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
        return ECFM_SUCCESS;
    }

    /* Stop the Remote Mep while Timer if running */

    if (!(ECFM_CC_IS_PORT_MODULE_STS_ENABLED (pMepNode->u2PortNum)))
    {
        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC,
                          "OFFLOAD: ECFM Module is Not Enabled On Port %d"
                          "\r\n", pPortInfo->u4IfIndex);
        /* Releasing Memory for FwdPortList */
        UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
        return ECFM_SUCCESS;
    }
    if (pMepNode->b1Active == ECFM_FALSE)
    {
        ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC,
                          "OFFLOAD: MEP %d is Not Active On Port %d"
                          "\r\n", pMepNode->u2MepId, pPortInfo->u4IfIndex);
        /* Releasing Memory for FwdPortList */
        UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
        return ECFM_SUCCESS;
    }

    if (pRmep->u2OffloadRMepRxHandle == ECFM_INIT_VAL)
    {
        gu4MepTxRxHandle = gu4MepTxRxHandle + 1;
        u2OffloadRMepRxHandle = gu4MepTxRxHandle;
    }
    else
    {
        u2MepRxVlanId = (UINT2) pMepNode->u4PrimaryVidIsid;
        u2OffloadRMepRxHandle = pRmep->u2OffloadRMepRxHandle;
    }

    /* Timer Interval Changed to Micro seconds */
    u4RxInterval = u4RxInterval * ECFM_NUM_OF_USEC_IN_A_MSEC;
    /* If Mep is Down then Pass the Information 
     * to Hardware for only this Port */

    if (pMepNode->pEcfmMplsParams != NULL)
    {
        ECFM_MEMSET (&MplsCcOffRxParams, 0x0, sizeof (tEcfmMplstpCcOffRxInfo));
        MplsCcOffRxParams.u4ContextId = pMepNode->u4ContextId;
        MplsCcOffRxParams.pEcfmMplsParams = pMepNode->pEcfmMplsParams;
        MplsCcOffRxParams.pSlotInfo = NULL;    /* Currently Unused */
        MplsCcOffRxParams.u4StartSeqNo = u4SeqNo;
        MplsCcOffRxParams.u4CcmRxTimeout = u4RxInterval;
        MplsCcOffRxParams.u2RMepId = u2RemMepId;
        MplsCcOffRxParams.u1MdLevel = u1MdLevel;
        EcfmCcmOffGenerateMAID (pMepNode, MplsCcOffRxParams.au1MAID);

        ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                          "OFFLOAD: CCM Start Rx NPAPI is called for MEP [%u] with "
                          "RMEPId [%u]\r\n", pMepNode->u2MepId,
                          pRmep->u2RMepId);

        if (EcfmMplstpCcOffCreateRxMep (&MplsCcOffRxParams) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "OFFLOAD: EcfmCcmOffCreateRemoteMepRx: returned "
                              " failure for MEPID [%u] with RMEPId [%u]\r\n",
                              pMepNode->u2MepId, pRmep->u2RMepId);
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
            return ECFM_FAILURE;
        }
    }
    else
    {
        ECFM_MEMSET (&EcfmCcOffRxInfo, ECFM_INIT_VAL,
                     sizeof (tEcfmCcOffRxInfo));
        EcfmCcOffRxInfo.u4StartSeqNo = u4SeqNo;
        EcfmCcOffRxInfo.u4CcmRxTimeout = u4RxInterval;
        EcfmCcOffRxInfo.u4VlanIdIsid = u4VlanIDIsid;
        EcfmCcOffRxInfo.u2RMepId = u2RemMepId;
        EcfmCcOffRxInfo.u1MdLevel = u1MdLevel;
        EcfmCcmOffGenerateMAID (pMepNode, EcfmCcOffRxInfo.au1MAID);

        if (pMepNode->u1Direction == ECFM_MP_DIR_DOWN)
        {
            ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                              "OFFLOAD: CCM Start Rx NPAPI "
                              "is called for MEP %d Vlan"
                              "%d on Port %d  \r\n",
                              pMepNode->u2MepId, u2MepRxVlanId, u4IfIndex);

            if (ECFM_IS_MEP_VLAN_AWARE (pMepNode->u4PrimaryVidIsid))
            {
                if (EcfmL2IwfMiIsVlanActive (ECFM_CC_CURR_CONTEXT_ID (),
                                             pMepNode->u4PrimaryVidIsid) ==
                    OSIX_FALSE)
                {
                    /* Releasing Memory for FwdPortList */
                    UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                    return ECFM_SUCCESS;
                }
            }
            ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                              "OFFLOAD: CCM Start Rx NPAPI is "
                              "called for MEP %d Vlan %d on Port"
                              "%d\r\n", u2MepId, u4VlanIDIsid, u4IfIndex);

            if ((EcfmCcmOffCreateRemoteMepRx
                 (u4IfIndex, u2MepId, &EcfmCcOffRxInfo,
                  u2OffloadRMepRxHandle, pMepNode, pRmep) != ECFM_SUCCESS))

            {
                ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                  " OFFLOAD:EcfmCcmOffCreateRemoteMepRx Returned"
                                  " Failure for MEPID %d with RMEPID %d\r\n",
                                  pMepNode->u2MepId, pRmep->u2RMepId);
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                return ECFM_FAILURE;
            }
        }
        else
        {
            ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                              "OFFLOAD: Direction of MEPID -> %d is UP  \r\n",
                              pMepNode->u2MepId);

            if ((ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)) ||
                (ECFM_IS_MEP_VLAN_AWARE (u4VlanIDIsid)))
            {
                /*Get All the Egress Ports for the MEP */
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                                  "OFFLOAD: MEPID %d is VLAN aware UP MEP"
                                  "\r\n", pMepNode->u2MepId);

                pIfFwdActualPortList =
                    (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));

                if (pIfFwdActualPortList == NULL)
                {
                    ECFM_CC_TRC (ECFM_CRITICAL_TRC | ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcmOffCreateRxRMep: Error in Allocating "
                                 " memory for bitlist \r\n");
                    /* Releasing Memory for FwdPortList */
                    UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                    return ECFM_FAILURE;
                }

                pUntagIfFwdActualPortList =
                    (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));

                if (pUntagIfFwdActualPortList == NULL)
                {
                    ECFM_CC_TRC (ECFM_CRITICAL_TRC | ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcmOffCreateRxRMep: Error in Allocating "
                                 " memory for bitlist \r\n");
                    /* Releasing Memory for FwdPortList */
                    UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                    FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
                    return ECFM_FAILURE;
                }

                ECFM_MEMSET (*pIfFwdActualPortList, ECFM_INIT_VAL,
                             sizeof (tPortListExt));
                ECFM_MEMSET (*pUntagIfFwdActualPortList, ECFM_INIT_VAL,
                             sizeof (tPortListExt));

                if (EcfmVlanGetFwdPortList
                    (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort, SrcAddr,
                     DestAddr, u4VlanIDIsid, pu1IfFwdPortList) != VLAN_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 " OFFLOAD: EcfmVlanGetFwdPortList Function Returned"
                                 " Failure Unable to get Egress Ports\r\n");
                    /* Releasing Memory for FwdPortList */
                    UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                    FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
                    FsUtilReleaseBitList ((UINT1 *) pUntagIfFwdActualPortList);
                    return ECFM_FAILURE;
                }
                ECFM_RESET_MEMBER_PORT (pu1IfFwdPortList, u2LocalPort);
                /* Remove the Port From the PortList If Port State is Discarding */
                /*Get the egress Port and forward the packet */
                for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
                     u2ByteInd++)
                {
                    if (pu1IfFwdPortList[u2ByteInd] == 0)
                    {
                        continue;
                    }
                    u1PortFlag = pu1IfFwdPortList[u2ByteInd];
                    for (u2BitIndex = 0;
                         ((u2BitIndex < BITS_PER_BYTE)
                          && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex)
                              != 0)); u2BitIndex++)
                    {
                        u2Port =
                            (u2ByteInd * BITS_PER_BYTE) +
                            EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
                        /*Reset the port info */
                        pPortInfo = NULL;
                        /*Get the port info */
                        pPortInfo = ECFM_CC_GET_PORT_INFO (u2Port);
                        if (pPortInfo != NULL)
                        {
                            /* Check if Spanning Tree is enabled in this Context */
                            if (EcfmIsStpEnabledInContext
                                (ECFM_CC_CURR_CONTEXT_ID () != ECFM_SUCCESS))
                            {
                                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                             ECFM_CONTROL_PLANE_TRC,
                                             "EcfmCcmOffCreateRxRMep:"
                                             "STP is disabled in this Context\r\n");

                                u1PortState = AST_PORT_STATE_FORWARDING;
                            }
                            else
                            {
                                u1PortState =
                                    EcfmL2IwfGetVlanPortState (u4VlanIDIsid,
                                                               pPortInfo->
                                                               u4IfIndex);
                            }
                            /* Port status is not forwarding */
                            if (u1PortState != AST_PORT_STATE_FORWARDING)
                            {
                                if (EcfmL2IwfMiIsVlanUntagMemberPort
                                    (pPortInfo->u4ContextId, u4VlanIDIsid,
                                     pPortInfo->u4IfIndex) == ECFM_TRUE)
                                {
                                    ECFM_RESET_MEMBER_PORT
                                        ((*pUntagIfFwdActualPortList),
                                         pPortInfo->u4IfIndex);
                                }
                                else
                                {
                                    ECFM_RESET_MEMBER_PORT
                                        ((*pIfFwdActualPortList),
                                         pPortInfo->u4IfIndex);
                                }
                            }
                            else
                            {
                                if (EcfmL2IwfMiIsVlanUntagMemberPort
                                    (pPortInfo->u4ContextId, u4VlanIDIsid,
                                     pPortInfo->u4IfIndex) == ECFM_TRUE)
                                {
                                    /*Reset the Port Info Ptr */
                                    pTempPortInfo = NULL;
                                    if ((ECFM_CC_ISPORT_SISP_LOG (u2Port,
                                                                  pTempPortInfo))
                                        == ECFM_FALSE)
                                    {
                                        ECFM_SET_MEMBER_PORT
                                            ((*pUntagIfFwdActualPortList),
                                             pPortInfo->u4IfIndex);
                                    }
                                    else
                                    {
                                        ECFM_SET_MEMBER_PORT
                                            ((*pUntagIfFwdActualPortList),
                                             pPortInfo->u4PhyPortNum);
                                    }
                                }
                                else
                                {
                                    /*Reset the Port Info Ptr */
                                    pTempPortInfo = NULL;
                                    if ((ECFM_CC_ISPORT_SISP_LOG (u2Port,
                                                                  pTempPortInfo))
                                        == ECFM_FALSE)
                                    {
                                        ECFM_SET_MEMBER_PORT
                                            ((*pIfFwdActualPortList),
                                             pPortInfo->u4IfIndex);
                                    }
                                    else
                                    {
                                        ECFM_SET_MEMBER_PORT
                                            ((*pIfFwdActualPortList),
                                             pPortInfo->u4PhyPortNum);
                                    }
                                }
                            }
                        }
                    }
                }
                /* Call the NPAPI fot Tx of Up Mep */

                EcfmCcOffRxInfo.u4StartSeqNo = u4SeqNo;
                EcfmCcOffRxInfo.u4CcmRxTimeout = u4RxInterval;
                EcfmCcOffRxInfo.u4VlanIdIsid = u4VlanIDIsid;
                EcfmCcOffRxInfo.u2RMepId = u2RemMepId;
                EcfmCcOffRxInfo.u1MdLevel = u1MdLevel;
                EcfmCcmOffGenerateMAID (pMepNode, EcfmCcOffRxInfo.au1MAID);

                ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                                  "OFFLOAD: CCM Start Rx NPAPI is"
                                  " called for MEP %d Vlan"
                                  "%d on Port %d  \r\n",
                                  pMepNode->u2MepId, u4VlanIDIsid,
                                  pMepNode->pPortInfo->u4IfIndex);

                if (EcfmStartCcmOffCreateMepRxOnPortList
                    (*pIfFwdActualPortList, *pUntagIfFwdActualPortList,
                     u2MepId, &EcfmCcOffRxInfo, ECFM_CC_CURR_CONTEXT_ID (),
                     u2OffloadRMepRxHandle, pMepNode, pRmep) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                 "OFFLOAD:EcfmStartCcmOffCreateMepRxOnPortList"
                                 " Function Returned Failure  \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
                    FsUtilReleaseBitList ((UINT1 *) pUntagIfFwdActualPortList);
                    UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                    return ECFM_FAILURE;
                }
                ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                                  "OFFLOAD: The Rx Handle Vlaue returned from"
                                  "hardware for MEPID %d & RMEPID is %d is %d\r\n",
                                  pMepNode->u2MepId, pRmep->u2RMepId, u2Handle);
                FsUtilReleaseBitList ((UINT1 *) pIfFwdActualPortList);
                FsUtilReleaseBitList ((UINT1 *) pUntagIfFwdActualPortList);
            }
            else
            {
                UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
                return ECFM_FAILURE;
            }
        }
    }
    ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                      "OFFLOAD: For MEPID %d EcfmCcmOffCreateRxRMep Function"
                      "Returned Success\r\n", pMepNode->u2MepId);
    pRmep->u2OffloadRMepRxHandle = u2OffloadRMepRxHandle;
    EcfmRedSynchOffRxFilterId (ECFM_CC_CURR_CONTEXT_ID (), pMepNode->u4MdIndex,
                               pMepNode->u4MaIndex,
                               pMepNode->u2MepId,
                               pRmep->u2RMepId, u2OffloadRMepRxHandle);
    /* Add in global RB-Tree */
    if (RBTreeAdd (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE, pRmep) == ECFM_RB_FAILURE)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcIfHandleCreatePort: Unable to add RMepDb in global"
                     " RB-Tree\n");
        UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
        return ECFM_SUCCESS;
    }

    ECFM_CC_TRC_FN_EXIT ();
    UtilPlstReleaseLocalPortList (pu1IfFwdPortList);
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffGenerateMAID
 *
 * Description        : This rotuine is used to generate the MAID
 * 
 * Input(s)           : au1MAID - pointer 
 *                      pMepInfo - Pointer to the Mep structure that stores the 
 *                      information regarding MEP info.
 * Output(s)          : None

 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcmOffGenerateMAID (tEcfmCcMepInfo * pMepInfo, UINT1 *pu1MAID)
{
    ECFM_CC_TRC_FN_ENTRY ();

    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->pPortInfo->u2PortNum))
    {
        /* Fills next 48 byte as MEGID, with Icc and Umc Codes */
        EcfmUtilPutMEGID (pMepInfo, pu1MAID);
    }
    else
    {
        /* Fills next 48 byte MAID as per the IEEE 802.1ag standard */
        EcfmUtilPutMAID (pMepInfo, pu1MAID);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffHwRegisterCallBack
 *
 * Description        : This rotuine is used to register Rx Call back with
 *                      hardware to handle Reception Timer Expiry Event in
 *                      Software.
 * 
 * Input(s)           : u4ContextId - Context Id 
 *  
 * Output(s)          : u4RxHandle - Handle for Reception information updated in
 *                                   offloaded module.
 *                      u4PortNo   - Interface Number on which reception  call
 *                                   is configured.
 * 
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcmOffHwRegisterCallBack (UINT4 u4ContextId)
{
    ECFM_CC_TRC_FN_ENTRY ();

#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        EcfmFsMiEcfmHwRegister (u4ContextId);
    }
#else
    UNUSED_PARAM (u4ContextId);
#endif

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffHandleTxFailureCallBack
 *
 * Description        : This rotuine is used to handle the call back recevied
 *                      from hardware. This will posts an event to ECFM CC Task.
 * 
 * Input(s)           : u4ContextId -Current Context Id
 *
 *                      u4TxHandle - Receive Handle for the remote MEP for which
 *                                   the Tx Failed.
 *                         
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffHandleTxFailureCallBack (UINT4 u4ContextId, UINT2 u2TxHandle)
{
    tEcfmCcMsg         *pMsg = NULL;
    /* Send Event to CC Task */
    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmCcmOffHandleTxFailureCallBack:"
                     "MemBlock Allocation Failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Node */
    pMsg->MsgType = (tEcfmMsgType) ECFM_TX_FAILURE_CALL_BACK;
    pMsg->uMsg.Indications.u2CcmOffloadHandle = u2TxHandle;
    pMsg->u4ContextId = u4ContextId;
    /* Sending Message and Event to own task */
    if (EcfmCcInterruptQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmOffHandleTxFailureCallBack: "
                     "Event Queuing Failed \n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffHandleTxFailureCallBack
 *
 * Description        : This rotuine is used to handle the call back recevied
 *                      from hardware. This will posts an event to ECFM CC Task.
 * 
 * Input(s)           : u4ContextId - Current Context Id
 *                    : pau1HwHandle - Receive H/W Handle for the remote MEP
 *                                     for which the Tx Failed.
 *                         
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmHwHandleTxFailureCallBack (UINT4 u4ContextId, UINT1 *pau1HwHandle)
{
    tEcfmCcMsg         *pMsg = NULL;
    /* Send Event to CC Task */
    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmCcmOffHandleTxFailureCallBack:"
                     "MemBlock Allocation Failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Node */
    pMsg->MsgType = (tEcfmMsgType) ECFM_TX_FAILURE_CALL_BACK_FROM_HW;
    MEMCPY (pMsg->uMsg.HwCalBackParam.au1HwHandler, pau1HwHandle,
            ECFM_HW_MEP_HANDLER_SIZE);
    pMsg->u4ContextId = u4ContextId;
    /* Sending Message and Event to own task */
    if (EcfmCcInterruptQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmHwHandleTxFailureCallBack: "
                     "Event Queuing Failed \n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmHwHandleTxFailureEvt
 *
 * Description        : This rotuine is used to handle the Tx Failure call 
 *                      back event received in CC Task.
 * 
 * Input(s)           : u4ContextId - Context Id 
 *                    : pau1HwHandle - Tx H/W Handle for the MEP for
 *                                     which CCM TX Failed.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmHwHandleTxFailureEvt (UINT4 u4ContextId, UINT1 *pau1HwHandle)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    MEMCPY (gpEcfmCcMepNode->au1HwMepHandler, pau1HwHandle,
            ECFM_HW_MEP_HANDLER_SIZE);

    pMepInfo = (tEcfmCcMepInfo *) RBTreeGet (ECFM_CC_GLOBAL_HW_MEP_TABLE,
                                             gpEcfmCcMepNode);
    if (pMepInfo != NULL)
    {
        ECFM_CC_HANDLE_TRANSMIT_FAILURE (pMepInfo->pPortInfo->u2PortNum,
                                         ECFM_OPCODE_CCM);
    }

    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffHandleCallBackEvt
 *
 * Description        : This rotuine is used to handle the call back event
 *                      received in CC Task.
 * 
 * Input(s)           : u4ContextId - Context Id 
 *                    : u4RxHandle - Receive Handle for the remote MEP for which
 *                                   the reception timer expired.
 *                      u4IfIndex - Interface Index
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffHandleCallBackEvt (UINT4 u4ContextId, UINT2 u2RxHandle,
                             UINT4 u4IfIndex)
{

    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcRMepDbInfo   RmepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
#ifdef MBSM_WANTED
    INT4                i4SlotId;
#endif
    ECFM_MEMSET (&RmepInfo, ECFM_INIT_VAL, sizeof (tEcfmCcRMepDbInfo));
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();
    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }
    RmepInfo.u2OffloadRMepRxHandle = u2RxHandle;

    /* Get Node from Global RBTree maintained for Remote MEP */
    pRmepInfo =
        (tEcfmCcRMepDbInfo *) RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE,
                                         &RmepInfo);
#ifndef MBSM_WANTED
    UNUSED_PARAM (u4IfIndex);

    if (pRmepInfo != NULL)
    {
        /* Get MEP Node with which this Remote
         * MEP is associated */
        pMepInfo = pRmepInfo->pMepInfo;

        if (pMepInfo->u1Direction == ECFM_MP_DIR_UP)
        {

            /* Assign MepInfo and Remote MEP Info 
             * in the PduSmInfo for calling the SM */
            PduSmInfo.pRMepInfo = pRmepInfo;
            PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                              "Software received the CCM "
                              "Timer Reception Expiry event"
                              "for MEPID %d\r\n", PduSmInfo.pMepInfo->u2MepId);
            /* Call the Remote MEP State Machine for TIME_OUT event */
            EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
        }
        else
        {
            /*For Down Meps if LOC Defect not present then call RMEP SM */
            if (pRmepInfo->b1RMepCcmDefect != ECFM_TRUE)
            {
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                                  "Software received the CCM "
                                  "Timer Reception Expiry event"
                                  "for MEPID %d\r\n",
                                  PduSmInfo.pMepInfo->u2MepId);
                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            }
        }
    }
#else /*MBSM_WANTED */

    if (u4IfIndex == 0)
    {
        return ECFM_FAILURE;
    }

    if (pRmepInfo != NULL)
    {
        /* Get MEP Node with which this Remote
         * MEP is associated */
        pMepInfo = pRmepInfo->pMepInfo;

        if (MbsmGetSlotFromPort (u4IfIndex, &i4SlotId) == MBSM_SUCCESS)
        {
            ECFM_RESET_LC_STATUS_ON_NOTIFICATION_RX
                (&(pRmepInfo->u1MepCcmLCStatus), i4SlotId);
        }
        /* For Down MEP raise connectivity loss trap.
         * For Up MEP check if all LCs sends receives notification for 
         * connectivity loss trap then generate trap */
        if (pMepInfo->u1Direction == ECFM_MP_DIR_UP)
        {
            /*Check if all the LCs are not receiving PDU then raise Trap */
            if (pRmepInfo->u1MepCcmLCStatus == ECFM_MBSM_LC_STATUS)
            {
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                                  "Software received the CCM "
                                  "Timer Reception Expiry event"
                                  "for MEPID %d\r\n",
                                  PduSmInfo.pMepInfo->u2MepId);

                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            }
        }
        else
        {
            /*Check if CC Defect already not present for this RMEP */
            if (pRmepInfo->b1RMepCcmDefect != ECFM_TRUE)
            {
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                                  "Software received the CCM "
                                  "Timer Reception Expiry event"
                                  "for MEPID %d\r\n",
                                  PduSmInfo.pMepInfo->u2MepId);

                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            }
        }
    }
#endif

    if (pMepInfo != NULL)
    {
        if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
        {
            if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_TRUE) ||
                (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_TRUE) ||
                (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE) ||
                (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_TRUE))
            {
                /* set presentRDI to True */
                pMepInfo->b1PresentRdi = ECFM_TRUE;
            }
            else
            {
                /* set presentRDI to False */
                pMepInfo->b1PresentRdi = ECFM_FALSE;
            }

            /* Check if the RDI bit is changed */
            if (pMepInfo->b1PresentRdi != pMepInfo->b1CcmOffloadLastRdi)
            {
                /* If RDI bit is changed we need to configure the offloaded
                 * module with the new CCM updated for the changed RDI value
                 */
                EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                if (EcfmCcmOffCreateTxRxForMep (pMepInfo) != ECFM_SUCCESS)
                {
                    return ECFM_FAILURE;
                }

                pMepInfo->b1CcmOffloadLastRdi = pMepInfo->b1PresentRdi;
            }
        }
    }
    /* Clearing values of PduSmInfo structure */
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}
#ifdef VTSS_SERVAL
/****************************************************************************
 * Function Name      : EcfmCcmOffHandleCallBackVttsEvt
 *
 * Description        : This rotuine is used to handle the call back event
 *                      received in CC Task.
 * 
 * Input(s)           : u4ContextId - Context Id 
 *                    : u4RxHandle - Receive Handle for the remote MEP for which
 *                                   the reception timer expired.
 *                      u4IfIndex - Interface Index
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffHandleCallBackVtssEvt (tEcfmCcMsg * pQMsg)
{
    UINT4 u4ContextId   = 0;
    UINT2 u2RxHandle    = 0;
    UINT4 u4IfIndex     = 0;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmMplsParams    *pMplsParams;
    tEcfmCcRMepDbInfo  *pRMep = NULL;
    tEcfmMacAddr        SrcMacAddr;
    tEcfmHwEvents       EcfmRcvdHwEvents;
    tEcfmHwParams       EcfmHwInfo;
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_MEMSET (gpEcfmCcMepNode, 0, sizeof (tEcfmCcMepInfo));
    ECFM_MEMSET (&SrcMacAddr, 0, sizeof (tEcfmMacAddr));

    u4ContextId = pQMsg->u4ContextId;
    u2RxHandle  = pQMsg->uMsg.Indications.u2CcmOffloadHandle;
    u4IfIndex   = pQMsg->u4IfIndex;

    gpEcfmCcMepNode->au1HwMepHandler[0] = pQMsg->uMsg.HwCalBackParam.au1HwHandler[0];
    pMepInfo = (tEcfmCcMepInfo *)
                RBTreeGet (ECFM_CC_GLOBAL_HW_MEP_TABLE,
		gpEcfmCcMepNode);
    ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
       		"EcfmCcInterruptQueueHandler: Event Received %x\r\n",
 		pQMsg->uMsg.HwCalBackParam.EcfmHwEvents);

    if (pMepInfo != NULL)
    {
        if (ECFM_CC_SELECT_CONTEXT (pMepInfo->pPortInfo->u4ContextId) 
	    != ECFM_SUCCESS)
	{
	    return;
	}
    }
    else
    {
	/* If the entry is not present then also event
	 * should be enabled in the hardware using the 
	 * hardwrae MEP handler for which the event received.
	 */
	if (ECFM_CC_SELECT_CONTEXT (ECFM_DEFAULT_CONTEXT)
			!= ECFM_SUCCESS)
	{
		return;
	}
	ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
			"EcfmCcInterruptQueueHandler: MEP"
			" instance %d not found in the RbTree"
				" \r\n", gpEcfmCcMepNode->au1HwMepHandler[0]);

	MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

	EcfmHwInfo.EcfmHwMepParams.u2MepId = 
		(UINT2) gpEcfmCcMepNode->au1HwMepHandler[0];

	if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_GET_HW_MEP_STATUS,
				&EcfmHwInfo) != FNP_SUCCESS)
	{
	    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
			"EcfmCcInterruptQueueHandler: NPAPI"
			"for Get MEP Status returned"
			"FAILURE\r\n");
	}

	EcfmHwInfo.EcfmHwEvents = ECFM_HW_ALL_EVENT_BITS;

	if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_ENABLE_HW_EVENTS,
				&EcfmHwInfo) != FNP_SUCCESS)
	{
	    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
			"EcfmCcInterruptQueueHandler: NPAPI"
			"to Enable Events for MEP returned"
			"FAILURE\r\n");
	}
	return;
    }
    EcfmRcvdHwEvents = pQMsg->uMsg.HwCalBackParam.EcfmHwEvents;
    /* Get the actual MEP status from Hardware */
    if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
    {
	MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

	EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, NULL);

	if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_GET_HW_MEP_STATUS,
				&EcfmHwInfo) != FNP_SUCCESS)
	{
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
	    "EcfmCcInterruptQueueHandler: NPAPI"
	    "for Get MEP Status returned"
	    "FAILURE\r\n");
	}
	else
        {
	    ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
            "EcfmCcInterruptQueueHandler: MEP Status %x\r\n",
	    EcfmHwInfo.EcfmHwEvents);

            ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
            pRMep = (tEcfmCcRMepDbInfo *)
	            TMO_DLL_First (&(pMepInfo->RMepDb));

            /* When a MEP is having more than one remote MEP then
             * VOE will not process CCM and send all the received
         * CCMs for that MEP to control plane.
	 * So when events are received for a MEP then that will
	 * be for the only remote MEP present in tEcfmCcRMepDbInfo.
	 */
	if (pRMep != NULL)
	{
		PduSmInfo.pRMepInfo = pRMep;
		PduSmInfo.pMepInfo = pRMep->pMepInfo;

		if (EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_LOC)
		{
			if (EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_LOC_BIT)
			{
				EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
			}
			else
			{
				EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_CCM_RCVD);
				if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
							&EcfmHwInfo) != FNP_SUCCESS)
				{
					ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
							"EcfmCcInterruptQueueHandler: NPAPI"
							"for Hit-me-Once returned"
							"FAILURE\r\n");
				}
			}
		}
		if ((EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_MEG_ID) ||
				(EcfmRcvdHwEvents & ECFM_HW_EVENT_MEG_LEVEL))
		{
			if ((EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_MEG_ID_BIT) || 
					(EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_MEG_LEVEL_BIT))
			{
				EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
			}
			else
			{
				/* Cross-connect defect clear, below hit-me-once invocation
				 * sends CCM packet to control plane */
				EcfmCcClntXconSm (&PduSmInfo, ECFM_SM_EV_XCON_TIMEOUT);
			}
			if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
						&EcfmHwInfo) != FNP_SUCCESS)
			{
				ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
						"EcfmCcInterruptQueueHandler: NPAPI"
						"for Hit-me-Once returned"
						"FAILURE\r\n");
			}
		}
		if ((EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_PERIOD) ||
				(EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_PRIORITY) ||
				(EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_ZERO_PERIOD) ||
				(EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_MEP_ID))
		{
			if ((EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_PERIOD_BIT) ||
					(EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_PRIORITY_BIT) ||
					(EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_ZERO_PERIOD_BIT) ||
					(EcfmHwInfo.EcfmHwEvents & ECFM_HW_CCM_MEP_ID_BIT))
			{
				EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
			}
			else
			{
				EcfmCcClntRmepErrSm (&PduSmInfo, ECFM_SM_EV_RMEP_ERR_TIMEOUT);
			}
			if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
						&EcfmHwInfo) != FNP_SUCCESS)
			{
				ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
						"EcfmCcInterruptQueueHandler: NPAPI"
						"for Hit-me-Once returned"
						"FAILURE\r\n");
			}
		}
		if (EcfmRcvdHwEvents & ECFM_HW_EVENT_CCM_RX_RDI)
		{
			if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
						&EcfmHwInfo) != FNP_SUCCESS)
			{
				ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
						"EcfmCcInterruptQueueHandler: NPAPI"
						"for Hit-me-Once returned"
						"FAILURE\r\n");
			}
		}
		}
		else
		{
			/* If remote MEP is not present then check for
			 * ECFM_HW_EVENT_CCM_MEP_ID, which is beacuse of receiving
			 * CCMs for non-existing remote mep in the cross-check list.
			 */
			if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
						&EcfmHwInfo) != FNP_SUCCESS)
			{
				ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
						"EcfmCcInterruptQueueHandler: NPAPI"
						"for Hit-me-Once returned"
						"FAILURE\r\n");
			}
		}
        }
        /* Enbale the events for the MEP in the hardware*/
        EcfmHwInfo.EcfmHwEvents = ECFM_HW_ALL_EVENT_BITS;
	if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_ENABLE_HW_EVENTS,
				&EcfmHwInfo) != FNP_SUCCESS)
	{
	    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
			"EcfmCcInterruptQueueHandler: NPAPI"
			"to Enable Events for MEP returned"
			"FAILURE\r\n");
	}
        return ECFM_SUCCESS;
    }
    return ECFM_SUCCESS;
}
#endif
/****************************************************************************
 * Function Name      : EcfmCcmOffHandleTxFailureEvt
 *
 * Description        : This rotuine is used to handle the Tx Failure call 
 *                      back event received in CC Task.
 * 
 * Input(s)           : u4ContextId - Context Id 
 *                    : u4TxHandle - Tx Handle for the MEP for which CCM TX
 *                                   Failed.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffHandleTxFailureEvt (UINT4 u4ContextId, UINT2 u2TxHandle)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();
    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            gpEcfmCcMepNode->u2OffloadMepTxHandle = u2TxHandle;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);
            /* Disable Tx/Rx for all MEPs configured on this port with 
             * the given VLAN Id 
             * */
            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);
                /*generate the CCM Tx Failure Trap */
                if (pMepInfo->u2OffloadMepTxHandle == u2TxHandle)
                {
                    ECFM_CC_HANDLE_TRANSMIT_FAILURE (pPortInfo->u2PortNum,
                                                     ECFM_OPCODE_CCM);
                    break;
                }
                /* Get Next configured MEP on this port */
                pMepInfo = pNextMepInfo;
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}
#ifdef VTSS_SERVAL
PUBLIC INT4
EcfmCcmOffHandleVoeEvents (tEcfmHwEvents EcfmHwEvents,
                           UINT1         au1HwHandler[])
{
    tEcfmCcMsg         *pMsg = NULL;

    /* Send Event to CC Task */
    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmCcmOffHandleRxCallBack:"
                     "MemBlock Allocation Failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));
    /* Updating Message Node */
    pMsg->MsgType = (tEcfmMsgType) ECFM_CALL_BACK;
    pMsg->uMsg.HwCalBackParam.au1HwHandler[0] = au1HwHandler[0];
    pMsg->uMsg.HwCalBackParam.EcfmHwEvents = EcfmHwEvents;
    /* Sending Message and Event to own task */
    if (EcfmCcInterruptQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmOffHandleRxCallBack: Event Queuing Failed \n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        /* Send an event that Interrupt Queue Failure has occured */
        ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_INT_QUEUE_OVERFLOW);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}
#endif

/****************************************************************************
 * Function Name      : EcfmCcmOffHandleRxCallBack
 *
 * Description        : This rotuine is used to handle the call back recevied
 *                      from hardware. This will posts an even to ECFM CC Task.
 * 
 * Input(s)           : u4ContextId - Context Id 
 *                    : u4RxHandle - Receive Handle for the remote MEP for which
 *                                   the reception timer expired.
 *                      u4IfIndex - Interface Index
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffHandleRxCallBack (UINT4 u4ContextId, UINT2 u2RxHandle,
                            UINT4 u4IfIndex)
{
    tEcfmCcMsg         *pMsg = NULL;

    /* Send Event to CC Task */
    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmCcmOffHandleRxCallBack:"
                     "MemBlock Allocation Failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Node */
    pMsg->MsgType = (tEcfmMsgType) ECFM_CALL_BACK;
    pMsg->uMsg.Indications.u2CcmOffloadHandle = u2RxHandle;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4IfIndex = u4IfIndex;
    /* Sending Message and Event to own task */
    if (EcfmCcInterruptQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmOffHandleRxCallBack: Event Queuing Failed \n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        /* Send an event that Interrupt Queue Failure has occured */
        ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_INT_QUEUE_OVERFLOW);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/**************************************************************************
 * Function Name  : EcfmCcmHwHandleRxCallBack
 *
 * Description    : This rotuine is used to handle the Rx call back recevied
 *                  from hardware. This will posts an event to ECFM CC Task.
 * 
 * Input(s)       : u4ContextId -Current Context Id
 *                  pau1HwHandler - Receive Handle for the remote MEP for which
 *                                  the Rx Failed.
 *                  u4IfIndex - Interface index.
 *                               
 * Output(s)      : None
 * Return Value(s): ECFM_SUCCESS / ECFM_FAILURE
 *************************************************************************/
PUBLIC INT4
EcfmCcmHwHandleRxCallBack (UINT4 u4ContextId, UINT1 *pau1HwHandler,
                           UINT4 u4IfIndex)
{
    tEcfmCcMsg         *pMsg = NULL;

    /* Send Event to CC Task */
    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Node */
    pMsg->MsgType = (tEcfmMsgType) ECFM_RX_CALL_BACK_FROM_HW;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4IfIndex = u4IfIndex;
    MEMCPY (pMsg->uMsg.HwCalBackParam.au1HwHandler, pau1HwHandler,
            ECFM_HW_MEP_HANDLER_SIZE);

    /* Sending Message and Event to own task */
    if (EcfmCcInterruptQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmHwHandleRxCallBackEvt
 *
 * Description        : This rotuine is used to handle the H/W call 
 *                      back event received in CC Task.
 * 
 * Input(s)           : u4ContextId - Context Id
 *                      pau1HwHandler - Receive H/W Handle for the remote MEP
 *                                      for which the reception timer expired.
 *                      u4IfIndex - Interface index 
 * Output(s)          : None
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmHwHandleRxCallBackEvt (UINT4 u4ContextId, UINT1 *pau1HwHandler,
                              UINT4 u4IfIndex)
{
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcRMepDbInfo   RmepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;

#ifdef MBSM_WANTED
    INT4                i4SlotId;
#endif
    ECFM_MEMSET (&RmepInfo, ECFM_INIT_VAL, sizeof (tEcfmCcRMepDbInfo));
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();
    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    MEMCPY (RmepInfo.au1HwRMepHandler, pau1HwHandler, ECFM_HW_MEP_HANDLER_SIZE);

    /* Get Node from Global RBTree maintained for Remote MEP */
    pRmepInfo =
        (tEcfmCcRMepDbInfo *) RBTreeGet (ECFM_CC_GLOBAL_HW_RMEP_TABLE,
                                         &RmepInfo);
#ifndef MBSM_WANTED
    UNUSED_PARAM (u4IfIndex);

    if (pRmepInfo != NULL)
    {
        /* Get MEP Node with which this Remote
         * MEP is associated */
        pMepInfo = pRmepInfo->pMepInfo;

        if (pMepInfo->u1Direction == ECFM_MP_DIR_UP)
        {

            /* Assign MepInfo and Remote MEP Info 
             * in the PduSmInfo for calling the SM */
            PduSmInfo.pRMepInfo = pRmepInfo;
            PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                              "Software received the CCM "
                              "Timer Reception Expiry event"
                              "for MEPID %d\r\n", PduSmInfo.pMepInfo->u2MepId);
            /* Call the Remote MEP State Machine for TIME_OUT event */
            EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
        }
        else
        {
            /*For Down Meps if LOC Defect not present then call RMEP SM */
            if (pRmepInfo->b1RMepCcmDefect != ECFM_TRUE)
            {
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                                  "Software received the CCM "
                                  "Timer Reception Expiry event"
                                  "for MEPID %d\r\n",
                                  PduSmInfo.pMepInfo->u2MepId);
                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            }
        }
    }
#else /*MBSM_WANTED */

    if (u4IfIndex == 0)
    {
        return ECFM_FAILURE;
    }

    if (pRmepInfo != NULL)
    {
        /* Get MEP Node with which this Remote
         * MEP is associated */
        pMepInfo = pRmepInfo->pMepInfo;

        if (MbsmGetSlotFromPort (u4IfIndex, &i4SlotId) == MBSM_SUCCESS)
        {
            ECFM_RESET_LC_STATUS_ON_NOTIFICATION_RX
                (&(pRmepInfo->u1MepCcmLCStatus), i4SlotId);
        }
        /* For Down MEP raise connectivity loss trap.
         * For Up MEP check if all LCs sends receives notification for 
         * connectivity loss trap then generate trap */
        if (pMepInfo->u1Direction == ECFM_MP_DIR_UP)
        {
            /*Check if all the LCs are not receiving PDU then raise Trap */
            if (pRmepInfo->u1MepCcmLCStatus == ECFM_MBSM_LC_STATUS)
            {
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                                  "Software received the CCM "
                                  "Timer Reception Expiry event"
                                  "for MEPID %d\r\n",
                                  PduSmInfo.pMepInfo->u2MepId);

                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            }
        }
        else
        {
            /*Check if CC Defect already not present for this RMEP */
            if (pRmepInfo->b1RMepCcmDefect != ECFM_TRUE)
            {
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                                  "Software received the CCM "
                                  "Timer Reception Expiry event"
                                  "for MEPID %d\r\n",
                                  PduSmInfo.pMepInfo->u2MepId);

                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
            }
        }
    }
#endif

    if (pMepInfo != NULL)
    {
        if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
        {
            if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_TRUE) ||
                (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_TRUE) ||
                (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE) ||
                (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_TRUE))
            {
                /* set presentRDI to True */
                pMepInfo->b1PresentRdi = ECFM_TRUE;
            }
            else
            {
                /* set presentRDI to False */
                pMepInfo->b1PresentRdi = ECFM_FALSE;
            }

            /* Check if the RDI bit is changed */
            if (pMepInfo->b1PresentRdi != pMepInfo->b1CcmOffloadLastRdi)
            {
                /* If RDI bit is changed we need to configure the offloaded
                 * module with the new CCM updated for the changed RDI value
                 */
                EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                if (EcfmCcmOffCreateTxRxForMep (pMepInfo) != ECFM_SUCCESS)
                {
                    return ECFM_FAILURE;
                }

                pMepInfo->b1CcmOffloadLastRdi = pMepInfo->b1PresentRdi;
            }
        }
    }
    /* Clearing values of PduSmInfo structure */
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMplstpOffHandleAisCond
 *
 * Description        : This rotuine is used to handle the call back for AIS
 *                      condition on MPLS-TP based MEPs from hardware. 
 *                      This will posts an even to ECFM CC Task.
 * 
 * Input(s)           : pAisOffParams - Pointer to AIS information.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplstpOffHandleAisCond (tEcfmMplstpAisOffParams * pAisOffParams)
{
    tEcfmCcMsg         *pMsg = NULL;

    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmMplstpOffHandleAisCond: "
                     "MemBlock Allocation Failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    if (pAisOffParams->b1AisCondition == TRUE)
    {
        pMsg->MsgType = (tEcfmMsgType) ECFM_AIS_COND_ENTRY;
    }
    else
    {
        pMsg->MsgType = (tEcfmMsgType) ECFM_AIS_COND_EXIT;
    }

    MEMCPY (&(pMsg->uMsg.MplstpAisOffParams),
            pAisOffParams, sizeof (tEcfmMplstpAisOffParams));

    /* Sending Message and Event to CC task */
    if (EcfmCcInterruptQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC |
                     ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplstpOffHandleAisCond: AIS Condition Event "
                     "Queuing Failed \n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        /* Send an event that Interrupt Queue Failure has occured */
        ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_INT_QUEUE_OVERFLOW);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMplstpOffSetMepAisStatus
 *
 * Description        : This rotuine is used to intimate HW about AIS
 *                      condition on MPLS-TP based MEPs. 
 * 
 * Input(s)           : pAisOffParams - Pointer to AIS information.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/

PUBLIC INT4
EcfmMplstpOffSetMepAisStatus (tEcfmMplstpAisOffParams * pAisOffParams)
{
    ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                      "\n OFFLOAD: EcfmMplstpOffSetMepAisStatus: "
                      "ContextId = [%u] Slot/If-Info = [%p] "
                      "AIS-Period = [%u] AIS-Interval = [%u] ",
                      pAisOffParams->u4ContextId, pAisOffParams->pSlotInfo,
                      pAisOffParams->u4Period, pAisOffParams->u4AisInterval);

    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC,
                      "\n OFFLOAD: EcfmMplstpOffSetMepAisStatus: "
                      "MdLevel = [%u] AIS-Phb = [%u] AIS-Condition = [%s]",
                      pAisOffParams->u1MdLevel, pAisOffParams->u1PhbValue,
                      ((pAisOffParams->b1AisCondition ==
                        TRUE) ? "ENTRY" : "EXIT"));

    if (pAisOffParams->EcfmMplsParams.u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpOffSetMepAisStatus: "
                          "VcId = [%d]\r\n",
                          pAisOffParams->EcfmMplsParams.MplsPathParams.u4PswId);
    }
    else
    {
        ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpOffSetMepAisStatus: Tunnel Id = [%u], "
                          "Tunnel Instance = [%u], Source = [%u], Destination = [%u]\r\n",
                          pAisOffParams->EcfmMplsParams.MplsPathParams.
                          MplsLspParams.u4TunnelId,
                          pAisOffParams->EcfmMplsParams.MplsPathParams.
                          MplsLspParams.u4TunnelInst,
                          pAisOffParams->EcfmMplsParams.MplsPathParams.
                          MplsLspParams.u4SrcLer,
                          pAisOffParams->EcfmMplsParams.MplsPathParams.
                          MplsLspParams.u4DstLer);
    }
#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /*
           if (FsMiEcfmMplstpHwSetAisStatus (pAisOffParams) != FNP_SUCCESS)
           {
           ECFM_CC_TRC_ARG (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
           "EcfmMplstpOffSetMepAisStatus: Unable to intimate the "
           "AIS condition to HW \r\n" );
           return ECFM_FAILURE;
           }
         */
        UNUSED_PARAM (pAisOffParams);
    }
#else
    UNUSED_PARAM (pAisOffParams);
#endif
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      :  EcfmUpdatePortStatusForDownMep
 *
 * Description        : This rotuine is used to handle Port State change Indication
 * 
 * Input(s)           : pMepInfo Pointer to MEP Structure Info.
 *                      
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmUpdatePortStatusForDownMep (tEcfmCcMepInfo * pMepInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

        /* First Disable MEP transmission from this MEP */
        /* Update Tx/Rx parameters for this MEP */
        EcfmCcmOffDeleteTxRxForMep (pMepInfo);

        if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
        {
            ECFM_CC_TRC_FN_EXIT ();
	    return ECFM_FAILURE;
        }
    /* Enable Tx/Rx for this MEP */
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      :  EcfmUpdatePortStatusForAllUpMep
 *
 * Description        : This rotuine is used to handle Port State change Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      u4IfIndex -  Interface Index of the port, whose port
 *                                   state is to be updated.
 *                      pPortInfo  - Pointer to port info             
 *                      u1PortState - Port state to be set
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmUpdatePortStatusForAllUpMep (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 tEcfmCcPortInfo * pPortInfo, UINT1 u1PortState)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pLocalPortInfo = NULL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    UNUSED_PARAM (*pPortInfo);
    UNUSED_PARAM (u1PortState);

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pLocalPortInfo = NULL;
        pLocalPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pLocalPortInfo != NULL)
        {
            gpEcfmCcMepNode->u4IfIndex = pLocalPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);

            while ((pMepInfo != NULL) &&
                   (pMepInfo->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ()))
            {
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);
                if (pMepInfo->u1Direction == ECFM_MP_DIR_UP)
                {
                    /* First Disable MEP transmission from this MEP 
                     * Update Tx/Rx parameters for this MEP
                     */
                    if (EcfmL2IwfMiIsVlanMemberPort (u4ContextId,
                                                     pMepInfo->u4PrimaryVidIsid,
                                                     u4IfIndex) == OSIX_TRUE)
                    {
                        EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                        /* Enable Tx/Rx for this MEP */
                        if (EcfmCcmOffCreateTxRxForMep (pMepInfo) ==
                            ECFM_FAILURE)
                        {
                            return ECFM_FAILURE;
                        }
                    }
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcmOffLoadUpdateTxRxStats
 *
 * Description        : This routine is used to update the Stats from the
 *                      Offloaded Module.
 *                        
 * Input(s)           : u4FsEcfmPortIndex - Port Number 
 *                      
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcmOffLoadUpdateTxRxStats (UINT4 u4FsEcfmPortIndex)
{
    tEcfmCcOffPortStats EcfmCcOffPortStats;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2OffloadMepTxHandle = ECFM_INIT_VAL;
    tEcfmCcOffMepTxStats EcfmCcOffMepTxStats;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    ECFM_MEMSET (&EcfmCcOffPortStats, ECFM_INIT_VAL,
                 sizeof (tEcfmCcOffPortStats));

    pPortInfo = ECFM_CC_GET_PORT_INFO ((UINT2) u4FsEcfmPortIndex);
    if (pPortInfo == NULL)
    {
        return;
    }
    u4ContextId = pPortInfo->u4ContextId;
    u4IfIndex = pPortInfo->u4IfIndex;
    gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
    gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    pMepNode = NULL;
    pMepNode = RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                              gpEcfmCcMepNode, NULL);

    while ((pMepNode != NULL) && (pMepNode->u4IfIndex == u4IfIndex) &&
           (pMepNode->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ()))
    {
        pNextMepInfo =
            (tEcfmCcMepInfo *)
            RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                           (tRBElem *) pMepNode, NULL);
        u2OffloadMepTxHandle = pMepNode->u2OffloadMepTxHandle;
        /*Update Tx Stats */
        if (u2OffloadMepTxHandle != ECFM_INIT_VAL)
        {
            ECFM_MEMSET (&EcfmCcOffMepTxStats, 0,
                         sizeof (tEcfmCcOffMepTxStats));
            if (EcfmGetCcmOffTxStatistics
                (u4ContextId, u2OffloadMepTxHandle,
                 &EcfmCcOffMepTxStats) == ECFM_FAILURE)
            {
                return;
            }
            if (!ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPortInfo->u2PortNum))
            {
                pMepNode->CcInfo.u4CciSentCcms =
                    EcfmCcOffMepTxStats.u4LastSeqNum;
            }
        }

        /*Update Rx Stats for All Remote MEPS */
        EcfmCcmoffUpdateRxStats (pPortInfo, pMepNode);
        pMepNode = pNextMepInfo;
    }
    /* update Tx Rx Stats Per Port */
    /*Reset the Port Info Ptr */
    pTempPortInfo = NULL;
    if (EcfmGetPortCcStats
        (u4ContextId,
         ECFM_CC_GET_PHY_PORT (u4FsEcfmPortIndex, pTempPortInfo),
         &EcfmCcOffPortStats) == ECFM_FAILURE)
    {
        return;
    }
    pPortInfo->u4TxCfmPduCount =
        pPortInfo->u4TxCfmPduCount + EcfmCcOffPortStats.u4NoOfCcTx;
    pPortInfo->u4TxCcmCount =
        pPortInfo->u4TxCcmCount + EcfmCcOffPortStats.u4NoOfCcTx;
    pPortInfo->u4RxCcmCount =
        pPortInfo->u4RxCcmCount + EcfmCcOffPortStats.u4NoOfCcRx;
    pPortInfo->u4RxCfmPduCount =
        pPortInfo->u4RxCfmPduCount + EcfmCcOffPortStats.u4NoOfCcRx;
    pPortInfo->u4FrwdCfmPduCount =
        pPortInfo->u4FrwdCfmPduCount + EcfmCcOffPortStats.u4NoOfCcFw;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcmoffUpdateRxStats
 *
 * Description        : This routine is used to update the Stats from the
 *                      Offloaded Module.
 *                        
 * Input(s)           : u4FsEcfmPortIndex - Port Number 
 *                      
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/

PUBLIC VOID
EcfmCcmoffUpdateRxStats (tEcfmCcPortInfo * pPortInfo, tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcOffMepRxStats EcfmCcOffMepRxStats;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2OffloadRMepRxHandle = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    u4ContextId = pPortInfo->u4ContextId;
    /* Get the first node from the RBtree of RMepDbTable in
     * MepInfo */
    pRMepInfo = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
    while (pRMepInfo != NULL)
    {
        u2OffloadRMepRxHandle = pRMepInfo->u2OffloadRMepRxHandle;
        if (u2OffloadRMepRxHandle != ECFM_INIT_VAL)
        {
            ECFM_MEMSET (&EcfmCcOffMepRxStats, ECFM_INIT_VAL,
                         sizeof (tEcfmCcOffMepRxStats));
            if (EcfmGetCcmOffRxStatistics
                (u4ContextId, u2OffloadRMepRxHandle,
                 &EcfmCcOffMepRxStats) == ECFM_FAILURE)
            {
                return;
            }
            pRMepInfo->u4SeqNum = EcfmCcOffMepRxStats.u4ExpectedSeqNum;
        }

        /* Get the next node form the tree */
        pRMepInfo = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                        &(pRMepInfo->
                                                          MepDbDllNode));
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

PUBLIC INT4
EcfmCcmOffCreateMepTxOnPortList (tPortListExt IfFwdPortList,
                                 tPortListExt UntagIfFwdPortList,
                                 UINT1 *pu1Buf,
                                 UINT2 u2PduLength,
                                 UINT4 u4CcmInterval,
                                 UINT4 u4ContextId,
                                 UINT2 u2OffloadMepTxHandle,
                                 tEcfmCcMepInfo * pMepInfo,
                                 tEcfmCcRMepDbInfo * pRMepInfo)
{
    tEcfmHwParams       EcfmHwInfo;
    tHwPortArray        IfFwdHwPortArray;
    tHwPortArray        UntagHwPortArray;
    UINT2               u2FwNumPorts = ECFM_INIT_VAL;
    UINT2               u2UntagNumPorts = ECFM_INIT_VAL;
    UINT1               u1Type = ECFM_INIT_VAL;

    MEMSET (&IfFwdHwPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&UntagHwPortArray, 0, sizeof (tHwPortArray));
    UNUSED_PARAM (u4CcmInterval);

    /* Allocating memory for IfFwdHwPortArry */
    if (ECFM_ALLOC_MEM_BLOCK_ECFM_HW_ARRAY (IfFwdHwPortArray.pu4PortArray)
        == NULL)
    {
        return ECFM_FAILURE;
    }

    MEMSET (IfFwdHwPortArray.pu4PortArray, 0,
            sizeof (*IfFwdHwPortArray.pu4PortArray));

    /*convert the port list in to array of ports */
    EcfmConvertPortListToArray (IfFwdPortList, IfFwdHwPortArray.pu4PortArray,
                                &u2FwNumPorts);

    IfFwdHwPortArray.i4Length = u2FwNumPorts;

    /* Allocating memory for UntagHwPortArray */
    if (ECFM_ALLOC_MEM_BLOCK_ECFM_HW_ARRAY (UntagHwPortArray.pu4PortArray)
        == NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                             (UINT1 *) IfFwdHwPortArray.pu4PortArray);
        return ECFM_FAILURE;
    }

    MEMSET (UntagHwPortArray.pu4PortArray, 0,
            sizeof (*UntagHwPortArray.pu4PortArray));

    /*convert the port list in to array of ports */
    EcfmConvertPortListToArray (UntagIfFwdPortList,
                                UntagHwPortArray.pu4PortArray,
                                &u2UntagNumPorts);

    UntagHwPortArray.i4Length = u2UntagNumPorts;

#ifdef NPAPI_WANTED
    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId = u4ContextId;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.pu4PortArray =
        IfFwdHwPortArray.pu4PortArray;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.i4Length =
        IfFwdHwPortArray.i4Length;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.UntagPortList.pu4PortArray =
        UntagHwPortArray.pu4PortArray;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.UntagPortList.i4Length =
        UntagHwPortArray.i4Length;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.pu1Pdu = pu1Buf;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2PduLength = u2PduLength;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2TxFilterId = u2OffloadMepTxHandle;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
        pMepInfo->pPortInfo->u4PhyPortNum;
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, pRMepInfo);
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /* In case it is been called on Card Insertion Event then
         * call MBSM related routine */
#ifdef MBSM_WANTED
        if (gbEcfmMbsmEvtHndl == ECFM_TRUE)
        {
            u1Type = ECFM_NP_MBSM_START_CCM_TX_ON_PORTLIST;

            if (EcfmFsMiEcfmMbsmHwCallNpApi
                (u1Type, &EcfmHwInfo, &gEcfmSlotInfo) != FNP_SUCCESS)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) IfFwdHwPortArray.pu4PortArray);
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) UntagHwPortArray.pu4PortArray);
                return ECFM_FAILURE;
            }
        }
        else
#endif
        {
            u1Type = ECFM_NP_START_CCM_TX_ON_PORTLIST;

            if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) IfFwdHwPortArray.pu4PortArray);
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) UntagHwPortArray.pu4PortArray);
                return ECFM_FAILURE;
            }
        }

    }
    /* Add MEP node in HW global RBTree */
    MEMCPY (pMepInfo->pMaInfo->au1HwMaHandler,
            EcfmHwInfo.EcfmHwMaParams.au1HwHandler, ECFM_HW_MA_HANDLER_SIZE);
    MEMCPY (pMepInfo->au1HwMepHandler,
            EcfmHwInfo.EcfmHwMepParams.au1HwHandler, ECFM_HW_MEP_HANDLER_SIZE);
    RBTreeAdd (ECFM_CC_GLOBAL_HW_MEP_TABLE, pMepInfo);
    EcfmRedSynchHwTxHandler (u4ContextId, pMepInfo->u4MdIndex,
                             pMepInfo->u4MaIndex, pMepInfo->u2MepId,
                             pMepInfo->au1HwMepHandler);

#else

    UNUSED_PARAM (IfFwdPortList);
    UNUSED_PARAM (UntagIfFwdPortList);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u2PduLength);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2OffloadMepTxHandle);
    UNUSED_PARAM (pMepInfo);
    UNUSED_PARAM (pRMepInfo);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (EcfmHwInfo);

#endif

    /* if Memory allocated then free it */
    if (IfFwdHwPortArray.pu4PortArray != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                             (UINT1 *) IfFwdHwPortArray.pu4PortArray);
    }
    if (UntagHwPortArray.pu4PortArray != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                             (UINT1 *) UntagHwPortArray.pu4PortArray);
    }

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmHwUpdateMaMepParams
 *
 * Description        : This routine is used to update the structure parameters
 *                        
 * Input(s)           : pMepInfo - pointer to MepInfo structure 
 *                    : pRMepInfo - Pointer to RMepInfo Structure
 *
 * Output(s)          : pEcfmHwInfo - Structure pointer to HwParams
 *
 * Return Value(s)    : None
 ******************************************************************************/

PUBLIC VOID
EcfmHwUpdateMaMepParams (tEcfmHwParams * pEcfmHwInfo,
                         tEcfmCcMepInfo * pMepInfo,
                         tEcfmCcRMepDbInfo * pRMepInfo)
{
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tMacAddr            NullMacAddr = { ECFM_INIT_VAL };
    UINT4               u4CcmInterval = 0;

    if (pMepInfo != NULL)
    {
        /* MEP information */
        pEcfmHwInfo->EcfmHwMepParams.u2MepId = pMepInfo->u2MepId;
        pEcfmHwInfo->EcfmHwMepParams.u1MdLevel = pMepInfo->u1MdLevel;
        pEcfmHwInfo->EcfmHwMepParams.u1Direction = pMepInfo->u1Direction;
        ECFM_GET_MAC_ADDR_OF_PORT(pMepInfo->u4IfIndex, 
                                  pEcfmHwInfo->EcfmHwMepParams.MepMacAddr);
        /* MEP Hw Handler */
        MEMCPY (pEcfmHwInfo->EcfmHwMepParams.au1HwHandler,
                pMepInfo->au1HwMepHandler, ECFM_HW_MEP_HANDLER_SIZE);

        /* MEP Destination Address */
        pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

        ECFM_MEMSET (NullMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

        if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
            (ECFM_MEMCMP (pCcInfo->UnicastCcmMacAddr, NullMacAddr,
                          ECFM_MAC_ADDR_LENGTH) != ECFM_SUCCESS))
        {
            /* Vlan Priority */
            pEcfmHwInfo->EcfmHwMepParams.u1CcmPriority = pCcInfo->u1CcmPriority;
            /* Destination Mac Address */
            ECFM_MEMCPY (pEcfmHwInfo->EcfmHwMepParams.DstMacAddr,
                         pCcInfo->UnicastCcmMacAddr, ECFM_MAC_ADDR_LENGTH);
        }
        else
        {
            /* Vlan Priority */
            pEcfmHwInfo->
                EcfmHwMepParams.u1CcmPriority = pMepInfo->u1CcmLtmPriority;
            /* Destination Mac Address */
            ECFM_GEN_MULTICAST_CLASS1_ADDR (pEcfmHwInfo->EcfmHwMepParams.
                                            DstMacAddr, pMepInfo->u1MdLevel);
        }

        /* Ma Information */
        pMaInfo = pMepInfo->pMaInfo;

        ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4CcmInterval);
        /* Time Interval Changed to MicroSeconds */
        u4CcmInterval = u4CcmInterval * ECFM_NUM_OF_USEC_IN_A_MSEC;
        pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval = u4CcmInterval;
        pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid = pMaInfo->u4PrimaryVidIsid;

        /* MAID */
        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
        {
            /* Fills 48 byte as MEGID, with Icc and Umc Codes */
            EcfmUtilPutMEGID (pMepInfo, pEcfmHwInfo->EcfmHwMaParams.au1MAID);
        }
        else
        {
            /* Fills 48 byte MAID as per the IEEE 802.1ag standard */
            EcfmUtilPutMAID (pMepInfo, pEcfmHwInfo->EcfmHwMaParams.au1MAID);
        }

        /* MA Hw Handler */
        MEMCPY (pEcfmHwInfo->EcfmHwMaParams.au1HwHandler,
                pMaInfo->au1HwMaHandler, ECFM_HW_MA_HANDLER_SIZE);
	pEcfmHwInfo->u1EcfmOffStatus = (UINT1) ECFM_GLOBAL_CCM_OFFLOAD_STATUS;
    }

    if (pRMepInfo != NULL)
    {
        pEcfmHwInfo->EcfmHwRMepParams.u2MepId = pRMepInfo->u2MepId;
        pEcfmHwInfo->EcfmHwRMepParams.u2RMepId = pRMepInfo->u2RMepId;
        pEcfmHwInfo->EcfmHwRMepParams.u1Direction =
            pRMepInfo->pMepInfo->u1Direction;
        pEcfmHwInfo->EcfmHwRMepParams.u1MdLevel =
            pRMepInfo->pMepInfo->u1MdLevel;
        /* RMEP Hw Handler */
        MEMCPY (pEcfmHwInfo->EcfmHwRMepParams.au1HwHandler,
                pRMepInfo->au1HwRMepHandler, ECFM_HW_MEP_HANDLER_SIZE);
        pEcfmHwInfo->EcfmHwRMepParams.u1State = pRMepInfo->u1State;
    }
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcmOffCreateMepTx
 *
 * Description        : This routine is used to Create Mep Transmission
 *                        
 * Input(s)           : 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS
 ******************************************************************************/
PUBLIC INT4
EcfmCcmOffCreateMepTx (UINT4 u4IfIndex, UINT1 *pu1Buf, UINT2 u2PduSize,
                       UINT4 u4CcmInterval, UINT2 u2OffloadMepTxHandle,
                       tEcfmCcMepInfo * pMepInfo)
{
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = (UINT2) ECFM_INIT_VAL;
    UINT1               u1Type = ECFM_INIT_VAL;

    UNUSED_PARAM (u4CcmInterval);
    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX
        (u4IfIndex, &u4ContextId, &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);

    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId = u4ContextId;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pPortInfo->u4PhyPortNum;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.pu1Pdu = pu1Buf;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2PduLength = u2PduSize;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2TxFilterId = u2OffloadMepTxHandle;
    EcfmHwInfo.u1EcfmPerPortPerMepOffStatus = ECFM_CCM_OFFLOAD_ENABLE;
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, NULL);
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /* In case it is been called on Card Insertion Event then
         * call MBSM related routine */
#ifdef MBSM_WANTED
        u1Type = ECFM_NP_MBSM_START_CCM_TX;
        if (gbEcfmMbsmEvtHndl == ECFM_TRUE)
        {
            if (EcfmFsMiEcfmMbsmHwCallNpApi
                (u1Type, &EcfmHwInfo, &gEcfmSlotInfo) != FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
        }
        else
#endif
        {
            u1Type = ECFM_NP_START_CCM_TX;

            if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
        }
    }
    /* Hw Handler */
    MEMCPY (pMepInfo->pMaInfo->au1HwMaHandler,
            EcfmHwInfo.EcfmHwMaParams.au1HwHandler, ECFM_HW_MA_HANDLER_SIZE);
    MEMCPY (pMepInfo->au1HwMepHandler,
            EcfmHwInfo.EcfmHwMepParams.au1HwHandler, ECFM_HW_MEP_HANDLER_SIZE);
    /* Add MEP node in HW global RBTree */
    RBTreeAdd (ECFM_CC_GLOBAL_HW_MEP_TABLE, pMepInfo);

    /* Sync to Standby */
    EcfmRedSynchHwTxHandler (u4ContextId,
                             pMepInfo->u4MdIndex,
                             pMepInfo->u4MaIndex,
                             pMepInfo->u2MepId, pMepInfo->au1HwMepHandler);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u2PduSize);
    UNUSED_PARAM (u4CcmInterval);
    UNUSED_PARAM (u2OffloadMepTxHandle);
    UNUSED_PARAM (pMepInfo);
#endif
    return ECFM_SUCCESS;
}

PUBLIC INT4
EcfmGetCcmOffTxStatistics (UINT4 u4ContextId,
                           UINT2 u2OffloadMepTxHandle,
                           tEcfmCcOffMepTxStats * pEcfmCcOffMepTxStats)
{
#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /* Update Tx Stats from the Offloaded Module */
        if (EcfmFsMiEcfmHwGetCcmTxStatistics
            (u4ContextId, u2OffloadMepTxHandle,
             pEcfmCcOffMepTxStats) != FNP_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2OffloadMepTxHandle);
    UNUSED_PARAM (pEcfmCcOffMepTxStats);
#endif
    return ECFM_SUCCESS;
}

PUBLIC VOID
EcfmCcmOffDeleteMepTx (UINT4 u4ContextId, UINT2 u2OffloadMepTxHandle,
                       tEcfmCcMepInfo * pMepInfo)
{
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               u1Type = ECFM_INIT_VAL;

    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));

    u1Type = ECFM_NP_STOP_CCM_TX;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId = u4ContextId;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2TxFilterId = u2OffloadMepTxHandle;
    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepInfo->u4IfIndex;
    EcfmHwInfo.u1EcfmPerPortPerMepOffStatus = ECFM_CCM_OFFLOAD_DISABLE;
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, NULL);

    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo);
    }

    RBTreeRem (ECFM_CC_GLOBAL_HW_MEP_TABLE, pMepInfo);
    /* Reset MEP ID */
    MEMSET (pMepInfo->au1HwMepHandler, 0, ECFM_HW_MEP_HANDLER_SIZE);

    EcfmRedSynchHwTxHandler (u4ContextId, pMepInfo->u4MdIndex,
                             pMepInfo->u4MaIndex, pMepInfo->u2MepId,
                             pMepInfo->au1HwMepHandler);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2OffloadMepTxHandle);
    UNUSED_PARAM (pMepInfo);
#endif
    return;
}

PUBLIC INT4
EcfmGetCcmOffRxStatistics (UINT4 u4ContextId,
                           UINT2 u2OffloadRMepRxHandle,
                           tEcfmCcOffMepRxStats * pEcfmCcOffMepRxStats)
{

#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        if (EcfmFsMiEcfmHwGetCcmRxStatistics
            (u4ContextId, u2OffloadRMepRxHandle,
             pEcfmCcOffMepRxStats) != FNP_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2OffloadRMepRxHandle);
    UNUSED_PARAM (pEcfmCcOffMepRxStats);
#endif
    return ECFM_SUCCESS;
}

PUBLIC INT4
EcfmStartCcmOffCreateMepRxOnPortList (tPortListExt IfFwdActualPortList,
                                      tPortListExt UntagIfFwdActualPortList,
                                      UINT2 u2MEPID,
                                      tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                                      UINT4 u4ContextId,
                                      UINT2 u2OffloadRMepRxHandle,
                                      tEcfmCcMepInfo * pMepNode,
                                      tEcfmCcRMepDbInfo * pRmep)
{
    tEcfmHwParams       EcfmHwInfo;
    tHwPortArray        IfFwdHwPortArray;
    tHwPortArray        UntagHwPortArray;
    UINT2               u2FwNumPorts = ECFM_INIT_VAL;
    UINT2               u2UntagNumPorts = ECFM_INIT_VAL;
    UINT1               u1Type = ECFM_INIT_VAL;

    MEMSET (&IfFwdHwPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&UntagHwPortArray, 0, sizeof (tHwPortArray));

    /* Allocating memory for IfFwdHwPortArray */
    if (ECFM_ALLOC_MEM_BLOCK_ECFM_HW_ARRAY (IfFwdHwPortArray.pu4PortArray)
        == NULL)
    {
        return ECFM_FAILURE;
    }
    MEMSET (IfFwdHwPortArray.pu4PortArray, 0,
            sizeof (*IfFwdHwPortArray.pu4PortArray));

    /*convert the port list in to array of ports */
    EcfmConvertPortListToArray (IfFwdActualPortList,
                                IfFwdHwPortArray.pu4PortArray, &u2FwNumPorts);

    IfFwdHwPortArray.i4Length = u2FwNumPorts;

    /* Allocating memory for UntagHwPortArray */
    if (ECFM_ALLOC_MEM_BLOCK_ECFM_HW_ARRAY (UntagHwPortArray.pu4PortArray)
        == NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                             (UINT1 *) IfFwdHwPortArray.pu4PortArray);
        return ECFM_FAILURE;
    }
    MEMSET (UntagHwPortArray.pu4PortArray, 0,
            sizeof (*UntagHwPortArray.pu4PortArray));

    /*convert the port list in to array of ports */
    EcfmConvertPortListToArray (UntagIfFwdActualPortList,
                                UntagHwPortArray.pu4PortArray,
                                &u2UntagNumPorts);

    UntagHwPortArray.i4Length = u2UntagNumPorts;

    UNUSED_PARAM (u2MEPID);
#ifdef NPAPI_WANTED

    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));

    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId = u4ContextId;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.pu4PortArray =
        IfFwdHwPortArray.pu4PortArray;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.i4Length =
        IfFwdHwPortArray.i4Length;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.UntagPortList.pu4PortArray =
        UntagHwPortArray.pu4PortArray;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.UntagPortList.i4Length =
        UntagHwPortArray.i4Length;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.pEcfmCcRxInfo = pEcfmCcOffRxInfo;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.u2RxFilterId = u2OffloadRMepRxHandle;
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, pRmep);
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /* In case it is been called on Card Insertion Event then
         * call MBSM related routine */
#ifdef MBSM_WANTED
        if (gbEcfmMbsmEvtHndl == ECFM_TRUE)
        {
            u1Type = ECFM_NP_MBSM_START_CCM_RX_ON_PORTLIST;

            if (EcfmFsMiEcfmMbsmHwCallNpApi
                (u1Type, &EcfmHwInfo, &gEcfmSlotInfo) != FNP_SUCCESS)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) IfFwdHwPortArray.pu4PortArray);
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) UntagHwPortArray.pu4PortArray);
                return ECFM_FAILURE;
            }
        }
        else
#endif
        {
            u1Type = ECFM_NP_START_CCM_RX_ON_PORTLIST;

            if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) IfFwdHwPortArray.pu4PortArray);
                ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                                     (UINT1 *) UntagHwPortArray.pu4PortArray);
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                  "OFFLOAD: %s"
                                  " Function Returned Failure  \r\n",
                                  __FUNCTION__);
                return ECFM_FAILURE;
            }
        }

    }
    /* Hw Handler */
    MEMCPY (pRmep->pMepInfo->pMaInfo->au1HwMaHandler,
            EcfmHwInfo.EcfmHwMaParams.au1HwHandler, ECFM_HW_MA_HANDLER_SIZE);
    MEMCPY (pRmep->au1HwRMepHandler,
            EcfmHwInfo.EcfmHwRMepParams.au1HwHandler, ECFM_HW_MEP_HANDLER_SIZE);
    /* Add MEP node in HW global RBTree */
    RBTreeAdd (ECFM_CC_GLOBAL_HW_RMEP_TABLE, pRmep);
    /* Sync to Standby */
    EcfmRedSynchHwRxHandler (u4ContextId, pRmep->u4MdIndex,
                             pRmep->u4MaIndex, pRmep->u2MepId,
                             pRmep->u2RMepId, pRmep->au1HwRMepHandler);

#else
    UNUSED_PARAM (IfFwdActualPortList);
    UNUSED_PARAM (UntagIfFwdActualPortList);
    UNUSED_PARAM (u2MEPID);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2OffloadRMepRxHandle);
    UNUSED_PARAM (pMepNode);
    UNUSED_PARAM (pRmep);
    UNUSED_PARAM (EcfmHwInfo);
    UNUSED_PARAM (u1Type);
#endif

    /* if Memory allocated then free it */
    if (IfFwdHwPortArray.pu4PortArray != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                             (UINT1 *) IfFwdHwPortArray.pu4PortArray);
    }
    if (UntagHwPortArray.pu4PortArray != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_HW_PORTLIST_ARRAY,
                             (UINT1 *) UntagHwPortArray.pu4PortArray);
    }

    return ECFM_SUCCESS;
}

PUBLIC VOID
EcfmCcmOffDeleteMepRx (UINT4 u4ContextId, UINT2 u2OffloadMepRxHandle,
                       tEcfmCcMepInfo * pMepInfo, tEcfmCcRMepDbInfo * pRMepInfo)
{
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               u1Type = ECFM_INIT_VAL;

    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));

    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex =
        pMepInfo->pPortInfo->u4PhyPortNum;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId = u4ContextId;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.u2RxFilterId = u2OffloadMepRxHandle;
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, pRMepInfo);

    u1Type = ECFM_NP_STOP_CCM_RX;

    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo);
    }

    RBTreeRem (ECFM_CC_GLOBAL_HW_RMEP_TABLE, pRMepInfo);
    /* Reset RMEP ID */
    MEMSET (pRMepInfo->au1HwRMepHandler, 0, ECFM_HW_MEP_HANDLER_SIZE);
    MEMSET (pRMepInfo->pMepInfo->pMaInfo->au1HwMaHandler, 0, ECFM_HW_MA_HANDLER_SIZE);
    /* Sync to Standby */
    EcfmRedSynchHwRxHandler (u4ContextId, pRMepInfo->u4MdIndex,
                             pRMepInfo->u4MaIndex, pRMepInfo->u2MepId,
                             pRMepInfo->u2RMepId, pRMepInfo->au1HwRMepHandler);

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2OffloadMepRxHandle);
    UNUSED_PARAM (pMepInfo);
    UNUSED_PARAM (pRMepInfo);
#endif
    return;

}

PUBLIC INT4
EcfmCcmOffCreateRemoteMepRx (UINT4 u4IfIndex,
                             UINT2 u2MepId,
                             tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                             UINT2 u2OffloadRMepRxHandle,
                             tEcfmCcMepInfo * pMepInfo,
                             tEcfmCcRMepDbInfo * pRmepInfo)
{
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = (UINT2) ECFM_INIT_VAL;
    UINT1               u1Type = ECFM_INIT_VAL;

    UNUSED_PARAM (u2MepId);
    MEMSET (&EcfmHwInfo, 0x0, sizeof (tEcfmHwParams));

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX
        (u4IfIndex, &u4ContextId, &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);

    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId = u4ContextId;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex = pPortInfo->u4PhyPortNum;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.pEcfmCcRxInfo = pEcfmCcOffRxInfo;
    EcfmHwInfo.unParam.EcfmHwCcRxParams.u2RxFilterId = u2OffloadRMepRxHandle;
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, pRmepInfo);
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /* In case it is been called on Card Insertion Event then
         * call MBSM related routine */
#ifdef MBSM_WANTED
        u1Type = ECFM_NP_MBSM_START_CCM_RX;

        if (gbEcfmMbsmEvtHndl == ECFM_TRUE)
        {
            if (EcfmFsMiEcfmMbsmHwCallNpApi
                (u1Type, &EcfmHwInfo, &gEcfmSlotInfo) != FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
        }
        else
#endif
        {
            u1Type = ECFM_NP_START_CCM_RX;

            if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
        }
    }

    /* Add MEP node in HW global RBTree */
    MEMCPY (pRmepInfo->au1HwRMepHandler,
            EcfmHwInfo.EcfmHwRMepParams.au1HwHandler, ECFM_HW_MEP_HANDLER_SIZE);
    RBTreeAdd (ECFM_CC_GLOBAL_HW_RMEP_TABLE, pRmepInfo);

    /* Sync to Standby */
    EcfmRedSynchHwRxHandler (u4ContextId,
                             pRmepInfo->u4MdIndex,
                             pRmepInfo->u4MaIndex,
                             pRmepInfo->u2MepId,
                             pRmepInfo->u2RMepId, pRmepInfo->au1HwRMepHandler);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MepId);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u2OffloadRMepRxHandle);
    UNUSED_PARAM (pMepInfo);
    UNUSED_PARAM (pRmepInfo);
#endif
    return ECFM_SUCCESS;
}

PUBLIC INT4
EcfmGetPortCcStats (UINT4 u4ContextId, UINT4 u4IfIndex,
                    tEcfmCcOffPortStats * pEcfmCcOffPortStats)
{
#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        if ((EcfmFsMiEcfmHwGetPortCcStats
             (u4ContextId, u4IfIndex, pEcfmCcOffPortStats) != FNP_SUCCESS))

        {
            return ECFM_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pEcfmCcOffPortStats);
#endif
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : FsEcfmOffloadSetRDI 
 *
 * Description        : This rotuine will Update the RDI Bit.
 *                      
 * * 
 * Input(s)           : pMepInfo :- Pointer to Mep Structure  
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/

INT4
FsEcfmOffloadSetRDI (tEcfmCcMepInfo * pMepInfo)
{
    /* Check if RDI is to be set/reset in Offloaded module */
    if (pMepInfo != NULL)
    {
        if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
        {
            ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                              "CCMOFFLOAD: Software received "
                              "the CCM PDU from hardware for MEPID %d\r\n",
                              pMepInfo->u2MepId);

            if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_TRUE) ||
                (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_TRUE) ||
                (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE) ||
                (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_TRUE))
            {
                /* set presentRDI to True */
                pMepInfo->b1PresentRdi = ECFM_TRUE;
            }
            else
            {
                /* set presentRDI to False */
                pMepInfo->b1PresentRdi = ECFM_FALSE;
            }

            if (pMepInfo->b1PresentRdi != pMepInfo->b1CcmOffloadLastRdi)
            {

                /* Call the Create Tx/Rx call for Mep */
                if (EcfmCcmOffCreateTxRxForMep (pMepInfo) != ECFM_SUCCESS)
                {
                    return ECFM_FAILURE;
                }

                pMepInfo->b1CcmOffloadLastRdi = pMepInfo->b1PresentRdi;
            }
        }
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleUpdateDeiBit
 *
 * Description        : This rotuine will Update the Offloaded Module when Dei
 *                      is set or Reset.
 * * 
 * Input(s)           : u4IfIndex :- Actual Port Number  
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmHandleUpdateDeiBit (UINT4 u4IfIndex)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = (UINT2) ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX
        (u4IfIndex, &u4ContextId, &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "OFFLOAD :"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }

    /* For all ports get MEPs configured on these ports */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);

    if (pPortInfo != NULL)
    {
        gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
        gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        pMepInfo = NULL;
        pMepInfo =
            (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                              gpEcfmCcMepNode, NULL);
        while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex) &&
               (pMepInfo->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ()))
        {
            pNextMepInfo =
                (tEcfmCcMepInfo *)
                RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                               (tRBElem *) pMepInfo, NULL);
            /* First Disable MEP transmission from this MEP */
            /* Update Tx/Rx parameters for this MEP */
            EcfmCcmOffDeleteTxRxForMep (pMepInfo);

            if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
            {
                return ECFM_FAILURE;
            }
            pMepInfo = pNextMepInfo;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffLoadUpdateLlc 
 *
 * Description        : This rotuine will Update the Offloaded Module when LLC
 *                      Encapsulation is set or Reset.
 * * 
 * Input(s)           : u4FsEcfmPortIndex :- Locat Port Number  
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffLoadUpdateLlc (UINT4 u4FsEcfmPortIndex)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "OFFLOAD :"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }

    /* For all ports get MEPs configured on these ports */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);

    if (pPortInfo != NULL)
    {
        u4IfIndex = pPortInfo->u4IfIndex;
        gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
        gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        pMepInfo = NULL;
        pMepInfo =
            (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                              gpEcfmCcMepNode, NULL);

        while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex) &&
               (pMepInfo->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ()))
        {
            pNextMepInfo =
                (tEcfmCcMepInfo *)
                RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                               (tRBElem *) pMepInfo, NULL);
            /* First Disable MEP transmission from this MEP */
            /* Update Tx/Rx parameters for this MEP */
            EcfmCcmOffDeleteTxRxForMep (pMepInfo);

            if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
            {
                return ECFM_FAILURE;
            }
            pMepInfo = pNextMepInfo;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcCtrlTxTransmitPktToOffLoad   
 *
 * Description        : This Routine will Forwared the Packet to Hardware
 *                      Depending upon the Direction of MEP. 
 *                      
 * Input(s)           : pBuf     - CRU Buffer Pointer 
 *                     pCcMepInfo - Pointer to MEP Structure Info 
 *                     pVlanInfo  - Pointer to VLAN Info 
 *                     pCcMepCcInfo - Pointer to CC Mep Info Structure
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS.
 *                      ECFM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcCtrlTxTransmitIsidPktToOffLoad (tEcfmBufChainHeader * pBuf,
                                      tEcfmCcMepInfo * pCcMepInfo,
                                      tEcfmIsidTagInfo * pIsidInfo,
                                      tEcfmCcMepCcInfo * pCcMepCcInfo)
{
    UINT1               u1MpDirection = ECFM_INIT_VAL;
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_CC_TRC_FN_ENTRY ();

    u1MpDirection = pCcMepInfo->u1Direction;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    if ((pIsidInfo->u4Isid - ECFM_INTERNAL_ISID_MIN) != ECFM_INIT_VAL)
    {
        /*Check the port memebership of Vlan */
        if (EcfmPbbMiIsIsidMemberPort
            (ECFM_CC_CURR_CONTEXT_ID (),
             pCcMepInfo->u4PrimaryVidIsid,
             pCcMepInfo->pPortInfo->u4IfIndex) != OSIX_TRUE)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "OFFLOAD: Function EcfmPbbMiIsVlanMemberPort has returned"
                         "\r\n");
            pCcMepInfo->b1MepCcmOffloadHwStatus = ECFM_FALSE;
            pCcMepInfo->u2OffloadMepTxHandle = ECFM_INIT_VAL;
            return ECFM_SUCCESS;
        }

        /* If MEP Dirextion is down */
        if (u1MpDirection == ECFM_MP_DIR_DOWN)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: MEP %d is Vlan Aware MEP with Direction Down "
                              "Isid ID is  %d\r\n", pCcMepInfo->u2MepId,
                              (pIsidInfo->u4Isid - ECFM_INTERNAL_ISID_MIN));
            /*Check the port operational Status */
            if (pCcMepInfo->pPortInfo->u1IfOperStatus != CFA_IF_UP)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "Operational State of the port is not UP\r\n");
                pCcMepInfo->b1MepCcmOffloadHwStatus = ECFM_FALSE;
                pCcMepInfo->u2OffloadMepTxHandle = ECFM_INIT_VAL;
                return ECFM_SUCCESS;
            }

            if (EcfmCcCtrlTxFwdToHwForDown (pBuf, pCcMepInfo, pCcMepCcInfo))
            {
                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD:Function EcfmCcCtrlTxFwdToHwForDown:"
                                  "Returned Failure for MEP %d\r\n",
                                  pCcMepInfo->u2MepId);
                return ECFM_FAILURE;
            }
        }
        else
        {
            /* If Up MP then transmit the the CFM-PDU to the
             * frame-filtering 
             */

            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: MEP %d is Vlan Aware MEP with Direction Up "
                              "VLAN ID is  %d\r\n",
                              pCcMepInfo->u2MepId, pIsidInfo->u4Isid);

            pCcMepInfo->pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pCcMepInfo);
            PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO
                (pCcMepInfo->pPortInfo->u2PortNum);
            PduSmInfo.pMepInfo = pCcMepInfo;
            PduSmInfo.pBuf = pBuf;
            PduSmInfo.u4RxVlanIdIsId = pIsidInfo->u4Isid;
            PduSmInfo.u1RxDirection = ECFM_MP_DIR_UP;
            PduSmInfo.EcfmTagType = ECFM_ISID_TAG_INFO;

            ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo.OuterVlanTag),
                         pIsidInfo, sizeof (tEcfmVlanTagInfo));
            EcfmCcOffCtrlTxParsePduHdrs (&PduSmInfo);

            if (EcfmCcCtrlTxFwdToHdForUp (&PduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "OFFLOAD:Function EcfmCcCtrlTxFwdToHdForUp "
                                  "has returned failure For MEPID %d \r\n",
                                  pCcMepInfo->u2MepId);
                return ECFM_FAILURE;
            }
        }                        /* end of If up/Down MP is Vlan aware */

    }
    else
    {
        if (EcfmCcCtrlTxFwdToHwForDown (pBuf, pCcMepInfo, pCcMepCcInfo))
        {
            ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "OFFLOAD: Function EcfmCcCtrlTxFwdToHwdForDown"
                              "ras returned failure for MEPID %d \r\n",
                              pCcMepInfo->u2MepId);
            return ECFM_FAILURE;
        }

    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcmOffFormatIsidEthHdr   
 *
 * Description        : This Routine will Fill the Ethernet Hedder. 
 *                      
 *                      
 * Input(s)           : pMepInfo           - Pointer to the MEP structure
 *                      pu1CcmPdu           - Pointer to the Pdu
 *                      u1Length         -Length of the CFM header formated so
 *                      far
 *                      VlanInfo         - Vlan Information 
 *                                    
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *                      
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcmOffFormatIsidEthHdr (tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu, UINT1
                            u1Length, tIsidTagInfo IsidInfo)
{
    tMacAddr            MacAddr;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2VlanProtoId = ECFM_INIT_VAL;
    UINT2               u2VlanId = ECFM_INIT_VAL;
    BOOL1               b1LlcEncapSts = ECFM_TRUE;
    UINT1              *pu1Pdu = NULL;

    u1Length = u1Length;
    IsidInfo = IsidInfo;
    u2VlanId = u2VlanId;
    u2VlanProtoId = u2VlanProtoId;

    ECFM_CC_TRC_FN_ENTRY ();

    pu1Pdu = *pu1CcmPdu;

    /*Get the LLC Encapsulation status from the port Info */
    b1LlcEncapSts = pMepInfo->pPortInfo->b1LLCEncapStatus;

    /* Fill in the 14 byte of Ethernet hedaer fields */
    /* Fill in the 6byte of Destination Address */
    ECFM_GEN_MULTICAST_CLASS1_ADDR (MacAddr, pMepInfo->u1MdLevel);

    /*Finally copying the Mac address to the pdu */
    ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);
    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 6byte with source address as the address of the Sending
     * MEP*/
    if (ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        return;
    }
    ECFM_GET_MAC_ADDR_OF_PORT ((ECFM_CC_PORT_INFO (pMepInfo->u2PortNum)->
                                u4IfIndex), pu1Pdu);

    u4IfIndex = ECFM_CC_PORT_INFO (pMepInfo->u2PortNum)->u4IfIndex;
    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 2 byte as Length of the CMF PDU if the LLC encap has to be
     * added else fill in as the CFM type */
    /* LLC Snap header not to be filled */
    if (b1LlcEncapSts != ECFM_TRUE)
    {
        /* Fill in the next 2 byte as the CFM type */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_PDU_TYPE_CFM);
    }
    else
    {
        /* Filling LLC Snap header */
        ECFM_CC_SET_LLC_SNAP_HDR (pu1Pdu, (u1Length + ECFM_LLC_SNAP_HDR_SIZE));
    }
    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    UNUSED_PARAM (u4IfIndex);
    return;
}

/****************************************************************************
 * Function Name      : EcfmOffloadHandleOUIChange
 *
 * Description        : This rotuine is reconfigure Offloaded MEPs with New
 *                      OUI value
 *
 * Input(s)           : u4ContextId - Context Identifier.
 *
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmOffloadHandleOUIChange ()
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    for (u4ContextId = ECFM_INIT_VAL; u4ContextId < ECFM_MAX_CONTEXTS;
         u4ContextId++)
    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            continue;
        }

        /* Check if ECFM module is started or not */
        if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
        {
            continue;
        }

        /* For all ports get MEPs configured on these ports */
        for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
             u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
        {
            pPortInfo = NULL;
            pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

            if (pPortInfo != NULL)
            {
                u4IfIndex = pPortInfo->u4IfIndex;
                gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
                pMepInfo = NULL;
                pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                    (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);
                /* Disable Tx/Rx for all MEPs configured on this port with 
                 * the given VLAN Id 
                 * */
                while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
                {
                    pNextMepInfo =
                        (tEcfmCcMepInfo *)
                        RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                       (tRBElem *) pMepInfo, NULL);
                    /* First Disable MEP transmission from this MEP */
                    /* Update Tx/Rx parameters for this MEP */
                    EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                    if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
                    {
                        return ECFM_FAILURE;
                    }
                    pMepInfo = pNextMepInfo;
                }
            }
        }
        ECFM_CC_RELEASE_CONTEXT ();
    }
    ECFM_CC_SELECT_CONTEXT (ECFM_DEFAULT_CONTEXT);
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmOffHandleIntQFailure
 *
 * Description        : This rotuine is used to handle the Interrupt Que 
 *                      Failure
 * 
 * Input(s)           : u2RxHandle - Receive Handle for the remote MEP for which
 *                                   the reception timer expired.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffHandleIntQFailure (UINT2 u2RxHandle)
{

    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcRMepDbInfo   RmepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4ContextId = 0;

    ECFM_MEMSET (&RmepInfo, ECFM_INIT_VAL, sizeof (tEcfmCcRMepDbInfo));
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    RmepInfo.u2OffloadRMepRxHandle = u2RxHandle;

    /* Get Node from Global RBTree maintained for Remote MEP */
    pRmepInfo =
        (tEcfmCcRMepDbInfo *) RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE,
                                         &RmepInfo);

    if (pRmepInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    /* Get MEP Node with which this Remote
     * MEP is associated */
    pMepInfo = pRmepInfo->pMepInfo;

    if (pMepInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepInfo);
    if (pPortInfo == NULL)
    {
        /*MEP is not yet configured on any port */
        return ECFM_SUCCESS;
    }
    u4ContextId = pPortInfo->u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    /*For Down Meps if LOC Defect not present then call RMEP SM */
    if (pRmepInfo->b1RMepCcmDefect != ECFM_TRUE)
    {
        /* Assign MepInfo and Remote MEP Info 
         * in the PduSmInfo for calling the SM */
        PduSmInfo.pRMepInfo = pRmepInfo;
        PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "CFFLOAD:"
                          "Software received the CCM "
                          "Timer Reception Expiry event"
                          "for MEPID %d\r\n", PduSmInfo.pMepInfo->u2MepId);
        /* Call the Remote MEP State Machine for TIME_OUT event */
        EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
    }

    if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
    {
        if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_TRUE) ||
            (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_TRUE) ||
            (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE) ||
            (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_TRUE))
        {
            /* set presentRDI to True */
            pMepInfo->b1PresentRdi = ECFM_TRUE;
        }
        else
        {
            /* set presentRDI to False */
            pMepInfo->b1PresentRdi = ECFM_FALSE;
        }

        if (pMepInfo->b1PresentRdi != pMepInfo->b1CcmOffloadLastRdi)
        {
            EcfmCcmOffDeleteTxRxForMep (pMepInfo);
            if (EcfmCcmOffCreateTxRxForMep (pMepInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_RELEASE_CONTEXT ();
                return ECFM_FAILURE;
            }

            pMepInfo->b1CcmOffloadLastRdi = pMepInfo->b1PresentRdi;
        }
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMplstpCcOffCreateTxMep
 *
 * Description        : This routine is used to initiate CC Tx Offload for
 *                      MPLS-TP path
 * 
 * Input(s)           : pCcTxOffParams - Pointer to CC Offload Information
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplstpCcOffCreateTxMep (tEcfmMplstpCcOffTxInfo * pCcTxOffParams)
{
    /* Converting the time to micro seconds */
    pCcTxOffParams->u4CcInterval = pCcTxOffParams->u4CcInterval *
        ECFM_NUM_OF_USEC_IN_A_MSEC;

    ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                      "\n OFFLOAD: EcfmMplstpCcOffCreateTxMep: "
                      "ContextId = [%u], u4CcInterval = [%u], u2PktLen = [%u],"
                      "u2TxFilterId = [%u]\r\n",
                      pCcTxOffParams->u4ContextId, pCcTxOffParams->u4CcInterval,
                      pCcTxOffParams->u2PduSize,
                      pCcTxOffParams->u2TxFilterHandle);

    if (pCcTxOffParams->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpCcOffCreateTxMep: "
                          "VcId = [%u]\r\n",
                          pCcTxOffParams->pEcfmMplsParams->MplsPathParams.
                          u4PswId);
    }
    else
    {
        ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpCcOffCreateTxMep: "
                          "Tunnel Id = [%u], Tunnel Instance = [%u], Source LSR = [%u],"
                          "Egress LSR = [%u]\r\n",
                          pCcTxOffParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4TunnelId,
                          pCcTxOffParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4TunnelInst,
                          pCcTxOffParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4SrcLer,
                          pCcTxOffParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4DstLer);
    }
    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC, "\n OFFLOAD: "
                      "EcfmMplstpCcOffCreateTxMep: Slot/IfIndex Info = [%p] "
                      "OutIfIndex = [%u]\r\n", pCcTxOffParams->pSlotInfo,
                      pCcTxOffParams->u4OutIfIndex);

    if (pCcTxOffParams->pBuf != NULL)
    {
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pCcTxOffParams->pBuf,
                          pCcTxOffParams->u2PduSize,
                          "EcfmMplstpCcOffCreateTxMep: Offloading the MPLSTP CC PDU to HW\r\n");
    }

#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /*
           if (FsMiEcfmMplsTpHwStartCcmTx(pCcTxOffParams) != FNP_SUCCESS)
           {
           ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
           "\n OFFLOAD: EcfmMplstpCcOffCreateTxMep: Unable to initiate "
           "CC Tx Offload for MPLS-TP Path: "
           "ContextId = [%u], u4CcInterval = [%u], u2PktLen = [%u],"
           "u2TxFilterId = [%u]\r\n",
           pCcTxOffParams->u4ContextId, pCcTxOffParams->u4CcInterval,
           pCcTxOffParams->u2PduSize, pCcTxOffParams->u2TxFilterHandle);

           return ECFM_FAILURE;
           }
         */
        UNUSED_PARAM (pCcTxOffParams);
    }
#else
    UNUSED_PARAM (pCcTxOffParams);
#endif

    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMplstpCcOffDeleteTxMep
 *
 * Description        : This routine is used to stop CC Tx Offload
 *                      for MPLS-TP transport path
 * 
 * Input(s)           : pCcOffTxParams - Pointer to CC Tx Offload Information 
 *                                     (Context, RxHandle, SlotInfo(MBSM) Only)
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplstpCcOffDeleteTxMep (tEcfmMplstpCcOffTxInfo * pCcOffTxParams)
{
    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "\n OFFLOAD: EcfmMplstpCcOffDeleteTxMep: "
                      "ContextId = [%u], u2TxFilterId = [%u]\r\n",
                      pCcOffTxParams->u4ContextId,
                      pCcOffTxParams->u2TxFilterHandle);

    if (pCcOffTxParams->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpCcOffDeleteTxMep: "
                          "VcId = [%u]\r\n",
                          pCcOffTxParams->pEcfmMplsParams->MplsPathParams.
                          u4PswId);
    }
    else
    {

        ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpCcOffDeleteTxMep: "
                          "Tunnel Id = [%u], Tunnel Instance = [%u], Source = [%u],"
                          "Destination = [%u]\r\n",
                          pCcOffTxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4TunnelId,
                          pCcOffTxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4TunnelInst,
                          pCcOffTxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4SrcLer,
                          pCcOffTxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4DstLer);
    }
#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /*
           if (FsMiEcfmMplsTpHwStopCcmTx (pCcOffTxParams) != FNP_SUCCESS)
           {
           ECFM_CC_TRC_ARG (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
           "EcfmMplstpCcOffCreateTxMep: Unable to stop "
           "CC Tx Offload for MPLS-TP Path\r\n");
           return ECFM_FAILURE;
           }
         */
        UNUSED_PARAM (pCcOffTxParams);
    }
#else
    UNUSED_PARAM (pCcOffTxParams);
#endif
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMplstpCcOffCreateRxMep
 *
 * Description        : This function is used to start monitoring the CC 
 *                      messages from Remote MPLS-TP MEP
 * 
 * Input(s)           : pCcOffRxParams - Pointer to CC-Rx Offload Information 
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplstpCcOffCreateRxMep (tEcfmMplstpCcOffRxInfo * pCcOffRxParams)
{
    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC,
                      "\n OFFLOAD: EcfmMplstpCcOffCreateRxMep: "
                      "ContextId = [%u], RxHandle = [%u], RMepId = [%u]",
                      pCcOffRxParams->u4ContextId,
                      pCcOffRxParams->u2RxFilterHandle,
                      pCcOffRxParams->u2RMepId);

    if (pCcOffRxParams->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpCcOffCreateRxMep "
                          "VcId = [%d]\r\n",
                          pCcOffRxParams->pEcfmMplsParams->MplsPathParams.
                          u4PswId);
    }
    else
    {
        ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                          "\n OFFLOAD: EcfmMplstpCcOffCreateRxMep Tunnel Id = [%u], "
                          "Tunnel Instance = [%u], Source = [%u], Destination = [%u]\r\n",
                          pCcOffRxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4TunnelId,
                          pCcOffRxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4TunnelInst,
                          pCcOffRxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4SrcLer,
                          pCcOffRxParams->pEcfmMplsParams->MplsPathParams.
                          MplsLspParams.u4DstLer);
    }

#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /*
           if (FsMiEcfmMplstpHwStartCcmRx (pCcOffRxParams) != FNP_SUCCESS)
           {
           ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
           "\n OFFLOAD: EcfmMplstpCcOffCreateRxMep: Unable to start "
           "monitoring the CC message from remote MPLS-TP MEP "
           "ContextId = [%u], RxHandle = [%u], RMepId = [%u]\r\n",
           pCcOffRxParams->u4ContextId, pCcOffRxParams->u2RxFilterHandle,
           pCcOffRxParams->u2RMepId);

           return ECFM_FAILURE;
           }
         */
        UNUSED_PARAM (pCcOffRxParams);
    }
#else
    UNUSED_PARAM (pCcOffRxParams);
#endif

    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMplstpCcOffDeleteRxMep
 *
 * Description        : This function is used to stop monitoring the CC 
 *                      messages from Remote MPLS-TP MEP
 * 
 * Input(s)           : pCcOffRxParams - Pointer to CC-Rx Offload Information 
 *                                     (Context, RxHandle, SlotInfo(MBSM) Only)
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplstpCcOffDeleteRxMep (tEcfmMplstpCcOffRxInfo * pCcOffRxParams)
{
    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "\n OFFLOAD: EcfmMplstpCcOffDeleteRxMep: "
                      "ContextId = [%u], RxHandle = [%u]",
                      pCcOffRxParams->u4ContextId,
                      pCcOffRxParams->u2RxFilterHandle);
#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        /*
           if (FsMiEcfmMplstpHwStopCcmRx (pCcOffRxParams) != FNP_SUCCESS)
           {
           ECFM_CC_TRC_ARG (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
           "EcfmMplstpCcOffDeleteRxMep: Unable to stop "
           "monitoring the CC message from remote MPLS-TP MEP\r\n");
           return ECFM_FAILURE;
           }
         */
        UNUSED_PARAM (pCcOffRxParams);
    }
#else
    UNUSED_PARAM (pCcOffRxParams);
#endif

    return ECFM_SUCCESS;
}
