/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbrdst.c,v 1.4 2009/09/24 14:30:34 prabuc Exp $
 *
 * Description: This file contains l2red specific stubs for lblt task.
 *************************************************************************/

#include "cfminc.h"
/*****************************************************************************/
/* Function Name      : EcfmRedSyncLtrCacheHldTmr                            */
/*                                                                           */
/* Description        : This function sends LTR Cache Time to standby        */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

PUBLIC VOID
EcfmRedSyncLtrCacheHldTmr (UINT4 u4RemainingTime)
{
    UNUSED_PARAM (u4RemainingTime);
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbrCacheHldTmr                            */
/*                                                                           */
/* Description        : This function sends LBR Cache Time to standby        */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

PUBLIC VOID
EcfmRedSyncLbrCacheHldTmr (UINT4 u4RemainingTime)
{
    UNUSED_PARAM (u4RemainingTime);
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDelayQueueTmrExpiry                       */
/*                                                                           */
/* Description        : This function sends Delay Queue Expiry to standby    */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDelayQueueTmrExpiry (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbLtPdu                                   */
/*                                                                           */
/* Description        : This function sends the received Valid LbLt Pdu to   */
/*                      the stand-by Node.                                   */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbLtPdu (tEcfmBufChainHeader * pBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbLtMepTmrExp                             */
/*                                                                           */
/* Description        : This function sends LTI/LBI  expiry to standby       */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbLtMepTmrExp (UINT1 u1TimerType,
                          UINT4 u4MdIndex, UINT4 u4MaIndex, UINT4 u4MepIndex)
{
    UNUSED_PARAM (u1TimerType);
    UNUSED_PARAM (u4MdIndex);
    UNUSED_PARAM (u4MaIndex);
    UNUSED_PARAM (u4MepIndex);
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncTransactionStopEvent                      */
/*                                                                           */
/* Description        : This function sends LB/TST/Throughput Transaction    */
/*                      stop event to STANDBY Node                           */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncTransactionStopEvent (UINT1 u1TransType,
                                 UINT4 u4MdIndex, UINT4 u4MaIndex,
                                 UINT4 u4MepIndex)
{
    UNUSED_PARAM (u1TransType);
    UNUSED_PARAM (u4MdIndex);
    UNUSED_PARAM (u4MaIndex);
    UNUSED_PARAM (u4MepIndex);
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmDeadlineTimerExp                        */
/*                                                                           */
/* Description        : This function will send DM Deadline Timeout event    */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmDeadlineTimerExp (tEcfmLbLtMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmStopTransaction                         */
/*                                                                           */
/* Description        : This function will send DM Stop Transaction event    */
/*                      to STANDBY node when number of DMMs to be transmitted*/
/*                      becomes 0.                                           */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmStopTransaction (tEcfmLbLtMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbCache                                   */
/*                                                                           */
/* Description        : This function  syncs the LB Cache at LBLT Task at    */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbCache ()
{
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmTransIntrvalExp                         */
/*                                                                           */
/* Description        : This function will send 1DM Tramsaction Interval Exp */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmTransIntrvalExp (tEcfmLbLtMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmCache                                   */
/*                                                                           */
/* Description        : This function syncs the DM Cache at LBLT Task at     */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmCache ()
{
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncStopLbTstTran                             */
/*                                                                           */
/* Description        : This function sends Stop event for LB/TST            */
/*                      transaction to the stand-by Node.                    */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncStopLbTstTran (UINT1 u1MsgType, tEcfmLbLtMsg * pMsg)
{
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (pMsg);
    return;
}
