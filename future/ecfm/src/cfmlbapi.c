/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbapi.c,v 1.46 2016/07/25 07:25:12 siva Exp $
 *
 * Description: This file cntains the API for ECFM Module for LBLT   
 *              Task
 *******************************************************************/

#include "cfminc.h"

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtNotifySM
 * 
 * DESCRIPTION      : API Function notify LBLT state machines about MEP state
 *                    (Active/NotActive).
 *
 * INPUT            : u4MdIndex - MD Index of MEP to be notified
 *                    u4MaIndex - MA Index of MEP to be notified
 *                    u2MepId -   MepIdentifier of the MEP to be notified
 *                    u4ContextId - Current Context ID
 *                    u1Indication - IND_MODULE_ENABLE/DISABLE     
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtNotifySM (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                  UINT4 u4ContextId, UINT1 u1Indication)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    ECFM_LBLT_LOCK ();

    /* Select Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP entry from PortInfo's MepInfoTree */
    if ((pLbLtMepNode =
         EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex,
                                         u2MepId)) == NULL)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Notify the the State Machines for the event */
    EcfmLbLtUtilNotifySm (pLbLtMepNode, u1Indication);
    EcfmLbLtUtilNotifyY1731 (pLbLtMepNode, u1Indication);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtAddMepEntry
 * 
 * DESCRIPTION      : API Function creates and add a MEP node in LBLT Global 
 *                    structure, MepTableIndex.
 *
 * INPUT            : pCcMepNode - Pointer to Cc Task's MEP Info
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *

 ******************************************************************************/
PUBLIC INT4
EcfmLbLtAddMepEntry (tEcfmCcMepInfo * pCcMepNode, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNewNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtMaInfo    *pMaInfo = NULL;
    ECFM_LBLT_LOCK ();

    /* Select Context for the LBLT Task */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for MEP Node */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_TABLE (pLbLtMepNewNode) == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtAddMepEntry:"
                      "MEM Block Allocation for MEP node Failed" "\r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pLbLtMepNewNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);

    /* Assigning Values to the MepNode to be added */
    pLbLtMepNewNode->u4MdIndex = pCcMepNode->u4MdIndex;
    pLbLtMepNewNode->u4MaIndex = pCcMepNode->u4MaIndex;
    pLbLtMepNewNode->u2MepId = pCcMepNode->u2MepId;
    pLbLtMepNewNode->u1MdLevel = pCcMepNode->u1MdLevel;
    pLbLtMepNewNode->u4PrimaryVidIsid = pCcMepNode->u4PrimaryVidIsid;
    pLbLtMepNewNode->u1Direction = pCcMepNode->u1Direction;
    pLbLtMepNewNode->u1CcmLtmPriority = pCcMepNode->u1CcmLtmPriority;
    pLbLtMepNewNode->u2PortNum = pCcMepNode->u2PortNum;
    pLbLtMepNewNode->b1Active = pCcMepNode->b1Active;
    pLbLtMepNewNode->b1LckDelayExp = ECFM_FALSE;
    pLbLtMepNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    /* Put backward pointer to its associated MA */
    pMaInfo = EcfmLbLtUtilGetMaEntry (pCcMepNode->u4MdIndex,
                                      pCcMepNode->u4MaIndex);
    pLbLtMepNewNode->pMaInfo = pMaInfo;

    /* Set LB related info with default values */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pLbLtMepNewNode);
    pLbInfo->b1TxLbmDropEligible = ECFM_TRUE;
    pLbInfo->u1TxLbmVlanPriority =
        pCcMepNode->pMaInfo->pMdInfo->u1CfmVlanPriority;
    pLbInfo->b1TxLbmResultOk = ECFM_TRUE;
    pLbInfo->u1TxLbmStatus = ECFM_TX_STATUS_READY;
    pLbInfo->u4NextLbmSeqNo = ECFM_LBM_SEQ_NUM_INIT_VAL;
    pLbInfo->u1LbmTtl = ECFM_LBM_TTL_DEF;
    pLbInfo->u1TxLbmMode = ECFM_LBLT_LB_REQ_RES_TYPE;

    /* Y.1731 Specific default values */
    pLbInfo->u1LbCapability = ECFM_ENABLE;
    pLbInfo->u1TxLbmTlvOrNone = ECFM_LBLT_LBM_WITHOUT_TLV;
    pLbInfo->u1TxLbmDestType = ECFM_TX_DEST_TYPE_UNICAST;
    pLbInfo->b1TxLbmVariableBytes = ECFM_FALSE;
    pLbInfo->u1TxLbmTstPatternType = ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC;
    pLbInfo->u2TxLbmMessages = ECFM_LB_MESG_DEF_VAL;
    pLbInfo->u1RxMCastLbmCap = ECFM_DISABLE;
    pLbInfo->u1TxLbmIntervalType = ECFM_LBLT_LB_INTERVAL_MSEC;
    pLbInfo->u4TxLbmInterval = ECFM_LB_BURST_INTERVAL_DEF_VAL;
    pLbInfo->u4TxLbmDeadline = Y1731_LB_DEADLINE_DEF_VAL;
    pLbInfo->u4RxLbrTimeout = ECFM_LBR_TIMEOUT_DEF_VAL;

    /* Set LT related info with default values */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pLbLtMepNewNode);
    pLtInfo->u2TxLtmTtl = ECFM_LTM_TTL_DEF_VAL;
    pLtInfo->b1TxLtmResult = ECFM_TRUE;
    pLtInfo->u1TxLtmStatus = ECFM_TX_STATUS_READY;

    /*Y.1731 Specific LT Defaults */
    pLtInfo->u2LtrTimeOut = ECFM_LTR_TIMEOUT_DEF_VAL;
    pLtInfo->b1TxLtmDropEligible = ECFM_FALSE;
    pLtInfo->u1TxLtmVlanPriority =
        pCcMepNode->pMaInfo->pMdInfo->u1CfmVlanPriority;
    pLtInfo->u4LtmNextSeqNum = ECFM_LTM_SEQ_NUM_INIT_VAL;
    ECFM_LBLT_SET_USE_FDB_ONLY (pLtInfo->u1TxLtmFlags);

    pDmInfo = &(pLbLtMepNewNode->DmInfo);
    pDmInfo->u2TxDmInterval = ECFM_DM_INTERVAL_DEF_VAL;
    pDmInfo->u1DmStatus = ECFM_TX_STATUS_READY;
    pDmInfo->u2NoOfDmrIn = ECFM_INIT_VAL;
    pDmInfo->u2NoOf1DmIn = ECFM_INIT_VAL;
    pDmInfo->b1Tx1DmDropEligible = ECFM_FALSE;
    pDmInfo->u1Tx1DmVlanPriority =
        pCcMepNode->pMaInfo->pMdInfo->u1CfmVlanPriority;
    pDmInfo->u1TxDmType = ECFM_LBLT_DM_TYPE_DMM;
    pDmInfo->u1Rcv1DmCapability = ECFM_ENABLE;
    pDmInfo->b1TxDmrOpFields = ECFM_FALSE;
    pDmInfo->b1TxDmmDropEligible = ECFM_FALSE;
    pDmInfo->u1TxDmmVlanPriority =
        pCcMepNode->pMaInfo->pMdInfo->u1CfmVlanPriority;
    pDmInfo->u4TxDmSeqNum = ECFM_DMM_SEQ_NUM_INIT_VAL;
    pDmInfo->u4FrmDelayThreshold = ECFM_FD_BUFFER_DEF_THRESHOLD;

    /* Set TST related info with default values */
    pTstInfo = &(pLbLtMepNewNode->TstInfo);
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pLbLtMepNewNode);
    pTstInfo->u4TstDeadLine = ECFM_TST_DEADLINE_DEF_VAL;
    pTstInfo->u4TstInterval = ECFM_TST_INTERVAL_DEF_VAL;
    pTstInfo->u4TstSeqNumber = ECFM_TST_SEQ_NUM_INIT_VAL;
    pTstInfo->u4TstMessages = ECFM_TST_MESG_DEF_VAL;
    pTstInfo->u4TstsSent = ECFM_INIT_VAL;
    pTstInfo->u4BitErroredTstIn = ECFM_INIT_VAL;
    pTstInfo->u4ValidTstIn = ECFM_INIT_VAL;
    pTstInfo->u2TstPatternSize = ECFM_INIT_VAL;
    pTstInfo->u2TstLastPatternSize = ECFM_INIT_VAL;
    pTstInfo->u1MulticastTstRxCap = ECFM_DISABLE;
    pTstInfo->u1TstCapability = ECFM_ENABLE;
    pTstInfo->u1TstPatternType = ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC;
    pTstInfo->u1TstVlanPriority =
        pCcMepNode->pMaInfo->pMdInfo->u1CfmVlanPriority;
    pTstInfo->u1TstStatus = ECFM_TX_STATUS_READY;
    pTstInfo->u1TxTstIntervalType = ECFM_LBLT_TST_INTERVAL_SEC;
    pTstInfo->u1TxTstDestType = ECFM_TX_DEST_TYPE_UNICAST;
    pTstInfo->b1TstDropEligible = ECFM_FALSE;
    pTstInfo->b1TstResultOk = ECFM_TRUE;
    pTstInfo->b1TstVariableBytes = ECFM_FALSE;

    /* Set TH related info with default values */
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pLbLtMepNewNode);

    pThInfo->u4TxThPps = ECFM_TH_PPS_DEF_VAL;
    pThInfo->u2TxThFrameSize = ECFM_LBLT_TH_PKT_DEF_SIZE;
    pThInfo->u4TxThInterval =
        (UINT4) (ECFM_NUM_OF_USEC_IN_A_SEC / pThInfo->u4TxThPps);
    pThInfo->u1ThState = ECFM_TH_STATE_DEFAULT;
    pThInfo->d8MeasuredThBps = ECFM_INIT_VAL;
    pThInfo->d8TxThTL = ECFM_LBLT_TH_MIN;
    pThInfo->d8TxThTU = ECFM_INIT_VAL;
    pThInfo->d8TxThTH = ECFM_INIT_VAL;
    pThInfo->u2TxThBurstMessages = ECFM_LBLT_TH_BURST_MESSAGES_DEF_VAL;
    pThInfo->u4TxThBurstDeadline = ECFM_LBLT_TH_BURST_DEADLINE_DEF_VAL;
    pThInfo->u1TxThType = ECFM_LBLT_TH_TYPE_DEF_VAL;
    pThInfo->u1TxThBurstType = ECFM_LBLT_TH_BURSTTYPE_DEF_VAL;
    pThInfo->b1TxThResultOk = ECFM_TRUE;
    /* Adding MEP Node to the MepTableIndex Tree in the Global Info also */
    /* Set the cli handle to the default values */
    pLbLtMepNewNode->ThInfo.CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
    pLbLtMepNewNode->LtInfo.CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
    pLbLtMepNewNode->LbInfo.CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
    pLbLtMepNewNode->TstInfo.CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;

    /* Stores the MPLS-TP Path Info pointer from CC Mep Entry.
     * It is expected that CC and LBLT lock is taken before this 
     * assignment and modification to MPLS-TP Path Params.
     */
    pLbLtMepNewNode->pEcfmMplsParams = pCcMepNode->pEcfmMplsParams;
    pLbLtMepNewNode->pCcMepInfo = pCcMepNode;

    if (RBTreeAdd (ECFM_LBLT_MEP_TABLE, pLbLtMepNewNode) == ECFM_RB_FAILURE)

    {
        /* Free memory allocated to MEP new node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_TABLE_POOL,
                             (UINT1 *) pLbLtMepNewNode);
        pLbLtMepNewNode = NULL;
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmLbLtAddMepEntry:MEP node addition FAILED \r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtAddMepInAPort
 * 
 * DESCRIPTION      : API Function adds MEP node in PortInfo's MepInfoTree 
 *                    in LBLT task.
 *
 * INPUT            : pMepNode - Pointer to CC Task's MEP Info
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtAddMepInAPort (tEcfmCcMepInfo * pMepNode)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtPortInfo  *pPortEntry = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get Mep node from the Global Info Mep Table */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (pMepNode->u4MdIndex,
                                        pMepNode->u4MaIndex, pMepNode->u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Set Mep node's MepInfoTree indices from corresponding CC Task's Mep 
     * Node */
    pLbLtMepNode->u2PortNum = pMepNode->u2PortNum;
    pLbLtMepNode->u4PrimaryVidIsid = pMepNode->u4PrimaryVidIsid;
    pLbLtMepNode->u1Direction = pMepNode->u1Direction;
    pLbLtMepNode->u1MdLevel = pMepNode->u1MdLevel;

    /* Add Mep Node to port Info's MepInfoTree */
    pPortEntry = ECFM_LBLT_GET_PORT_INFO (pMepNode->u2PortNum);

    if (pPortEntry == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmLbLtAddMepInAPort:No Port Information \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    pLbLtMepNode->pPortInfo = pPortEntry;
    pLbLtMepNode->u4IfIndex = pPortEntry->u4IfIndex;

    /* Mplstp Oam mep are not added into ECFM_LBLT_PORT_MEP_TABLE 
     * it is added only in ECFM_LBLT_MEP_TABLE.
     */
    if (!((pMepNode->pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMepNode->pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {
        if (RBTreeAdd (ECFM_LBLT_PORT_MEP_TABLE, pLbLtMepNode) ==
            ECFM_RB_FAILURE)
        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtAddMepInStack
 * 
 * DESCRIPTION      : API Function creates and add stack entry corresponding to 
 *                    Mep node ,in Global Info's Stack Table in LBLT task.
 *
 * INPUT            : u4MdIndex - MD Index 
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *                    u4ContextId - current Context Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *

 ******************************************************************************/
PUBLIC INT4
EcfmLbLtAddMepInStack (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                       UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtStackInfo *pStackNewNode = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get MEP node from LBLT Global Info */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_STACK_TABLE (pStackNewNode) == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtAddMepInStack"
                      "MEM Block Allocation for Stack node Failed" "\r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pStackNewNode, ECFM_INIT_VAL, ECFM_LBLT_STACK_INFO_SIZE);

    /* Put required parameters from MEP node */
    pStackNewNode->pLbLtMepInfo = pLbLtMepNode;
    pStackNewNode->pLbLtMipInfo = NULL;
    pStackNewNode->u4VlanIdIsid = pLbLtMepNode->u4PrimaryVidIsid;
    pStackNewNode->u1MdLevel = pLbLtMepNode->u1MdLevel;
    pStackNewNode->u1Direction = pLbLtMepNode->u1Direction;
    pStackNewNode->u2PortNum = pLbLtMepNode->u2PortNum;

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (pLbLtMepNode->u2PortNum);
    if (pPortInfo == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmLbLtAddMepInStack: No port information available \r\n");

        /*Release the memory allocated to Stack new node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackNewNode));
        pStackNewNode = NULL;
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Add stack node in global info's Stack Table */
    if (RBTreeAdd (pPortInfo->StackInfoTree, pStackNewNode) == ECFM_RB_FAILURE)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmLbLtAddMepInStack: MEP Addition in Stack Table"
                      "Failed \r\n");

        /*Release the memory allocated to Stack new node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackNewNode));
        pStackNewNode = NULL;
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtUpdateMepToMptpMepTbl
 *
 * Description        : This API is used to add/delete the MPLS-TP based MEPs
 *                      to/from LBLT MPLS-TP MEP RB Tree on addition/deletion 
 *                      of MPLS-TP based MEP entry in CC MPLS-TP MEP RB Tree.
 *
 *                      This API shall take the LBLT lock before adding or 
 *                      deleting the entry.
 * 
 *                      Usage: On reception of LBM/LBR PDU, based on the 
 *                      MPLS-TP path on which the packet is received, the 
 *                      associated MEP is obtained by searching this RB-Tree 
 *                      with MPLS-TP path information (LSP/PW information) 
 *                      and further processing is done.
 *
 *
 * Input(s)           : pCcMepNode - CC MEP Entry
 *                      u1Flag     - ECFM_ADD - To add entry to MPLS-TP MEP 
 *                                              table
 *                                   ECFM_DEL - To delete entry from MPLS-TP
 *                                              MEP table
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS - On successful addition/deletion
 *                      ECFM_FAILURE - Otherwise
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtUpdateMepToMptpMepTbl (tEcfmCcMepInfo * pCcMepNode, UINT1 u1Flag)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    INT4                i4RetVal = ECFM_SUCCESS;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get Mep node from the Global Info Mep Table */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (pCcMepNode->u4MdIndex,
                                        pCcMepNode->u4MaIndex,
                                        pCcMepNode->u2MepId);
    if (pLbLtMepNode == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    switch (u1Flag)
    {
        case ECFM_ADD:
            /* Add entry into RB-Tree based MPLS-TP MEP table */
            if (RBTreeAdd (ECFM_LBLT_MPLSMEP_TABLE, (tRBElem *) pLbLtMepNode)
                != RB_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "Unable to add MPLS-TP MEP entry into the RB-Tree based "
                               "LBLT MPLS-TP MEP table\r\n");
                i4RetVal = ECFM_FAILURE;
            }
            break;

        case ECFM_DEL:
            /* Delete entry from RB-Tree based MPLS-TP MEP table */
            if (RBTreeRem (ECFM_LBLT_MPLSMEP_TABLE, (tRBElem *) pLbLtMepNode)
                != RB_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "Unable to delete MPLS-TP MEP entry from the RB-Tree "
                               "based LBLT MPLS-TP MEP table\r\n");
                i4RetVal = ECFM_FAILURE;
            }
            break;

        default:
            i4RetVal = ECFM_FAILURE;
            break;
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmAddMipInLbLtStack
 *  
 *    DESCRIPTION      : This function adds MIP entry
 *                       corresponding to indices in StackTable. 
 *                            
 *   
 *    INPUT            : u2PortNum - Local Port number where MIP has been
 *                                   configured
 *                       u1MdLevel - MdLevel of MIP
 *                       u2VlanId - VlanId with which MIP is associated
 *   
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *   

 ******************************************************************************/
PRIVATE INT4
EcfmAddMipInLbLtStack (UINT2 u2PortNum, UINT1 u1MdLevel, UINT2 u2VlanId)
{
    tEcfmLbLtStackInfo *pStackUpNode = NULL;
    tEcfmLbLtStackInfo *pStackDownNode = NULL;
    tEcfmLbLtMipInfo   *pLbLtMipNode = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    /* Create Stack nodes corresponding to MIP */
    /* Allocate memory for Stack node corresponding for up MHF */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_STACK_TABLE (pStackUpNode) == NULL)

    {
        return ECFM_FAILURE;
    }

    /* Allocate memory for Stack node corresponding for down MHF */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_STACK_TABLE (pStackDownNode) == NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        pStackUpNode = NULL;
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pStackUpNode, ECFM_INIT_VAL, ECFM_LBLT_STACK_INFO_SIZE);
    ECFM_MEMSET (pStackDownNode, ECFM_INIT_VAL, ECFM_LBLT_STACK_INFO_SIZE);

    /* Get MIP Info from MipTable, in Global info */
    pLbLtMipNode = EcfmLbLtUtilGetMipEntry (u2PortNum, u1MdLevel, u2VlanId);
    if (pLbLtMipNode == NULL)
    {
        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Put required parameters for up MHF */
    pStackUpNode->pLbLtMepInfo = NULL;
    pStackUpNode->pLbLtMipInfo = pLbLtMipNode;
    pStackUpNode->u4VlanIdIsid = u2VlanId;
    pStackUpNode->u1MdLevel = u1MdLevel;
    pStackUpNode->u1Direction = ECFM_MP_DIR_UP;
    pStackUpNode->u2PortNum = u2PortNum;

    /* Put required parameters for down MHF */
    pStackDownNode->pLbLtMepInfo = NULL;
    pStackDownNode->pLbLtMipInfo = pLbLtMipNode;
    pStackDownNode->u4VlanIdIsid = u2VlanId;
    pStackDownNode->u1MdLevel = u1MdLevel;
    pStackDownNode->u1Direction = ECFM_MP_DIR_DOWN;
    pStackDownNode->u2PortNum = u2PortNum;

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }
    /* Add up MHF stack node in Global info's Stack Table */
    if (RBTreeAdd (pPortInfo->StackInfoTree, pStackUpNode) == ECFM_RB_FAILURE)

    {

        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }

    /* Add down MHF stack node in Global info's Stack Table */
    if (RBTreeAdd (pPortInfo->StackInfoTree, pStackDownNode) == ECFM_RB_FAILURE)

    {

        /* Remove up MHF stack node also */
        RBTreeRem (pPortInfo->StackInfoTree, pStackUpNode);

        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtDeleteLTMReplyEntryForMep
 *
 * DESCRIPTION      : API Function to delete entries in LTM Reply list for
                      this MEP.
 *
 * INPUT            : pLbLtMepNode: pointer to MepInfo 
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PRIVATE VOID
EcfmLbLtDeleteLTMReplyEntryForMep (tEcfmLbLtMepInfo * pLbLtMepNode)
{
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNode = NULL;
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNextNode = NULL;
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmLbLtLtrInfo   *pLtrTempNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tEcfmLbLtLtmReplyListInfo LtmReplyListInfo;
    ECFM_MEMSET (&LtmReplyListInfo, ECFM_INIT_VAL,
                 ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);

    LtmReplyListInfo.u4MdIndex = pLbLtMepNode->u4MdIndex;
    LtmReplyListInfo.u4MaIndex = pLbLtMepNode->u4MaIndex;
    LtmReplyListInfo.u2MepId = pLbLtMepNode->u2MepId;

    pLtmReplyListNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGet
        (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) & LtmReplyListInfo);

    while ((pLtmReplyListNode != NULL) &&
           (pLtmReplyListNode->u4MdIndex == pLbLtMepNode->u4MdIndex) &&
           (pLtmReplyListNode->u4MaIndex == pLbLtMepNode->u4MaIndex) &&
           (pLtmReplyListNode->u2MepId == pLbLtMepNode->u2MepId))
    {
        pLtmReplyListNextNode = RBTreeGetNext (ECFM_LBLT_LTM_REPLY_LIST,
                                               pLtmReplyListNode, NULL);

        /* Delete LtrListDll corresponding to a LTM and each LTR node
         * from LtrTable, in global info */
        pLtrNode = (tEcfmLbLtLtrInfo *)
            TMO_DLL_First (&(pLtmReplyListNode->LtrList));
        while (pLtrNode != NULL)

        {
            pLtrTempNode = pLtrNode;

            /* Move to next LTR corresponding to a LTM node */
            pLtrNode = (tEcfmLbLtLtrInfo *)
                TMO_DLL_Next (&(pLtmReplyListNode->LtrList),
                              &(pLtrTempNode->LtrTableMepDllNode));
            TMO_DLL_Delete (&(pLtmReplyListNode->LtrList),
                            &(pLtrTempNode->LtrTableMepDllNode));

            /* Remove LTR node from LTR Table in global info */
            RBTreeRem (ECFM_LBLT_LTR_TABLE, (tRBElem *) pLtrTempNode);

            /* Release the memory allocated to chassisId,Org specific TLV,
               PortId, Mgt address and its domain */
            pSenderId = &(pLtrTempNode->SenderId);
            if (pSenderId->ChassisId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL,
                                     pSenderId->ChassisId.pu1Octets);
                pSenderId->ChassisId.pu1Octets = NULL;
            }
            if (pSenderId->MgmtAddress.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL,
                                     pSenderId->MgmtAddress.pu1Octets);
                pSenderId->MgmtAddress.pu1Octets = NULL;
            }
            if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL,
                                     pSenderId->MgmtAddressDomain.pu1Octets);
                pSenderId->MgmtAddressDomain.pu1Octets = NULL;
            }
            if (pLtrTempNode->OrgSpecTlv.Value.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL,
                                     pLtrTempNode->OrgSpecTlv.Value.pu1Octets);
                pLtrTempNode->OrgSpecTlv.Value.pu1Octets = NULL;
            }
            if (pLtrTempNode->EgressPortId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
                                     pLtrTempNode->EgressPortId.pu1Octets);
                pLtrTempNode->EgressPortId.pu1Octets = NULL;
            }
            if (pLtrTempNode->IngressPortId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
                                     pLtrTempNode->IngressPortId.pu1Octets);
                pLtrTempNode->IngressPortId.pu1Octets = NULL;
            }

            /* Releasing mem block allocated to LTR node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                 (UINT1 *) (pLtrTempNode));
            pLtrTempNode = NULL;
        }

        RBTreeRem (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) pLtmReplyListNode);

        /* Releasing mem block allocated to LtmReplyListNode */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL,
                             (UINT1 *) (pLtmReplyListNode));

        pLtmReplyListNode = pLtmReplyListNextNode;
    }

}

/******************************************************************************
 *    FUNCTION NAME    : EcfmDeleteMipFromLbLtStack
 *  
 *    DESCRIPTION      : This function deletes MIP entry
 *                       corresponding to indices from StackTable. 
 *                            
 *   
 *    INPUT            : u2PortNum - Local Port Number where MIP has been
 *                                   configured
 *                       u1MdLevel - MdLevel of MIP
 *                       u2VlanId - VlanId with which MIP is associated
 *   
 *    OUTPUT           : None.
 *
 *    RETURNS          : None.
 *   
 ******************************************************************************/
PRIVATE VOID
EcfmDeleteMipFromLbLtStack (UINT2 u2PortNum, UINT1 u1MdLevel,
                            UINT4 u4VlanIdIsid)
{
    tEcfmLbLtStackInfo *pStackUpNode = NULL;
    tEcfmLbLtStackInfo *pStackDownNode = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return;
    }
    /* Get up Mhf node */
    pStackUpNode = EcfmLbLtUtilGetMp (u2PortNum, u1MdLevel, u4VlanIdIsid,
                                      ECFM_MP_DIR_UP);
    /* Check if it is MIP */
    if ((pStackUpNode != NULL) && (ECFM_LBLT_IS_MHF (pStackUpNode)))

    {

        /* Delete up MHF node */
        RBTreeRem (pPortInfo->StackInfoTree, pStackUpNode);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        pStackUpNode = NULL;
    }

    /* Get down Mhf node */
    pStackDownNode = EcfmLbLtUtilGetMp (u2PortNum, u1MdLevel, u4VlanIdIsid,
                                        ECFM_MP_DIR_DOWN);
    /* Check if it is MIP */
    if ((pStackDownNode != NULL) && (ECFM_LBLT_IS_MHF (pStackDownNode)))

    {

        /* Delete down MHF node */
        RBTreeRem (pPortInfo->StackInfoTree, pStackDownNode);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackDownNode = NULL;
    }
    return;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtAddMipEntry
 * 
 * DESCRIPTION      : API Function creates and add MIP entry in  
 *                    in Global Info's Stack Table and Mip Table in LBLT task.
 *
 * INPUT            : pMipNode  - Pointer to the MIP Node 
 *                    u4MdIndex - Md Index of MD at which MIP is configured
 *                    u4MaIndex - MaIndex of MA with which MIP creation
 *                    is based
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *

 ******************************************************************************/
PUBLIC INT4
EcfmLbLtAddMipEntry (tEcfmCcMipInfo * pMipNode, UINT4 u4ContextId)
{
    tEcfmLbLtMipInfo   *pMipNewNode = NULL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;       
    UINT4               u4MaIndex = ECFM_INIT_VAL;     
    UINT2               u2PortNum = ECFM_INIT_VAL;
    UINT2               u2VlanId = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
    UINT1               u1RowStatus = ECFM_INIT_VAL;

    u2PortNum = pMipNode->u2PortNum;
    u1MdLevel = pMipNode->u1MdLevel;
    u2VlanId = (UINT2) pMipNode->u4VlanIdIsid;
    u1RowStatus = pMipNode->u1RowStatus;
    u4MdIndex = pMipNode->u4MdIndex;
    u4MaIndex = pMipNode->u4MaIndex;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Create node for Mip */
    /* Allocate memory for Mip node */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MIP_TABLE (pMipNewNode) == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtAddMipEntry"
                      "Allocation for Mip Node Failed \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMipNewNode, ECFM_INIT_VAL, ECFM_LBLT_MIP_INFO_SIZE);

    /* put values in Mip node */
    pMipNewNode->u2PortNum = u2PortNum;
    pMipNewNode->u1MdLevel = u1MdLevel;
    pMipNewNode->u4VlanIdIsid = u2VlanId;
    pMipNewNode->b1Active = ECFM_TRUE;
    pMipNewNode->u1RowStatus = u1RowStatus;
    pMipNewNode->u4MdIndex = u4MdIndex;
    pMipNewNode->u4MaIndex = u4MaIndex;

    /* Add MIP node in GlobalInfo's RBTree MipTable */
    if (RBTreeAdd (ECFM_LBLT_MIP_TABLE, pMipNewNode) == ECFM_RB_FAILURE)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmLbLtAddMipEntry : MIP addition in LBLT MipTable"
                      "Failed!!\r\n");

        /* Release memory allocated to MIP node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_TABLE_POOL, (UINT1 *) (pMipNewNode));
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /*Add Mip in stack Table */
    if (EcfmAddMipInLbLtStack (u2PortNum, u1MdLevel, u2VlanId) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmLbLtAddMipEntry : MIP addition in LBLT StackTable"
                      "Failed!!\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtDeleteMipEntry
 * 
 * DESCRIPTION      : API Function to delete MipInfo, from MipTable
 *                    Stack Table.
 *
 * INPUT            : u2PortNum - Local Port Number of the MIP to be deleted.
 *                    u1MdLevel - Md Level of the MIP to be deleted.
 *                    u2VlanId -  ValnId of MIP to be deleted.
 *                    u4ContextId - Current Context Id 
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtDeleteMipEntry (UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VlanIdIsid,
                        UINT4 u4ContextId)
{
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pMipNode = EcfmLbLtUtilGetMipEntry (u2PortNum, u1MdLevel, u4VlanIdIsid);
    if (pMipNode == NULL)
    {
        return;
    }
    /* Delete node from Mip Table */
    RBTreeRem (ECFM_LBLT_MIP_TABLE, pMipNode);

    /* Release memory allocated to Mip node */
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_TABLE_POOL, (UINT1 *) (pMipNode));
    pMipNode = NULL;

    /* Delete corresponding nodes from Stack Table */
    EcfmDeleteMipFromLbLtStack (u2PortNum, u1MdLevel, u4VlanIdIsid);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfMepRowStatus
 * 
 * DESCRIPTION      : API Function update MEP RowStatus in LBLT task's MEP 
 *                    node.
 *
 * INPUT            : u4MdIndex - MD Index of the MEP 
 *                    u4MaIndex - MA Index of the MEP 
 *                    u2MepId - MepIdentifier of the MEP.
 *                    u1RowStatus - MEP's row status to be set in LBLT task 
 *                    u4ContextId - Current Context Id  
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfMepRowStatus (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                          UINT1 u1RowStatus, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;

    ECFM_LBLT_LOCK ();

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP entry from MepTableIndex,LBLT global info */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtConfMepRowStatus: MEP does not exists at LBLT task "
                      "\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Set row status in LBLT task's MEP */
    pLbLtMepNode->u1RowStatus = u1RowStatus;

    if (pLbLtMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        if ((pMaNode = EcfmSnmpLwGetMaEntry (u4MdIndex, u4MaIndex)) == NULL)
        {
            ECFM_LBLT_UNLOCK ();
            return;
        }

        if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
            (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
        {
            pLbLtMepNode->pPortInfo =
                ECFM_LBLT_GET_PORT_INFO (ECFM_LBLT_MAX_MEP_INFO);
        }
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfMepActive
 * 
 * DESCRIPTION      : API Function to update MepActive variable in LBLT task
 *                    MEP node .
 *
 * INPUT            : u4MdIndex - MD Index of the MEP 
 *                    u4MaIndex - MA Index of the MEP 
 *                    u2MepId - MepIdentifier of the MEP.
 *                    b1Active - Mep Active status to be set in LBLT task 
 *                    u4ContextId - Current Context Id 
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfMepActive (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId, BOOL1
                       b1Active, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP Info from MepTableIndex, in Global info */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtConfMepActive: MEP does not exists at LBLT task "
                      "\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Update MepActive in LBLT task's MEP */
    pLbLtMepNode->b1Active = b1Active;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfY1731Defaults
 * 
 * DESCRIPTION      : API Function to update Y.1731 specific default values in
 *                    the LBLT's MEP info.
 *
 * INPUT            : u4MdIndex - MD Index of the MEP 
 *                    u4MaIndex - MA Index of the MEP 
 *                    u2MepId - MepIdentifier of the MEP.
 *                    u4ContextId - Current Context Id 
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfY1731Defaults (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                           UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP Info from MepTableIndex, in Global info */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtConfMepActive: MEP does not exists at LBLT task "
                      "\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pLbLtMepNode->LbInfo.u2TxLbmMessages = Y1731_LB_MESG_DEF_VAL;
    pLbLtMepNode->LbInfo.u1RxMCastLbmCap = ECFM_DISABLE;
    pLbLtMepNode->LbInfo.u1TxLbmIntervalType = ECFM_LBLT_LB_INTERVAL_SEC;
    pLbLtMepNode->LbInfo.u4TxLbmInterval = Y1731_LB_INTERVAL_DEF_VAL;
    pLbLtMepNode->LbInfo.u4TxLbmDeadline = Y1731_LB_DEADLINE_DEF_VAL;
    pLbLtMepNode->LbInfo.u1TxLbmMode = ECFM_LBLT_LB_REQ_RES_TYPE;
    pLbLtMepNode->LbInfo.u1TxLbmTlvOrNone = ECFM_LBLT_LBM_WITHOUT_TLV;
    pLbLtMepNode->LbInfo.u4NextLbmSeqNo = ECFM_LBM_SEQ_NUM_INIT_VAL;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfMipActive
 * 
 * DESCRIPTION      : API Function to update MipActive variable in LBLT task
 *                    MIP node .
 *
 * INPUT            : u2PortNum - Local Port Number of the MIP 
 *                    u1MdLevel - Md Level of the MIP 
 *                    u2VlanId - VlanId of the MIP.
 *                    b1Active - Mip Active status to be set in LBLT task 
 *                    u4ContextId - Current Context Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfMipActive (UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VlanIdIsid,
                       BOOL1 b1Active, UINT4 u4ContextId)
{
    tEcfmLbLtMipInfo   *pLbLtMipNode = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MIP Info from MipTable, in Global info */
    pLbLtMipNode = EcfmLbLtUtilGetMipEntry (u2PortNum, u1MdLevel, u4VlanIdIsid);
    if (pLbLtMipNode == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Update MepActive in LBLT task's MIP */
    pLbLtMipNode->b1Active = b1Active;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfMipRowStatus
 * 
 * DESCRIPTION      : API Function to update Mip RowStatus variable in LBLT task
 *                    MIP node .
 *
 * INPUT            : u2PortNum - Local Port Number of the MIP 
 *                    u1MdLevel - Md Level of the MIP 
 *                    u2VlanId -  VlanId of the MIP.
 *                    u1RowStatus - Mip row status to be set in LBLT task 
 *                    u4ContextId - Current Context Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfMipRowStatus (UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VlanIdIsid,
                          UINT1 u1RowStatus, UINT4 u4ContextId)
{
    tEcfmLbLtMipInfo   *pLbLtMipNode = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MIP Info from MipTable, in Global info */
    pLbLtMipNode = EcfmLbLtUtilGetMipEntry (u2PortNum, u1MdLevel, u4VlanIdIsid);
    if (pLbLtMipNode == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Update Mip row status in LBLT task's MIP */
    pLbLtMipNode->u1RowStatus = u1RowStatus;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfMepCcmLtmPriority
 * 
 * DESCRIPTION      : API Function update MEP's CcmLtmPriority in LBLT task.
 *
 *
 * INPUT            : u4MdIndex - MD Index of the MEP 
 *                    u4MaIndex - MA Index of the MEP 
 *                    u2MepId - MepIdentifier of the MEP.
 *                    u1CcmLtmPriority - CCM/LTM priority to be set
 *                    u4ContextId - Currnet Context Id 
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfMepCcmLtmPriority (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                               UINT1 u1CcmLtmPriority, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Getting MEP Info  from MepTableIndex */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtConfMepCcmLtmPriority: MEP does not exist at"
                      " LBLT task \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Update CcmLtmPriority in LBLT task's MEP */
    pLbLtMepNode->u1CcmLtmPriority = u1CcmLtmPriority;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtDeleteMepEntry
 * 
 * DESCRIPTION      : API Function to delete MepInfo, from port info's
 *                    MepInfoTree and MpStack, and from global
 *                    info's MepInfoTree.
 *
 * INPUT            : u4MdIndex - MD Index of the MEP to be deleted.
 *                    u4MaIndex - MA Index of the MEP to be deleted.
 *                    u2MepId - MepIdentifier of the MEP to be deleted.
 *                    u4ContextId - Current Context Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtDeleteMepEntry (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                        UINT4 u4ContextId)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtMepInfo   *pTempMepNode = NULL;
    tEcfmLbLtStackInfo *pStackNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get particular MEP node */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (NULL == pLbLtMepNode)
    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pLbLtMepNode);
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pLbLtMepNode);
    UNUSED_PARAM (pLtInfo);

    /* Release memory to Lbm Data TLV if allocated */
    if (pLbInfo->TxLbmDataTlv.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_DATA_TLV_POOL,
                             pLbInfo->TxLbmDataTlv.pu1Octets);
        pLbInfo->TxLbmDataTlv.pu1Octets = NULL;
    }

    /* Check if MEP node present in port info's MepInfoTree */
    /* Get its corresponding port info */
    pLbLtPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pLbLtMepNode);
    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pLbLtMepNode->pMaInfo->u1SelectorType)
        == ECFM_VAL_0)
    {
        if (pLbLtPortInfo != NULL)

        {
            /* Remove Stack node corresponding to Mep node from stack Table */
            pStackNode =
                EcfmLbLtUtilGetMp (pLbLtMepNode->u2PortNum,
                                   pLbLtMepNode->u1MdLevel,
                                   pLbLtMepNode->u4PrimaryVidIsid,
                                   pLbLtMepNode->u1Direction);
            if (pStackNode != NULL)
            {
                if (pStackNode->pLbLtMepInfo != NULL)
                {
                    RBTreeRem (pLbLtPortInfo->StackInfoTree,
                               (tRBElem *) pStackNode);
                    /* Release the memory allocated to Stack node */
                    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                                         (UINT1 *) pStackNode);
                    pStackNode = NULL;
                }
            }
        }
    }
    /* Remove LTM Reply List Entries for this MEP */
    EcfmLbLtDeleteLTMReplyEntryForMep (pLbLtMepNode);

    /* Remove MEP from LBLT Global Info's MepTableIndex */
    RBTreeRem (ECFM_LBLT_MEP_TABLE, (tRBElem *) pLbLtMepNode);

    pTempMepNode = RBTreeGet (ECFM_LBLT_PORT_MEP_TABLE, pLbLtMepNode);
    if ((pTempMepNode != NULL) &&
        (pTempMepNode->u4MdIndex == pLbLtMepNode->u4MdIndex) &&
        (pTempMepNode->u4MaIndex == pLbLtMepNode->u4MaIndex) &&
        (pTempMepNode->u2MepId == pLbLtMepNode->u2MepId))
    {
        /* Remove Port MEP node from Context info */
        RBTreeRem (ECFM_LBLT_PORT_MEP_TABLE, (tRBElem *) pLbLtMepNode);
    }
    /* Release memory allocated to MEP */
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_TABLE_POOL, (UINT1 *) pLbLtMepNode);
    pLbLtMepNode = NULL;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfPortLLCEncapStatus
 * 
 * DESCRIPTION      : API Function to update Interface's LLC encapsulation
 *                    status in LBLT task.
 *
 *
 * INPUT            : u4ContextId - Current Context ID 
 *                    u2PortNum - Port Number of interface for which
 *                    LLC encapsulation status needs to be set.
 *                    BOOL1  - LLC Encapsulation Status to be set
 *                     *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfPortLLCEncapStatus (UINT4 u4ContextId, UINT2 u2PortNum,
                                BOOL1 b1LLCEncapSts)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;

    /* Get Port entry corresponding to IfIndex */
    ECFM_LBLT_LOCK ();

    /* Select the context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pLbLtPortInfo->b1LLCEncapStatus = b1LLCEncapSts;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtPortCreated
 * 
 * DESCRIPTION      : API Function to check whether particular port in 
 *                    LBLT task is created or not.
 *
 * INPUT            : u2LocalPort - Local Port No. of the port
 *                    u4ContextId - Current ContextId 
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 **************************************************************************/
PUBLIC              BOOL1
EcfmLbLtPortCreated (UINT2 u2LocalPort, UINT4 u4ContextId)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    /* Get Port entry corresponding to IfIndex */
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FALSE;
    }
    pPortInfo = (tEcfmLbLtPortInfo *) ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    if (pPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FALSE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_TRUE;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfOui
 * 
 * DESCRIPTION      : API Function to set oui in LBLT task.
 *
 * INPUT            : pu1Oui - pointer to OUI to be set
 *                    u4ContextId - Current Context ID
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfOui (UINT1 *pu1Oui)
{
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    ECFM_MEMCPY (ECFM_LBLT_ORG_UNIT_ID, pu1Oui, ECFM_OUI_LENGTH);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfTrapCtrl
 * 
 * DESCRIPTION      : API Function to set trap ctrl in LBLT task.
 *
 * INPUT            : u2TrapControl - trap control
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfTrapCtrl (UINT2 u2TrapControl)
{
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    Y1731_LBLT_TRAP_CONTROL = u2TrapControl;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfDropEnable
 * 
 * DESCRIPTION      : API Function to set drop enable in LBLT task.
 *
 * INPUT            : u4MdIndex - MD index
 *                    b1DropEnable - Vlan Drop Eligibility
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfDropEnable (UINT4 u4MdIndex, BOOL1 b1DropEnable)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);

    gpEcfmLbLtMepNode->u4MdIndex = u4MdIndex;
    pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
        (ECFM_LBLT_MEP_TABLE, (tRBElem *) gpEcfmLbLtMepNode, NULL);

    while ((pMepInfo != NULL) && (pMepInfo->u4MdIndex == u4MdIndex))
    {
        pMepInfo->LbInfo.b1TxLbmDropEligible = b1DropEnable;
        pMepInfo->LtInfo.b1TxLtmDropEligible = b1DropEnable;
        pMepInfo->DmInfo.b1TxDmmDropEligible = b1DropEnable;
        pMepInfo->DmInfo.b1Tx1DmDropEligible = b1DropEnable;
        pMepInfo->TstInfo.b1TstDropEligible = b1DropEnable;

        pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext (ECFM_LBLT_MEP_TABLE,
                                                       (tRBElem *) pMepInfo,
                                                       NULL);
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfVlanPriority
 * 
 * DESCRIPTION      : API Function to set vlan priority in LBLT task.
 *
 * INPUT            : u4MdIndex - MD index
 *                    u1VlanPriority - Vlan priority
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfVlanPriority (UINT4 u4MdIndex, UINT1 u1VlanPriority)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);

    gpEcfmLbLtMepNode->u4MdIndex = u4MdIndex;
    pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext (ECFM_LBLT_MEP_TABLE,
                                                   (tRBElem *)
                                                   gpEcfmLbLtMepNode, NULL);

    while ((pMepInfo != NULL) && (pMepInfo->u4MdIndex == u4MdIndex))

    {
        pMepInfo->LbInfo.u1TxLbmVlanPriority = u1VlanPriority;
        pMepInfo->LtInfo.u1TxLtmVlanPriority = u1VlanPriority;
        pMepInfo->DmInfo.u1TxDmmVlanPriority = u1VlanPriority;
        pMepInfo->DmInfo.u1Tx1DmVlanPriority = u1VlanPriority;
        pMepInfo->TstInfo.u1TstVlanPriority = u1VlanPriority;

        pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext (ECFM_LBLT_MEP_TABLE,
                                                       (tRBElem *) pMepInfo,
                                                       NULL);
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtConfUnawreMepPresent
 * 
 * DESCRIPTION      : API Function to set unaware MEP present status a LBLT's
 *                    task information.
 *
 * INPUT            : b1UnawareMepPresent - Unaware MEP present status
 *                    u2PortNum - logical port number
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtConfUnawreMepPresent (BOOL1 b1UnawareMepPresent, UINT2 u2PortNum)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
    }
    else
    {
        pPortInfo->b1UnawareMepPresent = b1UnawareMepPresent;
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
    }
}

/*****************************************************************************
 * Function Name      : EcfmLbLtConfLCKStatus
 *                                                                          
 * Description        : API function to update the
 *                      the LCK status for Data corresponding to Vlan     
 *                                                                       
 * INPUT              : u4MdIndex - MD Index of the MEP
 *                      u4MaIndex - MA Index of the MEP
 *                      u2MepId - MepIdentifier of the MEP.
 *                      u4ContextId - Current Context Id
 *                      b1LckStatus - Lck status to be set
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtConfLCKStatus (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                       UINT4 u4ContextId, BOOL1 b1LckStatus)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP Info from MepTableIndex, in Global info */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtConfLCKStatus: MEP does not exists at LBLT task "
                      "\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pLbLtMepNode->b1LckDelayExp = b1LckStatus;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtEnableMipCcmDb
 *                                                                          
 * Description        : This routine is used to enable MIP CCM DB at the LBLT
 *                      task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      u2PoolSize  - MIP CCM DB pool size
 * OUTPUT             : None
 * RETURNS            : ECFM_SUCCESS/ECFM_FAILURE
 ***************************************************************************/
PUBLIC INT4
EcfmLbLtEnableMipCcmDb (UINT4 u4ContextId, UINT2 u2PoolSize)
{
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Creating Mempool for MipCcmDb Table */
    if (ECFM_CREATE_MEM_POOL
        (ECFM_LBLT_MIP_CCM_DB_INFO_SIZE, u2PoolSize,
         MEM_DEFAULT_MEMORY_TYPE,
         &ECFM_LBLT_MIP_CCM_DB_POOL) == ECFM_MEM_FAILURE)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtEnableMipCcmDb:"
                       "ECFM_CREATE_MEM_POOL returned failure");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtDisableMipCcmDb
 *                                                                          
 * Description        : This routine is used to disable MIP CCM DB at the LBLT
 *                      task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtDisableMipCcmDb (UINT4 u4ContextId)
{
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    RBTreeDrain (ECFM_LBLT_MIP_CCM_DB_TABLE,
                 (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                 ECFM_LBLT_MIP_CCM_DB_ENTRY);

    /* Delete the mempool for MipCcmDb */
    ECFM_DELETE_MEM_POOL (ECFM_LBLT_MIP_CCM_DB_POOL);
    ECFM_LBLT_MIP_CCM_DB_POOL = 0;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtDrainMipCcmDb
 *                                                                          
 * Description        : This routine is used to drain MIP CCM DB at the LBLT
 *                      task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtDrainMipCcmDb (UINT4 u4ContextId)
{
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    RBTreeDrain (ECFM_LBLT_MIP_CCM_DB_TABLE,
                 (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                 ECFM_LBLT_MIP_CCM_DB_ENTRY);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtAddMipCcmDbEntry
 *                                                                          
 * Description        : This routine is used to add a entry in MIP CCM DB at 
 *                      the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcMipCcmEntry - MIP DB entry
 * OUTPUT             : None
 * RETURNS            : ECFM_SUCCESS/ECFM_FAILURE
 ***************************************************************************/
PUBLIC INT4
EcfmLbLtAddMipCcmDbEntry (UINT4 u4ContextId,
                          tEcfmCcMipCcmDbInfo * pCcMipCcmEntry)
{
    tEcfmLbLtMipCcmDbInfo *pMipCcmDbEntry = NULL;
    INT4                i4RetVal = ECFM_FAILURE;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    do

    {

        /* Get MipCcmDb Entry */
        pMipCcmDbEntry =
            EcfmLbLtUtilGetMipCcmDbEntry (pCcMipCcmEntry->u2Fid,
                                          pCcMipCcmEntry->SrcMacAddr);
        if (pMipCcmDbEntry == NULL)

        {

            /* Add New entry */
            /* Allocate Memory to the Node to be added in the MIP CCM DB */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_MIP_DB_TABLE (pMipCcmDbEntry) == NULL)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddMipCcmDbEntry:"
                               "ECFM_ALLOC_MEM_BLOCK_LBLT_MIP_DB_TABLE returned failure");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                break;
            }
            ECFM_MEMSET (pMipCcmDbEntry, ECFM_INIT_VAL,
                         ECFM_LBLT_MIP_CCM_DB_INFO_SIZE);
            pMipCcmDbEntry->u2Fid = pCcMipCcmEntry->u2Fid;
            ECFM_MEMCPY (pMipCcmDbEntry->SrcMacAddr,
                         pCcMipCcmEntry->SrcMacAddr, ECFM_MAC_ADDR_LENGTH);
            pMipCcmDbEntry->u2PortNum = pCcMipCcmEntry->u2PortNum;

            /* Add CCM Node to the Global MIP CCM  Tree maintianed Globally */
            if (RBTreeAdd (ECFM_LBLT_MIP_CCM_DB_TABLE, pMipCcmDbEntry) !=
                ECFM_RB_SUCCESS)

            {

                /* Free the memory allocated to the LTR Node from the LTR POOL */
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_CCM_DB_POOL,
                                     (UINT1 *) pMipCcmDbEntry);
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddMipCcmDbEntry:"
                               "RBTreeAdd returned failure");
                break;
            }
        }

        else

        {

            /* Udated the entry */
            pMipCcmDbEntry->u2PortNum = pCcMipCcmEntry->u2PortNum;
        }
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtRemoveMipCcmDbEntry
 *                                                                          
 * Description        : This routine is used to remove a entry from MIP 
 *                      CCM DB at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcMipCcmEntry - MIP DB entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtRemoveMipCcmDbEntry (UINT4 u4ContextId,
                             tEcfmCcMipCcmDbInfo * pCcMipCcmEntry)
{
    tEcfmLbLtMipCcmDbInfo *pMipCcmDbEntry = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pMipCcmDbEntry =
        EcfmLbLtUtilGetMipCcmDbEntry (pCcMipCcmEntry->u2Fid,
                                      pCcMipCcmEntry->SrcMacAddr);
    if (pMipCcmDbEntry != NULL)

    {

        /* Delete the MIP CCM DB Entry */
        RBTreeRem (ECFM_LBLT_MIP_CCM_DB_TABLE, pMipCcmDbEntry);

        /* Free the memory assigned to this node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_CCM_DB_POOL,
                             (UINT1 *) (pMipCcmDbEntry));
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtAddRMepDbEntry
 *                                                                          
 * Description        : This routine is used to add a entry in RMEP DB 
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcRMepEntry - Remote MEP DB entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC INT4
EcfmLbLtAddRMepDbEntry (UINT4 u4ContextId, tEcfmCcRMepDbInfo * pCcRMepEntry)
{
    tEcfmLbLtRMepDbInfo *pRMepDbEntry = NULL;
    INT4                i4RetVal = ECFM_FAILURE;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    do

    {
        pRMepDbEntry =
            EcfmLbLtUtilGetRMepDbEntry (pCcRMepEntry->u4MdIndex,
                                        pCcRMepEntry->u4MaIndex,
                                        pCcRMepEntry->u2MepId,
                                        pCcRMepEntry->u2RMepId);
        if (pRMepDbEntry == NULL)

        {

            /* Add new entry */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_RMEP_DB_TABLE (pRMepDbEntry) == NULL)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddRMepDbEntry:"
                               "ECFM_ALLOC_MEM_BLOCK_LBLT_RMEP_DB_TABLE returned failure");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                break;
            }
            ECFM_MEMSET (pRMepDbEntry, ECFM_INIT_VAL,
                         ECFM_LBLT_RMEP_DB_INFO_SIZE);

            /* Set the required indices for new remote MEP node */
            pRMepDbEntry->u4MdIndex = pCcRMepEntry->u4MdIndex;
            pRMepDbEntry->u4MaIndex = pCcRMepEntry->u4MaIndex;
            pRMepDbEntry->u2MepId = pCcRMepEntry->u2MepId;
            pRMepDbEntry->u2RMepId = pCcRMepEntry->u2RMepId;

            /* Adding remote MEP new node to global */
            if (RBTreeAdd (ECFM_LBLT_RMEP_DB_TABLE, pRMepDbEntry) !=
                ECFM_RB_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddRMepDbEntry:"
                               "RBTreeAdd returned failure");

                /* Release memory allocated to new remote MEP node */
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_RMEP_DB_POOL,
                                     (UINT1 *) (pRMepDbEntry));
                break;
            }
        }

        else

        {

            /* Update entry */
            /* Set the RMEP macAddress to received source mac address */
            ECFM_MEMCPY (pRMepDbEntry->RMepMacAddr,
                         pCcRMepEntry->RMepMacAddr, ECFM_MAC_ADDR_LENGTH);
        }
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtRemoveRMepDbEntry
 *                                                                          
 * Description        : This routine is used to remove a entry from RMEP DB 
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcRMepEntry - Remote MEP DB entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtRemoveRMepDbEntry (UINT4 u4ContextId, tEcfmCcRMepDbInfo * pCcRMepEntry)
{
    tEcfmLbLtRMepDbInfo *pRMepDbEntry = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pRMepDbEntry =
        EcfmLbLtUtilGetRMepDbEntry (pCcRMepEntry->u4MdIndex,
                                    pCcRMepEntry->u4MaIndex,
                                    pCcRMepEntry->u2MepId,
                                    pCcRMepEntry->u2RMepId);
    if (pRMepDbEntry != NULL)

    {
        RBTreeRem (ECFM_LBLT_RMEP_DB_TABLE, (tRBElem *) pRMepDbEntry);

        /* Free memory allocated to Remote MEP node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_RMEP_DB_POOL, (UINT1 *) (pRMepDbEntry));
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtUpdateDefaultMdEntry
 *                                                                          
 * Description        : This routine is used to update default MD table 
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcDefaultMdEntry - Default MD entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtUpdateDefaultMdEntry (UINT4 u4ContextId,
                              tEcfmCcDefaultMdTableInfo * pCcDefaultMdEntry)
{
    tEcfmLbLtDefaultMdTableInfo *pDefaultMdEntry = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pDefaultMdEntry =
        EcfmLbLtSnmpLwGetDefaultMdEntry (pCcDefaultMdEntry->u4PrimaryVidIsid);
    if (pDefaultMdEntry != NULL)
    {
        pDefaultMdEntry->u1IdPermission = pCcDefaultMdEntry->u1IdPermission;
        pDefaultMdEntry->b1Status = pCcDefaultMdEntry->b1Status;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtUpdateDefDefaultIdPerm
 *                                                                          
 * Description        : This routine is used to update default def sender id
 *                      permission  at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      u1IdPermission - Sender id permission
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtUpdateDefDefaultIdPerm (UINT4 u4ContextId, UINT1 u1IdPermission)
{
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    ECFM_LBLT_DEF_MD_DEFAULT_SENDER_ID_PERMISSION = u1IdPermission;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtAddMaEntry
 *                                                                          
 * Description        : This routine is used to add a entry in MA table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcMaInfo - MA entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC INT4
EcfmLbLtAddMaEntry (UINT4 u4ContextId, tEcfmCcMaInfo * pCcMaInfo)
{
    tEcfmLbLtMaInfo    *pMaInfo = NULL;
    tEcfmLbLtMdInfo    *pMdInfo = NULL;
    INT4                i4RetVal = ECFM_FAILURE;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    do

    {
        pMaInfo =
            EcfmLbLtUtilGetMaEntry (pCcMaInfo->u4MdIndex, pCcMaInfo->u4MaIndex);
        if (pMaInfo == NULL)

        {

            /* Add new entry */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_MA_TABLE (pMaInfo) == NULL)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddMaEntry:"
                               "ECFM_ALLOC_MEM_BLOCK_LBLT_MA_TABLE returned failure");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                break;
            }
            ECFM_MEMSET (pMaInfo, ECFM_INIT_VAL, ECFM_LBLT_MA_INFO_SIZE);
            pMaInfo->u4MdIndex = pCcMaInfo->u4MdIndex;
            pMaInfo->u4MaIndex = pCcMaInfo->u4MaIndex;
            pMaInfo->u1IdPermission = pCcMaInfo->u1IdPermission;
            pMaInfo->u4PrimaryVidIsid = pCcMaInfo->u4PrimaryVidIsid;
            /* Selector Type is assigned to identify the MA service type:
             * VLAN/ISID/LSP/PW MA in LBLT context.
             */
            pMaInfo->u1SelectorType = pCcMaInfo->u1SelectorType;

            /* Put Back pointer to MdInfo in MA node */
            pMdInfo = EcfmLbLtUtilGetMdEntry (pMaInfo->u4MdIndex);
            pMaInfo->pMdInfo = pMdInfo;

            /* Adding MA node to global */
            if (RBTreeAdd (ECFM_LBLT_MA_TABLE, pMaInfo) != ECFM_RB_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddMaEntry:"
                               "RBTreeAdd returned failure");
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MA_POOL, (UINT1 *) (pMaInfo));
                break;
            }
        }

        else

        {

            /* Update entry */
            pMaInfo->u1IdPermission = pCcMaInfo->u1IdPermission;
            pMaInfo->u4PrimaryVidIsid = pCcMaInfo->u4PrimaryVidIsid;
        }
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtRemoveMaEntry
 *                                                                          
 * Description        : This routine is used to remove a entry in MA table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcMaInfo - MA entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtRemoveMaEntry (UINT4 u4ContextId, tEcfmCcMaInfo * pCcMaInfo)
{
    tEcfmLbLtMaInfo    *pMaInfo = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pMaInfo =
        EcfmLbLtUtilGetMaEntry (pCcMaInfo->u4MdIndex, pCcMaInfo->u4MaIndex);
    if (pMaInfo != NULL)

    {
        RBTreeRem (ECFM_LBLT_MA_TABLE, (tRBElem *) pMaInfo);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MA_POOL, (UINT1 *) (pMaInfo));
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtAddMdEntry
 *                                                                          
 * Description        : This routine is used to add a entry in MD table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcMdInfo - MD entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC INT4
EcfmLbLtAddMdEntry (UINT4 u4ContextId, tEcfmCcMdInfo * pCcMdInfo)
{
    tEcfmLbLtMdInfo    *pMdInfo = NULL;
    INT4                i4RetVal = ECFM_FAILURE;

    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    do
    {
        pMdInfo = EcfmLbLtUtilGetMdEntry (pCcMdInfo->u4MdIndex);
        if (pMdInfo == NULL)
        {
            /* Add new entry */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_MD_TABLE (pMdInfo) == NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddMdEntry:"
                               "ECFM_ALLOC_MEM_BLOCK_LBLT_MD_TABLE returned failure");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                break;
            }

            ECFM_MEMSET (pMdInfo, ECFM_INIT_VAL, ECFM_LBLT_MD_INFO_SIZE);

            pMdInfo->u4MdIndex = pCcMdInfo->u4MdIndex;
            pMdInfo->u1IdPermission = pCcMdInfo->u1IdPermission;
            pMdInfo->u1Level = pCcMdInfo->u1Level;

            /* Adding MD node to global */
            if (RBTreeAdd (ECFM_LBLT_MD_TABLE, pMdInfo) != ECFM_RB_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddMdEntry:"
                               "RBTreeAdd returned failure");
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MD_POOL, (UINT1 *) (pMdInfo));
                break;
            }
        }
        else
        {
            /* Update entry */
            pMdInfo->u1IdPermission = pCcMdInfo->u1IdPermission;
        }
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtRemoveMdEntry
 *                                                                          
 * Description        : This routine is used to remove a entry from MD table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcMdInfo - MD entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtRemoveMdEntry (UINT4 u4ContextId, tEcfmCcMdInfo * pCcMdInfo)
{
    tEcfmLbLtMdInfo    *pMdInfo = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pMdInfo = EcfmLbLtUtilGetMdEntry (pCcMdInfo->u4MdIndex);
    if (pMdInfo != NULL)

    {
        RBTreeRem (ECFM_LBLT_MD_TABLE, (tRBElem *) pMdInfo);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MD_POOL, (UINT1 *) (pMdInfo));
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtAddVlanEntry
 *                                                                          
 * Description        : This routine is used to add a entry in Vlan table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcVlanInfo - Pointer to Vlan info 
 *                                                                       
 * OUTPUT             : None
 *                                                                       
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtAddVlanEntry (UINT4 u4ContextId, tEcfmCcVlanInfo * pCcVlanInfo)
{
    tEcfmLbLtVlanInfo   VlanInfo;
    tEcfmLbLtVlanInfo  *pLbLtVlanInfo = NULL;

    MEMSET (&VlanInfo, 0, sizeof (tEcfmLbLtVlanInfo));

    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    VlanInfo.u4VidIsid = pCcVlanInfo->u4VidIsid;
    VlanInfo.u4PrimaryVidIsid = pCcVlanInfo->u4PrimaryVidIsid;

    pLbLtVlanInfo =
        RBTreeGet ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable,
                   (tRBElem *) & VlanInfo);

    if (pLbLtVlanInfo == NULL)
    {
        /* Add new entry */
        pLbLtVlanInfo = (tEcfmLbLtVlanInfo *)
            MemAllocMemBlk (ECFM_LBLT_VLAN_MEM_POOL);
        if (pLbLtVlanInfo == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtAddMdEntry: Unable to allocate "
                           "Memory for Vlan table in LBLT\r\n");

            ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();

            return;
        }

        ECFM_MEMSET (pLbLtVlanInfo, ECFM_INIT_VAL, sizeof (tEcfmLbLtVlanInfo));

        pLbLtVlanInfo->u4VidIsid = pCcVlanInfo->u4VidIsid;
        pLbLtVlanInfo->u4PrimaryVidIsid = pCcVlanInfo->u4PrimaryVidIsid;
        pLbLtVlanInfo->u1RowStatus = pCcVlanInfo->u1RowStatus;

        if (RBTreeAdd ((ECFM_LBLT_CURR_CONTEXT_INFO ()->VlanTable),
                       pLbLtVlanInfo) == ECFM_RB_FAILURE)
        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return;
        }

        if (pCcVlanInfo->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
        {
            /* The entry needs to be added in the primary vlan table,
             * only when the row status is active
             * */
            if (RBTreeAdd ((ECFM_LBLT_CURR_CONTEXT_INFO ()->PrimaryVlanTable),
                           pLbLtVlanInfo) == ECFM_RB_FAILURE)
            {
                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();
                return;
            }
        }
    }
    else
    {
        pLbLtVlanInfo->u4VidIsid = pCcVlanInfo->u4VidIsid;
        pLbLtVlanInfo->u4PrimaryVidIsid = pCcVlanInfo->u4PrimaryVidIsid;
        pLbLtVlanInfo->u1RowStatus = pCcVlanInfo->u1RowStatus;

        if (pCcVlanInfo->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
        {
            /* The entry needs to be added in the primary vlan table,
             * only when the row status is active
             * */
            if (RBTreeAdd ((ECFM_LBLT_CURR_CONTEXT_INFO ()->PrimaryVlanTable),
                           pLbLtVlanInfo) == ECFM_RB_FAILURE)
            {
                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();
                return;
            }
        }
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
}

/*****************************************************************************
 * Function Name      : EcfmLbLtRemoveVlanEntry
 *                                                                          
 * Description        : This routine is used to remove a entry from Vlan table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcVlanInfo - Pointer to Vlan info 
 *                                                                       
 * OUTPUT             : None
 *                                                                       
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtRemoveVlanEntry (UINT4 u4ContextId, tEcfmCcVlanInfo * pCcVlanInfo)
{
    tEcfmLbLtVlanInfo   VlanInfo;
    tEcfmLbLtVlanInfo  *pLbLtVlanInfo = NULL;

    MEMSET (&VlanInfo, 0, sizeof (tEcfmLbLtVlanInfo));

    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    VlanInfo.u4VidIsid = pCcVlanInfo->u4VidIsid;
    VlanInfo.u4PrimaryVidIsid = pCcVlanInfo->u4PrimaryVidIsid;

    pLbLtVlanInfo =
        RBTreeGet ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable,
                   (tRBElem *) & VlanInfo);
    if (pLbLtVlanInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    RBTreeRem ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable,
               (tRBElem *) pLbLtVlanInfo);

    if (RBTreeGet ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable,
                   (tRBElem *) pLbLtVlanInfo) != NULL)
    {
        /* Entry present in the RBTree for primary vlan table */
        RBTreeRem ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable,
                   (tRBElem *) pLbLtVlanInfo);
    }
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_VLAN_MEM_POOL, (UINT1 *) pLbLtVlanInfo);

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtSetTransmitFailOpcode
 *                                                                          
 * Description        : This routine is used to update the last transmit 
 *                      failure opcode at LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      u2PortNum - Port number
 *                      u1OpCode - OpCode
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtSetTransmitFailOpcode (UINT4 u4ContextId, UINT2 u2PortNum,
                               UINT1 u1OpCode)
{
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    Y1731LbLtSnmpIfSendTxFailTrap (u2PortNum, u1OpCode);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtAddDefMdEntry
 *                                                                          
 * Description        : This routine is used to add a entry in DefMD table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcDefaultMdNode - MD entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC INT4
EcfmLbLtAddDefMdEntry (UINT4 u4ContextId,
                       tEcfmCcDefaultMdTableInfo * pCcDefaultMdNode)
{
    tEcfmLbLtDefaultMdTableInfo *pLbLtDefaultMdNode = NULL;
    INT4                i4RetVal = ECFM_FAILURE;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    do
    {
        pLbLtDefaultMdNode =
            EcfmLbLtSnmpLwGetDefaultMdEntry (pCcDefaultMdNode->
                                             u4PrimaryVidIsid);
        if (pLbLtDefaultMdNode == NULL)
        {
            /* Add new entry */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_DEF_MD_TABLE (pLbLtDefaultMdNode) ==
                NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddDefMdEntry:"
                               "ECFM_ALLOC_MEM_BLOCK_LBLT_DEF_MD_TABLE returned failure");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                break;
            }
            ECFM_MEMSET (pLbLtDefaultMdNode, ECFM_INIT_VAL,
                         ECFM_LBLT_DEF_MD_INFO_SIZE);
            pLbLtDefaultMdNode->b1Status = pCcDefaultMdNode->b1Status;
            pLbLtDefaultMdNode->u1IdPermission =
                pCcDefaultMdNode->u1IdPermission;
            pLbLtDefaultMdNode->u4PrimaryVidIsid =
                pCcDefaultMdNode->u4PrimaryVidIsid;
            /* Adding Def MD node to global */
            if (RBTreeAdd (ECFM_LBLT_DEF_MD_TABLE, pLbLtDefaultMdNode) !=
                ECFM_RB_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtAddDefMdEntry:"
                               "RBTreeAdd returned failure");
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_DEF_MD_TABLE_POOL,
                                     (UINT1 *) (pLbLtDefaultMdNode));
                break;
            }
        }
        else
        {
            /* Update entry */
            pLbLtDefaultMdNode->b1Status = pCcDefaultMdNode->b1Status;
            pLbLtDefaultMdNode->u1IdPermission =
                pCcDefaultMdNode->u1IdPermission;
        }
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : EcfmLbLtRemoveDefMdEntry
 *                                                                          
 * Description        : This routine is used to remove a entry from Def MD table
 *                      at the LBLT task.
 *                                                                       
 * INPUT              : u4ContextId - Current Context Id
 *                      pCcDefaultMdNode  - MD entry
 * OUTPUT             : None
 * RETURNS            : None
 ***************************************************************************/
PUBLIC VOID
EcfmLbLtRemoveDefMdEntry (UINT4 u4ContextId,
                          tEcfmCcDefaultMdTableInfo * pCcDefaultMdNode)
{
    tEcfmLbLtDefaultMdTableInfo *pLbLtDefaultMdNode = NULL;
    ECFM_LBLT_LOCK ();

    /* Select the current context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }
    pLbLtDefaultMdNode =
        EcfmLbLtSnmpLwGetDefaultMdEntry (pCcDefaultMdNode->u4PrimaryVidIsid);
    if (pLbLtDefaultMdNode != NULL)

    {
        RBTreeRem (ECFM_LBLT_DEF_MD_TABLE, (tRBElem *) pLbLtDefaultMdNode);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_DEF_MD_TABLE_POOL,
                             (UINT1 *) (pLbLtDefaultMdNode));
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmLbLtHandleAddToChannel
 *
 * Description        : This rotuine handles the event of port addition
 *                      to a port channel.
 *
 * Input(s)           : u2PortNum - Port Number
 *                      u4CurrContextId - ContextId 
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/

PUBLIC INT4
EcfmLbLtHandleAddToChannel (UINT2 u2PortNum, UINT4 u4CurrContextId)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2PortChannel = ECFM_INIT_VAL;
    UINT2               u2ChannelPortNum = ECFM_INIT_VAL;

    ECFM_LBLT_LOCK ();
    /* Select Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_ENTRY ();

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (EcfmL2IwfGetPortChannelForPort (pPortInfo->u4IfIndex, &u2PortChannel)
        != L2IWF_SUCCESS)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u2PortChannel,
                                            &u4ContextId, &u2ChannelPortNum)
        != VCM_SUCCESS)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    pPortInfo->u2ChannelPortNum = u2ChannelPortNum;

    ECFM_LBLT_TRC_FN_EXIT ();
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmLbLtHandleRemoveFromChannel
 *
 * Description        : This rotuine handles the event of port removal
 *                      from a port channel.
 *
 * Input(s)           : u2PortNum - Port Number
 *                      u4CurrContextId - ContextId 
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/

PUBLIC INT4
EcfmLbLtHandleRemoveFromChannel (UINT2 u2PortNum, UINT4 u4CurrContextId)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    ECFM_LBLT_LOCK ();
    /* Select Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_ENTRY ();

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }

    pPortInfo->u2ChannelPortNum = 0;

    ECFM_LBLT_TRC_FN_EXIT ();
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
  End of File cfmlbapi.c
 ******************************************************************************/
