/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbtmr.c,v 1.18 2013/12/07 11:08:16 siva Exp $
 *
 * Description: This file contains the timer functionality  for ECFM
 *              Module for LBLT Task.
 *******************************************************************/

#include "cfminc.h"

PRIVATE VOID EcfmLbLtTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID EcfmLbLtTmrLtmTxExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrLbiIntervalExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrLbiDeadlineExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrLbrTimeoutExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrLtrHoldExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrLbrHoldExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrDmDeadlineExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrDmIntervalExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmr1DmTransIntExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrTstIntervalExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrTstDeadlineExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrThDeadlineExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrVsrTimeoutExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrThGetTstRxExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmLbLtTmrDelayQExpired PROTO ((VOID *pArgs));
/**************************************************************************
 * Function Name      : EcfmLbLtTmrInit                                    *
 *                                                                         *
 * Description        : This function creates the timer  List for          *
 *                      LbLt Task.                                         *
 *                                                                         *
 * Input(s)           : None                                               *
 *                                                                         *
 * Output(s)          : None                                               *
 *                                                                         *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                        *
 **************************************************************************/
PUBLIC INT4
EcfmLbLtTmrInit ()
{

    /* LBLT Timer Initialization */
    if (ECFM_CREATE_TMR_LIST
        ((UINT1 *) ECFM_LBLT_TASK_NAME, ECFM_EV_TMR_EXP, NULL,
         &(ECFM_LBLT_TMRLIST_ID)) == ECFM_TMR_FAILURE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "EcfmLbLtTmrInit:Timer list creation FAILED"
                      "for LBLT Task \r\n");
        return ECFM_FAILURE;
    }
    EcfmLbLtTmrInitTmrDesc ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmLbLtTmrDeInit                                    *
 *                                                                           *
 * Description        : This function is called inorder de-intialise the     * 
 *                      timer for LBLT Task.                                 *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtTmrDeInit ()
{

    /* Deleting the Timer List */
    if (ECFM_LBLT_TMRLIST_ID != ECFM_INIT_VAL)

    {
        if (ECFM_DELETE_TMR_LIST (ECFM_LBLT_TMRLIST_ID) == ECFM_TMR_FAILURE)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                          ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                          ECFM_OS_RESOURCE_TRC,
                          "EcfmLbLtTmrDeInit:"
                          "Timer list deletion FAILED for LBLT Task \r\n");
            return;
        }
        ECFM_LBLT_TMRLIST_ID = ECFM_INIT_VAL;
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtTmrDeInit: Timer DeInitialization"
                      "for LBLT Task Successful \r\n");
    }

    return;
}

/**************************************************************************
 * Function Name      : EcfmLbLtTmrInitTmrDesc                            *
 *                                                                        *
 * Description        : This function creates a timer list for all the    *
 *                      timers of ECFM LBLT Task.                         *
 *                                                                        *
 * Input(s)           : None                                              *
 *                                                                        *
 * Output(s)          : None                                              *
 *                                                                        *
 * Return Value(s)    : None                                              *
 **************************************************************************/
PRIVATE VOID
EcfmLbLtTmrInitTmrDesc (VOID)
{

    INT2                i2DelayQInfoOffset = ECFM_INIT_VAL;
    INT2                i2LTInfoOffset = ECFM_INIT_VAL;
    INT2                i2LbInfoOffset = ECFM_INIT_VAL;
    INT2                i2DmInfoOffset = ECFM_INIT_VAL;
    INT2                i2TstInfoOffset = ECFM_INIT_VAL;
    INT2                i2ThInfoOffset = ECFM_INIT_VAL;

    i2DelayQInfoOffset =
        (INT2) (FSAP_OFFSETOF (tEcfmLbLtContextInfo, DelayQueueInfo));
    i2LTInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmLbLtMepInfo, LtInfo));
    i2LbInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmLbLtMepInfo, LbInfo));
    i2DmInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmLbLtMepInfo, DmInfo));
    i2TstInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmLbLtMepInfo, TstInfo));
    i2ThInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmLbLtMepInfo, ThInfo));

    /*Defining the value in the Timer Description array, about the offset and 
     * function to be called when the corresponding timers expires*/
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_DELAY_QUEUE].i2Offset =
        (i2DelayQInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtDelayQueueInfo,
                                                     DelayQueueTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_DELAY_QUEUE].TmrExpFn =
        EcfmLbLtTmrDelayQExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LTM_TX].i2Offset =
        (i2LTInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtLTInfo, LtmTxTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LTM_TX].TmrExpFn =
        EcfmLbLtTmrLtmTxExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBI_INTERVAL].i2Offset =
        (i2LbInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtLBInfo,
                                                 LbiIntervalTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBI_INTERVAL].TmrExpFn =
        EcfmLbLtTmrLbiIntervalExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBI_DEADLINE].i2Offset =
        (i2LbInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtLBInfo,
                                                 LbiDeadlineTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBI_DEADLINE].TmrExpFn =
        EcfmLbLtTmrLbiDeadlineExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBR_TIMEOUT].i2Offset =
        (i2LbInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtLBInfo, LbrTimeout)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBR_TIMEOUT].TmrExpFn =
        EcfmLbLtTmrLbrTimeoutExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LTR_HOLD].i2Offset =
        (INT2) (FSAP_OFFSETOF (tEcfmLbLtContextInfo, LtrHoldTimer));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LTR_HOLD].TmrExpFn =
        EcfmLbLtTmrLtrHoldExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBR_HOLD].i2Offset =
        (INT2) (FSAP_OFFSETOF (tEcfmLbLtContextInfo, LbrHoldTimer));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_LBR_HOLD].TmrExpFn =
        EcfmLbLtTmrLbrHoldExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_DM_DEADLINE].i2Offset =
        (i2DmInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtDmInfo,
                                                 DmInitDeadlineTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_DM_DEADLINE].TmrExpFn =
        EcfmLbLtTmrDmDeadlineExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_DM_INTERVAL].i2Offset =
        (i2DmInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtDmInfo,
                                                 DmInitIntervalTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_DM_INTERVAL].TmrExpFn =
        EcfmLbLtTmrDmIntervalExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_1DM_TRANS_INTERVAL].i2Offset =
        (i2DmInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtDmInfo,
                                                 OneDmTransIntervalTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_1DM_TRANS_INTERVAL].TmrExpFn =
        EcfmLbLtTmr1DmTransIntExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TST_INTERVAL].i2Offset =
        (i2TstInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtTstInfo, TstWhile)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TST_INTERVAL].TmrExpFn =
        EcfmLbLtTmrTstIntervalExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TST_DEADLINE].i2Offset =
        (i2TstInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtTstInfo,
                                                  TstDeadLine)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TST_DEADLINE].TmrExpFn =
        EcfmLbLtTmrTstDeadlineExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TH_DEADLINE].i2Offset =
        (i2ThInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtThInfo,
                                                 ThInitDeadlineTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TH_DEADLINE].TmrExpFn =
        EcfmLbLtTmrThDeadlineExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_VSR_TIMEOUT].i2Offset =
        (i2ThInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtThInfo,
                                                 ThVsrTimeoutTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_VSR_TIMEOUT].TmrExpFn =
        EcfmLbLtTmrVsrTimeoutExpired;

    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT].i2Offset =
        (i2ThInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmLbLtThInfo,
                                                 GetTstRxCounterTimer)));
    gEcfmLbLtGlobalInfo.aTmrDesc[ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT].TmrExpFn =
        EcfmLbLtTmrThGetTstRxExpired;

    return;
}

/****************************************************************************
 * Function Name      : EcfmLbLtTmrStartTimer                                *
 *                                                                           *
 * Description        : This function is called to start timer for LBLT Task.*
 *                                                                           *
 * Input(s)           : u1TimerType - Type of timer ot be started.           *
 *                      pMepInfo    - MepInfo for which the timer needs to   *
 *                                    be started.                            *
 *                      u4Duration  - Duration for which the timer needs to  *
 *                                    be started in units of msec.           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtTmrStartTimer (UINT1 u1TimerType, tEcfmLbLtMepInfo * pMepInfo,
                       UINT4 u4Duration)
{
    tTmrBlk            *pLbLtTmrBlk = NULL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT4               u4IntervalSec = ECFM_INIT_VAL;
    UINT4               u4IntervalMSec = ECFM_INIT_VAL;
    INT2                i2Offset = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {

        /*Start the timers only in Active Node */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLbLtTmrStartTimer:"
                       "TIMER NOT STARTED: Start the timers only in "
                       "Active Node \r\n");
        return ECFM_SUCCESS;
    }

    switch (u1TimerType)
    {
        case ECFM_LBLT_TMR_VSR_TIMEOUT:
        case ECFM_LBLT_TMR_TH_DEADLINE:
        case ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT:
        case ECFM_LBLT_TMR_LTM_TX:
        case ECFM_LBLT_TMR_LBI_INTERVAL:
        case ECFM_LBLT_TMR_LBI_DEADLINE:
        case ECFM_LBLT_TMR_LBR_TIMEOUT:
        case ECFM_LBLT_TMR_DM_DEADLINE:
        case ECFM_LBLT_TMR_DM_INTERVAL:
        case ECFM_LBLT_TMR_1DM_TRANS_INTERVAL:
        case ECFM_LBLT_TMR_TST_INTERVAL:
        case ECFM_LBLT_TMR_TST_DEADLINE:
            i2Offset = gEcfmLbLtGlobalInfo.aTmrDesc[u1TimerType].i2Offset;
            pLbLtTmrBlk =
                (tTmrBlk *) (VOID *) ((UINT1 *) (pMepInfo) + i2Offset);
            break;

        case ECFM_LBLT_TMR_DELAY_QUEUE:

            pLbLtTmrBlk = &(ECFM_LBLT_DELAY_QUEUE ()->DelayQueueTimer);
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtTmrStartTimer:"
                           "Starting Delay Queue Timer \r\n");
            break;

        case ECFM_LBLT_TMR_LTR_HOLD:

            pLbLtTmrBlk = &(ECFM_LBLT_LTR_HOLD_TIMER);
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtTmrStartTimer:"
                           "Starting Ltr Hold Timer \r\n");
            break;

        case ECFM_LBLT_TMR_LBR_HOLD:

            pLbLtTmrBlk = &(ECFM_LBLT_LBR_HOLD_TIMER);
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtTmrStartTimer:"
                           "Starting LBR Hold Timer \r\n");
            break;

        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtTmrStartTimer: Invalid timer type !!\n");
            i4RetVal = ECFM_FAILURE;
            break;
    }

    if (i4RetVal == ECFM_FAILURE)
    {
        return i4RetVal;
    }

    /* Converting Time Interval(in MSec) to Sec and MSec */
    u4IntervalSec = (u4Duration / 1000);
    u4IntervalMSec = (u4Duration % 1000);

    /* Start Timer */
    if (TmrStart (ECFM_LBLT_TMRLIST_ID, pLbLtTmrBlk, u1TimerType, u4IntervalSec,
                  u4IntervalMSec) == ECFM_TMR_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC, "EcfmLbLtTmrStartTimer: ERROR "
                       " Starting Timer for LBLT Task  \r\n");
        return ECFM_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 * Function Name      : EcfmLbLtTmrStopTimer                                 *
 *                                                                           *
 * Description        : This function is called to stop the timer for LBLT   *
 *                      task                                                 *
 *                                                                           *
 * Input(s)           : u1TimerType - Type of timer ot be started.           *
 *                      pMepInfo    - MEP Info for which the Timer  needs to *
 *                                    stopped.                               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtTmrStopTimer (UINT1 u1TimerType, tEcfmLbLtMepInfo * pMepInfo)
{
    tTmrBlk            *pLbLtTmrBlk = NULL;
    INT2                i2Offset = ECFM_INIT_VAL;

    switch (u1TimerType)
    {
        case ECFM_LBLT_TMR_VSR_TIMEOUT:
        case ECFM_LBLT_TMR_TH_DEADLINE:
        case ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT:
        case ECFM_LBLT_TMR_LTM_TX:
        case ECFM_LBLT_TMR_LBI_INTERVAL:
        case ECFM_LBLT_TMR_LBI_DEADLINE:
        case ECFM_LBLT_TMR_LBR_TIMEOUT:
        case ECFM_LBLT_TMR_DM_DEADLINE:
        case ECFM_LBLT_TMR_DM_INTERVAL:
        case ECFM_LBLT_TMR_1DM_TRANS_INTERVAL:
        case ECFM_LBLT_TMR_TST_INTERVAL:
        case ECFM_LBLT_TMR_TST_DEADLINE:
            i2Offset = gEcfmLbLtGlobalInfo.aTmrDesc[u1TimerType].i2Offset;
            pLbLtTmrBlk =
                (tTmrBlk *) (VOID *) ((UINT1 *) (pMepInfo) + i2Offset);
            break;

        case ECFM_LBLT_TMR_DELAY_QUEUE:

            pLbLtTmrBlk = &(ECFM_LBLT_DELAY_QUEUE ()->DelayQueueTimer);
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtTmrStopTimer:"
                           "Stop Delay Queue Timer \r\n");
            break;

        case ECFM_LBLT_TMR_LTR_HOLD:

            pLbLtTmrBlk = &(ECFM_LBLT_LTR_HOLD_TIMER);
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtTmrStopTimer:"
                           "Stop Ltr Hold Timer \r\n");
            break;

        case ECFM_LBLT_TMR_LBR_HOLD:

            pLbLtTmrBlk = &(ECFM_LBLT_LBR_HOLD_TIMER);
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtTmrStopTimer:"
                           "Stop LBR Hold Timer \r\n");
            break;

        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtTmrStopTimer: Invalid timer type !!\n");
            return ECFM_SUCCESS;

    }

    if (TmrStop (ECFM_LBLT_TMRLIST_ID, pLbLtTmrBlk) == ECFM_TMR_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLbLtTmrStopTimer:" "Stopping Timer Failed \r\n");
        return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmLbLtTmrExpHandler                                *
 *                                                                           *
 * Description        : This function is called from LBLT main routine that  *
 *                      that receives the timer ecpiry event.                *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtTmrExpHandler ()
{
    tTmrAppTimer       *pLbLtTimer = NULL;
    tEcfmLbLtPduSmInfo  PduSmInfo;
    INT2                i2Offset = ECFM_INIT_VAL;
    UINT1               u1TimerId = ECFM_INIT_VAL;
    if (ECFM_LBLT_TMRLIST_ID == ECFM_INIT_VAL)

    {
        return;
    }
    /* Intialise PDU SM Info */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);

    while ((pLbLtTimer =
            ECFM_GET_NEXT_EXPIRED_TMR (ECFM_LBLT_TMRLIST_ID)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pLbLtTimer)->u1TimerId;
        if (u1TimerId >= ECFM_LBLT_TMR_MAX_TYPES)
        {
            continue;
        }
        i2Offset = gEcfmLbLtGlobalInfo.aTmrDesc[u1TimerId].i2Offset;
        (*(gEcfmLbLtGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
            ((VOID *) (((UINT1 *) pLbLtTimer) - i2Offset));
    }

    /* Release Context */
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrLtmTxExpired                               *
 *                                                                            *
 * Description        : This routine is called when LTM Tx timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrLtmTxExpired (VOID *pArgs)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {

        EcfmRedSyncLbLtMepTmrExp (ECFM_RED_LTT_TIMER_EXPIRY,
                                  PduSmInfo.pMepInfo->u4MdIndex,
                                  PduSmInfo.pMepInfo->u4MaIndex,
                                  PduSmInfo.pMepInfo->u2MepId);

        /* Calling LTR Transmitter SM with TIMEOUT event */
        EcfmLbLtClntLtInitSm (&PduSmInfo, ECFM_SM_EV_LTI_TX_LTM_TIMESOUT);
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrLbiIntervalExpired                         *
 *                                                                            *
 * Description        : This routine is called when LBI Interval timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrLbiIntervalExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling LoopBack Initiator to Transmit next LBM if any is
         * left */
        EcfmLbLtClntLbInitiator (pMepInfo, ECFM_LBLT_LBI_INTERVAL_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrLbiDeadlineExpired                         *
 *                                                                            *
 * Description        : This routine is called when LBI deadline timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrLbiDeadlineExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling LoopBack Inititator to Stop the LBM transmission */
        EcfmLbLtClntLbInitiator (pMepInfo, ECFM_LBLT_LBI_DEADLINE_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrLbrTimeoutExpired                          *
 *                                                                            *
 * Description        : This routine is called when LBR Timeout timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrLbrTimeoutExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling LoopBack Inititator to Stop the LBM transmission */
        EcfmLbLtClntLbInitiator (pMepInfo, ECFM_LBLT_LBR_TIMEOUT_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrDmDeadlineExpired  
 *                                                                            *
 * Description        : This routine is called when DM deadline timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrDmDeadlineExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        EcfmLbLtClntDmInitiator (pMepInfo, ECFM_LBLT_DM_DEADLINE_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrDmIntervalExpired  
 *                                                                            *
 * Description        : This routine is called when DM Interval timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrDmIntervalExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call DM Initiator with DM While expiry event */
        EcfmLbLtClntDmInitiator (pMepInfo, ECFM_LBLT_DM_INTERVAL_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmr1DmTransIntExpired  
 *                                                                            *
 * Description        : This routine is called when 1DM Trans Interval timer 
 *                      gets expired.                                         *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmr1DmTransIntExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        pMepInfo->DmInfo.u1DmStatus = ECFM_TX_STATUS_READY;
        /* Initialize Number of 1DM In counter */
        pMepInfo->DmInfo.u2NoOf1DmIn = ECFM_INIT_VAL;
        /* Sync 1DM Transaction Interval Expiry at STANDBY Node */
        EcfmRedSyncDmTransIntrvalExp (pMepInfo);

        /* Sync DM Cache */
        EcfmRedSyncDmCache ();
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrTstIntervalExpired  
 *                                                                            *
 * Description        : This routine is called when Tst Interval timer gets 
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrTstIntervalExpired (VOID *pArgs)
{

    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling Tst Initiator to Transmit next TST if any is
         * left */
        EcfmLbLtClntTstInitiator (pMepInfo, ECFM_LBLT_TST_INTERVAL_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrTstDeadlineExpired                         *
 *                                                                            *
 * Description        : This routine is called when Tst Deadline gets         *
 *                      expired.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrTstDeadlineExpired (VOID *pArgs)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    pMepInfo = (tEcfmLbLtMepInfo *) pArgs;

    if (ECFM_LBLT_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {

        /* Calling Tst Inititator to Stop the TST transmission */
        EcfmLbLtClntTstInitiator (pMepInfo, ECFM_LBLT_TST_DEADLINE_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrThDeadlineExpired 
 *                                                                            *
 * Description        : This routine is called when Through Th Deadline 
 *                      gets expired.                                         * 
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrThDeadlineExpired (VOID *pArgs)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmLbLtMepInfo *) pArgs;
    if (ECFM_LBLT_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling TH Inititator to Stop the TH transmission */
        EcfmLbLtClntThInitiator (&PduSmInfo, ECFM_LBLT_TH_DEADLINE_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrVsrTimeoutExpired 
 *                                                                            *
 * Description        : This routine is called when Vsr Timeout gets expired. *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrVsrTimeoutExpired (VOID *pArgs)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmLbLtMepInfo *) pArgs;
    if (ECFM_LBLT_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling TH Inititator to Stop the TH transmission */
        EcfmLbLtClntThInitiator (&PduSmInfo, ECFM_LBLT_TH_VSR_TIMEOUT_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrThGetTstRxExpired 
 *                                                                            *
 * Description        : This routine is called when timer for getting the Tst
 *                      information gets expired.                             *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrThGetTstRxExpired (VOID *pArgs)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmLbLtMepInfo *) pArgs;
    if (ECFM_LBLT_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Calling TH Inititator */
        EcfmLbLtClntThInitiator (&PduSmInfo, ECFM_LBLT_TH_GET_TSTINFO_EXPIRY);

    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrLtrHoldExpired                             *
 *                                                                            *
 * Description        : This routine is called when Ltr HOLD timer expires.   *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to Context Info Structure             *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrLtrHoldExpired (VOID *pArgs)
{
    tEcfmLbLtContextInfo *pEcfmContextInfo = NULL;
    UINT4               u4LtrCacheHoldTime = ECFM_INIT_VAL;

    pEcfmContextInfo = (tEcfmLbLtContextInfo *) pArgs;
    if (ECFM_LBLT_SELECT_CONTEXT
        ((pEcfmContextInfo->u4ContextId)) != ECFM_SUCCESS)
    {
        return;
    }

    /* Convert default time value in to seconds */
    u4LtrCacheHoldTime =
        (UINT4) (ECFM_LBLT_LTR_CACHE_HOLD_TIME *
                 ECFM_NUM_OF_SEC_IN_A_MIN * ECFM_NUM_OF_MSEC_IN_A_SEC);

    /* Clear LTR Cache */
    RBTreeDrain (ECFM_LBLT_LTR_TABLE, (tRBKeyFreeFn)
                 EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LTR_ENTRY);
    EcfmRedSyncLtrCacheHldTmr (0);

    /* start Timer Again */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_LTR_HOLD, NULL, u4LtrCacheHoldTime) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtTmrExpHandler: LTR Hold Timer Cannot be"
                       "started !!\r\n");
        return;
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrLbrHoldExpired                             *
 *                                                                            *
 * Description        : This routine is called when Lbr HOLD timer expires.   *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to Context Info Structure             *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrLbrHoldExpired (VOID *pArgs)
{
    tEcfmLbLtContextInfo *pEcfmContextInfo = NULL;
    UINT4               u4LbrCacheHoldTime = ECFM_INIT_VAL;

    pEcfmContextInfo = (tEcfmLbLtContextInfo *) pArgs;
    if (ECFM_LBLT_SELECT_CONTEXT
        ((pEcfmContextInfo->u4ContextId)) != ECFM_SUCCESS)
    {
        return;
    }

    /* Convert default time value into seconds */
    u4LbrCacheHoldTime =
        ECFM_LBLT_LBR_CACHE_HOLD_TIME * ECFM_NUM_OF_SEC_IN_A_MIN;
    u4LbrCacheHoldTime = u4LbrCacheHoldTime * ECFM_NUM_OF_MSEC_IN_A_SEC;

    /* Clear LB Init Table maintained per LBM sent and remove the
     * LBRs received maintained per LBM */
    RBTreeDrain (ECFM_LBLT_LBM_TABLE, (tRBKeyFreeFn)
                 EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LBM_ENTRY);
    EcfmRedSyncLbrCacheHldTmr (0);
    /* start Timer Again */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_LBR_HOLD, NULL, u4LbrCacheHoldTime) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtTmrExpHandler: LBR Hold Timer Cannot be"
                       "started !!\r\n");
        return;
    }
}

/******************************************************************************
 * Function Name      : EcfmLbLtTmrDelayQExpired                              *
 *                                                                            *
 * Description        : This routine is called when Delay Queue timer 
 *                      expires.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to Context Info Structure             *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtTmrDelayQExpired (VOID *pArgs)
{
    tEcfmLbLtContextInfo *pEcfmContextInfo = NULL;

    pEcfmContextInfo = (tEcfmLbLtContextInfo *) pArgs;
    if (ECFM_LBLT_SELECT_CONTEXT
        ((pEcfmContextInfo->u4ContextId)) == ECFM_SUCCESS)
    {

        EcfmLbLtDelayQueueTimerTimeOut ();
    }
}

/****************************************************************************
  End of File cfmlbtmr.c
 ****************************************************************************/
