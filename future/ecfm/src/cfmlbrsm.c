/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbrsm.c,v 1.25 2015/08/08 12:58:42 siva Exp $
 *
 * Description: This file contains the functionality of the MEP 
 *              LoopBack Initiator Receive.
 *******************************************************************/

#include "cfminc.h"

PRIVATE VOID
    EcfmLbiRxPduCompare PROTO ((tEcfmLbLtPduSmInfo *, tEcfmLbLtLbrInfo *));
PRIVATE tEcfmLbLtLbmInfo *EcfmLbiRxGetInitLbmEntry PROTO ((UINT4, UINT4,
                                                           tEcfmLbLtMepInfo *));
PRIVATE INT4 EcfmLbiRxAddLbr PROTO ((tEcfmLbLtPduSmInfo *, tEcfmLbLtLbmInfo *));
PRIVATE             BOOL1
    EcfmLbiRxCheckForDuplicateLbr PROTO ((tEcfmLbLtPduSmInfo *,
                                          tEcfmLbLtLbmInfo *));
PRIVATE VOID EcfmSendLbrEventToCli PROTO ((tEcfmLbLtPduSmInfo *, UINT1));
PRIVATE tEcfmLbLtRMepDbInfo *EcfmLbRxGetRMepDbForMac
PROTO ((tEcfmLbLtMepInfo *, tEcfmMacAddr));

/*******************************************************************************
 * Function Name      : EcfmLbLtClntParserLbr
 *
 * Description        : This is called to parse the received LBR PDU to copy its
 *                      transaction Id into the tEcfmPduSmInfo Structure and to 
 *                      validate the first Tlv offset field value.
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      pu1Pdu     - Pointer to the LBR PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntParseLbr (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    ECFM_LBLT_TRC_FN_ENTRY ();

    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Copy the 4 byte Sequence number from the received LBR PDU */
    ECFM_GET_4BYTE (pPduSmInfo->uPduInfo.u4LbrSeqNumber, pu1Pdu);

    /* Check that First TLV Offset value received in the LBR PDU should be
     * greater or equal to 4 */
    if (pPduSmInfo->u1RxFirstTlvOffset < ECFM_LBR_FIRST_TLV_OFFSET)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParseLbr: "
                       "MEP Loopback Initiator Receive SM discards received LBR "
                       "since First TLV Offset value in the LBR PDU is less than "
                       "4\r\n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLbiRxProcessLbr
 *
 * Description        : This routine is called to process the received LBR.
 *                        
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbiRxProcessLbr (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtRMepDbInfo *pRMepDbInfo = NULL;

    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };
    UINT4              *pu4ExpectedSeqNum = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);

    if (pLbInfo->u1TxLbmStatus != ECFM_TX_STATUS_NOT_READY)
    {
        /* Discard the LBR frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbiRxProcessLbr: "
                       "discarding the received LBR frame, there is no on going"
                       " LB transaction for this MEP \r\n");
        return ECFM_FAILURE;
    }

    if (pLbInfo->LbmPdu.pu1Octets == NULL)
    {
        /* Discard the LBR frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbiRxProcessLbr: "
                       "discarding the received LBR frame, there is no LBM"
                       " transmitted for this MEP \r\n");
        return ECFM_FAILURE;
    }

    /* If the selector type is LSP/PW then validate for Replying MEP ID TLV and
     * Requesting MEP ID TLV
     */
    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        if (EcfmMpTpLbValidateLbr (pPduSmInfo) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiRxProcessLbr: "
                           "Validation for LBR failed!!! \r\n");
            return ECFM_FAILURE;
        }

        pu4ExpectedSeqNum = &pLbInfo->u4ExpectedLbrSeqNo;
    }
    else                        /* Ethernet base MEPs */
    {
        /* Check that Destination Address match that of the received MEP Mac 
         * Address */
        ECFM_LBLT_GET_MAC_ADDR_OF_MEP (pMepInfo, MepMacAddr);
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                                   MepMacAddr) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiRxProcessLbr: "
                           "MEP discards received LBR since destination address "
                           "doesnot match that of the received MEP Mac "
                           "address\r\n");
            return ECFM_FAILURE;
        }

        /* Check that Source address should not contains Group Address */
        if (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxSrcMacAddr) == ECFM_TRUE)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiRxProcessLbr: "
                           "MEP discards received LBR since source address "
                           "contains group address\r\n");
            return ECFM_FAILURE;
        }

        /*Check if the Multicast Lbm Initiated in case of burst mode then 
         * Expected Seq No. is taken from the relevent RMepDb for the MEP
         * otherwise from the LbInfo itself*/

        if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum) ||
            (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) &&
             (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_BURST_TYPE)))
        {
            if ((pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MULTICAST))
            {
                pRMepDbInfo =
                    EcfmLbRxGetRMepDbForMac (pMepInfo,
                                             pPduSmInfo->RxSrcMacAddr);

                if (pRMepDbInfo == NULL)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbiRxProcessLbr: "
                                   "No RMEP for this MEP\r\n");
                    EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_UNEXP_LBR);
                    return ECFM_FAILURE;
                }
                pu4ExpectedSeqNum = &pRMepDbInfo->u4ExpectedLbrSeqNo;
            }
            else
            {
                pu4ExpectedSeqNum = &pLbInfo->u4ExpectedLbrSeqNo;
            }
        }
        else
        {
            pu4ExpectedSeqNum = &pLbInfo->u4ExpectedLbrSeqNo;
        }
    }                            /* end of else - Ethernet based MEP processing */

    /* Y.1731/ECFM : Get the last LBM node sent from the LBInit Table 
     * then the received LBR is attached to that LBM entry */
    if (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE)
    {
        pLbmNode = EcfmLbiRxGetInitLbmEntry (pLbInfo->u4NextTransId,
                                             *pu4ExpectedSeqNum, pMepInfo);
        if (pLbmNode == NULL)
        {
            /* For CLI ping */
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiRxProcessLbr: "
                           "LBM entry not found - failure\r\n");
            EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_ERR_PROC_LBR);
            return ECFM_FAILURE;
        }
    }

    /* Check the received LBR's seq number with the last sent LBM's seq num if
     * doesnot matches increment the out-of-seq counter */
    if (*pu4ExpectedSeqNum != pPduSmInfo->uPduInfo.u4LbrSeqNumber)
    {
        ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                            "EcfmLbiRxProcessLbr: "
                            "LBR's Seq num  %d doesnot match the expected Seq Num %d. "
                            "Incrementing Number of Out-of-sequence LBRs "
                            "received\r\n", pPduSmInfo->uPduInfo.u4LbrSeqNumber,
                            *pu4ExpectedSeqNum);
        ECFM_LBLT_INCR_LBR_IN_OUT_OF_ORDER (pLbInfo);
        if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) &&
            (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE) && (pLbmNode != NULL))
        {
            ECFM_LBLT_INCR_UNEXP_LBR_IN (pLbmNode);
        }
        /* In case of ECFM we need to change the expected seq-no to the next
         * possible sequence no*/
        if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum) ||
            ((ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
             (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_BURST_TYPE)))
        {
            *pu4ExpectedSeqNum = pPduSmInfo->uPduInfo.u4LbrSeqNumber;
            *pu4ExpectedSeqNum = *pu4ExpectedSeqNum + ECFM_INCR_VAL;
        }

        /* For CLI ping */
        EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_UNEXP_LBR);
        return ECFM_FAILURE;
    }

    /* LBR expected sequence number to be set to the next possible sequence
     * number expected in LBR*/
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum) ||
        ((ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
         (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_BURST_TYPE)))
    {
        *pu4ExpectedSeqNum = *pu4ExpectedSeqNum + ECFM_INCR_VAL;
    }

    /* Check that the received LBR should not be a duplicated LBR */
    if ((pLbmNode != NULL) &&
        (EcfmLbiRxCheckForDuplicateLbr (pPduSmInfo, pLbmNode) == ECFM_TRUE))
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbiRxProcessLbr: "
                       "Duplicate LBR is received. "
                       "Incrementing Number of Duplicate LBRs " "received\r\n");
        ECFM_LBLT_INCR_DUP_LBR_IN (pLbmNode);
        /* For CLI ping */
        EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_DUP_MSG_LBR);
    }
    else
    {
        ECFM_LBLT_INCR_LBR_IN_ORDER (pLbInfo);

        /* New LBR entry is attached to the last sent LBM entry */
        if ((pLbmNode != NULL) &&
            (EcfmLbiRxAddLbr (pPduSmInfo, pLbmNode) != ECFM_SUCCESS))
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbiRxProcessLbr: "
                           "Adding LBM enty in the LbmInfo FAILED\r\n");
            /* For CLI ping */
            EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_ERR_PROC_LBR);
            return ECFM_FAILURE;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLbiRxGetInitLbmEntry
 *
 * Description        : This routine is used to get the pointer corresponding to
 *                      the LBM entry according to the Mep's TransId and Lbm's 
 *                      expected sequence number from the Lbm Table.
 *                        
 * Input(s)           : u4TransId - Current transaction
 *                      u4SeqNum  - Expected sequence number in LBR
 *                      pMepInfo  - Pointer to the MEP info
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : Pointer to LBM Node from the LbmInfo
 ******************************************************************************/
PRIVATE tEcfmLbLtLbmInfo *
EcfmLbiRxGetInitLbmEntry (UINT4 u4LbTransId, UINT4 u4LbSeqNum,
                          tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLbmInfo    LbmNode;
    tEcfmLbLtLbmInfo   *pLbmEntry = NULL;

    ECFM_MEMSET (&LbmNode, ECFM_INIT_VAL, ECFM_LBLT_LBM_INFO_SIZE);

    LbmNode.u4MdIndex = pMepInfo->u4MdIndex;
    LbmNode.u4MaIndex = pMepInfo->u4MaIndex;
    LbmNode.u2MepId = pMepInfo->u2MepId;
    LbmNode.u4SeqNum = u4LbSeqNum;
    LbmNode.u4TransId = u4LbTransId;

    /* Get LBM entry for the MEP using its indices and LBM's Sequence Number 
     * and Transaction id from the Lbm tree maintained */
    pLbmEntry = (tEcfmLbLtLbmInfo *) RBTreeGet (ECFM_LBLT_LBM_TABLE, &LbmNode);
    return pLbmEntry;
}

/****************************************************************************
 * Function Name    : EcfmLbiRxAddLbr 
 *
 * Description      : This routine is used to add the received LBR in the
 *                    LbrTable maintained on the basis of its reception.
 *
 * Input (s)        : pEcfmLbLtPduSmInfo - Pointer to the structure that stores
 *                    the information regarding the MP information, the PDU 
 *                    (if received) and other information related to the
 *                    funtionality.
 *                    pLbNode - Pointer to the structure containing 
 *                    information regarding Lbm Table.
 *                        
 * Output (s)       : None.
 *
 * Return Value(s)  : ECFM_SUCCESS / ECFM_FAILURE
 ****************************************************************************/
PRIVATE INT4
EcfmLbiRxAddLbr (tEcfmLbLtPduSmInfo * pPduSmInfo, tEcfmLbLtLbmInfo * pLbNode)
{
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLbrInfo   *pLbrSllLastNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmDllCurNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmDllNextNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmRbTreeDelNode = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP Information from the received PduSm Info */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get LB Information from the Mep Info */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);

    /* Y.1731/ECFM : Get the last LBM node sent from the LBInit Table */
    if (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE)
    {
        /* Allocate Memory to the LBR Node to be addded in the LbrTable */
        ECFM_ALLOC_MEM_BLOCK_LBLT_LBR_TABLE (pLbrNode);
        while (pLbrNode == NULL)
        {
            if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_LBR_TABLE_POOL) != 0)
            {
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                               ECFM_OS_RESOURCE_TRC,
                               "EcfmLbiRxAddLbr: "
                               "Memory Block exists but Memory Allocation to "
                               "LBR Node Failed\r\n");
                return ECFM_FAILURE;
            }

            /* If the addition of this LBR entry exceeds the resources
             * allocated to Lbr table, then delete the oldest LBM entry
             * if any in the LbmDllList.
             */
            UTL_DLL_OFFSET_SCAN (&(gEcfmLbLtGlobalInfo.LbmDllList),
                                 pLbmDllCurNode, pLbmDllNextNode,
                                 tEcfmLbLtLbmInfo *)
            {
                /* Break after getting the first node which is also the
                 * oldest LBM entry.
                 */
                break;
            }
            /* Get the correponding node from the RB Tree and delete it */
            pLbmRbTreeDelNode =
                (tEcfmLbLtLbmInfo *) RBTreeGet (ECFM_LBLT_LBM_TABLE,
                                                pLbmDllCurNode);
            if (pLbmRbTreeDelNode == NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbiRxAddLbr: " "Memory corrupted\r\n");
                return ECFM_FAILURE;
            }

            EcfmLbLtUtilRemoveLbEntry (pLbmRbTreeDelNode);
            ECFM_ALLOC_MEM_BLOCK_LBLT_LBR_TABLE (pLbrNode);
        }
        ECFM_MEMSET (pLbrNode, ECFM_INIT_VAL, ECFM_LBLT_LBR_INFO_SIZE);

        /* Fill the Time elapsed in receiving the LBR */
        do
        {
            UINT4               u4RemainingTicks = ECFM_INIT_VAL;
            UINT4               u4TotalTimeTicks = ECFM_INIT_VAL;

            if (ECFM_GET_REMAINING_TIME (ECFM_LBLT_TMRLIST_ID,
                                         &(pLbInfo->LbiIntervalTimer.
                                           TimerNode),
                                         &u4RemainingTicks) != ECFM_SUCCESS)
            {
                u4RemainingTicks = ECFM_INIT_VAL;
                pLbrNode->u4LbrRcvTime = ECFM_INIT_VAL;
                break;
            }
            if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_MSEC)
            {
                u4TotalTimeTicks =
                    ECFM_CONVERT_MSEC_TO_SNMP_TIME_TICKS (pLbInfo->
                                                          u4TxLbmInterval);
            }
            if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_SEC)
            {
                u4TotalTimeTicks =
                    ECFM_CONVERT_SEC_TO_SNMP_TIME_TICKS (pLbInfo->
                                                         u4TxLbmInterval);
            }
            /* Check is total time is greater than the remaining time ticks */
            if (u4TotalTimeTicks >= u4RemainingTicks)
            {
                pLbrNode->u4LbrRcvTime =
                    (u4TotalTimeTicks - u4RemainingTicks);
            }
        }
        while (0);

        /* Compare the TLVs in the LBR with the last sent LBM */
        /* Check if any tlv was sent in the LBM PDU */
        if (pLbNode->u1TlvStatus != ECFM_LBLT_LBM_WITHOUT_PATTERN)
        {
            EcfmLbiRxPduCompare (pPduSmInfo, pLbrNode);
        }
        else
        {
            /* For CLI ping */
            EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_VALID_LBR);
        }
        ECFM_LBLT_INCR_NUM_OF_RESPONDERS (pLbNode);
        /* Fill the LBR Mac address */
        ECFM_MEMCPY (pLbrNode->LbrSrcMacAddr, pPduSmInfo->RxSrcMacAddr,
                     ECFM_MAC_ADDR_LENGTH);

        pLbrNode->u1LbrDestType = pLbInfo->u1TxLbmDestType;
        pLbrNode->u2LbrDestMepId = pLbInfo->u2TxDestMepId;
        ECFM_MEMCPY (pLbrNode->au1LbrIcc, pLbInfo->au1LbmIcc,
                     ECFM_CARRIER_CODE_ARRAY_SIZE);
        pLbrNode->u4LbrNodeId = pLbInfo->u4LbmNodeId;
        pLbrNode->u4LbrIfNum = pLbInfo->u4LbmIfNum;

        /* Get Last LBR Entry in the SLL to increment the Receive Order in the LBR
         * SLL Node */
        pLbrSllLastNode =
            (tEcfmLbLtLbrInfo *) TMO_SLL_Last (&(pLbNode->LbrList));
        if (pLbrSllLastNode != NULL)
        {
            pLbrNode->u4RcvOrder = pLbrSllLastNode->u4RcvOrder + ECFM_INCR_VAL;
        }
        else
        {
            /* First LBR received for this LBM */
            pLbrNode->u4RcvOrder = ECFM_RCVD_ORDER_INIT_VAL;
        }

        /* Update the Number of LBR received counter */
        ECFM_LBLT_INCR_LBR_RCVD_COUNT (pLbInfo);
        pLbNode->u2NoOfLBRRcvd = pLbInfo->u2NoOfLbrIn;

        /* Add LBR  node in the SLL maintained by MEP per Transaction and per LBM's
         * Seq number */
        TMO_SLL_Add (&(pLbNode->LbrList),
                     (tEcfmSllNode *) & (pLbrNode->LbrTableSllNode));
    }
    else
    {
        EcfmLbiRxPduCompare (pPduSmInfo, NULL);
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLbiRxPduCompare
 *
 * Description        : This routine is called to perform the bit-by-bit
 *                      comparison of the received LBR PDU with the last 
 *                      transmitted LBM PDU.
 *
 * Input(s)           : pPduSmInfo  - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      pLbmInfo - Pointer to LBM Info.
 *                      pLbrInfo    - Pointer to LBR Info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLbiRxPduCompare (tEcfmLbLtPduSmInfo * pPduSmInfo,
                     tEcfmLbLtLbrInfo * pLbrInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1LbrPdu = NULL;
    UINT1              *pu1LbmPdu = NULL;
    UINT2               u2PduLen = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2TlvLength = ECFM_INIT_VAL;
    UINT1               u1TlvStatus = ECFM_INIT_VAL;
    UINT1               u1TlvType = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);

    pu1LbmPdu = pLbInfo->LbmPdu.pu1Octets;

    /* Move the pointer to the location of first TLV */
    pu1LbmPdu = pu1LbmPdu + ECFM_CFM_HDR_SIZE + ECFM_LBM_SEQ_NUM_FIELD_SIZE;

    /* Get the size of the TLVs field in the transmitted LBM */
    u2PduLen = (UINT2) pLbInfo->LbmPdu.u4OctLen -
        (UINT2) (ECFM_CFM_HDR_SIZE + ECFM_LBM_SEQ_NUM_FIELD_SIZE);

    /* Move the offset to the location of the first TLV */
    u2Offset = (UINT2) pPduSmInfo->u1CfmPduOffset +
        (UINT2) (ECFM_CFM_HDR_SIZE + ECFM_LBM_SEQ_NUM_FIELD_SIZE);

    /* For MPLS-TP case, LBR contains Target MEP ID TLV and optional 
     * Requesting MEP ID TLV. We need to move the offset of the TLV
     */
    /* Get First TLV Type from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u2Offset, u1TlvType);

    while (((u1TlvType == ECFM_REPLY_MPID_TLV_TYPE) ||
            (u1TlvType == ECFM_REQ_MEPID_TLV_TYPE)) &&
           u2Offset < (UINT2) pLbInfo->LbmPdu.u4OctLen)
    {
        u2Offset += ECFM_TLV_TYPE_FIELD_SIZE;
        /* Get the length of the TLV */
        ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u2Offset, u2TlvLength);
        u2Offset += (ECFM_TLV_LENGTH_FIELD_SIZE + u2TlvLength);

        pu1LbmPdu += (ECFM_TLV_TYPE_LEN_FIELD_SIZE + u2TlvLength);
        u2PduLen = u2PduLen - (ECFM_TLV_TYPE_LEN_FIELD_SIZE + u2TlvLength);

        ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u2Offset, u1TlvType);
    }

    pu1LbrPdu = ECFM_LBLT_PDU;

    /* Copy the TLVs from LBR pdu into pu1LbrPdu */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf,
                         pu1LbrPdu, (UINT4) (u2Offset), u2PduLen);

    /* TLV send in LB  */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        switch (pLbInfo->u1TxLbmTlvOrNone)
        {
            case ECFM_LBLT_LBM_WITH_DATA_TLV:
                u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN;
                break;

            case ECFM_LBLT_LBM_WITH_TEST_TLV:
                if ((pLbInfo->u1TxLbmTstPatternType ==
                     ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC) ||
                    (pLbInfo->u1TxLbmTstPatternType ==
                     ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC))
                {
                    u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN_CRC;
                }
                else
                {
                    u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN;
                }
                break;

            case ECFM_LBLT_LBM_WITHOUT_TLV:
                u1TlvStatus = ECFM_LBLT_LBM_WITHOUT_PATTERN;
                break;
            default:
                break;
        }
    }
    else
    {
        /* TLVs will be present as per 802.1g */
        u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN;
    }

    /* Compare the LBM and LBR PDUs TLVs */
    if (ECFM_MEMCMP (pu1LbmPdu, pu1LbrPdu, u2PduLen) != ECFM_SUCCESS)
    {
        switch (u1TlvStatus)
        {
            case ECFM_LBLT_LBM_WITH_PATTERN:
                /* Set the u1LbrErrorStatus in LbrInfo to reflect bad msdu error */
                if ((pLbrInfo != NULL)
                    &&
                    (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
                    && (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE))
                {
                    ECFM_LBLT_SET_BAD_MSDU_ERR (pLbrInfo);
                }
                /* Increment the counter if PDUs doesnot match */
                ECFM_LBLT_INCR_BAD_MSDU (pLbInfo);
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbiRxPduCompare: "
                               "MEP received LBR that doesnot match that of last "
                               "LBM transmitted by this MEP\r\n");
                /* For CLI ping */
                EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_BAD_MSDU_LBR);
                break;
            case ECFM_LBLT_LBM_WITH_PATTERN_CRC:
                /* Set the u1LbrErrorStatus in LbrInfo to reflect bit error */
                if ((pLbrInfo != NULL)
                    &&
                    (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
                    && (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE))
                {
                    ECFM_LBLT_SET_BIT_ERR (pLbrInfo);
                }
                /* Increment the counter if crc doesnot match */
                ECFM_LBLT_INCR_BIT_ERROR (pLbInfo);
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbiRxPduCompare: "
                               "MEP received LBR whose CRC value doesnot match "
                               "that of the last LBM transmitted by this MEP\r\n");
                /* For CLI ping */
                EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_BIT_ERR_LBR);
                break;
            default:
                break;
        }
    }
    else
    {
        /* For CLI ping */
        EcfmSendLbrEventToCli (pPduSmInfo, CLI_ECFM_VALID_LBR);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name    : EcfmLbiRxCheckForDuplicateLbr
 *
 * Description      : This routine is used to check for any duplicate LBR.
 *
 * Input (s)        : pEcfmLbLtPduSmInfo - Pointer to the structure that stores
 *                    the information regarding the MP information, the PDU 
 *                    (if received) and other information related to the
 *                    funtionality.
 *                    pLbNode - Pointer to the structure containing 
 *                    information regarding LBInit Table.
 *                        
 * Output (s)       : None.
 *
 * Return Value(s)  : ECFM_TRUE / ECFM_FALSE
 ****************************************************************************/
PRIVATE             BOOL1
EcfmLbiRxCheckForDuplicateLbr (tEcfmLbLtPduSmInfo * pPduSmInfo,
                               tEcfmLbLtLbmInfo * pLbNode)
{
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pMepInfo->u2PortNum) &&
        (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE))
    {
        pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_First (&(pLbNode->LbrList));
        while (pLbrNode != NULL)
        {
            if (ECFM_MEMCMP (pLbrNode->LbrSrcMacAddr, pPduSmInfo->RxSrcMacAddr,
                             ECFM_MAC_ADDR_LENGTH) == ECFM_SUCCESS)
            {
                return ECFM_TRUE;
            }
            /* Get the next LBR node from the LBR list */
            pLbrNode =
                (tEcfmLbLtLbrInfo *) TMO_SLL_Next (&(pLbNode->LbrList),
                                                   (tEcfmSllNode *) &
                                                   (pLbrNode->LbrTableSllNode));
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();

    return ECFM_FALSE;
}

/*******************************************************************************
 * Function Name      : EcfmSendLbrEventToCli
 *
 * Description        : This routine will send the event to the CLI on reception 
 *                      of the LBR.
 *
 * Input(s)           : pPduSmInfo  - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      u1LbrType   - Type of the LBR received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmSendLbrEventToCli (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 u1LbrType)
{
    tEcfmLbrRcvdInfo   *pLbrRcvd = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtCliEvInfo *pCliEvent = NULL;
    UINT4               u4RemainingTicks = ECFM_INIT_VAL;
    UINT4               u4TotalTimeTicks = ECFM_INIT_VAL;
    UINT2               u2ByteCount = ECFM_INIT_VAL;

    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);
    pCliEvent = ECFM_LBLT_CLI_EVENT_INFO (pLbInfo->CurrentCliHandle);
    if (pCliEvent == NULL)
    {
        return;
    }
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        return;
    }
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);

    if (pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY)
    {
        return;
    }

    ECFM_ALLOC_MEM_BLOCK_LBR_RCVD_INFO (pLbrRcvd);
    if (pLbrRcvd == NULL)
    {
        return;
    }

    ECFM_MEMSET (pLbrRcvd, 0x00, sizeof (tEcfmLbrRcvdInfo));

    ECFM_MEMCPY (pLbrRcvd->RxLbrSrcMacAddr, pPduSmInfo->RxSrcMacAddr,
                 ECFM_MAC_ADDR_LENGTH);

    /* Get the remaining time */
    pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_LBR_RECEIVED);
    if (ECFM_GET_REMAINING_TIME (ECFM_LBLT_TMRLIST_ID,
                                 &(pLbInfo->LbiIntervalTimer.TimerNode),
                                 &u4RemainingTicks) != ECFM_SUCCESS)
    {
        u4RemainingTicks = ECFM_INIT_VAL;
    }
    if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_MSEC)
    {
        u4TotalTimeTicks =
            ECFM_CONVERT_MSEC_TO_TIME_TICKS (pLbInfo->u4TxLbmInterval);
    }
    if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_SEC)
    {
        u4TotalTimeTicks =
            ECFM_CONVERT_SEC_TO_TIME_TICKS (pLbInfo->u4TxLbmInterval);
    }
    /* Check is total time is greater than the remaining time */
    if (u4TotalTimeTicks >= u4RemainingTicks)
    {
        pLbrRcvd->u4LbrRcvTime =
            ECFM_CONVERT_TIME_TICKS_TO_MSEC ((u4TotalTimeTicks -
                                              u4RemainingTicks));
    }
    else
    {
        pLbrRcvd->u4LbrRcvTime = ECFM_INIT_VAL;
    }
    /* Getting the size of PDU received */
    u2ByteCount = (UINT2) (ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pBuf));
    pLbrRcvd->u2LbrPduSize = u2ByteCount;

    /* Sending event to CLI depending on the type of the LBR received */
    switch (u1LbrType)
    {
        case CLI_ECFM_UNEXP_LBR:
            pLbrRcvd->u4LbrSeqNumber = pPduSmInfo->uPduInfo.u4LbrSeqNumber;
            pLbrRcvd->u1LbrType = CLI_ECFM_UNEXP_LBR;
            pLbrRcvd->u4LbrRcvTime = ECFM_INIT_VAL;
            break;
        case CLI_ECFM_DUP_MSG_LBR:
            pLbrRcvd->u4LbrSeqNumber = pPduSmInfo->uPduInfo.u4LbrSeqNumber;
            pLbrRcvd->u1LbrType = CLI_ECFM_DUP_MSG_LBR;
            pLbrRcvd->u4LbrRcvTime = ECFM_INIT_VAL;
            break;
        case CLI_ECFM_BIT_ERR_LBR:
            pLbrRcvd->u4LbrSeqNumber = pPduSmInfo->uPduInfo.u4LbrSeqNumber;
            pLbrRcvd->u1LbrType = CLI_ECFM_BIT_ERR_LBR;
            break;
        case CLI_ECFM_VALID_LBR:
            pLbrRcvd->u4LbrSeqNumber = pPduSmInfo->uPduInfo.u4LbrSeqNumber;
            pLbrRcvd->u1LbrType = CLI_ECFM_VALID_LBR;
            break;
        case CLI_ECFM_ERR_PROC_LBR:
        default:
            pLbrRcvd->u1LbrType = CLI_ECFM_ERR_PROC_LBR;
    }
    TMO_SLL_Add (&(pCliEvent->Msg.List),
                 (tEcfmSllNode *) & (pLbrRcvd->SllNode));
    ECFM_GIVE_SEMAPHORE (pCliEvent->SyncSemId);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLbRxGetRMepDbForMac
 *
 * Description        : This routine is used to get the RMEp Node Entry from the
 *                      RMep form where LBR is received
 *                        
 * Input(s)           : pMepInfo  - Pointer to the MEP info
 *                      MacAddr   - Src Mac Address of the Lbr Recived 
 *                      pMepInfo  - Pointer to the MEP info
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : Pointer to the RMepDb NodeLBM
 ******************************************************************************/
PRIVATE tEcfmLbLtRMepDbInfo *
EcfmLbRxGetRMepDbForMac (tEcfmLbLtMepInfo * pMepInfo, tEcfmMacAddr MacAddr)
{

    tEcfmLbLtRMepDbInfo RMepInfo;
    tEcfmLbLtRMepDbInfo *pRMepNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_LBLT_RMEP_DB_INFO_SIZE);

    /* Get Remote MEP entry corresponding to indices - MdIndex, 
     * MaIndex, MepId*/
    RMepInfo.u4MdIndex = pMepInfo->u4MdIndex;
    RMepInfo.u4MaIndex = pMepInfo->u4MaIndex;
    RMepInfo.u2MepId = pMepInfo->u2MepId;

    pRMepNode =
        RBTreeGetNext (ECFM_LBLT_RMEP_DB_TABLE, (tRBElem *) & RMepInfo, NULL);

    while (pRMepNode != NULL)
    {
        if (ECFM_MEMCMP (MacAddr,
                         pRMepNode->RMepMacAddr, ECFM_MAC_ADDR_LENGTH) == 0)
        {
            return pRMepNode;
        }

        pRMepNode = RBTreeGetNext (ECFM_LBLT_RMEP_DB_TABLE, pRMepNode, NULL);

        if ((pRMepNode == NULL) ||
            (pRMepNode->u4MdIndex != pMepInfo->u4MdIndex) ||
            (pRMepNode->u4MaIndex != pMepInfo->u4MaIndex) ||
            (pRMepNode->u2MepId != pMepInfo->u2MepId))
        {
            break;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return pRMepNode;
}

/******************************************************************************/
/*                           End  of cfmlbrsm.c                               */
/******************************************************************************/
