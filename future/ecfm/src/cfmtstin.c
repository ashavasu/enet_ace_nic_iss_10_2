/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmtstin.c,v 1.16 2014/03/16 11:34:12 siva Exp $
 *
 * Description: This file contains the functionality of the MEP 
 *              TST Initiator Transmit.
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmTstTxFormatTstPduHdr PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmTstTxPutTstInfo PROTO ((tEcfmLbLtTstInfo *, UINT1 **));
PRIVATE VOID EcfmTstTxPutTestTlv PROTO ((tEcfmLbLtTstInfo *, UINT1 **));
PRIVATE VOID EcfmTstTxAdjustPattern PROTO ((UINT1 *, UINT2, UINT2, UINT1 **));
PRIVATE INT4 EcfmTstTxXmitTstPdu PROTO ((tEcfmLbLtMepInfo *));
PRIVATE VOID EcfmTstTxStopXmitTstPdu PROTO ((tEcfmLbLtMepInfo *));
PRIVATE VOID EcfmSendTstEventToCli PROTO ((tEcfmLbLtMepInfo *, UINT1, UINT2));

/*******************************************************************************
 * Function Name      : EcfmLbLtClntTstInitiator  
 *
 * Description        : This is the interface routine of TST module, which 
 *                      calls the various routines to handle the occurrence of 
 *                      the various events.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntTstInitiator (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventId)
{
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    UINT4               u4DeadlineTime = ECFM_INIT_VAL;
    UINT4               u4RetVal = ECFM_FAILURE;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pMepInfo);

    /* Calling Functions acc. to the event received */
    switch (u1EventId)

    {
        case ECFM_EV_MEP_BEGIN:
            pTstInfo->u1TstStatus = ECFM_TX_STATUS_READY;
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_LBLT_TST_DEADLINE_EXPIRY:
        case ECFM_TST_STOP_TRANSACTION:
        case ECFM_EV_MEP_NOT_ACTIVE:

            /* Stop the running transaction */
            EcfmTstTxStopXmitTstPdu (pMepInfo);

            /* Sync the stop event to the standby node */
            EcfmRedSyncTransactionStopEvent (ECFM_RED_TST_TIMER_EXPIRY,
                                             pMepInfo->u4MdIndex,
                                             pMepInfo->u4MaIndex,
                                             pMepInfo->u2MepId);
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_TST_START_TRANSACTION:

            /* Status set to indicate no other transaction possible till this
             * completion */
            pTstInfo->u1TstStatus = ECFM_TX_STATUS_NOT_READY;
            pTstInfo->u2TstLastPatternSize = ECFM_INIT_VAL;

            pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);

            /*In case Throughput SM in Not Ready State, it means throughput
             * transaction running and deadline is to be started as Burst
             * Deadline in milliseconds*/
            if (pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY)
            {
                if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_DL)
                {
                    /* Start the Timer with u4DeadlineTime milliseconds */
                    if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_TST_DEADLINE,
                                               pMepInfo,
                                               pThInfo->u4TxThBurstDeadline) !=
                        ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                       ECFM_ALL_FAILURE_TRC,
                                       "EcfmLbLtClntLbInitiator: "
                                       "TstDeadLine Timer has not started\r\n");

                        u4RetVal = ECFM_FAILURE;
                        break;
                    }
                }
            }
            /* Y.1731 : Start Timer TstDeadLine, if configured */
            if (pTstInfo->u4TstDeadLine != 0)

            {
                u4DeadlineTime =
                    ECFM_NUM_OF_MSEC_IN_A_SEC * pTstInfo->u4TstDeadLine;

                /* Start the Timer with u4DeadlineTime milliseconds */
                if (EcfmLbLtTmrStartTimer
                    (ECFM_LBLT_TMR_TST_DEADLINE, pMepInfo,
                     u4DeadlineTime) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntLbInitiator: "
                                   "TSTDeadLine Timer has not started\r\n");
                    break;
                }
            }
            EcfmRedSynchHwAuditCmdEvtInfo (ECFM_LBLT_CURR_CONTEXT_ID (),
                                           ECFM_RED_TST_NPAPI,
                                           ECFM_INIT_VAL,
                                           pMepInfo->u4MdIndex,
                                           pMepInfo->u4MaIndex,
                                           pMepInfo->u2MepId,
                                           ECFM_INIT_VAL, ECFM_TRUE);

#ifdef NPAPI_WANTED
            if ((ECFM_HW_TST_SUPPORT () == ECFM_TRUE) &&
                (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE))
            {
                tEcfmMepInfoParams  EcfmMepInfoParams;
                tEcfmConfigTstInfo  EcfmConfigTstInfo;
                ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                             sizeof (tEcfmMepInfoParams));
                ECFM_MEMSET (&EcfmConfigTstInfo, ECFM_INIT_VAL,
                             sizeof (tEcfmConfigTstInfo));
                EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
                EcfmMepInfoParams.u4IfIndex =
                    ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum,
                                            pTempLbLtPortInfo);
                EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
                EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
                EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;
                EcfmConfigTstInfo.u1Status = pTstInfo->u1TstStatus;
                EcfmConfigTstInfo.u1TstTlvPatterType =
                    pTstInfo->u1TstPatternType;
                EcfmConfigTstInfo.b1VariableByte = pTstInfo->b1TstVariableBytes;
                EcfmConfigTstInfo.b1DropEnable = pTstInfo->b1TstDropEligible;
                EcfmConfigTstInfo.u4TxTstMessages = pTstInfo->u4TstMessages;
                EcfmConfigTstInfo.u4TstInterval = pTstInfo->u4TstInterval;
                EcfmConfigTstInfo.u2TstTlvPatterSize =
                    pTstInfo->u2TstPatternSize;
                EcfmConfigTstInfo.u4Deadline = pTstInfo->u4TstDeadLine;
                ECFM_MEMCPY (EcfmConfigTstInfo.DestMacAddr,
                             pTstInfo->TstDestMacAddr, ECFM_MAC_ADDR_LENGTH);

                if (EcfmFsMiEcfmStartTstTransaction
                    (&EcfmMepInfoParams, &EcfmConfigTstInfo) != FNP_SUCCESS)
                {
                    return ECFM_FAILURE;
                }
                return ECFM_SUCCESS;
            }
#endif /*  */

            /* Y.1731 : Check this u4TstMessages variable if configured 
             * then send the configured no. of TSTs else send infinite TSTs */
            if (pTstInfo->u4TstMessages == 0)

            {

                /* Set MSB in the variable to indicate infinite transmission */
                ECFM_SET_INFINITE_TX_STATUS (pTstInfo->u4TstMessages);
            }
            /* TST can be send using mepid, unicast or multicast mac addr
             * depending on the u1TstDestType. */
            if (pTstInfo->u1TxTstDestType == ECFM_TX_DEST_TYPE_MEPID)
            {
                /* Get RMEP MAC Address for the Destination MEPID */
                if (EcfmLbLtUtilGetRMepMacAddr
                    (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                     pMepInfo->u2MepId, pTstInfo->u2TstDestMepId,
                     pTstInfo->TstDestMacAddr) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC
                                   | ECFM_OS_RESOURCE_TRC,
                                   "EcfmLbLtClntTstInitiator"
                                   "Rmep Mac Address not Found !! \r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            else if (pTstInfo->u1TxTstDestType == ECFM_TX_DEST_TYPE_MULTICAST)
            {
                ECFM_GEN_MULTICAST_CLASS1_ADDR (pTstInfo->TstDestMacAddr,
                                                pMepInfo->u1MdLevel);
            }

            /* Call the routine to format and send the TST PDU */
            if (EcfmTstTxXmitTstPdu (pMepInfo) != ECFM_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtClntTstInitiator: "
                               "TST PDU cannot be transmitted\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_LBLT_TST_INTERVAL_EXPIRY:

            /* Call the routine to format and send the TST PDU */
            if (EcfmTstTxXmitTstPdu (pMepInfo) != ECFM_SUCCESS)

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtClntTstInitiator: "
                               "TST PDU cannot be transmitted\r\n");
                break;
            }
            u4RetVal = ECFM_SUCCESS;
            break;
        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtClntLbInitiator: "
                           "Event not SUPPORTED!\r\n");
    }
    if (u4RetVal == ECFM_FAILURE)

    {

        /* Update Result OK to False */
        pTstInfo->b1TstResultOk = ECFM_FALSE;

        /* Stop the running TST Transaction */
        EcfmTstTxStopXmitTstPdu (pMepInfo);
        return ECFM_FAILURE;
    }

    else

    {

        /* Update Result OK to True */
        pTstInfo->b1TstResultOk = ECFM_TRUE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmTstTxXmitTstPdu
 *
 * Description        : This routine formats and transmits the TST PDUs.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmTstTxXmitTstPdu (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4ThMsgPerExpiry = ECFM_VAL_1;
    UINT4               u4TstInterval = ECFM_INIT_VAL;
    UINT4               u4Index = ECFM_INIT_VAL;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1               u1DropEligible = ECFM_FALSE;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pMepInfo);

    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);
    if (pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY)
    {
        u4ThMsgPerExpiry = pThInfo->u4TxThPps / ECFM_NUM_OF_TIME_UNITS_IN_A_SEC;
        if (u4ThMsgPerExpiry < ECFM_VAL_1)
        {
            u4ThMsgPerExpiry = ECFM_VAL_1;
        }
    }

    for (u4Index = 0; u4Index < u4ThMsgPerExpiry; u4Index++)
    {
        if (ECFM_LBLT_IS_TST_DISABLED (pMepInfo))
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmTstTxXmitTstPdu: "
                           "TST PDU cannot be send as Tst capability is "
                           "disabled\r\n");
            return ECFM_FAILURE;
        }
        /* Y.1731 : Check the Number of TSTs to send, if it is zero stop the
         * transmission */
        if (pTstInfo->u4TstMessages == ECFM_INIT_VAL)

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmTstTxXmitTstPdu: "
                           "Stopping TST Transaction as total number of TST(s) "
                           "are sent\r\n");
            EcfmTstTxStopXmitTstPdu (pMepInfo);

            /* Sync the stop event to the standby node */
            EcfmRedSyncTransactionStopEvent (ECFM_RED_TST_STOP_TX,
                                             pMepInfo->u4MdIndex,
                                             pMepInfo->u4MaIndex,
                                             pMepInfo->u2MepId);
            return ECFM_SUCCESS;
        }

        /* Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_TST_PDU_SIZE, ECFM_INIT_VAL);
        if (pBuf == NULL)

        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmTstTxXmitTstPdu: "
                           "Buffer Allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
        ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_TST_PDU_SIZE);
        pu1PduStart = ECFM_LBLT_PDU;
        pu1PduEnd = ECFM_LBLT_PDU;
        pu1EthLlcHdr = ECFM_LBLT_PDU;

        /* Format the  TST PDU header */
        EcfmTstTxFormatTstPduHdr (pMepInfo, &pu1PduEnd);

        /* Fill in the TST PDU info like Sequence Number and other optional
         * tlvs */
        /* Routine called to format the TST PDU */
        EcfmTstTxPutTstInfo (pTstInfo, &pu1PduEnd);
        u2PduLength = (UINT2) (pu1PduEnd - pu1PduStart);

        /* Copying PDU over CRU buffer */
        if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                    (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmTstTxXmitTstPdu: "
                           "Copying into Buffer failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
        pu1PduStart = pu1EthLlcHdr;

        /* Format the Ethernet and the LLC header */
        EcfmFormatLbLtTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr,
                                     u2PduLength, ECFM_OPCODE_TST);
        /* Prepend the Ethernet and the LLC header in the CRU buffer */
        u2PduLength = (UINT2) (pu1EthLlcHdr - pu1PduStart);
        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart,
                                  (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmTstTxXmitTstPdu: "
                           "Prepending into Buffer failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
        /* Y.1731 : Drop eligibility of Multicast TST is always marked as FALSE */
        if (pTstInfo->u1TxTstDestType != ECFM_TX_DEST_TYPE_MULTICAST)

        {
            u1DropEligible = pTstInfo->b1TstDropEligible;
        }
        /* If Number of TSTs to send is configured then decrement it after
         * transmitting one TST PDU */
        if (ECFM_GET_INFINITE_TX_STATUS (pTstInfo->u4TstMessages) == ECFM_FALSE)

        {
            ECFM_LBLT_DECR_TRANSMIT_TST_MSG (pMepInfo);
        }
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        /* For CLI TST */
        EcfmSendTstEventToCli (pMepInfo, CLI_EV_ECFM_TST_TRANS_SEND,
                               (UINT2) u4PduSize);
        ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                            "EcfmTstTxXmitTstPdu: "
                            "Sending out CFM PDU to lower layer...\r\n");

        if (EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                       pMepInfo->u4PrimaryVidIsid,
                                       pTstInfo->u1TstVlanPriority,
                                       u1DropEligible, pMepInfo->u1Direction,
                                       ECFM_OPCODE_TST, NULL,
                                       NULL) != ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmTstTxXmitTstPdu: "
                           "Transmit CFM PDU failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            ECFM_LBLT_INCR_TX_FAILED_COUNT (pMepInfo->pPortInfo->u2PortNum);
            ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pMepInfo->pPortInfo->
                                                u4ContextId);
            return ECFM_FAILURE;
        }
        ECFM_LBLT_INCR_TST_SEQ_NUM (pTstInfo);
        ECFM_LBLT_INCR_TST_OUT (pTstInfo);
    }

    /* Start Timer TST Interval for the configured value */
    if (pTstInfo->u1TxTstIntervalType == ECFM_LBLT_TST_INTERVAL_MSEC)

    {
        u4TstInterval = (UINT4) (pTstInfo->u4TstInterval);
    }
    if (pTstInfo->u1TxTstIntervalType == ECFM_LBLT_TST_INTERVAL_SEC)

    {
        u4TstInterval =
            (UINT4) (ECFM_NUM_OF_MSEC_IN_A_SEC * pTstInfo->u4TstInterval);
    }

    if (pTstInfo->u1TxTstIntervalType == ECFM_LBLT_TST_INTERVAL_USEC)

    {
        u4TstInterval =
            (UINT4) (pTstInfo->u4TstInterval / ECFM_NUM_OF_USEC_IN_A_MSEC);
    }

    /* Start the Timer with u4TstInterval milliseconds */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_TST_INTERVAL, pMepInfo, u4TstInterval) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmTstTxXmitTstPdu: "
                       "TSTWhile Timer has not started\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Increment the Sequence Number for the next TST */
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmTstTxFormatTstPduHdr
 *
 * Description        : This rotuine is used to fill the CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1TstPdu - Pointer to the pointer of the Tst Pdu     
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmTstTxFormatTstPduHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1TstPdu)
{
    UINT1              *pu1Pdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *ppu1TstPdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1Pdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_OPCODE_TST);

    /* Fill the Next 1 byte with Flag In TST, Flag Field is set to ZERO */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_INIT_VAL);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TST_FIRST_TLV_OFFSET);
    *ppu1TstPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmTstTxPutTstInfo
 *
 * Description        : This routine is used to fill the Sequence number and
 *                      TLVs required to be send in the TST PDU.
 *                        
 * Input(s)           : pTstInfo - Pointer to the Pdu Info structure that stores
 *                      the information regarding TST info.
 * 
 * Output(s)          : ppu1TstPdu - Pointer to the pointer of TST Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmTstTxPutTstInfo (tEcfmLbLtTstInfo * pTstInfo, UINT1 **ppu1TstPdu)
{
    UINT1              *pu1Pdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *ppu1TstPdu;

    /* Fill the Next 4 bytes with Sequence Number */
    ECFM_PUT_4BYTE (pu1Pdu, pTstInfo->u4TstSeqNumber);

    /* Y.1731 : Check the u1TstPatternType, send the TLV
     * accordingly. */
    switch (pTstInfo->u1TstPatternType)

    {
        case ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITHOUT_CRC:
        case ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC:
        case ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC:
        case ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC:
            EcfmTstTxPutTestTlv (pTstInfo, &pu1Pdu);
            break;
        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmTstTxPutTstInfo: "
                           "TLV type not SUPPORTED!\r\n");
    }

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_END_TLV_TYPE);
    *ppu1TstPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmTstTxPutTestTlv
 *
 * Description        : This routine is used to fill the Test TLV 
 *                      to be send in the TST PDU.
 *                        
 * Input(s)           : pTstInfo - Pointer to the Pdu Info structure
 *                      that stores the information regarding TST info.
 *
 * Output(s)          : ppu1TstPdu - pointer to the pointer of Tst Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmTstTxPutTestTlv (tEcfmLbLtTstInfo * pTstInfo, UINT1 **ppu1TstPdu)
{
    UINT1              *pu1Pdu = NULL;
    UINT1              *pu1TlvStart = NULL;
    UINT4               u4Crc32val = ECFM_INIT_VAL;
    UINT2               u2TstDataSize = ECFM_INIT_VAL;
    UINT2               u2TlvLen = ECFM_INIT_VAL;
    UINT1               au1RandBytes[ECFM_LBLT_PRBS_SIZE];
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *ppu1TstPdu;
    pu1TlvStart = *ppu1TstPdu;
    ECFM_MEMSET (au1RandBytes, ECFM_INIT_VAL, ECFM_LBLT_PRBS_SIZE);

   /*----------------------TEST TLV---------------------------*/
    /* Type(1) | Length(2) | Pat-Type(1) | Value (N) | CRC (4) */
   /*---------------------------------------------------------*/
    /* "Length" field contains size of the data in "Value + CRC" 
     * fields */

    /* Put 1 byte as the Test TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TEST_TLV_TYPE);

    /* If variable bytes to send is ON then send PDU of varying size,
     * else send the data of the configured size */
    if (pTstInfo->b1TstVariableBytes == ECFM_TRUE)

    {

        /* If it is the first PDU or the last PDU is of 9K, 
         * then add data to make TST PDU of 64 bytes, else
         * send TST PDU of 128, 256, 512, 1024, 2048, 4096, 
         * 8192, 9000 bytes*/
        ECFM_LBLT_GET_TST_PATTERN_TO_SEND_SIZE (pTstInfo->
                                                u2TstLastPatternSize,
                                                u2TstDataSize);
        pTstInfo->u2TstLastPatternSize = u2TstDataSize;
    }

    else

    {
        if (pTstInfo->u2TstPatternSize != 0)

        {
            u2TstDataSize = pTstInfo->u2TstPatternSize;
        }

        else

        {

            /* Default size to send in TLV */
            u2TstDataSize = ECFM_LBM_TEST_PATTERN_SIZE_MIN;
        }
    }

    /* Put the next 2 byte as the length of the TLV */
    /* As per the TLV defintion Length includes Test Pattern Type with Test Pattern and CRC-32 */
    ECFM_PUT_2BYTE (pu1Pdu, (u2TstDataSize + ECFM_TST_PATTERN_TYPE_FIELD_SIZE));

    /* Put the next 1 byte as the Test TLV Pattern Type */
    ECFM_PUT_1BYTE (pu1Pdu, pTstInfo->u1TstPatternType);
    u2TlvLen =
        (UINT2) (ECFM_TLV_TYPE_FIELD_SIZE + ECFM_TLV_LENGTH_FIELD_SIZE +
                 ECFM_TST_PATTERN_TYPE_FIELD_SIZE + (u2TstDataSize -
                                                     ECFM_LBLT_TEST_TLV_CRC_SIZE));

    /* Put the next u2TstDataSize bytes as the value part of the TLV. */
    switch (pTstInfo->u1TstPatternType)

    {
        case ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITHOUT_CRC:
            ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, u2TstDataSize);
            pu1Pdu = pu1Pdu + u2TstDataSize;
            *ppu1TstPdu = pu1Pdu;
            break;
        case ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC:

            /* If Pattern size is less than 4 bytes then donot append any tlv */
            if (u2TstDataSize > ECFM_LBLT_TEST_TLV_CRC_SIZE)

            {

                /* Pattern to send is of the size provided by the user minus 
                 * crc field size */
                ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL,
                             u2TstDataSize - ECFM_LBLT_TEST_TLV_CRC_SIZE);
                pu1Pdu = pu1Pdu + (u2TstDataSize - ECFM_LBLT_TEST_TLV_CRC_SIZE);
                u4Crc32val = EcfmLbLtUtilCalculateCrc32 (pu1TlvStart, u2TlvLen);
                ECFM_PUT_4BYTE (pu1Pdu, u4Crc32val);
                *ppu1TstPdu = pu1Pdu;
                break;
            }
            *ppu1TstPdu = pu1TlvStart;
            break;
        case ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC:
            EcfmGenerateRandPseudoBytes (au1RandBytes);
            EcfmTstTxAdjustPattern (au1RandBytes, ECFM_LBLT_PRBS_SIZE,
                                    u2TstDataSize, &pu1Pdu);
            pu1Pdu = pu1Pdu + u2TstDataSize;
            *ppu1TstPdu = pu1Pdu;
            break;
        case ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC:

            /* If Pattern size is less than 4 bytes then donot append any tlv */
            if (u2TstDataSize > ECFM_LBLT_TEST_TLV_CRC_SIZE)

            {
                EcfmGenerateRandPseudoBytes (au1RandBytes);

                /* Pattern to send is of the size provided by the user minus crc
                 * field size */
                EcfmTstTxAdjustPattern (au1RandBytes, ECFM_LBLT_PRBS_SIZE,
                                        u2TstDataSize -
                                        ECFM_LBLT_TEST_TLV_CRC_SIZE, &pu1Pdu);
                pu1Pdu = pu1Pdu + (u2TstDataSize - ECFM_LBLT_TEST_TLV_CRC_SIZE);
                u4Crc32val = EcfmLbLtUtilCalculateCrc32 (pu1TlvStart, u2TlvLen);
                ECFM_PUT_4BYTE (pu1Pdu, u4Crc32val);
                *ppu1TstPdu = pu1Pdu;
                break;
            }
            *ppu1TstPdu = pu1TlvStart;
            break;
        default:
            *ppu1TstPdu = pu1TlvStart;
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbiTxPutTestTlv: "
                           "Pattern type not SUPPORTED!\r\n");
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmTstTxStopXmitTstPdu
 *
 * Description        : This routine is used to stop the TST PDU transmission
 *                      when either deadline timer has expired or when numbers 
 *                      of PDUs to send are transmitted or when any interrupt 
 *                      like CTRL-C from CLI or Stop from SNMP is received.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmTstTxStopXmitTstPdu (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    tEcfmLbLtPduSmInfo  PduSmInfo;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pMepInfo);
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);
    pTstInfo->u1TstStatus = ECFM_TX_STATUS_READY;
    ECFM_CLEAR_INFINITE_TX_STATUS (pTstInfo->u4TstMessages);

    PduSmInfo.pMepInfo = pMepInfo;
    /* Stop Timer TSTwhile if it is activated */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_TST_INTERVAL, pMepInfo);

    /* Y.1731 : Stop Timer TSTDeadLine if it is activated */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_TST_DEADLINE, pMepInfo);

    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmTstTxStopXmitTstPdu: "
                   "TST PDU Transmission is stopped\r\n");

    /* For CLI TST */
    if (pTstInfo->b1TstResultOk == ECFM_FALSE)

    {
        EcfmSendTstEventToCli (pMepInfo, CLI_EV_ECFM_TST_TRANS_ERROR,
                               ECFM_INIT_VAL);
    }
    EcfmSendTstEventToCli (pMepInfo, CLI_EV_ECFM_TST_TRANS_STOP, ECFM_INIT_VAL);

    /*Call the Throughput Initiator when TST burst Complete */
    if (pThInfo->b1TxThResultOk == ECFM_TRUE)
    {
        EcfmLbLtClntThInitiator (&(PduSmInfo), ECFM_EV_TST_BURST_COMPLETE);
    }

    pTstInfo->CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
#ifdef NPAPI_WANTED
    if ((ECFM_HW_TST_SUPPORT () == ECFM_TRUE) &&
        (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE))
    {
        tEcfmMepInfoParams  EcfmMepInfoParams;
        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                     sizeof (tEcfmMepInfoParams));
        EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        EcfmMepInfoParams.u4IfIndex =
            ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
        EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

        EcfmFsMiEcfmStopTstTransaction (&EcfmMepInfoParams);
        return;
    }
#endif /*  */

    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmTstTxAdjustPattern
 *
 * Description        : This routine is used to adjust the Test pattern till the
 *                      given size.
 *                        
 * Input(s)           : pu1Pattern    - Pattern to be adjusted.
 *                      u2PatternLen -  Pattern length
 *                      u2PatternSize - Size till which pattern is repeated.
 *
 * Output(s)          : ppu1TstPdu - Pointer to the pointer of Tst PDU. 
 *
 * Return Value(s)    : None.
 ******************************************************************************/
PRIVATE VOID
EcfmTstTxAdjustPattern (UINT1 *pu1Pattern, UINT2 u2PatternLen,
                        UINT2 u2PatternSize, UINT1 **ppu1TstPdu)
{
    UINT1              *pu1Pdu = NULL;
    UINT2               u2PatternNewSize = ECFM_INIT_VAL;
    UINT2               u2SizeCounter = ECFM_INIT_VAL;
    pu1Pdu = *ppu1TstPdu;
    if (u2PatternLen == ECFM_INIT_VAL)

    {
        ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, u2PatternSize);
        *ppu1TstPdu = pu1Pdu;
        return;
    }
    while (u2PatternNewSize < u2PatternSize)

    {
        for (u2SizeCounter = 0; u2SizeCounter < u2PatternLen; u2SizeCounter++)

        {
            pu1Pdu[u2PatternNewSize] = pu1Pattern[u2SizeCounter];
            u2PatternNewSize = u2PatternNewSize + ECFM_INCR_VAL;
            if (u2PatternNewSize == u2PatternSize)

            {
                break;
            }
        }
    }
    *ppu1TstPdu = pu1Pdu;
    return;
}

/*******************************************************************************
 * Function Name      : EcfmSendTstEventToCli
 *
 * Description        : This routine will be called on the following events
 *                      by the TST Initiator
 *                        - At the entry of TST transaction
 *                        - At the exit of TST transaction
 *                        - Error occurred while sending TST
 *                        
 * Input(s)           :  pMepInfo - Info of the MEP which has initiated the
 *                       TST.
 *                       i4EventType - Event type
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : None.
 ******************************************************************************/
PRIVATE VOID
EcfmSendTstEventToCli (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventType,
                       UINT2 u2PduSize)
{
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtCliEvInfo *pCliEvent = NULL;
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pMepInfo);
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pThInfo);
    pCliEvent = ECFM_LBLT_CLI_EVENT_INFO (pTstInfo->CurrentCliHandle);
    if (pCliEvent == NULL)

    {
        return;
    }
    switch (u1EventType)

    {
        case CLI_EV_ECFM_TST_TRANS_SEND:
            pCliEvent->u1Events = CLI_EV_ECFM_TST_TRANS_SEND;
            pCliEvent->Msg.u4Msg1 = u2PduSize;
            pCliEvent->Msg.u4Msg2 = pTstInfo->u4TstSeqNumber;
            break;
        case CLI_EV_ECFM_TST_TRANS_STOP:
            pCliEvent->u1Events = CLI_EV_ECFM_TST_TRANS_STOP;
            break;
        case CLI_EV_ECFM_TST_TRANS_ERROR:
            pCliEvent->u1Events = CLI_EV_ECFM_TST_TRANS_ERROR;
            break;
        default:
            pCliEvent->u1Events = CLI_EV_ECFM_TST_TRANS_ERROR;
            break;
    }
    ECFM_GIVE_SEMAPHORE (pCliEvent->SyncSemId);
    return;
}

/******************************************************************************/
/*                           End  of cfmtstin.c                               */
/******************************************************************************/
