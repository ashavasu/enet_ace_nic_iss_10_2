/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmcctsk.c,v 1.51 2015/09/04 07:03:47 siva Exp $
 *
 * Description: This file contains the functionality of the routine
 *                        which is the entry point to the CC Task
 *******************************************************************/

#include "cfminc.h"
/* Prototypes for the routines used in this file */
PRIVATE INT4 EcfmCcTaskInit PROTO ((VOID));
PRIVATE VOID EcfmCcHandleTaskInitFailure PROTO ((VOID));
PRIVATE INT4 EcfmCcStackTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcConfigErrTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcMipGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcMipPreventGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcPortTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcOffRMepDbTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcOffMepTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcHwMepTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcHwRMepTableGlobalCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE VOID EcfmCcProcessIntQueOverFlow PROTO ((VOID));

/******************************************************************************
 * Function Name      : EcfmCcMain
 *
 * Description        : This is the entry routine for the ECFM. It is 
 *                      continuosly waiting for the events, which on reception 
 *                      calls CC Tasks Pakcet/Configuration message Queue       
 *                      handler or Timer Expiry Handler based on the event      
 *                      received
 *
 * Input(s)           : pArg - pointer to the Arguments
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
VOID
EcfmCcMain (INT1 *pArg)
{
    UINT4               u4Event = ECFM_INIT_VAL;
    UNUSED_PARAM (pArg);

    /*Initilize the CC Task */
    if (EcfmCcTaskInit () != ECFM_SUCCESS)

    {
        EcfmCcHandleTaskInitFailure ();

        /* Indicate the status to the main module lrInitComplete */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /*Initlize the LBLT Task */
    if (EcfmLbLtTaskInit () != ECFM_SUCCESS)

    {
        EcfmCcHandleTaskInitFailure ();
        EcfmLbLtHandleTaskInitFailure ();

        /* Indicate the status to the main module lrInitComplete */
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    if (EcfmHandleCcAndLbLtStartModule () != ECFM_SUCCESS)

    {
        EcfmLbLtHandleTaskInitFailure ();
        EcfmCcHandleTaskInitFailure ();
        EcfmRedDeRegisterWithRM ();

        /* Indicate the Module Init Status to LR */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Register Rx Call back for Offloading */
    EcfmCcmOffHwRegisterCallBack (ECFM_DEFAULT_CONTEXT);
    /*  Register with the MPLS Module to get
     *      * Indication about the UP/DOWN event */
    if (EcfmMplsRegCallBack () != ECFM_SUCCESS)
    {
        return;
    }
#ifdef SNMP_2_WANTED
    /* Register Protocol MI MIB with SNMP */
    RegisterFSCFMM ();
    /* Register Y.1731 Protocol MI MIB with SNMP */
    RegisterFSMIY1 ();

    RegisterFSCFME ();

    /* Register Protocol IEEE 802.1AP Standard MIB with SNMP */
    RegisterCFMV2 ();

    RegisterCFMV2E ();

    /* Register SI MIB if SI Mode is enabled */
    if (EcfmVcmGetSystemModeExt (ECFM_PROTOCOL_ID) == VCM_SI_MODE)

    {
        /* Register Protocol Proprietary MIB with SNMP */
        RegisterFSECFM ();
    }

#endif /*  */
    /* Get the hardware capabilities */
#ifdef NPAPI_WANTED
    if (EcfmFsMiEcfmHwGetCapability (ECFM_DEFAULT_CONTEXT, &ECFM_HW_CAPABILITY)
        != FNP_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }
#endif
    /* Indicate the status to the main module lrInitComplete */
    lrInitComplete (OSIX_SUCCESS);
    while (1)

    {
        if (ECFM_RECEIVE_EVENT
            (ECFM_CC_TASK_ID, ECFM_EV_ALL, OSIX_WAIT, &u4Event) == ECFM_SUCCESS)
        {
            EcfmCcProcessQueue (u4Event, ECFM_TRUE);
        }
    }
}

/******************************************************************************
 * Function Name      : EcfmCcProcessQueue
 *
 * Description        : This is the routine used for processing the events, 
 *                      which on reception 
 *                      calls CC Tasks Pakcet/Configuration message Queue       
 *                      handler or Timer Expiry Handler based on the event      
 *                      received
 *
 * Input(s)           : u4Event - Event to be processed
 *                      b1CcLock - CC Lock to be taken or not 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
PUBLIC VOID
EcfmCcProcessQueue (UINT4 u4Event, BOOL1 b1CcLock)
{

    tEcfmCcMsg         *pQMsg = NULL;
    INT4                i4StartTime = 0;

    /* Handle Callback Interrupt PDUs received in the Interrupt Queue for CC Task */
    if (u4Event & ECFM_EV_INT_PDU_IN_QUEUE)

    {
        /* Call Interrupt Queue Handler for CC Task for handling the
         * received Callback Interrupt PDUs  */
        /* Event received, dequeue messages for processing */
        ECFM_GET_SYS_TIME ((UINT4 *) &i4StartTime);
        while (ECFM_DEQUE_MSG
               (ECFM_CC_INT_QUEUE_ID, (UINT1 *) &pQMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {

            /* Mutual exclusion flag ON */
            if (b1CcLock == ECFM_TRUE)
            {
                ECFM_CC_LOCK ();
            }

            EcfmCcInterruptQueueHandler (pQMsg);

            /* Release Context */
            ECFM_CC_RELEASE_CONTEXT ();

            /* Release the buffer to pool */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pQMsg);
            pQMsg = NULL;

            /* Mutual exclusion flag OFF */
            if (b1CcLock == ECFM_TRUE)
            {
                ECFM_CC_UNLOCK ();
            }

            ECFM_CC_RELINQUISH_QUEUE (i4StartTime,
                                      ECFM_CC_INT_RELINQUISH_THRESH,
                                      ECFM_EV_INT_PDU_IN_QUEUE,
                                      ECFM_CC_INT_QUEUE_ID);

        }
    }

    if (u4Event & ECFM_EV_INT_QUEUE_OVERFLOW)
    {
        if (b1CcLock == ECFM_TRUE)
        {
            ECFM_CC_LOCK ();
        }
        EcfmCcProcessIntQueOverFlow ();

        if (b1CcLock == ECFM_TRUE)
        {
            ECFM_CC_UNLOCK ();
        }
    }

    /* Handle CFM PDUs received in the Packet Queue for CC Task */
    if (u4Event & ECFM_EV_CFM_PDU_IN_QUEUE)

    {
        ECFM_GET_SYS_TIME ((UINT4 *) &i4StartTime);
        /* Call Packet Queue Handler for CC Task for handling the
         * received CFM PUD */
        /* Event received, dequeue messages for processing */
        while (ECFM_DEQUE_MSG
               (ECFM_CC_PKT_QUEUE_ID, (UINT1 *) &pQMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {

            /* Mutual exclusion flag ON */
            if (b1CcLock == ECFM_TRUE)
            {
                ECFM_CC_LOCK ();
            }

            EcfmCcPktQueueHandler (pQMsg);

            /* Release Context */
            ECFM_CC_RELEASE_CONTEXT ();

            /* Release the buffer to pool */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pQMsg);
            pQMsg = NULL;

            /* Mutual exclusion flag OFF */
            if (b1CcLock == ECFM_TRUE)
            {
                ECFM_CC_UNLOCK ();
            }

            ECFM_CC_RELINQUISH_QUEUE (i4StartTime,
                                      ECFM_CC_PKT_RELINQUISH_THRESH,
                                      ECFM_EV_CFM_PDU_IN_QUEUE,
                                      ECFM_CC_PKT_QUEUE_ID);

        }
    }

    /* Handle Timer Expiry Event received on CC Task */
    if (u4Event & ECFM_EV_TMR_EXP)

    {
        /* Mutual exclusion flag ON */
        if (b1CcLock == ECFM_TRUE)
        {
            ECFM_CC_LOCK ();
        }

        /* Call CC Task's Timer Expiry Handler */
        EcfmCcTmrExpHandler ();

        /* Mutual exclusion flag OFF */
        if (b1CcLock == ECFM_TRUE)
        {
            ECFM_CC_UNLOCK ();
        }

    }

    /* Handle Configuration Messages received in the Configuration Queue
     * for CC Task */
    if (u4Event & ECFM_EV_CFG_MSG_IN_QUEUE)

    {
        ECFM_GET_SYS_TIME ((UINT4 *) &i4StartTime);
        /* Call Configuration Queue Handler for handling Configuration
         * messages for CC Task */
        while (ECFM_DEQUE_MSG
               (ECFM_CC_CFG_QUEUE_ID, (UINT1 *) &pQMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)
        {
            /* Mutual exclusion flag ON */
            if (b1CcLock == ECFM_TRUE)
            {
                ECFM_CC_LOCK ();
            }

            EcfmCcCfgQueueHandler (pQMsg);

            /* Release Context */
            ECFM_CC_RELEASE_CONTEXT ();

            /* Release the buffer to pool */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pQMsg);
            pQMsg = NULL;

            /* Mutual exclusion flag OFF */
            if (b1CcLock == ECFM_TRUE)
            {
                ECFM_CC_UNLOCK ();
            }

            ECFM_CC_RELINQUISH_QUEUE (i4StartTime,
                                      ECFM_CC_CFG_RELINQUISH_THRESH,
                                      ECFM_EV_CFG_MSG_IN_QUEUE,
                                      ECFM_CC_CFG_QUEUE_ID);

        }
    }

#ifdef L2RED_WANTED
    /* Handle Bulk Request sent by Standby Node to Active Node */
    if (u4Event & ECFM_EV_RED_BULK_UPD)

    {
        EcfmRedHandleBulkUpdateEvent ();
    }
#endif /*  */
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcTaskInit
 *
 *    DESCRIPTION      : This function creates the task queue, allocates
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *     OUTPUT          : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcTaskInit ()
{
    tEcfmMacAddr        SwitchMacAddr;

    /* The structure tCfmCcPortArray is declared here locally to ensure that 
     * the sizing parameter structure is visible in debugger. 
     * This will be removed in the next subsequent changes in the sizing parameters. */
    tCfmCcPortArray    *pCfmCcPortPtr = NULL;
    UNUSED_PARAM (pCfmCcPortPtr);

    /* reset CC global info to all zeros */
    ECFM_MEMSET (&gEcfmCcGlobalInfo, ECFM_INIT_VAL, ECFM_CC_GLOBAL_INFO_SIZE);
    EcfmCfaGetSysMacAddress (SwitchMacAddr);
    ECFM_CHASSISID_LEN = ECFM_MAC_ADDR_LENGTH;
    ECFM_MEMCPY (ECFM_CHASSISID, SwitchMacAddr, ECFM_CHASSISID_LEN);
    ECFM_CHASSISID_SUBTYPE = ECFM_CHASSISID_SUB_MAC_ADDR;
    ECFM_MGMT_ADDR_DOMAIN_CODE = 0;
    ECFM_IPv4_L3IFINDEX = -1;
    ECFM_IPv6_L3IFINDEX = -1;
    ECFM_MGMT_ADDRESS_LEN = 0;
    ECFM_MEMSET (ECFM_MGMT_ADDRESS, 0x00, ECFM_MAX_MAN_ADDR_LEN);
    ECFM_RM_GET_NUM_PEERS_UP ();
    ECFM_SELF_TASK_ID (&ECFM_CC_TASK_ID);

    ECFM_GLB_TRC_OPTION = ECFM_FALSE;

    /* Protocol semaphore - created with initial value 0. */
    if (ECFM_CREATE_SEMAPHORE ((UINT1 *) ECFM_CC_SEM_NAME, &ECFM_CC_SEM_ID)
        != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem is 
     * called before using the semaphore. */
    ECFM_GIVE_SEMAPHORE (ECFM_CC_SEM_ID);

    /* Creating Packet Message Queue for ECFM Messages */
    if (ECFM_CREATE_MSG_QUEUE
        ((UINT1 *) ECFM_CC_PKT_QUEUE_NAME, ECFM_DEF_MSG_LEN,
         ECFM_CC_PKT_QUEUE_DEPTH, &ECFM_CC_PKT_QUEUE_ID) != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }

    /* Creating Configuration Message queue for configuration messages */
    if (ECFM_CREATE_MSG_QUEUE
        ((UINT1 *) ECFM_CC_CFG_QUEUE_NAME, ECFM_DEF_MSG_LEN,
         ECFM_CC_CFG_QUEUE_DEPTH, &ECFM_CC_CFG_QUEUE_ID) != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }
    /* Creating Callback Message queue for Interrupt messages */
    if (ECFM_CREATE_MSG_QUEUE
        ((UINT1 *) ECFM_CC_INT_QUEUE_NAME, ECFM_DEF_MSG_LEN,
         ECFM_CC_INT_QUEUE_DEPTH, &ECFM_CC_INT_QUEUE_ID) != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmCcHandleTaskInitFailure
 *
 *    DESCRIPTION      : Function called when the initialization of ECFM's CC
 *                       Task fails at any point during initialization.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
EcfmCcHandleTaskInitFailure ()
{
    tEcfmCcMsg         *pMsg = NULL;

    /* Clearing Packet Queue for CC Task */
    if (ECFM_CC_PKT_QUEUE_ID != 0)

    {

        /* Dequeue CFM PDU messages from the Packet Queue */
        while (ECFM_DEQUE_MSG
               (ECFM_CC_PKT_QUEUE_ID, (UINT1 *) &pMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {
            if (pMsg == NULL)

            {
                continue;
            }
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        }
        /* Delete Cfm Packet Queue */
        ECFM_DELETE_MSG_QUEUE (ECFM_CC_PKT_QUEUE_ID);
        ECFM_CC_PKT_QUEUE_ID = ECFM_INIT_VAL;
    }

    /* Clearing Config Queue for CC Task */
    if (ECFM_CC_CFG_QUEUE_ID != 0)

    {

        /* Dequeue Configuration messages from the Config Queue */
        while (ECFM_DEQUE_MSG
               (ECFM_CC_CFG_QUEUE_ID, (UINT1 *) &pMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {
            if (pMsg == NULL)

            {
                continue;
            }
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        }
        /* Delete Queue */
        ECFM_DELETE_MSG_QUEUE (ECFM_CC_CFG_QUEUE_ID);
        ECFM_CC_CFG_QUEUE_ID = ECFM_INIT_VAL;
    }

    /* Clearing Interrupt Queue for CC Task */
    if (ECFM_CC_INT_QUEUE_ID != 0)

    {

        /* Dequeue Call back messages from the InterruptQueue */
        while (ECFM_DEQUE_MSG
               (ECFM_CC_INT_QUEUE_ID, (UINT1 *) &pMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {
            if (pMsg == NULL)

            {
                continue;
            }
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        }
        /* Delete Queue */
        ECFM_DELETE_MSG_QUEUE (ECFM_CC_INT_QUEUE_ID);
        ECFM_CC_INT_QUEUE_ID = ECFM_INIT_VAL;
    }

    /* DeInitialize Global Info */
    EcfmCcDeInitGlobalInfo ();

    /* Delete TImer List and Timer MemPool for CC Task */
    EcfmCcTmrDeInit ();

    /* Delete Semaphore */
    if (ECFM_CC_SEM_ID != 0)

    {
        ECFM_DELETE_SEMAPHORE (ECFM_CC_SEM_ID);
        ECFM_CC_SEM_ID = ECFM_INIT_VAL;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcInitGlobalInfo 
 *
 *    DESCRIPTION      : This function initalizes global structure, sets the
 *                       Trace option, creates all the RBTrees and set the 
 *                       current context to DEFAULT.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcInitGlobalInfo ()
{
    INT4                i4RetVal = ECFM_FAILURE;
    UINT4               u4Context = ECFM_INIT_VAL;
    tMacAddr            MacAddr;
    ECFM_MEMSET (&MacAddr, 0, ECFM_MAC_ADDR_LENGTH);

    for (u4Context = 0; u4Context < ECFM_MAX_CONTEXTS; u4Context++)

    {
        ECFM_SET_SYSTEM_STATUS (u4Context, ECFM_SHUTDOWN);
        ECFM_SET_MODULE_STATUS (u4Context, ECFM_DISABLE);
        ECFM_SET_Y1731_STATUS (u4Context, ECFM_DISABLE);

        /* Set Trace Option */
        ECFM_TRACE_OPTION_FOR_CONTEXT (u4Context) = ECFM_CRITICAL_TRC;
    }

    /* Creating Mempool for Active MA Levels */
    if (ECFM_CREATE_MEM_POOL
        (ECFM_CC_ACTIVE_MA_LEVEL_SIZE, 1,
         MEM_DEFAULT_MEMORY_TYPE,
         &ECFM_CC_ACTIVE_MA_LEVEL_POOL) == ECFM_MEM_FAILURE)
    {
        return ECFM_FAILURE;
    }

    /* Creating Mempool for Active MD Levels */
    if (ECFM_CREATE_MEM_POOL
        (ECFM_CC_ACTIVE_MD_LEVEL_SIZE, 1,
         MEM_DEFAULT_MEMORY_TYPE,
         &ECFM_CC_ACTIVE_MD_LEVEL_POOL) == ECFM_MEM_FAILURE)
    {
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the ContextInfo */
    if (ECFM_ALLOC_MEM_BLOCK_CC_CONTEXT (ECFM_CC_CURR_CONTEXT_INFO ()) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Get Current Context */
    ECFM_CC_GET_CONTEXT_INFO (ECFM_DEFAULT_CONTEXT) =
        ECFM_CC_CURR_CONTEXT_INFO ();
    ECFM_MEMSET (ECFM_CC_CURR_CONTEXT_INFO (), 0, ECFM_CC_CONTEXT_INFO_SIZE);
    ECFM_CC_CURR_CONTEXT_ID () = ECFM_DEFAULT_CONTEXT;

    /* Assigning Memory to CCM global Varaible */
    ECFM_CC_PDU = gau1EcfmCCPdu;
    /* Create Global RB-Trees */
    /* Global Port Table */
    ECFM_CC_GLOBAL_PORT_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcPortInfo, PortTableGlobalNode),
                                  EcfmCcPortTableGlobalCmp);
    if (ECFM_CC_GLOBAL_PORT_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree "
                      "for port-info \r\n");
        return ECFM_FAILURE;
    }
    /*Create Global CC LocalPort Table */
    ECFM_CC_CREATE_LOCAL_PORT_TABLE (i4RetVal);
    if (i4RetVal == ECFM_FAILURE)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree "
                      "for CC Local port-info \r\n");
        return ECFM_FAILURE;
    }

    /*Create Global LBLT LocalPort Table */
    ECFM_LBLT_CREATE_LOCAL_PORT_TABLE (i4RetVal);
    if (i4RetVal == ECFM_FAILURE)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree "
                      "for LBLT Local port-info \r\n");
        return ECFM_FAILURE;
    }

    /* Global Stack Table */
    ECFM_CC_GLOBAL_STACK_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcStackInfo, StackTableGlobalNode),
                                  EcfmCcStackTableGlobalCmp);
    if (ECFM_CC_GLOBAL_STACK_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree "
                      "for stack table \r\n");
        return ECFM_FAILURE;
    }

    /* Global Configuration Error List Table */
    ECFM_CC_GLOBAL_CONFIG_ERR_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcConfigErrInfo,
                                   ConfigErrTableGlobalNode),
                                  EcfmCcConfigErrTableGlobalCmp);
    if (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree"
                      "for configuration error" " table \r\n");
        return ECFM_FAILURE;
    }

    /* Global MIP  Table */
    ECFM_CC_GLOBAL_MIP_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcMipInfo, MipTableGlobalNode),
                                  EcfmCcMipGlobalCmp);
    if (ECFM_CC_GLOBAL_MIP_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree"
                      " for MIP" " table \r\n");
        return ECFM_FAILURE;
    }
    /* Global Automatic MIP  Prevention Table */
    ECFM_CC_GLOBAL_MIP_PREVENT_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tEcfmCcMipPreventInfo, MipPreventTblGblNode),
                              EcfmCcMipPreventGlobalCmp);
    if (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree"
                      "for MIP Previntion" " table \r\n");
        return ECFM_FAILURE;
    }

    /* Global Offload RMepDb Table */
    ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcRMepDbInfo,
                                   OffloadRMepDbNode),
                                  EcfmCcOffRMepDbTableGlobalCmp);
    if (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree "
                      "for Offload RMepDb table \r\n");
        return ECFM_FAILURE;
    }

    /* Global Offload Mep Port Table */
    ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcMepInfo,
                                   OffloadMepNode), EcfmCcOffMepTableGlobalCmp);
    if (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInitGlobalInfo: unable to create global RB-Tree "
                      "for Offload RMepDb Port table \r\n");
        return ECFM_FAILURE;
    }
    /* Global Hardware Mep */
    ECFM_CC_GLOBAL_HW_MEP_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF (tEcfmCcMepInfo,
                                                 HwMepNode),
                                  EcfmCcHwMepTableGlobalCmp);
    if (ECFM_CC_GLOBAL_HW_MEP_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "In Func = %s [%d]:Unable to create global RB-Tree "
                      "for Meptable \r\n", __FUNCTION__, __LINE__);
        return ECFM_FAILURE;
    }

    /* Global Hardware Mep */
    ECFM_CC_GLOBAL_HW_RMEP_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF (tEcfmCcRMepDbInfo,
                                                 HwRMepNode),
                                  EcfmCcHwRMepTableGlobalCmp);
    if (ECFM_CC_GLOBAL_HW_RMEP_TABLE == NULL)
    {
        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (), ECFM_INIT_SHUT_TRC
                      | ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "In Func = %s [%d]:Unable to create global RB-Tree "
                      "for RMeptable \r\n", __FUNCTION__, __LINE__);
        return ECFM_FAILURE;
    }
    if (ECFM_ALLOC_MEM_BLOCK_ACTIVE_MD_LEVEL (gpu1ActiveMdLevels) == NULL)
    {
        gpu1ActiveMdLevels = NULL;
        gppActiveMaNodes = NULL;
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    if (ECFM_ALLOC_MEM_BLOCK_ACTIVE_MA_LEVEL (gppActiveMaNodes) == NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ACTIVE_MD_LEVEL_POOL,
                             (UINT1 *) (gpu1ActiveMdLevels));
        gpu1ActiveMdLevels = NULL;
        gppActiveMaNodes = NULL;
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    /* Allocating memory for CC MEP Info global structure */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MEP_TABLE (gpEcfmCcMepNode) == NULL)
    {
        /* Free the memory of CC context Info */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_CONTEXT_POOL,
                             (UINT1 *) ECFM_CC_CURR_CONTEXT_INFO ());
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ACTIVE_MD_LEVEL_POOL,
                             (UINT1 *) (gpu1ActiveMdLevels));
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ACTIVE_MA_LEVEL_POOL,
                             (UINT1 *) (gppActiveMaNodes));
        gpu1ActiveMdLevels = NULL;
        gppActiveMaNodes = NULL;
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    /* Initialize default OUI as System Mac Address */
    /* Get System MAC Address */
    CfaGetSysMacAddress (MacAddr);

    /* Set OUI for CC Task */
    ECFM_MEMCPY (ECFM_CC_ORG_UNIT_ID, &MacAddr, ECFM_OUI_LENGTH);

    return ECFM_SUCCESS;
}

/******************************************************************************
 *                                                                            *
 *    FUNCTION NAME    : EcfmCcDeInitGlobalInfo                               *
 *                                                                            *
 *    DESCRIPTION      : This function DeInitalizes global structure, deletes *
 *                       all the RBTrees                                      *
 *                                                                            *
 *    INPUT            : None                                                 *
 *                                                                            *
 *    OUTPUT           : None                                                 *
 *                                                                            *
 *    RETURNS          : None                                                 *
 *                                                                            *
 *****************************************************************************/
PUBLIC VOID
EcfmCcDeInitGlobalInfo ()
{

    /* Delete the default context created */
    EcfmCcHandleDeleteContext (ECFM_DEFAULT_CONTEXT);

    if (gpu1ActiveMdLevels != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ACTIVE_MD_LEVEL_POOL,
                             (UINT1 *) (gpu1ActiveMdLevels));
        gpu1ActiveMdLevels = NULL;
    }

    if (gppActiveMaNodes != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ACTIVE_MA_LEVEL_POOL,
                             (UINT1 *) (gppActiveMaNodes));
        gppActiveMaNodes = NULL;
    }

    /* Free the memory of CC MEP Info */
    if (gpEcfmCcMepNode != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL,
                             (UINT1 *) (gpEcfmCcMepNode));
        gpEcfmCcMepNode = NULL;
    }

    /* Delete Global RB-Trees */
    /* Delete Port Table */
    if (ECFM_CC_GLOBAL_PORT_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_PORT_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_PORT_ENTRY);
        ECFM_CC_GLOBAL_PORT_TABLE = NULL;
    }
    /*Delete Global CC LocalPort Table */
    ECFM_CC_DELETE_LOCAL_PORT_TABLE ();

    /*Delete Global LBLT LocalPort Table */
    ECFM_LBLT_DELETE_LOCAL_PORT_TABLE ();
    /* Delete Stack Table */
    if (ECFM_CC_GLOBAL_STACK_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_STACK_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_STACK_ENTRY);
        ECFM_CC_GLOBAL_STACK_TABLE = NULL;
    }
    /* Delete config error table */
    if (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_CONFIG_ERR_ENTRY);
        ECFM_CC_GLOBAL_CONFIG_ERR_TABLE = NULL;
    }
    /* Delete MIP Prevention table */
    if (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_MIP_PREVENT_ENTRY);
        ECFM_CC_GLOBAL_MIP_PREVENT_TABLE = NULL;
    }
    /* Delete MIP table */
    if (ECFM_CC_GLOBAL_MIP_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_MIP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_MIP_ENTRY);
        ECFM_CC_GLOBAL_MIP_TABLE = NULL;
    }

    /* Delete Offload RMepDb table */
    if (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_OFF_RMEP_ENTRY);
        ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE = NULL;
    }

    /* Delete Offload Mep table */
    if (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_OFF_MEP_ENTRY);
        ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE = NULL;
    }

    /* Global Hardware Mep */
    if (ECFM_CC_GLOBAL_HW_MEP_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_HW_MEP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_HW_MEP_ENTRY);
        ECFM_CC_GLOBAL_HW_MEP_TABLE = NULL;
    }
#ifdef NPAPI_WANTED
    /* Global Hardware Mep */
    if (ECFM_CC_GLOBAL_HW_RMEP_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_GLOBAL_HW_RMEP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_GLOBAL_HW_RMEP_ENTRY);
        ECFM_CC_GLOBAL_HW_MEP_TABLE = NULL;
    }
#endif

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcStackTableGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Stack RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Stack Node1
 *                       *pE2 - pointer to Stack Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcStackTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcStackInfo   *pEcfmEntryA = NULL;
    tEcfmCcStackInfo   *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcStackInfo *) pE1;
    pEcfmEntryB = (tEcfmCcStackInfo *) pE2;

    if (pEcfmEntryA->u4ContextId != pEcfmEntryB->u4ContextId)

    {
        if (pEcfmEntryA->u4ContextId < pEcfmEntryB->u4ContextId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    if (pEcfmEntryA->u4IfIndex != pEcfmEntryB->u4IfIndex)

    {
        if (pEcfmEntryA->u4IfIndex < pEcfmEntryB->u4IfIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4VlanIdIsid != pEcfmEntryB->u4VlanIdIsid)

    {
        if (pEcfmEntryA->u4VlanIdIsid < pEcfmEntryB->u4VlanIdIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1Direction != pEcfmEntryB->u1Direction)

    {
        if (pEcfmEntryA->u1Direction < pEcfmEntryB->u1Direction)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcOffRMepDbTableGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two RMepDb RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to RMepDb Node1
 *                       *pE2 - pointer to RMepDb Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcOffRMepDbTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcRMepDbInfo  *pEcfmEntryA = NULL;
    tEcfmCcRMepDbInfo  *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcRMepDbInfo *) pE1;
    pEcfmEntryB = (tEcfmCcRMepDbInfo *) pE2;
    if (pEcfmEntryA->u2OffloadRMepRxHandle !=
        pEcfmEntryB->u2OffloadRMepRxHandle)

    {
        if (pEcfmEntryA->u2OffloadRMepRxHandle <
            pEcfmEntryB->u2OffloadRMepRxHandle)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcConfigErrTableGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two ConfigErrList RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to ConfigErrList Node1
 *                       *pE2 - pointer to ConfigErrList Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcConfigErrTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcConfigErrInfo *pEcfmEntryA = NULL;
    tEcfmCcConfigErrInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcConfigErrInfo *) pE1;
    pEcfmEntryB = (tEcfmCcConfigErrInfo *) pE2;
    if (pEcfmEntryA->u4VidIsid != pEcfmEntryB->u4VidIsid)

    {
        if (pEcfmEntryA->u4VidIsid < pEcfmEntryB->u4VidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4IfIndex != pEcfmEntryB->u4IfIndex)

    {
        if (pEcfmEntryA->u4IfIndex < pEcfmEntryB->u4IfIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcMipGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Mip RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Mip Node1
 *                       *pE2 - pointer to Mip Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcMipGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMipInfo     *pEcfmEntryA = NULL;
    tEcfmCcMipInfo     *pEcfmEntryB = NULL;
    UINT4               u4IfIndexA = 0;
    UINT4               u4IfIndexB = 0;
    pEcfmEntryA = (tEcfmCcMipInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMipInfo *) pE2;

    if (pEcfmEntryA->u4ContextId != pEcfmEntryB->u4ContextId)

    {
        if (pEcfmEntryA->u4ContextId < pEcfmEntryB->u4ContextId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2PortNum == 0)
    {
        return -1;
    }
    if (pEcfmEntryB->u2PortNum == 0)
    {
        return 1;
    }
    u4IfIndexA =
        ECFM_CC_GET_IFINDEX (pEcfmEntryA->u4ContextId, pEcfmEntryA->u2PortNum);
    u4IfIndexB =
        ECFM_CC_GET_IFINDEX (pEcfmEntryB->u4ContextId, pEcfmEntryB->u2PortNum);

    if (u4IfIndexA != u4IfIndexB)

    {
        if (u4IfIndexA < u4IfIndexB)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4VlanIdIsid != pEcfmEntryB->u4VlanIdIsid)

    {
        if (pEcfmEntryA->u4VlanIdIsid < pEcfmEntryB->u4VlanIdIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcMipPreventGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Mip RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Mip Node1
 *                       *pE2 - pointer to Mip Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcMipPreventGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMipPreventInfo *pEcfmEntryA = NULL;
    tEcfmCcMipPreventInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMipPreventInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMipPreventInfo *) pE2;

    if (pEcfmEntryA->u4ContextId != pEcfmEntryB->u4ContextId)

    {
        if (pEcfmEntryA->u4ContextId < pEcfmEntryB->u4ContextId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    if (pEcfmEntryA->u4IfIndex != pEcfmEntryB->u4IfIndex)

    {
        if (pEcfmEntryA->u4IfIndex < pEcfmEntryB->u4IfIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4VlanIdIsid != pEcfmEntryB->u4VlanIdIsid)

    {
        if (pEcfmEntryA->u4VlanIdIsid < pEcfmEntryB->u4VlanIdIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcPortTableGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MD RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MD Node1
 *                       *pE2 - pointer to MD Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcPortTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcPortInfo    *pEcfmEntryA = NULL;
    tEcfmCcPortInfo    *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcPortInfo *) pE1;
    pEcfmEntryB = (tEcfmCcPortInfo *) pE2;

    if (pEcfmEntryA->u4ContextId != pEcfmEntryB->u4ContextId)

    {
        if (pEcfmEntryA->u4ContextId < pEcfmEntryB->u4ContextId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    if (pEcfmEntryA->u4IfIndex != pEcfmEntryB->u4IfIndex)

    {
        if (pEcfmEntryA->u4IfIndex < pEcfmEntryB->u4IfIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcProcessIntQueOverFlow
 *
 *    DESCRIPTION      : This routine does the processing if Interrupt Queue 
 *                       Overflows, it gets list of RxHandles and call the 
 *                       RMEP SM for the RMEP for which defect are not raised
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
EcfmCcProcessIntQueOverFlow ()
{

#ifdef NPAPI_WANTED
    tEcfmCcOffRxHandleInfo EcfmCcOffRxHandleInfo;
    UINT2               au2RxHandle[ECFM_RXHANDLES_PER_SLOT_MAX];

    UINT4               u4Event = 0;
    UINT2               u2Index = 0;
    BOOL1               b1More = OSIX_TRUE;

    MEMSET (&EcfmCcOffRxHandleInfo, 0, sizeof (tEcfmCcOffRxHandleInfo));
    MEMSET (au2RxHandle, 0, (sizeof (UINT2) * ECFM_RXHANDLES_PER_SLOT_MAX));

    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () != ECFM_TRUE)
    {
        return;
    }
    /* Relinquish to process the specified events */
    u4Event = ECFM_EV_CFM_PDU_IN_QUEUE | ECFM_EV_TMR_EXP;

    EcfmCcOffRxHandleInfo.u2StartRxHandle = 1;
    EcfmCcOffRxHandleInfo.u2Length = ECFM_RXHANDLES_PER_SLOT_MAX;

    ECFM_CC_RESET_STAGGERING_DELTA ();
    while (b1More == OSIX_TRUE)
    {
        /*Call NPAPI to the list of RxHandles */
        if ((EcfmFsMiEcfmHwHandleIntQFailure (ECFM_DEFAULT_CONTEXT,
                                              &EcfmCcOffRxHandleInfo,
                                              au2RxHandle,
                                              &b1More) != FNP_SUCCESS))
        {
            return;
        }

        for (u2Index = 0; u2Index < ECFM_RXHANDLES_PER_SLOT_MAX; u2Index++)
        {
            if (au2RxHandle[u2Index] == 0)
            {
                break;
            }

            /* Check whether Defect already raised for this, 
             * if no then raise defect 
             * else no defect is raised*/
            EcfmCcmOffHandleIntQFailure (au2RxHandle[u2Index]);
        }
        /* Relinqush Loop */
        EcfmCcUtilRelinquishLoop (u4Event);

        if (b1More == OSIX_TRUE)
        {
            EcfmCcOffRxHandleInfo.u2StartRxHandle =
                au2RxHandle[ECFM_RXHANDLES_PER_SLOT_MAX - 1];
            MEMSET (au2RxHandle, 0,
                    (sizeof (UINT2) * ECFM_RXHANDLES_PER_SLOT_MAX));
        }
    }
#endif
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcOffMepTableGlobalCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two RMepDb RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to RMepDb Node1
 *                       *pE2 - pointer to RMepDb Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcOffMepTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMepInfo     *pEcfmEntryA = NULL;
    tEcfmCcMepInfo     *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMepInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMepInfo *) pE2;
    if (pEcfmEntryA->u4IfIndex != pEcfmEntryB->u4IfIndex)

    {
        if (pEcfmEntryA->u4IfIndex < pEcfmEntryB->u4IfIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)

    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1Direction != pEcfmEntryB->u1Direction)

    {
        if (pEcfmEntryA->u1Direction < pEcfmEntryB->u1Direction)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcHwMepTableGlobalCmp
 *
 *    DESCRIPTION      : This compare function will be used 
 *                       to compare the two MepDb RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to RMepDb Node1
 *                       *pE2 - pointer to RMepDb Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcHwMepTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMepInfo     *pEcfmEntryA = NULL;
    tEcfmCcMepInfo     *pEcfmEntryB = NULL;

    pEcfmEntryA = (tEcfmCcMepInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMepInfo *) pE2;

    return (MEMCMP (pEcfmEntryA->au1HwMepHandler, pEcfmEntryB->au1HwMepHandler,
                    ECFM_HW_MEP_HANDLER_SIZE));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcHwRMepTableGlobalCmp
 *
 *    DESCRIPTION      : This compare function will be used 
 *                       to compare two RMepDb RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to RMepDb Node1
 *                       *pE2 - pointer to RMepDb Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcHwRMepTableGlobalCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcRMepDbInfo  *pEcfmEntryA = NULL;
    tEcfmCcRMepDbInfo  *pEcfmEntryB = NULL;

    pEcfmEntryA = (tEcfmCcRMepDbInfo *) pE1;
    pEcfmEntryB = (tEcfmCcRMepDbInfo *) pE2;

    return (MEMCMP
            (pEcfmEntryA->au1HwRMepHandler, pEcfmEntryB->au1HwRMepHandler,
             ECFM_HW_MEP_HANDLER_SIZE));
}

/*****************************************************************************/
/* Function Name      : EcfmCcAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function is called when ECFM Module is STARTED. */
/*                      Assign mempoolIds for CC mempools created in ECFM module*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
EcfmCcAssignMempoolIds (VOID)
{
    /*MAX_ECFM_AIS_MEP_IN_SYSTEM */
    ECFM_CC_AIS_PDU_POOL = ECFMMemPoolIds[MAX_ECFM_AIS_MEP_IN_SYSTEM_SIZING_ID];

    /*tEcfmCcConfigErrInfo */
    ECFM_CC_CONF_ERR_LIST_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_CONFIG_ERR_INFO_SIZING_ID];

    /*tEcfmCcContextInfo */
    ECFM_CC_CONTEXT_POOL = ECFMMemPoolIds[MAX_ECFM_CC_CONTEXT_INFO_SIZING_ID];

    /*tEcfmCcDefaultMdTableInfo */
    ECFM_CC_DEF_MD_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_DEF_MD_INFO_SIZING_ID];

    /*tEcfmCcMaInfo */
    ECFM_CC_MA_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_CC_MA_INFO_SIZING_ID];

    /*tEcfmCcMdInfo */
    ECFM_CC_MD_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_CC_MD_INFO_SIZING_ID];

    /*tEcfmCcMepInfo */
    ECFM_CC_MEP_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_CC_MEP_INFO_SIZING_ID];

    /*tEcfmCcMepListInfo */
    ECFM_CC_MA_MEP_LIST_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_MEP_LIST_INFO_SIZING_ID];

    /*tEcfmCcMipInfo */
    ECFM_CC_MIP_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_CC_MIP_INFO_SIZING_ID];

    /*tEcfmCcMipPreventInfo */
    ECFM_CC_MIP_PREVENT_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_MIP_PREVENT_INFO_SIZING_ID];

    /*tEcfmCcPortInfo */
    ECFM_CC_PORT_INFO_POOL = ECFMMemPoolIds[MAX_ECFM_CC_PORT_INFO_SIZING_ID];

    /*tEcfmCcMsg */
    ECFM_CC_MSGQ_POOL = ECFMMemPoolIds[MAX_ECFM_CC_Q_MESG_SIZING_ID];

    /*tEcfmRegParams */
    ECFM_CC_APP_REG_MEM_POOL = ECFMMemPoolIds[MAX_ECFM_CC_REG_APPS_SIZING_ID];

    /*tEcfmCcRMepDbInfo */
    ECFM_CC_MEP_DB_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_RMEP_DB_INFO_SIZING_ID];

    /*tEcfmCcStackInfo */
    ECFM_CC_STACK_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_CC_STACK_INFO_SIZING_ID];

    /*tEcfmCcVlanInfo */
    ECFM_CC_VLAN_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_CC_VLAN_INFO_SIZING_ID];

    /*MAX_ECFM_LCK_MEP_IN_SYSTEM */
    ECFM_CC_LCK_PDU_POOL = ECFMMemPoolIds[MAX_ECFM_LCK_MEP_IN_SYSTEM_SIZING_ID];

    /*MAX_ECFM_CC_MEP_ERR_CCM */
    ECFM_CC_MEP_ERR_CCM_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_MEP_ERR_CCM_SIZE_SIZING_ID];

    /*tEcfmMplsParams */
    ECFM_CC_MEP_MPTP_PARAMS_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_MEP_MPTP_PARAMS_SIZE_SIZING_ID];

    /*MAX_ECFM_CC_MEP_XCON_CCM */
    ECFM_CC_MEP_XCON_CCM_POOL =
        ECFMMemPoolIds[MAX_ECFM_CC_MEP_XCON_CCM_SIZE_SIZING_ID];

    /*tEcfmLbrRcvdInfo */
    ECFM_LBR_RCVD_INFO_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBR_RCVD_INFO_SIZE_SIZING_ID];

    /*MAX_ECFM_MAX_CHASSISID_LEN */
    ECFM_CC_RMEP_CHASSIS_ID_POOL =
        ECFMMemPoolIds[MAX_ECFM_MAX_CHASSISID_LEN_SIZING_ID];

    /*MAX_ECFM_MAX_MAN_ADDR_LEN */
    ECFM_CC_RMEP_MGMT_ADDR_POOL =
        ECFMMemPoolIds[MAX_ECFM_MAX_MAN_ADDR_LEN_SIZING_ID];

    /*MAX_ECFM_MAX_MGMT_DOMAIN_LEN */
    ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL =
        ECFMMemPoolIds[MAX_ECFM_MAX_MGMT_DOMAIN_LEN_SIZING_ID];

    /*tMplsApiInInfo */
    ECFM_MPLSTP_INPARAMS_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPLSTP_INPARAMS_SIZE_SIZING_ID];

    /*tMplsApiOutInfo */
    ECFM_MPLSTP_OUTPARAMS_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPLSTP_OUTPARAMS_SIZE_SIZING_ID];

    /*tEcfmMplsTpPathInfo */
    ECFM_MPLSTP_PATHINFO_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPLSTP_PATHINFO_SIZE_SIZING_ID];

    /*tServicePtr */
    ECFM_MPLSTP_SERVICE_PTR_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPLSTP_SERVICE_PTR_SIZE_SIZING_ID];

    /*MAX_ECFM_ACTIVE_MA_LEVEL */
    ECFM_CC_ACTIVE_MA_LEVEL_POOL =
        ECFMMemPoolIds[MAX_ECFM_ACTIVE_MA_LEVEL_SIZING_ID];

    /*MAX_ECFM_ACTIVE_MD_LEVEL */
    ECFM_CC_ACTIVE_MD_LEVEL_POOL =
        ECFMMemPoolIds[MAX_ECFM_ACTIVE_MD_LEVEL_SIZING_ID];

    /*tCfmCcPortArray */
    ECFM_CC_HW_PORTLIST_ARRAY =
        ECFMMemPoolIds[MAX_ECFM_CC_PORT_ARRAY_COUNT_SIZING_ID];
}

/****************************************************************************
  End of File cfmcctsk.c
 ****************************************************************************/
