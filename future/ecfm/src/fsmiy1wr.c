/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved]
*
* $Id: fsmiy1wr.c,v 1.29 2017/08/24 12:00:14 siva Exp $
*
* Description: This file contains the wrapper routines for
*              proprietary mib.
*******************************************************************/

# include  "cfminc.h"
# include  "fsmiy1db.h"

INT4
GetNextIndexFsMIY1731ContextTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731ContextTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731ContextTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

VOID
RegisterFSMIY1 ()
{
    SNMPRegisterMib (&fsmiy1OID, &fsmiy1Entry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmiy1OID, (const UINT1 *) "fsmiy1731");
}

VOID
UnRegisterFSMIY1 ()
{
    SNMPUnRegisterMib (&fsmiy1OID, &fsmiy1Entry);
    SNMPDelSysorEntry (&fsmiy1OID, (const UINT1 *) "fsmiy1731");
}

INT4
FsMIY1731ContextNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731ContextName
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameLossBufferClearGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FrameLossBufferClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameDelayBufferClearGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FrameDelayBufferClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731LbrCacheClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ErrorLogClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731ErrorLogClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameLossBufferSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FrameLossBufferSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameDelayBufferSizeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FrameDelayBufferSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731LbrCacheSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrCacheHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731LbrCacheHoldTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731TrapControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731TrapControl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ErrorLogStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731ErrorLogStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ErrorLogSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731ErrorLogSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731OperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731OperStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ContextTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731LbrCacheStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FrameLossBufferClearSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731FrameLossBufferClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameDelayBufferClearSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731FrameDelayBufferClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731LbrCacheClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ErrorLogClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731ErrorLogClear
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameLossBufferSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731FrameLossBufferSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameDelayBufferSizeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731FrameDelayBufferSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731LbrCacheSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731LbrCacheHoldTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731TrapControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731TrapControl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ErrorLogStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731ErrorLogStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ErrorLogSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731ErrorLogSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731OperStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731OperStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731LbrCacheStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameLossBufferClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731FrameLossBufferClear (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FrameDelayBufferClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731FrameDelayBufferClear (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrCacheClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731LbrCacheClear (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ErrorLogClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731ErrorLogClear (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FrameLossBufferSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2FsMIY1731FrameLossBufferSize (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FrameDelayBufferSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731FrameDelayBufferSize (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrCacheSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731LbrCacheSize (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrCacheHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731LbrCacheHoldTime (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TrapControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731TrapControl (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ErrorLogStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731ErrorLogStatus (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ErrorLogSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731ErrorLogSize (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731OperStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731OperStatus (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731LbrCacheStatus (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ContextTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731ContextTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIY1731MegTable (tSnmpIndex *
                               pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731MegTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731MegTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegClientMEGLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MegTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MegClientMEGLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegVlanPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MegTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MegVlanPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MegDropEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MegTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MegDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MegTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MegRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MegClientMEGLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MegClientMEGLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegVlanPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MegVlanPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegDropEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MegDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MegRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegClientMEGLevelTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MegClientMEGLevel (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MegVlanPriorityTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MegVlanPriority (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MegDropEnableTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MegDropEnable (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MegRowStatusTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MegRowStatus (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MegTableDep (UINT4 *pu4Error,
                      tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731MegTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIY1731MeTable (tSnmpIndex *
                              pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731MeTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731MeTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeCciEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MeTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MeCciEnabled
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeCcmApplicationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MeTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MeCcmApplication
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeMegIdIccGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MeTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MeMegIdIcc
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeMegIdUmcGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MeTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MeMegIdUmc
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MeTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MeRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeCciEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MeCciEnabled
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeCcmApplicationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MeCcmApplication
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeMegIdIccSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MeMegIdIcc
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeMegIdUmcSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MeMegIdUmc
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MeRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeCciEnabledTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MeCciEnabled (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeCcmApplicationTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MeCcmApplication (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeMegIdIccTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MeMegIdIcc (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeMegIdUmcTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MeMegIdUmc (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MeRowStatusTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MeRowStatus (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MeTableDep (UINT4 *pu4Error,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731MeTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIY1731MepTable (tSnmpIndex *
                               pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731MepTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731MepTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepOutOfServiceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepOutOfService
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepRdiCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepRdiCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepRdiPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepRdiPeriod
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepCcmDropEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepCcmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepCcmPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepCcmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepMulticastLbmRecvCapabilityGet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepMulticastLbmRecvCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLoopbackCapabilityGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepLoopbackCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLbmCurrentTransIdGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepLbmCurrentTransId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmStatusGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmResultOKGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDestMacAddressGet (tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;

    if (nmhGetFsMIY1731MepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDestMepIdGet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDestTypeGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmDestType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmMessagesGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmIntervalGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmIntervalTypeGet (tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmIntervalType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDeadlineGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDropEnableGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmPriorityGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmVariableBytesGet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmVariableBytes
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTlvTypeGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmTlvType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDataPatternGet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmDataPattern
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDataPatternSizeGet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmDataPatternSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTestPatternTypeGet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmTestPatternType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTestPatternSizeGet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmTestPatternSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmSeqNumberGet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLbmSeqNumber
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepBitErroredLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepBitErroredLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmStatusGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmResultOKGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmFlagsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmFlags
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetMacAddressGet (tSnmpIndex *
                                            pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmTargetMepIdGet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetIsMepIdGet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmTtl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmDropEnableGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmPriorityGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmSeqNumberGet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmSeqNumber
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTimeoutGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLtmTimeout
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepMulticastTstRecvCapabilityGet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepMulticastTstRecvCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstPatternTypeGet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstPatternType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstVariableBytesGet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstVariableBytes
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstPatternSizeGet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstPatternSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstDestTypeGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstDestType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstDestMacAddressGet (tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepTransmitTstDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDestMepIdGet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstSeqNumberGet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstSeqNumber
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstPriorityGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstDropEnableGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstMessagesGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstStatusGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstResultOKGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstIntervalGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstIntervalTypeGet (tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstIntervalType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstDeadlineGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitTstDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepBitErroredTstInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepBitErroredTstIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepValidTstInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepValidTstIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTstOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTstOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmStatusGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmResultOKGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmIntervalGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLmmDeadlineGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDropEnableGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLmmPriorityGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestMacAddressGet (tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepTransmitLmmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestMepIdGet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLmmDestIsMepIdGet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmMessagesGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepTransmitLmmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTxFCfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731MepTxFCf
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepRxFCbGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731MepRxFCb
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestMacAddressGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitThDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThFrameSizeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThFrameSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestMepIdGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestIsMepIdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThMessagesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThPpsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThPps
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDeadlineGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepThVerifiedFrameSizeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepThVerifiedFrameSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstMessagesGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThBurstMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstDeadlineGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThBurstDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThBurstType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThTestPatternTypeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitThTestPatternType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepThVerifiedBpsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepThVerifiedBps
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepThUnVerifiedBpsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepThUnVerifiedBps
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitLbmMode
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLbmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepLbmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTimeoutGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTransmitLbmTimeout
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAisOffloadGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731MepAisOffload (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLbmTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();

    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepLbmTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmIccGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();

    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepLbmIcc (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();

    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepLbmNodeId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue))
        != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmIfNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();

    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepLbmIfNum (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->u4_ULongValue))
        != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLoopbackStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731MepLoopbackStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLmCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLmCapability (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitThResultOKGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitThResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTstCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731MepTstCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTxFCbGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731MepTxFCb
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepNearEndFrameLossThresholdGet (tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepNearEndFrameLossThreshold
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepFarEndFrameLossThresholdGet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepFarEndFrameLossThreshold
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmResultOKGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmIntervalGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmMessagesGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmmDropEnableGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmit1DmDropEnableGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmit1DmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmmPriorityGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmit1DmPriorityGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmit1DmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestMacAddressGet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepTransmitDmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestMepIdGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestIsMepIdGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDeadlineGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepDmrOptionalFieldsGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepDmrOptionalFields
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Mep1DmRecvCapabilityGet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731Mep1DmRecvCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepFrameDelayThresholdGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepFrameDelayThreshold
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAisCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisConditionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisCondition
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAisPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisPeriod
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisDropEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisDestIsMulticastGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAisDestIsMulticast
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisClientMacAddressGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepAisClientMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitThFrameSizeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThFrameSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDestIsMulticastGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckDestIsMulticast
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckClientMacAddressGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepLckClientMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckConditionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckCondition
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckPeriod
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDropEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLckDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepLckDelay
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepDefectConditionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepDefectConditions
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepUnicastCcmMacAddressGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731MepUnicastCcmMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitThDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestMepIdSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestIsMepIdSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThMessagesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThPpsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThPps
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDeadlineSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstMessagesSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThBurstMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstDeadlineSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThBurstDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThBurstType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThTestPatternTypeSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitThTestPatternType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmMode
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLbmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepLbmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTimeoutSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmTimeout
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTstCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTstCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAisOffloadSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisOffload (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepLbmTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmIccSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepLbmIcc (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmNodeIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepLbmNodeId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmIfNumSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepLbmIfNum (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLoopbackStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetFsMIY1731MepLoopbackStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLmCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLmCapability (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepRxFCfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepRxFCf
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Mep1DmTransIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731Mep1DmTransInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Mep1DmTransIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731Mep1DmTransInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Mep1DmTransIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731Mep1DmTransInterval (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_THREE].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepOutOfServiceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepOutOfService
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepRdiCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepRdiCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepRdiPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepRdiPeriod
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepCcmDropEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepCcmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepCcmPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepCcmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepMulticastLbmRecvCapabilitySet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepMulticastLbmRecvCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLoopbackCapabilitySet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepLoopbackCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmStatusSet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDestMacAddressSet (tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDestMepIdSet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDestTypeSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDestType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmMessagesSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmIntervalSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmIntervalTypeSet (tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmIntervalType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDeadlineSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDropEnableSet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmPrioritySet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmVariableBytesSet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmVariableBytes
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTlvTypeSet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmTlvType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDataPatternSet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDataPattern
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDataPatternSizeSet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmDataPatternSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTestPatternTypeSet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmTestPatternType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTestPatternSizeSet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLbmTestPatternSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepBitErroredLbrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepBitErroredLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThFrameSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThFrameSize (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmStatusSet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmFlagsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmFlags
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetMacAddressSet (tSnmpIndex *
                                            pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetMepIdSet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetIsMepIdSet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTtlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmTtl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmDropEnableSet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmPrioritySet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTimeoutSet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLtmTimeout
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepMulticastTstRecvCapabilitySet (tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepMulticastTstRecvCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstPatternTypeSet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstPatternType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstVariableBytesSet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstVariableBytes
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstPatternSizeSet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstPatternSize
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDestTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstDestType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDestMepIdSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstPrioritySet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDropEnableSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstMessagesSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstIntervalSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstIntervalTypeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstIntervalType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDeadlineSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitTstDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepBitErroredTstInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepBitErroredTstIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepValidTstInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepValidTstIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTstOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTstOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmIntervalSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDeadlineSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDropEnableSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmPrioritySet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestMepIdSet (tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestIsMepIdSet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmMessagesSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepNearEndFrameLossThresholdSet (tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepNearEndFrameLossThreshold
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepFarEndFrameLossThresholdSet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepFarEndFrameLossThreshold
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmIntervalSet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmMessagesSet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmmDropEnableSet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmit1DmDropEnableSet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmit1DmDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmmPrioritySet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmit1DmPrioritySet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmit1DmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestMacAddressSet (tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestMepIdSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitThDestMacAddressTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepTransmitThDestMacAddress (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_THREE].
                                                       u4_ULongValue,
                                                       (*(tMacAddr *)
                                                        pMultiData->
                                                        pOctetStrValue->
                                                        pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThDestMepId (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDestIsMepIdTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThDestIsMepId (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThMessagesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThMessages (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThPpsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThPps (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThDeadlineTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThDeadline (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThType (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_THREE].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThStatus (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_THREE].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstMessagesTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThBurstMessages (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ZERO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ONE].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_TWO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_THREE].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstDeadlineTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThBurstDeadline (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ZERO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ONE].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_TWO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_THREE].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThBurstTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThBurstType (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitThTestPatternTypeTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitThTestPatternType (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex
                                                        [ECFM_INDEX_THREE].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmMode (pu4Error,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ZERO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ONE].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_TWO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_THREE].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLbmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepLbmOut (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                     u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmTimeout (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTstCapabilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTstCapability (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisOffloadTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisOffload (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepLbmTTL (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmIccTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepLbmIcc (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->pOctetStrValue)
        != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmNodeIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepLbmNodeId (pu4Error,
                                        pMultiIndex->pIndex[0].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[1].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[2].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[3].
                                        u4_ULongValue,
                                        pMultiData->u4_ULongValue)
        != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLbmIfNumTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepLbmIfNum (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->u4_ULongValue)
        != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLoopbackStatusTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2FsMIY1731MepLoopbackStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLmCapabilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLmCapability (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestIsMepIdSet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDeadlineSet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDeadline
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepDmrOptionalFieldsSet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepDmrOptionalFields
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Mep1DmRecvCapabilitySet (tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731Mep1DmRecvCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepFrameDelayThresholdSet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepFrameDelayThreshold
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAisCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisCapability
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisPeriod
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisDropEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisDestIsMulticastSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisDestIsMulticast
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisClientMacAddressSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAisClientMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDestIsMulticastSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckDestIsMulticast
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckClientMacAddressSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckClientMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckPeriod
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDropEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepLckDelay
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepUnicastCcmMacAddressSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepUnicastCcmMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepOutOfServiceTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepOutOfService (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiIndex->
                                           pIndex[ECFM_INDEX_THREE].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepRdiCapabilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepRdiCapability (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepRdiPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepRdiPeriod (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                        u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepCcmDropEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepCcmDropEnable (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepCcmPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepCcmPriority (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepMulticastLbmRecvCapabilityTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepMulticastLbmRecvCapability (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_ZERO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_ONE].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_TWO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_THREE].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLoopbackCapabilityTest (UINT4 *pu4Error,
                                    tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepLoopbackCapability (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmStatusTest (UINT4 *pu4Error,
                                   tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_THREE].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDestMacAddressTest (UINT4
                                           *pu4Error,
                                           tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        ECFM_LBLT_UNLOCK ();
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepTransmitLbmDestMacAddress (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex
                                                        [ECFM_INDEX_THREE].
                                                        u4_ULongValue,
                                                        (*(tMacAddr *)
                                                         pMultiData->
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDestMepIdTest (UINT4 *pu4Error,
                                      tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmDestMepId (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ZERO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ONE].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_TWO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_THREE].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDestTypeTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmDestType (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmMessagesTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmMessages (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmIntervalTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmInterval (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmIntervalTypeTest (UINT4
                                         *pu4Error,
                                         tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmIntervalType (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ZERO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ONE].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_TWO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_THREE].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmDeadlineTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmDeadline (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDropEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmDropEnable (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmPriorityTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmPriority (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmVariableBytesTest (UINT4
                                          *pu4Error,
                                          tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmVariableBytes (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_THREE].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmTlvTypeTest (UINT4 *pu4Error,
                                    tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmTlvType (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDataPatternTest (UINT4 *pu4Error,
                                        tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmDataPattern (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_THREE].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLbmDataPatternSizeTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmDataPatternSize (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_ZERO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_ONE].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_TWO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_THREE].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmTestPatternTypeTest (UINT4
                                            *pu4Error,
                                            tSnmpIndex *
                                            pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmTestPatternType (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_ZERO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_ONE].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_TWO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_THREE].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLbmTestPatternSizeTest (UINT4
                                            *pu4Error,
                                            tSnmpIndex *
                                            pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLbmTestPatternSize (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_ZERO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_ONE].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_TWO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_THREE].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepBitErroredLbrInTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepBitErroredLbrIn (pu4Error,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ZERO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ONE].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_TWO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_THREE].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmStatusTest (UINT4 *pu4Error,
                                   tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_THREE].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmFlagsTest (UINT4 *pu4Error,
                                  tSnmpIndex *
                                  pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmFlags (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_THREE].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetMacAddressTest (UINT4
                                             *pu4Error,
                                             tSnmpIndex
                                             *
                                             pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        ECFM_LBLT_UNLOCK ();
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepTransmitLtmTargetMacAddress (pu4Error,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_ZERO].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_ONE].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_TWO].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_THREE].
                                                          u4_ULongValue,
                                                          (*(tMacAddr *)
                                                           pMultiData->
                                                           pOctetStrValue->
                                                           pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTargetMepIdTest (UINT4 *pu4Error,
                                        tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmTargetMepId (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_THREE].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmTargetIsMepIdTest (UINT4
                                          *pu4Error,
                                          tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmTargetIsMepId (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_THREE].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTtlTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmTtl (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_THREE].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmDropEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmDropEnable (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLtmPriorityTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmPriority (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitLtmTimeoutTest (UINT4 *pu4Error,
                                    tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLtmTimeout (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepMulticastTstRecvCapabilityTest (UINT4
                                            *pu4Error,
                                            tSnmpIndex *
                                            pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepMulticastTstRecvCapability (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_ZERO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_ONE].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_TWO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_THREE].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstPatternTypeTest (UINT4 *pu4Error,
                                        tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstPatternType (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_THREE].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstVariableBytesTest (UINT4
                                          *pu4Error,
                                          tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstVariableBytes (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_THREE].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstPatternSizeTest (UINT4 *pu4Error,
                                        tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstPatternSize (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_THREE].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDestTypeTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstDestType (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitTstDestMacAddressTest (UINT4
                                           *pu4Error,
                                           tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        ECFM_LBLT_UNLOCK ();
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepTransmitTstDestMacAddress (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex
                                                        [ECFM_INDEX_THREE].
                                                        u4_ULongValue,
                                                        (*(tMacAddr *)
                                                         pMultiData->
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDestMepIdTest (UINT4 *pu4Error,
                                      tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstDestMepId (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ZERO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ONE].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_TWO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_THREE].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstPriorityTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstPriority (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDropEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstDropEnable (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstMessagesTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstMessages (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstStatusTest (UINT4 *pu4Error,
                                   tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_THREE].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstIntervalTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstInterval (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstIntervalTypeTest (UINT4
                                         *pu4Error,
                                         tSnmpIndex *
                                         pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstIntervalType (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ZERO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ONE].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_TWO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_THREE].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitTstDeadlineTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitTstDeadline (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepBitErroredTstInTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepBitErroredTstIn (pu4Error,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ZERO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ONE].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_TWO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_THREE].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepValidTstInTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepValidTstIn (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                         u4_ULongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTstOutTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTstOut (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                     u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmStatusTest (UINT4 *pu4Error,
                                   tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_THREE].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmIntervalTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmInterval (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDeadlineTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDeadline (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDropEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDropEnable (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmPriorityTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmPriority (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestMacAddressTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepTransmitLmmDestMacAddress (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex
                                                        [ECFM_INDEX_THREE].
                                                        u4_ULongValue,
                                                        (*(tMacAddr *)
                                                         pMultiData->
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestMepIdTest (UINT4 *pu4Error,
                                      tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDestMepId (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ZERO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ONE].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_TWO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_THREE].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmDestIsMepIdTest (UINT4 *pu4Error,
                                        tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2FsMIY1731MepTransmitLmmDestIsMepId (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_THREE].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitLmmMessagesTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmMessages (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepNearEndFrameLossThresholdTest (UINT4
                                           *pu4Error,
                                           tSnmpIndex *
                                           pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepNearEndFrameLossThreshold (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex
                                                        [ECFM_INDEX_THREE].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepFarEndFrameLossThresholdTest (UINT4
                                          *pu4Error,
                                          tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepFarEndFrameLossThreshold (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_THREE].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmStatus (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_THREE].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmTypeTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmType (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_THREE].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmIntervalTest (UINT4 *pu4Error,
                                    tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmInterval (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmMessagesTest (UINT4 *pu4Error,
                                    tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmMessages (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmmDropEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmmDropEnable (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmit1DmDropEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2FsMIY1731MepTransmit1DmDropEnable (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmmPriorityTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmmPriority (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmit1DmPriorityTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmit1DmPriority (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepTransmitDmDestMacAddressTest (UINT4
                                          *pu4Error,
                                          tSnmpIndex *
                                          pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        ECFM_LBLT_UNLOCK ();
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepTransmitDmDestMacAddress (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_THREE].
                                                       u4_ULongValue,
                                                       (*(tMacAddr *)
                                                        pMultiData->
                                                        pOctetStrValue->
                                                        pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestMepIdTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDestMepId (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDestIsMepIdTest (UINT4 *pu4Error,
                                       tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDestIsMepId (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ZERO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_ONE].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_TWO].
                                                    u4_ULongValue,
                                                    pMultiIndex->
                                                    pIndex[ECFM_INDEX_THREE].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTransmitDmDeadlineTest (UINT4 *pu4Error,
                                    tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDeadline (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepDmrOptionalFieldsTest (UINT4 *pu4Error,
                                   tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepDmrOptionalFields (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_THREE].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731Mep1DmRecvCapabilityTest (UINT4 *pu4Error,
                                   tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731Mep1DmRecvCapability (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_THREE].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepFrameDelayThresholdTest (UINT4 *pu4Error,
                                     tSnmpIndex *
                                     pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepFrameDelayThreshold (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisCapabilityTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisCapability (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisIntervalTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisInterval (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisPeriodTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisPeriod (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                        u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisPriorityTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisPriority (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisDropEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisDropEnable (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisDestIsMulticastTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAisDestIsMulticast (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAisClientMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepAisClientMacAddress (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  (*(tMacAddr *) pMultiData->
                                                   pOctetStrValue->
                                                   pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDestIsMulticastTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLckDestIsMulticast (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_THREE].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckClientMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsMIY1731MepLckClientMacAddress (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  (*(tMacAddr *) pMultiData->
                                                   pOctetStrValue->
                                                   pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLckInterval (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepLckPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLckPeriod (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                        u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckPriorityTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLckPriority (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                          u4_ULongValue,
                                          pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDropEnableTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLckDropEnable (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_THREE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepLckDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepLckDelay (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepUnicastCcmMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        ECFM_CC_UNLOCK ();
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepUnicastCcmMacAddress (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ZERO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ONE].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_TWO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_THREE].
                                                   u4_ULongValue,
                                                   (*(tMacAddr *) pMultiData->
                                                    pOctetStrValue->
                                                    pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepRowStatusTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepRowStatus (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731MepTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIY1731ErrorLogTable (tSnmpIndex *
                                    pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731ErrorLogTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731ErrorLogTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ErrorLogTimeStampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ErrorLogTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        if (nmhGetFsMIY1731ErrorLogTimeStamp
            (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;

        }

    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ErrorLogTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ErrorLogTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731ErrorLogType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731ErrorLogRMepIdentifierGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731ErrorLogTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731ErrorLogRMepIdentifier
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIY1731LtrTable (tSnmpIndex *
                               pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731LtrTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731LtrTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LtrReceiveTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LtrReceiveTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIY1731LbmTable (tSnmpIndex *
                               pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731LbmTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731LbmTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmBytesSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmBytesSent
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmTargetMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731LbmTargetMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmUnexptedLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmUnexptedLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbmDuplicatedLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmDuplicatedLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbmNumOfRespondersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmNumOfResponders
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmDestTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmDestType (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].u4_ULongValue,
                                    pMultiIndex->pIndex[5].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmDestMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmDestMepId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     pMultiIndex->pIndex[5].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmIccGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmIcc (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].u4_ULongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               pMultiIndex->pIndex[4].u4_ULongValue,
                               pMultiIndex->pIndex[5].u4_ULongValue,
                               pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmNodeId (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiIndex->pIndex[4].u4_ULongValue,
                                  pMultiIndex->pIndex[5].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmIfNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmIfNum (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 pMultiIndex->pIndex[4].u4_ULongValue,
                                 pMultiIndex->pIndex[5].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbmTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbmTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].u4_ULongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               pMultiIndex->pIndex[4].u4_ULongValue,
                               pMultiIndex->pIndex[5].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIY1731LbrTable (tSnmpIndex *
                               pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731LbrTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731LbrTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrResponderMacAddressGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731LbrResponderMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrReceiveTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrReceiveTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrErrorTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrErrorType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_SIX].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrDestTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrDestType (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].u4_ULongValue,
                                    pMultiIndex->pIndex[5].u4_ULongValue,
                                    pMultiIndex->pIndex[6].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbrDestMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrDestMepId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     pMultiIndex->pIndex[5].u4_ULongValue,
                                     pMultiIndex->pIndex[6].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrICCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrICC (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].u4_ULongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               pMultiIndex->pIndex[4].u4_ULongValue,
                               pMultiIndex->pIndex[5].u4_ULongValue,
                               pMultiIndex->pIndex[6].u4_ULongValue,
                               pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrNodeId (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiIndex->pIndex[4].u4_ULongValue,
                                  pMultiIndex->pIndex[5].u4_ULongValue,
                                  pMultiIndex->pIndex[6].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbrIfNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbrIfNum (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 pMultiIndex->pIndex[4].u4_ULongValue,
                                 pMultiIndex->pIndex[5].u4_ULongValue,
                                 pMultiIndex->pIndex[6].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIY1731LbStatsTable (tSnmpIndex *
                                   pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731LbStatsTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731LbStatsTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LbStatsLbmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsLbmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbStatsLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbStatsLbrTimeAverageGet (tSnmpIndex *
                                   pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsLbrTimeAverage
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbStatsLbrTimeMinGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsLbrTimeMin
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbStatsLbrTimeMaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsLbrTimeMax
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbStatsTotalRespondersGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsTotalResponders
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LbStatsAvgLbrsPerResponderGet (tSnmpIndex *
                                        pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731LbStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LbStatsAvgLbrsPerResponder
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIY1731FdTable (tSnmpIndex *
                              pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731FdTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731FdTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }

    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdTxTimeStampfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdTxTimeStampf
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdMeasurementTimeStampGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdMeasurementTimeStamp
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdPeerMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731FdPeerMepMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdIfIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdDelayValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdDelayValue
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdIFDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdIFDV
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdFDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdFDV
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdMeasurementTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdMeasurementType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIY1731FdStatsTable (tSnmpIndex *
                                   pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731FdStatsTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731FdStatsTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdStatsTimeStampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsTimeStamp
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdStatsDmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsDmmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdStatsDmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsDmrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdStatsDelayAverageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsDelayAverage
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdStatsFDVAverageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsFDVAverage
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731FdStatsIFDVAverageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsIFDVAverage
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdStatsDelayMinGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsDelayMin
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FdStatsDelayMaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FdStatsDelayMax
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIY1731FlTable (tSnmpIndex *
                              pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731FlTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731FlTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlMeasurementTimeStampGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlMeasurementTimeStamp
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlPeerMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsMIY1731FlPeerMepMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731FlIfIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlFarEndLossGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlFarEndLoss
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlNearEndLossGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceFsMIY1731FlTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlNearEndLoss
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlMeasurementTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlMeasurementTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FIVE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIY1731FlStatsTable (tSnmpIndex *
                                   pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731FlStatsTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731FlStatsTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsTimeStampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsTimeStamp
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsMessagesOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsMessagesOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsMessagesInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsMessagesIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsFarEndLossAverageGet (tSnmpIndex *
                                      pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsFarEndLossAverage
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsNearEndLossAverageGet (tSnmpIndex *
                                       pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsNearEndLossAverage
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsMeasurementTypeGet (tSnmpIndex *
                                    pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsMeasurementType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsFarEndLossMinGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsFarEndLossMin
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsFarEndLossMaxGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsFarEndLossMax
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsNearEndLossMinGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsNearEndLossMin
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731FlStatsNearEndLossMaxGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731FlStatsNearEndLossMax
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIY1731MplstpExtRemoteMepTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731MplstpExtRemoteMepTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731MplstpExtRemoteMepTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MplstpExtRMepServicePointerGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MplstpExtRemoteMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsMIY1731MepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIY1731MplstpExtRMepServicePointer
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MplstpExtRMepRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MplstpExtRemoteMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MplstpExtRMepRowStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MplstpExtRMepServicePointerSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MplstpExtRMepServicePointer
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MplstpExtRMepRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MplstpExtRMepRowStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MplstpExtRMepServicePointerTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MplstpExtRMepServicePointer (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[3].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[4].
                                                       u4_ULongValue,
                                                       pMultiData->pOidValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MplstpExtRMepRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MplstpExtRMepRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[4].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MplstpExtRemoteMepTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731MplstpExtRemoteMepTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

/* Per Context Stastistics Table */

INT4
GetNextIndexFsMIY1731StatsTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731StatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731StatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731AisOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731AisOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731AisInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731AisIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LckOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LckOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LckInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LckIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TstOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731TstOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TstInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731TstIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731LmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY17311DmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY17311DmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY17311DmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY17311DmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731DmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731DmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731DmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731DmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ApsOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731ApsOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ApsInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731ApsIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MccOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MccOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MccInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MccIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731VsmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731VsmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731VsrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731VsrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731ExmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731ExmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731ExrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731ExrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TxFailOpcodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731TxFailOpcode (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731AisOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731AisOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731AisInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731AisIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LckOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731LckOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LckInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731LckIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TstOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731TstOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TstInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731TstIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731LmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731LmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731LmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731LmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY17311DmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY17311DmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY17311DmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY17311DmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731DmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731DmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731DmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731DmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ApsOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731ApsOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ApsInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731ApsIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MccOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MccOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MccInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MccIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731VsmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731VsmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731VsrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731VsrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731ExmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731ExmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731ExrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731ExrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731AisOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731AisOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731AisInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731AisIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LckOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731LckOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LckInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731LckIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TstOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731TstOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731TstInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731TstIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731LmmOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731LmmIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731LmrOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731LmrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731LmrIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY17311DmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY17311DmOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY17311DmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY17311DmIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731DmmOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731DmmIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731DmrOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731DmrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731DmrIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ApsOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731ApsOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ApsInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731ApsIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MccOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MccOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MccInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MccIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731VsmOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731VsmIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731VsrOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731VsrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731VsrIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731ExmOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731ExmIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731ExrOut (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731ExrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731ExrIn (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731StatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731StatsTable (pu4Error, pSnmpIndexList,
                                         pSnmpvarbinds));
}

INT4
GetNextIndexFsMIY1731PortTable (tSnmpIndex *
                                pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731PortTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731PortTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortAisOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortAisOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortAisInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortAisIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLckOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortLckOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLckInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortLckIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortTstOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortTstOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortTstInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortTstIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortLmmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortLmmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortLmrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731PortLmrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Port1DmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731Port1DmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Port1DmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731Port1DmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortDmmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortDmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetFsMIY1731PortDmmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortDmrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortDmrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortApsOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortApsOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortApsInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortApsIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortMccOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortMccOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortMccInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortMccIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortVsmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortVsmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortVsrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortVsrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortExmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortExmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortExrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731PortExrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if ((nmhGetFsMIY1731PortOperStatus
         (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
          &(pMultiData->i4_SLongValue))) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortAisOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortAisOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortAisInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortAisIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLckOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortLckOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLckInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortLckIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortTstOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortTstOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortTstInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortTstIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortLmmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortLmmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortLmrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortLmrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Port1DmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731Port1DmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Port1DmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731Port1DmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortDmmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortDmmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortDmrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortDmrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortDmrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortApsOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortApsOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortApsInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortApsIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortMccOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortMccOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortMccInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortMccIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortVsmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortVsmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortVsmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortVsmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortVsrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortVsrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortVsrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortExmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortExmOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731LastTxFailOpcodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731PortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if ((nmhGetFsMIY1731LastTxFailOpcode
         (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
          &(pMultiData->u4_ULongValue))) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortExmIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortExrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortExrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortExrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731PortExrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortOperStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731PortOperStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortAisOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortAisOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortAisInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortAisIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortLckOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortLckOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortLckInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2FsMIY1731PortLckIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortTstOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortTstOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortTstInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortTstIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmmOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortLmmOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmmInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortLmmIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmrOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortLmrOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortLmrInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortLmrIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Port1DmOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731Port1DmOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731Port1DmInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731Port1DmIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmmOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortDmmOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmmInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortDmmIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortDmrOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortDmrOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortDmrInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortDmrIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortApsOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortApsOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731PortApsInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortApsIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortMccOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortMccOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortMccInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortMccIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsmOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortVsmOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsmInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortVsmIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsrOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortVsrOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortVsrInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortVsrIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExmOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortExmOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExmInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortExmIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExrOutTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortExrOut (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortExrInTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731PortExrIn (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)

    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortOperStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731PortOperStatus (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          i4_SLongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731PortTableDep (UINT4 *pu4Error,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIY1731PortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIY1731MepAvailabilityTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIY1731MepAvailabilityTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIY1731MepAvailabilityTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityResultOKGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityResultOK
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;

    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityIntervalGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityInterval
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;

    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityDeadlineGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityDeadline
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityLowerThresholdGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityLowerThreshold
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityUpperThresholdGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityUpperThreshold
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityModestAreaIsAvailableGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityModestAreaIsAvailable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityWindowSizeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityWindowSize
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityDestMacAddressGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIY1731MepAvailabilityDestMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityDestMepIdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityDestMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestIsMepIdGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityDestIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityType
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilitySchldDownInitTimeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilitySchldDownInitTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilitySchldDownEndTimeGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilitySchldDownEndTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityPriorityGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityDropEnableGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityDropEnable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityPercentageGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityPercentage
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIY1731MepAvailabilityRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIY1731MepAvailabilityRowStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityIntervalSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityInterval
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDeadlineSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDeadline
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityLowerThresholdSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityLowerThreshold
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityUpperThresholdSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityUpperThreshold
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityModestAreaIsAvailableSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityModestAreaIsAvailable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityWindowSizeSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityWindowSize
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDestMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestMepIdSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDestMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestIsMepIdSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDestIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityType
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilitySchldDownInitTimeSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilitySchldDownInitTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilitySchldDownEndTimeSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilitySchldDownEndTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityPrioritySet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDropEnableSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDropEnable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityRowStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityInterval (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDeadlineTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDeadline (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityLowerThresholdTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityLowerThreshold (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityUpperThresholdTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityUpperThreshold (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityModestAreaIsAvailableTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityModestAreaIsAvailable (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[2].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[3].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityWindowSizeTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityWindowSize (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestMacAddressTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIY1731MepAvailabilityDestMacAddress (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         u4_ULongValue,
                                                         (*(tMacAddr *)
                                                          pMultiData->
                                                          pOctetStrValue->
                                                          pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestMepIdTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDestMepId (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDestIsMepIdTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDestIsMepId (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilitySchldDownInitTimeTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilitySchldDownInitTime (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilitySchldDownEndTimeTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilitySchldDownEndTime (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[3].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityPriority (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityDropEnableTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDropEnable (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIY1731MepAvailabilityTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    ECFM_CC_LOCK ();
    if (nmhDepv2FsMIY1731MepAvailabilityTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}
