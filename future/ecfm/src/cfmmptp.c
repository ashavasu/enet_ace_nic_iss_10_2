/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmmptp.c,v 1.31 2014/03/16 11:34:12 siva Exp $
 *
 * Description: This file contains the MPLS-TP specific Tx and Rx 
 *              Routines for ECFM.
 *******************************************************************/

#ifndef _CFMMPTP_C_
#define _CFMMPTP_C_

#include "cfminc.h"
#include "mplsutl.h"
/******************************************************************************
 * Function Name      : EcfmMplsTpCcTxPacket
 *
 * Description        : This routine is used to transmit the CFM-PDU over
 *                      MPLS-TP transport path.  
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      pMepInfo - Pointer to the MEP information structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplsTpCcTxPacket (tEcfmBufChainHeader * pBuf,
                      tEcfmCcMepInfo * pMepInfo, UINT1 u1OpCode)
{
    tEcfmMepProcInfo    MepProcInfo;
    tEcfmMplsTpPathInfo *pMplsPathInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               au1DestMac[MAC_ADDR_LEN];

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (au1DestMac, 0, MAC_ADDR_LEN);
    MEMSET (&MepProcInfo, 0x00, sizeof (tEcfmMepProcInfo));

    if ((pBuf == NULL) || (pMepInfo == NULL) ||
        (pMepInfo->pEcfmMplsParams == NULL))
    {
        /* Packet buffer or MEP information or MPLS path information is NULL */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "ECfmMplsTpTxPacket: "
                     "Packet buffer or MEP information or MPLS "
                     "path information is NULL \r\n");
        return ECFM_FAILURE;
    }

    u4ContextId = pMepInfo->u4ContextId;

    /* Allocate Memory for Path Info Structure */
    pMplsPathInfo = (tEcfmMplsTpPathInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_PATHINFO_POOLID);

    if (pMplsPathInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "ECfmMplsTpTxPacket: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    MEMSET (pMplsPathInfo, 0x00, sizeof (tEcfmMplsTpPathInfo));

    /* Get the MPLS-TP path information from MPLS module */
    if (EcfmGetMplsPathInfo (u4ContextId,
                             pMepInfo->pEcfmMplsParams,
                             pMplsPathInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "ECfmMplsTpTxPacket: "
                     "MPLS-TP Path Information get failed\r\n");
        return ECFM_FAILURE;
    }

    if (pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN)
    {
        /* Path Status down */
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        return ECFM_FAILURE;
    }

    MepProcInfo.pMplsParms = pMepInfo->pEcfmMplsParams;
    MepProcInfo.u1Priority = pMepInfo->CcInfo.u1CcmPriority;
    MepProcInfo.u1Ttl = ECFM_MPTP_CC_TTL_DEF;

    /* Construct MPLS headers */
    if (EcfmMplsTpConstructPacket (&MepProcInfo, pMplsPathInfo,
                                   &u4IfIndex, au1DestMac, pBuf)
        == ECFM_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "ECfmMplsTpTxPacket: "
                     "MPLS-TP Packet Construction failed\r\n");
        return ECFM_FAILURE;
    }

    /* Send the packet to MPLS-RTR module for MPLS forwarding */
    if (EcfmSendPktToMpls (u4ContextId, u4IfIndex, au1DestMac, pBuf, u1OpCode)
        == ECFM_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (u4ContextId);
        return ECFM_FAILURE;
    }

    EcfmIncrOutMplsPktCnt (MepProcInfo.pMplsParms->u1MplsPathType,
                           MepProcInfo.pMplsParms->MplsPathParams.u4PswId,
                           ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_CC_INCR_CTX_TX_CFM_PDU_COUNT (u4ContextId);
    ECFM_CC_INCR_CTX_TX_COUNT (u4ContextId, u1OpCode);
    MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID, (UINT1 *) pMplsPathInfo);
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmMplsTpLbLtTxPacket
 *
 * Description        : This routine is used to transmit the LBLT context PDU
 *                      over MPLS-TP transport path.
 *
 * Input(s)           : pBuf     - CRU Buffer Pointer to the packet
 *                      pMepInfo - Pointer to the MEP information structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplsTpLbLtTxPacket (tEcfmBufChainHeader * pBuf,
                        tEcfmLbLtMepInfo * pMepInfo, UINT1 u1OpCode)
{
    tEcfmMepProcInfo    MepProcInfo;
    tEcfmMplsTpPathInfo *pMplsPathInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               au1DestMac[MAC_ADDR_LEN] = { 0 };

    ECFM_LBLT_TRC_FN_ENTRY ();

    MEMSET (&MepProcInfo, 0, sizeof (tEcfmMepProcInfo));
    u4ContextId = ECFM_DEFAULT_CONTEXT;

    /* Allocate Memory for Path Info Structure */
    pMplsPathInfo = (tEcfmMplsTpPathInfo *)
        MemAllocMemBlk (ECFM_MPTP_LBLT_PATHINFO_POOLID);

    if (pMplsPathInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "ECfmMplsTpLbLtTxPacket: "
                       "Mempool allocation failed\r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    MEMSET (pMplsPathInfo, 0, sizeof (tEcfmMplsTpPathInfo));

    /* Get the MPLS-TP path information from MPLS module */
    if (EcfmGetMplsPathInfo (u4ContextId,
                             pMepInfo->pEcfmMplsParams,
                             pMplsPathInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPTP_LBLT_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "ECfmMplsTpLbLtTxPacket: "
                       "MPLS-TP Path Information get failed\r\n");
        return ECFM_FAILURE;
    }

    if (pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN)
    {
        /* Path Status down */
        MemReleaseMemBlock (ECFM_MPTP_LBLT_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        return ECFM_FAILURE;
    }

    MepProcInfo.pMplsParms = pMepInfo->pEcfmMplsParams;
    MepProcInfo.u1Priority = pMepInfo->LbInfo.u1TxLbmVlanPriority;
    if (u1OpCode == ECFM_OPCODE_LBM)
    {
        MepProcInfo.u1Ttl = pMepInfo->LbInfo.u1LbmTtl;
    }
    else
    {
        MepProcInfo.u1Ttl = ECFM_MPTP_CC_TTL_DEF;
    }

    /* Construct MPLS headers */
    if (EcfmMplsTpConstructPacket (&MepProcInfo, pMplsPathInfo,
                                   &u4IfIndex, au1DestMac, pBuf)
        == ECFM_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPTP_LBLT_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "ECfmMplsTpLbLtTxPacket: "
                       "MPLS-TP Packet Construction failed\r\n");
        return ECFM_FAILURE;
    }

    /* Send the packet to MPLS-RTR module for MPLS forwarding */
    if (EcfmSendPktToMpls (u4ContextId, u4IfIndex, au1DestMac, pBuf, u1OpCode)
        == ECFM_FAILURE)
    {
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (u4ContextId);

        MemReleaseMemBlock (ECFM_MPTP_LBLT_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        return ECFM_FAILURE;
    }

    EcfmIncrOutMplsPktCnt (MepProcInfo.pMplsParms->u1MplsPathType,
                           MepProcInfo.pMplsParms->MplsPathParams.u4PswId,
                           ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_LBLT_INCR_CTX_TX_CFM_PDU_COUNT (u4ContextId);
    ECFM_LBLT_INCR_CTX_TX_COUNT (u4ContextId, u1OpCode);
    MemReleaseMemBlock (ECFM_MPTP_LBLT_PATHINFO_POOLID,
                        (UINT1 *) pMplsPathInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcContructOffloadMplsTpTxPkt 
 *
 * Description        : This routine is used to construct MPLS-TP CFM-PDU over
 *                      which will be provided to the offloaded module  
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      pMepInfo - Pointer to the MEP information structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/

PUBLIC INT4
EcfmCcContructOffloadMplsTpTxPkt (tEcfmBufChainHeader * pBuf,
                                  tEcfmCcMepInfo * pMepInfo,
                                  UINT1 *au1MplsDesMac, UINT4 *u4MplsIfIndex)
{
    tEcfmMepProcInfo    MepProcInfo;
    tEcfmMplsTpPathInfo *pMplsPathInfo = NULL;
    UINT4               u4ContextId = 0;

    MEMSET (au1MplsDesMac, 0, MAC_ADDR_LEN);
    MEMSET (&MepProcInfo, 0, sizeof (tEcfmMepProcInfo));

    if ((pBuf == NULL) || (pMepInfo == NULL) ||
        (pMepInfo->pEcfmMplsParams == NULL))
    {
        /* Packet buffer or MEP information or MPLS 
         * path information is NULL */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcContructOffloadMplsTpTxPkt: "
                     "Packet buffer or MEP information or MPLS "
                     "path information is NULL \r\n");
        return ECFM_FAILURE;
    }

    u4ContextId = pMepInfo->u4ContextId;

    /* Allocate Memory for Path Info Structure */
    pMplsPathInfo = (tEcfmMplsTpPathInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_PATHINFO_POOLID);
    if (pMplsPathInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcContructOffloadMplsTpTxPkt: "
                     "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    MEMSET (pMplsPathInfo, 0x00, sizeof (tEcfmMplsTpPathInfo));

    /* Get the MPLS-TP path information from MPLS module */
    if (EcfmGetMplsPathInfo (u4ContextId,
                             pMepInfo->pEcfmMplsParams,
                             pMplsPathInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcContructOffloadMplsTpTxPkt: "
                     "MPLS-TP Path Information get failed\r\n");
        return ECFM_FAILURE;
    }

    if (pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN)
    {
        /* Path Status down */
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        return ECFM_FAILURE;
    }

    MepProcInfo.pMplsParms = pMepInfo->pEcfmMplsParams;
    MepProcInfo.u1Priority = pMepInfo->CcInfo.u1CcmPriority;
    MepProcInfo.u1Ttl = ECFM_MPTP_CC_TTL_DEF;

    /* Construct MPLS headers */
    if (EcfmMplsTpConstructPacket (&MepProcInfo, pMplsPathInfo,
                                   u4MplsIfIndex, au1MplsDesMac, pBuf)
        == ECFM_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcContructOffloadMplsTpTxPkt: "
                     "MPLS-TP Packet Construction failed\r\n");
        return ECFM_FAILURE;
    }

    MemReleaseMemBlock (ECFM_MPLSTP_PATHINFO_POOLID, (UINT1 *) pMplsPathInfo);

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmGetMplsPathInfo
 *
 * Description        : This routine is used to fetch the MPLS-TP path
 *                      information from MPLS module.
 *
 * Input(s)           : u4ContextId - Context Id
 *                      pEcfmMplsTpPathParams - pointer to the path identifier
 *                      structure
 *
 * Output(s)          : pEcfmMplsTpPathInfo - pointer to the structure 
 *                      containing the path information
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmGetMplsPathInfo (UINT4 u4ContextId,
                     tEcfmMplsParams * pEcfmMplsTpPathParams,
                     tEcfmMplsTpPathInfo * pEcfmMplsTpPathInfo)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    BOOL1               bGetMplsTnlInfoFromPw = OSIX_FALSE;
    BOOL1               bGetMplsLspInfoFromPw = OSIX_FALSE;

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_INPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmGetMplsPathInfo: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Allocate memory for the Output structure from
     * MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_OUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmGetMplsPathInfo: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Check if path type is PW */
    if (pEcfmMplsTpPathParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        /* Fill the MPLS Input information to fetch the
         * complete PW path information */
        pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;

        /* Fill the path id with PW VC ID */
        pMplsApiInInfo->InPathId.PwId.u4VcId =
            pEcfmMplsTpPathParams->MplsPathParams.u4PswId;

        /* Fetch PW path information from MPLS module */
        if (EcfmMplsHandleExtInteraction (MPLS_GET_PW_INFO,
                                          pMplsApiInInfo,
                                          pMplsApiOutInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
            return ECFM_FAILURE;
        }

        /* Check the PW status */
        if (pMplsApiOutInfo->OutPwInfo.i1OperStatus == MPLS_OPER_DOWN)
        {
            pEcfmMplsTpPathInfo->u1PathStatus = MPLS_PATH_STATUS_DOWN;
        }

        /* Copy the PW information, to Path info structure, from 
         * MPLS output structure */
        MEMCPY (&(pEcfmMplsTpPathInfo->MplsPwApiInfo),
                &(pMplsApiOutInfo->OutPwInfo), sizeof (tPwApiInfo));

        pEcfmMplsTpPathInfo->u2PathFilled |= CFM_CHECK_PATH_PW_AVAIL;

        if (pEcfmMplsTpPathInfo->MplsPwApiInfo.u1MplsType == L2VPN_MPLS_TYPE_TE)
        {
            /* Underlying path for PW is TE tunnel */
            bGetMplsTnlInfoFromPw = OSIX_TRUE;
        }

        if (pEcfmMplsTpPathInfo->MplsPwApiInfo.u1MplsType ==
            L2VPN_MPLS_TYPE_NONTE)
        {
            /* Underlying path for PW is NON-TE LSP */
            bGetMplsLspInfoFromPw = OSIX_TRUE;
        }
    }

    /* Check if Path type is tunnel */
    if ((pEcfmMplsTpPathParams->u1MplsPathType ==
         MPLS_PATH_TYPE_TUNNEL) || (bGetMplsTnlInfoFromPw == OSIX_TRUE))
    {
        /* Fill the MPLS Input information */
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

        if (bGetMplsTnlInfoFromPw == OSIX_TRUE)
        {
            /* Fill the underlying tunnel id, with info from PW */
            MEMCPY (&(pMplsApiInInfo->InPathId.TnlId),
                    &(pEcfmMplsTpPathInfo->MplsPwApiInfo.TeTnlId),
                    sizeof (tMplsTnlLspId));
        }
        else
        {
            /* Fill the MPLS-TP tunnel identifier information */
            pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                pEcfmMplsTpPathParams->MplsPathParams.MplsLspParams.u4TunnelId;

            pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                pEcfmMplsTpPathParams->MplsPathParams.MplsLspParams.
                u4TunnelInst;

            pMplsApiInInfo->InPathId.TnlId.
                SrcNodeId.MplsRouterId.u4_addr[0] =
                pEcfmMplsTpPathParams->MplsPathParams.MplsLspParams.u4SrcLer;

            pMplsApiInInfo->InPathId.TnlId.
                DstNodeId.MplsRouterId.u4_addr[0] =
                pEcfmMplsTpPathParams->MplsPathParams.MplsLspParams.u4DstLer;
        }

        /* Fetch MPLS Tunnel path information from MPLS module */
        if (EcfmMplsHandleExtInteraction (MPLS_GET_TUNNEL_INFO,
                                          pMplsApiInInfo,
                                          pMplsApiOutInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
            return ECFM_FAILURE;
        }

        /* Check tunnel state. No need to check the tunnel state,
         * if the tunnel is an underlying path for PW. In that case,
         * PW state is sufficient */
        if (bGetMplsTnlInfoFromPw != OSIX_TRUE)
        {
            if (pMplsApiOutInfo->OutTeTnlInfo.u1OperStatus == MPLS_OPER_DOWN)
            {
                pEcfmMplsTpPathInfo->u1PathStatus = MPLS_PATH_STATUS_DOWN;
            }
        }

        /* Check from the path info if it is egress, if so,
         * fetch the reverse path */
        if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
        {
            /* In Code, Only Fwd info is used. hence in co-routed case
             * Copying from rev to fwd */
            if ((pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                 TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
            {
                MEMCPY (&pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevInSegInfo,
                        &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo,
                        sizeof (tMplsInSegInfo));

                MEMCPY (&pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo,
                        &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo,
                        sizeof (tMplsOutSegInfo));
            }
            /* If Tunnel Mode is TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL */
            else
            {
                /* Fill the MPLS In info */
                pMplsApiInInfo->u4ContextId = u4ContextId;
                pMplsApiInInfo->u4SrcModId = Y1731_MODULE;

                /* Path params are already filled above */
                pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
                pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4DstTnlNum;

                MEMCPY (&pMplsApiInInfo->InPathId.TnlId.SrcNodeId,
                        &pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.DstNodeId,
                        sizeof (pMplsApiInInfo->InPathId.TnlId.SrcNodeId));

                MEMCPY (&pMplsApiInInfo->InPathId.TnlId.DstNodeId,
                        &pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.SrcNodeId,
                        sizeof (pMplsApiInInfo->InPathId.TnlId.DstNodeId));

                /* Finding the Reverse Path Information for Associated
                 * bi-directional Tunnel */
                if (EcfmMplsHandleExtInteraction (MPLS_GET_TUNNEL_INFO,
                                                  pMplsApiInInfo,
                                                  pMplsApiOutInfo) ==
                    OSIX_FAILURE)
                {
                    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                        (UINT1 *) pMplsApiOutInfo);
                    return ECFM_FAILURE;
                }

                if (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_NONTE_LSP)
                {
                    /* Copy the LSP information */
                    MEMCPY (&(pEcfmMplsTpPathInfo->MplsNonTeApiInfo),
                            &(pMplsApiOutInfo->OutNonTeTnlInfo),
                            sizeof (tNonTeApiInfo));

                    pEcfmMplsTpPathInfo->u2PathFilled |=
                        CFM_CHECK_PATH_NONTE_AVAIL;

                    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                        (UINT1 *) pMplsApiOutInfo);

                    return ECFM_SUCCESS;
                }
            }
        }

        /* Copy MPLS tunnel path information */
        MEMCPY (&(pEcfmMplsTpPathInfo->MplsTeTnlApiInfo),
                &(pMplsApiOutInfo->OutTeTnlInfo), sizeof (tTeTnlApiInfo));

        pEcfmMplsTpPathInfo->u2PathFilled |= CFM_CHECK_PATH_TE_AVAIL;
    }

    /* Check that underlying path for PW is NON-TE LSP */
    if (bGetMplsLspInfoFromPw == OSIX_TRUE)
    {
        /* Fill the MPLS In info */
        pMplsApiInInfo->u4SubReqType = MPLS_GET_LSP_INFO_FROM_XC_INDEX;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;

        pMplsApiInInfo->InPathId.LspId.u4XCIndex =
            pEcfmMplsTpPathInfo->MplsPwApiInfo.NonTeTnlId;

        /* Fetch the NON-TE LSP path infromation
         * from MPLS module */
        if (EcfmMplsHandleExtInteraction (MPLS_GET_TUNNEL_INFO,
                                          pMplsApiInInfo,
                                          pMplsApiOutInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
            return ECFM_FAILURE;
        }

        /* Copy the LSP path information */
        MEMCPY (&(pEcfmMplsTpPathInfo->MplsNonTeApiInfo),
                &(pMplsApiOutInfo->OutNonTeTnlInfo), sizeof (tNonTeApiInfo));

        pEcfmMplsTpPathInfo->u2PathFilled |= CFM_CHECK_PATH_NONTE_AVAIL;
    }

    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                        (UINT1 *) pMplsApiOutInfo);

    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name    : EcfmMplsGetPwIndex
 *
 * Description      : This function gets pwIndex(SNMP Index to pwTable) from
 *                    the pwTable corresponding to the pwID(VC Identifier).
 *                    
 * Input(s)         : u4PswId    - pwID (VC Identifier) in pwTable
 *
 * Output(s)        : pu4PwIndex - pwIndex (SNMP Index) to pwTable
 * 
 * Return Value(s)  : ECFM_SUCCESS/ECFM_FAILURE
 ****************************************************************************/
PUBLIC INT4
EcfmMplsGetPwIndex (UINT4 u4PswId, UINT4 *pu4PwIndex)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (ECFM_MPLSTP_INPARAMS_POOLID)) == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsPwGetPwIndex: " "Mempool allocation failed\r\n");
        return ECFM_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (ECFM_MPLSTP_OUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsPwGetPwIndex: " "Mempool allocation failed\r\n");
        return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->InPathId.PwId.u4VcId = u4PswId;

    if (EcfmMplsHandleExtInteraction (MPLS_GET_PW_INFO, pMplsApiInInfo,
                                      pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
        return ECFM_FAILURE;
    }

    *pu4PwIndex = pMplsApiOutInfo->OutPwInfo.MplsPwPathId.u4PwIndex;

    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                        (UINT1 *) pMplsApiOutInfo);
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name    : EcfmMplsGetPwId
 *
 * Description      : This function gets pwID(VC Identifier) from the pwTable 
 *                    corsspeondig to the pwIndex(SNMP Index). 
 *
 * Input(s)         : u4PwIndex - pwIndex (SNMP Index) to pwTable
 *
 * Output(s)        : pu4PswId   - pwID (VC Identifier) in pwTable
 *
 * Return Value(s)  : ECFM_SUCCESS/ECFM_FAILURE
 ****************************************************************************/
PUBLIC INT4
EcfmMplsGetPwId (UINT4 u4PwIndex, UINT4 *pu4PswId)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (ECFM_MPLSTP_INPARAMS_POOLID)) == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsPwGetPwIndex: " "Mempool allocation failed\r\n");
        return ECFM_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (ECFM_MPLSTP_OUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsPwGetPwIndex: " "Mempool allocation failed\r\n");
        return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->InPathId.PwId.u4PwIndex = u4PwIndex;

    if (EcfmMplsHandleExtInteraction (MPLS_GET_PW_INFO, pMplsApiInInfo,
                                      pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
        return ECFM_FAILURE;
    }

    *pu4PswId = pMplsApiOutInfo->OutPwInfo.MplsPwPathId.u4VcId;

    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                        (UINT1 *) pMplsApiOutInfo);
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 * Function     : EcfmMplsTpConstructPacket                                  
 *                                                                           
 * Description  : Constructs the MPLS headers                                
 *                                                                           
 * Input        : pMepProcInfo  - Pointer to Mep ProcInfo structure 
 *                pMplsPathInfo - Pointer to the entire MPLS path info       
 *                                                                           
 * Output       : pu4IfIndex  - Pointer to the interface index              
 *                pu1DestMac  - Contains the next hop MAC address
 *                pBuf        - Pointer to CRU Buf which contains constructed
 *                              packet                                       
 *                                                                           
 * Returns      : ECFM_SUCCESS/ECFM_FAILURE                                  
 *                                                                           
 *****************************************************************************/
PUBLIC INT4
EcfmMplsTpConstructPacket (tEcfmMepProcInfo * pMepProcInfo,
                           tEcfmMplsTpPathInfo * pMplsPathInfo,
                           UINT4 *pu4IfIndex, UINT1 *pu1DestMac,
                           tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tMplsHdr            MplsHdr;
    tMplsAchHdr         MplsAchHdr;
    tMplsLblInfo        MplsLblInfo[MPLS_MAX_LABEL_STACK];
    UINT1               u1LblCnt = 0;
    UINT1               u1Count = 0;
    BOOL1               bIsGalLbl = OSIX_FALSE;
    BOOL1               bIsPwLbl = OSIX_FALSE;

    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));
    MEMSET (&MplsAchHdr, 0, sizeof (tMplsAchHdr));

    /* It is mandatory to have the ACH header
     * Because from this header only CFA will be extracting the Protocol Type
     * before forwarding*/

    /* Fill ACH Header */
    MplsAchHdr.u4AchType = 1;
    MplsAchHdr.u1Version = 0;
    MplsAchHdr.u1Rsvd = 0;
    MplsAchHdr.u2ChannelType = CFM_Y1731_CHANNEL_TYPE;

    /* Call MPLS utility function to fill ACH 
     * header in the buffer */
    MplsUtlFillMplsAchHdr (&MplsAchHdr, pBuf);

    /* Check if path is PW */
    if ((pMplsPathInfo->u2PathFilled & CFM_CHECK_PATH_PW_AVAIL) ==
        CFM_CHECK_PATH_PW_AVAIL)
    {
        /* Fill PW Label */
        MplsHdr.u4Lbl = pMplsPathInfo->MplsPwApiInfo.u4OutVcLabel;

        if (pMplsPathInfo->MplsPwApiInfo.
            u1CcSelected & CFM_L2VPN_VCCV_CC_TTL_EXP)
        {
            MplsHdr.Ttl = 1;
        }
        else
        {
            MplsHdr.Ttl = pMplsPathInfo->MplsPwApiInfo.u1Ttl;
        }

        MplsHdr.Exp = pMepProcInfo->u1Priority;
        MplsHdr.SI = 1;

        /* Call MPLS utility function to fill PW Label */
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);

        if (pMplsPathInfo->MplsPwApiInfo.u1CcSelected & CFM_L2VPN_VCCV_CC_RAL)
        {
            /*Set the Router Alert Label */
            MplsHdr.u4Lbl = 1;
            MplsHdr.Ttl = 1;
            MplsHdr.Exp = 0;
            MplsHdr.SI = 0;

            /* Call MPLS utility function to fill RAL Label */
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        }
        bIsPwLbl = OSIX_TRUE;
    }
    else
    {
        /* Fill GAL Label */
        MplsHdr.u4Lbl = CFM_GAL_LABEL;
        MplsHdr.Ttl = 1;
        MplsHdr.Exp = pMepProcInfo->u1Priority;
        MplsHdr.SI = 1;

        /* Call MPLS utility function to fill GAL Label */
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        bIsGalLbl = OSIX_TRUE;
    }

    /* Fill MPLS TE tunnel Label */
    if ((pMplsPathInfo->u2PathFilled & CFM_CHECK_PATH_TE_AVAIL) ==
        CFM_CHECK_PATH_TE_AVAIL)
    {
        /* Fill the out interface index */
        *pu4IfIndex = pMplsPathInfo->MplsTeTnlApiInfo.u4TnlIfIndex;

        /* Fill the Next hop MAC address */
        MEMCPY (pu1DestMac, pMplsPathInfo->MplsTeTnlApiInfo.au1NextHopMac,
                MAC_ADDR_LEN);

        /* Copy Label stack information */
        u1LblCnt = pMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
            FwdOutSegInfo.u1LblStkCnt;

        if (u1LblCnt <= MPLS_MAX_LABEL_STACK)
        {
            MEMCPY (MplsLblInfo,
                    pMplsPathInfo->
                    MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo.LblInfo,
                    (sizeof (tMplsLblInfo) * u1LblCnt));
        }
        else
        {
            /* Exceeding max label stack depth. 
             * stack depth : MPLS_MAX_LABEL_STACK */
            ECFM_GLB_TRC (ECFM_DEFAULT_CONTEXT, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmMplsTpConstructPacket: "
                          "Max No. of Label stack exceeded\r\n");
            return ECFM_FAILURE;
        }

        /* Fill Tunnel Label */
        /* while (u1Count < u1LblCnt) */
        u1Count = u1LblCnt;
        while (u1Count > 0)
        {
            MplsHdr.u4Lbl = MplsLblInfo[u1Count - 1].u4Label;
            MplsHdr.Ttl = pMepProcInfo->u1Ttl;
            MplsHdr.Exp = pMepProcInfo->u1Priority;

            if ((bIsPwLbl == OSIX_TRUE) || (bIsGalLbl == OSIX_TRUE))
            {
                /* PW or GAL label exists. 'S' bit should
                 * be set to 0. */
                MplsHdr.SI = 0;
            }

            /* Call MPLS utility function to fill MPLS Label */
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
            u1Count--;
        };
    }
    /* Fill MPLS NON-TE LSP Label */
    else if ((pMplsPathInfo->u2PathFilled & CFM_CHECK_PATH_NONTE_AVAIL) ==
             CFM_CHECK_PATH_NONTE_AVAIL)
    {
        /* Fill the out interface index */
        *pu4IfIndex = pMplsPathInfo->
            MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.u4OutIf;

        /* Fill the Next hop MAC address */
        MEMCPY (pu1DestMac, pMplsPathInfo->MplsNonTeApiInfo.
                XcApiInfo.FwdOutSegInfo.au1NextHopMac, MAC_ADDR_LEN);

        /* Copy Label stack information */
        u1LblCnt = pMplsPathInfo->
            MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.u1LblStkCnt;

        if (u1LblCnt <= MPLS_MAX_LABEL_STACK)
        {
            MEMCPY (MplsLblInfo,
                    pMplsPathInfo->
                    MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.LblInfo,
                    (sizeof (tMplsLblInfo) * u1LblCnt));
        }
        else
        {
            /* Exceeding max label stack depth. 
             * stack depth : MPLS_MAX_LABEL_STACK */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "ECfmMplsTpConstructPacket: "
                          "Max No. of Label stack exceeded\r\n");
            return ECFM_FAILURE;
        }

        /* Fill Non-te path Label */
        while (u1Count < u1LblCnt)
        {
            MplsHdr.u4Lbl = MplsLblInfo[u1Count].u4Label;
            MplsHdr.Ttl = pMepProcInfo->u1Ttl;
            MplsHdr.Exp = pMepProcInfo->u1Priority;
            if ((bIsPwLbl == OSIX_TRUE) || (bIsGalLbl == OSIX_TRUE))
            {
                /* PW or GAL label exists. 'S' bit should
                 * be set to 0. */
                MplsHdr.SI = 0;
            }

            /* Call MPLS utility function to fill MPLS Label */
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
            u1Count++;
        };
    }

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Name               : EcfmMpTpMepMplsPathCmp
 *
 * Description        : This routine is used to perform the MPLS path parameter 
 *                      comparision between two MPLS-TP MEPs 
 *                      This routine is called from both CC and LBLT context.
 *
 * Input(s)           : pMep1MplsParams - Pointer to MEP-1 MPLS Path Params.
 *                      pMep2MplsParams - Pointer to MEP-2 MPLS Path Params.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : returns -1 - if pMep1MplsParams < pMep2MplsParams
 *                      returns  1 - if pMep1MplsParams > pMep2MplsParams
 *                      otherwise returns 0
 *
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpMepMplsPathCmp (tEcfmMplsParams * pMep1MplsParams,
                        tEcfmMplsParams * pMep2MplsParams)
{
    if (pMep1MplsParams->u1MplsPathType != pMep2MplsParams->u1MplsPathType)
    {
        /* Type of the two nodes differ. One of them monitors LSP and other
         * PW
         */
        if (pMep1MplsParams->u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    /* Nodes are of the same type,.i.e both are either LSP or PW */
    switch (pMep1MplsParams->u1MplsPathType)
    {
        case MPLS_PATH_TYPE_TUNNEL:

            if (pMep1MplsParams->MplsPathParams.MplsLspParams.
                u4TunnelId !=
                pMep2MplsParams->MplsPathParams.MplsLspParams.u4TunnelId)
            {
                if (pMep1MplsParams->MplsPathParams.
                    MplsLspParams.u4TunnelId >
                    pMep2MplsParams->MplsPathParams.MplsLspParams.u4TunnelId)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            if (pMep1MplsParams->MplsPathParams.MplsLspParams.
                u4TunnelInst !=
                pMep2MplsParams->MplsPathParams.MplsLspParams.u4TunnelInst)
            {
                if (pMep1MplsParams->MplsPathParams.
                    MplsLspParams.u4TunnelInst >
                    pMep2MplsParams->MplsPathParams.MplsLspParams.u4TunnelInst)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            if (pMep1MplsParams->MplsPathParams.MplsLspParams.
                u4SrcLer !=
                pMep2MplsParams->MplsPathParams.MplsLspParams.u4SrcLer)
            {
                if (pMep1MplsParams->MplsPathParams.
                    MplsLspParams.u4SrcLer >
                    pMep2MplsParams->MplsPathParams.MplsLspParams.u4SrcLer)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            if (pMep1MplsParams->MplsPathParams.MplsLspParams.
                u4DstLer !=
                pMep2MplsParams->MplsPathParams.MplsLspParams.u4DstLer)
            {
                if (pMep1MplsParams->MplsPathParams.
                    MplsLspParams.u4DstLer >
                    pMep2MplsParams->MplsPathParams.MplsLspParams.u4DstLer)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            return 0;

        case MPLS_PATH_TYPE_PW:

            if (pMep1MplsParams->MplsPathParams.u4PswId >
                pMep2MplsParams->MplsPathParams.u4PswId)
            {
                return 1;
            }
            else if (pMep1MplsParams->MplsPathParams.u4PswId <
                     pMep2MplsParams->MplsPathParams.u4PswId)
            {
                return -1;
            }
            return 0;

        default:
            return 0;
    }
}

/******************************************************************************
 * Name               : EcfmMpTpCcMepMplsPathCmp
 *
 * Description        : This routine is used to perform the MPLS path parameter
 *                      comparison between two MPLS-TP based MEPs maintained in
 *                      CC task.
 *
 * Input(s)           : pE1 - Pointer to CC MEP Information Block(1).
 *                      pE2 - pointer to CC MEP Information Block(2).
 *
 * Output(s)          : None
 *
 * Return Value(s)    : returns -1 - if pE1 (MplsParams) < pE2 (MplsParams)
 *                      returns  1 - if pE1 (MplsParams) > PE2 (MplsParams)
 *                      otherwise returns 0
 *
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpCcMepMplsPathCmp (tRBElem * pE1, tRBElem * pE2)
{
    return (EcfmMpTpMepMplsPathCmp (((tEcfmCcMepInfo *) pE1)->pEcfmMplsParams,
                                    ((tEcfmCcMepInfo *) pE2)->pEcfmMplsParams));
}

/******************************************************************************
 * Name               : EcfmMpTpLbLtMepMplsPathCmp
 *
 * Description        : This routine is used to perform the MPLS path parameter
 *                      comparison between two MPLS-TP based MEPs maintained in
 *                      LBLT task.
 *
 * Input(s)           : pE1 - Pointer to LBLT MEP Information Block(1).
 *                      pE2 - pointer to LBLT MEP Information Block(2).
 *
 * Output(s)          : None
 *
 * Return Value(s)    : returns -1 - if pE1 (MplsParams) < pE2 (MplsParams)
 *                      returns  1 - if pE1 (MplsParams) > PE2 (MplsParams)
 *                      otherwise returns 0
 *
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpLbLtMepMplsPathCmp (tRBElem * pE1, tRBElem * pE2)
{
    return (EcfmMpTpMepMplsPathCmp (((tEcfmLbLtMepInfo *) pE1)->pEcfmMplsParams,
                                    ((tEcfmLbLtMepInfo *) pE2)->
                                    pEcfmMplsParams));
}

/*****************************************************************************
 *                                                                           
 * Function     : EcfmSendPktToMpls                                          
 *                                                                           
 * Description  : This function is used to call external OAM function to     
 *                give the packet to MPLS-RTR to forward                     
 *                                                                           
 * Input        : u4ContextId - Context Id                                   
 *                u4IfIndex   - Interface Index                             
 *                pu1DestMac  - Contains Next hop MAC address
 *                pBuf        - Pointer to the packet CRU Buffer             
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : ECFM_SUCCESS/ECFM_FAILURE                                  
 *                                                                           
 *****************************************************************************/
PUBLIC INT4
EcfmSendPktToMpls (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DestMac,
                   tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1OpCode)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    UINT4               u4PduSize = 0;
    INT4                i4RetVal = 0;

    switch (u1OpCode)
    {
        case ECFM_OPCODE_CCM:
        case ECFM_OPCODE_LMM:
        case ECFM_OPCODE_LMR:
            /* Allocate memory for the Input structure to
             * MPLS module */
            pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
                (ECFM_MPLSTP_INPARAMS_POOLID);
            if (pMplsApiInInfo == NULL)
            {
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                              "Mempool allocation failed\r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            /* Allocate memory for the Output structure from
             * MPLS module */
            pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
                (ECFM_MPLSTP_OUTPARAMS_POOLID);
            if (pMplsApiOutInfo == NULL)
            {
                MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                              "Mempool allocation failed\r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }
            break;
        case ECFM_OPCODE_LBM:
        case ECFM_OPCODE_LBR:
        case ECFM_OPCODE_1DM:
        case ECFM_OPCODE_DMM:
        case ECFM_OPCODE_DMR:
            /* Allocate memory for the Input structure to
             * MPLS module */
            pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
                (ECFM_MPTP_LBLT_INPARAMS_POOLID);
            if (pMplsApiInInfo == NULL)
            {
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                              "Mempool allocation failed\r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            /* Allocate memory for the Output structure from
             * MPLS module */
            pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
                (ECFM_MPTP_LBLT_OUTPARAMS_POOLID);
            if (pMplsApiOutInfo == NULL)
            {
                MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmSendPktToMpls: "
                              "Mempool allocation failed\r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }
            break;
        default:
            return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->u4ContextId = u4ContextId;
    pMplsApiInInfo->InPktInfo.u4OutIfIndex = u4IfIndex;
    pMplsApiInInfo->InPktInfo.bIsUnNumberedIf = FALSE;
    pMplsApiInInfo->InPktInfo.bIsMplsLabelledPacket = TRUE;

    MEMCPY (pMplsApiInInfo->InPktInfo.au1DstMac, pu1DestMac, MAC_ADDR_LEN);

    /* Point the buffer to input structure */
    pMplsApiInInfo->InPktInfo.pBuf = pBuf;
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    UNUSED_PARAM (u4PduSize);
    /* Send the packet to MPLS-RTR module for MPLS forwarding */
    i4RetVal = EcfmMplsHandleExtInteraction (MPLS_PACKET_HANDLE_FROM_APP,
                                             pMplsApiInInfo, pMplsApiOutInfo);

    if ((u1OpCode == ECFM_OPCODE_CCM) ||
        (u1OpCode == ECFM_OPCODE_LMM) || (u1OpCode == ECFM_OPCODE_LMR))
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    }
    else if ((u1OpCode == ECFM_OPCODE_LBM) ||
             (u1OpCode == ECFM_OPCODE_LBR) ||
             (u1OpCode == ECFM_OPCODE_1DM) ||
             (u1OpCode == ECFM_OPCODE_DMM) || (u1OpCode == ECFM_OPCODE_DMR))
    {
        MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPTP_LBLT_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmMplsRegCallBack
 *
 * Description        : This routine is used to register a callback function
 *                      with MPLS module, to receive Y.1731 PDUs and MPLS-TP
 *                      transport path status change notifications.
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplsRegCallBack (VOID)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_INPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSendPktToMpls: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));

    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->u4SubReqType = MPLS_APP_REG_CALL_BACK;
    pMplsApiInInfo->InRegParams.u4ModId = MPLS_Y1731_APP_ID;
    pMplsApiInInfo->InRegParams.u4Events =
        (MPLS_TNL_UP_EVENT | MPLS_TNL_DOWN_EVENT | MPLS_PW_UP_EVENT |
         MPLS_PW_DOWN_EVENT | MPLS_Y1731_PACKET);
    pMplsApiInInfo->InRegParams.pFnRcvPkt = EcfmApiMplsCallBack;

    /* Register Call back function with MPLS module */
    if (EcfmMplsHandleExtInteraction (MPLS_OAM_REG_APP_FOR_NOTIF,
                                      pMplsApiInInfo, NULL) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        return ECFM_SUCCESS;
    }
    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmRegMplsTpPath
 *
 * Description        : This routine is used to register MPLS-TP transport
 *                      path for receiving status change notification.
 *
 * Input(s)           : bEcfmRegMplsPath - TRUE/FALSE to register/deregister
 *                      pMepInfo - Pointer to the MEP information structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmRegMplsTpPath (BOOL1 bEcfmRegMplsPath, tEcfmCcMepInfo * pMepInfo)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;

    if ((pMepInfo == NULL) || (pMepInfo->pEcfmMplsParams) == NULL)
    {
        return ECFM_FAILURE;
    }

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_INPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSendPktToMpls: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));

    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->u4ContextId = pMepInfo->u4ContextId;
    pMplsApiInInfo->u4SubReqType = MPLS_APP_REG_PATH_FOR_NOTIF;

    if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;

        /* Fill the path id with PW VC ID */
        pMplsApiInInfo->InPathId.PwId.u4VcId =
            pMepInfo->pEcfmMplsParams->MplsPathParams.u4PswId;
    }
    else
    {
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

        /* Fill the MPLS-TP tunnel identifier information */
        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pMepInfo->pEcfmMplsParams->MplsPathParams.MplsLspParams.u4TunnelId;

        pMplsApiInInfo->InPathId.TnlId.u4LspNum =
            pMepInfo->pEcfmMplsParams->MplsPathParams.MplsLspParams.
            u4TunnelInst;

        pMplsApiInInfo->InPathId.TnlId.
            SrcNodeId.MplsRouterId.u4_addr[0] =
            pMepInfo->pEcfmMplsParams->MplsPathParams.MplsLspParams.u4SrcLer;

        pMplsApiInInfo->InPathId.TnlId.
            DstNodeId.MplsRouterId.u4_addr[0] =
            pMepInfo->pEcfmMplsParams->MplsPathParams.MplsLspParams.u4DstLer;
    }

    pMplsApiInInfo->InY1731OamStatus = bEcfmRegMplsPath;

    /* Register MPLS path to receive status change notification */
    if (EcfmMplsHandleExtInteraction (MPLS_OAM_REG_APP_FOR_NOTIF,
                                      pMplsApiInInfo, NULL) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        return ECFM_FAILURE;
    }

    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmMplsGetBaseServiceOID
 *
 * Description        : This routine is used to fetch the base MPLS
 *                      service pointer OID.
 *
 * Input(s)           : u4ContextId - Context Id
 *                      u1PathType - MPLS path type - Tunnel/PW
 *
 * Output(s)          : pServicePtr - pointer to the service pointer
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplsGetBaseServiceOID (UINT4 u4ContextId, UINT1 u1PathType,
                           tServicePtr * pServicePtr)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_INPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSendPktToMpls: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Allocate memory for the Output structure from
     * MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (ECFM_MPLSTP_OUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSendPktToMpls: " "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
    pMplsApiInInfo->u4ContextId = u4ContextId;

    if (u1PathType == MPLS_PATH_TYPE_PW)
    {
        pMplsApiInInfo->u4SubReqType = MPLS_GET_BASE_PW_OID;
    }
    else
    {
        pMplsApiInInfo->u4SubReqType = MPLS_GET_BASE_TNL_OID;
    }

    /* Fetch Base service pointer information from MPLS module */
    if (EcfmMplsHandleExtInteraction (MPLS_GET_SERVICE_POINTER_OID,
                                      pMplsApiInInfo,
                                      pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
        return ECFM_FAILURE;
    }

    MEMCPY (pServicePtr->au4ServicePtr,
            pMplsApiOutInfo->OutServiceOid.au4ServiceOidList,
            (pMplsApiOutInfo->OutServiceOid.u2ServiceOidLen * sizeof (UINT4)));

    pServicePtr->u4OidLength = pMplsApiOutInfo->OutServiceOid.u2ServiceOidLen;

    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID, (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                        (UINT1 *) pMplsApiOutInfo);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmMplsValidateServiceOID
 *
 * Description        : This routine is used to validate the base MPLS
 *                      service pointer OID.
 *
 * Input(s)           : u4ContextId - Context Id
 *                      u1PathType - MPLS path type - Tunnel/PW
 *                      pServicePtr - pointer to the service pointer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplsValidateServiceOID (UINT4 u4ContextId, UINT1 u1PathType,
                            tServicePtr * pServicePtr)
{
    tServicePtr        *pBaseServicePtr;
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    /* Allocate Memory for Service Ptr Structure */
    pBaseServicePtr = (tServicePtr *) MemAllocMemBlk
        (ECFM_MPLSTP_SERVICE_PTR_POOLID);

    if (pBaseServicePtr == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsValidateServiceOID: "
                     "Mempool allocation failed\r\n");
        return ECFM_FAILURE;
    }

    MEMSET (pBaseServicePtr, 0, sizeof (tServicePtr));

    if (EcfmMplsGetBaseServiceOID (u4ContextId, u1PathType, pBaseServicePtr)
        == ECFM_FAILURE)
    {
        /* Fetching base service pointer from MPLS module failed */
        MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                            (UINT1 *) pBaseServicePtr);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsValidateServiceOID: "
                     "MPLS-TP Service Pointer get failed\r\n");
        return ECFM_FAILURE;
    }

    if (MEMCMP (pServicePtr->au4ServicePtr, pBaseServicePtr->au4ServicePtr,
                (pBaseServicePtr->u4OidLength * sizeof (UINT4))) != 0)
    {
        /* Releasing memory for Base Service Pointer */
        MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                            (UINT1 *) pBaseServicePtr);
        /* Invalid Service Pointer */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsValidateServiceOID: "
                     "MPLS-TP Service Pointer comparision got failed\r\n");
        return ECFM_FAILURE;
    }

    /* for the selector_type LSP check if the OAM is established at MPLS as well
       if it is established return error */
    /* Fetch MPLS Tunnel path information from MPLS module */

    if (u1PathType == MPLS_PATH_TYPE_TUNNEL)
    {
        /* Allocate memory for the Input structure to
           MPLS module */
        pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
            (ECFM_MPLSTP_INPARAMS_POOLID);
        if (pMplsApiInInfo == NULL)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                                (UINT1 *) pBaseServicePtr);
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmGetMplsPathInfo: "
                         "Mempool allocation failed\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }

        /* Allocate memory for the Output structure from
           MPLS module */
        pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
            (ECFM_MPLSTP_OUTPARAMS_POOLID);
        if (pMplsApiOutInfo == NULL)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                                (UINT1 *) pBaseServicePtr);
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmGetMplsPathInfo: "
                         "Mempool allocation failed\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }

        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = Y1731_MODULE;

        /* Path params are already filled above */
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength];
        pMplsApiInInfo->InPathId.TnlId.u4LspNum =
            pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength + 1];
        pMplsApiInInfo->InPathId.TnlId.
            SrcNodeId.MplsRouterId.u4_addr[0] =
            pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength + 2];
        pMplsApiInInfo->InPathId.TnlId.
            DstNodeId.MplsRouterId.u4_addr[0] =
            pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength + 3];

        if (EcfmMplsHandleExtInteraction (MPLS_GET_TUNNEL_INFO,
                                          pMplsApiInInfo,
                                          pMplsApiOutInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                                (UINT1 *) pBaseServicePtr);
            return ECFM_FAILURE;
        }
        if ((pMplsApiOutInfo->OutTeTnlInfo.MplsMegId.u4MegIndex != 0) ||
            (pMplsApiOutInfo->OutTeTnlInfo.MplsMegId.u4MpIndex != 0) ||
            (pMplsApiOutInfo->OutTeTnlInfo.MplsMegId.u4MeIndex != 0))
        {
            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                                (UINT1 *) pBaseServicePtr);
            CLI_SET_ERR (CLI_ECFM_TUNNEL_ASSOC_WITH_MPLS_OAM);
            return ECFM_FAILURE;

        }

        MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    }

    /* Releasing memory for Base Service Pointer */
    MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                        (UINT1 *) pBaseServicePtr);

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmMplsGetOidFromPathId
 *
 * Description        : This routine is used to form the service pointer for
 *                      MPLS path ID given
 *
 * Input(s)           : u4ContextId - Context Id
 *                      pMplsPathId - pointer to the MPLS Path identifier
 *
 * Output(s)          : pServicePtr - pointer to the service pointer OID
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMplsGetOidFromPathId (UINT4 u4ContextId,
                          tEcfmMplsParams * pMplsPathId,
                          tServicePtr * pServicePtr)
{
    UINT4               u4PwIndex = 0;
    tServicePtr        *pBaseServicePtr;

    /* Allocate Memory for Service Ptr Structure */
    pBaseServicePtr = (tServicePtr *) MemAllocMemBlk
        (ECFM_MPLSTP_SERVICE_PTR_POOLID);

    if (pBaseServicePtr == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsGetOidFromPathId: "
                     "Mempool allocation failed\r\n");
        return ECFM_FAILURE;
    }

    MEMSET (pBaseServicePtr, 0, sizeof (tServicePtr));

    if (pMplsPathId == NULL)
    {
        /* MPLS path information is NULL */
        MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                            (UINT1 *) pBaseServicePtr);
        return ECFM_FAILURE;
    }

    if (EcfmMplsGetBaseServiceOID (u4ContextId, pMplsPathId->u1MplsPathType,
                                   pBaseServicePtr) == ECFM_FAILURE)
    {
        /* Fetching base service pointer from MPLS module failed */
        MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                            (UINT1 *) pBaseServicePtr);
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMplsGetOidFromPathId: "
                     "MPLS-TP Service Pointer get failed\r\n");
        return ECFM_FAILURE;
    }

    /* Copy the base service pointer OID */
    MEMCPY (pServicePtr->au4ServicePtr, pBaseServicePtr->au4ServicePtr,
            ((pBaseServicePtr->u4OidLength) * sizeof (UINT4)));

    /* Fill the indices to form the complete OID */
    if (pMplsPathId->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        if (EcfmMplsGetPwIndex (pMplsPathId->MplsPathParams.u4PswId, &u4PwIndex)
            != ECFM_SUCCESS)
        {
            /* Releasing memory for Base Service Pointer */
            MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                                (UINT1 *) pBaseServicePtr);

            ECFM_CC_TRC_ARG1 (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                              "\tUnable to retrieve the pwIndex to pwTable correspondig to "
                              "pwID(VC-ID): [%u]\r\n",
                              pMplsPathId->MplsPathParams.u4PswId);
            return ECFM_FAILURE;
        }
        pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength] = u4PwIndex;

        pServicePtr->u4OidLength = (pBaseServicePtr->u4OidLength + 1);
    }
    else
    {
        pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength] =
            pMplsPathId->MplsPathParams.MplsLspParams.u4TunnelId;
        pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength + 1] =
            pMplsPathId->MplsPathParams.MplsLspParams.u4TunnelInst;
        pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength + 2] =
            pMplsPathId->MplsPathParams.MplsLspParams.u4SrcLer;
        pServicePtr->au4ServicePtr[pBaseServicePtr->u4OidLength + 3] =
            pMplsPathId->MplsPathParams.MplsLspParams.u4DstLer;

        pServicePtr->u4OidLength = (pBaseServicePtr->u4OidLength + 4);
    }
    /* Releasing memory for Base Service Pointer */
    MemReleaseMemBlock (ECFM_MPLSTP_SERVICE_PTR_POOLID,
                        (UINT1 *) pBaseServicePtr);

    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    FUNCTION NAME    : EcfmProcessMpTpPdu     
 *
 *    DESCRIPTION      : This function parse the MPLS Labels/Headers and
 *                       processes the Y.1731 PDU (CC/LBLT) received over
 *                       the MPLS-TP networks. This routine invokes the
 *                       sub-routines to achieve the PDU reception.
 *                       This routine is the common utility for both the 
 *                       CC and LBLT tasks.
 *                       
 *    INPUT            : pBuf - Pointer to the Buffer containing the PDU.     
 *                       u4IfIndex - Interface index over which the PDU has
 *                                   been received.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
EcfmProcessMpTpPdu (tEcfmBufChainHeader * pBuf, UINT4 u4IfIndex)
{
    tEcfmMplsParams     EcfmMplsParams;
    UINT4               u4PktLen = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT1               u1RxOpcode = ECFM_INIT_VAL;

    ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC, "EcfmProcessMpTpPdu: "
                  "Y.1731 PDU received over MPLS-TP path \r\n");

    ECFM_MEMSET (&EcfmMplsParams, 0, sizeof (tEcfmMplsParams));

    u4PktLen = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);
    ECFM_GLB_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PktLen,
                       "EcfmProcessMpTpPdu: Dumping Y.1731 PDU received "
                       "from MPLS-TP path...\r\n");

    if (EcfmRxProcessMplsHeaders (pBuf, u4IfIndex, u4PktLen,
                                  &EcfmMplsParams) == ECFM_FAILURE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmProcessMpTpPdu: Unable to parse and process "
                      "the MPLS Labels/headers...\r\n");
        return ECFM_FAILURE;
    }

    /* Fetch the Opcode to process CC/LBLT task message */
    /* Move the offset to point to the opcode field */
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pBuf, u4Offset, u1RxOpcode);

    switch (u1RxOpcode)
    {
        case ECFM_OPCODE_CCM:
        case ECFM_OPCODE_LMM:
        case ECFM_OPCODE_LMR:
            if (EcfmMpTpCcProcessPdu (pBuf, &EcfmMplsParams) == ECFM_FAILURE)
            {
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmProcessMpTpPdu: "
                              "Unable to process the Y1731 PDU...\r\n");
                return ECFM_FAILURE;
            }
            ECFM_CC_INCR_CTX_RX_CFM_PDU_COUNT (u4ContextId);
            ECFM_CC_INCR_CTX_RX_COUNT (u4ContextId, u1RxOpcode);
            break;
        case ECFM_OPCODE_LBM:
        case ECFM_OPCODE_LBR:
        case ECFM_OPCODE_1DM:
        case ECFM_OPCODE_DMM:
        case ECFM_OPCODE_DMR:
            if (EcfmMpTpLbLtProcessPdu (pBuf, &EcfmMplsParams) == ECFM_FAILURE)
            {
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmProcessMpTpPdu: "
                              "Unable to process the Y1731 PDU...\r\n");
                return ECFM_FAILURE;
            }
            ECFM_LBLT_INCR_CTX_RX_CFM_PDU_COUNT (u4ContextId);
            ECFM_LBLT_INCR_CTX_RX_COUNT (u4ContextId, u1RxOpcode);
            break;
        default:
            ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmProcessMpTpPdu: "
                          "Unable to process the Y1731 PDU...\r\n");
            return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : EcfmMpTpCcProcessPdu
 *
 * Description        : This routine is used to process the Y.1731 CC PDU
 *                      received over the MPLS-TP networks. This routine
 *                      identifies the MEP information based on the LSP/PW
 *                      identifier.
 *
 * Input(s)           : pBuf        - Pointer to the CRU Buffer of the received PDU.
 *                      pMplsParams - Pointer to the LSP/PW ID.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpCcProcessPdu (tEcfmBufChainHeader * pBuf, tEcfmMplsParams * pMplsParams)
{
    PRIVATE tEcfmCcPduSmInfo PduSmInfo;
    tEcfmCcMaInfo       MaInfo;
    tEcfmMplsParams     MplsParams;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1MdLvlAndVer = 0;

    ECFM_CC_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PduSmInfo, 0, sizeof (tEcfmCcPduSmInfo));
    ECFM_MEMSET (&MaInfo, 0, sizeof (tEcfmCcMaInfo));
    ECFM_MEMSET (gpEcfmCcMepNode, 0, sizeof (tEcfmCcMepInfo));
    ECFM_MEMSET (&MplsParams, 0, sizeof (tEcfmMplsParams));

    ECFM_MEMCPY (&MplsParams, pMplsParams, sizeof (tEcfmMplsParams));

    gpEcfmCcMepNode->pEcfmMplsParams = &MplsParams;

    /* The above value works as the key. Search the RBTree to obtain the
     * exact MEP Information Block.
     */
    pMepInfo = RBTreeGet (ECFM_CC_MPLSMEP_TABLE, (tRBElem *) (gpEcfmCcMepNode));
    if (pMepInfo == NULL)
    {
        /* No such valid MEP exists for the received PDU. Hence drop the
         * PDU without processing further
         */
        return ECFM_FAILURE;
    }
    pMaInfo = pMepInfo->pMaInfo;
    PduSmInfo.pBuf = pBuf;
    /* The following information needs to be pushed into the PduSmInfo
     * structure for further processing.
     * PortInfo - Dummy portinfo associated for all the MEPs running over
     *            LSP/PW
     * MepInfo - Will be obtained from the MA information block
     * MdInfo  - Will be obtained from the MA information block
     * MdLevel - MD Level in the PDU
     * Opcode  - Value of the opcode present in the PDU.
     */

    PduSmInfo.pMepInfo = pMepInfo;

    /* Assign Dummy MPLS Port */
    PduSmInfo.pPortInfo = pMepInfo->pPortInfo;
    PduSmInfo.pMdInfo = pMaInfo->pMdInfo;

    ECFM_CRU_GET_1_BYTE (pBuf, 0, u1MdLvlAndVer);

    PduSmInfo.u1RxMdLevel = ECFM_GET_MDLEVEL (u1MdLvlAndVer);
    PduSmInfo.u1RxVersion = ECFM_GET_VERSION (u1MdLvlAndVer);
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE ((PduSmInfo.pBuf), u4Offset, PduSmInfo.u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flag from Received PDU */
    ECFM_CRU_GET_1_BYTE (PduSmInfo.pBuf, u4Offset, PduSmInfo.u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (PduSmInfo.pBuf, u4Offset,
                         PduSmInfo.u1RxFirstTlvOffset);

    if (EcfmCcCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmMpTpCcProcessPdu: "
                     "EcfmCcCtrlRxLevelDeMux returned Failure\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : EcfmMpTpLbLtProcessPdu
 *
 * Description      : This routine is used to process the Y.1731 LbLt PDU
 *                    received over the MPLS-TP networks. This routine
 *                    identifies the MEP information based on the LSP/PW
 *                    identifier.
 *
 * Input(s)         : pBuf        - Pointer to the CRU Buffer of the received PDU.
 *                    pMplsParams - Pointer to the LSP/PW ID.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpLbLtProcessPdu (tEcfmBufChainHeader * pBuf,
                        tEcfmMplsParams * pMplsParams)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    tEcfmLbLtMaInfo     MaInfo;
    tEcfmMplsParams     MplsParams;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1MdLvlAndVer = 0;

    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PduSmInfo, 0, sizeof (tEcfmLbLtPduSmInfo));
    ECFM_MEMSET (&MaInfo, 0, sizeof (tEcfmLbLtMaInfo));
    ECFM_MEMSET (gpEcfmLbLtMepNode, 0, sizeof (tEcfmLbLtMepInfo));
    ECFM_MEMSET (&MplsParams, 0, sizeof (tEcfmMplsParams));
    ECFM_MEMSET (&PduSmInfo, 0, sizeof (tEcfmLbLtPduSmInfo));
    ECFM_MEMSET (&MaInfo, 0, sizeof (tEcfmLbLtMaInfo));
    ECFM_MEMSET (&MplsParams, 0, sizeof (tEcfmMplsParams));

    ECFM_MEMCPY (&MplsParams, pMplsParams, sizeof (tEcfmMplsParams));

    /* Fetch the MepInfo with the MplsParams information */
    gpEcfmLbLtMepNode->pEcfmMplsParams = &MplsParams;

    /* MplsParams works as a key to search the RBTree and obtains the
     * exact MEP Information from LBLT MEP Table.
     */
    pMepInfo = RBTreeGet (ECFM_LBLT_MPLSMEP_TABLE,
                          (tRBElem *) (gpEcfmLbLtMepNode));
    if (pMepInfo == NULL)
    {
        /* No such valid MEP exists for the received PDU. Hence drop the
         * PDU without processing further
         */
        return ECFM_FAILURE;
    }

    PduSmInfo.pBuf = pBuf;
    /* The following information needs to be pushed into the PduSmInfo
     * structure for further processing.
     * PortInfo - Dummy portinfo associated for all the MEPs running over
     *            LSP/PW
     * MepInfo  - Will be obtained from the MA information block
     * MdInfo   - Will be obtained from the MA information block
     * MdLevel  - MD Level in the PDU
     * Opcode   - Value of the opcode present in the PDU.
     */
    PduSmInfo.pMepInfo = pMepInfo;

    /* Assign Dummy MPLS Port */
    PduSmInfo.pPortInfo = pMepInfo->pPortInfo;

    ECFM_CRU_GET_1_BYTE (pBuf, 0, u1MdLvlAndVer);

    PduSmInfo.u1RxMdLevel = ECFM_GET_MDLEVEL (u1MdLvlAndVer);
    PduSmInfo.u1RxVersion = ECFM_GET_VERSION (u1MdLvlAndVer);
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE ((PduSmInfo.pBuf), u4Offset, PduSmInfo.u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flags from Received PDU */
    ECFM_CRU_GET_1_BYTE (PduSmInfo.pBuf, u4Offset, PduSmInfo.u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (PduSmInfo.pBuf, u4Offset,
                         PduSmInfo.u1RxFirstTlvOffset);

    if (EcfmLbLtCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmMpTpLbLtProcessPdu: "
                       "EcfmLbLtCtrlRxLevelDeMux returned Failure\r\n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmMpTpDelMaEntry
 *
 * Description        : This routine delets the MA information block into the
 *                      RBTree that maintains the MA information blocks over
 *                      MPLS TP network.
 *
 * Input(s)           : pMaNode - Pointer to the MA Information block.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCES/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpDelMaEntry (tEcfmCcMepInfo * pMepNode)
{

    if (pMepNode->pEcfmMplsParams == NULL)
    {
        /* This node does not contain MPLS Path parameters. Hence this ME is
         * on ethernet networks. Return safely from this point
         * */
        return ECFM_SUCCESS;
    }
    /* This node is for ME running over MPLS path,.i.e either LSP or PW. Add
     * the ME in the RBTree for  MPLS path
     */
    if (RBTreeRem (ECFM_CC_MPLSMEP_TABLE, (tRBElem *) pMepNode) == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "Unable to delete MA Info in the RB-Tree for "
                     "MPLS TP Path\r\n");
        return ECFM_FAILURE;
    }

    /* Delete the MEP entry from RB-Tree based LBLT MPLS-TP MEP table */
    if (EcfmLbLtUpdateMepToMptpMepTbl (pMepNode, ECFM_DEL) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "Unable to delete MEP entry from the RB-Tree based LBLT "
                     "MPLS-TP MEP Table\r\n");
        return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmRxProcessMplsHeaders
 *
 * Description        : This routine is used to process the MPLS headers.
 *                      Strips the MPLS headers and fills the Path Id
 *                      information by querying MPLS module.
 *                      This is the utility function used from both CC 
 *                      and LBLT tasks
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet with 
 *                      MPLS headers
 *                      u4IfIndex - Incoming Interface Index
 *                      u4PktLen  - Length of the packet
 *
 * Output(s)          : pBuf - CRU Buffer Pointer to the packet without
 *                      MPLS headers
 *                      pMplsPathId - pointer to the MPLS path identifier
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmRxProcessMplsHeaders (tEcfmBufChainHeader * pBuf, UINT4 u4IfIndex,
                          UINT4 u4PktLen, tEcfmMplsParams * pMplsPathId)
{
    tMplsHdr            MplsHdr;
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    UINT4               u4OffSet = ECFM_INIT_VAL;
    UINT4               u4PathType = ECFM_INIT_VAL;
    UINT4               u4PrevLabel = ECFM_INIT_VAL;
    UINT4               u4InLabel = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT1               u1RxOpcode = ECFM_INIT_VAL;

    ECFM_GLB_TRC (u4ContextId, ECFM_FN_ENTRY_TRC,
                  "EcfmRxProcessMplsHeaders: " "Process MPLS Headers\r\n");
    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));

    do
    {
        EcfmRxGetMplsHdr (pBuf, &MplsHdr);
        u4OffSet += MPLS_HDR_LEN;

        if (MplsHdr.SI == 1)
        {
            if (MplsHdr.u4Lbl == CFM_GAL_LABEL)
            {
                /* GAL Label */
                u4PathType = MPLS_PATH_TYPE_TUNNEL;
            }
            else
            {
                /* PW label */
                u4PathType = MPLS_PATH_TYPE_PW;
            }
        }

        u4PrevLabel = u4InLabel;
        u4InLabel = MplsHdr.u4Lbl;

        if (CRU_BUF_Move_ValidOffset (pBuf, MPLS_HDR_LEN) == CRU_FAILURE)
        {
            /* Unable to remove Mpls Header */
            ECFM_GLB_TRC (u4ContextId, ECFM_ALL_FAILURE_TRC |
                          ECFM_CONTROL_PLANE_TRC, "EcfmRxProcessMplsHeaders: "
                          "Unable to move the offset \r\n");
            return ECFM_FAILURE;
        }
    }
    while ((MplsHdr.SI != 1) && (u4OffSet < u4PktLen));

    if (u4OffSet >= u4PktLen)
    {
        /* Invalid Packet encapsulation. No SI bit set in MPLS Hdr */
        ECFM_GLB_TRC (u4ContextId, ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC, "EcfmRxProcessMplsHeaders: "
                      "Invalid packet encapsulation \r\n");
        return ECFM_FAILURE;
    }

    /* Move the pointer to start of Y.1731 PDU */
    if (CRU_BUF_Move_ValidOffset (pBuf, MPLS_HDR_LEN) == CRU_FAILURE)
    {
        /* Unable to remove Mpls Header */
        ECFM_GLB_TRC (u4ContextId, ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC, "EcfmRxProcessMplsHeaders: "
                      "unable to move to Y.1731 PDU\r\n");
        return ECFM_FAILURE;
    }

    if (u4PathType == MPLS_PATH_TYPE_TUNNEL)
    {
        /* Copy tunnel Label. 
         * Current label is GAL for tunnel, copy the previous label */
        u4InLabel = u4PrevLabel;
    }

    /* Move the offset to point to the opcode field */
    u4OffSet = ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pBuf, u4OffSet, u1RxOpcode);

    switch (u1RxOpcode)
    {
        case ECFM_OPCODE_CCM:
        case ECFM_OPCODE_LMM:
        case ECFM_OPCODE_LMR:
            /* Allocate memory for the Input structure to MPLS module */
            pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
                (ECFM_MPLSTP_INPARAMS_POOLID);
            if (pMplsApiInInfo == NULL)
            {
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Mempool allocation failed for pMplsApiInInfo \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
            /* Allocate memory for the Output structure from MPLS module */
            pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
                (ECFM_MPLSTP_OUTPARAMS_POOLID);
            if (pMplsApiOutInfo == NULL)
            {
                MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Mempool allocation failed for pMplsApiOutInfo \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));
            /* Fill the MPLS Input structure */
            pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
            pMplsApiInInfo->InInLblInfo.u4InIf = u4IfIndex;
            pMplsApiInInfo->InInLblInfo.u4Inlabel = u4InLabel;

            EcfmMplsHandleExtInteraction (MPLS_INCR_IN_PACKET_COUNT,
                                          pMplsApiInInfo, pMplsApiOutInfo);

            MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

            /* Get path information from the In label & interface */
            if (EcfmMplsHandleExtInteraction (MPLS_GET_PATH_FROM_INLBL_INFO,
                                              pMplsApiInInfo,
                                              pMplsApiOutInfo) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Getting the path information failed \r\n");
                return ECFM_FAILURE;
            }
            pMplsPathId->u1MplsPathType = pMplsApiOutInfo->u4PathType;

            if (pMplsApiOutInfo->u4PathType != u4PathType)
            {
                /* Invalid Path type or Label Encapsulation */
                MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Invalid PathType\r\n");
                return ECFM_FAILURE;
            }

            if (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_PW)
            {
                pMplsPathId->MplsPathParams.u4PswId =
                    pMplsApiOutInfo->OutPwInfo.MplsPwPathId.u4VcId;
            }
            else
            {
                if ((pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.u4NodeType
                     != MPLS_ADDR_TYPE_GLOBAL_NODE_ID) ||
                    (pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.DstNodeId.u4NodeType
                     != MPLS_ADDR_TYPE_GLOBAL_NODE_ID))
                {
                    /* Not an MPLS-TP tunnel */
                    MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                        (UINT1 *) pMplsApiOutInfo);
                    ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                                  ECFM_ALL_FAILURE_TRC,
                                  "EcfmRxProcessMplsHeaders: "
                                  "Invalid MPLS-TP Tunnel\r\n");
                    return ECFM_FAILURE;
                }

                pMplsPathId->MplsPathParams.MplsLspParams.u4TunnelId =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4SrcTnlNum;
                pMplsPathId->MplsPathParams.MplsLspParams.u4TunnelInst =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4LspNum;
                pMplsPathId->MplsPathParams.MplsLspParams.u4SrcLer =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum;
                pMplsPathId->MplsPathParams.MplsLspParams.u4DstLer =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4DstLocalMapNum;
            }

            MemReleaseMemBlock (ECFM_MPLSTP_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPLSTP_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
            break;
        case ECFM_OPCODE_LBM:
        case ECFM_OPCODE_LBR:
        case ECFM_OPCODE_1DM:
        case ECFM_OPCODE_DMM:
        case ECFM_OPCODE_DMR:
            /* Allocate memory for the Input structure to MPLS module */
            pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
                (ECFM_MPTP_LBLT_INPARAMS_POOLID);
            if (pMplsApiInInfo == NULL)
            {
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Mempool allocation failed for pMplsApiInInfo \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            MEMSET (pMplsApiInInfo, 0x00, sizeof (tMplsApiInInfo));
            /* Allocate memory for the Output structure from MPLS module */
            pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
                (ECFM_MPTP_LBLT_OUTPARAMS_POOLID);
            if (pMplsApiOutInfo == NULL)
            {
                MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Mempool allocation failed for pMplsApiOutInfo \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            MEMSET (pMplsApiOutInfo, 0x00, sizeof (tMplsApiOutInfo));
            /* Fill the MPLS Input structure */
            pMplsApiInInfo->u4SrcModId = Y1731_MODULE;
            pMplsApiInInfo->InInLblInfo.u4InIf = u4IfIndex;
            pMplsApiInInfo->InInLblInfo.u4Inlabel = u4InLabel;

            EcfmMplsHandleExtInteraction (MPLS_INCR_IN_PACKET_COUNT,
                                          pMplsApiInInfo, pMplsApiOutInfo);

            MEMSET (pMplsApiOutInfo, 0x00, sizeof (tMplsApiOutInfo));

            /* Get path information from the In label & interface */
            if (EcfmMplsHandleExtInteraction (MPLS_GET_PATH_FROM_INLBL_INFO,
                                              pMplsApiInInfo,
                                              pMplsApiOutInfo) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (ECFM_MPTP_LBLT_OUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Getting the path information failed \r\n");
                return ECFM_FAILURE;
            }
            pMplsPathId->u1MplsPathType = pMplsApiOutInfo->u4PathType;

            if (pMplsApiOutInfo->u4PathType != u4PathType)
            {
                /* Invalid Path type or Label Encapsulation */
                MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (ECFM_MPTP_LBLT_OUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
                ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC, "EcfmRxProcessMplsHeaders: "
                              "Invalid PathType\r\n");
                return ECFM_FAILURE;
            }

            if (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_PW)
            {
                pMplsPathId->MplsPathParams.u4PswId =
                    pMplsApiOutInfo->OutPwInfo.MplsPwPathId.u4VcId;
            }
            else
            {
                if ((pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.u4NodeType
                     != MPLS_ADDR_TYPE_GLOBAL_NODE_ID) ||
                    (pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.DstNodeId.u4NodeType
                     != MPLS_ADDR_TYPE_GLOBAL_NODE_ID))
                {
                    /* Not an MPLS-TP tunnel */
                    MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (ECFM_MPTP_LBLT_OUTPARAMS_POOLID,
                                        (UINT1 *) pMplsApiOutInfo);
                    ECFM_GLB_TRC (u4ContextId, ECFM_CONTROL_PLANE_TRC |
                                  ECFM_ALL_FAILURE_TRC,
                                  "EcfmRxProcessMplsHeaders: "
                                  "Invalid MPLS-TP Tunnel\r\n");
                    return ECFM_FAILURE;
                }

                pMplsPathId->MplsPathParams.MplsLspParams.u4TunnelId =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4SrcTnlNum;
                pMplsPathId->MplsPathParams.MplsLspParams.u4TunnelInst =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4LspNum;
                pMplsPathId->MplsPathParams.MplsLspParams.u4SrcLer =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum;
                pMplsPathId->MplsPathParams.MplsLspParams.u4DstLer =
                    pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4DstLocalMapNum;
            }

            MemReleaseMemBlock (ECFM_MPTP_LBLT_INPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (ECFM_MPTP_LBLT_OUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);

            break;
        default:
            return ECFM_FAILURE;
    }                            /* end of switch */

    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function     : EcfmRxGetMplsHdr                                            
 *                                                                           
 * Description  : Parse the MPLS Header from the given buffer                
 *                                                                           
 * Input        : pBuf   - Pointer to the buffer                             
 *                                                                           
 * Output       : pHdr   - pointer to tMplsHdr to parse the MPLS header      
 *                                                                           
 * Returns      : None                                                       
 *****************************************************************************/
PUBLIC VOID
EcfmRxGetMplsHdr (tEcfmBufChainHeader * pBuf, tMplsHdr * pHdr)
{
    UINT4               u4Hdr = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4Hdr, 0, MPLS_HDR_LEN);
    u4Hdr = OSIX_NTOHL (u4Hdr);
    pHdr->u4Lbl = (u4Hdr & 0xfffff000) >> 12;
    pHdr->Exp = (u4Hdr & 0x00000e00) >> 9;
    pHdr->SI = (u4Hdr & 0x00000100) >> 8;
    pHdr->Ttl = u4Hdr & 0x000000ff;

}

/*****************************************************************************
 * Name               : EcfmMpTpAddMaEntry
 *
 * Description        : This routine adds the MA information block into the    
 *                      RBTree that maintains the MA information blocks over   
 *                      MPLS TP network.
 *
 * Input(s)           : pMaNode - Pointer to the MA Information block.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCES/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmMpTpAddMaEntry (tEcfmCcMepInfo * pMepNode)
{
    if (pMepNode->pEcfmMplsParams == NULL)
    {
        /* This node does not contain MPLS Path parameters. Hence this ME is
         * on ethernet networks. Return safely from this point
         * */
        return ECFM_SUCCESS;
    }
    /* This node is for ME running over MPLS path,.i.e either LSP or PW. Add
     * the ME in the RBTree for  MPLS path
     */
    if (RBTreeAdd (ECFM_CC_MPLSMEP_TABLE, (tRBElem *) pMepNode) == RB_FAILURE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "Unable to Add MA Info in the RB Tree for "
                     "MPLS TP Path\r\n");
        return ECFM_FAILURE;
    }

    /* Add the MEP entry in RB-Tree based LBLT MPLS-TP MEP table */
    if (EcfmLbLtUpdateMepToMptpMepTbl (pMepNode, ECFM_ADD) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "Unable to add MEP entry in the RB-Tree based LBLT MPLS-TP"
                     " MEP Table\r\n");
        RBTreeRem (ECFM_CC_MPLSMEP_TABLE, pMepNode);
        return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    FUNCTION NAME    : EcfmCcProcessMplsPathStatusChg
 *
 *    DESCRIPTION      : This function operates on the path status change  
 *                       trigger obtained from the MPLS module for the given
 *                       LSP/PW specified in the pEcfmMplsParams. If this 
 *                       indicates a tunnel down, then ECFM triggers a LOC
 *                       for the LSP/PW and deletes the MEP from the hardware
 *                       if offloading is enabled. If offloading is disabled, it
 *                       simply raises an LOC for the MEPs over LSP/PW
 *                       
 *                       
 *    INPUT            : pEcfmMplsParams - Pointer to the MPLS Path Specific  
 *                                         parameters.
 *                       u1PathStatus    - Value indicating the status of the 
 *                                         path (MPLS_PATH_UP/MPLS_PATH_DOWN)
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcProcessMplsPathStatusChg (tEcfmMplsParams * pEcfmMplsParams,
                                UINT1 u1PathStatus)
{
    tEcfmMplsParams     MplsParams;
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pTempMepInfo = NULL;

    ECFM_MEMSET (&MplsParams, 0, sizeof (tEcfmMplsParams));
    ECFM_MEMSET (&MaInfo, 0, sizeof (tEcfmCcMaInfo));
    ECFM_MEMSET (gpEcfmCcMepNode, 0, sizeof (tEcfmCcMepInfo));

    ECFM_MEMCPY (&MplsParams, pEcfmMplsParams, sizeof (tEcfmMplsParams));

    gpEcfmCcMepNode->pEcfmMplsParams = &MplsParams;

    pTempMepInfo = RBTreeGet (ECFM_CC_MPLSMEP_TABLE,
                              (tRBElem *) gpEcfmCcMepNode);
    if (pTempMepInfo == NULL)
    {
        /* No such valid MA/ME exists for the received PDU. Hence drop the
         * PDU without processing further
         */
        return ECFM_SUCCESS;
    }

    switch (u1PathStatus)
    {
        case ECFM_MPLSTP_PATH_UP:

            if (pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                                  "Detected Path UP Status for the LSP "
                                  "%d:%d:%d:%d \r\n",
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4TunnelId,
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4TunnelInst,
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4SrcLer,
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4DstLer);
            }
            else
            {
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                  "Detected Path UP Status for the PW %d\r\n",
                                  pEcfmMplsParams->MplsPathParams.u4PswId);
            }
            pMaInfo = pTempMepInfo->pMaInfo;
            pMepInfo = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaInfo->MepTable));

            while (pMepInfo != NULL)
            {
                /* For all the MEPs present in this MA, handle the UP stats
                 * */
                if (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
                {
                    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                                      "Creating MEP in the H/W for MAID: %u, "
                                      "MEPID: %u....\r\n",
                                      pMepInfo->pMaInfo->u4MaIndex,
                                      pMepInfo->u2MepId);
                    if (EcfmCcmOffCreateTxRxForMep (pMepInfo) != ECFM_SUCCESS)
                    {
                        return ECFM_FAILURE;
                    }
                }

                pMepInfo = (tEcfmCcMepInfo *)
                    TMO_DLL_Next (&(pMaInfo->MepTable),
                                  &(pMepInfo->MepTableDllNode));
            }
            break;

        case ECFM_MPLSTP_PATH_DOWN:

            /* Raise LOC for all the RMEPs and delete the session from h/w
             * */
            if (pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                                  "Detected Path Down Status for the LSP "
                                  "%d:%d:%d:%d \r\n",
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4TunnelId,
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4TunnelInst,
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4SrcLer,
                                  pEcfmMplsParams->MplsPathParams.MplsLspParams.
                                  u4DstLer);
            }
            else
            {
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                  "Detected Path Down Status for the PW %d\r\n",
                                  pEcfmMplsParams->MplsPathParams.u4PswId);
            }
            pMaInfo = pTempMepInfo->pMaInfo;
            pMepInfo = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaInfo->MepTable));

            while (pMepInfo != NULL)
            {
                /* For all the MEPs present in this MA, handle the down stats */
                EcfmCcProcessMplsPathDown (pMepInfo);

                pMepInfo = (tEcfmCcMepInfo *)
                    TMO_DLL_Next (&(pMaInfo->MepTable),
                                  &(pMepInfo->MepTableDllNode));
            }
            break;

        default:

            return ECFM_FAILURE;
            break;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    FUNCTION NAME    : EcfmCcProcessMplsPathDown
 *
 *    DESCRIPTION      : This function operates on the path down status    
 *                       for the given MEP. This raises LOC for all the RMEPs
 *                       for the particular MEP and deletes the MEP from the
 *                       hardware.                                            
 *                       
 *    INPUT            : pEcfmMepInfo - Pointer to the MEP information        
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
EcfmCcProcessMplsPathDown (tEcfmCcMepInfo * pEcfmMepInfo)
{
    PRIVATE tEcfmCcPduSmInfo PduSmInfo;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;

    ECFM_MEMSET (&PduSmInfo, 0, sizeof (tEcfmCcPduSmInfo));

    pRMepInfo = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pEcfmMepInfo->RMepDb));
    while (pRMepInfo != NULL)
    {
        PduSmInfo.pRMepInfo = pRMepInfo;
        PduSmInfo.pMepInfo = pEcfmMepInfo;

        ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC,
                          "Generating LOC for the MEP, MAID: %u, "
                          "MEPID: %u & RMEPID: %u....\r\n",
                          pEcfmMepInfo->pMaInfo->u4MaIndex,
                          pEcfmMepInfo->u2MepId, pRMepInfo->u2MepId);

        EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);

        if (pEcfmMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
        {
            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                              "Deleting MEP from the H/W for MAID: %u, "
                              "MEPID: %u....\r\n",
                              pEcfmMepInfo->pMaInfo->u4MaIndex,
                              pEcfmMepInfo->u2MepId);

            EcfmCcmOffDeleteTxRxForMep (pEcfmMepInfo);
        }
        pRMepInfo = (tEcfmCcRMepDbInfo *)
            TMO_DLL_Next (&(pEcfmMepInfo->RMepDb), &(pRMepInfo->MepDbDllNode));
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMptpHandleBulkPathStatusInd 
 *
 *    Description      : This function is used to handle the message 
 *                       ECFM_MPLS_BULK_PATH_STATUS_IND received in
 *                       ECFM CC config queue from MPLS module.
 *
 *                       This function reads each Path Status information 
 *                       record in the message and invokes the Path Status 
 *                       change handling API for further processing along 
 *                       with the path status value.
 *
 *    INPUT            : pBuf - CRU Buffer containing the path info records.
 *                       u4ContextId - Context Id
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 *****************************************************************************/
PUBLIC INT4
EcfmMptpHandleBulkPathStatusInd (tEcfmBufChainHeader * pBuf, UINT4 u4ContextId)
{
    tEcfmMplsParams     MplsParams;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2NumRcvdRecords = ECFM_INIT_VAL;
    UINT2               u2NumProcRecords = ECFM_INIT_VAL;
    UINT1               u1PathStatus = ECFM_INIT_VAL;
    UINT1               u1Pathtype = ECFM_INIT_VAL;

    UNUSED_PARAM (u4ContextId);
    ECFM_MEMSET (&MplsParams, ECFM_INIT_VAL, sizeof (tEcfmMplsParams));

    /* Get the No. of path status information record present in the msg */
    ECFM_CRU_GET_2_BYTE (pBuf, ECFM_INIT_VAL, u2NumRcvdRecords);
    u4Offset = u4Offset + ECFM_VAL_2;

    /*Get the Path Status */
    ECFM_CRU_GET_1_BYTE (pBuf, u4Offset, u1PathStatus);
    u4Offset = u4Offset + ECFM_VAL_1;
    u1PathStatus = ((u1PathStatus == MPLS_PATH_STATUS_UP) ?
                    ECFM_MPLSTP_PATH_UP : ECFM_MPLSTP_PATH_DOWN);
    /* Get the record information from the message and process the individual 
     * records
     */
    for (u2NumProcRecords = ECFM_VAL_0;
         u2NumProcRecords < u2NumRcvdRecords; u2NumProcRecords++)
    {
        /* Get the Path Type (LSP/PW) */
        ECFM_CRU_GET_1_BYTE (pBuf, u4Offset, u1Pathtype);
        u4Offset = u4Offset + ECFM_VAL_1;

        if (u1Pathtype == MPLS_PATH_TYPE_PW)
        {
            ECFM_CRU_GET_4_BYTE (pBuf, u4Offset,
                                 MplsParams.MplsPathParams.u4PswId);
            u4Offset = u4Offset + ECFM_VAL_4;

            if (EcfmCcProcessMplsPathStatusChg (&MplsParams, u1PathStatus)
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG1 (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                  "EcfmMptpHandleBulkPathStatusInd: Processing of"
                                  "Path Status update for PW Index [%u] failed \r\n",
                                  MplsParams.MplsPathParams.u4PswId);
                return ECFM_FAILURE;
            }
        }
        else if (u1Pathtype == MPLS_PATH_TYPE_TUNNEL)
        {

            ECFM_CRU_GET_4_BYTE (pBuf, u4Offset,
                                 MplsParams.MplsPathParams.MplsLspParams.
                                 u4TunnelId);
            u4Offset = u4Offset + ECFM_VAL_4;

            ECFM_CRU_GET_4_BYTE (pBuf, u4Offset,
                                 MplsParams.MplsPathParams.MplsLspParams.
                                 u4TunnelInst);
            u4Offset = u4Offset + ECFM_VAL_4;

            ECFM_CRU_GET_4_BYTE (pBuf, u4Offset,
                                 MplsParams.MplsPathParams.MplsLspParams.
                                 u4SrcLer);
            u4Offset = u4Offset + ECFM_VAL_4;

            ECFM_CRU_GET_4_BYTE (pBuf, u4Offset,
                                 MplsParams.MplsPathParams.MplsLspParams.
                                 u4DstLer);
            u4Offset = u4Offset + ECFM_VAL_4;

            if (EcfmCcProcessMplsPathStatusChg (&MplsParams, u1PathStatus)
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmMptpHandleBulkPathStatusInd: Processing of "
                                  "Path Status update for LSP with Indices's TnlId [%u] "
                                  "TnlInst [%u], Src [%u], Dest [%u] failed \r\n",
                                  MplsParams.MplsPathParams.MplsLspParams.
                                  u4TunnelId,
                                  MplsParams.MplsPathParams.MplsLspParams.
                                  u4TunnelInst,
                                  MplsParams.MplsPathParams.MplsLspParams.
                                  u4SrcLer,
                                  MplsParams.MplsPathParams.MplsLspParams.
                                  u4DstLer);
                return ECFM_FAILURE;
            }
        }
    }

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmMpTpLbValidateLbm
 *
 * Description        : This routine is used to validate the received LBM for
 *                      Target Mep/MIP Id TLV and Requesting MEP ID TLV
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the
 *                      information regarding mp info, the PDU if received
 *                      and other information related to the functionality.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmMpTpLbValidateLbm (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1              *pu1PduStart = NULL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    UINT2               u2TlvLength = ECFM_INIT_VAL;
    UINT2               u2RMepId = ECFM_INIT_VAL;
    UINT1               u1LbIndication = ECFM_INIT_VAL;
    UINT1               u1TlvType = ECFM_INIT_VAL;
    UINT1               u1SubType = ECFM_INIT_VAL;
    BOOL1               b1TgtFlag = ECFM_FALSE;
    BOOL1               b1ReqFlag = ECFM_FALSE;
    BOOL1               b1DataFlag = ECFM_FALSE;
    BOOL1               b1TestFlag = ECFM_FALSE;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pLbInfo);

    /* MPLS-TP Related Variables */
    /* Get the Linear buffer from the CRU buffer */
    pBuf = pPduSmInfo->pBuf;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

    pu1Pdu = ECFM_GET_DATA_PTR_IF_LINEAR (pBuf,
                                          pPduSmInfo->u1CfmPduOffset,
                                          u4ByteCount);
    if (pu1Pdu == NULL)
    {
        pu1Pdu = ECFM_LBLT_PDU;
        ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
        if (ECFM_COPY_FROM_CRU_BUF (pBuf, pu1Pdu,
                                    pPduSmInfo->u1CfmPduOffset, u4ByteCount)
            == ECFM_CRU_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmMpTpLbValidateLbm: "
                           "Received MPLS-TP packet: Copy "
                           "From CRU Buffer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }
    pu1PduStart = pu1Pdu;

    /* 1. LBM PDU shall include ICC based Target MEP ID TLV if destType is MEP 
     * 2. LBM PDU shall include ICC based Target MIP ID TLV if destType is MIP
     * 3. LBM PDU shall include optional Requesting MEP ID TLV only when bi-directional 
     *    connectivity verification is done
     * 4. Received LBM PDU shall be dropped if the Requesting MEP ID TLV
     *    is present and the Loopback indication byte is not 0x00(1B)
     * 5. LBM PDU shall be accepted and shall respond to peer even 
     *    when the Requesting MEP ID TLV 
     * 6. LBM PDU shall include Target MEP ID TLV as the first TLV
     * 7. LBM PDU shall be dropped when Requesting MEP ID TLV if present doesn't 
     *    follow the Target MEP ID TLV respectively
     * 8. Received LBM packet shall be dropped when the MEP Id in the Target MEP Id 
     *    TLV is not matching its local MEP Identifier or the type is not equal to Target(0x21) 
     * 9. Received LBM packet shall be dropped when Requesting MEP Id TLV is present 
     *    along with test TLV. 
     * 10.Received LBM packet shall be dropped when the Target MIP Id TLV is
     *    present.    
     * 11.The Type field shall be changed from 0x21(Target MEP/MIP ID TLV) to
     *    0x22(Replying MEP ID TLV) 
     * 12.Received LBM PDU shall be dropped when MEP Id in the Requesting MEP Id
     *    TLV(if present) of the received LBM PDU is not the same as the configured
     *    remote MEP Identifier.    
     * 13. On reception of the LBM PDU with Target MEP/MIP ID with sub-type as
     *     0x01 or 0x02 (discovery Target Ingress/Egress MEP/MIP node), the packet
     *     shall be dropped.    
     */

    /* Move the pointer to 4 bytes(CFM Header) to fetch the TLV fields */
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;
    /* Move the pointer to 4 bytes(Transaction Id) */
    pu1Pdu = pu1Pdu + ECFM_CCM_SEQ_NUM_FIELD_SIZE;

    /* Get TLV Type */
    ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    while (u1TlvType != ECFM_END_TLV_TYPE)
    {
        if ((UINT4) ((pu1Pdu + ECFM_TLV_LENGTH_FIELD_SIZE) -
                     pu1PduStart) > u4ByteCount)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmMpTpLbValidateLbm: "
                           "Received malformed packet \r\n");
            return ECFM_FAILURE;
        }

        switch (u1TlvType)
        {
            case ECFM_TARGET_MPID_TLV_TYPE:
                if ((b1ReqFlag == ECFM_TRUE) || (b1DataFlag == ECFM_TRUE) ||
                    (b1TestFlag == ECFM_TRUE) || (b1TgtFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Malformed packet \r\n");
                    return ECFM_FAILURE;
                }
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmMpTpLbValidateLbm: "
                               "Received Target MEP/MIP ID TLV \r\n");

                /* Validate the TLV length */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                if (u2TlvLength != ECFM_TARGET_MPID_TLV_LENGTH)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Received invalid "
                                   "Target MEP/MIP ID TLV length \r\n");
                    return ECFM_FAILURE;
                }

                /* Check if the PDU is received with the required size 
                 * (including the next TLV type len (1 byte)) for processing.
                 */
                if ((UINT4) ((pu1Pdu + u2TlvLength + 1) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the Sub-Type 1byte (0x02) */
                ECFM_GET_1BYTE (u1SubType, pu1Pdu);
                if (u1SubType != ECFM_TARGET_MEPID_TLV_SUB_TYPE)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Received invalid "
                                   "Sub-Type in Target MEP/MIP ID TLV. "
                                   "If received Sub-Type is (0x03 "
                                   "for MIP ID TLV or 0x00 for discovery "
                                   "ingress and 0x01 for discovery Egress), "
                                   "drop the packet\r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the MEP ID */
                ECFM_GET_2BYTE (u2RMepId, pu1Pdu);
                if (u2RMepId != pMepInfo->u2MepId)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Received invalid "
                                   "MEP ID in Target MEP ID TLV \r\n");
                    return ECFM_FAILURE;
                }

                /* Move the pointer, such that it points to end of the TLV */
                pu1Pdu = pu1Pdu + (ECFM_TARGET_MPID_TLV_LENGTH -
                                   (ECFM_TLV_SUB_TYPE_SIZE +
                                    ECFM_MEP_ID_FIELD_SIZE));
                b1TgtFlag = ECFM_TRUE;
                break;
            case ECFM_REQ_MEPID_TLV_TYPE:
                if ((b1TgtFlag != ECFM_TRUE) || (b1DataFlag == ECFM_TRUE) ||
                    (b1TestFlag == ECFM_TRUE) || (b1ReqFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the TLV length */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                if (u2TlvLength != ECFM_REQ_MEPID_TLV_LENGTH)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Received invalid "
                                   "Request MEP ID TLV length \r\n");
                    return ECFM_FAILURE;
                }

                if ((UINT4) ((pu1Pdu + u2TlvLength + 1) -
                             pu1PduStart) > u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the Loopback Indiaction 1byte (0x00) */
                ECFM_GET_1BYTE (u1LbIndication, pu1Pdu);
                if (u1LbIndication != ECFM_REQ_MEPID_TLV_LBM_INDICATION)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Received Loopback "
                                   "indication is not valid \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the MEP ID */
                ECFM_GET_2BYTE (u2RMepId, pu1Pdu);

                if (EcfmLbLtUtilGetRMepDbEntry (pMepInfo->u4MdIndex,
                                                pMepInfo->u4MaIndex,
                                                pMepInfo->u2MepId,
                                                u2RMepId) == NULL)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Received invalid "
                                   "MEP ID in Requesting MEP ID TLV \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate MEGID */
                if (EcfmUtilCompareMEGID (pu1Pdu, pMepInfo->pCcMepInfo) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: MEGID does not match\r\n");
                    return ECFM_FAILURE;
                }

                pu1Pdu = pu1Pdu + ECFM_MEGID_FIELD_SIZE;
                /* Move the pointer to 2 bytes to skip the Reserved field */
                pu1Pdu = pu1Pdu + ECFM_REQ_MEPID_TLV_RESV_FIELD_SIZE;

                b1ReqFlag = ECFM_TRUE;
                break;
            case ECFM_TEST_TLV_TYPE:
                if ((b1TgtFlag != ECFM_TRUE) || (b1DataFlag == ECFM_TRUE) ||
                    (b1ReqFlag == ECFM_TRUE) || (b1TestFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Malformed Packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Collect the length to move the pointer to end of the TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                if ((UINT4) ((pu1Pdu + u2TlvLength) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                pu1Pdu = pu1Pdu + u2TlvLength;
                b1TestFlag = ECFM_TRUE;
                break;
            case ECFM_DATA_TLV_TYPE:
                if ((b1TgtFlag != ECFM_TRUE) || (b1TestFlag == ECFM_TRUE) ||
                    (b1DataFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: Malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Collect the length to move the pointer to end of the TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                if ((UINT4) ((pu1Pdu + u2TlvLength + 1) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbm: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                pu1Pdu = pu1Pdu + u2TlvLength;
                b1DataFlag = ECFM_TRUE;
                break;
            default:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmMpTpLbValidateLbm: Unknown TLV Type...\r\n");
                return ECFM_FAILURE;
        }                        /* end of switch */
        ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    }                            /* end of while */

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name    : EcfmMpTpLbValidateLbr
 *
 * Description      : This routine is used to validate received LBR.
 *                    Replying MEP ID TLV, Requesting MEP ID TLV
 *
 * Input(s)         : pPduSmInfo - Pointer to the structure that stores the
 *                    information regarding mp info, the PDU if received
 *                    and other information related to the functionality.
 *
 * Output(s)        : None
 *
 * Return Value(s)  : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmMpTpLbValidateLbr (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    UINT4               u4NodeId = ECFM_INIT_VAL;
    UINT4               u4IfNum = ECFM_INIT_VAL;
    UINT2               u2TlvLength = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT1               u1LbIndication = ECFM_INIT_VAL;
    UINT1               u1TlvType = ECFM_INIT_VAL;
    UINT1               u1SubType = ECFM_INIT_VAL;
    UINT1               au1Icc[ECFM_CARRIER_CODE_ARRAY_SIZE] = { 0 };
    BOOL1               b1ReplyFlag = ECFM_FALSE;
    BOOL1               b1ReqFlag = ECFM_FALSE;
    BOOL1               b1DataFlag = ECFM_FALSE;
    BOOL1               b1TestFlag = ECFM_FALSE;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);

    /* Get the linear buffer from the CRU buffer */
    pBuf = pPduSmInfo->pBuf;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

    pu1Pdu = ECFM_GET_DATA_PTR_IF_LINEAR (pBuf,
                                          pPduSmInfo->u1CfmPduOffset,
                                          u4ByteCount);
    if (pu1Pdu == NULL)
    {
        pu1Pdu = ECFM_LBLT_PDU;
        ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
        if (ECFM_COPY_FROM_CRU_BUF (pBuf, pu1Pdu,
                                    pPduSmInfo->u1CfmPduOffset, u4ByteCount)
            == ECFM_CRU_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmMpTpLbValidateLbr: "
                           "Received Bpdu Copy From CRU Buffer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }
    pu1PduStart = pu1Pdu;

    /* 1. LBR PDU shall be dropped if the Requesting MEP ID TLV is present and
     *    the Loopback indication byte is not 0x01(1B)
     * 2. LBR PDU shall include Replying MEP/MIP TLV as the first TLV   
     * 3. LBR PDU shall be dropped when Requesting MEP ID TLV if present
     *    doesn't follow the Replying MEP/MID TLV
     * 4. Received LBR packet shall be dropped when the MEP Id in the Replying
     *    MEP ID TLV is not matching the configured Remote MEP ID, Type is not
     *    equal to 0x22(Replying)   
     * 5. Type field shall be changed from 0x21(Target MEP/MIP ID TLV) to
     *    0x22(Replying MEP/MIP ID TLV)   
     * 6. Received LBR PDU shall be dropped when MEP Id in the Requesting MEP Id
     *    TLV(if present) of the received LBR PDU is not the same as the local MEP Id
     */

    /* Move the pointer to 4 bytes(CFM Header) to read the CFM PDU fields */
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Move the pointer to 4 bytes(Transaction Id) */
    pu1Pdu = pu1Pdu + ECFM_CCM_SEQ_NUM_FIELD_SIZE;

    /* Get TLV Type */
    ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    while (u1TlvType != ECFM_END_TLV_TYPE)
    {
        if ((UINT4) ((pu1Pdu + ECFM_TLV_LENGTH_FIELD_SIZE) -
                     pu1PduStart) > u4ByteCount)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmMpTpLbValidateLbr: "
                           "Received malformed packet \r\n");
            return ECFM_FAILURE;
        }

        switch (u1TlvType)
        {
            case ECFM_REPLY_MPID_TLV_TYPE:
                if ((b1ReqFlag == ECFM_TRUE) || (b1DataFlag == ECFM_TRUE) ||
                    (b1TestFlag == ECFM_TRUE) || (b1ReplyFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: Malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmMpTpLbValidateLbr:"
                               "Received Replying MEP/MIP ID TLV \r\n");

                /* Validate the TLV length */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                if (u2TlvLength != ECFM_REPLY_MPID_TLV_LENGTH)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMpTpLbValidateLbr:"
                                   "Invalid length received \r\n");
                    return ECFM_FAILURE;
                }

                if ((UINT4) ((pu1Pdu + u2TlvLength + 1) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the Sub-Type 1byte (0x02) */
                ECFM_GET_1BYTE (u1SubType, pu1Pdu);
                if (((u1SubType == ECFM_REPLY_MEPID_TLV_SUB_TYPE) ||
                     (u1SubType == ECFM_REPLY_MIPID_TLV_SUB_TYPE)) ==
                    ECFM_VAL_0)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMpTpLbValidateLbr:"
                                   "Invalid SubType received - Replying "
                                   "MEP/MIP ID TLV \r\n");
                    return ECFM_FAILURE;
                }

                if (u1SubType == ECFM_REPLY_MEPID_TLV_SUB_TYPE)
                {
                    /* Validate the MEP ID */
                    ECFM_GET_2BYTE (u2MepId, pu1Pdu);
                    if (u2MepId != pLbInfo->u2TxDestMepId)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmMpTpLbValidateLbr:"
                                       "MEP ID does not match - Replying MEPID TLV \r\n");
                        return ECFM_FAILURE;
                    }

                    /* Move the pointer, such that it points to end of the TLV */
                    pu1Pdu =
                        pu1Pdu + (ECFM_REPLY_MPID_TLV_LENGTH -
                                  (ECFM_TLV_SUB_TYPE_SIZE +
                                   ECFM_MEP_ID_FIELD_SIZE));
                }
                else            /* Replying MIP ID TLV */
                {
                    /* Get the ICC Code */
                    ECFM_GET_NBYTE (au1Icc, pu1Pdu, ECFM_CARRIER_CODE_SIZE);
                    if (MEMCMP (au1Icc, pLbInfo->au1LbmIcc,
                                ECFM_CARRIER_CODE_SIZE) != 0)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmMpTpLbValidateLbr:"
                                       "ICC code does not match \r\n");
                        return ECFM_FAILURE;
                    }

                    /* Get the Node-Id */
                    ECFM_GET_4BYTE (u4NodeId, pu1Pdu);
                    if (u4NodeId != pLbInfo->u4LbmNodeId)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmMpTpLbValidateLbr:"
                                       "Node Id does not match \r\n");
                        return ECFM_FAILURE;
                    }

                    /* Get the IF-Num */
                    ECFM_GET_4BYTE (u4IfNum, pu1Pdu);
                    if (u4IfNum != pLbInfo->u4LbmIfNum)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmMpTpLbValidateLbr:"
                                       "IFNum does not match \r\n");
                        return ECFM_FAILURE;
                    }
                    /* Move the pointer, such that it points to end of the TLV */
                    pu1Pdu =
                        pu1Pdu + (ECFM_REPLY_MPID_TLV_LENGTH -
                                  ECFM_REPLY_MIP_ID_FIELD_SIZE);
                }
                b1ReplyFlag = ECFM_TRUE;
                break;
            case ECFM_REQ_MEPID_TLV_TYPE:
                if ((b1ReplyFlag != ECFM_TRUE) || (b1DataFlag == ECFM_TRUE) ||
                    (b1TestFlag == ECFM_TRUE) || (b1ReqFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: Malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the TLV length */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                if (u2TlvLength != ECFM_REQ_MEPID_TLV_LENGTH)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMpTpLbValidateLbr:"
                                   "Invalid length received - Replying MEPID TLV \r\n");
                    return ECFM_FAILURE;
                }

                if ((UINT4) ((pu1Pdu + u2TlvLength + 1) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate the Loopback Indiaction 1byte (0x01) */
                ECFM_GET_1BYTE (u1LbIndication, pu1Pdu);
                if (u1LbIndication != ECFM_REQ_MEPID_TLV_LBR_INDICATION)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMpTpLbValidateLbr: Invalid loopback "
                                   "indication byte rcvd - Replying MEPID TLV \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate MEP ID */
                ECFM_GET_2BYTE (u2MepId, pu1Pdu);
                if (u2MepId != pMepInfo->u2MepId)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmMpTpLbValidateLbr: MEP ID does not "
                                   "match - Replying MEPID TLV \r\n");
                    return ECFM_FAILURE;
                }

                /* Validate MEGID */
                if (EcfmUtilCompareMEGID (pu1Pdu, pMepInfo->pCcMepInfo) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: MEGID does not match\r\n");
                    return ECFM_FAILURE;
                }

                pu1Pdu = pu1Pdu + ECFM_MEGID_FIELD_SIZE;

                /* Move the pointer to 2 bytes to skip the Reserved field */
                pu1Pdu = pu1Pdu + ECFM_REQ_MEPID_TLV_RESV_FIELD_SIZE;
                b1ReqFlag = ECFM_TRUE;
                break;
            case ECFM_TEST_TLV_TYPE:
                if ((b1ReplyFlag != ECFM_TRUE) || (b1DataFlag == ECFM_TRUE) ||
                    (b1ReqFlag == ECFM_TRUE) || (b1TestFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: Malformed Packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Collect the length to move the pointer to the endo of TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                if ((UINT4) ((pu1Pdu + u2TlvLength) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                pu1Pdu = pu1Pdu + u2TlvLength;
                b1TestFlag = ECFM_TRUE;
                break;
            case ECFM_DATA_TLV_TYPE:
                if ((b1ReplyFlag != ECFM_TRUE) || (b1TestFlag == ECFM_TRUE) ||
                    (b1DataFlag == ECFM_TRUE))
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: Malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                /* Collect the length to move the pointer to the endo of TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                if ((UINT4) ((pu1Pdu + u2TlvLength + 1) - pu1PduStart) >
                    u4ByteCount)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmMpTpLbValidateLbr: "
                                   "Received malformed packet \r\n");
                    return ECFM_FAILURE;
                }

                pu1Pdu = pu1Pdu + u2TlvLength;
                b1DataFlag = ECFM_TRUE;
                break;
            default:
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmMpTpLbValidateLbr:"
                               "TLV Type Unknown...\r\n");
                return ECFM_FAILURE;
        }
        ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmMpTpLbPutTrgtMepIdTlv 
 *
 * Description        : This routine is used to fill the Target MEP ID TLV. 
 *                      This should be the first TLV in the LBM.
 *
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure
 *                      that stores the information regarding MEP info.
 *
 * Output(s)          : ppu1LbPdu - pointer to the Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmMpTpLbPutTrgtMepIdTlv (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT2               u2LbmTlvSize = 0;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pu1Pdu = *ppu1LbPdu;

    /*
       0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       | Type (21)   |           Length (25)          | Sub-Type (0x02)|
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |          MEP ID                |                              |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++                              |
       |                               ...                             |
       |                           MUST be ZERO                        |
       |                               ...                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       Figure 8 Target or Replying MEP/MIP ID TLV format (ICC-based MEP ID)
     */

    /* Put 1 byte as the Target MEP TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TARGET_MPID_TLV_TYPE);

    /* Put the next 2 byte as the length of the TLV */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_TARGET_MPID_TLV_LENGTH);

    /* Put 1 byte as Sub-Type (0x02) */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TARGET_MEPID_TLV_SUB_TYPE);

    /* Put 2 bytes as Target MEPID */
    ECFM_PUT_2BYTE (pu1Pdu, pLbInfo->u2TxDestMepId);

    u2LbmTlvSize = (ECFM_TARGET_MPID_TLV_LENGTH - ECFM_VAL_3);

    /* Pad with zeros to complete the 25bytes */
    ECFM_MEMSET (pu1Pdu, 0x0, u2LbmTlvSize);
    pu1Pdu += u2LbmTlvSize;

    *ppu1LbPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
}

/*******************************************************************************
 * Function Name      : EcfmMpTpLbPutTrgtMipIdTlv 
 *
 * Description        : This routine is used to fill the Target MIP ID TLV. 
 *                      This should the first TLV in the LBM PDU, 
 *                      if initiated for MIP as the target.  
 *
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure
 *                      that stores the information regarding MEP info.
 *
 * Output(s)          : ppu1LbPdu - pointer to the Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmMpTpLbPutTrgtMipIdTlv (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT2               u2LbmTlvSize = 0;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pu1Pdu = *ppu1LbPdu;

    /* Put 1 byte as the Target MEP/MIP TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TARGET_MPID_TLV_TYPE);

    /*
       0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |    Type      |          Length (25)          |Sub-Type (0x03) |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                    ITU-T Carrier Code (ICC)                   |
       |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                               |           Node-ID             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |            Node-ID            |            IF-Num             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |             IF-Num            |                               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
       |                              ...                              |
       |                          MUST be ZERO                         |
       |                              ...                              |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       Figure 9 Target or Replying MEP/MIP ID TLV format (ICC-based MIP ID)
     */

    /* Put the next 2 byte as the length of the TLV */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_TARGET_MPID_TLV_LENGTH);

    /* Put 1 byte as Sub-Type (0x03) */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TARGET_MIPID_TLV_SUB_TYPE);

    /* Put 6 bytes as ITU-T Carrier Code (ICC) */
    ECFM_PUT_NBYTE (pu1Pdu, pLbInfo->au1LbmIcc, ECFM_CARRIER_CODE_SIZE);

    /* Put 4 bytes as Node-ID */
    ECFM_PUT_4BYTE (pu1Pdu, pLbInfo->u4LbmNodeId);

    /* Put 4 bytes as IF-Num */
    ECFM_PUT_4BYTE (pu1Pdu, pLbInfo->u4LbmIfNum);

    u2LbmTlvSize = (ECFM_TARGET_MPID_TLV_LENGTH - ECFM_VAL_15);

    /* Pad with zeros to complete the 25bytes */
    ECFM_MEMSET (pu1Pdu, 0x0, u2LbmTlvSize);
    pu1Pdu = pu1Pdu + u2LbmTlvSize;

    *ppu1LbPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
}

/*******************************************************************************
 * Function Name      : EcfmMpTpLbPutRqstMepIdTlv 
 *
 * Description        : This routine is used to fill the Requesting MEP ID TLV. 
 *                      Requesting MEP ID TLV is filled by default, if the LBM
 *                      is not initiated for diagnostic test (Test TLV). 
 *
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure
 *                      that stores the information regarding MEP info.
 *
 * Output(s)          : ppu1LbPdu - pointer to the Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmMpTpLbPutRqstMepIdTlv (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1Pdu = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pLbInfo);
    pu1Pdu = *ppu1LbPdu;

    /*
       0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       | Type(0x23)  |           Length (53)          | Loopback Ind.  |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |          MEP ID                |                              |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++                              |
       |                                                               |
       |                              MEGID                            |
       |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-|
       |                               |      Reserved (0x0000)        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       Figure 10 Requesting MEP ID TLV format 
     */

    /* Put 1 byte as the Requesting MEPID TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_REQ_MEPID_TLV_TYPE);

    /* Put the next 2 byte as the length of the TLV */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_REQ_MEPID_TLV_LENGTH);

    /* Put 1 byte as LoopBack Indication, this MUST be set
     * to 0x00 when this TLV is inserted in an LBM PDU.
     */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_REQ_MEPID_TLV_LBM_INDICATION);

    /* Put 2 bytes as Target MEPID */
    ECFM_PUT_2BYTE (pu1Pdu, pMepInfo->u2MepId);

    /* Fill next 48 byte as MEGID */
    EcfmUtilPutMEGID (pMepInfo->pCcMepInfo, pu1Pdu);
    pu1Pdu = pu1Pdu + ECFM_MAID_FIELD_SIZE;

    /* Put 2 bytes as Reserved Field */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_REQ_MEPID_TLV_RSRVD_VALUE);

    *ppu1LbPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
}
#endif /* _CFMMPTP_C_ */
