/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccapi.c,v 1.10 2008/10/21 06:36:47 prabuc-iss Exp $
 *
 * Description: This file contains the procedures called by lblt
 *              modules to access te functionality of this module
 *******************************************************************/

#include "cfminc.h"
/****************************************************************************
  End of File cfmccapi.c
 ****************************************************************************/
