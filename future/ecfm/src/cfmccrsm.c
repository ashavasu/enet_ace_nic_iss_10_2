/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccrsm.c,v 1.42 2016/03/04 11:01:39 siva Exp $
 *
 * Description: This file contains the Functionality of the Continuity 
 *                        check receiver State Machine.
 *******************************************************************/

#include "cfminc.h"
PRIVATE INT4 EcfmCcmRcvdSmCcmValidation PROTO ((tEcfmCcPduSmInfo *));
PRIVATE INT4 EcfmCcmRcvdSmCcmCompareMAID PROTO ((tEcfmCcPduSmInfo *));
PRIVATE INT4 EcfmCcMepBelongsToMa PROTO ((UINT2, tEcfmCcMepInfo *));
PRIVATE INT4 EcfmCcmRcvdSmCcmCompareMEGID PROTO ((tEcfmCcPduSmInfo *));

/****************************************************************************
 * Function Name      : EcfmCcClntProcessEqCcm
 *
 * Description        : This routine Validates and processes 
 *                      the valid CCM at equal MD Level and 
 *                      call the RMEP SEM with valid RMEP node.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntProcessEqCcm (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    tEcfmMepInfoParams  MepInfo;
    /*pointer used to get the various table */
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcRxCcmPduInfo *pCcRx = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tMacAddr            NullMacAddr = { ECFM_INIT_VAL };
    UINT1               u1RecvdInterval = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    tEcfmMacAddr        MepMacAddr = { 0 };
#ifdef MBSM_WANTED
    INT4                i4SlotId;
#endif
    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    /* validate the CCM received */
    if (EcfmCcmRcvdSmCcmValidation (pPduSmInfo) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessEqCcm: Pdu validation Failed\r\n");
        return ECFM_FAILURE;
    }
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcRx = ECFM_CC_GET_CCM_FROM_PDUSM (pPduSmInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
    {
	    ECFM_GET_MAC_ADDR_OF_PORT (pPduSmInfo->pPortInfo->u4IfIndex, MepMacAddr);
	    if (ECFM_IS_GROUP_DMAC_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_FALSE)
	    {
		    if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
					    MepMacAddr) != ECFM_SUCCESS)
		    {
			    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
					    "EcfmCcCtrlRxParsePduHdrs: "
					    "Received wrong unicast address for CCM\r\n");
			    return ECFM_FAILURE;
		    }
	    }
    }

    /* Y.1731: When Y.1731 is enabled, Compare the MEGID 
     * otherwise MAID*/
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))

    {
        i4RetVal = EcfmCcmRcvdSmCcmCompareMEGID (pPduSmInfo);
    }

    else

    {
        i4RetVal = EcfmCcmRcvdSmCcmCompareMAID (pPduSmInfo);
    }
    if (pMaInfo->u1CcRole != ECFM_CC_LM_TYPE_2LM &&
        pMepInfo->CcInfo.b1UnicastCcmEnabled != ECFM_TRUE)
    {
        ECFM_MEMCPY (pMepInfo->CcInfo.UnicastCcmMacAddr,NullMacAddr,
                                               ECFM_MAC_ADDR_LENGTH);
    }

    if (i4RetVal != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessEqCcm: MEGID/MAID does not match"
                     "Cross connected SEM \r\n");

        /* Mismerge Trap is raised and Error Log Entry is done when 
         * Y.1731 is enabled */
        /* In case LCK condition is true for this MEP, this MEP should not raise
         * mismerge alarm
         */
        if (pMepInfo->CcInfo.b1MisMergeDefect == ECFM_FALSE)
        {
            /*Add Error Entry to Error Log Table */
            pCcErrLog =
                EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_MISMERGE_DFCT_ENTRY);
            pMepInfo->CcInfo.b1MisMergeDefect = ECFM_TRUE;

            /* Generate the SNMP trap for the fault */
            Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_MISMERGE_EN_TRAP_VAL);

            MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;
            MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                      pTempPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif
            /* Notify the registered applications that Delay has 
             * exceeded threshold 
             */
            ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   &MepInfo, NULL,
                                   ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                   ECFM_CC_TASK_ID);
        }

        /* Call the cross connected state machine */
        if (EcfmCcClntXconSm (pPduSmInfo, ECFM_SM_EV_XCON_CCM_RCVD) !=
            ECFM_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessEqCcm: XCON SEM,Not able to "
                         "handle the event\r\n");
            return ECFM_FAILURE;
        }

        /* Required in case of OFFLOADING CCM */
        /* Check if RDI is to be set/reset in Offloaded module */
        return FsEcfmOffloadSetRDI (pMepInfo);
    }

    /*Check for Error CCM Defect */

    /* Get the CCI interval value from the flag field */
    u1RecvdInterval = ECFM_CC_GET_CCM_INTERVAL (pPduSmInfo->u1RxFlags);

    /* As MAID is same as configured value ,therefore  check for the MEPID and 
     * the CCI interval*/

    /* Check if received MEPID is configures in the receiving MEP 
       or Received MEPID is same as MEPID of the receiving MEP 
       or CCI interval received is not as configured one then call the 
       RMEP errors state machine */
    i4RetVal =
        EcfmCcMepBelongsToMa (pPduSmInfo->uPduInfo.Ccm.u2MepId, pMepInfo);
    while ((i4RetVal != ECFM_SUCCESS) || (pCcRx->u2MepId == pMepInfo->u2MepId))

    {

        /* Unexpected Mep Trap is raised and Error Log Entry is done when 
         * Y.1731 is enabled */
        /* In case LCK condition is true for this MEP, this MEP should not raise
         * unexpected MEP alarm
         */

        if (pMepInfo->CcInfo.b1UnExpectedMepDefect == ECFM_FALSE)

        {

            /*Add Unexpected Mep Error Entry to Error Log Table */
            pCcErrLog =
                EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_MEP_DFCT_ENTRY);
            Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_MEP_EN_TRAP_VAL);
            pMepInfo->CcInfo.b1UnExpectedMepDefect = ECFM_TRUE;

            MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
            MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;
            MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                      pTempPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
            /* Notify the registered applications that Unexpected MEP 
             * has been encountered 
             */
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

            ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   &MepInfo, NULL,
                                   ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                   ECFM_CC_TASK_ID);
        }
        break;
    }
    while (u1RecvdInterval != pMaInfo->u1CcmInterval)
    {
        /* Unexpected Period Trap is raised and Error Log Entry is done when 
         * Y.1731 is enabled
         */
        /* In case LCK condition is true for this MEP,
         * this MEP should not raise unexpected Period alarm
         */
        if (pMepInfo->CcInfo.b1UnExpectedPeriodDefect == ECFM_FALSE)
        {
            /* Add entry for Unexpected Period Error to Error Log Table */
            pCcErrLog =
                EcfmCcAddErrorLogEntry (pPduSmInfo,
                                        ECFM_UNEXP_PERIOD_DFCT_ENTRY);
            Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_PERIOD_EN_TRAP_VAL);
            pMepInfo->CcInfo.b1UnExpectedPeriodDefect = ECFM_TRUE;

            MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;
            MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                      pTempPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
            /* Notify the registered applications that unexpected period
             * has been encountered 
             */
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

            ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   &MepInfo, NULL,
                                   ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                   ECFM_CC_TASK_ID);
        }
        break;
    }
    if ((i4RetVal != ECFM_SUCCESS) ||
        (pCcRx->u2MepId == pMepInfo->u2MepId) ||
        (u1RecvdInterval != pMaInfo->u1CcmInterval))

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessEqCcm: Received wrong CCM"
                     "calling the errored SEM\r\n");

        /* Call the RMEP Error state machine */
        if (EcfmCcClntRmepErrSm (pPduSmInfo, ECFM_SM_EV_RMEP_ERR_CCM_RCVD)
            != ECFM_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessEqCcm: RMEP ERR SEM,Not able to "
                         "handle the event\r\n");
            return ECFM_FAILURE;
        }

        /* Required in case of OFFLOADING CCM */
        /* Check if RDI is to be set/reset in Offloaded module */
        return FsEcfmOffloadSetRDI (pMepInfo);
    }

    /* Get the RMEP for this MEP */
    pRMepNode = EcfmUtilGetRmepInfo (pMepInfo, pCcRx->u2MepId);
    if (pRMepNode != NULL)

    {
        pPduSmInfo->pRMepInfo = pRMepNode;
    }

    else

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessEqCcm: RMEP node not found\r\n");
        return ECFM_FAILURE;
    }
    /* Call the RMEP SEM with the correct received CCM */
    if (EcfmCcClntRmepSm (pPduSmInfo, ECFM_SM_EV_RMEP_CCM_RCVD) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessEqCcm: RMEP SEM,"
                     " Not able to handle the event\r\n");
        return ECFM_FAILURE;
    }

    /* Y.1731: Seqence No. always received as zero */
    /* If the Sequence Number received is not equal to 0 and 
     * Sequence Number in the CCM database is also not equal to 0 
     * moreover the  Sequence Number received is NOT one greater than the 
     * Sequence Number in MEP CCM database, then increment the error counter
     * and save the received sequence number in the data base.*/
    if ((pCcRx->u4SeqNumber != 0) &&
        (pPduSmInfo->pRMepInfo->u4SeqNum != 0) &&
        (pCcRx->u4SeqNumber != (pPduSmInfo->pRMepInfo->u4SeqNum + 1)))

    {
        ECFM_CC_INCR_CCM_SEQ_ERRORS (pCcInfo);
    }
    pRMepNode->u4SeqNum = pCcRx->u4SeqNumber;

    /* SEM is already in idle state to shift to some other transient state is
     * not required*/

    /* Required in case of OFFLOADING CCM */
    /* Check if RDI is to be set/reset in Offloaded module */
    if (FsEcfmOffloadSetRDI (pMepInfo) == ECFM_FAILURE)

    {
        return ECFM_FAILURE;
    }

#ifdef MBSM_WANTED
    /* Find the LC at which this PDU is received.
     * Set bit for that LC in u1MepCcmLCStatus.
     * This info is used for handling Multiple Trap Notification received
     * from CCM Offloaded Modules */
    if (MbsmGetSlotFromPort (pPduSmInfo->pPortInfo->u4IfIndex, &i4SlotId) ==
        MBSM_SUCCESS)
    {
        ECFM_SET_LC_STATUS_ON_PDU_RX (&(pRMepNode->u1MepCcmLCStatus), i4SlotId);
    }
#endif

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcMepBelongsToMa
 *
 * Description        : This is function is used to check whether this MEP is
 *                      part of same MA 
 *
 * Input(s)           : u2MepId - Remote MEP identifier
 *                      pMepInfo -Pointer to MEP Node , corresponding to which 
 *                      node is to retrieve.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 ****************************************************************************/
PRIVATE INT4
EcfmCcMepBelongsToMa (UINT2 u2RmepId, tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcRMepDbInfo  *pRMep = NULL;
    tEcfmCcRMepDbInfo   RMepInfo;
    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);
    pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepInfo->RMepDb));
    while (pRMep != NULL)
    {
        if (pRMep->u2RMepId == u2RmepId)
        {
            if (pMepInfo->pEcfmMplsParams == NULL)
            {
                return ECFM_SUCCESS;
            }
            else
            {
                /* For MPLS-TP based Services, RMep Id should be associated
                 * explicitly to RMep table, in addition to maintaining
                 * in MEP Cross Check List.
                 */
                if (pRMep->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
                {
                    return ECFM_SUCCESS;
                }
            }
        }
        pRMep =
            (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepInfo->RMepDb),
                                                &(pRMep->MepDbDllNode));
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_FAILURE;
}

/****************************************************************************
 * Function Name      : EcfmCcClntProcessLowCcm
 *
 * Description        : This routine is used to Validates and processes 
 *                      the lower MD Level CCM received.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntProcessLowCcm (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    /* validate the CCM */
    if (EcfmCcmRcvdSmCcmValidation (pPduSmInfo) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessLowCcm: Pdu validation Failed\r\n");
        return ECFM_FAILURE;
    }
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmCcClntProcessLowCcm: Calling the XCON SEM\r\n ");

    /* In case LCK condition is true for this MEP, this MEP should not raise
     * unexpected LEVEL alarm
     */
    if (pMepInfo->CcInfo.b1UnExpectedLevelDefect == ECFM_FALSE)
    {
        /*Add Entry for Unexpected Mep Level error to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo,
                                    ECFM_UNEXP_MEG_LEVEL_DFCT_ENTRY);

        /* Generate the SNMP trap for the entry of Unexpected Mep 
         * Level defect*/
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_LEVEL_EN_TRAP_VAL);
        pMepInfo->CcInfo.b1UnExpectedLevelDefect = ECFM_TRUE;
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                               &MepInfo, NULL,
                               ECFM_DEFECT_CONDITION_ENCOUNTERED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

    }
    /* Call the cross connected state machine */
    if (EcfmCcClntXconSm (pPduSmInfo, ECFM_SM_EV_XCON_CCM_RCVD) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessLowCcm: XCON SEM,Not able to handle"
                     "the event\r\n");
        return ECFM_FAILURE;
    }

    /* Required in case of OFFLOADING CCM */
    /* Check if RDI is to be set in Offloaded module */
    if (pPduSmInfo->pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC, "CCMOFFLOAD: "
                          "Software received the CCM PDU from "
                          "hardware for MEPID %d\r\n",
                          pPduSmInfo->pMepInfo->u2MepId);

        /* set presentRDI to True */
        pPduSmInfo->pMepInfo->b1PresentRdi = ECFM_TRUE;
        if (pPduSmInfo->pMepInfo->b1PresentRdi !=
            pPduSmInfo->pMepInfo->b1CcmOffloadLastRdi)
        {
            /* Call the Create Tx/Rx call for Mep */
            if (EcfmCcmOffCreateTxRxForMep (pPduSmInfo->pMepInfo) !=
                ECFM_SUCCESS)
            {
                return ECFM_FAILURE;
            }
            pPduSmInfo->pMepInfo->b1CcmOffloadLastRdi =
                pPduSmInfo->pMepInfo->b1PresentRdi;
        }
    }
    /* SEM is already in idled state to shift to some 
     * other transient state is not required
     */
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmCcAddErrorLogEntry 
 *
 * Description        : This routine is used to add the entry in the error log 
 *                      Table for the defect.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *                      u1FaultType - It informs about Type of Errors.
 *                       
 * Output(s)          : None.
 *
 * Returns            : pCcErrorNode - Pointer to the error log node
 ******************************************************************************/
tEcfmCcErrLogInfo  *
EcfmCcAddErrorLogEntry (tEcfmCcPduSmInfo * pPduSmInfo, UINT2 u2FaultType)
{
    tUtlSysPreciseTime  SysPreciseTime;
    tEcfmCcErrLogInfo  *pCcErrorNode = NULL;
    tEcfmCcErrLogInfo  *pFirstCcErrorNode = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    ECFM_CC_TRC_FN_ENTRY ();

    /* If Caching is Enabled then only add the entry in Error Log otherwise
     * return */
    if ((ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum)) ||
        (ECFM_CC_IS_ERR_LOG_DISABLED () == ECFM_TRUE))

    {
        return NULL;
    }

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        /* Only Active node needs to add error log entry/exit */
        return NULL;
    }
    /* Allocate a memory block for CC Error node */
    ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE (pCcErrorNode);
    while (pCcErrorNode == NULL)

    {

        /* Memory allocation failed, so now check if the memory pool is 
         * exhausted
         */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcAddErrorLogEntry: "
                     "failure allocating memory for Cc Error buffer node\r\n");
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_CC_ERR_LOG_POOL) != ECFM_INIT_VAL)

        {

            /* memory failure failure has occured even when we have free 
             * memory in the pool
             */
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcAddErrorLogEntry: "
                         "Cc Error memory pool corruption\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return NULL;
        }

        /* Memory pool is exhausted for sure, now we can delete 
         * the first entry in the pool to have some free memory
         */
        pFirstCcErrorNode = (tEcfmCcErrLogInfo *)
            RBTreeGetFirst (ECFM_CC_ERR_LOG_TABLE);
        if (pFirstCcErrorNode == NULL)

        {

            /* Memory pool is exhausted and we dont have anyting in the CC
             * Error table to free.*/
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcAddErrorLogEntry"
                         "Cc Error buffer table corruption \r\n");
            return NULL;
        }

        /* Remove the first node from the table */
        RBTreeRem (ECFM_CC_ERR_LOG_TABLE, pFirstCcErrorNode);

        /* Free the memory for the node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL, (UINT1 *) pFirstCcErrorNode);
        pFirstCcErrorNode = NULL;

        /* Now we can again try for allocating memory */
        ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE (pCcErrorNode);
    } ECFM_MEMSET (pCcErrorNode, ECFM_INIT_VAL, ECFM_CC_ERR_LOG_INFO_SIZE);

    /* Add Error Log fields  */
    pCcErrorNode->u4MdIndex = pMepInfo->u4MdIndex;
    pCcErrorNode->u4MaIndex = pMepInfo->u4MaIndex;
    pCcErrorNode->u2MepId = pMepInfo->u2MepId;
    pCcErrorNode->u2RmepId = ECFM_INIT_VAL;
    ECFM_CC_ERR_LOG_SEQ_NUM = ECFM_CC_ERR_LOG_SEQ_NUM + ECFM_INCR_VAL;
    pCcErrorNode->u4SeqNum = ECFM_CC_ERR_LOG_SEQ_NUM;

    /* RMEP-ID is valid for only the following defect
     * ECFM_RDI_CCM_DFCT_ENTRY
     * ECFM_LOC_DFCT_ENTRY
     *
     * ECFM_RDI_CCM_DFCT_EXIT
     * ECFM_LOC_DFCT_EXIT
     */
    if (((u2FaultType == ECFM_RDI_CCM_DFCT_ENTRY) ||
         (u2FaultType == ECFM_LOC_DFCT_ENTRY) ||
         (u2FaultType == ECFM_RDI_CCM_DFCT_EXIT) ||
         (u2FaultType == ECFM_LOC_DFCT_EXIT)) &&
        (pPduSmInfo->pRMepInfo != NULL))
    {
        pCcErrorNode->u2RmepId = pPduSmInfo->pRMepInfo->u2RMepId;
    }
    /*Get Epoch Time in Sec and storing */
    UtlGetPreciseSysTime (&SysPreciseTime);
    pCcErrorNode->TimeStamp = SysPreciseTime.u4Sec;
    pCcErrorNode->u2LogType = u2FaultType;

    /* Add RBTree Node */
    if (RBTreeAdd (ECFM_CC_ERR_LOG_TABLE, pCcErrorNode) != ECFM_RB_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                          ECFM_OS_RESOURCE_TRC,
                          "EcfmCcAddErrorLogEntry:"
                          " RBTree Add Failed!! %d \r\n",
                          pCcErrorNode->TimeStamp);
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL, (UINT1 *) pCcErrorNode);
        return NULL;
    }

    /* Sync Error Log entry in the STANDBY node also */
    EcfmRedSyncErrorLogEntry (pCcErrorNode);
    ECFM_CC_TRC_FN_EXIT ();
    return pCcErrorNode;
}

/****************************************************************************
 * Function Name      : EcfmCcmRcvdSmCcmValidation
 *                    
 * Description        : This routine is used to check the validity of the
 *                       received CCM based of the validation test 
 *                         
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *                    
 * Output(s)          : None
 *                    
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmCcmRcvdSmCcmValidation (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcRxCcmPduInfo *pCcm = NULL;    /* Received CCM Info */
    UINT1               u1NameLength = ECFM_INIT_VAL;
    UINT1               u1MaNameIndex = ECFM_INIT_VAL;
    UINT1               u1Offset = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pCcm = &(pPduSmInfo->uPduInfo.Ccm);

    /*Y.1731: When Y.1731 is enabled, Validate the Reserved field, MEGID, 
     * Length */
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pPortInfo->u2PortNum))

    {
        /* Verify the reserved field */
        if (pCcm->au1MAID[0] != ECFM_MEGID_RESERVED_VALUE)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmValidation: Reserved field is "
                         "invalid \r\n");
            return ECFM_FAILURE;
        }
        /* Check the MEGID Format */
        u1Offset = u1Offset + ECFM_INCR_VAL;
        /* Verify if the received MEGID format is same as the configured one */
        if (pCcm->au1MAID[u1Offset] != ECFM_MEGID_FORMAT_TYPE)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmValidation: MEGID format is "
                         "invalid\r\n");
            return ECFM_FAILURE;
        }
        /* Check the length of the MEGID */
        u1Offset = u1Offset + ECFM_INCR_VAL;
        if (pCcm->au1MAID[u1Offset] != ECFM_MEG_ID_LEN)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmValidation: MEGID length is not "
                         "valid\r\n");
            return ECFM_FAILURE;
        }
    }
    else
    {
        if ((pCcm->au1MAID[ECFM_INIT_VAL] < ECFM_DOMAIN_NAME_TYPE_NONE) ||
            (pCcm->au1MAID[ECFM_INIT_VAL] > ECFM_DOMAIN_NAME_TYPE_CHAR_STRING))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmValidation: Md Name format "
                         "invalid\r\n");
            return ECFM_FAILURE;
        }

        if (pCcm->au1MAID[ECFM_INIT_VAL] != ECFM_DOMAIN_NAME_TYPE_NONE)
        {
            /* Check if the MD Name length shld be greater than 0 and less 
             * than 43
             */
            u1NameLength = pPduSmInfo->uPduInfo.Ccm.au1MAID[1];
            if ((u1NameLength <= 0) || (u1NameLength > ECFM_MD_NAME_MAX_LEN))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcmRcvdSmCcmValidation: Md Name length "
                             "invalid\r\n");
                return ECFM_FAILURE;
            }
            /* MA Name length shld be greater or equal to 1 */
            u1MaNameIndex =
                (UINT1) (ECFM_MIN_MA_NAME_LEN_OFFSET) + u1NameLength;
        }
        else
        {
            u1MaNameIndex = (ECFM_MD_NAME_FRMT_FIELD_SIZE +
                             ECFM_MA_NAME_FRMT_FIELD_SIZE);
        }

        if (u1MaNameIndex < ECFM_MAID_FIELD_SIZE)
        {
            u1NameLength = pPduSmInfo->uPduInfo.Ccm.au1MAID[u1MaNameIndex];
        }

        if ((u1NameLength < ECFM_MA_NAME_MIN_LEN) ||
            (((pCcm->au1MAID[u1MaNameIndex - ECFM_MA_NAME_FRMT_FIELD_SIZE])
              == ECFM_ASSOC_NAME_ICC) && (u1NameLength != ECFM_MEG_ID_LEN)))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmValidation: Ma Name length "
                         "invalid\r\n");
            return ECFM_FAILURE;
        }
    }
    /* validate MEPID is in the range 1-8191 */
    if ((pPduSmInfo->uPduInfo.Ccm.u2MepId > ECFM_MEPID_MAX) ||
        (pPduSmInfo->uPduInfo.Ccm.u2MepId < ECFM_MEPID_MIN))
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmRcvdSmCcmValidation: MEP ID not in the range\r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmRcvdSmCcmCompareMEGID
 *
 * Description        : This routine compare the MEGID received with configured
 *                      one                         
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 * Output(s)          : None
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmCcmRcvdSmCcmCompareMEGID (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRxCcmPduInfo *pCcm = NULL;    /* Received CCM Info */

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcm = &(pPduSmInfo->uPduInfo.Ccm);

    if (EcfmUtilCompareMEGID (pCcm->au1MAID, pMepInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmRcvdSmCcmCompareMEGID: MEGID "
                     "does not match\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcmRcvdSmCcmCompareMAID
 *
 * Description        : This routine comapre the MAID received with configured
 *                      one                         
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 * Output(s)          : None
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmCcmRcvdSmCcmCompareMAID (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMdInfo      *pMdInfo = NULL;
    tEcfmCcRxCcmPduInfo *pCcm = NULL;    /* Received CCM Info */
    UINT1               au1PadVal[ECFM_MAID_FIELD_SIZE];
    UINT1               u1Offset = ECFM_INIT_VAL;
    UINT1               u1Length = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (au1PadVal, 0, ECFM_MEGID_FIELD_SIZE);
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);
    pCcm = &(pPduSmInfo->uPduInfo.Ccm);

    /* Verify if the received MD format is same as the configured one */
    if (pCcm->au1MAID[0] != pMdInfo->u1NameFormat)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcmRcvdSmCcmCompareMAID: Md Name format does "
                     "not match\r\n");
        return ECFM_FAILURE;
    }
    /* If the MDName format on the received CCM PDU is equal to 
     * ECFM_DOMAIN_NAME_TYPE_NONE(01) then process this PDU as per 
     * Section 21.6.5.1 Table 21-18 in IEEE 802.1ag standard.
     */
    if (pCcm->au1MAID[0] != ECFM_DOMAIN_NAME_TYPE_NONE)
    {
        u1Offset = u1Offset + ECFM_INCR_VAL;
        u1Length = pMdInfo->u1NameLength;
        if (pCcm->au1MAID[u1Offset] != u1Length)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMAID: Md Name length does "
                         "not match\r\n");
            return ECFM_FAILURE;
        }
        /* Compare the MD name */
        u1Offset = u1Offset + ECFM_INCR_VAL;
        if ((u1Length <= ECFM_MD_NAME_MAX_LEN) &&
            (ECFM_MEMCMP ((pCcm->au1MAID + u1Offset),
                          pMdInfo->au1Name, u1Length) != 0))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMAID: Md Name does "
                         "not match\r\n");
            return ECFM_FAILURE;
        }
        /* Verify the Ma Format */
        u1Offset = u1Offset + u1Length;
    }
    else
    {
        u1Offset = u1Offset + ECFM_INCR_VAL;
    }
    /* If the received MA format type is ECFM_ASSOC_NAME_ICC 
     * then compare the Ma Name with the ICC and UMC value.
     */
    if ((u1Offset < ECFM_MAID_FIELD_SIZE) &&
        (pCcm->au1MAID[u1Offset] == ECFM_ASSOC_NAME_ICC))
    {
        if (EcfmCcmRcvdSmCcmCompareMEGID (pPduSmInfo) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMAID: MEGID format does "
                         "not match\r\n");
            return ECFM_FAILURE;
        }
    }
    else
    {
        /* Verify if the received MA format is same as the configured one */
        if ((u1Offset < ECFM_MAID_FIELD_SIZE) && pCcm->au1MAID[u1Offset]
            != pMaInfo->u1NameFormat)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMAID: Ma Name format does "
                         "not match\r\n");
            return ECFM_FAILURE;
        }
        /* Check the length of the MA name */
        u1Offset = u1Offset + ECFM_INCR_VAL;
        u1Length = pMaInfo->u1NameLength;
        if ((u1Offset < ECFM_MAID_FIELD_SIZE - u1Offset)
            && pCcm->au1MAID[u1Offset] != u1Length)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMAID: Ma Name length does "
                         "not match\r\n");
            return ECFM_FAILURE;
        }
        /* Compare the MA name */
        u1Offset = u1Offset + ECFM_INCR_VAL;
        if ((u1Length <= ECFM_MD_NAME_MAX_LEN) &&
            (u1Offset < ECFM_MAID_FIELD_SIZE) &&
            (ECFM_MEMCMP
             ((pCcm->au1MAID + u1Offset), pMaInfo->au1Name, u1Length) != 0))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMAID: Ma Name does not "
                         "match\r\n");
            return ECFM_FAILURE;
        }
        /* if 48 byte are still not yet comapred it means rest of the bytes 
         * shld be padded as 0 */
        u1Offset = u1Offset + u1Length;
        if (u1Offset < ECFM_MAID_FIELD_SIZE)
        {
            if (ECFM_MEMCMP ((pCcm->au1MAID + u1Offset), (au1PadVal + u1Offset),
                             (ECFM_MAID_FIELD_SIZE - u1Offset)) != 0)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcmRcvdSmCcmCompareMAID: Rest of the MAID is "
                             "not same\r\n");
                return ECFM_FAILURE;
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcClntParseCcm
 *
 * Description        : This routine processes and extracts the fields from the 
 *                      received CCM and correspondingly fills the 
 *                      tEcfmCcPduSmInfo structure.
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU received and 
 *                      other state machine related information.
 *                      pu1Pdu - linear buffer conatining the CFM PDU to be
 *                      extracted
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntParseCcm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmSenderId      *pSenderIdInfo = NULL;
    tEcfmOrgSpecific   *pOrgSpecific = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRxCcmPduInfo *pCcmInfo = NULL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    UINT2               u2TlvLength = ECFM_INIT_VAL;    /* Read the next TLV */

    /*used to skip tlvs other than port,interface and sender id */
    UINT1               u1TlvType = ECFM_INIT_VAL;    /* TLV type */
    UINT2               u2SenderIdLength = ECFM_INIT_VAL;    /* Length of the 
                                                             * Sender ID TLV */
    UINT2               u1PduLength = ECFM_INIT_VAL;
    /* TSK */
    UINT1               u1AddressVlanFieldLength = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pCcmInfo = ECFM_CC_GET_CCM_FROM_PDUSM (pPduSmInfo);
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    UNUSED_PARAM (pMepInfo);
    /* if CFM pdu is greater than 128 octets then return */
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pBuf);
    u1PduLength = (UINT1) (u4ByteCount - pPduSmInfo->u1CfmPduOffset);
    if (u1PduLength >= ECFM_MAX_CCM_PDU_SIZE)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParseCcm: PDU length  >= 128\r\n");
        return ECFM_FAILURE;
    }

    /* First TLV Offset value should be equal or greater than 70 */
    if (pPduSmInfo->u1RxFirstTlvOffset < ECFM_CCM_FIRST_TLV_OFFSET)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntParseCcm: First TLV offset is not >= 70\r\n");
        return ECFM_FAILURE;
    }
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Extract the 4 byte Sequence number from the received CCM */
    ECFM_GET_4BYTE (pCcmInfo->u4SeqNumber, pu1Pdu);

    /* Get 2byte mepid from the current location */
    ECFM_GET_2BYTE (pCcmInfo->u2MepId, pu1Pdu);

    /* Get 48 byte MAID */
    ECFM_MEMCPY (pCcmInfo->au1MAID, pu1Pdu, ECFM_MAID_FIELD_SIZE);
    pu1Pdu = pu1Pdu + ECFM_MAID_FIELD_SIZE;

    /* Extract the 4 byte TxFCf value from the received CCM */
    ECFM_GET_4BYTE (pCcmInfo->u4TxFCf, pu1Pdu);

    /* Extract the 4 byte RxFCb value from the received CCM */
    ECFM_GET_4BYTE (pCcmInfo->u4RxFCb, pu1Pdu);

    /* Extract the 4 byte TxFCb value from the received CCM */
    ECFM_GET_4BYTE (pCcmInfo->u4TxFCb, pu1Pdu);

    /* 4 byte Reserved fields */
    pu1Pdu = pu1Pdu + ECFM_CCM_RESERVE_FIELD_SIZE;

    /*traverese the pdu till the end tlv and read the port and 
       interface status tlv and Sender id */
    ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    /* mac_service_data_unit length = Ethernet Frame length - 
       Address filed size + 
       Vlan field size      */
    u1AddressVlanFieldLength = (2 * ECFM_MAC_ADDR_LENGTH) +
        ECFM_ETH_TYPE_LEN_FIELD_SIZE + ECFM_VLAN_TYPE_LEN_FIELD_SIZE;

    while (u1TlvType != ECFM_END_TLV_TYPE)

    {
        switch (u1TlvType)

        {

                /* Check for the Port status TLV */
            case ECFM_PORT_STATUS_TLV_TYPE:
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcClntParseCcm:"
                             "Received Port-Status TLV \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
/* TSK */
                /* IEEE 802.1ag, Section 20.46.4.3 
                 * If the following test fails, the receiving CFM PDU 
                 * shall consider invalid and discard it: 
                 * c) A TLV Length field does not run over the end of the 
                 *    mac_service_data_unit. 
                 */
                if ((pPduSmInfo->u4ByteCount - u1AddressVlanFieldLength) <
                    u2TlvLength)
                {
                    return ECFM_FAILURE;
                }

                /* Validate the TLV length */
                if (u2TlvLength != ECFM_PORT_STATUS_VALUE_SIZE)

                {
                    return ECFM_FAILURE;
                }

                /* read the 1 byte port status tlv */
                ECFM_GET_1BYTE (pCcmInfo->u1RecvdPortStatus, pu1Pdu);
                break;

                /* Check for the Interface status TLV */
            case ECFM_INTERFACE_STATUS_TLV_TYPE:
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcClntParseCcm:"
                             "Received Interface-Status TLV \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
/* TSK */
                /* IEEE 802.1ag, Section 20.46.4.3 
                 * If the following test fails, the receiving CFM PDU 
                 * shall consider invalid and discard it: 
                 * c) A TLV Length field does not run over the end of the 
                 *    mac_service_data_unit. 
                 */

                if ((pPduSmInfo->u4ByteCount - u1AddressVlanFieldLength) <
                    u2TlvLength)
                {
                    return ECFM_FAILURE;
                }

                /* Validate the TLV length */
                if (u2TlvLength != ECFM_INTERFACE_STATUS_VALUE_SIZE)

                {
                    return ECFM_FAILURE;
                }

                /* read the 1 byte interface status tlv  */
                ECFM_GET_1BYTE (pCcmInfo->u1RecvdIfStatus, pu1Pdu);
                break;

                /* Check for the Sender ID TLV */
            case ECFM_SENDER_ID_TLV_TYPE:
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcClntParseCcm:"
                             "Received Sender-ID  TLV \r\n");
                pSenderIdInfo = &(pCcmInfo->RecvdSenderId);

                /* Extract 2 byte Sender id Length, updating the offset */
                ECFM_GET_2BYTE (u2SenderIdLength, pu1Pdu);
/* TSK */
                /* IEEE 802.1ag, Section 20.46.4.3 
                 * If the following test fails, the receiving CFM PDU 
                 * shall consider invalid and discard it: 
                 * c) A TLV Length field does not run over the end of the 
                 *    mac_service_data_unit. 
                 */

                if ((pPduSmInfo->u4ByteCount - u1AddressVlanFieldLength) <
                    u2SenderIdLength)
                {
                    return ECFM_FAILURE;
                }

                /* Extract the other fields from the sender id */

                /* Get 1 byte Chassis Id length,
                 * Chassis id length always present in the sender id ,
                 * A non zero length means chassis id subtype and chassis id
                 * fields are present */
                ECFM_GET_1BYTE (pSenderIdInfo->ChassisId.u4OctLen, pu1Pdu);
                u2SenderIdLength = u2SenderIdLength -
                    (UINT2) ECFM_CHASSIS_ID_LENGTH_FIELD_SIZE;

                /* Non zero chassis id length means chassis id is present */

                /* Extract the chassis id and update the offset and the
                 * sender id length counter,used to extract the mgmt address 
                 * domain values */
                if (pSenderIdInfo->ChassisId.u4OctLen != 0)

                {

                    /* Read the Chassis id subtype and the chassis id and 
                     * update the offset and sender id length counter */
                    ECFM_GET_1BYTE (pSenderIdInfo->u1ChassisIdSubType, pu1Pdu);
                    u2SenderIdLength =
                        u2SenderIdLength - (UINT2) ECFM_TLV_TYPE_FIELD_SIZE;

                    /* Extract the chassis id */
                    pSenderIdInfo->ChassisId.pu1Octets = pu1Pdu;
                    pu1Pdu = pu1Pdu + pSenderIdInfo->ChassisId.u4OctLen;
                    u2SenderIdLength = u2SenderIdLength -
                        (UINT2) pSenderIdInfo->ChassisId.u4OctLen;
                }                /* chassis id length non zero */

                /* Check if  management address fiels are present or not 
                 * non zero Sender id lenght counter means mngmt address
                 * domain length field is present */
                if (u2SenderIdLength != 0)

                {

                    /* Read 1 byte Managment address Domain length */
                    ECFM_GET_1BYTE (pSenderIdInfo->MgmtAddressDomain.
                                    u4OctLen, pu1Pdu);
                    u2SenderIdLength =
                        u2SenderIdLength -
                        (UINT2) ECFM_MGMT_ADDR_DOMAIN_LENGTH_FIELD_SIZE;

                    /* Non zero Mgmt domain length indiactes that sender id 
                     * tlv contains the mngmt domain address and 
                     * the mgmt address
                     */
                    if (pSenderIdInfo->MgmtAddressDomain.u4OctLen != 0)

                    {

                        /* Extract the Mgmt address Domain */
                        pSenderIdInfo->MgmtAddressDomain.pu1Octets = pu1Pdu;
                        pu1Pdu =
                            pu1Pdu + pSenderIdInfo->MgmtAddressDomain.u4OctLen;
                        u2SenderIdLength =
                            u2SenderIdLength -
                            (UINT2) pSenderIdInfo->MgmtAddressDomain.u4OctLen;

                        /* Extract the management address if present */
                        if (u2SenderIdLength != 0)

                        {

                            /* Read 1 byte Managment address length */
                            ECFM_GET_1BYTE (pSenderIdInfo->MgmtAddress.
                                            u4OctLen, pu1Pdu);
                            pSenderIdInfo->MgmtAddress.pu1Octets = pu1Pdu;
                            pu1Pdu =
                                pu1Pdu + pSenderIdInfo->MgmtAddress.u4OctLen;
                        }        /* extracting the mgmt address if present */
                    }            /* Mgmt address domain length non zero */
                }                /* mgmt fileds are present */
                break;

                /* Check for the Organization Specific  TLV */
            case ECFM_ORG_SPEC_TLV_TYPE:
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcClntParseCcm:"
                             "Received Organization Specific TLV \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
/* TSK */
                /* IEEE 802.1ag, Section 20.46.4.3 
                 * If the following test fails, the receiving CFM PDU 
                 * shall consider invalid and discard it: 
                 * c) A TLV Length field does not run over the end of the 
                 *    mac_service_data_unit. 
                 */

                if ((pPduSmInfo->u4ByteCount - u1AddressVlanFieldLength) <
                    u2TlvLength)
                {
                    return ECFM_FAILURE;
                }

                pOrgSpecific = &(pCcmInfo->RecvdOrgSpecifc);

                /* Copy OUI */
                ECFM_MEMCPY (pOrgSpecific->au1Oui, pu1Pdu, ECFM_OUI_LENGTH);
                pu1Pdu = pu1Pdu + ECFM_OUI_LENGTH;

                /* Copy SubType */
                ECFM_GET_1BYTE (pOrgSpecific->u1SubType, pu1Pdu);
                u2TlvLength =
                    u2TlvLength - (ECFM_OUI_LENGTH +
                                   ECFM_ORG_SPEC_SUB_TYPE_FIELD_SIZE);
                pOrgSpecific->Value.u4OctLen = (UINT4) (u2TlvLength);
                if (pOrgSpecific->Value.u4OctLen != 0)

                {

                    /* Read VALUE in OrgSpecific TLV */
                    pOrgSpecific->Value.pu1Octets = pu1Pdu;
                    pu1Pdu = pu1Pdu + (UINT1) u2TlvLength;
                }
                break;
            default:
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC |
                                  ECFM_ALL_FAILURE_TRC,
                                  "EcfmCcClntParseCcm:TLV Not SUPPORTED! = "
                                  "[%02x]\r\n", u1TlvType);

                /* Extract the next tlv */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Check if the value is present for the TLv or not,if length
                 * is zero it means value is not present
                 * else offset is incremented length of the value field */
                if (u2TlvLength != 0)

                {
                    pu1Pdu = pu1Pdu + (UINT1) u2TlvLength;
                }

                else

                {
                    pu1Pdu = pu1Pdu + (UINT1) ECFM_TLV_LENGTH_FIELD_SIZE;
                }
        }                        /* end of switch */
        ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    }                            /* end of while */
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmCcClntNotifyRmep 
 *                                                                          
 *    DESCRIPTION      : This function notifies the Rmep state machines 
 *                       for events like MEP active/inactive.
 *
 *    INPUT            : pPduInfo - Pointer to the PduSmInfo to notify
 *                       Event - Event to notify
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcClntNotifyRmep (tEcfmCcPduSmInfo * pPduInfo, UINT1 u1Event)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRmep = NULL;
    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduInfo);

    /* Get the first node from the RBtree of RMepDbTable in
     * MepInfo */
    pRmep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepInfo->RMepDb));
    while (pRmep != NULL)

    {
        pPduInfo->pRMepInfo = pRmep;

        /* Call the rmep SEM with the event */
        if (EcfmCcClntRmepSm (pPduInfo, u1Event) != ECFM_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntNotifyRmep:RMEP SEM,"
                         "Not able to handle the event\r\n");
        }

        /* Get the next node form the tree */
        pRmep =
            (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepInfo->RMepDb),
                                                &(pRmep->MepDbDllNode));
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcClntMhfProcessCcm
 *
 * Description        : This routine Validates and processes the received CCM
 *                      and MIP CCM database entry is made for the valid CCM.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntMhfProcessCcm (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMipCcmDbInfo *pMipCcmNode = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* validate the CCM */
    if (EcfmCcmRcvdSmCcmValidation (pPduSmInfo) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntMhfProcessCcm: Received CCM PDU Validation"
                     "Failed\r\n");
        return ECFM_FAILURE;
    }

    /*Only Down MHF can populate MIP CCM DB */
    if (pPduSmInfo->pStackInfo->u1Direction != ECFM_MP_DIR_DOWN)

    {
        return ECFM_SUCCESS;
    }
    if (ECFM_IS_MIP_CCM_DB_ENABLED ())

    {

        /* Check if Entry already exists for the source MAC Address and Vlan 
         * ID then update the entry for the Port */
        pMipCcmNode = EcfmCcUtilGetMipCcmDbEntry (pPduSmInfo->u4RxVlanIdIsId,
                                                  pPduSmInfo->RxSrcMacAddr);
        if (pMipCcmNode != NULL)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcClntMhfProcessCcm:"
                         "Entry Already Exists with Given Source MAC Address"
                         "& FID\n Updating the MIP CCM DB entry \r\n");
            if (pMipCcmNode->u2PortNum != pPduSmInfo->pStackInfo->u2PortNum)

            {
                if (EcfmLbLtAddMipCcmDbEntry
                    (ECFM_CC_CURR_CONTEXT_ID (), pMipCcmNode) != ECFM_SUCCESS)

                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcClntMhfProcessCcm: "
                                 "EcfmLbLtAddMipCcmDbEntry returned failure"
                                 "\r\n");
                    return ECFM_FAILURE;
                }
            }
            pMipCcmNode->u2PortNum = pPduSmInfo->pStackInfo->u2PortNum;
            ECFM_GET_TIMESTAMP (&pMipCcmNode->TimeStamp);
            return ECFM_SUCCESS;
        }

        /* Allocate Memory to the Node to be added in the MIP CCM DB */
        if (ECFM_ALLOC_MEM_BLOCK_CC_MIP_DB_TABLE (pMipCcmNode) == NULL)

        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcClntMhfProcessCcm: Memory Allocation FAILED"
                         "\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
        ECFM_MEMSET (pMipCcmNode, ECFM_INIT_VAL, ECFM_CC_MIP_CCM_DB_INFO_SIZE);

        /* Fill Filtering Identifier */
        pMipCcmNode->u2Fid = (UINT2) pPduSmInfo->u4RxVlanIdIsId;

        /* Fill Source MAC Address */
        ECFM_MEMCPY (pMipCcmNode->SrcMacAddr, pPduSmInfo->RxSrcMacAddr,
                     ECFM_MAC_ADDR_LENGTH);

#ifdef L2RED_WANTED
        if (pMipCcmNode->u2PortNum != pPduSmInfo->pStackInfo->u2PortNum)

        {

            /* Entry in MIP CCM DB is changed, hence set the flag */
            pMipCcmNode->b1Changed = ECFM_TRUE;
        }

#endif /*  */
        /* Fill the receiving MHFs IfIndex */
        pMipCcmNode->u2PortNum = pPduSmInfo->pStackInfo->u2PortNum;

        /* Fill the current time value */
        ECFM_GET_TIMESTAMP (&pMipCcmNode->TimeStamp);

        /* Add CCM Node to the Global MIP CCM  Tree maintianed Globally */
        if (RBTreeAdd (ECFM_CC_MIP_CCM_DB_TABLE, pMipCcmNode) !=
            ECFM_RB_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_OS_RESOURCE_TRC,
                         "EcfmCcClntMhfProcessCcm:"
                         " RBTree Add Failed!! \r\n");

            /* Free the memory allocated to the LTR Node from the LTR POOL */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_DB_TABLE_POOL,
                                 (UINT1 *) pMipCcmNode);
            return ECFM_FAILURE;
        }
        if (EcfmLbLtAddMipCcmDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pMipCcmNode)
            != ECFM_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcClntMhfProcessCcm: "
                         "EcfmLbLtAddMipCcmDbEntry returned failure" "\r\n");
            return ECFM_FAILURE;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmCcAgeoutMipDbEntry                               *
 *                                                                           *
 * Description        : This function is called from CC Timer Expiry Handler *
 *                      on expiry of MIP CCM DB Hold Timer. This routine     *
 *                      checks the TimeStamp Value in the DB Node, if it     *
 *                      AgesOut, then deletes the Node from the MIP CCM DB   *
 *                                                                           *
 * Input(s)           : pointer to the MipCcmDbInfo                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcAgeoutMipDbEntry ()
{
    tEcfmCcMipCcmDbInfo *pMipDbNode = NULL;
    tEcfmCcMipCcmDbInfo *pMipDbNextNode = NULL;
    UINT4               u4CurTime = ECFM_INIT_VAL;
    UINT4               u4AgeoutInTicks = ECFM_INIT_VAL;
    UINT4               u4TimeDiff = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Get the current Time */
    ECFM_GET_TIMESTAMP (&u4CurTime);
    u4AgeoutInTicks = (ECFM_CC_MIP_CCM_DB_HOLD_TIME * SYS_TIME_TICKS_IN_A_SEC);

    /* Get nodes from the MIP CCM DB RBTree */
    pMipDbNode = (tEcfmCcMipCcmDbInfo *) RBTreeGetFirst
        (ECFM_CC_MIP_CCM_DB_TABLE);
    while (pMipDbNode != NULL)

    {

        /* Get Next MIP CCM DB Node */
        pMipDbNextNode = RBTreeGetNext (ECFM_CC_MIP_CCM_DB_TABLE,
                                        (tRBElem *) pMipDbNode, NULL);

        /* Get the Time Difference in the current timestamp and timeVlaue in
         * the MIP CCM DB Node */
        u4TimeDiff = u4CurTime - pMipDbNode->TimeStamp;
        if (u4TimeDiff >= u4AgeoutInTicks)

        {

            /* Delete the MIP CCM DB Entry */
            RBTreeRem (ECFM_CC_MIP_CCM_DB_TABLE, pMipDbNode);
            EcfmLbLtRemoveMipCcmDbEntry (ECFM_CC_CURR_CONTEXT_ID (),
                                         pMipDbNode);

            /* Free the memory assigned to this node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_DB_TABLE_POOL,
                                 (UINT1 *) (pMipDbNode));
        }
        pMipDbNode = pMipDbNextNode;
    } ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcHandleMepHldTmrExpiry                          *
 *                                                                           *
 * Description        : This function handles the MEP Archive hold timer     *
 *                      expiry event                                         *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC VOID
EcfmCcHandleMepHldTmrExpiry (tEcfmCcMdInfo * pMdInfo)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Get the MA Assocated with this MD */
    pMaNode = RBTreeGetFirst (pMdInfo->MaTable);
    while (pMaNode != NULL)

    {

        /* Scan through all MEPs */
        pMepNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));
        while (pMepNode != NULL)

        {
            if (pMepNode->pPortInfo != NULL)

            {
                if (pMepNode->b1MepCcmOffloadStatus == ECFM_FALSE)
                {
                    /* Scan through all Remote MEPs */
                    pRMepNode =
                        (tEcfmCcRMepDbInfo *)
                        TMO_DLL_First (&(pMepNode->RMepDb));
                    while (pRMepNode != NULL)

                    {

                        /* Clear out the Remote Mep Db */
                        pRMepNode->u4SeqNum = ECFM_INIT_VAL;
                        pRMepNode->u4FailedOkTime = ECFM_INIT_VAL;
                        ECFM_MEMSET (pRMepNode->RMepMacAddr, 0x00,
                                     ECFM_MAC_ADDR_LENGTH);
                        pRMepNode->b1LastRdi = ECFM_FALSE;
                        pRMepNode->b1RMepPortStatusDefect = ECFM_FALSE;
                        pRMepNode->b1RMepInterfaceStatusDefect = ECFM_FALSE;
                        pRMepNode->b1RMepCcmDefect = ECFM_FALSE;
                        pRMepNode->u1LastPortStatus = ECFM_INIT_VAL;
                        pRMepNode->u1LastInterfaceStatus = ECFM_INIT_VAL;
                        pRMepNode = (tEcfmCcRMepDbInfo *)
                            TMO_DLL_Next (&(pMepNode->RMepDb),
                                          &(pRMepNode->MepDbDllNode));
                    }
                }
            }
            pMepNode = (tEcfmCcMepInfo *) TMO_DLL_Next
                (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));
        }
        pMaNode = RBTreeGetNext (pMdInfo->MaTable, pMaNode, NULL);
    }
    ECFM_CC_TRC_FN_EXIT ();
}

/****************************************************************************
  End of File cfmccrsm.c
 *****************************************************************************/
