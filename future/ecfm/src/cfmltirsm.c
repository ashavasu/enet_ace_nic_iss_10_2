/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmltirsm.c,v 1.24 2016/04/06 10:22:05 siva Exp $
 *
 * Description: This file contains the Functionality of the Linktrace 
 *              Initiator receive State Machine.
 *******************************************************************/

#include "cfminc.h"
PRIVATE tEcfmLbLtLtmReplyListInfo *EcfmLtiRxGetLtmReplyListEntry
PROTO ((UINT4 u4LtmSeqNum, tEcfmLbLtMepInfo * pMepInfo));
PRIVATE INT4        EcfmLtiRxAddLtr
PROTO ((tEcfmLbLtPduSmInfo * pPduSmInfo, tEcfmLbLtLtmReplyListInfo * pLtmNode));
PRIVATE INT4        EcfmLtiRxAddLtrInLtmReplyList
PROTO ((tEcfmLbLtLtmReplyListInfo * pLtmNode, tEcfmLbLtLtrInfo * pLtrNode));

/*****************************************************************************
 * Function           : EcfmLtInitRxSmProcessLtr 
 *
 * Description        : This routine processes the received LTR PDU and 
 *                      if valid, an entry is made in the LtmReplyList for the 
 *                      received LTR. 
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other SM related information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / :ECFM_FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
EcfmLtInitRxSmProcessLtr (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtLtmReplyListInfo *pLtmNode = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLTInfo    *pLtirSmInfo = NULL;
    tEcfmMacAddr        MepMacAddr = {
        0
    };
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP information from the received pdusm info */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    ECFM_LBLT_GET_MAC_ADDR_OF_MEP (pMepInfo, MepMacAddr);

    /* Get the LT initiator receive SM information maintained per MEP */
    pLtirSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pPduSmInfo->pMepInfo);

    /*Y.1731: When Y1731 Module is enabled and If Ltm While Timer Expires 
     * then LTR received will not be processed */
    if ((pLtirSmInfo->u1TxLtmStatus == ECFM_TX_STATUS_READY) &&
        (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)))

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtInitRxSmProcessLtr: LTR received after the "
                       "Ltm Timeout expired so LTR will not be processed "
                       "\r\n");
        return ECFM_FAILURE;
    }

    /* If the I/G bit in the source_address indicates a Group Address,
     * instead of an Individual addres and no futher processing should happen */
    if (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxSrcMacAddr) == ECFM_TRUE)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtInitRxSmProcessLtr: Source address is a group"
                       "address. LTR cannot be processed further \r\n");
        return ECFM_FAILURE;
    }

    /* If the destination_address parameter does not contains the MAC address of 
     * the receiving MP no further processing of the LTR is performed */
    if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr)
        != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtInitRxSmProcessLtr: Destination address in "
                       "received LTR is not of the receiving MEPs Mac Address"
                       "\r\n");
        return ECFM_FAILURE;
    }

    /* If the LTR Transaction Identifier matches an LTM entry in the 
     * ltmReplyList, then a new LTR entry is attached to that LTM entry */
    pLtmNode =
        EcfmLtiRxGetLtmReplyListEntry (pPduSmInfo->uPduInfo.Ltr.u4TransId,
                                       pMepInfo);
    if (pLtmNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLtInitRxSmProcessLtr: No LTM for the received the"
                       "Transaction Id in the LTR . Incrementing Number of"
                       "Unexpected LTRs received \r\n");

        /* Increment the number of Unexpected LTRs */
        ECFM_LBLT_INCR_UNEXP_LTR_IN (pLtirSmInfo);
        return ECFM_FAILURE;
    }

    /* If Caching is Enabled then only add the LTR Node */
    if (ECFM_IS_LTR_CACHE_ENABLED () == ECFM_TRUE)

    {

        /* Add LTR entry to the LTM Reply List for the respective LTM */
        if (EcfmLtiRxAddLtr (pPduSmInfo, pLtmNode) != ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtInitRxSmProcessLtr: Adding LTM entry in the "
                           "LtmReplyListInfo FAILED \r\n");
            return ECFM_FAILURE;
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLtiRxGetLtmReplyListEntry
 *
 * DESCRIPTION      : This function returns the pointer to the LTM entry
 *                    from the LtmReplyList.
 *
 * INPUT            : u4LtmSeqNum - Transaction ID received in the LTR 
 *                                  corresponding to which an LTM entry is 
 *                                  to be found in the LtmReplyList.
 *                    pMepNode    - pointer to MEPInfo which received this LTR
 *
 * OUTPUT           : None.
 *
 * RETURNS          : pointer to LTM Node from the LtmReplyListInfo
 *
 ****************************************************************************/
PRIVATE tEcfmLbLtLtmReplyListInfo *
EcfmLtiRxGetLtmReplyListEntry (UINT4 u4LtmSeqNum, tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLtmReplyListInfo LtmNode;
    tEcfmLbLtLtmReplyListInfo *pLtmEntry = NULL;
    ECFM_MEMSET (&LtmNode, ECFM_INIT_VAL, sizeof (tEcfmLbLtLtmReplyListInfo));
    LtmNode.u4MdIndex = pMepInfo->u4MdIndex;
    LtmNode.u4MaIndex = pMepInfo->u4MaIndex;
    LtmNode.u2MepId = pMepInfo->u2MepId;
    LtmNode.u4LtmSeqNum = u4LtmSeqNum;

    /* Get LTM entry for the Sequence Number from the LtmReplyList tree 
     * maintained per MEP */
    pLtmEntry =
        (tEcfmLbLtLtmReplyListInfo *) RBTreeGet (ECFM_LBLT_LTM_REPLY_LIST,
                                                 &LtmNode);
    return pLtmEntry;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLtiRxAddLtr 
 *
 * DESCRIPTION      : This is function is used to add LTR entry in the DLL 
 *                    maintained per LTM based on the LTMs Transaction Id. 
 *                    Same LTR entry is also maintained in the GlobalInfo.
 *                    It allocates memory for the node and fills the node 
 *                    with the LTR info from the received LTR PDU.
 *
 * INPUT            : pMepNode  pointer to mep corresponding to which LTR 
 *                               entry needs to be added.
 *                    pLtmNode - pointer to node at which LTR node needs to 
 *                               be added.
 *                       
 * OUTPUT           : None.
 *
 * RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLtiRxAddLtr (tEcfmLbLtPduSmInfo * pPduSmInfo, tEcfmLbLtLtmReplyListInfo
                 * pLtmNode)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmLbLtRxLtrPduInfo *pLtrInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLTInfo    *pLtirSmInfo = NULL;
    tEcfmLbLtLtmReplyListInfo *pLtmFirstNode = NULL;
    tEcfmAppTimer      *pLtmWhileTmr = NULL;
    UINT4               u4TimeLeft = ECFM_INIT_VAL;
    INT4                i4Result = ECFM_SUCCESS;
    UINT1               u1Count = ECFM_INIT_VAL;
    tEcfmLbLtLtmReplyListInfo LtmNode;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP Information from the received PduSm Info */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get LTR PDU info from the received PduSm Info */
    pLtrInfo = ECFM_LBLT_GET_LTR_FROM_PDUSM (pPduSmInfo);

    /* Get the LT initiator receive SM information maintained per MEP */
    pLtirSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pPduSmInfo->pMepInfo);
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_TABLE (pLtrNode) == NULL)

    {

        /* If the addition of this LTR entry exceeds the resources allocated  
         * to the LTR Table and if there is only one LTM node in the          
         * LtmReplyList then the oldest LTR node should be deleted otherwise  
         * the LTM entry should be deleted after deleting the LTR nodes       
         * associated with that LTM */

        /* Check if it is the only single node in the LtmReplyList */
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();

        ECFM_MEMSET (&LtmNode, ECFM_INIT_VAL,
                     ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);
        LtmNode.u4MdIndex = 0;
        LtmNode.u4MaIndex = 0;
        LtmNode.u2MepId = 0;

        pLtmFirstNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGetNext
            (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) & LtmNode, NULL);

        while ((pLtmFirstNode != NULL) &&
               (TMO_DLL_Count (&pLtmFirstNode->LtrList) == 0))
        {
            LtmNode.u4MdIndex = pLtmFirstNode->u4MdIndex;
            LtmNode.u4MaIndex = pLtmFirstNode->u4MaIndex;
            LtmNode.u2MepId = pLtmFirstNode->u2MepId;
            LtmNode.u4LtmSeqNum = pLtmFirstNode->u4LtmSeqNum;

            pLtmFirstNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGetNext
                (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) & LtmNode, NULL);
        }

        if ((pLtmFirstNode != pLtmNode) && (pLtmFirstNode != NULL))

        {
            EcfmLbLtUtilHandleLtNodeAddFail (pLtmFirstNode);

            /* Allocate Memory to the LTR Node to be addded in the            
             * LtmReplyList Info */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_TABLE (pLtrNode) == NULL)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC,
                               "EcfmLtiRxAddLtr: Memory Allocation to LTR node"
                               "FAILED\r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }
        }

        else

        {

            /* Get First LTR(oldest) Node */
            pLtrNode =
                (tEcfmLbLtLtrInfo *) TMO_DLL_First (&(pLtmNode->LtrList));
            if (pLtrNode != NULL)

            {

                /* Delete the LTR node from the DLL maintained by MEP per LTM */
                TMO_DLL_Delete (&(pLtmNode->LtrList),
                                &(pLtrNode->LtrTableMepDllNode));

		        if (pLtrNode->EgressPortId.pu1Octets != NULL)
		        {
		        	ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
		        			pLtrNode->EgressPortId.pu1Octets);
		        	pLtrNode->EgressPortId.pu1Octets = NULL;
		        }
		        if (pLtrNode->IngressPortId.pu1Octets != NULL)
		        {
		        	ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
		        			pLtrNode->IngressPortId.pu1Octets);
		        	pLtrNode->IngressPortId.pu1Octets = NULL;
		        }
		        /* Delete the LTR RBTree Node maintianed in the Global */
		        RBTreeRem (ECFM_LBLT_LTR_TABLE, (tRBElem *) pLtrNode);

                /* Free the memory allocated to the LTR Node from the LTR POOL */
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
            }

            /* Allocate Memory to the LTR Node to be added in the            
             * LtmReplyList Info */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_TABLE (pLtrNode) == NULL)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC,
                               "EcfmLtiRxAddLtr: Memory Allocation to LTR node"
                               "FAILED\r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }
        }
    }

    do

    {
        ECFM_MEMSET (pLtrNode, ECFM_INIT_VAL, ECFM_LBLT_LTR_INFO_SIZE);

        /* Fill the MD Index from the MepInfo */
        pLtrNode->u4MdIndex = pMepInfo->u4MdIndex;

        /* Fill the MA Index from the MepInfo */
        pLtrNode->u4MaIndex = pMepInfo->u4MaIndex;

        /* Fill the Mep Id from the MepInfo */
        pLtrNode->u2MepId = pMepInfo->u2MepId;

        /* Fill the LTR sequence number from the RxLTRInfo in PduSmInfo */
        pLtrNode->u4SeqNum = pLtrInfo->u4TransId;

        /* Fill the LTR TTL from the RxLTRInfo in PduSmInfo */
        pLtrNode->u1LtrTtl = pLtrInfo->u1ReplyTtl;

        /* Fill the LTR TTL from the RxLTRInfo in PduSmInfo */
        pLtrNode->u1RelayAction = pLtrInfo->u1RelayAction;

        /* Fill the Time elapsed in receiving the LTR */

        pLtmWhileTmr = &(pLtirSmInfo->LtmTxTimer.TimerNode);
        if (ECFM_GET_REMAINING_TIME
            (ECFM_LBLT_TMRLIST_ID, pLtmWhileTmr, &u4TimeLeft) != ECFM_SUCCESS)

        {
            u4TimeLeft = ECFM_INIT_VAL;
        }
        else
        {
            if (u4TimeLeft != 0)
            {
                pLtrNode->u2TimeTakenToRcvLtr = pLtirSmInfo->u2LtrTimeOut -
                    (UINT2)
                    ECFM_CONVERT_TIME_TICKS_TO_SNMP_TIME_TICKS (u4TimeLeft);
            }
            else
            {
                pLtrNode->u2TimeTakenToRcvLtr = pLtirSmInfo->u2LtrTimeOut;
            }
        }

        /* Check for the Terminal MEP bit or FwdYes bit in the Flag field in the 
         * received LTR PDU */
        if (ECFM_LBLT_GET_TERM_MEP (pPduSmInfo->u1RxFlags) == ECFM_TRUE)

        {

            /* Set Terminal MEP bit from the Flag field in the header of the      
             * received LTR */
            pLtrNode->b1TerminalMep = ECFM_TRUE;
        }
        if (ECFM_LBLT_GET_FWD_YES (pPduSmInfo->u1RxFlags) == ECFM_TRUE)

        {

            /* Set LTMForward bit from the Flag field in the header of the        
             * received LTR */
            pLtrNode->b1LtmForwarded = ECFM_TRUE;
        }
        if (pLtrInfo->SenderId.ChassisId.u4OctLen != 0)

        {

            /* Fill the ChasisID from the Sender ID TLV in received LTR */
            if (pLtrNode->SenderId.ChassisId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL,
                                     pLtrNode->SenderId.ChassisId.pu1Octets);
                pLtrNode->SenderId.ChassisId.pu1Octets = NULL;
                pLtrNode->SenderId.ChassisId.u4OctLen = 0;
            }

            ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_SENDER_TLV_CHASSIS_ID
                (pLtrNode->SenderId.ChassisId.pu1Octets);
            if (pLtrNode->SenderId.ChassisId.pu1Octets == NULL)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtiRxAddLtr: unable to allocate memory for chassis-id \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                u1Count = ECFM_LBLT_LTR_FAILURE_1;
                i4Result = ECFM_FAILURE;
                break;
            }

            /* Fill the ChasisID sub type from Sender ID TLV in received LTR */
            pLtrNode->SenderId.u1ChassisIdSubType =
                pLtrInfo->SenderId.u1ChassisIdSubType;
            pLtrNode->SenderId.ChassisId.u4OctLen =
                pLtrInfo->SenderId.ChassisId.u4OctLen;
            ECFM_MEMCPY (pLtrNode->SenderId.ChassisId.pu1Octets,
                         pLtrInfo->SenderId.ChassisId.pu1Octets,
                         pLtrInfo->SenderId.ChassisId.u4OctLen);
        }
        if (pLtrInfo->SenderId.MgmtAddressDomain.u4OctLen != 0)

        {
            if (pLtrNode->SenderId.MgmtAddressDomain.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL,
                                     pLtrNode->SenderId.MgmtAddressDomain.
                                     pu1Octets);
                pLtrNode->SenderId.MgmtAddressDomain.pu1Octets = NULL;
                pLtrNode->SenderId.MgmtAddressDomain.u4OctLen = 0;
            }

            ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN
                (pLtrNode->SenderId.MgmtAddressDomain.pu1Octets);
            if (pLtrNode->SenderId.MgmtAddressDomain.pu1Octets == NULL)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtiRxAddLtr: unable to allocate memory for management"
                               " address domain \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                u1Count = ECFM_LBLT_LTR_FAILURE_2;
                i4Result = ECFM_FAILURE;
                break;
            }
            pLtrNode->SenderId.MgmtAddressDomain.u4OctLen =
                pLtrInfo->SenderId.MgmtAddressDomain.u4OctLen;
            ECFM_MEMCPY (pLtrNode->SenderId.MgmtAddressDomain.pu1Octets,
                         pLtrInfo->SenderId.MgmtAddressDomain.pu1Octets,
                         pLtrInfo->SenderId.MgmtAddressDomain.u4OctLen);
        }
        if (pLtrInfo->SenderId.MgmtAddress.u4OctLen != 0)

        {
            if (pLtrNode->SenderId.MgmtAddress.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL,
                                     pLtrNode->SenderId.MgmtAddress.pu1Octets);
                pLtrNode->SenderId.MgmtAddress.pu1Octets = NULL;
                pLtrNode->SenderId.MgmtAddress.u4OctLen = 0;
            }

            ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_SENDER_TLV_MGMT_ADDR
                (pLtrNode->SenderId.MgmtAddress.pu1Octets);
            if (pLtrNode->SenderId.MgmtAddress.pu1Octets == NULL)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtiRxAddLtr: unable to allocate memory for management"
                               " address \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                u1Count = ECFM_LBLT_LTR_FAILURE_3;
                i4Result = ECFM_FAILURE;
                break;
            }
            pLtrNode->SenderId.MgmtAddress.u4OctLen =
                pLtrInfo->SenderId.MgmtAddress.u4OctLen;
            ECFM_MEMCPY (pLtrNode->SenderId.MgmtAddress.pu1Octets,
                         pLtrInfo->SenderId.MgmtAddress.pu1Octets,
                         pLtrInfo->SenderId.MgmtAddress.u4OctLen);
        }

        /* Fill Organization Specific TLV from received LTR in the LTR Node */
        /* Fill OUI */
        ECFM_MEMCPY (pLtrNode->OrgSpecTlv.au1Oui,
                     pLtrInfo->OrgSpecific.au1Oui, ECFM_OUI_LENGTH);

        /* Fill SubType */
        pLtrNode->OrgSpecTlv.u1SubType = pLtrInfo->OrgSpecific.u1SubType;

        if (pLtrInfo->OrgSpecific.Value.u4OctLen != 0)
        {
            if (pLtrNode->OrgSpecTlv.Value.pu1Octets != NULL)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL,
                                     pLtrNode->OrgSpecTlv.Value.pu1Octets);
                pLtrNode->OrgSpecTlv.Value.pu1Octets = NULL;
                pLtrNode->OrgSpecTlv.Value.u4OctLen = 0;
            }

            ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_ORG_TLV_VALUE
                (pLtrNode->OrgSpecTlv.Value.pu1Octets);
            if (pLtrNode->OrgSpecTlv.Value.pu1Octets == NULL)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtiRxAddLtr: unable to allocate memory for"
                               "organization TLV \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                u1Count = ECFM_LBLT_LTR_FAILURE_4;
                i4Result = ECFM_FAILURE;
                break;
            }

            pLtrNode->OrgSpecTlv.Value.u4OctLen =
                pLtrInfo->OrgSpecific.Value.u4OctLen;
            ECFM_MEMCPY (pLtrNode->OrgSpecTlv.Value.pu1Octets,
                         pLtrInfo->OrgSpecific.Value.pu1Octets,
                         pLtrInfo->OrgSpecific.Value.u4OctLen);
        }
        if (pLtrInfo->ReplyEgress.PortId.u4OctLen != 0)
        {
            /* Fill the Egress Port ID from Reply Egress TLV in received LTR */
            if (pLtrNode->EgressPortId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
                                     pLtrNode->EgressPortId.pu1Octets);
                pLtrNode->EgressPortId.pu1Octets = NULL;
                pLtrNode->EgressPortId.u4OctLen = 0;
            }

            ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_EGRESS_TLV_EPORT_ID
                (pLtrNode->EgressPortId.pu1Octets);
            if (pLtrNode->EgressPortId.pu1Octets == NULL)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtiRxAddLtr: unable to allocate memory for egress"
                               " port-id \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                u1Count = ECFM_LBLT_LTR_FAILURE_5;
                i4Result = ECFM_FAILURE;
                break;
            }
            pLtrNode->EgressPortId.u4OctLen =
                pLtrInfo->ReplyEgress.PortId.u4OctLen;
            ECFM_MEMCPY (pLtrNode->EgressPortId.pu1Octets,
                         pLtrInfo->ReplyEgress.PortId.pu1Octets,
                         pLtrInfo->ReplyEgress.PortId.u4OctLen);
        }

        /* Fill Egress Port ID sub type from Reply Egress TLV in received LTR */
        pLtrNode->u1EgressPortIdSubType = pLtrInfo->ReplyEgress.u1PortIdSubType;

        /* Fill the Egress Mac Address from Reply Egress TLV in received LTR */
        ECFM_MEMCPY (pLtrNode->EgressMacAddr, pLtrInfo->ReplyEgress.MacAddr,
                     ECFM_MAC_ADDR_LENGTH);

        /* Fill the Egress Action from Reply Egress TLV in received LTR */
        pLtrNode->u1EgressAction = pLtrInfo->ReplyEgress.u1Action;
        if (pLtrInfo->ReplyIngress.PortId.u4OctLen != 0)
        {
            /* Fill the Ingress Port ID from Reply Ingress TLV in received LTR */
            if (pLtrNode->IngressPortId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
                                     pLtrNode->IngressPortId.pu1Octets);
                pLtrNode->IngressPortId.pu1Octets = NULL;
                pLtrNode->IngressPortId.u4OctLen = 0;
            }

            ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_INRESS_TLV_EPORT_ID
                (pLtrNode->IngressPortId.pu1Octets);
            if (pLtrNode->IngressPortId.pu1Octets == NULL)

            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLtiRxAddLtr: unable to allocate memory for ingress"
                               " port-id \r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                u1Count = ECFM_LBLT_LTR_FAILURE_6;
                i4Result = ECFM_FAILURE;
                break;
            }
            pLtrNode->IngressPortId.u4OctLen =
                pLtrInfo->ReplyIngress.PortId.u4OctLen;
            ECFM_MEMCPY (pLtrNode->IngressPortId.pu1Octets,
                         pLtrInfo->ReplyIngress.PortId.pu1Octets,
                         pLtrInfo->ReplyIngress.PortId.u4OctLen);
        }

        /* Fill Ingress Port ID sub type from Reply Ingress TLV in received LTR  */
        pLtrNode->u1IngressPortIdSubType =
            pLtrInfo->ReplyIngress.u1PortIdSubType;

        /* Fill the Ingress Mac Address from Reply Ingress TLV in received LTR */
        ECFM_MEMCPY (pLtrNode->IngressMacAddr,
                     pLtrInfo->ReplyIngress.MacAddr, ECFM_MAC_ADDR_LENGTH);

        /* Fill the Ingress Action from Reply Ingress TLV in received LTR */
        pLtrNode->u1IngressAction = pLtrInfo->ReplyIngress.u1Action;

        /* Fill Last Egress ID from LTR Egresss Identifier TLV in received LTR */
        ECFM_MEMCPY (pLtrNode->au1LastEgressId, pLtrInfo->EgressId.au1Last,
                     ECFM_EGRESS_ID_LENGTH);

        /* Fill Next Egress ID from LTR Egresss Identifier TLV in received LTR */
        ECFM_MEMCPY (pLtrNode->au1NextEgressId, pLtrInfo->EgressId.au1Next,
                     ECFM_EGRESS_ID_LENGTH);

        /* Add LTR Node in the DLL maintained by MEP per LTM and in the LTR Tree
         * maintianed in the Global Info also */
        if (EcfmLtiRxAddLtrInLtmReplyList (pLtmNode, pLtrNode) != ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtiRxAddLtr: LTR Node addition FAILED \r\n");
            u1Count = ECFM_LBLT_LTR_FAILURE_7;
            i4Result = ECFM_FAILURE;
            break;
        }
    }
    while (0);
    if (i4Result == ECFM_FAILURE)

    {
        switch (u1Count)

        {
            case ECFM_LBLT_LTR_FAILURE_7:
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
                                     pLtrNode->IngressPortId.pu1Octets);
                pLtrNode->IngressPortId.pu1Octets = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
		        break;
            case ECFM_LBLT_LTR_FAILURE_6:
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
                                     pLtrNode->EgressPortId.pu1Octets);
                pLtrNode->EgressPortId.pu1Octets = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
		        break;
            case ECFM_LBLT_LTR_FAILURE_5:
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL,
                                     pLtrNode->OrgSpecTlv.Value.pu1Octets);
                pLtrNode->OrgSpecTlv.Value.pu1Octets = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
		        break;
            case ECFM_LBLT_LTR_FAILURE_4:
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL,
                                     pLtrNode->SenderId.MgmtAddress.pu1Octets);
                pLtrNode->SenderId.MgmtAddress.pu1Octets = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
		        break;
            case ECFM_LBLT_LTR_FAILURE_3:
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL,
                                     pLtrNode->SenderId.MgmtAddressDomain.
                                     pu1Octets);
                pLtrNode->SenderId.MgmtAddressDomain.pu1Octets = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
		        break;
            case ECFM_LBLT_LTR_FAILURE_2:
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL,
                                     pLtrNode->SenderId.ChassisId.pu1Octets);
                pLtrNode->SenderId.ChassisId.pu1Octets = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
		        break;
            default:/* case: ECFM_LBLT_LTR_FAILURE_1*/
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) pLtrNode);
                pLtrNode = NULL;
                break;
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return i4Result;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLtiRxAddLtrInLtmReplyList 
 *
 * DESCRIPTION      : This is function is used to add LTR entry in the DLL 
 *                    maintained per LTM based on the LTMs Transaction Id. 
 *                    Same LTR entry is also maintained in the GlobalInfo.
 *
 * INPUT            :pLtmNode - pointer to node at which LTR node needs to 
 *                               be added.
 *                    pLtrNode - LTR Node to be added in the DLL.
 *
 * OUTPUT           : None.
 *
 * RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
EcfmLtiRxAddLtrInLtmReplyList (tEcfmLbLtLtmReplyListInfo * pLtmNode,
                               tEcfmLbLtLtrInfo * pLtrNode)
{
    tEcfmLbLtLtrInfo   *pLtrDllLastNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get Last LTR Entry in the DLL to increment the Receive Order in the LTR
     * DLL Node */
    pLtrDllLastNode = (tEcfmLbLtLtrInfo *) TMO_DLL_Last (&(pLtmNode->LtrList));
    if (pLtrDllLastNode != NULL)

    {
        pLtrNode->u4RcvOrder = pLtrDllLastNode->u4RcvOrder + ECFM_INCR_VAL;
    }

    else

    {
        pLtrNode->u4RcvOrder = 1;    /* first LTR received for this LTM 
                                     */
    }

    /* Add LTR  node in the DLL maintained by MEP per LTM Transaction ID */
    TMO_DLL_Add (&(pLtmNode->LtrList), (tTMO_DLL_NODE *)
                 & (pLtrNode->LtrTableMepDllNode));

    /* Add LTR node to the Global LTR Tree maintianed for MIB Walk */
    if (RBTreeAdd (ECFM_LBLT_LTR_TABLE, pLtrNode) != ECFM_RB_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLtiRxAddLtrInLtmReplyList:"
                       " RBTree Add Failed!! \r\n");

        /* Delete the DLL Node for the LTR maintianed in the LtmReplyList */
        TMO_DLL_Delete (&(pLtmNode->LtrList), &(pLtrNode->LtrTableMepDllNode));

        /* Free the memory allocated to the LTR Node from the LTR POOL */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL, (UINT1 *) pLtrNode);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_ENTRY ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtClntParseLtr 
 *
 * DESCRIPTION      : This is the LTR Parser which extracts the TLVs from 
 *                    the LTR and places then into the tEcfmLbLtPduSmInfo 
 *                    Structure.
 *
 * Input(s)         :  pPduSmInfo - Pointer to the structure that stores the 
 *                                  information regarding MP info, PDU rcvd 
 *                                  and other SM related information.
 *                     pu1Pdu     - Pointer to the PDU Received 
 *
 * OUTPUT           : None.
 *
 * RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtClntParseLtr (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    UINT2               u2TlvLength = ECFM_INIT_VAL;
    UINT1               u1TlvType = ECFM_INIT_VAL;
    tEcfmLbLtRxLtrPduInfo *pLtrPduInfo = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get LTR PDU Info from PduSmInfo */
    pLtrPduInfo = ECFM_LBLT_GET_LTR_FROM_PDUSM (pPduSmInfo);

    /* Validate the Value of FirstTLVOffset in LTR PDU */
    if (pPduSmInfo->u1RxFirstTlvOffset < ECFM_LTR_FIRST_TLV_OFFSET)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtClntParseLtr: FirstTLVOffset in received"
                       "LTR is less than the required value \r\n");
        return ECFM_FAILURE;
    }

    /* Move Pointer for 4 bytes(CFM Header) to read the CFM PDU fields */
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Read the LTR Transaction Identifier */
    ECFM_GET_4BYTE (pLtrPduInfo->u4TransId, pu1Pdu);

    /* Read Reply TTL from the received LTR */
    ECFM_GET_1BYTE (pLtrPduInfo->u1ReplyTtl, pu1Pdu);

    /* Read Relay Action from the received LTR */
    ECFM_GET_1BYTE (pLtrPduInfo->u1RelayAction, pu1Pdu);

    /* Read the received LTR till End TLV, TLVs that can be received are: 
     * 1. LTR Egress Identifier TLV, 
     * 2. Reply Ingress TLV, 
     * 3. Reply Egress TLV 
     * 4. Sender Id TLV
     * 5. Organizational Specific TLV */

    /* Get TLV Type */
    ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    while (u1TlvType != ECFM_END_TLV_TYPE)
    {
        switch (u1TlvType)

        {
            case ECFM_LTR_EGRESS_ID_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtr:"
                               "Received Egress Id TLV \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Validate the TLV length */
                if (u2TlvLength !=
                    (ECFM_LAST_EGRESS_ID_FIELD_SIZE +
                     ECFM_NEXT_EGRESS_ID_FIELD_SIZE))

                {
                    return ECFM_FAILURE;
                }

                /* Read Last Egress Identifier */
                ECFM_MEMCPY (pLtrPduInfo->EgressId.au1Last, pu1Pdu,
                             ECFM_EGRESS_ID_LENGTH);

                /* Move pointer by ECFM_EGRESS_ID_LENGTH for Next Egress Id */
                pu1Pdu = pu1Pdu + ECFM_LAST_EGRESS_ID_FIELD_SIZE;

                /* Read the Next Egress Identifier from the LTR Egress 
                 * Identifier */
                ECFM_MEMCPY (pLtrPduInfo->EgressId.au1Next, pu1Pdu,
                             ECFM_EGRESS_ID_LENGTH);

                /* Move pointer by ECFM_EGRESS_ID_LENGTH for Next Egress Id */
                pu1Pdu = pu1Pdu + ECFM_NEXT_EGRESS_ID_FIELD_SIZE;
                break;
            case ECFM_REPLY_INGRESS_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtr:"
                               "Received Reply Ingress TLV \r\n");

                /* Get 2 byte length field from the TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                /* Validate the TLV length */
                if (u2TlvLength <
                    (ECFM_INGRESS_ACTION_FIELD_SIZE +
                     ECFM_INGRESS_MAC_ADDR_FIELD_SIZE))
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtClntParseLtr:"
                                   "LTR Reply ingress TLV Length is Invalid"
                                   "Thus received LTR PDU is invalid \r\n ");
                    return ECFM_FAILURE;
                }

                /* Read Ingress Action from received LTR's ReplyIngress 
                 * TLV */
                ECFM_GET_1BYTE (pLtrPduInfo->ReplyIngress.u1Action, pu1Pdu);
                /* Discard the invalid ingress action field LTR */
                if (pLtrPduInfo->ReplyIngress.u1Action >
                    ECFM_LBLT_PORT_FILTERING_ACTION_ING_VID)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtClntParseLtr:"
                                   "LTR Reply ingress Action is Invalid\n"
                                   "Thus received LTR PDU is invalid \r\n ");
                    return ECFM_FAILURE;
                }
                /* If Ingress Action is zero then remaining field in this TLV is meaning less */
                else if (pLtrPduInfo->ReplyIngress.u1Action ==
                         ECFM_LBLT_PORT_FILTERING_ACTION_ING_NOTLV)
                {
                    break;
                }

                /* Read the Ingress MAC Address */
                ECFM_MEMCPY (pLtrPduInfo->ReplyIngress.MacAddr, pu1Pdu,
                             ECFM_MAC_ADDR_LENGTH);
                /* Checking Ingress MAC Address received in LTR Reply ingress TLV is a Group Address */
                if (ECFM_IS_MULTICAST_ADDR (pLtrPduInfo->ReplyIngress.MacAddr)
                    == ECFM_TRUE)

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtClntParseLtr:"
                                   "Ingress MAC Address received in the\n"
                                   "LTR Reply ingress TLV is group address. Thus received\n"
                                   "LTR PDU is invalid \r\n ");
                    return ECFM_FAILURE;
                }

                /* Move the pointer with MAC ADDRESS Length - 6 bytes */
                pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

                /* Check if the Length field in TLV is less than 7. If it 
                 * is, then other fields in the TLV are not present */
                /* Read the Ingress Port ID Lenght */
                if (u2TlvLength <=
                    (ECFM_INGRESS_ACTION_FIELD_SIZE +
                     ECFM_INGRESS_MAC_ADDR_FIELD_SIZE))
                {
                    break;
                }
                ECFM_GET_1BYTE (pLtrPduInfo->ReplyIngress.PortId.u4OctLen,
                                pu1Pdu);
                if (pLtrPduInfo->ReplyIngress.PortId.u4OctLen == 0)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtClntParseLtr:"
                                   "Ingress Port ID Length received in\n"
                                   "LTR Reply ingress TLV is Invalid. Thus received\n"
                                   "LTR PDU is invalid \r\n ");
                    return ECFM_FAILURE;
                }
                else
                {

                    /* Read 1 byte Port ID Subtype */
                    ECFM_GET_1BYTE (pLtrPduInfo->ReplyIngress.
                                    u1PortIdSubType, pu1Pdu);
                    if ((pLtrPduInfo->ReplyIngress.u1PortIdSubType >
                         ECFM_PORTID_SUB_IF_LOCAL)
                        || (pLtrPduInfo->ReplyIngress.u1PortIdSubType <
                            ECFM_PORTID_SUB_IF_ALIAS))
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtClntParseLtr:"
                                       "Ingress Port SubType received in"
                                       "LTR Reply ingress TLV is Invalid. Thus received "
                                       "LTR PDU is invalid \r\n ");
                        return ECFM_FAILURE;
                    }

                    /* Get the Ingress Port ID */
                    pLtrPduInfo->ReplyIngress.PortId.pu1Octets = pu1Pdu;

                    /* Move the pointer by Port Id Length received   */
                    pu1Pdu = pu1Pdu + pLtrPduInfo->ReplyIngress.PortId.u4OctLen;
                }
                break;
            case ECFM_REPLY_EGRESS_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtr:"
                               "Received Reply Egress TLV \r\n");

                /* Get 2 byte lenght field from the TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Read 1 byte Egress Action */
                ECFM_GET_1BYTE (pLtrPduInfo->ReplyEgress.u1Action, pu1Pdu);

                /* Read the Ingress MAC Address */
                ECFM_MEMCPY (pLtrPduInfo->ReplyEgress.MacAddr, pu1Pdu,
                             ECFM_MAC_ADDR_LENGTH);

                /* Move the pointer with MAC ADDR Length - 6 byte */
                pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

                /* Check if the Length field in TLV is less than 8. 
                 *  If it is, then other fields in the TLV are not present */
                /* Read the Ingress Port ID Lenght */
                if (u2TlvLength <=
                    (ECFM_EGRESS_ACTION_FIELD_SIZE +
                     ECFM_EGRESS_MAC_ADDR_FIELD_SIZE))
                {
                    break;
                }
                ECFM_GET_1BYTE (pLtrPduInfo->ReplyEgress.PortId.u4OctLen,
                                pu1Pdu);
                if (pLtrPduInfo->ReplyEgress.PortId.u4OctLen != 0)

                {

                    /* Read 1 byte Port ID Subtype */
                    ECFM_GET_1BYTE (pLtrPduInfo->ReplyEgress.
                                    u1PortIdSubType, pu1Pdu);

                    /* Read the Ingress Port ID */
                    pLtrPduInfo->ReplyEgress.PortId.pu1Octets = pu1Pdu;

                    /* Move the pointer by Port Id Lenght received */
                    pu1Pdu = pu1Pdu + pLtrPduInfo->ReplyEgress.PortId.u4OctLen;
                }
                break;
            case ECFM_SENDER_ID_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtr:"
                               "Received Sender-Id TLV \r\n");
                pSenderId = &(pLtrPduInfo->SenderId);

                /* Extract 2 byte Sender id Length, updating the offset */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Read 1 byte Chasis ID Length from Sender ID Tlv */
                ECFM_GET_1BYTE (pSenderId->ChassisId.u4OctLen, pu1Pdu);
                u2TlvLength = u2TlvLength -
                    (UINT2) ECFM_CHASSIS_ID_LENGTH_FIELD_SIZE;
                if (pSenderId->ChassisId.u4OctLen != 0)

                {

                    /* Read 1 byte Chasis Id Subtype */
                    ECFM_GET_1BYTE (pSenderId->u1ChassisIdSubType, pu1Pdu);
                    u2TlvLength = u2TlvLength -
                        (UINT2) ECFM_CHASSIS_ID_SUB_TYPE_FIELD_SIZE;

                    /* Read Chassis ID from Sender Id TLV */
                    pSenderId->ChassisId.pu1Octets = pu1Pdu;

                    /* Move the pointer by lenght of Chasis Id */
                    pu1Pdu = pu1Pdu + pSenderId->ChassisId.u4OctLen;
                    u2TlvLength =
                        u2TlvLength - (UINT1) pSenderId->ChassisId.u4OctLen;
                }

                /* Check if further Management Address Domain is present or not. 
                 * If the reminaing Length in the Length field of the TLV is Non
                 * Zero */
                if (u2TlvLength != 0)

                {

                    /* Read 1 byte Mgmt Addr Domain Length */
                    ECFM_GET_1BYTE (pSenderId->MgmtAddressDomain.u4OctLen,
                                    pu1Pdu);
                    u2TlvLength =
                        u2TlvLength -
                        (UINT2) ECFM_MGMT_ADDR_DOMAIN_LENGTH_FIELD_SIZE;

                    /* Read the Management Address Domain from Sender ID TLV */
                    pSenderId->MgmtAddressDomain.pu1Octets = pu1Pdu;

                    /* Move the Pointer by MgmtAddr Domain length */
                    pu1Pdu = pu1Pdu + pSenderId->MgmtAddressDomain.u4OctLen;
                    u2TlvLength = u2TlvLength -
                        (UINT2) pSenderId->MgmtAddressDomain.u4OctLen;
                }

                /* Check if the remaining length of the SenderId TLV Length
                 * is non-zero. If the it is non zero then Management Address
                 * field is present */
                if (u2TlvLength != 0)

                {

                    /* Read 1 byte Management Address Length */
                    ECFM_GET_1BYTE (pSenderId->MgmtAddress.u4OctLen, pu1Pdu);

                    /* Read the Mgmt Addr from the Sender ID */
                    pSenderId->MgmtAddress.pu1Octets = pu1Pdu;

                    /* Move the pointer by Mgmt Addr lenght */
                    pu1Pdu = pu1Pdu + (UINT1) pSenderId->MgmtAddress.u4OctLen;
                }
                break;
            case ECFM_ORG_SPEC_TLV_TYPE:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtr:"
                               "Received Organization Specific TLV \r\n");

                /* Get 2 byte lenght field from the TLV */
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);
                ECFM_MEMCPY (pLtrPduInfo->OrgSpecific.au1Oui, pu1Pdu,
                             ECFM_OUI_LENGTH);
                pu1Pdu = pu1Pdu + ECFM_OUI_LENGTH;

                /* Read 1 byte OUI Sub type from the TLV */
                ECFM_GET_1BYTE (pLtrPduInfo->OrgSpecific.u1SubType, pu1Pdu);

                /* Length for the Value field in the Org specific TLV 
                 * is calculated */
                u2TlvLength =
                    u2TlvLength - (UINT2) (ECFM_OUI_FIELD_SIZE +
                                           ECFM_ORG_SPEC_SUB_TYPE_FIELD_SIZE);

                /* Store the Length of the Value field in SenderID TLV */
                pLtrPduInfo->OrgSpecific.Value.u4OctLen = (UINT4) (u2TlvLength);
                if (pLtrPduInfo->OrgSpecific.Value.u4OctLen != 0)

                {

                    /* Read VALUE in OrgSpecific TLV */
                    pLtrPduInfo->OrgSpecific.Value.pu1Octets = pu1Pdu;

                    /* Move the pointer by lenght of the Value field from the org 
                     * specific TLV */
                    pu1Pdu = pu1Pdu + u2TlvLength;
                }
                break;
            default:
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntParseLtr: TLV NOT SUPPORTED \r\n");
                ECFM_GET_2BYTE (u2TlvLength, pu1Pdu);

                /* Check if the value is present for the TLv or not,if length
                 * is zero it means value is not present we have to increment 
                 * the offset by 2 byte only, else offset is incremented 
                 * 2byte + length of the value field */
                if (u2TlvLength != 0)

                {
                    pu1Pdu = pu1Pdu + u2TlvLength;
                }

                else

                {
                    pu1Pdu = pu1Pdu + (UINT1) ECFM_TLV_LENGTH_FIELD_SIZE;
                }
        }
        ECFM_GET_1BYTE (u1TlvType, pu1Pdu);
    }
    return ECFM_SUCCESS;
}

/******************************************************************************/
/*                           End  of cfmltirsm.c                              */
/******************************************************************************/
