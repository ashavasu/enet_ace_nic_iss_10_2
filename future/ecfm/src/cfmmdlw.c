/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmmdlw.c,v 1.33 2014/10/03 10:22:41 siva Exp $
 *
 * Description: This file contains the Protocol Low Level Routines
 *               for MD Table of standard ECFM MIB.
 *******************************************************************/

#include "cfminc.h"
#include  "fscfmmcli.h"
#include "fsmiy1cli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdTableNextIndex
 Input       :  The Indices

                The Object 
                retValDot1agCfmMdTableNextIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdTableNextIndex (UINT4 *pu4RetValDot1agCfmMdTableNextIndex)
{
    return EcfmMdUtlGetAgMdTableNextIndex (pu4RetValDot1agCfmMdTableNextIndex);
}

/* LOW LEVEL Routines for Table : Dot1agCfmMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmMdTable
 Input       :  The Indices
                Dot1agCfmMdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1agCfmMdTable (UINT4 u4Dot1agCfmMdIndex)
{
    return EcfmMdUtlValAgMdTable (u4Dot1agCfmMdIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmMdTable
 Input       :  The Indices
                Dot1agCfmMdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1agCfmMdTable (UINT4 *pu4Dot1agCfmMdIndex)
{
    return (nmhGetNextIndexDot1agCfmMdTable (0, pu4Dot1agCfmMdIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmMdTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmMdTable (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 *pu4NextDot1agCfmMdIndex)
{
    return EcfmMdUtlGetNextIndexAgMdTable (u4Dot1agCfmMdIndex,
                                           pu4NextDot1agCfmMdIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdFormat
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdFormat (UINT4 u4Dot1agCfmMdIndex,
                         INT4 *pi4RetValDot1agCfmMdFormat)
{

    return EcfmMdUtlGetAgMdFormat (u4Dot1agCfmMdIndex,
                                   pi4RetValDot1agCfmMdFormat);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdName
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdName (UINT4 u4Dot1agCfmMdIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValDot1agCfmMdName)
{
    return EcfmMdUtlGetAgMdName (u4Dot1agCfmMdIndex, pRetValDot1agCfmMdName);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdMdLevel
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdMdLevel (UINT4 u4Dot1agCfmMdIndex,
                          INT4 *pi4RetValDot1agCfmMdMdLevel)
{
    return EcfmMdUtlGetAgMdMdLevel (u4Dot1agCfmMdIndex,
                                    pi4RetValDot1agCfmMdMdLevel);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdMhfCreation
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdMhfCreation (UINT4 u4Dot1agCfmMdIndex,
                              INT4 *pi4RetValDot1agCfmMdMhfCreation)
{
    return EcfmMdUtlGetAgMdMhfCreation (u4Dot1agCfmMdIndex,
                                        pi4RetValDot1agCfmMdMhfCreation);

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdMhfIdPermission
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMhfIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdMhfIdPermission (UINT4 u4Dot1agCfmMdIndex,
                                  INT4 *pi4RetValDot1agCfmMdMhfIdPermission)
{
    return EcfmMdUtlGetAgMdMhfIdPermission (u4Dot1agCfmMdIndex,
                                            pi4RetValDot1agCfmMdMhfIdPermission);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdMaTableNextIndex
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdMaTableNextIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdMaNextIndex (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 *pu4RetValDot1agCfmMdMaTableNextIndex)
{
    return EcfmMdUtlGetAgMdMaTableNextIndex (u4Dot1agCfmMdIndex,
                                             pu4RetValDot1agCfmMdMaTableNextIndex);
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmMdRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                retValDot1agCfmMdRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmMdRowStatus (UINT4 u4Dot1agCfmMdIndex,
                            INT4 *pi4RetValDot1agCfmMdRowStatus)
{
    return EcfmMdUtlGetAgMdRowStatus (u4Dot1agCfmMdIndex,
                                      pi4RetValDot1agCfmMdRowStatus);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmMdFormat
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMdFormat (UINT4 u4Dot1agCfmMdIndex,
                         INT4 i4SetValDot1agCfmMdFormat)
{
    return EcfmMdUtlSetAgMdFormat (u4Dot1agCfmMdIndex,
                                   i4SetValDot1agCfmMdFormat);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMdName
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMdName (UINT4 u4Dot1agCfmMdIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValDot1agCfmMdName)
{
    return EcfmMdUtlSetAgMdName (u4Dot1agCfmMdIndex, pSetValDot1agCfmMdName);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMdMdLevel
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMdMdLevel (UINT4 u4Dot1agCfmMdIndex,
                          INT4 i4SetValDot1agCfmMdMdLevel)
{
    return EcfmMdUtlSetAgMdMdLevel (u4Dot1agCfmMdIndex,
                                    i4SetValDot1agCfmMdMdLevel);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMdMhfCreation
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMdMhfCreation (UINT4 u4Dot1agCfmMdIndex,
                              INT4 i4SetValDot1agCfmMdMhfCreation)
{
    return EcfmMdUtlSetAgMdMhfCreation (u4Dot1agCfmMdIndex,
                                        i4SetValDot1agCfmMdMhfCreation);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMdMhfIdPermission
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdMhfIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMdMhfIdPermission (UINT4 u4Dot1agCfmMdIndex,
                                  INT4 i4SetValDot1agCfmMdMhfIdPermission)
{
    return EcfmMdUtlSetAgMdMhfIdPermission (u4Dot1agCfmMdIndex,
                                            i4SetValDot1agCfmMdMhfIdPermission);
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmMdRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                setValDot1agCfmMdRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmMdRowStatus (UINT4 u4Dot1agCfmMdIndex,
                            INT4 i4SetValDot1agCfmMdRowStatus)
{
    return EcfmMdUtlSetAgMdRowStatus (u4Dot1agCfmMdIndex,
                                      i4SetValDot1agCfmMdRowStatus);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMdFormat
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMdFormat (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                            INT4 i4TestValDot1agCfmMdFormat)
{
    return EcfmMdUtlTestv2AgMdFormat (pu4ErrorCode,
                                      u4Dot1agCfmMdIndex,
                                      i4TestValDot1agCfmMdFormat);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMdName
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMdName (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValDot1agCfmMdName)
{

    return EcfmMdUtlTestv2AgMdName (pu4ErrorCode,
                                    u4Dot1agCfmMdIndex,
                                    pTestValDot1agCfmMdName);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMdMdLevel
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMdMdLevel (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                             INT4 i4TestValDot1agCfmMdMdLevel)
{
    return EcfmMdUtlTestv2AgMdMdLevel (pu4ErrorCode,
                                       u4Dot1agCfmMdIndex,
                                       i4TestValDot1agCfmMdMdLevel);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMdMhfCreation
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMdMhfCreation (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                 INT4 i4TestValDot1agCfmMdMhfCreation)
{
    return EcfmMdUtlTestv2AgMdMhfCreation (pu4ErrorCode,
                                           u4Dot1agCfmMdIndex,
                                           i4TestValDot1agCfmMdMhfCreation);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMdMhfIdPermission
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdMhfIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMdMhfIdPermission (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     INT4 i4TestValDot1agCfmMdMhfIdPermission)
{
    return EcfmMdUtlTestAgMdMhfIdPermission (pu4ErrorCode,
                                             u4Dot1agCfmMdIndex,
                                             i4TestValDot1agCfmMdMhfIdPermission);
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmMdRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex

                The Object 
                testValDot1agCfmMdRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmMdRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                               INT4 i4TestValDot1agCfmMdRowStatus)
{
    return EcfmMdUtlTestv2AgMdRowStatus (pu4ErrorCode,
                                         u4Dot1agCfmMdIndex,
                                         i4TestValDot1agCfmMdRowStatus);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmMdTable
 Input       :  The Indices
                Dot1agCfmMdIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmMdTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/******************************************************************************/
/*                           End  of file cfmmdlw.c                           */
/******************************************************************************/
