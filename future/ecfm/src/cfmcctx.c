/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmcctx.c,v 1.49 2015/09/02 12:19:21 siva Exp $
 *
 * Description: This file contains the functionality of the
 *              tranmsitter for CC Task in Control Sub Module
 *******************************************************************/

#include "cfminc.h"
/******************************************************************************
 * Function Name      : EcfmCcADCtrlTxTransmitPkt 
 *
 * Description        : This routine is used to transmit the CFM-PDU. It 
 *                      transmits the CRU buffer to the frame filtering or port
 *                      depending upon the Mp Direction
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Port number
 *                      pVlanInfo - Vlan tag information of the MP.
 *                      u1MpDirection - Direction of the transmitting MP UP/DOWN
 *                      u1Opcode - PDU Type 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcADCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                           tEcfmVlanTag * pVlanInfo, UINT1 u1MpDirection,
                           UINT1 u1OpCode)
{
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_BUF_SET_OPCODE (pBuf, u1OpCode);
    /* Clearing values of PduSmInfo structure */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    PduSmInfo.pBuf = pBuf;
    PduSmInfo.pHeaderBuf = NULL;
    PduSmInfo.u1RxDirection = u1MpDirection;
    ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo),
                 pVlanInfo, sizeof (tEcfmVlanTag));

    EcfmCcCtrlTxParsePduHdrs (&PduSmInfo);
    if (ECFM_IS_MEP_VLAN_AWARE (pVlanInfo->OuterVlanTag.u2VlanId))
    {
        PduSmInfo.u4RxVlanIdIsId = pVlanInfo->OuterVlanTag.u2VlanId;
        switch (u1OpCode)
        {
                /* Process PDU if Client Level MEP exists on this port */
            case ECFM_OPCODE_AIS:
            case ECFM_OPCODE_LCK:

                if (EcfmCcADSendForAllVLAN (pBuf, u2LocalPort, pVlanInfo,
                                            u1MpDirection) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcCtrlTxTransmitPkt:"
                                 "EcfmCcADSendForAllVLAN returned failure\r\n");
                }
                break;
            default:
                if (EcfmCcTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                               u2LocalPort,
                                               PduSmInfo.u4RxVlanIdIsId,
                                               u1MpDirection) == ECFM_TRUE)
                {
                    EcfmCcCtrlRxPktFree (pBuf);
                    pBuf = NULL;
                    return ECFM_SUCCESS;
                }
                if (u1MpDirection == ECFM_MP_DIR_DOWN)
                {
                    /* If Down MP is Vlan aware then transmit the the CFM-PDU to the
                     * Port */
                    if(EcfmVlanIsMemberPort(ECFM_CC_CURR_CONTEXT_ID (),                                          u2LocalPort,*pVlanInfo)!=ECFM_SUCCESS)
                    {
                        ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlTxTransmitPkt:"
                                     "EcfmCcADCtrlTxFwdToFf returned failure\r\n");
                        return ECFM_FAILURE;
                    }

                    if (EcfmCcADCtrlTxFwdToPort (pBuf, u2LocalPort, pVlanInfo)
                        == ECFM_FAILURE)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlTxTransmitPkt:"
                                     "EcfmCcADCtrlTxFwdToPort returned"
                                     "failure\r\n");
                        return ECFM_FAILURE;
                    }
                    break;
                }
                else
                {
                    /* If Up MP is Vlan aware then transmit the the CFM-PDU to the
                     * frame-filtering 
                     */
                    if(EcfmVlanIsMemberPort(ECFM_CC_CURR_CONTEXT_ID (),
			u2LocalPort,*pVlanInfo)!=ECFM_SUCCESS)
                    {
                         ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
                         ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                      ECFM_CONTROL_PLANE_TRC,
                                      "EcfmCcCtrlTxTransmitPkt:"
                                      "EcfmCcADCtrlTxFwdToFf returned failure\r\n");
                         return ECFM_FAILURE;
                    }

                    if (EcfmCcADCtrlTxFwdToFf (&PduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlTxTransmitPkt:"
                                     "EcfmCcADCtrlTxFwdToFf returned failure\r\n");
                        return ECFM_FAILURE;
                    }
                    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
                    {
                        /*Incrementing Counters for Transmitted PDUs */
                        ECFM_CC_INCR_CTX_TX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->
                                                           u4ContextId);
                        ECFM_CC_INCR_CTX_TX_COUNT (PduSmInfo.pPortInfo->
                                                   u4ContextId, u1OpCode);
                        ECFM_CC_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
                        ECFM_CC_INCR_TX_COUNT (u2LocalPort,
                                               ECFM_BUF_GET_OPCODE (pBuf));
                    }
                    break;
                }
        }                        /* end of If up/Down MP is Vlan aware */
        EcfmCcCtrlRxPktFree (pBuf);
        pBuf = NULL;
    }
    else                        /* If MP is Vlan un-aware */
    {
        UINT4               u4PduLength = ECFM_INIT_VAL;
        UINT4               u4TempIfIndex = ECFM_INIT_VAL;
        u4PduLength = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);
        /* Process PDU if Client Level MEP exists on this port */
        if ((u1OpCode == ECFM_OPCODE_AIS) || (u1OpCode == ECFM_OPCODE_LCK))
        {
            if (EcfmCcADSendForAllVLAN (pBuf, u2LocalPort, pVlanInfo,
                                        u1MpDirection) != ECFM_SUCCESS)
            {
                /* Memory will be freed from where the state machine is 
                 * handled. */
                return ECFM_FAILURE;
            }
            else
            {
                EcfmCcCtrlRxPktFree (pBuf);
                return ECFM_SUCCESS;
            }
        }
        if (EcfmCcTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                       u2LocalPort,
                                       PduSmInfo.u4RxVlanIdIsId,
                                       ECFM_MP_DIR_DOWN) == ECFM_TRUE)
        {
            EcfmCcCtrlRxPktFree (pBuf);
            pBuf = NULL;
            return ECFM_SUCCESS;
        }

        if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: NODE NOT ACTIVE- Dropping the packet \r\n");
            EcfmCcCtrlRxPktFree (pBuf);
        }
        else
        {
            if (ECFM_CC_GET_PORT_INFO (u2LocalPort) != NULL)
            {
                u4TempIfIndex = (ECFM_CC_PORT_INFO (u2LocalPort)->u4IfIndex);
            }
            else
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlTxTransmitPkt: No Port Information "
                             "present- CFMPDU transmission FAILED!!!\r\n");
                EcfmCcCtrlRxPktFree (pBuf);
                return ECFM_FAILURE;
            }

            /* Routine posts the PDU to CFA task */
            if (EcfmL2IwfHandleOutgoingPktOnPort
                (pBuf, u4TempIfIndex, u4PduLength, ECFM_PROT_BPDU,
                 ECFM_ENCAP_NONE) != L2IWF_SUCCESS)
            {
                ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlTxTransmitPkt: Transmit CFMPDU to a"
                             "Port failed\r\n");
                ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
                ECFM_CC_INCR_CTX_TX_FAILED_COUNT (PduSmInfo.pPortInfo->
                                                  u4ContextId);
                return ECFM_FAILURE;
            }

            if ((PduSmInfo.pPortInfo->u1IfOperStatus == CFA_IF_DOWN) &&
                (u1MpDirection == ECFM_MP_DIR_DOWN))
            {
                ECFM_CC_TRC_FN_EXIT ();
                return ECFM_SUCCESS;
            }

            ECFM_CC_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
            ECFM_CC_INCR_CTX_TX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->
                                               u4ContextId);
            ECFM_CC_INCR_TX_COUNT (u2LocalPort, u1OpCode);
            ECFM_CC_INCR_CTX_TX_COUNT (PduSmInfo.pPortInfo->u4ContextId,
                                       u1OpCode);
        }
    }                            /* end of If MP is Vlan aware/Unaware */
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcADCtrlTxFwdToFf
 *
 * Description        : This routine is used to forward the CFM-PDU to the Frame 
 *                      filtering 
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                       structure holds the information about the MP and the 
 *                       so far parsed CFM-PDU.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcADCtrlTxFwdToFf (tEcfmCcPduSmInfo * pPduSmInfo)
{
    UINT1              *pu1FwdPortList = NULL;
    UINT1              *pu1DeMuxPortList = NULL;
    UINT1              *pu1TempDeMuxPortList = NULL;
    UINT1              *pu1FloodPortList = NULL;
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmBufChainHeader *pCruBuf = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT2               u2WordIndex = 0;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2ByteEnd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2Vid = ECFM_INIT_VAL;
    INT4                i4RetVal = 0;
    INT4                i4LoopCount = 0;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT1               au1NullPorts[ECFM_WORD_SIZE] = { 0 };
    UINT4               au4EgressPorts[ISS_MAX_UPLINK_PORTS];
    tEcfmMacAddr        DestAddr;
    tEcfmMacAddr        SrcAddr;
    tEcfmCcPduSmInfo    DupPduSmInfo;
    tIssPortIsoInfo     PortIsoInfo;
    tEcfmCcStackInfo   *pStackInfo = NULL;
    BOOL1               bUpMep = ECFM_FALSE;

    MEMSET (&au4EgressPorts,0,sizeof(au4EgressPorts));
    MEMSET (&PortIsoInfo,0,sizeof(tIssPortIsoInfo));

    ECFM_CC_TRC_FN_ENTRY ();

    pu1FwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1FwdPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Error in allocating memory "
                     "for FwdPortList\r\n");
        return ECFM_FAILURE;
    }
    pu1DeMuxPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1DeMuxPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Error in allocating memory "
                     "for DeMuxPortList\r\n");
        UtilPlstReleaseLocalPortList (pu1FwdPortList);
        return ECFM_FAILURE;
    }

    pu1FloodPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1FloodPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Error in allocating memory "
                     "for FloodPortList\r\n");
        UtilPlstReleaseLocalPortList (pu1FwdPortList);
        UtilPlstReleaseLocalPortList (pu1DeMuxPortList);
        return ECFM_FAILURE;
    }

    pu1TempDeMuxPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1TempDeMuxPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Error in allocating memory "
                     "for pu1TempDeMuxPortListt\r\n");
        UtilPlstReleaseLocalPortList (pu1FwdPortList);
        UtilPlstReleaseLocalPortList (pu1DeMuxPortList);
        UtilPlstReleaseLocalPortList (pu1FloodPortList);
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pu1FwdPortList, 0, sizeof (tLocalPortListExt));
    ECFM_MEMSET (pu1DeMuxPortList, 0, sizeof (tLocalPortListExt));
    ECFM_MEMSET (pu1FloodPortList, 0, sizeof (tLocalPortListExt));
    ECFM_MEMSET (pu1TempDeMuxPortList, 0, sizeof (tLocalPortListExt));

    pPortInfo = pPduSmInfo->pPortInfo;
    u2LocalPort = pPortInfo->u2PortNum;
    u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
    pCruBuf = pPduSmInfo->pBuf;
    ECFM_MEMSET (SrcAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (DestAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));

    ECFM_COPY_FROM_CRU_BUF (pCruBuf, DestAddr, 0, ECFM_MAC_ADDR_LENGTH);
    ECFM_COPY_FROM_CRU_BUF (pCruBuf, SrcAddr, ECFM_MAC_ADDR_LENGTH,
                            ECFM_MAC_ADDR_LENGTH);

    if (EcfmVlanGetFwdPortList
        (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort, SrcAddr, DestAddr, u2Vid,
         pu1FwdPortList) != VLAN_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
        /* Flood the packet if it is unicast as no forwarding information was found */
        if (ECFM_IS_MULTICAST_ADDR (DestAddr) != ECFM_TRUE)
        {
            if (EcfmL2IwfMiGetVlanEgressPorts
                (ECFM_CC_CURR_CONTEXT_ID (), u2Vid,
                 pu1FwdPortList) != L2IWF_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                UtilPlstReleaseLocalPortList (pu1DeMuxPortList);
                UtilPlstReleaseLocalPortList (pu1FloodPortList);
                UtilPlstReleaseLocalPortList (pu1TempDeMuxPortList);
                return ECFM_FAILURE;
            }
        }
        else
        {
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            UtilPlstReleaseLocalPortList (pu1DeMuxPortList);
            UtilPlstReleaseLocalPortList (pu1FloodPortList);
            UtilPlstReleaseLocalPortList (pu1TempDeMuxPortList);
            return ECFM_FAILURE;
        }
    }
    /* Disable the port on which the packet was received */
    ECFM_RESET_MEMBER_PORT (pu1FwdPortList, u2LocalPort);

    ECFM_GET_DEMUX_PORTS (pu1TempDeMuxPortList);

    /* Get the sub-set of list of port on which we just need to flood the
     * CFM-PDU, on the rest of the ports continue to do level deMux
     */
    for (u2WordIndex = 0; u2WordIndex < ECFM_PORT_LIST_SIZE;
         u2WordIndex += ECFM_WORD_SIZE)
    {
        if (((u2WordIndex + ECFM_WORD_SIZE) <= ECFM_PORT_LIST_SIZE) &&
            (ECFM_MEMCMP
             ((pu1FwdPortList + u2WordIndex), &au1NullPorts,
              ECFM_WORD_SIZE) == 0))
        {
            continue;
        }
        u2ByteInd = u2WordIndex;
        u2ByteEnd = u2WordIndex + ECFM_WORD_SIZE;

        for (; u2ByteInd < u2ByteEnd; u2ByteInd++)
        {
            if (pu1FwdPortList[u2ByteInd] == 0)
            {
                continue;
            }
            /* Get the sub-set of list of port on which we just need to flood the
             * CFM-PDU, on the rest of the ports continue to do level deMux
             */
            pu1DeMuxPortList[u2ByteInd] =
                pu1TempDeMuxPortList[u2ByteInd] & pu1FwdPortList[u2ByteInd];
            pu1FloodPortList[u2ByteInd] =
                (~pu1TempDeMuxPortList[u2ByteInd]) &
                (pu1FwdPortList[u2ByteInd]);
            if (pu1DeMuxPortList[u2ByteInd] == 0)
            {
                continue;
            }
            u1PortFlag = pu1DeMuxPortList[u2ByteInd];
            for (u2BitIndex = 0;
                 ((u2BitIndex < BITS_PER_BYTE)
                  && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
                 u2BitIndex++)
            {
                u2Port =
                    (u2ByteInd * BITS_PER_BYTE) +
                    EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
                /*Reset the port info */
                pPortInfo = NULL;
                /*Get the port info */
                pPortInfo = ECFM_CC_GET_PORT_INFO (u2Port);

                if (EcfmUtilCheckIfIcclInterface (pPduSmInfo->pPortInfo->u4IfIndex)
                                                        == OSIX_SUCCESS)
                {
                    if (EcfmUtilCheckIfMcLagInterface (u2Port) == OSIX_SUCCESS)
                    {
                       pStackInfo = EcfmCcUtilGetMp (u2Port,
                                                     pPduSmInfo->u1RxMdLevel,
                                                     pPduSmInfo->u4RxVlanIdIsId,
                                                     ECFM_MP_DIR_UP);

                       if (pStackInfo != NULL)
                       {
                           if (ECFM_CC_IS_MEP (pStackInfo))
                           {
                                if (pStackInfo->pMepInfo->u1Direction == ECFM_MP_DIR_UP)
                                {
                                    bUpMep = ECFM_TRUE;
                                }
                           }
                       }
                       PortIsoInfo.u4IngressPort = pPduSmInfo->pPortInfo->u4IfIndex;
                       PortIsoInfo.InVlanId = 0;

                       i4RetVal =  IssApiGetIsolationEgressPorts (&PortIsoInfo,
                                                                au4EgressPorts);

                       if ((i4RetVal == ISS_SUCCESS) && (bUpMep != ECFM_TRUE))
                       {
                           while (i4LoopCount < ISS_MAX_UPLINK_PORTS)
                           {
                               if (au4EgressPorts[i4LoopCount] == (UINT4) u2Port)
                               {
                                   /* Breaking the loop as egress port is
                                      * present in Port Isolation Table */
                                   break;
                               }
                               i4LoopCount++;
                           }

                           if (i4LoopCount == ISS_MAX_UPLINK_PORTS)
                           {
                               /* This means, the entire egress port has been
                                 * scanned and MCLAG interface is not present in
                                 * egress port list; so resetting it */
                               continue;
                           }
                        }
                     }
                }

                if (pPortInfo != NULL)
                {
                    pDupCruBuf = EcfmCcUtilDupCruBuf (pCruBuf);
                    if (pDupCruBuf != NULL)
                    {
                        /*Duplicate PDU-SM info */
                        ECFM_MEMCPY (&DupPduSmInfo, pPduSmInfo,
                                     sizeof (tEcfmCcPduSmInfo));
                        DupPduSmInfo.pBuf = pDupCruBuf;
                        DupPduSmInfo.pHeaderBuf = NULL;
                        DupPduSmInfo.u1RxDirection = ECFM_MP_DIR_UP;
                        DupPduSmInfo.pPortInfo = pPortInfo;
                        DupPduSmInfo.pMepInfo = NULL;
                        DupPduSmInfo.pMipInfo = NULL;
                        DupPduSmInfo.pStackInfo = NULL;
                        if (EcfmCcCtrlRxLevelDeMux (&DupPduSmInfo) !=
                            ECFM_SUCCESS)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "EcfmCcADCtrlTxFwdToFf: Level DeMux"
                                         "returned failure\r\n");
                            ECFM_CC_INCR_DSRD_CFM_PDU_COUNT (u2Port);
                            ECFM_CC_INCR_CTX_DSRD_CFM_PDU_COUNT
                                (pPortInfo->u4ContextId);
                        }
                        /*Free the duplicated buffer */
                        EcfmCcCtrlRxPktFree (pDupCruBuf);
                        pDupCruBuf = NULL;
                    }
                    else
                    {
                        /* Unable to duplicate the buffer */
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcADCtrlTxFwdToFf: Error Occurred in"
                                     "duplication of CRU-Buffer\r\n");
                    }
                }
            }
        }

    }

    /* Reason for adding below logic: 
     * If CFM packets are received on ICCL interface, 
     * which needs to be forwarded on VLAN member ports, MCLAG port channel
     * may be the member of vlan which will cause looping.
     * So while constructing forward list in control plane, 
     * egress ports from port isolation has to be checked; 
     * if MC-LAG port channel interface is not present in egress port list, 
     * it can be excluded in forward port list constructed by ECFM. 
     * This is avoid loops; since port-isolation programmed for ICCL interface 
     * will work only for data packets.
     */

    if (EcfmUtilCheckIfIcclInterface (pPduSmInfo->pPortInfo->u4IfIndex) == OSIX_SUCCESS)
    {
	/* This means Pdu is recevied on ICCL interface */
        for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
              u2ByteInd++)
        {
	     if (pu1FloodPortList[u2ByteInd] == 0)
	     {
		 continue;
	     }

	     u1PortFlag = pu1FwdPortList[u2ByteInd];
	     for (u2BitIndex = 0;
		  ((u2BitIndex < BITS_PER_BYTE) && 
			(EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
		   u2BitIndex++)
	     {
		  u2Port = (u2ByteInd * BITS_PER_BYTE) +
			     EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
                  /* Reinitializing i4LoopCount */
		  i4LoopCount = 0;
		  if (EcfmUtilCheckIfMcLagInterface (u2Port) == OSIX_SUCCESS)
		  {
		      PortIsoInfo.u4IngressPort = pPduSmInfo->pPortInfo->u4IfIndex;
		      PortIsoInfo.InVlanId = 0;
		      i4RetVal =  IssApiGetIsolationEgressPorts (&PortIsoInfo, 
								au4EgressPorts);
		      if (i4RetVal == ISS_SUCCESS)
		      {
		       	  while (i4LoopCount < ISS_MAX_UPLINK_PORTS)
			  {
			      if (au4EgressPorts[i4LoopCount] == (UINT4) u2Port)
			      {
				  /* Breaking the loop as egress port is 
				   * present in Port Isolation Table */
				  break;
			      }
			      i4LoopCount++;
			      if (i4LoopCount == ISS_MAX_UPLINK_PORTS)
			      {
				  /* This means, the entire egress port has been 
				   * scanned and MCLAG interface is not present in
				   * egress port list; so resetting it */ 
				  ECFM_RESET_MEMBER_PORT (pu1FloodPortList, u2Port);
			      }
			  }
		      }
		  }
	     }
	}
    }


    /* Transmit the CFM-PDU to the flood list */
    if (EcfmVlanApiForwardOnPortList (ECFM_CC_CURR_CONTEXT_ID (),
                                      pPduSmInfo->pPortInfo->u4IfIndex,
                                      &(pPduSmInfo->VlanClassificationInfo),
                                      pCruBuf,
                                      pu1FloodPortList,
                                      DestAddr) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToFf: Unable to transmit CFM-PDU to "
                     " port-list\r\n");
        UtilPlstReleaseLocalPortList (pu1FwdPortList);
        UtilPlstReleaseLocalPortList (pu1DeMuxPortList);
        UtilPlstReleaseLocalPortList (pu1FloodPortList);
        UtilPlstReleaseLocalPortList (pu1TempDeMuxPortList);
        return ECFM_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pu1FwdPortList);
    UtilPlstReleaseLocalPortList (pu1DeMuxPortList);
    UtilPlstReleaseLocalPortList (pu1FloodPortList);
    UtilPlstReleaseLocalPortList (pu1TempDeMuxPortList);
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcADCtrlTxFwdToPort
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Index of Interface on which CFM-PDU needs to 
 *                                  be transmitted.
 *                      pVlanInfo - Pointer to the Vlan Tag Information of the 
 *                                  CRU Buffer
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcADCtrlTxFwdToPort (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                         tEcfmVlanTag * pVlanTag)
{
    tEcfmBufChainHeader *pDupBuf = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    if ((pTempPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort)) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToPort:"
                     "No Port information available \r\n");
        ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
        return ECFM_FAILURE;
    }

    pDupBuf = ECFM_DUPLICATE_CRU_BUF (pBuf);

    u4CurrContextId = pTempPortInfo->u4ContextId;
    if (pDupBuf == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToPort:"
                     "unable to duplicate CRU-BUFFER \r\n");
        ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (u4CurrContextId);
        return ECFM_FAILURE;
    }
#ifdef L2RED_WANTED
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToPort: NODE NOT ACTIVE- Dropping the packet \r\n");

        EcfmCcCtrlRxPktFree (pDupBuf);
        pDupBuf = NULL;
    }
    else
#endif
    if (EcfmVlanTransmitCfmFrame (pDupBuf,
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u2LocalPort, *pVlanTag,
                                      VLAN_CFM_FRAME) != ECFM_SUCCESS)
    {
        ECFM_CC_HANDLE_TRANSMIT_FAILURE (u2LocalPort,
                                         ECFM_BUF_GET_OPCODE (pBuf));
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADCtrlTxFwdToPort:" "VlanTransmitCfmFrame returned"
                     "failure\r\n");
        EcfmCcCtrlRxPktFree (pDupBuf);
        pDupBuf = NULL;
        ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (u4CurrContextId);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_ARG1 (ECFM_DATA_PATH_TRC,
                      "EcfmCcADCtrlTxFwdToPort:"
                      "CFM-PDU Out From Port [%d]\r\n", u2LocalPort);

    if (pTempPortInfo->u1IfOperStatus == CFA_IF_DOWN) 
    {
        ECFM_CC_TRC_FN_EXIT ();
        return ECFM_SUCCESS;
    }
    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
    {
        ECFM_CC_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
        ECFM_CC_INCR_CTX_TX_CFM_PDU_COUNT (u4CurrContextId);
        ECFM_CC_INCR_TX_COUNT (u2LocalPort, ECFM_BUF_GET_OPCODE (pBuf));
        ECFM_CC_INCR_CTX_TX_COUNT (u4CurrContextId, ECFM_BUF_GET_OPCODE (pBuf));
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmCcCtrlTxParsePduHdrs 
 *
 * Description        : This is called to parse the received CFM PDU by 
 *                      extracting the Header values from that and places then 
 *                      into the tEcfmCcPduSmInfo Structure.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                                   structure holds the information about the MP 
 *                                   and the so far parsed CFM-PDU
 * Output(s)          : None
 *
 * Return Value(s)    : NONE
 *
 *****************************************************************************/
PUBLIC VOID
EcfmCcCtrlTxParsePduHdrs (tEcfmCcPduSmInfo * pPduSmInfo)
{
    UINT4               u4Offset = ECFM_INIT_VAL;    /* Stores offset */
    UINT2               u2TypeLength = ECFM_INIT_VAL;    /* Store CFM PDU Type */
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;    /* Stores first byte of CFM HDR */

    ECFM_CC_TRC_FN_ENTRY ();

    /* Copy the 6byte destination address from the recived PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxDestMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    /* Copy the 6byte Received source address from the LBM PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxSrcMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        if (pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            u4Offset = u4Offset + ECFM_VLAN_HDR_SIZE;
        }
        /* Check for InnerVlan Tag */
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            u4Offset = u4Offset + ECFM_ISID_HDR_SIZE;
        }
    }
    /* Check for OuterVlan Tag */
    if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        u4Offset = u4Offset + ECFM_VLAN_HDR_SIZE;
    }
    /* Check for InnerVlan Tag */
    if (pPduSmInfo->VlanClassificationInfo.InnerVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        u4Offset = u4Offset + ECFM_VLAN_HDR_SIZE;
    }

    /* Get Type Length */
    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset, u2TypeLength);

    /* Move the pointer by the 2 byte to get the MD LEVEL */
    u4Offset = u4Offset + ECFM_VLAN_TYPE_LEN_FIELD_SIZE;

    /* Check if TypeLength value contains ECFM_PDU_TYPE_CFM type then LLC Snap  
     * Header is present then move offset by 8 bytes
     */
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)
    {
        u4Offset = u4Offset + ECFM_LLC_SNAP_HDR_SIZE;
    }

    pPduSmInfo->u1CfmPduOffset = (UINT1) u4Offset;
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1MdLvlAndVer);

    /* Read the 3MSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxMdLevel = (UINT1) (ECFM_GET_MDLEVEL (u1MdLvlAndVer));

    /* Read the 5LSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxVersion = (UINT1) (ECFM_GET_VERSION (u1MdLvlAndVer));
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flag from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset,
                         pPduSmInfo->u1RxFirstTlvOffset);
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcAHCtrlTxFwdToFf
 *
 * Description        : This routine is used to forward the CFM-PDU to the Frame 
 *                      filtering 
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                       structure holds the information about the MP and the 
 *                       so far parsed CFM-PDU.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcAHCtrlTxFwdToFf (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcPduSmInfo    DupPduSmInfo;
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmBufChainHeader *pCruBuf = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2PipPort = ECFM_INIT_VAL;
    UINT2               u2VipPort = ECFM_INIT_VAL;
    UINT2               u2Vid = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT1              *pu1FwdPortList = NULL;
    tEcfmMacAddr        DestAddr;
    tEcfmMacAddr        SrcAddr;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pu1FwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1FwdPortList == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcAHCtrlTxFwdToFf: Error in allocating memory "
                     "for FwdPortList\r\n");
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pu1FwdPortList, 0, sizeof (tLocalPortListExt));
    pPortInfo = pPduSmInfo->pPortInfo;
    u2LocalPort = pPortInfo->u2PortNum;
    pCruBuf = pPduSmInfo->pBuf;
    ECFM_MEMSET (SrcAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (DestAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_COPY_FROM_CRU_BUF (pCruBuf, DestAddr, 0, ECFM_MAC_ADDR_LENGTH);
    ECFM_COPY_FROM_CRU_BUF (pCruBuf, SrcAddr, ECFM_MAC_ADDR_LENGTH,
                            ECFM_MAC_ADDR_LENGTH);

    if (pPduSmInfo->u1RxPbbPortType == ECFM_PROVIDER_NETWORK_PORT)
    {
        pPduSmInfo->u4RxVlanIdIsId =
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId;
        /* Get the port list associated with B-vid this list includes 
         * the CBP ports and PNP port also */
        if (EcfmVlanGetFwdPortList (ECFM_CC_CURR_CONTEXT_ID (),
                                    u2LocalPort, SrcAddr, DestAddr,
                                    pPduSmInfo->PbbClassificationInfo.
                                    OuterVlanTag.u2VlanId,
                                    pu1FwdPortList) != ECFM_SUCCESS)
        {
            if (EcfmL2IwfMiGetVlanEgressPorts
                (ECFM_CC_CURR_CONTEXT_ID (),
                 pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId,
                 pu1FwdPortList) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
    }
    else if (pPduSmInfo->u1RxPbbPortType == ECFM_CUSTOMER_BACKBONE_PORT)
    {
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0)
        {
            if (EcfmPbbGetBvidForIsid (ECFM_CC_CURR_CONTEXT_ID (),
                                       u2LocalPort,
                                       pPduSmInfo->PbbClassificationInfo.
                                       InnerIsidTag.u4Isid,
                                       &u2Vid) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcAHCtrlTxFwdToFf: Unable to B-vid for the Isid\r\n");
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId = u2Vid;
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_UNTAGGED;
        }
        else
        {
            u2Vid = pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId;
        }
        if (EcfmVlanGetFwdPortList
            (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort,
             SrcAddr, DestAddr, u2Vid, pu1FwdPortList) != ECFM_SUCCESS)
        {
            if (EcfmL2IwfMiGetVlanEgressPorts (ECFM_CC_CURR_CONTEXT_ID (),
                                               u2Vid,
                                               pu1FwdPortList) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");

                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
        /* CBP information is lost at the PNP so we need to fill the BDA, and fill the
         * BSA here only.
         */
        /* for the PDU comming from PIP BDA is swapped at the ingress only */
        if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType !=
             VLAN_TAGGED) &&
            (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0))
        {
            /* Get the BDA */
            /* For multicast pdus only we need to used B-SIGA, unicast PDU will be
             * having the CDA as the B-SIGA
             */
            if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1UcaBitValue ==
                 OSIX_TRUE)
                &&
                (ECFM_IS_MULTICAST_ADDR
                 (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.CDAMacAddr) ==
                 ECFM_TRUE))
            {
                EcfmPbbGetBCompBDA (ECFM_CC_CURR_CONTEXT_ID (),
                                    u2LocalPort,
                                    pPduSmInfo->PbbClassificationInfo.
                                    InnerIsidTag.u4Isid,
                                    pPduSmInfo->PbbClassificationInfo.
                                    BDAMacAddr, L2IWF_INGRESS, OSIX_TRUE);
            }
            else
            {
                ECFM_MEMCPY (pPduSmInfo->PbbClassificationInfo.BDAMacAddr,
                             pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                             CDAMacAddr, ECFM_MAC_ADDR_LENGTH);
            }

            if ((pTempPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort)) != NULL)
            {
                ECFM_GET_MAC_ADDR_OF_PORT (pTempPortInfo->u4IfIndex,
                                           pPduSmInfo->PbbClassificationInfo.
                                           BSAMacAddr);
            }
            else
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcAHCtrlTxFwdToFf: Unable to get Port Info\r\n");
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }

        }
        /* Demux at PNPs for the B-VID */
        pPduSmInfo->u4RxVlanIdIsId = u2Vid;
    }
    else if (pPduSmInfo->u1RxPbbPortType == ECFM_PROVIDER_INSTANCE_PORT)
    {
        if (EcfmPbbGetPipVipWithIsid (ECFM_CC_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->PbbClassificationInfo.
                                      InnerIsidTag.u4Isid, &u2VipPort,
                                      &u2PipPort) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxParsePduHdrs: "
                         "Unable to get VIP information for the ISID\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        /* Get the VIP Corresponding to ISID 
         * remove this VIP from the port list associated with the Vid
         * received from Vlan */
        u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        /* Check the spanning tree port state of the VIP port */
        if (ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_CC_CURR_CONTEXT_ID (),
                                              u2VipPort,
                                              &u4IfIndex) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHCtrlTxFwdToFf: Unable to IfIndex from local-port\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        if (EcfmL2IwfGetVlanPortState (u2Vid, u4IfIndex) !=
            AST_PORT_STATE_FORWARDING)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHCtrlTxFwdToFf: spanning tree port state of VIP is not"
                         " forwarding\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }

        if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            /* get the forward port list for this S/c vlan id */
            if (EcfmVlanGetFwdPortList
                (ECFM_CC_CURR_CONTEXT_ID (), u2VipPort,
                 SrcAddr, DestAddr, u2Vid, pu1FwdPortList) != ECFM_SUCCESS)
            {
                if (EcfmL2IwfMiGetVlanEgressPorts (ECFM_CC_CURR_CONTEXT_ID (),
                                                   u2Vid,
                                                   pu1FwdPortList) !=
                    ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
                    UtilPlstReleaseLocalPortList (pu1FwdPortList);
                    return ECFM_FAILURE;
                }
            }
        }
        else
        {
            /* First check for CNP port with the PISID, if np such CNP is found
             * the use the PVID of the VIP to get the CNP port*/
            if (EcfmPbbGetCnpMemberPortsForIsid (ECFM_CC_CURR_CONTEXT_ID (),
                                                 pPduSmInfo->
                                                 PbbClassificationInfo.
                                                 InnerIsidTag.u4Isid,
                                                 pu1FwdPortList) !=
                ECFM_SUCCESS)
            {

                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
                /* Use the PVID of the VIP to get the CNP port */
                if (EcfmL2IwfMiGetVlanEgressPorts (ECFM_CC_CURR_CONTEXT_ID (),
                                                   u2Vid,
                                                   pu1FwdPortList) !=
                    ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
                    UtilPlstReleaseLocalPortList (pu1FwdPortList);
                    return ECFM_FAILURE;
                }
            }
            else
            {
                /* No C/S-VLAN present in the PDU */
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId = 0;
            }
        }
    }
    else
    {
        UINT2               u2SVlanId = ECFM_INIT_VAL;
        if (EcfmPbbGetPipVipWithIsid (ECFM_CC_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->PbbClassificationInfo.
                                      InnerIsidTag.u4Isid, &u2VipPort,
                                      &u2PipPort) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType =
            VLAN_UNTAGGED;
        /* Note: no need to send EcfmCcCtrlRxLevelDeMux, for this no UP-MEP or MIP 
         * configured on PIP, we need to send directly to the PIP port */
        /* Check the spanning-tree port state of the VIP, MEP's configured on
         * PIP send the CFM-PDUs to the VIP first and then they go to the PIP,
         * so spanning tree check needs to be placed here.*/

        /* We dont have VIPs created in ECFM, so we need to use VCM for getting
         * the If-Index of the VIP*/
        if (ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_CC_CURR_CONTEXT_ID (),
                                              u2VipPort,
                                              &u4IfIndex) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHCtrlTxFwdToFf: Unable to IfIndex from local-port\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId ==
            ECFM_INIT_VAL)
        {
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1Priority =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1Priority;
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1DropEligible =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1DropEligible;
            u2SVlanId =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        }
        else
        {
            /* Get the PVID of the CNP port */
            if (EcfmL2IwfGetVlanPortPvid (pPortInfo->u4IfIndex, &u2SVlanId) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcAHCtrlTxFwdToFf: Unable to get the PVID of the VIP port\r\n");
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
        if (EcfmL2IwfGetVlanPortState (u2SVlanId, u4IfIndex) !=
            AST_PORT_STATE_FORWARDING)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHCtrlTxFwdToFf: spanning tree port state of VIP is not"
                         " forwarding\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        /* Note: there is no need to do the demux for all the member CNP ports,
         * this CFM-PDU will have UCA bit as ON can we cannot send PBBN CFM-PDU
         * to the PB/C network*/
        /* Transmit to the corresping VIP */
        pDupCruBuf = ECFM_DUPLICATE_CRU_BUF (pCruBuf);
        if (pDupCruBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHCtrlTxFwdToFf: unable to duplicate CRU buffer\r\n");
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        if (EcfmL2IwfTransmitFrameOnVip (ECFM_CC_CURR_CONTEXT_ID (),
                                         u4IfIndex, pDupCruBuf,
                                         &(pPduSmInfo->
                                           VlanClassificationInfo),
                                         &(pPduSmInfo->
                                           PbbClassificationInfo),
                                         L2_ECFM_PACKET) != ECFM_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        else
        {
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_SUCCESS;
        }

    }
    /* Disable the port on which the packet was received */
    ECFM_RESET_MEMBER_PORT (pu1FwdPortList, u2LocalPort);
    /*Get the egress Port and forward the packet */
    for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
         u2ByteInd++)
    {
        if (pu1FwdPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = pu1FwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {
            u2Port =
                (u2ByteInd * BITS_PER_BYTE) +
                EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
            /*Reset the port info */
            pPortInfo = NULL;
            /*Get the port info */
            pPortInfo = ECFM_CC_GET_PORT_INFO (u2Port);
            if (pPortInfo != NULL)
            {
                pDupCruBuf = EcfmCcUtilDupCruBuf (pCruBuf);
                if (pDupCruBuf != NULL)
                {
                    /*Duplicate PDU-SM info */
                    ECFM_MEMCPY (&DupPduSmInfo, pPduSmInfo,
                                 sizeof (tEcfmCcPduSmInfo));
                    DupPduSmInfo.pBuf = pDupCruBuf;
                    DupPduSmInfo.pHeaderBuf = NULL;
                    DupPduSmInfo.u1RxDirection = ECFM_MP_DIR_UP;
                    DupPduSmInfo.pPortInfo = pPortInfo;
                    DupPduSmInfo.pMepInfo = NULL;
                    DupPduSmInfo.pMipInfo = NULL;
                    DupPduSmInfo.pStackInfo = NULL;
                    DupPduSmInfo.u1RxPbbPortType = pPortInfo->u1PortType;
                    if (EcfmCcCtrlRxLevelDeMux (&DupPduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcAHCtrlTxFwdToFf: Level DeMux"
                                     "returned failure\r\n");
                        ECFM_CC_INCR_DSRD_CFM_PDU_COUNT (u2Port);
                        ECFM_CC_INCR_CTX_DSRD_CFM_PDU_COUNT
                            (pPortInfo->u4ContextId);
                    }
                    /*Free the duplicated buffer */
                    EcfmCcCtrlRxPktFree (pDupCruBuf);
                    pDupCruBuf = NULL;
                }
                else
                {
                    /* Unable to duplicate the buffer */
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                 ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcAHCtrlTxFwdToFf: Error Occurred in"
                                 "duplication of CRU-Buffer\r\n");
                }

            }
        }
    }
    UtilPlstReleaseLocalPortList (pu1FwdPortList);
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcAHCtrlTxFwdToPort
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Index of Interface on which CFM-PDU needs to 
 *                                  be transmitted.
 *                      pVlanInfo - Pointer to the Vlan Tag Information of the 
 *                                  CRU Buffer
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcAHCtrlTxFwdToPort (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                         tEcfmVlanTag * pVlanTag, tEcfmPbbTag * pPbbTag)
{
    tEcfmBufChainHeader *pDupBuf = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    BOOL1               b1MemFlag = ECFM_FALSE;

    ECFM_CC_TRC_FN_ENTRY ();
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);

    if (pPortInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcAHCtrlTxFwdToPort:" "unable to get port info \r\n");
        ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
        return ECFM_FAILURE;
    }

    /* Get OpCode from the buffer */
    pDupBuf = ECFM_DUPLICATE_CRU_BUF (pBuf);

    if (pDupBuf == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcAHCtrlTxFwdToPort:"
                     "unable to duplicate CRU-BUFFER \r\n");
        ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }
#ifdef L2RED_WANTED
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcAHCtrlTxFwdToPort: NODE NOT ACTIVE- Dropping the packet \r\n");
        b1MemFlag = ECFM_TRUE;
    }
    else
#endif
    if (EcfmL2IwfTransmitCfmFrame (pDupBuf,
                                       pPortInfo->u4IfIndex, *pVlanTag,
                                       *pPbbTag,
                                       L2_ECFM_PACKET) == L2IWF_FAILURE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcAHCtrlTxFwdToPort:"
                     "EcfmL2IwfTransmitCfmFrame returned" "failure\r\n");
        ECFM_CC_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_ARG1 (ECFM_DATA_PATH_TRC,
                      "EcfmCcAHCtrlTxFwdToPort:"
                      "CFM-PDU Out From Port [%d]\r\n", u2LocalPort);

    if (b1MemFlag == ECFM_TRUE)
    {
        ECFM_RELEASE_CRU_BUF (pDupBuf, FALSE);
    }
   if (pPortInfo->u1IfOperStatus == CFA_IF_DOWN) 
    {
        ECFM_CC_TRC_FN_EXIT ();
        return ECFM_SUCCESS;
    }
    ECFM_CC_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
    ECFM_CC_INCR_CTX_TX_CFM_PDU_COUNT (pPortInfo->u4ContextId);
    ECFM_CC_INCR_TX_COUNT (u2LocalPort, ECFM_BUF_GET_OPCODE (pBuf));
    ECFM_CC_INCR_CTX_TX_COUNT (pPortInfo->u4ContextId,
                               ECFM_BUF_GET_OPCODE (pBuf));

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcAHCtrlTxTransmitPkt
 *
 * Description        : This routine is used to transmit the CFM-PDU. It 
 *                      transmits the CRU buffer to the frame filtering or port
 *                      depending upon the Mp Direction
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Port number
 *                      pVlanInfo - Vlan tag information of the MP.
 *                      u1MpDirection - Direction of the transmitting MP UP/DOWN
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcAHCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                           tEcfmVlanTag * pVlanTag,
                           tEcfmPbbTag * pPbbTag, UINT1 u1MpDirection,
                           UINT1 u1OpCode)
{
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_BUF_SET_OPCODE (pBuf, u1OpCode);
    switch (u1OpCode)
    {
            /* Process PDU if Client Level MEP exists on this port */
        case ECFM_OPCODE_AIS:
        case ECFM_OPCODE_LCK:
        {
            switch (ECFM_CC_GET_PORT_TYPE (u2LocalPort))
            {
                case ECFM_CUSTOMER_BACKBONE_PORT:
                    /* Send the AIS/LCK for all the ISIDs which are memeber
                     * of this CBP port.
                     */
                    if (EcfmSendForAllISIDFromCBP
                        (pBuf, u2LocalPort, pVlanTag, pPbbTag,
                         u1MpDirection) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcAHCtrlTxTransmitPkt:"
                                     "EcfmSendForAllISIDFromCBP returned"
                                     "failure\r\n");
                    }
                    break;
                case ECFM_PROVIDER_INSTANCE_PORT:
                case ECFM_PROVIDER_NETWORK_PORT:
                case ECFM_CNP_CTAGGED_PORT:
                case ECFM_CNP_PORTBASED_PORT:
                case ECFM_CNP_STAGGED_PORT:
                default:
                    if (EcfmCcAHSendForAllVLAN
                        (pBuf, u2LocalPort, pVlanTag, pPbbTag,
                         u1MpDirection) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcAHCtrlTxTransmitPkt:"
                                     "EcfmCcAHSendForAllVLAN returned"
                                     "failure\r\n");
                    }
                    break;
            }
        }
            break;
        default:
            /* Clearing values of PduSmInfo structure */
            ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
            PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);
            PduSmInfo.u1RxPbbPortType = ECFM_CC_GET_PORT_TYPE (u2LocalPort);
            PduSmInfo.pBuf = pBuf;
            PduSmInfo.u4RxVlanIdIsId =
                ECFM_ISID_TO_ISID_INTERNAL (pPbbTag->InnerIsidTag.u4Isid);

            PduSmInfo.u1RxDirection = u1MpDirection;
            ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo),
                         pVlanTag, sizeof (tEcfmVlanTag));
            ECFM_MEMCPY (&(PduSmInfo.PbbClassificationInfo),
                         pPbbTag, sizeof (tEcfmPbbTag));
            EcfmCcCtrlTxParsePduHdrs (&PduSmInfo);
            /* If Down MP initiated the transmission */
            if (u1MpDirection == ECFM_MP_DIR_DOWN)
            {
                /* Check id the PDU is orginated from CNP itself,
                 * */
                if ((ECFM_CC_GET_PORT_TYPE (u2LocalPort) ==
                     ECFM_CNP_CTAGGED_PORT)
                    || (ECFM_CC_GET_PORT_TYPE (u2LocalPort) ==
                        ECFM_CNP_PORTBASED_PORT)
                    || (ECFM_CC_GET_PORT_TYPE (u2LocalPort) ==
                        ECFM_CNP_STAGGED_PORT))
                {
                    ECFM_MEMCPY (&(pVlanTag->OuterVlanTag),
                                 &(pPbbTag->OuterVlanTag),
                                 sizeof (tVlanTagInfo));
                }

                if (EcfmCcTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                               u2LocalPort,
                                               PduSmInfo.u4RxVlanIdIsId,
                                               u1MpDirection) == ECFM_TRUE)
                {
                    EcfmCcCtrlRxPktFree (pBuf);
                    pBuf = NULL;
                    return ECFM_SUCCESS;
                }

                if (EcfmCcAHCtrlTxFwdToPort
                    (pBuf, u2LocalPort, pVlanTag, pPbbTag))
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcAHCtrlTxTransmitPkt:"
                                 "EcfmCcAHCtrlTxFwdToPort returned"
                                 "failure\r\n");
                    return ECFM_FAILURE;
                }
            }
            else
            {
                if (EcfmCcTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                               u2LocalPort,
                                               PduSmInfo.u4RxVlanIdIsId,
                                               u1MpDirection) == ECFM_TRUE)
                {
                    EcfmCcCtrlRxPktFree (pBuf);
                    pBuf = NULL;
                    return ECFM_SUCCESS;
                }

                if (EcfmCcAHCtrlTxFwdToFf (&PduSmInfo) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcAHCtrlTxTransmitPkt:"
                                 "EcfmCcAHCtrlTxFwdToFf returned failure\r\n");
                    return ECFM_FAILURE;
                }
            }
            break;
    }
    EcfmCcCtrlRxPktFree (pBuf);
    pBuf = NULL;
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcCtrlTxTransmitPkt
 *
 * Description        : This routine is used to transmit the CFM-PDU. It 
 *                      transmits the CRU buffer to the frame filtering or port
 *                      depending upon the Mp Direction
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2PortNum - Port number
 *                      u4VlanIdIsid - VlanId or Isid of the MEP 
 *                      u1Priority - Vlan priority to be transmited in vlan tag
 *                      u1DropEligible - drop eligibility to be transmited in vlan 
 *                                       tag 
 *                      u1Direction - Direction of the MP
 *                      u1OpCode - OpCode of the CFM-PDU
 *                      pVlanInfo - VLAN tag/classification information
 *                      pPbbInfo -  PBB tag/classification information
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, UINT2 u2PortNum,
                         UINT4 u4VlanIdIsid, UINT1 u1Priority,
                         UINT1 u1DropEligible, UINT1 u1Direction,
                         UINT1 u1OpCode, tEcfmVlanTag * pVlanInfo,
                         tEcfmPbbTag * pPbbInfo)
{
    tEcfmVlanTag        VlanTag;
    INT4                i4RetVal = ECFM_SUCCESS;

    ECFM_CC_TRC_FN_ENTRY ();
    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_FALSE)
    {
        if (pVlanInfo == NULL)
        {
            ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));
            /* Setting the value in Vlan Info structure */
            VlanTag.OuterVlanTag.u2VlanId = (UINT2) u4VlanIdIsid;
            VlanTag.OuterVlanTag.u1Priority = u1Priority;
            VlanTag.OuterVlanTag.u1DropEligible = u1DropEligible;
            VlanTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
            pVlanInfo = &VlanTag;
        }
        i4RetVal = EcfmCcADCtrlTxTransmitPkt (pBuf, u2PortNum,
                                              pVlanInfo, u1Direction, u1OpCode);
    }
    else
    {
        tEcfmPbbTag         PbbTag;
        if (pVlanInfo == NULL)
        {
            VlanTag.OuterVlanTag.u2VlanId = ECFM_INIT_VAL;
            VlanTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
            pVlanInfo = &VlanTag;
        }
        if (pPbbInfo == NULL)
        {
            PbbTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
            PbbTag.OuterVlanTag.u1Priority = u1Priority;
            PbbTag.OuterVlanTag.u1DropEligible = u1DropEligible;
            PbbTag.InnerIsidTag.u1TagType = ECFM_VLAN_UNTAGGED;
            PbbTag.InnerIsidTag.u1Priority = u1Priority;
            PbbTag.InnerIsidTag.u1DropEligible = u1DropEligible;
            if (ECFM_IS_MEP_ISID_AWARE (u4VlanIdIsid))
            {
                PbbTag.InnerIsidTag.u4Isid =
                    ECFM_ISID_INTERNAL_TO_ISID (u4VlanIdIsid);
                PbbTag.OuterVlanTag.u2VlanId = 0;
                /* UCA-Bit is only set for MEPs configured at CBP and PIP
                 * */
                if ((ECFM_CC_GET_PORT_TYPE (u2PortNum) ==
                     ECFM_CUSTOMER_BACKBONE_PORT)
                    || (ECFM_CC_GET_PORT_TYPE (u2PortNum) ==
                        ECFM_PROVIDER_INSTANCE_PORT))
                {
                    PbbTag.InnerIsidTag.u1UcaBitValue = OSIX_TRUE;
                }
            }
            else
            {
                PbbTag.OuterVlanTag.u2VlanId = u4VlanIdIsid;
                PbbTag.InnerIsidTag.u4Isid = 0;
            }
            pPbbInfo = &PbbTag;
        }
        /* If we are transmitting from a CNP, then make sure that we dont add
         * S/C Tag
         */
        ECFM_CRU_GET_STRING (pBuf, pPbbInfo->InnerIsidTag.CDAMacAddr, 0,
                             ECFM_MAC_ADDR_LENGTH);
        ECFM_CRU_GET_STRING (pBuf, pPbbInfo->InnerIsidTag.CSAMacAddr,
                             MAC_ADDR_LEN, ECFM_MAC_ADDR_LENGTH);
        i4RetVal = EcfmCcAHCtrlTxTransmitPkt (pBuf, u2PortNum,
                                              pVlanInfo, pPbbInfo,
                                              u1Direction, u1OpCode);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : EcfmSendForAllISIDFromCBP
 *
 * Description        : This routine is used to transmit the CFM-PDU in case of
 *                      AIS/LCK condition in DOWN direction.
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Port number
 *                      pVlanInfo - Vlan tag information of the MP.
 *                      u1MpDirection - Direction of the transmitting MP UP/DOWN
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmSendForAllISIDFromCBP (tEcfmBufChainHeader * pBuf,
                           UINT2 u2LocalPort, tEcfmVlanTag * pVlanTag,
                           tEcfmPbbTag * pPbbTag, UINT1 u1Direction)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmBufChainHeader *pDupBuf = NULL;
    UINT4               u4CurrIsid = ECFM_INIT_VAL;
    UINT2               u2BVid = ECFM_INIT_VAL;
    /* Clearing values of PduSmInfo structure */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    PduSmInfo.u1RxPbbPortType = ECFM_CC_GET_PORT_TYPE (u2LocalPort);
    PduSmInfo.u1RxDirection = u1Direction;
    PduSmInfo.pBuf = pBuf;
    ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo),
                 pVlanTag, sizeof (tEcfmVlanTag));
    ECFM_MEMCPY (&(PduSmInfo.PbbClassificationInfo),
                 pPbbTag, sizeof (tEcfmPbbTag));
    /* Set the UCA-Bit as always true */
    PduSmInfo.PbbClassificationInfo.InnerIsidTag.u1UcaBitValue = OSIX_TRUE;
    EcfmCcCtrlTxParsePduHdrs (&PduSmInfo);

    if (EcfmPbbGetfirstIsidForCBP (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort,
                                   &u4CurrIsid) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    do
    {
        if (pPbbTag->OuterVlanTag.u2VlanId != 0)
        {
            if (EcfmPbbGetBvidForIsid (ECFM_CC_CURR_CONTEXT_ID (),
                                       u2LocalPort, u4CurrIsid,
                                       &u2BVid) != ECFM_SUCCESS)
            {
                return ECFM_FAILURE;
            }
            if (u2BVid != pPbbTag->OuterVlanTag.u2VlanId)
            {
                if (EcfmPbbGetNextIsidForCBP
                    (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort, u4CurrIsid,
                     &u4CurrIsid) != ECFM_SUCCESS)
                {
                    return ECFM_FAILURE;
                }
                continue;
            }
        }
        PduSmInfo.PbbClassificationInfo.InnerIsidTag.u4Isid = u4CurrIsid;
        PduSmInfo.u4RxVlanIdIsId = ECFM_ISID_TO_ISID_INTERNAL (u4CurrIsid);
        pDupBuf = EcfmCcUtilDupCruBuf (pBuf);
        if (pDupBuf != NULL)
        {
            PduSmInfo.pBuf = pDupBuf;
            if (EcfmCcCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "EcfmSendForAllISIDFromCBP:"
                             "EcfmCcCtrlRxLevelDeMux returned " "failure\r\n");
            }
            /*Free the duplicated buffer */
            EcfmCcCtrlRxPktFree (pDupBuf);
            pDupBuf = NULL;
        }
    }
    while (EcfmPbbGetNextIsidForCBP (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort,
                                     u4CurrIsid, &u4CurrIsid) == ECFM_SUCCESS);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcAHSendForAllVLAN
 *
 * Description        : This routine is used to transmit the CFM-PDU in case of
 *                      AIS/LCK condition in DOWN direction.
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Port number
 *                      pVlanInfo - Vlan tag information of the MP.
 *                      u1MpDirection - Direction of the transmitting MP UP/DOWN
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcAHSendForAllVLAN (tEcfmBufChainHeader * pBuf,
                        UINT2 u2LocalPort, tEcfmVlanTag * pVlanTag,
                        tEcfmPbbTag * pPbbTag, UINT1 u1Direction)
{
    UINT1              *pVlanList = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmBufChainHeader *pDupBuf = NULL;
    UINT4               u4BitIndex = ECFM_INIT_VAL;
    UINT2               u2PortTemp = ECFM_INIT_VAL;    /*To store the local port
                                                       number whose information 
                                                       can be found in ECFM */
    UINT4               u4ByteIndex = ECFM_INIT_VAL;
    UINT4               u2CurrVid = ECFM_INIT_VAL;
    UINT1               u1VlanFlag = ECFM_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE VlanListAll;

    pVlanList = UtilVlanAllocVlanListSize (sizeof (tVlanListExt));
    if (pVlanList == NULL)
    {
        return ECFM_FAILURE;
    }
    CLI_MEMSET (pVlanList, ECFM_INIT_VAL, ECFM_VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pVlanList;
    VlanListAll.i4_Length = ECFM_VLAN_LIST_SIZE;

    /* Clearing values of PduSmInfo structure */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    PduSmInfo.u1RxPbbPortType = ECFM_CC_GET_PORT_TYPE (u2LocalPort);
    PduSmInfo.u1RxDirection = u1Direction;
    PduSmInfo.pBuf = pBuf;
    ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo),
                 pVlanTag, sizeof (tEcfmVlanTag));
    ECFM_MEMCPY (&(PduSmInfo.PbbClassificationInfo),
                 pPbbTag, sizeof (tEcfmPbbTag));
    EcfmCcCtrlTxParsePduHdrs (&PduSmInfo);
    u2PortTemp = u2LocalPort;
    if (ECFM_CC_GET_PORT_TYPE (u2LocalPort) == ECFM_PROVIDER_INSTANCE_PORT)
    {
        UINT2               u2VipPort = ECFM_INIT_VAL;
        UINT2               u2PipPort = ECFM_INIT_VAL;
        /* Get the VIP port */
        if (EcfmPbbGetPipVipWithIsid (ECFM_CC_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPbbTag->InnerIsidTag.u4Isid,
                                      &u2VipPort, &u2PipPort) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcAHSendForAllVLAN:"
                         "EcfmPbbGetPipVipWithIsid returned" "failure\r\n");
            UtilVlanReleaseVlanListSize (pVlanList);
            return ECFM_FAILURE;
        }
        u2LocalPort = u2VipPort;
    }
    if (EcfmL2IwfGetPortVlanList (ECFM_CC_CURR_CONTEXT_ID (), &VlanListAll,
                                  u2LocalPort,
                                  ECFM_L2_MEMBER_PORT) != ECFM_SUCCESS)
    {
        UtilVlanReleaseVlanListSize (pVlanList);
        return ECFM_FAILURE;
    }
    for (u4ByteIndex = 0; u4ByteIndex < ECFM_VLAN_LIST_SIZE; u4ByteIndex++)
    {
        if (VlanListAll.pu1_OctetList[u4ByteIndex] == 0)
        {
            continue;
        }
        u1VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];
        for (u4BitIndex = 0;
             ((u4BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1VlanFlag, u4BitIndex) != 0));
             u4BitIndex++)
        {
            u2CurrVid =
                (u4ByteIndex * BITS_PER_BYTE) +
                EcfmUtilQueryBitListTable (u1VlanFlag, u4BitIndex);
            PduSmInfo.VlanClassificationInfo.OuterVlanTag.u2VlanId = u2CurrVid;

            if (ECFM_CC_GET_PORT_TYPE (u2PortTemp) ==
                ECFM_PROVIDER_INSTANCE_PORT)
            {
                PduSmInfo.u4RxVlanIdIsId =
                    ECFM_ISID_TO_ISID_INTERNAL (pPbbTag->InnerIsidTag.u4Isid);
            }
            else
            {
                PduSmInfo.u4RxVlanIdIsId = u2CurrVid;
            }
            pDupBuf = EcfmCcUtilDupCruBuf (pBuf);
            if (pDupBuf != NULL)
            {
                PduSmInfo.pBuf = pDupBuf;
                if (EcfmCcCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                 ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcAHSendForAllVLAN:"
                                 "EcfmCcCtrlRxLevelDeMux returned "
                                 "failure\r\n");
                }
                /*Free the duplicated buffer */
                EcfmCcCtrlRxPktFree (pDupBuf);
                pDupBuf = NULL;
            }
        }
    }
    UtilVlanReleaseVlanListSize (pVlanList);
    return ECFM_SUCCESS;
}

/******************************************************************************     
 * Function Name      : EcfmCcADSendForAllVLAN     
 *     
 * Description        : This routine is used to transmit the CFM-PDU in case
 *                      of AIS/LCK condition in DOWN direction.     
 *      
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet     
 *                      u2LocalPort - Port number     
 *                      pVlanInfo - Vlan tag information of the MP.     
 *                      u1MpDirection - Direction of the transmitting MP
 *                                      UP/DOWN     
 * 
 * Output(s)          : None     
 *     
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE     
 ******************************************************************************/
PUBLIC INT4
EcfmCcADSendForAllVLAN (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                        tEcfmVlanTag * pVlanTag, UINT1 u1Direction)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    INT4                i4RetVal = ECFM_FAILURE;
    UINT1               u1TxMepMdLevel = ECFM_INIT_VAL;

    /* Clearing values of PduSmInfo structure */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.u1RxDirection = u1Direction;
    PduSmInfo.pBuf = pBuf;
    ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo),
                 pVlanTag, sizeof (tEcfmVlanTag));
    EcfmCcCtrlTxParsePduHdrs (&PduSmInfo);

    /* Get Md Level of the MEP transmitting AIS or LCK PDU */
    u1TxMepMdLevel = ECFM_BUF_GET_LEVEL (pBuf);

    /* Get MEP Entry from Stack Table */
    pStackInfo = EcfmCcUtilGetMp (u2LocalPort, u1TxMepMdLevel,
                                  pVlanTag->OuterVlanTag.u2VlanId, u1Direction);
    if (pStackInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    /* Get MA Entry */
    pMaInfo = pStackInfo->pMepInfo->pMaInfo;
    if (pMaInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    PduSmInfo.pMepInfo = pStackInfo->pMepInfo;
    PduSmInfo.pStackInfo = pStackInfo;
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);
    if (PduSmInfo.pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }

    /* Check of the tramsmiting MEP's MA is VLAN-aware */
    if (pMaInfo->u4PrimaryVidIsid != 0)
    {
        /* Process AIS transmission for VLAN aware MA */
        i4RetVal = EcfmCcSendForVlanAwareMa (&PduSmInfo);
    }
    else
    {
        /* Process AIS transmission for VLAN unaware MA */
        i4RetVal = EcfmCcSendForVlanUnAwareMa (&PduSmInfo);
    }
    return i4RetVal;
}

/******************************************************************************     
 * Function Name      : EcfmCcSendForVlanAwareMa
 *     
 * Description        : This routine is used to transmit the AIS-PDU from a
 *                      VLAN unaware MEP, the AIS-PDU will be transmited for all
 *                      the member VLANs of the port.
 *      
 * Input(s)           : pPduSmInfo - Pointer to structure containing AIS-PDU and
 *                      transmiting MEP information.
 * Output(s)          : None     
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE     
 ******************************************************************************/
PUBLIC INT4
EcfmCcSendForVlanAwareMa (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcVlanInfo     Key;
    tEcfmBufChainHeader *pDupBuf = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcVlanInfo    *pVlanInfo = NULL;
    UINT4               u4Event = ECFM_INIT_VAL;

    ECFM_MEMSET (&Key, ECFM_INIT_VAL, sizeof (tEcfmCcVlanInfo));
    pMaInfo = pPduSmInfo->pMepInfo->pMaInfo;
    pBuf = pPduSmInfo->pBuf;
    /* Check is a single VLAN/Multiple is mapped to the MA */
    if (pMaInfo->u2NumberOfVids >= 1)
    {
        /* Send AIS for primary-VID always */

        pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
            pMaInfo->u4PrimaryVidIsid;
        pPduSmInfo->u4RxVlanIdIsId = pMaInfo->u4PrimaryVidIsid;
        pDupBuf = EcfmCcUtilDupCruBuf (pBuf);
        if (pDupBuf != NULL)
        {
            pPduSmInfo->pBuf = pDupBuf;
            if (EcfmCcCtrlRxLevelDeMux (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcSendForVlanAwareMa:"
                             "EcfmCcCtrlRxLevelDeMux returned " "failure\r\n");
            }
            /*Free the duplicated buffer */
            EcfmCcCtrlRxPktFree (pDupBuf);
            pDupBuf = NULL;
        }
    }
    /* scan through all the VLAN associated with this MA and send for
     * all */
    if (pMaInfo->u2NumberOfVids > 1)
    {
        Key.u4PrimaryVidIsid = pMaInfo->u4PrimaryVidIsid;
        Key.u4VidIsid = 0;

        pVlanInfo =
            (tEcfmCcVlanInfo *) RBTreeGetNext (ECFM_CC_PRIMARY_VLAN_TABLE,
                                               (tRBElem *) & Key, NULL);

        ECFM_CC_RESET_STAGGERING_DELTA ();
        while ((pVlanInfo != NULL) &&
               (pVlanInfo->u4PrimaryVidIsid == pMaInfo->u4PrimaryVidIsid))
        {
            pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
                pVlanInfo->u4VidIsid;
            pPduSmInfo->u4RxVlanIdIsId = pVlanInfo->u4VidIsid;
            pDupBuf = EcfmCcUtilDupCruBuf (pBuf);
            if (pDupBuf != NULL)
            {
                pPduSmInfo->pBuf = pDupBuf;
                if (EcfmCcCtrlRxLevelDeMux (pPduSmInfo) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                 ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcADSendForAllVLAN:"
                                 "EcfmCcCtrlRxLevelDeMux returned "
                                 "failure\r\n");
                }
                /*Free the duplicated buffer */
                EcfmCcCtrlRxPktFree (pDupBuf);
                pDupBuf = NULL;
            }

            u4Event = ECFM_EV_INT_PDU_IN_QUEUE |
                ECFM_EV_CFM_PDU_IN_QUEUE | ECFM_EV_CFG_MSG_IN_QUEUE;

            EcfmCcUtilRelinquishLoop (u4Event);

            pVlanInfo =
                (tEcfmCcVlanInfo *)
                RBTreeGetNext (ECFM_CC_PRIMARY_VLAN_TABLE,
                               (tRBElem *) pVlanInfo, NULL);
        }
    }
    return ECFM_SUCCESS;
}

/******************************************************************************     
 * Function Name      : EcfmCcSendForVlanUnAwareMa
 *     
 * Description        : This routine is used to transmit the AIS-PDU from a
 *                      VLAN aware MEP based upon the association this routine
 *                      will send AIS-PDU for a single VLAN or all associated
 *                      VLAN.
 *      
 * Input(s)           : pPduSmInfo - Pointer to structure containing AIS-PDU and
 *                      transmiting MEP information.
 *      
 * Output(s)          : None     
 *     
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE     
 ******************************************************************************/
PUBLIC INT4
EcfmCcSendForVlanUnAwareMa (tEcfmCcPduSmInfo * pPduSmInfo)
{
    UINT1              *pVlanList = NULL;
    tEcfmBufChainHeader *pDupBuf = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4BitIndex = ECFM_INIT_VAL;
    UINT4               u4ByteIndex = ECFM_INIT_VAL;
    UINT4               u4ByteEnd = ECFM_INIT_VAL;
    UINT4               u4Event = ECFM_INIT_VAL;
    UINT2               u2WordIndex = ECFM_INIT_VAL;
    UINT2               u2VlanFlag = ECFM_INIT_VAL;
    UINT2               u2CurrVid = ECFM_INIT_VAL;
    UINT2               u2LocalPort = 0;
    UINT1               au1NullPorts[ECFM_WORD_SIZE] = { 0 };

    tSNMP_OCTET_STRING_TYPE VlanListAll;

    pVlanList = UtilVlanAllocVlanListSize (sizeof (tVlanListExt));
    if (pVlanList == NULL)
    {
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pVlanList, ECFM_INIT_VAL, ECFM_VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pVlanList;
    VlanListAll.i4_Length = ECFM_VLAN_LIST_SIZE;

    pBuf = pPduSmInfo->pBuf;
    pPortInfo = pPduSmInfo->pPortInfo;
    u2LocalPort = pPortInfo->u2PortNum;
    if (pPortInfo->u2ChannelPortNum != 0)
    {
        pPortInfo =
            ECFM_CC_GET_PORT_INFO (pPduSmInfo->pPortInfo->u2ChannelPortNum);
        if (pPortInfo != NULL)
        {
            u2LocalPort = pPortInfo->u2PortNum;
        }
    }

    /* Get the PVID of the port, this first AIS-PDU for unware MEP should be
     * sent using the PVID of the port
     */
    if (pPortInfo == NULL)
    {
        UtilVlanReleaseVlanListSize (pVlanList);
        return FAILURE;
    }
    if (EcfmL2IwfGetVlanPortPvid (pPortInfo->u4IfIndex, &u2CurrVid)
        != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                     ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcSendForVlanUnAwareMa:"
                     "Unable to get PVID of the port.\r\n");
        UtilVlanReleaseVlanListSize (pVlanList);
        return ECFM_FAILURE;
    }
    /* Send the first AIS for the PVID of the port */
    pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId = u2CurrVid;
    pPduSmInfo->u4RxVlanIdIsId = 0;
    pDupBuf = EcfmCcUtilDupCruBuf (pBuf);
    if (pDupBuf != NULL)
    {
        pPduSmInfo->pBuf = pDupBuf;
        if (EcfmCcCtrlRxLevelDeMux (pPduSmInfo) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                         ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcSendForVlanUnAwareMa:"
                         "EcfmCcCtrlRxLevelDeMux returned " "failure\r\n");
        }
        /*Free the duplicated buffer */
        EcfmCcCtrlRxPktFree (pDupBuf);
        pDupBuf = NULL;
    }
    /* For unaware MEP only, get the list of VLANs associated with thie port */
    /* Get VLAN Id'w with which port is a memeber Port */
    if (EcfmL2IwfGetPortVlanList (ECFM_CC_CURR_CONTEXT_ID (), &VlanListAll,
                                  u2LocalPort,
                                  ECFM_L2_MEMBER_PORT) != ECFM_SUCCESS)
    {
        UtilVlanReleaseVlanListSize (pVlanList);
        return ECFM_FAILURE;
    }
    if (pPduSmInfo->pPortInfo->u2ChannelPortNum != 0)
    {
        pPduSmInfo->pPortInfo = pPortInfo;
    }
    /* Disable the PVID in the vlan-list, as for the PVID we have already send
     * the AIS-PDU
     */
    ECFM_RESET_MEMBER_VLAN (VlanListAll.pu1_OctetList, u2CurrVid);
    ECFM_CC_RESET_STAGGERING_DELTA ();
    for (u2WordIndex = 0; u2WordIndex < ECFM_VLAN_LIST_SIZE;
         u2WordIndex += ECFM_WORD_SIZE)
    {
        if (((u2WordIndex + ECFM_WORD_SIZE) <= ECFM_VLAN_LIST_SIZE) &&
            (ECFM_MEMCMP ((VlanListAll.pu1_OctetList + u2WordIndex),
                          &au1NullPorts, ECFM_WORD_SIZE) == 0))
        {
            continue;
        }
        u4ByteIndex = u2WordIndex;
        u4ByteEnd = u2WordIndex + ECFM_WORD_SIZE;

        for (; u4ByteIndex < u4ByteEnd; u4ByteIndex++)
        {
            if (VlanListAll.pu1_OctetList[u4ByteIndex] == 0)
            {
                continue;
            }
            u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];
            for (u4BitIndex = 0;
                 ((u4BitIndex < BITS_PER_BYTE)
                  && (EcfmUtilQueryBitListTable (u2VlanFlag, u4BitIndex) != 0));
                 u4BitIndex++)
            {
                u2CurrVid =
                    (u4ByteIndex * BITS_PER_BYTE) +
                    EcfmUtilQueryBitListTable (u2VlanFlag, u4BitIndex);
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
                    u2CurrVid;
                pPduSmInfo->u4RxVlanIdIsId = u2CurrVid;
                pDupBuf = EcfmCcUtilDupCruBuf (pBuf);
                if (pDupBuf != NULL)
                {
                    pPduSmInfo->pBuf = pDupBuf;
                    if (EcfmCcCtrlRxLevelDeMux (pPduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcSendForVlanUnAwareMa:"
                                     "EcfmCcCtrlRxLevelDeMux returned "
                                     "failure\r\n");
                    }
                    /*Free the duplicated buffer */
                    EcfmCcCtrlRxPktFree (pDupBuf);
                    pDupBuf = NULL;
                }

                u4Event = ECFM_EV_INT_PDU_IN_QUEUE |
                    ECFM_EV_CFM_PDU_IN_QUEUE | ECFM_EV_CFG_MSG_IN_QUEUE;

                EcfmCcUtilRelinquishLoop (u4Event);
            }
        }
    }
    UtilVlanReleaseVlanListSize (pVlanList);
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmCcTxCheckLckCondition
 *
 * Description        : This routine is used to check is any lower level MEP
 *                      is locked.
 *
 * Input(s)           : u1MdLevel - MD Level of CFM PDU received.
 *                      u2LocalPort - Port on which CFM PDU received.
 *                      u4VlanIdIsid - Received CFM PDU's VlanId or ISID value  
 *                      u1MpDirection - Direction of MEP from which CFM PDU
 *                      received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_TRUE/ECFM_FALSE
 *****************************************************************************/

PUBLIC              BOOL1
EcfmCcTxCheckLckCondition (UINT1 u1MdLevel, UINT2 u2LocalPort,
                           UINT4 u4VlanIdIsid, UINT1 u1MpDirection)
{
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    INT1                i1MdLevel = ECFM_INIT_VAL;

    /* In case Mep is configured Out of service then block data 
     * traffic through it */
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (u2LocalPort) != ECFM_TRUE)
    {
        return ECFM_FALSE;
    }
    for (i1MdLevel = u1MdLevel - 1;
         i1MdLevel >= ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
    {
        pStackInfo = EcfmCcUtilGetMp (u2LocalPort, i1MdLevel,
                                      u4VlanIdIsid, u1MpDirection);

        /* If MP at Lower Level exists on Port */
        if (pStackInfo != NULL)
        {
            /* if Mep at Low level exists */
            if (ECFM_CC_IS_MEP (pStackInfo))
            {
                /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                 * and the MD Level
                 */
                pMepInfo = pStackInfo->pMepInfo;
                if (ECFM_CC_LCK_IS_CONFIGURED (pMepInfo) == ECFM_TRUE)
                {
                    /* Stop Processing as MEP is configured Out of Service */
                    return ECFM_TRUE;
                }
            }
        }
    }

    return ECFM_FALSE;
}

/*****************************************************************************/
/*                               end of file cfmcctx.c                       */
/*****************************************************************************/
