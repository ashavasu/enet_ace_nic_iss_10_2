/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccutl.c,v 1.66 2014/12/09 12:45:07 siva Exp $
 *
 * Description: This file contains the Utility Procedures used by
 *              CC Task.
 *******************************************************************/

#include "cfminc.h"
PRIVATE BOOL1       EcfmMepWithVidAtSameOrHgrLvl
PROTO ((UINT2 *, UINT2, UINT2, UINT1, BOOL1));
PRIVATE BOOL1 EcfmGetVlanIdsForAPrimaryVid PROTO ((UINT2 *, UINT2 *, UINT2));
PRIVATE VOID EcfmCcUtilTryCreateMip PROTO ((UINT2 u2PortNum, UINT4 u4VlanIsid));

/*****************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilGetMepEntryFrmPort
 *
 *    DESCRIPTION      : This function returns the pointer to the Mep entry
 *                       
 *    INPUT            : u2PortNum - Local Port Number  for which the entry is
 *                                   required,
 *                       u1MdLevel -   Md Level of the MEP.
 *                       u1Direction - Direction of the MEP.
 *                       u2Vid     -   Primary Vlan Id of the MEP.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to MepInfo.
 *
 *****************************************************************************/
tEcfmCcMepInfo     *
EcfmCcUtilGetMepEntryFrmPort (UINT1 u1MdLevel, UINT4 u4VidIsid, UINT2 u2PortNum,
                              UINT1 u1Direction)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    gpEcfmCcMepNode->u1Direction = u1Direction;
    gpEcfmCcMepNode->u1MdLevel = u1MdLevel;
    gpEcfmCcMepNode->u4PrimaryVidIsid = u4VidIsid;
    pMepNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode);
    return pMepNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcUtilGetMepEntryFrmGlob
 *
 *    DESCRIPTION      : This function returns the pointer to the Mep entry
 *                       corresponding to indices from the table MepTableIndex
 *                       in global structure.
 *
 *    INPUT            : u4MdIndex  - MD Index,
 *                       u4MaIndex  - MA Index,
 *                       u2MepIndex - MEP identifier
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to Mep Entry
 *
 ****************************************************************************/
PUBLIC tEcfmCcMepInfo *
EcfmCcUtilGetMepEntryFrmGlob (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    gpEcfmCcMepNode->u4MdIndex = u4MdIndex;
    gpEcfmCcMepNode->u4MaIndex = u4MaIndex;
    gpEcfmCcMepNode->u2MepId = u2MepId;

    /* Get Mep Entry */
    pMepNode =
        (tEcfmCcMepInfo *) RBTreeGet (ECFM_CC_MEP_TABLE, gpEcfmCcMepNode);
    return pMepNode;
}

/****************************************************************************
 *  
 *  FUNCTION NAME    : EcfmCcUtilFreeEntryFn
 * 
 *  DESCRIPTION      : This function releases the memory allocated to
 *                     each node of the specified Table.
 *  
 *  INPUT            : pElem - pointer to the node to be freed,
 *                     u4Arg - From which table node needs to be freed.     
 *  
 *  OUTPUT           : None.
 *
 *  RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 * **************************************************************************/
PUBLIC INT4
EcfmCcUtilFreeEntryFn (tRBElem * pElem, UINT4 u4Arg)
{
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipCcmDbInfo *pMipCcmDbNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcRMepDbInfo  *pTempRMepNode = NULL;
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcMepListInfo *pMepListNode = NULL;
    tEcfmCcStackInfo   *pStackNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tEcfmCcErrLogInfo  *pCcErrorNode = NULL;
    tEcfmCcLmInfo      *pEcfmCcLmNode = NULL;
    tEcfmCcDefaultMdTableInfo *pCcDefMdNode = NULL;
    tEcfmCcMipPreventInfo *pMipPreventInfo = NULL;
    tEcfmCcMepListInfo *pMepListNextNode = NULL;
    tEcfmCcMepListInfo  MepListInfo;

    ECFM_MEMSET (&MepListInfo, ECFM_INIT_VAL, ECFM_CC_MEP_LIST_SIZE);

    switch (u4Arg)

    {
        case ECFM_CC_VLAN_ENTRY:
            pVlanNode = (tEcfmCcVlanInfo *) pElem;

            /* Release memory allocated to VLAN node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_VLAN_TABLE_POOL,
                                 (UINT1 *) (pVlanNode));
            break;
        case ECFM_CC_GLOBAL_MIP_PREVENT_ENTRY:
            pMipPreventInfo = (tEcfmCcMipPreventInfo *) pElem;
            RBTreeRem (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                       (tRBElem *) pMipPreventInfo);
            /* Release memory allocated to Mip node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_PREVENT_TABLE_POOL,
                                 (UINT1 *) (pMipNode));
            break;
        case ECFM_CC_MIP_CCM_DB_ENTRY:
            pMipCcmDbNode = (tEcfmCcMipCcmDbInfo *) pElem;

            /* Release memory allocated to Mip node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_DB_TABLE_POOL,
                                 (UINT1 *) (pMipCcmDbNode));
            break;
        case ECFM_CC_MD_ENTRY:

        {
            tEcfmCcPduSmInfo    PduSmInfo;
            ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
            pMdNode = (tEcfmCcMdInfo *) pElem;
            PduSmInfo.pMdInfo = pMdNode;
            EcfmCcTmrStopTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD, &PduSmInfo);

            /* Destroy the Ma Tree per MD. */
            RBTreeDestroy (pMdNode->MaTable,
                           (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                           ECFM_CC_MA_ENTRY_IN_MD);

            /* Release memory allocated to MD node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MD_TABLE_POOL, (UINT1 *) (pMdNode));
            break;
        }
        case ECFM_CC_MA_ENTRY:
            pMaNode = (tEcfmCcMaInfo *) pElem;

            MepListInfo.u4MdIndex = pMaNode->u4MdIndex;
            MepListInfo.u4MaIndex = pMaNode->u4MaIndex;

            pMepListNode = RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE,
                                          (tRBElem *) & MepListInfo, NULL);
            while ((pMepListNode != NULL) &&
                   (pMepListNode->u4MdIndex == pMaNode->u4MdIndex) &&
                   (pMepListNode->u4MaIndex == pMaNode->u4MaIndex))
            {
                pMepListNextNode =
                    RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE, pMepListNode,
                                   NULL);

                RBTreeRem (ECFM_CC_MA_MEP_LIST_TABLE, pMepListNode);
                pMepListNode->pMaInfo = NULL;

                /*Release the memory allocated to MepList node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_MEP_LIST_TABLE_POOL,
                                     (UINT1 *) (pMepListNode));

                pMepListNode = pMepListNextNode;
            }

            /* Remove other references of MA node */
            pMdNode = pMaNode->pMdInfo;

            /* From MD node's MaTable */
            RBTreeRem (pMdNode->MaTable, (tRBElem *) pMaNode);
            pMaNode->pMdInfo = NULL;

            /* Release memory allocated to MA node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_TABLE_POOL, (UINT1 *) (pMaNode));
            break;
        case ECFM_CC_MA_ENTRY_IN_MD:
            pMaNode = (tEcfmCcMaInfo *) pElem;

            /* Clear the the back pointer to MD info */
            pMaNode->pMdInfo = NULL;

            /* Release memory allocated to MA node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_TABLE_POOL, (UINT1 *) (pMaNode));
            break;
        case ECFM_CC_MEPLIST_ENTRY:
            pMepListNode = (tEcfmCcMepListInfo *) pElem;

            /*Remove other references of this node, if any */
            pMaNode = pMepListNode->pMaInfo;

            /*Release the memory allocated to MepList node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_MEP_LIST_TABLE_POOL,
                                 (UINT1 *) (pMepListNode));
            break;
        case ECFM_CC_RMEP_DB_ENTRY:
            pRMepNode = (tEcfmCcRMepDbInfo *) pElem;

            /* Remove other references of RMep node */
            pMepNode = pRMepNode->pMepInfo;

            /* Remove RMep node from its associated MEP's RMepDb */
            TMO_DLL_Delete (&(pMepNode->RMepDb), &(pRMepNode->MepDbDllNode));

            /* Clear its backward pointer to MEP info and timer */
            pRMepNode->pMepInfo = NULL;

            /* Release if memory allocated to SenderId TLV */
            pSenderId = &(pRMepNode->LastSenderId);
            if (pSenderId->ChassisId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_CHASSIS_ID_POOL,
                                     pSenderId->ChassisId.pu1Octets);
                pSenderId->ChassisId.pu1Octets = NULL;
            }
            if (pSenderId->MgmtAddress.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_POOL,
                                     pSenderId->MgmtAddress.pu1Octets);
                pSenderId->MgmtAddress.pu1Octets = NULL;
            }
            if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL,
                                     pSenderId->MgmtAddressDomain.pu1Octets);
                pSenderId->MgmtAddressDomain.pu1Octets = NULL;
            }

            /* Release the memory allocated to RMep node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_DB_TABLE_POOL,
                                 (UINT1 *) (pRMepNode));
            break;
        case ECFM_CC_MEP_ENTRY:
            pMepNode = (tEcfmCcMepInfo *) pElem;

            /* Remove other references of MEP node */
            pMaNode = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);

            /* Destroy Remote Mep Db Info Tree */
            pRMepNode =
                (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
            while (pRMepNode != NULL)
            {
                pTempRMepNode = pRMepNode;
                pRMepNode = (tEcfmCcRMepDbInfo *)
                    TMO_DLL_Next (&(pMepNode->RMepDb),
                                  &(pTempRMepNode->MepDbDllNode));

                EcfmDeleteRMepEntry (pMepNode, pTempRMepNode);
                pTempRMepNode = NULL;
            }

            ECFM_MEMSET (&(pMepNode->RMepDb), ECFM_INIT_VAL,
                         sizeof (pMepNode->RMepDb));
            /* From its associated MA node's Dll */
            TMO_DLL_Delete (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));
            pMaNode = NULL;

            /* Deallocate the memory allocated for MPLS-TP Params block */
            if (pMepNode->pEcfmMplsParams != NULL)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_MPTP_PARAMS_POOL,
                                     (UINT1 *) (pMepNode->pEcfmMplsParams));
            }

            /* Releasing mem block allocated to MEP node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL, (UINT1 *) (pMepNode));
            break;
        case ECFM_CC_ERR_LOG_ENTRY:
            pCcErrorNode = (tEcfmCcErrLogInfo *) pElem;

            /* Release memory allocated to Error Log node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL,
                                 (UINT1 *) (pCcErrorNode));
            break;
        case ECFM_CC_FL_BUFFER_ENTRY:
            pEcfmCcLmNode = (tEcfmCcLmInfo *) pElem;

            /* Release memory allocated to Frame Loss Buffer Table */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_FRM_LOSS_POOL,
                                 (UINT1 *) (pEcfmCcLmNode));
            break;
            /* Global RB-Tree destroy will only be called after deletion of all the
             * context and their correspong RB-Trees, therefore there will be on
             * entries in global Tables.*/
        case ECFM_CC_GLOBAL_PORT_ENTRY:
            break;
        case ECFM_CC_GLOBAL_STACK_ENTRY:
            break;
        case ECFM_CC_GLOBAL_CONFIG_ERR_ENTRY:
            break;
        case ECFM_CC_GLOBAL_MIP_ENTRY:
            break;
        case ECFM_CC_GLOBAL_OFF_RMEP_ENTRY:
            break;
        case ECFM_CC_DEF_MD_ENTRY:
            pCcDefMdNode = (tEcfmCcDefaultMdTableInfo *) pElem;
            /* Release memory allocated to Error Log node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_DEF_MD_TABLE_POOL,
                                 (UINT1 *) (pCcDefMdNode));
            break;

        case ECFM_CC_STACK_ENTRY_IN_PORT:
            pStackNode = (tEcfmCcStackInfo *) pElem;
            RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) pStackNode);
            ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                                 (UINT1 *) pStackNode);
            break;
        case ECFM_CC_PORT_MEP_ENTRY:
            break;
        case ECFM_CC_GLOBAL_OFF_MEP_ENTRY:
            break;
        case ECFM_CC_GLOBAL_HW_MEP_ENTRY:
            break;
        case ECFM_CC_GLOBAL_HW_RMEP_ENTRY:
            break;
        default:
            break;
    }
    return 1;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilNotifySm 
 *                                                                          
 *    DESCRIPTION      : This function notifies the various state machines 
 *                       for events like Module enable/disable, interface
 *                       Up/Down and  MEP active/inactive.
 *
 *    INPUT            : pMepInfo - Pointer to the MEPInfo to notify
 *                       Indication - Event to notify
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilNotifySm (tEcfmCcMepInfo * pMepInfo, UINT1 u1Indication)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = pMepInfo;
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepInfo);
    switch (u1Indication)

    {
        case ECFM_IND_MODULE_ENABLE:
        case ECFM_IND_MEP_ACTIVE:

            /* Send event to State machines if Port's Module status is ENABLE
             * and MEP is ACTIVE */
            if ((pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
                (pMepInfo->b1Active == ECFM_TRUE))

	    {
		    EcfmCcClntCciSm (&PduSmInfo, ECFM_EV_MEP_BEGIN);
		    EcfmCcClntXconSm (&PduSmInfo, ECFM_EV_MEP_BEGIN);
		    EcfmCcClntFngSm (&PduSmInfo, ECFM_EV_MEP_BEGIN);
		    EcfmCcClntRmepErrSm (&PduSmInfo, ECFM_EV_MEP_BEGIN);
		    EcfmCcClntNotifyRmep (&PduSmInfo, ECFM_EV_MEP_BEGIN);

		    /* Check if CCM messages are to be initiated */
		    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
		    if (pCcInfo->b1CciEnabled == ECFM_TRUE)
		    {
			    {
#ifdef L2RED_WANTED
				    UINT4               u4CcmInterval = ECFM_INIT_VAL;
				    ECFM_GET_SYS_TIME (&pMepInfo->CcInfo.CciTimeStamp);
				    ECFM_GET_CCM_INTERVAL (pMepInfo->pMaInfo->u1CcmInterval,
						    u4CcmInterval);
				    u4CcmInterval =
					    ECFM_CONVERT_MSEC_TO_TIME_TICKS (u4CcmInterval);
				    pMepInfo->CcInfo.CciTimeStamp += u4CcmInterval;
#endif
				    /* Send the event to CC Task to Start CCM Transmission */
				    EcfmCcUtilPostTransaction (pMepInfo,
						    ECFM_CC_START_TRANSACTION);
			    }
		    }
                /* Create Transmission/Reception call for the MEP */
                /* Check if MEP's CC Transmission status is enabled or not
                 * and correspondigly enable transmission/reception from the
                 * Offloaded MEP 
                 * */
                if ((pCcInfo->b1CciEnabled == ECFM_TRUE) &&
                    (pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE))
		{
#ifdef MBSM_WANTED
			/* If No port is present in the system for this MEP then
			 * don't call offloading NPAPI */
			if (EcfmMbsmIsPortPresent (pMepInfo->pPortInfo->u4IfIndex,
						pMepInfo->u4PrimaryVidIsid,
						pMepInfo->u1Direction) ==
					ECFM_FALSE)
			{
				break;
			}
#endif
			if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
			{
				return;
			}

		}
	    }
            break;
        case ECFM_IND_IF_DELETE:
        case ECFM_IND_MODULE_DISABLE:
        case ECFM_IND_MEP_INACTIVE:
            if ((pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
                (pMepInfo->b1MepCcmOffloadStatus != ECFM_TRUE))

            {
#ifdef L2RED_WANTED
                pMepInfo->CcInfo.CciTimeStamp = ECFM_INIT_VAL;
#endif
                EcfmCcClntCciSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmCcClntXconSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmCcClntFngSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmCcClntRmepErrSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmCcClntNotifyRmep (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
            }

            else if ((pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE) &&
                     (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE))

	    {

		    /* Check transmission status for this Offloaded MEP */
		    /* Delete MEP information maintained for this MEP */

		    EcfmCcmOffDeleteTxRxForMep (pMepInfo);
		    EcfmCcClntCciSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
		    EcfmCcClntXconSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
		    EcfmCcClntFngSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
		    EcfmCcClntRmepErrSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
		    EcfmCcClntNotifyRmep (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
	    }
            break;
        default:
            break;
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilGetRmepInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the RMep entry
 *                       from MEP Node's, RMepDb Table.
 *
 *    INPUT            : pMepNode - Pointer to MepInfo
 *                       u2RMepId  - Remote Mep Identifier
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to RMep Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcRMepDbInfo *
EcfmUtilGetRmepInfo (tEcfmCcMepInfo * pMepNode, UINT2 u2RMepId)
{
    tEcfmCcRMepDbInfo   RMepInfo;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);

    /* Get Remote MEP entry corresponding to indices, RMepId */
    RMepInfo.u2RMepId = u2RMepId;
    pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
    while (pRMepNode != NULL)
    {
        if (pRMepNode->u2RMepId == u2RMepId)
        {
            return pRMepNode;
        }
        pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                        &(pRMepNode->
                                                          MepDbDllNode));
    }
    return pRMepNode;
}

/*****************************************************************************
 * Name               : EcfmCcUtilGetMp
 *
 * Description        : This is function is used to get entry from Stack table 
 *                     in a global info for particular Md Level, VId and
 *                     direction, IfIndex.
 *
 * Input(s)           : u2PortNum - Local Port Number of the port of MP
 *                     u1MdLevel - MdLevel of MP
 *                     u2VlanId - VlanId of MP
 *                     u1Direction - direction of the MP
 *
 * Output(s)          : None
 *
 * Return Value(s)    : pstackNode - Pointer to stack node
 *****************************************************************************/
PUBLIC tEcfmCcStackInfo *
EcfmCcUtilGetMp (UINT2 u2PortNum, UINT1 u1MdLevel,
                 UINT4 u4VlanIdIsid, UINT1 u1Direction)
{
    tEcfmCcStackInfo    CcStackInfoNode;
    tEcfmCcStackInfo   *pStackInfo = NULL;    /* Pointer to Stack info */
    tEcfmCcPortInfo    *pPortInfo = NULL;
    ECFM_MEMSET (&CcStackInfoNode, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);
    CcStackInfoNode.u4VlanIdIsid = u4VlanIdIsid;
    CcStackInfoNode.u1MdLevel = u1MdLevel;
    CcStackInfoNode.u1Direction = u1Direction;
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return NULL;
    }
    /* Get entry from Stack table on a port for particular
     * Md Level, VId and Direction  */
    pStackInfo = (tEcfmCcStackInfo *) RBTreeGet (pPortInfo->StackInfoTree,
                                                 (tRBElem *) & CcStackInfoNode);
    return pStackInfo;
}

/******************************************************************************
 * Function Name      : EcfmCcADUtilChkPortFiltering
 *
 * Description        : This routine applies the port-filtering rules to the Vid 
 *                      and IfIndex Provided
 *
 * Input(s)           : u2LocalPort - Interface Index
 *                    : u4Vid -  VLAN-ID
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcADUtilChkPortFiltering (UINT2 u2LocalPort, UINT4 u4VidIsid)
{
    UINT4               u4IfIndex = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    if (ECFM_CC_GET_PORT_INFO (u2LocalPort) == NULL)
    {
        return ECFM_FAILURE;
    }

    u4IfIndex = ECFM_CC_PORT_INFO (u2LocalPort)->u4IfIndex;

    /*Check the port memebership of Vlan */
    if (EcfmL2IwfMiIsVlanMemberPort
        (ECFM_CC_CURR_CONTEXT_ID (), u4VidIsid, u4IfIndex) != OSIX_TRUE)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADUtilChkPortFiltering:"
                     "EcfmL2IwfMiIsVlanMemberPort returned failure\r\n");
        return ECFM_FAILURE;
    }

    /*check for the port state */
    if (EcfmL2IwfGetVlanPortState (u4VidIsid, u4IfIndex) !=
        AST_PORT_STATE_FORWARDING)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcADUtilChkPortFiltering:"
                     "EcfmL2IwfGetVlanPortState returned port state !forwarding\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcUtilGetMipCcmDbEntry
 *
 *    DESCRIPTION      : This function gets the IfIndex for the received Source 
 *                       MAC Address and the Vlan Id from the MIP CCM Database.
 *
 *    INPUT            : u2VLanId   - VlanId of the received CCM PDU
 *                       SrcMacAddr - Source MAC Address in the CCM PDU
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to tEcfmCcMipDbInfo
 *
 ****************************************************************************/
PUBLIC tEcfmCcMipCcmDbInfo *
EcfmCcUtilGetMipCcmDbEntry (UINT2 u2VlanId, UINT1 *pu1SrcMacAddr)
{
    tEcfmCcMipCcmDbInfo MipCcmDbInfo;
    tEcfmCcMipCcmDbInfo *pMipCcmDbNode = NULL;
    ECFM_MEMSET (&MipCcmDbInfo, ECFM_INIT_VAL, ECFM_CC_MIP_CCM_DB_INFO_SIZE);
    MipCcmDbInfo.u2Fid = u2VlanId;
    ECFM_MEMCPY (&(MipCcmDbInfo.SrcMacAddr), pu1SrcMacAddr,
                 ECFM_MAC_ADDR_LENGTH);

    /* Get MipCcmDb Entry */
    pMipCcmDbNode =
        (tEcfmCcMipCcmDbInfo *) RBTreeGet (ECFM_CC_MIP_CCM_DB_TABLE,
                                           &MipCcmDbInfo);
    return pMipCcmDbNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilGetMipEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns a pointer to the MIP entry
 *                       from the global structure, MipTable.
 *
 *    INPUT            : u2PortNum - Local Port Number 
 *                       u1MdLevel - Mdlevel
 *                       u2VlanId - VlanId 
 *                       Indices of MIP Table for which an entry is
 *                       required.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Mip Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcMipInfo *
EcfmCcUtilGetMipEntry (UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VlanIdIsid)
{
    tEcfmCcMipInfo      MipInfo;
    tEcfmCcMipInfo     *pMipNode = NULL;
    ECFM_MEMSET (&MipInfo, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);

    /* Get MIP entry, based on conetextId, ifIndex, MdLevel,u2VlanId */
    MipInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    MipInfo.u2PortNum = u2PortNum;
    MipInfo.u1MdLevel = u1MdLevel;
    MipInfo.u4VlanIdIsid = u4VlanIdIsid;
    pMipNode = RBTreeGet (ECFM_CC_GLOBAL_MIP_TABLE, (tRBElem *) & MipInfo);
    return pMipNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilGetMipPreventEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns a pointer to the MIP prevent 
 *                       entry from the global structure, MipTable.
 *
 *    INPUT            : u2PortNum - Local Port Number 
 *                       u1MdLevel - Mdlevel
 *                       u2VlanId - VlanId 
 *                       Indices of MIP prevent Table for which an entry is
 *                       required.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Mip prevent Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcMipPreventInfo *
EcfmCcUtilGetMipPreventEntry (UINT2 u2PortNum, UINT1 u1MdLevel,
                              UINT4 u4VlanIdIsid)
{
    tEcfmCcMipPreventInfo MipInfo;
    tEcfmCcMipPreventInfo *pMipNode = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_MEMSET (&MipInfo, ECFM_INIT_VAL, ECFM_CC_MIP_PREVENT_INFO_SIZE);

    /* Get MIP entry, based on ifIndex, MdLevel,u2VlanId */
    MipInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (u2PortNum, pTempPortInfo);
    MipInfo.u1MdLevel = u1MdLevel;
    MipInfo.u4VlanIdIsid = u4VlanIdIsid;
    pMipNode = RBTreeGet (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                          (tRBElem *) & MipInfo);
    return pMipNode;
}

/****************************************************************************/
/*dynamic mip creation routines*/
/****************************************************************************/

/******************************************************************************
 *    FUNCTION NAME    : EcfmAddMipInStack
 *  
 *    DESCRIPTION      : This function adds MIP entry
 *                       corresponding to indices in StackTable. 
 *                            
 *   
 *    INPUT            : u2PortNum - Local Port number
 *                       u1MdLevel - MdLevel 
 *                       u2VlanId - VlanId with which MIP is associated
 *                       u4MdIndex - MD index of MIP if, its creation is based
 *                       on any MA
 *                       u4MaIndex - MA index of MIP if, its creation is based
 *                       on any MA
 *   
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *   
 *******************************************************************************/
PRIVATE INT4
EcfmAddMipInStack (tEcfmCcMipInfo * pMipInfo,
                   UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VlanIdIsid)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcStackInfo   *pStackUpNode = NULL;
    tEcfmCcStackInfo   *pStackDownNode = NULL;

    /* Create Stack nodes corresponding to MIP */
    /* Allocate memory for Stack node corresponding for up MHF */
    if (ECFM_ALLOC_MEM_BLOCK_CC_STACK_TABLE (pStackUpNode) == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmAddMipInStack:"
                     "MEM Block Allocation for Up MHF Stack node Failed"
                     "\r\n");
        return ECFM_FAILURE;
    }

    /* Allocate memory for Stack node corresponding for down MHF */
    if (ECFM_ALLOC_MEM_BLOCK_CC_STACK_TABLE (pStackDownNode) == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmAddMipInStack:"
                     "MEM Block Allocation for down MHFStack node Failed"
                     "\r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pStackUpNode, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);
    ECFM_MEMSET (pStackDownNode, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmAddMipInStack: No port information available \r\n");
        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }
    /* Put required parameters for up MHF */
    pStackUpNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    pStackUpNode->u2PortNum = u2PortNum;
    pStackUpNode->pMepInfo = NULL;
    pStackUpNode->pMipInfo = pMipInfo;
    pStackUpNode->u4VlanIdIsid = u4VlanIdIsid;
    pStackUpNode->u1MdLevel = u1MdLevel;
    pStackUpNode->u1Direction = ECFM_MP_DIR_UP;
    pStackUpNode->u4IfIndex = ECFM_CC_PORT_INFO (u2PortNum)->u4IfIndex;

    /* Put required parameters for down MHF */
    pStackDownNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    pStackDownNode->u2PortNum = u2PortNum;
    pStackDownNode->pMepInfo = NULL;
    pStackDownNode->pMipInfo = pMipInfo;
    pStackDownNode->u4VlanIdIsid = u4VlanIdIsid;
    pStackDownNode->u1MdLevel = u1MdLevel;
    pStackDownNode->u1Direction = ECFM_MP_DIR_DOWN;
    pStackDownNode->u4IfIndex = pPortInfo->u4IfIndex;
    /* Add in global RB-Tree also */
    if (RBTreeAdd (ECFM_CC_GLOBAL_STACK_TABLE, pStackUpNode) == ECFM_RB_FAILURE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmAddMipInStack: up MHF Addition in Global Stack Table"
                     " Failed \r\n");
        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }
    if (RBTreeAdd (pPortInfo->StackInfoTree, pStackUpNode) == ECFM_RB_FAILURE)
    {

        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmAddMipInStack: up MHF Addition in Global Stack Table"
                     " Failed \r\n");
        /* Remove up MHF stack node also */
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, pStackUpNode);
        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }
    /* Add in global RB-Tree also */
    if (RBTreeAdd (ECFM_CC_GLOBAL_STACK_TABLE, pStackDownNode) ==
        ECFM_RB_FAILURE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmAddMipInStack: down MHF Addition in global Stack Table"
                     " Failed \r\n");
        /* Remove up MHF stack node also */
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, pStackUpNode);
        RBTreeRem (pPortInfo->StackInfoTree, pStackUpNode);

        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }

    if (RBTreeAdd (pPortInfo->StackInfoTree, pStackDownNode) == ECFM_RB_FAILURE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmAddMipInStack: down MHF Addition in global Stack Table"
                     " Failed \r\n");
        /* Remove up MHF stack node also */
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, pStackUpNode);
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, pStackDownNode);
        RBTreeRem (pPortInfo->StackInfoTree, pStackUpNode);

        /* Release the memory allocated to Stack up and down MHF's nodes */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackUpNode = NULL;
        pStackDownNode = NULL;
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmDeleteMipFromStack
 *  
 *    DESCRIPTION      : This function deletes MIP entry
 *                       corresponding to indices from StackTable. 
 *                            
 *   
 *    INPUT            : u2PortNum - Local Port number where MIP has been
 *                                   configured
 *                       u1MdLevel - MdLevel of MIP
 *                       u2VlanId - VlanId with which MIP is associated
 *   
 *    OUTPUT           : None.
 *
 *    RETURNS          : None.
 *   
 *******************************************************************************/
PRIVATE VOID
EcfmDeleteMipFromStack (UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VlanIdIsid)
{
    tEcfmCcStackInfo   *pStackUpNode = NULL;
    tEcfmCcStackInfo   *pStackDownNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return;
    }

    /* Get up Mhf node */
    pStackUpNode = EcfmCcUtilGetMp (u2PortNum, u1MdLevel, u4VlanIdIsid,
                                    ECFM_MP_DIR_UP);
    /* Check if it is MIP */
    if ((pStackUpNode != NULL) && (ECFM_CC_IS_MHF (pStackUpNode)))

    {
        RBTreeRem (pPortInfo->StackInfoTree, pStackUpNode);
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, pStackUpNode);
        /* Delete up MHF node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackUpNode));
        pStackUpNode = NULL;
    }
    else
    {
        /* up MHF node does not exists so there can not be down MHF also */
        return;
    }
    /* Get down Mhf node */
    pStackDownNode = EcfmCcUtilGetMp (u2PortNum, u1MdLevel, u4VlanIdIsid,
                                      ECFM_MP_DIR_DOWN);
    /* Check if it is MIP */
    if ((pStackDownNode != NULL) && (ECFM_CC_IS_MHF (pStackDownNode)))

    {
        RBTreeRem (pPortInfo->StackInfoTree, pStackDownNode);
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, pStackDownNode);
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackDownNode));
        pStackDownNode = NULL;
    }
    return;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilAddMipEntry
 *
 *    DESCRIPTION      : This function adds MIP entry
 *                       in MipTable in MipTable of both the tasks . 
 *
 *    INPUT            : pMipNewNode- pointer to MIP Node that needs to be 
 *                       added.
 *                       pMaNode - If MIP that is to be added is based on any
 *                       MA
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcUtilAddMipEntry (tEcfmCcMipInfo * pMipNewNode, tEcfmCcMaInfo * pMaNode)
{
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4EntryCount = 0;

    /* Number of MIP created count is fetched from RBTree structure and it
     * is compared with maximum Number of MIP that are allowed to create.
     * While creating the maximum MIP, Trace message will be displayed.
     * On exceeding the maximum limit, Error message will be thrown in
     * memory allocation failure. */
    RBTreeCount (ECFM_CC_GLOBAL_MIP_TABLE, &u4EntryCount);
    if (u4EntryCount >= ECFM_MAX_MIP_IN_SYSTEM - ECFM_VAL_1)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcUtilAddMipEntry : Reaches maximum MIP creation"
                     "!!\r\n");
    }
    /* Put Md Index and Ma Index, if MIP to be created is based on  any MA */
    if (pMaNode != NULL)

    {
        u4MdIndex = pMaNode->u4MdIndex;
        u4MaIndex = pMaNode->u4MaIndex;
    }

    /*Add Mip in stack Table */
    if (EcfmAddMipInStack
        (pMipNewNode, pMipNewNode->u2PortNum,
         pMipNewNode->u1MdLevel, pMipNewNode->u4VlanIdIsid) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcUtilAddMipEntry : MIP addition in StackTable"
                     "Failed!!\r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_CC_GET_PORT_INFO (pMipNewNode->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcUtilAddMipEntry : No Port Information !!\r\n");
        return ECFM_FAILURE;
    }
    pMipNewNode->u4MdIndex = u4MdIndex;
    pMipNewNode->u4MaIndex = u4MaIndex;
    pMipNewNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    if (RBTreeAdd (ECFM_CC_GLOBAL_MIP_TABLE, pMipNewNode) == ECFM_RB_FAILURE)
    {
        EcfmDeleteMipFromStack (pMipNewNode->u2PortNum, pMipNewNode->u1MdLevel,
                                pMipNewNode->u4VlanIdIsid);
        return ECFM_FAILURE;
    }
    /* Add MIP node in LBLT task's MipTable in global info */
    if (EcfmLbLtAddMipEntry
        (pMipNewNode, ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        RBTreeRem (ECFM_CC_GLOBAL_MIP_TABLE, pMipNewNode);
        EcfmDeleteMipFromStack (pMipNewNode->u2PortNum, pMipNewNode->u1MdLevel,
                                pMipNewNode->u4VlanIdIsid);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilAddMipPreventEntry
 *
 *    DESCRIPTION      : This function adds MIP Prevent entry
 *                       in MipPreventTable RBTree. 
 *
 *    INPUT            : pMipNewNode- pointer to MIP Prevent Node that needs
                                      to be added.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcUtilAddMipPreventEntry (tEcfmCcMipPreventInfo * pMipNewNode)
{
    if (RBTreeAdd (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE, (tRBElem *) pMipNewNode)
        == ECFM_RB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilGetHighestMpMdLevel 
 *                                                                          
 *    DESCRIPTION      : This function returns highest level of any MP
 *                       configured on specified port, with particular VlanId
 *                       and whether that entity is MEP/MIP. if no such MP found
 *                       returns ECFM_FAILURE
 *
 *    INPUT            : u2PortNum - Local Port Number, at which highest MP needs
 *                                   to find.
 *                       u4VidIsid     - VlanId / ISID
 *    
 *    OUTPUT           : pu1RetMdLevel - Highest MD level at specified port and
 *                                       Vid
 *                       pb1IsMep - Whether MP is MEP or not.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmCcUtilGetHighestMpMdLevel (UINT2 u2PortNum, UINT4 u4VidIsid,
                               UINT1 *pu1RetMdLevel, BOOL1 * pb1IsMep)
{
    tEcfmCcStackInfo    StackInfo;
    tEcfmCcStackInfo   *pMpNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    INT1                i1HighestLevel = ECFM_INVALID_VALUE;
    BOOL1               b1IsMep = ECFM_FALSE;

    ECFM_MEMSET (&StackInfo, 0, sizeof (tEcfmCcStackInfo));

    /*Get Stack entry corresponding to IfIndex, VlanId */
    StackInfo.u4VlanIdIsid = u4VidIsid;

    /* Check if any entry is present in stack table for the given combination.
     * If not do a getNext after that
     */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    pMpNode = (tEcfmCcStackInfo *) RBTreeGetNext
        (pPortInfo->StackInfoTree, (tRBElem *) & StackInfo, NULL);

    while ((pMpNode != NULL) && (pMpNode->u4VlanIdIsid == u4VidIsid))

    {
        /* Check if MP is associated with same VlanId and IfIndex */
        if ((INT1) (pMpNode->u1MdLevel) > i1HighestLevel)
        {
            i1HighestLevel = (INT1) pMpNode->u1MdLevel;

            /* Set if MP is MEP or not */
            if (pMpNode->pMepInfo == NULL)

            {
                b1IsMep = ECFM_FALSE;
            }

            else

            {
                b1IsMep = ECFM_TRUE;
            }
        }
        pMpNode = RBTreeGetNext (pPortInfo->StackInfoTree, pMpNode, NULL);
    }
    if (i1HighestLevel != ECFM_INVALID_VALUE)

    {
        *pu1RetMdLevel = (UINT1) i1HighestLevel;
        *pb1IsMep = b1IsMep;
        return ECFM_SUCCESS;
    }
    return ECFM_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcUtilDeleteMipEntry
 *
 *    DESCRIPTION      : This function deletes particular MIP entry from
 *                       Global info's MipTable and corresponding stack 
 *                       entry of both the tasks. 
 *
 *    INPUT            : pMipNode - pointer to MIP entry that needs to be
 *                       deleted.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilDeleteMipEntry (tEcfmCcMipInfo * pMipNode)
{

    /* Remove node from MipTable */
    RBTreeRem (ECFM_CC_GLOBAL_MIP_TABLE, pMipNode);

    /* Delete Mip from Stack Table */
    EcfmDeleteMipFromStack (pMipNode->u2PortNum, pMipNode->u1MdLevel,
                            pMipNode->u4VlanIdIsid);

    /* Delete Mip from LBLT task also */
    EcfmLbLtDeleteMipEntry (pMipNode->u2PortNum, pMipNode->u1MdLevel,
                            pMipNode->u4VlanIdIsid, ECFM_CC_CURR_CONTEXT_ID ());
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilDelMipPreventEntry
 *
 *    DESCRIPTION      : This function deletes particular MIP prevent entry 
 *                       from Global info's Mip Prevent Table. 
 *
 *    INPUT            : pMipNode - pointer to MIP prevent entry that needs to
 *                                  be deleted.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilDelMipPreventEntry (tEcfmCcMipPreventInfo * pMipNode)
{

    /* Remove node from Mip prevention Table */
    RBTreeRem (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE, pMipNode);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilGetMaAssocWithVid
 *                                                                          
 *    DESCRIPTION      : This function returns MA associated with particular
 *                       vid at specified Md Level. 
 *                       
 *
 *    INPUT            : u1MdLevel - Md Level at which required MA needs to
 *                                   return. 
 *                       u4VidIsid -  
 *    
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : Pointer to MA node.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcMaInfo *
EcfmCcUtilGetMaAssocWithVid (UINT4 u4VidIsid, UINT1 u1MdLevel)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT2               u4PrimaryVid = ECFM_INIT_VAL;

    /* Check if there is any MA associated with more than one VlanId and vid
     * with which MIP is to be associated is not primary vlanid of MA */
    /* Get primaryVid of that VlanId with which MIP is to be associated */
    pVlanNode = RBTreeGetFirst (ECFM_CC_VLAN_TABLE);
    while (pVlanNode != NULL)

    {
        if (pVlanNode->u4VidIsid == u4VidIsid)
        {
            u4PrimaryVid = pVlanNode->u4PrimaryVidIsid;
            break;
        }
        pVlanNode = RBTreeGetNext (ECFM_CC_VLAN_TABLE, pVlanNode, NULL);
    }

    /* Get MA associated with u4VidIsid at level u1MdLevel */
    pMdNode = RBTreeGetFirst (ECFM_CC_MD_TABLE);
    while (pMdNode != NULL)

    {

        /* Get Md node for u1MdLevel */
        if ((pMdNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE) &&
            (pMdNode->u1Level == u1MdLevel))

        {
            pMaNode = RBTreeGetFirst (pMdNode->MaTable);
            while (pMaNode != NULL)

            {
                if (pMaNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)

                {
                    pMaNode = RBTreeGetNext (pMdNode->MaTable, pMaNode, NULL);
                    continue;
                }

                /* Check Ma node for the u4VidIsid */
                if (((u4PrimaryVid != ECFM_INIT_VAL) &&
                     ((pMaNode->u4PrimaryVidIsid == u4PrimaryVid) ||
                      (pMaNode->u4PrimaryVidIsid == u4VidIsid))) ||
                    ((u4PrimaryVid == ECFM_INIT_VAL) &&
                     (pMaNode->u4PrimaryVidIsid == u4VidIsid)))
                {
                    return pMaNode;
                }
                pMaNode = RBTreeGetNext (pMdNode->MaTable, pMaNode, NULL);
            }
        }
        pMdNode = RBTreeGetNext (ECFM_CC_MD_TABLE, pMdNode, NULL);
    }
    return pMaNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmIsMaValidForActiveMdLevel 
 *                                                                          
 *    DESCRIPTION      : This function checks whether MA should be included 
 *                       in Active MD level list maintained for dynamic MIP
 *                       evaluation. 
 *                       
 *    INPUT            : u4MdIndex - Md Index of MA
 *                       u4MaIndex - Ma Index of MA            
 *                       u2PortNum - Port number for which Active Md Levels 
 *                       needs to find.
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE             BOOL1
EcfmIsMaValidForActiveMdLevel (UINT4 u4MdIndex, UINT4 u4MaIndex,
                               UINT2 u2PortNum)
{
    /* 1. The MD Level of each of the MAs (if any) that includes VID x, and a MEP
     *    configured on Bridge Port p;
     * 2. The MD Level of each of the MAs (if any) that inlcudes VID x, and has
     *    at least one Up MEP configured on some Bridge Port in this Bridge 
     *    other than Port p
     * 3. The MD Level of each of the MAs (if any) that includes VID x, and has
     *    no MEPs configured on any Bridge Port in this Bridge;
     */
    if (EcfmIsMepAssocWithMa (u4MdIndex, u4MaIndex, u2PortNum, -1) == ECFM_TRUE)

    {
        return ECFM_TRUE;
    }
    else if ((EcfmIsMepAssocWithMa (u4MdIndex, u4MaIndex, -1, ECFM_MP_DIR_UP)
              == ECFM_TRUE) &&
             (EcfmIsMepAssocWithMa
              (u4MdIndex, u4MaIndex, u2PortNum, ECFM_MP_DIR_UP) == ECFM_FALSE))
    {
        return ECFM_TRUE;
    }
    else if (EcfmIsMepAssocWithMa (u4MdIndex, u4MaIndex, -1, -1) == ECFM_FALSE)
    {
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmGetActiveMdLevels
 *                                                                          
 *    DESCRIPTION      : This function sets active MD Levels and  MA associated
 *                       with that Md levels in pu1NoOfMdLevels and
 *                       ppActiveMaNodes array respectively. 
 *                       
 *    INPUT            : u4VlanIdIsid - VlanId/Isid for which Active Md Levels
 *                                        needs to find
 *                       u2PortNum - Port number for which Active Md Levels needs to 
 *                                   find
 *    OUTPUT           :  pu1NoOfMdLevels - List of Active MD levels.
 *                        ppActiveMaNodes - List of corresponding MA nodes
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EcfmGetActiveMdLevels (UINT4 u4VlanIdIsid, UINT1 *pu1ActiveMdLevels,
                       UINT1 *pu1NoOfMdLevels, tEcfmCcMaInfo ** ppActiveMaNodes,
                       UINT2 u2PortNum)
{
    tEcfmCcVlanInfo     VlanNode;
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    UINT4               u4PrimaryVidIsid = ECFM_INIT_VAL;
    UINT1               u1LevelCounter = ECFM_INIT_VAL;
    UINT1               u1Counter = ECFM_INIT_VAL;

    /* The MD Level of the entry in the Default MD Level managed object 
     * (12.14.3) for VID x.*/
    ECFM_CC_GET_DEFAULT_MD_ENTRY (u4VlanIdIsid, pDefaultMdNode);
    if (pDefaultMdNode != NULL && pDefaultMdNode->b1Status == ECFM_TRUE)
    {
        if (pDefaultMdNode->i1MdLevel != -1)
        {
            pu1ActiveMdLevels[0] = (UINT1) pDefaultMdNode->i1MdLevel;
            ppActiveMaNodes[0] = NULL;

            /* Set the no of Active MdLevels found */
            *pu1NoOfMdLevels = *pu1NoOfMdLevels + ECFM_INCR_VAL;

            /* Update Array index for the next entry in pu1ActiveMdLevels */
            u1LevelCounter = u1LevelCounter + ECFM_INCR_VAL;
        }
        else
        {
            if (ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION != ECFM_MHF_CRITERIA_NONE)
            {
                pu1ActiveMdLevels[0] = ECFM_CC_DEF_MD_DEFAULT_LEVEL;
                ppActiveMaNodes[0] = NULL;

                /* Set the no of Active MdLevels found */
                *pu1NoOfMdLevels = *pu1NoOfMdLevels + ECFM_INCR_VAL;

                /* Update Array index for the next entry in pu1ActiveMdLevels */
                u1LevelCounter = u1LevelCounter + ECFM_INCR_VAL;
            }
        }
    }
    /* Check if there is any MA associated with more than one VlanIds */
    /* Get primaryVid of that VlanId for which active mdlevels is to be
     * extracted */
    VlanNode.u4VidIsid = u4VlanIdIsid;
    VlanNode.u4PrimaryVidIsid = 0;
    pVlanNode = RBTreeGet (ECFM_CC_VLAN_TABLE, &VlanNode);
    if (pVlanNode != NULL)
    {
        u4PrimaryVidIsid = pVlanNode->u4PrimaryVidIsid;
    }
    /* Get MdLevels of MAs associated with u2VlanId */
    pMaNode = RBTreeGetFirst (ECFM_CC_MA_TABLE);
    while (pMaNode != NULL)
    {
        /* Check if it is a valid MA node */
        if (pMaNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
        {
            pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
            continue;
        }
        /* Check MA node for the u2VlanId */
        if (((u4PrimaryVidIsid != ECFM_INIT_VAL) &&
             (pMaNode->u4PrimaryVidIsid == u4PrimaryVidIsid)) ||
            ((u4PrimaryVidIsid == ECFM_INIT_VAL) &&
             (pMaNode->u4PrimaryVidIsid == u4VlanIdIsid)))
        {

            if (EcfmIsMaValidForActiveMdLevel (pMaNode->u4MdIndex,
                                               pMaNode->u4MaIndex,
                                               u2PortNum) != ECFM_TRUE)

            {
                pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
                continue;
            }

            /* Add active MD level and its corresponding MA nodes 
             * in ascending order */
            pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);
            u1Counter = u1LevelCounter;
            while ((u1Counter > 0) &&
                   (pu1ActiveMdLevels[u1Counter - 1] > pMdNode->u1Level))

            {
                pu1ActiveMdLevels[u1Counter] = pu1ActiveMdLevels[u1Counter - 1];
                ppActiveMaNodes[u1Counter] = ppActiveMaNodes[u1Counter - 1];
                u1Counter = u1Counter - ECFM_DECR_VAL;
            }
            pu1ActiveMdLevels[u1Counter] = pMdNode->u1Level;
            ppActiveMaNodes[u1Counter] = pMaNode;
            u1LevelCounter = u1LevelCounter + ECFM_INCR_VAL;

            /* Set the no of Active MdLevels found */
            *pu1NoOfMdLevels = *pu1NoOfMdLevels + ECFM_INCR_VAL;
        }
        pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
    }
    *pu1NoOfMdLevels = u1LevelCounter;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmGetLowestActiveMdLevel
 *                                                                          
 *    DESCRIPTION      : This function gets lowest active MD Level Index
 *                       from the active md level array (pu1NoOfMdLevels)
 *                       such that No MEP exist at or higher active MdLevel. 
 *                       
 *    INPUT            : u2PortNum - Local Port Number at which active MD level
 *                                   needs to find.
 *                       u4VlanIdIsid - VanId/Isid for which active MD level
 *                       needs to find
 *                       u1NoOfMdLevels - Number of Active Md levels            
 *
 *    OUTPUT           : pu1ActiveMdLevelIndex - Active MD level index in
 *                                               pu1NoOfMdLevels array.
 *                       pi1NextLowerLevel - Next lower active MD level                       
 *                       *pi1PrevMipLevel - Level of MIP if it is configured
 *                                          at level lower than the eligible
 *                                          Md level(Lowest active Md level)
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE             BOOL1
EcfmGetLowestActiveMdLevel (UINT2 u2PortNum, UINT4 u4VlanIdIsid,
                            UINT1 *pau1ActiveMdLevels,
                            UINT1 u1NoOfMdLevels,
                            UINT1 *pu1ActiveMdLevelIndex,
                            INT1 *pi1NextLowerLevel, INT1 *pi1PrevMipLevel)
{
    UINT1               u1HighestMdLevel = ECFM_INIT_VAL;
    UINT1               u1LevelCounter = ECFM_INIT_VAL;
    BOOL1               b1IsMep = ECFM_INIT_VAL;

    /* Get Highest Md level associated with u4VlanIdIsid configured 
     * on u4IfIndex */
    if (EcfmCcUtilGetHighestMpMdLevel (u2PortNum, u4VlanIdIsid,
                                       &u1HighestMdLevel,
                                       &b1IsMep) != ECFM_SUCCESS)
    {

        /* No level associated with u4VlanIdIsid configured on u4IfIndex */
        /* return the lowest MdLevel from pau1ActiveMdLevels */
        *pu1ActiveMdLevelIndex = 0;
        *pi1NextLowerLevel = (INT1) -1;
        return ECFM_TRUE;
    }

    /* MP associated with u4VlanIdIsid is configured on u4IfIndex */
    /* Get if any active Md Level is higher than the highest level 
     * MEP configured */
    for (u1LevelCounter = ECFM_INIT_VAL; u1LevelCounter < u1NoOfMdLevels;
         u1LevelCounter++)

    {

        /* Check if such active Md Level found */
        if (pau1ActiveMdLevels[u1LevelCounter] > u1HighestMdLevel)

        {

            /* Entity associated with u4VlanIdIsid configured on highest 
             * level should be MEP */
            if (!b1IsMep)

            {
                *pi1PrevMipLevel = (INT1) u1HighestMdLevel;
            }

            /* If on highest level entity configured is MEP */
            *pi1PrevMipLevel = (INT1) -1;

            /* Lowest active MD level from the list is one which is greater than
             * the highest */
            *pu1ActiveMdLevelIndex = u1LevelCounter;

            /* Set the next lower active MD level */
            if (u1LevelCounter == ECFM_INIT_VAL)

            {
                *pi1NextLowerLevel = (INT1) -1;
            }

            else

            {
                *pi1NextLowerLevel = pau1ActiveMdLevels[u1LevelCounter - 1];
            }
            return ECFM_TRUE;
        }
    }

    /* No such active md level exists */
    return ECFM_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmMepExistsAtPort
 *                                                                          
 *    DESCRIPTION      : This function checks if any MEP associated with
 *                       u2VlanId at level u2VlanId exists at port u4IfIndex
 *                       otherwise returns ECFM_FALSE.
 *                       
 *    INPUT            : u2PortNum - Local Port Number at which MEP needs to find
 *                       u1Level -   MD level with which MEP needs to find
 *                       u2VlanId -  VlanId for which MEP needs to find should
 *                                   be associated
 *
 *    OUTPUT           : None                        
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE             BOOL1
EcfmMepExistsAtPort (UINT2 u2PortNum, UINT1 u1Level, UINT4 u4VlanIdIsid)
{
    tEcfmCcMepInfo     *pMepUpNode = NULL;
    tEcfmCcMepInfo     *pMepDownNode = NULL;
    tEcfmCcPortInfo    *pPortNode = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    pPortNode = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortNode == NULL)
    {
        return ECFM_FALSE;
    }

    /* Get Up MEP node if exists */
    gpEcfmCcMepNode->u1MdLevel = u1Level;
    gpEcfmCcMepNode->u4PrimaryVidIsid = u4VlanIdIsid;
    gpEcfmCcMepNode->u1Direction = ECFM_MP_DIR_UP;
    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    pMepUpNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode);

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    /* Get down MEP node if exists */
    gpEcfmCcMepNode->u1MdLevel = u1Level;
    gpEcfmCcMepNode->u4PrimaryVidIsid = u4VlanIdIsid;
    gpEcfmCcMepNode->u1Direction = ECFM_MP_DIR_DOWN;
    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    pMepDownNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode);

    /* Check if any MEP associated with u4VlanIdIsid,
     * u1Level exists at u4IfIndex */
    if ((pMepUpNode != NULL) || (pMepDownNode != NULL))

    {
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCheckMhfCreationCriteria
 *                                                                          
 *    DESCRIPTION      : This function checks if MIP can be created at
 *                       particular IfIndex, VlanId and Mdlevel based on Mhf 
 *                       creation criteria
 *                       otherwise returns ECFM_FALSE.
 *                       
 *    INPUT            : u2PortNum - Local Port Number at which MIP needs to be
 *                                   configured
 *                       u2VlanId -  VlanId for which MIP needs to be associated
 *                                   be associated
 *                       u1Level -   MD level with which MIP needs to be
 *                                   configured
 *                       i1NextLower - Next lower active MD level             
 *
 *    OUTPUT           : None                        
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE             BOOL1
EcfmCheckMhfCreationCriteria (tEcfmCcMaInfo * pMaNode, UINT2 u2PortNum,
                              UINT4 u4VlanIdIsid, UINT1 u1MdLevel,
                              INT1 i1NextLower)
{
    UINT1               u1MhfCreation = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;

    /* check if Mhf Creation criteria is to be checked from MA */
    if (pMaNode != NULL)
    {
        /* Set Mhf creation criteria from MA or MD */
        u1MhfCreation = pMaNode->u1MhfCreation;
        if (u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
        {
            pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);
            u1MhfCreation = pMdNode->u1MhfCreation;
        }
    }
    else
    {
        /* Get the DefaultMd entry corresponding to VlanId */
        ECFM_CC_GET_DEFAULT_MD_ENTRY (u4VlanIdIsid, pDefaultMdNode);
        /* Check if it is a valid entry for MIP creation */
        if (pDefaultMdNode != NULL && pDefaultMdNode->b1Status != ECFM_FALSE)
        {
            if (pDefaultMdNode->i1MdLevel != -1)
            {
                /* Check if MIP can be created with Default Md level corresponding to
                 * Primary vlanId */
                if ((UINT1) pDefaultMdNode->i1MdLevel != u1MdLevel)
                {
                    return ECFM_FALSE;
                }
                /* Set Mhf creation criteria from default Md Table entry or 
                 * from scalar Mhf creation criteria */
                u1MhfCreation = pDefaultMdNode->u1MhfCreation;
                if (pDefaultMdNode->u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
                {
                    u1MhfCreation = ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION;
                }
            }
            else                /* Default MdLevel should be checked from Scalar default MD level */
            {
                /* Check if MIP can be created with scalar Default Md level */
                if (ECFM_CC_DEF_MD_DEFAULT_LEVEL != u1MdLevel)
                {
                    return ECFM_FALSE;
                }
                /* MIP can be created with scalar Default Md level, set Mhf
                 * creation criteria from default Md entry or scalar default Mhf
                 * creation criteria */
                u1MhfCreation = pDefaultMdNode->u1MhfCreation;
                if (u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
                {
                    u1MhfCreation = ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION;
                }
            }
        }
        else
        {
            return ECFM_FALSE;
        }
    }
    /* Check for Mhf creation criteria */
    switch (u1MhfCreation)
    {
        case ECFM_MHF_CRITERIA_NONE:
            return ECFM_FALSE;
        case ECFM_MHF_CRITERIA_DEFAULT:

            /* Check if Lower active MD level exists, then MEP should exists on
             * that lower active MD level*/
            if (i1NextLower == -1)
            {
                return ECFM_TRUE;
            }
            return (EcfmMepExistsAtPort (u2PortNum, i1NextLower, u4VlanIdIsid));
        case ECFM_MHF_CRITERIA_EXPLICIT:
            /* Lower active MD level should exist for explicit MIP creation */
            if (i1NextLower == -1)
            {
                return ECFM_FALSE;
            }
            /* Lower active MD level exists, then MEP should exists on
             * that lower active MD level*/
            return (EcfmMepExistsAtPort (u2PortNum, i1NextLower, u4VlanIdIsid));
        default:
            return ECFM_FALSE;
    }
}

/****************************************************************************
 * Function Name         : EcfmDeleteImplicitlyCreatedMips 
 *                                        
 * Description           : This function deletes all implicitly created MIPs
 *                         based on the particular PortNum and VlanId.
 *                         
 *                                        
 * Input (s)             : i2PortNum - PortNumber, -1 indicates MIPs are to be
 *                         deleted on any port. 
 *                         i2VlanId - VlanId, -1 indicates MIPs are to be
 *                         deleted for any VlanId.              
 * Output (s)            : None        
 *                                     
 * Returns               : None`
 ****************************************************************************/
PUBLIC VOID
EcfmDeleteImplicitlyCreatedMips (INT2 i2PortNum, INT4 i4VlanIdIsid)
{
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipInfo      MipNode;
    tEcfmCcMipInfo     *pMipNextNode = NULL;
    UINT1               u1LevelCounter = ECFM_INIT_VAL;

    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);

    if ((i2PortNum != -1) && (i4VlanIdIsid != -1))
    {
        for (u1LevelCounter = ECFM_MD_LEVEL_MIN;
             u1LevelCounter <= ECFM_MD_LEVEL_MAX; u1LevelCounter++)
        {
            pMipNode = NULL;
            pMipNode = EcfmCcUtilGetMipEntry ((UINT2) i2PortNum,
                                              u1LevelCounter,
                                              (UINT2) i4VlanIdIsid);
            if ((pMipNode != NULL)
                && (pMipNode->b1ImplicitlyCreated == ECFM_TRUE))
            {
                /* Delete MIP node */
                EcfmCcUtilDeleteMipEntry (pMipNode);

                /* Release memory allocated to Mip node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL,
                                     (UINT1 *) (pMipNode));
                pMipNode = NULL;
            }
        } return;
    }

    MipNode.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    if (i2PortNum != -1)
    {
        MipNode.u2PortNum = i2PortNum;
        pMipNode = RBTreeGet (ECFM_CC_GLOBAL_MIP_TABLE, &MipNode);
    }
    if (pMipNode == NULL)
    {
        pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE, &MipNode, NULL);
    }
    while ((pMipNode != NULL)
           && (pMipNode->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ())
           && ((i2PortNum == -1) || (pMipNode->u2PortNum == i2PortNum)))
    {
        pMipNextNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE, pMipNode, NULL);

        if ((pMipNode->b1ImplicitlyCreated == ECFM_TRUE)
            && ((i4VlanIdIsid == -1)
                || (pMipNode->u4VlanIdIsid == (UINT4) i4VlanIdIsid)))
        {
            /* Delete MIP node */
            EcfmCcUtilDeleteMipEntry (pMipNode);

            /* Release memory allocated to Mip node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL, (UINT1 *) (pMipNode));
            pMipNode = NULL;
            pMipNode = pMipNextNode;
            continue;
        }
        pMipNode = pMipNextNode;
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilEvaluateAndCreateMip
 *                                                                          
 *    DESCRIPTION      : This function evaluates and create if implicit MIP
 *                       creation conditions are satisfied.
 *                       
 *    INPUT            : i2PortNum - Local Port Num, if evaluation 
 *                                   is to be done for port having ifindex
 *                                   i4IfIndex, otherwise for -1,evaluation is
 *                                   to be done for all ports.
 *                       i2VlanId - If evaluation is to be done for a particular
 *                                  vlanId or all, -1 indicates for all vlanIds
 *
 *    OUTPUT           : None.                        
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilEvaluateAndCreateMip (INT2 i2PortNum, INT4 i4VlanIdIsid,
                                BOOL1 b1DelImplicitMips)
{
    UINT1              *pVlanList = NULL;
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4VlanCounter = ECFM_INIT_VAL;
    UINT4               u4BitIndex = ECFM_INIT_VAL;
    UINT4               u4ByteIndex = ECFM_INIT_VAL;
    UINT4               u4ByteEnd = ECFM_INIT_VAL;
    UINT4               u4Event = ECFM_INIT_VAL;

    UINT2               u2VlanFlag = ECFM_INIT_VAL;
    UINT2               u2WordIndex = ECFM_INIT_VAL;
    UINT2               u2PortCounter = ECFM_INIT_VAL;
    UINT2               u2MaxPorts = (UINT2) ECFM_CC_MAX_PORT_INFO;
    UINT2               u2MinPorts = ECFM_PORTS_PER_CONTEXT_MIN;
    UINT1               au1NullVlans[ECFM_WORD_SIZE] = { 0 };

    pVlanList = UtilVlanAllocVlanListSize (sizeof (tVlanListExt));
    if (pVlanList == NULL)
    {
        return;
    }
    ECFM_MEMSET (pVlanList, ECFM_INIT_VAL, ECFM_VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pVlanList;
    VlanListAll.i4_Length = ECFM_VLAN_LIST_SIZE;

    u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();

    /* Events which needs to be processes in loop relinqush
     * */
    u4Event = ECFM_EV_CFM_PDU_IN_QUEUE | ECFM_EV_TMR_EXP | ECFM_EV_RED_BULK_UPD;

    if (b1DelImplicitMips == ECFM_TRUE)
    {
        /* Delete all previously implicitly configured MIPs */
        EcfmDeleteImplicitlyCreatedMips (i2PortNum, i4VlanIdIsid);
    }
    if (ECFM_CC_MIP_DYNAMIC_EVALUATION_STATUS == ECFM_FALSE)
    {
        UtilVlanReleaseVlanListSize (pVlanList);
        return;
    }

    /* Check if evaulation is to be done for a port or all the ports */
    if (i2PortNum != -1)

    {
        u2MinPorts = (UINT2) i2PortNum;
        u2MaxPorts = (UINT2) i2PortNum;
    }
    /* Evaluate MIP creation for each port */
    for (u2PortCounter = u2MinPorts; u2PortCounter <= u2MaxPorts;
         u2PortCounter++)

    {
        ECFM_CC_RESET_STAGGERING_DELTA ();

        /* Check if port is created */
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortCounter);
        if (pPortInfo == NULL)

        {
            continue;
        }
        /* Port should not be part of link aggregated Port */
        if (pPortInfo->u2ChannelPortNum != 0)
        {
            continue;
        }
        if (i4VlanIdIsid != -1)
        {
            u4VlanCounter = i4VlanIdIsid;
            /*  Check if Vlan is member of particular u2PortCounter port */
            if (EcfmMiIsVlanIsidMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                            u4VlanCounter,
                                            u2PortCounter) != OSIX_TRUE)
            {
                continue;
            }
            /* Check the various MIP creation criterias and if all goes well
             * create the MIP */
            EcfmCcUtilTryCreateMip (u2PortCounter, u4VlanCounter);
        }
        else
        {
            /* If we need to evaluate for a list of VLAN then get the list of
             * VIDs  which are member of the port
             */
            if (EcfmL2IwfGetPortVlanList (u4ContextId,
                                          &VlanListAll,
                                          u2PortCounter,
                                          ECFM_L2_MEMBER_PORT) != ECFM_SUCCESS)
            {
                continue;
            }

            for (u2WordIndex = 0; u2WordIndex < ECFM_VLAN_LIST_SIZE;
                 u2WordIndex += ECFM_WORD_SIZE)
            {
                if (((u2WordIndex + ECFM_WORD_SIZE) <= ECFM_VLAN_LIST_SIZE) &&
                    (ECFM_MEMCMP ((VlanListAll.pu1_OctetList + u2WordIndex),
                                  &au1NullVlans, ECFM_WORD_SIZE) == 0))
                {
                    continue;
                }
                u4ByteIndex = u2WordIndex;
                u4ByteEnd = u2WordIndex + ECFM_WORD_SIZE;

                for (; u4ByteIndex < u4ByteEnd; u4ByteIndex++)
                {
                    if (VlanListAll.pu1_OctetList[u4ByteIndex] == 0)
                    {
                        continue;
                    }
                    u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];
                    for (u4BitIndex = 0;
                         ((u4BitIndex < BITS_PER_BYTE)
                          && (EcfmUtilQueryBitListTable (u2VlanFlag, u4BitIndex)
                              != 0)); u4BitIndex++)
                    {
                        u4VlanCounter =
                            (u4ByteIndex * BITS_PER_BYTE) +
                            EcfmUtilQueryBitListTable (u2VlanFlag, u4BitIndex);
                        /* Check the various MIP creation criterias and if all goes well
                         * create the MIP */
                        EcfmCcUtilTryCreateMip (u2PortCounter, u4VlanCounter);
                    }
                }
            }
        }
        /* Check if threshold time of a loop has achived, if so
         * then process other ECFM event and then come back
         */
        EcfmCcUtilRelinquishLoop (u4Event);
    }
    UtilVlanReleaseVlanListSize (pVlanList);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilTryCreateMip
 *                                                                          
 *    DESCRIPTION      : This function creates the MIP if allowed by the
 *                       configuration.
 *                       
 *    INPUT            : i2PortNum - Local Port Num
 *                       i2VlanId -  VLAN ID
 *
 *    OUTPUT           : None.                        
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EcfmCcUtilTryCreateMip (UINT2 u2PortNum, UINT4 u4VlanIsid)
{
    UINT1              *pu1ActiveMdLevels = NULL;
    tEcfmCcMaInfo     **ppActiveMaNodes = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMipInfo     *pMipNewNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;

    UINT1               u1ActiveMdLevelIndex = ECFM_INIT_VAL;
    UINT1               u1ActiveMdLevel = ECFM_INIT_VAL;
    UINT1               u1NoOfMdLevels = ECFM_INIT_VAL;
    INT1                i1NextLowerLevel = ECFM_INIT_VAL;
    INT1                i1PrevMipLevel = -1;

    if (gppActiveMaNodes == NULL || gpu1ActiveMdLevels == NULL)
    {
        return;
    }
    ppActiveMaNodes = gppActiveMaNodes;
    pu1ActiveMdLevels = gpu1ActiveMdLevels;
    do
    {
        /* Initialize variables for another port and vlan combnation */
        ECFM_MEMSET (ppActiveMaNodes, ECFM_INIT_VAL,
                     ECFM_CC_ACTIVE_MA_LEVEL_SIZE);
        ECFM_MEMSET (pu1ActiveMdLevels, ECFM_INIT_VAL,
                     ECFM_CC_ACTIVE_MD_LEVEL_SIZE);

        u1ActiveMdLevelIndex = ECFM_INIT_VAL;
        u1ActiveMdLevel = ECFM_INIT_VAL;
        u1NoOfMdLevels = ECFM_INIT_VAL;
        i1NextLowerLevel = (INT1) -1;
        i1PrevMipLevel = (INT1) -1;
        pMipNewNode = NULL;
        pMipNode = NULL;

        /* Get active levels for particular port and VlanId */
        EcfmGetActiveMdLevels (u4VlanIsid, pu1ActiveMdLevels,
                               &u1NoOfMdLevels, ppActiveMaNodes, u2PortNum);

        /* Get Lowest Active MdLevel index and next lower active MD level */
        if (EcfmGetLowestActiveMdLevel (u2PortNum, u4VlanIsid,
                                        pu1ActiveMdLevels, u1NoOfMdLevels,
                                        &u1ActiveMdLevelIndex,
                                        &i1NextLowerLevel,
                                        &i1PrevMipLevel) != ECFM_TRUE)
        {
            break;
        }

        /* clear the corresponding error if already exists */
        if (u1ActiveMdLevelIndex < ECFM_MIP_ACTIVE_MD_LEVELS_MAX)
        {
            /* Get MA and active Md level from index, u1ActiveMdLevelIndex */
            pMaNode = ppActiveMaNodes[u1ActiveMdLevelIndex];
            u1ActiveMdLevel = pu1ActiveMdLevels[u1ActiveMdLevelIndex];
        }
        /* Check if Mhf creation criteria for Active MD level validates 
         * the MIP creation for particular port, vlanId*/
        if (EcfmCheckMhfCreationCriteria (pMaNode, u2PortNum,
                                          u4VlanIsid, u1ActiveMdLevel,
                                          i1NextLowerLevel) != ECFM_TRUE)
        {
            break;
        }

        /* Delete MIP if it exists at level lower than any lowest active
         * Mdlevel found */
        if (i1PrevMipLevel != -1)

        {
            pMipNode = EcfmCcUtilGetMipEntry (u2PortNum, i1PrevMipLevel,
                                              u4VlanIsid);
            if (pMipNode != NULL)
            {
                EcfmCcUtilDeleteMipEntry (pMipNode);

                /* Release memory allocated to Mip node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL,
                                     (UINT1 *) (pMipNode));
                pMipNode = NULL;
            }
        }
        /* Create node for MIP */
        if (ECFM_ALLOC_MEM_BLOCK_CC_MIP_TABLE (pMipNewNode) == NULL)

        {
            break;
        }
        ECFM_MEMSET (pMipNewNode, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);

        /* put values in Mip new node */
        pMipNewNode->u2PortNum = u2PortNum;
        pMipNewNode->u1MdLevel = u1ActiveMdLevel;
        pMipNewNode->u4VlanIdIsid = u4VlanIsid;
        pMipNewNode->b1Active = ECFM_TRUE;
        pMipNewNode->b1ImplicitlyCreated = ECFM_TRUE;
        pMipNewNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

        /*MIP status can be made to Active only ECFM is enabled at that */
        /*particular interface */
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
        if (NULL != pPortInfo)
        {
            if (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE)
            {
                pMipNewNode->b1Active = ECFM_TRUE;
            }
            else
            {
                pMipNewNode->b1Active = ECFM_FALSE;
            }
        }

        /* Add MIP in MIP and stack Tables of both the tasks */
        if ((EcfmCcUtilAddMipEntry (pMipNewNode, pMaNode)) != ECFM_SUCCESS)

        {

            /*DeAllocate memory assigned to new node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL,
                                 (UINT1 *) (pMipNewNode));
            pMipNewNode = NULL;
            break;
        }
    }
    while (0);

    return;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilAddConfigErrEntry
 *
 *    DESCRIPTION      : This function creates and add configuration error
 *                       entry in configuration Error list Table in Global info
 *
 *    INPUT            : u2PortNum - Local Port Number for which an error needs
 *                                   to be set. 
 *                       u2VlanId  - VlanId for which an error needs to be set
 *                       u1ErrType - Error type to be set for a particular 
 *                                   IfIndex with VlanId.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcUtilAddConfigErrEntry (UINT2 u2PortNum, UINT4 u4VlanIdIsid,
                             UINT1 u1ErrType)
{
    tEcfmCcConfigErrInfo *pConfigErrNode = NULL;
    tEcfmCcConfigErrInfo ConfigErrInfo;
    BOOL1               b1NewNode = ECFM_FALSE;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    /* Check if node corresponding to these indices already exists then just
     * modify the error type */
    ECFM_MEMSET (&ConfigErrInfo, ECFM_INIT_VAL, ECFM_CC_CONFIG_ERR_INFO_SIZE);

    if (u2PortNum != ECFM_VAL_0)
    {
        ConfigErrInfo.u4IfIndex = ECFM_CC_PORT_INFO (u2PortNum)->u4IfIndex;
    }
    ConfigErrInfo.u4VidIsid = u4VlanIdIsid;
    pConfigErrNode =
        RBTreeGet (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, &ConfigErrInfo);

    /* Create new node for these indices */
    if (pConfigErrNode == NULL)

    {

        /* Create a node for Config error */
        if (ECFM_ALLOC_MEM_BLOCK_CC_CONF_ERR_LIST_TABLE (pConfigErrNode) ==
            NULL)

        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                         ECFM_OS_RESOURCE_TRC,
                         "EcfmCcUtilAddConfigErrEntry : "
                         " Allocation for Configuration Error list Node Failed \r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
        ECFM_MEMSET (pConfigErrNode, ECFM_INIT_VAL,
                     ECFM_CC_CONFIG_ERR_INFO_SIZE);

        /* Fill  elements of config error node */
        if ((pTempPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum)) != NULL)
        {
            pConfigErrNode->u4IfIndex = pTempPortInfo->u4IfIndex;
        }
        pConfigErrNode->u4VidIsid = u4VlanIdIsid;
        b1NewNode = ECFM_TRUE;
    }

    /* Fill Error type */
    switch (u1ErrType)

    {
        case ECFM_CONFIG_ERR_CFM_LEAK:
            ECFM_CC_SET_CONFIG_ERROR_CFM_LEAK (pConfigErrNode);
            break;
        case ECFM_CONFIG_ERR_CONFLICT_VIDS:
            ECFM_CC_SET_CONFIG_ERROR_CONFLICT_VIDS (pConfigErrNode);
            break;
        case ECFM_CONFIG_ERR_OVERLAPPED_LEVELS:
            ECFM_CC_SET_CONFIG_ERROR_OVERLAPPED_LEVELS (pConfigErrNode);
            break;
        default:
            break;
    }

    /*If it is new node then add it */
    if (b1NewNode)

    {

        /* Add entry in global RB-Tree */
        if (RBTreeAdd (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, pConfigErrNode) ==
            ECFM_RB_FAILURE)
        {
            /*Release the memory allocated to Config Error new node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_CONF_ERR_LIST_TABLE_POOL,
                                 (UINT1 *) (pConfigErrNode));
            pConfigErrNode = NULL;
            return ECFM_FAILURE;
        }

    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilChkIfCfgCfmLeakErr
 *
 *    DESCRIPTION      : This function check if ifIndex and vid is configured
 *                       with CFM LEAK error. 
 *
 *    INPUT            : pMaInfo - pointer to MA info
 *                       u2PortNum - Local Port Number for which an error needs
 *                                   to be check. 
 *                       u2Vid  - VlanId for which an error needs to be set
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 ****************************************************************************/
PUBLIC              BOOL1
EcfmCcUtilChkIfCfgCfmLeakErr (tEcfmCcMaInfo * pMaInfo, UINT2 u2PortNum,
                              UINT2 u2Vid)
{
    UINT2              *pu2VlanId = NULL;
    UINT2               u2NoOfVlanIds = ECFM_INIT_VAL;
    UINT2               u2Counter = ECFM_INIT_VAL;
    UINT2               u2VlanCounter = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaTempNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT1                i1LevelCounter = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
    BOOL1               b1VidMember = ECFM_FALSE;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    /* For vlan Unaware MA, no error can be set */
    if (pMaInfo->u4PrimaryVidIsid == 0)
    {
        return ECFM_FALSE;
    }

    /* Allocating memory for ECFM_MAX_VLAN_ID = 4094 */
    pu2VlanId = (UINT2 *) (VOID *) UtilVlanAllocVlanIdSize (ECFM_VLANID_MAX *
                                                            sizeof (UINT2));

    if (pu2VlanId == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcUtilChkIfCfgCfmLeakErr:"
                     "memory allocation failure\r\n");
        return ECFM_FALSE;
    }

    ECFM_MEMSET (pu2VlanId, ECFM_INIT_VAL, ECFM_VLANID_MAX * sizeof (UINT2));

    /* Check if MAs Vid can pass through Port */
    if (pMaInfo->u2NumberOfVids == 1)
    {
        if (EcfmMiIsVlanIsidMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                        pMaInfo->u4PrimaryVidIsid,
                                        u2PortNum) != OSIX_TRUE)
        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return ECFM_FALSE;
        }
        pu2VlanId[0] = u2Vid;
        u2NoOfVlanIds = 1;
    }
    else
    {
        /* Get VlanId list for MA */
        if (EcfmGetVlanIdsForAPrimaryVid (pu2VlanId,
                                          &u2NoOfVlanIds,
                                          pMaInfo->u4PrimaryVidIsid) !=
            ECFM_TRUE)
        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return ECFM_FALSE;
        }

        /* Check if any Vid can pass through Port */
        for (u2Counter = 0; u2Counter < u2NoOfVlanIds; u2Counter++)

        {
            if ((u2Counter <= ECFM_VLANID_MAX) &&
                (EcfmMiIsVlanIsidMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                             pu2VlanId[u2Counter],
                                             u2PortNum) == OSIX_TRUE))
            {
                b1VidMember = ECFM_TRUE;
                break;
            }
        }

        /* No vid is member of this port */
        if (b1VidMember != ECFM_TRUE)

        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return ECFM_FALSE;
        }
    }

    /* Get MD Level from MA */
    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaInfo);
    u1MdLevel = pMdNode->u1Level;

    /* Check if there is any up MEP configured on this port and down MEP
     * configured on any port  */
    if ((EcfmIsMepAssocWithMa (pMaInfo->u4MdIndex, pMaInfo->u4MaIndex,
                               u2PortNum, ECFM_MP_DIR_UP) == ECFM_FALSE) &&
        (EcfmIsMepAssocWithMa (pMaInfo->u4MdIndex, pMaInfo->u4MaIndex,
                               -1, ECFM_MP_DIR_DOWN) == ECFM_FALSE))

    {

        /* Check if there is any MEP associated with any other MAy associated
         * with one of vids associated with MAx,at higher level */
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
        UNUSED_PARAM (pPortInfo);
        gpEcfmCcMepNode->u2PortNum = u2PortNum;
        pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                  gpEcfmCcMepNode, NULL);
        if ((pMepNode == NULL) ||
            ((pMepNode != NULL) && (pMepNode->u2PortNum != u2PortNum)))

        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return ECFM_FALSE;
        }

        /* Check if there exists a MEP at higher Md level associated with one of
         * Vids of MA */
        if (EcfmMepWithVidAtSameOrHgrLvl (pu2VlanId, u2NoOfVlanIds,
                                          u2PortNum, u1MdLevel, ECFM_TRUE)
            == ECFM_TRUE)

        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return ECFM_TRUE;
        }
    }

    /* Check if there exists an MA for a level lower than this MAs level
     * with vid */
    if (u1MdLevel == 0)

    {
        /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
        UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
        return ECFM_FALSE;
    }

    /* No domain exists */
    for (i1LevelCounter = (INT1) (u1MdLevel - 1); i1LevelCounter >= 0;
         i1LevelCounter--)

    {
        for (u2VlanCounter = 0; u2VlanCounter < u2NoOfVlanIds; u2VlanCounter++)

        {
            if (u2VlanCounter <= ECFM_VLANID_MAX)
            {
                pMaTempNode = NULL;
                pMaTempNode = EcfmCcUtilGetMaAssocWithVid
                    (pu2VlanId[u2VlanCounter], i1LevelCounter);
                if (pMaTempNode != NULL)

                {
                    if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaTempNode) !=
                        ECFM_TRUE)

                    {
                        /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
                        UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
                        return ECFM_TRUE;
                    }

                    /* Mep exists */
                    /* but no up MEP is configured on this port with
                     * this MA  */
                    /* No down MEP is configured on any port */
                    if ((EcfmIsMepAssocWithMa
                         (pMaTempNode->u4MdIndex, pMaTempNode->u4MaIndex,
                          u2PortNum, ECFM_MP_DIR_UP) == ECFM_FALSE)
                        &&
                        (EcfmIsMepAssocWithMa
                         (pMaTempNode->u4MdIndex, pMaTempNode->u4MaIndex,
                          -1, ECFM_MP_DIR_DOWN) == ECFM_FALSE))

                    {
                        /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
                        UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
                        return ECFM_TRUE;
                    }
                }
            }
        }
    }
    /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
    UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
    return ECFM_FALSE;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilClearCfmLeakErr
 *
 *    DESCRIPTION      : While making MEP active, clear the CFMLeak error in
 *                       MEPs which are already created in the higher level
 *                       with same vlan id
 *    INPUT            : pMepNode - pointer to Mep info
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilClearCfmLeakErr (tEcfmCcMaInfo * pMaInfo, UINT2 u2PortNum,
                           UINT2 u2Vid)
{
    tEcfmCcMepInfo     *pTempMepNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT2              *pu2VlanId = NULL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
    UINT2               u2NoOfVlanIds = ECFM_INIT_VAL;
    UINT2               u2Counter = ECFM_INIT_VAL;
    UINT2               u2VlanCounter = ECFM_INIT_VAL;
    BOOL1               b1VidMember = ECFM_FALSE;

    if (pMaInfo == NULL)
    {
        return;
    }

    /* For vlan Unaware MA, no error can be set */
    if (pMaInfo->u4PrimaryVidIsid == 0)
    {
        return;
    }

    /* Allocating memory for ECFM_MAX_VLAN_ID = 4094 */
    pu2VlanId = (UINT2 *) (VOID *) UtilVlanAllocVlanIdSize (ECFM_VLANID_MAX *
                                                            sizeof (UINT2));
    if (pu2VlanId == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcUtilClearCfmLeakErr:"
                     "memory allocation failure\r\n");
        return;
    }

    ECFM_MEMSET (pu2VlanId, ECFM_INIT_VAL, ECFM_VLANID_MAX * sizeof (UINT2));

    /* Check if MAs Vid can pass through Port */
    if (pMaInfo->u2NumberOfVids == 1)
    {
        if (EcfmMiIsVlanIsidMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                        pMaInfo->u4PrimaryVidIsid,
                                        u2PortNum) != OSIX_TRUE)
        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return;
        }
        pu2VlanId[0] = u2Vid;
        u2NoOfVlanIds = 1;
    }
    else
    {
        /* Get VlanId list for MA */
        if (EcfmGetVlanIdsForAPrimaryVid (pu2VlanId,
                                          &u2NoOfVlanIds,
                                          pMaInfo->u4PrimaryVidIsid) !=
            ECFM_TRUE)
        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return;
        }

        /* Check if any Vid can pass through Port */
        for (u2Counter = 0; u2Counter < u2NoOfVlanIds; u2Counter++)

        {
            if ((u2Counter <= ECFM_VLANID_MAX) &&
                (EcfmMiIsVlanIsidMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                             pu2VlanId[u2Counter],
                                             u2PortNum) == OSIX_TRUE))
            {
                b1VidMember = ECFM_TRUE;
                break;
            }
        }

        /* No vid is member of this port */
        if (b1VidMember != ECFM_TRUE)

        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return;
        }
    }
    /* Get MD Level from MA */
    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaInfo);
    u1MdLevel = pMdNode->u1Level;

    /* Get MEP associated with any of the Vids from pu2VlanId */
    for (u2VlanCounter = 0; u2VlanCounter < u2NoOfVlanIds; u2VlanCounter++)
    {
        pTempMepNode =
            (tEcfmCcMepInfo *) RBTreeGetFirst (ECFM_CC_PORT_MEP_TABLE);
        while (pTempMepNode != NULL)
        {
            if ((pTempMepNode->u1MdLevel > u1MdLevel) &&
                (pTempMepNode->u4PrimaryVidIsid == pu2VlanId[u2VlanCounter]))
            {
                /* Clear CFM LEAK error if exists already */
                EcfmCcUtilDeleteConfigErrEntry (pTempMepNode->u2PortNum,
                                                pTempMepNode->
                                                u4PrimaryVidIsid,
                                                ECFM_CONFIG_ERR_CFM_LEAK);

                if (EcfmCcUtilChkIfCfgCfmLeakErr
                    (pTempMepNode->pMaInfo, pTempMepNode->u2PortNum,
                     pTempMepNode->u4PrimaryVidIsid) == ECFM_TRUE)

                {
                    if (EcfmCcUtilAddConfigErrEntry
                        (pTempMepNode->u2PortNum,
                         pTempMepNode->u4PrimaryVidIsid,
                         ECFM_CONFIG_ERR_CFM_LEAK) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                     ECFM_OS_RESOURCE_TRC,
                                     "EcfmMepUtlSetAgMepRowStatus : Could not add"
                                     "Configuration error ECFM_CONFIG_ERR_CFM_LEAK"
                                     "in Configuration Error List Table"
                                     "\r\n");
                    }
                }

            }
            pTempMepNode =
                RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, pTempMepNode, NULL);
        }
    }

    /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
    UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUtilChkIfCfgConfictVidErr
 *
 *    DESCRIPTION      : This function check if ifIndex and vid is configured
 *                       with conficting Vids error. 
 *
 *    INPUT            : pMaInfo - pointer to MA info
 *                       u2PortNum - Local Port Number for which an error needs
 *                                   to be check. 
 *                       u2Vid  - VlanId for which an error needs to be set
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 ****************************************************************************/
PUBLIC              BOOL1
EcfmCcUtilChkIfCfgConfictVidErr (tEcfmCcMaInfo * pMaInfo,
                                 UINT2 u2PortNum, UINT2 u2Vid)
{
    UINT2              *pu2VlanId = NULL;
    UINT2               u2NoOfVlanIds = ECFM_INIT_VAL;
    UINT2               u2Counter = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaTempNode = NULL;

    /* For vlan Unaware MA no error can be set */
    if (pMaInfo->u4PrimaryVidIsid == 0)

    {
        return ECFM_FALSE;
    }
    if ((EcfmIsMepAssocWithMa
         (pMaInfo->u4MdIndex, pMaInfo->u4MaIndex, u2PortNum,
          ECFM_MP_DIR_UP) == ECFM_FALSE))

    {
        return ECFM_FALSE;
    }

    /* Allocating memory for ECFM_MAX_VLAN_ID = 4094 */
    pu2VlanId = (UINT2 *) (VOID *) UtilVlanAllocVlanIdSize (ECFM_VLANID_MAX
                                                            * sizeof (UINT2));
    if (pu2VlanId == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcUtilChkIfCfgConfictVidErr:"
                     "memory allocation failure\r\n");
        return ECFM_FALSE;
    }

    ECFM_MEMSET (pu2VlanId, ECFM_INIT_VAL, ECFM_VLANID_MAX * sizeof (UINT2));

    /* Get VlanIds associated with MA */
    if (pMaInfo->u2NumberOfVids == 1)

    {
        pu2VlanId[0] = u2Vid;
        u2NoOfVlanIds = 1;
    }

    else

    {

        /* Get VlanId list for MA */
        if (EcfmGetVlanIdsForAPrimaryVid (pu2VlanId,
                                          &u2NoOfVlanIds,
                                          pMaInfo->u4PrimaryVidIsid) !=
            ECFM_TRUE)
        {
            /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
            UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
            return ECFM_FALSE;
        }
    }

    /* Get MD Level from MA */
    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaInfo);
    u1MdLevel = pMdNode->u1Level;

    /* Check if there exists up MEP at same Md level associated with one of
     * Vids of MA at any port */
    for (u2Counter = 0; u2Counter < u2NoOfVlanIds; u2Counter++)

    {
        if (u2Counter < ECFM_VLANID_MAX)
        {
            pMaTempNode = NULL;
            pMaTempNode = EcfmCcUtilGetMaAssocWithVid
                (pu2VlanId[u2Counter], u1MdLevel);
            if (pMaTempNode != NULL)

            {
                if ((pMaTempNode->u4MdIndex != pMaInfo->u4MdIndex) ||
                    (pMaTempNode->u4MaIndex != pMaInfo->u4MaIndex))

                {
                    if (EcfmIsMepAssocWithMa
                        (pMaTempNode->u4MdIndex, pMaTempNode->u4MaIndex, -1,
                         ECFM_MP_DIR_UP) == ECFM_TRUE)

                    {
                        /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
                        UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
                        return ECFM_TRUE;
                    }
                }
            }
        }
    }
    /* Releasing memory of ECFM_MAX_VLAN_ID = 4094 */
    UtilVlanReleaseVlanIdSize ((UINT1 *) pu2VlanId);
    return ECFM_FALSE;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmGetVlanIdsForAPrimaryVid
 *
 *    DESCRIPTION      : This function gets Vlands associated with a primaryVid
 *                       into a list, if no entry found return false.
 *
 *    INPUT            : u2PrimaryVid - Primary VlanId for which vlanIds need to 
 *                                      be found.
 *
 *    OUTPUT           : pu2VlanId - VlanIds associated with Primary Vid
 *                       pu2NoOfVlanIds - Number of Vlands associated with
 *                                        Primary vid.
 *
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 ****************************************************************************/
PRIVATE             BOOL1
EcfmGetVlanIdsForAPrimaryVid (UINT2 *pu2VlanId, UINT2 *pu2NoOfVlanIds,
                              UINT2 u2PrimaryVid)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    UINT2               u2Counter = ECFM_INIT_VAL;
    BOOL1               b1Found = ECFM_FALSE;
    pVlanNode = RBTreeGetFirst (ECFM_CC_VLAN_TABLE);
    if (pVlanNode == NULL)

    {
        return ECFM_FALSE;
    }
    while (pVlanNode != NULL)

    {

        /* Get if required VlanNode found */
        if (pVlanNode->u4PrimaryVidIsid == u2PrimaryVid)

        {

            /* Add in the list of vlanIds */
            b1Found = ECFM_TRUE;
            pu2VlanId[u2Counter] = pVlanNode->u4VidIsid;
            u2Counter = u2Counter + ECFM_INCR_VAL;
        }
        pVlanNode = RBTreeGetNext (ECFM_CC_VLAN_TABLE, pVlanNode, NULL);
    }
    if (b1Found == ECFM_TRUE)

    {
        pu2VlanId[u2Counter] = u2PrimaryVid;
        *pu2NoOfVlanIds = u2Counter + ECFM_INCR_VAL;
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmMepWithVidAtSameOrHgrLvl
 *
 *    DESCRIPTION      : This function checks if there is any MEP exists at 
 *                       at same or higher level than u1MdLevel associtiated
 *                       with any of VlanIds from list pu2VlanId at port u4IfIndex.
 *
 *    INPUT            : pu2VlanId - List of VlanIds
 *                       u2NoOfVlanIds - Number of Vlands associated with
 *                                       Primary vid.
 *                       u2PortNum - Local Port Number at which MEP needs to 
 *                                   find
 *                       u1MdLevel - MdLevel of MEP.
 *                       b1HigherLevel - True If MEP higher than u1MdLevel needs
 *                                       to be found otherwise MEP at same level
 *                                       needs to be found
 *                         
 *    OUTPT           : None.
 *
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 ****************************************************************************/
PRIVATE             BOOL1
EcfmMepWithVidAtSameOrHgrLvl (UINT2 *pu2VlanId, UINT2 u2NoOfVlanIds,
                              UINT2 u2PortNum, UINT1 u1MdLevel,
                              BOOL1 b1HigherLevel)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT2               u2VlanCounter = ECFM_INIT_VAL;
    UINT1               u1MinLevel = ECFM_INIT_VAL;
    UINT1               u1MaxLevel = ECFM_MD_LEVEL_MAX;
    UINT1               u1LevelCounter = ECFM_INIT_VAL;
    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (b1HigherLevel == ECFM_TRUE)

    {
        u1MinLevel = u1MdLevel + (UINT1) 1;
        u1MaxLevel = ECFM_MD_LEVEL_MAX;
    }

    else

    {
        u1MinLevel = u1MdLevel;
        u1MaxLevel = u1MdLevel;
    }
    if (pPortInfo == NULL)

    {
        return ECFM_FALSE;
    }

    /* Scan for MEPs higher than u1MdLevel */
    for (u1LevelCounter = u1MinLevel; u1LevelCounter <= u1MaxLevel;
         u1LevelCounter++)

    {

        /* Get MEP associated with any of the Vids from pu2VlanId */
        for (u2VlanCounter = 0; u2VlanCounter < u2NoOfVlanIds; u2VlanCounter++)

        {
            ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
            gpEcfmCcMepNode->u1MdLevel = u1LevelCounter;
            gpEcfmCcMepNode->u4PrimaryVidIsid = pu2VlanId[u2VlanCounter];
            gpEcfmCcMepNode->u2PortNum = u2PortNum;
            gpEcfmCcMepNode->u1Direction = ECFM_MP_DIR_DOWN;
            pMepNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode);
            if (pMepNode != NULL)

            {
                return ECFM_TRUE;
            }

            /* Get for UP direction */
            gpEcfmCcMepNode->u1Direction = ECFM_MP_DIR_UP;
            pMepNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode);
            if (pMepNode != NULL)

            {
                return ECFM_TRUE;
            }
        }
        if (b1HigherLevel != ECFM_TRUE)

        {
            return ECFM_FALSE;
        }
    }
    return ECFM_FALSE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcUtilDeleteConfigErrEntry
 *
 *    DESCRIPTION      : This function deletes configuration error list entry
 *                       from Configuration error ListTable present in Global
 *                       info, if and only if there is no error, otherwise just
 *                       clear the particular type of error
 *
 *    INPUT            : u2PortNum - Local Port Number
 *                       u2VlanId -  Vlan Id.
 *                       u1Type  - Type of error to be cleared
 *                       Indices for config error list entry to be deleted.
 *    OUTPUT           : None.
 *
 *    RETURNS          : None.
 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilDeleteConfigErrEntry (UINT2 u2PortNum, UINT4 u4VlanIdIsid,
                                UINT1 u1Type)
{
    tEcfmCcConfigErrInfo *pConfigErrNode = NULL;
    tEcfmCcConfigErrInfo ConfigErrInfo;
    ECFM_MEMSET (&ConfigErrInfo, ECFM_INIT_VAL, ECFM_CC_CONFIG_ERR_INFO_SIZE);
    if (u2PortNum != ECFM_VAL_0)
    {
        ConfigErrInfo.u4IfIndex = ECFM_CC_PORT_INFO (u2PortNum)->u4IfIndex;
    }
    ConfigErrInfo.u4VidIsid = u4VlanIdIsid;
    pConfigErrNode =
        RBTreeGet (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, &ConfigErrInfo);

    /* Check if Config err node corresponding to indices exist and its error 
     * is other than cfm leak and conflicting vids */
    if (pConfigErrNode == NULL)

    {
        return;
    }

    /* Clear the particular type of error */
    if (u1Type == ECFM_CONFIG_ERR_CFM_LEAK)

    {
        ECFM_CC_CLEAR_CONFIG_ERROR_CFM_LEAK (pConfigErrNode);
    }

    else if (u1Type == ECFM_CONFIG_ERR_CONFLICT_VIDS)

    {
        ECFM_CC_CLEAR_CONFIG_ERROR_CONFLICT_VIDS (pConfigErrNode);
    }

    if (pConfigErrNode->u1ErrorType == 0)

    {

        /* Delete the node */
        RBTreeRem (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, pConfigErrNode);

        /* Release the memory allocated to node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_CONF_ERR_LIST_TABLE_POOL,
                             (UINT1 *) (pConfigErrNode));
        pConfigErrNode = NULL;
    }
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmIsMepAssocWithMa 
 *                                                                          
 *    DESCRIPTION      : This function checks if there exists any or (up/down)MEP 
 *                       associated with specific MA, at a specified or any port
 *                       
 *                       
 *    INPUT            : u4MdIndex - Md Index of MA
 *                       u4MaIndex - Ma Index of MA
 *                       i2PortNum - Local Port Number at which MEP needs to 
 *                                   find. -1, indicates MEP needs to find on
 *                                   any port.
 *                       i1Dir  -  Direction of required MEP. -1, indicates any
 *                                 MEP needs to find
 *
 *    OUTPUT           : None                        
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE.
 *    
 *                                                                          
 ******************************************************************************/
PUBLIC              BOOL1
EcfmIsMepAssocWithMa (UINT4 u4MdIndex, UINT4 u4MaIndex, INT2 i2PortNum,
                      INT1 i1Dir)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    gpEcfmCcMepNode->u4MdIndex = u4MdIndex;
    gpEcfmCcMepNode->u4MaIndex = u4MaIndex;
    gpEcfmCcMepNode->u2MepId = 0;

    pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, gpEcfmCcMepNode, NULL);
    while ((pMepNode != NULL) &&
           (pMepNode->u4MdIndex == u4MdIndex) &&
           (pMepNode->u4MaIndex == u4MaIndex))
    {
        if ((i2PortNum != -1) && (pMepNode->u2PortNum != i2PortNum))
        {
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
            continue;
        }
        if ((i1Dir != -1) && (pMepNode->u1Direction != i1Dir))
        {
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
            continue;
        }
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/*******************************************************************************
 * Function Name      : EcfmCcUtilPostTransaction
 *                                                                          
 * Description        : This routine is used to send the transaction start/stop
 *                      event to the cc task.
 *    
 * Input(s)           : pMepInfo - Pointer to the mep info for which the
 *                                 transaction will be started/stopped
 *                      u1TransType - Transaction Type
 * Output(s)          : None
 *                                                                          
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcUtilPostTransaction (tEcfmCcMepInfo * pMepInfo, UINT1 u1TransType)
{
    tEcfmCcMsg         *pMsg = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCctUtilPostTransaction:"
                     "memory allocation failure\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);
    pMsg->MsgType = (tEcfmMsgType) u1TransType;
    pMsg->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    pMsg->u2PortNum = pMepInfo->u2PortNum;
    if (pMepInfo->pEcfmMplsParams == NULL)
    {
        pMsg->uMsg.Mep.u4VidIsid = pMepInfo->u4PrimaryVidIsid;
        pMsg->uMsg.Mep.u1MdLevel = pMepInfo->u1MdLevel;
        pMsg->uMsg.Mep.u1Direction = pMepInfo->u1Direction;
        pMsg->uMsg.Mep.u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;
    }
    else                        /* For Mplstp get the MepInfo from ECFM_CC_MEP_TABLE */
    {
        pMsg->uMsg.Mep.u4MdIndex = pMepInfo->u4MdIndex;
        pMsg->uMsg.Mep.u4MaIndex = pMepInfo->u4MaIndex;
        pMsg->uMsg.Mep.u2MepId = pMepInfo->u2MepId;
        pMsg->uMsg.Mep.u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;
    }

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCctUtilPostTransaction:"
                     "failure occurred while posting cfg message to cc queue\r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcGetLocDefectFromRmep
 *
 * Description        : This routine is used to find out if LOC exists between 
 *                      a MEP and a particular remote MEP.
 *                        
 * Input(s)           : pMepInfo - Pointer to the mep info for which the 
 *                                 information is desired
 *                      u2TargetRMepId - MEP ID of the remote MEP with whom LOC
 *                                  status is desired
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcGetLocDefectFromRmep (tEcfmCcMepInfo * pMepInfo, UINT2 u2TargetRMepId)
{
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    if (ECFM_CC_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }

    /* Get Corresponding Remote MEP Info for the found CC Tasks
     * MepInfo 
     */
    pRMepInfo = EcfmUtilGetRmepInfo (pMepInfo, u2TargetRMepId);
    if (pRMepInfo == NULL)

    {

        /* No Remote RMEP is present */
        return ECFM_FAILURE;
    }
    if (pRMepInfo->b1RMepCcmDefect == ECFM_TRUE)

    {

        /* There is no connectivity between the 2 MEPs */
        return ECFM_SUCCESS;
    }
    return ECFM_FAILURE;
}

/*******************************************************************************
 * Function Name      : EcfmCcUtilGetMepSenderIdPerm
 *
 * Description        : This routine is used to check if sender-id tlv is to be
 *                      included for a MEP.
 *                        
 * Input(s)           : pMepInfo - Pointer to the mep info.
 * Output(s)          : None
 *
 * Return Value(s)    : Sender-Id Permission
 ******************************************************************************/
PUBLIC UINT1
EcfmCcUtilGetMepSenderIdPerm (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcMaInfo      *pMaInfo = pMepInfo->pMaInfo;
    tEcfmCcMdInfo      *pMdInfo = pMaInfo->pMdInfo;
    UINT1               u1SenderIdPermission = ECFM_SENDER_ID_NONE;
    ECFM_CC_TRC_FN_ENTRY ();
    if (pMaInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

    {
        if (pMdInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

        {

            /* there is no encompassing Maintenance Domain, the
             * value sendIdDefer takes the meaning of sendIdChassisManage
             */
            u1SenderIdPermission = ECFM_SENDER_ID_CHASSID_MANAGE;
        }

        else

        {
            u1SenderIdPermission = pMdInfo->u1IdPermission;
        }
    }

    else

    {
        u1SenderIdPermission = pMaInfo->u1IdPermission;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return u1SenderIdPermission;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilNotifyY1731 
 *                                                                          
 *    DESCRIPTION      : This function notifies the various state machines 
 *                       for events like Module enable/disable, interface
 *                       Up/Down and  MEP active/inactive.
 *
 *    INPUT            : pMepInfo - Pointer to the MEPInfo to notify
 *                       Indication - Event to notify
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilNotifyY1731 (tEcfmCcMepInfo * pMepInfo, UINT1 u1Indication)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = pMepInfo;
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepInfo);
    switch (u1Indication)

    {
        case ECFM_IND_MODULE_ENABLE:
        case ECFM_IND_MEP_ACTIVE:

            /* Send event to State machines if Port's Module status is ENABLE
             * and MEP is ACTIVE */
            if ((pPortInfo->u1PortY1731Status == ECFM_ENABLE) &&
                (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
                (pMepInfo->b1Active == ECFM_TRUE))

            {

                /* Send Mep Active event to AIS Initiator */
                EcfmCcClntAisInitiator (pMepInfo, ECFM_EV_MEP_BEGIN);

                /* Send Mep Active event to LCK Initiator */
                EcfmCcClntLckInitiator (pMepInfo, ECFM_EV_MEP_BEGIN);

                /* Send Mep Active event to LM Initiator */
                EcfmCcClntLmInitiator (&PduSmInfo, ECFM_EV_MEP_BEGIN);
            }
            break;
        case ECFM_IND_IF_DELETE:
        case ECFM_IND_MODULE_DISABLE:
        case ECFM_IND_MEP_INACTIVE:
            if ((pPortInfo->u1PortY1731Status == ECFM_ENABLE) &&
                (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE))

            {

                /* Send Mep Inactive event to AIS Initiator */
                EcfmCcClntAisInitiator (pMepInfo, ECFM_EV_MEP_NOT_ACTIVE);

                /* Send Mep Inactive event to LCK Initiator */
                EcfmCcClntLckInitiator (pMepInfo, ECFM_EV_MEP_NOT_ACTIVE);

                /* Send Mep Inactive event to LM Initiator */
                EcfmCcClntLmInitiator (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
            }
            break;
        default:
            break;
    }
    return;
}

/**************************************************************************/
/* Function Name       : EcfmConvertPortListToArray                       */
/*                                                                        */
/* Description         : This function converts Local port list to array  */
/*                                                                        */
/* Input(s)            : PortList - Local port list                       */
/*                       pu4IfPortArray - array to hold list of ports     */
/*                       u1NumPorts - Number of ports in array            */
/*                                                                        */
/* Output(s)           : pu4IfPortArray, pu1NumPorts                      */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
EcfmConvertPortListToArray (tPortListExt PortList,
                            UINT4 *pu4IfPortArray, UINT2 *pu2NumPorts)
{
    UINT2               u2Port;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT2               u2Counter = 0;

    for (u2BytIndex = 0; u2BytIndex < ECFM_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortList[u2BytIndex];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {
            u2Port =
                (u2BytIndex * BITS_PER_BYTE) +
                EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
            if (u2Counter < ECFM_MAX_PORTS_PER_CONTEXT)
            {
                pu4IfPortArray[u2Counter] = u2Port;
                u2Counter = (UINT2) (u2Counter + 1);
            }
        }
    }
    /* update number of ports */
    *pu2NumPorts = u2Counter;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcUtilUpdatePortState
 *                                                                          
 *    DESCRIPTION      : This function updates the port state variable.
 *
 *    INPUT            : pMepInfo - Pointer to the MEPInfo.
 *                       u1PortState - Port state value.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcUtilUpdatePortState (tEcfmCcMepInfo * pMepInfo, UINT1 u1PortState)
{
    BOOL1               bIsMemPort = OSIX_FALSE;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    /* For Vlan Unaware MEPs if the spanning-tree mode being ran is MSTP,
     * then this TLV should not be transmitted.
     * Refer Section 21.5.4 Port Status TLV of IEEE 802.1ag/D8.1 2007. 
     * Hence, only RSTP will be run if this TLV is to be transmitted.*/

    if (u1PortState == (UINT1) ECFM_INIT_VAL)
    {
        if ((pMepInfo->pPortInfo->u1PortType == CFA_CUSTOMER_EDGE_PORT) &&
            (pMepInfo->u1Direction == ECFM_MP_DIR_UP))
        {
            u1PortState = L2IwfGetPbPortState (pMepInfo->u4IfIndex,
                                               (UINT2) pMepInfo->
                                               u4PrimaryVidIsid);
            if (u1PortState == AST_PORT_STATE_FORWARDING)
            {
                pMepInfo->u1PepPortState = ECFM_PORT_IS_UP;
            }
            else
            {
                pMepInfo->u1PepPortState = ECFM_PORT_IS_BLOCKED;
            }
            return;
        }
        else if (EcfmIsStpEnabledInContext (ECFM_CC_CURR_CONTEXT_ID ()
                                            != ECFM_SUCCESS))
        {
            u1PortState = AST_PORT_STATE_FORWARDING;
        }
        else if (pMepInfo->u4PrimaryVidIsid == ECFM_INIT_VAL)
        {
            /* Acquire the port state based on index instead of vlan.
             * Instance 0 corresponds to RSTP.*/
            if ((pTempPortInfo =
                 ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum)) != NULL)
            {
                u1PortState = L2IwfGetInstPortState (ECFM_INIT_VAL,
                                                     pTempPortInfo->u4IfIndex);
            }
        }
        else
        {
            if ((pTempPortInfo =
                 ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum)) != NULL)
            {
                u1PortState =
                    EcfmL2IwfGetVlanPortState (pMepInfo->u4PrimaryVidIsid,
                                               pTempPortInfo->u4IfIndex);
            }
        }
    }

    if (u1PortState == AST_PORT_STATE_FORWARDING)
    {
        if (pMepInfo->u4PrimaryVidIsid == 0)
        {
            pMepInfo->pPortInfo->u1PortState = ECFM_PORT_IS_UP;
        }
        else
        {
            bIsMemPort =
                EcfmMiIsVlanIsidMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                            pMepInfo->u4PrimaryVidIsid,
                                            pMepInfo->u2PortNum);
            if (bIsMemPort != OSIX_TRUE)
            {
                pMepInfo->pPortInfo->u1PortState = ECFM_PORT_IS_BLOCKED;
            }
            else
            {
                pMepInfo->pPortInfo->u1PortState = ECFM_PORT_IS_UP;
            }
        }
    }
    else
    {
        pMepInfo->pPortInfo->u1PortState = ECFM_PORT_IS_BLOCKED;
    }
    return;
}

/*****************************************************************************
 * Name               : EcfmCcUtilDupCruBuf 
 *
 * Description        : This is used to duplicate the CRU-Buffer
 *
 * Input(s)           : pSrcCruBuf - Pointer to source CRU-Buffer which needs to 
 *                                   be duplicated
 * Output(s)          : None
 *
 * Return Value(s)    : tEcfmBufChainHeader*
 *****************************************************************************/
PUBLIC tEcfmBufChainHeader *
EcfmCcUtilDupCruBuf (tEcfmBufChainHeader * pSrcCruBuf)
{
    UINT4               u4BufSize = ECFM_INIT_VAL;
    tEcfmBufChainHeader *pCruBuf = NULL;
    UINT1              *pu1Frame = NULL;
    UINT1               u1OpCode = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();
    u4BufSize = ECFM_GET_CRU_VALID_BYTE_COUNT (pSrcCruBuf);
    if (u4BufSize == ECFM_INIT_VAL)
    {
        return NULL;
    }
    pCruBuf = ECFM_ALLOC_CRU_BUF (u4BufSize, ECFM_INIT_VAL);
    if (pCruBuf == NULL)
    {
        ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
        return NULL;
    }
    pu1Frame = ECFM_GET_DATA_PTR_IF_LINEAR (pSrcCruBuf, 0, u4BufSize);
    if (pu1Frame == NULL)
    {
        pu1Frame = ECFM_CC_PDU;
        ECFM_MEMSET (pu1Frame, ECFM_INIT_VAL, u4BufSize);
        ECFM_COPY_FROM_CRU_BUF (pSrcCruBuf, pu1Frame, 0, u4BufSize);
    }
    ECFM_COPY_OVER_CRU_BUF (pCruBuf, pu1Frame, 0, u4BufSize);
    u1OpCode = ECFM_BUF_GET_OPCODE (pSrcCruBuf);
    ECFM_BUF_SET_OPCODE (pCruBuf, u1OpCode);
    ECFM_CC_TRC_FN_EXIT ();
    return pCruBuf;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmCcUtilGetDefaultMdNode
 * 
 * DESCRIPTION      : This funtion is used to add a default MD table entry
 *
 * INPUT            : u4VlanIdIsid - VLAN or ISID
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 **************************************************************************/
PUBLIC VOID
EcfmCcUtilGetDefaultMdNode (UINT4 u4VlanIdIsid,
                            tEcfmCcDefaultMdTableInfo ** ppDefaultMdNode)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    static tEcfmCcDefaultMdTableInfo DefMdDummyEntry;

    *ppDefaultMdNode = NULL;
    DefMdDummyEntry.u4PrimaryVidIsid = u4VlanIdIsid;
    pDefaultMdNode = (tEcfmCcDefaultMdTableInfo *) RBTreeGet
        (ECFM_CC_DEF_MD_TABLE, &DefMdDummyEntry);

    if (pDefaultMdNode == NULL)
    {
        ECFM_MEMSET (&DefMdDummyEntry, 0x00,
                     sizeof (tEcfmCcDefaultMdTableInfo));
        DefMdDummyEntry.i1MdLevel = ECFM_DEF_MD_LEVEL_DEF_VAL;
        DefMdDummyEntry.u1MhfCreation = ECFM_MHF_CRITERIA_DEFER;
        DefMdDummyEntry.u1IdPermission = ECFM_SENDER_ID_DEFER;
        DefMdDummyEntry.u4PrimaryVidIsid = u4VlanIdIsid;
        DefMdDummyEntry.b1Status = ECFM_TRUE;
        pDefaultMdNode = &DefMdDummyEntry;
    }
    *ppDefaultMdNode = pDefaultMdNode;
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmCcUtilRelinquishLoop
 *
 *    Description          : This is the function will to relenquish and process
 *                           CFM events
 *
 *    Input(s)             : u4ProcessEvent - Events to be processed
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcUtilRelinquishLoop (UINT4 u4ProcessEvent)
{
    UINT4               u4Event = ECFM_INIT_VAL;
    tOsixSysTime        u4CurrentTime = 0;

    OsixGetSysTime (&u4CurrentTime);
    /*When Current Time graetor the Threshold value then Relinqiush is done
     * and other events are processed*/
    if (u4CurrentTime >= gEcfmCcGlobalInfo.u4StaggeringDelta)
    {
        /*Receive the Events for the CFM PDUS, Interrupt PDUs, Config PDUs */
        ECFM_RECEIVE_EVENT (ECFM_CC_TASK_ID, u4ProcessEvent,
                            OSIX_NO_WAIT, &u4Event);

        gEcfmCcGlobalInfo.u4StaggeredCtxId = ECFM_CC_CURR_CONTEXT_ID ();

        /*Process the Events in the CC Queues */
        EcfmCcProcessQueue (u4Event, ECFM_FALSE);

        ECFM_CC_SELECT_CONTEXT (gEcfmCcGlobalInfo.u4StaggeredCtxId);

        /* Update the next relinquish interval */
        OsixGetSysTime (&u4CurrentTime);

        gEcfmCcGlobalInfo.u4StaggeringDelta =
            (u4CurrentTime + ECFM_CC_STAGGERING_INTERVAL);
    }

    return;
}

/*****************************************************************************
 *    Function Name        : EcfmCcUtilY1731EnableMep
 *
 *    Description          : This is the function will enable the Y1731 on a 
 *                           given Mep 
 *
 *    Input(s)             : pMepInfo - Mep on which Y1731 should be enabled 
 *
 *    Output(s)            : None.
 *
 *    Returns              : None
 *****************************************************************************/

PUBLIC VOID
EcfmCcUtilY1731EnableMep (tEcfmCcMepInfo * pMepInfo)
{
    /* Set the Default Values according to Y.1731 */
    /* Set CC enabled status as the one in the MA */
    pMepInfo->CcInfo.b1CciEnabled = pMepInfo->pMaInfo->b1CciEnabled;
    pMepInfo->CcInfo.b1UnExpectedPeriodDefect = ECFM_FALSE;
    pMepInfo->CcInfo.b1UnExpectedMepDefect = ECFM_FALSE;
    pMepInfo->CcInfo.b1MisMergeDefect = ECFM_FALSE;
    pMepInfo->CcInfo.b1UnExpectedLevelDefect = ECFM_FALSE;

    /* Set AIS and LCK condition to false */
    pMepInfo->AisInfo.b1AisCondition = ECFM_FALSE;
    pMepInfo->LckInfo.b1LckCondition = ECFM_FALSE;

    /* Enable Y1731 state machines for Y.1731 */
    EcfmCcUtilNotifySm (pMepInfo, ECFM_IND_MODULE_ENABLE);
    EcfmCcUtilNotifyY1731 (pMepInfo, ECFM_IND_MODULE_ENABLE);

    return;
}

/*****************************************************************************
 *    Function Name        : EcfmCcUtilY1731DisableMep 
 *
 *    Description          : This is the function will disable the Y1731 on a 
 *                           given Mep 
 *
 *    Input(s)             : pMepInfo - Mep on which Y1731 should be disabled 
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcUtilY1731DisableMep (tEcfmCcMepInfo * pMepInfo)
{
    EcfmCcUtilNotifySm (pMepInfo, ECFM_IND_MODULE_DISABLE);
    EcfmCcUtilNotifyY1731 (pMepInfo, ECFM_IND_MODULE_DISABLE);

    /* Set the Default Values according to 802.1ag */
    /* Set CC enabled status as the one in the MA */
    pMepInfo->CcInfo.b1CciEnabled = pMepInfo->pMaInfo->b1CciEnabled;
    pMepInfo->CcInfo.b1UnExpectedPeriodDefect = ECFM_FALSE;
    pMepInfo->CcInfo.b1UnExpectedMepDefect = ECFM_FALSE;
    pMepInfo->CcInfo.b1MisMergeDefect = ECFM_FALSE;
    pMepInfo->CcInfo.b1UnExpectedLevelDefect = ECFM_FALSE;

    /* Set AIS and LCK condition to false */
    pMepInfo->AisInfo.b1AisCondition = ECFM_FALSE;
    pMepInfo->LckInfo.b1LckCondition = ECFM_FALSE;

    return;
}

/*****************************************************************************
 *    Function Name        : EcfmCcUtilModuleEnableForAMep
 *
 *    Description          : This function is called to enable ECFM module 
 *                           on a given Mep 
 *
 *    Input(s)             : pMepInfo - Mep on which ECFM module to be enabled
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/

PUBLIC VOID
EcfmCcUtilModuleEnableForAMep (tEcfmCcMepInfo * pMepInfo)
{
    /* Update port state value for this Mep */
    EcfmCcUtilUpdatePortState (pMepInfo, ECFM_INIT_VAL);
    /* Sending Indication to all SM for both the tasks */
    EcfmCcUtilNotifySm (pMepInfo, ECFM_IND_MODULE_ENABLE);

    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcUtilModuleEnableForAMep:"
                      "State Machine initialized for Mep %u on port %u"
                      "\r\n", pMepInfo->u2MepId, pMepInfo->u4IfIndex);

    return;
}

/*****************************************************************************
 *    Function Name        : EcfmCcUtilModuleDisableForAMep 
 *
 *    Description          : This function is called to disable ECFM module 
 *                           on a given Mep 
 *
 *    Input(s)             : pMepInfo - Mep on which ECFM module to be disabled.
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcUtilModuleDisableForAMep (tEcfmCcMepInfo * pMepInfo)
{

    EcfmCcUtilNotifySm (pMepInfo, ECFM_IND_MODULE_DISABLE);
    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcUtilModuleDisableForAMep: SM Reverted"
                      "back for Mep %u on port %u\r\n",
                      pMepInfo->u2MepId, pMepInfo->u4IfIndex);
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmClearCcContextStats  
 *
 *    Description          : This function is called to clear the Specific 
 *                           Statistics on a given Context 
 *
 *    Input(s)             : u4CtxId - ContextId
 *                           u1Statstype - Type of Statistics object to clear
 *
 *    Output(s)            : None
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmClearCcContextStats (UINT4 u4CtxId, UINT1 u1Statstype)
{
    if (ECFM_CC_SELECT_CONTEXT (u4CtxId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (u4CtxId))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC, "\tECFM module is Shutdown\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    switch (u1Statstype)
    {
        case ECFM_CTX_AIS_OUT:
            gpEcfmCcContextInfo->CcStats.u4TxAisCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_AIS_IN:
            gpEcfmCcContextInfo->CcStats.u4RxAisCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LCK_OUT:
            gpEcfmCcContextInfo->CcStats.u4TxLckCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LCK_IN:
            gpEcfmCcContextInfo->CcStats.u4RxLckCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LMM_OUT:
            gpEcfmCcContextInfo->CcStats.u4TxLmmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LMM_IN:
            gpEcfmCcContextInfo->CcStats.u4RxLmmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LMR_OUT:
            gpEcfmCcContextInfo->CcStats.u4TxLmrPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LMR_IN:
            gpEcfmCcContextInfo->CcStats.u4RxLmrPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_CFM_OUT:
            gpEcfmCcContextInfo->CcStats.u4TxCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_CFM_IN:
            gpEcfmCcContextInfo->CcStats.u4RxCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_CCM_OUT:
            gpEcfmCcContextInfo->CcStats.u4TxCcmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_CCM_IN:
            gpEcfmCcContextInfo->CcStats.u4RxCcmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_FAILED_COUNT:
            gpEcfmCcContextInfo->CcStats.u4TxFailedCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_BAD_PDU:
            gpEcfmCcContextInfo->CcStats.u4RxBadCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_FWD_PDU:
            gpEcfmCcContextInfo->CcStats.u4FrwdCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DSRD_PDU:
            gpEcfmCcContextInfo->CcStats.u4DsrdCfmPduCount = ECFM_INIT_VAL;
            break;
        default:
            break;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : EcfmGetCcContextStats 
 *
 *    Description          : This function is called to get the Specific 
 *                           Statistics on a given Context 
 *
 *    Input(s)             : u4CtxId - ContextId
 *                           u1Statstype - Type of Statistics object to get.
 *
 *    Output(s)            : pu4Value - Pointer to Statistics Value 
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmGetCcContextStats (UINT4 u4CtxId, UINT4 *pu4Value, UINT1 u1Statstype)
{
    if (ECFM_CC_SELECT_CONTEXT (u4CtxId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (u4CtxId))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC, "\tECFM module is Shutdown\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_SUCCESS;
    }

    switch (u1Statstype)
    {
        case ECFM_CTX_AIS_IN:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxAisCount;
            break;
        case ECFM_CTX_AIS_OUT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxAisCount;
            break;
        case ECFM_CTX_LCK_IN:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxLckCount;
            break;
        case ECFM_CTX_LCK_OUT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxLckCount;
            break;
        case ECFM_CTX_LMM_IN:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxLmmPduCount;
            break;
        case ECFM_CTX_LMM_OUT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxLmmPduCount;
            break;
        case ECFM_CTX_LMR_IN:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxLmrPduCount;
            break;
        case ECFM_CTX_LMR_OUT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxLmrPduCount;
            break;
        case ECFM_CTX_CFM_IN:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxCfmPduCount;
            break;
        case ECFM_CTX_CFM_OUT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxCfmPduCount;
            break;
        case ECFM_CTX_CCM_IN:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxCcmCount;
            break;
        case ECFM_CTX_CCM_OUT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxCcmCount;
            break;
        case ECFM_CTX_FAILED_COUNT:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4TxFailedCount;
            break;
        case ECFM_CTX_BAD_PDU:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4RxBadCfmPduCount;
            break;
        case ECFM_CTX_FWD_PDU:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4FrwdCfmPduCount;
            break;
        case ECFM_CTX_DSRD_PDU:
            *pu4Value = gpEcfmCcContextInfo->CcStats.u4DsrdCfmPduCount;
            break;
        default:
            break;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : EcfmTestCcContextStats 
 *
 *    Description          : This function is called to validate the specific 
 *                           Statistics value to be set.
 *
 *    Input(s)             : u4CtxId - ContextId
 *                           u4Value - Statistics Value to Set 
 *
 *    Output(s)            : pu4ErrorCode - Pointer to Error Code 
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmTestCcContextStats (UINT4 *pu4ErrorCode, UINT4 u4CtxId, UINT4 u4Value)
{
    if (ECFM_CC_SELECT_CONTEXT (u4CtxId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (u4Value != ECFM_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC, "\tInvalid Value\n");
        ECFM_CC_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmccutl.c
 ****************************************************************************/
