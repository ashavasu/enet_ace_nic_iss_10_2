/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbif.c,v 1.29 2015/03/15 10:20:16 siva Exp $
 *
 * Description: This file contains the routine for 
 *              creating/deleting interface structure and change in
 *              the interface operational status notification from
 *              lower layer.for LB LT Task
 *******************************************************************/

#include "cfminc.h"

/* Prototypes for */
PRIVATE INT4 EcfmLbLtStackCmpInPort PROTO ((tRBElem * pS1, tRBElem * pS2));

/****************************************************************************
 * FUNCTION NAME    : EcfmLbLtIfCreateAllPorts
 *
 * DESCRIPTION      : This function creates all valid ports in ECFM
 *                    module for the LBLT Task.
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtIfCreateAllPorts ()
{

    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PrevPort = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    while (EcfmL2IwfGetNextValidPortForContext
           (ECFM_LBLT_CURR_CONTEXT_ID (), u2PrevPort, &u2PortNum,
            &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (EcfmLbLtIfHandleCreatePort (ECFM_LBLT_CURR_CONTEXT_ID (), u4IfIndex,
                                        u2PortNum) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                                "EcfmLbLtIfCreateAllPorts: Port %u Entry creation"
                                "FAILED\n", u2PortNum);
        }
        u2PrevPort = u2PortNum;
    }

    /* Create one Dummy Port (Last Local Port# in the context reserved) for all 
     * MPLS-TP Meps in the context to point.
     */
#ifdef MPLS_WANTED
    if (EcfmLbLtIfCreateMplsPort (ECFM_CC_CURR_CONTEXT_ID (),
                                  (CFA_MIN_IVR_IF_INDEX +
                                   ECFM_CC_CURR_CONTEXT_ID ()),
                                  (UINT2)ECFM_LBLT_MAX_PORT_INFO) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmCcIfCreateAllPorts: Dummy MPLS Port %u Entry creation"
                          "FAILED \r\n", ECFM_MAX_PORTS_PER_CONTEXT);
    }
#endif
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtDeleteAllPorts
 * 
 * DESCRIPTION      : Function for deleting all Ports and clearing PortInfo
 *                    structure for LBLT Task
 *
 * INPUT            : None    
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtDeleteAllPorts ()
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;

    /* Deleting All the Ports */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_LBLT_MAX_PORT_INFO; u2LocalPortNum++)
    {
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPortNum);
        if (pPortInfo != NULL)
        {
            EcfmLbLtIfHandleDeletePort (u2LocalPortNum);
        }
    }
    return;
}

/****************************************************************************
 * FUNCTION NAME    : EcfmLbLtIfHandleCreatePort
 *
 * DESCRIPTION      : This function allocates mempools, updates the port info
 *                    in ECFM  module for LBLT Task.
 * 
 * INPUT            : u4ContextId - Context Identifier
 *                    u4IfIndex - IfIndex of the Port to be created
 *                    u4LocalPort - Local Port Number
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtIfHandleCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2
                            u2LocalPort)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    tEcfmCfaIfInfo      CfaIfInfo;
    UINT4               u4PhyIfIndex = ECFM_INIT_VAL;
    UINT4               u4TempCxtId = 0;
    UINT2               u2PortChannel = 0;
    UINT2               u2ChannelPortNum = 0;
    UINT1               u1InterfaceType = 0;

    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_LBLT_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtIfHandleCreatePort:"
                        "Creating Port %u \r\n", u2LocalPort);

    /* Check if the Port Entry already exists */
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    if (pPortInfo != NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtIfHandleCreatePort:"
                       "Port Already exits \r\n");
        return ECFM_SUCCESS;
    }
    if ((EcfmUtilGetIfInfo (u4IfIndex, &CfaIfInfo)) != CFA_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtIfHandleCreatePort: Getting Interface Info from "
                       "CFA FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Allocate memory for the new port entry */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_PORT_INFO (pPortInfo) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLbLtIfHandleCreatePort: Memory Allocation for the Port Entry FAILED"
                       "\r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pPortInfo, ECFM_INIT_VAL, ECFM_LBLT_PORT_INFO_SIZE);

    /* Creating Stack Tree for the Port Created */
    pPortInfo->StackInfoTree = EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                                         (tEcfmLbLtStackInfo,
                                                          StackPortInfoNode),
                                                         EcfmLbLtStackCmpInPort);
    if (pPortInfo->StackInfoTree == NULL)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC, "EcfmLbLtIfHandleCreatePort:"
                       "Stack Tree creation failed \r\n");

        /* Free the MEM Block allocated for the Port Entry */

        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortInfo);
        return ECFM_FAILURE;
    }

    /* Update the interface specific paramaters obtained from lower layer. */
    /* Interface Index of the Port */
    pPortInfo->u4IfIndex = u4IfIndex;

    /* Interface Operational Status of the Port */
    pPortInfo->u1IfOperStatus = CfaIfInfo.u1IfOperStatus;

    /* Interface Type - Ethernet or LAGG */
    pPortInfo->u1IfType = CfaIfInfo.u1IfType;

    /*Store the Maximum Throughput value supported by interface */
    pPortInfo->u4MaxBps = CfaIfInfo.u4IfSpeed;

    /* Port's Module Status */
    pPortInfo->u1PortEcfmStatus = ECFM_MODULE_STATUS (u4ContextId);
    pPortInfo->u1PortY1731Status = ECFM_Y1731_STATUS (u4ContextId);

    /* Ports LLC Encapsulation Status i.e. frames transmitted from this port
     * includes LLC SNAP Header or not. By Default - FALSE */
    pPortInfo->b1LLCEncapStatus = ECFM_FALSE;
    pPortInfo->u2PortNum = u2LocalPort;
    pPortInfo->u4ContextId = u4ContextId;

    pPortInfo->u1PortIdSubType = ECFM_PORTID_SUB_IF_ALIAS;
    EcfmCfaGetInterfaceNameFromIndex (u4IfIndex, pPortInfo->au1PortId);
    if (CfaIfInfo.u1BrgPortType == L2IWF_CNP_STAGGED_PORT)
    {
        if (EcfmL2IwfGetInterfaceType (u4ContextId, &u1InterfaceType) ==
            L2IWF_FAILURE)
        {
            /* Free the memory block assigned */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortInfo);
            return ECFM_FAILURE;
        }
        if (u1InterfaceType == L2IWF_C_INTERFACE_TYPE)
        {
            CfaIfInfo.u1BrgPortType = L2IWF_CNP_CTAGGED_PORT;
        }
    }
    pPortInfo->u1PortType = CfaIfInfo.u1BrgPortType;

    /* If this port is part of the port channel update the port channel
     * number in port info */

    if (EcfmL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        /* Get the Global portChannel number from Global interface index */
        if (EcfmL2IwfGetPortChannelForPort (u4IfIndex, &u2PortChannel)
            != L2IWF_SUCCESS)
        {
            ECFM_LBLT_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                                ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                "LbLtIfHandleCreatePort: Port Channel retrieval"
                                "failed for port %u\n", u4IfIndex);
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortInfo);
            return ECFM_FAILURE;
        }

        /* Get the Local portChannel number from Global PortChannel Number */
        if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u2PortChannel, &u4TempCxtId,
                                                &u2ChannelPortNum)
            != VCM_SUCCESS)
        {
            ECFM_LBLT_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                                ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                "LbLtIfHandleCreatePort: Getting the locali "
                                "Port Channel failed for port %u\n", u4IfIndex);
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortInfo);
            return ECFM_FAILURE;
        }

        pPortInfo->u2ChannelPortNum = u2ChannelPortNum;
    }

    if ((u4IfIndex >= ECFM_MIN_SISP_INDEX) &&
        (u4IfIndex <= ECFM_MAX_SISP_INDEX))
    {
        /* The ifindex range falls under SISP logical interface index.
         * Hence, query VCM-SISP module to obtain the physical interface mapped
         * to this logical interface
         * */
        if (EcfmVcmSispGetPhysicalPortOfSispPort (u4IfIndex, &u4PhyIfIndex)
            != VCM_SUCCESS)
        {
            ECFM_LBLT_TRC_ARG1 (ECFM_INIT_SHUT_TRC | ECFM_OS_RESOURCE_TRC |
                                ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                "LbLtIfHandleCreatePort: Physical port retrieval from VCM-SISP failed for port %u\n",
                                u4IfIndex);
            /* Free the memory block assigned */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortInfo);

            return ECFM_FAILURE;
        }

        pPortInfo->u4PhyPortNum = u4PhyIfIndex;
    }
    else
    {
        pPortInfo->u4PhyPortNum = u4IfIndex;
    }
    /* Add the port entry to the LBLTs Port Info */
    ECFM_LBLT_SET_PORT_INFO (u2LocalPort, pPortInfo);

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtIfCreateMplsPort 
 *
 *    DESCRIPTION      : This function is used to create a dummy Port interface
 *                       which will be used for MPLS Networks. 
 *
 *    INPUT            : u4ContextId - Context Id 
 *                       u4IfIndex   - IfIndex
 *                       u2LocalPort - Local Port Number 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Returns ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtIfCreateMplsPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPort)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_LBLT_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtIfHandleCreatePort:"
                        "Creating Port %u \r\n", u2LocalPort);

    /* Check if the Port Entry already exists */
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    if (pPortInfo != NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtIfHandleCreatePort:"
                       "Port Already exits \r\n");
        return ECFM_SUCCESS;
    }

    /* Allocate memory for the new port entry */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_PORT_INFO (pPortInfo) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLbLtIfHandleCreatePort: Memory Allocation for the "
                       "Port Entry FAILED\r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pPortInfo, ECFM_INIT_VAL, ECFM_LBLT_PORT_INFO_SIZE);

    /* No If Stack exists for MPLS-TP based MEPs */
    pPortInfo->StackInfoTree = NULL;

    /* Interface Index of the Port */
    pPortInfo->u4IfIndex = u4IfIndex;

    /* Interface Operational Status of the Port */
    pPortInfo->u1IfOperStatus = UP;

    /* Interface Type - Dummy MPLS port */
    pPortInfo->u1IfType = CFA_MPLS;

    /*Store the Maximum Throughput value supported by interface */
    pPortInfo->u4MaxBps = 0;

    /* Port's Module Status */
    pPortInfo->u1PortEcfmStatus = ECFM_MODULE_STATUS (u4ContextId);
    pPortInfo->u1PortY1731Status = ECFM_ENABLE;

    pPortInfo->b1LLCEncapStatus = ECFM_FALSE;
    pPortInfo->u2PortNum = u2LocalPort;
    pPortInfo->u4ContextId = u4ContextId;

    MEMCPY (pPortInfo->au1PortId, "MPLSPORT", STRLEN ("MPLSPORT"));

    pPortInfo->u1PortType = ECFM_MPLSTP_DEFAULT_PORT_TYPE;
    pPortInfo->u1PortIdSubType = ECFM_MPLSTP_PORTID_SUB_IF_ALIAS;

    pPortInfo->u4PhyPortNum = u4IfIndex;

    /* Add the port entry to the LBLTs Port Info */
    ECFM_LBLT_SET_PORT_INFO (u2LocalPort, pPortInfo);

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtDeleteMpFromPort
 *
 * DESCRIPTION      : This function Deletes the stack nodes and MIP
 *                    corresponding to a IfIndex.
 * 
 * INPUT            : UINT2 u2PortNum - port for which MP node is to be
 *                    deleted. 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None. 
 *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtDeleteMpFromPort (UINT2 u2PortNum)
{
    tEcfmLbLtMipInfo    MipNode;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    tEcfmLbLtMipInfo   *pMipNextNode = NULL;
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtMepInfo   *pMepNextNode = NULL;

    ECFM_MEMSET (&MipNode, 0x00, sizeof (tEcfmLbLtMipInfo));
    ECFM_MEMSET (gpEcfmLbLtMepNode, 0x00, sizeof (tEcfmLbLtMepInfo));

    gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
    pMepNode =
        (tEcfmLbLtMepInfo *) RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                            gpEcfmLbLtMepNode, NULL);
    while ((pMepNode != NULL) && (pMepNode->u2PortNum == u2PortNum))
    {
        pMepNextNode = RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                      (tRBElem *) pMepNode, NULL);
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtDeleteMpFromPort: Reverting back State"
                       "Machine \r\n");
        EcfmLbLtUtilNotifySm (pMepNode, ECFM_IND_IF_DELETE);
        EcfmLbLtUtilNotifyY1731 (pMepNode, ECFM_IND_IF_DELETE);

        /* Release memory to Lbm Data TLV if allocated */
        if (pMepNode->LbInfo.TxLbmDataTlv.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_DATA_TLV_POOL,
                                 pMepNode->LbInfo.TxLbmDataTlv.pu1Octets);
            pMepNode->LbInfo.TxLbmDataTlv.pu1Octets = NULL;
        }

        /*Remove other references of this node */
        RBTreeRem (ECFM_LBLT_PORT_MEP_TABLE, (tRBElem *) pMepNode);
        /* from Global Info */
        RBTreeRem (ECFM_LBLT_MEP_TABLE, (tRBElem *) pMepNode);

        pMepNode->pPortInfo = NULL;

        /* Releasing mem block allocated to MEP */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_TABLE_POOL, (UINT1 *) (pMepNode));
        /*Get next Node */
        pMepNode = pMepNextNode;
    }
    MipNode.u2PortNum = u2PortNum;
    /* Remove node corresponding to PortNum from Mip Table */
    pMipNode = RBTreeGetNext (ECFM_LBLT_MIP_TABLE, &MipNode, NULL);
    while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
    {
        pMipNextNode = RBTreeGetNext (ECFM_LBLT_MIP_TABLE, pMipNode, NULL);
        /* Remove MIP node from Mip Table */
        RBTreeRem (ECFM_LBLT_MIP_TABLE, (tRBElem *) pMipNode);

        /* Release memory allocated to MIP node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_TABLE_POOL, (UINT1 *) (pMipNode));
        pMipNode = pMipNextNode;
    }
    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtIfHandleDeletePort
 *
 * DESCRIPTION      : This function deletes the Port on getting an indication
 *                    from the lower layer.
 * 
 * INPUT            : UINT2 u2PortNum - Port to be Deleted 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtIfHandleDeletePort (UINT2 u2PortNum)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);

    /* If Port Entry exists and any MEP is configured on it, Notify all its SM
     * with MEP_NOT_ACTIVE to reset the SM to DEFAULT State */
    if (pPortInfo == NULL)
    {
        return;
    }

    /* If not MPLS Dummy Port */
    if (u2PortNum != ECFM_LBLT_MAX_PORT_INFO)
    {
        EcfmLbLtDeleteMpFromPort (u2PortNum);
        /* Deleting MEP Tree in a Port */
        RBTreeDestroy (pPortInfo->StackInfoTree,
                       (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                       ECFM_LBLT_STACK_ENTRY_IN_PORT);
    }

    /* Freeing MEM Block for the Port and assiging NULL */
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortInfo);
    pPortInfo = NULL;
    ECFM_LBLT_SET_PORT_INFO (u2PortNum, NULL);
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtIfOperChg
 *
 * DESCRIPTION      : This function changes the operational status of the Port.
 * 
 * INPUT            : u2PortNum - IfIndex of the Port whose state needs to be 
 *                                updated
 *                    u1PortState  - New State of the Port.  
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtIfHandlePortOperChg (UINT2 u2PortNum, UINT1 u1PortState)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    /* Check if the Port Entry already exists */
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);

    if (pPortInfo == NULL)
    {
        ECFM_LBLT_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "EcfmLbLtIfHandlePortOperChg:"
                            "Port %u Doesn't Exists \r\n", u2PortNum);
        return ECFM_FAILURE;
    }

    /* Update Port State in the LBLTs PortInfo */
    pPortInfo->u1IfOperStatus = u1PortState;
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : EcfmLbLtIfHandleIntfTypeChg
 *
 * DESCRIPTION      : This function allocates mempools, updates the port info
 *                    in ECFM  module for LBLT Task.
 * 
 * INPUT            : u4ContextId - Context Identifier
 *                    u4IfIndex - IfIndex of the Port to be created
 *                    u4LocalPort - Local Port Number
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtIfHandleIntfTypeChg (UINT1 u1IntfType)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT2               u2Port;
    UINT1               u1PortType;
    UINT1               u1NewPortType;
    ECFM_LBLT_TRC_FN_ENTRY ();

    if (u1IntfType == L2IWF_C_INTERFACE_TYPE)
    {
        u1PortType = L2IWF_CNP_STAGGED_PORT;
        u1NewPortType = L2IWF_CNP_CTAGGED_PORT;
    }
    else
    {
        u1PortType = L2IWF_CNP_CTAGGED_PORT;
        u1NewPortType = L2IWF_CNP_STAGGED_PORT;
    }
    for (u2Port = ECFM_PORTS_PER_CONTEXT_MIN;
         u2Port <= ECFM_LBLT_MAX_PORT_INFO; u2Port++)
    {
        /* Check if the Port Entry already exists */
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2Port);
        if (pPortInfo != NULL)
        {
            if (pPortInfo->u1PortType == u1PortType)
            {
                pPortInfo->u1PortType = u1NewPortType;
            }
            pPortInfo = NULL;
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 * FUNCTION NAME    : EcfmLbLtStackCmpInPort
 *
 * DESCRIPTION      : Compare function for creating RBTree Embedded. 
 * 
 * INPUT            : *pS1 - pointer to RB Node
 *                    *pS2 - Pointer to RB Node 
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : -1 represents S1 is less than S2
 *                     1 represents S1 is greater than S2
 *                     0 represents S1 is equals to S2    
 * 
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtStackCmpInPort (tRBElem * pS1, tRBElem * pS2)
{
    tEcfmLbLtStackInfo *pEcfmEntryA = NULL;
    tEcfmLbLtStackInfo *pEcfmEntryB = NULL;

    pEcfmEntryA = (tEcfmLbLtStackInfo *) pS1;
    pEcfmEntryB = (tEcfmLbLtStackInfo *) pS2;

    /* First Index - MEPs MD Level */
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)
    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    /* Second Index - MEPs Primary VID */
    if (pEcfmEntryA->u4VlanIdIsid != pEcfmEntryB->u4VlanIdIsid)
    {
        if (pEcfmEntryA->u4VlanIdIsid < pEcfmEntryB->u4VlanIdIsid)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    /* Third Index - MEPs Direction */
    if (pEcfmEntryA->u1Direction != pEcfmEntryB->u1Direction)
    {
        if (pEcfmEntryA->u1Direction < pEcfmEntryB->u1Direction)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

    return 0;
}

/****************************************************************************
  End of File cfmlbif.c
 ****************************************************************************/
