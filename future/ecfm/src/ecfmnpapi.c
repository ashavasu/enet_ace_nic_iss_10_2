/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmnpapi.c,v 1.7 2016/02/08 10:30:19 siva Exp $
 *
 * Description: This file contains the NP wrappers for ECFM module.
 *****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmClearRcvdLbrCounter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmClearRcvdLbrCounter
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmClearRcvdLbrCounter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __ECFM_NP_API_C
#define __ECFM_NP_API_C

#include "nputil.h"
#include "cfminc.h"

UINT1
EcfmFsMiEcfmClearRcvdLbrCounter (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmClearRcvdLbrCounter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdLbrCounter;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmClearRcvdTstCounter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmClearRcvdTstCounter
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmClearRcvdTstCounter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmClearRcvdTstCounter (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmClearRcvdTstCounter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdTstCounter;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmGetRcvdLbrCounter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmGetRcvdLbrCounter
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmGetRcvdLbrCounter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmGetRcvdLbrCounter (tEcfmMepInfoParams * pEcfmMepInfoParams,
                               UINT4 *pu4LbrIn)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmGetRcvdLbrCounter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_GET_RCVD_LBR_COUNTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdLbrCounter;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;
    pEntry->pu4LbrIn = pu4LbrIn;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmGetRcvdTstCounter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmGetRcvdTstCounter
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmGetRcvdTstCounter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmGetRcvdTstCounter (tEcfmMepInfoParams * pEcfmMepInfoParams,
                               UINT4 *pu4TstIn)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmGetRcvdTstCounter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_GET_RCVD_TST_COUNTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdTstCounter;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;
    pEntry->pu4TstIn = pu4TstIn;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwCallNpApi                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwCallNpApi
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwCallNpApi
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwCallNpApi (UINT1 u1Type, tEcfmHwParams * pEcfmHwInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwCallNpApi *pEntry = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_CALL_NP_API,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwCallNpApi;

    pEntry->u1Type = u1Type;
    pEntry->pEcfmHwInfo = pEcfmHwInfo;
    if ((u1Type == ECFM_NP_MEP_CREATE) || 
        (u1Type == ECFM_NP_MEP_DELETE))
    {

        pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);
        while (pMepNode != NULL)
        {
            if ((pMepNode->u2MepId == 
                 pEcfmHwInfo->EcfmHwMepParams.u2MepId) &&
                (pMepNode->u1MdLevel == 
                 pEcfmHwInfo->EcfmHwMepParams.u1MdLevel) &&
                (pMepNode->u4PrimaryVidIsid == 
                 pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid))
            {
                pEcfmHwInfo->u1EcfmOffStatus = 
                    pMepNode->b1MepCcmOffloadStatus;

                MEMCPY (pEcfmHwInfo->au1HwHandler,
                        pMepNode->au1HwMepHandler, 
                        ECFM_HW_MEP_HANDLER_SIZE);
                break;
            }
            pMepNode = 
                RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
        }
    }
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    if ((pMepNode != NULL) && (u1Type == ECFM_NP_MEP_CREATE))
    {
        /* Store the MEP handler returned by the hardware */
        MEMCPY (pMepNode->au1HwMepHandler,
                pEcfmHwInfo->au1HwHandler, ECFM_HW_MEP_HANDLER_SIZE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwDeInit (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwDeInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwDeInit;

    pEntry->u4ContextId = u4ContextId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwGetCapability                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwGetCapability
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwGetCapability
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwGetCapability (UINT4 u4ContextId, UINT4 *pu4HwCapability)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwGetCapability *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_GET_CAPABILITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCapability;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pu4HwCapability = pu4HwCapability;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwGetCcmRxStatistics                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwGetCcmRxStatistics
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwGetCcmRxStatistics
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwGetCcmRxStatistics (UINT4 u4ContextId, UINT2 u2RxFilterId,
                                  tEcfmCcOffMepRxStats * pEcfmCcOffMepRxStats)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwGetCcmRxStatistics *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmRxStatistics;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2RxFilterId = u2RxFilterId;
    pEntry->pEcfmCcOffMepRxStats = pEcfmCcOffMepRxStats;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwGetCcmTxStatistics                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwGetCcmTxStatistics
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwGetCcmTxStatistics
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwGetCcmTxStatistics (UINT4 u4ContextId, UINT2 u2TxFilterId,
                                  tEcfmCcOffMepTxStats * pEcfmCcOffMepTxStats)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwGetCcmTxStatistics *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmTxStatistics;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2TxFilterId = u2TxFilterId;
    pEntry->pEcfmCcOffMepTxStats = pEcfmCcOffMepTxStats;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwGetPortCcStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwGetPortCcStats
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwGetPortCcStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwGetPortCcStats (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tEcfmCcOffPortStats * pEcfmCcOffPortStats)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwGetPortCcStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_GET_PORT_CC_STATS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetPortCcStats;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pEcfmCcOffPortStats = pEcfmCcOffPortStats;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwHandleIntQFailure                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwHandleIntQFailure
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwHandleIntQFailure
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwHandleIntQFailure (UINT4 u4ContextId,
                                 tEcfmCcOffRxHandleInfo *
                                 pEcfmCcOffRxHandleInfo, UINT2 *pu2RxHandle,
                                 BOOL1 * pb1More)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwHandleIntQFailure *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwHandleIntQFailure;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pEcfmCcOffRxHandleInfo = pEcfmCcOffRxHandleInfo;
    pEntry->pu2RxHandle = pu2RxHandle;
    pEntry->pb1More = pb1More;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwInit (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwInit;

    pEntry->u4ContextId = u4ContextId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwRegister                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwRegister
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwRegister
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwRegister (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwRegister *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_REGISTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwRegister;

    pEntry->u4ContextId = u4ContextId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmHwSetVlanEtherType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmHwSetVlanEtherType
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmHwSetVlanEtherType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmHwSetVlanEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2EtherTypeValue, UINT1 u1EtherType)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmHwSetVlanEtherType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwSetVlanEtherType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2EtherTypeValue = u2EtherTypeValue;
    pEntry->u1EtherType = u1EtherType;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmStartLbmTransaction                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmStartLbmTransaction
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmStartLbmTransaction
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmStartLbmTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams,
                                 tEcfmConfigLbmInfo * pEcfmConfigLbmInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmStartLbmTransaction *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_START_LBM_TRANSACTION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLbmTransaction;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;
    pEntry->pEcfmConfigLbmInfo = pEcfmConfigLbmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmStartTstTransaction                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmStartTstTransaction
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmStartTstTransaction
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmStartTstTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams,
                                 tEcfmConfigTstInfo * pEcfmConfigTstInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmStartTstTransaction *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_START_TST_TRANSACTION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartTstTransaction;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;
    pEntry->pEcfmConfigTstInfo = pEcfmConfigTstInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmStopLbmTransaction                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmStopLbmTransaction
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmStopLbmTransaction
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmStopLbmTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmStopLbmTransaction *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_STOP_LBM_TRANSACTION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLbmTransaction;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmStopTstTransaction                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmStopTstTransaction
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmStopTstTransaction
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmStopTstTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmStopTstTransaction *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_STOP_TST_TRANSACTION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopTstTransaction;

    pEntry->pEcfmMepInfoParams = pEcfmMepInfoParams;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmTransmit1Dm                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmTransmit1Dm
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmTransmit1Dm
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmTransmit1Dm (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DmPdu,
                         UINT2 u2PduLength, tVlanTag VlanTag, UINT1 u1Direction)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmTransmit1Dm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_TRANSMIT1_DM,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmit1Dm;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1DmPdu = pu1DmPdu;
    pEntry->u2PduLength = u2PduLength;
    pEntry->VlanTag = VlanTag;
    pEntry->u1Direction = u1Direction;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmTransmitDmm                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmTransmitDmm
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmTransmitDmm
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmTransmitDmm (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DmmPdu,
                         UINT2 u2PduLength, tVlanTag VlanTag, UINT1 u1Direction)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmTransmitDmm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_TRANSMIT_DMM,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmm;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1DmmPdu = pu1DmmPdu;
    pEntry->u2PduLength = u2PduLength;
    pEntry->VlanTag = VlanTag;
    pEntry->u1Direction = u1Direction;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmTransmitDmr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmTransmitDmr
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmTransmitDmr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmTransmitDmr (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DmrPdu,
                         UINT2 u2PduLength, tVlanTag VlanTag, UINT1 u1Direction)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmTransmitDmr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_TRANSMIT_DMR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmr;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1DmrPdu = pu1DmrPdu;
    pEntry->u2PduLength = u2PduLength;
    pEntry->VlanTag = VlanTag;
    pEntry->u1Direction = u1Direction;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmTransmitLmm                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmTransmitLmm
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmTransmitLmm
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmTransmitLmm (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1LmmPdu,
                         UINT2 u2PduLength, tVlanTagInfo VlanTag,
                         UINT1 u1Direction)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmTransmitLmm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_TRANSMIT_LMM,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmm;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1LmmPdu = pu1LmmPdu;
    pEntry->u2PduLength = u2PduLength;
    pEntry->VlanTag = VlanTag;
    pEntry->u1Direction = u1Direction;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmTransmitLmr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmTransmitLmr
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmTransmitLmr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmTransmitLmr (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1LmrPdu,
                         UINT2 u2PduLength, tVlanTagInfo * pVlanTag,
                         UINT1 u1Direction)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmTransmitLmr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_TRANSMIT_LMR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmr;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1LmrPdu = pu1LmrPdu;
    pEntry->u2PduLength = u2PduLength;
    pEntry->pVlanTag = pVlanTag;
    pEntry->u1Direction = u1Direction;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmStartLm                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmStartLm
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmStartLm
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmStartLm (tEcfmHwLmParams * pHwLmInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmStartLm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_START_LM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLm;

    pEntry->pHwLmInfo = pHwLmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmStopLm                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmStopLm
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmStopLm
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmStopLm (tEcfmHwLmParams * pHwLmInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmStopLm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_STOP_LM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLm;

    pEntry->pHwLmInfo = pHwLmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmMbsmHwCallNpApi                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmMbsmHwCallNpApi
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmMbsmHwCallNpApi
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmMbsmHwCallNpApi (UINT1 u1Type, tEcfmHwParams * pEcfmHwInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmMbsmHwCallNpApi *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_MBSM_HW_CALL_NP_API,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmMbsmHwCallNpApi;

    pEntry->u1Type = u1Type;
    pEntry->pEcfmHwInfo = pEcfmHwInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmFsMiEcfmMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiEcfmMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EcfmFsMiEcfmMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmMbsmNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmMbsmNpInitHw;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)

/***************************************************************************
 *
 *    Function Name       : EcfmFsMiEcfmSlaTest
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *
 *    Input(s)            : Arguments of FsMiEcfmSlaTest
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
INT4
EcfmFsMiEcfmSlaTest (tHwTestInfo * pHwTestInfo)

{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmSlaTest *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_SLA_TEST,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmSlaTest;

    pEntry->pHwTestInfo = pHwTestInfo;;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : EcfmFsMiEcfmAddHwLoopbackInfo
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmAddHwLoopbackInfo
 *
 *    Input(s)            : Arguments of FsMiEcfmAddHwLoopbackInfo
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
EcfmFsMiEcfmAddHwLoopbackInfo (tHwLoopbackInfo * pHwLoopbackInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmAddHwLoopbackInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_ADD_HW_LOOPBACK_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmAddHwLoopbackInfo;

    pEntry->pHwLoopbackInfo = pHwLoopbackInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : EcfmFsMiEcfmDelHwLoopbackInfo
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiEcfmDelHwLoopbackInfo
 *
 *    Input(s)            : Arguments of FsMiEcfmDelHwLoopbackInfo
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
EcfmFsMiEcfmDelHwLoopbackInfo (tHwLoopbackInfo * pHwLoopbackInfo)
{
    tFsHwNp             FsHwNp;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmNpWrFsMiEcfmDelHwLoopbackInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ECFM_MOD,    /* Module ID */
                         FS_MI_ECFM_DEL_HW_LOOPBACK_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEcfmNpModInfo = &(FsHwNp.EcfmNpModInfo);
    pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmDelHwLoopbackInfo;

    pEntry->pHwLoopbackInfo = pHwLoopbackInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
#endif /*__ECFM_NP_API_C */
