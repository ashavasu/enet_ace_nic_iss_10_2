/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbrx.c,v 1.54 2014/03/16 11:34:11 siva Exp $
 *
 * Description: This file contains the Functionality of the LBLT 
 *               Receiver of the Control Sub Module.
 *******************************************************************/

#include "cfminc.h"

/* Prototypes for the routines only used in this file */
PRIVATE INT4 EcfmLbLtCtrlRxParsePduHdrs PROTO ((tEcfmLbLtPduSmInfo *));
PRIVATE INT4
    EcfmLbLtCtrlRxForwardCfmPdu PROTO ((tEcfmLbLtPduSmInfo * pPduSmInfo));
PRIVATE BOOL1 EcfmLbLtCheckLockedCondition PROTO ((tEcfmLbLtPduSmInfo *));
/*****************************************************************************
 * Name               :  EcfmLbLtCtrlRxPkt
 *
 * Description        : Receives the incoming frame from lower layer and Frame 
 *                      filtering, validates the CFM-PDU Header and checks for 
 *                      Link aggregated to selects the appropriate port if packet 
 *                      received from port for level de multiplexing. 
 *
 * Input(s)           : pBuf - Pointer to the received buffer
 *                      u4IfIndex - Index of Interface on which CFM-PDU was 
 *                                  received.
 *                      u2PortNum - Logical Index of Interface on which CFM-PDU was 
 *                                  received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtCtrlRxPkt (tEcfmBufChainHeader * pBuf, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;    /* Pointer to Port specific info */
    tEcfmLbLtPduSmInfo  PduSmInfo;    /* To store information about the
                                     * MP and the so far parsed CFM-PDU
                                     */
    UINT4               u4ByteCount = ECFM_INIT_VAL;    /* No of bytes of PDU */
    UINT2               u2CurrentPortNum = ECFM_INIT_VAL;
    BOOL1               b1PortInChannel = ECFM_FALSE;
    ECFM_LBLT_TRC_FN_ENTRY ();
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);
    ECFM_LBLT_TRC_ARG2 (ECFM_DATA_PATH_TRC,
                        "EcfmLbLtCtrlRxPkt: ECFM PDU received on Port,IfIndex =[%d]"
                        "[%d]\r\n", u2PortNum, u4IfIndex);

    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4ByteCount,
                        "EcfmLbLtCtrlRxPkt: "
                        "Dumping CFMPDU received from lower layer...\r\n");

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);
    u2CurrentPortNum = u2PortNum;
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlRxPkt: "
                       "No Port Information available \r\n");
        return ECFM_FAILURE;
    }
    if (pPortInfo->u2ChannelPortNum != 0)
    {
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (pPortInfo->u2ChannelPortNum);
        if (pPortInfo != NULL)
        {
            b1PortInChannel = ECFM_TRUE;
        }
        else
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxPkt: "
                           "No Port Information available \r\n");
            return ECFM_FAILURE;
        }
        /* Routine to get Port Info */
    }
    PduSmInfo.pPortInfo = pPortInfo;
    PduSmInfo.pBuf = pBuf;
    PduSmInfo.pHeaderBuf = NULL;
    PduSmInfo.u1RxDirection = ECFM_MP_DIR_DOWN;
    PduSmInfo.u1NoFwd = OSIX_FALSE;
    if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        PduSmInfo.u1RxPbbPortType = ECFM_LBLT_GET_PORT_TYPE (u2CurrentPortNum);
    }
    /* Routine to get VlanTag,MdLevel,opcode,flag,FirstTLVOffset,IngressAction 
     * from the buffer 
     */
    if (EcfmLbLtCtrlRxParsePduHdrs (&PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlRxPkt: "
                       "EcfmLbLtCtrlRxParsePduHdrs returned Failure\r\n");
        ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT (u2PortNum);
        ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (PduSmInfo.pPortInfo->
                                                 u4ContextId);
        /* Clearing values of PduSmInfo structure */
        return ECFM_FAILURE;
    }
    if (PduSmInfo.u1NoFwd == OSIX_TRUE)
    {
        return ECFM_SUCCESS;
    }
    if (b1PortInChannel)
    {
        /* Set the port as the one on which we have received the packet */
        /* Routine to get Port Info */
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
        if (pPortInfo == NULL)
        {
            return ECFM_FAILURE;
        }
        PduSmInfo.pPortInfo = pPortInfo;
    }
    /* Every CFM-PDU must first be forwared to the unware MEP */
    /* Check if unware MEP is present on the port for the 
     * received untagged CFM-PDU*/
    if ((PduSmInfo.VlanClassificationInfo.OuterVlanTag.u1TagType ==
         ECFM_VLAN_UNTAGGED)
        && (ECFM_LBLT_UNAWARE_MEP_PRESENT (u2PortNum) == ECFM_TRUE))
    {
        /* only untagged CFM-PDU are processed by VLAN unaware MEPs */
        PduSmInfo.u4RxVlanIdIsId = 0;
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlRxPkt: "
                       "untagged cfm-pdu received on a port with VLAN unaware MEP\r\n");
    }
    else
    {
        /* check if the port is part of a port-channel */
        if (pPortInfo->u2ChannelPortNum != 0)
        {
            /* switch the port with port channel */
            pPortInfo = ECFM_LBLT_GET_PORT_INFO (pPortInfo->u2ChannelPortNum);
            if (pPortInfo == NULL)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxPkt:No Port Information available\r\n");
                return ECFM_FAILURE;
            }
            PduSmInfo.pPortInfo = pPortInfo;
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxPkt: "
                           "cfm-pdu received on a port which is a member of port channel\r\n");
        }
    }
    /* Selects the MP on the Port for which CFM-PDU is received and passes the
     * Parsed PDU to that MP for further processing 
     */
    if ((PduSmInfo.u1RxOpcode == ECFM_OPCODE_LTR) ||
        (PduSmInfo.u1RxOpcode == ECFM_OPCODE_LTM))
    {
        /*Send the PDU to Stand-by Node Also */
        EcfmRedSyncLbLtPdu (pBuf, u4IfIndex);
    }

    /* Increment the per port pdu counters */

    ECFM_LBLT_INCR_RX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->u2PortNum);
    ECFM_LBLT_INCR_CTX_RX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->u4ContextId);
    ECFM_LBLT_INCR_RX_COUNT (PduSmInfo.pPortInfo->u2PortNum,
                             PduSmInfo.u1RxOpcode);
    ECFM_LBLT_INCR_CTX_RX_COUNT (PduSmInfo.pPortInfo->u4ContextId,
                                 PduSmInfo.u1RxOpcode);
    ECFM_BUF_SET_OPCODE (pBuf, PduSmInfo.u1RxOpcode);
    if (EcfmLbLtCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlRxPkt: "
                       "EcfmLbLtCtrlRxLevelDeMux returned Failure\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmLbLtCtrlRxLevelDeMux
 *
 * Description        : This routine selects the MP on the Port using VLAN-ID 
 *                      and MDLevel received in the CFM-PDU from the Stack 
 *                      Table in UP or DOWN direction. 
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                      structure holds the information about the MP and the 
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtCtrlRxLevelDeMux (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtStackInfo *pStackInfo = NULL;    /* Pointer to Stack info */
    tEcfmLbLtMepInfo   *pMepInfo = NULL;    /* Pointer to Mep info */
    tEcfmLbLtMipInfo   *pMipInfo = NULL;    /* Pointer to Mep info */
    UINT2               u2LocalPort = ECFM_INIT_VAL;    /* Interface Index of the 
                                                           port */
    UINT1               u1MdLevel = ECFM_INIT_VAL;    /* Md level at which this MP
                                                       is configured */
    INT4                i4RetVal = ECFM_FAILURE;

    ECFM_LBLT_TRC_FN_ENTRY ();

    u2LocalPort = pPduSmInfo->pPortInfo->u2PortNum;

    /* If the Received packet is a MPLS-TP OAM Packet then
     * process the packet with OpcodeDemux level as Equal.
     */
    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        if (pPduSmInfo->u1RxMdLevel ==
            pPduSmInfo->pMepInfo->pMaInfo->pMdInfo->u1Level)
        {
            /* Call OpCode Demultiplexer to process the PDU */
            if (EcfmLbLtCtrlRxOpCodeDeMux (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxLevelDeMux: "
                               "EcfmLbLtCtrlRxOpCodeDeMux returned Failure\r\n");
                return ECFM_FAILURE;
            }
        }
        else
        {
            /* Level is not equal, hence drop the PDU */
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxLevelDeMux: Received Level in "
                           "the PDU is not equal. Hence drop the PDU..\r\n");
            return ECFM_FAILURE;
        }
        return ECFM_SUCCESS;
    }
    else
    {
        /* Forward the CFM-PDU as normal data-packet in case ECFM is disabled
         * on this port*/
        if (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (u2LocalPort) == ECFM_FALSE)
        {
            return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
        }

        /* Apply the forwarding rule of CBP and PIP */
        switch (pPduSmInfo->u1RxPbbPortType)
        {
            case ECFM_CUSTOMER_BACKBONE_PORT:

                /* For the CBP port we need to do DeMux for B-VID for PDU w/o 
                 * I-TAG and DeMux for ISID for PDU with I-TAG.
                 */
                if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
                    VLAN_TAGGED)
                {
                    /* for CBP port we only need to do DeMux for CFM-PDUs having the
                     * UCA-Bit as ON, else this PDU was intended for CN/PBN*/
                    if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                        u1UcaBitValue == OSIX_TRUE)
                    {
                        /* Overwrite the B-VID with ISID for I-Tagged CFM-PDUs */
                        pPduSmInfo->u4RxVlanIdIsId =
                            ECFM_ISID_TO_ISID_INTERNAL (pPduSmInfo->
                                                        PbbClassificationInfo.
                                                        InnerIsidTag.u4Isid);
                    }
                    else
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtCtrlRxLevelDeMux: "
                                       "EcfmLbLtCtrlRxOpCodeDeMux UCA-Bit is OFF, no need to DeMux at CBP\r\n");

                        /* Check if any lower Level MEP is locked */
                        if (EcfmLbLtCheckLockedCondition (pPduSmInfo) ==
                            ECFM_TRUE)
                        {
                            return ECFM_SUCCESS;
                        }

                        return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                    }
                }
                break;
            default:
                break;

        }
        /* Check if any lower Level MEP is locked */
        if (EcfmLbLtCheckLockedCondition (pPduSmInfo) == ECFM_TRUE)
        {
            return ECFM_SUCCESS;
        }
        /* This is function is used to get entry from Stack table */
        pStackInfo = EcfmLbLtUtilGetMp (u2LocalPort,
                                        pPduSmInfo->u1RxMdLevel,
                                        pPduSmInfo->u4RxVlanIdIsId,
                                        pPduSmInfo->u1RxDirection);
        /* If MP exists at same Level exists */
        if (pStackInfo != NULL)
        {
            pPduSmInfo->pStackInfo = pStackInfo;
            /* if Mep of same level exists */
            if (ECFM_LBLT_IS_MEP (pStackInfo))
            {
                /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                 * and the MD Level
                 */
                pMepInfo = pStackInfo->pLbLtMepInfo;

                /* Fill adress of MepInfo (i.e pMepInfo) in PduSmInfo */
                pPduSmInfo->pMepInfo = pMepInfo;
            }
            else
            {
                /* Get the MIP Info  from the Stack Info indexed by Vlan Id 
                 * and the MD Level
                 */
                pMipInfo = pStackInfo->pLbLtMipInfo;

                /* Fill adress of MipInfo (i.e pMipInfo) in PduSmInfo */
                pPduSmInfo->pMipInfo = pMipInfo;
            }
            /*Call OpCode Demultiplexer */
            if (EcfmLbLtCtrlRxOpCodeDeMux (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxLevelDeMux: EcfmLbLtCtrlRxOpCodeDeMux returned Failure\r\n");
                return ECFM_FAILURE;
            }
            i4RetVal = ECFM_SUCCESS;
        }
        else
        {
            if (pPduSmInfo->u1RxOpcode == ECFM_OPCODE_RAPS)
            {
                if (EcfmUtilGetMepInfoFromMaVlans (pPduSmInfo) == ECFM_SUCCESS)
                {
                    return ECFM_SUCCESS;
                }
                else if (pPduSmInfo->u1RxDirection == ECFM_MP_DIR_UP)
                {
                    /* If MEP is not present with same level,same direction and same VLAN,
                     * then this function EcfmLbLtCtrlRxForwardCfmPdu is called to get the egress
                     * port list and function will change the Direction as UP
                     * and start searching for the egress port list stack info.
                     * For ERPS,we need to check the egress port list MEP's
                     * as down and check  whether any down MEP present with the same level
                     * and vlan.
                     * If Yes, then process the PDU and notify to ERPS module.*/

                    pPduSmInfo->u1RxDirection =
                        ECFM_REVERSE_MEP_DIR (pPduSmInfo->u1RxDirection);
                    pStackInfo =
                        EcfmLbLtUtilGetMp (u2LocalPort, pPduSmInfo->u1RxMdLevel,
                                           pPduSmInfo->u4RxVlanIdIsId,
                                           pPduSmInfo->u1RxDirection);
                    /* If StackInfo for the local port is not NULL,
                     * Down MEP is present with same level,same vlan for Down-Mep
                     * so Process the PDU and notify to ERPS Module.

                     * If Stack Info for the local port is NULL when the
                     * Direction is in DOWN to transmit the packet to 
                     * Local port,then PDU received by ECFM might be from UP-MEP
                     * so get the egress port lists to forward the packets*/

                    if (pStackInfo == NULL)
                    {
                        /* If MEP is not present with same level or same direction or same VLAN,
                         * for Down-MEP, call the EcfmLbLtCtrlRxForwardCfmPdu function 
                         * to get the Egress port list by changing the Direction as UP.
                         * For ERPS,UP-MEP packets should be forwarded to egress ports.*/
                        pPduSmInfo->u1RxDirection =
                            ECFM_REVERSE_MEP_DIR (pPduSmInfo->u1RxDirection);
                        return (EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo));
                    }
                    pPduSmInfo->pStackInfo = pStackInfo;

                    /* If MEP of same level exists */
                    if (ECFM_LBLT_IS_MEP (pStackInfo))
                    {
                        /* Get the MEP Info from the Stack Info indexed by Vlan Id
                         *                           * and MD Level.
                         *                                                     */
                        pMepInfo = pStackInfo->pLbLtMepInfo;

                        /* Fill the adress of MepInfo (i.e pMepInfo) in PduSmInfo */
                        pPduSmInfo->pMepInfo = pMepInfo;
                    }
                    else
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtCtrlRxLevelDeMux: No MEP found with "
                                       "expected level and vlan\r\n");
                        return ECFM_FAILURE;
                    }

                    if (EcfmLbLtCtrlRxOpCodeDeMux (pPduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtCtrlRxLevelDeMux: EcfmLbLtCtrlRxOpCodeDeMux "
                                       "returned Failure\r\n");
                        return ECFM_FAILURE;
                    }
                    return ECFM_SUCCESS;
                }

            }

            /* Get next higher level MP if any from the port belongs to same Vid 
             * and same direction */
            for (u1MdLevel = pPduSmInfo->u1RxMdLevel + 1;
                 u1MdLevel <= ECFM_MD_LEVEL_MAX; u1MdLevel = u1MdLevel + 1)
            {
                pStackInfo =
                    EcfmLbLtUtilGetMp ((UINT4) (u2LocalPort), u1MdLevel,
                                       (UINT2) pPduSmInfo->u4RxVlanIdIsId,
                                       pPduSmInfo->u1RxDirection);
                /* If MP at Higer Level exists on Port */
                if (pStackInfo != NULL)
                {
                    if (ECFM_LBLT_IS_MEP (pStackInfo))
                    {
                        /* Discard the CRU-Buffer as we have now a MEP at higher level 
                         * then the level received in the CFM-PDU
                         */
                        return ECFM_FAILURE;
                    }
                    else
                    {
                        /* Forward the CFM-PDU in case a MHF is found at higher level */
                        break;
                    }
                }                /* end of if */
            }                    /* end of for Loop */
            /* No MP at the same level or at any higher level is found */
            /* check if level-demux happened for unaware MEP */
            if (pPduSmInfo->u4RxVlanIdIsId == 0)
            {
                /* switch to the classified PVID */
                pPduSmInfo->u4RxVlanIdIsId = pPduSmInfo->VlanClassificationInfo.
                    OuterVlanTag.u2VlanId;
                /* If receiving port is part of a port channel then we need to level de-mux
                 * again for the port channel, with the received vlan-id
                 */
                ECFM_LBLT_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                    "EcfmLbLtCtrlRxLevelDeMux: "
                                    "VLAN unware MEP not found with the level %d \r\n",
                                    pPduSmInfo->u1RxMdLevel);
                if (pPduSmInfo->pPortInfo->u2ChannelPortNum != 0)
                {
                    /* switch the port with port channel */
                    pPduSmInfo->pPortInfo =
                        ECFM_LBLT_GET_PORT_INFO (pPduSmInfo->pPortInfo->
                                                 u2ChannelPortNum);
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtCtrlRxLevelDeMux: "
                                   "switching port info to port channel's info \r\n");
                }
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxLevelDeMux: "
                               "level demux again ... \r\n");
                if (pPduSmInfo->u4RxVlanIdIsId != 0)
                {
                    i4RetVal = EcfmLbLtCtrlRxLevelDeMux (pPduSmInfo);
                }
                else
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtCtrlRxLevelDeMux: VLAN-ID as zero received\r\n");
                    i4RetVal = ECFM_FAILURE;
                }
            }
            /* Check if MEP for Opposite direction (opposite of which received in the
             * PDU) is configured on the port. In that case PDU has to be dropped as per
             * specification.
             */
            else if (EcfmLbLtUtilGetMepEntryFrmPort (pPduSmInfo->u1RxMdLevel,
                                                     pPduSmInfo->u4RxVlanIdIsId,
                                                     u2LocalPort,
                                                     ECFM_REVERSE_MEP_DIR
                                                     (pPduSmInfo->
                                                      u1RxDirection)) != NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmCcCtrlRxLevelDeMux: "
                               "CFM PDU is dropped as Same Level BUT Opposite direction"
                               "MEP as received in PDU exists on the port\r\n");
                /* Discard CFM PDU */
                return ECFM_SUCCESS;
            }
            else
            {
                /*Forward the CFM-PDU */
                i4RetVal = EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
        }
    }                            /* end of else - !CFA_MPLS */

    ECFM_LBLT_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Name               : EcfmLbLtCtrlRxOpCodeDeMux
 *
 * Description        : This routine is invoked whenever a MEP at the same level
 *                      as that of the CFM-PDU is found. It validate the OpCode 
 *                      received in the CFM-PDU for LBM, LBR, LTM and LTR only, 
 *                      this routine also calls the ECFM client routine to parse
 *                      the CFM-PDU.and calls the State machine of the Client.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtCtrlRxOpCodeDeMux (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmBufChainHeader *pBuf = NULL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2StripLen = ECFM_INIT_VAL;
    UINT1              *pu1EcfmPdu = NULL;
    BOOL1               bFwrdPdu = ECFM_FALSE;
    BOOL1               b1IsThPdu = ECFM_TRUE;

    ECFM_LBLT_TRC_FN_ENTRY ();

    if ((pPduSmInfo->u1RxOpcode == ECFM_OPCODE_LTR) ||
        (pPduSmInfo->u1RxOpcode == ECFM_OPCODE_LTM))
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Dont Drop LTM and LTR at ACTIVE Node \r\n");

    }
    else if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: NODE NOT ACTIVE- Dropping the packet \r\n");
        return i4RetVal;
    }
    /* PDU is Parsed and SM is called only if that MEP/MHF is in Active State */

    /* Check if Selector Type is LSP/PW and MEP is inactive return success */
    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        if (ECFM_LBLT_IS_MEP_ACTIVE (pPduSmInfo->pMepInfo) == ECFM_FALSE)
        {
            return i4RetVal;
        }
    }
    else
    {
        if (ECFM_LBLT_IS_MEP (pPduSmInfo->pStackInfo))
        {
            if (ECFM_LBLT_IS_MEP_ACTIVE (pPduSmInfo->pMepInfo) == ECFM_FALSE)
            {
                return i4RetVal;
            }
        }
        else
        {
            if (ECFM_LBLT_IS_MIP_ACTIVE (pPduSmInfo->pMipInfo) == ECFM_FALSE)
            {
                return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
        }
    }
    /* In case Y1731 Specific PDUs are received & Y1731 is disabled on the
     * receiving port then do not process further */
    switch (pPduSmInfo->u1RxOpcode)
    {
        case ECFM_OPCODE_DMM:
        case ECFM_OPCODE_DMR:
        case ECFM_OPCODE_1DM:
        case ECFM_OPCODE_TST:
        case ECFM_OPCODE_APS:
        case ECFM_OPCODE_RAPS:
        case ECFM_OPCODE_MCC:
        case ECFM_OPCODE_VSM:
        case ECFM_OPCODE_VSR:
        case ECFM_OPCODE_EXM:
        case ECFM_OPCODE_EXR:
            if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT
                (pPduSmInfo->pPortInfo->u2PortNum) == ECFM_TRUE)
            {
                return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            break;
        default:
            break;
    }

    pBuf = pPduSmInfo->pBuf;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /*Remove the ethernet header size from the pdu length */
    u4ByteCount = u4ByteCount - pPduSmInfo->u1CfmPduOffset;
    pu1EcfmPdu = ECFM_GET_DATA_PTR_IF_LINEAR (pBuf,
                                              pPduSmInfo->u1CfmPduOffset,
                                              u4ByteCount);
    if (pu1EcfmPdu == NULL)
    {
        pu1EcfmPdu = ECFM_LBLT_PDU;
        ECFM_MEMSET (pu1EcfmPdu, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
        if (ECFM_COPY_FROM_CRU_BUF (pBuf, pu1EcfmPdu,
                                    pPduSmInfo->u1CfmPduOffset, u4ByteCount)
            == ECFM_CRU_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxOpCodeDeMux:"
                           "Received BPDU Copy From CRU Buffer FAILED\r\n");
            i4RetVal = ECFM_FAILURE;
            return i4RetVal;
        }
    }

    if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
    {
        /* Incase we have the UCA bit as ON then we need to use the CDA and BDA
         * for CFM funtionality as the CFM-PDU was generated from the PBBN only*/

        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
            VLAN_TAGGED)
        {
            ECFM_MEMCPY (pPduSmInfo->RxSrcMacAddr,
                         pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                         CSAMacAddr, sizeof (tMacAddr));
            ECFM_MEMCPY (pPduSmInfo->RxDestMacAddr,
                         pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                         CDAMacAddr, sizeof (tMacAddr));
            /* Strip off the BDA, BSA, B-TAG and I-TAG if present, we will add
             * new BDA, BSA, B-TAG and I-TAG for the response PDU, ECFM must not
             * change anyting in the BDA, BSA, I-TAG for the B-TAG*/
            /*BDA + BSA */
            u2StripLen = 2 * ECFM_MAC_ADDR_LENGTH;
            /*ISID length + Ether Type */
            u2StripLen += PBB_ISID_TAG_PID_LEN;
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType =
                VLAN_UNTAGGED;
            if (pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType ==
                VLAN_TAGGED)
            {
                u2StripLen += ECFM_VLAN_HDR_SIZE;
                pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                    VLAN_UNTAGGED;
            }
            pPduSmInfo->pHeaderBuf = pPduSmInfo->pBuf;
            if (ECFM_FRAGMENT_CRU_BUF
                (pPduSmInfo->pHeaderBuf, u2StripLen,
                 &pPduSmInfo->pBuf) != ECFM_CRU_SUCCESS)
            {
                i4RetVal = ECFM_FAILURE;
                return i4RetVal;
            }
            pPduSmInfo->u1CfmPduOffset -= u2StripLen;
        }
    }

    switch (pPduSmInfo->u1RxOpcode)
    {
        case ECFM_OPCODE_LBR:
            if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
            {
                /*LBR is to be forwarded in case of MHF */
                if (ECFM_LBLT_IS_MHF (pPduSmInfo->pStackInfo))
                {
                    /*Forward the LBM */
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }
            }

            /* Routine to parse LBR PDU */
            if (EcfmLbLtClntParseLbr (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse LBR PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }

            /* Call the LB Initiator Rx State Machine */
            if (EcfmLbiRxProcessLbr (pPduSmInfo) != ECFM_SUCCESS)
            {
                /*As per IEEE 802.1ag standard LBR counter has to be */
                /*incremented for valid packets */
                /*So decrementing the RX counter */
                ECFM_LBLT_DECR_RX_COUNT (pPduSmInfo->pPortInfo->u2PortNum,
                                         pPduSmInfo->u1RxOpcode);
                ECFM_LBLT_DECR_CTX_RX_COUNT (pPduSmInfo->pPortInfo->u4ContextId,
                                             pPduSmInfo->u1RxOpcode);

                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               " Could not process the LBR Received\r\n");
                i4RetVal = ECFM_FAILURE;
            }
            break;

        case ECFM_OPCODE_LBM:
            /* Call the LBR Responder */
            if (EcfmLbResProcessLbm (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               " Could not process the LBM Received\r\n");
                /* If the LBM needs to be forwarded, in case of MHF when LBM is
                 * received with Dest-Mac Address not equal to the MHF's Mac
                 * Address
                 */
                if (bFwrdPdu == ECFM_TRUE)
                {
                    /*Forward the LBM */
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }

                i4RetVal = ECFM_FAILURE;
            }
            break;

        case ECFM_OPCODE_LTR:
            /*LTR is to be forwarded in case of MHF */
            if (ECFM_LBLT_IS_MHF (pPduSmInfo->pStackInfo))
            {
                /*Forward the LBM */
                return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            /* Routine to parse LTR PDU */
            if (EcfmLbLtClntParseLtr (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse LTR PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* Call the LT Initiator Rx State Machine */
            if (EcfmLtInitRxSmProcessLtr (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               "Could not Process the LTR received\r\n");
                i4RetVal = ECFM_FAILURE;
            }
            break;

        case ECFM_OPCODE_LTM:
            /* Routine to parse LTM PDU */
            if (EcfmLbLtClntParseLtm (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse LTM PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* Call the LTM Rx State Machine */
            if (EcfmLtmRxSmProcessLtm (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               "Couldnot process the LTM received\r\n");
                i4RetVal = ECFM_FAILURE;
            }
            break;
            /*Y.1731: DMM and DMR opcode types */
        case ECFM_OPCODE_DMM:
            /* Routine to parse DMM PDU */
            if (EcfmLbLtClntParseDmm (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse DMM PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* Call the DM Responder */
            if (EcfmLbltClntProcessDmm (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not process the DMM frame received\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                i4RetVal = ECFM_FAILURE;
            }
            break;
        case ECFM_OPCODE_DMR:
            if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
            {
                if (ECFM_LBLT_IS_MHF (pPduSmInfo->pStackInfo))
                {
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }
            }
            /* Routine to parse DMR PDU */
            if (EcfmLbLtClntParseDmr (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse DMr PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* Call the DM Initiator Receiver */
            if (EcfmLbltClntProcessDmr (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               "Could not process the DMR frame received\r\n");
                i4RetVal = ECFM_FAILURE;
            }
            break;

        case ECFM_OPCODE_1DM:
            /* Routine to parse 1DM PDU */
            if (EcfmLbLtClntParse1Dm (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse 1DM PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* Process the parsed 1DM PDU */
            if (EcfmLbltClntProcess1Dm (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               "Could not process the 1DM frame received\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                i4RetVal = ECFM_FAILURE;
            }
            break;

        case ECFM_OPCODE_TST:
            /* Routine to parse TST PDU */
            if (EcfmLbLtClntParseTst (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux: "
                               "Could not Parse TST PDU\r\n");
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* Process the TST PDU */
            if (EcfmLbltClntProcessTst (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               "Could not process the TST frame received\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
            }
            break;
        case ECFM_OPCODE_VSM:
            /* Routine to parse VSM PDU */
            if (EcfmLbLtClntThParseVsm (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                b1IsThPdu = ECFM_FALSE;
            }
            /* Call the TH Responder */
            if (b1IsThPdu == ECFM_TRUE)
            {
                if (EcfmLbltClntThProcessVsm (pPduSmInfo, &bFwrdPdu) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtCtrlRxOpCodeDeMux: "
                                   "Could not process the VSM frame received\r\n");
                    if (bFwrdPdu == ECFM_TRUE)
                    {
                        return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                    }
                    i4RetVal = ECFM_FAILURE;
                }
            }
            break;
        case ECFM_OPCODE_VSR:
            if (ECFM_LBLT_IS_MHF (pPduSmInfo->pStackInfo))
            {
                return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            /* Routine to parse VSR PDU */
            if (EcfmLbLtClntThParseVsr (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                b1IsThPdu = ECFM_FALSE;
            }
            /* Call the TH Initiator Receiver */
            if (b1IsThPdu == ECFM_TRUE)
            {
                if (EcfmLbltClntThProcessVsr (pPduSmInfo) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtCtrlRxOpCodeDeMux:"
                                   "Could not process the VSR frame received\r\n");
                    i4RetVal = ECFM_FAILURE;
                }
            }
            break;
        case ECFM_OPCODE_APS:
        case ECFM_OPCODE_RAPS:
        case ECFM_OPCODE_MCC:
        case ECFM_OPCODE_EXM:
        case ECFM_OPCODE_EXR:
            if (EcfmLbLtClntProcessExApi (pPduSmInfo, &bFwrdPdu) !=
                ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxOpCodeDeMux:"
                               "Could not process the Ext PDU received\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                         u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            /* When MIP receives the ERPS Packet from cfa it should 
             * forward that packet to EcfmLbLtCtrlRxForwardCfmPdu for
             * port filtering or frame filtering entity*/
            if ((pPduSmInfo->u1RxOpcode == ECFM_OPCODE_RAPS) &&
                (pPduSmInfo->pMepInfo == NULL))
            {
                return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            /* Routine to Fwd the rcvd PDU to the registered applications */
            EcfmLbLtClntFwdExPdu (pPduSmInfo, pu1EcfmPdu);
            break;
        case ECFM_OPCODE_CCM:
            break;
        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxOpCodeDeMux: " "Invalid OpCode\r\n");
            /*forwarded all other CFM-PDUs case of MHF */
            if (ECFM_LBLT_IS_MHF (pPduSmInfo->pStackInfo))
            {
                return EcfmLbLtCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            else
            {
                /* Discard the PDU */
                i4RetVal = ECFM_FAILURE;
            }
            break;
    }                            /* end of switch case */

    if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
    {
        /* Check if we did the buffer fragmentation */
        if (pPduSmInfo->pHeaderBuf != NULL)
        {
            /* Check what all tags were fragmented */
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType =
                VLAN_TAGGED;
            if (u2StripLen >
                ((2 * ECFM_MAC_ADDR_LENGTH) + PBB_ISID_TAG_PID_LEN))
            {
                pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                    VLAN_TAGGED;
            }
            /* Concat the fragmant with the main buffer */
            ECFM_CONCAT_CRU_BUF (pPduSmInfo->pHeaderBuf, pPduSmInfo->pBuf);
            pPduSmInfo->pBuf = pPduSmInfo->pHeaderBuf;
            pPduSmInfo->pHeaderBuf = NULL;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Name               : EcfmLbLtCtrlRxParsePduHdrs
 *
 * Description        : This is called to parse the received CFM PDU by 
 *                      extracting the Header values from the and places then 
 *                      into the tEcfmPduSmInfo Structure.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *******************************************************************************/
PRIVATE INT4
EcfmLbLtCtrlRxParsePduHdrs (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmVlanTag        VlanTag;    /* Struture containing VlanId, Priority, DE */
    tEcfmPbbTag         PbbTag;
    UINT2               u2LocalPort = ECFM_INIT_VAL;    /* Interface Index of the port */
    UINT4               u4Offset = ECFM_INIT_VAL;    /* Stores offset */
    UINT2               u2TypeLength = ECFM_INIT_VAL;    /* Store CFM PDU Type */
    UINT1               u1IngressAction = ECFM_INIT_VAL;    /* Stores Ingress Action */
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;    /* Stores first byte of CFM HDR */
    UINT1               u1IsSend = OSIX_FALSE;
    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PbbTag, ECFM_INIT_VAL, sizeof (tEcfmPbbTag));
    ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));

    u2LocalPort = pPduSmInfo->pPortInfo->u2PortNum;

    /* Copy the 6byte destination address from the recived PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxDestMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    /* Copy the 6byte Received source address from the LBM PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxSrcMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;
    if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        pPduSmInfo->u4ITagOffset = ECFM_INIT_VAL;

        if (EcfmL2IwfGetTagInfoFromFrame
            (ECFM_LBLT_CURR_CONTEXT_ID (),
             pPduSmInfo->pPortInfo->u4IfIndex,
             pPduSmInfo->pBuf, &VlanTag, &PbbTag,
             &u1IngressAction,
             &pPduSmInfo->u4ITagOffset,
             &u4Offset, L2_ECFM_PACKET, &u1IsSend) == L2IWF_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxParsePduHdrs: "
                           "Unable to get VLAN information of the frame\r\n");
            return ECFM_FAILURE;
        }
        if (u1IsSend == OSIX_TRUE)
        {
            if (EcfmL2IwfPbbTxFrameToIComp (ECFM_LBLT_CURR_CONTEXT_ID (),
                                            pPduSmInfo->pPortInfo->u4IfIndex,
                                            pPduSmInfo->pBuf, &VlanTag,
                                            &PbbTag,
                                            &pPduSmInfo->u4ITagOffset,
                                            L2_ECFM_PACKET,
                                            u2LocalPort) == L2IWF_FAILURE)
            {
                return L2IWF_FAILURE;
            }
            pPduSmInfo->u1NoFwd = OSIX_TRUE;
            return ECFM_SUCCESS;
        }
        pPduSmInfo->u4PduOffset = u4Offset;
        /* copy the Classified VLAN-Information in PDU-SM */
        ECFM_MEMCPY (&pPduSmInfo->VlanClassificationInfo, &VlanTag,
                     sizeof (tEcfmVlanTag));
        /* copy the Classified VLAN-Information in PDU-SM */
        ECFM_MEMCPY (&pPduSmInfo->PbbClassificationInfo, &PbbTag,
                     sizeof (tEcfmPbbTag));
    }
    else
    {
        /*Get the frame VLAN information */
        if (EcfmVlanGetVlanInfoFromFrame (ECFM_LBLT_CURR_CONTEXT_ID (),
                                          u2LocalPort,
                                          pPduSmInfo->pBuf, &VlanTag,
                                          &u1IngressAction,
                                          &u4Offset) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxParsePduHdrs: "
                           "Unable to get VLAN information of the frame\r\n");
            return ECFM_FAILURE;
        }
        /* Process for CEP */
        if (EcfmVlanProcessPktForCep (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->pBuf,
                                      &VlanTag) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxParsePduHdrs: "
                           "Unable to process the packet for CEP.\r\n");
        }
        /* copy the Classified VLAN-Information in PDU-SM */
        ECFM_MEMCPY (&pPduSmInfo->VlanClassificationInfo, &VlanTag,
                     sizeof (tEcfmVlanTag));
        /* pVlanTag->InnerVlanTag.u2VlanId check is for PB cases, where we have a
         * inner tage on C-VID and the outer-tag is S-VID untagged*/
    }
    if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        pPduSmInfo->u1RxPbbPortType = ECFM_LBLT_GET_PORT_TYPE (u2LocalPort);

        switch (pPduSmInfo->u1RxPbbPortType)
        {
            case ECFM_CNP_STAGGED_PORT:
            case ECFM_CNP_CTAGGED_PORT:
            case ECFM_CNP_PORTBASED_PORT:
                pPduSmInfo->u4RxVlanIdIsId = VlanTag.OuterVlanTag.u2VlanId;
                break;
            case ECFM_PROVIDER_NETWORK_PORT:
                pPduSmInfo->u4RxVlanIdIsId = PbbTag.OuterVlanTag.u2VlanId;
                break;
            case ECFM_PROVIDER_INSTANCE_PORT:
            case ECFM_CUSTOMER_BACKBONE_PORT:
                /* convertisid to ECFM internal isid */
                pPduSmInfo->u4RxVlanIdIsId =
                    ECFM_ISID_TO_ISID_INTERNAL (PbbTag.InnerIsidTag.u4Isid);
                break;
            default:
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlRxParsePduHdrs: "
                               "Unable to get VLAN information of the frame\r\n");
                return ECFM_FAILURE;

        }
    }
    else
    {
        pPduSmInfo->u4RxVlanIdIsId = VlanTag.OuterVlanTag.u2VlanId;
    }

    /* Get Type Length */
    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset, u2TypeLength);

    /* Move the pointer by the 2 byte to get the MD LEVEL */
    u4Offset = u4Offset + ECFM_VLAN_TYPE_LEN_FIELD_SIZE;

    /* Check if TypeLength value contains ECFM_PDU_TYPE_CFM type then LLC Snap  
     * Header is present then move offset by 8 bytes
     */
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)
    {
        u4Offset = u4Offset + ECFM_LLC_SNAP_HDR_SIZE;
    }

    pPduSmInfo->u1CfmPduOffset = (UINT1) u4Offset;
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1MdLvlAndVer);

    /* Read the 3MSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxMdLevel = (UINT1) (ECFM_GET_MDLEVEL (u1MdLvlAndVer));

    /* Read the 5LSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxVersion = (UINT1) (ECFM_GET_VERSION (u1MdLvlAndVer));
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flag from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset,
                         pPduSmInfo->u1RxFirstTlvOffset);

    /* If recvd packet DA is CFM group destination multicast then based on 
     * the opcode the level from the DestMac and the CFM header are compared
     * to check if both are in sync
     */
    if (ECFM_IS_GROUP_DMAC_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_TRUE)
    {
        if ((ECFM_IS_MULTICAST_CLASS1_ADDR
             (pPduSmInfo->RxDestMacAddr, pPduSmInfo->u1RxMdLevel) !=
             ECFM_TRUE) && (ECFM_IS_MULTICAST_CLASS2_ADDR
                            (pPduSmInfo->RxDestMacAddr,
                             pPduSmInfo->u1RxMdLevel) != ECFM_TRUE))

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCtrlRxParsePduHdrs: "
                           "Received wrong multicast class address\r\n");
            return ECFM_FAILURE;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtCtrlRxPktFree
 *
 * Description        : Free CRU Buffer memory
 *
 * Input(s)           : pBuf - pointer to CRU Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtCtrlRxPktFree (tEcfmBufChainHeader * pBuf)
{
    ECFM_LBLT_TRC_FN_ENTRY ();
    if (ECFM_RELEASE_CRU_BUF (pBuf, FALSE) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC, "EcfmLbLtCtrlRxPktFree: "
                       "Received frame CRU Buffer Release Failed\r\n");
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Name               : EcfmLbLtCtrlRxForwardCfmPdu
 *
 * Description        : This routine is used to forward the CFM-PDU to the port
 *                      or frame filtering entity, it also applies the 
 *                      port-filtering rules the CFM-PDU.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtCtrlRxForwardCfmPdu (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmMacAddr        PortMacAddr = { 0 };
    /* Check if the CFM-PDU was destined for this port if so, then discard the
     * the CFM-PDU*/
    /* Check if we did the buffer fragmentation */
    if (pPduSmInfo->pHeaderBuf != NULL)
    {
        /* Check what all tags were fragmented */
        pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType = VLAN_TAGGED;
        if (ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pHeaderBuf) >
            ((2 * ECFM_MAC_ADDR_LENGTH) + PBB_ISID_TAG_PID_LEN))
        {
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_TAGGED;
        }
        /* Concat the fragmant with the main buffer */
        ECFM_CONCAT_CRU_BUF (pPduSmInfo->pHeaderBuf, pPduSmInfo->pBuf);
        pPduSmInfo->pBuf = pPduSmInfo->pHeaderBuf;
        pPduSmInfo->pHeaderBuf = NULL;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (pPduSmInfo->pPortInfo->u4IfIndex, PortMacAddr);
    if (ECFM_COMPARE_MAC_ADDR (PortMacAddr, pPduSmInfo->RxDestMacAddr)
        == ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlRxForwardCfmPdu:"
                       "Port Mac-Address and CFM-PDU Dest Mac-Address Matched\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        /*Apply the Port-Filtering Rules */
        if (EcfmAHUtilChkPortFiltering (pPduSmInfo->pPortInfo->u4IfIndex,
                                        pPduSmInfo->pPortInfo->u1IfOperStatus,
                                        pPduSmInfo->u4RxVlanIdIsId,
                                        pPduSmInfo->u1RxPbbPortType,
                                        &pPduSmInfo->PbbClassificationInfo,
                                        pPduSmInfo->u1RxDirection) !=
            ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                "EcfmLbLtCtrlRxForwardCfmPdu:"
                                "CFM-PDU Filtered by EcfmAHUtilChkPortFiltering, Port =[%d],"
                                "VID=[%d]\r\n",
                                pPduSmInfo->pPortInfo->u2PortNum,
                                pPduSmInfo->u4RxVlanIdIsId);
            return ECFM_FAILURE;
        }
        if (pPduSmInfo->u1RxDirection == ECFM_MP_DIR_UP)
        {
            return EcfmLbLtAHCtrlTxFwdToPort (pPduSmInfo->pBuf,
                                              pPduSmInfo->pPortInfo->
                                              u2PortNum,
                                              &(pPduSmInfo->
                                                VlanClassificationInfo),
                                              &(pPduSmInfo->
                                                PbbClassificationInfo));
        }
        /*else if the PDU  is received from the port then forward it to frame 
         * filtering
         */
        else
        {
            ECFM_LBLT_INCR_FRWD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                               u2PortNum);
            return EcfmLbLtAHCtrlTxFwdToFf (pPduSmInfo);
        }
    }
    else
    {
        /*Apply the Port-Filtering Rules */
        if (EcfmLbLtUtilChkPortFiltering (pPduSmInfo->pPortInfo->u2PortNum,
                                          pPduSmInfo->VlanClassificationInfo.
                                          OuterVlanTag.u2VlanId) !=
            ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                "EcfmLbLtCtrlRxForwardCfmPdu:"
                                "CFM-PDU Filtered by EcfmLbLtUtilChkPortFiltering, Port =[%d],"
                                "VID=[%d]\r\n",
                                pPduSmInfo->pPortInfo->u2PortNum,
                                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.
                                u2VlanId);
            return ECFM_FAILURE;
        }
        if (pPduSmInfo->u1RxDirection == ECFM_MP_DIR_UP)
        {
            return EcfmLbLtADCtrlTxFwdToPort (pPduSmInfo->pBuf,
                                              pPduSmInfo->pPortInfo->u2PortNum,
                                              &(pPduSmInfo->
                                                VlanClassificationInfo));
        }
        /*else if the PDU is received from the port then forward it to frame filtering 
         */
        else
        {
            ECFM_LBLT_INCR_FRWD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                               u2PortNum);
            return EcfmLbLtADCtrlTxFwdToFf (pPduSmInfo);
        }
    }
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbLtClntProcessExApi
 *
 * Description        : This routine processes the received External API PDU
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwdExApi - Pointer to Boolean indicating whether the 
 *                      External API PDU needs to be forwarded in case the Receving entity 
 *                      is a MHF.
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntProcessExApi (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdExApi)
{
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmMacAddr        MpMacAddr = { 0 };

    ECFM_LBLT_TRC_FN_ENTRY ();
    pStackInfo = pPduSmInfo->pStackInfo;
    /* Get the mac-addres of the MP */
    if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtClntProcessExApi: "
                       "No Port Information available\r\n");
        return ECFM_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pStackInfo->u2PortNum)->u4IfIndex, MpMacAddr);

    /* Check that destination_address parameter contains neither MAC address of 
     * the receiving MP nor CCM gp address then no further processing of the
     * external API pdu is performed */
    if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                               MpMacAddr) != ECFM_SUCCESS)
    {
        /* R-APS Will be having different DestMac, so don't compare for
         * R-APS PDU */
        if (pPduSmInfo->u1RxOpcode != ECFM_OPCODE_RAPS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtClntProcessExApi: "
                           "Destination address of received External API not equal to "
                           "MEPs Mac Address, so MEP cannot process the External API PDU "
                           "further\r\n");

            /* MIP should forward the Ex API PDU if the MAC address doesnot match */
            if (ECFM_LBLT_IS_MHF (pStackInfo))
            {
                /* Set the Flag to indicate the Receiver to forward the External API */
                *pbFrwdExApi = ECFM_TRUE;
            }

            return ECFM_FAILURE;
        }
    }
    else
    {
        /* MIP should not Process the PDU destined to it */
        if (ECFM_LBLT_IS_MHF (pStackInfo))
        {

            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtClntProcessExApi: "
                           "MIP  can not process teh PDU External API PDU"
                           "destined to it\r\n");
            return ECFM_FAILURE;
        }
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmLbLtCheckLockedCondition
 *
 * Description        : This routine is used to check is any lower level MEP
 *                      is locked.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_TRUE/ECFM_FALSE
 *****************************************************************************/
PRIVATE             BOOL1
EcfmLbLtCheckLockedCondition (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    INT1                i1MdLevel = ECFM_INIT_VAL;
    u2LocalPort = pPduSmInfo->pPortInfo->u2PortNum;
    if ((ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (u2LocalPort) == ECFM_FALSE) ||
        ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (u2LocalPort) != ECFM_TRUE)
    {
        return ECFM_FALSE;
    }

    for (i1MdLevel = pPduSmInfo->u1RxMdLevel - 1;
         i1MdLevel >= ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
    {
        pStackInfo = EcfmLbLtUtilGetMp (u2LocalPort, i1MdLevel,
                                        pPduSmInfo->u4RxVlanIdIsId,
                                        ECFM_MP_DIR_UP);
        if (pStackInfo != NULL)
        {
            pPduSmInfo->pStackInfo = pStackInfo;

            if (ECFM_LBLT_IS_MEP (pStackInfo))
            {
                pMepInfo = pStackInfo->pLbLtMepInfo;

                if (ECFM_LBLT_LCK_IS_CONFIGURED (pMepInfo) == ECFM_TRUE)
                {
                    return ECFM_TRUE;
                }
            }
        }
    }
    for (i1MdLevel = pPduSmInfo->u1RxMdLevel - 1;
         i1MdLevel >= ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
    {
        pStackInfo = EcfmLbLtUtilGetMp (u2LocalPort, i1MdLevel,
                                        pPduSmInfo->u4RxVlanIdIsId,
                                        ECFM_MP_DIR_DOWN);
        if (pStackInfo != NULL)
        {
            pPduSmInfo->pStackInfo = pStackInfo;

            if (ECFM_LBLT_IS_MEP (pStackInfo))
            {
                pMepInfo = pStackInfo->pLbLtMepInfo;

                if (ECFM_LBLT_LCK_IS_CONFIGURED (pMepInfo) == ECFM_TRUE)
                {
                    return ECFM_TRUE;
                }
            }
        }
    }
    return ECFM_FALSE;
}

/*****************************************************************************
  End of File cfmlbrx.c
 ******************************************************************************/
