/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfmcfelw.c,v 1.11 2012/02/10 10:14:53 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"

#include "cfminc.h"

/* LOW LEVEL Routines for Table : Ieee8021CfmConfigErrorListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable (INT4
                                                         i4Ieee8021CfmConfigErrorListSelectorType,
                                                         UINT4
                                                         u4Ieee8021CfmConfigErrorListSelector,
                                                         INT4
                                                         i4Ieee8021CfmConfigErrorListIfIndex)
{
    tEcfmCcConfigErrInfo *pCofigErrNode = NULL;
    UINT4               u4VlanIdIsidOrNone =
        u4Ieee8021CfmConfigErrorListSelector;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmConfigErrorListSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmConfigErrorListSelector);
    }

    /* Get Config Err entry corresponding to indices, VlanId, IfIndex */
    pCofigErrNode = EcfmSnmpLwGetConfigErrEntry (u4VlanIdIsidOrNone,
                                                 i4Ieee8021CfmConfigErrorListIfIndex);
    if (pCofigErrNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Config Error Entry found\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmConfigErrorListTable
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmConfigErrorListTable (INT4
                                                 *pi4Ieee8021CfmConfigErrorListSelectorType,
                                                 UINT4
                                                 *pu4Ieee8021CfmConfigErrorListSelector,
                                                 INT4
                                                 *pi4Ieee8021CfmConfigErrorListIfIndex)
{
    return (nmhGetNextIndexIeee8021CfmConfigErrorListTable (0,
                                                            pi4Ieee8021CfmConfigErrorListSelectorType,
                                                            0,
                                                            pu4Ieee8021CfmConfigErrorListSelector,
                                                            0,
                                                            pi4Ieee8021CfmConfigErrorListIfIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmConfigErrorListTable
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                nextIeee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                nextIeee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex
                nextIeee8021CfmConfigErrorListIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmConfigErrorListTable (INT4
                                                i4Ieee8021CfmConfigErrorListSelectorType,
                                                INT4
                                                *pi4NextIeee8021CfmConfigErrorListSelectorType,
                                                UINT4
                                                u4Ieee8021CfmConfigErrorListSelector,
                                                UINT4
                                                *pu4NextIeee8021CfmConfigErrorListSelector,
                                                INT4
                                                i4Ieee8021CfmConfigErrorListIfIndex,
                                                INT4
                                                *pi4NextIeee8021CfmConfigErrorListIfIndex)
{
    tEcfmCcConfigErrInfo ConfigErrInfo;
    tEcfmCcConfigErrInfo *pConfigErrNextNode = NULL;
    UINT4               u4VlanIdIsidOrNone =
        u4Ieee8021CfmConfigErrorListSelector;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmConfigErrorListSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmConfigErrorListSelector);
    }

    /* Get ConfigErrList entry corresponding to indices next to VlanId, IfIndex */
    ECFM_MEMSET (&ConfigErrInfo, ECFM_INIT_VAL, ECFM_CC_CONFIG_ERR_INFO_SIZE);
    ConfigErrInfo.u4IfIndex = (UINT4) i4Ieee8021CfmConfigErrorListIfIndex;
    ConfigErrInfo.u4VidIsid = u4VlanIdIsidOrNone;

    pConfigErrNextNode = (tEcfmCcConfigErrInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, (tRBElem *) & ConfigErrInfo, NULL);
    if (pConfigErrNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Config Error Entry found\n");
        return SNMP_FAILURE;
    }

    /* ConfigErrList entry corresponding to indices next to VlanId, IfIndex exists */
    /* Set the indices corresponding to ConfigErrList entry */
    if ((pConfigErrNextNode->u4VidIsid) >= ECFM_INTERNAL_ISID_MIN)
    {
        *pu4NextIeee8021CfmConfigErrorListSelector =
            ECFM_ISID_INTERNAL_TO_ISID ((pConfigErrNextNode->u4VidIsid));
        *pi4NextIeee8021CfmConfigErrorListSelectorType =
            ECFM_SERVICE_SELECTION_ISID;
    }
    else
    {
        *pu4NextIeee8021CfmConfigErrorListSelector =
            ((pConfigErrNextNode->u4VidIsid));
        *pi4NextIeee8021CfmConfigErrorListSelectorType =
            ECFM_SERVICE_SELECTION_VLAN;
    }

    *pi4NextIeee8021CfmConfigErrorListIfIndex =
        (INT4) pConfigErrNextNode->u4IfIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmConfigErrorListErrorType
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex

                The Object 
                retValIeee8021CfmConfigErrorListErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmConfigErrorListErrorType (INT4
                                           i4Ieee8021CfmConfigErrorListSelectorType,
                                           UINT4
                                           u4Ieee8021CfmConfigErrorListSelector,
                                           INT4
                                           i4Ieee8021CfmConfigErrorListIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValIeee8021CfmConfigErrorListErrorType)
{
    tEcfmCcConfigErrInfo *pCofigErrNode = NULL;
    UINT4               u4VlanIdIsidOrNone =
        u4Ieee8021CfmConfigErrorListSelector;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmConfigErrorListSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmConfigErrorListSelector);
    }

    /* Get Config Err entry corresponding to indices, VlanId, IfIndex */
    pCofigErrNode = EcfmSnmpLwGetConfigErrEntry (u4VlanIdIsidOrNone,
                                                 i4Ieee8021CfmConfigErrorListIfIndex);
    if (pCofigErrNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Config Error Entry Found\n");
        return SNMP_FAILURE;
    }
    pRetValIeee8021CfmConfigErrorListErrorType->pu1_OctetList[0] =
        pCofigErrNode->u1ErrorType;
    pRetValIeee8021CfmConfigErrorListErrorType->i4_Length = 1;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Dot1agCfmConfigErrorListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmConfigErrorListTable
 Input       :  The Indices
                Dot1agCfmConfigErrorListVid
                Dot1agCfmConfigErrorListIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmConfigErrorListTable (INT4
                                                       i4Dot1agCfmConfigErrorListVid,
                                                       INT4
                                                       i4Dot1agCfmConfigErrorListIfIndex)
{
    UNUSED_PARAM (i4Dot1agCfmConfigErrorListVid);
    UNUSED_PARAM (i4Dot1agCfmConfigErrorListIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmConfigErrorListTable
 Input       :  The Indices
                Dot1agCfmConfigErrorListVid
                Dot1agCfmConfigErrorListIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmConfigErrorListTable (INT4
                                               *pi4Dot1agCfmConfigErrorListVid,
                                               INT4
                                               *pi4Dot1agCfmConfigErrorListIfIndex)
{
    UNUSED_PARAM (pi4Dot1agCfmConfigErrorListVid);
    UNUSED_PARAM (pi4Dot1agCfmConfigErrorListIfIndex);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmConfigErrorListTable
 Input       :  The Indices
                Dot1agCfmConfigErrorListVid
                nextDot1agCfmConfigErrorListVid
                Dot1agCfmConfigErrorListIfIndex
                nextDot1agCfmConfigErrorListIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmConfigErrorListTable (INT4
                                              i4Dot1agCfmConfigErrorListVid,
                                              INT4
                                              *pi4NextDot1agCfmConfigErrorListVid,
                                              INT4
                                              i4Dot1agCfmConfigErrorListIfIndex,
                                              INT4
                                              *pi4NextDot1agCfmConfigErrorListIfIndex)
{
    UNUSED_PARAM (i4Dot1agCfmConfigErrorListVid);
    UNUSED_PARAM (pi4NextDot1agCfmConfigErrorListVid);
    UNUSED_PARAM (i4Dot1agCfmConfigErrorListIfIndex);
    UNUSED_PARAM (pi4NextDot1agCfmConfigErrorListIfIndex);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmConfigErrorListErrorType
 Input       :  The Indices
                Dot1agCfmConfigErrorListVid
                Dot1agCfmConfigErrorListIfIndex

                The Object 
                retValDot1agCfmConfigErrorListErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmConfigErrorListErrorType (INT4 i4Dot1agCfmConfigErrorListVid,
                                         INT4 i4Dot1agCfmConfigErrorListIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValDot1agCfmConfigErrorListErrorType)
{
    UNUSED_PARAM (i4Dot1agCfmConfigErrorListVid);
    UNUSED_PARAM (i4Dot1agCfmConfigErrorListIfIndex);
    UNUSED_PARAM (pRetValDot1agCfmConfigErrorListErrorType);
    return SNMP_FAILURE;
}

/****************************************************************************
  End of File cfmcfelw.c
 ****************************************************************************/
