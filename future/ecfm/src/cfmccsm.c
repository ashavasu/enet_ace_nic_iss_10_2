/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccsm.c,v 1.43 2015/07/02 10:51:35 siva Exp $
 *
 * Description: This file contains the Functionality of the Continuity
 *              check Initiator State Machine.
 *******************************************************************/

#include "cfminc.h"
#include "cfmccsm.h"

PRIVATE VOID EcfmCciFrameLossPutInfo PROTO ((tEcfmCcMepInfo *, UINT1 **));

/****************************************************************************
 * Function Name      : EcfmCcClntCciSm
 *
 * Description        : This is the CC Initiator State Machine that traverse 
 *                      the state event matrix depending event given to it.
 *                                        
 * Input(s)           : u1Event - Specifying the event received by this 
 *                                state machine.
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntCciSm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);

    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcClntCciSm: Called with Event: %d, and State: %d"
                      "\r\n", u1Event, ECFM_CC_CCI_GET_STATE (pMepInfo));

    /* Call function pointer */
    if (ECFM_CC_CCI_STATE_MACHINE (u1Event, pMepInfo->CcInfo.u1CciState,
                                   pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmCcClntCciSm:CCM Initiator State machine for"
                          "Mep %u on port %u does not function Correctly\r\n",
                          pMepInfo->u2MepId, pMepInfo->u2PortNum);
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciSmSetStateIdle
 *
 * Description        : This rotuine is used to handle the occurence of event
 *                      BEGIN in DEFAULT state the state machine is set to 
 *                      IDLE state and then initiaze state machine. 
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmCciSmSetStateIdle (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /*State set to Idle unconditionally */
    ECFM_CC_CCI_SET_STATE (pMepInfo, ECFM_CCI_STATE_IDLE);

    /* MacStatusChanged should be set to false. */
    pMepInfo->CcInfo.b1MacStatusChanged = ECFM_FALSE;

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmCciSmSetStateIdle: CCI SEM Moved to Idle state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciSmSetStateDefault
 *
 * Description        : This rotuine is used to handle the occurence of event
 *                      MepNotActive in which state machine reset to default
 *                      STATE.
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmCciSmSetStateDefault (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);
    /*Stop the timer if running as MEP is disabled */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_CCI_WHILE, pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCciSmSetStateDefault:Unable to stop the timer"
                     "\r\n");
        return ECFM_FAILURE;

    }

    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pMepInfo->u2PortNum))
    {
        if (pCcInfo->u4RdiCapPeriod != 0)
        {
            if (EcfmCcTmrStopTimer (ECFM_CC_TMR_RDI_PERIOD, pPduSmInfo) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCciSmSetStateDefault:Unable to stop the timer"
                             "\r\n");
                return ECFM_FAILURE;
            }
        }
    }
    /* State machine is set to Default state */
    ECFM_CC_CCI_SET_STATE (pMepInfo, ECFM_CCI_STATE_DEFAULT);

    /* Reset the Macstatus change. */
    pMepInfo->CcInfo.b1MacStatusChanged = ECFM_FALSE;

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmCciSmSetStateDefault:CCI SEM Moved to Default state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/****************************************************************************
 * Function Name      : EcfmCciSmSetStateWaiting
 *
 * Description        : Thi routine handle the occurence of event CCIEnable 
 *                      event .This rotuine is used to handle the formation 
 *                      and Tx of CCM PDU. It also resets the MacStatusChanged. 
 *                      If ErrMacStatus is True and time interval is less than
 *                      10 sec an extra CCM PDU is transmitted.And set the state
 *                      to Waiting state.
 * 
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmCciSmSetStateWaiting (tEcfmCcPduSmInfo * pPduSmInfo)
{
    UINT4               u4Interval = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmCciSmSetStateWaiting: CCI SEM Moved to Waiting state "
                 "\r\n");

    /* Formats and transmit the CCM PDU */
    /* Start the timer for CCM with CCIInterval */
    ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4Interval);
    if (EcfmCciSmXmitCCM (pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCciSmSetStateWaiting:"
                     "CCM PDU Cant be formatted, hence fail in "
                     "transmission\r\n");

        if (pMepInfo->b1MepCcmOffloadStatus == ECFM_FALSE)
        {
            if (EcfmCcTmrStartTimer
                (ECFM_CC_TMR_CCI_WHILE, pPduSmInfo, u4Interval) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCciSmSetStateWaiting:CCStart Timer FAILED\r\n");
                return ECFM_FAILURE;
            }
        }
        /* Changing current state to WAITING */
        ECFM_CC_CCI_SET_STATE (pMepInfo, ECFM_CCI_STATE_WAITING);
        return ECFM_FAILURE;
    }
    if (pMepInfo->b1MepCcmOffloadStatus == ECFM_FALSE)
    {
        if (EcfmCcTmrStartTimer (ECFM_CC_TMR_CCI_WHILE, pPduSmInfo, u4Interval)
            != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCciSmSetStateWaiting:CCStart Timer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }
    /* Y.1731: When Y.1731 is disabled only then there is a check for 
     * MacStatusChange*/
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        if (pMepInfo->CcInfo.b1MacStatusChanged == ECFM_TRUE)
        {
            EcfmRedSyncCCDataOnChng (ECFM_CC_CURR_CONTEXT_ID (),
                                     pMepInfo->u4MdIndex,
                                     pMepInfo->u4MaIndex,
                                     pMepInfo->u2MepId, ECFM_INIT_VAL);
        }
    }
    /* Reset the MacStatusChange */
    pMepInfo->CcInfo.b1MacStatusChanged = ECFM_FALSE;
    /* Changing current state to WAITING */
    ECFM_CC_CCI_SET_STATE (pMepInfo, ECFM_CCI_STATE_WAITING);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciSmXmitCCM
 *
 * Description        : This rotuine is used to format and transmit the CCM PDU 
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmCciSmXmitCCM (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pCcMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;    /* pointer to CRU buffer */
    UINT1              *pu1EthLlcHdr = NULL;    /* Pointer for filling the LLC Hdr */
    UINT1              *pu1PduStart = NULL;    /* pointer containing Start of PDU */
    UINT1              *pu1PduEnd = NULL;    /* pointer containing End of PDU */
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT1               u1PduLength = ECFM_INIT_VAL;
    UINT1               u1Priority = ECFM_INIT_VAL;
    UINT1               u1DropEligible = ECFM_FALSE;

    ECFM_CC_TRC_FN_ENTRY ();

    pCcMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);

#ifdef MBSM_WANTED
    /* If Ports are not present for this MEP then don't transmit PDU */
    if (EcfmMbsmIsPortPresent (pCcMepInfo->pPortInfo->u4IfIndex,
                               pCcMepInfo->u4PrimaryVidIsid,
                               pCcMepInfo->u1Direction) == ECFM_FALSE)
    {
        return ECFM_SUCCESS;
    }
#endif

    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pCcMepInfo);

    if (pCcMepInfo->pEcfmMplsParams != NULL)
    {
        /*Allocates the CRU Buffer */
        pBuf =
            ECFM_ALLOC_CRU_BUF (ECFM_MAX_PDU_SIZE, ECFM_MPLSTP_CTRL_PKT_OFFSET);
        if (pBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "\n EcfmCciSmXmitCCM:Buffer Allocation failed\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_PDU_SIZE, ECFM_CTRL_PKT_OFFSET);
        if (pBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "\n EcfmCciSmXmitCCM:Buffer Allocation failed\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }

    pu1PduStart = ECFM_CC_PDU;
    pu1EthLlcHdr = ECFM_CC_PDU;
    pu1PduEnd = ECFM_CC_PDU;

    /* Format the  CCM PDU header */
    EcfmCciSmFormatCcmPduHdr (pCcMepInfo, &pu1PduEnd);

    /* Fill in the CCM PDU info like Mep id,seq num,ber,MAId and other optional
     * tlvs*/
    EcfmCciSmPutInfo (pPduSmInfo, &pu1PduEnd);
    u1PduLength = (UINT1) (pu1PduEnd - pu1PduStart);

    /* check for the Pdu length if it is less than equal to 128 octets then
     * only write to CRU bufffer else retuen failure */
    if (u1PduLength > ECFM_MAX_CCM_PDU_SIZE)
    {
        ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "\n EcfmCciSmXmitCCM:PDU length is greater than 128"
                     " octets\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /*Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                (UINT4) (u1PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "\n EcfmCciSmXmitCCM:Copying into Bufferfailed\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    if (pCcMepInfo->pEcfmMplsParams != NULL)
    {
        /* This MEP belongs to MPLS TP network. Do not add an ethernet header.
         * send the raw packet for transmission
         * */
        if (EcfmMplsTpCcTxPacket (pBuf, pCcMepInfo,
                                  ECFM_OPCODE_CCM) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCciSmXmitCCM: Transmit CFMPDU failed\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                          "EcfmCciSmXmitCCM: Sending out CFMPDU to "
                          "lower layer.\n");

#ifdef L2RED_WANTED
        /* Synch  STANDBY node calculates Sequence number to be sent after 
         * switchover using this timestamp. The STANDBY node does not take care
         * of time taken by ACTIVE to format and transamit CCM PDU. Over the 
         * time for large number of CCM PDUs transmitted this time will be
         * significant. Hence to avoid this Time stamp and transmit sequence 
         * number is synched to Standby after every 100 CCM PDUs are 
         * transmitted. */
        if (!(pCcInfo->u4CciSentCcms % ECFM_RED_SYNC_SEQ_NO_INTERVAL))
        {
            EcfmRedSyncCcSeqCounter (pPduSmInfo);
        }
#endif

        if ((pCcMepInfo->pPortInfo->u1IfOperStatus == CFA_IF_DOWN) &&
            (pCcMepInfo->u1Direction == ECFM_MP_DIR_DOWN))
        {
            return ECFM_SUCCESS;
        }
        /* Increment MepCciSentCcms by 1  */
        ECFM_CC_INCR_CCM_SEQ_NUMBER (pCcInfo);
        pCcInfo->u4CciCcmCount++;
        return ECFM_SUCCESS;
    }

    else
    {
        /* Format the Ether net and the LLC header */
        EcfmFormatCcTaskPduEthHdr (pCcMepInfo, &pu1EthLlcHdr,
                                   (UINT2) u1PduLength, ECFM_OPCODE_CCM);

        /* Prepend the Ethernet and the LLC header in the CRU buffer */
        u1PduLength = (UINT1) (pu1EthLlcHdr - pu1PduStart);
        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart,
                                  (UINT4) (u1PduLength)) != ECFM_CRU_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                         ALL_FAILURE_TRC,
                         "\n EcfmCciSmXmitCCM:Prepending into Bufferfailed\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

        /* Y.1731: Ccm Priority and Drop Eligibility are configured acc. to  
         * Y1731 Module enable/disable status*/

        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pCcMepInfo->u2PortNum))
        {
            u1Priority = pCcMepInfo->CcInfo.u1CcmPriority;
            u1DropEligible = pCcMepInfo->CcInfo.b1CcmDropEligible;
        }
        else
        {
            u1Priority = pCcMepInfo->u1CcmLtmPriority;
            u1DropEligible = ECFM_FALSE;
        }
    }
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                      "EcfmCciSmXmitCCM: Sending out CFMPDU to lower layer.\n");
    if (pCcMepInfo->b1MepCcmOffloadStatus != ECFM_TRUE)
    {
        if (EcfmCcCtrlTxTransmitPkt (pBuf, pCcMepInfo->u2PortNum,
                                     pCcMepInfo->u4PrimaryVidIsid,
                                     u1Priority, u1DropEligible,
                                     pCcMepInfo->u1Direction, ECFM_OPCODE_CCM,
                                     NULL, NULL) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCciSmXmitCCM: Transmit CFMPDU failed\n");
            return ECFM_FAILURE;
        }
#ifdef L2RED_WANTED
        /* Synch  STANDBY node calculates Sequence number to be sent after 
         * switchover using this timestamp. The STANDBY node does not take care
         * of time taken by ACTIVE to format and transamit CCM PDU. Over the 
         * time for large number of CCM PDUs transmitted this time will be
         * significant. Hence to avoid this Time stamp and transmit sequence 
         * number is synched to Standby after every 100 CCM PDUs are 
         * transmitted. */
        if (!(pCcInfo->u4CciSentCcms % ECFM_RED_SYNC_SEQ_NO_INTERVAL))
        {
            EcfmRedSyncCcSeqCounter (pPduSmInfo);
        }

#endif
        if ((pCcMepInfo->pPortInfo->u1IfOperStatus == CFA_IF_DOWN) &&
            (pCcMepInfo->u1Direction == ECFM_MP_DIR_DOWN))
        {
            ECFM_CC_TRC_FN_EXIT ();
            return ECFM_SUCCESS;
        }
        /* Increment MepCciSentCcms by 1  */
        ECFM_CC_INCR_CCM_SEQ_NUMBER (pCcInfo);
        pCcInfo->u4CciCcmCount++;
    }
    else
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCciSmXmitCCM: PDU will be transmitted from"
                     "Offloaded module, Free PDU in software\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciSmEvtImpossible 
 *
 * Description        : This rotuine is used to handle the occurence of event 
 *                      which canmt be possible in the current state of the 
 *                      state machine.  
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 *                      specific MEP  
 * Output(s)          : None
 * Return Value(s)    : ECFM_SUCCESS 
 *****************************************************************************/
PRIVATE INT4
EcfmCciSmEvtImpossible (tEcfmCcPduSmInfo * pPduSmInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

    UNUSED_PARAM (pPduSmInfo);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCciSmEvtImpossible:"
                 "IMPOSSIBLE EVENT/STATE Combination Occurred in CCI SEM \r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciSmFormatCcmPduHdr
 *
 * Description        : This rotuine is used to fill the CFM PDU Header
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       pu1CcmPdu - Pointer to the Pdu     
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCciSmFormatCcmPduHdr (tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu)
{
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1Flag = ECFM_INIT_VAL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);
    pu1Pdu = *pu1CcmPdu;

    /* Fill in the 4 byte as CFM header */

    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1Pdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_OPCODE_CCM);

    /* Fill in the Next 1 byte of FLag(RDI(MSB),Reserve(4Bit),CCIInterval(3Bit) */

    /* Y.1731: When Y.1731 is enabled only then Rdi Capability is checked for 
     * including RDI bit in CCM PDU otherwise not*/

    /* Set the MSB as the RDI field */

    if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
        (ECFM_CC_IS_RDI_ENABLED (pMepInfo)) &&
        (pMepInfo->b1PresentRdi == ECFM_TRUE))
    {
        /* Set the MSB in the FLAGS of the CCM-PDU */
        ECFM_SET_U1BIT (u1Flag, ECFM_CC_RDI_BIT);
    }

    if ((ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum)) &&
        (pMepInfo->b1PresentRdi == ECFM_TRUE))

    {
        /* Set the MSB in the FLAGS of the CCM-PDU */
        ECFM_SET_U1BIT (u1Flag, ECFM_CC_RDI_BIT);
    }

    /* next 4bit are reserved */
    /* Fill in the next 3 bits of CCI Interval */
    ECFM_CC_SET_CCM_INTERVAL (u1Flag, pMaInfo->u1CcmInterval);
    ECFM_PUT_1BYTE (pu1Pdu, u1Flag);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_CCM_FIRST_TLV_OFFSET);

    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *function Name      : EcfmCciSmPutInfo
 *
 * Description        : This rotuine is used to fill the Information other than
 *                      optional TLVs in the PDU(Sequence number,MepId and MAID)
 *
 * Input(s)           : pu1CcmPdu - pointer to the Pdu
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 *                      
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCciSmPutInfo (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 **pu1CcmPdu)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT1              *pu1Pdu = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pu1Pdu = *pu1CcmPdu;

    /*Y.1731: When Y.1731 is enabled fill in the Sequence number as zero, MEGID
     *Performance Monitoring fields otherwise Sequence number, MAID, TLVs */

    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        /*Fill the 4byte CCI sequence number as zero */
        ECFM_PUT_4BYTE (pu1Pdu, 0);

        /* Fill the  next 2 byte as MEPID */
        ECFM_PUT_2BYTE (pu1Pdu, pMepInfo->u2MepId);

        /* Fills next 48 byte as MEGID, with Icc and Umc Codes */
        EcfmUtilPutMEGID (pMepInfo, pu1Pdu);
        pu1Pdu = pu1Pdu + ECFM_MAID_FIELD_SIZE;

        /* Fill the Performace Monitoring fields with the Frame Loss API 
         * Check whether pointer is incremented in API or not*/
        EcfmCciFrameLossPutInfo (pMepInfo, &pu1Pdu);

        /*Reserved fields are added */
        ECFM_MEMSET (pu1Pdu, ECFM_RESERVE_VALUE, ECFM_CCM_RESERVE_FIELD_SIZE);
        pu1Pdu = pu1Pdu + ECFM_CCM_RESERVE_FIELD_SIZE;
    }
    else
    {
        /*Fill the 4byte CCI sequence number */
        ECFM_PUT_4BYTE (pu1Pdu, pMepInfo->CcInfo.u4CciSentCcms);

        /* Fill the  next 2 byte as MEPID */
        ECFM_PUT_2BYTE (pu1Pdu, pMepInfo->u2MepId);

        /* Fills next 48 byte as MAID, with 1 byte each as MD and MA Name format
         * , 1 byte each for Ma and MD Name Length, and remaining 44 byte as Ma
         * and MD name*/
        EcfmUtilPutMAID (pMepInfo, pu1Pdu);
        pu1Pdu = pu1Pdu + ECFM_MAID_FIELD_SIZE;

        /* Fill the next 16 byte as 0 for fields defined in ITU-TY.1731 */
        ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, ECFM_ITU_RESERVE_FIELD_SIZE);
        pu1Pdu = pu1Pdu + ECFM_ITU_RESERVE_FIELD_SIZE;

        /* optional TLVs are filled in the PDU */
        EcfmCciSmPutCCMTlv (pPduSmInfo, &pu1Pdu);
    }

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_END_TLV_TYPE);

    *pu1CcmPdu = pu1Pdu;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCciSmPutCCMTlv
 *
 * Description        : This rotuine is used to fill the Optional CCM TLVs in 
 *                      the PDU
 * Input(s)           : pu1CcmPdu - pointer to the PDU
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None

 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCciSmPutCCMTlv (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 **pu1CcmPdu)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT1              *pu1Pdu = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pu1Pdu = *pu1CcmPdu;
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Fill Sender ID TLV */
    ECFM_CC_SET_SENDER_ID_TLV (pMepInfo, pu1Pdu);
    /* Fill Port Status TLV */
    /* Port status TLV is not required to be filled in case MSTP is enabled and
     * MEP which is required to send CCM is VLAN unaware */
    if ((EcfmAstIsMstEnabledInContext (ECFM_CC_CURR_CONTEXT_ID ()) !=
         MST_ENABLED) ||
        ((EcfmAstIsMstEnabledInContext (ECFM_CC_CURR_CONTEXT_ID ()) ==
          MST_ENABLED) && (pMepInfo->u4PrimaryVidIsid != 0)))
    {
        EcfmCciSmPutPortStatusTlv (pPduSmInfo, &pu1Pdu);
    }

    /* Fill Interface Status in all the cases whether MSTP is enabled or not */
    EcfmCciSmPutInterfaceStatusTlv (pPduSmInfo->pMepInfo, &pu1Pdu);

    /* Fill Organisational TLV */
    /* Fill 1 byte Org specific tlv type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_TLV_TYPE);

    /* fill 2  byte length value as 4 by default
     * (3 as OUI size and 1 for subtype),value is ignored at present */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_ORG_SPEC_MIN_LENGTH);

    /* fill in the 3 byte OUI from global */
    ECFM_CC_SET_OUI (pu1Pdu);

    /* fill in 1 byte as subtype as 0 */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_SUBTYPE_VALUE);

    /*rest few bytes are for value which is optional */

    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           :EcfmCciSmPutPortStatusTlv
 *
 * Description        : Forms the Port Status TLV afetr comparing the current
 *                      status with the previously send status
 *
 * Input(s)           : pu1CcmPdu - pointer to the Pdu to be filled 
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 *
 *
 * Output(s)          : None.
 *
 * Returns            : NONE 
 ******************************************************************************/
PUBLIC VOID
EcfmCciSmPutPortStatusTlv (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 **pu1CcmPdu)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    if (pPduSmInfo == NULL)
    {
        return;
    }

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);

    if (pMepInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCciSmPutPortStatusTlv:"
                     "MEP Information is not present \r\n");
        return;
    }

    pu1Pdu = *pu1CcmPdu;

    if (ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCciSmPutPortStatusTlv:"
                     "Port Info is not present \r\n");
        return;
    }
    if ((pMepInfo->pPortInfo->u1PortType == CFA_CUSTOMER_EDGE_PORT) &&
        (pMepInfo->u1Direction == ECFM_MP_DIR_UP))
    {
        if (pMepInfo->u1PepPortState == ECFM_PORT_IS_UP)
        {
            pMepInfo->b1EnableRmepDefect = ECFM_TRUE;
        }
        else
        {
            pMepInfo->b1EnableRmepDefect = ECFM_FALSE;
        }
        pMepInfo->CcInfo.u1PortState = pMepInfo->u1PepPortState;
    }
    else
    {
        if (pMepInfo->pPortInfo->u1PortState == ECFM_PORT_IS_BLOCKED)
        {
            pMepInfo->b1EnableRmepDefect = ECFM_FALSE;
        }
        else if (pMepInfo->pPortInfo->u1PortState == ECFM_PORT_IS_UP)
        {
            pMepInfo->b1EnableRmepDefect = ECFM_TRUE;
        }

        /*Check if this is the first CCM, i.e there is no saved port status */
        if (pMepInfo->CcInfo.u4CciSentCcms == 1)
        {
            pMepInfo->CcInfo.u1PortState = pMepInfo->pPortInfo->u1PortState;
        }
        /* Not the first CCM */
        else
        {
            /* Check if there is change in the Port state */
            if ((pMepInfo->CcInfo.u1PortState !=
                 pMepInfo->pPortInfo->u1PortState)
                || (pMepInfo->CcInfo.b1MacStatusChanged == ECFM_TRUE))
            {
                /* if the port state has been changed from Up to Blocked then gerate
                 * the event for rmep SEM fro change in the port state*/
                if ((pMepInfo->pPortInfo->u1PortState == ECFM_PORT_IS_BLOCKED)
                    && (pMepInfo->CcInfo.u1PortState == ECFM_PORT_IS_UP))
                {
                    EcfmCcNotEnableRmepIndication (pPduSmInfo);
                    /* Set enableRmepDefect to False */
                    pMepInfo->b1EnableRmepDefect = ECFM_FALSE;
                }
                /* Set the port state in CCMSMInfo  to the new value */
                pMepInfo->CcInfo.u1PortState = pMepInfo->pPortInfo->u1PortState;
            }
        }
    }
    /* Put 1byte as the Port status TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_PORT_STATUS_TLV_TYPE);

    /* Put the next 2 byte as the length of the TLV */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_PORT_STATUS_VALUE_SIZE);

    /*Finally copies the 1byte of the port state in the PDU */
    ECFM_PUT_1BYTE (pu1Pdu, pMepInfo->CcInfo.u1PortState);

    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           :EcfmCciSmPutInterfaceStatusTlv
 *
 * Description        : Forms the Interface Status TLV after comparing the
 *                      currrent interface status with the previously send 
 *                      interface status
 *
 * Input(s)           : pu1CcmPdu - pointer to the PDU
 *                      pMepInfo - Pointer to the MEP structure that stores the 
 *                      information regarding MEP.
 *
 * Output(s)          : None.
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
EcfmCciSmPutInterfaceStatusTlv (tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu)
{
    UINT1              *pu1Pdu = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    pu1Pdu = *pu1CcmPdu;
    /*Check if this is the first CCM */
    if (pMepInfo->CcInfo.u4CciSentCcms == 1)
    {
        pMepInfo->CcInfo.u1IfStatus = pMepInfo->pPortInfo->u1IfOperStatus;
    }
    /* Not the first CCM */
    else
    {
        /* Check if there is change in the Port state */
        if ((pMepInfo->CcInfo.u1IfStatus !=
             pMepInfo->pPortInfo->u1IfOperStatus) ||
            (pMepInfo->CcInfo.b1MacStatusChanged == ECFM_TRUE))
        {
            /* Set the Interface status in CCMSMInfo  to the new value */
            pMepInfo->CcInfo.u1IfStatus = pMepInfo->pPortInfo->u1IfOperStatus;
        }
    }

    /* Form the contents of Interface  Status TLV */
    /* Fill 1byte as the Interface TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_INTERFACE_STATUS_TLV_TYPE);

    /*Fill the 2byte length of the Inteface status  */
    ECFM_PUT_2BYTE (pu1Pdu, ECFM_INTERFACE_STATUS_VALUE_SIZE);

    /* Fill in the Interface status */
    ECFM_PUT_1BYTE (pu1Pdu, pMepInfo->CcInfo.u1IfStatus);

    *pu1CcmPdu = pu1Pdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcNotEnableRmepIndication
 *
 * Description        : This routine generated NOT_ENABLE_INIDCATION to all the
 *                      RMEP in a MEP when port statstus chages from psUp to 
 *                      psDown.This event sets the SEM to Default state.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcNotEnableRmepIndication (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMep = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get the first node from the RBtree of RMepDbTable in MepInfo */
    pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepInfo->RMepDb));
    while (pRMep != NULL)
    {
        pPduSmInfo->pRMepInfo = pRMep;

        /* Call the rmep SEM with ECFM_SM_EV_RMEP_NO_DEFECT */
        EcfmCcClntRmepSm (pPduSmInfo, ECFM_SM_EV_RMEP_NO_DEFECT);

        /* Get the next node form the tree */
        pRMep =
            (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepInfo->RMepDb),
                                                &(pRMep->MepDbDllNode));
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcRdiPeriodTimeout
 *
 * Description        : This routine called on expiry of RDI Period Timer 
 *                      to toggle the RDI Capability.
 *                                        
 * Input(s)           : pMepInfo- Pointer to the structure that stores the 
 *                      information regarding CC Mep info.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcRdiPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
	ECFM_CC_TRC_FN_ENTRY ();

    ECFM_CC_TRC_FN_ENTRY ();

    /* Reset Rdi Capability Period */
    pMepInfo->CcInfo.u4RdiCapPeriod = ECFM_INIT_VAL;

    /*Checking the RDI Capability and toggling the capability on RDI Period 
     *Timeout*/
    if (ECFM_CC_IS_RDI_ENABLED (pMepInfo))
    {
        ECFM_CC_SET_RDI_DISABLE (pMepInfo);
    }
    else
    {
        ECFM_CC_SET_RDI_ENABLE (pMepInfo);
    }
    if ((pMepInfo->b1MepCcmOffloadStatus == ECFM_TRUE))
    {
	    /* First Disable MEP transmission from this MEP */
	    /* Update Tx/Rx parameters for this MEP */
	    EcfmCcmOffDeleteTxRxForMep (pMepInfo);
	    if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
	    {
		    return ECFM_FAILURE;
	    }
	    /* Enable Tx/Rx for this MEP */
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCciFrameLossPutInfo
 *
 * Description        : This routine is used to put the Performance Monitoring 
 *                      fields in CC PDU.
 *                      
 * Input(s)           : pu1CcmPdu - pointer to the Pdu to be filled 
 *                                        
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmCciFrameLossPutInfo (tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu)
{
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    UINT4               u4TxFCf = ECFM_INIT_VAL;
    UINT4               u4RxFCl = ECFM_INIT_VAL;
    UINT4               u4RxFCb = ECFM_INIT_VAL;
    UINT4               u4TxFCb = ECFM_INIT_VAL;
    UINT1              *pu1Pdu = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepInfo);

    pu1Pdu = *pu1CcmPdu;

    /* Check if Performance Monitoring for CCM is enabled or not */
    if (pMaInfo->u1CcRole == ECFM_CC_ROLE_PM)
    {
        /* CCM is configured for Performance monitoring */
        u4RxFCb = pLmInfo->u4PreRxFCl;
        u4TxFCb = pLmInfo->u4PreTxFCf;
        /* Get the value of u4TxFCf */
        if (pMepInfo->pEcfmMplsParams != NULL)
        {
            if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
            {
                EcfmGetMplsPktCnt (pMepInfo, &u4TxFCf, &u4RxFCl);
            }
        }
        else
        {
            EcfmGetPacketCounters (pMepInfo->pPortInfo->u4IfIndex,
                                   pMepInfo->u4PrimaryVidIsid, &u4TxFCf,
                                   &u4RxFCl);

        }
    }
    /* Fill the next 4 byte for TxFCf field */
    ECFM_PUT_4BYTE (pu1Pdu, u4TxFCf);
    /* Fill the next 4 byte for RxFCb field */
    ECFM_PUT_4BYTE (pu1Pdu, u4RxFCb);
    /* Fill the next 4 byte for TxFCb field */
    ECFM_PUT_4BYTE (pu1Pdu, u4TxFCb);

    *pu1CcmPdu = pu1Pdu;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcHandleMacStatusChange
 *
 * Description        : This routine validates and sends an extra CCM. 
 *                      Criteria for sending extra CCM are
 *                      1. Offloading is not enabled
 *                      2. CCM transmission is enabled
 *                      3. CCM Tx interval is >= 10 seconds
 *                      4. Interface status or port status has a change.
 *
 *                      Note: pMepNode should not be NULL while calling 
 *                            this function.
 *                      
 * Input(s)           : pMepNode - Pointer to MEP Node.
 *                      u4Event  - Event on which validation is being done.
 *                                 1. ECFM_OPER_STATUS_CHG_MSG
 *                                 2. ECFM_CCM_VLAN_PORT_STATE_CHANGE
 *                                        
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
EcfmCcHandleMacStatusChange (tEcfmCcMepInfo * pMepNode, UINT4 u4Event)
{
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_MEMSET (&PduSmInfo, 0x00, sizeof (tEcfmCcPduSmInfo));

    if (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE)
    {
        return;
    }

    if (pMepNode->CcInfo.b1CciEnabled == ECFM_FALSE)
    {
        return;
    }

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);

    if (pMaInfo->u1CcmInterval < ECFM_CCM_INTERVAL_10_S)
    {
        return;
    }

    if (u4Event == ECFM_OPER_STATUS_CHG_MSG)
    {
        if (pMepNode->CcInfo.u1IfStatus == pMepNode->pPortInfo->u1IfOperStatus)
        {
            return;
        }
    }
    else if ((u4Event == ECFM_PORT_STATE_CHANGE) ||
             (u4Event == ECFM_VLAN_MEMBER_CHANGE))
    {
        if (pMepNode->CcInfo.u1PortState == pMepNode->pPortInfo->u1PortState)
        {
            return;
        }
    }

    pMepNode->CcInfo.b1MacStatusChanged = ECFM_TRUE;

    /* Since mac status changed, call the CCI Sem to send extra CCM. */
    PduSmInfo.pMepInfo = pMepNode;
    EcfmCcClntCciSm (&PduSmInfo, ECFM_SM_EV_CCI_TIMER_TIMEOUT);
    return;
}

/****************************************************************************
  End of File cfmccsm.c
 *****************************************************************************/
