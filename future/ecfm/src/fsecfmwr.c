/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: fsecfmwr.c,v 1.14 2011/01/25 15:11:14 siva Exp $
 *
 * Description: This file contains the wrapper routines for 
 *              proprietary mib.
 *******************************************************************/

# include  "cfminc.h"
# include  "fsecfmdb.h"

VOID
RegisterFSECFM ()
{
    SNMPRegisterMib (&fsecfmOID, &fsecfmEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsecfmOID, (const UINT1 *) "fsecfm");
}

VOID
UnRegisterFSECFM ()
{
    SNMPUnRegisterMib (&fsecfmOID, &fsecfmEntry);
    SNMPDelSysorEntry (&fsecfmOID, (const UINT1 *) "fsecfm");
}

INT4
FsEcfmSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmSystemControl (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmModuleStatus (&(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmOuiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmOui (pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmTraceOption (&(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtrCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmLtrCacheStatus (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtrCacheClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmLtrCacheClear (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtrCacheHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmLtrCacheHoldTime (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtrCacheSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmLtrCacheSize (&(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmSystemControl (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmModuleStatus (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmOuiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmOui (pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmTraceOption (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtrCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmLtrCacheStatus (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtrCacheClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmLtrCacheClear (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtrCacheHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmLtrCacheHoldTime (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtrCacheSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmLtrCacheSize (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmSystemControl (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmModuleStatus (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmOuiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmOui (pu4Error, pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmTraceOption (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtrCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmLtrCacheStatus (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtrCacheClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmLtrCacheClear (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtrCacheHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmLtrCacheHoldTime (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtrCacheSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmLtrCacheSize (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmModuleStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmOuiDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmOui (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmTraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmLtrCacheStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmLtrCacheStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmLtrCacheClearDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmLtrCacheClear
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmLtrCacheHoldTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmLtrCacheHoldTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmLtrCacheSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmLtrCacheSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmPortTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmPortTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmPortTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortLLCEncapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortLLCEncapStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortModuleStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortTxCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxCcmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortTxCcmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxLbmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortTxLbmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortTxLbrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxLtmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortTxLtmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxLtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortTxLtrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxFailedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortTxFailedCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortRxCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortRxCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxCcmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortRxCcmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLbmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortRxLbmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortRxLbrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortRxLtmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortRxLtmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmPortRxLtrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortRxBadCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_LOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortRxBadCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortFrwdCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_LOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortFrwdCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortDsrdCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmPortTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmPortDsrdCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortLLCEncapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortLLCEncapStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortModuleStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortTxCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxCcmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortTxCcmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLbmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortTxLbmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLbrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortTxLbrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLtmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortTxLtmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLtrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortTxLtrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxFailedCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortTxFailedCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortRxCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxCcmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortRxCcmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLbmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortRxLbmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLbrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortRxLbrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLtmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortRxLtmCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLtrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmPortRxLtrCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxBadCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortRxBadCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortFrwdCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortFrwdCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortDsrdCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmPortDsrdCfmPduCount
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortLLCEncapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortLLCEncapStatus (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortModuleStatus (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortTxCfmPduCount (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxCcmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortTxCcmCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLbmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortTxLbmCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmPortTxLbrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortTxLbrCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLtmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortTxLtmCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxLtrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortTxLtrCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTxFailedCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortTxFailedCount (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortRxCfmPduCount (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxCcmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortRxCcmCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLbmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortRxLbmCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLbrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortRxLbrCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLtmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortRxLtmCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxLtrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmPortRxLtrCount (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortRxBadCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortRxBadCfmPduCount (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortFrwdCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortFrwdCfmPduCount (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortDsrdCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmPortDsrdCfmPduCount (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMipCcmDbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMipCcmDbStatus (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMipCcmDbClear (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipCcmDbSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMipCcmDbSize (&(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipCcmDbHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMipCcmDbHoldTime (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMemoryFailureCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMemoryFailureCount (&(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmBufferFailureCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmBufferFailureCount (&(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmUpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmUpCount (&(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmDownCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmDownCount (&(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmNoDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmNoDftCount (&(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRdiDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmRdiDftCount (&(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMacStatusDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMacStatusDftCount (&(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRemoteCcmDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmRemoteCcmDftCount (&(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmErrorCcmDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmErrorCcmDftCount (&(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmXconDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmXconDftCount (&(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmCrosscheckDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmCrosscheckDelay (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipDynamicEvaluationStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmMipDynamicEvaluationStatus (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipCcmDbStatus (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipCcmDbClear (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipCcmDbSize (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipCcmDbHoldTime (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMemoryFailureCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMemoryFailureCount (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmBufferFailureCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmBufferFailureCount (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmUpCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmUpCount (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmDownCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmDownCount (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmNoDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmNoDftCount (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRdiDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRdiDftCount (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMacStatusDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMacStatusDftCount (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRemoteCcmDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRemoteCcmDftCount (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmErrorCcmDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmErrorCcmDftCount (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmXconDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmXconDftCount (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmCrosscheckDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmCrosscheckDelay (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipDynamicEvaluationStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipDynamicEvaluationStatus (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipCcmDbStatus (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipCcmDbClear (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipCcmDbSize (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipCcmDbHoldTime (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMemoryFailureCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMemoryFailureCount (pu4Error, pMultiData->i4_SLongValue)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmBufferFailureCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmBufferFailureCount (pu4Error, pMultiData->i4_SLongValue)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmUpCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmUpCount (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmDownCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmDownCount (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmNoDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmNoDftCount (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRdiDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRdiDftCount (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMacStatusDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMacStatusDftCount (pu4Error, pMultiData->i4_SLongValue)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRemoteCcmDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRemoteCcmDftCount (pu4Error, pMultiData->i4_SLongValue)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmErrorCcmDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmErrorCcmDftCount (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmXconDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmXconDftCount (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmCrosscheckDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmCrosscheckDelay (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipDynamicEvaluationStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipDynamicEvaluationStatus (pu4Error,
                                                   pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipCcmDbStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMipCcmDbStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMipCcmDbClearDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMipCcmDbClear
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMipCcmDbSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMipCcmDbSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMipCcmDbHoldTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMipCcmDbHoldTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMemoryFailureCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMemoryFailureCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmBufferFailureCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmBufferFailureCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmUpCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmUpCount (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmDownCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmDownCount (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmNoDftCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmNoDftCount (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmRdiDftCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmRdiDftCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMacStatusDftCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMacStatusDftCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmRemoteCcmDftCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmRemoteCcmDftCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmErrorCcmDftCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmErrorCcmDftCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmXconDftCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmXconDftCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmCrosscheckDelayDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmCrosscheckDelay
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmMipDynamicEvaluationStatusDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMipDynamicEvaluationStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmMipTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmMipTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmMipTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMipTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMipActive
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMipTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMipRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMipActiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipActive
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMipRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipActiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipActive (pu4Error,
                                  pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                  i4_SLongValue,
                                  pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                  i4_SLongValue,
                                  pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                  i4_SLongValue,
                                  pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMipRowStatus (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     i4_SLongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                     i4_SLongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                     i4_SLongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMipTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmMipCcmDbTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmMipCcmDbTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[ECFM_INDEX_ONE].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmMipCcmDbTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[ECFM_INDEX_ONE].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[ECFM_INDEX_ONE].pOctetStrValue->i4_Length =
        ECFM_MAC_ADDR_LENGTH;
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMipCcmIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMipCcmDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[ECFM_INDEX_ONE].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMipCcmIfIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[ECFM_INDEX_ONE].pOctetStrValue->
          pu1_OctetList), &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmGlobalCcmOffloadGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmGlobalCcmOffload (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmGlobalCcmOffloadSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmGlobalCcmOffload (pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmGlobalCcmOffloadTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmGlobalCcmOffload (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmGlobalCcmOffloadDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmGlobalCcmOffload
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmDynMipPreventionTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmDynMipPreventionTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmDynMipPreventionTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmDynMipPreventionRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmDynMipPreventionTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmDynMipPreventionRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmDynMipPreventionRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmDynMipPreventionRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmDynMipPreventionRowStatusTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmDynMipPreventionRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmDynMipPreventionTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmDynMipPreventionTable (pu4Error,
                                                 pSnmpIndexList,
                                                 pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmRemoteMepDbExTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmRemoteMepDbExTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmRemoteMepDbExTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepCcmSequenceNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepCcmSequenceNum
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepPortStatusDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepPortStatusDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepInterfaceStatusDefectGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepInterfaceStatusDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepCcmDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepCcmDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepRDIDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepRDIDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsEcfmRMepMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepRdiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepRdi (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
                             pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
                             pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
                             pMultiIndex->pIndex[ECFM_INDEX_THREE].
                             u4_ULongValue,
                             &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepPortStatusTlvGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepPortStatusTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepInterfaceStatusTlvGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepInterfaceStatusTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepChassisIdSubtype
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepDbChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepDbChassisId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepManAddressDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepManAddressDomain
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepManAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmRMepManAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepPortStatusDefectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepPortStatusDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepInterfaceStatusDefectSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepInterfaceStatusDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepCcmDefectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepCcmDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepRDIDefectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepRDIDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepMacAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepRdiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepRdi (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
                             pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
                             pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
                             pMultiIndex->pIndex[ECFM_INDEX_THREE].
                             u4_ULongValue,
                             pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepPortStatusTlvSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepPortStatusTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepInterfaceStatusTlvSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepInterfaceStatusTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepChassisIdSubtypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepChassisIdSubtype
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDbChassisIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepDbChassisId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepManAddressDomainSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepManAddressDomain
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepManAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmRMepManAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepPortStatusDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepPortStatusDefect (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_THREE].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepInterfaceStatusDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepInterfaceStatusDefect (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_THREE].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepCcmDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepCcmDefect (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepRDIDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepRDIDefect (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsEcfmRMepMacAddress (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                       u4_ULongValue,
                                       (*(tMacAddr *) pMultiData->
                                        pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmRMepRdiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepRdi (pu4Error,
                                pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                u4_ULongValue,
                                pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                u4_ULongValue,
                                pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                u4_ULongValue,
                                pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                u4_ULongValue,
                                pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepPortStatusTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepPortStatusTlv (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepInterfaceStatusTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepInterfaceStatusTlv (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_THREE].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepChassisIdSubtypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepChassisIdSubtype (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_THREE].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDbChassisIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepDbChassisId (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                       u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepManAddressDomainTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepManAddressDomain (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_THREE].
                                             u4_ULongValue,
                                             pMultiData->pOidValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRMepManAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmRMepManAddress (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_THREE].
                                       u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmRemoteMepDbExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmRemoteMepDbExTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmLtmTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmLtmTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmLtmTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmLtmTargetMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmLtmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = ECFM_MAC_ADDR_LENGTH;
    if (nmhGetFsEcfmLtmTargetMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmLtmTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmLtmTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmLtmTtl (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
                            pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
                            pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
                            pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
                            &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsEcfmMepExTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmMepExTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmMepExTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmXconnRMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmXconnRMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmErrorRMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmErrorRMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectRDICcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepDefectRDICcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectMacStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepDefectMacStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepDefectRemoteCcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepDefectRemoteCcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepDefectErrorCcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepDefectErrorCcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectXconnCcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepDefectXconnCcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepCcmOffloadGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepCcmOffload
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmMepLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrInOutOfOrderGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmMepLbrInOutOfOrder
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrBadMsduGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmMepLbrBadMsdu
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepUnexpLtrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmMepUnexpLtrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsEcfmMepLbrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepCcmSequenceErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepCcmSequenceErrors
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepCciSentCcmsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMepExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepCciSentCcms
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmXconnRMepIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmXconnRMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmErrorRMepIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmErrorRMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectRDICcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepDefectRDICcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectMacStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepDefectMacStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepDefectRemoteCcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepDefectRemoteCcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectErrorCcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepDefectErrorCcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectXconnCcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepDefectXconnCcm
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepCcmOffloadSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepCcmOffload
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmMepLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrInOutOfOrderSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmMepLbrInOutOfOrder
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepLbrBadMsduSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmMepLbrBadMsdu
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepUnexpLtrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmMepUnexpLtrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsEcfmMepLbrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsEcfmMepCcmSequenceErrorsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepCcmSequenceErrors
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepCciSentCcmsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepCciSentCcms
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmXconnRMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmXconnRMepId (pu4Error,
                                    pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                    u4_ULongValue,
                                    pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                    u4_ULongValue,
                                    pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                    u4_ULongValue,
                                    pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmErrorRMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmErrorRMepId (pu4Error,
                                    pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                    u4_ULongValue,
                                    pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                    u4_ULongValue,
                                    pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                    u4_ULongValue,
                                    pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectRDICcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepDefectRDICcm (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectMacStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepDefectMacStatus (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepDefectRemoteCcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepDefectRemoteCcm (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepDefectErrorCcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepDefectErrorCcm (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepDefectXconnCcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepDefectXconnCcm (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepCcmOffloadTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepCcmOffload (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepLbrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmMepLbrIn (pu4Error,
                                 pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                 u4_ULongValue,
                                 pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                 u4_ULongValue,
                                 pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                 u4_ULongValue,
                                 pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepLbrInOutOfOrderTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmMepLbrInOutOfOrder (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrBadMsduTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmMepLbrBadMsdu (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepUnexpLtrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmMepUnexpLtrIn (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepLbrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsEcfmMepLbrOut (pu4Error,
                                  pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                  u4_ULongValue,
                                  pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                  u4_ULongValue,
                                  pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                  u4_ULongValue,
                                  pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepCcmSequenceErrorsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepCcmSequenceErrors (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepCciSentCcmsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepCciSentCcms (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMepExTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmMdExTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmMdExTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmMdExTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepArchiveHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMdExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMepArchiveHoldTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMepArchiveHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMepArchiveHoldTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMepArchiveHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMepArchiveHoldTime (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMdExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMdExTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsEcfmMaExTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsEcfmMaExTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsEcfmMaExTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMaCrosscheckStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsEcfmMaExTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsEcfmMaCrosscheckStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMaCrosscheckStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmMaCrosscheckStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsEcfmMaCrosscheckStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmMaCrosscheckStatus (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmMaExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmMaExTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEcfmTrapControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmTrapControl (pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTrapTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsEcfmTrapType (&(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTrapControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsEcfmTrapControl (pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTrapControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsEcfmTrapControl (pu4Error, pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsEcfmTrapControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEcfmTrapControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
