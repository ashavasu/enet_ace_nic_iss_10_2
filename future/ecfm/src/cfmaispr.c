/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmaispr.c,v 1.18 2011/11/25 11:29:14 siva Exp $
 *
 * Description: This file contains the Functionality of the AIS 
 *              Control Sub Module.
 *******************************************************************/
#include "cfminc.h"

/*******************************************************************************
 * Function           : EcfmCcClntProcessAis
 *
 * Description        : This routine parse AIS Pdu & if valid then processes 
 *                      the received AIS PDU
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwdAis - Pointer to Boolean indicating whether the 
 *                      AIS needs to be forwarded in case the Receving entity 
 *                      is a MHF.
 *                      
 * Output(s)          : pbFrwdAis - Returns status whether Ais Pdu needs to be
 *                      forwarded or not
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcClntProcessAis (tEcfmCcPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdAis)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT1               u1AisInterval = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pStackInfo = pPduSmInfo->pStackInfo;

    if (pStackInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessAis: "
                     "No STACK Information received\r\n");
        return ECFM_FAILURE;
    }
    /* Validate the Value of FirstTLVOffset in AIS PDU */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_INIT_VAL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessAis: "
                     "FirstTLVOffset in AIS PDU is not equal to required value"
                     " so MEP cannot process AIS PDU further\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessAis: "
                     "Port Information is not present\r\n");
        return ECFM_FAILURE;
    }
    /* Get the MEP's MAC Address */
    ECFM_GET_MAC_ADDR_OF_PORT ((ECFM_CC_PORT_INFO (pStackInfo->u2PortNum)->
                                u4IfIndex), MepMacAddr);

    /* Check if the Mac Address received in the AIS is neither same as that of the
     * receiving MEP nor multicast then discard that
     */
    if ((ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr)
         == ECFM_FAILURE) &&
        (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_FALSE))
    {
        /* Discard the AIS frame */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessAis: "
                     "discarding the received AIS frame\r\n");
        if (ECFM_CC_IS_MHF (pStackInfo))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessAis: "
                         "Ais forwarded in case of MIP\r\n");
            *pbFrwdAis = ECFM_TRUE;
        }
        return ECFM_FAILURE;
    }
    else
    {
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr)
            == ECFM_SUCCESS)
        {
            if (ECFM_CC_IS_MHF (pStackInfo))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntProcessAis: "
                             "discarding the received AIS frame\r\n");
                return ECFM_FAILURE;
            }
        }
        if ((ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_TRUE))
        {
            if (ECFM_CC_IS_MHF (pStackInfo))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntProcessAis: "
                             "Ais forwarded in case of MIP\r\n");
                *pbFrwdAis = ECFM_TRUE;
                return ECFM_FAILURE;
            }
        }
    }

    if (pMepInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessAis: "
                     "No MEP Information received\r\n");
        return ECFM_FAILURE;
    }

    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);
    if (pAisInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessAis: "
                     "No AIS realted Information present in MEP\r\n");
        return ECFM_FAILURE;
    }
    /* Stop the AIS while timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_AIS_RXWHILE, pPduSmInfo);

    /* Ais RxWhile Timer is to be started as per the Interval received */
    if (pPduSmInfo->u1RxFlags == ECFM_AIS_LCK_INTERVAL_1_S_FLAG)
    {
        u1AisInterval = ECFM_CC_AIS_LCK_INTERVAL_1_SEC;
    }
    else
    {
        u1AisInterval = ECFM_CC_AIS_LCK_INTERVAL_1_MIN;
    }

    /* Start the Ais RxWhile Timer */
    ECFM_GET_AIS_LCK_INTERVAL (u1AisInterval, u4Interval);
    u4Interval = u4Interval * ECFM_XCHK_DELAY_DEF_VAL;
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_RXWHILE, pPduSmInfo,
                             u4Interval) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessAis:Start Timer FAILED\r\n");
        return ECFM_FAILURE;
    }
#ifdef L2RED_WANTED
    pMepInfo->AisInfo.u4AisRcvdRxWhileValue = u4Interval;
#endif

    /* Set the Ais Condition */
    EcfmCcSetAisCondition (pMepInfo, pPduSmInfo->RxSrcMacAddr,
                           pPduSmInfo->u1RxMdLevel);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcSetAisCondition
 *
 * Description        : This routine is used to set the Ais condition
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *                      SrcMacAddr - Mac address of the AIS generating MEP
 *                      u1MdLevel - MD level of the AIS generating MEP
 *                      
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 ******************************************************************************/
PUBLIC VOID
EcfmCcSetAisCondition (tEcfmCcMepInfo * pMepInfo, tEcfmMacAddr SrcMacAddr,
                       UINT1 u1MdLevel)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcErrLogInfo   LocErrLog;
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmCcRMepDbInfo  *pRMep = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    UNUSED_PARAM (u1MdLevel);
    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;
    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);

    /* Check if AIS condition is already Set or not */
    if (pAisInfo->b1AisCondition == ECFM_TRUE)
    {
        return;
    }
    else
    {
        /* Set Ais Condition. Now Alarm will be suppressed at this level */
        pAisInfo->b1AisCondition = ECFM_TRUE;

        /* Sync AIS Condition in STANBY Node */
        EcfmRedSyncAisCondition (pMepInfo);

        /*Add entry for AIS Condition Defect Entry to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (&PduSmInfo, ECFM_AIS_CONDITION_ENTRY);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_AIS_COND_EN_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);

        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
        /* Notify the registered applications that AIS Condition is Set */
        ECFM_NOTIFY_PROTOCOLS (ECFM_AIS_CONDITION_ENCOUNTERED,
                               &MepInfo, (tMacAddr *) SrcMacAddr,
                               ECFM_AIS_CONDITION_ENCOUNTERED, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_CC_TASK_ID);

        /* Call LOC Exit at Client level */
        if (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE)
        {
            /* Get the first node from the RBtree of RMepDbTable in MepInfo */
            pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepInfo->RMepDb));
            while (pRMep != NULL)
            {
                if (pRMep->b1RMepCcmDefect == ECFM_TRUE)
                {
                    PduSmInfo.pRMepInfo = pRMep;
                    /*Add Error Exit to Error Log Table */
                    LocErrLog.u4MdIndex = pMepInfo->u4MdIndex;
                    LocErrLog.u4MaIndex = pMepInfo->u4MaIndex;
                    LocErrLog.u2MepId = pMepInfo->u2MepId;
                    LocErrLog.u4SeqNum = 0;
                    LocErrLog.u2LogType = ECFM_LOC_DFCT_EXIT;
                    /* Generate the SNMP trap for the fault */
                    Y1731_CC_GENERATE_TRAP (&LocErrLog, Y1731_LOC_TRAP_EX_VAL);

                    EcfmRedSyncSmData (&PduSmInfo);
                }
                /* Get the next node form the tree */
                pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepInfo->RMepDb),
                                                            &(pRMep->
                                                              MepDbDllNode));

            }
        }
        /* Trigger AIS Transmission start  only if CC transmission is NOT
         * enabled */
        if (pMepInfo->CcInfo.b1CciEnabled != ECFM_TRUE)
        {
            ECFM_CC_AIS_TRIGGER_START (pMepInfo);
        }
        EcfmRedSyncSmData (&PduSmInfo);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcClearAisCondition
 *
 * Description        : This routine is used to clear the Ais condition
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 ******************************************************************************/
PUBLIC VOID
EcfmCcClearAisCondition (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    if (pMepInfo == NULL)
    {
        return;
    }
    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;

    /*Ais Exit Trap is raised when Y.1731 is enabled */
    if (pMepInfo->AisInfo.b1AisCondition == ECFM_TRUE)
    {
        /* Clear Ais Condition. Now it will not suppress Alarm at this level */
        pMepInfo->AisInfo.b1AisCondition = ECFM_FALSE;

        /* Sync AIS Condition in STANDBY Node */
        EcfmRedSyncAisCondition (pMepInfo);

        /* Add entry for AIS Condition Defect Exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (&PduSmInfo, ECFM_AIS_CONDITION_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_AIS_COND_EX_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);

        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
        /* Notify the registered applications that AIS Condition is Clear */
        ECFM_NOTIFY_PROTOCOLS (ECFM_AIS_CONDITION_CLEARED, &MepInfo, NULL,
                               ECFM_AIS_CONDITION_CLEARED, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_CC_TASK_ID);

        /* Trigger AIS Transmission stop */
        ECFM_CC_AIS_TRIGGER_STOP (pMepInfo);

        EcfmRedSyncSmData (&PduSmInfo);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  End of File cfmaispr.c
 ******************************************************************************/
