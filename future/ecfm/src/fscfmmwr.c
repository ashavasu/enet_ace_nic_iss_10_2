/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved]
*
* $Id: fscfmmwr.c,v 1.15 2011/06/22 09:04:17 siva Exp $
*
* Description: This file contains the wrapper routines for
*              proprietary mib.
*******************************************************************/

# include  "cfminc.h"
# include  "fscfmmdb.h"

VOID
RegisterFSCFMM ()
{
    SNMPRegisterMib (&fscfmmOID, &fscfmmEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fscfmmOID, (const UINT1 *) "fscfmmi");
}

VOID
UnRegisterFSCFMM ()
{
    SNMPUnRegisterMib (&fscfmmOID, &fscfmmEntry);
    SNMPDelSysorEntry (&fscfmmOID, (const UINT1 *) "fscfmmi");
}

INT4
FsMIEcfmGlobalTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsMIEcfmGlobalTrace (&(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmOuiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhGetFsMIEcfmOui (pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmGlobalTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmGlobalTrace (pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmOuiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmOui (pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmGlobalTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmGlobalTrace (pu4Error, pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmOuiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmOui (pu4Error, pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmGlobalTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmGlobalTrace
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIEcfmOuiDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmOui (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmContextTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmContextTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmContextTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIEcfmSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmSystemControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmModuleStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdDefLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDefaultMdDefMhfCreationGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdDefMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefIdPermissionGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdDefIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdTableNextIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdTableNextIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmLtrCacheStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrCacheClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmLtrCacheClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmLtrCacheHoldTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmLtrCacheSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmDbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipCcmDbStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipCcmDbClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipCcmDbClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipCcmDbSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipCcmDbSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipCcmDbHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipCcmDbHoldTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMemoryFailureCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMemoryFailureCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmBufferFailureCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmBufferFailureCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmUpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmUpCount (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDownCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDownCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmNoDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmNoDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRdiDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRdiDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMacStatusDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMacStatusDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRemoteCcmDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRemoteCcmDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmErrorCcmDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmErrorCcmDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmXconDftCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmXconDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmCrosscheckDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmCrosscheckDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipDynamicEvaluationStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipDynamicEvaluationStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmContextNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmContextName (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmTrapControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmTrapControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmTrapTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmTrapType (pMultiIndex->pIndex[0].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmTraceOption (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmGlobalCcmOffloadGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetFsMIEcfmGlobalCcmOffload (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmContextTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhSetFsMIEcfmSystemControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmModuleStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDefaultMdDefLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDefaultMdDefMhfCreationSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDefaultMdDefMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefIdPermissionSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDefaultMdDefIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmLtrCacheStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrCacheClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmLtrCacheClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmLtrCacheHoldTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrCacheSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmLtrCacheSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipCcmDbStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipCcmDbStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmDbClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipCcmDbClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipCcmDbSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipCcmDbSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmDbHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipCcmDbHoldTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMemoryFailureCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMemoryFailureCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmBufferFailureCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmBufferFailureCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmUpCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmUpCount (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDownCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDownCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmNoDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmNoDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRdiDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRdiDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMacStatusDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMacStatusDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRemoteCcmDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRemoteCcmDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmErrorCcmDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmErrorCcmDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmXconDftCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmXconDftCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmCrosscheckDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmCrosscheckDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipDynamicEvaluationStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipDynamicEvaluationStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmTrapControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmTrapControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmTraceOption (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmGlobalCcmOffloadSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmGlobalCcmOffload (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmSystemControl (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmModuleStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDefaultMdDefLevel (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDefaultMdDefMhfCreation (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdDefIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDefaultMdDefIdPermission (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmLtrCacheStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmLtrCacheClear (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmLtrCacheHoldTime (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrCacheSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmLtrCacheSize (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmDbStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipCcmDbStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipCcmDbClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipCcmDbClear (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmDbSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipCcmDbSize (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmDbHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipCcmDbHoldTime (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMemoryFailureCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMemoryFailureCount (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmBufferFailureCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmBufferFailureCount (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmUpCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmUpCount (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDownCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDownCount (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmNoDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmNoDftCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRdiDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRdiDftCount (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMacStatusDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMacStatusDftCount (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRemoteCcmDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRemoteCcmDftCount (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmErrorCcmDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmErrorCcmDftCount (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmXconDftCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmXconDftCount (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmCrosscheckDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmCrosscheckDelay (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipDynamicEvaluationStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipDynamicEvaluationStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmTrapControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmTrapControl (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmTraceOption (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmGlobalCcmOffloadTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmGlobalCcmOffload (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmContextTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmContextTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmPortTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortLLCEncapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortLLCEncapStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortModuleStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortTxCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortTxCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortTxCcmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortTxCcmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortTxLbmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortTxLbmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortTxLbrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortTxLtmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortTxLtmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortTxLtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortTxLtrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortTxFailedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortTxFailedCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortRxCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortRxCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortRxCcmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortRxCcmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLbmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortRxLbmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortRxLbrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLtmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortRxLtmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue))
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmPortRxLtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmPortRxLtrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxBadCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortRxBadCfmPduCount
        (pMultiIndex->pIndex[0].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortFrwdCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortFrwdCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortDsrdCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmPortDsrdCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortLLCEncapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortLLCEncapStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortModuleStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortTxCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxCcmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortTxCcmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLbmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortTxLbmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLbrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortTxLbrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLtmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortTxLtmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLtrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortTxLtrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxFailedCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortTxFailedCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortRxCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxCcmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortRxCcmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLbmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortRxLbmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLbrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortRxLbrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLtmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortRxLtmCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLtrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmPortRxLtrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxBadCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortRxBadCfmPduCount
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortFrwdCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortFrwdCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortDsrdCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmPortDsrdCfmPduCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortLLCEncapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortLLCEncapStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortModuleStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxCfmPduCount (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxCcmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxCcmCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLbmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxLbmCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLbrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxLbrCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLtmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxLtmCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxLtrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxLtrCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTxFailedCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortTxFailedCount (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxCfmPduCount (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxCcmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxCcmCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLbmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxLbmCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLbrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxLbrCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLtmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxLtmCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxLtrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxLtrCount (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortRxBadCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortRxBadCfmPduCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortFrwdCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortFrwdCfmPduCount (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortDsrdCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmPortDsrdCfmPduCount (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmPortTable (pu4Error, pSnmpIndexList,
                                       pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmStackTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmStackTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmStackTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmStackMdIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmStackMdIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmStackMaIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmStackMaIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmStackMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmStackMepId (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].i4_SLongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmStackMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmStackMacAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       (tMacAddr *) pMultiData->pOctetStrValue->
                                       pu1_OctetList) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIEcfmVlanTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmVlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmVlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmVlanPrimaryVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmVlanPrimaryVid (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmVlanRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmVlanRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmVlanPrimaryVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmVlanPrimaryVid (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmVlanRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmVlanRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmVlanPrimaryVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmVlanPrimaryVid (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmVlanRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmVlanRowStatus (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmVlanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmVlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmDefaultMdTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmDefaultMdTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmDefaultMdTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDefaultMdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdIdPermissionGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmDefaultMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDefaultMdIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDefaultMdLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDefaultMdMhfCreation
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdIdPermissionSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDefaultMdIdPermission
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDefaultMdLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDefaultMdLevel (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDefaultMdMhfCreation (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDefaultMdIdPermission (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDefaultMdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmDefaultMdTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmConfigErrorListTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmConfigErrorListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmConfigErrorListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmConfigErrorListErrorTypeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmConfigErrorListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmConfigErrorListErrorType
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIEcfmMdTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMdTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMdTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdName (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdMdLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdMdLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdMhfCreation (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMhfIdPermissionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdMhfIdPermission (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMaTableNextIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdMaTableNextIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMdRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMdFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMdName (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMdLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMdMdLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMdMhfCreation (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMhfIdPermissionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMdMhfIdPermission (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMdRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMdFormat (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMdName (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdMdLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMdMdLevel (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMdMhfCreation (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdMhfIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMdMhfIdPermission (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMdRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMdTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmMaTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMaTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMaTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMaPrimaryVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaPrimaryVlanId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaName (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiIndex->pIndex[2].u4_ULongValue,
                              pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaMhfCreation (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaIdPermissionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaIdPermission (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaCcmIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaCcmInterval (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaNumberOfVidsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaNumberOfVids (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMaRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaPrimaryVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaPrimaryVlanId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaName (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiIndex->pIndex[2].u4_ULongValue,
                              pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaMhfCreation (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaIdPermissionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaIdPermission (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaCcmIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaCcmInterval (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaPrimaryVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaPrimaryVlanId (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMaFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaFormat (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaName (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaMhfCreation (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMaIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaIdPermission (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaCcmIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaCcmInterval (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMaTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmMaMepListTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMaMepListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMaMepListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMaMepListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMaMepListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaMepListRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaMepListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaMepListRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaMepListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaMepListRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaMepListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMaMepListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmMepTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMepTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMepTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDirection (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepPrimaryVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepPrimaryVid (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepActive (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepFngStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepFngState (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCciEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepCciEnabled (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmLtmPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepCcmLtmPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmMepMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     (tMacAddr *) pMultiData->pOctetStrValue->
                                     pu1_OctetList) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLowPrDefGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepLowPrDef (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepFngAlarmTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepFngAlarmTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepFngResetTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepFngResetTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepHighestPrDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepHighestPrDefect (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDefects (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepErrorCcmLastFailureGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepErrorCcmLastFailure
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepXconCcmLastFailureGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepXconCcmLastFailure
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmSequenceErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepCcmSequenceErrors
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepCciSentCcmsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepCciSentCcms (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepNextLbmTransIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepNextLbmTransId (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepLbrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrInOutOfOrderGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepLbrInOutOfOrder (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrBadMsduGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepLbrBadMsdu (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepLtmNextSeqNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepLtmNextSeqNumber (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepUnexpLtrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepUnexpLtrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepLbrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepLbrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLbmStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestMacAddressGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmMepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestMepIdGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmDestMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestIsMepIdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmDestIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmMessagesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmMessages
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDataTlvGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmDataTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmVlanPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLbmVlanDropEnableGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmVlanDropEnable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmResultOKGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmResultOK
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmSeqNumberGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLbmSeqNumber
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmFlagsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmFlags (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmTargetMacAddressGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmMepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTargetMepIdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTargetIsMepIdGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmTtl (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmResultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmResult
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmSeqNumberGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmSeqNumber
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmMepTransmitLtmEgressIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmOffloadGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepCcmOffload (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepDirection (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepPrimaryVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepPrimaryVid (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepActiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepActive (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCciEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepCciEnabled (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmLtmPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepCcmLtmPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepLowPrDefSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepLowPrDef (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepFngAlarmTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepFngAlarmTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepFngResetTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepFngResetTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepCcmSequenceErrorsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepCcmSequenceErrors
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepCciSentCcmsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepCciSentCcms (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepLbrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrInOutOfOrderSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepLbrInOutOfOrder (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrBadMsduSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepLbrBadMsdu (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepUnexpLtrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepUnexpLtrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepLbrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestMepIdSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmDestMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestIsMepIdSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmDestIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLbmMessagesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmMessages
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLbmDataTlvSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmDataTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmVlanPriority
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmVlanDropEnableSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLbmVlanDropEnable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmFlagsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmFlags (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTargetMacAddressSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmTargetMepIdSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTargetIsMepIdSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmTtlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmTtl (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmEgressIdentifierSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmMepTransmitLtmEgressIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmOffloadSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepCcmOffload (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepIfIndex (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepDirection (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepPrimaryVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepPrimaryVid (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepActiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepActive (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCciEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepCciEnabled (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmLtmPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepCcmLtmPriority (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLowPrDefTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepLowPrDef (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepFngAlarmTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepFngAlarmTime (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepFngResetTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepFngResetTime (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmSequenceErrorsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepCcmSequenceErrors (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepCciSentCcmsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepCciSentCcms (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepLbrIn (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrInOutOfOrderTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepLbrInOutOfOrder (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepLbrBadMsduTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepLbrBadMsdu (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepUnexpLtrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepUnexpLtrIn (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepLbrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepLbrOut (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLbmDestMacAddressTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIEcfmMepTransmitLbmDestMacAddress (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[3].
                                                       u4_ULongValue,
                                                       (*(tMacAddr *)
                                                        pMultiData->
                                                        pOctetStrValue->
                                                        pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLbmDestMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmDestMepId (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDestIsMepIdTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmDestIsMepId (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmMessagesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmMessages (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmDataTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmDataTlv (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[3].
                                                u4_ULongValue,
                                                pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmVlanPriorityTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmVlanPriority (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLbmVlanDropEnableTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLbmVlanDropEnable (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[3].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLtmStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmFlagsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{

    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLtmFlags (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[3].
                                              u4_ULongValue,
                                              pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTargetMacAddressTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        ECFM_LBLT_UNLOCK ();
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIEcfmMepTransmitLtmTargetMacAddress (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         u4_ULongValue,
                                                         (*(tMacAddr *)
                                                          pMultiData->
                                                          pOctetStrValue->
                                                          pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmTargetMepIdTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLtmTargetMepId (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTransmitLtmTargetIsMepIdTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLtmTargetIsMepId (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmTtlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLtmTtl (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepTransmitLtmEgressIdentifierTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmMepTransmitLtmEgressIdentifier (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepRowStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepCcmOffloadTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepCcmOffload (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMepTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmLtrTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmLtrTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmLtrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrTtl (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiIndex->pIndex[2].u4_ULongValue,
                              pMultiIndex->pIndex[3].u4_ULongValue,
                              pMultiIndex->pIndex[4].u4_ULongValue,
                              pMultiIndex->pIndex[5].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrForwardedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrForwarded (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].u4_ULongValue,
                                    pMultiIndex->pIndex[5].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrTerminalMepGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrTerminalMep (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiIndex->pIndex[5].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrLastEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrLastEgressIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrNextEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrNextEgressIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrRelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrRelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                pMultiIndex->pIndex[4].u4_ULongValue,
                                pMultiIndex->pIndex[5].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrChassisIdSubtype (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrChassisId (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].u4_ULongValue,
                                    pMultiIndex->pIndex[5].u4_ULongValue,
                                    pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrManAddressDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrManAddressDomain (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].u4_ULongValue,
                                           pMultiData->pOidValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrManAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrManAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     pMultiIndex->pIndex[5].u4_ULongValue,
                                     pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrIngressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrIngress (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiIndex->pIndex[4].u4_ULongValue,
                                  pMultiIndex->pIndex[5].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrIngressMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmLtrIngressMac (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     pMultiIndex->pIndex[5].u4_ULongValue,
                                     (tMacAddr *) pMultiData->pOctetStrValue->
                                     pu1_OctetList) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrIngressPortIdSubtypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrIngressPortIdSubtype
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrIngressPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrIngressPortId (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiIndex->pIndex[5].u4_ULongValue,
                                        pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrEgressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrEgress (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 pMultiIndex->pIndex[4].u4_ULongValue,
                                 pMultiIndex->pIndex[5].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrEgressMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmLtrEgressMac (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].u4_ULongValue,
                                    pMultiIndex->pIndex[5].u4_ULongValue,
                                    (tMacAddr *) pMultiData->pOctetStrValue->
                                    pu1_OctetList) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrEgressPortIdSubtypeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrEgressPortIdSubtype
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmLtrEgressPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrEgressPortId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiIndex->pIndex[5].u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtrOrganizationSpecificTlvGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtrOrganizationSpecificTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiData->pOctetStrValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsMIEcfmMepDbTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMepDbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMepDbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepDbRMepStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbRMepState (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbRMepFailedOkTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbRMepFailedOkTime
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmMepDbMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       (tMacAddr *) pMultiData->pOctetStrValue->
                                       pu1_OctetList) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbRdiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbRdi (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                pMultiIndex->pIndex[4].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbPortStatusTlvGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbPortStatusTlv (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbInterfaceStatusTlvGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbInterfaceStatusTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbChassisIdSubtypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbChassisIdSubtype
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbChassisId (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbManAddressDomainGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbManAddressDomain
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDbManAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDbManAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIEcfmMipTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMipTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMipTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMipTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipActive (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMipTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipActiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipActive (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMipRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipActiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipActive (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMipRowStatus (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMipTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMipTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmDynMipPreventionTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmDynMipPreventionTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmDynMipPreventionTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDynMipPreventionRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmDynMipPreventionTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDynMipPreventionRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDynMipPreventionRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDynMipPreventionRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmDynMipPreventionRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDynMipPreventionRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDynMipPreventionTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmDynMipPreventionTable (pu4Error,
                                                   pSnmpIndexList,
                                                   pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmMipCcmDbTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMipCcmDbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMipCcmDbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMipCcmIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMipCcmDbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {

        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMipCcmIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     (*(tMacAddr *) pMultiIndex->pIndex[2].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIEcfmRemoteMepDbExTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmRemoteMepDbExTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmRemoteMepDbExTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepCcmSequenceNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepCcmSequenceNum (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepPortStatusDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepPortStatusDefect
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepInterfaceStatusDefectGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepInterfaceStatusDefect
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepCcmDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepCcmDefect (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepRDIDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepRDIDefect (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmRMepMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      (tMacAddr *) pMultiData->pOctetStrValue->
                                      pu1_OctetList) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepRdiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepRdi (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].u4_ULongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               pMultiIndex->pIndex[4].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepPortStatusTlvGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepPortStatusTlv (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepInterfaceStatusTlvGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepInterfaceStatusTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepChassisIdSubtype
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepDbChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepDbChassisId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepManAddressDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepManAddressDomain
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepManAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRMepManAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepPortStatusDefectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepPortStatusDefect
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepInterfaceStatusDefectSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepInterfaceStatusDefect
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepCcmDefectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepCcmDefect (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepRDIDefectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepRDIDefect (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepMacAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      (*(tMacAddr *) pMultiData->
                                       pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepRdiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepRdi (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].u4_ULongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               pMultiIndex->pIndex[4].u4_ULongValue,
                               pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepPortStatusTlvSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepPortStatusTlv (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepInterfaceStatusTlvSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepInterfaceStatusTlv
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepChassisIdSubtypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepChassisIdSubtype
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepDbChassisIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepDbChassisId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepManAddressDomainSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepManAddressDomain
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiData->pOidValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepManAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRMepManAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepPortStatusDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepPortStatusDefect (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepInterfaceStatusDefectTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepInterfaceStatusDefect (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[4].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepCcmDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepCcmDefect (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepRDIDefectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepRDIDefect (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsMIEcfmRMepMacAddress (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].u4_ULongValue,
                                         (*(tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepRdiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepRdi (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  pMultiIndex->pIndex[4].u4_ULongValue,
                                  pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepPortStatusTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepPortStatusTlv (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[4].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRMepInterfaceStatusTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepInterfaceStatusTlv (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[4].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepChassisIdSubtypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepChassisIdSubtype (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepDbChassisIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepDbChassisId (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepManAddressDomainTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepManAddressDomain (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].
                                               u4_ULongValue,
                                               pMultiData->pOidValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRMepManAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRMepManAddress (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].u4_ULongValue,
                                         pMultiData->pOctetStrValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmRemoteMepDbExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmRemoteMepDbExTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmLtmTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmLtmTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmLtmTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtmTargetMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetFsMIEcfmLtmTargetMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           (tMacAddr *) pMultiData->
                                           pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmLtmTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmLtmTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmLtmTtl (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiIndex->pIndex[2].u4_ULongValue,
                              pMultiIndex->pIndex[3].u4_ULongValue,
                              pMultiIndex->pIndex[4].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMIEcfmMepExTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMepExTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMepExTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmXconnRMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmXconnRMepId (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmErrorRMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmErrorRMepId (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectRDICcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDefectRDICcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectMacStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDefectMacStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectRemoteCcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDefectRemoteCcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectErrorCcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDefectErrorCcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectXconnCcmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMepExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepDefectXconnCcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmXconnRMepIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmXconnRMepId (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmErrorRMepIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmErrorRMepId (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectRDICcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepDefectRDICcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectMacStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepDefectMacStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectRemoteCcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepDefectRemoteCcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectErrorCcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepDefectErrorCcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectXconnCcmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepDefectXconnCcm (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmXconnRMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmXconnRMepId (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmErrorRMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmErrorRMepId (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepDefectRDICcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepDefectRDICcm (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepDefectMacStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepDefectMacStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectRemoteCcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepDefectRemoteCcm (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepDefectErrorCcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepDefectErrorCcm (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepDefectXconnCcmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepDefectXconnCcm (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMepExTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmMdExTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMdExTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMdExTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepArchiveHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmMdExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMepArchiveHoldTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMepArchiveHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMepArchiveHoldTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMepArchiveHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMepArchiveHoldTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMdExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMdExTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmMaExTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmMaExTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmMaExTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmMaCrosscheckStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceFsMIEcfmMaExTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmMaCrosscheckStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaCrosscheckStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmMaCrosscheckStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaCrosscheckStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmMaCrosscheckStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIEcfmMaExTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmMaExTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIEcfmStatsTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIEcfmStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIEcfmStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmTxCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxCcmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmTxCcmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLbmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmTxLbmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmTxLbrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLtmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmTxLtmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmTxLtrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxFailedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {

        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmTxFailedCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRxCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxCcmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRxCcmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLbmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmRxLbmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmRxLbrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLtmCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmRxLtmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {

        ECFM_CC_UNLOCK ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIEcfmRxLtrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxBadCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmRxBadCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmFrwdCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmFrwdCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDsrdCfmPduCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIEcfmStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetFsMIEcfmDsrdCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmTxCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxCcmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmTxCcmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLbmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmTxLbmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLbrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmTxLbrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLtmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmTxLtmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLtrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmTxLtrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxFailedCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmTxFailedCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRxCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxCcmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRxCcmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLbmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmRxLbmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLbrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmRxLbrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLtmCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmRxLtmCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLtrCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIEcfmRxLtrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxBadCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmRxBadCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmFrwdCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmFrwdCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDsrdCfmPduCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIEcfmDsrdCfmPduCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmTxCfmPduCount (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxCcmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmTxCcmCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLbmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmTxLbmCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLbrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmTxLbrCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLtmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmTxLtmCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxLtrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmTxLtrCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmTxFailedCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmTxFailedCount (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRxCfmPduCount (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxCcmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRxCcmCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLbmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmRxLbmCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLbrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmRxLbrCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLtmCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmRxLtmCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxLtrCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIEcfmRxLtrCount (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmRxBadCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmRxBadCfmPduCount (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmFrwdCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmFrwdCfmPduCount (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmDsrdCfmPduCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIEcfmDsrdCfmPduCount (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIEcfmStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIEcfmStatsTable (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds));
}
