/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmcctmr.c,v 1.23 2016/09/17 12:41:26 siva Exp $
 *
 * Description: This file contains the routines for Timer 
 *                        functionality in ECFM's CC Task.
 *******************************************************************/

#include "cfminc.h"

PRIVATE VOID EcfmCcTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID EcfmCcTmrCciExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrErrCcmExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrXConCcmExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrFngExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrFngResetExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrMipDbHoldExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrMepArchiveHoldExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrRdiExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrLmWhileExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrLmDeadLineExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrAisExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrAisIntervalExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrAisRxExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrLckPeriodExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrLckIntervalExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrLckRxExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrLckDelayExpired PROTO ((VOID *pArgs));
PRIVATE VOID EcfmCcTmrCcRMepDbExpired PROTO ((VOID *pArgs));
/**************************************************************************
 * Function Name      : EcfmCcTmrInit                                      *
 *                                                                         *
 * Description        : This function creates the timer List for CC Task.  *
 *                                                                         *
 * Input(s)           : None                                               *
 *                                                                         *
 * Output(s)          : None                                               *
 *                                                                         *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                        *
 **************************************************************************/
PUBLIC INT4
EcfmCcTmrInit ()
{
    /* CC Timer Initialization */
    if (ECFM_CREATE_TMR_LIST
        ((UINT1 *) ECFM_CC_TASK_NAME, ECFM_EV_TMR_EXP, NULL,
         &(ECFM_CC_TMRLIST_ID)) == ECFM_TMR_FAILURE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "EcfmCcTaskInit:Timer list creation FAILED"
                      "for CC Task \r\n");
        return ECFM_FAILURE;
    }
    EcfmCcTmrInitTmrDesc ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * Function Name      : EcfmCcTmrDeInit                                    *
 *                                                                         *
 * Description        : This function deletes the timer list created for CC*
 *                      Task.                                               *
 *                                                                         *
 * Input(s)           : None                                               *
 *                                                                         *
 * Output(s)          : None                                               *
 *                                                                         *
 * Return Value(s)    : None                                               *
 **************************************************************************/
PUBLIC VOID
EcfmCcTmrDeInit ()
{

    if (ECFM_CC_TMRLIST_ID != ECFM_INIT_VAL)
    {
        /* Deleting the Timer List */
        if (ECFM_DELETE_TMR_LIST (ECFM_CC_TMRLIST_ID) == ECFM_TMR_FAILURE)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                          ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                          ECFM_OS_RESOURCE_TRC,
                          "EcfmCcTmrDeInit:"
                          "Timer list deletion FAILED for CC Task \r\n");
            return;
        }
        ECFM_CC_TMRLIST_ID = ECFM_INIT_VAL;
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcTmrDeInit:CC Task Timers DeInitialized \r\n");
    }
    return;
}

/**************************************************************************
 * Function Name      : EcfmCcTmrInitTmrDesc                               *
 *                                                                         *
 * Description        : This function creates a timer list for all the     *
 *                      timers of ECFM CC Task.                            *
 *                                                                         *
 * Input(s)           : None                                               *
 *                                                                         *
 * Output(s)          : None                                               *
 *                                                                         *
 * Return Value(s)    : None                                               *
 **************************************************************************/
PRIVATE VOID
EcfmCcTmrInitTmrDesc (VOID)
{
    INT2                i2CcInfoOffset = ECFM_INIT_VAL;
    INT2                i2FngInfoOffset = ECFM_INIT_VAL;
    INT2                i2LmInfoOffset = ECFM_INIT_VAL;
    INT2                i2AisInfoOffset = ECFM_INIT_VAL;
    INT2                i2LckInfoOffset = ECFM_INIT_VAL;

    i2CcInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmCcMepInfo, CcInfo));
    i2FngInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmCcMepInfo, FngInfo));
    i2LmInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmCcMepInfo, LmInfo));
    i2AisInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmCcMepInfo, AisInfo));
    i2LckInfoOffset = (INT2) (FSAP_OFFSETOF (tEcfmCcMepInfo, LckInfo));

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_CCI_WHILE].i2Offset =
        (i2CcInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcMepCcInfo,
                                                 CciWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_CCI_WHILE].TmrExpFn =
        EcfmCcTmrCciExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_ERR_CCM_WHILE].i2Offset =
        (i2CcInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcMepCcInfo,
                                                 ErrCCMWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_ERR_CCM_WHILE].TmrExpFn =
        EcfmCcTmrErrCcmExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_XCON_CCM_WHILE].i2Offset =
        (i2CcInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcMepCcInfo, XconCCMWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_XCON_CCM_WHILE].TmrExpFn =
        EcfmCcTmrXConCcmExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_FNG_WHILE].i2Offset =
        (i2FngInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcFngInfo, FngAlarmWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_FNG_WHILE].TmrExpFn =
        EcfmCcTmrFngExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_FNG_RESET_WHILE].i2Offset =
        (i2FngInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcFngInfo, FngResetWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_FNG_RESET_WHILE].TmrExpFn =
        EcfmCcTmrFngResetExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_MIP_DB_HOLD].i2Offset =
        (INT2) (FSAP_OFFSETOF (tEcfmCcContextInfo, MipDbHoldTimer));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_MIP_DB_HOLD].TmrExpFn =
        EcfmCcTmrMipDbHoldExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_MEP_ARCHIVE_HOLD].i2Offset =
        ((INT2) (FSAP_OFFSETOF (tEcfmCcMdInfo, MepArchiveHoldTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_MEP_ARCHIVE_HOLD].TmrExpFn =
        EcfmCcTmrMepArchiveHoldExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_RDI_PERIOD].i2Offset =
        (i2CcInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcMepCcInfo, RdiPeriodTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_RDI_PERIOD].TmrExpFn =
        EcfmCcTmrRdiExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LM_WHILE].i2Offset =
        (i2LmInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcLmInfo, LmmInitWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LM_WHILE].TmrExpFn =
        EcfmCcTmrLmWhileExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LM_DEADLINE].i2Offset =
        (i2LmInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcLmInfo, LmInitDeadlineTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LM_DEADLINE].TmrExpFn =
        EcfmCcTmrLmDeadLineExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_AIS_PERIOD].i2Offset =
        (i2AisInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcAisInfo, AisPeriod)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_AIS_PERIOD].TmrExpFn =
        EcfmCcTmrAisExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_AIS_INTERVAL].i2Offset =
        (i2AisInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcAisInfo, AisInterval)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_AIS_INTERVAL].TmrExpFn =
        EcfmCcTmrAisIntervalExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_AIS_RXWHILE].i2Offset =
        (i2AisInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcAisInfo, AisRxWhile)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_AIS_RXWHILE].TmrExpFn =
        EcfmCcTmrAisRxExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_PERIOD].i2Offset =
        (i2LckInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcLckInfo, LckPeriod)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_PERIOD].TmrExpFn =
        EcfmCcTmrLckPeriodExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_INTERVAL].i2Offset =
        (i2LckInfoOffset + (INT2)
         (FSAP_OFFSETOF (tEcfmCcLckInfo, LckInterval)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_INTERVAL].TmrExpFn =
        EcfmCcTmrLckIntervalExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_RXWHILE].i2Offset =
        (i2LckInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcLckInfo, LckRxWhile)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_RXWHILE].TmrExpFn =
        EcfmCcTmrLckRxExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_DELAY].i2Offset =
        (i2LckInfoOffset + (INT2) (FSAP_OFFSETOF (tEcfmCcLckInfo, LckDelay)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_LCK_DELAY].TmrExpFn =
        EcfmCcTmrLckDelayExpired;

    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_RMEP_WHILE].i2Offset =
        ((INT2) (FSAP_OFFSETOF (tEcfmCcRMepDbInfo, RMepWhileTimer)));
    gEcfmCcGlobalInfo.aTmrDesc[ECFM_CC_TMR_RMEP_WHILE].TmrExpFn =
        EcfmCcTmrCcRMepDbExpired;

    return;
}

/****************************************************************************
 * Function Name      : EcfmCcTmrStartTimer                                  *
 *                                                                           *
 * Description        : This function is called to start timer for CC Task.  *
 *                                                                           *
 * Input(s)           : u1TimerType - Type of timer ot be started.           *
 *                      pPduSmInfo  - PduSmInfo contains information about   * 
 *                                    MEPs RMEPs, and other information      *
 *                      u4Duration  - Duration for which the timer needs to  *
 *                                    be started in units of msec            *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/

PUBLIC INT4
EcfmCcTmrStartTimer (UINT1 u1TimerType, tEcfmCcPduSmInfo * pPduSmInfo,
                     UINT4 u4Duration)
{
    tTmrBlk            *pEcfmTmrBlk = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT4               u4IntervalSec = ECFM_INIT_VAL;
    UINT4               u4IntervalMSec = ECFM_INIT_VAL;
#ifdef L2RED_WANTED
    UINT4               u4Interval = ECFM_INIT_VAL;
#endif
    INT4                i4RetVal = ECFM_SUCCESS;
    INT2                i2Offset = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        /*Start the timers only in Active Node */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC, "EcfmCcTmrStartTimer:"
                     "TIMER NOT STARTED: Start the timers only in Active Node \r\n");
        return ECFM_SUCCESS;
    }

    if (u4Duration == 0)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC, "EcfmCcTmrStartTimer"
                     "Trying to Start Timer for Zero Duration \r\n");
        return ECFM_FAILURE;
    }

    switch (u1TimerType)
    {
        case ECFM_CC_TMR_CCI_WHILE:
        case ECFM_CC_TMR_ERR_CCM_WHILE:
        case ECFM_CC_TMR_XCON_CCM_WHILE:
        case ECFM_CC_TMR_FNG_WHILE:
        case ECFM_CC_TMR_FNG_RESET_WHILE:
        case ECFM_CC_TMR_RDI_PERIOD:
        case ECFM_CC_TMR_AIS_PERIOD:
        case ECFM_CC_TMR_AIS_INTERVAL:
        case ECFM_CC_TMR_AIS_RXWHILE:
        case ECFM_CC_TMR_LCK_PERIOD:
        case ECFM_CC_TMR_LCK_INTERVAL:
        case ECFM_CC_TMR_LCK_RXWHILE:
        case ECFM_CC_TMR_LCK_DELAY:
        case ECFM_CC_TMR_LM_WHILE:
        case ECFM_CC_TMR_LM_DEADLINE:
            pMepInfo = pPduSmInfo->pMepInfo;
            i2Offset = gEcfmCcGlobalInfo.aTmrDesc[u1TimerType].i2Offset;
            pEcfmTmrBlk =
                (tTmrBlk *) (VOID *) ((UINT1 *) (pMepInfo) + i2Offset);
            break;

        case ECFM_CC_TMR_RMEP_WHILE:
            pRMepInfo = pPduSmInfo->pRMepInfo;
            i2Offset = gEcfmCcGlobalInfo.aTmrDesc[u1TimerType].i2Offset;
            pEcfmTmrBlk =
                (tTmrBlk *) (VOID *) ((UINT1 *) (pRMepInfo) + i2Offset);
            break;

        case ECFM_CC_TMR_MIP_DB_HOLD:
            pEcfmTmrBlk = &(ECFM_CC_TMR_MIP_DB_HOLD_TMR);
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcTmrStartTimer:"
                         "Starting MIP CCM DB Hold Timer \r\n");
            break;

        case ECFM_CC_TMR_MEP_ARCHIVE_HOLD:
            pEcfmTmrBlk = &(pPduSmInfo->pMdInfo->MepArchiveHoldTimer);
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcTmrStartTimer:"
                         "Starting MEP Archive Timer \r\n");
            break;

        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcTmrStartTimer:Invalid timer type \n");
            i4RetVal = ECFM_FAILURE;
            break;
    }

    if (i4RetVal == ECFM_FAILURE)
    {
        return i4RetVal;
    }

    /* Converting Time Interval(in MSec) to Sec and MSec */
    u4IntervalSec = (u4Duration / 1000);
    u4IntervalMSec = (u4Duration % 1000);

    /* Start Timer */
    if (TmrStart (ECFM_CC_TMRLIST_ID, pEcfmTmrBlk, u1TimerType, u4IntervalSec,
                  u4IntervalMSec) == ECFM_TMR_FAILURE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC, "EcfmCcTmrStartTimer: ERROR "
                     " Starting Timer for CC Task  \r\n");
        return ECFM_FAILURE;
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the Timestamp 
     * on reception of this msg*/
    if ((u1TimerType == ECFM_CC_TMR_FNG_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_FNG_RESET_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_ERR_CCM_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_XCON_CCM_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_RDI_PERIOD) ||
        (u1TimerType == ECFM_CC_TMR_LCK_PERIOD) ||
        (u1TimerType == ECFM_CC_TMR_RMEP_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_AIS_PERIOD))
    {
        /* Converting Time Interval(in Sec) to System Timer Ticks 
         * (1s=1000ms, and 1s=10 Timer Ticks) */
        u4Interval = ECFM_CONVERT_MSEC_TO_TIME_TICKS (u4Duration);

        /*Synch start of timer */
        EcfmRedSyncTmr (ECFM_RED_SYNC_START_TMR,
                        u1TimerType, pPduSmInfo, u4Interval);
    }
#endif
    return i4RetVal;
}

/****************************************************************************
 * Function Name      : EcfmCcTmrStopTimer                                  *
 *                                                                           *
 * Description        : This function is called to stop the timer for CC     *
 *                      task                                                 *
 *                                                                           *
 * Input(s)           : u1TimerType - Type of timer ot be started.           *
 *                      pPduSmInfo  - PduSmInfo contains information about   *
 *                                    MEPs RMEPs, and other information      *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                           *
 ****************************************************************************/
PUBLIC INT4
EcfmCcTmrStopTimer (UINT1 u1TimerType, tEcfmCcPduSmInfo * pPduSmInfo)
{
    tTmrBlk            *pEcfmTmrBlk = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    INT2                i2Offset = ECFM_INIT_VAL;

    switch (u1TimerType)
    {
        case ECFM_CC_TMR_CCI_WHILE:
        case ECFM_CC_TMR_ERR_CCM_WHILE:
        case ECFM_CC_TMR_XCON_CCM_WHILE:
        case ECFM_CC_TMR_FNG_WHILE:
        case ECFM_CC_TMR_FNG_RESET_WHILE:
        case ECFM_CC_TMR_RDI_PERIOD:
        case ECFM_CC_TMR_AIS_PERIOD:
        case ECFM_CC_TMR_AIS_INTERVAL:
        case ECFM_CC_TMR_AIS_RXWHILE:
        case ECFM_CC_TMR_LCK_PERIOD:
        case ECFM_CC_TMR_LCK_INTERVAL:
        case ECFM_CC_TMR_LCK_RXWHILE:
        case ECFM_CC_TMR_LCK_DELAY:
        case ECFM_CC_TMR_LM_WHILE:
        case ECFM_CC_TMR_LM_DEADLINE:
            pMepInfo = pPduSmInfo->pMepInfo;
            i2Offset = gEcfmCcGlobalInfo.aTmrDesc[u1TimerType].i2Offset;
            pEcfmTmrBlk =
                (tTmrBlk *) (VOID *) ((UINT1 *) (pMepInfo) + i2Offset);
            break;

        case ECFM_CC_TMR_RMEP_WHILE:
            pRMepInfo = pPduSmInfo->pRMepInfo;
            i2Offset = gEcfmCcGlobalInfo.aTmrDesc[u1TimerType].i2Offset;
            pEcfmTmrBlk =
                (tTmrBlk *) (VOID *) ((UINT1 *) (pRMepInfo) + i2Offset);
            break;

        case ECFM_CC_TMR_MIP_DB_HOLD:
            pEcfmTmrBlk = &(ECFM_CC_TMR_MIP_DB_HOLD_TMR);
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcTmrStopTimer:"
                         "Stopping MIP CCM DB Hold Timer \r\n");
            break;

        case ECFM_CC_TMR_MEP_ARCHIVE_HOLD:
            pEcfmTmrBlk = &(pPduSmInfo->pMdInfo->MepArchiveHoldTimer);
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcTmrStopTimer:"
                         "Stopping Mep Archive Hold Timer \r\n");
            break;

        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcTmrStopTimer:Invalid Timer Type \r\n");
            return ECFM_FAILURE;
    }

    /* Stopping Timer */
    if (TmrStop (ECFM_CC_TMRLIST_ID, pEcfmTmrBlk) == ECFM_TMR_FAILURE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC,
                     "EcfmCcTmrStopTimer: Stop Timer Failed !\r\n");
        return ECFM_FAILURE;
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the Timestamp 
     * on reception of this msg*/
    if ((u1TimerType == ECFM_CC_TMR_FNG_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_FNG_RESET_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_ERR_CCM_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_XCON_CCM_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_RDI_PERIOD) ||
        (u1TimerType == ECFM_CC_TMR_LCK_PERIOD) ||
        (u1TimerType == ECFM_CC_TMR_RMEP_WHILE) ||
        (u1TimerType == ECFM_CC_TMR_AIS_PERIOD))
    {
        if (u1TimerType == ECFM_CC_TMR_RDI_PERIOD)
        {
            EcfmCcRdiPeriodTimeout (pMepInfo);
        }
        /*Synch start of timer */
        EcfmRedSyncTmr (ECFM_RED_SYNC_STOP_TMR,
                        u1TimerType, pPduSmInfo, ECFM_INIT_VAL);
    }
#endif
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmCcTmrExpHandler                                  *
 *                                                                           *
 * Description        : This function is called from CC  main routine that   *
 *                      that receives the timer expiry event.                *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC VOID
EcfmCcTmrExpHandler ()
{
    tTmrAppTimer       *pEcfmTimer = NULL;
    INT4                i4LoopStartTime = 0;
    INT4                i4TimerProcessTime = 0;
    INT2                i2Offset = ECFM_INIT_VAL;
    UINT1               u1TimerId = ECFM_INIT_VAL;
    if (ECFM_CC_TMRLIST_ID == ECFM_INIT_VAL)
    {
        return;
    }
    /* Intialize PDU SM Info */
    ECFM_GET_SYS_TIME ((UINT4 *) &i4LoopStartTime);
    while ((pEcfmTimer =
            ECFM_GET_NEXT_EXPIRED_TMR (ECFM_CC_TMRLIST_ID)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pEcfmTimer)->u1TimerId;
        if (u1TimerId >= ECFM_CC_TMR_MAX_TYPES)
        {
            continue;
        }
        i2Offset = gEcfmCcGlobalInfo.aTmrDesc[u1TimerId].i2Offset;
        (*(gEcfmCcGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
            ((VOID *) (((UINT1 *) pEcfmTimer) - i2Offset));
        ECFM_GET_SYS_TIME ((UINT4 *) &i4TimerProcessTime);
        if ((i4TimerProcessTime - i4LoopStartTime) >=
            ECFM_CC_TMR_RELINQUISH_THRESH)
        {
            /* Release Context */
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_TMR_EXP);
            return;
        }
        /* Release Context */
        ECFM_CC_RELEASE_CONTEXT ();
    }
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcTmrCciExpired                                   *
 *                                                                            *
 * Description        : This routine is called when CCI timer gets expired.   *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrCciExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;

    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;

    if (PduSmInfo.pMepInfo->pPortInfo != NULL)
    {
        if (ECFM_CC_SELECT_CONTEXT
            ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
        {
            /* Call the CC Initiator State Machine */
            EcfmCcClntCciSm (&PduSmInfo, ECFM_SM_EV_CCI_TIMER_TIMEOUT);
        }
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrErrCcmExpired                                *
 *                                                                            *
 * Description        : This routine is called when Errror CCM timer expires. *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrErrCcmExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;

    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Remote MEP Error State Machine */
        EcfmCcClntRmepErrSm (&PduSmInfo, ECFM_SM_EV_RMEP_ERR_TIMEOUT);
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_ERR_CCM_WHILE, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/******************************************************************************
 * Function Name      : EcfmCcTmrXConCcmExpired                               *
 *                                                                            *
 * Description        : This routine is called when Cross connect CCM timer   *
 *                      expires.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrXConCcmExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Cross Connect CCM State Machine */
        EcfmCcClntXconSm (&PduSmInfo, ECFM_SM_EV_XCON_TIMEOUT);
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_XCON_CCM_WHILE, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/******************************************************************************
 * Function Name      : EcfmCcTmrFngExpired                                   *
 *                                                                            *
 * Description        : This routine is called when FNG timer expires.        *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrFngExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Fault Notofication Generator State Machine */
        EcfmCcClntFngSm (&PduSmInfo, ECFM_SM_EV_FNG_TIMEOUT);
    }

#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_FNG_WHILE, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/******************************************************************************
 * Function Name      : EcfmCcTmrFngResetExpired                              *
 *                                                                            *
 * Description        : This routine is called when FNG RESET timer expires.  *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrFngResetExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Fault Notofication Generator State Machine */
        EcfmCcClntFngSm (&PduSmInfo, ECFM_SM_EV_FNG_RESET_TIMEOUT);
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_FNG_RESET_WHILE, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/*********************BREAK****************************/
/******************************************************************************
 * Function Name      : EcfmCcTmrMipDbHoldExpired                             *
 *                                                                            *
 * Description        : This routine is called when MIP DB HOLD timer expires.*
 *                                                                            *
 * Input(s)           : pArgs - Pointer to Context Info Structure             *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrMipDbHoldExpired (VOID *pArgs)
{
    tEcfmCcContextInfo *pEcfmContextInfo = NULL;
    UINT4               u4MipCcmDbHoldTime = ECFM_INIT_VAL;

    pEcfmContextInfo = (tEcfmCcContextInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((pEcfmContextInfo->u4ContextId)) != ECFM_SUCCESS)
    {
        return;
    }
    EcfmCcAgeoutMipDbEntry ();
    EcfmRedSyncMipDbHldTmr (0);
    /* Start Mip CcmDb Timer for which MipCcmDbentries are to be retained */
    /* Get the Configured Time Value and convert it to milliseconds */
    u4MipCcmDbHoldTime = ECFM_CONVERT_HRS_TO_MSEC
        (ECFM_CC_MIP_CCM_DB_HOLD_TIME);
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL,
                             u4MipCcmDbHoldTime) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcTmrExpHandler:MipCcmDb Timer start failed \r\n");
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrMepArchiveHoldExpired                        *
 *                                                                            *
 * Description        : This routine is called when MEP Archive hold Timer    *
 *                      expires.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MD Info  structure                 *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrMepArchiveHoldExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcMdInfo      *pMdInfo = NULL;
    UINT4               u4TimeTick = ECFM_INIT_VAL;
    UINT4               u4TimeInMsc = ECFM_INIT_VAL;

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    pMdInfo = (tEcfmCcMdInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT ((pMdInfo->u4ContextId)) != ECFM_SUCCESS)
    {
        return;
    }
    /*Handle the MEP Archive Hold Timer Expiry */
    EcfmCcHandleMepHldTmrExpiry (pMdInfo);
    PduSmInfo.pMdInfo = pMdInfo;
    u4TimeTick =
        ECFM_CONVERT_SEC_TO_TIME_TICKS (pMdInfo->u2MepArchiveHoldTime *
                                        ECFM_NUM_OF_SEC_IN_A_MIN);
    u4TimeInMsc = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4TimeTick);
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD,
                             &PduSmInfo, u4TimeInMsc) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC,
                     "EcfmCcTmrExpHandler: Unable to Start MEP-Archive Hold Timer \r\n");
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrRdiExpired                                   *
 *                                                                            *
 * Description        : This routine is called when RDI timer expires.        *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrRdiExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);

    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    PduSmInfo.pMepInfo = pMepInfo;

    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Rdi Period Timeout in CC Initiator */
        EcfmCcRdiPeriodTimeout (pMepInfo);
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_RDI_PERIOD, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/******************************************************************************
 * Function Name      : EcfmCcTmrAisExpired                                   *
 *                                                                            *
 * Description        : This routine is called when AIS period timer expires. *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrAisExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);

    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    PduSmInfo.pMepInfo = pMepInfo;

    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Ais Period Timeout */
        EcfmCcAisPeriodTimeout (pMepInfo);
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_AIS_PERIOD, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/******************************************************************************
 * Function Name      : EcfmCcTmrAisIntervalExpired                           *
 *                                                                            *
 * Description        : This routine is called on AIS interval timeout.       *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrAisIntervalExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Ais Interval Timeout */
        EcfmCcClntAisInitiator (pMepInfo, ECFM_EV_AIS_INTERVAL_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrAisRxExpired                                 *
 *                                                                            *
 * Description        : This routine is called when AIS RxWhile timer expires.*
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrAisRxExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Ais RxWhile Timeout */
        EcfmCcClearAisCondition (pMepInfo);
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrLckPeriodExpired                             *
 *                                                                            *
 * Description        : This routine is called on Lck period timeout.         *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrLckPeriodExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);

    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    PduSmInfo.pMepInfo = pMepInfo;
    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Lck Period Timeout */
        EcfmCcLckPeriodTimeout (pMepInfo);
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_LCK_PERIOD, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/******************************************************************************
 * Function Name      : EcfmCcTmrLckIntervalExpired                           *
 *                                                                            *
 * Description        : This routine is called when Lck Interval timer expires*
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrLckIntervalExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Lck Interval Timeout */
        EcfmCcClntLckInitiator (pMepInfo, ECFM_EV_LCK_INTERVAL_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrLckRxExpired                                 *
 *                                                                            *
 * Description        : This routine is called when Lck Rxwhile timer expires.*
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrLckRxExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Lck RxWhile Timeout */
        EcfmCcClearLckCondition (pMepInfo);
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrLckDelayExpired                              *
 *                                                                            *
 * Description        : This routine is called when Lck Delay timer expires.  *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrLckDelayExpired (VOID *pArgs)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    pMepInfo = (tEcfmCcMepInfo *) pArgs;

    if (ECFM_CC_SELECT_CONTEXT
        ((pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call the Lck Delay Timeout */
        EcfmCcUpdateLckStatus (pMepInfo);

        /*Sync LckStatus to Standby Node */
        EcfmRedSyncLckStatus (pMepInfo);
    }

}

/******************************************************************************
 * Function Name      : EcfmCcTmrLmDeadLineExpired                            *
 *                                                                            *
 * Description        : This routine is called when LM Deadline timer expires.*
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrLmDeadLineExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call LM Initiator with LM Deadline expiry event */
        EcfmCcClntLmInitiator (&PduSmInfo, ECFM_CC_LM_DEADLINE_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrLmWhileExpired                               *
 *                                                                            *
 * Description        : This routine is called when LM while timer expires.   *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to MEP Info  structure                *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrLmWhileExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = (tEcfmCcMepInfo *) pArgs;
    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
        /* Call LM Initiator with LM While expiry event */
        EcfmCcClntLmInitiator (&PduSmInfo, ECFM_CC_LM_WHILE_EXPIRY);
    }
}

/******************************************************************************
 * Function Name      : EcfmCcTmrCcRMepDbExpired                              *
 *                                                                            *
 * Description        : This routine is called when RMepDb while timer 
 *                      expires.                                              *
 *                                                                            *
 * Input(s)           : pArgs - Pointer to RMEP Info  structure               *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *****************************************************************************/
PRIVATE VOID
EcfmCcTmrCcRMepDbExpired (VOID *pArgs)
{
    tEcfmCcPduSmInfo    PduSmInfo;
#ifdef ICCH_WANTED
    UINT4                u4IcclIfIndex = 0;
    UINT4                u4PeerNodeState = 0;
#endif

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pRMepInfo = (tEcfmCcRMepDbInfo *) pArgs;
    PduSmInfo.pMepInfo = PduSmInfo.pRMepInfo->pMepInfo;

    if (ECFM_CC_SELECT_CONTEXT
        ((PduSmInfo.pMepInfo->pPortInfo->u4ContextId)) == ECFM_SUCCESS)
    {
#ifdef ICCH_WANTED
        IcchGetIcclIfIndex (&u4IcclIfIndex);
        u4PeerNodeState = IcchGetPeerNodeState ();
        /* Skip calling the timeout event when the interface
         * index belongs to ICCL Index and the Peer state is UP.
         */
        if (!((PduSmInfo.pMepInfo->pPortInfo->u4IfIndex == u4IcclIfIndex) &&
                    (ICCH_PEER_UP == u4PeerNodeState )))
#endif
        {
        /* Call RMep SM RMep Timeout event */
        EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);
    }
    }
#ifdef L2RED_WANTED
    /*Synch Start of timer with standby, standby stores the
     * Timestamp on reception of this msg*/
    /*Synch start of timer */
    EcfmRedSyncTmr (ECFM_RED_SYNC_EXPIRY_TMR,
                    ECFM_CC_TMR_RMEP_WHILE, &PduSmInfo, ECFM_INIT_VAL);
#endif

}

/****************************************************************************
  End of File cfmcctmr.c
 ****************************************************************************/
