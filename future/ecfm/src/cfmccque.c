/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccque.c,v 1.67 2015/10/13 11:36:32 siva Exp $
 *
 * Description: This file contains the procedure related to         
 *              processing of QMsgs and Enquing QMsg from external
 *              module to ECFM's CC Task
 *******************************************************************/

#include "cfminc.h"
PRIVATE INT4 EcfmHandleVlanMemberChg PROTO ((UINT2));
PRIVATE VOID EcfmHandleIpv4ManAddrChg PROTO ((tEcfmCcMsg *));
PRIVATE VOID EcfmHandleIpv6ManAddrChg PROTO ((tEcfmCcMsg *));
PRIVATE INT4 EcfmHandelLocSysInfoChg PROTO ((tEcfmCcMsg *));
PRIVATE INT4 EcfmHandleCreateVlan PROTO ((UINT4, UINT2));
PRIVATE INT4 EcfmHandleMstpDisable PROTO ((UINT4));
PRIVATE INT4 EcfmHandleMstpEnable PROTO ((UINT4));
PRIVATE INT4 EcfmHandleCreateIsid PROTO ((UINT4, UINT4));
PRIVATE INT4 EcfmHandleDeleteIsid PROTO ((UINT4, UINT4));
PRIVATE INT4 EcfmHandleRemoveFromChannel PROTO ((UINT2));
PRIVATE INT4 EcfmHandleAddToChannel PROTO ((UINT2));
PRIVATE INT4 EcfmHandleCreateContext PROTO ((UINT4));
PRIVATE INT4 EcfmHandleVlanEtherTypeChngInd PROTO ((UINT4, UINT2, UINT1));
/******************************************************************************
 * Function Name      : EcfmCcPktQueueHandler
 *
 * Description        : This function receives and processes the CFM PDUs  
 *                      received on the CC Task Packet Queue.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC VOID
EcfmCcPktQueueHandler (tEcfmCcMsg * pQMsg)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;

    /* Select Context for which the Message is received */
    if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
    {
        EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
        pQMsg->uMsg.pEcfmPdu = NULL;
        return;
    }
    if ((pQMsg->MsgType != ECFM_MPLS_PATH_STATUS_CHG) &&
        (pQMsg->MsgType != ECFM_EV_MPLSTP_CC_PDU_IN_QUEUE))

    {
        /* Get PortInfo for the received Index */
        pPortInfo = ECFM_CC_GET_PORT_INFO (pQMsg->u2PortNum);
        /* Check if the Port Module Status is enabled or not */
        if (pPortInfo == NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcPktQueueHandler: CFM-PDU Received in "
                         "invalid port\r\n");
            EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            return;
        }
    }
    else
    {
        if (pQMsg->MsgType == ECFM_EV_MPLSTP_CC_PDU_IN_QUEUE)
        {
            /* Get ECFM module staus from the Dummy MPLS Port */
            pPortInfo = ECFM_CC_GET_PORT_INFO (ECFM_CC_MAX_PORT_INFO);
            /* Check if the Port Module Status is enabled or not */
            if ((pPortInfo == NULL) ||
                (pPortInfo->u1PortEcfmStatus != ECFM_ENABLE))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcPktQueueHandler: CFM-PDU Received: but "
                             "ECFM module status is disabled\r\n");
                EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
                pQMsg->uMsg.pEcfmPdu = NULL;
                return;
            }
        }
    }

    switch (pQMsg->MsgType)
    {
        case ECFM_PDU_RCV_FRM_CFA:
            ECFM_CC_TRC (ECFM_DATA_PATH_TRC,
                         "EcfmCcPktQueueHandler: Received CFMPDU "
                         "from CFA " "\r\n");
            /* Call ECFM Receiver for processing the received packet */
            if (EcfmCcCtrlRxPkt (pQMsg->uMsg.pEcfmPdu,
                                 pQMsg->u4IfIndex, pQMsg->u2PortNum)
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcPktQueueHandler: EcfmCcCtrlReceiver"
                             "returned failure\r\n");
                ECFM_CC_INCR_DSRD_CFM_PDU_COUNT (pQMsg->u2PortNum);
                ECFM_CC_INCR_CTX_DSRD_CFM_PDU_COUNT (pQMsg->u4ContextId);
            }
            EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            break;
        case ECFM_EV_MPLSTP_CC_PDU_IN_QUEUE:
            /* Received a Y.1731 CC PDU from MPLS-TP network. Process the same
             */
            ECFM_CC_TRC (ECFM_DATA_PATH_TRC,
                         "EcfmCcPktQueueHandler: Y.1731 PDU received over"
                         "MPLS-TP Path\r\n");

            if (EcfmProcessMpTpPdu (pQMsg->uMsg.pEcfmPdu,
                                    pQMsg->u4IfIndex) == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcPktQueueHandler: Unable to process "
                             "the Y.1731 PDU\r\n");
            }
            EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            break;

        case ECFM_MPLS_PATH_STATUS_CHG:
            EcfmCcProcessMplsPathStatusChg (&(pQMsg->uMsg.EcfmMplsParams),
                                            pQMsg->u1MplsTnlPwOperState);
            break;
        default:
            ECFM_CC_TRC (INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcPktQueueHandler: Invalid Event \n");
            EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            break;
    }
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcCfgQueueHandler
 *
 * Description        : This function receives and processes the configuration
 *                      messages received on the CC Tasks Configuration Queue.
 *
 * Input(s)           : pQMsg - Message information received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC VOID
EcfmCcCfgQueueHandler (tEcfmCcMsg * pQMsg)
{
    tEcfmMplsParams     MplsParams;
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmMacAddr        SrcMacAddr;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
#ifdef MBSM_WANTED
#ifdef NPAPI_WANTED
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
#endif /*NPAPI_WANTED*/
#endif /* MBSM_WANTED */
    ECFM_MEMSET (&MplsParams, 0, sizeof (tEcfmMplsParams));
    ECFM_MEMSET (&MaInfo, 0, sizeof (tEcfmCcMaInfo));
    ECFM_MEMSET (gpEcfmCcMepNode, 0, sizeof (tEcfmCcMepInfo));
    ECFM_MEMSET (&SrcMacAddr, 0, sizeof (tEcfmMacAddr));

    ECFM_CC_SELECT_CONTEXT (ECFM_DEFAULT_CONTEXT);
    switch (pQMsg->MsgType)
    {
        case ECFM_CREATE_PORT_MSG:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            EcfmCcIfHandleCreatePort (pQMsg->u4ContextId, pQMsg->u4IfIndex,
                                      pQMsg->u2PortNum);
            /* Port Create at LBLT also */
            ECFM_LBLT_HANDLE_CREATE_PORT (pQMsg->u4ContextId, pQMsg->u4IfIndex,
                                          pQMsg->u2PortNum);
            L2_SYNC_GIVE_SEM ();
            break;

        case ECFM_MAP_PORT_MSG:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }
            EcfmCcIfHandleCreatePort (pQMsg->u4ContextId, pQMsg->u4IfIndex,
                                      pQMsg->u2PortNum);
            /* Map Port at LBLT also */
            ECFM_LBLT_HANDLE_MAP_PORT (pQMsg->u4ContextId, pQMsg->u4IfIndex,
                                       pQMsg->u2PortNum);
            L2MI_SYNC_GIVE_SEM ();
            break;

        case ECFM_DELETE_PORT_MSG:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }

            if ((pTempPortInfo =
                 ECFM_CC_GET_PORT_INFO (pQMsg->u2PortNum)) != NULL)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (pQMsg->u4ContextId,
                                               ECFM_DELETE_PORT_MSG,
                                               pTempPortInfo->u4IfIndex,
                                               ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_TRUE);
            }
            else
            {
                break;
            }
            EcfmCcIfHandleDeletePort (pQMsg->u2PortNum);
            /* Port delete at LBLT also */
            ECFM_LBLT_HANDLE_DELETE_PORT (pQMsg->u2PortNum);
            EcfmRedSynchHwAuditCmdEvtInfo (pQMsg->u4ContextId,
                                           ECFM_DELETE_PORT_MSG,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_FALSE);
            break;

        case ECFM_UNMAP_PORT_MSG:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            pTempPortInfo = NULL;
            if ((pTempPortInfo =
                 ECFM_CC_GET_PORT_INFO (pQMsg->u2PortNum)) != NULL)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (pQMsg->u4ContextId,
                                               ECFM_UNMAP_PORT_MSG,
                                               pTempPortInfo->u4IfIndex,
                                               ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_INIT_VAL,
                                               ECFM_INIT_VAL, ECFM_TRUE);
            }
            else
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }
            EcfmCcIfHandleDeletePort (pQMsg->u2PortNum);
            /* Unmap Port at LBLT also */
            ECFM_LBLT_HANDLE_UNMAP_PORT (pQMsg->u2PortNum);
            EcfmRedSynchHwAuditCmdEvtInfo (pQMsg->u4ContextId,
                                           ECFM_UNMAP_PORT_MSG,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_FALSE);
            L2MI_SYNC_GIVE_SEM ();
            break;

        case ECFM_INTF_TYPE_CHANGE:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            EcfmCcIfInterfaceTypeChg (pQMsg->uMsg.u1IntfType);
            /* Unmap Port at LBLT also */
            ECFM_LBLT_HANDLE_INTF_TYPE_CHANGE (pQMsg->u4ContextId,
                                               pQMsg->uMsg.u1IntfType);
            L2_SYNC_GIVE_SEM ();
            break;
        case ECFM_OPER_STATUS_CHG_MSG:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            pTempPortInfo = NULL;
            if ((pTempPortInfo =
                 ECFM_CC_GET_PORT_INFO (pQMsg->u2PortNum)) == NULL)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            EcfmRedSynchHwAuditCmdEvtInfo (pQMsg->u4ContextId,
                                           ECFM_OPER_STATUS_CHG_MSG,
                                           pTempPortInfo->u4IfIndex,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_TRUE);
            EcfmCcIfHandlePortOperChg (pQMsg->u2PortNum,
                                       pQMsg->uMsg.u1PortOperState);
            EcfmRedSynchHwAuditCmdEvtInfo (pQMsg->u4ContextId,
                                           ECFM_OPER_STATUS_CHG_MSG,
                                           pTempPortInfo->u4IfIndex,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_FALSE);
            /* Update Port Status at LBLT also */
            ECFM_LBLT_HANDLE_PORT_OPER_CHG (pQMsg->u2PortNum,
                                            pQMsg->uMsg.u1PortOperState);
            L2_SYNC_GIVE_SEM ();
            break;
        case ECFM_CREATE_CONTEXT_MSG:
            if (EcfmHandleCreateContext (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: EcfmHandleCreateContext"
                              " returned failure!!!\n");
            }
            L2MI_SYNC_GIVE_SEM ();
            break;
        case ECFM_DELETE_CONTEXT_MSG:
            /* Shutdown ECFM for the context to be deleted */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            EcfmUtilModuleShutDown ();
            /* Delete Context at LBLT also */
            ECFM_LBLT_HANDLE_DELETE_CONTEXT (pQMsg->u4ContextId);
            EcfmCcHandleDeleteContext (pQMsg->u4ContextId);
            L2MI_SYNC_GIVE_SEM ();
            break;
        case ECFM_CC_START_TRANSACTION:
        case ECFM_LM_START_TRANSACTION:
        case ECFM_LM_STOP_TRANSACTION:
        case ECFM_AIS_START_TRANSACTION:
        case ECFM_AIS_STOP_TRANSACTION:
        case ECFM_LCK_START_TRANSACTION:
        case ECFM_LCK_STOP_TRANSACTION:
        case ECFM_AVLBLTY_START_TRANSACTION:
        case ECFM_AVLBLTY_STOP_TRANSACTION:
        {
            tEcfmCcMepInfo     *pMepNode = NULL;    /* For receiving pointer to 
                                                     * MEP Node found */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }
            ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCfgQueueHandler:" "Received Message for"
                         " CC Task from SNMP \r\n");
            /* Get MEP Info from the information received in the Message 
             * Node*/
            if (!
                ((pQMsg->uMsg.Mep.u1SelectorType == ECFM_SERVICE_SELECTION_LSP)
                 || (pQMsg->uMsg.Mep.u1SelectorType ==
                     ECFM_SERVICE_SELECTION_PW)))
            {
                pMepNode =
                    EcfmCcUtilGetMepEntryFrmPort (pQMsg->uMsg.Mep.u1MdLevel,
                                                  pQMsg->uMsg.Mep.u4VidIsid,
                                                  pQMsg->u2PortNum,
                                                  pQMsg->uMsg.Mep.u1Direction);
            }
            else
            {
                /* For MPLSTP get the Mep from ECFM_CC_MEP_TABLE */
                pMepNode = EcfmCcUtilGetMepEntryFrmGlob
                    (pQMsg->uMsg.Mep.u4MdIndex,
                     pQMsg->uMsg.Mep.u4MaIndex, pQMsg->uMsg.Mep.u2MepId);
            }

            if (pMepNode != NULL)
            {
                tEcfmCcPduSmInfo    PduSmInfo;
                ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                             ECFM_CC_PDUSM_INFO_SIZE);
                PduSmInfo.pMepInfo = pMepNode;
                switch (pQMsg->MsgType)
                {
                    case ECFM_CC_START_TRANSACTION:
                        EcfmCcClntCciSm (&PduSmInfo, ECFM_SM_EV_CCI_ENABLE);
                        break;
                    case ECFM_LM_START_TRANSACTION:
                    case ECFM_LM_STOP_TRANSACTION:
                        EcfmCcClntLmInitiator (&PduSmInfo, pQMsg->MsgType);
                        break;
                    case ECFM_AIS_START_TRANSACTION:
                    case ECFM_AIS_STOP_TRANSACTION:
                        EcfmCcClntAisInitiator (pMepNode, pQMsg->MsgType);
                        break;
                    case ECFM_LCK_START_TRANSACTION:
                    case ECFM_LCK_STOP_TRANSACTION:
                        EcfmCcClntLckInitiator (pMepNode, pQMsg->MsgType);
                        break;
                    case ECFM_AVLBLTY_START_TRANSACTION:
                    case ECFM_AVLBLTY_STOP_TRANSACTION:
                        EcfmCcClntAvlbltyInitiator (&PduSmInfo, pQMsg->MsgType);
                        break;
                    default:
                        break;
                }
            }
            break;
        }

#ifdef L2RED_WANTED
        case ECFM_RM_FRAME:
            EcfmCcRedHandleRmEvents (pQMsg);
            break;
#endif

        case ECFM_CREATE_VLAN:
            EcfmHandleCreateVlan (pQMsg->u4ContextId,
                                  pQMsg->uMsg.Indications.u4VlanIdIsid);
            break;

        case ECFM_MSTP_DISABLE:
            EcfmHandleMstpDisable (pQMsg->u4ContextId);
            break;

        case ECFM_MSTP_ENABLE:
            EcfmHandleMstpEnable (pQMsg->u4ContextId);
            break;

        case ECFM_PORT_STATE_CHANGE:
            EcfmHandlePortStateChg (pQMsg->uMsg.Indications.u2MstInst,
                                    pQMsg->u4IfIndex,
                                    pQMsg->uMsg.Indications.u1PortState);
            break;
        case ECFM_PB_PORT_STATE_CHANGE:
            EcfmHandlePbPortStateChg (pQMsg->u4IfIndex,
                                      pQMsg->uMsg.Mep.u4VidIsid,
                                      pQMsg->uMsg.Indications.u1PortState);
            break;
        case ECFM_UPDATE_LOC_SYS_INFO:
            if (EcfmHandelLocSysInfoChg (pQMsg) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: EcfmHandelLocSysInfoChg"
                              " returned failure!!!\n");
            }
            break;
        case ECFM_IP4_MAN_ADDR_CHG_MSG:
            EcfmHandleIpv4ManAddrChg (pQMsg);
            break;
        case ECFM_IP6_MAN_ADDR_CHG_MSG:
            EcfmHandleIpv6ManAddrChg (pQMsg);
            break;

        case ECFM_VLAN_MEMBER_CHANGE:
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }
            /* Do processing for all ports and provided VLAN */
            EcfmHandleVlanMemberChg (pQMsg->uMsg.Indications.u4VlanIdIsid);
            break;
        case ECFM_DELETE_VLAN:
            EcfmHandleDeleteVlan (pQMsg->u4ContextId,
                                  pQMsg->uMsg.Indications.u4VlanIdIsid);
            break;
        case ECFM_CREATE_ISID:
            EcfmHandleCreateIsid (pQMsg->u4ContextId,
                                  pQMsg->uMsg.Indications.u4VlanIdIsid);
            break;

        case ECFM_DELETE_ISID:
            EcfmHandleDeleteIsid (pQMsg->u4ContextId,
                                  pQMsg->uMsg.Indications.u4VlanIdIsid);
            break;
        case ECFM_ADD_TO_PORT_CHANNEL:
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }
            if (EcfmHandleAddToChannel (pQMsg->u2PortNum) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: EcfmHandleAddToChannel"
                              " returned failure!!!\n");
            }
            break;
        case ECFM_REMOVE_FROM_PORT_CHANNEL:
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            if (EcfmHandleRemoveFromChannel (pQMsg->u2PortNum) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: EcfmHandleRemoveFromChannel"
                              " returned failure!!!\n");
            }
            L2_SYNC_GIVE_SEM ();
            break;
        case ECFM_VLAN_UPDATE_DEI_BIT:
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }

            EcfmHandleUpdateDeiBit (pQMsg->u4IfIndex);
            break;
#ifdef MBSM_WANTED
        case MBSM_MSG_CARD_INSERT:
        case MBSM_MSG_CARD_REMOVE:
            EcfmMbsmHandleLcStatusChg (pQMsg->uMsg.MbsmCardUpdate.
                                       pMbsmProtoMsg, pQMsg->MsgType);
            MEM_FREE (pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg);
            pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg = NULL;
            break;
#ifdef NPAPI_WANTED
         case MBSM_MSG_SELF_CARD_INSERT:
            EcfmInitWithMBSMandRM (ECFM_CC_CURR_CONTEXT_ID()= u4CurrContextId);
            MEM_FREE (pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg);
            pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg = NULL; 
            break;
#endif /* NPAPI_WANTED */
#endif /* MBSM_WANTED */
        case ECFM_MPLS_BULK_PATH_STATUS_IND:
            if (EcfmMptpHandleBulkPathStatusInd (pQMsg->uMsg.pEcfmPdu,
                                                 pQMsg->u4ContextId) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcCfgQueueHandler: "
                             "EcfmMptpHandleBulkPathStatusInd failed in "
                             "processing the Bulk Path Status record"
                             "..................\r\n");
            }
            EcfmCcCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            break;
        case ECFM_VLAN_ETHER_TYPE_CHANGE:
            /* Select Context for which the Message is received */
            if (ECFM_CC_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }
            EcfmHandleVlanEtherTypeChngInd (pQMsg->u4IfIndex,
                                            pQMsg->uMsg.Indications.
                                            u2EtherTypeValue,
                                            pQMsg->uMsg.Indications.
                                            u1EtherType);

            break;
        case ECFM_AIS_COND_ENTRY:

            ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: AIS condition received"
                              "for Level : %d && ", pQMsg->u1MdLevel);

            if (pQMsg->uMsg.EcfmMplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_TUNNEL)
            {
                ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCfgQueueHandler: AIS condition"
                                  "received for Tunnel %d:%d:%d:%d.........\r\n",
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4TunnelId,
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4TunnelInst,
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4SrcLer,
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4DstLer);
            }
            else
            {
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCfgQueueHandler: AIS condition "
                                  "received for PW %d...............\r\n",
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  u4PswId);
            }

            ECFM_MEMCPY (&MplsParams, &(pQMsg->uMsg.EcfmMplsParams),
                         sizeof (tEcfmMplsParams));
            gpEcfmCcMepNode->pEcfmMplsParams = &MplsParams;

            pMepInfo = RBTreeGet (ECFM_CC_MPLSMEP_TABLE,
                                  (tRBElem *) (gpEcfmCcMepNode));
            if (pMepInfo == NULL)
            {
                /* No such valid MA/ME exists for the received PDU. Hence drop 
                 * the PDU without processing furthur
                 * */
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcCfgQueueHandler: Unable to obtain the ME"
                             "information for the received MPLS path.....\r\n");
                return;
            }
            while (pMepInfo != NULL)
            {
                EcfmCcSetAisCondition (pMepInfo, SrcMacAddr, pQMsg->u1MdLevel);
                pMepInfo = (tEcfmCcMepInfo *)
                    TMO_DLL_Next (&(pMaInfo->MepTable),
                                  &(pMepInfo->MepTableDllNode));
            }
            break;

        case ECFM_AIS_COND_EXIT:

            ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: AIS condition exited"
                              "received for Level : %d", pQMsg->u1MdLevel);
            if (pQMsg->uMsg.EcfmMplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_TUNNEL)
            {
                ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCfgQueueHandler: AIS condition"
                                  "received for Tunnel %d:%d:%d:%d.........\r\n",
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4TunnelId,
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4TunnelInst,
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4SrcLer,
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  MplsLspParams.u4DstLer);
            }
            else
            {
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCfgQueueHandler: AIS condition "
                                  "received for PW %d...............\r\n",
                                  pQMsg->uMsg.EcfmMplsParams.MplsPathParams.
                                  u4PswId);
            }
            ECFM_MEMCPY (&MplsParams, &(pQMsg->uMsg.EcfmMplsParams),
                         sizeof (tEcfmMplsParams));
            gpEcfmCcMepNode->pEcfmMplsParams = &MplsParams;

            pMepInfo = RBTreeGet (ECFM_CC_MPLSMEP_TABLE,
                                  (tRBElem *) (gpEcfmCcMepNode));

            if (pMepInfo == NULL)
            {
                /* No such valid MA/ME exists for the received PDU. Hence drop 
                 * the PDU without processing furthur
                 * */
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcCfgQueueHandler: AIS exit "
                             "Unable to obtain the ME information for the "
                             "received MPLS path..................\r\n");
                return;
            }
            while (pMepInfo != NULL)
            {
                EcfmCcClearAisCondition (pMepInfo);
                pMepInfo = (tEcfmCcMepInfo *)
                    TMO_DLL_Next (&(pMaInfo->MepTable),
                                  &(pMepInfo->MepTableDllNode));
            }
            break;

        default:
            ECFM_CC_TRC (INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCfgQueueHandler: Invalid Event !!!\n");
            break;
    }
    return;
}

/******************************************************************************
 * Function Name      : EcfmCcInterruptQueueHandler
 *
 * Description        : This function receives and processes the interrupt 
 *                      messages received on the CC Tasks Interrupt Queue.
 *
 * Input(s)           : pQMsg - Message information received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC VOID
EcfmCcInterruptQueueHandler (tEcfmCcMsg * pQMsg)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmMplsParams    *pMplsParams;
    tEcfmMacAddr        SrcMacAddr;

    ECFM_MEMSET (gpEcfmCcMepNode, 0, sizeof (tEcfmCcMepInfo));
    ECFM_MEMSET (&SrcMacAddr, 0, sizeof (tEcfmMacAddr));

    switch (pQMsg->MsgType)
    {
        case ECFM_CALL_BACK:
           #ifdef VTSS_SERVAL
            EcfmCcmOffHandleCallBackVtssEvt(pQMsg);
           #else

            EcfmCcmOffHandleCallBackEvt (pQMsg->u4ContextId,
                                         pQMsg->uMsg.Indications.
                                         u2CcmOffloadHandle, pQMsg->u4IfIndex);
           #endif
            break;
        case ECFM_TX_FAILURE_CALL_BACK:
            EcfmCcmOffHandleTxFailureEvt (pQMsg->u4ContextId,
                                          pQMsg->uMsg.Indications.
                                          u2CcmOffloadHandle);
            break;
        case ECFM_TX_FAILURE_CALL_BACK_FROM_HW:
            EcfmCcmHwHandleTxFailureEvt (pQMsg->u4ContextId,
                                         pQMsg->uMsg.HwCalBackParam.
                                         au1HwHandler);
            break;
        case ECFM_RX_CALL_BACK_FROM_HW:
            EcfmCcmHwHandleRxCallBackEvt (pQMsg->u4ContextId,
                                          pQMsg->uMsg.HwCalBackParam.
                                          au1HwHandler, pQMsg->u4IfIndex);
            break;
        case ECFM_AIS_COND_ENTRY:
        case ECFM_AIS_COND_EXIT:

            pMplsParams = &pQMsg->uMsg.MplstpAisOffParams.EcfmMplsParams;

            ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCfgQueueHandler: AIS condition: [%s] "
                              "for Level: %d \r\n",
                              ((pQMsg->uMsg.MplstpAisOffParams.b1AisCondition
                                == TRUE) ? "ENTRY" : "EXIT"),
                              pQMsg->uMsg.MplstpAisOffParams.u1MdLevel);

            if (pMplsParams->u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                ECFM_CC_TRC_ARG4 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCfgQueueHandler: AIS info for MPLSTP Tunnel: "
                                  "%d:%d:%d:%d.........\r\n",
                                  pMplsParams->MplsPathParams.MplsLspParams.
                                  u4TunnelId,
                                  pMplsParams->MplsPathParams.MplsLspParams.
                                  u4TunnelInst,
                                  pMplsParams->MplsPathParams.MplsLspParams.
                                  u4SrcLer,
                                  pMplsParams->MplsPathParams.MplsLspParams.
                                  u4DstLer);
            }
            else
            {
                ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCfgQueueHandler: AIS info for MPLSTP PW: "
                                  "%d...............\r\n",
                                  pMplsParams->MplsPathParams.u4PswId);
            }

            gpEcfmCcMepNode->pEcfmMplsParams = pMplsParams;

            pMepInfo = RBTreeGet (ECFM_CC_MPLSMEP_TABLE,
                                  (tRBElem *) (gpEcfmCcMepNode));
            if (pMepInfo == NULL)
            {
                /* No such valid MA/ME exists for the received PDU. Hence drop 
                 * the PDU without processing furthur
                 * */
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcCfgQueueHandler: Unable to obtain the MEP"
                             "information for the received MPLS path.....\r\n");
                return;
            }

            if (pQMsg->uMsg.MplstpAisOffParams.b1AisCondition == TRUE)
            {
                EcfmCcSetAisCondition (pMepInfo, SrcMacAddr,
                                       pQMsg->uMsg.MplstpAisOffParams.
                                       u1MdLevel);
            }
            else
            {
                EcfmCcClearAisCondition (pMepInfo);
            }
            break;

        default:
            ECFM_CC_TRC (INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcInterruptQueueHandler: Invalid Event !!!\n");
            break;
    }
    return;
}

/******************************************************************************
 * Function Name      : EcfmHandelLocSysInfoChg
 *
 * Description        : This function handles the local system information 
 *                      change message received from LLDP.
 *
 * Input(s)           : pQMsg - Message information received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandelLocSysInfoChg (tEcfmCcMsg * pMsg)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    switch (pMsg->MsgSubType)
    {
        case ECFM_UPDATE_CHASSISID:
            ECFM_LBLT_LOCK ();

            if ((pMsg->uMsg.ChassisId.u2IdLen > 0) &&
                (pMsg->uMsg.ChassisId.u2IdLen < ECFM_MAX_CHASSISID_LEN))
            {

                /* update the chassis-id subtype */
                ECFM_CHASSISID_SUBTYPE = pMsg->uMsg.ChassisId.u1SubType;
                /* update the chassis-id */
                ECFM_MEMCPY (ECFM_CHASSISID, pMsg->uMsg.ChassisId.au1Id,
                             pMsg->uMsg.ChassisId.u2IdLen);
                /* update the chassis-id len */
                ECFM_CHASSISID_LEN = pMsg->uMsg.ChassisId.u2IdLen;
            }
            ECFM_LBLT_UNLOCK ();
            break;
        case ECFM_UPDATE_PORTID:
            if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pMsg->u4IfIndex,
                                                    &u4ContextId,
                                                    &u2LocalPort) !=
                ECFM_VCM_SUCCESS)
            {
                return ECFM_FAILURE;
            }
            if ((pMsg->uMsg.PortId.u2IdLen > 0) &&
                (pMsg->uMsg.PortId.u2IdLen < ECFM_MAX_PORTID_LEN))
            {

                ECFM_CC_SELECT_CONTEXT (u4ContextId);
                /* store the port-id subtype */
                ECFM_CC_SET_PORTID_SUBTYPE (u2LocalPort,
                                            pMsg->uMsg.PortId.u1SubType);
                if (ECFM_CC_PORTID (u2LocalPort) != NULL)
                {
                    /* store the port-id in cc */
                    ECFM_MEMCPY (ECFM_CC_PORT_INFO (u2LocalPort)->au1PortId,
                                 pMsg->uMsg.PortId.au1Id,
                                 pMsg->uMsg.PortId.u2IdLen);
                }
                ECFM_CC_RELEASE_CONTEXT ();

                ECFM_LBLT_LOCK ();
                if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
                {
		    ECFM_LBLT_UNLOCK ();
                    return ECFM_FAILURE;
                }
                /* store the port-id subtype */
                ECFM_LBLT_SET_PORTID_SUBTYPE (u2LocalPort,
                                              pMsg->uMsg.PortId.u1SubType);
                if (ECFM_LBLT_PORTID (u2LocalPort) != NULL)
                {
                    /* store the port-id in lblt */
                    ECFM_MEMCPY (ECFM_LBLT_PORT_INFO (u2LocalPort)->au1PortId,
                                 pMsg->uMsg.PortId.au1Id,
                                 pMsg->uMsg.PortId.u2IdLen);
                }
                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();
            }
            break;
        default:
            return ECFM_FAILURE;

    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmHandleIpv4ManAddrChg
 *
 * Description        : This function handles the Ipv4 address change information 
 *                      change message received from IPv4.
 *
 * Input(s)           : pQMsg - Message information received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE VOID
EcfmHandleIpv4ManAddrChg (tEcfmCcMsg * pMsg)
{
    tNetIpv4IfInfo     *pNetIp4IfInfo = NULL;
    UINT4               u4L3IfIndex = 0;
    UINT4               u4BitMap = 0;
    UINT4               u4IpAddr = 0;

    pNetIp4IfInfo = &(pMsg->uMsg.Ip4ManAddr.NetIpIfInfo);
    u4L3IfIndex = pNetIp4IfInfo->u4IfIndex;
    u4BitMap = pMsg->uMsg.Ip4ManAddr.u4BitMap;

    if (u4BitMap & IFACE_DELETED)
    {
        /* Remove the management address entry */
        if ((INT1) u4L3IfIndex == ECFM_IPv4_L3IFINDEX)
        {
            ECFM_MEMSET (ECFM_MGMT_ADDRESS, 0x00, ECFM_MAX_MAN_ADDR_LEN);
            ECFM_MGMT_ADDR_DOMAIN_CODE = 0;
            ECFM_IPv4_L3IFINDEX = -1;
        }
    }
    else if (u4BitMap & OPER_STATE)
    {
        if (pNetIp4IfInfo->u4Oper == IPIF_OPER_ENABLE)
        {
            /* add the management address entry */
            if (ECFM_IPv4_L3IFINDEX == -1)
            {
                ECFM_MGMT_ADDR_DOMAIN_CODE = IPVX_ADDR_FMLY_IPV4;
                ECFM_MGMT_ADDRESS_LEN = sizeof (UINT4);
                u4IpAddr = pNetIp4IfInfo->u4Addr;
                u4IpAddr = OSIX_NTOHL (u4IpAddr);
                MEMCPY (ECFM_MGMT_ADDRESS, &u4IpAddr, sizeof (UINT4));
                ECFM_IPv4_L3IFINDEX = u4L3IfIndex;
            }
        }
        else if (pNetIp4IfInfo->u4Oper == IPIF_OPER_DISABLE)
        {
            /* Remove the management address entry */
            if ((INT1) u4L3IfIndex == ECFM_IPv4_L3IFINDEX)
            {
                ECFM_MEMSET (ECFM_MGMT_ADDRESS, 0x00, ECFM_MAX_MAN_ADDR_LEN);
                ECFM_MGMT_ADDR_DOMAIN_CODE = 0;
                ECFM_IPv4_L3IFINDEX = -1;
            }
        }
    }
    return;
}

/******************************************************************************
 * Function Name      : EcfmHandleIpv6ManAddrChg
 *
 * Description        : This function handles the Ipv6 address change information 
 *                      change message received from IPv6.
 *
 * Input(s)           : pQMsg - Message information received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE VOID
EcfmHandleIpv6ManAddrChg (tEcfmCcMsg * pMsg)
{
    tNetIpv6AddrInfo   *pNetIpv6AddrInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT4               u4BitMap = ECFM_INIT_VAL;
    pNetIpv6AddrInfo = &(pMsg->uMsg.Ip6ManAddr.Ipv6AddrInfo);
    u4IfIndex = pMsg->uMsg.Ip6ManAddr.u4Index;
    u4BitMap = pMsg->uMsg.Ip6ManAddr.u4Mask;
    if (u4BitMap == NETIPV6_ADDRESS_DELETE)
    {
        if ((INT1) u4IfIndex == ECFM_IPv6_L3IFINDEX)
        {
            ECFM_MEMSET (ECFM_MGMT_ADDRESS, 0x00, ECFM_MAX_MAN_ADDR_LEN);
            ECFM_MGMT_ADDR_DOMAIN_CODE = ECFM_INIT_VAL;
            ECFM_IPv6_L3IFINDEX = -1;
        }
    }
    else if (u4BitMap == NETIPV6_ADDRESS_ADD)
    {
        if (ECFM_IPv6_L3IFINDEX == -1)
        {
            ECFM_MGMT_ADDR_DOMAIN_CODE = IPVX_ADDR_FMLY_IPV6;
            ECFM_MEMCPY (ECFM_MGMT_ADDRESS, pNetIpv6AddrInfo->Ip6Addr.u1_addr,
                         IPVX_IPV6_ADDR_LEN);
            ECFM_MGMT_ADDRESS_LEN = IPVX_IPV6_ADDR_LEN;
            ECFM_IPv4_L3IFINDEX = u4IfIndex;
        }
    }
}

/****************************************************************************
 * Function Name      : EcfmHandleVlanMemberChg 
 *
 * Description        : This rotuine is used to handle Vlan Port Member ship
 *                      change Indication
 * 
 * Input(s)           : u2VlanId    - Vlan Id 
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleVlanMemberChg (UINT2 u2VlanId)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = NULL;
    pMepInfo = (tEcfmCcMepInfo *) RBTreeGetFirst (ECFM_CC_PORT_MEP_TABLE);

    do
    {
        if (pMepInfo == NULL)
        {
            break;
        }
        EcfmCcUtilUpdatePortState (pMepInfo, ECFM_INIT_VAL);

        if ((pMepInfo->b1MepCcmOffloadStatus ==
             ECFM_TRUE) && (pMepInfo->u4PrimaryVidIsid == u2VlanId))
        {
            /* First Disable MEP transmission from this MEP */
            /* Update Tx/Rx parameters for this MEP */
            EcfmCcmOffDeleteTxRxForMep (pMepInfo);

            /* Enable Tx/Rx for this MEP */
            if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
            {
                continue;
            }
        }

        EcfmCcHandleMacStatusChange (pMepInfo, ECFM_VLAN_MEMBER_CHANGE);
    }
    while ((pMepInfo = (tEcfmCcMepInfo *)
            RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                           (tRBElem *) pMepInfo, NULL)) != NULL);

    /* Evaluate and create if Mips can be created for all the ports and this
     * particular VLAN (Implicit MIPs) */
    EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanId, ECFM_TRUE);

    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleDeleteVlan
 *
 * Description        : This rotuine is used to handle VLAN Deletion Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      u2VlanId    - Vlan Identifier.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmHandleDeleteVlan (UINT4 u4ContextId, UINT2 u2VlanId)
{

    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmHandleDeleteVlan:"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }

    /* For all ports get MEPs configured on these ports */
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);
            /* Disable Tx/Rx for all MEPs configured on this port with 
             * the given VLAN Id 
             * */
            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);
                if (pMepInfo->u4PrimaryVidIsid == u2VlanId)
                {
                    EcfmCcmOffDeleteTxRxForMep (pMepInfo);

                    /* Disable MEP transmission from this MEP */
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    /* Evaluate and create if Mips can be created for all the ports and VLANs 
     * (Implicit MIPs) */
    EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanId, ECFM_TRUE);

    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleCreateVlan
 *
 * Description        : This rotuine is used to handle VLAN Creation Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      u2VlanId    - Vlan Identifier.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleCreateVlan (UINT4 u4ContextId, UINT2 u2VlanId)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmHandleCreateVlan:"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }

    /* For all ports get MEPs configured on these ports */
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {
                /* Get Next configured MEP on this port */
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);
                if (pMepInfo->u4PrimaryVidIsid == u2VlanId)
                {
                    /* First Disable MEP transmission from this MEP */
                    /* Update Tx/Rx parameters for this MEP */
                    EcfmCcmOffDeleteTxRxForMep (pMepInfo);

                    if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
                    {
                        return ECFM_FAILURE;
                    }
                    /* Enable Tx/Rx for this MEP */
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    /* Evaluate and create if Mips can be created for all the ports and this
     * particular VLAN (Implicit MIPs) */
    EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanId, ECFM_TRUE);
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleMstpDisable 
 *
 * Description        : This rotuine is used to handle MSTP Disable Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleMstpDisable (UINT4 u4ContextId)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_ENTRY ();

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmMstpDisableIndication:"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }

    /* For all ports get MEPs configured on these ports */
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);
            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {
                /* Get Next configured MEP on this port */
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);

                /* First Disable MEP transmission from this MEP */
                /* Update Tx/Rx parameters for this MEP */
                EcfmCcmOffDeleteTxRxForMep (pMepInfo);

                if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
                {
                    return ECFM_FAILURE;
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleMstpEnable 
 *
 * Description        : This rotuine is used to handle MSTP Enable Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleMstpEnable (UINT4 u4ContextId)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();

    /* For all ports get MEPs configured on these ports */
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo <= ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {
                /* Get Next configured MEP on this port */
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);

                /* First Disable MEP transmission from this MEP */
                /* Update Tx/Rx parameters for this MEP */
                EcfmCcmOffDeleteTxRxForMep (pMepInfo);

                if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
                {
                    return ECFM_FAILURE;
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleCreateIsid
 *
 * Description        : This rotuine is used to handle Isid Creation Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      u4Isid.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleCreateIsid (UINT4 u4ContextId, UINT4 u4Isid)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmHandleCreateIsid:"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }

    /* For all ports get MEPs configured on these ports */
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo < ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);

            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {                    /* Get Next configured MEP on this port */
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);

                if (pMepInfo->u4PrimaryVidIsid == u4Isid)
                {
                    /* First Disable MEP transmission from this MEP */
                    /* Update Tx/Rx parameters for this MEP */
                    EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                    if (EcfmCcmOffCreateTxRxForMep (pMepInfo) == ECFM_FAILURE)
                    {
                        return ECFM_FAILURE;
                    }
                    /* Enable Tx/Rx for this MEP */
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    EcfmCcUtilEvaluateAndCreateMip (-1, u4Isid, ECFM_TRUE);
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleDeleteIsid
 *
 * Description        : This rotuine is used to handle Isid Deletion Indication
 * 
 * Input(s)           : u4ContextId - Context Identifier.
 *                      u4Isid   .
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleDeleteIsid (UINT4 u4ContextId, UINT4 u4Isid)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepInfo     *pNextMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNo = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    if ((u4ContextId >= ECFM_MAX_CONTEXTS))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_ENTRY ();

    /* Check if ECFM module is started or not */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmHandleDeleteIsid:"
                     "ECFM Module is SHUTDOWN\r\n");
        return ECFM_FAILURE;
    }
    EcfmDeleteImplicitlyCreatedMips (-1, u4Isid);

    /* For all ports get MEPs configured on these ports */
    for (u2PortNo = ECFM_PORTS_PER_CONTEXT_MIN;
         u2PortNo < ECFM_CC_MAX_PORT_INFO; u2PortNo++)
    {
        pPortInfo = NULL;
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNo);

        if (pPortInfo != NULL)
        {
            u4IfIndex = pPortInfo->u4IfIndex;
            gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
            pMepInfo = NULL;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode, NULL);
            /* Disable Tx/Rx for all MEPs configured on this port with 
             * the given VLAN Id 
             * */
            while ((pMepInfo != NULL) && (pMepInfo->u4IfIndex == u4IfIndex))
            {
                pNextMepInfo =
                    (tEcfmCcMepInfo *)
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                   (tRBElem *) pMepInfo, NULL);
                if (pMepInfo->u4PrimaryVidIsid == u4Isid)
                {
                    EcfmCcmOffDeleteTxRxForMep (pMepInfo);
                    /* Disable MEP transmission from this MEP */
                }
                pMepInfo = pNextMepInfo;
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    ECFM_CC_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleAddToChannel
 *
 * Description        : This rotuine handles the event of port addition 
 *                      to a port channel.
 * 
 * Input(s)           : u2PortNum - Port Number
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleAddToChannel (UINT2 u2PortNum)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2PortChannel = ECFM_INIT_VAL;
    UINT2               u2ChannelPortNum = ECFM_INIT_VAL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);
    while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
    {
        /* Make all the vlan/isid aware MEPs inactive */
        if (pMepInfo->u4PrimaryVidIsid != 0)
        {
            pMepInfo->b1SavedActiveValue = pMepInfo->b1Active;
            if (pMepInfo->b1Active == ECFM_TRUE)
            {
                pMepInfo->b1Active = ECFM_FALSE;
                EcfmCcUtilNotifySm (pMepInfo, ECFM_IND_MEP_INACTIVE);
                EcfmCcUtilNotifyY1731 (pMepInfo, ECFM_IND_MEP_INACTIVE);
                EcfmLbLtNotifySM (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                                  pMepInfo->u2MepId,
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  ECFM_IND_MEP_INACTIVE);
            }
        }
        pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                  (tRBElem *) pMepInfo, NULL);
    }
    if (EcfmL2IwfGetPortChannelForPort (pPortInfo->u4IfIndex, &u2PortChannel)
        != L2IWF_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u2PortChannel,
                                            &u4ContextId, &u2ChannelPortNum)
        != VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    pPortInfo->u2ChannelPortNum = u2ChannelPortNum;

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    EcfmLbLtHandleAddToChannel (u2PortNum, u4CurrContextId);
    /* Evaluate MIP Creation for all the ports, for all VLANs */
    EcfmCcUtilEvaluateAndCreateMip (-1, -1, ECFM_TRUE);
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleRemoveFromChannel
 *
 * Description        : This rotuine handles the event of port removal
 *                      from a port channel.
 * 
 * Input(s)           : u2PortNum - Port Number
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleRemoveFromChannel (UINT2 u2PortNum)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    gpEcfmCcMepNode->u2PortNum = u2PortNum;
    pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);
    while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
    {
        /* Make all the vlan/isid aware MEPs active */
        if (pMepInfo->u4PrimaryVidIsid != 0)
        {
            pMepInfo->b1Active = pMepInfo->b1SavedActiveValue;
            if (pMepInfo->b1Active == ECFM_TRUE)
            {
                EcfmCcUtilNotifySm (pMepInfo, ECFM_IND_MEP_ACTIVE);
                EcfmCcUtilNotifyY1731 (pMepInfo, ECFM_IND_MEP_ACTIVE);
                EcfmLbLtNotifySM (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                                  pMepInfo->u2MepId,
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  ECFM_IND_MEP_ACTIVE);

            }
        }
        pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                  (tRBElem *) pMepInfo, NULL);
    }
    pPortInfo->u2ChannelPortNum = 0;

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    EcfmLbLtHandleRemoveFromChannel (u2PortNum, u4CurrContextId);
    /* Evaluate MIP Creation for all the ports, for all VLANs */
    EcfmCcUtilEvaluateAndCreateMip (-1, -1, ECFM_TRUE);

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleCreateContext
 *
 * Description        : This rotuine handles the event of context creation.
 * 
 * Input(s)           : u4ContextId - Context Identifier
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleCreateContext (UINT4 u4ContextId)
{
    UINT4               u4BridgeMode = ECFM_INVALID_BRIDGE_MODE;
    ECFM_CC_TRC_FN_ENTRY ();
    if (EcfmCcHandleCreateContext (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* Create Context at LBLT also */
    ECFM_LBLT_HANDLE_CREATE_CONTEXT (u4ContextId);
    /* Get the bridge mode */
    if (EcfmL2IwfGetBridgeMode (ECFM_CC_CURR_CONTEXT_ID (), &u4BridgeMode)
        != L2IWF_SUCCESS)
    {
        /* Delete Context at LBLT also */
        ECFM_LBLT_HANDLE_DELETE_CONTEXT (u4ContextId);
        EcfmCcHandleDeleteContext (u4ContextId);
        return ECFM_FAILURE;
    }
    ECFM_CC_BRIDGE_MODE () = u4BridgeMode;
    ECFM_LBLT_UPDATE_BRIDGE_MODE (ECFM_CC_BRIDGE_MODE ());
    if (u4BridgeMode == ECFM_INVALID_BRIDGE_MODE)
    {
        /* Don't Start ECFM module */
        return ECFM_SUCCESS;
    }
    /* Start ECFM module for the newely created context */
    if (EcfmUtilModuleStart () != ECFM_SUCCESS)
    {
        /* Delete Context at LBLT also */
        ECFM_LBLT_HANDLE_DELETE_CONTEXT (u4ContextId);
        EcfmCcHandleDeleteContext (u4ContextId);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmHandleVlanEtherTypeChngInd 
 *
 * Description        : This rotuine is used to handle VLAN EtherType Change
 *                      change Indication.
 * 
 * Input(s)             u4IfIndex   - IfIndex of the port for which Ether Type
 *                                    is Updated.
 *                      u1EtherType - Ingress or Egress Ether Type
 *                      u2EtherTypeValue - New Ether Type Value  
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmHandleVlanEtherTypeChngInd (UINT4 u4IfIndex, UINT2 u2EtherTypeValue,
                                UINT1 u1EtherType)
{
    ECFM_CC_TRC_FN_ENTRY ();

#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherTypeValue);
    UNUSED_PARAM (u1EtherType);
#endif

    /* Call NPAPI to Update Rx Ether Type for Offloaded MEPs */
#ifdef NPAPI_WANTED
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        if (EcfmFsMiEcfmHwSetVlanEtherType (ECFM_CC_CURR_CONTEXT_ID (),
                                            u4IfIndex, u2EtherTypeValue,
                                            u1EtherType) != FNP_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                         "EcfmHandleVlanEtherTypeChngInd:"
                         " Vlan Ether Type Set failed \r\n");
            return ECFM_FAILURE;
        }
    }
#endif /* NPAPI_WANTED  */

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmccque.c
 ****************************************************************************/
