/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmltism.c,v 1.27 2015/05/13 11:00:16 siva Exp $
 *
 * Description: This file contains the Functionality of the LinkTrace
 *              Initiator State Machine.
 *******************************************************************/

#include "cfminc.h"
#include "cfmltism.h"

/******************************************************************************
 * Function Name      : EcfmLbLtClntLtInitSm                                   
 *                                                                             
 * Description        : LTM Initiator State Machine formats and transmits the  
 *                      LTM PDU on Administrators command.                     
 *                      It adds the transmitted LTM entry in the LtmReplylist. 
 *                                                                             
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd  
 *                                    and other SM related information.        
 *                       u1Event - Event ID                                    
 *                                                                             
 * Output(s)          : None                                                   
 *                                                                             
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                            
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtClntLtInitSm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 u1Event)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLTInfo    *pLtiSmInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP information from the PduSmInfo received */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get LT Initiator SM information maintained per MEP */
    pLtiSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);
    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLbLtClntLtInitSm: LT Init SM Called with Event:"
                        " %d, &State: %d \r\n", u1Event,
                        pLtiSmInfo->u1LtInitSmState);

    /* Get the index of the action routine and call the function pointer based 
     * on state and event */
    if (ECFM_LBLT_LTINIT_STATE_MACHINE (u1Event, pLtiSmInfo->u1LtInitSmState,
                                        pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                            "EcfmLbLtClntLtInitSm : LinkTrace Initiator SM for"
                            "Mep%u on port %u doesn't function Correctly \r\n",
                            pMepInfo->u2MepId, pMepInfo->u2PortNum);

        /* Set the Transmit LTM Status to READY */
        pLtiSmInfo->u1TxLtmStatus = ECFM_TX_STATUS_READY;
        pLtiSmInfo->b1TxLtmResult = ECFM_FALSE;
        EcfmSendLtmEventToCli (pMepInfo, CLI_EV_ECFM_LT_TRANS_STOP);
        pLtiSmInfo->CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmLtInitSmSetStateIdle 
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      BEGIN in DEFAULT state which changes the current state
 *                      to IDLE. It also resets the LTM Transmit Status. 
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS
 *****************************************************************************/
PRIVATE INT4
EcfmLtInitSmSetStateIdle (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLTInfo    *pLtiSmInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP information from the received pdusm information */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get LT Initiator SM information maintained per MEP */
    pLtiSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);

    /* Set the Transmit LTM Status to READY */
    pLtiSmInfo->u1TxLtmStatus = ECFM_TX_STATUS_READY;

    /* Sets the state to IDLE state */
    ECFM_LBLT_LTM_INIT_SET_STATE (pMepInfo, ECFM_LTI_STATE_IDLE);
    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                   "EcfmLtInitSmSetStateIdle :LT Init State Set to IDLE \r\n");
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLtInitSmSetStateDefault 
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      ECFM_SM_EV_MEP_NOT_ACTIVE in which state machine is 
 *                      set to DEFAULT State to halt its functionality. 
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS
 *****************************************************************************/
PRIVATE INT4
EcfmLtInitSmSetStateDefault (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP Info from received PduSmInfo */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Stop LTM TX Timer if runnning. 
     * If DEFAULT State is achieved from TRANSMT state then this timer will 
     * always be running, but this can also be acheived from IDLE state where 
     * this timer will not be running */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LTM_TX, pMepInfo);

    /* Changes the current state to DEFAULT state */
    ECFM_LBLT_LTM_INIT_SET_STATE (pPduSmInfo->pMepInfo, ECFM_LTI_STATE_DEFAULT);
    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                   "EcfmLtInitSmSetStateDefault :"
                   "MEP is InActive, LT Init SM State set to DEFAULT \r\n");
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLtInitSmSetStateTransmit
 *
 * Description        : In this state the state machine formats and transmits 
 *                      LTM and an entry is made in the LtmReplylist.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other SM related information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLtInitSmSetStateTransmit (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP information from the received pdusm information */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Change state to TRANSMIT */
    ECFM_LBLT_LTM_INIT_SET_STATE (pMepInfo, ECFM_LTI_STATE_TRANSMIT);

    /* Set the Transmit LTM Status to NotReady */
    pMepInfo->LtInfo.u1TxLtmStatus = ECFM_TX_STATUS_NOT_READY;

    /* Format LTM PDU for transmission */
    if (EcfmLtInitSmXmitLtm (pPduSmInfo) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLtInitSmSetStateTransmit:"
                       " LTM PDU formation failed \r\n");

        /* Change state to TRANSMIT */
        ECFM_LBLT_LTM_INIT_SET_STATE (pMepInfo, ECFM_LTI_STATE_IDLE);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLtInitSmXmitLtm 
 *
 * Description        : This routine formats and transmits the LTM.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other SM related information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLtInitSmXmitLtm (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLTInfo    *pLtiSmInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;    /* pointer to CRU buffer */
    UINT1              *pu1PduStart = NULL;    /* pointer containing Start of PDU */
    UINT1              *pu1PduEnd = NULL;    /* pointer containing End of PDU */
    UINT1              *pu1EthLlcHdr = NULL;    /* Pointer for filling the LLC 
                                                 * Hdr */
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1               u1Priority = ECFM_INIT_VAL;
    UINT1               u1DropEligible = ECFM_FALSE;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get MEP Info from PduSmInfo Structure */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);

    /* Get LT Initiator SM Info maintained per MEP */
    pLtiSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);

    /*Allocates the CRU Buffer */
    pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_LTM_PDU_SIZE, ECFM_INIT_VAL);
    if (pBuf == NULL)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_ALL_FAILURE_TRC,
                       "EcfmLtInitSmXmitLtm:Buffer Allocation failed\r\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);

    /* Initialise start and end pointers */
    pu1PduStart = ECFM_LBLT_PDU;
    pu1PduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;

    /* LTM PDU header */
    EcfmLtInitSmFormatLtmPduHdr (pMepInfo, &pu1PduEnd);

    /* Fill the Other fields of LTM PDU */
    if (EcfmLtInitSmPutLtmInfo (pMepInfo, &pu1PduEnd) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLtInitSmXmitLtm "
                       "LTM Info doesn't have Rmep Mac Address !! \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Fill Additional LTM TLVs */
    EcfmLtInitSmPutLtmTlv (pMepInfo, &pu1PduEnd);

    /* Calculate the Formatted PDU Length */
    u2PduLength = (UINT2) (pu1PduEnd - pu1PduStart);

    /*Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "EcfmLtInitSmXmitLtm:Copying into Bufferfailed \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Y.1731: format Ethernet & LLC SNAP Header in the CRU Buffer 
     */
    EcfmFormatLbLtTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr,
                                 u2PduLength, ECFM_OPCODE_LTM);
    u2PduLength = (UINT2) (pu1EthLlcHdr - pu1PduStart);
    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart, (UINT4) (u2PduLength))
        != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "EcfmLtInitSmXmitLtm:Prepending into Buffer failed \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Y.1731: Ltm Priority and Drop Eligibility are configured acc. to  
     * Y1731 Module enable/disable status*/
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))

    {
        u1Priority = pMepInfo->LtInfo.u1TxLtmVlanPriority;
        u1DropEligible = pMepInfo->LtInfo.b1TxLtmDropEligible;
    }

    else

    {
        u1Priority = pMepInfo->u1CcmLtmPriority;
    }
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmLtInitSmXmitLtm: Sending out CFMPDU to lower "
                        "layer...\n");
    /* Add entry in the LtmReplyList */
    if (EcfmLtInitSmAddLtmReplyListEntry (pLtiSmInfo, pMepInfo) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLtInitSmXmitLtm "
                       "LTM Node cannot be added to LtmReplyList!! \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
    {
        if (EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->pPortInfo->u2PortNum,
                                       pMepInfo->u4PrimaryVidIsid,
                                       u1Priority, u1DropEligible,
                                       pMepInfo->u1Direction,
                                       ECFM_OPCODE_LTM, NULL,
                                       NULL) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtInitSmXmitLtm: PDU Transmission failed \r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
    }
    else
    {
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
    }

    /* Y.1731: When Y1731 Module is enabled the LTR Tx Timer start with the 
     * configured Ltr Timeout value otherwise default
     */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))

    {
        UINT4               u4LtrTimeout = ECFM_INIT_VAL;

        /*Converting LtrTimeout to MSEC */
        u4LtrTimeout =
            ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (pMepInfo->LtInfo.
                                                  u2LtrTimeOut);

        /* Start Timer LTM TX Timer with configured interval 
         */
        if (EcfmLbLtTmrStartTimer
            (ECFM_LBLT_TMR_LTM_TX, pMepInfo, u4LtrTimeout) != ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtInitSmXmitLtm: LTM Tx Timer cannot be "
                           "started \r\n");
            return ECFM_FAILURE;
        }
    }

    else

    {

        /* Start Timer LTM TX Timer with 5 sec */
        if (EcfmLbLtTmrStartTimer
            (ECFM_LBLT_TMR_LTM_TX, pMepInfo,
             ECFM_LTM_TX_INTERVAL * ECFM_NUM_OF_MSEC_IN_A_SEC) != ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLtInitSmXmitLtm: LTM Tx Timer cannot "
                           "be started \r\n");
            return ECFM_FAILURE;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmLtInitSmFormatLtmPduHdr
 *
 * Description        : This rotuine is used to fill the CFM PDU Header
 * 
 * Input              : pMepInfo     - Pointer to the MEP structure  
 *                   ppu1PduEnd   - Pointer to the Pdu     
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmLtInitSmFormatLtmPduHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1PduEnd)
{
    UINT1               u1Flag = ECFM_INIT_VAL;    /* Value of the flag */
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    UINT1              *pu1Pdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *ppu1PduEnd;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1Pdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_OPCODE_LTM);
    if (ECFM_LBLT_GET_USE_FDB_ONLY (pMepInfo->LtInfo.u1TxLtmFlags))

        /* Fill 1 byte of Flag field with only UseFDBOnly Bit set and all 
         * other bits as 0 */
    {
        ECFM_LBLT_SET_USE_FDB_ONLY (u1Flag);
    }
    ECFM_PUT_1BYTE (pu1Pdu, u1Flag);

    /* Fill 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_LTM_FIRST_TLV_OFFSET);
    *ppu1PduEnd = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function           : EcfmLtInitSmPutLtmInfo 
 *
 * Description        : This routine is used to fill the fields in the LTM to be 
 *                      transmitted. 
 *
 * Input(s)           : pMepInfo   - Pointer to MEP Info that generates LTM
 *                      ppu1PduEnd - Pointer to PDU from where LTM fields need 
 to 
 *                                  be filled.
 *                       
 * Output(s)          : None.
 *
 * Returns            : None
 ******************************************************************************/
PRIVATE INT4
EcfmLtInitSmPutLtmInfo (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1PduEnd)
{
    tEcfmLbLtLTInfo    *pLtiSmInfo = NULL;
    tEcfmMacAddr        RMepMacAddr;
    UINT1              *pu1Pdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get LT Initiator SM Info maintained per MEP */
    pLtiSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);

    /* Fill the 4 byte Transaction ID to be transmitted. At MEP
     * Creation time both these transaction Id's are intialized with 1.
     * Next LTM Transaction ID is incremented after the timer LTM_TX
     *  timesout */
    pLtiSmInfo->u4TxLtmSeqNum = pLtiSmInfo->u4LtmNextSeqNum;
    pu1Pdu = *ppu1PduEnd;
    ECFM_PUT_4BYTE (pu1Pdu, pLtiSmInfo->u4TxLtmSeqNum);

    /* Set the LTM TTL field */
    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pLtiSmInfo->u2TxLtmTtl));

    if (ECFM_LBLT_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        return ECFM_FAILURE;
    }
    /* Set Original MAC Address i.e. MEPs MAC Address */
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pMepInfo->u2PortNum)->u4IfIndex, pu1Pdu);
    pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;

    /* If the Target Mep's MepId is provided, getthat RMEPs MAC Address
     * from RMep CCM Database and fill the Target MAC Address */
    if (pLtiSmInfo->b1TxLtmTargetIsMepId != ECFM_TRUE)

    {
        ECFM_MEMCPY (pu1Pdu, pLtiSmInfo->TxLtmTargetMacAddr,
                     ECFM_MAC_ADDR_LENGTH);
    }

    else

    {

        /* Get the Remote MEPs MAC Address */
        ECFM_MEMSET (&RMepMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
        if (EcfmLbLtUtilGetRMepMacAddr
            (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex, pMepInfo->u2MepId,
             pLtiSmInfo->u2TxLtmTargetMepId, RMepMacAddr) != ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_OS_RESOURCE_TRC,
                           "EcfmLtInitSmPutLtmInfo "
                           "Rmep Mac Address not Found !! \r\n");
            return ECFM_FAILURE;
        }

        /* Put the Remote MEPs MACAddress asthe Target MEP's MAC
         * Address */
        ECFM_MEMCPY (pu1Pdu, RMepMacAddr, ECFM_MAC_ADDR_LENGTH);
        ECFM_MEMCPY (pLtiSmInfo->TxLtmTargetMacAddr, RMepMacAddr,
                     ECFM_MAC_ADDR_LENGTH);
    }
    pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;
    *ppu1PduEnd = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function           : EcfmLtInitSmPutLtmTlv                                  
 *                                                                             
 * Description        : This routine is used to fill the LTM Tlvs to be        
 *                      transmitted.                                           
 *                                                                             
 * Input(s)           : pMepInfo    - Pointer to MEP Info that generates LTM    

 *                      ppu1PduEnd  - Pointer to PDU from where LTM fields need 

 *                                    to be filled.                            
 *                                                                             
 * Output(s)          : None.                                                  
 *                                                                             
 * Returns            : None                                                   
 *****************************************************************************/
PRIVATE VOID
EcfmLtInitSmPutLtmTlv (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1PduEnd)
{
    tEcfmLbLtLTInfo    *pLtiSmInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1               au1NullEgressId[ECFM_EGRESS_ID_LENGTH];
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (au1NullEgressId, ECFM_INIT_VAL, ECFM_EGRESS_ID_LENGTH);
    pu1Pdu = *ppu1PduEnd;

    /* Get LT Initiator SM Info maintained per MEP */
    pLtiSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);

    /* Y.1731: When Y.1731 is disabled only then Additional TLVs are 
     * transmitted in LTM PDU
     */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum))

    {

        /* Fill LTM Egress Identifier TLV Type */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_LTM_EGRESS_ID_TLV_TYPE);

        /* Fill LTM Egress Identifier TLV Length */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_EGRESS_ID_LENGTH);
        if (ECFM_MEMCMP
            (pLtiSmInfo->au1TxLtmEgressId, au1NullEgressId,
             ECFM_EGRESS_ID_LENGTH) == 0)

        {
            ECFM_PUT_2BYTE (pu1Pdu, pMepInfo->u2PortNum);
            EcfmCfaGetContextMacAddr (ECFM_LBLT_CURR_CONTEXT_ID (), pu1Pdu);
            pu1Pdu = pu1Pdu + ECFM_MAC_ADDR_LENGTH;
            MEMCPY (pLtiSmInfo->au1TxLtmEgressId,(pu1Pdu-ECFM_EGRESS_ID_LENGTH),ECFM_EGRESS_ID_LENGTH);
        }

        else

        {

            /* Fill LTM Egress Identifier TLV Value */
            ECFM_MEMCPY (pu1Pdu, pLtiSmInfo->au1TxLtmEgressId,
                         ECFM_EGRESS_ID_LENGTH);
            pu1Pdu = pu1Pdu + ECFM_EGRESS_ID_LENGTH;
        }

        /* Format Sender ID TLV If the MA to which this MEP belongs allows to 
         * transmit Sender ID TLV then add Sender ID TLV in the LTM */
        ECFM_LBLT_SET_SENDER_ID_TLV (pMepInfo, pu1Pdu);

        /* Fill Organizational Specific TLV Type */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_TLV_TYPE);

        /* Fill Organizational Specific TLV Length */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_ORG_SPEC_MIN_LENGTH);

        /* Fill OUI */
        ECFM_LBLT_SET_OUI (pu1Pdu);

        /* Fill SubType in OrgSpecTLV */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_SUBTYPE_VALUE);
    }

    /* Fill the Optional Field "VALUE" in the Organization Specific TLV */
    /* Currently this is not transmitted  */
    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_END_TLV_TYPE);
    *ppu1PduEnd = pu1Pdu;
    ECFM_LBLT_TRC_FN_ENTRY ();
    return;
}

/*****************************************************************************
 * Function           : EcfmLtInitSmAddLtmReplyListEntry 
 *
 * Description        : This routine is used to add transmitted LTM in the      
 *                      LtmReplyList indexed by the Transaction Id. This routine 
 *                      handles the LTM node addition failure in case of        
 *                      resource availability failure.
 *
 * Input(s)           : pLtiSmInfo -  Pointer to the LT Info  
 *                      pMepInfo    - Pointer to Mep that transmitted this LTM.
 *                       
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmLtInitSmAddLtmReplyListEntry (tEcfmLbLtLTInfo * pLtiSmInfo,
                                  tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLtmReplyListInfo LtmNode;
    tEcfmLbLtLtmReplyListInfo *pLtmNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocate Memory to the LTM Node to be addded in the LtmReplyList Info */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_LTM_REPLY_LIST_TABLE (pLtmNode) == NULL)

    {
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL) != 0)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_OS_RESOURCE_TRC,
                           "EcfmLtInitSmAddLtmReplyListEntry:"
                           "Memory Block exists but Memory Allocation to LTM "
                           "Node Failed \r\n");
            return ECFM_FAILURE;
        }

        /* If the Addition of this LTM entry exceeds the resources allocated
         * to LtmReplyList, then the oldest LTM entry in the LtmReplyList are
         * deleted. */
        /* Get the LTM Node from the LtmReplyListInfo */
        ECFM_MEMSET (&LtmNode, ECFM_INIT_VAL,
                     ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);
        LtmNode.u4MdIndex = pMepInfo->u4MdIndex;
        LtmNode.u4MaIndex = pMepInfo->u4MaIndex;
        LtmNode.u2MepId = pMepInfo->u2MepId;

        pLtmNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGet
            (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) & LtmNode);
        if (pLtmNode == NULL)
        {
            pLtmNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGetNext
                (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) & LtmNode, NULL);
        }

        if (pLtmNode != NULL)

        {
            EcfmLbLtUtilHandleLtNodeAddFail (pLtmNode);
        }

        /* Allocate Memory to the LTM Node to be addded in the 
         * LtmReplyList Info */
        if (ECFM_ALLOC_MEM_BLOCK_LBLT_LTM_REPLY_LIST_TABLE (pLtmNode) == NULL)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_OS_RESOURCE_TRC,
                           "EcfmLtInitSmAddLtmReplyListEntry:"
                           "Memory Allocation to LTMnode FAILED\r\n");
            return ECFM_FAILURE;
        }
    }
    ECFM_MEMSET (pLtmNode, ECFM_INIT_VAL, ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);

    /* Add MegIndex field */
    pLtmNode->u4MdIndex = pMepInfo->u4MdIndex;

    /* Add MeIndex field */
    pLtmNode->u4MaIndex = pMepInfo->u4MaIndex;

    /* Add MegId field */
    pLtmNode->u2MepId = pMepInfo->u2MepId;

    /* Add Sequence Number field  */
    pLtmNode->u4LtmSeqNum = pLtiSmInfo->u4TxLtmSeqNum;

    /* Add LTM TTL field  */
    pLtmNode->u2LtmTtl = pLtiSmInfo->u2TxLtmTtl;

    /* Add Target MAC Address in the LTM Node */
    ECFM_MEMCPY (pLtmNode->TargetMacAddr, pLtiSmInfo->TxLtmTargetMacAddr,
                 ECFM_MAC_ADDR_LENGTH);

    /* Add RBTree Node */
    if (RBTreeAdd (ECFM_LBLT_LTM_REPLY_LIST, pLtmNode) != ECFM_RB_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLtInitSmAddLtmReplyListEntry:"
                       " RBTree Add Failed!! \r\n");
        return ECFM_FAILURE;
    }

    /* Initialise the LTR DLL for this LTM Node to store the LTRs that will be 
     * received for this LTM sequence Number */
    TMO_DLL_Init (&(pLtmNode->LtrList));
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmLtInitSmEvtTimerTimeout 
 *
 * Description        : This routine increments the nextTransID by 1 and 
 *                      changes the state to IDLE whenever it receives an event
 *                      from FSAP2 for LTM Tx Timer times out.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other SM related information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS 
 ******************************************************************************/
PRIVATE INT4
EcfmLtInitSmEvtTimerTimeout (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtLTInfo    *pLtiSmInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get LT Inititator SM information maintained per MEP */
    pLtiSmInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pPduSmInfo->pMepInfo);

    /* Increment the LTM Transaction ID for the next LTM transmission */
    ECFM_LBLT_INCR_LTM_SEQ_NUM (pLtiSmInfo);
    EcfmSendLtmEventToCli (pPduSmInfo->pMepInfo, CLI_EV_ECFM_LT_TRANS_STOP);
    pLtiSmInfo->CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
    /* State should be reset to IDLE and Transmit LTM status should be reset so  
     * that another LTM intiation can be handled */
    EcfmLtInitSmSetStateIdle (pPduSmInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function           : EcfmLtInitSmEvtImpossible 
 *
 * Description        : This routine handles the occurrence of an impossible or 
 *                      wrong event in a given state. In this routine this 
 *                      event is ignored and no processing is done based on 
 *                      this event state combination.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                                    information regarding MP info, PDU rcvd 
 *                                    and other SM related information.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS
 ******************************************************************************/
PRIVATE INT4
EcfmLtInitSmEvtImpossible (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    ECFM_LBLT_TRC_FN_ENTRY ();
    UNUSED_PARAM (pPduSmInfo);
    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLtInitSmEvtImpossible:"
                   "IMPOSSIBLE EVENT-STATE Combination Occurred LT initiator "
                   "SM\r\n");
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_FAILURE;
}

/******************************************************************************
 * Function           : EcfmLbLtUtilHandleLtNodeAddFail
 * 
 * Description        : This procedure is used to get the oldest LTM Entry in
 *                      the LtmReplyList. If found, then the LTM entry is
 *                      deleted from the list.
 *
 * Input(s)           : pLtmNode - Pointer to the LTM node from the LtmReplyList
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtUtilHandleLtNodeAddFail (tEcfmLbLtLtmReplyListInfo * pLtmNode)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmLbLtLtrInfo   *pLtrNextNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Before Deleting the LTM node, its related LTR nodes maintained in the DLL
     * needs to be deleted. Also the LTR nodes from the Global RBTree maintained
     * needs to be deleted. The memory assigned to some variables while storing
     * the LTR node also needs to be freed */
    pLtrNode = (tEcfmLbLtLtrInfo *) TMO_DLL_First (&(pLtmNode->LtrList));
    while (pLtrNode != NULL)

    {
        pSenderId = &(pLtrNode->SenderId);

        /* Freeing the Memory allocated to the variables in the LTR node */
        if (pSenderId->ChassisId.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL,
                                 pSenderId->ChassisId.pu1Octets);
            pSenderId->ChassisId.pu1Octets = NULL;
        }
        if (pSenderId->MgmtAddress.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL,
                                 pSenderId->MgmtAddress.pu1Octets);
            pSenderId->MgmtAddress.pu1Octets = NULL;
        }
        if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL,
                                 pSenderId->MgmtAddressDomain.pu1Octets);
            pSenderId->MgmtAddressDomain.pu1Octets = NULL;
        }
        if (pLtrNode->OrgSpecTlv.Value.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL,
                                 pLtrNode->OrgSpecTlv.Value.pu1Octets);
            pLtrNode->OrgSpecTlv.Value.pu1Octets = NULL;
        }
        if (pLtrNode->EgressPortId.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
                                 pLtrNode->EgressPortId.pu1Octets);
            pLtrNode->EgressPortId.pu1Octets = NULL;
        }
        if (pLtrNode->IngressPortId.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
                                 pLtrNode->IngressPortId.pu1Octets);
            pLtrNode->IngressPortId.pu1Octets = NULL;
        }

        /* Get the next LTR node from the LTR list */
        pLtrNextNode =
            (tEcfmLbLtLtrInfo *) TMO_DLL_Next (&(pLtmNode->LtrList),
                                               (tEcfmDllNode *) & (pLtrNode->
                                                                   LtrTableMepDllNode));

        /* Delete the LTR node from the DLL maintained by MEP per LTM */
        TMO_DLL_Delete (&(pLtmNode->LtrList), &(pLtrNode->LtrTableMepDllNode));

        /* Delete the LTR RBTree Node maintianed in the Global */
        RBTreeRem (ECFM_LBLT_LTR_TABLE, (tRBElem *) pLtrNode);

        /* Free the memory allocated to the LTR Node from the LTR POOL */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL, (UINT1 *) pLtrNode);
        pLtrNode = pLtrNextNode;
    }
    /* Delete the LTM RBTree Node from the LtmReplyList */
    RBTreeRem (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) pLtmNode);
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL,
                         (UINT1 *) pLtmNode);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmSendLtmEventToCli
 *
 * Description        : This routine will be called on the exit of the LTM 
 *                      transaction by the LT initiator and LT initiator 
 *                      receive SM.
 *                        
 * Input(s)           :  pMepInfo - Info of the MEP which has initiated the
 *                       LTM.
 *                       i4EventType - Event type
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : None.
 ******************************************************************************/
PUBLIC VOID
EcfmSendLtmEventToCli (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventType)
{
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tEcfmLbLtCliEvInfo *pCliEvent = NULL;
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepInfo);
    pCliEvent = ECFM_LBLT_CLI_EVENT_INFO (pLtInfo->CurrentCliHandle);
    if (pCliEvent == NULL)

    {
        return;
    }
    switch (u1EventType)

    {
        case CLI_EV_ECFM_LT_TRANS_STOP:
        default:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_LT_TRANS_STOP);
            break;
    }
    ECFM_GIVE_SEMAPHORE (pCliEvent->SyncSemId);
    return;
}

/******************************************************************************
 *                           End  of File cfmltism.c                          *
 ******************************************************************************/
