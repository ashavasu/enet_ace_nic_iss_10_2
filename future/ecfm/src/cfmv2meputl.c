/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmv2meputl.c,v 1.33 2016/03/23 12:08:21 siva Exp $
 *
 * Description: This file contains commom util Low Level Routines 
 *              for MEP Table used by both IEEECFM MIB 
 *              Versions (D8.0 and D8.1).
 *******************************************************************/

#include "cfminc.h"
#include "fscfmmcli.h"
#include "fsmiy1cli.h"

/* LOW LEVEL Routines for Table : Dot1agCfmMepTable. */

/****************************************************************************
 Function    :  EcfmMepUtlValAgMepTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

PUBLIC INT1
EcfmMepUtlValAgMepTable (UINT4 u4Dot1agCfmMdIndex,
                         UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to indices MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP exists for the indices\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetNextIndexAgMepTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                nextDot1agCfmMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
PUBLIC INT1
EcfmMepUtlGetNextIndexAgMepTable (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 *pu4NextDot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 *pu4NextDot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4NextDot1agCfmMepIdentifier)
{
    tEcfmCcMepInfo     *pMepNextNode = NULL;

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    gpEcfmCcMepNode->u4MdIndex = u4Dot1agCfmMdIndex;
    gpEcfmCcMepNode->u4MaIndex = u4Dot1agCfmMaIndex;
    gpEcfmCcMepNode->u2MepId = (UINT2) u4Dot1agCfmMepIdentifier;

    /* Get MEP entry corresponding to indices next to MdIndex, MaIndex, MepId */
    pMepNextNode = (tEcfmCcMepInfo *) RBTreeGetNext
        (ECFM_CC_MEP_TABLE, (tRBElem *) (gpEcfmCcMepNode), NULL);

    if (pMepNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP exists for the indices\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to indices next to MdIndex, MaIndex, MepId exists 
     */
    /* Set the next indices from corresponding MEP next node */
    *pu4NextDot1agCfmMdIndex = pMepNextNode->u4MdIndex;
    *pu4NextDot1agCfmMaIndex = pMepNextNode->u4MaIndex;
    *pu4NextDot1agCfmMepIdentifier = (UINT4) (pMepNextNode->u2MepId);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepIfIndex
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepIfIndex (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           INT4 *pi4RetValDot1agCfmMepIfIndex)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the IfIndex from the corresponding MEP entry */
    *pi4RetValDot1agCfmMepIfIndex = (INT4) (pMepNode->u2PortNum);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDirection
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDirection (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 *pi4RetValDot1agCfmMepDirection)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the MEP's Direction from the corresponding MEP entry */
    *pi4RetValDot1agCfmMepDirection = (INT4) (pMepNode->u1Direction);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepPrimaryVid
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepPrimaryVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepPrimaryVid (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 *pu4RetValDot1agCfmMepPrimaryVid)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    /* Check if MEP's primary Vid is same as MA's Primary Vid,
       MEP primaryVid to be returned should be 0 */
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry Exists for given indices\n");
        return ECFM_FAILURE;

    }
    if (pMaNode->u4PrimaryVidIsid == pMepNode->u4PrimaryVidIsid)
    {
        *pu4RetValDot1agCfmMepPrimaryVid = 0;
        return SNMP_SUCCESS;
    }
    /* Set the MEP PrimaryVid from the corresponding MEP entry */
    *pu4RetValDot1agCfmMepPrimaryVid = (UINT4) (pMepNode->u4PrimaryVidIsid);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepActive
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepActive (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          INT4 *pi4RetValDot1agCfmMepActive)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the MepActive from the corresponding MEP entry */
    if (pMepNode->b1Active == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepActive = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepActive = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepFngState
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepFngState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepFngState (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            INT4 *pi4RetValDot1agCfmMepFngState)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepNode);

    /* Set the Mep Fng State from the corresponding MEP entry */
    switch (pFngInfo->u1FngState)
    {
        case ECFM_MEP_FNG_STATE_RESET:
            *pi4RetValDot1agCfmMepFngState = ECFM_MEP_FNG_RESET;
            break;
        case ECFM_MEP_FNG_STATE_DEFECT:
            *pi4RetValDot1agCfmMepFngState = ECFM_MEP_FNG_DEFECT;
            break;
        case ECFM_MEP_FNG_STATE_DEFECT_REPORTED:
            *pi4RetValDot1agCfmMepFngState = ECFM_MEP_FNG_DEFECT_REPORTED;
            break;
        case ECFM_MEP_FNG_STATE_DEFECT_CLEARING:
            *pi4RetValDot1agCfmMepFngState = ECFM_MEP_FNG_DEFECT_CLEARING;
            break;
        default:
            *pi4RetValDot1agCfmMepFngState = ECFM_MEP_FNG_RESET;
            break;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepCciEnabled
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCciEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepCciEnabled (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 *pi4RetValDot1agCfmMepCciEnabled)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    if (pCcInfo->b1CciEnabled == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepCciEnabled = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepCciEnabled = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepCcmLtmPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCcmLtmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepCcmLtmPriority (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4RetValDot1agCfmMepCcmLtmPriority)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Mep CcmLtmPriority from the corresponding Mep node */
    *pu4RetValDot1agCfmMepCcmLtmPriority = (UINT4) (pMepNode->u1CcmLtmPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepMacAddress (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              tMacAddr * pRetValDot1agCfmMepMacAddress)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Put MacAddress from PortInfo corresponding to Mep's IfIndex */
    if (pMepNode->u2PortNum == 0)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Port information exists for the MEP\n");
        ECFM_MEMSET (pRetValDot1agCfmMepMacAddress, ECFM_INIT_VAL,
                     ECFM_MAC_ADDR_LENGTH);
        return SNMP_SUCCESS;
    }
    if (ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Port information \n");
        ECFM_MEMSET (pRetValDot1agCfmMepMacAddress, ECFM_INIT_VAL,
                     ECFM_MAC_ADDR_LENGTH);
        return SNMP_SUCCESS;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_CC_PORT_INFO
                               (pMepNode->u2PortNum)->u4IfIndex,
                               pRetValDot1agCfmMepMacAddress);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepLowPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLowPrDef
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepLowPrDef (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            INT4 *pi4RetValDot1agCfmMepLowPrDef)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Mep Low priority defect from the corresponding MEP entry */
    *pi4RetValDot1agCfmMepLowPrDef = (INT4) (pMepNode->u1LowestAlarmPri);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepFngAlarmTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepFngAlarmTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepFngAlarmTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 *pi4RetValDot1agCfmMepFngAlarmTime)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Fng Alarm time from the corresponding MEP entry */
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepNode);

    *pi4RetValDot1agCfmMepFngAlarmTime = (INT4) (pFngInfo->u2FngAlarmTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepFngResetTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepFngResetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepFngResetTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 *pi4RetValDot1agCfmMepFngResetTime)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Fng Reset time from the corresponding MEP entry */
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepNode);

    *pi4RetValDot1agCfmMepFngResetTime = (INT4) (pFngInfo->u2FngResetTime);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepHighestPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepHighestPrDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepHighestPrDef (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 *pi4RetValDot1agCfmMepHighestPrDefect)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Mep HighestPriorityDefect from the corresponding MEP entry */

    *pi4RetValDot1agCfmMepHighestPrDefect = (INT4)
        (pMepNode->FngInfo.u1HighestDefect);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDefects
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepDefects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDefects (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           tSNMP_OCTET_STRING_TYPE * pRetValDot1agCfmMepDefects)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT1               u1Val = ECFM_INIT_VAL;
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    u1Val = (UINT1) (u1Val | (pMepNode->FngInfo.b1SomeRdiDefect));
    u1Val = (UINT1) (u1Val << ECFM_SHIFT_1BIT);
    u1Val = (UINT1) (u1Val | (pMepNode->FngInfo.b1SomeMacStatusDefect));
    u1Val = (UINT1) (u1Val << ECFM_SHIFT_1BIT);
    u1Val = (UINT1) (u1Val | (pMepNode->FngInfo.b1SomeRMepCcmDefect));
    u1Val = (UINT1) (u1Val << ECFM_SHIFT_1BIT);
    u1Val = (UINT1) (u1Val | (pMepNode->CcInfo.b1ErrorCcmDefect));
    u1Val = (UINT1) (u1Val << ECFM_SHIFT_1BIT);
    u1Val = (UINT1) (u1Val | (pMepNode->CcInfo.b1XconCcmDefect));
    u1Val = (UINT1) (u1Val << ECFM_SHIFT_3BITS);
    pRetValDot1agCfmMepDefects->pu1_OctetList[0] = u1Val;
    pRetValDot1agCfmMepDefects->i4_Length = sizeof (u1Val);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepErrCcmLastFail
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepErrorCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepErrCcmLastFail (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDot1agCfmMepErrorCcmLastFailure)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Ccm pdu, that triggered DefErrorCcm, from the corresponding 
     * MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);

    /* Check if CCM pdu, which causes error recieved */
    if (pCcInfo->ErrorCcmLastFailure.pu1Octets != NULL)
    {
        /* Set Ccm pdu's length */
        pRetValDot1agCfmMepErrorCcmLastFailure->i4_Length =
            pCcInfo->ErrorCcmLastFailure.u4OctLen;

        ECFM_MEMCPY (pRetValDot1agCfmMepErrorCcmLastFailure->pu1_OctetList,
                     pCcInfo->ErrorCcmLastFailure.pu1Octets,
                     pCcInfo->ErrorCcmLastFailure.u4OctLen);
    }
    else
    {
        pRetValDot1agCfmMepErrorCcmLastFailure->i4_Length = 1;
        pRetValDot1agCfmMepErrorCcmLastFailure->pu1_OctetList[0] = '\0';
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepXconCcmLastFail
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepXconCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepXconCcmLastFail (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValDot1agCfmMepXconCcmLastFailure)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Ccm pdu, that triggered DefXconnCcm, from the corresponding 
     * MEP entry */

    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    /* Check if Xconn failure related CCM pdu recieved */
    if (pCcInfo->XconCcmLastFailure.pu1Octets != NULL)

    {

        /*Set Ccm pdu length */
        pRetValDot1agCfmMepXconCcmLastFailure->i4_Length =
            pCcInfo->XconCcmLastFailure.u4OctLen;
        ECFM_MEMCPY (pRetValDot1agCfmMepXconCcmLastFailure->pu1_OctetList,
                     pCcInfo->XconCcmLastFailure.pu1Octets,
                     pCcInfo->XconCcmLastFailure.u4OctLen);
    }
    else
    {
        pRetValDot1agCfmMepXconCcmLastFailure->i4_Length = 1;
        pRetValDot1agCfmMepXconCcmLastFailure->pu1_OctetList[0] = '\0';
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepCcmSeqErrors
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCcmSequenceErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepCcmSeqErrors (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 *pu4RetValDot1agCfmMepCcmSequenceErrors)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Ccm Sequence errors from the corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    if (pMepNode->pPortInfo != NULL)
    {
        EcfmCcmOffLoadUpdateTxRxStats (pMepNode->pPortInfo->u2PortNum);
        *pu4RetValDot1agCfmMepCcmSequenceErrors = pCcInfo->u4CcmSeqErrors;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepCciSentCcms
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepCciSentCcms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepCciSentCcms (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 *pu4RetValDot1agCfmMepCciSentCcms)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Ccm Sequence errors from the corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    if (pMepNode->pPortInfo != NULL)

    {
        EcfmCcmOffLoadUpdateTxRxStats (pMepNode->pPortInfo->u2PortNum);
        *pu4RetValDot1agCfmMepCciSentCcms = pCcInfo->u4CciCcmCount;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepNextLbmTransId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepNextLbmTransId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepNextLbmTransId (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4RetValDot1agCfmMepNextLbmTransId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Next Lbm transanctionId from the corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepNextLbmTransId = pLbInfo->u4NextLbmSeqNo;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepLbrIn
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepLbrIn (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 *pu4RetValDot1agCfmMepLbrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of valid in order Lbrs from the corresponding MEP 
       entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepLbrIn = pLbInfo->u4LbrIn;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepLbrInOutOfOrder
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrInOutOfOrder
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepLbrInOutOfOrder (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   UINT4 *pu4RetValDot1agCfmMepLbrInOutOfOrder)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of valid out-of- order LBRs from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepLbrInOutOfOrder = pLbInfo->u4LbrInOutOfOrder;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepLbrBadMsdu
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrBadMsdu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepLbrBadMsdu (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 *pu4RetValDot1agCfmMepLbrBadMsdu)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the total no. of valid out-of- order Lbrs from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepLbrBadMsdu = pLbInfo->u4LbrBadMsdu;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepLtmNextSeqNo
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLtmNextSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepLtmNextSeqNo (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 *pu4RetValDot1agCfmMepLtmNextSeqNumber)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Ltm next sequence no. from the
     * corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepLtmNextSeqNumber = pLtInfo->u4LtmNextSeqNum;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepUnexpLtrIn
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepUnexpLtrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepUnexpLtrIn (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 *pu4RetValDot1agCfmMepUnexpLtrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the total no. of unexpected LTRs received from the
     * corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepUnexpLtrIn = pLtInfo->u4UnexpLtrIn;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepLbrOut
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepLbrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepLbrOut (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          UINT4 *pu4RetValDot1agCfmMepLbrOut)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of Lbrs transmitted, from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepLbrOut = pLbInfo->u4LbrOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmStatus (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 *pi4RetValDot1agCfmMepTransmitLbmStatus)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the transmit Lbm Status, from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    *pi4RetValDot1agCfmMepTransmitLbmStatus = (INT4) (pLbInfo->u1TxLbmStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmDstMacAddr
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmDstMacAddr (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   tMacAddr *
                                   pRetValDot1agCfmMepTransmitLbmDestMacAddress)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Transmit Lbm Destination Mac Address , from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    ECFM_MEMCPY (pRetValDot1agCfmMepTransmitLbmDestMacAddress,
                 pLbInfo->TxLbmDestMacAddr, ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmDestMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmDestMepId (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4
                                  *pu4RetValDot1agCfmMepTransmitLbmDestMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Destination MepId, from the corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepTransmitLbmDestMepId =
        (UINT4) (pLbInfo->u2TxDestMepId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmDstIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmDstIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4
                                   *pi4RetValDot1agCfmMepTransmitLbmDestIsMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the transmit Lbm Status, from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MEPID)
    {
        *pi4RetValDot1agCfmMepTransmitLbmDestIsMepId = ECFM_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLbmDestIsMepId = ECFM_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmMessages
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmMessages (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 INT4 *pi4RetValDot1agCfmMepTransmitLbmMessages)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT2               u2TxLbmMsgsBckUp = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the No. of messages to send, from the corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    u2TxLbmMsgsBckUp = pLbInfo->u2TxLbmMessages;
    ECFM_CLEAR_INFINITE_TX_STATUS (pLbInfo->u2TxLbmMessages);

    if ((pMepNode->u2PortNum) == ECFM_INIT_VAL)
    {
        *pi4RetValDot1agCfmMepTransmitLbmMessages = ECFM_LB_MESG_DEF_VAL;
        return SNMP_SUCCESS;
    }

    if ((ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum)) &&
        (Y1731_LB_MESG_DEF_VAL == pLbInfo->u2TxLbmMessages))

    {
        *pi4RetValDot1agCfmMepTransmitLbmMessages = ECFM_LB_MESG_DEF_VAL;
    }

    else

    {
        *pi4RetValDot1agCfmMepTransmitLbmMessages = pLbInfo->u2TxLbmMessages;
    }
    pLbInfo->u2TxLbmMessages = u2TxLbmMsgsBckUp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmDataTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmDataTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmDataTlv (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1agCfmMepTransmitLbmDataTlv)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Transmit Lbm Data Tlv and its length , from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    if ((pLbInfo->TxLbmDataTlv.u4OctLen != ECFM_INIT_VAL) &&
        (pLbInfo->TxLbmDataTlv.pu1Octets != NULL))
    {
        ECFM_MEMCPY (pRetValDot1agCfmMepTransmitLbmDataTlv->pu1_OctetList,
                     pLbInfo->TxLbmDataTlv.pu1Octets,
                     pLbInfo->TxLbmDataTlv.u4OctLen);
        pRetValDot1agCfmMepTransmitLbmDataTlv->i4_Length =
            (INT4) (pLbInfo->TxLbmDataTlv.u4OctLen);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmVlanPri
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmVlanPri (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4
                                *pi4RetValDot1agCfmMepTransmitLbmVlanPriority)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Vlan Priority from the corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pi4RetValDot1agCfmMepTransmitLbmVlanPriority = (INT4)
        (pLbInfo->u1TxLbmVlanPriority);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmVlanDropEna
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmVlanDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmVlanDropEna (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    INT4
                                    *pi4RetValDot1agCfmMepTransmitLbmVlanDropEnable)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Vlan DropEnable from the corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    if (pLbInfo->b1TxLbmDropEligible == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepTransmitLbmVlanDropEnable = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLbmVlanDropEnable = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmResultOK
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmResultOK (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 INT4 *pi4RetValDot1agCfmMepTransmitLbmResultOK)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the ResultOk from the corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    if (pLbInfo->b1TxLbmResultOk == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepTransmitLbmResultOK = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLbmResultOK = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLbmSeqNumber
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLbmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLbmSeqNumber (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4
                                  *pu4RetValDot1agCfmMepTransmitLbmSeqNumber)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Transmit Lbm sequence no. from the corresponding Mep node */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepTransmitLbmSeqNumber =
        (UINT4) (pLbInfo->u4TxLbmSeqNumber);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmStatus (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 *pi4RetValDot1agCfmMepTransmitLtmStatus)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Transmit Ltm status from the corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    *pi4RetValDot1agCfmMepTransmitLtmStatus = (INT4) (pLtInfo->u1TxLtmStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmFlags
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmFlags (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDot1agCfmMepTransmitLtmFlags)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Transmit Ltm flags from the corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);
    ECFM_LBLT_MASK_LTM_FLAGS_LSB (pLtInfo->u1TxLtmFlags);

    pRetValDot1agCfmMepTransmitLtmFlags->pu1_OctetList[0] =
        pLtInfo->u1TxLtmFlags;
    pRetValDot1agCfmMepTransmitLtmFlags->i4_Length = 1;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmTgtMacAddr
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTargetMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmTgtMacAddr (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   tMacAddr *
                                   pRetValDot1agCfmMepTransmitLtmTargetMacAddress)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Target Ltm mac address from corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    ECFM_MEMCPY (pRetValDot1agCfmMepTransmitLtmTargetMacAddress,
                 pLtInfo->TxLtmTargetMacAddr, ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmTgtMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTargetMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmTgtMepId (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4
                                 *pu4RetValDot1agCfmMepTransmitLtmTargetMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Target MepId  from corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepTransmitLtmTargetMepId = (UINT4)
        (pLtInfo->u2TxLtmTargetMepId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmTgtIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTargetIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmTgtIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4
                                   *pi4RetValDot1agCfmMepTransmitLtmTargetIsMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the TargetIsMepId status from the corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    if (pLtInfo->b1TxLtmTargetIsMepId == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepTransmitLtmTargetIsMepId = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLtmTargetIsMepId = ECFM_SNMP_TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmTtl (UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            UINT4 *pu4RetValDot1agCfmMepTransmitLtmTtl)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Transmit Ttl  from corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    ECFM_LBLT_MASK_LTM_TTL_MSB (pLtInfo->u2TxLtmTtl);
    *pu4RetValDot1agCfmMepTransmitLtmTtl = (UINT4) (pLtInfo->u2TxLtmTtl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmResult
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmResult (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 *pi4RetValDot1agCfmMepTransmitLtmResult)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the TargetIsMepId status from the corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    if (pLtInfo->b1TxLtmResult == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepTransmitLtmResult = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepTransmitLtmResult = ECFM_SNMP_TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmSeqNumber
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmSeqNumber (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4
                                  *pu4RetValDot1agCfmMepTransmitLtmSeqNumber)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Transmit Ltm Sequence no.  from corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    *pu4RetValDot1agCfmMepTransmitLtmSeqNumber = pLtInfo->u4TxLtmSeqNum;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepTxLtmEgrId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepTransmitLtmEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepTxLtmEgrId (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDot1agCfmMepTransmitLtmEgressIdentifier)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Transmit Ltm egress Id  from corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    ECFM_MEMCPY (pRetValDot1agCfmMepTransmitLtmEgressIdentifier->pu1_OctetList,
                 pLtInfo->au1TxLtmEgressId, ECFM_EGRESS_ID_LENGTH);
    pRetValDot1agCfmMepTransmitLtmEgressIdentifier->i4_Length =
        ECFM_EGRESS_ID_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                retValDot1agCfmMepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepRowStatus (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 *pi4RetValDot1agCfmMepRowStatus)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the row status from corresponding Mep entry */
    *pi4RetValDot1agCfmMepRowStatus = (INT4) (pMepNode->u1RowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepIfIndex
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepIfIndex (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           INT4 i4SetValDot1agCfmMepIfIndex)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MA Entry from the Given Indices */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry Exists\n");
        return SNMP_FAILURE;

    }
    /* If the MA service type is either
     * LSP /PW then set the port number to the default value
     */
    if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
        (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: IfIndex is not used for MPLS-TP MEPs\n");
        return SNMP_SUCCESS;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the IfIndex to corresponding MEP entry */
    pMepNode->u2PortNum = (UINT2) i4SetValDot1agCfmMepIfIndex;

    /* Incase of Y.1731 CCI enabled status should be as that of 
     * MA*/
    if ((pMepNode->u2PortNum) != ECFM_INIT_VAL)
    {

        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))

        {
            pMepNode->CcInfo.b1CciEnabled = pMaNode->b1CciEnabled;

            /* configure Y.1731 specific default values */
            EcfmLbLtConfY1731Defaults (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                       u4Dot1agCfmMepIdentifier,
                                       ECFM_CC_CURR_CONTEXT_ID ());
        }
    }

    /* Sending Trigger to MSR */
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);
    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepIfIndex, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        /* Get IfIndex from Local Port for the Current Context */
        ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_CC_CURR_CONTEXT_ID (),
                                          (UINT2) i4SetValDot1agCfmMepIfIndex,
                                          &u4IfIndex);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          (INT4) u4IfIndex));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepDirection
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepDirection (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 i4SetValDot1agCfmMepDirection)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcMaInfo      *pMaNode = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MA Entry from the Given Indices */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry Exists\n");
        return SNMP_FAILURE;

    }
    /* If the MA service type is either
     * LSP /PW then set the direction by default to DOWN */

    if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
        (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
    {
        i4SetValDot1agCfmMepDirection = ECFM_MP_DIR_DOWN;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the MEP Direction to corresponding MEP entry */
    pMepNode->u1Direction = (UINT1) i4SetValDot1agCfmMepDirection;

    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);

    /* Sending Trigger to MSR */
    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepDirection, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepDirection));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepPrimaryVid
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepPrimaryVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepPrimaryVid (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 u4SetValDot1agCfmMepPrimaryVid)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);
    /* If the Primary Vlan Id is "0" then set the MA's Vlan Id. */
    if (u4SetValDot1agCfmMepPrimaryVid == ECFM_INIT_VAL)
    {
        pMepNode->u4PrimaryVidIsid = u4RetPrimaryVid;
    }
    else
    {
        /* Set the PrimaryVid to corresponding MEP entry */
        pMepNode->u4PrimaryVidIsid = u4SetValDot1agCfmMepPrimaryVid;
    }
    /* Sending Trigger to MSR */
    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepPrimaryVid, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          u4SetValDot1agCfmMepPrimaryVid));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepActive
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepActive (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          INT4 i4SetValDot1agCfmMepActive)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the MepActive to corresponding MEP entry */
    if (((i4SetValDot1agCfmMepActive == ECFM_SNMP_FALSE) &&
         (pMepNode->b1Active == ECFM_FALSE)) ||
        ((i4SetValDot1agCfmMepActive == ECFM_SNMP_TRUE) &&
         (pMepNode->b1Active == ECFM_TRUE)))
    {
        /* MepActive is already set */
        return SNMP_SUCCESS;
    }
    if (i4SetValDot1agCfmMepActive == ECFM_SNMP_TRUE)
    {
        pMepNode->b1Active = ECFM_TRUE;
    }
    else
    {
        pMepNode->b1Active = ECFM_FALSE;
    }
    /* Update MepActive in LbLt task's MEP entry */
    EcfmLbLtConfMepActive (pMepNode->u4MdIndex, pMepNode->u4MaIndex,
                           pMepNode->u2MepId, pMepNode->b1Active,
                           ECFM_CC_CURR_CONTEXT_ID ());

    /* Sending Trigger to MSR */
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);

    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepActive, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepActive));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepCciEnabled
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepCciEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepCciEnabled (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 i4SetValDot1agCfmMepCciEnabled)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the CciEnabled to corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);

    if (i4SetValDot1agCfmMepCciEnabled == ECFM_SNMP_FALSE)
    {
        pCcInfo->b1CciEnabled = ECFM_FALSE;
    }
    else
    {
        pCcInfo->b1CciEnabled = ECFM_TRUE;
    }
    /* Sending Trigger to MSR */
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);

    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepCciEnabled, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepCciEnabled));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepCcmLtmPriority
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepCcmLtmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepCcmLtmPriority (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 u4SetValDot1agCfmMepCcmLtmPriority)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check if CcmLtmPriority is already same */
    if (pMepNode->u1CcmLtmPriority ==
        (UINT1) u4SetValDot1agCfmMepCcmLtmPriority)
    {
        return SNMP_SUCCESS;
    }
    /* Set CcmLtmPriority to corresponding MEP entry */
    pMepNode->u1CcmLtmPriority = (UINT1) u4SetValDot1agCfmMepCcmLtmPriority;

    /* Update CcmLtmPriority in LbLt task's MEP entry */
    EcfmLbLtConfMepCcmLtmPriority (pMepNode->u4MdIndex, pMepNode->u4MaIndex,
                                   pMepNode->u2MepId,
                                   pMepNode->u1CcmLtmPriority,
                                   ECFM_CC_CURR_CONTEXT_ID ());
    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepCcmLtmPriority,
                              u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          u4SetValDot1agCfmMepCcmLtmPriority));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepLowPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepLowPrDef
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepLowPrDef (UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            INT4 i4SetValDot1agCfmMepLowPrDef)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Lowest Defect priority to corresponding MEP entry */

    pMepNode->u1LowestAlarmPri = (UINT1) i4SetValDot1agCfmMepLowPrDef;

    /* Sending Trigger to MSR */
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);

    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepLowPrDef, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepLowPrDef));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepFngAlarmTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepFngAlarmTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepFngAlarmTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4SetValDot1agCfmMepFngAlarmTime)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Fng Alarm time to corresponding MEP entry */
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepNode);

    pFngInfo->u2FngAlarmTime = (UINT2) i4SetValDot1agCfmMepFngAlarmTime;

    /* Sending Trigger to MSR */
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);

    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepFngAlarmTime, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepFngAlarmTime));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepFngResetTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepFngResetTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepFngResetTime (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4SetValDot1agCfmMepFngResetTime)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Fng Reset time to corresponding MEP entry */
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepNode);

    pFngInfo->u2FngResetTime = (UINT2) i4SetValDot1agCfmMepFngResetTime;

    /* Sending Trigger to MSR */

    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);

    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepFngResetTime, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepFngResetTime));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmStatus (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 i4SetValDot1agCfmMepTransmitLbmStatus)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLbmStatus,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Trasmit LBM status to corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    /* Check if LBM transmission is to be initiated, 
     * and send corresponding event to LBI state machine */
    if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_LB_START_TRANSACTION)
        != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP:Post Event for Transaction Initiation Failed\n");
        pLbInfo->b1TxLbmResultOk = ECFM_FALSE;

        if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
        {
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                              ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                              u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                              i4SetValDot1agCfmMepTransmitLbmStatus));
        }
        if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        return SNMP_FAILURE;
    }
    pLbInfo->u1TxLbmStatus = (UINT1) i4SetValDot1agCfmMepTransmitLbmStatus;
    pLbInfo->b1TxLbmResultOk = ECFM_TRUE;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLbmStatus));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmDstMacAddr
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmDstMacAddr (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   tMacAddr
                                   SetValDot1agCfmMepTransmitLbmDestMacAddress)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Destination MacAddress to corresponding Mep node */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    ECFM_MEMCPY (pLbInfo->TxLbmDestMacAddr,
                 SetValDot1agCfmMepTransmitLbmDestMacAddress,
                 ECFM_MAC_ADDR_LENGTH);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIEcfmMepTransmitLbmDestMacAddress, u4SeqNum,
                              FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %m",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          SetValDot1agCfmMepTransmitLbmDestMacAddress));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmDestMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmDestMepId (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4
                                  u4SetValDot1agCfmMepTransmitLbmDestMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Destination MepId to corresponding Mep node */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    pLbInfo->u2TxDestMepId = (UINT2) u4SetValDot1agCfmMepTransmitLbmDestMepId;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLbmDestMepId,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          u4SetValDot1agCfmMepTransmitLbmDestMepId));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmDstIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmDstIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4
                                   i4SetValDot1agCfmMepTransmitLbmDestIsMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    /*Set the "Destination is MepId", to corresponding Mep node */
    if (i4SetValDot1agCfmMepTransmitLbmDestIsMepId == ECFM_SNMP_FALSE)
    {
        pLbInfo->u1TxLbmDestType = ECFM_TX_DEST_TYPE_UNICAST;
    }
    else
    {
        pLbInfo->u1TxLbmDestType = ECFM_TX_DEST_TYPE_MEPID;
    }

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLbmDestIsMepId,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLbmDestIsMepId));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmMessages
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmMessages (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 INT4 i4SetValDot1agCfmMepTransmitLbmMessages)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);

    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    /* Set the No.of LBMs to send to corresponding MEP entry */
    pLbInfo->u2TxLbmMessages = (UINT2) i4SetValDot1agCfmMepTransmitLbmMessages;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLbmMessages,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLbmMessages));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmDataTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmDataTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmDataTlv (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValDot1agCfmMepTransmitLbmDataTlv)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);

    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* Check if memory for DataTlv is already allocated */
    if (pMepNode->LbInfo.TxLbmDataTlv.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_DATA_TLV_POOL,
                             pMepNode->LbInfo.TxLbmDataTlv.pu1Octets);
        pMepNode->LbInfo.TxLbmDataTlv.pu1Octets = NULL;
        pMepNode->LbInfo.TxLbmDataTlv.u4OctLen = ECFM_INIT_VAL;
    }

    /* Allocate memory for new Data Tlv as its length is arbitrary */
    if ((pSetValDot1agCfmMepTransmitLbmDataTlv->pu1_OctetList != NULL) &&
        (pSetValDot1agCfmMepTransmitLbmDataTlv->i4_Length != ECFM_INIT_VAL))

    {
        ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_LBM_DATA_TLV
            (pMepNode->LbInfo.TxLbmDataTlv.pu1Octets);
        if (pMepNode->LbInfo.TxLbmDataTlv.pu1Octets == NULL)

        {
            ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                           "\tSNMP: Allocation to data TLV Failed\n"
                           "MAX LB Sessions crossed, tune the macro\r\n"
                           "ECFM_MAX_LB_SESSIONS to expected value\r\n");
            return SNMP_FAILURE;
        }
        ECFM_MEMSET (pMepNode->LbInfo.TxLbmDataTlv.pu1Octets, ECFM_INIT_VAL,
                     pSetValDot1agCfmMepTransmitLbmDataTlv->i4_Length);

        /*Set the Data Tlv, to corresponding Mep node */
        ECFM_MEMCPY (pMepNode->LbInfo.TxLbmDataTlv.pu1Octets,
                     pSetValDot1agCfmMepTransmitLbmDataTlv->pu1_OctetList,
                     pSetValDot1agCfmMepTransmitLbmDataTlv->i4_Length);

        /* Update the length of the dta tlv */
        pMepNode->LbInfo.TxLbmDataTlv.u4OctLen = (UINT4)
            (pSetValDot1agCfmMepTransmitLbmDataTlv->i4_Length);
    }

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLbmDataTlv,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %s",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          pSetValDot1agCfmMepTransmitLbmDataTlv));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmVlanPri
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmVlanPri (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4
                                i4SetValDot1agCfmMepTransmitLbmVlanPriority)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the  Vlan Priority to corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    pLbInfo->u1TxLbmVlanPriority =
        (UINT1) i4SetValDot1agCfmMepTransmitLbmVlanPriority;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIEcfmMepTransmitLbmVlanPriority, u4SeqNum,
                              FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLbmVlanPriority));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLbmVlanDropEna
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLbmVlanDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLbmVlanDropEna (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    INT4
                                    i4SetValDot1agCfmMepTransmitLbmVlanDropEnable)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set Drop Enable, to corresponding Mep entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);

    if (i4SetValDot1agCfmMepTransmitLbmVlanDropEnable == ECFM_SNMP_FALSE)
    {
        pLbInfo->b1TxLbmDropEligible = ECFM_FALSE;
    }
    else
    {
        pLbInfo->b1TxLbmDropEligible = ECFM_TRUE;
    }
    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIEcfmMepTransmitLbmVlanDropEnable, u4SeqNum,
                              FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLbmVlanDropEnable));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmStatus (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 i4SetValDot1agCfmMepTransmitLtmStatus)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLtmStatus,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Trasmit LBM status to corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    /* Check if LTM transmission is to be initiated, 
     * and send corresponding event to LTI state machine */
    if (EcfmSnmpLwInitiateLtmTx (pMepNode->u4MdIndex,
                                 pMepNode->u4MaIndex, pMepNode->u2MepId) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: LTM initiation Failed\n");
        pLtInfo->b1TxLtmResult = ECFM_FALSE;
        /* Reverting back the initiation parametrs */
        ECFM_LBLT_MASK_LTM_TTL_MSB (pLtInfo->u2TxLtmTtl);
        ECFM_LBLT_MASK_LTM_FLAGS_LSB (pLtInfo->u1TxLtmFlags);
        ECFM_MEMSET (pLtInfo->TxLtmTargetMacAddr, ECFM_INIT_VAL,
                     ECFM_MAC_ADDR_LENGTH);
        if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
        {
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                              ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                              u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                              i4SetValDot1agCfmMepTransmitLtmStatus));
        }
        if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        return SNMP_FAILURE;
    }
    pLtInfo->u1TxLtmStatus = (UINT1) i4SetValDot1agCfmMepTransmitLtmStatus;
    pLtInfo->b1TxLtmResult = ECFM_TRUE;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLtmStatus));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmFlags
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmFlags (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValDot1agCfmMepTransmitLtmFlags)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set Ltm Flags, to corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    pLtInfo->u1TxLtmFlags =
        pSetValDot1agCfmMepTransmitLtmFlags->pu1_OctetList[0];

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {

        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLtmFlags,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %s",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          pSetValDot1agCfmMepTransmitLtmFlags));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmTgtMacAddr
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTargetMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmTgtMacAddr (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   tMacAddr
                                   SetValDot1agCfmMepTransmitLtmTargetMacAddress)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set Target MacAddress to corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    ECFM_MEMCPY (pLtInfo->TxLtmTargetMacAddr,
                 SetValDot1agCfmMepTransmitLtmTargetMacAddress,
                 ECFM_MAC_ADDR_LENGTH);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {

        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIEcfmMepTransmitLtmTargetMacAddress, u4SeqNum,
                              FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %m",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          SetValDot1agCfmMepTransmitLtmTargetMacAddress));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmTgtMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTargetMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmTgtMepId (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4
                                 u4SetValDot1agCfmMepTransmitLtmTargetMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    /* Set Target MepId to corresponding MEP entry */
    pLtInfo->u2TxLtmTargetMepId =
        (UINT2) u4SetValDot1agCfmMepTransmitLtmTargetMepId;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {

        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLtmTargetMepId,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          u4SetValDot1agCfmMepTransmitLtmTargetMepId));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmTgtIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTargetIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmTgtIsMepId (UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4
                                   i4SetValDot1agCfmMepTransmitLtmTargetIsMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    /* Set TargetIsMepId, to corresponding MEP entry */
    if (i4SetValDot1agCfmMepTransmitLtmTargetIsMepId == ECFM_SNMP_FALSE)
    {
        pLtInfo->b1TxLtmTargetIsMepId = ECFM_FALSE;
    }
    else
    {
        pLtInfo->b1TxLtmTargetIsMepId = ECFM_TRUE;
    }

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {

        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIEcfmMepTransmitLtmTargetIsMepId, u4SeqNum,
                              FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepTransmitLtmTargetIsMepId));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmTtl (UINT4 u4Dot1agCfmMdIndex,
                            UINT4 u4Dot1agCfmMaIndex,
                            UINT4 u4Dot1agCfmMepIdentifier,
                            UINT4 u4SetValDot1agCfmMepTransmitLtmTtl)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set TTL to corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);
    pLtInfo->u2TxLtmTtl = (UINT2) u4SetValDot1agCfmMepTransmitLtmTtl;

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {

        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepTransmitLtmTtl,
                              u4SeqNum, FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          u4SetValDot1agCfmMepTransmitLtmTtl));
    }
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepTxLtmEgrId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepTransmitLtmEgressIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepTxLtmEgrId (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValDot1agCfmMepTransmitLtmEgressIdentifier)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set EgressId to corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    ECFM_MEMCPY (pLtInfo->au1TxLtmEgressId,
                 pSetValDot1agCfmMepTransmitLtmEgressIdentifier->pu1_OctetList,
                 pSetValDot1agCfmMepTransmitLtmEgressIdentifier->i4_Length);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMepNode->u4PrimaryVidIsid)))
    {

        u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIEcfmMepTransmitLtmEgressIdentifier, u4SeqNum,
                              FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %s",
                          ECFM_LBLT_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          pSetValDot1agCfmMepTransmitLtmEgressIdentifier));
        if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlSetAgMepRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                setValDot1agCfmMepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlSetAgMepRowStatus (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 i4SetValDot1agCfmMepRowStatus)
{
    tEcfmCcStackInfo    StackInfo;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
    tEcfmHwLmParams     LmHwInfo;
#endif
    UINT2               u2PortNum = ECFM_INIT_VAL;
    UINT2               u2VlanId = ECFM_INIT_VAL;
    UINT1               u1Direction = ECFM_INIT_VAL;
    BOOL1               b1MepConfigured = ECFM_FALSE;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMepInfo     *pMepNewNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmCcLckInfo     *pLckInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcRMepDbInfo  *pTempRMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;

    ECFM_MEMSET (&StackInfo, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
#ifdef   NPAPI_WANTED
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL, ECFM_HW_MEP_HANDLER_SIZE);
    MEMSET (&LmHwInfo, 0, sizeof (tEcfmHwLmParams));
#endif

    /* Get MD Entry for the given Indices */
    pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        if (i4SetValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            /* As per RFC1903, Deleting a non-existing row status should return success */
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    /* Get MA Entry for the given Indices */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        if (i4SetValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            /* As per RFC1903, Deleting a non-existing row status should return success */
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier);
    if (pMepNode != NULL)
    {
        /* MEP entry exists and its row status is same as user wants to set */
        if (pMepNode->u1RowStatus == (UINT1) i4SetValDot1agCfmMepRowStatus)

        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        /* As per RFC1903, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }
    else if (i4SetValDot1agCfmMepRowStatus != ECFM_ROW_STATUS_CREATE_AND_WAIT)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }

    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4Dot1agCfmMdIndex,
                                                  u4Dot1agCfmMaIndex,
                                                  &u4RetPrimaryVid);
    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepRowStatus, u4SeqNum,
                              TRUE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    }
    switch (i4SetValDot1agCfmMepRowStatus)

    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
#ifdef L2RED_WANTED
            if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE &&
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd == ECFM_FALSE)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                               ECFM_RED_MEP_ROW_STS_CMD,
                                               ECFM_INIT_VAL,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               ECFM_INIT_VAL, ECFM_TRUE);

                if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
                {
                    gEcfmRedGlobalInfo.HwAuditInfo.bError = ECFM_FALSE;
                }
            }
#endif

            pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;

            /* Update MEP row status in LBLT task's MEP node also */
            EcfmLbLtConfMepRowStatus (pMepNode->u4MdIndex,
                                      pMepNode->u4MaIndex,
                                      pMepNode->u2MepId,
                                      pMepNode->u1RowStatus,
                                      ECFM_CC_CURR_CONTEXT_ID ());

            /* Indicate MEP related State machines about row status */
            if (pMepNode->b1Active == ECFM_TRUE)

	    {
		    EcfmCcUtilNotifySm (pMepNode, ECFM_IND_MEP_INACTIVE);
		    EcfmCcUtilNotifyY1731 (pMepNode, ECFM_IND_MEP_INACTIVE);
		    EcfmLbLtNotifySM (pMepNode->u4MdIndex, pMepNode->u4MaIndex,
				    pMepNode->u2MepId,
				    ECFM_CC_CURR_CONTEXT_ID (),
				    ECFM_IND_MEP_INACTIVE);
	    }
            break;
        case ECFM_ROW_STATUS_ACTIVE:

            if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
                  (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
            {
                /* If user wants to make MEP entry's row status ACTIVE */
                if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_NOT_IN_SERVICE)
                {
                    if (pMepNode->pPortInfo == NULL)
                    {
                        /*&Check if Mep Port Num is zero */
                        if ((pMepNode->u2PortNum) == ECFM_INIT_VAL)
                        {
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                         "\tSNMP: MEP Port Number cannot be ZERO\n");
                            return SNMP_FAILURE;
                        }
                        if (pMepNode->u4PrimaryVidIsid == 0)

                        {
                            pMepNode->u4PrimaryVidIsid =
                                pMaNode->u4PrimaryVidIsid;
                        }

                        if (EcfmSnmpLwIsInfoConfiguredForMep (pMepNode)
                            != ECFM_SUCCESS)
                        {
                            CLI_SET_ERR (CLI_ECFM_MEP_CONFIG_INFO_ERR);
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                         "\tSNMP: MEP cannot be configured with provided"
                                         " parameters\n");
                            return SNMP_FAILURE;
                        }
                    }
                }
            }

            if ((pMepNode->pMaInfo->u1SelectorType ==
                 ECFM_SERVICE_SELECTION_LSP) ||
                (pMepNode->pMaInfo->u1SelectorType ==
                 ECFM_SERVICE_SELECTION_PW))
            {
                pMepNode->u2PortNum = (UINT2) ECFM_CC_MAX_MEP_INFO;
            }

            if (pMepNode->pEcfmMplsParams == NULL)
            {
                if (pMepNode->pPortInfo == NULL)

                {
                    /*&Check if Mep Port Num is zero */
                    if ((pMepNode->u2PortNum) == ECFM_INIT_VAL)
                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "\tSNMP: MEP Port Number cannot be ZERO\n");
                        return SNMP_FAILURE;
                    }
                    if (pMepNode->u4PrimaryVidIsid == 0)

                    {
                        pMepNode->u4PrimaryVidIsid = pMaNode->u4PrimaryVidIsid;
                    }
                    /* Add MEP node in PortInfo's MepInfoTree */
                    if (EcfmSnmpLwAddMepInAPort (pMepNode) != ECFM_SUCCESS)

                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "\tSNMP: Add MEP FAILS\n");
                        if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                        {
                            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                              ECFM_CC_CURR_CONTEXT_ID (),
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4SetValDot1agCfmMepRowStatus));
                            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                        }
                        return SNMP_FAILURE;
                    }
                    /* Add Stack entry corresponding to MEP node in both tasks */
                    if (EcfmSnmpLwAddMepInStack (pMepNode) != ECFM_SUCCESS)

                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "\tSNMP: Add MEP Entry FAILS \n");

                        if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                        {
                            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                              ECFM_CC_CURR_CONTEXT_ID (),
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4SetValDot1agCfmMepRowStatus));
                            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                        }
                        return SNMP_FAILURE;
                    }
                }
                /* Remove this node from all other MEPs RMepDb, and add its
                 * remote MEP nodes */
                EcfmSnmpLwUpdateRMepDbEntries (pMepNode);

                /* Evaluate for MIP creation for a change in port having ifIndex
                 * u4IfIndex and VlanId, but only for vlanaware MEPs */
                if (pMepNode->u4PrimaryVidIsid != 0)

                {

                    /* Update default MD status */
                    if (pMepNode->u1Direction == ECFM_MP_DIR_UP)

                    {
                        EcfmSnmpLwUpdateDefaultMdStatus (pMdNode->u1Level,
                                                         pMaNode->
                                                         u4PrimaryVidIsid,
                                                         ECFM_FALSE);
                    }

                    /* Check if there is CfmLeak/ConflictingVids error configured on port 
                     * having interface Index with VlanId as of MEPs Interface Index,
                     * PrimaryVid respectively */
                    if (EcfmCcUtilChkIfCfgCfmLeakErr
                        (pMepNode->pMaInfo, pMepNode->u2PortNum,
                         pMepNode->u4PrimaryVidIsid) == ECFM_TRUE)

                    {
                        if (EcfmCcUtilAddConfigErrEntry
                            (pMepNode->u2PortNum, pMepNode->u4PrimaryVidIsid,
                             ECFM_CONFIG_ERR_CFM_LEAK) != ECFM_SUCCESS)

                        {
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                         ECFM_OS_RESOURCE_TRC,
                                         "EcfmMepUtlSetAgMepRowStatus : Could not add"
                                         "Configuration error ECFM_CONFIG_ERR_CFM_LEAK"
                                         "in Configuration Error List Table"
                                         "\r\n");
                        }
                    }

                    else

                    {

                        /* Clear CFM LEAK error if exists already */
                        EcfmCcUtilDeleteConfigErrEntry (pMepNode->u2PortNum,
                                                        pMepNode->
                                                        u4PrimaryVidIsid,
                                                        ECFM_CONFIG_ERR_CFM_LEAK);
                    }

                    /* While making MEP active, all MEPs which are already created
                     * in the higher level and same vlan id */
                    EcfmCcUtilClearCfmLeakErr (pMepNode->pMaInfo,
                                               pMepNode->u2PortNum,
                                               pMepNode->u4PrimaryVidIsid);

                    if (EcfmCcUtilChkIfCfgConfictVidErr
                        (pMepNode->pMaInfo, pMepNode->u2PortNum,
                         pMepNode->u4PrimaryVidIsid) == ECFM_TRUE)

                    {
                        if (EcfmCcUtilAddConfigErrEntry
                            (pMepNode->u2PortNum, pMepNode->u4PrimaryVidIsid,
                             ECFM_CONFIG_ERR_CONFLICT_VIDS) != ECFM_SUCCESS)

                        {
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                         ECFM_OS_RESOURCE_TRC,
                                         "Could not add"
                                         "Configuration error ECFM_CONFIG_ERR_CONFLICT_VIDS"
                                         "in Configuration Error List Table"
                                         "\r\n");
                        }
                    }

                    else

                    {

                        /* Clear CONFLICTING VIDs error if exists already */
                        EcfmCcUtilDeleteConfigErrEntry (pMepNode->u2PortNum,
                                                        pMepNode->
                                                        u4PrimaryVidIsid,
                                                        ECFM_CONFIG_ERR_CONFLICT_VIDS);
                    }
                }
            }
            else
            {                    /* Add MEP node in PortInfo's MepInfoTree */
                if (pMepNode->pPortInfo == NULL)
                {
                    if (EcfmSnmpLwAddMepInAPort (pMepNode) != ECFM_SUCCESS)

                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "\tSNMP: Add MEP FAILS\n");
                        if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                        {
                            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                              ECFM_CC_CURR_CONTEXT_ID (),
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              u4Dot1agCfmMepIdentifier,
                                              i4SetValDot1agCfmMepRowStatus));
                            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                        }
                        return SNMP_FAILURE;
                    }
                }
                EcfmSnmpLwUpdateRMepDbEntries (pMepNode);
            }
            pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

            /* Update LBLT related MEP about Row status ACTIVE */
            EcfmLbLtConfMepRowStatus (pMepNode->u4MdIndex,
                                      pMepNode->u4MaIndex,
                                      pMepNode->u2MepId,
                                      pMepNode->u1RowStatus,
                                      ECFM_CC_CURR_CONTEXT_ID ());
            if (pMepNode->pEcfmMplsParams == NULL)
            {
                if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
                {
                    if (EcfmAHCheckNValidateMepCreation (pMepNode->u2PortNum,
                                                         pMepNode->
                                                         u4PrimaryVidIsid,
                                                         pMepNode->
                                                         u1Direction) !=
                        ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                     ECFM_OS_RESOURCE_TRC,
                                     "EcfmMepUtlSetAgMepRowStatus :MEP Creation"
                                     "not allowed \r\n");
                        return SNMP_FAILURE;
                    }
                }
                /* Evaluation has to be done for all the ports */
                EcfmCcUtilEvaluateAndCreateMip (-1,
                                                pMepNode->u4PrimaryVidIsid,
                                                ECFM_TRUE);
            }
#ifdef L2RED_WANTED
            if (gEcfmRedGlobalInfo.HwAuditInfo.bMdCmd == ECFM_FALSE &&
                gEcfmRedGlobalInfo.HwAuditInfo.bMaCmd == ECFM_FALSE)
            {
                EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                               ECFM_RED_MEP_ROW_STS_CMD,
                                               ECFM_INIT_VAL,
                                               u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier,
                                               ECFM_INIT_VAL, ECFM_TRUE);

                if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
                {
                    gEcfmRedGlobalInfo.HwAuditInfo.bError = ECFM_FALSE;
                }
            }
#endif
#ifdef NPAPI_WANTED
            /* Before the state machine invokes NPAPI to transmit
             * and receive CCM, inform about MEP creation.
             */ 
            if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
                             ECFM_HW_MEP_HANDLER_SIZE) == 0)
	    {
		    /* Since the MEP handler is not valid value, invoke NPAPI */
		    MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
		    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);
		    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepNode->u4IfIndex;
		    if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_CREATE, 
					    &EcfmHwInfo) == FNP_FAILURE)
		    {
			    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
					    "EcfmMepUtlSetAgMepRowStatus :MEP Creation"
					    "indication to H/W failed. \r\n");
			    return SNMP_FAILURE;
		    }
	    }
#endif

            /* Send BEGIN event to MEP related State machines */
            if (pMepNode->b1Active == ECFM_TRUE)

	    {
		    EcfmCcUtilNotifySm (pMepNode, ECFM_IND_MEP_ACTIVE);
		    EcfmCcUtilNotifyY1731 (pMepNode, ECFM_IND_MEP_ACTIVE);
		    EcfmLbLtNotifySM (pMepNode->u4MdIndex, pMepNode->u4MaIndex,
				    pMepNode->u2MepId,
				    ECFM_CC_CURR_CONTEXT_ID (),
				    ECFM_IND_MEP_ACTIVE);
	    }

            /* Check is a VLAN unaware MEP is created, if so then we have to set
             * the Un-Aware MEP flag in port info
             */
            if (pMepNode->u4PrimaryVidIsid == 0)

            {

                /* sync the vlan unaware MEP present status at lblt also */
                EcfmLbLtConfUnawreMepPresent (ECFM_TRUE,
                                              pMepNode->pPortInfo->u2PortNum);
                pMepNode->pPortInfo->b1UnawareMepPresent = ECFM_TRUE;
            }

            else

            {

                /* sync the vlan unaware MEP present status at lblt also */
                EcfmLbLtConfUnawreMepPresent (ECFM_FALSE,
                                              pMepNode->pPortInfo->u2PortNum);
                pMepNode->pPortInfo->b1UnawareMepPresent = ECFM_FALSE;
            }
            /* Set the port as ON in demux port-list for MEP */
            ECFM_SET_DEMUX_PORT (pMepNode->pPortInfo->u2PortNum);
             ECFM_SET_DEMUX_PORT (pMepNode->pPortInfo->u2PortNum);
#ifdef NPAPI_WANTED
           LmHwInfo.u4PortId = pMepNode->pPortInfo->u4IfIndex;
           LmHwInfo.u2VlanId = pMepNode->u4PrimaryVidIsid;
           LmHwInfo.u4TxStatsHwId = pMepNode->u4LmTxStatsHwId;
           LmHwInfo.u4RxStatsHwId = pMepNode->u4LmRxStatsHwId;
            if (EcfmFsMiEcfmStartLm (&LmHwInfo) != FNP_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                        "EcfmMepUtlSetAgMepRowStatus: EcfmStartLM FAILED \r\n");
                return SNMP_FAILURE;
            }
            pMepNode->u4LmTxStatsHwId = LmHwInfo.u4TxStatsHwId;
            pMepNode->u4LmRxStatsHwId = LmHwInfo.u4RxStatsHwId;
#endif

            break;

        case ECFM_ROW_STATUS_CREATE_AND_WAIT:

            /* User wants to create a new MEP corresponding to the indices */
            /* Create a node for MEP */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MEP_TABLE (pMepNewNode) == NULL)

            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "\tSNMP: Allocate Node for MEP Entry FAILS \n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      i4SetValDot1agCfmMepRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }

            ECFM_MEMSET (pMepNewNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

            /* put MEP Identifier this node */
            pMepNewNode->u2MepId = (UINT2) u4Dot1agCfmMepIdentifier;

            /* Put MdIndex */
            pMepNewNode->u4MdIndex = u4Dot1agCfmMdIndex;

            /* Put MaIndex */
            pMepNewNode->u4MaIndex = u4Dot1agCfmMaIndex;

            /* Put backward pointer to its associated MA */
            pMaNode = EcfmSnmpLwGetMaEntry
                (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
            if (pMaNode == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC, "\tSNMP: pMaNode = NULL \n");

                ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL,
                                     (UINT1 *) (pMepNewNode));

                if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      i4SetValDot1agCfmMepRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }
            pMepNewNode->pMaInfo = pMaNode;

            /* Put MdLevel from its associated MD */
            pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);
            if (pMdNode == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC, "\tSNMP: pMdNode = NULL \n");

                ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL,
                                     (UINT1 *) (pMepNewNode));

                if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      i4SetValDot1agCfmMepRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }
            pMepNewNode->u1MdLevel = pMdNode->u1Level;

            if (ECFM_IS_SELECTOR_TYPE_MPLS_TP
                (pMepNewNode->pMaInfo->u1SelectorType) == ECFM_TRUE)
            {
                /* For MPLS LSP/PW services, currently only DOWN direction
                 * is supported.
                 */
                pMepNewNode->u1Direction = ECFM_MP_DIR_DOWN;

                if (ECFM_ALLOC_MEM_BLOCK_CC_MEP_MPTP_PARAMS_BLK
                    (pMepNewNode->pEcfmMplsParams) == NULL)
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                 ECFM_OS_RESOURCE_TRC,
                                 "\tSNMP: Allocate Node for MEP MPLS-TP Params FAILS \n");
                    ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();

                    ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL,
                                         (UINT1 *) (pMepNewNode));

                    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                    {
                        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                          ECFM_CC_CURR_CONTEXT_ID (),
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          i4SetValDot1agCfmMepRowStatus));
                        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                    }
                    return SNMP_FAILURE;
                }
            }

            /* Create RBTree RMepDB */
            TMO_DLL_Init (&(pMepNewNode->RMepDb));
            pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNewNode);
            pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepNewNode);
            pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepNewNode);
            pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepNewNode);
            pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepNewNode);

            /* Put other default values */
            pMepNewNode->u1CcmLtmPriority = ECFM_DEF_VLAN_PRIORITY;
            pMepNewNode->u1LowestAlarmPri = ECFM_DEF_MAC_REM_ERR_XCON;
            pMepNewNode->i4LoopbackStatus = ECFM_SNMP_FALSE;
            pCcInfo->u4CciSentCcms = ECFM_CCI_SENT_DEF_VAL;
            pCcInfo->u4CciCcmCount = ECFM_CCI_SENT_DEF_VAL;
            pCcInfo->u1CcmPriority = pMdNode->u1CfmVlanPriority;
            pCcInfo->b1CcmDropEligible = ECFM_FALSE;
            pCcInfo->u1RdiCapability = ECFM_ENABLE;
            pFngInfo->u2FngAlarmTime = ECFM_FNG_ALARM_TIME_MIN;
            pFngInfo->u2FngResetTime = ECFM_FNG_RESET_TIME_MAX;
            pAisInfo->u1AisPriority = pMdNode->u1CfmVlanPriority;
            pAisInfo->u1AisInterval = ECFM_CC_AIS_LCK_INTERVAL_1_SEC;
            pAisInfo->u4AisPeriod = ECFM_INIT_VAL;
            pAisInfo->u1AisCapability = ECFM_DISABLE;
            pAisInfo->b1AisDropEnable = ECFM_FALSE;
            pAisInfo->b1AisCondition = ECFM_FALSE;
            pAisInfo->b1AisDestIsMulticast = ECFM_TRUE;
            pAisInfo->b1AisTsmting = ECFM_FALSE;
            pLckInfo->u1LckPriority = pMdNode->u1CfmVlanPriority;
            pLckInfo->u1LckInterval = ECFM_CC_AIS_LCK_INTERVAL_1_SEC;
            pLckInfo->u4LckPeriod = ECFM_INIT_VAL;
            pLckInfo->u1LckDelay = ECFM_CC_LCK_DELAY_MIN;
            pLckInfo->b1OutOfService = ECFM_FALSE;
            pLckInfo->b1LckDropEnable = ECFM_FALSE;
            pLckInfo->b1LckCondition = ECFM_FALSE;
            pLckInfo->b1LckDelayExp = ECFM_FALSE;
            pLckInfo->b1LckDestIsMulticast = ECFM_TRUE;

            /* Set DEFAULT state of various state machines */
            pCcInfo->u1CciState = ECFM_CCI_STATE_DEFAULT;
            pCcInfo->u1MepXconState = ECFM_MEP_XCON_STATE_DEFAULT;
            pCcInfo->u1RMepErrState = ECFM_RMEP_ERR_STATE_DEFAULT;
            pFngInfo->u1FngState = ECFM_MEP_FNG_STATE_RESET;

            /* Set the Default values for Loss Measurement */
            pLmInfo->u2NoOfLmrIn = ECFM_INIT_VAL;
            pLmInfo->u1TxLmmStatus = ECFM_TX_STATUS_READY;
            pLmInfo->u1TxLmmPriority = pMdNode->u1CfmVlanPriority;
            pLmInfo->u1LossMeasurementType = ECFM_CC_LM_TYPE_1LM;
            pLmInfo->b1LmmDropEnable = ECFM_FALSE;
            pLmInfo->u2TxLmmInterval = ECFM_CC_LMM_INTERVAL_100_Ms;
            pLmInfo->u4NearEndFrmLossThreshold = ECFM_INIT_VAL;
            pLmInfo->u4FarEndFrmLossThreshold = ECFM_INIT_VAL;
            pLmInfo->b1TxLmByAvlbility = ECFM_FALSE;

            /* Set MEP row status */
            pMepNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;

            /* Now Add MEP node in Global info's MepTableIndex
             * and in its associated MA's DLL */
            if (EcfmSnmpLwAddMepEntry (pMepNewNode) != ECFM_SUCCESS)

            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Add MEP Entry FAILS \n");
                /* Destroy RMepDb */

                pRMepNode =
                    (tEcfmCcRMepDbInfo *)
                    TMO_DLL_First (&(pMepNewNode->RMepDb));
                while (pRMepNode != NULL)
                {
                    pTempRMepNode = pRMepNode;
                    pRMepNode = (tEcfmCcRMepDbInfo *)
                        TMO_DLL_Next (&(pMepNewNode->RMepDb),
                                      &(pTempRMepNode->MepDbDllNode));

                    EcfmDeleteRMepEntry (pMepNewNode, pTempRMepNode);
                    pTempRMepNode = NULL;
                }

                ECFM_MEMSET (&(pMepNewNode->RMepDb), ECFM_INIT_VAL,
                             sizeof (pMepNewNode->RMepDb));

                /* Deallocate the memory allocated for MPLS-TP Params block */
                if (pMepNewNode->pEcfmMplsParams != NULL)
                {
                    ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_MPTP_PARAMS_POOL,
                                         (UINT1 *) (pMepNewNode->
                                                    pEcfmMplsParams));
                }

                /* Deallocate the memory allocated to this node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL,
                                     (UINT1 *) (pMepNewNode));

                if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      i4SetValDot1agCfmMepRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }

            /* Add MEP node in LbLt Global info's MepTableIndex */
            if (EcfmLbLtAddMepEntry
                (pMepNewNode, ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Add MEP Entry FAILS in LBLT Task\n");

                /* Destroy RMepDb */
                pRMepNode =
                    (tEcfmCcRMepDbInfo *)
                    TMO_DLL_First (&(pMepNewNode->RMepDb));
                while (pRMepNode != NULL)
                {
                    pTempRMepNode = pRMepNode;
                    pRMepNode = (tEcfmCcRMepDbInfo *)
                        TMO_DLL_Next (&(pMepNewNode->RMepDb),
                                      &(pRMepNode->MepDbDllNode));

                    EcfmDeleteRMepEntry (pMepNewNode, pTempRMepNode);
                    pTempRMepNode = NULL;
                }

                ECFM_MEMSET (&(pMepNewNode->RMepDb), ECFM_INIT_VAL,
                             sizeof (pMepNewNode->RMepDb));

                EcfmSnmpLwHandleMepAddFailure (pMepNewNode);

                if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                                      ECFM_CC_CURR_CONTEXT_ID (),
                                      u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      i4SetValDot1agCfmMepRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }
            if (ECFM_IS_GLOBAL_CCM_OFFLOAD_ENABLED ())

            {
                pMepNewNode->b1MepCcmOffloadStatus = ECFM_TRUE;
            }

            else

            {
                pMepNewNode->b1MepCcmOffloadStatus = ECFM_FALSE;
            }

            /* To hadle scenerio in wchish CCi is enabled before MEP is created
             * in the system */
            nmhSetDot1agCfmMepCciEnabled (u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4Dot1agCfmMepIdentifier,
                                          ECFM_CONVERT_TO_SNMP_BOOL (pMaNode->
                                                                     b1CciEnabled));

            break;

        case ECFM_ROW_STATUS_DESTROY:

#ifdef NPAPI_WANTED
            if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
            {
                if (pMepNode->pPortInfo != NULL)
                {
                    LmHwInfo.u4PortId = pMepNode->pPortInfo->u4IfIndex;
                    LmHwInfo.u2VlanId = pMepNode->u4PrimaryVidIsid;
                    LmHwInfo.u4TxStatsHwId = pMepNode->u4LmTxStatsHwId;
                    LmHwInfo.u4RxStatsHwId = pMepNode->u4LmRxStatsHwId;
                }

                pMepNode->u4LmTxStatsHwId = pMepNode->u4LmRxStatsHwId = 0;
            }
#endif
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
	    if (pMepNode->i4LoopbackStatus == ECFM_SNMP_TRUE)
	    {
	        if (EcfmDeleteHwLoopback (pMepNode) != ECFM_SUCCESS)
	        {
		    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
	   		         "\tEcfmMepUtlSetAgMepRowStatus: Failed to Delete\n");
		    return SNMP_FAILURE;
		}
	    }
#endif
            /* Check if MEP has been configured on particular port */
            if (pMepNode->pPortInfo != NULL)
            {
                /* Set ifIndex if MEP is configured to do MIP or Error 
                 * evaluation */
                b1MepConfigured = ECFM_TRUE;
                u2PortNum = pMepNode->u2PortNum;
                u2VlanId = (UINT2) pMepNode->u4PrimaryVidIsid;
                u1Direction = pMepNode->u1Direction;
            }

#ifdef NPAPI_WANTED	    /* Remove the entry from global h/w MEP table */
            if (RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode) != NULL)
            {
                RBTreeRem (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode);
            }
#endif
            /* Remove node from its associated MA and 
             * MepTableIndex in Global info */
            EcfmSnmpLwDeleteMepEntry (pMepNode);

            if (pMepNode->pEcfmMplsParams != NULL)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_MPTP_PARAMS_POOL,
                                     (UINT1 *) (pMepNode->pEcfmMplsParams));
            }

            /* Release this node's memory from its pool */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL, (UINT1 *) (pMepNode));
            pMepNode = NULL;

            /* Evaluate for MIP creation for a change in port's entities
             * configuration having ifIndex  u4IfIndex */
            /* Check if MEP that has been deleted was configured on particular 
             * port */
            if (b1MepConfigured)

            {

                /* Check if it was vlan unaware MEP, then MIP creation will not
                 * be affected with this */
                if (u2VlanId != 0)

                {

                    /* Update default MD status, if there exists any UP MEP
                     * associated with this MA */
                    if ((u1Direction == ECFM_MP_DIR_UP) &&
                        (EcfmIsMepAssocWithMa
                         (pMaNode->u4MdIndex, pMaNode->u4MaIndex, -1,
                          ECFM_MP_DIR_UP) == ECFM_FALSE))

                    {
                        EcfmSnmpLwUpdateDefaultMdStatus (pMdNode->u1Level,
                                                         u2VlanId, ECFM_TRUE);
                    }
                    /* Evaluation has to be done for all the ports */
                    EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanId, ECFM_TRUE);
                    if (EcfmCcUtilChkIfCfgCfmLeakErr
                        (pMaNode, u2PortNum, u2VlanId) == ECFM_TRUE)

                    {
                        if (EcfmCcUtilAddConfigErrEntry
                            (u2PortNum, u2VlanId,
                             ECFM_CONFIG_ERR_CFM_LEAK) != ECFM_SUCCESS)

                        {
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                         ECFM_OS_RESOURCE_TRC,
                                         "Could not add"
                                         "Configuration error ECFM_CONFIG_ERR_CFM_LEAK"
                                         "in Configuration Error List Table"
                                         "\r\n");
                        }
                    }

                    else

                    {

                        /* Clear CFM leak error if exists already */
                        EcfmCcUtilDeleteConfigErrEntry (u2PortNum,
                                                        u2VlanId,
                                                        ECFM_CONFIG_ERR_CFM_LEAK);
                    }

                    /* While destroying the  MEP, to verify whether CFM Leak. If Leak exist 
                       raise the CFM Leak */
                    EcfmCcUtilClearCfmLeakErr (pMaNode, u2PortNum, u2VlanId);

                    if (EcfmCcUtilChkIfCfgConfictVidErr
                        (pMaNode, u2PortNum, u2VlanId) == ECFM_TRUE)

                    {
                        if (EcfmCcUtilAddConfigErrEntry
                            (u2PortNum, u2VlanId,
                             ECFM_CONFIG_ERR_CONFLICT_VIDS) != ECFM_SUCCESS)

                        {
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                                         ECFM_OS_RESOURCE_TRC,
                                         "Could not add"
                                         "Configuration error ECFM_CONFIG_ERR_CONFLICT_VIDS"
                                         "in Configuration Error List Table"
                                         "\r\n");
                        }
                    }

                    else

                    {

                        /* Clear CONFLICTING VIDs error if exists already */
                        EcfmCcUtilDeleteConfigErrEntry (u2PortNum,
                                                        u2VlanId,
                                                        ECFM_CONFIG_ERR_CONFLICT_VIDS);
                    }
                }

                else

                {

                    /* A VLAN unaware MEP is deleted, check if any other vlan
                     * unware MEP is present in the system
                     */
                    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
                    if (pPortInfo == NULL)
                    {
                        break;
                    }
                    /* sync the vlan unaware MEP present status at lblt also */
                    pPortInfo->b1UnawareMepPresent = ECFM_FALSE;
                    EcfmLbLtConfUnawreMepPresent (ECFM_FALSE, u2PortNum);
                    gpEcfmCcMepNode->u2PortNum = u2PortNum;
                    gpEcfmCcMepNode->u4PrimaryVidIsid = u2VlanId;
                    pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                              gpEcfmCcMepNode, NULL);
                    while ((pMepNode != NULL) &&
                           (pMepNode->u2PortNum == u2PortNum))
                    {
                        if (pMepNode->u4PrimaryVidIsid == 0)

                        {
                            pPortInfo->b1UnawareMepPresent = ECFM_TRUE;

                            /* sync the vlan unaware MEP present status at lblt also */
                            EcfmLbLtConfUnawreMepPresent (ECFM_TRUE, u2PortNum);
                            break;
                        }
                        pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                                  (tRBElem *) pMepNode, NULL);
                    }
                }
            }
            /* Check if any other MEP is present on this port, if no then set
             * the related BIT off in Demux port-list
             */
            pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
            StackInfo.u1MdLevel = 0;
            StackInfo.u4VlanIdIsid = 0;
            StackInfo.u1Direction = 0;
            if ((pPortInfo != NULL) && RBTreeGetNext
                (pPortInfo->StackInfoTree, (tRBElem *) & StackInfo,
                 NULL) == NULL)
            {
                ECFM_RESET_DEMUX_PORT (u2PortNum);
#ifdef NPAPI_WANTED
                if (EcfmFsMiEcfmStopLm (&LmHwInfo) != FNP_SUCCESS)
                {

                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                 "EcfmMepUtlSetAgMepRowStatus: EcfmStopLM FAILED \r\n");
                    return SNMP_FAILURE;
                }
#endif
            }
            break;
        default:
            break;
    }

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4Dot1agCfmMdIndex,
                          u4Dot1agCfmMaIndex, u4Dot1agCfmMepIdentifier,
                          i4SetValDot1agCfmMepRowStatus));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* workaround for incrememntal MSR */
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepIfIndex
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepIfIndex (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 i4TestValDot1agCfmMepIfIndex)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;

    /*   UINT4    u4ContextId = ECFM_INIT_VAL;
       UINT2    u2LocalPortNum = ECFM_INIT_VAL; */

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MA Entry from the Given Indices */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry Exists\n");
        return SNMP_FAILURE;

    }
    /*If the MA service type is either
     * LSP /PW then set the port number to the default value */
    if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
        (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MA Selector type is either LSP | PW"
                     " Interface Index cannot be set for this MEP\n");
        return SNMP_FAILURE;
    }

    /* Validate IfIndex value to be set */
    if ((i4TestValDot1agCfmMepIfIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        ((UINT4) i4TestValDot1agCfmMepIfIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Interface Index for MEP \n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP node exists for the indices\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check whether IfIndex corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP Row Status already ATIVE"
                     " Given Interface Index cannot be set for this MEP\n");
        return SNMP_FAILURE;
    }
    /* Check If MEP had already been configured with some 
     * IfIndex, level, PrimaryVid and Direction */
    if (pMepNode->pPortInfo != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP Entry with given parameters exists \n");
        return SNMP_FAILURE;
    }
    /* Check if MEP is already configured with same IfIndex */
    if (i4TestValDot1agCfmMepIfIndex == (INT4) (pMepNode->u2PortNum))
    {
        return SNMP_SUCCESS;
    }
    pPortInfo = ECFM_CC_GET_PORT_INFO (i4TestValDot1agCfmMepIfIndex);
    if ((pPortInfo == NULL) ||
        (EcfmLbLtPortCreated (i4TestValDot1agCfmMepIfIndex,
                              ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_TRUE))
    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Add MEP Entry FAILED \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepDirection
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepDirection (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4TestValDot1agCfmMepDirection)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown \n");
        return SNMP_FAILURE;
    }
    /* Get MA Entry from the Given Indices */
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry Exists\n");
        return SNMP_FAILURE;

    }

    /* Validate MEP direction value to be set */
    if ((i4TestValDot1agCfmMepDirection != ECFM_MP_DIR_DOWN) &&
        (i4TestValDot1agCfmMepDirection != ECFM_MP_DIR_UP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MEP Direction\n");
        return SNMP_FAILURE;
    }

    /*  If the MA service type is either
     * LSP /PW then set the port number to the default value */
    if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
        (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
    {
        if (i4TestValDot1agCfmMepDirection == ECFM_MP_DIR_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: MEP Entry with given Indices exists\n");
            return SNMP_FAILURE;
        }
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Direction corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP Entry with given Indices exists\n");
        return SNMP_FAILURE;
    }
    /* Check MEP's Direction can be modified */
    /* Check If MEP had already been configured with some 
     * IfIndex, level, PrimaryVid and Direction */
    if (pMepNode->pPortInfo != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP Entry already exists\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1agCfmMepDirection == ECFM_MP_DIR_UP) &&
         (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))
    {
#ifdef NPAPI_WANTED
        if (ECFM_HW_UPMEP_OFFLOAD_SUPPORT() == ECFM_TRUE)
        {
            return SNMP_SUCCESS;
        }
#endif
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Offload Enabled For Up Mep \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepPrimaryVid
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepPrimaryVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepPrimaryVid (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4 u4TestValDot1agCfmMepPrimaryVid)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        /* Validate PrimaryVid value to be set */
        if (u4TestValDot1agCfmMepPrimaryVid >
            ECFM_ISID_MAX + ECFM_VLANID_MAX + 1)
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Primary VID value\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Validate PrimaryVid value to be set */
        if (u4TestValDot1agCfmMepPrimaryVid > ECFM_MEP_PRIMARY_VLANID_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Primary VID value\n");
            return SNMP_FAILURE;
        }
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists with given Indices\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check if PrimaryVid corresponding to MEP entry can be set */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:  Cannot set Primary VID"
                     " as Row Status for this MEP is ACTIVE\n");
        return SNMP_FAILURE;
    }
    /* Check If MEP had already been configured with 
       some IfIndex, Level, PrimaryVid and Direction */
    if (pMepNode->pPortInfo != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP Entry already exists \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepActive
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepActive (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             INT4 i4TestValDot1agCfmMepActive)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    UINT4               u4PhyPort  = 0;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is  Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate MepActive value, that is to be set */
    if ((i4TestValDot1agCfmMepActive != ECFM_SNMP_FALSE) &&
        (i4TestValDot1agCfmMepActive != ECFM_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid value for MEP Active\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check MepActive corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Cannot set MEP Active status"
                     "as Row Status is already ACTIVE \n");
        return SNMP_FAILURE;
    }

    pTempPortInfo = NULL;
    u4PhyPort = ECFM_CC_GET_PHY_PORT (pMepNode->u2PortNum,
                          pTempPortInfo);

    if ((pMepNode->u1Direction == ECFM_MP_DIR_UP) &&
        (EcfmUtilCheckIfIcclInterface (u4PhyPort) == OSIX_SUCCESS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Up MEP cannot be enabled on ICCL interface \r\n");
        return SNMP_FAILURE;
    }

    if ((pMepNode->u1Direction == ECFM_MP_DIR_DOWN) &&
        (EcfmUtilCheckIfMcLagInterface (u4PhyPort) == OSIX_SUCCESS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Down MEP cannot be enabled on MCLAG interface \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepCciEnabled
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepCciEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepCciEnabled (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 INT4 i4TestValDot1agCfmMepCciEnabled)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate CCiEnabled value, that is to be set */
    if ((i4TestValDot1agCfmMepCciEnabled != ECFM_SNMP_FALSE) &&
        (i4TestValDot1agCfmMepCciEnabled != ECFM_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for CCI Enabled\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check CciEnabled corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: CCI Enabled cannot be set"
                     "as MEP Row Status is ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepCcmLtmPri
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepCcmLtmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepCcmLtmPri (UINT4 *pu4ErrorCode,
                             UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4TestValDot1agCfmMepCcmLtmPriority)
{

    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate Ccm Ltm Priority value, that is to be set */
    if (u4TestValDot1agCfmMepCcmLtmPriority > ECFM_VLAN_PRIORITY_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for CCM-LTM Priority\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry exists \n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check CcmLtmPriority corresponding to MEP entry
     * can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: CCM-LTM Priority cannot be set"
                     "as MEP Row Status is ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepLowPrDef
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepLowPrDef
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepLowPrDef (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               INT4 i4TestValDot1agCfmMepLowPrDef)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdowm\n");
        return SNMP_FAILURE;
    }
    /* Check the Low priority defect test value */
    if ((i4TestValDot1agCfmMepLowPrDef < ECFM_DEF_ALL) ||
        (i4TestValDot1agCfmMepLowPrDef > ECFM_DEF_NO_XCON))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for Lowest Priority Defect\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP entry exist for given indices\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check MEP Lowest priority defects corresponding to MEP entry
     * can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Cannot set Lowest Priority"
                     " Defect as MEP Row Status is ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepFngAlmTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepFngAlarmTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepFngAlmTime (UINT4 *pu4ErrorCode,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 i4TestValDot1agCfmMepFngAlarmTime)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate Fng alarm time value, to be set */
    if ((i4TestValDot1agCfmMepFngAlarmTime < ECFM_FNG_ALARM_TIME_MIN) ||
        (i4TestValDot1agCfmMepFngAlarmTime > ECFM_FNG_ALARM_TIME_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for FNG Alarm Time\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check FngAlarm time corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Cannot set FNG Alarm Time"
                     " as MEP Row Status is ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepFngRstTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepFngResetTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepFngRstTime (UINT4 *pu4ErrorCode,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              INT4 i4TestValDot1agCfmMepFngResetTime)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Check the Fng reset time value, to be set */
    if ((i4TestValDot1agCfmMepFngResetTime < ECFM_FNG_RESET_TIME_MIN) ||
        (i4TestValDot1agCfmMepFngResetTime > ECFM_FNG_RESET_TIME_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for FNG Reset Time\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check FngReset time corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Cannot set FNG Reset Time "
                     " as MEP Row Status is ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepTxLbmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepTxLbmStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  INT4 i4TestValDot1agCfmMepTransmitLbmStatus)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate the Transmit Lbm status value, to be set */
    if (i4TestValDot1agCfmMepTransmitLbmStatus != ECFM_TX_STATUS_START)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value Transmit LBM Status\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
#ifdef MBSM_WANTED
    /*Check whether Ports are Present in system for this MEP */
    if (EcfmMbsmIsPortPresent (pMepNode->pPortInfo->u4IfIndex,
                               pMepNode->u4PrimaryVidIsid,
                               pMepNode->u1Direction) == ECFM_FALSE)
    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_PRESENT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is Enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Transmit Lbm status corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set TransmitLBM Status "
                       " as MEP Row Status is other than ECFM_ROW_STATUST_ACTIVE\n");
        return SNMP_FAILURE;
    }

    if (pMepNode->b1Active != ECFM_TRUE)
    {
        CLI_SET_ERR (CLI_ECFM_MEP_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is in InActive State"
                       " Cannot Set TransmitLBM Status\n");
        return SNMP_FAILURE;
    }

    /* Get Port Info from MEP */
    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pMepNode);
    if (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (pPortInfo->u2PortNum)
        == ECFM_FALSE)
    {
        CLI_SET_ERR (CLI_ECFM_NOT_ENABLED_ON_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM is not Enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    /* Check if LBM initiation transaction can be initiated */
    if (pLbInfo->u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        CLI_SET_ERR (CLI_ECFM_MEP_NOT_READY_TO_TRANSMIT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate Transaction\n");
        return SNMP_FAILURE;
    }
/* Y.1731 */
    /* Check if LBM initiation transaction can be initiated with configured LBM
     * parameters */
    if (EcfmSnmpLwTestLbmTxParam (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                  u4Dot1agCfmMepIdentifier) != ECFM_TRUE)
    {
        CLI_SET_ERR (CLI_ECFM_MEP_PARAM_CONFIG_TRANSMIT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: LBM cannot be initiated "
                       " with configured parameters\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmDstMacAddr
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmDstMacAddr (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   tMacAddr
                                   TestValDot1agCfmMepTransmitLbmDestMacAddress)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tMacAddr            TempMacAddr;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Information doesn't exists\n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is Enabled on MEP Port\n");
        return SNMP_FAILURE;
    }
    /* Validate MacAddress value that is to be set */
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if (ECFM_MEMCMP (TestValDot1agCfmMepTransmitLbmDestMacAddress, TempMacAddr,
                     ECFM_MAC_ADDR_LENGTH) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Destination MAC Address\n");
        return SNMP_FAILURE;
    }
    /* It should be the unicast mac address */
    if (ECFM_IS_MULTICAST_ADDR (TestValDot1agCfmMepTransmitLbmDestMacAddress))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Destination MAC Address\n");
        return SNMP_FAILURE;
    }

    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to Initiate LBM transaction\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Lbm Target Mac Address corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot Initiate "
                       " LBM transaction as MEP Row Status is not ACTIVE\n");
        return SNMP_FAILURE;
    }
    /* It should not be MEP's own Mac Addreess */
    if (pMepNode->pPortInfo != NULL)
    {
        if (ECFM_LBLT_GET_PORT_INFO (pMepNode->u2PortNum) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                           "\tSNMP: No Port Information \n");
            return SNMP_FAILURE;
        }
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                   (pMepNode->u2PortNum)->u4IfIndex,
                                   TempMacAddr);
        if (ECFM_MEMCMP
            (TempMacAddr, TestValDot1agCfmMepTransmitLbmDestMacAddress,
             ECFM_MAC_ADDR_LENGTH) == 0)
        {
            CLI_SET_ERR (CLI_ECFM_DEST_MAC_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                           "\tSNMP: Invalid Destination MAC Address"
                           " Destination MAC Address is of the Initiating MEP\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmDestMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmDestMepId (UINT4 *pu4ErrorCode,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4
                                  u4TestValDot1agCfmMepTransmitLbmDestMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtRMepDbInfo *pRMepNode = NULL;
    tEcfmMacAddr        TempMacAddr = { 0 };

    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Inforamtion doesn't Exists\n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is Enabled on MEP Port\n");
        return SNMP_FAILURE;
    }
    /* Now checking the test value */
    if (u4TestValDot1agCfmMepTransmitLbmDestMepId > ECFM_MEPID_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value for Destination MEP Identifier\n");
        return SNMP_FAILURE;
    }
    /* Check if Target MepId is not same as MepId of corresponding MEP entry */
    if (u4TestValDot1agCfmMepTransmitLbmDestMepId == u4Dot1agCfmMepIdentifier)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MEPID_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Destination MEP ID is of the Initiating MEP\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Lbm Target MepId corresponding to MEP entry can 
     * be set or not */
    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate Transaction\n");
        return SNMP_FAILURE;
    }

    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot Initiate transaction "
                       " as MEP Row Status is not ACTIVE\n");
        return SNMP_FAILURE;
    }
    pRMepNode = EcfmLbLtUtilGetRMepDbEntry (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4TestValDot1agCfmMepTransmitLbmDestMepId);
    /* Check if Target MepId is part of same MA, and if it is configured */
    if (pRMepNode == NULL)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MEPID_RMEPDB_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Remote MEP ID "
                       " is not of same MA as of Initiating MEP\n");
        return SNMP_FAILURE;
    }
    /* Check if the Mac address of the remote mep is learned (using CCM) or not */
    if (ECFM_MEMCMP (TempMacAddr, pRMepNode->RMepMacAddr, ECFM_MAC_ADDR_LENGTH)
        == 0)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MEPID_RMEPDB_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Destination MEP ID doesn't "
                       "Exists in Remote MEP DB\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmDstIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmDstIsMepId (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4
                                   i4TestValDot1agCfmMepTransmitLbmDestIsMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: NO MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Information doesn't exits \n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* Validate the value to be set */
    if ((i4TestValDot1agCfmMepTransmitLbmDestIsMepId != ECFM_SNMP_FALSE) &&
        (i4TestValDot1agCfmMepTransmitLbmDestIsMepId != ECFM_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value \n");
        return SNMP_FAILURE;
    }
    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate transaction\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Lbm Target MepIsId corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set DestIsMepId"
                       "as MEP Row Status is not ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmMessages
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmMessages (UINT4 *pu4ErrorCode,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 INT4 i4TestValDot1agCfmMepTransmitLbmMessages)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Information doesn't exists\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* Check the Lbm messages to send, value to be set */
    if ((i4TestValDot1agCfmMepTransmitLbmMessages < ECFM_LBM_MESSAGE_MIN)
        || (i4TestValDot1agCfmMepTransmitLbmMessages > ECFM_LBM_MESSAGE_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate LBM transaction\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Lbm messages to send corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set Number LBMs to be sent as MEP "
                       "Row Status is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmDataTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmDataTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmDataTlv (UINT4 *pu4ErrorCode,
                                UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValDot1agCfmMepTransmitLbmDataTlv)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Information doesn't exist\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* Check Lbm Data TLV value that is to be set */
    if (pTestValDot1agCfmMepTransmitLbmDataTlv->i4_Length >
        ECFM_LBM_DATA_TLV_LEN_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Wrong LBM Data TLV Length\n");
        return SNMP_FAILURE;
    }
    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate Transaction\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Lbm Data TLV  corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot Set Data TLV as MEP Row Status is"
                       "other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmVlanPri
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmVlanPri (UINT4 *pu4ErrorCode,
                                UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4
                                i4TestValDot1agCfmMepTransmitLbmVlanPriority)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP:MEP's Port Info Doesn't Exists\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* VAlidate the Lbm Vlan Priority value, to be set */
    if ((i4TestValDot1agCfmMepTransmitLbmVlanPriority < ECFM_VLAN_PRIORITY_MIN)
        || (i4TestValDot1agCfmMepTransmitLbmVlanPriority >
            ECFM_VLAN_PRIORITY_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid LBM Vlan Priority value\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Lbm Vlan Priority  corresponding to MEP entry can 
     * be set or not */
    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate another transaction\n");
        return SNMP_FAILURE;
    }

    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set Vlan Priority as MEP Row Status is"
                       "other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLbmVlanDropEna
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLbmVlanDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLbmVlanDropEna (UINT4 *pu4ErrorCode,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    INT4
                                    i4TestValDot1agCfmMepTransmitLbmVlanDropEnable)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Info doesn't Exists\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* Validate the Vlan Drop enable status value, that is to be set */
    if ((i4TestValDot1agCfmMepTransmitLbmVlanDropEnable != ECFM_SNMP_TRUE) &&
        (i4TestValDot1agCfmMepTransmitLbmVlanDropEnable != ECFM_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: InValid Value for Vlan Drop Enable parameter\n");
        return SNMP_FAILURE;
    }

    /* Check is there is already a LBM transaction going on */
    if (pMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate another transaction\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Vlan Drop Enable parameter cannot be set as "
                       "MEP Row Status is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepTxLtmStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepTxLtmStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  INT4 i4TestValDot1agCfmMepTransmitLtmStatus)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry Exists\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Info doesn't Exists\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* Validate the Transmit Ltm status value, to be set */
    if (i4TestValDot1agCfmMepTransmitLtmStatus != ECFM_TX_STATUS_START)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value for TransmitLbmStatus\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Transmit Lbm status corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set TransmitLbmStatus as MEP "
                       "Row Status is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    if (pMepNode->b1Active != ECFM_TRUE)
    {
        CLI_SET_ERR (CLI_ECFM_MEP_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP status is disabled\n");
        return SNMP_FAILURE;
    }

    /* Get Port Info from MEP */
    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pMepNode);
    if (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (pPortInfo->u2PortNum)
        == ECFM_FALSE)
    {
        CLI_SET_ERR (CLI_ECFM_NOT_ENABLED_ON_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is not enabled in LBLT Task\n");
        return SNMP_FAILURE;
    }
    if (ECFM_IS_LTR_CACHE_DISABLED () == ECFM_TRUE)
    {
        CLI_SET_ERR (CLI_ECFM_LTR_CACHE_DISABLE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: LTR Cache is disabled\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);

    /* Check if LTM initiation transaction can be initiated */
    if (pLtInfo->u1TxLtmStatus != ECFM_TX_STATUS_READY)
    {
        CLI_SET_ERR (CLI_ECFM_MEP_NOT_READY_TO_TRANSMIT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP is not ready to initiate another "
                       "LTM transaction\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepTxLtmFlags
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepTxLtmFlags (UINT4 *pu4ErrorCode,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValDot1agCfmMepTransmitLtmFlags)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP entry exists for given indices\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Info doesn't Exists\n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);
    if (pTestValDot1agCfmMepTransmitLtmFlags->pu1_OctetList == NULL)
    {
        /* set Ltm flags LSB */
        ECFM_LBLT_SET_LTM_FLAGS_LSB (pLtInfo->u1TxLtmFlags);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value for LTM Flags\n");
        return SNMP_FAILURE;
    }

    if ((*(pTestValDot1agCfmMepTransmitLtmFlags->pu1_OctetList) != 0) &&
        (*(pTestValDot1agCfmMepTransmitLtmFlags->pu1_OctetList) !=
         ECFM_MASK_WITH_VAL_128))

    {
        /* set Ltm flags LSB */
        ECFM_LBLT_SET_LTM_FLAGS_LSB (pLtInfo->u1TxLtmFlags);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value for LTM Flags\n");
        return SNMP_FAILURE;
    }
    /* Validate the Tx Ltm Flags value, that is to be set */
    if (pTestValDot1agCfmMepTransmitLtmFlags->i4_Length !=
        ECFM_FLAGS_FIELD_SIZE)
    {
        /* set Ltm flags LSB */
        ECFM_LBLT_SET_LTM_FLAGS_LSB (pLtInfo->u1TxLtmFlags);

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid length for LTM Flags\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Tx Ltm flags corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set LTM transmit Flag as MEP Row Status "
                       "is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLtmTgtMacAddr
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTargetMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLtmTgtMacAddr (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4
                                   u4Dot1agCfmMepIdentifier,
                                   tMacAddr
                                   TestValDot1agCfmMepTransmitLtmTargetMacAddress)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmMacAddr        TempMacAddr;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP entry exists for given Indices\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Info doesn't exists\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }
    /* Validate MacAddress value that is to be set */
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if (ECFM_MEMCMP
        (TestValDot1agCfmMepTransmitLtmTargetMacAddress, TempMacAddr,
         ECFM_MAC_ADDR_LENGTH) == 0)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value for LTM Target MAC Address\n");
        return SNMP_FAILURE;
    }
    /* It should be the unicast mac address */
    if (ECFM_IS_MULTICAST_ADDR (TestValDot1agCfmMepTransmitLtmTargetMacAddress))
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value for LTM Target MAC Address"
                       "Should be Unicast MAC Address\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Ltm Target Mac Address corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set Target MAC address as MEP "
                       "Row Status is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    /* It should not be MEP's own Mac Addreess */
    if (pMepNode->pPortInfo != NULL)
    {
        if (ECFM_LBLT_GET_PORT_INFO (pMepNode->u2PortNum) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                           "\tSNMP: No Port Information \n");
            return SNMP_FAILURE;
        }
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                   (pMepNode->u2PortNum)->u4IfIndex,
                                   TempMacAddr);
        if (ECFM_MEMCMP
            (TempMacAddr, TestValDot1agCfmMepTransmitLtmTargetMacAddress,
             ECFM_MAC_ADDR_LENGTH) == 0)
        {
            CLI_SET_ERR (CLI_ECFM_DEST_MAC_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                           "\tSNMP: Target MAC address is of Initiating MEP\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLtmTgtMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTargetMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLtmTgtMepId (UINT4 *pu4ErrorCode,
                                 UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4
                                 u4TestValDot1agCfmMepTransmitLtmTargetMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtRMepDbInfo *pRMepNode = NULL;
    tEcfmMacAddr        TempMacAddr = { 0 };

    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry exists for given Indices\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Info doesn't exists\n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled\n");
        return SNMP_FAILURE;
    }
    /* Validate the Target MepId value */
    if (u4TestValDot1agCfmMepTransmitLtmTargetMepId > ECFM_MEPID_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value for Target MEP ID\n");
        return SNMP_FAILURE;
    }
    /* Check if Target MepId is not same as MepId of corresponding MEP entry */
    if (u4TestValDot1agCfmMepTransmitLtmTargetMepId == u4Dot1agCfmMepIdentifier)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MEPID_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Target MEPId is of the Initiating MEP Id\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Ltm Target MepId corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set Target MepId as MEP Row Status"
                       " is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    pRMepNode = EcfmLbLtUtilGetRMepDbEntry (u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            u4Dot1agCfmMepIdentifier,
                                            u4TestValDot1agCfmMepTransmitLtmTargetMepId);
    /* Check if Target MepId is part of same MA, and if it is configured */
    if (pRMepNode == NULL)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MEPID_RMEPDB_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Target MepId doesn't belong to same MA as of "
                       "initiating MEP\n");
        return SNMP_FAILURE;
    }
    /* Check if the Mac address of the remote mep is learned (using CCM) or not */
    if (ECFM_MEMCMP (TempMacAddr, pRMepNode->RMepMacAddr, ECFM_MAC_ADDR_LENGTH)
        == 0)
    {
        CLI_SET_ERR (CLI_ECFM_DEST_MEPID_RMEPDB_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Entry of Target MepId in Remote MEP DB\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLtmTgtIsMepId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTargetIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLtmTgtIsMepId (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1agCfmMdIndex,
                                   UINT4 u4Dot1agCfmMaIndex,
                                   UINT4 u4Dot1agCfmMepIdentifier,
                                   INT4
                                   i4TestValDot1agCfmMepTransmitLtmTargetIsMepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry exists for given Indices\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Info doesn't exists\n");
        return SNMP_FAILURE;
    }

    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled \n");
        return SNMP_FAILURE;
    }
    /* Validate  the TargetIsMepId value, that is to be set */
    if ((i4TestValDot1agCfmMepTransmitLtmTargetIsMepId != ECFM_SNMP_FALSE) &&
        (i4TestValDot1agCfmMepTransmitLtmTargetIsMepId != ECFM_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value for TargetIsMepId\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set TargetIsMepId as MEP Row Status"
                       " is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepTxLtmTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepTxLtmTtl (UINT4 *pu4ErrorCode,
                               UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4TestValDot1agCfmMepTransmitLtmTtl)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry exists for given Indices\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Information doesn't exists\n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP:Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);
    /* Validate the Ltm's TTL value, that is to be set */
    if (u4TestValDot1agCfmMepTransmitLtmTtl > ECFM_LTM_TTL_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        /* Set Ttl's MSB */
        ECFM_LBLT_SET_LTM_TTL_MSB (pLtInfo->u2TxLtmTtl);
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value of LTM TTL\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Ltm's Ttl value to send corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: LTM TTL value cannot be set as MEP Row Status"
                       "is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlTstAgMepTxLtmEgrId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepTransmitLtmEgressIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTstAgMepTxLtmEgrId (UINT4 *pu4ErrorCode,
                              UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4
                              u4Dot1agCfmMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValDot1agCfmMepTransmitLtmEgressIdentifier)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                               u4Dot1agCfmMaIndex,
                                               u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry exists with given Indices\n");
        return SNMP_FAILURE;
    }
/*Check whether PortInfo in the MepNode is NULL or not*/
    if (pMepNode->pPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: MEP's Port Information doesn't exists\n");
        return SNMP_FAILURE;
    }
    /* Check Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Y1731 is enabled on MEP Port\n");
        return SNMP_FAILURE;
    }

    /* Validate the EgressId value, that is to be set */
    if (pTestValDot1agCfmMepTransmitLtmEgressIdentifier->i4_Length !=
        ECFM_EGRESS_ID_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid length for LTM Egress Identifier \n");
        return SNMP_FAILURE;
    }
    if (pTestValDot1agCfmMepTransmitLtmEgressIdentifier->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid value of LTM Egress Identifier\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check Ltm's EgressId corresponding to MEP entry can 
     * be set or not */
    if (pMepNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Cannot set LTM Egress Identifier as MEP Row"
                       "Status is other than ECFM_ROW_STATUS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlTestv2AgMepRowStatus
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier

                The Object 
                testValDot1agCfmMepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlTestv2AgMepRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                INT4 i4TestValDot1agCfmMepRowStatus)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepListInfo *pMaMepListNode = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    tEcfmCcPortInfo    *pGetPortInfo = NULL;
    UINT4               u4EntryCount = 0;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate Row status value */
    if ((i4TestValDot1agCfmMepRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValDot1agCfmMepRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        CLI_SET_ERR (CLI_ECFM_INVALID_ROWSTATUS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid value for Row Status\n");
        return SNMP_FAILURE;
    }
    /* Validate index range */
    if (ECFM_VALIDATE_MEP_TABLE_INDICES (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex,
                                         u4Dot1agCfmMepIdentifier))
    {
        CLI_SET_ERR (CLI_ECFM_MEP_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid indices for MEP\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4Dot1agCfmMepIdentifier);
    if (pMepNode == NULL)
    {

        /* MEP entry corresponding to MdIndex, MaIndex, MepId does not exists,
         * and user wants to change its row status */

        if (i4TestValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No MEP Entry for the given Index \r\n");
            /* As per RFC1903, Deleting a non-existing row status should return success */
            return SNMP_SUCCESS;
        }

        else if (i4TestValDot1agCfmMepRowStatus !=
                 ECFM_ROW_STATUS_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_ECFM_MEP_NOT_PRESENT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: MEP Entry doesn't exist for given Indices\n");
            return SNMP_FAILURE;
        }
        /* Get MD and MA entries and check its Row status, should be ACTIVE */
        pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
        pMdNode = EcfmSnmpLwGetMdEntry (u4Dot1agCfmMdIndex);

        if (pMdNode == NULL)
        {
            CLI_SET_ERR (CLI_ECFM_DOM_NOT_PRESENT_ERR);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No MD entries exist for given Indices\n");
            return SNMP_FAILURE;
        }
        if (pMaNode == NULL)
        {
            CLI_SET_ERR (CLI_ECFM_MA_NOT_PRESENT_ERR);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No MA entries exist for given Indices\n");
            return SNMP_FAILURE;
        }
        if ((pMdNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE) ||
            (pMaNode->u1RowStatus != ECFM_ROW_STATUS_ACTIVE))
        {
            CLI_SET_ERR (CLI_ECFM_INVALID_ROWSTATUS);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: MD or MA Row Status is other than "
                         "ECFM_ROW_STATUS_ACTIVE \n");
            return SNMP_FAILURE;
        }
        /* Check entry corresponding to this MepId, should be present in its 
         * associated MA's MepList */
        pMaMepListNode = EcfmSnmpLwGetMaMepListEntry (u4Dot1agCfmMdIndex,
                                                      u4Dot1agCfmMaIndex,
                                                      u4Dot1agCfmMepIdentifier);
        if (pMaMepListNode == NULL)
        {
            CLI_SET_ERR (CLI_ECFM_MEP_CONFIG_MEP_LIST_ERR);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No Entry found for MaMepList for given Indices \n");
            return SNMP_FAILURE;
        }
        /* Number of MEP created count is fetched from RBTree structure and it 
         * is compared with maximum Number of MEP that are allowed to create. 
         * During initialization tEcfmCcMepInfo structure consumes one block of 
         * memory so error will be thrown when we try to configure the maximum 
         * MEP */ 
        RBTreeCount (ECFM_CC_MEP_TABLE, &u4EntryCount); 
        if (u4EntryCount >= ECFM_MAX_MEP_IN_SYSTEM - ECFM_VAL_1) 
        { 
            CLI_SET_ERR (CLI_ECFM_MAX_MEP_CREATION_REACHED); 
            *pu4ErrorCode = SNMP_ERR_GEN_ERR; 
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC, 
                         "\tSNMP:Reaches maximum MEP creation\n"); 
            return SNMP_FAILURE; 
        } 

        return SNMP_SUCCESS;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists,
     * and its row status is same as user wants to set */
    if (pMepNode->u1RowStatus == (UINT1) i4TestValDot1agCfmMepRowStatus)
    {
        return SNMP_SUCCESS;
    }
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists,
     * and user wants to create new MEP entry with same indices */
    if ((i4TestValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT) ||
        (i4TestValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        CLI_SET_ERR (CLI_ECFM_MEP_CONFIG_EXIST_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP entry with given indices already exists\n");
        return SNMP_FAILURE;
    }

    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        CLI_SET_ERR (CLI_ECFM_MA_NOT_PRESENT_ERR);
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Information for given indices\n");
        return SNMP_FAILURE;
    }
    if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {
        /* If user wants to make MEP entry's row status ACTIVE */
        if ((i4TestValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_ACTIVE) &&
            (pMepNode->u1RowStatus == ECFM_ROW_STATUS_NOT_IN_SERVICE))
        {
            if (pMepNode->pPortInfo == NULL)
            {
                if (EcfmSnmpLwIsInfoConfiguredForMep (pMepNode) != ECFM_SUCCESS)
                {
                    CLI_SET_ERR (CLI_ECFM_MEP_CONFIG_INFO_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "\tSNMP: MEP cannot be configured with provided"
                                 " parameters\n");
                    return SNMP_FAILURE;
                }
            }
        }
    }
    if (i4TestValDot1agCfmMepRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry for the given Index \r\n");
        /* As per RFC1903, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }

    /* As per the IEEE standard sec 22.1.8, if Vlan Aware MEP present on a port which is part of
     * port channel then, user should not be allowed to make it as active*/
    pMaNode = EcfmSnmpLwGetMaEntry (u4Dot1agCfmMdIndex, u4Dot1agCfmMaIndex);
    if (pMaNode == NULL)
    {
        CLI_SET_ERR (CLI_ECFM_MA_NOT_PRESENT_ERR);
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Information for given indices\n");
        return SNMP_FAILURE;
    }
    if (pMaNode->u4PrimaryVidIsid != 0)
    {
        if ((pGetPortInfo =
             ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum)) == NULL)
        {
            CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No Port Information\n");
            return SNMP_FAILURE;
        }
        if ((pMepNode->u2PortNum != 0) &&
            (ECFM_CC_PORT_INFO (pMepNode->u2PortNum)->u2ChannelPortNum != 0)
            && (pMepNode->b1Active == ECFM_TRUE))
        {
            CLI_SET_ERR (CLI_ECFM_VLAN_AWARE_MEP_ACTIVE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Vlan unaware MEP cannot be set as active \n");
            return SNMP_FAILURE;
        }

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        if (ECFM_CC_ISPORT_SISP_LOG (pMepNode->u2PortNum,
                                     pTempPortInfo) == ECFM_TRUE)
        {
            if (EcfmL2IwfMiIsVlanUntagMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                                  (UINT2) pMaNode->
                                                  u4PrimaryVidIsid,
                                                  pGetPortInfo->u4IfIndex)
                == OSIX_TRUE)
            {
                /* SISP logical interface is to be 
                 * configured with MEP for vlan
                 * for which this port is untagged member port. 
                 * This configuration is invalid.
                 * */
                CLI_SET_ERR (CLI_ECFM_MEP_UNTAG_SISP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    else if (pMepNode->u2PortNum != 0)
    {
        if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
              (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
        {
            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;
            if (ECFM_CC_ISPORT_SISP_LOG (pMepNode->u2PortNum,
                                         pTempPortInfo) == ECFM_TRUE)
            {
                /* Vlan unaware MEP cannot be nstalled on SISP lgical 
                 * interfaces
                 * */
                CLI_SET_ERR (CLI_ECFM_VLAN_UNAWARE_MEP_SISP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/* LOW LEVEL Routines for Table : Dot1agCfmLtrTable. */

/****************************************************************************
 Function    :  EcfmMepUtlValAgLtrTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

PUBLIC INT1
EcfmMepUtlValAgLtrTable (UINT4 u4Dot1agCfmMdIndex,
                         UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 u4Dot1agCfmLtrSeqNumber,
                         UINT4 u4Dot1agCfmLtrReceiveOrder)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get LTR entry corresponding to MdIndex, MaIndex,MepId,LtrSeqNum,
     * LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    /* Check if LTR entry exists or not */
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entry found for given Indices\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetNextIndexAgLtrTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                nextDot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                nextDot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder
                nextDot1agCfmLtrReceiveOrder
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
PUBLIC INT1
EcfmMepUtlGetNextIndexAgLtrTable (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 *pu4NextDot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 *pu4NextDot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 *pu4NextDot1agCfmMepIdentifier,
                                  UINT4 u4Dot1agCfmLtrSeqNumber,
                                  UINT4 *pu4NextDot1agCfmLtrSeqNumber,
                                  UINT4 u4Dot1agCfmLtrReceiveOrder,
                                  UINT4 *pu4NextDot1agCfmLtrReceiveOrder)
{
    tEcfmLbLtLtrInfo    LtrInfo;
    tEcfmLbLtLtrInfo   *pLtrNextNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    ECFM_MEMSET (&LtrInfo, ECFM_INIT_VAL, ECFM_LBLT_LTR_INFO_SIZE);
    LtrInfo.u4MdIndex = u4Dot1agCfmMdIndex;
    LtrInfo.u4MaIndex = u4Dot1agCfmMaIndex;
    LtrInfo.u2MepId = (UINT2) u4Dot1agCfmMepIdentifier;
    LtrInfo.u4SeqNum = (UINT2) u4Dot1agCfmLtrSeqNumber;
    LtrInfo.u4RcvOrder = u4Dot1agCfmLtrReceiveOrder;

    pLtrNextNode = (tEcfmLbLtLtrInfo *) RBTreeGetNext
        (ECFM_LBLT_LTR_TABLE, (tRBElem *) & LtrInfo, NULL);

    if (pLtrNextNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entry for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the next indices from corresponding LTR entry */
    *pu4NextDot1agCfmMdIndex = pLtrNextNode->u4MdIndex;
    *pu4NextDot1agCfmMaIndex = pLtrNextNode->u4MaIndex;
    *pu4NextDot1agCfmMepIdentifier = (UINT4) (pLtrNextNode->u2MepId);
    *pu4NextDot1agCfmLtrSeqNumber = pLtrNextNode->u4SeqNum;
    *pu4NextDot1agCfmLtrReceiveOrder = pLtrNextNode->u4RcvOrder;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrTtl
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrTtl (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                       UINT4 u4Dot1agCfmMepIdentifier,
                       UINT4 u4Dot1agCfmLtrSeqNumber,
                       UINT4 u4Dot1agCfmLtrReceiveOrder,
                       UINT4 *pu4RetValDot1agCfmLtrTtl)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No traceroute entry for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Ltr Ttl from corresponding LTR entry */
    *pu4RetValDot1agCfmLtrTtl = (UINT4) (pLtrNode->u1LtrTtl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrForwarded
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrForwarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrForwarded (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             INT4 *pi4RetValDot1agCfmLtrForwarded)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entry for given indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the LtmForwarded status from corresponding LTR entry */
    if (pLtrNode->b1LtmForwarded == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmLtrForwarded = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmLtrForwarded = ECFM_SNMP_TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrTerminalMep
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrTerminalMep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrTerminalMep (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4Dot1agCfmLtrSeqNumber,
                               UINT4 u4Dot1agCfmLtrReceiveOrder,
                               INT4 *pi4RetValDot1agCfmLtrTerminalMep)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the TerminalMep status from corresponding LTR entry */
    if (pLtrNode->b1TerminalMep == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmLtrTerminalMep = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmLtrTerminalMep = ECFM_SNMP_TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrLastEgrId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrLastEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrLastEgrId (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDot1agCfmLtrLastEgressIdentifier)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the EgressID from corresponding LTR entry */
    ECFM_MEMCPY (pRetValDot1agCfmLtrLastEgressIdentifier->pu1_OctetList,
                 pLtrNode->au1LastEgressId, ECFM_EGRESS_ID_LENGTH);
    pRetValDot1agCfmLtrLastEgressIdentifier->i4_Length = ECFM_EGRESS_ID_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrNextEgrId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrNextEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrNextEgrId (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDot1agCfmLtrNextEgressIdentifier)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Next EgressID from corresponding LTR entry */
    ECFM_MEMCPY (pRetValDot1agCfmLtrNextEgressIdentifier->pu1_OctetList,
                 pLtrNode->au1NextEgressId, ECFM_EGRESS_ID_LENGTH);
    pRetValDot1agCfmLtrNextEgressIdentifier->i4_Length = ECFM_EGRESS_ID_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrRelay
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrRelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrRelay (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 u4Dot1agCfmLtrSeqNumber,
                         UINT4 u4Dot1agCfmLtrReceiveOrder,
                         INT4 *pi4RetValDot1agCfmLtrRelay)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the LtrRelay from corresponding LTR entry */
    *pi4RetValDot1agCfmLtrRelay = (INT4) (pLtrNode->u1RelayAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrChassisIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrChassisIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmLtrSeqNumber,
                                    UINT4 u4Dot1agCfmLtrReceiveOrder,
                                    INT4 *pi4RetValDot1agCfmLtrChassisIdSubtype)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the ChassisId sub type from corresponding LTR entry */
    pSenderId = &(pLtrNode->SenderId);
    *pi4RetValDot1agCfmLtrChassisIdSubtype =
        (INT4) (pSenderId->u1ChassisIdSubType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrChassisId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrChassisId (UINT4 u4Dot1agCfmMdIndex,
                             UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDot1agCfmLtrChassisId)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    MEMSET (pRetValDot1agCfmLtrChassisId->pu1_OctetList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the ChassisId from corresponding LTR entry */
    pSenderId = &(pLtrNode->SenderId);
    if ((pSenderId->ChassisId.pu1Octets != NULL) &&
        (pSenderId->ChassisId.u4OctLen != 0))

    {
        ECFM_MEMCPY (pRetValDot1agCfmLtrChassisId->pu1_OctetList,
                     pSenderId->ChassisId.pu1Octets,
                     pSenderId->ChassisId.u4OctLen);
        pRetValDot1agCfmLtrChassisId->i4_Length =
            (INT4) (pSenderId->ChassisId.u4OctLen);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrManAddressDomain
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrManAddressDomain (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmLtrSeqNumber,
                                    UINT4 u4Dot1agCfmLtrReceiveOrder,
                                    tSNMP_OID_TYPE *
                                    pRetValDot1agCfmLtrManAddressDomain)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the MgtAddress Domain from corresponding LTR entry */
    pSenderId = &(pLtrNode->SenderId);
    if ((pSenderId->MgmtAddressDomain.pu1Octets != NULL) &&
        (pSenderId->MgmtAddressDomain.u4OctLen != 0))
    {
        EcfmUtilDecodeAsn1BER (&
                               (pSenderId->MgmtAddressDomain.
                                pu1Octets[ECFM_VAL_2]),
                               pSenderId->MgmtAddressDomain.u4OctLen -
                               ECFM_VAL_2,
                               pSenderId->MgmtAddressDomain.pu1Octets[1],
                               pRetValDot1agCfmLtrManAddressDomain->pu4_OidList,
                               &(pRetValDot1agCfmLtrManAddressDomain->
                                 u4_Length));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrManAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrManAddress (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 u4Dot1agCfmLtrSeqNumber,
                              UINT4 u4Dot1agCfmLtrReceiveOrder,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDot1agCfmLtrManAddress)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the MgtAddress from corresponding LTR entry */
    pSenderId = &(pLtrNode->SenderId);
    if ((pSenderId->MgmtAddress.pu1Octets != NULL) &&
        (pSenderId->MgmtAddress.u4OctLen != 0))
    {
        ECFM_MEMCPY (pRetValDot1agCfmLtrManAddress->pu1_OctetList,
                     pSenderId->MgmtAddress.pu1Octets,
                     pSenderId->MgmtAddress.u4OctLen);
        pRetValDot1agCfmLtrManAddress->i4_Length =
            pSenderId->MgmtAddress.u4OctLen;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrIngress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrIngress (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           UINT4 u4Dot1agCfmLtrSeqNumber,
                           UINT4 u4Dot1agCfmLtrReceiveOrder,
                           INT4 *pi4RetValDot1agCfmLtrIngress)
{

    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the LtrIngress from corresponding LTR entry */
    *pi4RetValDot1agCfmLtrIngress = (INT4) (pLtrNode->u1IngressAction);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrIngressMac
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngressMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrIngressMac (UINT4 u4Dot1agCfmMdIndex,
                              UINT4 u4Dot1agCfmMaIndex,
                              UINT4 u4Dot1agCfmMepIdentifier,
                              UINT4 u4Dot1agCfmLtrSeqNumber,
                              UINT4 u4Dot1agCfmLtrReceiveOrder,
                              tMacAddr * pRetValDot1agCfmLtrIngressMac)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the LtrIngress Mac Address from corresponding LTR entry */
    ECFM_MEMCPY (pRetValDot1agCfmLtrIngressMac,
                 pLtrNode->IngressMacAddr, ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrIngPortIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngressPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrIngPortIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmLtrSeqNumber,
                                    UINT4 u4Dot1agCfmLtrReceiveOrder,
                                    INT4
                                    *pi4RetValDot1agCfmLtrIngressPortIdSubtype)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Ingress port type from corresponding LTR entry */
    *pi4RetValDot1agCfmLtrIngressPortIdSubtype =
        (INT4) (pLtrNode->u1IngressPortIdSubType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrIngressPortId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrIngressPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrIngressPortId (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4 u4Dot1agCfmLtrSeqNumber,
                                 UINT4 u4Dot1agCfmLtrReceiveOrder,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValDot1agCfmLtrIngressPortId)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Ingress portId from corresponding LTR entry */
    ECFM_MEMCPY (pRetValDot1agCfmLtrIngressPortId->pu1_OctetList,
                 pLtrNode->IngressPortId.pu1Octets,
                 pLtrNode->IngressPortId.u4OctLen);
    pRetValDot1agCfmLtrIngressPortId->i4_Length =
        (INT4) (pLtrNode->IngressPortId.u4OctLen);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrEgress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrEgress (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                          UINT4 u4Dot1agCfmMepIdentifier,
                          UINT4 u4Dot1agCfmLtrSeqNumber,
                          UINT4 u4Dot1agCfmLtrReceiveOrder,
                          INT4 *pi4RetValDot1agCfmLtrEgress)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Egress action from corresponding LTR entry */
    *pi4RetValDot1agCfmLtrEgress = (INT4) (pLtrNode->u1EgressAction);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrEgressMac
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgressMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrEgressMac (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                             UINT4 u4Dot1agCfmMepIdentifier,
                             UINT4 u4Dot1agCfmLtrSeqNumber,
                             UINT4 u4Dot1agCfmLtrReceiveOrder,
                             tMacAddr * pRetValDot1agCfmLtrEgressMac)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Egress mac address from corresponding LTR entry */
    ECFM_MEMCPY (pRetValDot1agCfmLtrEgressMac,
                 pLtrNode->EgressMacAddr, ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrEgrPortIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgressPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrEgrPortIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmLtrSeqNumber,
                                    UINT4 u4Dot1agCfmLtrReceiveOrder,
                                    INT4
                                    *pi4RetValDot1agCfmLtrEgressPortIdSubtype)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Egress PortId type address from corresponding LTR entry */
    *pi4RetValDot1agCfmLtrEgressPortIdSubtype =
        (INT4) (pLtrNode->u1EgressPortIdSubType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrEgressPortId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrEgressPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrEgressPortId (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmLtrSeqNumber,
                                UINT4 u4Dot1agCfmLtrReceiveOrder,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1agCfmLtrEgressPortId)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Egress PortId from corresponding LTR entry */
    ECFM_MEMCPY (pRetValDot1agCfmLtrEgressPortId->pu1_OctetList,
                 pLtrNode->EgressPortId.pu1Octets,
                 pLtrNode->EgressPortId.u4OctLen);

    pRetValDot1agCfmLtrEgressPortId->i4_Length = (INT4)
        (pLtrNode->EgressPortId.u4OctLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgLtrOrgSpecificTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmLtrSeqNumber
                Dot1agCfmLtrReceiveOrder

                The Object 
                retValDot1agCfmLtrOrganizationSpecificTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgLtrOrgSpecificTlv (UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  UINT4 u4Dot1agCfmMepIdentifier,
                                  UINT4 u4Dot1agCfmLtrSeqNumber,
                                  UINT4 u4Dot1agCfmLtrReceiveOrder,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDot1agCfmLtrOrganizationSpecificTlv)
{
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    UINT1              *pu1TempOrgSpecTlv = NULL;
    UINT2               u2OrgSpecTlvLength = ECFM_INIT_VAL;

    /* Get LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder */
    pLtrNode = EcfmSnmpLwGetLtrEntry (u4Dot1agCfmMdIndex,
                                      u4Dot1agCfmMaIndex,
                                      u4Dot1agCfmMepIdentifier,
                                      u4Dot1agCfmLtrSeqNumber,
                                      u4Dot1agCfmLtrReceiveOrder);
    if (pLtrNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Traceroute entries for given Indices\n");
        return SNMP_FAILURE;
    }
    /* LTR entry corresponding to indices next to MdIndex, MaIndex,MepId,
     * LtrSeqNum, LtrRecvOrder exists */
    /* Set the Organization spec TLV from corresponding LTR entry */
    pu1TempOrgSpecTlv =
        pRetValDot1agCfmLtrOrganizationSpecificTlv->pu1_OctetList;

    /* Put Org specific TLV's length field */
    u2OrgSpecTlvLength =
        (UINT2) (ECFM_OUI_FIELD_SIZE + ECFM_ORG_SPEC_SUB_TYPE_FIELD_SIZE) +
        (UINT2) pLtrNode->OrgSpecTlv.Value.u4OctLen;
    ECFM_PUT_2BYTE (pu1TempOrgSpecTlv, u2OrgSpecTlvLength);

    /* Put Org specific TLV's OUI field */
    ECFM_MEMCPY (pu1TempOrgSpecTlv, pLtrNode->OrgSpecTlv.au1Oui,
                 ECFM_OUI_LENGTH);
    pu1TempOrgSpecTlv = pu1TempOrgSpecTlv + ECFM_OUI_LENGTH;

    /* Put Org specific TLV's subtype field */
    ECFM_PUT_1BYTE (pu1TempOrgSpecTlv, pLtrNode->OrgSpecTlv.u1SubType);

    /* Put Org specific TLV's value field */
    ECFM_MEMCPY (pu1TempOrgSpecTlv, pLtrNode->OrgSpecTlv.Value.pu1Octets,
                 pLtrNode->OrgSpecTlv.Value.u4OctLen);

    /* Put Org specific TLV's length */
    pRetValDot1agCfmLtrOrganizationSpecificTlv->i4_Length =
        ECFM_TLV_LENGTH_FIELD_SIZE + u2OrgSpecTlvLength;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1agCfmMepDbTable. */

/****************************************************************************
 Function    :  EcfmMepUtlValAgMepDbTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

PUBLIC INT1
EcfmMepUtlValAgMepDbTable (UINT4 u4Dot1agCfmMdIndex,
                           UINT4 u4Dot1agCfmMaIndex,
                           UINT4 u4Dot1agCfmMepIdentifier,
                           UINT4 u4Dot1agCfmMepDbRMepIdentifier)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get Remote Mep entry corresponding to MdIndex,MaIndex,MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    /* check if RMep entry exists or not */
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetNxtIdxAgMepDbTable
 Input       :  The Indices
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                nextDot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier
                nextDot1agCfmMepDbRMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
PUBLIC INT1
EcfmMepUtlGetNxtIdxAgMepDbTable (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 *pu4NextDot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 *pu4NextDot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4 *pu4NextDot1agCfmMepIdentifier,
                                 UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                 UINT4 *pu4NextDot1agCfmMepDbRMepIdentifier)
{
    tEcfmCcRMepDbInfo   RMepInfo;
    tEcfmCcRMepDbInfo  *pRMepNextNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);
    RMepInfo.u4MdIndex = u4Dot1agCfmMdIndex;
    RMepInfo.u4MaIndex = u4Dot1agCfmMaIndex;
    RMepInfo.u2MepId = (UINT2) u4Dot1agCfmMepIdentifier;
    RMepInfo.u2RMepId = (UINT2) u4Dot1agCfmMepDbRMepIdentifier;

    pRMepNextNode = (tEcfmCcRMepDbInfo *) RBTreeGetNext
        (ECFM_CC_RMEP_TABLE, (tRBElem *) & RMepInfo, NULL);

    if (pRMepNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set next indices from corresponding RMep Entry */
    *pu4NextDot1agCfmMdIndex = pRMepNextNode->u4MdIndex;
    *pu4NextDot1agCfmMaIndex = pRMepNextNode->u4MaIndex;
    *pu4NextDot1agCfmMepIdentifier = (UINT4) (pRMepNextNode->u2MepId);
    *pu4NextDot1agCfmMepDbRMepIdentifier = (UINT4) (pRMepNextNode->u2RMepId);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbRMepState
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbRMepState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbRMepState (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                               INT4 *pi4RetValDot1agCfmMepDbRMepState)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Remote Mep state from corresponding RMep Entry */
    switch (pRMepNode->u1State)
    {
        case ECFM_RMEP_STATE_DEFAULT:
            *pi4RetValDot1agCfmMepDbRMepState = ECFM_RMEP_IDLE;
            break;
        case ECFM_RMEP_STATE_START:
            *pi4RetValDot1agCfmMepDbRMepState = ECFM_RMEP_START;
            break;
        case ECFM_RMEP_STATE_FAILED:
            *pi4RetValDot1agCfmMepDbRMepState = ECFM_RMEP_FAILED;
            break;
        case ECFM_RMEP_STATE_OK:
            *pi4RetValDot1agCfmMepDbRMepState = ECFM_RMEP_OK;
            break;
        default:
            *pi4RetValDot1agCfmMepDbRMepState = ECFM_RMEP_IDLE;
            break;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbRMepFailOkTime
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbRMepFailedOkTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbRMepFailOkTime (UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    UINT4 u4Dot1agCfmMepIdentifier,
                                    UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                    UINT4
                                    *pu4RetValDot1agCfmMepDbRMepFailedOkTime)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    /*Get if an entry found */
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set FailedOk Time from corresponding RMep Entry */
    *pu4RetValDot1agCfmMepDbRMepFailedOkTime = pRMepNode->u4FailedOkTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbMacAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbMacAddress (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                tMacAddr * pRetValDot1agCfmMepDbMacAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set RMep Mac Address from corresponding RMep Entry */
    ECFM_MEMCPY (pRetValDot1agCfmMepDbMacAddress,
                 pRMepNode->RMepMacAddr, ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbRdi
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbRdi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbRdi (UINT4 u4Dot1agCfmMdIndex, UINT4 u4Dot1agCfmMaIndex,
                         UINT4 u4Dot1agCfmMepIdentifier,
                         UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                         INT4 *pi4RetValDot1agCfmMepDbRdi)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Rdi bit status from corresponding RMep Entry */
    if (pRMepNode->b1LastRdi == ECFM_FALSE)
    {
        *pi4RetValDot1agCfmMepDbRdi = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValDot1agCfmMepDbRdi = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbPortStatTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbPortStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbPortStatTlv (UINT4 u4Dot1agCfmMdIndex,
                                 UINT4 u4Dot1agCfmMaIndex,
                                 UINT4 u4Dot1agCfmMepIdentifier,
                                 UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                 INT4 *pi4RetValDot1agCfmMepDbPortStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Port status Tlv from corresponding RMep Entry */
    *pi4RetValDot1agCfmMepDbPortStatusTlv =
        (INT4) (pRMepNode->u1LastPortStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbInterfaceStatTlv
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbInterfaceStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbInterfaceStatTlv (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                      INT4
                                      *pi4RetValDot1agCfmMepDbInterfaceStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Interface status Tlv from corresponding RMep Entry */
    *pi4RetValDot1agCfmMepDbInterfaceStatusTlv =
        (INT4) (pRMepNode->u1LastInterfaceStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbChassisIdSubtype
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbChassisIdSubtype (UINT4 u4Dot1agCfmMdIndex,
                                      UINT4 u4Dot1agCfmMaIndex,
                                      UINT4 u4Dot1agCfmMepIdentifier,
                                      UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                      INT4
                                      *pi4RetValDot1agCfmMepDbChassisIdSubtype)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set ChassisId type from corresponding RMep Entry */
    pSenderId = &(pRMepNode->LastSenderId);

    *pi4RetValDot1agCfmMepDbChassisIdSubtype =
        (INT4) (pSenderId->u1ChassisIdSubType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbChassisId
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbChassisId (UINT4 u4Dot1agCfmMdIndex,
                               UINT4 u4Dot1agCfmMaIndex,
                               UINT4 u4Dot1agCfmMepIdentifier,
                               UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValDot1agCfmMepDbChassisId)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set ChassisId from corresponding RMep Entry */
    pSenderId = &(pRMepNode->LastSenderId);
    /* Check if valid CCM recieved */
    if ((pSenderId->ChassisId.pu1Octets != NULL) &&
        (pSenderId->ChassisId.u4OctLen != 0))

    {
        pRetValDot1agCfmMepDbChassisId->i4_Length = (INT4)
            (pSenderId->ChassisId.u4OctLen);
        ECFM_MEMCPY (pRetValDot1agCfmMepDbChassisId->pu1_OctetList,
                     pSenderId->ChassisId.pu1Octets,
                     pSenderId->ChassisId.u4OctLen);
    }
    else
    {
        pRetValDot1agCfmMepDbChassisId->i4_Length = 1;
        pRetValDot1agCfmMepDbChassisId->pu1_OctetList[0] = '\0';
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbManAddrDom
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbManAddrDom (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                tSNMP_OID_TYPE *
                                pRetValDot1agCfmMepDbManAddressDomain)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Mgt Agddress domain from corresponding RMep Entry */
    pSenderId = &(pRMepNode->LastSenderId);

    /* Check if valid CCM recieved */
    if ((pSenderId->MgmtAddressDomain.pu1Octets != NULL) &&
        (pSenderId->MgmtAddressDomain.u4OctLen != 0))
    {
        EcfmUtilDecodeAsn1BER (&
                               (pSenderId->MgmtAddressDomain.
                                pu1Octets[ECFM_VAL_2]),
                               pSenderId->MgmtAddressDomain.u4OctLen -
                               ECFM_VAL_2,
                               pSenderId->MgmtAddressDomain.pu1Octets[1],
                               pRetValDot1agCfmMepDbManAddressDomain->
                               pu4_OidList,
                               &(pRetValDot1agCfmMepDbManAddressDomain->
                                 u4_Length));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmMepUtlGetAgMepDbManAddress
 Input       :  The Indices
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
                Dot1agCfmMepIdentifier
                Dot1agCfmMepDbRMepIdentifier

                The Object 
                retValDot1agCfmMepDbManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PUBLIC INT1
EcfmMepUtlGetAgMepDbManAddress (UINT4 u4Dot1agCfmMdIndex,
                                UINT4 u4Dot1agCfmMaIndex,
                                UINT4 u4Dot1agCfmMepIdentifier,
                                UINT4 u4Dot1agCfmMepDbRMepIdentifier,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1agCfmMepDbManAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        u4Dot1agCfmMepIdentifier,
                                        u4Dot1agCfmMepDbRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry exist for given Indices\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Mgt Agddress domain from corresponding RMep Entry */
    pSenderId = &(pRMepNode->LastSenderId);

    /* Check if valid CCM recieved */
    if ((pSenderId->MgmtAddress.pu1Octets != NULL) &&
        (pSenderId->MgmtAddress.u4OctLen != 0))

    {
        pRetValDot1agCfmMepDbManAddress->i4_Length =
            pSenderId->MgmtAddress.u4OctLen;
        ECFM_MEMCPY (pRetValDot1agCfmMepDbManAddress->pu1_OctetList,
                     pSenderId->MgmtAddress.pu1Octets,
                     pSenderId->MgmtAddress.u4OctLen);
    }
    else
    {
        pRetValDot1agCfmMepDbManAddress->i4_Length = 1;
        pRetValDot1agCfmMepDbManAddress->pu1_OctetList[0] = '\0';
    }
    return SNMP_SUCCESS;
}

/******************************************************************************/
/*                           End  of file cfmmeputil.c                        */
/******************************************************************************/
