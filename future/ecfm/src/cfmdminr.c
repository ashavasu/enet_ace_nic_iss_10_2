/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmdminr.c,v 1.20 2016/07/25 07:25:11 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way and 2 
 *              way Delay Mesaurement of Control Sub Module.
 *******************************************************************/
#include "cfminc.h"
PRIVATE INT4 EcfmDmInrCalcDiffInTimeRep PROTO ((tEcfmTimeRepresentation *,
                                                tEcfmTimeRepresentation *,
                                                tEcfmTimeRepresentation *));
PRIVATE INT4 Ecfm1DmInrCalcDiffInTimeRep PROTO ((tEcfmTimeRepresentation *,
                                                 tEcfmTimeRepresentation *,
                                                 tEcfmTimeRepresentation *));
PRIVATE tEcfmLbLtFrmDelayBuff *EcfmDmInrGetDmEntry
PROTO ((tEcfmLbLtPduSmInfo *));
PRIVATE VOID EcfmDmCalcFrameDelayVariation PROTO ((tEcfmLbLtMepInfo *,
                                                   tEcfmLbLtFrmDelayBuff *));

/*******************************************************************************
 * Function Name      : EcfmLbLtClntParseDmr
 *
 * Description        : This is called to parse the received DMR PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      pu1Pdu - Pointer to the received PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntParseDmr (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRxDmrPduInfo *pDmrPduInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pDmrPduInfo = &(pPduSmInfo->uPduInfo.Dmr);
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Copy the TxTimeStampf */
    ECFM_GET_4BYTE (pDmrPduInfo->TxTimeStampf.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (pDmrPduInfo->TxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Copy the RxTimeStampf */
    ECFM_GET_4BYTE (pDmrPduInfo->RxTimeStampf.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (pDmrPduInfo->RxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Copy the TxTimeStampb */
    ECFM_GET_4BYTE (pDmrPduInfo->TxTimeStampb.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (pDmrPduInfo->TxTimeStampb.u4NanoSeconds, pu1Pdu);

    /* Copy the Locally filled RxTimeStampb */
    ECFM_GET_4BYTE (pDmrPduInfo->RxTimeStampb.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (pDmrPduInfo->RxTimeStampb.u4NanoSeconds, pu1Pdu);

    /* Check if RxTimeStampb was filled by the HW on reception */
    if ((pDmrPduInfo->RxTimeStampb.u4Seconds == ECFM_INIT_VAL) &&
        (pDmrPduInfo->RxTimeStampb.u4NanoSeconds == ECFM_INIT_VAL))
    {
        /* RxTimeStampb was not filled by the HW */
        /* Concider this time as the reception time */
        EcfmLbLtUtilDmGetCurrentTime (&(pDmrPduInfo->RxTimeStampb));
    }
    /* Check that First TLV Offset value received in the LBR PDU should be
     * greater or equal to 4 */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_DMR_FIRST_TLV_OFFSET)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParserDmr: DMR frame is discarded as the"
                       " first tlv offset if wrong is\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbltClntProcessDmr
 *
 * Description        : This routine processes the received DMR PDU. 
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbltClntProcessDmr (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tUtlSysPreciseTime  SysPreciseTime;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNodeTmp = NULL;
    tEcfmLbLtRxDmrPduInfo *pDmrPduInfo = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    tEcfmMepInfoParams  MepInfo;
    UINT4               u4FrmDelayInUsec = ECFM_INIT_VAL;
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };
    tEcfmTimeRepresentation ProcessingTime;
    MEMSET (&ProcessingTime, ECFM_INIT_VAL, sizeof (ProcessingTime));

    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pDmrPduInfo = &(pPduSmInfo->uPduInfo.Dmr);
    pDmInfo = &(pMepInfo->DmInfo);

    if (pPduSmInfo->pPortInfo->u1IfType != CFA_MPLS)
    {
        /* Get the MEP's MAC Address */
        if (ECFM_LBLT_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessDmr: "
                           "dNo Port Information is present\r\n");
            return ECFM_FAILURE;
        }
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO (pMepInfo->u2PortNum)->
                                   u4IfIndex, MepMacAddr);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

        /* Check if the Mac Address received in the DMR is same as that of the
         * receiving MEP
         */
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                                   MepMacAddr) != ECFM_SUCCESS)
        {
            /* Discard the DMR frame */
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessDmr: "
                           "discarding the received DMR frame\r\n");
            return ECFM_FAILURE;
        }
    }

    /* Check the state of DM Transaction */
    if (pMepInfo->DmInfo.u1DmStatus != ECFM_TX_STATUS_NOT_READY)
    {
        /* Discard the DMM frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessDmr: "
                       "discarding the received DMR frame, there is no on going"
                       " DM transaction for this MEP \r\n");
        return ECFM_FAILURE;
    }

    /*Get the entry for the correspond DMM transmitted */
    pFrmDelayBuffNode = EcfmDmInrGetDmEntry (pPduSmInfo);
    if (pFrmDelayBuffNode == NULL)
    {
        /* Out of sequence DMR received, DMR frame will be discarded */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessDmr: "
                       "discarding the received DMR frame as the corresponding"
                       " DMM entry is not found\r\n");
        return ECFM_FAILURE;
    }
    /* Fill the current time from epoch as the mesaurement time */
    UtlGetPreciseSysTime (&SysPreciseTime);
    pFrmDelayBuffNode->MeasurementTimeStamp = SysPreciseTime.u4Sec;

    if (pMepInfo->DmInfo.u1TxDmType == ECFM_LBLT_DM_TYPE_DMM)
    {
        /* Update the Number of DMR received counter */
        ECFM_LBLT_INCR_DMR_RCVD_COUNT (pMepInfo);
        pFrmDelayBuffNodeTmp = pFrmDelayBuffNode;

        /* Traverse all the nodes after this node in the current transaction & 
         * update the number of DMR received counter. 
         * This is done so to handle the case of Wrapping */
        do
        {
            pFrmDelayBuffNodeTmp->u2NoOfDMRRcvd = pMepInfo->DmInfo.u2NoOfDmrIn;
        }
        while ((pFrmDelayBuffNodeTmp =
                EcfmGetNextFrmDelayNodeForATrans (pFrmDelayBuffNodeTmp)) !=
               NULL);
    }

    /* Calculate the Frame Delay Value, first we will calculate the frame delay
     * value plus processing time*/
    /* Frame Delay = (RxTimeStampb - TxTimeStampf) */
    if (EcfmDmInrCalcDiffInTimeRep (&(pDmrPduInfo->RxTimeStampb),
                                    &(pFrmDelayBuffNode->TxTimeStampf),
                                    &(pFrmDelayBuffNode->FrmDelayValue))
        != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessDmr: "
                       "discarding the received DMR frame, "
                       "invalid timestamps received\r\n");
        return ECFM_FAILURE;
    }
    /* Check if processing time is included in DMR, ie TxTimeStamb and
     * RxTimeStampf are non zero*/
    if (((pDmrPduInfo->TxTimeStampb.u4Seconds != ECFM_INIT_VAL) ||
         (pDmrPduInfo->TxTimeStampb.u4NanoSeconds != ECFM_INIT_VAL)) &&
        ((pDmrPduInfo->RxTimeStampf.u4Seconds != ECFM_INIT_VAL) ||
         (pDmrPduInfo->RxTimeStampf.u4NanoSeconds != ECFM_INIT_VAL)))
    {
        /* Calculate the Processing Time */
        /* Processing Time = (TxTimeStampb - RxTimeStampf) */
        if (EcfmDmInrCalcDiffInTimeRep (&(pDmrPduInfo->TxTimeStampb),
                                        &(pDmrPduInfo->RxTimeStampf),
                                        &(ProcessingTime)) == ECFM_SUCCESS)
        {
            /* substract the processing time from the calculate frame delay
             * ie Actual Frame Delay Value = Calculate Frame Delay - Processing
             * Time*/
            if (EcfmDmInrCalcDiffInTimeRep (&(pFrmDelayBuffNode->FrmDelayValue),
                                            &(ProcessingTime),
                                            &(pFrmDelayBuffNode->FrmDelayValue))
                != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbltClntProcessDmr: "
                               "processing time cannot be substracted from"
                               "calculated frame delay, invalid timestamps"
                               "received\r\n");
            }
        }
        else
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessDmr: "
                           "processing time not calculated, "
                           "invalid timestamps received\r\n");
        }

    }
    /* Raise Trap if Delay Value is greater than provided threshold */
    /* Convert the Frame Delay value into microseconds */
    u4FrmDelayInUsec =
        pFrmDelayBuffNode->FrmDelayValue.u4Seconds * ECFM_NUM_OF_NSEC_IN_A_SEC;
    u4FrmDelayInUsec =
        u4FrmDelayInUsec + pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds;
    u4FrmDelayInUsec = u4FrmDelayInUsec / ECFM_NUM_OF_NSEC_IN_A_USEC;
    if (u4FrmDelayInUsec > pDmInfo->u4FrmDelayThreshold)
    {
        /* Generate the required trap */
        Y1731_LBLT_GENERATE_TRAP (pFrmDelayBuffNode, Y1731_FRM_DELAY_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;
        MepInfo.u4IfIndex =
            ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;

        /* Notify the registered applications that Delay has
         * exceeded threshold 
         */
        ECFM_NOTIFY_PROTOCOLS (ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD,
                               &MepInfo, NULL, u4FrmDelayInUsec,
                               ECFM_INIT_VAL, ECFM_INIT_VAL,
                               NULL, ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_LBLT_TASK_ID);
    }
    ECFM_LBLT_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                        "EcfmLbltClntProcessDmr: "
                        " values in fd buffer timestamp=%x, "
                        "frm-delay-seconds=%x, frm-delay-nanseconds=%x \r\n",
                        pFrmDelayBuffNode->MeasurementTimeStamp,
                        pFrmDelayBuffNode->FrmDelayValue.u4Seconds,
                        pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds);
    /* Calculate the Frame Delay Variation */
    EcfmDmCalcFrameDelayVariation (pMepInfo, pFrmDelayBuffNode);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLbLtClntParse1Dm
 *
 * Description        : This is called to parse the received 1DM PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      pu1Pdu - Pointer to the received PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntParse1Dm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRx1DmPduInfo *p1DmPduInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    p1DmPduInfo = &(pPduSmInfo->uPduInfo.OneDm);
    ECFM_MEMSET (p1DmPduInfo, ECFM_INIT_VAL, sizeof (tEcfmLbLtRx1DmPduInfo));
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    /* Copy the TxTimeStampf */
    ECFM_GET_4BYTE (p1DmPduInfo->TxTimeStampf.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (p1DmPduInfo->TxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Copy the RxTimeStampf */
    ECFM_GET_4BYTE (p1DmPduInfo->RxTimeStampf.u4Seconds, pu1Pdu);
    ECFM_GET_4BYTE (p1DmPduInfo->RxTimeStampf.u4NanoSeconds, pu1Pdu);
    /* Check if RxTimeStampb was filled by the HW on reception */
    if ((p1DmPduInfo->RxTimeStampf.u4Seconds == ECFM_INIT_VAL) &&
        (p1DmPduInfo->RxTimeStampf.u4NanoSeconds == ECFM_INIT_VAL))
    {
        /* RxTimeStampf was not filled by the HW 
         * Concider this time as the reception time 
         */
        EcfmLbLtUtilDmGetCurrentTime (&(p1DmPduInfo->RxTimeStampf));
    }
    /* Check that First TLV Offset value received in the 1DM PDU should be 
     * equal to 16
     */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_1DM_FIRST_TLV_OFFSET)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParser1Dm: 1DM frame is discarded as the"
                       " first tlv offset is wrong \r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbltClntProcess1Dm
 *
 * Description        : This routine processes the received 1DM PDU.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwd1Dm - Boolean indicating if the PDU needs to be
 *                      forwarded in case of MIP
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbltClntProcess1Dm (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pbFrwd1Dm)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;
    tEcfmLbLtRx1DmPduInfo *p1DmPduInfo = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmMepInfoParams  MepInfo;
    tEcfmTimeRepresentation FrameDelayValue;
    tUtlSysPreciseTime  SysPreciseTime;
    UINT4               u4FrmDelayInUsec = ECFM_INIT_VAL;
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT1               u1SelectorType = ECFM_INIT_VAL;
    INT4                i4OneDmTransInterval = ECFM_INIT_VAL;

    MEMSET (&FrameDelayValue, ECFM_INIT_VAL, sizeof (FrameDelayValue));
    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    p1DmPduInfo = &(pPduSmInfo->uPduInfo.OneDm);
    pDmInfo = &(pMepInfo->DmInfo);
    pStackInfo = pPduSmInfo->pStackInfo;

    if (pMepInfo != NULL)
    {
        u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;
    }

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) != ECFM_TRUE)
    {
        /* Get the MEP's MAC Address */
        if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcess1Dm: "
                           "No Port Information is present\r\n");
            return ECFM_FAILURE;
        }
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO (pStackInfo->u2PortNum)->
                                   u4IfIndex, MepMacAddr);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
        /* Check if the Mac Address received in the 1DM is same as that of the
         * receiving MEP
         */
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                                   MepMacAddr) != ECFM_SUCCESS)
        {
            /* Discard the 1DM frame */
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcess1Dm: "
                           "discarding the received 1DM frame. The destination address"
                           "is not the same as of the receiving MEP\r\n");
            if (ECFM_LBLT_IS_MHF (pStackInfo))
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbltClntProcess1Dm: "
                               "1DM forwarded in case of MIP\r\n");
                *pbFrwd1Dm = ECFM_TRUE;
            }
            return ECFM_FAILURE;
        }
        if (ECFM_LBLT_IS_MHF (pStackInfo))
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcess1Dm: "
                           "discarding the received 1DM frame\r\n");
            return ECFM_FAILURE;
        }
    }

    if (pMepInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcess1Dm: "
                       "MEP is not found, discarding the received 1DM frame\r\n");
        return ECFM_FAILURE;
    }

    pMepInfo->DmInfo.u1TxDmType = ECFM_LBLT_DM_TYPE_1DM;
    ECFM_MEMCPY (pMepInfo->DmInfo.TxDmDestMacAddr,
                 pPduSmInfo->RxSrcMacAddr, ECFM_MAC_ADDR_LENGTH);
    /* Check the state of DM Transaction */
    if (pMepInfo->DmInfo.u1Rcv1DmCapability != ECFM_1DM_RCV_CAPABILITY_ENABLED)
    {
        /* Discard the 1DM frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcess1Dm: "
                       "discarding the received 1DM frame. 1DM receiving "
                       " capability is not enabled for this MEP \r\n");
        return ECFM_FAILURE;
    }
    /* Calculate the Frame Delay Value, */
    /* Frame Delay = (RxTimeStampb - TxTimeStampf) */
    if (Ecfm1DmInrCalcDiffInTimeRep (&(p1DmPduInfo->RxTimeStampf),
                                     &(p1DmPduInfo->TxTimeStampf),
                                     &FrameDelayValue) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcess1Dm: "
                       "discarding the received 1DM frame, invalid timestamps received\r\n");
        return ECFM_FAILURE;
    }
    /* If the 1DM transaction interval is configured, then Frame Delay Variation
     * can be calculated. Check if the received PDU is part of a new transaction
     * in that case
     */
    if (pDmInfo->u2OneDmTransInterval != ECFM_INIT_VAL)
    {
        /* If the 1DM Transaction timer is running, the received PDU is part of
         * the same transaction. Restart the timer in this case.
         */

        if (ECFM_GET_REMAINING_TIME
            (ECFM_LBLT_TMRLIST_ID,
             &(pMepInfo->DmInfo.OneDmTransIntervalTimer.TimerNode),
             &u4RemainingTime) != ECFM_SUCCESS)
        {
            u4RemainingTime = ECFM_INIT_VAL;
        }

        if (u4RemainingTime == ECFM_INIT_VAL)
        {
            /* The timer is not runnig. This marks the start of a new transaction */
            ECFM_LBLT_INCR_DM_TRANS_ID (pMepInfo);
        }
        if (pDmInfo->u2OneDmTransInterval < ECFM_NUM_OF_TICKS_IN_A_MSEC)
        {
            i4OneDmTransInterval = 0;
        }
        else

        {
            i4OneDmTransInterval = pDmInfo->u2OneDmTransInterval / ECFM_NUM_OF_TICKS_IN_A_MSEC;
        }
        if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_1DM_TRANS_INTERVAL,
                                   pMepInfo,
                                   ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC
                                   (i4OneDmTransInterval))
            != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmit1DmPdu: "
                           "Failure starting 1DM Transaction Interval timer\r\n");
            return ECFM_FAILURE;
        }
    }
    /* Convert the Frame Delay value into microseconds */
    u4FrmDelayInUsec = FrameDelayValue.u4Seconds * ECFM_NUM_OF_NSEC_IN_A_SEC;
    u4FrmDelayInUsec = u4FrmDelayInUsec + FrameDelayValue.u4NanoSeconds;
    u4FrmDelayInUsec = u4FrmDelayInUsec / ECFM_NUM_OF_NSEC_IN_A_USEC;

    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
    {

        /*Add Entry in the DM Buffer */
        pFrmDelayBuffNode = EcfmDmInitAddDmEntry (pMepInfo);
        if (pFrmDelayBuffNode == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmit1DmPdu: "
                           "Failure adding node in the frame delay buffer\r\n");
            return ECFM_FAILURE;
        }

        /* Store the Transmit time. Frame Delay and the Measurement Time
         * of the 1DM into the Frame delay buffer 
         */
        ECFM_COPY_TIME_REPRESENTATION (&(pFrmDelayBuffNode->TxTimeStampf),
                                       &(p1DmPduInfo->TxTimeStampf));
        ECFM_COPY_TIME_REPRESENTATION (&(pFrmDelayBuffNode->FrmDelayValue),
                                       &(FrameDelayValue));

        UtlGetPreciseSysTime (&SysPreciseTime);
        pFrmDelayBuffNode->MeasurementTimeStamp = SysPreciseTime.u4Sec;
        /* Raise Trap if Delay Value is greater than provided threshold */
        if (u4FrmDelayInUsec > pDmInfo->u4FrmDelayThreshold)
        {
            /* Generate the required trap */
            Y1731_LBLT_GENERATE_TRAP (pFrmDelayBuffNode,
                                      Y1731_FRM_DELAY_TRAP_VAL);

            MepInfo.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;
            MepInfo.u4IfIndex =
                ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
            /* Send the notification to the applications that have registered */
            ECFM_NOTIFY_PROTOCOLS (ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD,
                                   &MepInfo, NULL, u4FrmDelayInUsec,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                   ECFM_LBLT_TASK_ID);
        }
        ECFM_LBLT_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                            "EcfmLbltClntProcess1Dm: "
                            " values in fd buffer timestamp=%x, frm-delay-seconds=%x, frm-delay-nanseconds=%x \r\n",
                            pFrmDelayBuffNode->MeasurementTimeStamp,
                            pFrmDelayBuffNode->FrmDelayValue.u4Seconds,
                            pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds);

        /* Calculate the Frame Delay Variation */
        EcfmDmCalcFrameDelayVariation (pMepInfo, pFrmDelayBuffNode);
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmDmInrCalcDiffInTimeRep
 *
 * Description        : This routine is used to get the time difference between
 *                      two timestams. 
 *
 * Input(s)           : pTimeOp1 - Pointer to TimeStamp operand one
 *                      pTimeOp2 -Pointer to  TimeStamp operand two 
 *                      
 * Output(s)          : pTimeResult - Pointer to the resultant timestamp
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmDmInrCalcDiffInTimeRep (tEcfmTimeRepresentation * pTimeOp1,
                            tEcfmTimeRepresentation * pTimeOp2,
                            tEcfmTimeRepresentation * pTimeResult)
{
    INT4                i4NanoSeconds = ECFM_INIT_VAL;
    UINT4               u4StoredTimeResult = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    /* Check for invalid timestamp values */
    if (pTimeOp1->u4Seconds < pTimeOp2->u4Seconds)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInrCalcDiffInTimeRep: "
                       " invalid seconds value in operands\r\n");
        return ECFM_FAILURE;
    }
    /* Store the Seconds value in the output timestamp to that it can be reverted
     * back in case of failure*/
    u4StoredTimeResult = pTimeResult->u4Seconds;
    pTimeResult->u4Seconds = pTimeOp1->u4Seconds - pTimeOp2->u4Seconds;
    i4NanoSeconds = pTimeOp1->u4NanoSeconds - pTimeOp2->u4NanoSeconds;
    /* Check if wrap wround has happened in the time stamps */
    if (i4NanoSeconds < ECFM_INIT_VAL)
    {
        /* Check if we have enough seconds to take the offset */
        if (pTimeResult->u4Seconds == ECFM_INIT_VAL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInrCalcDiffInTimeRep: "
                           " invalid seconds value in operands\r\n");
            /* Revert back the no of seconds the the output timestamp */
            pTimeResult->u4Seconds = u4StoredTimeResult;
            return ECFM_FAILURE;
        }
        /* Decrement One Second from the Seconds field */
        pTimeResult->u4Seconds--;
        pTimeResult->u4NanoSeconds =
            (ECFM_NUM_OF_NSEC_IN_A_SEC + i4NanoSeconds);
    }
    else
    {
        pTimeResult->u4NanoSeconds = (UINT4) i4NanoSeconds;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function           : EcfmDmInrGetDmEntry
 *
 * Description        : This routine is used to get the DMM node for the DMR
 *                      received.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                        
 * Output(s)          : None
 *
 * Returns            : tEcfmLbLtFrmDelayBuff * - Pointer to the node
 ******************************************************************************/
PRIVATE tEcfmLbLtFrmDelayBuff *
EcfmDmInrGetDmEntry (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtFrmDelayBuff FrmDelyBuffNode;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelyBuffEntry = NULL;
    tEcfmLbLtRxDmrPduInfo *pDmrPduInfo = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pDmrPduInfo = &(pPduSmInfo->uPduInfo.Dmr);
    pDmInfo = &(pMepInfo->DmInfo);
    /* We will first try to get the entry corresponding to current transaction ID
     * and the sequence no*/
    ECFM_MEMSET (&FrmDelyBuffNode, ECFM_INIT_VAL,
                 sizeof (tEcfmLbLtFrmDelayBuff));
    FrmDelyBuffNode.u4MdIndex = pMepInfo->u4MdIndex;
    FrmDelyBuffNode.u4MaIndex = pMepInfo->u4MaIndex;
    FrmDelyBuffNode.u2MepId = pMepInfo->u2MepId;
    FrmDelyBuffNode.u4TransId = pDmInfo->u4CurrTransId;
    FrmDelyBuffNode.u4SeqNum = pDmInfo->u4TxDmSeqNum - ECFM_DECR_VAL;
    pFrmDelyBuffEntry =
        (tEcfmLbLtFrmDelayBuff *) RBTreeGet (ECFM_LBLT_FD_BUFFER_TABLE,
                                             (tRBElem *) & FrmDelyBuffNode);
    if (pFrmDelyBuffEntry != NULL)
    {
        /* Entry found in the Frame Delay buffer for the current transaction and
         * sequence no*/
        /* Check if the entry found corresponds to the DMR received */
        if (ECFM_MEMCMP (&(pFrmDelyBuffEntry->TxTimeStampf),
                         &(pDmrPduInfo->TxTimeStampf),
                         sizeof (tEcfmTimeRepresentation)) == 0)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                           "EcfmDmInrGetDmEntry: "
                           "entry found in the frame delay buffer with the timestamp"
                           " received in DMR.\r\n");
            return pFrmDelyBuffEntry;
        }
    }
    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                   "EcfmDmInrGetDmEntry: "
                   "entry not found in the frame delay, going to search the whole"
                   " frame delay buffer\r\n");
    /* Now we have hit the worst case, DMR just received does'nt corresponds to
     * the last transmitted DMM. In this case we will have to search all the
     * frame delay nodes for the current transaction with the TxTimeStampf
     * received in the DMR*/
    /* Try to get the first entry for the current transaction */
    FrmDelyBuffNode.u4SeqNum = ECFM_INIT_VAL;
    pFrmDelyBuffEntry =
        (tEcfmLbLtFrmDelayBuff *) RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);

    while (pFrmDelyBuffEntry != NULL)
    {
        /* Check if the frame delay buffer node found is for the current on going
         * transaction
         */
        if (pFrmDelyBuffEntry->u4TransId == pDmInfo->u4CurrTransId)
        {
            /* Check if the entry found corresponds to the DMR received */
            if (ECFM_MEMCMP (&(pFrmDelyBuffEntry->TxTimeStampf),
                             &(pDmrPduInfo->TxTimeStampf),
                             sizeof (tEcfmTimeRepresentation)) == 0)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmDmInrGetDmEntry: "
                               "entry found in the frame delay buffer with the TimeStamp"
                               " received in DMR.\r\n");
                break;
            }
        }
        pFrmDelyBuffEntry =
            (tEcfmLbLtFrmDelayBuff *) RBTreeGetNext (ECFM_LBLT_FD_BUFFER_TABLE,
                                                     (tRBElem *)
                                                     pFrmDelyBuffEntry, NULL);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return pFrmDelyBuffEntry;
}

/*****************************************************************************
 * Function           : EcfmLbLtClntCalcFrameDelayVariation
 *
 * Description        : This routine calculates the following types of Frame 
 *                      Delay Variations:
 *                      1. Frame Delay Variation
 *                      2. Inter Frame Delay Variation
 *
 * Input(s)           : pMepInfo - Pointer to MEP info
 *                      pFrmDelayBuffNode - Pointer to Frame Delay Buffer Node 
 *                        
 * Output(s)          : None
 *
 * Returns            : None
 ******************************************************************************/
PRIVATE VOID
EcfmDmCalcFrameDelayVariation (tEcfmLbLtMepInfo * pMepInfo,
                               tEcfmLbLtFrmDelayBuff * pCurFrmDelayBuffNode)
{
    tEcfmLbLtFrmDelayBuff *pPrvFrmDelayBuffEntry = NULL;
    tEcfmTimeRepresentation FrameDelayVariation;
    tEcfmLbLtFrmDelayBuff PrevFrmDelayBuffNode;

    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_MEMSET (&FrameDelayVariation, ECFM_INIT_VAL,
                 sizeof (tEcfmTimeRepresentation));
    ECFM_MEMSET (&PrevFrmDelayBuffNode, ECFM_INIT_VAL,
                 sizeof (tEcfmLbLtFrmDelayBuff));
    PrevFrmDelayBuffNode.u4MdIndex = pCurFrmDelayBuffNode->u4MdIndex;
    PrevFrmDelayBuffNode.u4MaIndex = pCurFrmDelayBuffNode->u4MaIndex;
    PrevFrmDelayBuffNode.u2MepId = pCurFrmDelayBuffNode->u2MepId;
    PrevFrmDelayBuffNode.u4TransId = pCurFrmDelayBuffNode->u4TransId;
    PrevFrmDelayBuffNode.u4SeqNum = pCurFrmDelayBuffNode->u4SeqNum;
    /* We need to search for an entry which was stored just prior to this node
     * for the same transaction
     */
    PrevFrmDelayBuffNode.u4SeqNum = PrevFrmDelayBuffNode.u4SeqNum -
        ECFM_DECR_VAL;
    pPrvFrmDelayBuffEntry =
        (tEcfmLbLtFrmDelayBuff *) RBTreeGet (ECFM_LBLT_FD_BUFFER_TABLE,
                                             (tRBElem *) &
                                             PrevFrmDelayBuffNode);
    if (pPrvFrmDelayBuffEntry != NULL)
    {
        /* Entry exists for the same transaction ID. 
         * Calculate Frame Delay
         */
        if (pPrvFrmDelayBuffEntry->FrmDelayValue.u4Seconds >
            pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds)
        {
            EcfmDmInrCalcDiffInTimeRep (&(pPrvFrmDelayBuffEntry->FrmDelayValue),
                                        &(pCurFrmDelayBuffNode->FrmDelayValue),
                                        &FrameDelayVariation);
        }
        else if (pPrvFrmDelayBuffEntry->FrmDelayValue.u4Seconds ==
                 pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds)
        {
            if (pPrvFrmDelayBuffEntry->FrmDelayValue.u4NanoSeconds >
                pCurFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds)
            {

                EcfmDmInrCalcDiffInTimeRep (&
                                            (pPrvFrmDelayBuffEntry->
                                             FrmDelayValue),
                                            &(pCurFrmDelayBuffNode->
                                              FrmDelayValue),
                                            &FrameDelayVariation);
            }
            else
            {

                EcfmDmInrCalcDiffInTimeRep (&
                                            (pCurFrmDelayBuffNode->
                                             FrmDelayValue),
                                            &(pPrvFrmDelayBuffEntry->
                                              FrmDelayValue),
                                            &FrameDelayVariation);
            }
        }
        else
        {
            EcfmDmInrCalcDiffInTimeRep (&(pCurFrmDelayBuffNode->FrmDelayValue),
                                        &(pPrvFrmDelayBuffEntry->FrmDelayValue),
                                        &FrameDelayVariation);
        }
    }
    else
    {
        /* This is the first node that is getting added in the buffer
         * Frame Delay Variation cannot be calculated for this case.
         * Store the delay calculated as the Minimum Delay.
         */
        pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds =
            pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds;
        pMepInfo->DmInfo.MinFrameDelayValue.u4NanoSeconds =
            pCurFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds;
        return;
    }
    /* Store the value of Frame Delay Variation */
    ECFM_COPY_TIME_REPRESENTATION (&(pCurFrmDelayBuffNode->
                                     FrameDelayVariation),
                                   &FrameDelayVariation);

    /* Calculate  Inter Frame Delay Variation */
    if (pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds >
        pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds)
    {
        pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds =
            pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds;
        pMepInfo->DmInfo.MinFrameDelayValue.u4NanoSeconds =
            pCurFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds;
    }
    if (pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds ==
        pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds)
    {
        /* If its Nanoseconds value is also less or equal to the stored minimum
         * then this is the new minimum value. Otherwise the minimum remains the same
         */
        if (pCurFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds <=
            pMepInfo->DmInfo.MinFrameDelayValue.u4NanoSeconds)
        {

            pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds =
                pCurFrmDelayBuffNode->FrmDelayValue.u4Seconds;
            pMepInfo->DmInfo.MinFrameDelayValue.u4NanoSeconds =
                pCurFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds;
        }
    }
    EcfmDmInrCalcDiffInTimeRep (&(pCurFrmDelayBuffNode->FrmDelayValue),
                                &(pMepInfo->DmInfo.MinFrameDelayValue),
                                &(pCurFrmDelayBuffNode->
                                  InterFrmDelayVariation));
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function           : Ecfm1DmInrCalcDiffInTimeRep
 *
 * Description        : This routine is used to get the time difference between
 *                      two timestamps.This routine is used to carry out the
 *                      calculations when the clocks at the Tx and Rx end are
 *                      not in synch. 
 *
 * Input(s)           : pTimeOp1 - Pointer to TimeStamp operand one
 *                      pTimeOp2 -Pointer to  TimeStamp operand two 
 *                      
 * Output(s)          : pTimeResult - Pointer to the resultant timestamp
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
Ecfm1DmInrCalcDiffInTimeRep (tEcfmTimeRepresentation * pTimeOp1,
                             tEcfmTimeRepresentation * pTimeOp2,
                             tEcfmTimeRepresentation * pTimeResult)
{
    INT4                i4NanoSeconds = ECFM_INIT_VAL;
    UINT4               u4StoredTimeResult = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    /* Store the Seconds value in the output timestamp to that it can be reverted
     * back in case of failure*/
    u4StoredTimeResult = pTimeResult->u4Seconds;
    if (pTimeOp1->u4Seconds >= pTimeOp2->u4Seconds)
    {
        pTimeResult->u4Seconds = pTimeOp1->u4Seconds - pTimeOp2->u4Seconds;
        i4NanoSeconds = pTimeOp1->u4NanoSeconds - pTimeOp2->u4NanoSeconds;
    }
    else
    {
        pTimeResult->u4Seconds = pTimeOp2->u4Seconds - pTimeOp1->u4Seconds;
        i4NanoSeconds = pTimeOp2->u4NanoSeconds - pTimeOp1->u4NanoSeconds;
    }
    /* Check if wrap wround has happened in the time stamps */
    if (i4NanoSeconds < ECFM_INIT_VAL)
    {
        /* Check if we have enough seconds to take the offset */
        if (pTimeResult->u4Seconds == ECFM_INIT_VAL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "Ecfm1DmInrCalcDiffInTimeRep: "
                           " invalid seconds value in operands\r\n");
            /* Revert back the no of seconds the the output timestamp */
            pTimeResult->u4Seconds = u4StoredTimeResult;
            return ECFM_FAILURE;
        }
        /* Decrement One Second from the Seconds field */
        pTimeResult->u4Seconds--;
        pTimeResult->u4NanoSeconds =
            (ECFM_NUM_OF_NSEC_IN_A_SEC + i4NanoSeconds);
    }
    else
    {
        pTimeResult->u4NanoSeconds = (UINT4) i4NanoSeconds;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
  End of File cfmdminr.c
 ******************************************************************************/
