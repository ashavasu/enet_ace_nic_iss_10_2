/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlckin.c,v 1.11 2014/11/11 10:37:43 siva Exp $
 *
 * Description: This file contains the Functionality of the LCK 
 *              Control Sub Module.
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmCcInitFormatLckPdu PROTO ((tEcfmCcMepInfo *, UINT1 **));
PRIVATE INT4 EcfmCcInitXmitLckPdu PROTO ((tEcfmCcMepInfo *));
PRIVATE INT4 EcfmCcInitConstructLck PROTO ((tEcfmBufChainHeader * pBuf,
                                            tEcfmCcMepInfo * pMepInfo));
/*******************************************************************************
 * Function           : EcfmCcClntLckInitiator
 *
 * Description        : The routine implements the LCK Initiator, it calls up
 *                      routine to format and transmit LCK frame depending upon
 *                      the various events.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/

PUBLIC UINT4
EcfmCcClntLckInitiator (tEcfmCcMepInfo * pMepInfo, UINT1 u1EventID)
{
    tEcfmCcLckInfo     *pLckInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4RetVal = ECFM_SUCCESS;

    ECFM_CC_TRC_FN_ENTRY ();
    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;

    /* Check for the event received */
    switch (u1EventID)
    {
        case ECFM_EV_MEP_BEGIN:
            /* Check if Mep is configured for Out of Service on this 
             * interface then transmit LCK Pdu */
            if (pLckInfo->b1OutOfService == ECFM_TRUE)
            {
                /* Call the routine to transmit the LCK Packet */
                if (EcfmCcInitXmitLckPdu (pMepInfo) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                                 ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcClntLckInitiator: "
                                 "Unable to transmit LCK Pdu\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
                /* Start LCK Delay Timer */
                if (EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_DELAY, &PduSmInfo,
                                         (UINT4) (pLckInfo->u1LckDelay *
                                                  ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT))
                    != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                                 ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcClntLckInitiator: "
                                 "Unable to Start Delay Timer\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            break;
        case ECFM_EV_MEP_NOT_ACTIVE:
            /* Stop the LCK Transmission */
            EcfmCcInitStopLckTx (pMepInfo);
            /* Stop Lck Period Timer */
            EcfmCcTmrStopTimer (ECFM_CC_TMR_LCK_PERIOD, &PduSmInfo);
            break;
        case ECFM_LCK_START_TRANSACTION:
            /* Call the routine to transmit the LCK Packet */
            if (EcfmCcInitXmitLckPdu (pMepInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                             ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntLckInitiator: "
                             "Unable to transmit LCK Pdu\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
#ifdef L2RED_WANTED
            ECFM_GET_SYS_TIME (&pMepInfo->LckInfo.LckIntervalTimeStamp);
#endif
            /* b1LckDelayExp condition will be set when Delay timer 
             * expires */
            pLckInfo->b1LckDelayExp = ECFM_FALSE;

            /* Sync LCK Status with LBLT Task */
            EcfmLbLtConfLCKStatus (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                                   pMepInfo->u2MepId,
                                   pMepInfo->pPortInfo->u4ContextId,
                                   ECFM_FALSE);

            /* Start LCK Delay Timer */
            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_DELAY, &PduSmInfo,
                                     (UINT4) (pLckInfo->u1LckDelay *
                                              ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT))
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                             ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntLckInitiator: "
                             "Unable to Start Delay Timer\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            break;
        case ECFM_LCK_STOP_TRANSACTION:
            /* Call the routine to stop LCK transmission */
            EcfmCcInitStopLckTx (pMepInfo);
#ifdef L2RED_WANTED
            pMepInfo->LckInfo.LckIntervalTimeStamp = ECFM_INIT_VAL;
#endif
            break;
        case ECFM_EV_LCK_INTERVAL_EXPIRY:
            /* Call the routine to transmit the LCK Packet */
            if (EcfmCcInitXmitLckPdu (pMepInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                             ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntLckInitiator: "
                             "Unable to transmit LCK Pdu\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            break;
        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntLckInitiator: " "Event Not Supported\r\n");
            u4RetVal = ECFM_FAILURE;
            break;
    }
    if (u4RetVal == ECFM_FAILURE)
    {
        /* Stop the LCK Transaction */
        EcfmCcInitStopLckTx (pMepInfo);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitXmitLckPdu
 *
 * Description        : This routine formats and transmits the LCK PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmCcInitXmitLckPdu (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcLckInfo     *pLckInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum);
    PduSmInfo.pMepInfo = pMepInfo;
    do
    {
        pBuf = CRU_BUF_Allocate_MsgBufChain (ECFM_MAX_LCK_PDU_SIZE, 0);
        if (pBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcInitXmitLckPdu: "
                         "Buffer allocation failed\r\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            break;
        }
        if (pMepInfo->LckInfo.SavedLckPdu.pu1Octets != NULL)
        {
            if (ECFM_COPY_OVER_CRU_BUF
                (pBuf, pMepInfo->LckInfo.SavedLckPdu.pu1Octets, 0,
                 (UINT4) (pMepInfo->LckInfo.SavedLckPdu.u4OctLen)) !=
                ECFM_CRU_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcInitXmitLckPdu: "
                             "Buffer copy operation failed\r\n");
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                break;
            }
        }
        else
        {
            i4RetVal = EcfmCcInitConstructLck (pBuf, pMepInfo);
            if (i4RetVal == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                             ALL_FAILURE_TRC,
                             "EcfmCcInitXmitLckPdu: Unable to construct LCK\r\n");
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                break;
            }
        }

        ECFM_BUF_SET_LEVEL (pBuf, pMepInfo->u1MdLevel);
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                          "EcfmCcInitXmitLckPdu: "
                          "Sending out LCK-PDU to lower layer...\r\n");
        if (EcfmCcCtrlTxTransmitPkt
            (pBuf, pMepInfo->u2PortNum, pMepInfo->u4PrimaryVidIsid,
             pLckInfo->u1LckPriority, pLckInfo->b1LckDropEnable,
             pMepInfo->u1Direction, ECFM_OPCODE_LCK, NULL,
             NULL) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcInitXmitLckPdu: "
                         "LCK transmission to the lower layer failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            break;
        }
        if(pMepInfo->u1Direction == ECFM_MP_DIR_DOWN)
        {
            ECFM_CC_INCR_TX_CFM_PDU_COUNT (pMepInfo->u2PortNum);
            ECFM_CC_INCR_TX_COUNT (pMepInfo->u2PortNum, ECFM_BUF_GET_OPCODE (pBuf));
            ECFM_CC_INCR_CTX_TX_COUNT (PduSmInfo.pPortInfo->u4ContextId,
                                ECFM_BUF_GET_OPCODE (pBuf));
        }
    }
    while (0);
    /* Start LCK Interval Timer */
    ECFM_GET_AIS_LCK_INTERVAL (pLckInfo->u1LckInterval, u4Interval);
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_INTERVAL, &PduSmInfo,
                             u4Interval) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitXmitLckPdu: "
                     "Failure starting LCK Interval timer\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitConstructLck
 *
 * Description        : This rotuine is used to construct the new LCK CFM PDU.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : pBuf - Lck Pdu Buffer
 * 
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmCcInitConstructLck (tEcfmBufChainHeader * pBuf, tEcfmCcMepInfo * pMepInfo)
{
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1LckPduStart = NULL;
    UINT1              *pu1LckPduEnd = NULL;
    UINT1              *pu1LckPdu = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    ECFM_MEMSET (ECFM_CC_PDU, ECFM_INIT_VAL, ECFM_MAX_LCK_PDU_SIZE);

    pu1LckPdu = (UINT1 *) MemAllocMemBlk (ECFM_CC_LCK_PDU_POOL);
    if (pu1LckPdu == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitConstructLck: "
                     "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    pu1LckPduStart = ECFM_CC_PDU;
    pu1LckPduEnd = ECFM_CC_PDU;
    pu1EthLlcHdr = ECFM_CC_PDU;

    /* Format the  Lck PDU header */
    EcfmCcInitFormatLckPdu (pMepInfo, &pu1LckPduEnd);

    u2PduLength = (UINT2) (pu1LckPduEnd - pu1LckPduStart);

    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1LckPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitXmitLckPdu: "
                     "Buffer copy operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        MemReleaseMemBlock (ECFM_CC_LCK_PDU_POOL, (UINT1 *) pu1LckPdu);
        return ECFM_FAILURE;
    }
    pu1LckPduStart = pu1EthLlcHdr;
    EcfmFormatCcTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr, u2PduLength,
                               ECFM_OPCODE_LCK);

    /* Prepend the Ethernet and the LLC header in the
     * CRU buffer */
    u2PduLength = (UINT2) (pu1EthLlcHdr - pu1LckPduStart);
    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1LckPduStart,
                              (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitXmitLckPdu: "
                     "Buffer prepend operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        MemReleaseMemBlock (ECFM_CC_LCK_PDU_POOL, (UINT1 *) pu1LckPdu);
        return ECFM_FAILURE;
    }
    /* Set MD LEvel of the transmitting MEP in the Buffer's reserved fields as
     * it is required for Transmitting LCK PDU to all VLANs
     */
    ECFM_BUF_SET_LEVEL (pBuf, pMepInfo->u1MdLevel);

    u2PduLength = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CRU_BUF_Copy_FromBufChain (pBuf, pu1LckPdu, 0, u2PduLength);

    pMepInfo->LckInfo.SavedLckPdu.pu1Octets = pu1LckPdu;
    pMepInfo->LckInfo.SavedLckPdu.u4OctLen = u2PduLength;

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitFormatLckPdu
 *
 * Description        : This rotuine is used to fill the LCK CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1LckPdu - Pointer to Pointer to the LCK PDU.
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmCcInitFormatLckPdu (tEcfmCcMepInfo * pMepInfo, UINT1 **ppu1LckPdu)
{
    tEcfmCcMdInfo      *pMdInfo = NULL;
    UINT1              *pu1LckPdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    pu1LckPdu = *ppu1LckPdu;
    pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, (UINT1) pMdInfo->i1ClientLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1LckPdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1LckPdu, ECFM_OPCODE_LCK);

    /* Fill the Next 1 byte with Flag In LCK, Flag Field is set corresponding 
     * to interval */
    if (pMepInfo->LckInfo.u1LckInterval == ECFM_CC_AIS_LCK_INTERVAL_1_SEC)
    {
        ECFM_PUT_1BYTE (pu1LckPdu, ECFM_AIS_LCK_INTERVAL_1_S_FLAG);
    }
    else
    {
        ECFM_PUT_1BYTE (pu1LckPdu, ECFM_AIS_LCK_INTERVAL_1_MIN_FLAG);
    }

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1LckPdu, ECFM_INIT_VAL);

    /*Fill in the next 1 byte as End TLV */
    ECFM_PUT_1BYTE (pu1LckPdu, ECFM_INIT_VAL);
    *ppu1LckPdu = pu1LckPdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitStopLckTx
 *
 * Description        : This routine is used to stop the on going LCK Tx 
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcInitStopLckTx (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcLckInfo     *pLckInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;

    /* Stop Lck RxWhile Timer */
    EcfmCcClearLckCondition (pMepInfo);
    EcfmCcTmrStopTimer (ECFM_CC_TMR_LCK_RXWHILE, &PduSmInfo);

    /* Stop Lck Interval Timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_LCK_INTERVAL, &PduSmInfo);

    /* Stop LCK Delay Timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_LCK_DELAY, &PduSmInfo);

    /* Update LCK Delay expired bool */
    pLckInfo->b1LckDelayExp = ECFM_FALSE;

    /* Sync LCK Status with LBLT Task */
    EcfmLbLtConfLCKStatus (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                           pMepInfo->u2MepId,
                           pMepInfo->pPortInfo->u4ContextId, ECFM_FALSE);

    pPortInfo = ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum);
    if (pPortInfo == NULL)
    {
        return;
    }
    /* Call L2iwf API to update the LCK status in its database */
    EcfmSetPortLockStatus (pPortInfo->u4IfIndex,
                           pMepInfo->u4PrimaryVidIsid, ECFM_FALSE);

    if (pMepInfo->LckInfo.SavedLckPdu.pu1Octets != NULL)
    {
        MemReleaseMemBlock (ECFM_CC_LCK_PDU_POOL,
                            (UINT1 *) pMepInfo->LckInfo.SavedLckPdu.pu1Octets);
    }
    pMepInfo->LckInfo.SavedLckPdu.pu1Octets = NULL;
    pMepInfo->LckInfo.SavedLckPdu.u4OctLen = 0;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcLckPeriodConfiguration
 *
 * Description        : This routine is used to set the LCK Period
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcLckPeriodConfiguration (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcLckInfo     *pLckInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;
    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);

    /* Stop Lck Period Timer */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_LCK_PERIOD, &PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmCcLckPeriodConfiguration: "
                     "Lck Period Timer has not stopped\r\n");
        return ECFM_FAILURE;
    }

    /* Start the LCK Period Timer iff it is non-zero value */
    if (pLckInfo->u4LckPeriod != 0)
    {
        if (EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_PERIOD, &PduSmInfo,
                                 (UINT4) (pLckInfo->u4LckPeriod *
                                          ECFM_NUM_OF_MSEC_IN_A_SEC)) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "EcfmCcLckPeriodConfiguration: "
                         "Lck Period Timer has not started\r\n");
            return ECFM_FAILURE;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcLckPeriodTimeout
 *
 * Description        : This routine is used to toggle the LCK capability on the
 *                      expiry of Period Timer
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcLckPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcLckInfo     *pLckInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    /* Sync LCK Period Timeout at STANDBY Node */
    EcfmRedSyncLckPeriodTimeout (pMepInfo);

    /* Reset LCK Period value */
    pMepInfo->LckInfo.u4LckPeriod = ECFM_INIT_VAL;

    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);
    if (pLckInfo->b1OutOfService == ECFM_TRUE)
    {
        EcfmCcClntLckInitiator (pMepInfo, ECFM_LCK_STOP_TRANSACTION);
        /* Configure MEP In Service */
        pLckInfo->b1OutOfService = ECFM_FALSE;
    }
    else
    {
        /* Configure MEP Out Of Service */
        pLckInfo->b1OutOfService = ECFM_TRUE;
        if (EcfmCcClntLckInitiator (pMepInfo, ECFM_LCK_START_TRANSACTION) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcLckPeriodTimeout"
                         "Unable to start LCK Tx \r\n");
            return;
        }
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *   Function Name      : EcfmCcUpdateLckStatus
 *   Description        : This routine is used to update the Lck status
 *   Input(s)           : pMepInfo - Pointer to the structure that stores
 *                        the information regarding MEP info.
 *   Output(s)          : None
 *   Return Value(s)    : None
 *******************************************************************************/
PUBLIC VOID
EcfmCcUpdateLckStatus (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Update LCK Delay expired bool */
    pMepInfo->LckInfo.b1LckDelayExp = ECFM_TRUE;

    /* Sync LCK Status with LBLT Task */
    EcfmLbLtConfLCKStatus (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                           pMepInfo->u2MepId,
                           pMepInfo->pPortInfo->u4ContextId, ECFM_TRUE);

    pPortInfo = ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum);
    if (pPortInfo == NULL)
    {
        return;
    }
    /* Call L2iwf API to update the LCK status in its database */
    EcfmSetPortLockStatus (pPortInfo->u4IfIndex,
                           pMepInfo->u4PrimaryVidIsid, ECFM_TRUE);

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  End of File cfmlckini.c
 ******************************************************************************/
