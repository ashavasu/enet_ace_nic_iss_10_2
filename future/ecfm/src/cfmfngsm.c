/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmfngsm.c,v 1.18 2013/12/18 12:48:02 siva Exp $
 *
 * Description: This file contains the Functionality of the FNG 
 *              State Machine.
 *******************************************************************/

#include "cfminc.h"
#include "cfmfngsm.h"

/****************************************************************************
 * Function Name      : EcfmCcClntFngSm
 *
 * Description        : This is the FNG State Machine that traverses 
 *                      the state event matrix depending event given to it. 
 *
 * Input(s)           : u1Event - Specifying the event received by this 
 *                                state machine.
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PUBLIC VOID
EcfmCcClntFngSm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);

    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcClntFngSm: Called with Event: %d, and the current "
                      "State: %d\r\n",
                      u1Event, ECFM_CC_FNG_GET_STATE (pMepInfo));

    ECFM_CC_FNG_STATE_MACHINE (u1Event, pFngInfo->u1FngState, pPduSmInfo);
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFngSmSetStateReset
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      BEGIN in DEFAULT state the state machine is set to 
 *                      RESET state and then initialize the local variable
 *                      of state machine.This routine is called when state 
 *                      machine is in DEFECT state or in the Defect Clearing 
 *                      state and there is an event for NO_DEFECT_INDICATION 
 *                      indicating that CCM received is correct and fault 
 *                      generated previously can be removed.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmSetStateReset (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);

    pFngInfo->u1HighestDefectPri = ECFM_INIT_VAL;
    pFngInfo->u1HighestDefect = ECFM_INIT_VAL;
    pFngInfo->u1FngPriority = ECFM_INIT_VAL;
    pFngInfo->u1FngDefect = ECFM_INIT_VAL;
    pFngInfo->b1SomeRMepCcmDefect = ECFM_FALSE;
    pFngInfo->b1SomeMacStatusDefect = ECFM_FALSE;
    pFngInfo->b1SomeRdiDefect = ECFM_FALSE;
    pMepInfo->b1MaDefectIndication = ECFM_FALSE;
    pMepInfo->b1PresentRdi = ECFM_FALSE;
    /* Declare all the RMEPs as UP */
    pMepInfo->b1AllRMepsDead = ECFM_FALSE;
    /* Stop the timer if running */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_FNG_WHILE, pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmFngSmSetStateReset:CC Stop Timer FAILED\r\n");
    }

    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_FNG_RESET_WHILE,
                            pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmFngSmSetStateReset:CC Stop Timer FAILED\r\n");
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->FngInfo.u1FngState,
                               ECFM_MEP_FNG_STATE_RESET, pPduSmInfo);

    /*State set to Default */
    ECFM_CC_FNG_SET_STATE (pMepInfo, ECFM_MEP_FNG_STATE_RESET);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmFngSmSetStateReset:FNG SEM Moved to RESET state\r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFngSmSetStateDefect
 *
 * Description        : This routine is called when state machine is in RESET
 *                      state and there is an Indication of any defect.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmSetStateDefect (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    UINT4               u4FngTimer = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);
    u4FngTimer = ECFM_CC_CONVERT_FNG_TIMER_TO_MSEC (pFngInfo->u2FngAlarmTime);
    /* Start the FNG alarm timer for 2.5 sec */
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_FNG_WHILE, pPduSmInfo,
                             u4FngTimer) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmFngSmSetStateDefect:CC Start Timer FAILED\r\n");
        return;
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->FngInfo.u1FngState,
                               ECFM_MEP_FNG_STATE_DEFECT, pPduSmInfo);

    /* Set the State to Defect */
    ECFM_CC_FNG_SET_STATE (pMepInfo, ECFM_MEP_FNG_STATE_DEFECT);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmFngSmSetStateDefect: FNG SEM Moved to Defect state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return;

}

/****************************************************************************
 * Function Name      : EcfmFngSmSetStateRptDft
 *
 * Description        : This routine is called when state machine is in DEFECT
 *                      state and FNG Alarm Timeout event occurred. 
 * 
 * Input(s)           : pEcfmPduSmInfo . Pointer to the structure that stores 
 *                      the information regarding mp info, the PDU if received 
 *                      and other information related to the state machine.
 * 
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmSetStateRptDft (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);
    pFngInfo->u1FngDefect = pFngInfo->u1HighestDefect;
    pFngInfo->u1FngPriority = pFngInfo->u1HighestDefectPri;
    /* Generate the SNMP trap for the fault */
    ECFM_CC_GENERATE_TRAP (pPduSmInfo, ECFM_FAULT_ALARM_TRAP_VAL);

    EcfmRedCheckAndSyncSmData (&pMepInfo->FngInfo.u1FngState,
                               ECFM_MEP_FNG_STATE_DEFECT_REPORTED, pPduSmInfo);

    /* State set to Defect Reported unconditionally */
    ECFM_CC_FNG_SET_STATE (pMepInfo, ECFM_MEP_FNG_STATE_DEFECT_REPORTED);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmFngSmSetStateDftRptd: FNG SEM Moved to Defect Reported"
                 "state \r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFngSmSetStateRptDftFrmRptd
 *
 * Description        : This routine is called when state machine is in DEFECT
 *                      state and Timeout event occurred and fault has 
 *                      occurred for which fault alarm has to be generated.
 *                      This routine is called when state machine is in
 *                      DefectReported state and there is another fault 
 *                      indication for new Defect.
 * 
 * Input(s)           : pEcfmPduSmInfo . Pointer to the structure that stores 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 * 
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmSetStateRptDftFrmRptd (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);
    /* genearte the fault if the priority of the received defect is greater than
     * the last generted defect priority */
    if (pFngInfo->u1HighestDefectPri > pFngInfo->u1FngPriority)
    {
        /* Report ther Defect */
        EcfmFngSmSetStateRptDft (pPduSmInfo);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFngSmSetStateDftRptdFrmClr
 * 
 * Description        : This routine is used to handle the event 
 *                      NoDefectIndication when state machine is in 
 *                      DefectClearing state.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmSetStateDftRptdFrmClr (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);

    /* Stop the timer ,set the state to Defect Reported and check If priority 
     * of the received defect is greater than the last generated defect then 
     * generate the fault else set the state to Defect_Reported only */
    if (EcfmCcTmrStopTimer
        (ECFM_CC_TMR_FNG_RESET_WHILE, pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmFngSmSetStateDftRptdFrmClr:CC Stop Timer FAILED\r\n");

    }
    /* genearte the fault if the priority of the received defect is greater than
     * the last generted defect priority */
    if (pFngInfo->u1HighestDefectPri > pFngInfo->u1FngPriority)
    {
        /* Report ther Defect */
        EcfmFngSmSetStateRptDft (pPduSmInfo);
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->FngInfo.u1FngState,
                               ECFM_MEP_FNG_STATE_DEFECT_REPORTED, pPduSmInfo);

    /* State set to Defect Reported unconditionally */
    ECFM_CC_FNG_SET_STATE (pMepInfo, ECFM_MEP_FNG_STATE_DEFECT_REPORTED);

    ECFM_CC_TRC_FN_EXIT ();
    return;

}

/****************************************************************************
 * Function Name      : EcfmFngSmSetStateDftClearing
 *
 * Description        : This routine is called when state machine is in
 *                      DEFECT_REPORTED state and there is an event of 
 *                      NO MA DEFECT INDICATION.
 * 
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmSetStateDftClearing (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcFngInfo     *pFngInfo = NULL;
    UINT4               u4FngTimer = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pFngInfo = ECFM_CC_GET_FNGINFO_FROM_MEP (pMepInfo);
    u4FngTimer = ECFM_CC_CONVERT_FNG_TIMER_TO_MSEC (pFngInfo->u2FngResetTime);
    /* Start the FNG Reset timer for 10 sec */
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_FNG_RESET_WHILE, pPduSmInfo,
                             u4FngTimer) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmFngSmSetStateDftClearing:CC Start Timer FAILED\r\n");
        return;
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->FngInfo.u1FngState,
                               ECFM_MEP_FNG_STATE_DEFECT_CLEARING, pPduSmInfo);

    /* Set the State to Defect Clearing state */
    ECFM_CC_FNG_SET_STATE (pMepInfo, ECFM_MEP_FNG_STATE_DEFECT_CLEARING);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmFngSmSetStateDftClearing: Moved to Defect state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFngSmEvtImpossible
 *
 * Description        : This rotuine is used to handle the occurence of event
 *                      which canmt be possible in the current state of the
 *                      state machine.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmFngSmEvtImpossible (tEcfmCcPduSmInfo * pPduSmInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

    UNUSED_PARAM (pPduSmInfo);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmFngSmEvtImpossible:"
                 "IMPOSSIBLE EVENT/STATE Combination Occurred in "
                 "FNG SEM \r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcFngCalulateRemoteDefects
 *
 * Description        : This rotuine is used to calculate Rdi, Remote CCM and
 *                      MAC Status defects, it also sends the corresponding
 *                      event to the FNG SM
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcFngCalulateRemoteDefects (tEcfmCcPduSmInfo * pPduSmInfo,
                                BOOL1 b1CcmRcvdStatus)
{
    tEcfmCcRMepDbInfo  *pRMep = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT1               u1NumOfPortStsDefects = ECFM_INIT_VAL;
    UINT1               u1NumOfIfStsDefects = ECFM_INIT_VAL;
    UINT1               u1NumOfRdiDefects = ECFM_INIT_VAL;
    UINT1               u1NumOfRemoteCcmDefects = ECFM_INIT_VAL;
    UINT1               u1NumOfRMeps = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    /* traverse the whole RMEP tree in the MEP to find out the port status of 
     * all the RMEP should be psUp or the interface status of any RMEP is not 
     * IsUp or the RDI bit of any RMEP is not set*/
    /* in either case generate the Fault alarm for the defected CCM received */

    /* Get the first node from the RBtree of RMepDbTable in MepInfo */
    if (pMepInfo != NULL)
    {
        pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepInfo->RMepDb));
    }
    else
    {
        return;
    }

    while (pRMep != NULL)
    {
        /* check if the port sts is present and is not psUp */
        if (pRMep->b1RMepPortStatusDefect == ECFM_TRUE)
        {
            u1NumOfPortStsDefects =
                u1NumOfPortStsDefects + (UINT1) ECFM_INCR_VAL;
        }
        /* Check for the interface status */
        if (pRMep->b1RMepInterfaceStatusDefect == ECFM_TRUE)
        {
            u1NumOfIfStsDefects = u1NumOfIfStsDefects + (UINT1) ECFM_INCR_VAL;
        }
        /* check the rdi bit */
        if (pRMep->b1LastRdi == ECFM_TRUE)
        {
            u1NumOfRdiDefects = u1NumOfRdiDefects + (UINT1) ECFM_INCR_VAL;
        }
        /* check for remote CCM */
        if (pRMep->b1RMepCcmDefect == ECFM_TRUE)
        {
            u1NumOfRemoteCcmDefects =
                u1NumOfRemoteCcmDefects + (UINT1) ECFM_INCR_VAL;
        }
        /* increment the counter for the number of nodes in the tree
         * means number of rmep in a mep */
        u1NumOfRMeps = u1NumOfRMeps + (UINT1) ECFM_INCR_VAL;
        /* Get the next node form the tree */
        pRMep =
            (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepInfo->RMepDb),
                                                &(pRMep->MepDbDllNode));
    }
    /* NOTE: The order of rasing of defects is made according to the priority of 
     * the various defects */
    /* ------------------------------------- */
    /* Rasie defect for the someRMEPCCMdefect */
    if ((u1NumOfRemoteCcmDefects > 0) && (b1CcmRcvdStatus == ECFM_FALSE))
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngCalulateRemoteDefects: someRMEPCCMdefect Occurred"
                     " \r\n");
        /* Generate the SNMP trap for the fault */
        ECFM_CC_GENERATE_TRAP (pPduSmInfo, ECFM_REMOTE_CCM_TRAP_VAL);

        /* Set someRMEPCCMdefect to True */
        pMepInfo->FngInfo.b1SomeRMepCcmDefect = ECFM_TRUE;
        /* Call the MaDefectindication routine that generates the defect by 
         * calling the FNG SEM if the priority is greater than the lowest 
         * defect priority */
        EcfmCcFngMaDefectIndication (pPduSmInfo, ECFM_DEF_REMOTE_CCM);
        /* Check for allRMEPsDead */
        if (u1NumOfRemoteCcmDefects == u1NumOfRMeps)
        {
            /* Set allRMEPsDead to True */
            pMepInfo->b1AllRMepsDead = ECFM_TRUE;
        }
        else
        {
            /* Set allRMEPsDead to False */
            pMepInfo->b1AllRMepsDead = ECFM_FALSE;
        }
        /* Increment the defect counter */
        ECFM_CC_INCR_REMOTE_CCM_DEFECT_COUNT ();
    }
    else if (u1NumOfRemoteCcmDefects == 0)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngCalulateRemoteDefects: someRMEPCCMdefect Cleared"
                     " \r\n");
        /* Set someMACstatusDefect to False */
        pMepInfo->FngInfo.b1SomeRMepCcmDefect = ECFM_FALSE;
        /* Send NoMaDefection Indiaction to FNG */
        EcfmCcFngNoMaDefectIndication (pPduSmInfo);
        /* Set allRMEPsDead to False */
        pMepInfo->b1AllRMepsDead = ECFM_FALSE;
    }

    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        /* ------------------------------------- */
        /* Rasie defect for the someMACStatusDefect */
        if (((u1NumOfRMeps > 0) && (u1NumOfPortStsDefects == u1NumOfRMeps)) ||
            (u1NumOfIfStsDefects > 0))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcFngCalulateRemoteDefects: someMACstatusDefect"
                         "Occurred \r\n");
            /* Generate the SNMP trap for the fault */
            ECFM_CC_GENERATE_TRAP (pPduSmInfo, ECFM_MAC_STATUS_TRAP_VAL);

            /* Set someMACstatusDefect to True */
            pMepInfo->FngInfo.b1SomeMacStatusDefect = ECFM_TRUE;

            /* Call the MaDefectindication routine thta generate the defect by 
             * calling the FNG SEM if the priority is greater than the lowest 
             * defect priority */
            EcfmCcFngMaDefectIndication (pPduSmInfo, ECFM_DEF_MAC_STS);

            /* Increment the defect counter */
            ECFM_CC_INCR_MAC_STATUS_DEFECT_COUNT ();
        }
        else
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcFngCalulateRemoteDefects: someMACstatusDefect"
                         "Cleared \r\n");
            /* Set someMACstatusDefect to False */
            pMepInfo->FngInfo.b1SomeMacStatusDefect = ECFM_FALSE;
            /* Send NoMaDefection Indiaction to FNG */
            EcfmCcFngNoMaDefectIndication (pPduSmInfo);
        }
    }
    /* ------------------------------------- */
    /* Rasie defect for the someRDIdefect */
    if (u1NumOfRdiDefects > 0)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngCalulateRemoteDefects: someRDIdefect Occurred"
                     "\r\n");
        /* Generate the SNMP trap for the fault */
        ECFM_CC_GENERATE_TRAP (pPduSmInfo, ECFM_RDI_CCM_TRAP_VAL);
        /* Set someRDIdefect to True */
        pMepInfo->FngInfo.b1SomeRdiDefect = ECFM_TRUE;
        /* Call the MaDefectindication routine that generates the defect by 
         * calling the FNG SEM if the priority is greater than the lowest 
         * defect priority */
        EcfmCcFngMaDefectIndication (pPduSmInfo, ECFM_DEF_RDI_CCM);
        /* Increment the defect counter */
        ECFM_CC_INCR_RDI_DEFECT_COUNT ();
    }
    else
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngCalulateRemoteDefects: someRDIdefect Cleared"
                     "\r\n");
        /* Set someRDIdefect to False */
        pMepInfo->FngInfo.b1SomeRdiDefect = ECFM_FALSE;
        /* Send NoMaDefection Indiaction to FNG */
        EcfmCcFngNoMaDefectIndication (pPduSmInfo);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcFngMaDefectIndication
 *
 * Description        : This routine is used to raise fault alarms by calling 
 *                      the FNG state machine after setting the corresponding 
 *                      bit in the MepDeffect variable and checking the 
 *                      Priority of the deffet
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *                      u1Priority - defect priorities
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcFngMaDefectIndication (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Defect)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT1               u1DefPriority = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    /* Defect condition occurred trigger for AIS */
    /* Check is we have already in defect condition */
    /* Note: we dont trigger AIS for RDI defect condition */
    /* Note: we dont trigger AIS for MAC status defect condition */
    if ((u1Defect != ECFM_DEF_RDI_CCM) &&
        (u1Defect != ECFM_DEF_MAC_STS) &&
        (pMepInfo->b1MaDefectIndication != ECFM_TRUE))
    {
        ECFM_CC_AIS_TRIGGER_START (pMepInfo);
        /* Notify the applications about the same */
    }
    /* Get defect priority */
    u1DefPriority = ECFM_CC_GET_DEFECT_PRIORITY (u1Defect);

    /* If the priorty of the received defect is greater than the lowest defect
     * priority value in the MepInfo then call the FNG state machine to 
     * generate the defetct */
    if (u1DefPriority >= pMepInfo->u1LowestAlarmPri)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngMaDefectIndication: MaDefectIndication\r\n");
        /* update the defect */
        pMepInfo->FngInfo.u1HighestDefect = u1Defect;
        /* update the priority of the defect */
        pMepInfo->FngInfo.u1HighestDefectPri = u1DefPriority;
        /* Set MAdefectIndication to True */
        pMepInfo->b1MaDefectIndication = ECFM_TRUE;
        if (u1Defect != ECFM_DEF_RDI_CCM)
        {
            pMepInfo->b1PresentRdi = ECFM_TRUE;
        }
        EcfmCcClntFngSm (pPduSmInfo, ECFM_SM_EV_FNG_MA_MEP_DEFECT);
    }
    else
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngMaDefectIndication:Fault cannot be raised for"
                     "this defect \r\n");
    }

    if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
        (ECFM_CC_IS_RDI_ENABLED (pMepInfo)) && (u1Defect != ECFM_DEF_RDI_CCM))
    {
        pMepInfo->b1PresentRdi = ECFM_TRUE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmCcFngNoMaDefectIndication
 *
 * Description        : This routine generated NOT_MA_INIDCATION. for resetting
 *                      the FNG state Machine after a valid CCM is received. 
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcFngNoMaDefectIndication (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    /* Defect condition cleared trigger for AIS */
    /* Check is we have already in defect cleared state */
    /* Note: mac status defect is always in cleared state for Y.1731 */
    /* Note: RDI is not taken into consideration */
    if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_FALSE) &&
        (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_FALSE) &&
        (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_FALSE) &&
        (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_FALSE))
    {
        pMepInfo->b1PresentRdi = ECFM_FALSE;
    }
    if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_FALSE) &&
        (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_FALSE) &&
        (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_FALSE) &&
        (pMepInfo->b1MaDefectIndication == ECFM_TRUE))
    {
        ECFM_CC_AIS_TRIGGER_STOP (pMepInfo);
    }
    if ((pMepInfo->CcInfo.b1ErrorCcmDefect == ECFM_FALSE) &&
        (pMepInfo->CcInfo.b1XconCcmDefect == ECFM_FALSE) &&
        (pMepInfo->FngInfo.b1SomeRdiDefect == ECFM_FALSE) &&
        (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_FALSE) &&
        (pMepInfo->FngInfo.b1SomeMacStatusDefect == ECFM_FALSE) &&
        (pMepInfo->b1MaDefectIndication == ECFM_TRUE))
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcFngNoMaDefectIndication: !MaDefectIndication\r\n");
        ECFM_CC_INCR_NO_DEFECT_COUNT ();
        /* Set MAdefectIndication to False */
        pMepInfo->b1MaDefectIndication = ECFM_FALSE;
        /* Call FNG with !MAdefectIndication */
        EcfmCcClntFngSm (pPduSmInfo, ECFM_SM_EV_FNG_NO_MA_MEP_DEFECT);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
  End of File cfmfngsm.c
 *****************************************************************************/
