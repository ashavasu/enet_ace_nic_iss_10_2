/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmltrsm.c,v 1.14 2013/12/07 11:08:16 siva Exp $
 *
 * Description: This file contains the Functionality of the Link Trace
 *              Repy Transmitter State Machine.
 *******************************************************************/

#include "cfminc.h"

/*****************************************************************************
 * Function           : EcfmLbLtInitDelayQueue
 *
 * Description        : Routine to initialize the Delay Queue timer to NULL 
 *
 * Input(s)           : None
 *
 * Output(s)          : None.
 *
 * Returns            : None
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtInitDelayQueue ()
{

    ECFM_LBLT_TRC_FN_ENTRY ();

    TMO_SLL_Init (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll));

    ECFM_LBLT_TRC_FN_EXIT ();

    return;
}

/*****************************************************************************
 * Function           : EcfmLbLtDeInitDelayQueue
 *
 * Description        : Routine to clear the pending Delay Queue PDUs when ECFM 
 *                      Module is DISABLED. 
 *                      It also intitializes the Pending Delay Queue PDUs to 
 *                      ZERO.
 *
 * Input(s)           : None
 *
 * Output(s)          : None.
 *
 * Returns            : None
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtDeInitDelayQueue ()
{
    tEcfmLbLtDelayQueueNode *pNodeToRelease = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    while ((pNodeToRelease = (tEcfmLbLtDelayQueueNode *) TMO_SLL_First
            (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll))) != NULL)
    {
        pNodeToRelease = (tEcfmLbLtDelayQueueNode *) TMO_SLL_First
            (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll));
        if (pNodeToRelease == NULL)
        {
            break;
        }
        TMO_SLL_Delete (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll),
                        &(pNodeToRelease->SllNode));
        /* Release CRU Buffer */
        if (ECFM_RELEASE_CRU_BUF (pNodeToRelease->pBuf, ECFM_INIT_VAL) !=
            ECFM_CRU_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_BUFFER_TRC |
                           ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtDeInitDelayQueue:"
                           "Cannot release CRU buffer \r\n");
        }
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_DELAY_QUEUE_POOL,
                             (UINT1 *) (pNodeToRelease));
        pNodeToRelease = NULL;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function           : EcfmLbLtDelayQueueTimerTimeOut
 *
 * Description        : This routine transmits all the Delay Queue Pdus on 
 *                      Delay Queue Time Timeout.
 *
 * Input(s)           : None. 
 *
 * Output(s)          : None.
 *
 * Returns            : None 
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtDelayQueueTimerTimeOut ()
{
    tEcfmLbLtDelayQueueNode *pNodeToSend = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    while ((pNodeToSend = (tEcfmLbLtDelayQueueNode *) TMO_SLL_First
            (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll))) != NULL)
    {
        TMO_SLL_Delete (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll),
                        &(pNodeToSend->SllNode));
        u4PduSize = ECFM_GET_CRU_VALID_BYTE_COUNT (pNodeToSend->pBuf);
        /*Transmitting the CFM-PDU */
        ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pNodeToSend->pBuf, u4PduSize,
                            "EcfmLbLtDelayQueueTimerTimeOut :"
                            "Transmitting the CFMPDU \r\n");
        /* Transmit the Bufffer to frame filtering or port depending on the
         * MP Direction and the VLAN ID stored in Reserved Field of CRU 
         * Buffer */
        if (EcfmLbLtCtrlTxTransmitPkt (pNodeToSend->pBuf,
                                       pNodeToSend->u2PortNum,
                                       pNodeToSend->u4RxVlanIdIsId, 0, 0,
                                       pNodeToSend->u1Direction,
                                       pNodeToSend->u1OpCode,
                                       &(pNodeToSend->VlanClassificationInfo),
                                       &(pNodeToSend->PbbClassificationInfo)) !=
            ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC |
                           ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtDelayQueueTimerTimeOut:"
                           "LTR Transmission Failed\r\n");
            ECFM_RELEASE_CRU_BUF (pNodeToSend->pBuf, ECFM_FALSE);
        }
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_DELAY_QUEUE_POOL,
                             (UINT1 *) (pNodeToSend));
        pNodeToSend = NULL;
    }
    /* If there are any Pending CFM PDU in the queue, transmit all the PDUs */
    if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_DELAY_QUEUE, NULL,
                               (ECFM_DELAY_QUEUE_INTERVAL *
                                ECFM_NUM_OF_MSEC_IN_A_SEC)) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtDelayQueueTimerTimeOut:"
                       " Delay Que Timer Start FAILED\r\n");
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EcfmLbLtAddCfmPduToDelayQueue
 *
 * Description        : This routine adds the Response of CFM PDU
 *                      to the queue.
 *
 *
 * Input(s)           : pNodeToQueue - CFM-PDU node to be queued. 
 *
 * Output(s)          : None.
 *
 * Returns            : None 
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtAddCfmPduToDelayQueue (tEcfmLbLtDelayQueueNode * pNodeToQueue)
{
    tEcfmLbLtDelayQueueNode *pNode = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_ALLOC_MEM_BLOCK_LBLT_DELAY_QUEUE (pNode);
    if (pNode == NULL)
    {
        ECFM_RELEASE_CRU_BUF (pNodeToQueue->pBuf, ECFM_FALSE);
        return;
    }
    ECFM_MEMCPY (pNode, pNodeToQueue, sizeof (tEcfmLbLtDelayQueueNode));
    TMO_SLL_Add (&(ECFM_LBLT_DELAY_QUEUE ()->CfmPduSll),
                 (tTMO_SLL_NODE *) & (pNode->SllNode));
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/******************************************************************************/
/*                           End  of file cfmltr.c                            */
/******************************************************************************/
