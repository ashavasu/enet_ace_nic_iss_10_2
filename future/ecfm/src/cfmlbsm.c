/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbsm.c,v 1.24 2015/05/26 13:09:18 siva Exp $
 *
 * Description: This file contains the functionality of the MP 
 *             LoopBack Responder.
 *******************************************************************/

#include "cfminc.h"

PRIVATE INT4 EcfmLbResXmitLbr PROTO ((tEcfmLbLtPduSmInfo * pPduSmInfo));
/*******************************************************************************
 * Function           : EcfmLbResProcessLbm
 *
 * Description        : This routine processes the received LBM PDU and if 
 *                      valid then LBR is formatted and transmitted.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwdLbm - Pointer to Boolean indicating whether the 
 *                      LBM needs to be forwarded in case the Receving entity 
 *                      is a MHF.
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbResProcessLbm (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdLbm)
{
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmMacAddr        MpMacAddr = { 0 };
    UINT4               u4Offset = 0;
    UINT1               u1TlvType = 0;
    UINT2               u2TlvLength = 0;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pLbInfo);
    /* If the selector type is LSP/PW then validate for Target MEP/MIP TLV and
     * Requesting MEP ID TLV
     */
    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        if (EcfmMpTpLbValidateLbm (pPduSmInfo) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbResProcessLbm: LBM received is not valid\r\n");
            return ECFM_FAILURE;
        }
    }
    else
    {
        pStackInfo = pPduSmInfo->pStackInfo;
        /* Get the mac-addres of the MP */
        if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbResProcessLbm: "
                           "No Port Information Available\r\n");
            return ECFM_FAILURE;
        }
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                   (pStackInfo->u2PortNum)->u4IfIndex,
                                   MpMacAddr);

        /* Y.1731 : Check that Loopback capability should be enabled for MEP 
         * to process the LBM PDU */
        if ((ECFM_LBLT_IS_MEP (pStackInfo))
            && (ECFM_LBLT_IS_LB_DISABLED (pMepInfo)))
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbResProcessLbm: "
                           "Loopback capability is disabled, So MEP cannot process "
                           "the LBM PDU further\r\n");
            return ECFM_FAILURE;
        }

        /* Validate the Value of FirstTLVOffset in LBM PDU */
        if (pPduSmInfo->u1RxFirstTlvOffset < ECFM_LBM_FIRST_TLV_OFFSET)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbResProcessLbm: "
                           "FirstTLVOffset in LBM PDU less than required value "
                           "so MEP cannot process LBM PDU further\r\n");
            return ECFM_FAILURE;
        }

        /* If the I/G bit in the source_address indicates a Group address,
         * instead of an Individual addres and no futher processing should happen */
        if (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxSrcMacAddr) == ECFM_TRUE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbResProcessLbm: "
                           "Source address is a group address so MEP "
                           "cannot processed LBM PDU further\r\n");
            return ECFM_FAILURE;
        }

        /* Check that destination_address parameter contains neither MAC address of 
         * the receiving MP nor CCM gp address then no further processing of the LBM
         * is performed */
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                                   MpMacAddr) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbResProcessLbm: "
                           "Destination address of received LBM not equal to "
                           "MEPs Mac Address, so MEP cannot process the LBM PDU "
                           "further\r\n");

            /* MIP should forward the LBM PDU if the MAC address doesnot match */
            if (ECFM_LBLT_IS_MHF (pStackInfo))
            {
                /* Set the Flag to indicate the LB Receiver to forward the LBM */
                *pbFrwdLbm = ECFM_TRUE;
            }

            /* If the destination address of the LBM is not CCM/LTM Group Address app to 
             * the receving MP's MD level then no further processing is performed */
            if (((ECFM_IS_MULTICAST_CLASS1_ADDR (pPduSmInfo->RxDestMacAddr,
                                                 pStackInfo->u1MdLevel)) ||
                 (ECFM_IS_MULTICAST_CLASS2_ADDR (pPduSmInfo->RxDestMacAddr,
                                                 pStackInfo->u1MdLevel))) !=
                ECFM_TRUE)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbResProcessLbm: "
                               "Destination address of received LBM not equal "
                               "to CCM/LTM Group Addr app to rcv MEP's Md Level, "
                               "MEP Responder SM cannot process the LBM PDU "
                               "further\r\n");
                return ECFM_FAILURE;
            }
            else
            {
                /* Check is the receiving MP is a MIP, if yes it should discard the
                 * LBM PDU and no futher processing should be done */
                if (ECFM_LBLT_IS_MHF (pStackInfo))
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbResProcessLbm: "
                                   "Destination address of received LBM equal to CCM "
                                   "Group Addr app to rcv MP's Md Level, MIP "
                                   "cannot process the LBM PDU further\r\n");
                    return ECFM_FAILURE;
                }
                /* Y.1731 : Check that for processing Multicast LBM PDU, MEPs
                 * Multicast Loopback reception capability should be enabled */
                if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
                {
                    if (ECFM_LBLT_IS_MCAST_LBM_RX_DISABLED (pMepInfo))
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbResProcessLbm: "
                                       "Multicast Loopback reception capability is "
                                       "disabled, So MEP cannot process the LBM PDU "
                                       "further\r\n");
                        return ECFM_FAILURE;
                    }
                }
            }
        }

        /* As per IEEE Std 802.1ag-2007, section 20.46.4.3,(point D)
         * A TLV Length field does not indicate a length that is shorter
         * than the minimum length for that TLV as specified by the Version
         * field of the CFM PDU.
         * So if  Organization specific TLV length is less than Minimum
         * length MEP should not process the LBM */

        if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pStackInfo->u2PortNum))
        {
            u4Offset = (UINT4) pPduSmInfo->u1CfmPduOffset + ECFM_INDEX_EIGHT;

            ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1TlvType);

            if (u1TlvType != 0)
            {
                if (u1TlvType == ECFM_SENDER_ID_TLV_TYPE)
                {
                    /*Increase offset by 1 to get the Sender ID TLV length field */

                    u4Offset = u4Offset + ECFM_INDEX_ONE;

                    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset,
                                         u2TlvLength);

                    /* Increase the offset by TLV length value + 1 byte of TLV length
                     * field + 1 byte to get the next TLV type */

                    u4Offset += (UINT4) u2TlvLength + ECFM_INDEX_TWO;

                }

                ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1TlvType);

                if (u1TlvType == ECFM_DATA_TLV_TYPE)
                {

                    /* Increase offset by 1 to get the Sender ID TLV length field */

                    u4Offset = u4Offset + ECFM_INDEX_ONE;

                    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset,
                                         u2TlvLength);

                    /* Increase the offset by TLV length value + 1 byte of TLV length
                     * field + 1 byte to get the next TLV type */

                    u4Offset += (UINT4) u2TlvLength + ECFM_INDEX_TWO;

                }

                ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1TlvType);

                if (u1TlvType == ECFM_ORG_SPEC_TLV_TYPE)
                {
                    /* Increase offset by 1 to get the Organization specific TLV length field */

                    u4Offset = u4Offset + ECFM_INDEX_ONE;

                    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset,
                                         u2TlvLength);

                    if (u2TlvLength < ECFM_ORG_SPEC_MIN_LENGTH)
                    {

                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbResProcessLbm: "
                                       "Organization specific TLV length is shorter than"
                                       "Minimum length MEP cannot process the LBM\r\n");

                        return ECFM_FAILURE;
                    }
                }
            }
        }
    }

    /* Format and transmits LBR */
    if (EcfmLbResXmitLbr (pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbResProcessLbm: "
                       "MP could not transmit the LBM PDU\r\n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbResXmitLbr
 *
 * Description        : It duplicates the received CRU Buffer and change the 
 *                      source, destination address and opcode in the received
 *                      LBM and transmits the LBR.  
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmLbResXmitLbr (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmMacAddr        MpMacAddr = { 0 };
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT1               u1Opcode = ECFM_INIT_VAL;
    UINT1               u1TlvType = ECFM_INIT_VAL;
    UINT1               u1LbIndication = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    u1Opcode = ECFM_OPCODE_LBR;
    /* Get the Mep Info */
    pMepInfo = ECFM_LBLT_GET_MEP_FROM_PDUSM (pPduSmInfo);
    /* Get the LB info */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);

    /* Duplicating the CRU Buffer */
    pDupCruBuf = ECFM_DUPLICATE_CRU_BUF (pPduSmInfo->pBuf);
    if (pDupCruBuf == NULL)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbResXmitLbr: "
                       "Cru Buffer Duplication Failed\r\n");
        return ECFM_FAILURE;
    }

    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        /* Only LBR is transmitted for MEP not for MIP */

        /* Change the TLV Type field to Replying MEP ID TLV(0x022) */
        u1TlvType = ECFM_REPLY_MPID_TLV_TYPE;
        /* Move 8 bytes to point to the Type field in the TLV */
        u2Offset = u2Offset + (ECFM_MDLEVEL_VER_FIELD_SIZE +
                               ECFM_OPCODE_FIELD_SIZE +
                               ECFM_FLAGS_FIELD_SIZE +
                               ECFM_FIRST_TLV_OFFSET_FIELD_SIZE +
                               ECFM_LBR_FIRST_TLV_OFFSET);

        /* Changing the Type to Replying MEP ID TLV */
        if (ECFM_COPY_OVER_CRU_BUF (pDupCruBuf, (UINT1 *) &u1TlvType,
                                    (UINT4) (u2Offset),
                                    ECFM_TLV_TYPE_FIELD_SIZE) !=
            ECFM_CRU_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbResXmitLbr: "
                           "Copy to Cru Buffer Failed\r\n");
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

        /* Check if present Requesting MEP ID TLV, change the loopback 
         * indication to 0x01 (1b). 
         */
        u2Offset = u2Offset + (ECFM_TLV_TYPE_LEN_FIELD_SIZE +
                               ECFM_REPLY_MPID_TLV_LENGTH);

        /* Get TLV TYPE from Received PDU */
        ECFM_CRU_GET_1_BYTE (pDupCruBuf, (UINT4) u2Offset, u1TlvType);

        if (u1TlvType == ECFM_REQ_MEPID_TLV_TYPE)
        {
            u1LbIndication = ECFM_REQ_MEPID_TLV_LBR_INDICATION;

            u2Offset = u2Offset + ECFM_TLV_TYPE_LEN_FIELD_SIZE;
            if (ECFM_COPY_OVER_CRU_BUF (pDupCruBuf, (UINT1 *) &u1LbIndication,
                                        (UINT4) (u2Offset),
                                        ECFM_REQ_MEPID_TLV_LB_IND_SIZE)
                != ECFM_CRU_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbResXmitLbr: "
                               "Copy to Cru Buffer Failed\r\n");
                ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
                return ECFM_FAILURE;
            }
        }
    }
    else
    {
        pStackInfo = pPduSmInfo->pStackInfo;

        /* Changing the Destination addr field */
        if (ECFM_COPY_OVER_CRU_BUF (pDupCruBuf, pPduSmInfo->RxSrcMacAddr,
                                    0,
                                    ECFM_MAC_ADDR_LENGTH) != ECFM_CRU_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbResXmitLbr: "
                           "Copy to Cru Buffer Failed\r\n");
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

        /* Move offset by 6 bytes to point it to the Source address field */
        u2Offset = u2Offset + (UINT2) ECFM_MAC_ADDR_LENGTH;
        /* Changing the Source addr field */
        if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbResXmitLbr: No Port Information \r\n");
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
        ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                                   (pStackInfo->u2PortNum)->u4IfIndex,
                                   MpMacAddr);
        if (ECFM_COPY_OVER_CRU_BUF
            (pDupCruBuf, MpMacAddr, u2Offset,
             ECFM_MAC_ADDR_LENGTH) != ECFM_CRU_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbResXmitLbr: "
                           "Copy to Cru Buffer Failed\r\n");
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

    }

    /* Move offset by 1 byte to point it to Opcode field */
    u2Offset =
        (UINT2) (pPduSmInfo->u1CfmPduOffset + ECFM_MDLEVEL_VER_FIELD_SIZE);

    /* Changing the Opcode to LBR */
    if (ECFM_COPY_OVER_CRU_BUF (pDupCruBuf, (UINT1 *) &u1Opcode,
                                (UINT4) (u2Offset), ECFM_OPCODE_FIELD_SIZE) !=
        ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbResXmitLbr: " "Copy to Cru Buffer Failed\r\n");
        ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Get the PDU size to dump the packet */
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pDupCruBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pDupCruBuf, u4PduSize,
                        "EcfmLbResXmitLbr: "
                        "Sending out LBR PDU to lower layer...\r\n");

    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        /* Send the LBR packet for transmission over MPLS-TP Network */
        if (EcfmMplsTpLbLtTxPacket (pDupCruBuf, pMepInfo,
                                    ECFM_OPCODE_LBR) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbResXmitLbr: LBR Transmission failed !!!\r\n");
            ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

        /* Increments the LBR transmission counter by 1 */
        ECFM_LBLT_INCR_LBR_OUT (pLbInfo);
    }
    else                        /* Transmit the LBR PDU over ethernet */
    {
        pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
            (UINT2) pStackInfo->u4VlanIdIsid;
       if (EcfmLbLtCtrlTxTransmitPkt (pDupCruBuf,
                                      pStackInfo->u2PortNum,
                                      pStackInfo->u4VlanIdIsid, 0, 0,
                                      pStackInfo->u1Direction,
                                      ECFM_OPCODE_LBR,
                                      &(pPduSmInfo->
                                        VlanClassificationInfo),
                                      &(pPduSmInfo->
                                        PbbClassificationInfo)) !=
           ECFM_SUCCESS)
       {
           ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                          "EcfmLbResXmitLbr: "
                          "LBR PDU Could not be transmitted\r\n");
           ECFM_RELEASE_CRU_BUF (pDupCruBuf, ECFM_FALSE);
           return ECFM_FAILURE;
       }
       
        /* Increments the LBR transmission counter by 1 */
        if (ECFM_LBLT_IS_MEP (pStackInfo))
        {
            ECFM_LBLT_INCR_LBR_OUT (pLbInfo);
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************/
/*                           End  of cfmlbsm.c                                */
/******************************************************************************/
