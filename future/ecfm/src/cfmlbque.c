/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbque.c,v 1.21 2015/11/21 11:09:59 siva Exp $
 *
 * Description: This file contains the procedure related to handling
 *              of the messages posted by the CFA for LBLT task for
 *              further processing.
 *******************************************************************/

#include "cfminc.h"

/******************************************************************************
 * Function Name      : EcfmLbLtPktQueueHandler
 *
 * Description        : This function receives and processes the CFM PDUs  
 *                      received on the LBLT Task Packet Queue.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtPktQueueHandler (tEcfmLbLtMsg * pQMsg)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    /* Select Context for which the Message is received */
    if (ECFM_LBLT_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
    {
        EcfmLbLtCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
        pQMsg->uMsg.pEcfmPdu = NULL;
        return;
    }

    if (pQMsg->MsgType == ECFM_EV_MPLSTP_LBLT_PDU_IN_QUE)
    {
        /* Get ECFM module staus from the Dummy MPLS Port */
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (ECFM_LBLT_MAX_PORT_INFO);
        /* Check if the Port Module Status is enabled or not */
        if ((pPortInfo == NULL) || (pPortInfo->u1PortEcfmStatus != ECFM_ENABLE))
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtPktQueueHandler: CFM-PDU Received: but "
                           "ECFM module status is disabled\r\n");
            EcfmLbLtCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            ECFM_LBLT_RELEASE_CONTEXT ();
            return;
        }
    }
    else                        /* Ethernet */
    {
        /* Get PortInfo for the received Index */
        pPortInfo = ECFM_LBLT_GET_PORT_INFO (pQMsg->u2PortNum);
        /* Check if the Port Module Status is enabled or not */
        if (pPortInfo == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtPktQueueHandler: CFM-PDU Received on "
                           "invalid port\r\n");
            EcfmLbLtCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            ECFM_LBLT_RELEASE_CONTEXT ();
            return;
        }
    }

    switch (pQMsg->MsgType)
    {
        case ECFM_PDU_RCV_FRM_CFA:
            ECFM_LBLT_TRC (ECFM_DATA_PATH_TRC,
                           "EcfmLbLtPktQueueHandler: Received CFMPDU from"
                           "CFA  \r\n");
            /* Call LBLT Tasks receiver for processing the received 
             * CFMPDU */
            if (EcfmLbLtCtrlRxPkt (pQMsg->uMsg.pEcfmPdu,
                                   pQMsg->u4IfIndex, pQMsg->u2PortNum)
                != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtPktQueueHandler: EcfmLbLtCtrlReceiver"
                               "returned failure \r\n");
                ECFM_LBLT_INCR_DSRD_CFM_PDU_COUNT (pQMsg->u2PortNum);
                ECFM_LBLT_INCR_CTX_DSRD_CFM_PDU_COUNT (pQMsg->u4ContextId);
            }
            EcfmLbLtCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            break;
        case ECFM_EV_MPLSTP_LBLT_PDU_IN_QUE:
            /* Received a Y.1731 LBM/LBR PDU from MPLS-TP network */
            ECFM_LBLT_TRC (ECFM_DATA_PATH_TRC,
                           "EcfmLbLtPktQueueHandler: Y.1731 PDU received over"
                           "MPLS-TP Path\r\n");

            if (EcfmProcessMpTpPdu (pQMsg->uMsg.pEcfmPdu,
                                    pQMsg->u4IfIndex) == ECFM_FAILURE)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtPktQueueHandler: Unable to process "
                               "the Y.1731 PDU received over MPLS-TP path\r\n");
            }

            EcfmLbLtCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);

            pQMsg->uMsg.pEcfmPdu = NULL;
            break;
        default:
            ECFM_LBLT_TRC (INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtPktQueueHandler: Invalid Event \n");
            EcfmLbLtCtrlRxPktFree (pQMsg->uMsg.pEcfmPdu);
            pQMsg->uMsg.pEcfmPdu = NULL;
            break;
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/******************************************************************************
 * Function Name      : EcfmLbLtCfgQueueHandler
 * 
 * Description        : This function receives and processes the configuration
 *                      messages received on the LBLT Tasks Configuration Queue.
 *
 * 
 * Input(s)           : None
 * 
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/

PUBLIC VOID
EcfmLbLtCfgQueueHandler (tEcfmLbLtMsg * pQMsg)
{
    switch (pQMsg->MsgType)
    {
        case ECFM_LT_START_TRANSACTION:
        case ECFM_LT_STOP_TRANSACTION:
        case ECFM_LB_START_TRANSACTION:
        case ECFM_LB_STOP_TRANSACTION:
        case ECFM_DM_START_TRANSACTION:
        case ECFM_DM_STOP_TRANSACTION:
        case ECFM_TST_START_TRANSACTION:
        case ECFM_TST_STOP_TRANSACTION:
        case ECFM_TH_START_TRANSACTION:
        case ECFM_TH_STOP_TRANSACTION:
        {
            tEcfmLbLtMepInfo   *pMepNode = NULL;    /* For receiving pointer to 
                                                     * MEP Node found */
            tEcfmLbLtPduSmInfo  PduSmInfo;

            if (ECFM_LBLT_SELECT_CONTEXT (pQMsg->u4ContextId) != ECFM_SUCCESS)
            {
                break;
            }

            ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);
            ECFM_LBLT_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtCfgQueueHandler:"
                           "Received Transmit LBM/LTM  Message from SNMP \r\n");

            /* Get MEP Info from the information received in the Message 
             * Node*/
            if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pQMsg->uMsg.Mep.u1SelectorType)
                != ECFM_TRUE)
            {
                pMepNode =
                    EcfmLbLtUtilGetMepEntryFrmPort (pQMsg->uMsg.Mep.u1MdLevel,
                                                    pQMsg->uMsg.Mep.u4VidIsid,
                                                    pQMsg->u2PortNum,
                                                    pQMsg->uMsg.Mep.
                                                    u1Direction);

            }
            else
            {
                /* For MPLSTP get the Mep from ECFM_CC_MEP_TABLE */
                pMepNode =
                    EcfmLbLtUtilGetMepEntryFrmGlob (pQMsg->uMsg.Mep.u4MdIndex,
                                                    pQMsg->uMsg.Mep.u4MaIndex,
                                                    pQMsg->uMsg.Mep.u2MepId);
            }

            if (pMepNode != NULL)
            {
                /* Fill MepInfo to PduSmInfo for calling the SM */
                PduSmInfo.pMepInfo = pMepNode;
                /* Fill PortInfo to PduSmInfo */
                PduSmInfo.pPortInfo = pMepNode->pPortInfo;

                switch (pQMsg->MsgType)
                {
                    case ECFM_LB_STOP_TRANSACTION:
                    case ECFM_LB_START_TRANSACTION:
                        /* Call LB Initiator State to transmit LBMs */
                        EcfmLbLtClntLbInitiator (pMepNode, pQMsg->MsgType);
                        break;
                    case ECFM_LT_START_TRANSACTION:
                        /* Call LT Initiator State Machine for transmitting LTM */
                        EcfmLbLtClntLtInitSm (&(PduSmInfo),
                                              ECFM_SM_EV_LTI_TX_LTM);
                        break;
                    case ECFM_LT_STOP_TRANSACTION:
                        /* Call LT Initiator State Machine for Stopping LTM */
                        EcfmLbLtClntLtInitSm (&PduSmInfo,
                                              ECFM_SM_EV_LTI_TX_LTM_TIMESOUT);
                        break;
                    case ECFM_DM_START_TRANSACTION:
                    case ECFM_DM_STOP_TRANSACTION:
                        /* Call DM Initiator to start transaction */
                        EcfmLbLtClntDmInitiator (pMepNode, pQMsg->MsgType);
                        break;
                    case ECFM_TST_START_TRANSACTION:
                    case ECFM_TST_STOP_TRANSACTION:
                        /* Call TST Initiator to start transaction */
                        EcfmLbLtClntTstInitiator (pMepNode, pQMsg->MsgType);

                        break;
                    case ECFM_TH_START_TRANSACTION:
                    case ECFM_TH_STOP_TRANSACTION:
                        /* Call TH Initiator to start transaction */
                        EcfmLbLtClntThInitiator (&(PduSmInfo), pQMsg->MsgType);

                        break;
                    default:
                        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                       ECFM_ALL_FAILURE_TRC,
                                       "EcfmLbLtCfgQueueHandler: Invalid Start/Stop "
                                       "Transaction event \r\n");
                        break;
                }
            }
            break;
        }
#ifdef L2RED_WANTED
        case ECFM_RM_FRAME:
            EcfmLbLtRedHandleRmEvents (pQMsg);
            break;
#endif

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
        case ECFM_Y1564_REQ_SLA_CONF_TEST_START:
        case ECFM_Y1564_REQ_SLA_PERF_TEST_START:
        case ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST:
        case ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST:
        case ECFM_Y1564_REQ_CFM_GET_TEST_REPORT:
        case ECFM_R2544_REQ_START_THROUGHPUT_TEST:
        case ECFM_R2544_REQ_START_LATENCY_TEST:
        case ECFM_R2544_REQ_START_FRAMELOSS_TEST:
        case ECFM_R2544_REQ_START_BACKTOBACK_TEST:
        case ECFM_R2544_REQ_STOP_BENCHMARK_TEST:
        case ECFM_R2544_REQ_SLA_TEST_RESULT:
            EcfmLbLtHandleY1564andRFC2544Events (pQMsg);
            break;
#endif

        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtCfgQueueHandler: Invalid Event \r\n");
            break;

    }
    return;
}

/****************************************************************************
                            End of File cfmlbque.c
 ****************************************************************************/
