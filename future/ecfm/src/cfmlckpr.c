/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlckpr.c,v 1.14 2014/03/16 11:34:12 siva Exp $
 *
 * Description: This file contains the Functionality of the LCK 
 *              Control Sub Module.
 *******************************************************************/
#include "cfminc.h"

PRIVATE VOID        EcfmCcSetLckCondition
PROTO ((tEcfmCcMepInfo *, tEcfmMacAddr, UINT1));

/*******************************************************************************
 * Function           : EcfmCcClntProcessLck
 *
 * Description        : This routine parse LCK Pdu & if valid then processes 
 *                      the received LCK PDU
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwdLck - Pointer to Boolean indicating whether the 
 *                      LCK needs to be forwarded in case the Receving entity 
 *                      is a MHF.
 *                      
 * Output(s)          : pbFrwdLck - Returns status whether Lck Pdu needs to be
 *                      forwarded or not
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcClntProcessLck (tEcfmCcPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdLck)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcLckInfo     *pLckInfo = NULL;
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT1               u1LckInterval = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pStackInfo = pPduSmInfo->pStackInfo;
    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);

    /* Validate the Value of FirstTLVOffset in LCK PDU */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_INIT_VAL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessLck: "
                     "FirstTLVOffset in LCK PDU is not equal to required value"
                     " so MEP cannot process LCK PDU further\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcClntProcessLck: " "No Port Information\r\n");
        return ECFM_FAILURE;
    }
    /* Get the MEP's MAC Address */
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_CC_PORT_INFO
                               (pStackInfo->u2PortNum)->u4IfIndex, MepMacAddr);

    /* Check if the Mac Address received in the LCK is neither same as that of the
     * receiving MEP nor multicast then discard that
     */
    if ((ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr)
         == ECFM_FAILURE) &&
        (ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_FALSE))
    {
        /* Discard the LCK frame */
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessLck: "
                     "discarding the received LCK frame\r\n");
        if (ECFM_CC_IS_MHF (pStackInfo))
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntProcessLck: "
                         "Lck forwarded in case of MIP\r\n");
            *pbFrwdLck = ECFM_TRUE;
        }
        return ECFM_FAILURE;
    }
    else
    {
        if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr, MepMacAddr)
            == ECFM_SUCCESS)
        {
            if (ECFM_CC_IS_MHF (pStackInfo))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntProcessLck: "
                             "discarding the received LCK frame\r\n");
                return ECFM_FAILURE;
            }
        }
        if ((ECFM_IS_MULTICAST_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_TRUE))
        {
            if (ECFM_CC_IS_MHF (pStackInfo))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntProcessAis: "
                             "Ais forwarded in case of MIP\r\n");
                *pbFrwdLck = ECFM_TRUE;
                return ECFM_FAILURE;
            }
        }
    }

    /* Lck RxWhile Timer is to be started as per the Interval received */
    if (pPduSmInfo->u1RxFlags == ECFM_AIS_LCK_INTERVAL_1_S_FLAG)
    {
        u1LckInterval = ECFM_CC_AIS_LCK_INTERVAL_1_SEC;
    }
    else
    {
        u1LckInterval = ECFM_CC_AIS_LCK_INTERVAL_1_MIN;
    }

    /* Start the Lck RxWhile Timer */
    ECFM_GET_AIS_LCK_INTERVAL (pLckInfo->u1LckInterval, u4Interval);
    u4Interval = u4Interval * ECFM_XCHK_DELAY_DEF_VAL;
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_RXWHILE, pPduSmInfo,
                             u4Interval) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcClntProcessLck:Start Timer FAILED\r\n");
        return ECFM_FAILURE;
    }
#ifdef L2RED_WANTED
    pMepInfo->LckInfo.u4LckRcvdRxWhileValue = u4Interval;
#endif

    /* Set the Lck Condition */
    EcfmCcSetLckCondition (pMepInfo, pPduSmInfo->RxSrcMacAddr,
                           pPduSmInfo->u1RxMdLevel);

    ECFM_CC_TRC_FN_EXIT ();
    UNUSED_PARAM (u1LckInterval);
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcSetLckCondition
 *
 * Description        : This routine is used to set the Lck condition
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *                      SrcMacAddr - Mac address of the AIS generating MEP
 *                      u1MdLevel - MD level of the AIS generating MEP
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmCcSetLckCondition (tEcfmCcMepInfo * pMepInfo, tEcfmMacAddr SrcMacAddr,
                       UINT1 u1MdLevel)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcErrLogInfo   LocErrLog;
    tEcfmCcLckInfo     *pLckInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMep = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    UNUSED_PARAM (u1MdLevel);

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;
    pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);

    /* Check if LCK condition is already Set or not */
    if (pLckInfo->b1LckCondition == ECFM_TRUE)
    {
        return;
    }
    else
    {
        /*Add entry for LCK Condition Defect Entry to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (&PduSmInfo, ECFM_LCK_CONDITION_ENTRY);

        /* Set Lck Condition. Now Data packet will not be forwarded through 
         * this Mep */
        pLckInfo->b1LckCondition = ECFM_TRUE;

        /* Sync Lock Condition at STANDBY */
        EcfmRedSyncLckCondition (pMepInfo);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_LCK_COND_EN_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
        /* Notify the registered applications that LCK Condition is Set */
        ECFM_NOTIFY_PROTOCOLS (ECFM_LCK_CONDITION_ENCOUNTERED,
                               &MepInfo, (tMacAddr *) SrcMacAddr,
                               ECFM_LCK_CONDITION_ENCOUNTERED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        /* Call LOC Exit at Client level */
        if (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE)
        {
            /* Get the first node from the RBtree of RMepDbTable in MepInfo */
            pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepInfo->RMepDb));
            while (pRMep != NULL)
            {
                if (pRMep->b1RMepCcmDefect == ECFM_TRUE)
                {
                    PduSmInfo.pRMepInfo = pRMep;
                    LocErrLog.u4MdIndex = pMepInfo->u4MdIndex;
                    LocErrLog.u4MaIndex = pMepInfo->u4MaIndex;
                    LocErrLog.u2MepId = pMepInfo->u2MepId;
                    LocErrLog.u4SeqNum = 0;
                    LocErrLog.u2RmepId = pRMep->u2RMepId;
                    LocErrLog.u2LogType = ECFM_LOC_DFCT_EXIT;
                    EcfmRedSyncSmData (&PduSmInfo);
                    /* Generate the SNMP trap for the fault */
                    Y1731_CC_GENERATE_TRAP (&LocErrLog, Y1731_LOC_TRAP_EX_VAL);
                }
                /* Get the next node form the tree */
                pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepInfo->RMepDb),
                                                            &(pRMep->
                                                              MepDbDllNode));
            }
        }
        if (pMepInfo->CcInfo.b1CciEnabled != ECFM_TRUE)
        {
            /* Trigger AIS Transmission */
            ECFM_CC_AIS_TRIGGER_START (pMepInfo);
            EcfmRedSyncSmData (&PduSmInfo);
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcClearLckCondition
 *
 * Description        : This routine is used to clear the Lck condition
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcClearLckCondition (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;

    /* Synch LCK condition at STANDBY Node also */
    EcfmRedSyncLckCondition (pMepInfo);

    /*Lck Exit Trap is raised when Y.1731 is enabled */
    if (pMepInfo->LckInfo.b1LckCondition == ECFM_TRUE)
    {
        /* Add entry for LCK Condition Defect Exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (&PduSmInfo, ECFM_LCK_CONDITION_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_LCK_COND_EX_TRAP_VAL);

        /* Clear Lck Condition. Now it will not suppress Alarm at this level */
        pMepInfo->LckInfo.b1LckCondition = ECFM_FALSE;

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum,
                                                  pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;

        /* Notify the registered applications that LCK Condition is Clear */
        ECFM_NOTIFY_PROTOCOLS (ECFM_LCK_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_LCK_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_CC_TASK_ID);

        /* Trigger AIS Transmission stop */
        ECFM_CC_AIS_TRIGGER_STOP (pMepInfo);
        EcfmRedSyncSmData (&PduSmInfo);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  End of File cfmlckpr.c
 ******************************************************************************/
