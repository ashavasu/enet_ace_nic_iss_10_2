/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmvlnlw.c,v 1.26 2016/05/27 11:14:54 siva Exp $
 *
 * Description: This file contains the Protocol Low Level Routines 
 *               for Vlan Table of standard ECFM MIB.
 *******************************************************************/

#include "cfminc.h"
#include "fscfmmcli.h"

/* LOW LEVEL Routines for Table : Ieee8021CfmVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmVlanTable (UINT4
                                              u4Ieee8021CfmVlanComponentId,
                                              UINT4 u4Ieee8021CfmVlanSelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get Vlan entry corresponding to VlanSelector */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for the given Indices\n");
        return SNMP_FAILURE;
    }
    /* Vlan entry corresponding to VlanId exists */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmVlanTable (UINT4 *pu4Ieee8021CfmVlanComponentId,
                                      UINT4 *pu4Ieee8021CfmVlanSelector)
{
    return (nmhGetNextIndexIeee8021CfmVlanTable
            (0, pu4Ieee8021CfmVlanComponentId, 0, pu4Ieee8021CfmVlanSelector));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                nextIeee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
                nextIeee8021CfmVlanSelector
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmVlanTable (UINT4 u4Ieee8021CfmVlanComponentId,
                                     UINT4 *pu4NextIeee8021CfmVlanComponentId,
                                     UINT4 u4Ieee8021CfmVlanSelector,
                                     UINT4 *pu4NextIeee8021CfmVlanSelector)
{
    tEcfmCcVlanInfo     VlanInfo;
    tEcfmCcVlanInfo    *pVlanNextNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get next VLAN entry corresponding to VlanId */
    ECFM_MEMSET (&VlanInfo, ECFM_INIT_VAL, ECFM_CC_VLAN_INFO_SIZE);
    VlanInfo.u4VidIsid = (UINT2) u4Ieee8021CfmVlanSelector;

    pVlanNextNode = (tEcfmCcVlanInfo *) RBTreeGetNext
        (ECFM_CC_VLAN_TABLE, (tRBElem *) & VlanInfo, NULL);

    if (pVlanNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Next VLAN entry corresponding to VlanId exists */
    /* Set next index corresponding to VLAN entry */
    *pu4NextIeee8021CfmVlanSelector = (INT4) (pVlanNextNode->u4VidIsid);

    *pu4NextIeee8021CfmVlanComponentId = ECFM_CC_CURR_CONTEXT_ID ();
    ECFM_CONVERT_CTXT_ID_TO_COMP_ID (*pu4NextIeee8021CfmVlanComponentId);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmVlanPrimarySelector
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                retValIeee8021CfmVlanPrimarySelector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmVlanPrimarySelector (UINT4 u4Ieee8021CfmVlanComponentId,
                                      UINT4 u4Ieee8021CfmVlanSelector,
                                      UINT4
                                      *pu4RetValIeee8021CfmVlanPrimarySelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /*Get entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Indices\n");
        return SNMP_FAILURE;
    }
    /* VLAN entry corresponding to VlanId exists */
    /* Set PrimarySelector corresponding to VLAN entry */
    *pu4RetValIeee8021CfmVlanPrimarySelector =
        (INT4) (pVlanNode->u4PrimaryVidIsid);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmVlanRowStatus
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                retValIeee8021CfmVlanRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmVlanRowStatus (UINT4 u4Ieee8021CfmVlanComponentId,
                                UINT4 u4Ieee8021CfmVlanSelector,
                                INT4 *pi4RetValIeee8021CfmVlanRowStatus)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /*Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for Given Indices\n");
        return SNMP_FAILURE;
    }
    /* VLAN entry corresponding to VlanId exists */
    /* Set Row status corresponding to VLAN entry */
    *pi4RetValIeee8021CfmVlanRowStatus = (INT4) (pVlanNode->u1RowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CfmVlanPrimarySelector
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                setValIeee8021CfmVlanPrimarySelector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmVlanPrimarySelector (UINT4 u4Ieee8021CfmVlanComponentId,
                                      UINT4 u4Ieee8021CfmVlanSelector,
                                      UINT4
                                      u4SetValIeee8021CfmVlanPrimarySelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Indices\n");
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_NOT_READY)
    {
        pVlanNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    }
    /* VLAN entry corresponding to VlanId exists */
    /* Set PrimaryVid corresponding to VLAN entry */
    pVlanNode->u4PrimaryVidIsid =
        (UINT2) u4SetValIeee8021CfmVlanPrimarySelector;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmVlanPrimaryVid, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Ieee8021CfmVlanSelector,
                      u4SetValIeee8021CfmVlanPrimarySelector));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmVlanRowStatus
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                setValIeee8021CfmVlanRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmVlanRowStatus (UINT4 u4Ieee8021CfmVlanComponentId,
                                UINT4 u4Ieee8021CfmVlanSelector,
                                INT4 i4SetValIeee8021CfmVlanRowStatus)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcVlanInfo    *pVlanNewNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode != NULL)
    {
        /* VLAN entry corresponding to VlanId exists and its row status is same 
         * as user wants to set */
        if (pVlanNode->u1RowStatus == (UINT1) i4SetValIeee8021CfmVlanRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    else if (i4SetValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No VLAN Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }

    else if (i4SetValIeee8021CfmVlanRowStatus !=
             ECFM_ROW_STATUS_CREATE_AND_WAIT)
    {
        return SNMP_FAILURE;
    }

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmVlanRowStatus, u4SeqNum,
                          TRUE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    switch (i4SetValIeee8021CfmVlanRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:

            RBTreeRem (ECFM_CC_PRIMARY_VLAN_TABLE, pVlanNode);
            pVlanNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNode);
            break;

        case ECFM_ROW_STATUS_ACTIVE:
            /* Check whether VLAN entry's row status can be changed to ACTIVE */
            if (pVlanNode->u4PrimaryVidIsid == 0)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Vlan Row status set to Active Failed\n");
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Ieee8021CfmVlanSelector,
                                  i4SetValIeee8021CfmVlanRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* Update NoOfVlanIds for MAs associated with VLAN entry's 
             * PrimarySelector*/
            pMaNode = (tEcfmCcMaInfo *) RBTreeGetFirst (ECFM_CC_MA_TABLE);
            while (pMaNode != NULL)
            {
                /* Get MA entry associated with PrimarySelector */
                if ((pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE) &&
                    (pMaNode->u4PrimaryVidIsid == pVlanNode->u4PrimaryVidIsid))
                {
                    pMaNode->u2NumberOfVids =
                        (pMaNode->u2NumberOfVids) + (UINT2) ECFM_INCR_VAL;

                    /* Evaluate MIP creation for this particular vlanId */
                    EcfmCcUtilEvaluateAndCreateMip (-1,
                                                    pMaNode->u4PrimaryVidIsid,
                                                    ECFM_TRUE);
                }
                pMaNode = (tEcfmCcMaInfo *) RBTreeGetNext
                    (ECFM_CC_MA_TABLE, (tRBElem *) pMaNode, NULL);
            }
            /* Add new VLAN node in primary VLAN table */
            if (RBTreeAdd (ECFM_CC_PRIMARY_VLAN_TABLE, pVlanNode) ==
                ECFM_RB_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Additon to primary VLAN Failed\n");
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* Set row status ACTIVE */
            pVlanNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNode);
            break;

        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
            /* User wants to create new entry with VlanId */
            /* Create node for new VLAN entry in VLAN Table */
            if (ECFM_ALLOC_MEM_BLOCK_CC_VLAN_TABLE (pVlanNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "nmhSetIeee8021CfmVlanRowStatus: Row Status Create and Go"
                             "Allocation for VLAN Node Failed \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Ieee8021CfmVlanSelector,
                                  i4SetValIeee8021CfmVlanRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            ECFM_MEMSET (pVlanNewNode, ECFM_INIT_VAL, ECFM_CC_VLAN_INFO_SIZE);

            /* Put index in new VLAN node */
            pVlanNewNode->u4VidIsid = (UINT2) u4Ieee8021CfmVlanSelector;
            /* Put other default values */
            pVlanNewNode->u4PrimaryVidIsid = ECFM_INIT_VAL;
            /* Set VLAN entry's rowstatus */
            pVlanNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_READY;

            /* Add new VLAN node in VlanTable in global info */
            if (RBTreeAdd (ECFM_CC_VLAN_TABLE, pVlanNewNode) == ECFM_RB_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Allocation to Vlan Failed\n");
                /*Addition of node in VlanTable failed */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_VLAN_TABLE_POOL,
                                     (UINT1 *) (pVlanNewNode));
                pVlanNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Ieee8021CfmVlanSelector,
                                  i4SetValIeee8021CfmVlanRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNewNode);
            break;

        case ECFM_ROW_STATUS_DESTROY:
            /* Update NoOfVlanIds for all MAs with VLAN's PrimarySelector */
            if (pVlanNode->u4PrimaryVidIsid != 0)
            {
                pMaNode = (tEcfmCcMaInfo *) RBTreeGetFirst (ECFM_CC_MA_TABLE);
                while (pMaNode != NULL)
                {
                    /* Get MA entry associated with PrimarySelector */
                    if ((pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE) &&
                        (pMaNode->u4PrimaryVidIsid ==
                         pVlanNode->u4PrimaryVidIsid))
                    {
                        pMaNode->u2NumberOfVids =
                            (pMaNode->u2NumberOfVids) - (UINT2) ECFM_DECR_VAL;
                    }
                    pMaNode = (tEcfmCcMaInfo *) RBTreeGetNext
                        (ECFM_CC_MA_TABLE, (tRBElem *) pMaNode, NULL);
                }
                /* Evaluate MIP creation for this particular
                 * vlanId */
                EcfmCcUtilEvaluateAndCreateMip (-1,
                                                pVlanNode->u4PrimaryVidIsid,
                                                ECFM_TRUE);
            }
            /* Remove VLAN node from VlanTable in Global info */
            RBTreeRem (ECFM_CC_VLAN_TABLE, (tRBElem *) pVlanNode);
            RBTreeRem (ECFM_CC_PRIMARY_VLAN_TABLE, (tRBElem *) pVlanNode);

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtRemoveVlanEntry (u4CurrContextId, pVlanNode);

            /* Release the memory assigned to VLAN node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_VLAN_TABLE_POOL,
                                 (UINT1 *) (pVlanNode));
            pVlanNode = NULL;
            break;
        default:
            break;
    }

    /* Sending Trigger to MSR */
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Ieee8021CfmVlanSelector,
                      i4SetValIeee8021CfmVlanRowStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmVlanPrimarySelector
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                testValIeee8021CfmVlanPrimarySelector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmVlanPrimarySelector (UINT4 *pu4ErrorCode,
                                         UINT4 u4Ieee8021CfmVlanComponentId,
                                         UINT4 u4Ieee8021CfmVlanSelector,
                                         UINT4
                                         u4TestValIeee8021CfmVlanPrimarySelector)
{
    UINT2		u2NoVidsforPvid = ECFM_INIT_VAL;
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNextNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);

    MaInfo.u4MdIndex = 0;
    MaInfo.u4MaIndex = 0;
    pMaNextNode = (tEcfmCcMaInfo *)
        RBTreeGetNext (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo, NULL);

    while (pMaNextNode != NULL)
    {
        if (pMaNextNode->u4PrimaryVidIsid == (UINT4) u4Ieee8021CfmVlanSelector)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:VID is primary VLAN for some other. Can not"
                         " be configured as secondary VLAN \n");
            return SNMP_FAILURE;
        }

        MaInfo.u4MdIndex = pMaNextNode->u4MdIndex;
        MaInfo.u4MaIndex = pMaNextNode->u4MaIndex;

        pMaNextNode = (tEcfmCcMaInfo *) RBTreeGetNext
            (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo, NULL);
    }

    /* Validating the value for PrimarySelector */
    if ((u4TestValIeee8021CfmVlanPrimarySelector < ECFM_VLANID_MIN) ||
        (u4TestValIeee8021CfmVlanPrimarySelector > ECFM_VLANID_MAX) ||
        (u4TestValIeee8021CfmVlanPrimarySelector == u4Ieee8021CfmVlanSelector))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: PrimarySelector validation failed\n");
        return SNMP_FAILURE;
    }

    u2NoVidsforPvid = EcfmSnmpLwGetNoOfEntriesForPVid (
		    (UINT2) u4TestValIeee8021CfmVlanPrimarySelector);

    if (u2NoVidsforPvid >= ECFM_MAX_SEC_VLAN_PER_PVLAN)
    {
	*pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: PrimarySelector is already associated with 32 vlans\n");
        return SNMP_FAILURE;

    }
    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Index\n");
        return SNMP_FAILURE;
    }

    /* VLAN entry corresponding to VlanId exists */
    /* Check whether PrimarySelector can be set */
    if (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Primary VID cannot be set as row"
                     " status is already ECFM_ROW_STATIS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmVlanRowStatus
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                testValIeee8021CfmVlanRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmVlanRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021CfmVlanComponentId,
                                   UINT4 u4Ieee8021CfmVlanSelector,
                                   INT4 i4TestValIeee8021CfmVlanRowStatus)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate row status value */
    if ((i4TestValIeee8021CfmVlanRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValIeee8021CfmVlanRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid Row Status Value\n");
        return SNMP_FAILURE;
    }

    /*  validate index range */
    if ((u4Ieee8021CfmVlanSelector < ECFM_VLANID_MIN) ||
        (u4Ieee8021CfmVlanSelector > ECFM_VLANID_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:VlanSelector validation failed\n");
        return SNMP_FAILURE;
    }
    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        /* VLAN entry corresponding to VlanId does not exists and user wants 
         * to change its row status */
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No VLAN Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        if (i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }

        else if (i4TestValIeee8021CfmVlanRowStatus !=
                 ECFM_ROW_STATUS_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Invalid Value\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    /* VLAN entry corresponding to VlanId exists and its row status is 
     *  same as user wants to set */
    if (pVlanNode->u1RowStatus == (UINT1) i4TestValIeee8021CfmVlanRowStatus)
    {
        return SNMP_SUCCESS;
    }
    /* VLAN entry corresponding to VlanId exists and user wants 
     *  to create new VLAN entry with same VlanId */
    if ((i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT)
        || (i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Vlan entry already exists\n");
        return SNMP_FAILURE;
    }
    /* If user wants to make the row status, corresponding to VLAN entry to 
     * ACTIVE */
    if ((i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_ACTIVE) &&
        (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_NOT_IN_SERVICE))
    {
        /* check if PrimarySelector has been set or not */
        if (pVlanNode->u4PrimaryVidIsid == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Invalid Row Status Value\n");
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CfmVlanTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1agCfmVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmVlanTable
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmVlanTable (UINT4 u4Dot1agCfmVlanComponentId,
                                            INT4 i4Dot1agCfmVlanVid)
{
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmVlanTable
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmVlanTable (UINT4 *pu4Dot1agCfmVlanComponentId,
                                    INT4 *pi4Dot1agCfmVlanVid)
{
    UNUSED_PARAM (pu4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (pi4Dot1agCfmVlanVid);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmVlanTable
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                nextDot1agCfmVlanComponentId
                Dot1agCfmVlanVid
                nextDot1agCfmVlanVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmVlanTable (UINT4 u4Dot1agCfmVlanComponentId,
                                   UINT4 *pu4NextDot1agCfmVlanComponentId,
                                   INT4 i4Dot1agCfmVlanVid,
                                   INT4 *pi4NextDot1agCfmVlanVid)
{
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (pu4NextDot1agCfmVlanComponentId);
    UNUSED_PARAM (pi4NextDot1agCfmVlanVid);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmVlanPrimaryVid
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid

                The Object 
                retValDot1agCfmVlanPrimaryVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmVlanPrimaryVid (UINT4 u4Dot1agCfmVlanComponentId,
                               INT4 i4Dot1agCfmVlanVid,
                               INT4 *pi4RetValDot1agCfmVlanPrimaryVid)
{
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (pi4RetValDot1agCfmVlanPrimaryVid);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmVlanRowStatus
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid

                The Object 
                retValDot1agCfmVlanRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmVlanRowStatus (UINT4 u4Dot1agCfmVlanComponentId,
                              INT4 i4Dot1agCfmVlanVid,
                              INT4 *pi4RetValDot1agCfmVlanRowStatus)
{
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (pi4RetValDot1agCfmVlanRowStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmVlanPrimaryVid
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid

                The Object 
                setValDot1agCfmVlanPrimaryVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmVlanPrimaryVid (UINT4 u4Dot1agCfmVlanComponentId,
                               INT4 i4Dot1agCfmVlanVid,
                               INT4 i4SetValDot1agCfmVlanPrimaryVid)
{
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (i4SetValDot1agCfmVlanPrimaryVid);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmVlanRowStatus
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid

                The Object 
                setValDot1agCfmVlanRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmVlanRowStatus (UINT4 u4Dot1agCfmVlanComponentId,
                              INT4 i4Dot1agCfmVlanVid,
                              INT4 i4SetValDot1agCfmVlanRowStatus)
{
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (i4SetValDot1agCfmVlanRowStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmVlanPrimaryVid
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid

                The Object 
                testValDot1agCfmVlanPrimaryVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmVlanPrimaryVid (UINT4 *pu4ErrorCode,
                                  UINT4 u4Dot1agCfmVlanComponentId,
                                  INT4 i4Dot1agCfmVlanVid,
                                  INT4 i4TestValDot1agCfmVlanPrimaryVid)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (i4TestValDot1agCfmVlanPrimaryVid);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmVlanRowStatus
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid

                The Object 
                testValDot1agCfmVlanRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmVlanRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Dot1agCfmVlanComponentId,
                                 INT4 i4Dot1agCfmVlanVid,
                                 INT4 i4TestValDot1agCfmVlanRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmVlanComponentId);
    UNUSED_PARAM (i4Dot1agCfmVlanVid);
    UNUSED_PARAM (i4TestValDot1agCfmVlanRowStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmVlanTable
 Input       :  The Indices
                Dot1agCfmVlanComponentId
                Dot1agCfmVlanVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmVlanTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/******************************************************************************/
/*                           End  of file cfmvlnlw.c                          */
/******************************************************************************/
