/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmsz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ECFMSZ_C
#include "cfminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
EcfmSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ECFM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsECFMSizingParams[i4SizingId].u4StructSize,
                              FsECFMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(ECFMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            EcfmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
EcfmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsECFMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ECFMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
EcfmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ECFM_MAX_SIZING_ID; i4SizingId++)
    {
        if (ECFMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ECFMMemPoolIds[i4SizingId]);
            ECFMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
