/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: fscfmv2elw.c,v 1.5 2012/01/20 13:18:33 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include  "cfminc.h"

/* LOW LEVEL Routines for Table : FsMIEcfmExtStackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmExtStackTable
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmExtStackTable (INT4 i4FsMIEcfmExtStackIfIndex,
                                               INT4
                                               i4FsMIEcfmExtStackServiceSelectorType,
                                               UINT4
                                               u4FsMIEcfmExtStackServiceSelectorOrNone,
                                               INT4 i4FsMIEcfmExtStackMdLevel,
                                               INT4 i4FsMIEcfmExtStackDirection)
{
    INT1                i1RetVal;

    i1RetVal =
        nmhValidateIndexInstanceIeee8021CfmStackTable
        (i4FsMIEcfmExtStackIfIndex, i4FsMIEcfmExtStackServiceSelectorType,
         u4FsMIEcfmExtStackServiceSelectorOrNone, i4FsMIEcfmExtStackMdLevel,
         i4FsMIEcfmExtStackDirection);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmExtStackTable
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmExtStackTable (INT4 *pi4FsMIEcfmExtStackIfIndex,
                                       INT4
                                       *pi4FsMIEcfmExtStackServiceSelectorType,
                                       UINT4
                                       *pu4FsMIEcfmExtStackServiceSelectorOrNone,
                                       INT4 *pi4FsMIEcfmExtStackMdLevel,
                                       INT4 *pi4FsMIEcfmExtStackDirection)
{
    return (nmhGetNextIndexFsMIEcfmExtStackTable (0, pi4FsMIEcfmExtStackIfIndex,
                                                  0,
                                                  pi4FsMIEcfmExtStackServiceSelectorType,
                                                  0,
                                                  pu4FsMIEcfmExtStackServiceSelectorOrNone,
                                                  0, pi4FsMIEcfmExtStackMdLevel,
                                                  0,
                                                  pi4FsMIEcfmExtStackDirection));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmExtStackTable
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                nextFsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                nextFsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                nextFsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                nextFsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection
                nextFsMIEcfmExtStackDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmExtStackTable (INT4 i4FsMIEcfmExtStackIfIndex,
                                      INT4 *pi4NextFsMIEcfmExtStackIfIndex,
                                      INT4
                                      i4FsMIEcfmExtStackServiceSelectorType,
                                      INT4
                                      *pi4NextFsMIEcfmExtStackServiceSelectorType,
                                      UINT4
                                      u4FsMIEcfmExtStackServiceSelectorOrNone,
                                      UINT4
                                      *pu4NextFsMIEcfmExtStackServiceSelectorOrNone,
                                      INT4 i4FsMIEcfmExtStackMdLevel,
                                      INT4 *pi4NextFsMIEcfmExtStackMdLevel,
                                      INT4 i4FsMIEcfmExtStackDirection,
                                      INT4 *pi4NextFsMIEcfmExtStackDirection)
{
    if (nmhGetNextIndexIeee8021CfmStackTable (i4FsMIEcfmExtStackIfIndex,
                                              pi4NextFsMIEcfmExtStackIfIndex,
                                              i4FsMIEcfmExtStackServiceSelectorType,
                                              pi4NextFsMIEcfmExtStackServiceSelectorType,
                                              u4FsMIEcfmExtStackServiceSelectorOrNone,
                                              pu4NextFsMIEcfmExtStackServiceSelectorOrNone,
                                              i4FsMIEcfmExtStackMdLevel,
                                              pi4NextFsMIEcfmExtStackMdLevel,
                                              i4FsMIEcfmExtStackDirection,
                                              pi4NextFsMIEcfmExtStackDirection)
        != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtStackMdIndex
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection

                The Object 
                retValFsMIEcfmExtStackMdIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtStackMdIndex (INT4 i4FsMIEcfmExtStackIfIndex,
                               INT4 i4FsMIEcfmExtStackServiceSelectorType,
                               UINT4 u4FsMIEcfmExtStackServiceSelectorOrNone,
                               INT4 i4FsMIEcfmExtStackMdLevel,
                               INT4 i4FsMIEcfmExtStackDirection,
                               UINT4 *pu4RetValFsMIEcfmExtStackMdIndex)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetIeee8021CfmStackMdIndex (i4FsMIEcfmExtStackIfIndex,
                                              i4FsMIEcfmExtStackServiceSelectorType,
                                              u4FsMIEcfmExtStackServiceSelectorOrNone,
                                              i4FsMIEcfmExtStackMdLevel,
                                              i4FsMIEcfmExtStackDirection,
                                              pu4RetValFsMIEcfmExtStackMdIndex);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtStackMaIndex
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection

                The Object 
                retValFsMIEcfmExtStackMaIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtStackMaIndex (INT4 i4FsMIEcfmExtStackIfIndex,
                               INT4 i4FsMIEcfmExtStackServiceSelectorType,
                               UINT4 u4FsMIEcfmExtStackServiceSelectorOrNone,
                               INT4 i4FsMIEcfmExtStackMdLevel,
                               INT4 i4FsMIEcfmExtStackDirection,
                               UINT4 *pu4RetValFsMIEcfmExtStackMaIndex)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetIeee8021CfmStackMaIndex (i4FsMIEcfmExtStackIfIndex,
                                              i4FsMIEcfmExtStackServiceSelectorType,
                                              u4FsMIEcfmExtStackServiceSelectorOrNone,
                                              i4FsMIEcfmExtStackMdLevel,
                                              i4FsMIEcfmExtStackDirection,
                                              pu4RetValFsMIEcfmExtStackMaIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtStackMepId
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection

                The Object 
                retValFsMIEcfmExtStackMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtStackMepId (INT4 i4FsMIEcfmExtStackIfIndex,
                             INT4 i4FsMIEcfmExtStackServiceSelectorType,
                             UINT4 u4FsMIEcfmExtStackServiceSelectorOrNone,
                             INT4 i4FsMIEcfmExtStackMdLevel,
                             INT4 i4FsMIEcfmExtStackDirection,
                             UINT4 *pu4RetValFsMIEcfmExtStackMepId)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetIeee8021CfmStackMepId (i4FsMIEcfmExtStackIfIndex,
                                            i4FsMIEcfmExtStackServiceSelectorType,
                                            u4FsMIEcfmExtStackServiceSelectorOrNone,
                                            i4FsMIEcfmExtStackMdLevel,
                                            i4FsMIEcfmExtStackDirection,
                                            pu4RetValFsMIEcfmExtStackMepId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtStackMacAddress
 Input       :  The Indices
                FsMIEcfmExtStackIfIndex
                FsMIEcfmExtStackServiceSelectorType
                FsMIEcfmExtStackServiceSelectorOrNone
                FsMIEcfmExtStackMdLevel
                FsMIEcfmExtStackDirection

                The Object 
                retValFsMIEcfmExtStackMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtStackMacAddress (INT4 i4FsMIEcfmExtStackIfIndex,
                                  INT4 i4FsMIEcfmExtStackServiceSelectorType,
                                  UINT4 u4FsMIEcfmExtStackServiceSelectorOrNone,
                                  INT4 i4FsMIEcfmExtStackMdLevel,
                                  INT4 i4FsMIEcfmExtStackDirection,
                                  tMacAddr * pRetValFsMIEcfmExtStackMacAddress)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetIeee8021CfmStackMacAddress (i4FsMIEcfmExtStackIfIndex,
                                                 i4FsMIEcfmExtStackServiceSelectorType,
                                                 u4FsMIEcfmExtStackServiceSelectorOrNone,
                                                 i4FsMIEcfmExtStackMdLevel,
                                                 i4FsMIEcfmExtStackDirection,
                                                 pRetValFsMIEcfmExtStackMacAddress);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIEcfmExtDefaultMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable (UINT4 u4FsMIEcfmContextId,
                                                   INT4
                                                   i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                                   UINT4
                                                   u4FsMIEcfmExtDefaultMdPrimarySelector)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmExtDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmExtDefaultMdTable (UINT4 *pu4FsMIEcfmContextId,
                                           INT4
                                           *pi4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                           UINT4
                                           *pu4FsMIEcfmExtDefaultMdPrimarySelector)
{
    if (nmhGetNextIndexFsMIEcfmExtDefaultMdTable
        (0, pu4FsMIEcfmContextId,
         0, pi4FsMIEcfmExtDefaultMdPrimarySelectorType,
         0, pu4FsMIEcfmExtDefaultMdPrimarySelector) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmExtDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                nextFsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector
                nextFsMIEcfmExtDefaultMdPrimarySelector
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmExtDefaultMdTable (UINT4 u4FsMIEcfmContextId,
                                          UINT4 *pu4NextFsMIEcfmContextId,
                                          INT4
                                          i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                          INT4
                                          *pi4NextFsMIEcfmExtDefaultMdPrimarySelectorType,
                                          UINT4
                                          u4FsMIEcfmExtDefaultMdPrimarySelector,
                                          UINT4
                                          *pu4NextFsMIEcfmExtDefaultMdPrimarySelector)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexIeee8021CfmDefaultMdTable
            (u4FsMIEcfmContextId,
             pu4NextFsMIEcfmContextId,
             i4FsMIEcfmExtDefaultMdPrimarySelectorType,
             pi4NextFsMIEcfmExtDefaultMdPrimarySelectorType,
             u4FsMIEcfmExtDefaultMdPrimarySelector,
             pu4NextFsMIEcfmExtDefaultMdPrimarySelector) == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexIeee8021CfmDefaultMdTable
           (pu4NextFsMIEcfmContextId,
            pi4NextFsMIEcfmExtDefaultMdPrimarySelectorType,
            pu4NextFsMIEcfmExtDefaultMdPrimarySelector) != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtDefaultMdStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                retValFsMIEcfmExtDefaultMdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtDefaultMdStatus (UINT4 u4FsMIEcfmContextId,
                                  INT4
                                  i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                  UINT4 u4FsMIEcfmExtDefaultMdPrimarySelector,
                                  INT4 *pi4RetValFsMIEcfmExtDefaultMdStatus)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmDefaultMdStatus
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         pi4RetValFsMIEcfmExtDefaultMdStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtDefaultMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                retValFsMIEcfmExtDefaultMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtDefaultMdLevel (UINT4 u4FsMIEcfmContextId,
                                 INT4 i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                 UINT4 u4FsMIEcfmExtDefaultMdPrimarySelector,
                                 INT4 *pi4RetValFsMIEcfmExtDefaultMdLevel)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIeee8021CfmDefaultMdLevel
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         pi4RetValFsMIEcfmExtDefaultMdLevel);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtDefaultMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                retValFsMIEcfmExtDefaultMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtDefaultMdMhfCreation (UINT4 u4FsMIEcfmContextId,
                                       INT4
                                       i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                       UINT4
                                       u4FsMIEcfmExtDefaultMdPrimarySelector,
                                       INT4
                                       *pi4RetValFsMIEcfmExtDefaultMdMhfCreation)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmDefaultMdMhfCreation
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         pi4RetValFsMIEcfmExtDefaultMdMhfCreation);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtDefaultMdIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                retValFsMIEcfmExtDefaultMdIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtDefaultMdIdPermission (UINT4 u4FsMIEcfmContextId,
                                        INT4
                                        i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                        UINT4
                                        u4FsMIEcfmExtDefaultMdPrimarySelector,
                                        INT4
                                        *pi4RetValFsMIEcfmExtDefaultMdIdPermission)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmDefaultMdIdPermission
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         pi4RetValFsMIEcfmExtDefaultMdIdPermission);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtDefaultMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                setValFsMIEcfmExtDefaultMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtDefaultMdLevel (UINT4 u4FsMIEcfmContextId,
                                 INT4 i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                 UINT4 u4FsMIEcfmExtDefaultMdPrimarySelector,
                                 INT4 i4SetValFsMIEcfmExtDefaultMdLevel)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetIeee8021CfmDefaultMdLevel
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         i4SetValFsMIEcfmExtDefaultMdLevel);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtDefaultMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                setValFsMIEcfmExtDefaultMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtDefaultMdMhfCreation (UINT4 u4FsMIEcfmContextId,
                                       INT4
                                       i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                       UINT4
                                       u4FsMIEcfmExtDefaultMdPrimarySelector,
                                       INT4
                                       i4SetValFsMIEcfmExtDefaultMdMhfCreation)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmDefaultMdMhfCreation
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         i4SetValFsMIEcfmExtDefaultMdMhfCreation);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtDefaultMdIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                setValFsMIEcfmExtDefaultMdIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtDefaultMdIdPermission (UINT4 u4FsMIEcfmContextId,
                                        INT4
                                        i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                        UINT4
                                        u4FsMIEcfmExtDefaultMdPrimarySelector,
                                        INT4
                                        i4SetValFsMIEcfmExtDefaultMdIdPermission)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmDefaultMdIdPermission
        (u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         i4SetValFsMIEcfmExtDefaultMdIdPermission);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtDefaultMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                testValFsMIEcfmExtDefaultMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtDefaultMdLevel (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    INT4
                                    i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                    UINT4 u4FsMIEcfmExtDefaultMdPrimarySelector,
                                    INT4 i4TestValFsMIEcfmExtDefaultMdLevel)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Ieee8021CfmDefaultMdLevel
        (pu4ErrorCode,
         u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         i4TestValFsMIEcfmExtDefaultMdLevel);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtDefaultMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                testValFsMIEcfmExtDefaultMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtDefaultMdMhfCreation (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIEcfmContextId,
                                          INT4
                                          i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                          UINT4
                                          u4FsMIEcfmExtDefaultMdPrimarySelector,
                                          INT4
                                          i4TestValFsMIEcfmExtDefaultMdMhfCreation)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Ieee8021CfmDefaultMdMhfCreation
        (pu4ErrorCode,
         u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         i4TestValFsMIEcfmExtDefaultMdMhfCreation);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtDefaultMdIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector

                The Object 
                testValFsMIEcfmExtDefaultMdIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtDefaultMdIdPermission (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMIEcfmContextId,
                                           INT4
                                           i4FsMIEcfmExtDefaultMdPrimarySelectorType,
                                           UINT4
                                           u4FsMIEcfmExtDefaultMdPrimarySelector,
                                           INT4
                                           i4TestValFsMIEcfmExtDefaultMdIdPermission)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Ieee8021CfmDefaultMdIdPermission
        (pu4ErrorCode,
         u4FsMIEcfmContextId,
         i4FsMIEcfmExtDefaultMdPrimarySelectorType,
         u4FsMIEcfmExtDefaultMdPrimarySelector,
         i4TestValFsMIEcfmExtDefaultMdIdPermission);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmExtDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtDefaultMdPrimarySelectorType
                FsMIEcfmExtDefaultMdPrimarySelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmExtDefaultMdTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmExtConfigErrorListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmExtConfigErrorListTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtConfigErrorListSelectorType
                FsMIEcfmExtConfigErrorListSelector
                FsMIEcfmExtConfigErrorListIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmExtConfigErrorListTable (INT4
                                                         i4FsMIEcfmExtConfigErrorListSelectorType,
                                                         UINT4
                                                         u4FsMIEcfmExtConfigErrorListSelector,
                                                         INT4
                                                         i4FsMIEcfmExtConfigErrorListIfIndex)
{
    INT1                i1RetVal;

    i1RetVal =
        nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable
        (i4FsMIEcfmExtConfigErrorListSelectorType,
         u4FsMIEcfmExtConfigErrorListSelector,
         i4FsMIEcfmExtConfigErrorListIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmExtConfigErrorListTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtConfigErrorListSelectorType
                FsMIEcfmExtConfigErrorListSelector
                FsMIEcfmExtConfigErrorListIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmExtConfigErrorListTable (INT4
                                                 *pi4FsMIEcfmExtConfigErrorListSelectorType,
                                                 UINT4
                                                 *pu4FsMIEcfmExtConfigErrorListSelector,
                                                 INT4
                                                 *pi4FsMIEcfmExtConfigErrorListIfIndex)
{
    return (nmhGetNextIndexFsMIEcfmExtConfigErrorListTable
            (0, pi4FsMIEcfmExtConfigErrorListSelectorType, 0,
             pu4FsMIEcfmExtConfigErrorListSelector, 0,
             pi4FsMIEcfmExtConfigErrorListIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmExtConfigErrorListTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmExtConfigErrorListSelectorType
                nextFsMIEcfmExtConfigErrorListSelectorType
                FsMIEcfmExtConfigErrorListSelector
                nextFsMIEcfmExtConfigErrorListSelector
                FsMIEcfmExtConfigErrorListIfIndex
                nextFsMIEcfmExtConfigErrorListIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmExtConfigErrorListTable (INT4
                                                i4FsMIEcfmExtConfigErrorListSelectorType,
                                                INT4
                                                *pi4NextFsMIEcfmExtConfigErrorListSelectorType,
                                                UINT4
                                                u4FsMIEcfmExtConfigErrorListSelector,
                                                UINT4
                                                *pu4NextFsMIEcfmExtConfigErrorListSelector,
                                                INT4
                                                i4FsMIEcfmExtConfigErrorListIfIndex,
                                                INT4
                                                *pi4NextFsMIEcfmExtConfigErrorListIfIndex)
{

    if (nmhGetNextIndexIeee8021CfmConfigErrorListTable
        (i4FsMIEcfmExtConfigErrorListSelectorType,
         pi4NextFsMIEcfmExtConfigErrorListSelectorType,
         u4FsMIEcfmExtConfigErrorListSelector,
         pu4NextFsMIEcfmExtConfigErrorListSelector,
         i4FsMIEcfmExtConfigErrorListIfIndex,
         pi4NextFsMIEcfmExtConfigErrorListIfIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtConfigErrorListErrorType
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtConfigErrorListSelectorType
                FsMIEcfmExtConfigErrorListSelector
                FsMIEcfmExtConfigErrorListIfIndex

                The Object 
                retValFsMIEcfmExtConfigErrorListErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtConfigErrorListErrorType (INT4
                                           i4FsMIEcfmExtConfigErrorListSelectorType,
                                           UINT4
                                           u4FsMIEcfmExtConfigErrorListSelector,
                                           INT4
                                           i4FsMIEcfmExtConfigErrorListIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValFsMIEcfmExtConfigErrorListErrorType)
{
    INT1                i1RetVal;

    i1RetVal =
        nmhGetIeee8021CfmConfigErrorListErrorType
        (i4FsMIEcfmExtConfigErrorListSelectorType,
         u4FsMIEcfmExtConfigErrorListSelector,
         i4FsMIEcfmExtConfigErrorListIfIndex,
         pRetValFsMIEcfmExtConfigErrorListErrorType);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIEcfmExtMaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmExtMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmExtMaTable (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmExtMaIndex,
                                            INT4
                                            i4FsMIEcfmExtMaPrimarySelectorType,
                                            UINT4
                                            u4FsMIEcfmExtMaPrimarySelectorOrNone)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1agCfmMaNetTable (u4FsMIEcfmMdIndex,
                                                            u4FsMIEcfmExtMaIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmExtMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmExtMaTable (UINT4 *pu4FsMIEcfmContextId,
                                    UINT4 *pu4FsMIEcfmMdIndex,
                                    UINT4 *pu4FsMIEcfmExtMaIndex,
                                    INT4 *pi4FsMIEcfmExtMaPrimarySelectorType,
                                    UINT4
                                    *pu4FsMIEcfmExtMaPrimarySelectorOrNone)
{
    return (nmhGetNextIndexFsMIEcfmExtMaTable (0, pu4FsMIEcfmContextId,
                                               0, pu4FsMIEcfmMdIndex,
                                               0, pu4FsMIEcfmExtMaIndex,
                                               0,
                                               pi4FsMIEcfmExtMaPrimarySelectorType,
                                               0,
                                               pu4FsMIEcfmExtMaPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmExtMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                nextFsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                nextFsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone
                nextFsMIEcfmExtMaPrimarySelectorOrNone
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmExtMaTable (UINT4 u4FsMIEcfmContextId,
                                   UINT4 *pu4NextFsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 *pu4NextFsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   UINT4 *pu4NextFsMIEcfmExtMaIndex,
                                   INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                   INT4
                                   *pi4NextFsMIEcfmExtMaPrimarySelectorType,
                                   UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                   UINT4
                                   *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone)
{
    UINT4               u4ContextId;
    UINT4               u4VlanIdIsidOrNone;
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        /* If MaPrimarySelectorType or MaPrimarySelectorOrNone is equal to
         * zero, then get the MaPrimarySelectorType and
         * MaPrimarySelectorOrNone from the MA and return this as the next
         * valid index.
         */
        if ((i4FsMIEcfmExtMaPrimarySelectorType == 0) ||
            (u4FsMIEcfmExtMaPrimarySelectorOrNone == 0))
        {
            pMaInfo = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex);
            do
            {
                if (pMaInfo == NULL)
                {
                    break;
                }

                if (i4FsMIEcfmExtMaPrimarySelectorType == 0)
                {
                    if (pMaInfo->u1SelectorType == 0)
                    {
                        *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                            ECFM_SERVICE_SELECTION_VLAN;
                        *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone =
                            pMaInfo->u4PrimaryVidIsid;
                    }
                    else
                    {
                        *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                            pMaInfo->u1SelectorType;
                        *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone =
                            pMaInfo->u4PrimaryVidIsid;
                    }
                }
                else            /* Selector0rNone == zero */
                {
                    /* SelecctorOrNone is always zero for LSP/PW, hence
                     * get the next entry.
                     */
                    if ((pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_LSP)
                        || (pMaInfo->u1SelectorType ==
                            ECFM_SERVICE_SELECTION_PW))
                    {
                        break;
                    }
                    *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                        i4FsMIEcfmExtMaPrimarySelectorType;
                    *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone =
                        pMaInfo->u4PrimaryVidIsid;
                }

                *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;
                *pu4NextFsMIEcfmMdIndex = u4FsMIEcfmMdIndex;
                *pu4NextFsMIEcfmExtMaIndex = u4FsMIEcfmExtMaIndex;

                ECFM_CC_RELEASE_CONTEXT ();
                return SNMP_SUCCESS;

            }
            while (0);
        }

        if (nmhGetNextIndexDot1agCfmMaNetTable (u4FsMIEcfmMdIndex,
                                                pu4NextFsMIEcfmMdIndex,
                                                u4FsMIEcfmExtMaIndex,
                                                pu4NextFsMIEcfmExtMaIndex) ==
            SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            if (nmhGetIeee8021CfmMaCompPrimarySelectorOrNone
                (u4FsMIEcfmContextId, *pu4NextFsMIEcfmMdIndex,
                 *pu4NextFsMIEcfmExtMaIndex,
                 &u4VlanIdIsidOrNone) == SNMP_FAILURE)
            {
                ECFM_CC_RELEASE_CONTEXT ();
                return SNMP_FAILURE;
            }

            if (u4VlanIdIsidOrNone >= ECFM_INTERNAL_ISID_MIN)
            {
                *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                    ECFM_SERVICE_SELECTION_ISID;
                *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone =
                    ECFM_ISID_INTERNAL_TO_ISID (u4VlanIdIsidOrNone);
            }
            else
            {
                /* Get MA entry corresponding to indices next to MdIndex, 
                 * MaIndex
                 */
                ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
                MaInfo.u4MdIndex = *pu4NextFsMIEcfmMdIndex;
                MaInfo.u4MaIndex = *pu4NextFsMIEcfmExtMaIndex;
                pMaNode = (tEcfmCcMaInfo *) RBTreeGet
                    (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo);
                if (pMaNode == NULL)
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "\tSNMP: No Maintaince Association for "
                                 "given Indices\n");
                    ECFM_CC_RELEASE_CONTEXT ();
                    return SNMP_FAILURE;
                }

                if ((pMaNode->u1SelectorType ==
                     ECFM_SERVICE_SELECTION_LSP) ||
                    (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
                {
                    *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                        pMaNode->u1SelectorType;
                    /* For LSP/PW the associated LSP/PW can be retrieved from
                     * MPLSTP Remote MEP table. Hence the value is kept as none.
                     */
                    *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone = 0;
                }
                else
                {
                    *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                        ECFM_SERVICE_SELECTION_VLAN;
                }
            }

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;

        *pu4NextFsMIEcfmContextId = u4ContextId;
    }
    while (nmhGetFirstIndexDot1agCfmMaNetTable (pu4NextFsMIEcfmMdIndex,
                                                pu4NextFsMIEcfmExtMaIndex) !=
           SNMP_SUCCESS);

    if (nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (u4FsMIEcfmContextId,
                                                      *pu4NextFsMIEcfmMdIndex,
                                                      *pu4NextFsMIEcfmExtMaIndex,
                                                      &u4VlanIdIsidOrNone) ==
        SNMP_FAILURE)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;

    }

    if (u4VlanIdIsidOrNone >= ECFM_INTERNAL_ISID_MIN)
    {
        *pi4NextFsMIEcfmExtMaPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
        *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone =
            ECFM_ISID_INTERNAL_TO_ISID (u4VlanIdIsidOrNone);
    }
    else
    {
        /* Get MA entry corresponding to indices next to MdIndex, MaIndex */
        ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
        MaInfo.u4MdIndex = *pu4NextFsMIEcfmMdIndex;
        MaInfo.u4MaIndex = *pu4NextFsMIEcfmExtMaIndex;
        pMaNode = (tEcfmCcMaInfo *) RBTreeGet
            (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo);
        if (pMaNode == NULL)
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: No Maintaince Association for "
                         "given Indices\n");
            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_FAILURE;
        }

        if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
            (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
        {
            *pi4NextFsMIEcfmExtMaPrimarySelectorType = pMaNode->u1SelectorType;
            /* For LSP/PW the associated LSP/PW can be retrieved from
             * MPLS TP Remote MEP table. Hence the value is kept as none.
             */
            *pu4NextFsMIEcfmExtMaPrimarySelectorOrNone = 0;
        }
        else
        {
            *pi4NextFsMIEcfmExtMaPrimarySelectorType =
                ECFM_SERVICE_SELECTION_VLAN;
        }
    }

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaFormat (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmExtMaIndex,
                           INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                           UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                           INT4 *pi4RetValFsMIEcfmExtMaFormat)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaNetFormat (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                    pi4RetValFsMIEcfmExtMaFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaName (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmExtMaIndex,
                         INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                         UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmExtMaName)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaNetName (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                  pRetValFsMIEcfmExtMaName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaMhfCreation (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                INT4 *pi4RetValFsMIEcfmExtMaMhfCreation)
{
    INT1                i1RetVal;

    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmMaCompMhfCreation (u4FsMIEcfmContextId,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            pi4RetValFsMIEcfmExtMaMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaIdPermission (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                 UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                 INT4 *pi4RetValFsMIEcfmExtMaIdPermission)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmMaCompIdPermission (u4FsMIEcfmContextId,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             pi4RetValFsMIEcfmExtMaIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaCcmInterval
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaCcmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaCcmInterval (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                INT4 *pi4RetValFsMIEcfmExtMaCcmInterval)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaNetCcmInterval (u4FsMIEcfmMdIndex,
                                         u4FsMIEcfmExtMaIndex,
                                         pi4RetValFsMIEcfmExtMaCcmInterval);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaNumberOfVids
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaNumberOfVids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaNumberOfVids (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                 UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                 UINT4 *pu4RetValFsMIEcfmExtMaNumberOfVids)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmMaCompNumberOfVids (u4FsMIEcfmContextId,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             pu4RetValFsMIEcfmExtMaNumberOfVids);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaRowStatus (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                              UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                              INT4 *pi4RetValFsMIEcfmExtMaRowStatus)
{
    INT1                i1RetVal;
    INT1                i1CompRetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaNetRowStatus (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                       pi4RetValFsMIEcfmExtMaRowStatus);
    i1CompRetVal =
        nmhGetIeee8021CfmMaCompRowStatus (u4FsMIEcfmContextId,
                                          u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          pi4RetValFsMIEcfmExtMaRowStatus);

    if ((SNMP_SUCCESS == i1RetVal) && (SNMP_SUCCESS == i1CompRetVal))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    else
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMaCrosscheckStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                retValFsMIEcfmExtMaCrosscheckStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMaCrosscheckStatus (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                     UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                     INT4
                                     *pi4RetValFsMIEcfmExtMaCrosscheckStatus)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMaPrimarySelectorOrNone;

    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    }
    i1RetVal = nmhGetFsMIEcfmMaCrosscheckStatus (u4FsMIEcfmContextId,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmExtMaIndex,
                                                 pi4RetValFsMIEcfmExtMaCrosscheckStatus);
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMaFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaFormat (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmExtMaIndex,
                           INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                           UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                           INT4 i4SetValFsMIEcfmExtMaFormat)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4VlanIdIsidOrNone =
        u4FsMIEcfmExtMaPrimarySelectorOrNone;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
    if ((pMaNode->u1SelectorType == i4FsMIEcfmExtMaPrimarySelectorType) &&
        (pMaNode->u4PrimaryVidIsid == (UINT4) i4VlanIdIsidOrNone))
    {
        /* MA entry corresponding to indices MdIndex, MaIndex exists */
        /* Set the Ma name format to corresponding MA entry */
        pMaNode->u1NameFormat = (UINT1) i4SetValFsMIEcfmExtMaFormat;
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMaName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaName (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmExtMaIndex,
                         INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                         UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsMIEcfmExtMaName)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4VlanIdIsidOrNone =
        u4FsMIEcfmExtMaPrimarySelectorOrNone;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
    if ((pMaNode->u1SelectorType == (UINT1) i4FsMIEcfmExtMaPrimarySelectorType)
        && (pMaNode->u4PrimaryVidIsid == (UINT4) i4VlanIdIsidOrNone))
    {
        ECFM_MEMSET (pMaNode->au1Name, ECFM_INIT_VAL, ECFM_MA_NAME_MAX_LEN);
        ECFM_MEMCPY (pMaNode->au1Name, pSetValFsMIEcfmExtMaName->pu1_OctetList,
                     pSetValFsMIEcfmExtMaName->i4_Length);
        pMaNode->u1NameLength = pSetValFsMIEcfmExtMaName->i4_Length;

        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMaMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaMhfCreation (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                INT4 i4SetValFsMIEcfmExtMaMhfCreation)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4VlanIdIsidOrNone =
        u4FsMIEcfmExtMaPrimarySelectorOrNone;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
    if ((pMaNode->u1SelectorType == (UINT1) i4FsMIEcfmExtMaPrimarySelectorType)
        && (pMaNode->u4PrimaryVidIsid == (UINT4) i4VlanIdIsidOrNone))
    {
        /*Set the Mhf Creation criteria value to corresponding MA entry */
        pMaNode->u1MhfCreation = (UINT1) i4SetValFsMIEcfmExtMaMhfCreation;
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMaIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaIdPermission (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                 UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                 INT4 i4SetValFsMIEcfmExtMaIdPermission)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4VlanIdIsidOrNone =
        u4FsMIEcfmExtMaPrimarySelectorOrNone;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
    if ((pMaNode->u1SelectorType == (UINT1) i4FsMIEcfmExtMaPrimarySelectorType)
        && (pMaNode->u4PrimaryVidIsid == (UINT4) i4VlanIdIsidOrNone))
    {
        /* Set SenderId permission to corresponding MA entry */
        pMaNode->u1IdPermission = (UINT1) i4SetValFsMIEcfmExtMaIdPermission;
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMaCcmInterval
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaCcmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaCcmInterval (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                INT4 i4SetValFsMIEcfmExtMaCcmInterval)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4VlanIdIsidOrNone =
        u4FsMIEcfmExtMaPrimarySelectorOrNone;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    }

    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }
    if ((pMaNode->u1SelectorType == (UINT1) i4FsMIEcfmExtMaPrimarySelectorType)
        && (pMaNode->u4PrimaryVidIsid == (UINT4) i4VlanIdIsidOrNone))
    {
        /* Set the CcmInterval value */
        pMaNode->u1CcmInterval = (UINT1) i4SetValFsMIEcfmExtMaCcmInterval;
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMaRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaRowStatus (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                              UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                              INT4 i4SetValFsMIEcfmExtMaRowStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaNewNode = NULL;
    UINT2               u2NumberOfVids = ECFM_INIT_VAL;
    UINT2               u2VlanId = ECFM_INIT_VAL;
    UINT1               u1MdLevel = ECFM_INIT_VAL;
    UINT1               au1DefaultMaName[] = "DEFAULT";
    BOOL1               b1VlanAwareMa = ECFM_FALSE;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Get MD Entry for the given indices */
    pMdNode = EcfmSnmpLwGetMdEntry (u4FsMIEcfmMdIndex);
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC,
                     "nmhSetDot1agCfmMaRowStatus: No MD Entry for the"
                     " given index\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    /* Get MA entry corresponding to indices MdIndex, MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);
    if (pMaNode != NULL)
    {
        /* MA entry corresponding to indices MdIndex, MaIndex exists,
         * and MA's row status is same as user wants to set */
        if (pMaNode->u1RowStatus == (UINT1) i4SetValFsMIEcfmExtMaRowStatus)
        {
            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    else if ((i4SetValFsMIEcfmExtMaRowStatus != ECFM_ROW_STATUS_CREATE_AND_WAIT)
             && (i4SetValFsMIEcfmExtMaRowStatus !=
                 ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIEcfmExtMaRowStatus != ECFM_ROW_STATUS_NOT_IN_SERVICE)
    {
        pMdNode = EcfmSnmpLwGetMdEntry (u4FsMIEcfmMdIndex);
        if (pMdNode == NULL)
        {
            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsMIEcfmExtMaRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
            pMaNode->u1RowStatus = (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE;
            pMaNode->u1CompRowStatus = (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE;
            /* Update all MEPs row status, associated with MA */
            EcfmSnmpLwUpdateAllMepRowStatus (pMaNode);
            break;

        case ECFM_ROW_STATUS_ACTIVE:
            if (pMaNode->u1RowStatus != (UINT1) ECFM_ROW_STATUS_NOT_IN_SERVICE)
            {
                ECFM_CC_RELEASE_CONTEXT ();
                return SNMP_FAILURE;
            }
            if (ECFM_ANY_MEP_CONFIGURED_IN_MA (pMaNode) != ECFM_TRUE)
            {
                /* Set number of VlanIds as depending upon no. of entries 
                 * found in VLAN table with MA's PrimaryVid */
                if (pMaNode->u4PrimaryVidIsid == 0)
                {
                    pMaNode->u2NumberOfVids = 0;
                }
                else
                {
                    u2NumberOfVids =
                        EcfmSnmpLwGetNoOfEntriesForPVid (pMaNode->
                                                         u4PrimaryVidIsid);
                    if (u2NumberOfVids == 0)
                    {
                        pMaNode->u2NumberOfVids = 1;
                    }
                    else
                    {
                        pMaNode->u2NumberOfVids = u2NumberOfVids +
                            (UINT2) ECFM_INCR_VAL;
                    }
                }
            }
            /* Set MA row status ACTIVE */
            pMaNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
            pMaNode->u1CompRowStatus = ECFM_ROW_STATUS_ACTIVE;

            /* Setting all associated MEPs row status to ACTIVE */
            EcfmSnmpLwUpdateAllMepRowStatus (pMaNode);

            /* Update DefaultMd status corresponding to MA primaryVid and
             * level and 
             Evaluate for MIP creation for a change of MA, VlanId */
            if (pMaNode->u4PrimaryVidIsid != 0)
            {
                /* If UP MEP associated with this MA exists then only update 
                 * default MdStatus */
                if (EcfmIsMepAssocWithMa (pMaNode->u4MdIndex,
                                          pMaNode->u4MaIndex,
                                          -1, ECFM_MP_DIR_UP) == ECFM_TRUE)
                {
                    EcfmSnmpLwUpdateDefaultMdStatus (pMdNode->u1Level,
                                                     pMaNode->u4PrimaryVidIsid,
                                                     ECFM_FALSE);
                }

                EcfmCcUtilEvaluateAndCreateMip (-1, pMaNode->u4PrimaryVidIsid,
                                                ECFM_TRUE);
            }

            /* Update the MA at LBLT task also */
            EcfmLbLtAddMaEntry (ECFM_CC_CURR_CONTEXT_ID (), pMaNode);
            break;
        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
        case ECFM_ROW_STATUS_CREATE_AND_GO:
            /* User wants to create a new MA entry with MdIndex and MaIndex */
            /* Create a node for MA entry that needs to be created */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MA_TABLE (pMaNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "nmhSetDot1agCfmMaRowStatus: Row Status Create and Go"
                             "Allocation for MA Node Failed \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                ECFM_CC_RELEASE_CONTEXT ();
                return SNMP_FAILURE;
            }
            ECFM_MEMSET (pMaNewNode, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);

            /* Put MA  index in new MA node */
            pMaNewNode->u4MaIndex = u4FsMIEcfmExtMaIndex;
            /* Put MD  index in new MA node */
            pMaNewNode->u4MdIndex = u4FsMIEcfmMdIndex;
            pMaNewNode->pMdInfo = pMdNode;
            /* put other default values */
            ECFM_MEMCPY (pMaNewNode->au1Name, au1DefaultMaName,
                         sizeof (au1DefaultMaName));
            pMaNewNode->u1CcmInterval = ECFM_CCM_INTERVAL_1_S;
            pMaNewNode->u1MhfCreation = ECFM_MHF_CRITERIA_DEFER;
            pMaNewNode->u1IdPermission = ECFM_SENDER_ID_DEFER;
            if (i4FsMIEcfmExtMaPrimarySelectorType ==
                ECFM_SERVICE_SELECTION_ISID)
            {
                pMaNewNode->u4PrimaryVidIsid =
                    ECFM_ISID_TO_ISID_INTERNAL
                    (u4FsMIEcfmExtMaPrimarySelectorOrNone);
                pMaNewNode->u1SelectorType = ECFM_SERVICE_SELECTION_ISID;
            }
            else if (i4FsMIEcfmExtMaPrimarySelectorType ==
                     ECFM_SERVICE_SELECTION_LSP)
            {
                pMaNewNode->u1SelectorType = ECFM_SERVICE_SELECTION_LSP;
                pMaNewNode->u4PrimaryVidIsid = ECFM_INIT_VAL;
            }
            else if (i4FsMIEcfmExtMaPrimarySelectorType ==
                     ECFM_SERVICE_SELECTION_PW)
            {
                pMaNewNode->u1SelectorType = ECFM_SERVICE_SELECTION_PW;
                pMaNewNode->u4PrimaryVidIsid = ECFM_INIT_VAL;
            }
            else
            {
                pMaNewNode->u4PrimaryVidIsid =
                    u4FsMIEcfmExtMaPrimarySelectorOrNone;
                pMaNewNode->u1SelectorType = ECFM_SERVICE_SELECTION_VLAN;
            }

            /*Enable Cross Checking by default */
            pMaNewNode->u1CrossCheckStatus = ECFM_ENABLE;
            /*Initialise DLL for MEPs and RBTree for MepList */
            TMO_DLL_Init (&(pMaNewNode->MepTable));
            /* Set MA rowstatus to _NOT_IN_SERVICE */
            pMaNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            pMaNewNode->u1CompRowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;

            /* Add new MA node in MaTableIndex in global info and in its 
             * associated MD's MaTable */
            if (EcfmSnmpLwAddMaEntry (pMaNewNode) != ECFM_SUCCESS)
            {
                /* Delete MepList associated with MA */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_TABLE_POOL,
                                     (UINT1 *) (pMaNewNode));
                pMaNewNode = NULL;
                ECFM_CC_RELEASE_CONTEXT ();
                return SNMP_FAILURE;
            }
            break;

        case ECFM_ROW_STATUS_DESTROY:

            /* Delete MA from LBLT task also */
            EcfmLbLtRemoveMaEntry (ECFM_CC_CURR_CONTEXT_ID (), pMaNode);

            /* Update DefaultMd status corresponding to MA primaryVid and
             * level and Evaluate for MIP creation for a change of MA */
            if (pMaNode->u4PrimaryVidIsid != 0)
            {
                b1VlanAwareMa = ECFM_TRUE;
                u1MdLevel = pMdNode->u1Level;
                u2VlanId = (UINT2) pMaNode->u4PrimaryVidIsid;
            }
            /* Remove MA node from MD's MaTable and from MaTableIndex 
             * in Global info */
            EcfmSnmpLwDeleteMaEntry (pMaNode);
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MA_TABLE_POOL, (UINT1 *) (pMaNode));
            pMaNode = NULL;

            if (b1VlanAwareMa == ECFM_TRUE)
            {
                /* Update default MdStatus, as all MEPs associated 
                 * would have been deleted */
                EcfmSnmpLwUpdateDefaultMdStatus (u1MdLevel, u2VlanId,
                                                 ECFM_TRUE);

                EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanId, ECFM_TRUE);
            }

            break;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/**********************************************************************
 Function    :  nmhSetFsMIEcfmExtMaCrosscheckStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                setValFsMIEcfmExtMaCrosscheckStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMaCrosscheckStatus (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                     UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                     INT4 i4SetValFsMIEcfmExtMaCrosscheckStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4VlanIdIsidOrNone =
        u4FsMIEcfmExtMaPrimarySelectorOrNone;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsMIEcfmExtMaPrimarySelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    }
    /* Get MA entry corresponding to indices MdIndex,
     * MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry\n");
        return SNMP_FAILURE;
    }
    if ((pMaNode->u1SelectorType == (UINT1) i4FsMIEcfmExtMaPrimarySelectorType)
        && (pMaNode->u4PrimaryVidIsid == (UINT4) i4VlanIdIsidOrNone))
    {
        pMaNode->u1CrossCheckStatus =
            (UINT1) i4SetValFsMIEcfmExtMaCrosscheckStatus;
    }

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaFormat (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                              UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                              INT4 i4TestValFsMIEcfmExtMaFormat)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMaNetFormat (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmExtMaIndex,
                                              i4TestValFsMIEcfmExtMaFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaName (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                            UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmExtMaIndex,
                            INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                            UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsMIEcfmExtMaName)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMaNetName (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            pTestValFsMIEcfmExtMaName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaMhfCreation (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                   UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                   INT4 i4TestValFsMIEcfmExtMaMhfCreation)
{
    INT1                i1RetVal;

    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmMaCompMhfCreation (pu4ErrorCode,
                                               u4FsMIEcfmContextId,
                                               u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmExtMaIndex,
                                               i4TestValFsMIEcfmExtMaMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaIdPermission (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                    UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                    INT4 i4TestValFsMIEcfmExtMaIdPermission)
{
    INT1                i1RetVal;

    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmMaCompIdPermission (pu4ErrorCode,
                                                u4FsMIEcfmContextId,
                                                u4FsMIEcfmMdIndex,
                                                u4FsMIEcfmExtMaIndex,
                                                i4TestValFsMIEcfmExtMaIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaCcmInterval
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaCcmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaCcmInterval (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                   UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                   INT4 i4TestValFsMIEcfmExtMaCcmInterval)
{
    INT1                i1RetVal;

    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMaNetCcmInterval (pu4ErrorCode,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            i4TestValFsMIEcfmExtMaCcmInterval);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                 UINT4 u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                 INT4 i4TestValFsMIEcfmExtMaRowStatus)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMaNetRowStatus (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          i4TestValFsMIEcfmExtMaRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/*******************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMaCrosscheckStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone

                The Object 
                testValFsMIEcfmExtMaCrosscheckStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMaCrosscheckStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIEcfmContextId,
                                        UINT4 u4FsMIEcfmMdIndex,
                                        UINT4 u4FsMIEcfmExtMaIndex,
                                        INT4 i4FsMIEcfmExtMaPrimarySelectorType,
                                        UINT4
                                        u4FsMIEcfmExtMaPrimarySelectorOrNone,
                                        INT4
                                        i4TestValFsMIEcfmExtMaCrosscheckStatus)
{
    INT1                i1RetVal;
    UNUSED_PARAM (i4FsMIEcfmExtMaPrimarySelectorType);
    UNUSED_PARAM (u4FsMIEcfmExtMaPrimarySelectorOrNone);
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMaCrosscheckStatus (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  u4FsMIEcfmExtMaIndex,
                                                  i4TestValFsMIEcfmExtMaCrosscheckStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmExtMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMaPrimarySelectorType
                FsMIEcfmExtMaPrimarySelectorOrNone
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmExtMaTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmExtMipTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmExtMipTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmExtMipTable (INT4 i4FsMIEcfmExtMipIfIndex,
                                             INT4 i4FsMIEcfmExtMipMdLevel,
                                             INT4 i4FsMIEcfmExtMipSelectorType,
                                             UINT4
                                             u4FsMIEcfmExtMipPrimarySelector)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }

    i1RetVal =
        nmhValidateIndexInstanceFsMIEcfmMipTable (i4FsMIEcfmExtMipIfIndex,
                                                  i4FsMIEcfmExtMipMdLevel,
                                                  i4VlanIdIsidOrNone);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmExtMipTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmExtMipTable (INT4 *pi4FsMIEcfmExtMipIfIndex,
                                     INT4 *pi4FsMIEcfmExtMipMdLevel,
                                     INT4 *pi4FsMIEcfmExtMipSelectorType,
                                     UINT4 *pu4FsMIEcfmExtMipPrimarySelector)
{
    return (nmhGetNextIndexFsMIEcfmExtMipTable (0, pi4FsMIEcfmExtMipIfIndex,
                                                0, pi4FsMIEcfmExtMipMdLevel,
                                                0,
                                                pi4FsMIEcfmExtMipSelectorType,
                                                0,
                                                pu4FsMIEcfmExtMipPrimarySelector));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmExtMipTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                nextFsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                nextFsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                nextFsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector
                nextFsMIEcfmExtMipPrimarySelector
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmExtMipTable (INT4 i4FsMIEcfmExtMipIfIndex,
                                    INT4 *pi4NextFsMIEcfmExtMipIfIndex,
                                    INT4 i4FsMIEcfmExtMipMdLevel,
                                    INT4 *pi4NextFsMIEcfmExtMipMdLevel,
                                    INT4 i4FsMIEcfmExtMipSelectorType,
                                    INT4 *pi4NextFsMIEcfmExtMipSelectorType,
                                    UINT4 u4FsMIEcfmExtMipPrimarySelector,
                                    UINT4 *pu4NextFsMIEcfmExtMipPrimarySelector)
{
    INT4                i4VlanIdIsidOrNone;

    *pi4NextFsMIEcfmExtMipSelectorType = ECFM_SERVICE_SELECTION_VLAN;
    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }

    if (nmhGetNextIndexFsMIEcfmMipTable (i4FsMIEcfmExtMipIfIndex,
                                         pi4NextFsMIEcfmExtMipIfIndex,
                                         i4FsMIEcfmExtMipMdLevel,
                                         pi4NextFsMIEcfmExtMipMdLevel,
                                         i4VlanIdIsidOrNone,
                                         (INT4 *)
                                         pu4NextFsMIEcfmExtMipPrimarySelector)
        == SNMP_SUCCESS)
    {
        if (*pu4NextFsMIEcfmExtMipPrimarySelector >= ECFM_INTERNAL_ISID_MIN)
        {
            *pi4NextFsMIEcfmExtMipSelectorType = ECFM_SERVICE_SELECTION_ISID;
            *pu4NextFsMIEcfmExtMipPrimarySelector =
                ECFM_ISID_INTERNAL_TO_ISID
                (*pu4NextFsMIEcfmExtMipPrimarySelector);
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMipActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector

                The Object 
                retValFsMIEcfmExtMipActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMipActive (INT4 i4FsMIEcfmExtMipIfIndex,
                            INT4 i4FsMIEcfmExtMipMdLevel,
                            INT4 i4FsMIEcfmExtMipSelectorType,
                            UINT4 u4FsMIEcfmExtMipPrimarySelector,
                            INT4 *pi4RetValFsMIEcfmExtMipActive)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }

    i1RetVal =
        nmhGetFsMIEcfmMipActive (i4FsMIEcfmExtMipIfIndex,
                                 i4FsMIEcfmExtMipMdLevel,
                                 i4VlanIdIsidOrNone,
                                 pi4RetValFsMIEcfmExtMipActive);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMipRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector

                The Object 
                retValFsMIEcfmExtMipRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMipRowStatus (INT4 i4FsMIEcfmExtMipIfIndex,
                               INT4 i4FsMIEcfmExtMipMdLevel,
                               INT4 i4FsMIEcfmExtMipSelectorType,
                               UINT4 u4FsMIEcfmExtMipPrimarySelector,
                               INT4 *pi4RetValFsMIEcfmExtMipRowStatus)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }

    i1RetVal =
        nmhGetFsMIEcfmMipRowStatus (i4FsMIEcfmExtMipIfIndex,
                                    i4FsMIEcfmExtMipMdLevel,
                                    i4VlanIdIsidOrNone,
                                    pi4RetValFsMIEcfmExtMipRowStatus);

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMipActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector

                The Object 
                setValFsMIEcfmExtMipActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMipActive (INT4 i4FsMIEcfmExtMipIfIndex,
                            INT4 i4FsMIEcfmExtMipMdLevel,
                            INT4 i4FsMIEcfmExtMipSelectorType,
                            UINT4 u4FsMIEcfmExtMipPrimarySelector,
                            INT4 i4SetValFsMIEcfmExtMipActive)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmExtMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }
    i1RetVal =
        nmhSetFsEcfmMipActive ((INT4) u2LocalPort, i4FsMIEcfmExtMipMdLevel,
                               i4VlanIdIsidOrNone,
                               i4SetValFsMIEcfmExtMipActive);

    ECFM_CC_RELEASE_CONTEXT ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMipRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector

                The Object 
                setValFsMIEcfmExtMipRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMipRowStatus (INT4 i4FsMIEcfmExtMipIfIndex,
                               INT4 i4FsMIEcfmExtMipMdLevel,
                               INT4 i4FsMIEcfmExtMipSelectorType,
                               UINT4 u4FsMIEcfmExtMipPrimarySelector,
                               INT4 i4SetValFsMIEcfmExtMipRowStatus)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmExtMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }

    i1RetVal =
        nmhSetFsEcfmMipRowStatus ((INT4) u2LocalPort,
                                  i4FsMIEcfmExtMipMdLevel, i4VlanIdIsidOrNone,
                                  i4SetValFsMIEcfmExtMipRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMipActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector

                The Object 
                testValFsMIEcfmExtMipActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMipActive (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIEcfmExtMipIfIndex,
                               INT4 i4FsMIEcfmExtMipMdLevel,
                               INT4 i4FsMIEcfmExtMipSelectorType,
                               UINT4 u4FsMIEcfmExtMipPrimarySelector,
                               INT4 i4TestValFsMIEcfmExtMipActive)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }
    i1RetVal = nmhTestv2FsMIEcfmMipActive (pu4ErrorCode,
                                           i4FsMIEcfmExtMipIfIndex,
                                           i4FsMIEcfmExtMipMdLevel,
                                           i4VlanIdIsidOrNone,
                                           i4TestValFsMIEcfmExtMipActive);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMipRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector

                The Object 
                testValFsMIEcfmExtMipRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMipRowStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIEcfmExtMipIfIndex,
                                  INT4 i4FsMIEcfmExtMipMdLevel,
                                  INT4 i4FsMIEcfmExtMipSelectorType,
                                  UINT4 u4FsMIEcfmExtMipPrimarySelector,
                                  INT4 i4TestValFsMIEcfmExtMipRowStatus)
{
    INT1                i1RetVal;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4FsMIEcfmExtMipPrimarySelector;

    if (i4FsMIEcfmExtMipSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4FsMIEcfmExtMipPrimarySelector);

    }
    i1RetVal =
        nmhTestv2FsMIEcfmMipRowStatus (pu4ErrorCode,
                                       i4FsMIEcfmExtMipIfIndex,
                                       i4FsMIEcfmExtMipMdLevel,
                                       i4VlanIdIsidOrNone,
                                       i4TestValFsMIEcfmExtMipRowStatus);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmExtMipTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmExtMipIfIndex
                FsMIEcfmExtMipMdLevel
                FsMIEcfmExtMipSelectorType
                FsMIEcfmExtMipPrimarySelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmExtMipTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmExtMepTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmExtMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmExtMepTable (UINT4 u4FsMIEcfmContextId,
                                             UINT4 u4FsMIEcfmMdIndex,
                                             UINT4 u4FsMIEcfmExtMaIndex,
                                             UINT4 u4FsMIEcfmExtMepIdentifier)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1agCfmMepTable (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmExtMaIndex,
                                                          u4FsMIEcfmExtMepIdentifier);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmExtMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmExtMepTable (UINT4 *pu4FsMIEcfmContextId,
                                     UINT4 *pu4FsMIEcfmMdIndex,
                                     UINT4 *pu4FsMIEcfmExtMaIndex,
                                     UINT4 *pu4FsMIEcfmExtMepIdentifier)
{
    return (nmhGetNextIndexFsMIEcfmExtMepTable (0, pu4FsMIEcfmContextId, 0,
                                                pu4FsMIEcfmMdIndex, 0,
                                                pu4FsMIEcfmExtMaIndex, 0,
                                                pu4FsMIEcfmExtMepIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmExtMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                nextFsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier
                nextFsMIEcfmExtMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmExtMepTable (UINT4 u4FsMIEcfmContextId,
                                    UINT4 *pu4NextFsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 *pu4NextFsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 *pu4NextFsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4 *pu4NextFsMIEcfmExtMepIdentifier)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexDot1agCfmMepTable (u4FsMIEcfmMdIndex,
                                              pu4NextFsMIEcfmMdIndex,
                                              u4FsMIEcfmExtMaIndex,
                                              pu4NextFsMIEcfmExtMaIndex,
                                              u4FsMIEcfmExtMepIdentifier,
                                              pu4NextFsMIEcfmExtMepIdentifier)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexDot1agCfmMepTable (pu4NextFsMIEcfmMdIndex,
                                              pu4NextFsMIEcfmExtMaIndex,
                                              pu4NextFsMIEcfmExtMepIdentifier)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepIfIndex (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmExtMaIndex,
                             UINT4 u4FsMIEcfmExtMepIdentifier,
                             INT4 *pi4RetValFsMIEcfmExtMepIfIndex)
{
    INT1                i1RetVal;
    i1RetVal =
        nmhGetFsMIEcfmMepIfIndex (u4FsMIEcfmContextId, u4FsMIEcfmMdIndex,
                                  u4FsMIEcfmExtMaIndex,
                                  u4FsMIEcfmExtMepIdentifier,
                                  pi4RetValFsMIEcfmExtMepIfIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepDirection
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepDirection (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmExtMaIndex,
                               UINT4 u4FsMIEcfmExtMepIdentifier,
                               INT4 *pi4RetValFsMIEcfmExtMepDirection)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDirection (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                     u4FsMIEcfmExtMepIdentifier,
                                     pi4RetValFsMIEcfmExtMepDirection);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepPrimaryVidOrIsid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepPrimaryVidOrIsid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepPrimaryVidOrIsid (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmExtMaIndex,
                                      UINT4 u4FsMIEcfmExtMepIdentifier,
                                      UINT4
                                      *pu4RetValFsMIEcfmExtMepPrimaryVidOrIsid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepPrimaryVid (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      pu4RetValFsMIEcfmExtMepPrimaryVidOrIsid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepActive (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmExtMaIndex,
                            UINT4 u4FsMIEcfmExtMepIdentifier,
                            INT4 *pi4RetValFsMIEcfmExtMepActive)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepActive (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                  u4FsMIEcfmExtMepIdentifier,
                                  pi4RetValFsMIEcfmExtMepActive);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepFngState
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepFngState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepFngState (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              UINT4 u4FsMIEcfmExtMepIdentifier,
                              INT4 *pi4RetValFsMIEcfmExtMepFngState)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepFngState (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                    u4FsMIEcfmExtMepIdentifier,
                                    pi4RetValFsMIEcfmExtMepFngState);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepCciEnabled
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepCciEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepCciEnabled (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                INT4 *pi4RetValFsMIEcfmExtMepCciEnabled)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCciEnabled (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      pi4RetValFsMIEcfmExtMepCciEnabled);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepCcmLtmPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepCcmLtmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepCcmLtmPriority (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMIEcfmExtMepCcmLtmPriority)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCcmLtmPriority (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          u4FsMIEcfmExtMepIdentifier,
                                          pu4RetValFsMIEcfmExtMepCcmLtmPriority);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepMacAddress (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                tMacAddr * pRetValFsMIEcfmExtMepMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepMacAddress (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      pRetValFsMIEcfmExtMepMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepLowPrDef
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepLowPrDef
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepLowPrDef (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              UINT4 u4FsMIEcfmExtMepIdentifier,
                              INT4 *pi4RetValFsMIEcfmExtMepLowPrDef)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLowPrDef (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                    u4FsMIEcfmExtMepIdentifier,
                                    pi4RetValFsMIEcfmExtMepLowPrDef);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepFngAlarmTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepFngAlarmTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepFngAlarmTime (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmExtMaIndex,
                                  UINT4 u4FsMIEcfmExtMepIdentifier,
                                  INT4 *pi4RetValFsMIEcfmExtMepFngAlarmTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepFngAlarmTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        pi4RetValFsMIEcfmExtMepFngAlarmTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepFngResetTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepFngResetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepFngResetTime (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmExtMaIndex,
                                  UINT4 u4FsMIEcfmExtMepIdentifier,
                                  INT4 *pi4RetValFsMIEcfmExtMepFngResetTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepFngResetTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        pi4RetValFsMIEcfmExtMepFngResetTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepHighestPrDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepHighestPrDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepHighestPrDefect (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     UINT4 u4FsMIEcfmExtMepIdentifier,
                                     INT4
                                     *pi4RetValFsMIEcfmExtMepHighestPrDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepHighestPrDefect (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmExtMaIndex,
                                           u4FsMIEcfmExtMepIdentifier,
                                           pi4RetValFsMIEcfmExtMepHighestPrDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepDefects
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepDefects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepDefects (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmExtMaIndex,
                             UINT4 u4FsMIEcfmExtMepIdentifier,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIEcfmExtMepDefects)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDefects (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                   u4FsMIEcfmExtMepIdentifier,
                                   pRetValFsMIEcfmExtMepDefects);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepErrorCcmLastFailure
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepErrorCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepErrorCcmLastFailure (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmExtMaIndex,
                                         UINT4 u4FsMIEcfmExtMepIdentifier,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsMIEcfmExtMepErrorCcmLastFailure)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepErrorCcmLastFailure (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmExtMaIndex,
                                                      u4FsMIEcfmExtMepIdentifier,
                                                      pRetValFsMIEcfmExtMepErrorCcmLastFailure);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepXconCcmLastFailure
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepXconCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepXconCcmLastFailure (UINT4 u4FsMIEcfmContextId,
                                        UINT4 u4FsMIEcfmMdIndex,
                                        UINT4 u4FsMIEcfmExtMaIndex,
                                        UINT4 u4FsMIEcfmExtMepIdentifier,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIEcfmExtMepXconCcmLastFailure)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepXconCcmLastFailure (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     pRetValFsMIEcfmExtMepXconCcmLastFailure);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepCcmSequenceErrors
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepCcmSequenceErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepCcmSequenceErrors (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       UINT4
                                       *pu4RetValFsMIEcfmExtMepCcmSequenceErrors)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCcmSequenceErrors (u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             u4FsMIEcfmExtMepIdentifier,
                                             pu4RetValFsMIEcfmExtMepCcmSequenceErrors);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepCciSentCcms
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepCciSentCcms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepCciSentCcms (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 UINT4 u4FsMIEcfmExtMepIdentifier,
                                 UINT4 *pu4RetValFsMIEcfmExtMepCciSentCcms)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCciSentCcms (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                       u4FsMIEcfmExtMepIdentifier,
                                       pu4RetValFsMIEcfmExtMepCciSentCcms);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepNextLbmTransId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepNextLbmTransId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepNextLbmTransId (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMIEcfmExtMepNextLbmTransId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepNextLbmTransId (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          u4FsMIEcfmExtMepIdentifier,
                                          pu4RetValFsMIEcfmExtMepNextLbmTransId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepLbrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepLbrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepLbrIn (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmExtMaIndex,
                           UINT4 u4FsMIEcfmExtMepIdentifier,
                           UINT4 *pu4RetValFsMIEcfmExtMepLbrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrIn (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                 u4FsMIEcfmExtMepIdentifier,
                                 pu4RetValFsMIEcfmExtMepLbrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepLbrInOutOfOrder
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepLbrInOutOfOrder
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepLbrInOutOfOrder (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     UINT4 u4FsMIEcfmExtMepIdentifier,
                                     UINT4
                                     *pu4RetValFsMIEcfmExtMepLbrInOutOfOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrInOutOfOrder (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmExtMaIndex,
                                           u4FsMIEcfmExtMepIdentifier,
                                           pu4RetValFsMIEcfmExtMepLbrInOutOfOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepLbrBadMsdu
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepLbrBadMsdu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepLbrBadMsdu (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                UINT4 *pu4RetValFsMIEcfmExtMepLbrBadMsdu)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrBadMsdu (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      pu4RetValFsMIEcfmExtMepLbrBadMsdu);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepLtmNextSeqNumber
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepLtmNextSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepLtmNextSeqNumber (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmExtMaIndex,
                                      UINT4 u4FsMIEcfmExtMepIdentifier,
                                      UINT4
                                      *pu4RetValFsMIEcfmExtMepLtmNextSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLtmNextSeqNumber (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            u4FsMIEcfmExtMepIdentifier,
                                            pu4RetValFsMIEcfmExtMepLtmNextSeqNumber);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepUnexpLtrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepUnexpLtrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepUnexpLtrIn (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                UINT4 *pu4RetValFsMIEcfmExtMepUnexpLtrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepUnexpLtrIn (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      pu4RetValFsMIEcfmExtMepUnexpLtrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepLbrOut
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepLbrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepLbrOut (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmExtMaIndex,
                            UINT4 u4FsMIEcfmExtMepIdentifier,
                            UINT4 *pu4RetValFsMIEcfmExtMepLbrOut)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrOut (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                  u4FsMIEcfmExtMepIdentifier,
                                  pu4RetValFsMIEcfmExtMepLbrOut);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmStatus (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       INT4
                                       *pi4RetValFsMIEcfmExtMepTransmitLbmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlGetAgMepTxLbmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmExtMaIndex,
                                              u4FsMIEcfmExtMepIdentifier,
                                              pi4RetValFsMIEcfmExtMepTransmitLbmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmDestMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmDestMacAddress (UINT4 u4FsMIEcfmContextId,
                                               UINT4 u4FsMIEcfmMdIndex,
                                               UINT4 u4FsMIEcfmExtMaIndex,
                                               UINT4 u4FsMIEcfmExtMepIdentifier,
                                               tMacAddr *
                                               pRetValFsMIEcfmExtMepTransmitLbmDestMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLbmDestMacAddress (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     pRetValFsMIEcfmExtMepTransmitLbmDestMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmDestMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmDestMepId (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          UINT4
                                          *pu4RetValFsMIEcfmExtMepTransmitLbmDestMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmDestMepId (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       pu4RetValFsMIEcfmExtMepTransmitLbmDestMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmDestIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmDestIsMepId (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmExtMaIndex,
                                            UINT4 u4FsMIEcfmExtMepIdentifier,
                                            INT4
                                            *pi4RetValFsMIEcfmExtMepTransmitLbmDestIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmDestIsMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmExtMaIndex,
                                                         u4FsMIEcfmExtMepIdentifier,
                                                         pi4RetValFsMIEcfmExtMepTransmitLbmDestIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmMessages
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmMessages (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmExtMaIndex,
                                         UINT4 u4FsMIEcfmExtMepIdentifier,
                                         INT4
                                         *pi4RetValFsMIEcfmExtMepTransmitLbmMessages)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmMessages (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmExtMaIndex,
                                                      u4FsMIEcfmExtMepIdentifier,
                                                      pi4RetValFsMIEcfmExtMepTransmitLbmMessages);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmDataTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmDataTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmDataTlv (UINT4 u4FsMIEcfmContextId,
                                        UINT4 u4FsMIEcfmMdIndex,
                                        UINT4 u4FsMIEcfmExtMaIndex,
                                        UINT4 u4FsMIEcfmExtMepIdentifier,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIEcfmExtMepTransmitLbmDataTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmDataTlv (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     pRetValFsMIEcfmExtMepTransmitLbmDataTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmVlanIsidPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidPriority (UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 INT4
                                                 *pi4RetValFsMIEcfmExtMepTransmitLbmVlanIsidPriority)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmVlanPriority (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmExtMaIndex,
                                                          u4FsMIEcfmExtMepIdentifier,
                                                          pi4RetValFsMIEcfmExtMepTransmitLbmVlanIsidPriority);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable (UINT4 u4FsMIEcfmContextId,
                                                   UINT4 u4FsMIEcfmMdIndex,
                                                   UINT4 u4FsMIEcfmExtMaIndex,
                                                   UINT4
                                                   u4FsMIEcfmExtMepIdentifier,
                                                   INT4
                                                   *pi4RetValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1agCfmMepTransmitLbmVlanDropEnable (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     pi4RetValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmResultOK
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmResultOK (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmExtMaIndex,
                                         UINT4 u4FsMIEcfmExtMepIdentifier,
                                         INT4
                                         *pi4RetValFsMIEcfmExtMepTransmitLbmResultOK)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmResultOK (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmExtMaIndex,
                                                      u4FsMIEcfmExtMepIdentifier,
                                                      pi4RetValFsMIEcfmExtMepTransmitLbmResultOK);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLbmSeqNumber
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLbmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLbmSeqNumber (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          UINT4
                                          *pu4RetValFsMIEcfmExtMepTransmitLbmSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmSeqNumber (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       pu4RetValFsMIEcfmExtMepTransmitLbmSeqNumber);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmStatus (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       INT4
                                       *pi4RetValFsMIEcfmExtMepTransmitLtmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = EcfmMepUtlGetAgMepTxLtmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmExtMaIndex,
                                              u4FsMIEcfmExtMepIdentifier,
                                              pi4RetValFsMIEcfmExtMepTransmitLtmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmFlags
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmFlags (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmExtMaIndex,
                                      UINT4 u4FsMIEcfmExtMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIEcfmExtMepTransmitLtmFlags)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmFlags (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            u4FsMIEcfmExtMepIdentifier,
                                            pRetValFsMIEcfmExtMepTransmitLtmFlags);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmTargetMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmTargetMacAddress (UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 tMacAddr *
                                                 pRetValFsMIEcfmExtMepTransmitLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmTargetMacAddress (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       pRetValFsMIEcfmExtMepTransmitLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmTargetMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmTargetMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmTargetMepId (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmExtMaIndex,
                                            UINT4 u4FsMIEcfmExtMepIdentifier,
                                            UINT4
                                            *pu4RetValFsMIEcfmExtMepTransmitLtmTargetMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLtmTargetMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmExtMaIndex,
                                                         u4FsMIEcfmExtMepIdentifier,
                                                         pu4RetValFsMIEcfmExtMepTransmitLtmTargetMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmTargetIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmTargetIsMepId (UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmExtMaIndex,
                                              UINT4 u4FsMIEcfmExtMepIdentifier,
                                              INT4
                                              *pi4RetValFsMIEcfmExtMepTransmitLtmTargetIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmTargetIsMepId (u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmExtMaIndex,
                                                    u4FsMIEcfmExtMepIdentifier,
                                                    pi4RetValFsMIEcfmExtMepTransmitLtmTargetIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmTtl (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMIEcfmExtMepTransmitLtmTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmTtl (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          u4FsMIEcfmExtMepIdentifier,
                                          pu4RetValFsMIEcfmExtMepTransmitLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmResult
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmResult (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       INT4
                                       *pi4RetValFsMIEcfmExtMepTransmitLtmResult)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmResult (u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             u4FsMIEcfmExtMepIdentifier,
                                             pi4RetValFsMIEcfmExtMepTransmitLtmResult);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmSeqNumber
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmSeqNumber (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          UINT4
                                          *pu4RetValFsMIEcfmExtMepTransmitLtmSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLtmSeqNumber (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       pu4RetValFsMIEcfmExtMepTransmitLtmSeqNumber);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepTransmitLtmEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepTransmitLtmEgressIdentifier (UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pRetValFsMIEcfmExtMepTransmitLtmEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmEgressIdentifier (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       pRetValFsMIEcfmExtMepTransmitLtmEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepRowStatus (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmExtMaIndex,
                               UINT4 u4FsMIEcfmExtMepIdentifier,
                               INT4 *pi4RetValFsMIEcfmExtMepRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepRowStatus (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                     u4FsMIEcfmExtMepIdentifier,
                                     pi4RetValFsMIEcfmExtMepRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmExtMepCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                retValFsMIEcfmExtMepCcmOffload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmExtMepCcmOffload (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                INT4 *pi4RetValFsMIEcfmExtMepCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepCcmOffload (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                   u4FsMIEcfmExtMepIdentifier,
                                   pi4RetValFsMIEcfmExtMepCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepIfIndex (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmExtMaIndex,
                             UINT4 u4FsMIEcfmExtMepIdentifier,
                             INT4 i4SetValFsMIEcfmExtMepIfIndex)
{

    INT1                i1RetVal;
    i1RetVal =
        nmhSetFsMIEcfmMepIfIndex (u4FsMIEcfmContextId, u4FsMIEcfmMdIndex,
                                  u4FsMIEcfmExtMaIndex,
                                  u4FsMIEcfmExtMepIdentifier,
                                  i4SetValFsMIEcfmExtMepIfIndex);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepDirection
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepDirection (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmExtMaIndex,
                               UINT4 u4FsMIEcfmExtMepIdentifier,
                               INT4 i4SetValFsMIEcfmExtMepDirection)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepDirection (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                     u4FsMIEcfmExtMepIdentifier,
                                     i4SetValFsMIEcfmExtMepDirection);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepPrimaryVidOrIsid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepPrimaryVidOrIsid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepPrimaryVidOrIsid (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmExtMaIndex,
                                      UINT4 u4FsMIEcfmExtMepIdentifier,
                                      UINT4
                                      u4SetValFsMIEcfmExtMepPrimaryVidOrIsid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepPrimaryVid (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      ECFM_ISID_TO_ISID_INTERNAL
                                      (u4SetValFsMIEcfmExtMepPrimaryVidOrIsid));
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepActive (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmExtMaIndex,
                            UINT4 u4FsMIEcfmExtMepIdentifier,
                            INT4 i4SetValFsMIEcfmExtMepActive)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepActive (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                  u4FsMIEcfmExtMepIdentifier,
                                  i4SetValFsMIEcfmExtMepActive);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepCciEnabled
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepCciEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepCciEnabled (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                INT4 i4SetValFsMIEcfmExtMepCciEnabled)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepCciEnabled (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      i4SetValFsMIEcfmExtMepCciEnabled);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepCcmLtmPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepCcmLtmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepCcmLtmPriority (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4 u4SetValFsMIEcfmExtMepCcmLtmPriority)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepCcmLtmPriority (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          u4FsMIEcfmExtMepIdentifier,
                                          u4SetValFsMIEcfmExtMepCcmLtmPriority);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepLowPrDef
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepLowPrDef
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepLowPrDef (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              UINT4 u4FsMIEcfmExtMepIdentifier,
                              INT4 i4SetValFsMIEcfmExtMepLowPrDef)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepLowPrDef (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                    u4FsMIEcfmExtMepIdentifier,
                                    i4SetValFsMIEcfmExtMepLowPrDef);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepFngAlarmTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepFngAlarmTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepFngAlarmTime (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmExtMaIndex,
                                  UINT4 u4FsMIEcfmExtMepIdentifier,
                                  INT4 i4SetValFsMIEcfmExtMepFngAlarmTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepFngAlarmTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        i4SetValFsMIEcfmExtMepFngAlarmTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepFngResetTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepFngResetTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepFngResetTime (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmExtMaIndex,
                                  UINT4 u4FsMIEcfmExtMepIdentifier,
                                  INT4 i4SetValFsMIEcfmExtMepFngResetTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepFngResetTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        i4SetValFsMIEcfmExtMepFngResetTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepCcmSequenceErrors
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepCcmSequenceErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepCcmSequenceErrors (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       UINT4
                                       u4SetValFsMIEcfmExtMepCcmSequenceErrors)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepCcmSequenceErrors (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          u4FsMIEcfmExtMepIdentifier,
                                          u4SetValFsMIEcfmExtMepCcmSequenceErrors);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepCciSentCcms
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepCciSentCcms
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepCciSentCcms (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 UINT4 u4FsMIEcfmExtMepIdentifier,
                                 UINT4 u4SetValFsMIEcfmExtMepCciSentCcms)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepCciSentCcms (u4FsMIEcfmMdIndex,
                                    u4FsMIEcfmExtMaIndex,
                                    u4FsMIEcfmExtMepIdentifier,
                                    u4SetValFsMIEcfmExtMepCciSentCcms);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepLbrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepLbrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepLbrIn (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmExtMaIndex,
                           UINT4 u4FsMIEcfmExtMepIdentifier,
                           UINT4 u4SetValFsMIEcfmExtMepLbrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrIn (u4FsMIEcfmMdIndex,
                              u4FsMIEcfmExtMaIndex,
                              u4FsMIEcfmExtMepIdentifier,
                              u4SetValFsMIEcfmExtMepLbrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepLbrInOutOfOrder
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepLbrInOutOfOrder
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepLbrInOutOfOrder (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     UINT4 u4FsMIEcfmExtMepIdentifier,
                                     UINT4
                                     u4SetValFsMIEcfmExtMepLbrInOutOfOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrInOutOfOrder (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        u4SetValFsMIEcfmExtMepLbrInOutOfOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepLbrBadMsdu
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepLbrBadMsdu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepLbrBadMsdu (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                UINT4 u4SetValFsMIEcfmExtMepLbrBadMsdu)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrBadMsdu (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmExtMaIndex,
                                   u4FsMIEcfmExtMepIdentifier,
                                   u4SetValFsMIEcfmExtMepLbrBadMsdu);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepUnexpLtrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepUnexpLtrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepUnexpLtrIn (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                UINT4 u4SetValFsMIEcfmExtMepUnexpLtrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepUnexpLtrIn (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmExtMaIndex,
                                   u4FsMIEcfmExtMepIdentifier,
                                   u4SetValFsMIEcfmExtMepUnexpLtrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepLbrOut
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepLbrOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepLbrOut (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmExtMaIndex,
                            UINT4 u4FsMIEcfmExtMepIdentifier,
                            UINT4 u4SetValFsMIEcfmExtMepLbrOut)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrOut (u4FsMIEcfmMdIndex,
                               u4FsMIEcfmExtMaIndex,
                               u4FsMIEcfmExtMepIdentifier,
                               u4SetValFsMIEcfmExtMepLbrOut);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmStatus (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       INT4
                                       i4SetValFsMIEcfmExtMepTransmitLbmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlSetAgMepTxLbmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmExtMaIndex,
                                              u4FsMIEcfmExtMepIdentifier,
                                              i4SetValFsMIEcfmExtMepTransmitLbmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmDestMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmDestMacAddress (UINT4 u4FsMIEcfmContextId,
                                               UINT4 u4FsMIEcfmMdIndex,
                                               UINT4 u4FsMIEcfmExtMaIndex,
                                               UINT4 u4FsMIEcfmExtMepIdentifier,
                                               tMacAddr
                                               SetValFsMIEcfmExtMepTransmitLbmDestMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLbmDestMacAddress (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     SetValFsMIEcfmExtMepTransmitLbmDestMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmDestMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmDestMepId (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          UINT4
                                          u4SetValFsMIEcfmExtMepTransmitLbmDestMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmDestMepId (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       u4SetValFsMIEcfmExtMepTransmitLbmDestMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmDestIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmDestIsMepId (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmExtMaIndex,
                                            UINT4 u4FsMIEcfmExtMepIdentifier,
                                            INT4
                                            i4SetValFsMIEcfmExtMepTransmitLbmDestIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmDestIsMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmExtMaIndex,
                                                         u4FsMIEcfmExtMepIdentifier,
                                                         i4SetValFsMIEcfmExtMepTransmitLbmDestIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmMessages
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmMessages (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmExtMaIndex,
                                         UINT4 u4FsMIEcfmExtMepIdentifier,
                                         INT4
                                         i4SetValFsMIEcfmExtMepTransmitLbmMessages)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmMessages (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmExtMaIndex,
                                                      u4FsMIEcfmExtMepIdentifier,
                                                      i4SetValFsMIEcfmExtMepTransmitLbmMessages);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmDataTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmDataTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmDataTlv (UINT4 u4FsMIEcfmContextId,
                                        UINT4 u4FsMIEcfmMdIndex,
                                        UINT4 u4FsMIEcfmExtMaIndex,
                                        UINT4 u4FsMIEcfmExtMepIdentifier,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFsMIEcfmExtMepTransmitLbmDataTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmDataTlv (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     pSetValFsMIEcfmExtMepTransmitLbmDataTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmVlanIsidPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidPriority (UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 INT4
                                                 i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidPriority)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1agCfmMepTransmitLbmVlanPriority (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmExtMaIndex,
                                                          u4FsMIEcfmExtMepIdentifier,
                                                          i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidPriority);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable (UINT4 u4FsMIEcfmContextId,
                                                   UINT4 u4FsMIEcfmMdIndex,
                                                   UINT4 u4FsMIEcfmExtMaIndex,
                                                   UINT4
                                                   u4FsMIEcfmExtMepIdentifier,
                                                   INT4
                                                   i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLbmVlanDropEnable (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmExtMaIndex,
                                                     u4FsMIEcfmExtMepIdentifier,
                                                     i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmStatus (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       INT4
                                       i4SetValFsMIEcfmExtMepTransmitLtmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlSetAgMepTxLtmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmExtMaIndex,
                                              u4FsMIEcfmExtMepIdentifier,
                                              i4SetValFsMIEcfmExtMepTransmitLtmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmFlags
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmFlags (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmExtMaIndex,
                                      UINT4 u4FsMIEcfmExtMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsMIEcfmExtMepTransmitLtmFlags)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmFlags (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            u4FsMIEcfmExtMepIdentifier,
                                            pSetValFsMIEcfmExtMepTransmitLtmFlags);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmTargetMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmTargetMacAddress (UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 tMacAddr
                                                 SetValFsMIEcfmExtMepTransmitLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmTargetMacAddress (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       SetValFsMIEcfmExtMepTransmitLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmTargetMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmTargetMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmTargetMepId (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmExtMaIndex,
                                            UINT4 u4FsMIEcfmExtMepIdentifier,
                                            UINT4
                                            u4SetValFsMIEcfmExtMepTransmitLtmTargetMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLtmTargetMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmExtMaIndex,
                                                         u4FsMIEcfmExtMepIdentifier,
                                                         u4SetValFsMIEcfmExtMepTransmitLtmTargetMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmTargetIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmTargetIsMepId (UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmExtMaIndex,
                                              UINT4 u4FsMIEcfmExtMepIdentifier,
                                              INT4
                                              i4SetValFsMIEcfmExtMepTransmitLtmTargetIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmTargetIsMepId (u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmExtMaIndex,
                                                    u4FsMIEcfmExtMepIdentifier,
                                                    i4SetValFsMIEcfmExtMepTransmitLtmTargetIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmTtl (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4 u4SetValFsMIEcfmExtMepTransmitLtmTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmTtl (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmExtMaIndex,
                                          u4FsMIEcfmExtMepIdentifier,
                                          u4SetValFsMIEcfmExtMepTransmitLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepTransmitLtmEgressIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepTransmitLtmEgressIdentifier (UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pSetValFsMIEcfmExtMepTransmitLtmEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmEgressIdentifier (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmExtMaIndex,
                                                       u4FsMIEcfmExtMepIdentifier,
                                                       pSetValFsMIEcfmExtMepTransmitLtmEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepRowStatus (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmExtMaIndex,
                               UINT4 u4FsMIEcfmExtMepIdentifier,
                               INT4 i4SetValFsMIEcfmExtMepRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepRowStatus (u4FsMIEcfmMdIndex, u4FsMIEcfmExtMaIndex,
                                     u4FsMIEcfmExtMepIdentifier,
                                     i4SetValFsMIEcfmExtMepRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmExtMepCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                setValFsMIEcfmExtMepCcmOffload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmExtMepCcmOffload (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                INT4 i4SetValFsMIEcfmExtMepCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepCcmOffload (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmExtMaIndex,
                                   u4FsMIEcfmExtMepIdentifier,
                                   i4SetValFsMIEcfmExtMepCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepIfIndex (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmExtMaIndex,
                                UINT4 u4FsMIEcfmExtMepIdentifier,
                                INT4 i4TestValFsMIEcfmExtMepIfIndex)
{
    INT1                i1RetVal;
    i1RetVal = nmhTestv2FsMIEcfmMepIfIndex (pu4ErrorCode,
                                            u4FsMIEcfmContextId,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            u4FsMIEcfmExtMepIdentifier,
                                            i4TestValFsMIEcfmExtMepIfIndex);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepDirection
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepDirection (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmExtMaIndex,
                                  UINT4 u4FsMIEcfmExtMepIdentifier,
                                  INT4 i4TestValFsMIEcfmExtMepDirection)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepDirection (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        i4TestValFsMIEcfmExtMepDirection);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepPrimaryVidOrIsid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepPrimaryVidOrIsid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepPrimaryVidOrIsid (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmExtMaIndex,
                                         UINT4 u4FsMIEcfmExtMepIdentifier,
                                         UINT4
                                         u4TestValFsMIEcfmExtMepPrimaryVidOrIsid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMepPrimaryVid (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                         u4FsMIEcfmExtMaIndex,
                                         u4FsMIEcfmExtMepIdentifier,
                                         u4TestValFsMIEcfmExtMepPrimaryVidOrIsid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepActive (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmExtMaIndex,
                               UINT4 u4FsMIEcfmExtMepIdentifier,
                               INT4 i4TestValFsMIEcfmExtMepActive)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMepActive (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmExtMaIndex,
                                            u4FsMIEcfmExtMepIdentifier,
                                            i4TestValFsMIEcfmExtMepActive);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepCciEnabled
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepCciEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepCciEnabled (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   UINT4 u4FsMIEcfmExtMepIdentifier,
                                   INT4 i4TestValFsMIEcfmExtMepCciEnabled)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepCciEnabled (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                         u4FsMIEcfmExtMaIndex,
                                         u4FsMIEcfmExtMepIdentifier,
                                         i4TestValFsMIEcfmExtMepCciEnabled);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepCcmLtmPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepCcmLtmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepCcmLtmPriority (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       UINT4
                                       u4TestValFsMIEcfmExtMepCcmLtmPriority)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepCcmLtmPriority (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             u4FsMIEcfmExtMepIdentifier,
                                             u4TestValFsMIEcfmExtMepCcmLtmPriority);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepLowPrDef
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepLowPrDef
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepLowPrDef (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmExtMaIndex,
                                 UINT4 u4FsMIEcfmExtMepIdentifier,
                                 INT4 i4TestValFsMIEcfmExtMepLowPrDef)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepLowPrDef (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                       u4FsMIEcfmExtMaIndex,
                                       u4FsMIEcfmExtMepIdentifier,
                                       i4TestValFsMIEcfmExtMepLowPrDef);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepFngAlarmTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepFngAlarmTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepFngAlarmTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     UINT4 u4FsMIEcfmExtMepIdentifier,
                                     INT4 i4TestValFsMIEcfmExtMepFngAlarmTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepFngAlarmTime (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmExtMaIndex,
                                           u4FsMIEcfmExtMepIdentifier,
                                           i4TestValFsMIEcfmExtMepFngAlarmTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepFngResetTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepFngResetTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepFngResetTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmExtMaIndex,
                                     UINT4 u4FsMIEcfmExtMepIdentifier,
                                     INT4 i4TestValFsMIEcfmExtMepFngResetTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMepFngResetTime (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmExtMaIndex,
                                           u4FsMIEcfmExtMepIdentifier,
                                           i4TestValFsMIEcfmExtMepFngResetTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          INT4
                                          i4TestValFsMIEcfmExtMepTransmitLbmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlTestv2AgMepTxLbmStatus (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmExtMaIndex,
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 i4TestValFsMIEcfmExtMepTransmitLbmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepCcmSequenceErrors
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepCcmSequenceErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepCcmSequenceErrors (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          UINT4
                                          u4TestValFsMIEcfmExtMepCcmSequenceErrors)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepCcmSequenceErrors (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             u4FsMIEcfmExtMepIdentifier,
                                             u4TestValFsMIEcfmExtMepCcmSequenceErrors);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepCciSentCcms
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepCciSentCcms
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepCciSentCcms (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmExtMaIndex,
                                    UINT4 u4FsMIEcfmExtMepIdentifier,
                                    UINT4 u4TestValFsMIEcfmExtMepCciSentCcms)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepCciSentCcms (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                       u4FsMIEcfmExtMaIndex,
                                       u4FsMIEcfmExtMepIdentifier,
                                       u4TestValFsMIEcfmExtMepCciSentCcms);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepLbrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepLbrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepLbrIn (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmExtMaIndex,
                              UINT4 u4FsMIEcfmExtMepIdentifier,
                              UINT4 u4TestValFsMIEcfmExtMepLbrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepLbrIn (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                 u4FsMIEcfmExtMaIndex,
                                 u4FsMIEcfmExtMepIdentifier,
                                 u4TestValFsMIEcfmExtMepLbrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepLbrInOutOfOrder
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepLbrInOutOfOrder
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepLbrInOutOfOrder (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIEcfmContextId,
                                        UINT4 u4FsMIEcfmMdIndex,
                                        UINT4 u4FsMIEcfmExtMaIndex,
                                        UINT4 u4FsMIEcfmExtMepIdentifier,
                                        UINT4
                                        u4TestValFsMIEcfmExtMepLbrInOutOfOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepLbrInOutOfOrder (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmExtMaIndex,
                                           u4FsMIEcfmExtMepIdentifier,
                                           u4TestValFsMIEcfmExtMepLbrInOutOfOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepLbrBadMsdu
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepLbrBadMsdu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepLbrBadMsdu (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   UINT4 u4FsMIEcfmExtMepIdentifier,
                                   UINT4 u4TestValFsMIEcfmExtMepLbrBadMsdu)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepLbrBadMsdu (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                      u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      u4TestValFsMIEcfmExtMepLbrBadMsdu);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepUnexpLtrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepUnexpLtrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepUnexpLtrIn (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   UINT4 u4FsMIEcfmExtMepIdentifier,
                                   UINT4 u4TestValFsMIEcfmExtMepUnexpLtrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepUnexpLtrIn (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                      u4FsMIEcfmExtMaIndex,
                                      u4FsMIEcfmExtMepIdentifier,
                                      u4TestValFsMIEcfmExtMepUnexpLtrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepLbrOut
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepLbrOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepLbrOut (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmExtMaIndex,
                               UINT4 u4FsMIEcfmExtMepIdentifier,
                               UINT4 u4TestValFsMIEcfmExtMepLbrOut)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepLbrOut (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                  u4FsMIEcfmExtMaIndex,
                                  u4FsMIEcfmExtMepIdentifier,
                                  u4TestValFsMIEcfmExtMepLbrOut);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmDestMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDestMacAddress (UINT4 *pu4ErrorCode,
                                                  UINT4 u4FsMIEcfmContextId,
                                                  UINT4 u4FsMIEcfmMdIndex,
                                                  UINT4 u4FsMIEcfmExtMaIndex,
                                                  UINT4
                                                  u4FsMIEcfmExtMepIdentifier,
                                                  tMacAddr
                                                  TestValFsMIEcfmExtMepTransmitLbmDestMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmDestMacAddress (pu4ErrorCode,
                                                               u4FsMIEcfmMdIndex,
                                                               u4FsMIEcfmExtMaIndex,
                                                               u4FsMIEcfmExtMepIdentifier,
                                                               TestValFsMIEcfmExtMepTransmitLbmDestMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmDestMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDestMepId (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMIEcfmContextId,
                                             UINT4 u4FsMIEcfmMdIndex,
                                             UINT4 u4FsMIEcfmExtMaIndex,
                                             UINT4 u4FsMIEcfmExtMepIdentifier,
                                             UINT4
                                             u4TestValFsMIEcfmExtMepTransmitLbmDestMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmDestMepId (pu4ErrorCode,
                                                          u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmExtMaIndex,
                                                          u4FsMIEcfmExtMepIdentifier,
                                                          u4TestValFsMIEcfmExtMepTransmitLbmDestMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmDestIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDestIsMepId (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMIEcfmContextId,
                                               UINT4 u4FsMIEcfmMdIndex,
                                               UINT4 u4FsMIEcfmExtMaIndex,
                                               UINT4 u4FsMIEcfmExtMepIdentifier,
                                               INT4
                                               i4TestValFsMIEcfmExtMepTransmitLbmDestIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmDestIsMepId (pu4ErrorCode,
                                                            u4FsMIEcfmMdIndex,
                                                            u4FsMIEcfmExtMaIndex,
                                                            u4FsMIEcfmExtMepIdentifier,
                                                            i4TestValFsMIEcfmExtMepTransmitLbmDestIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmMessages
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmMessages (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmExtMaIndex,
                                            UINT4 u4FsMIEcfmExtMepIdentifier,
                                            INT4
                                            i4TestValFsMIEcfmExtMepTransmitLbmMessages)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLbmMessages (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  u4FsMIEcfmExtMaIndex,
                                                  u4FsMIEcfmExtMepIdentifier,
                                                  i4TestValFsMIEcfmExtMepTransmitLbmMessages);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmDataTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmDataTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDataTlv (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMIEcfmContextId,
                                           UINT4 u4FsMIEcfmMdIndex,
                                           UINT4 u4FsMIEcfmExtMaIndex,
                                           UINT4 u4FsMIEcfmExtMepIdentifier,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFsMIEcfmExtMepTransmitLbmDataTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLbmDataTlv (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmExtMaIndex,
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 pTestValFsMIEcfmExtMepTransmitLbmDataTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmVlanIsidPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidPriority (UINT4 *pu4ErrorCode,
                                                    UINT4 u4FsMIEcfmContextId,
                                                    UINT4 u4FsMIEcfmMdIndex,
                                                    UINT4 u4FsMIEcfmExtMaIndex,
                                                    UINT4
                                                    u4FsMIEcfmExtMepIdentifier,
                                                    INT4
                                                    i4TestValFsMIEcfmExtMepTransmitLbmVlanIsidPriority)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmVlanPriority (pu4ErrorCode,
                                                             u4FsMIEcfmMdIndex,
                                                             u4FsMIEcfmExtMaIndex,
                                                             u4FsMIEcfmExtMepIdentifier,
                                                             i4TestValFsMIEcfmExtMepTransmitLbmVlanIsidPriority);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable (UINT4 *pu4ErrorCode,
                                                      UINT4 u4FsMIEcfmContextId,
                                                      UINT4 u4FsMIEcfmMdIndex,
                                                      UINT4
                                                      u4FsMIEcfmExtMaIndex,
                                                      UINT4
                                                      u4FsMIEcfmExtMepIdentifier,
                                                      INT4
                                                      i4TestValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmVlanDropEnable (pu4ErrorCode,
                                                               u4FsMIEcfmMdIndex,
                                                               u4FsMIEcfmExtMaIndex,
                                                               u4FsMIEcfmExtMepIdentifier,
                                                               i4TestValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmExtMaIndex,
                                          UINT4 u4FsMIEcfmExtMepIdentifier,
                                          INT4
                                          i4TestValFsMIEcfmExtMepTransmitLtmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = EcfmMepUtlTestv2AgMepTxLtmStatus (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmExtMaIndex,
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 i4TestValFsMIEcfmExtMepTransmitLtmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmFlags
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmFlags (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmExtMaIndex,
                                         UINT4 u4FsMIEcfmExtMepIdentifier,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsMIEcfmExtMepTransmitLtmFlags)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmFlags (pu4ErrorCode,
                                               u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmExtMaIndex,
                                               u4FsMIEcfmExtMepIdentifier,
                                               pTestValFsMIEcfmExtMepTransmitLtmFlags);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmTargetMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMacAddress (UINT4 *pu4ErrorCode,
                                                    UINT4 u4FsMIEcfmContextId,
                                                    UINT4 u4FsMIEcfmMdIndex,
                                                    UINT4 u4FsMIEcfmExtMaIndex,
                                                    UINT4
                                                    u4FsMIEcfmExtMepIdentifier,
                                                    tMacAddr
                                                    TestValFsMIEcfmExtMepTransmitLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmTargetMacAddress (pu4ErrorCode,
                                                          u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmExtMaIndex,
                                                          u4FsMIEcfmExtMepIdentifier,
                                                          TestValFsMIEcfmExtMepTransmitLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmTargetMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMepId (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMIEcfmContextId,
                                               UINT4 u4FsMIEcfmMdIndex,
                                               UINT4 u4FsMIEcfmExtMaIndex,
                                               UINT4 u4FsMIEcfmExtMepIdentifier,
                                               UINT4
                                               u4TestValFsMIEcfmExtMepTransmitLtmTargetMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLtmTargetMepId (pu4ErrorCode,
                                                            u4FsMIEcfmMdIndex,
                                                            u4FsMIEcfmExtMaIndex,
                                                            u4FsMIEcfmExtMepIdentifier,
                                                            u4TestValFsMIEcfmExtMepTransmitLtmTargetMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmTargetIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTargetIsMepId (UINT4 *pu4ErrorCode,
                                                 UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmExtMaIndex,
                                                 UINT4
                                                 u4FsMIEcfmExtMepIdentifier,
                                                 INT4
                                                 i4TestValFsMIEcfmExtMepTransmitLtmTargetIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLtmTargetIsMepId (pu4ErrorCode,
                                                              u4FsMIEcfmMdIndex,
                                                              u4FsMIEcfmExtMaIndex,
                                                              u4FsMIEcfmExtMepIdentifier,
                                                              i4TestValFsMIEcfmExtMepTransmitLtmTargetIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTtl (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmExtMaIndex,
                                       UINT4 u4FsMIEcfmExtMepIdentifier,
                                       UINT4
                                       u4TestValFsMIEcfmExtMepTransmitLtmTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmTtl (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             u4FsMIEcfmExtMepIdentifier,
                                             u4TestValFsMIEcfmExtMepTransmitLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepTransmitLtmEgressIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmEgressIdentifier (UINT4 *pu4ErrorCode,
                                                    UINT4 u4FsMIEcfmContextId,
                                                    UINT4 u4FsMIEcfmMdIndex,
                                                    UINT4 u4FsMIEcfmExtMaIndex,
                                                    UINT4
                                                    u4FsMIEcfmExtMepIdentifier,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pTestValFsMIEcfmExtMepTransmitLtmEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmEgressIdentifier (pu4ErrorCode,
                                                          u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmExtMaIndex,
                                                          u4FsMIEcfmExtMepIdentifier,
                                                          pTestValFsMIEcfmExtMepTransmitLtmEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmExtMaIndex,
                                  UINT4 u4FsMIEcfmExtMepIdentifier,
                                  INT4 i4TestValFsMIEcfmExtMepRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepRowStatus (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmExtMaIndex,
                                        u4FsMIEcfmExtMepIdentifier,
                                        i4TestValFsMIEcfmExtMepRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmExtMepCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier

                The Object 
                testValFsMIEcfmExtMepCcmOffload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmExtMepCcmOffload (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmExtMaIndex,
                                   UINT4 u4FsMIEcfmExtMepIdentifier,
                                   INT4 i4TestValFsMIEcfmExtMepCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMepCcmOffload (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmExtMaIndex,
                                             u4FsMIEcfmExtMepIdentifier,
                                             i4TestValFsMIEcfmExtMepCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmExtMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmExtMaIndex
                FsMIEcfmExtMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmExtMepTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
