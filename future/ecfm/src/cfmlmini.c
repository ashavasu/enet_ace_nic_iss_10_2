/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlmini.c,v 1.21 2015/04/16 06:29:36 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way 
 *              and 2 way Loss Mesaurement of Control Sub Module.
 *******************************************************************/

#include "cfminc.h"
PRIVATE INT4 EcfmLmInitXmitLmmPdu PROTO ((tEcfmCcPduSmInfo *));
PRIVATE VOID EcfmLmInitFormatLmmPduHdr PROTO ((tEcfmCcMepInfo *, UINT1 **));
PRIVATE VOID EcfmLmResetVars PROTO ((tEcfmCcLmInfo *));

/*******************************************************************************
 * Function           : EcfmCcClntLmInitiator
 *
 * Description        : The routine implements the LM Initiator, it calls up
 *                      routine to format and transmit LMM frame.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC UINT4
EcfmCcClntLmInitiator (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    UINT4               u4RetVal = ECFM_SUCCESS;
    UINT4               u4DeadlineTime = ECFM_INIT_VAL;
    UINT4               u4TxFrames = ECFM_INIT_VAL;
    UINT4               u4RxFrames = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepInfo);

    /* Check for the received event */
    switch (u1Event)

    {
        case ECFM_EV_MEP_BEGIN:

            /*Set LM Initiator related values to their defaults */
            pLmInfo->u1TxLmmStatus = ECFM_TX_STATUS_READY;
            break;
        case ECFM_CC_LM_DEADLINE_EXPIRY:

            /*
             * Wait for one more extra unit for timing out the last 
             * response
             * There is a very much possibility that the deadline timer expired
             * before timing out the response of the last request, in this case
             * the response will be received after the expiry of the
             * state-machine and thus the response will be treated as invalid
             * and which is not correct.
             */
            if (ECFM_GET_EXTRA_DEADLINE_STATUS (pLmInfo->u4TxLmmDeadline) ==
                ECFM_FALSE)

            {
                UINT4               u4LmmInterval = ECFM_INIT_VAL;
                ECFM_GET_LMM_INTERVAL (pLmInfo->u2TxLmmInterval, u4LmmInterval);
                ECFM_SET_EXTRA_DEADLINE_STATUS (pLmInfo->u4TxLmmDeadline);

                /* from now on we have to mask the Interval timers expiry 
                 * events
                 */
                if (EcfmCcTmrStartTimer
                    (ECFM_CC_TMR_LM_DEADLINE, pPduSmInfo,
                     u4LmmInterval) != ECFM_SUCCESS)

                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                                 ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcClntLmInitiator: "
                                 "Extra time DeadLine Timer has not started\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
                break;
            }
            /* Sync event at STANDBY Node also */
            EcfmRedSyncLmDeadlineTmrExp (pMepInfo);
        case ECFM_EV_MEP_NOT_ACTIVE:
        case ECFM_LM_STOP_TRANSACTION:

            /* Stop the LM Transaction */
            EcfmLmInitStopLmTransaction (pPduSmInfo);

            /* Sync LM Cache */
            EcfmRedSyncLmCache ();
            break;
        case ECFM_CC_LM_WHILE_EXPIRY:
            /* ignore the interval expiry event in case we are waiting
             * for the last reponse
             */
            if (ECFM_GET_EXTRA_DEADLINE_STATUS (pLmInfo->u4TxLmmDeadline) ==
                ECFM_TRUE)

            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntLmInitiator: ECFM_CC_LM_WHILE_EXPIRY ignored\r\n");
                break;
            }
        case ECFM_LM_START_TRANSACTION:
            if (EcfmCcGetLocDefectFromRmep (pMepInfo, pLmInfo->u2TxLmDestMepId)
                == ECFM_SUCCESS)

            {

                /* There is no connectivity between this MEP and the remote MEP.
                 * In this case the frame loss is considered as 100%.
                 * Add a node in the frame loss table and notify the registered 
                 * applications
                 */
                if (u1Event == ECFM_LM_START_TRANSACTION)
                {
                    ECFM_CC_INCR_LM_TRANS_ID (pMepInfo);
                    EcfmLmResetVars (pLmInfo);
                    ECFM_CC_RESET_LM_SEQ_NUM (pMepInfo);
                }
                pFrmLossBuffNode = EcfmLmInitAddLmEntry (pMepInfo);
                if (pFrmLossBuffNode == NULL)

                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcClntCalcFrameLoss: "
                                 "Failure adding node in the frame loss buffer\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
                pFrmLossBuffNode->RxTimeStamp = EcfmUtilGetTimeFromEpoch ();
                pFrmLossBuffNode->u4MeasurementInterval = ECFM_INIT_VAL;

                /* In case of LOC, the far end loss is not known.
                 * Near end loss will be all the PDUs that have been sent
                 */
                if (pMepInfo->pEcfmMplsParams != NULL)
                {
                    if (pMepInfo->pEcfmMplsParams->u1MplsPathType ==
                        MPLS_PATH_TYPE_PW)
                    {
                        EcfmGetMplsPktCnt (pMepInfo, &u4TxFrames, &u4RxFrames);
                    }
                }
                else
                {

                    EcfmGetPacketCounters (pMepInfo->pPortInfo->u4IfIndex,
                                           pMepInfo->u4PrimaryVidIsid,
                                           &u4TxFrames, &u4RxFrames);
                }
                pFrmLossBuffNode->u4NearEndLoss =
                    u4TxFrames - pLmInfo->u4PreTxFCf;
                pFrmLossBuffNode->u4FarEndLoss = ECFM_INIT_VAL;
            }
            if (u1Event == ECFM_LM_START_TRANSACTION)

            {

                /* Set the transmission status as not ready 
                 * so that no other transaction can happen
                 * before this finishes
                 */
                pLmInfo->u1TxLmmStatus = ECFM_TX_STATUS_NOT_READY;
                ECFM_CC_INCR_LM_TRANS_ID (pMepInfo);

                /* Start the deadline timer if configured */
                if (pLmInfo->u4TxLmmDeadline != ECFM_INIT_VAL)

                {
                    u4DeadlineTime =
                        ECFM_NUM_OF_MSEC_IN_A_SEC * pLmInfo->u4TxLmmDeadline;

                    /* Start the LM Deadline Timer */
                    if (EcfmCcTmrStartTimer
                        (ECFM_CC_TMR_LM_DEADLINE, pPduSmInfo,
                         u4DeadlineTime) != ECFM_SUCCESS)

                    {
                        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                                     ECFM_ALL_FAILURE_TRC,
                                     "EcfmCcClntLmInitiator:"
                                     "Start of LM Deadline Timer FAILED\r\n");
                        u4RetVal = ECFM_FAILURE;
                        break;
                    }
                }

                /* Check if infinite transmission is required */
                if (pLmInfo->u2TxLmmMessages == ECFM_INIT_VAL)

                {
                    ECFM_SET_INFINITE_TX_STATUS (pLmInfo->u2TxLmmMessages);
                }

                /* This is the start of a transaction. Reset All the counters and
                 * variables
                 */
                EcfmLmResetVars (pLmInfo);
                ECFM_CC_RESET_LM_SEQ_NUM (pMepInfo);
            }

            /* Transmit the LMM PDU */
            if (EcfmLmInitXmitLmmPdu (pPduSmInfo) != ECFM_SUCCESS)

            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntLmInitiator:LMM Transmission FAILED !!!\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            break;
        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntLmInitiator: Event Not Supported\r\n");
            u4RetVal = ECFM_FAILURE;
            break;
    }
    if (u4RetVal == ECFM_FAILURE)

    {

        /* Update Result OK to False */
        pLmInfo->b1ResultOk = ECFM_FALSE;

        /* Stop the DM Transaction */
        EcfmLmInitStopLmTransaction (pPduSmInfo);
    }

    else

    {

        /* Update Result OK to True */
        pLmInfo->b1ResultOk = ECFM_TRUE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmLmInitXmitLmmPdu
 *
 * Description        : This routine formats and transmits the LMM PDU.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that stores the
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *******************************************************************************/
PRIVATE INT4
EcfmLmInitXmitLmmPdu (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    UINT4               u4TxFCf = ECFM_INIT_VAL;
    UINT4               u4RxFCf = ECFM_INIT_VAL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4Duration = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1LmmPduStart = NULL;
    UINT1              *pu1LmmPduEnd = NULL;
#ifdef NPAPI_WANTED
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
#endif

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepInfo);

    /* Check if the desired number of LMM messages have been sent */
    if ((pLmInfo->u2TxLmmMessages == ECFM_INIT_VAL) &&
        (pLmInfo->b1TxLmByAvlbility != ECFM_TRUE))

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLmInitXmitLmmPdu: "
                     "Stopping LM Transaction as the configured number of LMM"
                     " PDUs have been transmitted\r\n");

        /* Stop LM transaction */
        EcfmLmInitStopLmTransaction (pPduSmInfo);

        /* Sync Stop LM Transaction at STAND BY node also */
        EcfmRedSyncLmStopTransaction (pPduSmInfo->pMepInfo);

        /* Sync LB Cache */
        EcfmRedSyncLmCache ();
        return ECFM_SUCCESS;
    }

    /* Start forming the PDU.
     * Alloacte CRU Buffer first 
     */
    if (pMepInfo->pEcfmMplsParams != NULL)
    {
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_LMM_PDU_SIZE,
                                   ECFM_MPLSTP_CTRL_PKT_OFFSET);
        if (pBuf == NULL)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmLmInitXmitLmmPdu: Buffer allocation failed\r\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_LMM_PDU_SIZE, ECFM_INIT_VAL);
        if (pBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmLmInitXmitLmmPdu: Buffer allocation failed\r\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }

    ECFM_MEMSET (ECFM_CC_PDU, ECFM_INIT_VAL, ECFM_MAX_LMM_PDU_SIZE);
    pu1LmmPduStart = ECFM_CC_PDU;
    pu1LmmPduEnd = ECFM_CC_PDU;
    pu1EthLlcHdr = ECFM_CC_PDU;

    /* Format the LMM PDU header */
    EcfmLmInitFormatLmmPduHdr (pMepInfo, &pu1LmmPduEnd);

    /* Fill rest of the fields in LMM PDU */
    /* Fetch the value of TxFcf(u4TxFCf) */

    if (pMepInfo->pEcfmMplsParams != NULL)
    {
        if (pMepInfo->pEcfmMplsParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
        {
            EcfmGetMplsPktCnt (pMepInfo, &u4TxFCf, &u4RxFCf);
        }
    }
    else
    {

        EcfmGetPacketCounters (pMepInfo->pPortInfo->u4IfIndex,
                               pMepInfo->u4PrimaryVidIsid, &u4TxFCf, &u4RxFCf);
    }
    ECFM_PUT_4BYTE (pu1LmmPduEnd, u4TxFCf);

    /* Fill the next 8 bytes with ZERO as they will be used for LMR PDU */
    ECFM_MEMSET (pu1LmmPduEnd, ECFM_RESERVE_VALUE, ECFM_LMM_RESERVED_SIZE);
    pu1LmmPduEnd = pu1LmmPduEnd + ECFM_LMM_RESERVED_SIZE;

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1LmmPduEnd, ECFM_END_TLV_TYPE);
    u2PduLength = (UINT2) (pu1LmmPduEnd - pu1LmmPduStart);

    /* Copy the formed PDU over CRU Buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1LmmPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmInitXmitLmmPdu: "
                     "Buffer copy operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Fill the information for the VLAN Tag */
    ECFM_GET_LMM_INTERVAL (pLmInfo->u2TxLmmInterval, u4Duration);

    /* Start LMWhile Timer */
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_LM_WHILE, pPduSmInfo, u4Duration)
        != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmInitXmitLmmPdu: "
                     "Failure starting LM While timer\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    if (pMepInfo->pEcfmMplsParams != NULL)
    {
        /* Dump the PDU */
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                          "EcfmLmInitXmitLmmPdu: Dumping LMM-PDU... r\n");

        /* This MEP belongs to MPLS TP network. Do not add an ethernet header.
         * send the raw packet for transmission
         * */
        if (EcfmMplsTpCcTxPacket (pBuf, pMepInfo, ECFM_OPCODE_LMM) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCciSmXmitLmmPdu: Transmit CFMPDU failed\n");
            i4RetVal = ECFM_FAILURE;
        }
        else
        {
            u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
            ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                              "EcfmCciSmXmitLmmPdu: Sending out CFMPDU to "
                              "lower layer.\n");
            ECFM_CC_TRC_FN_EXIT ();
            i4RetVal = ECFM_SUCCESS;
        }
    }
    else
    {

        /* Prepare the Ethernet and LLC header */
        pu1LmmPduStart = pu1EthLlcHdr;
        EcfmFormatCcTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr, u2PduLength,
                                   ECFM_OPCODE_LMM);
        /* Prepend the Ethernet and LLC Header to CRU Buffer */
        u2PduLength = (UINT2) (pu1EthLlcHdr - pu1LmmPduStart);
        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1LmmPduStart,
                                  (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmLmInitXmitLmmPdu: "
                         "Buffer prepend operation failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }

        /* Dump the PDU */
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                          "EcfmLmInitXmitLmmPdu: Dumping LMM-PDU... r\n");

#ifdef NPAPI_WANTED
        if (ECFM_HW_LMM_SUPPORT () == ECFM_FALSE)
        {
            i4RetVal =
                EcfmCcCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                         pMepInfo->u4PrimaryVidIsid,
                                         pLmInfo->u1TxLmmPriority,
                                         pLmInfo->b1LmmDropEnable,
                                         pMepInfo->u1Direction, ECFM_OPCODE_LMM,
                                         NULL, NULL);
        }
        else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
        {
            tEcfmVlanTagInfo    VlanTagInfo;
            UINT1              *pu1LmmPdu = ECFM_CC_PDU;
            ECFM_MEMSET (&VlanTagInfo, ECFM_INIT_VAL,
                         sizeof (tEcfmVlanTagInfo));
            VlanTagInfo.u2VlanId = pMepInfo->u4PrimaryVidIsid;
            VlanTagInfo.u1Priority = pLmInfo->u1TxLmmPriority;
            VlanTagInfo.u1DropEligible = (UINT1) pLmInfo->b1LmmDropEnable;
            VlanTagInfo.u1TagType = ECFM_VLAN_UNTAGGED;
            /*Copy the CRU-BUFF into linear buffer */
            ECFM_COPY_FROM_CRU_BUF (pBuf, pu1LmmPdu, ECFM_INIT_VAL, u4PduSize);

            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;

            if (EcfmFsMiEcfmTransmitLmm
                (ECFM_CC_CURR_CONTEXT_ID (),
                 ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo),
                 pu1LmmPdu, (UINT2) u4PduSize, VlanTagInfo,
                 pMepInfo->u1Direction) != FNP_SUCCESS)

            {
                i4RetVal = ECFM_FAILURE;
            }

            else
            {
                ECFM_LBLT_INCR_TX_COUNT (pMepInfo->u2PortNum, ECFM_OPCODE_LMM);
                ECFM_LBLT_INCR_CTX_TX_COUNT (pMepInfo->u4ContextId,
                                             ECFM_OPCODE_LMM);
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                pBuf = NULL;
                i4RetVal = ECFM_SUCCESS;
            }
        }
#else /*  */
        i4RetVal =
            EcfmCcCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                     pMepInfo->u4PrimaryVidIsid,
                                     pLmInfo->u1TxLmmPriority,
                                     pLmInfo->b1LmmDropEnable,
                                     pMepInfo->u1Direction, ECFM_OPCODE_LMM,
                                     NULL, NULL);

#endif /*  */
    }

    /* Transmit the PDU */
    if (i4RetVal != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmLmInitXmitLmmPdu: "
                     "LMM transmission to the lower layer failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        ECFM_CC_INCR_TX_FAILED_COUNT (pMepInfo->u2PortNum);
        ECFM_CC_INCR_CTX_TX_FAILED_COUNT (pMepInfo->u4ContextId);
        return ECFM_FAILURE;
    }

    /* Decrement the no of LMMs to be transmitted if 
     * infinite transmission is not desired
     */
    if (ECFM_GET_INFINITE_TX_STATUS (pLmInfo->u2TxLmmMessages) == ECFM_FALSE)

    {

        /* Decrement the number of LM Message to be transmitted */
        ECFM_CC_DECR_TRANSMIT_LM_MSG (pMepInfo);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLmInitFormatLmmPduHdr
 *
 * Description        : This rotuine is used to fill the LMM CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1LmmPdu - Pointer to Pointer to the LMM PDU.
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLmInitFormatLmmPduHdr (tEcfmCcMepInfo * pMepInfo, UINT1 **ppu1LmmPdu)
{
    UINT1              *pu1LmmPdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();
    pu1LmmPdu = *ppu1LmmPdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1LmmPdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1LmmPdu, ECFM_OPCODE_LMM);

    /* Fill the Next 1 byte with Flag In LMM, Flag Field is set to ZERO */
    ECFM_PUT_1BYTE (pu1LmmPdu, 0);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1LmmPdu, ECFM_LMM_FIRST_TLV_OFFSET);
    *ppu1LmmPdu = pu1LmmPdu;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLmInitStopLmTransaction
 *
 * Description        : This routine is used to stop the on going LM transaction
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmLmInitStopLmTransaction (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepInfo);

    /* Stop LMWhile Timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_LM_WHILE, pPduSmInfo);

    /* Stop LMDeadline Timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_LM_DEADLINE, pPduSmInfo);

    /* Revert Back the Tx Status */
    pMepInfo->LmInfo.u1TxLmmStatus = ECFM_TX_STATUS_READY;
    ECFM_CLEAR_INFINITE_TX_STATUS (pMepInfo->LmInfo.u2TxLmmMessages);
    ECFM_CLEAR_EXTRA_DEADLINE_STATUS (pMepInfo->LmInfo.u4TxLmmDeadline);
    pMepInfo->LmInfo.u2TxLmmMessages = ECFM_INIT_VAL;

    /* Stop Availability and calculate Percentage */
    if (pLmInfo->b1TxLmByAvlbility == ECFM_TRUE)
    {
        EcfmCcAvlbltyStopTransaction (pPduSmInfo);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLmResetVars 
 *
 * Description        : This routine is used to reset the LMM specific variables
 *                      at the start of a transaction
 *                        
 * Input(s)           : pLmInfo - Pointer to the structure that stores the 
 *                      information regarding Loss Measurement.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLmResetVars (tEcfmCcLmInfo * pLmInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

    /* At the start of a transaction set all the counters to ZERO */
    pLmInfo->u4RxFCf = ECFM_INIT_VAL;
    pLmInfo->u4PreTxFCf = ECFM_INIT_VAL;
    pLmInfo->u4PreTxFCb = ECFM_INIT_VAL;
    pLmInfo->u4PreRxFCl = ECFM_INIT_VAL;
    pLmInfo->u4PreRxFCb = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}
