/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmrbutl.c,v 1.1 2014/02/26 12:14:26 siva Exp $
 *
 * Description:This file contains the functions for RBTree utilities
 *             for L2IWF used when enabling the ECFM_ARRAY_TO_RBTREE_WANTED flag
 *
 *****************************************************************************/
#include <cfminc.h>

/*****************************************************************************
 *                  UTILITIES FOR  Global CC LocalPort RBTREE
 ******************************************************************************/
/*****************************************************************************/
/* Function Name      : EcfmCcLocalPortTableGlobalCmp                        */
/*                                                                           */
/* Description        : This routine is used in LocaPort Table for comparing */
/*                      key(Port Index) used in RBTree functionality.        */
/*                                                                           */
/* Input(s)           : Two node of LocalPort Table to be compared           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*    Returns            : 1  - pRBElem1 > pRBElem2                          */
/*                        -1  - pRBElem1 < pRBElem2                          */
/*                         0  - both are equal                               */
/*****************************************************************************/
INT4
EcfmCcLocalPortTableGlobalCmp (tRBElem * pRBElem1,
                                 tRBElem *pRBElem2)
{
    tEcfmCcPortInfo *pPortEntry = NULL;
    tEcfmCcPortInfo *pPortEntryIn = NULL;

    pPortEntry = (tEcfmCcPortInfo *)pRBElem1;
    pPortEntryIn = (tEcfmCcPortInfo *)pRBElem2;

      /* ContextId is  the Primary key*/
    if (pPortEntry->u4ContextId < pPortEntryIn->u4ContextId)
    {
        return ECFM_RBTREE_KEY_LESSER;
    }
    if (pPortEntry->u4ContextId > pPortEntryIn->u4ContextId)
        {
        return ECFM_RBTREE_KEY_GREATER;
    }

      /* LocalPortNum is  the Secondary key*/
    if (pPortEntry->u2PortNum < pPortEntryIn->u2PortNum)
    {
        return ECFM_RBTREE_KEY_LESSER;
    }
    if (pPortEntry->u2PortNum > pPortEntryIn->u2PortNum)
        {
        return ECFM_RBTREE_KEY_GREATER;
    }

    return ECFM_RBTREE_KEY_EQUAL;
}
/*****************************************************************************/
/* Function Name      : EcfmAddCcLocalPortEntry                              */
/*                                                                           */
/* Description        : This routine adds PortEntry to the CC LocalPort Table*/
/*                                                                           */
/* Input(s)           : pPortEntry     - Entry to be added                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURS                            */
/*****************************************************************************/
INT4
EcfmAddCcLocalPortEntry (tEcfmCcPortInfo *pPortEntry)
{
    tEcfmCcPortInfo     *pTempPortEntry = NULL;

     pTempPortEntry = (tEcfmCcPortInfo*) RBTreeGet
                           (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) pPortEntry);
     if (pTempPortEntry != NULL)
     {
         /*Already Entry is present in Cc LocalPort Table*/
         return ECFM_SUCCESS;
     }
    /*Add a a Entry in Cc LocalPort Table*/
    if (RBTreeAdd (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE ,(tRBElem *) pPortEntry)
            == RB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmDelCcLocalPortEntry                              */
/*                                                                           */
/* Description        : This routine deletes Port entry from CCLocalPort Table*/
/*                                                                           */
/* Input(s)           : pPortEntry - Entry to be deleted                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURS                            */
/*****************************************************************************/
INT4
EcfmDelCcLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId)
{
    tEcfmCcPortInfo     *pPortEntry = NULL;
    
    pPortEntry = EcfmGetCcLocalPortEntry (u4ContextId , u2PortId);
    if (pPortEntry == NULL)
    {
        return ECFM_SUCCESS;
    }

    if (RBTreeRemove (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) pPortEntry)
            == RB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmGetCcLocalPortEntry                              */
/*                                                                           */
/* Description        : This routine gets a port entry from CCLocalPort Table*/
/*                                                                           */
/* Input(s)           : u2PortId - PortID                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pPortEntry  - Pointer to the tEcfmCcPortInfo         */
/*****************************************************************************/
tEcfmCcPortInfo*
EcfmGetCcLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId)
{
     tEcfmCcPortInfo    PortEntry;
     tEcfmCcPortInfo   *pPortEntry = NULL;

     MEMSET(&PortEntry, 0, sizeof (tEcfmCcPortInfo));

     PortEntry.u4ContextId = u4ContextId;
     PortEntry.u2PortNum = u2PortId;

     pPortEntry = (tEcfmCcPortInfo *) RBTreeGet
                     (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) &PortEntry);
     return pPortEntry;
}

/*****************************************************************************/
/* Function Name      : EcfmGetNextCcLocalPortEntry                          */
/*                                                                           */
/* Description        : This routine gets Next port entry from CCLocalPort Table*/
/*                                                                           */
/* Input(s)           : u2PortId - PortID                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pPortEntry  - Pointer to the tEcfmCcPortInfo         */
/*****************************************************************************/
tEcfmCcPortInfo*
EcfmGetNextCcLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId)
{
     tEcfmCcPortInfo    PortEntry;
     tEcfmCcPortInfo   *pPortEntry = NULL;

     MEMSET(&PortEntry, 0, sizeof (tEcfmCcPortInfo));

     PortEntry.u4ContextId = u4ContextId;
     PortEntry.u2PortNum = u2PortId;

     pPortEntry = (tEcfmCcPortInfo *) RBTreeGetNext
                     (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) &PortEntry, NULL);
     return pPortEntry;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : EcfmGetPortsFromDB                                  */
 /*                                                                           */
 /* Description         : This function reads the (u4Context + u2PortNum)     */
 /*                       tuple. And if the u2Flag for this tuple is          */
 /*                       set in the data base, then it will be set in the    */
 /*                       LocalPortList.                                      */
 /*                                                                           */
 /* Input(s)            : u4ContextId   - Context Identifier                  */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */                                                                              /* Output(s)           : LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Returns             :  NONE.                                              */
 /*****************************************************************************/
VOID
EcfmGetPortsFromDB (UINT4 u4ContextId, UINT2 u2Flag, tLocalPortListExt LocalPortList)
{
    tEcfmCcPortInfo    *pPortEntry = NULL;
    UINT2               u2PortNum = 0;
    /* Do not memset LocalPortList since, it may have some ports already set */

    while ((pPortEntry = EcfmGetNextCcLocalPortEntry
            (u4ContextId, u2PortNum)) != NULL)
    {
        if (u4ContextId != pPortEntry->u4ContextId)
        {
            break;
        }
        if ((pPortEntry->u2LocalPortMask & u2Flag) == u2Flag)
        {
            ECFM_SET_MEMBER_PORT (LocalPortList, pPortEntry->u2PortNum);
        }
        u2PortNum = pPortEntry->u2PortNum;
    }
    return;
}


 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : EcfmResetSinglePortInDB                             */
 /*                                                                           */
 /* Description         : This function resets  a node with u4ContextId       */
 /*                       and u2PortNum  provided with the given u2Flag.      */
 /*                                                                           */
 /* Input(s)            : u4ContextId Context Identifier                      */
 /*                       u2PortNum    - Local Port identifier                */
 /*                       u2Flag    - Port property flag                      */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : ECFM_FAILURE/ECFM_SUCCESS.                          */
 /*****************************************************************************/
INT4
EcfmResetSinglePortInDB (UINT4 u4ContextId, UINT2 u2PortNum, UINT2 u2Flag)
{
    tEcfmCcPortInfo    *pPortEntry = NULL;

    if ((pPortEntry = EcfmGetCcLocalPortEntry (u4ContextId, u2PortNum))
        == NULL)
    {
        return ECFM_FAILURE;
    }

    pPortEntry->u2LocalPortMask &= ((UINT2)(~u2Flag));
    return ECFM_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : EcfmAddSinglePortInDB                               */
 /*                                                                           */
 /* Description         : This function creates a node with u4ContextId and   */
 /*                       u2PortNum and then sets the u2Flag if the node is not*/
 /*                       created already.                                    */
 /*                       It just sets the u2Flag for the respective          */
 /*                       u2Vlan + u2PortNum Tuple if the node exists.        */
 /*                                                                           */
 /* Input(s)            : u4ContextId - Context Identifier                    */
 /*                       u2PortNum - Local Port identifier                   */
 /*                       u2Flag    - Port property flag                      */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : ECFM_FAILURE/ECFM_SUCCESS.                          */
 /*****************************************************************************/
INT4
EcfmAddSinglePortInDB (UINT4 u4ContextId, UINT2 u2PortNum, UINT2 u2Flag)
{
    tEcfmCcPortInfo    *pPortEntry = NULL;

    if ((pPortEntry = EcfmGetCcLocalPortEntry (u4ContextId, u2PortNum))
        == NULL)
    {
        return ECFM_FAILURE;
    }

    pPortEntry->u2LocalPortMask |= u2Flag;
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *                  UTILITIES FOR  Global LBLT LocalPort RBTREE
 ******************************************************************************/
/*****************************************************************************/
/* Function Name      : EcfmLbLtLocalPortTableGlobalCmp                      */
/*                                                                           */
/* Description        : This routine is used in LocaPort Table for comparing */
/*                      key(Port Index) used in RBTree functionality.        */
/*                                                                           */
/* Input(s)           : Two node of LocalPort Table to be compared           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*    Returns            : 1  - pRBElem1 > pRBElem2                          */
/*                        -1  - pRBElem1 < pRBElem2                          */
/*                         0  - both are equal                               */
/*****************************************************************************/
INT4
EcfmLbLtLocalPortTableGlobalCmp (tRBElem * pRBElem1,
                                 tRBElem *pRBElem2)
{
    tEcfmLbLtPortInfo *pPortEntry = NULL;
    tEcfmLbLtPortInfo *pPortEntryIn = NULL;

    pPortEntry = (tEcfmLbLtPortInfo *)pRBElem1;
    pPortEntryIn = (tEcfmLbLtPortInfo *)pRBElem2;

      /* ContextId is  the Primary key*/
    if (pPortEntry->u4ContextId < pPortEntryIn->u4ContextId)
    {
        return ECFM_RBTREE_KEY_LESSER;
    }
    if (pPortEntry->u4ContextId > pPortEntryIn->u4ContextId)
        {
        return ECFM_RBTREE_KEY_GREATER;
    }

      /* LocalPortNum is  the Secondary key*/
    if (pPortEntry->u2PortNum < pPortEntryIn->u2PortNum)
    {
        return ECFM_RBTREE_KEY_LESSER;
    }
    if (pPortEntry->u2PortNum > pPortEntryIn->u2PortNum)
        {
        return ECFM_RBTREE_KEY_GREATER;
    }

    return ECFM_RBTREE_KEY_EQUAL;
}

/*****************************************************************************/
/* Function Name      : EcfmAddLbLtLocalPortEntry                            */
/*                                                                           */
/* Description        : This routine adds PortEntry to LBLT LocalPort Table  */
/*                                                                           */
/* Input(s)           : pPortEntry     - Entry to be added                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURS                            */
/*****************************************************************************/
INT4
EcfmAddLbLtLocalPortEntry (tEcfmLbLtPortInfo *pPortEntry)
{
    tEcfmLbLtPortInfo     *pTempPortEntry = NULL;

     pTempPortEntry = (tEcfmLbLtPortInfo*) RBTreeGet
                           (ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) pPortEntry);
     if (pTempPortEntry != NULL)
     {
         /*Already Entry is present in PortInfo Table*/
         return ECFM_SUCCESS;
     }
    /*Add a a Entry in PortInfo Table*/
    if (RBTreeAdd (ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) pPortEntry)
            == RB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmDelLbLtLocalPortEntry                            */
/*                                                                           */
/* Description        : This routine deletes Port entry from LBLT LocalPortTable*/
/*                                                                           */
/* Input(s)           : pPortEntry - Entry to be deleted                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURS                            */
/*****************************************************************************/
INT4
EcfmDelLbLtLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId)
{
    tEcfmLbLtPortInfo  *pPortEntry = NULL;

    pPortEntry = EcfmGetLbLtLocalPortEntry(u4ContextId, u2PortId);
    if (pPortEntry == NULL)
    {
        return ECFM_SUCCESS;
    }

    if (RBTreeRemove (ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) pPortEntry)
            == RB_FAILURE)
    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmGetLbLtLocalPortEntry                            */
/*                                                                           */
/* Description        : This routine gets a port entry from LBLT LocalPortTable*/
/*                                                                           */
/* Input(s)           : u2PortId - PortID                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pPortEntry  - Pointer to the tEcfmLbLtPortInfo       */
/*****************************************************************************/
tEcfmLbLtPortInfo*
EcfmGetLbLtLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId)
{
     tEcfmLbLtPortInfo  PortEntry;
     tEcfmLbLtPortInfo  *pPortEntry = NULL;

     MEMSET(&PortEntry, 0, sizeof (tEcfmLbLtPortInfo));

     PortEntry.u4ContextId = u4ContextId;
     PortEntry.u2PortNum = u2PortId;

     pPortEntry = (tEcfmLbLtPortInfo*) RBTreeGet
                     (ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE, (tRBElem *) &PortEntry);
     return pPortEntry;
}

