/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbutl.c,v 1.43 2016/07/25 07:25:12 siva Exp $
 *
 * Description: This file contains the Utility Procedures used for 
 *              LBLT Task.
 *******************************************************************/

#include "cfminc.h"
PRIVATE UINT4 EcfmLbLtUtilReflectBits PROTO ((UINT4, INT4));

/*****************************************************************************
 *    FUNCTION NAME    : EcfmLbLtUtilGetMepEntryFrmPort
 *
 *    DESCRIPTION      : This function returns the pointer to the Mep entry
 *                       
 *    INPUT            : u2PortNum - Local Port Number  for which the entry is
 *                                   required,
 *                       u1MdLevel -   Md Level of the MEP.
 *                       u1Direction - Direction of the MEP.
 *                       u2Vid     -   Primary Vlan Id of the MEP.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to MepInfo.
 *
 *****************************************************************************/
tEcfmLbLtMepInfo   *
EcfmLbLtUtilGetMepEntryFrmPort (UINT1 u1MdLevel, UINT4 u4VidIsid,
                                UINT2 u2PortNum, UINT1 u1Direction)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);
    gpEcfmLbLtMepNode->u1Direction = u1Direction;
    gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
    gpEcfmLbLtMepNode->u1MdLevel = u1MdLevel;
    gpEcfmLbLtMepNode->u4PrimaryVidIsid = u4VidIsid;

    /* Get MEP from PortInfo's MepInfoTree */
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo != NULL)

    {
        pMepNode = RBTreeGet (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode);
    }
    return pMepNode;
}

/****************************************************************************
 *  
 *  FUNCTION NAME    : EcfmLbLtUtilFreeEntryFn
 * 
 *  DESCRIPTION      : This function releases the memory allocated to
 *                     each node of the specified Table.
 *  
 *  INPUT            : pRBElem - pointer to the node to be freed.
 *  
 *  OUTPUT           : None.
 *
 *  RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 * **************************************************************************/
PUBLIC INT4
EcfmLbLtUtilFreeEntryFn (tRBElem * pElem, UINT4 u4Arg)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    tEcfmLbLtStackInfo *pStackNode = NULL;
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNode = NULL;
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    tEcfmLbLtLtrInfo   *pLtrTempNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrTempNode = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelyBuffNode = NULL;
    tEcfmLbLtDefaultMdTableInfo *pLbLtDefMdNode = NULL;
    tEcfmLbLtLtmReplyListInfo LtmReplyListInfo;

    switch (u4Arg)
    {
        case ECFM_LBLT_STACK_ENTRY_IN_PORT:
            pStackNode = (tEcfmLbLtStackInfo *) pElem;
            pStackNode->pLbLtMepInfo = NULL;
            pStackNode->pLbLtMipInfo = NULL;
            /* Releasing mem block allocated to MEP */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                                 (UINT1 *) (pStackNode));
            break;
        case ECFM_LBLT_LTR_ENTRY:
            pLtrNode = (tEcfmLbLtLtrInfo *) pElem;

            /*Remove other references of this node, if exists */
            pMepNode =
                EcfmLbLtUtilGetMepEntryFrmGlob (pLtrNode->u4MdIndex,
                                                pLtrNode->u4MaIndex,
                                                pLtrNode->u2MepId);
            if (pMepNode == NULL)
            {
                break;
            }
            ECFM_MEMSET (&LtmReplyListInfo, ECFM_INIT_VAL,
                         ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);
            LtmReplyListInfo.u4MdIndex = pLtrNode->u4MdIndex;
            LtmReplyListInfo.u4MaIndex = pLtrNode->u4MaIndex;
            LtmReplyListInfo.u2MepId = pLtrNode->u2MepId;
            LtmReplyListInfo.u4LtmSeqNum = pLtrNode->u4SeqNum;
            pLtmReplyListNode = RBTreeGet (ECFM_LBLT_LTM_REPLY_LIST,
                                           (tRBElem *) & LtmReplyListInfo);
            if (pLtmReplyListNode == NULL)

            {
                break;
            }
            TMO_DLL_Delete (&(pLtmReplyListNode->LtrList),
                            &(pLtrNode->LtrTableMepDllNode));
            pSenderId = &(pLtrNode->SenderId);
            if (pSenderId->ChassisId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL,
                                     pSenderId->ChassisId.pu1Octets);
                pSenderId->ChassisId.pu1Octets = NULL;
            }
            if (pSenderId->MgmtAddress.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL,
                                     pSenderId->MgmtAddress.pu1Octets);
                pSenderId->MgmtAddress.pu1Octets = NULL;
            }
            if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL,
                                     pSenderId->MgmtAddressDomain.pu1Octets);
                pSenderId->MgmtAddressDomain.pu1Octets = NULL;
            }
            if (pLtrNode->OrgSpecTlv.Value.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL,
                                     pLtrNode->OrgSpecTlv.Value.pu1Octets);
                pLtrNode->OrgSpecTlv.Value.pu1Octets = NULL;
            }
            if (pLtrNode->EgressPortId.pu1Octets != NULL)

            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
                                     pLtrNode->EgressPortId.pu1Octets);
                pLtrNode->EgressPortId.pu1Octets = NULL;
            }

            if (pLtrNode->IngressPortId.pu1Octets != NULL)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
                                     pLtrNode->IngressPortId.pu1Octets);
                pLtrNode->IngressPortId.pu1Octets = NULL;
            }

            /* Releasing mem block allocated to MEP */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                 (UINT1 *) (pLtrNode));
            break;
        case ECFM_LBLT_STACK_ENTRY:
            pStackNode = (tEcfmLbLtStackInfo *) pElem;

            /* Release memory allocated to Stack node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_STACK_TABLE_POOL,
                                 (UINT1 *) (pStackNode));
            break;
        case ECFM_LBLT_MIP_ENTRY:
            pMipNode = (tEcfmLbLtMipInfo *) pElem;

            /* Release memory allocated to Mip node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_TABLE_POOL,
                                 (UINT1 *) (pMipNode));
            break;
        case ECFM_LBLT_LTM_REPLY_LIST_ENTRY:
            pLtmReplyListNode = (tEcfmLbLtLtmReplyListInfo *) pElem;

            /* Delete LtrListDll corresponding to a LTM and each LTR node
             * from LtrTable, in global info */
            pLtrNode = (tEcfmLbLtLtrInfo *)
                TMO_DLL_First (&(pLtmReplyListNode->LtrList));
            while (pLtrNode != NULL)

            {
                pLtrTempNode = pLtrNode;

                /* Move to next LTR corresponding to a LTM node */
                pLtrNode = (tEcfmLbLtLtrInfo *)
                    TMO_DLL_Next (&(pLtmReplyListNode->LtrList),
                                  &(pLtrTempNode->LtrTableMepDllNode));
                TMO_DLL_Delete (&(pLtmReplyListNode->LtrList),
                                &(pLtrTempNode->LtrTableMepDllNode));

                /* Remove LTR node from LTR Table in global info */
                RBTreeRem (ECFM_LBLT_LTR_TABLE, (tRBElem *) pLtrTempNode);

                /* Release the memory allocated to chassisId,Org specific TLV,
                   PortId, Mgt address and its domain */
                pSenderId = &(pLtrTempNode->SenderId);

                if (pSenderId->ChassisId.pu1Octets != NULL)
                {
                    ECFM_FREE_MEM_BLOCK
                        (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL,
                         pSenderId->ChassisId.pu1Octets);
                    pSenderId->ChassisId.pu1Octets = NULL;
                }
                if (pSenderId->MgmtAddress.pu1Octets != NULL)

                {
                    ECFM_FREE_MEM_BLOCK
                        (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL,
                         pSenderId->MgmtAddress.pu1Octets);
                    pSenderId->MgmtAddress.pu1Octets = NULL;
                }
                if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)

                {
                    ECFM_FREE_MEM_BLOCK
                        (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL,
                         pSenderId->MgmtAddressDomain.pu1Octets);
                    pSenderId->MgmtAddressDomain.pu1Octets = NULL;
                }
                if (pLtrTempNode->OrgSpecTlv.Value.pu1Octets != NULL)

                {
                    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL,
                                         pLtrTempNode->OrgSpecTlv.Value.
                                         pu1Octets);
                    pLtrTempNode->OrgSpecTlv.Value.pu1Octets = NULL;
                }
                if (pLtrTempNode->EgressPortId.pu1Octets != NULL)

                {
                    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL,
                                         pLtrTempNode->EgressPortId.pu1Octets);
                    pLtrTempNode->EgressPortId.pu1Octets = NULL;
                }

                if (pLtrTempNode->IngressPortId.pu1Octets != NULL)
                {
                    ECFM_FREE_MEM_BLOCK
                        (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL,
                         pLtrTempNode->IngressPortId.pu1Octets);
                    pLtrTempNode->IngressPortId.pu1Octets = NULL;
                }

                /* Releasing mem block allocated to LTR node */
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTR_TABLE_POOL,
                                     (UINT1 *) (pLtrTempNode));
                pLtrTempNode = NULL;
            }
            /* Releasing mem block allocated to LtmReplyListNode */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL,
                                 (UINT1 *) (pLtmReplyListNode));
            break;
        case ECFM_LBLT_MEP_ENTRY:
            pMepNode = (tEcfmLbLtMepInfo *) pElem;

            /* Remove other references of this node */
            /* first get if it is present in port info's Mep Tree */
            /* check if there are other references */
            /* Releasing mem block allocated to MEP */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_TABLE_POOL,
                                 (UINT1 *) (pMepNode));
            break;
        case ECFM_LBLT_LBM_ENTRY:
            pLbmNode = (tEcfmLbLtLbmInfo *) pElem;

            /* Before Deleting the LBM node, its related LBR nodes maintained 
             * in the SLL needs to be deleted */
            pLbrNode =
                (tEcfmLbLtLbrInfo *) TMO_SLL_First (&(pLbmNode->LbrList));
            while (pLbrNode != NULL)

            {
                pLbrTempNode = pLbrNode;

                /* Get the next LBR node from the LBR list */
                pLbrNode =
                    (tEcfmLbLtLbrInfo *) TMO_SLL_Next (&(pLbmNode->LbrList),
                                                       (tEcfmSllNode *) &
                                                       (pLbrTempNode->
                                                        LbrTableSllNode));

                /* Delete the LBR node from the SLL maintained by MEP per LBM */
                TMO_SLL_Delete (&(pLbmNode->LbrList),
                                &(pLbrTempNode->LbrTableSllNode));

                /* Releasing Memblock allocated to LBR node */
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LBR_TABLE_POOL,
                                     (UINT1 *) (pLbrTempNode));
                pLbrTempNode = NULL;
            }

            TMO_DLL_Delete (&(gEcfmLbLtGlobalInfo.LbmDllList),
                            &(pLbmNode->LbmDllNextNode));
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LBM_TABLE_POOL, (UINT1 *) pLbmNode);
            break;
        case ECFM_LBLT_FD_BUFFER_ENTRY:
            pFrmDelyBuffNode = (tEcfmLbLtFrmDelayBuff *) pElem;

            /* Release memory allocated to FD node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_FD_BUFFER_POOL,
                                 (UINT1 *) (pFrmDelyBuffNode));
            break;
        case ECFM_LBLT_MIP_CCM_DB_ENTRY:

            /* Release memory allocated to MIP CCM DB node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MIP_CCM_DB_POOL, (UINT1 *) (pElem));
            break;
        case ECFM_LBLT_RMEP_DB_ENTRY:

            /* Release memory allocated to MIP CCM DB node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_RMEP_DB_POOL, (UINT1 *) (pElem));
            break;
        case ECFM_LBLT_MA_ENTRY:

            /* Release memory allocated to MA node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MA_POOL, (UINT1 *) (pElem));
            break;
        case ECFM_LBLT_MD_ENTRY:

            /* Release memory allocated to MD node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MD_POOL, (UINT1 *) (pElem));
            break;
        case ECFM_LBLT_DEF_MD_ENTRY:
            pLbLtDefMdNode = (tEcfmLbLtDefaultMdTableInfo *) pElem;
            /* Release memory allocated to Error Log node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_DEF_MD_TABLE_POOL,
                                 (UINT1 *) (pLbLtDefMdNode));
            break;
        case ECFM_LBLT_PORT_MEP_ENTRY:
            pMepNode = (tEcfmLbLtMepInfo *) pElem;
            RBTreeRem (ECFM_LBLT_PORT_MEP_TABLE, (tRBElem *) pMepNode);
            break;

        case ECFM_LBLT_VLAN_ENTRY:

            /* Release the memory allocated to the VLAN node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_VLAN_MEM_POOL, (UINT1 *) pElem);
            break;

        default:
            break;
    }
    return 1;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtUtilGetMepEntryFrmGlob
 *
 *    DESCRIPTION      : This function returns the pointer to the Mep entry
 *                       from LbLt global structure,MepTableIndex
 *                       
 *
 *    INPUT            : u4MdIndex  -    MD Index,
 *                       u4MaIndex  -    MA Index,
 *                       u2MepId    -    MEP Identifier 
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to Mep Entry
 *
 ****************************************************************************/
PUBLIC tEcfmLbLtMepInfo *
EcfmLbLtUtilGetMepEntryFrmGlob (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);
    gpEcfmLbLtMepNode->u4MdIndex = u4MdIndex;
    gpEcfmLbLtMepNode->u4MaIndex = u4MaIndex;
    gpEcfmLbLtMepNode->u2MepId = u2MepId;

    /* Get MEP Entry */
    pMepNode = (tEcfmLbLtMepInfo *) RBTreeGet (ECFM_LBLT_MEP_TABLE,
                                               gpEcfmLbLtMepNode);
    return pMepNode;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtUtilNotifySm
 * 
 * DESCRIPTION      : Function notify LBLT state machines about MEP state
 *                    (Active/NotActive).
 *
 * INPUT            : pMepInfo- pointer to MepInfo
 *                    u1Event - BEGIN/MEP_NOT_ACTIVE    
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtUtilNotifySm (tEcfmLbLtMepInfo * pLbLtMepNode, UINT1 u1Indication)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_LBLT_PDUSM_INFO_SIZE);

    PduSmInfo.pMepInfo = pLbLtMepNode;

    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pLbLtMepNode);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilNotifySm:"
                       "LbLt Port entry is not created\r\n");
        return;
    }

    switch (u1Indication)

    {
        case ECFM_IND_MODULE_ENABLE:
        case ECFM_IND_MEP_ACTIVE:

            /* Send event to State machines if Port's Module status is ENABLE
             * and MEP is ACTIVE */
            if ((pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
                (pLbLtMepNode->b1Active == ECFM_TRUE))

            {
                EcfmLbLtClntLbInitiator (pLbLtMepNode, ECFM_EV_MEP_BEGIN);
                EcfmLbLtClntLtInitSm (&PduSmInfo, ECFM_EV_MEP_BEGIN);
            }
            break;
        case ECFM_IND_IF_DELETE:
        case ECFM_IND_MODULE_DISABLE:
        case ECFM_IND_MEP_INACTIVE:
            if (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE)

            {
                EcfmLbLtClntLbInitiator (pLbLtMepNode, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmLbLtClntLtInitSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************
 * Name               : EcfmLbLtUtilGetMp
 *
 * Description        : This is function is used to get entry from Stack table 
 *                      in global info for particular Md Level, VId and
 *                      direction, ifIndex.
 *
 * Input(s)           : u2PortNum - Local Port NUmber of MP
 *                      u1MdLevel - MdLevel of MP
 *                      u4VidIsid - VlanId of MP
 *                      u1Direction - direction of the MP
 *
 * Output(s)          : None
 *
 * Return Value(s)    : pstackNode - Pointer to stack node
 *****************************************************************************/
PUBLIC tEcfmLbLtStackInfo *
EcfmLbLtUtilGetMp (UINT2 u2PortNum, UINT1 u1MdLevel,
                   UINT4 u4VidIsid, UINT1 u1Direction)
{
    tEcfmLbLtStackInfo  StackInfoNode;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;

    ECFM_MEMSET (&StackInfoNode, ECFM_INIT_VAL, ECFM_LBLT_STACK_INFO_SIZE);
    StackInfoNode.u1MdLevel = u1MdLevel;
    StackInfoNode.u4VlanIdIsid = u4VidIsid;
    StackInfoNode.u1Direction = u1Direction;

    /* Get entry from Stack table on a port for particular
     * Md Level, Vid and Direction */

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        return NULL;
    }

    pStackInfo = (tEcfmLbLtStackInfo *) RBTreeGet
        (pPortInfo->StackInfoTree, (tRBElem *) & StackInfoNode);

    return pStackInfo;
}

/******************************************************************************
 * Function Name      : EcfmLbLtUtilChkPortFiltering
 *
 * Description        : This routine applies the port-filtering rules to the Vid 
 *                      and IfIndex Provided
 *
 * Input(s)           : u2LocalPort - Interface Index
 *                      u4Vid -  VLAN-ID
 *                      pu1Action - Ingress/Egress action
 *                      b1Ingress - Flag value for specifying 
 *                                  Ingress Filtering(if TRUE) else
 *                                  Egress Filtering if required 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtUtilChkPortFiltering (UINT2 u2LocalPort, UINT4 u4VidIsid)
{
    UINT4               u4IfIndex = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    if (ECFM_LBLT_GET_PORT_INFO (u2LocalPort) == NULL)
    {
        return ECFM_FAILURE;
    }

    u4IfIndex = ECFM_LBLT_PORT_INFO (u2LocalPort)->u4IfIndex;

    /*Check the port operational Status */
    if (ECFM_LBLT_PORT_INFO (u2LocalPort)->u1IfOperStatus != CFA_IF_UP)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilChkPortFiltering:"
                       "Operational State of the port is not UP\r\n");
        return ECFM_FAILURE;
    }

    /*Check the port memebership of Vlan */
    if (EcfmL2IwfMiIsVlanMemberPort
        (ECFM_LBLT_CURR_CONTEXT_ID (), u4VidIsid, u4IfIndex) != OSIX_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilChkPortFiltering:"
                       "EcfmL2IwfMiIsVlanMemberPort returned failure\r\n");
        return ECFM_FAILURE;
    }

    if (EcfmL2IwfGetVlanPortState (u4VidIsid, u4IfIndex) !=
        AST_PORT_STATE_FORWARDING)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilChkPortFiltering:"
                       "EcfmL2IwfGetVlanPortState returned port state !forwarding\r\n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtUtilGetMipEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns a pointer to the MIP entry
 *                       from the global structure, MipTable.
 *
 *    INPUT            : u2PortNum - Local Port Number 
 *                       u1MdLevel - Mdlevel
 *                       u4VidIsid - VlanId 
 *                       Indices of MIP Table for which an entry is
 *                       required.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Mip Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtMipInfo *
EcfmLbLtUtilGetMipEntry (UINT2 u2PortNum, UINT1 u1MdLevel, UINT4 u4VidIsid)
{
    tEcfmLbLtMipInfo    MipInfo;
    tEcfmLbLtMipInfo   *pMipNode = NULL;
    ECFM_MEMSET (&MipInfo, ECFM_INIT_VAL, ECFM_LBLT_MIP_INFO_SIZE);

    /* Get MIP entry, based on ifIndex, MdLevel,u4VidIsid */
    MipInfo.u2PortNum = u2PortNum;
    MipInfo.u1MdLevel = u1MdLevel;
    MipInfo.u4VlanIdIsid = u4VidIsid;
    pMipNode = RBTreeGet (ECFM_LBLT_MIP_TABLE, (tRBElem *) & MipInfo);
    return pMipNode;
}

/*******************************************************************************
 * Function Name      : EcfmLbLtUtilDmGetCurrentTime
 *
 * Description        : This routine is used to get the current time stamp
 *                        
 * Input(s)           : None
 *
 * Output(s)          : pTimeStamp - Pointer to the time tamp value.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtUtilDmGetCurrentTime (tEcfmTimeRepresentation * pTimeStamp)
{
    tUtlSysPreciseTime  SysPreciseTime;

    ECFM_LBLT_TRC_FN_ENTRY ();

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    /* Function used to return the system time in seconds and nanoseconds */
    UtlGetPreciseSysTime (&SysPreciseTime);
    /* Time returned in milliseconds */
    pTimeStamp->u4Seconds = SysPreciseTime.u4Sec;
    pTimeStamp->u4NanoSeconds = SysPreciseTime.u4NanoSec;
    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLbLtUtilDmGetCurrentTime:"
                        "seconds=%x, nanoseconds=%x\r\n",
                        pTimeStamp->u4Seconds, pTimeStamp->u4NanoSeconds);
    ECFM_LBLT_TRC_FN_EXIT ();
}

/*******************************************************************************
 * Function Name      : EcfmLbLtUtilPostTransaction
 *
 * Description        : This routine is used to send the transaction start/stop
 *                      event to the lblt task.
 *                        
 * Input(s)           : pMepInfo - Pointer to the mep info for which the
 *                                 transaction will be started/stopped
 *                      u1TransType - Transaction Type
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtUtilPostTransaction (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1TransType)
{
    tEcfmLbLtMsg       *pMsg = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pMsg) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilPostTransaction:"
                       "memory allocation failure\r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_LBLT_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) u1TransType;
    pMsg->u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    pMsg->u2PortNum = pMepInfo->u2PortNum;

    if (pMepInfo->pEcfmMplsParams == NULL)
    {
        pMsg->uMsg.Mep.u4VidIsid = pMepInfo->u4PrimaryVidIsid;
        pMsg->uMsg.Mep.u1MdLevel = pMepInfo->u1MdLevel;
        pMsg->uMsg.Mep.u1Direction = pMepInfo->u1Direction;
        pMsg->uMsg.Mep.u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;
    }
    else                        /* For MPLS-TP, get the MepInfo from ECFM_LBLT_MEP_TABLE */
    {
        pMsg->uMsg.Mep.u4MdIndex = pMepInfo->u4MdIndex;
        pMsg->uMsg.Mep.u4MaIndex = pMepInfo->u4MaIndex;
        pMsg->uMsg.Mep.u2MepId = pMepInfo->u2MepId;
        pMsg->uMsg.Mep.u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;
    }

    if (EcfmLbLtCfgQueMsg (pMsg) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilPostTransaction:"
                       "failure Occurred while posting cfg message to LBLT queue\r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLbLtUtilReflectBits
 *
 * Description        : This routine is used to reflect the input bits used in
 *                      crc 32 calculation.
 *                        
 * Input(s)           : u4Crc - CRC bit to be reflected
 *                      i4BitNum - number of bits in crc
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : CRC after reflecting
 ******************************************************************************/
PRIVATE UINT4
EcfmLbLtUtilReflectBits (UINT4 u4Crc, INT4 i4BitNum)
{
    UINT4               u4bitshift = 1;
    UINT4               u4bitmask = 1;
    UINT4               u4Crcout = ECFM_INIT_VAL;
    for (u4bitshift = (UINT4) 1 << (i4BitNum - 1); u4bitshift; u4bitshift >>= 1)

    {
        if (u4Crc & u4bitshift)
        {
            u4Crcout |= u4bitmask;
        }
        u4bitmask <<= 1;
    }
    return (u4Crcout);
}

/*******************************************************************************
 * Function Name      : EcfmLbLtUtilCalculateCrc32
 *
 * Description        : This routine is used to calculate the Crc32 of the input 
 *                      data
 *                        
 * Input(s)           : pData - data on which crc calculation to be performed 
 *                      u4Len - Length of the data 
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC UINT4
EcfmLbLtUtilCalculateCrc32 (UINT1 *pData, UINT4 u4Len)
{
    UINT4               u4Crc = ECFM_UINT4_MAX;
    u4Crc = EcfmLbLtUtilReflectBits (u4Crc, ECFM_NUM_OF_BITS_IN_CRC);
    while (u4Len--)
    {
        u4Crc =
            (u4Crc >> ECFM_SHIFT_8BITS) ^
            gu4CfmCrcTable[(u4Crc & ECFM_UINT1_MAX) ^ *pData++];
    }
    u4Crc ^= ECFM_UINT4_MAX;
    u4Crc &= ECFM_UINT4_MAX;
    return u4Crc;
}

/******************************************************************************
 * Function Name      : EcfmLbLtUtilRemoveLbEntry
 * 
 * Description        : This routine is used to delete the Linked List of LBRs
 *                      associated with the LBM node and then delete the LBM 
 *                      node.
 *
 * Input(s)           : pLbmInfo - Pointer to the structure containing
 *                      information regarding LBM Table.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtUtilRemoveLbEntry (tEcfmLbLtLbmInfo * pLbmInfo)
{
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrTempNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Before Deleting the LBM node, its related LBR nodes maintained 
     * in the SLL needs to be deleted */
    pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_First (&(pLbmInfo->LbrList));
    while (pLbrNode != NULL)

    {
        pLbrTempNode = pLbrNode;

        /* Get the next LBR node from the LBR list */
        pLbrNode =
            (tEcfmLbLtLbrInfo *) TMO_SLL_Next (&(pLbmInfo->LbrList),
                                               (tEcfmSllNode *) &
                                               (pLbrTempNode->LbrTableSllNode));

        /* Delete the LBR node from the SLL maintained by MEP per LBM */
        TMO_SLL_Delete (&(pLbmInfo->LbrList), &(pLbrTempNode->LbrTableSllNode));

        /* Releasing Memblock allocated to LBR node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LBR_TABLE_POOL,
                             (UINT1 *) (pLbrTempNode));
        pLbrTempNode = NULL;
    }
    /* Delete the LBM RBTree Node from the LbInitInfo */
    RBTreeRem (ECFM_LBLT_LBM_TABLE, (tRBElem *) pLbmInfo);
    TMO_DLL_Delete (&(gEcfmLbLtGlobalInfo.LbmDllList),
                    &(pLbmInfo->LbmDllNextNode));
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LBM_TABLE_POOL, (UINT1 *) pLbmInfo);
    return;
}

/******************************************************************************* 
 * Function Name      : EcfmLbLtClntFwdExPdu 
 *
 * Description        : This is called to parse the received External PDU and fill
 *                      required data structures
 *
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the
 *                      information regarding mp info, the PDU if received and
 *                      other information related to the state machine.
 *                      pu1Pdu- Pointer to ECFM pdu
 * Output(s)          : None
 *
 ******************************************************************************/
PUBLIC VOID
EcfmLbLtClntFwdExPdu (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmBufChainHeader *pu1Data = NULL;
    tEcfmMepInfoParams  MepInfo;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    UINT4               u4Indication = ECFM_INIT_VAL;
    UINT4               u4DataLen = ECFM_INIT_VAL;
    UINT1              *pu1Oui = NULL;
    UINT1               u1Flags = ECFM_INIT_VAL;
    UINT1               u1TlvOffset = ECFM_INIT_VAL;
    UINT1               u1SubOpCode = ECFM_INIT_VAL;

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    /* Get PDU specific information */
    pu1Pdu = pu1Pdu + ECFM_OFFSET_FOR_FLAG;

    /* Copy the Flag */
    ECFM_GET_1BYTE (u1Flags, pu1Pdu);

    /*Copy the TLV Offset */
    ECFM_GET_1BYTE (u1TlvOffset, pu1Pdu);
    if (pPduSmInfo->u1RxOpcode == ECFM_OPCODE_APS)

    {
        u4DataLen = ECFM_APS_DATA_LENGTH;
    }

    else

    {

        /* Get OUI */
        pu1Oui = pu1Pdu;

        /*Copy the SubOpcode */
        pu1Pdu = pu1Pdu + ECFM_OFFSET_FOR_OUI;
        ECFM_GET_1BYTE (u1SubOpCode, pu1Pdu);
        u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pBuf);
        u4ByteCount = u4ByteCount - pPduSmInfo->u1CfmPduOffset;
        u4DataLen = u4ByteCount - ECFM_OFFSET_EX_API_LENGTH;
    }

    /* Duplicate the received PDU and pass the same to the */
    pu1Data = ECFM_DUPLICATE_CRU_BUF (pPduSmInfo->pBuf);
    if (pu1Data == NULL)

    {
        return;
    }
    switch (pPduSmInfo->u1RxOpcode)

    {
        case ECFM_OPCODE_APS:
            u4Indication = ECFM_APS_FRAME_RECEIVED;
            break;
        case ECFM_OPCODE_RAPS:
            u4Indication = ECFM_RAPS_FRAME_RECEIVED;
            break;
        case ECFM_OPCODE_MCC:
            u4Indication = ECFM_MCC_FRAME_RECEIVED;
            break;
        case ECFM_OPCODE_VSM:
            u4Indication = ECFM_VSM_FRAME_RECEIVED;
            break;
        case ECFM_OPCODE_VSR:
            u4Indication = ECFM_VSR_FRAME_RECEIVED;
            break;
        case ECFM_OPCODE_EXM:
            u4Indication = ECFM_EXM_FRAME_RECEIVED;
            break;
        case ECFM_OPCODE_EXR:
            u4Indication = ECFM_EXR_FRAME_RECEIVED;
            break;
        default:
            break;
    }
    MepInfo.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    MepInfo.u4VlanIdIsid = pPduSmInfo->pMepInfo->pMaInfo->u4PrimaryVidIsid;
    MepInfo.u4IfIndex = ECFM_LBLT_GET_PHY_PORT
        (pPduSmInfo->pMepInfo->u2PortNum, pTempLbLtPortInfo);
    MepInfo.u4MdIndex = pPduSmInfo->pMepInfo->u4MdIndex;
    MepInfo.u4MaIndex = pPduSmInfo->pMepInfo->u4MaIndex;
    MepInfo.u2MepId = pPduSmInfo->pMepInfo->u2MepId;
    MepInfo.u1MdLevel = pPduSmInfo->pMepInfo->u1MdLevel;
    MepInfo.u1Direction = pPduSmInfo->pMepInfo->u1Direction;

    ECFM_NOTIFY_PROTOCOLS (u4Indication, &MepInfo,
                           &(pPduSmInfo->RxSrcMacAddr),
                           u4Indication, u1Flags,
                           u1TlvOffset, pu1Data, pPduSmInfo->u1CfmPduOffset,
                           pu1Oui, u1SubOpCode, ECFM_LBLT_TASK_ID);
    UNUSED_PARAM (u4DataLen);
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtUtilNotifyY1731
 * 
 * DESCRIPTION      : Function notify LBLT state machines about MEP state
 *                    (Active/NotActive).
 *
 * INPUT            : pMepInfo- pointer to MepInfo
 *                    u1Event - BEGIN/MEP_NOT_ACTIVE    
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtUtilNotifyY1731 (tEcfmLbLtMepInfo * pLbLtMepNode, UINT1 u1Indication)
{
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    tEcfmLbLtPduSmInfo  PduSmInfo;

    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pLbLtMepNode);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtUtilNotifyY1731:"
                       "LbLt Port entry is not created\r\n");
        return;
    }

    PduSmInfo.pMepInfo = pLbLtMepNode;

    switch (u1Indication)

    {
        case ECFM_IND_MODULE_ENABLE:
        case ECFM_IND_MEP_ACTIVE:

            /* Send event to State machines if Port's Module status is ENABLE
             * and MEP is ACTIVE */
            if ((pPortInfo->u1PortY1731Status == ECFM_ENABLE) &&
                (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE) &&
                (pLbLtMepNode->b1Active == ECFM_TRUE))

            {
                EcfmLbLtClntDmInitiator (pLbLtMepNode, ECFM_EV_MEP_BEGIN);
                EcfmLbLtClntTstInitiator (pLbLtMepNode, ECFM_EV_MEP_BEGIN);
                EcfmLbLtClntThInitiator (&(PduSmInfo), ECFM_EV_MEP_BEGIN);
            }
            break;
        case ECFM_IND_IF_DELETE:
        case ECFM_IND_MODULE_DISABLE:
        case ECFM_IND_MEP_INACTIVE:
            if ((pPortInfo->u1PortY1731Status == ECFM_ENABLE) &&
                (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE))

            {
                EcfmLbLtClntDmInitiator (pLbLtMepNode, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmLbLtClntTstInitiator (pLbLtMepNode, ECFM_EV_MEP_NOT_ACTIVE);
                EcfmLbLtClntThInitiator (&(PduSmInfo), ECFM_EV_MEP_NOT_ACTIVE);
            }
            break;
        default:
            break;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtUtilGetMipCcmDbEntry
 *
 *    DESCRIPTION      : This function gets the IfIndex for the received Source 
 *                       MAC Address and the Vlan Id from the MIP CCM Database.
 *
 *    INPUT            : u2VLanId   - VlanId of the received CCM PDU
 *                       SrcMacAddr - Source MAC Address in the CCM PDU
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to tEcfmLbLtMipDbInfo
 *
 ****************************************************************************/
PUBLIC tEcfmLbLtMipCcmDbInfo *
EcfmLbLtUtilGetMipCcmDbEntry (UINT2 u2VlanId, UINT1 *pu1SrcMacAddr)
{
    tEcfmLbLtMipCcmDbInfo MipCcmDbInfo;
    tEcfmLbLtMipCcmDbInfo *pMipCcmDbNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (&MipCcmDbInfo, ECFM_INIT_VAL, ECFM_LBLT_MIP_CCM_DB_INFO_SIZE);
    MipCcmDbInfo.u2Fid = u2VlanId;
    ECFM_MEMCPY (&(MipCcmDbInfo.SrcMacAddr), pu1SrcMacAddr,
                 ECFM_MAC_ADDR_LENGTH);

    /* Get MipCcmDb Entry */
    pMipCcmDbNode =
        (tEcfmLbLtMipCcmDbInfo *) RBTreeGet (ECFM_LBLT_MIP_CCM_DB_TABLE,
                                             &MipCcmDbInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return pMipCcmDbNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtUtilGetRMepDbEntry
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the RMep entry
 *                       from the global structure, RMepDb Table.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u2RMepId  - Remote Mep Identifier
 *    OUTPUT           : None.
 *    RETURNS          : pointer to RMep Entry
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtRMepDbInfo *
EcfmLbLtUtilGetRMepDbEntry (UINT4 u4MdIndex,
                            UINT4 u4MaIndex, UINT2 u2MepId, UINT2 u2RMepId)
{
    tEcfmLbLtRMepDbInfo RMepInfo;
    tEcfmLbLtRMepDbInfo *pRMepNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_LBLT_RMEP_DB_INFO_SIZE);

    /* Get Remote MEP entry corresponding to indices - MdIndex, 
     * MaIndex, MepId and RMepId*/
    RMepInfo.u4MdIndex = u4MdIndex;
    RMepInfo.u4MaIndex = u4MaIndex;
    RMepInfo.u2MepId = u2MepId;
    RMepInfo.u2RMepId = u2RMepId;
    pRMepNode = RBTreeGet (ECFM_LBLT_RMEP_DB_TABLE, (tRBElem *) & RMepInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return pRMepNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtGetRMepMacAddr
 *
 *    DESCRIPTION      : API Routine to get the MAC Address of the remote MEP
 *                       from the Remote MEP CCM Database
 *
 *    INPUT            : u4MdIndex - MD Index for the RMEP Info
 *                       u4MaIndex - MA Index for the RMEP Info 
 *                       u2MepId   - MEP ID corresponding to which the RMEP info 
 *                                   is required. 
 *                       u2TargetRMepId  - Mep ID of the remote MEP whose 
 *                                         MAC Address is required 
 *
 *    OUTPUT           : RmepMacAddr
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtUtilGetRMepMacAddr (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId,
                            UINT2 u2TargetRMepId, UINT1 *pu1MacAddr)
{
    tEcfmLbLtRMepDbInfo *pRMepInfo = NULL;
    tEcfmMacAddr        NullMacAddr = {
        ECFM_INIT_VAL
    };
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Get corresponding MepNode in CCTasks MepInfo */
    pRMepInfo =
        EcfmLbLtUtilGetRMepDbEntry (u4MdIndex, u4MaIndex, u2MepId,
                                    u2TargetRMepId);
    if (pRMepInfo == NULL)

    {
        return ECFM_FAILURE;
    }
    if (ECFM_MEMCMP
        (NullMacAddr, pRMepInfo->RMepMacAddr, ECFM_MAC_ADDR_LENGTH) == 0)

    {
        return ECFM_FAILURE;
    }
    ECFM_MEMCPY (pu1MacAddr, pRMepInfo->RMepMacAddr, ECFM_MAC_ADDR_LENGTH);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtUtilGetMaEntry
 *
 *    DESCRIPTION      : This function returns the pointer to the MA entry
 *                       corresponding to MdIndex, MaIndex from the global 
 *                       structure, MaTableIndex.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index, 
 *                       u4MaIndex - Maintenance Association Index
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to Ma Entry
 *
 ****************************************************************************/
PUBLIC tEcfmLbLtMaInfo *
EcfmLbLtUtilGetMaEntry (UINT4 u4MdIndex, UINT4 u4MaIndex)
{
    tEcfmLbLtMaInfo     MaInfo;
    tEcfmLbLtMaInfo    *pMaNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_LBLT_MA_INFO_SIZE);

    /* Get MA entry corresponding to indices - MdIndex and MaIndex */
    MaInfo.u4MdIndex = u4MdIndex;
    MaInfo.u4MaIndex = u4MaIndex;
    pMaNode = (tEcfmLbLtMaInfo *) RBTreeGet (ECFM_LBLT_MA_TABLE,
                                             (tRBElem *) & MaInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return pMaNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmLbLtUtilGetMdEntry
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Md entry
 *                       corresponding to MdIndex from the global structure, 
 *                       MdTableIndex.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index 
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Md Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtMdInfo *
EcfmLbLtUtilGetMdEntry (UINT4 u4MdIndex)
{
    tEcfmLbLtMdInfo     MdInfo;
    tEcfmLbLtMdInfo    *pMdNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (&MdInfo, ECFM_INIT_VAL, ECFM_LBLT_MD_INFO_SIZE);

    /* Get MD entry corresponding to MdIndex */
    MdInfo.u4MdIndex = u4MdIndex;
    pMdNode = RBTreeGet (ECFM_LBLT_MD_TABLE, (tRBElem *) & MdInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return pMdNode;
}

/*******************************************************************************
 * Function Name      : EcfmLbLtUtilGetMepSenderIdPerm
 *
 * Description        : This routine is used to check if sender-id tlv is to be
 *                      included for a MEP.
 *                        
 * Input(s)           : pMepInfo - Pointer to the mep info.
 * Output(s)          : None
 *
 * Return Value(s)    : Sender-Id Permission
 ******************************************************************************/
PUBLIC UINT1
EcfmLbLtUtilGetMepSenderIdPerm (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtMaInfo    *pMaInfo = NULL;
    tEcfmLbLtMdInfo    *pMdInfo = NULL;
    UINT1               u1SenderIdPermission = ECFM_SENDER_ID_NONE;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pMdInfo = EcfmLbLtUtilGetMdEntry (pMepInfo->u4MdIndex);
    pMaInfo = EcfmLbLtUtilGetMaEntry (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex);
    if ((pMdInfo == NULL) || (pMaInfo == NULL))

    {
        return u1SenderIdPermission;
    }
    if (pMaInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

    {
        if (pMdInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

        {

            /* there is no encompassing Maintenance Domain, the
             * value sendIdDefer takes the meaning of sendIdChassisManage
             */
            u1SenderIdPermission = ECFM_SENDER_ID_CHASSID_MANAGE;
        }

        else

        {
            u1SenderIdPermission = pMdInfo->u1IdPermission;
        }
    }

    else

    {
        u1SenderIdPermission = pMaInfo->u1IdPermission;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return u1SenderIdPermission;
}

/*******************************************************************************
 * Function Name      : EcfmLbLtUtilGetMhfSenderIdPerm
 *
 * Description        : This routine is used to check if sender-id tlv is to be
 *                      included for a MHF's of a MIP.
 *                        
 * Input(s)           : pStackInfo - Pointer to the stack info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Sender-Id Permission
 ******************************************************************************/
PUBLIC UINT1
EcfmLbLtUtilGetMhfSenderIdPerm (tEcfmLbLtStackInfo * pStackInfo)
{
    UINT1               u1SenderIdPermission = ECFM_SENDER_ID_NONE;
    tEcfmLbLtMipInfo   *pLbLtMipInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /*Check if this mip was created using DefaultMdTable. */
    if (pStackInfo == NULL || pStackInfo->pLbLtMipInfo == NULL)

    {
        return u1SenderIdPermission;
    }
    pLbLtMipInfo = pStackInfo->pLbLtMipInfo;
    if (pLbLtMipInfo->u4MdIndex == 0)
    {
        tEcfmLbLtDefaultMdTableInfo *pDefaultMdInfo = NULL;
        /*MIP was created using defaultMd Table */
        ECFM_LBLT_GET_DEFAULT_MD_ENTRY (pStackInfo->u4VlanIdIsid,
                                        pDefaultMdInfo);
        if (pDefaultMdInfo != NULL)
        {
            if (pDefaultMdInfo->b1Status == ECFM_FALSE)

            {
                return u1SenderIdPermission;
            }
            if (pDefaultMdInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

            {
                u1SenderIdPermission =
                    ECFM_LBLT_DEF_MD_DEFAULT_SENDER_ID_PERMISSION;
            }

            else

            {
                u1SenderIdPermission = pDefaultMdInfo->u1IdPermission;
            }
        }
    }
    else
    {
        tEcfmLbLtMaInfo    *pMaInfo = NULL;
        tEcfmLbLtMdInfo    *pMdInfo = NULL;

        pMdInfo = EcfmLbLtUtilGetMdEntry (pLbLtMipInfo->u4MdIndex);
        pMaInfo =
            EcfmLbLtUtilGetMaEntry (pLbLtMipInfo->u4MdIndex,
                                    pLbLtMipInfo->u4MaIndex);

        if ((pMdInfo == NULL) || (pMaInfo == NULL))

        {
            return u1SenderIdPermission;
        }
        if (pMaInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

        {
            if (pMdInfo->u1IdPermission == ECFM_SENDER_ID_DEFER)

            {

                /* there is no encompassing Maintenance Domain, the
                 * value sendIdDefer takes the meaning of sendIdChassisManage
                 */
                u1SenderIdPermission = ECFM_SENDER_ID_CHASSID_MANAGE;
            }
            else
            {
                u1SenderIdPermission = pMdInfo->u1IdPermission;
            }
        }
        else
        {
            u1SenderIdPermission = pMaInfo->u1IdPermission;
        }
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return u1SenderIdPermission;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtUtilGetDefaultMdNode
 * 
 * DESCRIPTION      : This funtion is used to add a default MD table entry
 *
 * INPUT            : u4VlanIdIsid - VLAN or ISID
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmLbLtUtilGetDefaultMdNode (UINT4 u4VlanIdIsid,
                              tEcfmLbLtDefaultMdTableInfo ** ppDefaultMdNode)
{
    tEcfmLbLtDefaultMdTableInfo *pDefaultMdNode = NULL;
    static tEcfmLbLtDefaultMdTableInfo DefMdDummyEntry;

    *ppDefaultMdNode = NULL;
    DefMdDummyEntry.u4PrimaryVidIsid = u4VlanIdIsid;
    pDefaultMdNode = (tEcfmLbLtDefaultMdTableInfo *) RBTreeGet
        (ECFM_LBLT_DEF_MD_TABLE, &DefMdDummyEntry);

    if (pDefaultMdNode == NULL)
    {
        ECFM_MEMSET (&DefMdDummyEntry, 0x00,
                     sizeof (tEcfmLbLtDefaultMdTableInfo));
        DefMdDummyEntry.u1IdPermission = ECFM_SENDER_ID_DEFER;
        DefMdDummyEntry.u4PrimaryVidIsid = u4VlanIdIsid;
        DefMdDummyEntry.b1Status = ECFM_TRUE;
        pDefaultMdNode = &DefMdDummyEntry;
    }
    *ppDefaultMdNode = pDefaultMdNode;
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmLbLtUtilY1731EnableMep
 *
 *    Description          : This is the function will enable the Y1731 on a
 *                           given Mep for LBLT.
 *
 *    Input(s)             : pMepInfo - Mep on which Y1731 should be enabled
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtUtilY1731EnableMep (tEcfmLbLtMepInfo * pMepInfo)
{
    EcfmLbLtUtilNotifySm (pMepInfo, ECFM_IND_MODULE_DISABLE);
    pMepInfo->LbInfo.u2TxLbmMessages = Y1731_LB_MESG_DEF_VAL;
    pMepInfo->LbInfo.u1TxLbmTlvOrNone = ECFM_LBLT_LBM_WITHOUT_TLV;
    pMepInfo->LbInfo.u4TxLbmInterval = Y1731_LB_INTERVAL_DEF_VAL;
    pMepInfo->LbInfo.u1TxLbmIntervalType = ECFM_LBLT_LB_INTERVAL_SEC;
    pMepInfo->LbInfo.u1RxMCastLbmCap = ECFM_DISABLE;
    pMepInfo->LbInfo.u4NextLbmSeqNo = ECFM_LBM_SEQ_NUM_INIT_VAL;
    pMepInfo->LbInfo.u1TxLbmMode = ECFM_LBLT_LB_REQ_RES_TYPE;
    if (pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_DATA_TLV_POOL,
                             pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets);
        pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets = NULL;
    }
    pMepInfo->LbInfo.TxLbmDataTlv.u4OctLen = ECFM_INIT_VAL;
    EcfmLbLtUtilNotifySm (pMepInfo, ECFM_IND_MODULE_ENABLE);
    EcfmLbLtUtilNotifyY1731 (pMepInfo, ECFM_IND_MODULE_ENABLE);
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmLbLtUtilY1731DisableMep
 *
 *    Description          : This is the function will disable the Y1731 on a
 *                           given Mep for LBLT.
 *
 *    Input(s)             : pMepInfo - Mep on which Y1731 should be disabled
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtUtilY1731DisableMep (tEcfmLbLtMepInfo * pMepInfo)
{
    EcfmLbLtUtilNotifySm (pMepInfo, ECFM_IND_MODULE_DISABLE);
    EcfmLbLtUtilNotifyY1731 (pMepInfo, ECFM_IND_MODULE_DISABLE);
    pMepInfo->LbInfo.u1LbCapability = ECFM_ENABLE;
    pMepInfo->LbInfo.u1TxLbmIntervalType = ECFM_LBLT_LB_INTERVAL_MSEC;
    pMepInfo->LbInfo.u4TxLbmInterval = ECFM_LB_BURST_INTERVAL_DEF_VAL;
    pMepInfo->LbInfo.u2TxLbmMessages = ECFM_LB_MESG_DEF_VAL;
    pMepInfo->LbInfo.u1TxLbmDestType = ECFM_TX_DEST_TYPE_UNICAST;
    pMepInfo->LbInfo.b1TxLbmVariableBytes = ECFM_FALSE;
    pMepInfo->LbInfo.u1TxLbmTlvOrNone = ECFM_LBLT_LBM_WITHOUT_TLV;
    pMepInfo->LbInfo.u1RxMCastLbmCap = ECFM_DISABLE;
    pMepInfo->LbInfo.u4NextLbmSeqNo = ECFM_LBM_SEQ_NUM_INIT_VAL;
    if (pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_DATA_TLV_POOL,
                             pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets);
        pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets = NULL;
    }
    pMepInfo->LbInfo.TxLbmDataTlv.u4OctLen = ECFM_INIT_VAL;
    pMepInfo->LbInfo.u2TxLbmPatternSize = ECFM_INIT_VAL;
    pMepInfo->LtInfo.u2LtrTimeOut = ECFM_LTR_TIMEOUT_DEF_VAL;
    EcfmLbLtUtilNotifySm (pMepInfo, ECFM_IND_MODULE_ENABLE);
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmLbLtUtilModuleEnableForAMep
 *
 *    Description          : This function is called to enable ECFM module
 *                           on a given Mep for LBLT
 *
 *    Input(s)             : pMepInfo - Mep on which ECFM module to be enabled
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtUtilModuleEnableForAMep (tEcfmLbLtMepInfo * pMepInfo)
{
    EcfmLbLtUtilNotifySm (pMepInfo, ECFM_IND_MODULE_ENABLE);
    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLbLtModuleEnableForAPort:"
                        "State Machine initialized for Mep %u on port %u"
                        "\r\n", pMepInfo->u2MepId, pMepInfo->u4IfIndex);
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmLbLtUtilModuleDisableForAMep
 *
 *    Description          : This function is called to disable ECFM module
 *                           on a given Mep for LBLT
 *
 *    Input(s)             : pMepInfo - Mep on which ECFM module to be disabled
 *
 *    Output(s)            : None
 *
 *    Returns              : None
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtUtilModuleDisableForAMep (tEcfmLbLtMepInfo * pMepInfo)
{
    /* Notify the the State Machines for the event */
    EcfmLbLtUtilNotifySm (pMepInfo, ECFM_IND_MODULE_DISABLE);
    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLbLtModuleDisableForAMep: SM Reverted back for"
                        "Mep %u on port %u\r\n", pMepInfo->u2MepId,
                        pMepInfo->u4IfIndex);
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmClearLbLtContextStats 
 *
 *    Description          : This function is called to clear the LBLT Specific 
 *                           Statistics on a given Context 
 *
 *    Input(s)             : u4CtxId - ContextId
 *                           u1Statstype - Type of Statistics object to set.
 *
 *    Output(s)            : None
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmClearLbLtContextStats (UINT4 u4CtxId, UINT1 u1Statstype)
{
    if (ECFM_LBLT_SELECT_CONTEXT (u4CtxId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (u4CtxId))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC, "\tECFM module is Shutdown\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }

    switch (u1Statstype)
    {
        case ECFM_CTX_TST_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxTstCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_TST_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxTstCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4Tx1DmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4Rx1DmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DMM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxDmmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DMM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxDmmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_APS_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxApsCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_APS_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxApsCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DMR_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxDmrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DMR_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxDmrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_MCC_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxMccCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_MCC_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxMccCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_VSM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxVsmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_VSM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxVsmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_VSR_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxVsrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_VSR_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxVsrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_EXM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxExmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_EXM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxExmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_EXR_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxExrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_EXR_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxExrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_CFM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_CFM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LBM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxLbmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LBM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxLbmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LBR_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxLbrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LBR_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxLbrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LTM_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxLtmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LTM_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxLtmCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LTR_OUT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxLtrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_LTR_IN:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxLtrCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_FAILED_COUNT:
            gpEcfmLbLtContextInfo->LbLtStats.u4TxFailedCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_BAD_PDU:
            gpEcfmLbLtContextInfo->LbLtStats.u4RxBadCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_FWD_PDU:
            gpEcfmLbLtContextInfo->LbLtStats.u4FrwdCfmPduCount = ECFM_INIT_VAL;
            break;
        case ECFM_CTX_DSRD_PDU:
            gpEcfmLbLtContextInfo->LbLtStats.u4DsrdCfmPduCount = ECFM_INIT_VAL;
            break;
        default:
            break;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : EcfmGetLbLtContextStats 
 *
 *    Description          : This function is called to get the LBLT specific 
 *                           Statistics on a given Context 
 *
 *    Input(s)             : u4CtxId - ContextId
 *                           u1Statstype - Type of Statistics object to get.
 *
 *    Output(s)            : u4Value  - Statistics Value 
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmGetLbLtContextStats (UINT4 u4CtxId, UINT4 *pu4Value, UINT1 u1Statstype)
{
    if (ECFM_LBLT_SELECT_CONTEXT (u4CtxId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (u4CtxId))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC, "\tECFM module is Shutdown\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return ECFM_SUCCESS;
    }

    switch (u1Statstype)
    {
        case ECFM_CTX_TST_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxTstCount;
            break;
        case ECFM_CTX_TST_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxTstCount;
            break;
        case ECFM_CTX_DM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4Tx1DmCount;
            break;
        case ECFM_CTX_DM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4Rx1DmCount;
            break;
        case ECFM_CTX_DMM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxDmmCount;
            break;
        case ECFM_CTX_DMM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxDmmCount;
            break;
        case ECFM_CTX_APS_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxApsCount;
            break;
        case ECFM_CTX_APS_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxApsCount;
            break;
        case ECFM_CTX_DMR_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxDmrCount;
            break;
        case ECFM_CTX_DMR_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxDmrCount;
            break;
        case ECFM_CTX_MCC_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxMccCount;
            break;
        case ECFM_CTX_MCC_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxMccCount;
            break;
        case ECFM_CTX_VSM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxVsmCount;
            break;
        case ECFM_CTX_VSM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxVsmCount;
            break;
        case ECFM_CTX_VSR_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxVsrCount;
            break;
        case ECFM_CTX_VSR_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxVsrCount;
            break;
        case ECFM_CTX_EXM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxExmCount;
            break;
        case ECFM_CTX_EXM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxExmCount;
            break;
        case ECFM_CTX_EXR_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxExrCount;
            break;
        case ECFM_CTX_EXR_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxExrCount;
            break;
        case ECFM_CTX_CFM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxCfmPduCount;
            break;
        case ECFM_CTX_CFM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxCfmPduCount;
            break;
        case ECFM_CTX_LBM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxLbmCount;
            break;
        case ECFM_CTX_LBM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxLbmCount;
            break;
        case ECFM_CTX_LBR_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxLbrCount;
            break;
        case ECFM_CTX_LBR_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxLbrCount;
            break;
        case ECFM_CTX_LTM_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxLtmCount;
            break;
        case ECFM_CTX_LTM_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxLtmCount;
            break;
        case ECFM_CTX_LTR_OUT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxLtrCount;
            break;
        case ECFM_CTX_LTR_IN:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxLtrCount;
            break;
        case ECFM_CTX_FAILED_COUNT:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4TxFailedCount;
            break;
        case ECFM_CTX_BAD_PDU:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4RxBadCfmPduCount;
            break;
        case ECFM_CTX_FWD_PDU:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4FrwdCfmPduCount;
            break;
        case ECFM_CTX_DSRD_PDU:
            *pu4Value = gpEcfmLbLtContextInfo->LbLtStats.u4DsrdCfmPduCount;
            break;
        default:
            break;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : EcfmTestLbLtContextStats 
 *
 *    Description          : This function is called to validate the LBLT 
 *                           specific Statistics value to be set.
 *
 *    Input(s)             : u4CtxId - ContextId
 *                           u4Value - Statistics Value to get
 *
 *    Output(s)            : pu4ErrorCode - Pointer to Error Code 
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmTestLbLtContextStats (UINT4 *pu4ErrorCode, UINT4 u4CtxId, UINT4 u4Value)
{
    if (ECFM_LBLT_SELECT_CONTEXT (u4CtxId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (u4Value != ECFM_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tInvalid Value\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : EcfmLbltCreateMemPoolsForLtrTlvInfo 
 *
 *    Description          : This function is called to create the  
 *                           mem-pools for LTR-TLV info.
 *
 *    Input(s)             : i4LtrCacheSize - Cache Size
 *                           
 *    Output(s)            : Nil
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbltCreateMemPoolsForLtrTlvInfo (INT4 i4LtrCacheSize)
{
    if (ECFM_CREATE_MEM_POOL (ECFM_MAX_CHASSISID_LEN, i4LtrCacheSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL)
        == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of MemPool for LTR node's "
                       "ChassisId FAILED!\n");
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        return ECFM_FAILURE;
    }

    if (ECFM_CREATE_MEM_POOL (ECFM_MAX_MGMT_DOMAIN_LEN, i4LtrCacheSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL)
        == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of MemPool for LTR node's "
                       "ManAddress Domain FAILED!\n");
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        return ECFM_FAILURE;
    }

    if (ECFM_CREATE_MEM_POOL (ECFM_MAX_MAN_ADDR_LEN, i4LtrCacheSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL)
        == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of MemPool for LTR node's "
                       "ManAddress FAILED!\n");
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        return ECFM_FAILURE;
    }

    if (ECFM_CREATE_MEM_POOL (ECFM_MAX_ORG_TLV_VALUE_LEN, i4LtrCacheSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL)
        == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of MemPool for LTR node's "
                       "Org TLV FAILED!\n");
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        return ECFM_FAILURE;
    }

    if (ECFM_CREATE_MEM_POOL (ECFM_MAX_EGRESS_PORT_ID_LEN, i4LtrCacheSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL)
        == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of MemPool for LTR node's "
                       "Egress port Id FAILED!\n");
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        return ECFM_FAILURE;
    }

    if (ECFM_CREATE_MEM_POOL (ECFM_MAX_INGRESS_PORT_ID_LEN, i4LtrCacheSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL)
        == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of MemPool for LTR node's "
                       "Ingress Port Id FAILED!\n");
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : EcfmLbltDeleteMemPoolsForLtrTlvInfo 
 *
 *    Description          : This function is called to delete the  
 *                           mem-pools of LTR-TLV info.
 *
 *    Input(s)             : Nil
 *                           
 *    Output(s)            : Nil
 *
 *    Returns              : VOID
 *****************************************************************************/
PUBLIC VOID
EcfmLbltDeleteMemPoolsForLtrTlvInfo ()
{
    if (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL != 0)
    {
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL);
        ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL = ECFM_INIT_VAL;
    }

    if (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL != 0)
    {
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL);
        ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL = ECFM_INIT_VAL;
    }

    if (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL != 0)
    {
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL);
        ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL = ECFM_INIT_VAL;
    }

    if (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL != 0)
    {
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL);
        ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL = ECFM_INIT_VAL;
    }

    if (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL != 0)
    {
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL);
        ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL = ECFM_INIT_VAL;
    }

    if (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL != 0)
    {
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL);
        ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL = ECFM_INIT_VAL;
    }

    return;
}
/*****************************************************************************
 *    Function Name        : EcfmLbLtUtilValidateRMepMacAddress
 *
 *    Description          : This function is to validate RMEP MAC Address
 *                           when user triggers on-demand(PM/LB/LT) using 
 *                           RMEP MAC Address
 *
 *    Input(s)             : tEcfmLbLtMepInfo - Local MEP Info
 *                           DestMacAddress - RMEP MAC Address
 *
 *    Output(s)            : Nil
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4 EcfmLbLtUtilValidateRMepMacAddress (tEcfmCcMepInfo *pMepNode,
                                                tMacAddr DestMacAddress)
{
    tEcfmCcRMepDbInfo  *pRMep = NULL;
    /* Assumption : Calling function has properly validated pMepNode */
    /* Fetch Remote MEP DB*/
    pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
    while (pRMep != NULL)
    {
        if ((pMepNode->u4MdIndex == pRMep->u4MdIndex) &&
            (pMepNode->u4MaIndex == pRMep->u4MaIndex) &&
            (pMepNode->u2MepId == pRMep->u2MepId))
        {
            if (ECFM_MEMCMP
                    (pRMep->RMepMacAddr, DestMacAddress,
                     ECFM_MAC_ADDR_LENGTH) == 0)

            {                                                                           
                /*validation  passed */
                return ECFM_SUCCESS;
            }
            else
            {
               /* RMEP Mac address mismatch */
                return ECFM_FAILURE;
            }
        }
        /* Get the next node form the tree */
        pRMep = (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                &(pRMep->
                    MepDbDllNode));
    }
    
    return ECFM_FAILURE;
}
/******************************************************************************/
/*                           End  of cfmlbutl.c                               */
/******************************************************************************/
