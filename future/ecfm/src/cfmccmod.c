/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccmod.c,v 1.60 2015/12/31 11:25:34 siva Exp $
 *
 * Description: This file contains the module start/shutdown
 *                        enable/disable routines
 *******************************************************************/

#include "cfminc.h"
#include "cfmccext.h"

/* Prototypes of the Private Routines */
PRIVATE INT4 EcfmCcMdCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcMaCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmMepListCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmMepCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmRMepDbCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmVlanCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmPrimaryVlanCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcDefaultMdCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcErrLogCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcFrmLossBuffCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 EcfmCcCreateGlobalRBTrees PROTO ((VOID));
PRIVATE VOID EcfmCcDeleteGlobalRBTrees PROTO ((VOID));
PRIVATE INT4 EcfmPortMepCmp PROTO ((tRBElem *, tRBElem *));
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcY1731Enable                                  
 *                                                                          
 *    DESCRIPTION      : This function is called to Y.1731 funtionality for CC
 *                       Task from disabled state.                           
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcY1731Enable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Get CC Tasks current CONTEXT Id */
    /* Scan the ECFM enabled ports state machine for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_CC_MAX_PORT_INFO; u2LocalPortNum++)

    {
        EcfmCcY1731EnableForAPort (u2LocalPortNum);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcY1731EnableForPort                                  
 *                                                                          
 *    DESCRIPTION      : This function is called to Y.1731 funtionality for CC
 *                       Task from disabled state.                           
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcY1731EnableForAPort (UINT2 u2PortNum)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfoNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipInfo      MipNode;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, 0x00, ECFM_CC_MEP_INFO_SIZE);
    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);
    pPortInfoNode = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfoNode != NULL)

    {

        /* The following sequence for state machines is desired 
         * 1. Disable the CC related State Machines ECFM module
         * 2. Set the variables
         * 3. Enable the CC related State Machines for Y1731 Module
         * 3. Enable Y1731 specific functionalities
         * 4. If the Port is a dummy MPLS port then enable y1731 on 
         *    all the MEP associated with that port.
         */
        EcfmCcModuleDisableForAPort (u2PortNum);

        /* Revert back the ECFM status to enable */
        pPortInfoNode->u1PortEcfmStatus = ECFM_ENABLE;

        /* Set Y.1731 Status to enabled */
        ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_ENABLE;
        if (u2PortNum < ECFM_CC_MAX_MEP_INFO)
        {
            gpEcfmCcMepNode->u2PortNum = u2PortNum;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                                         gpEcfmCcMepNode, NULL);
            while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
            {
                pMepInfo->b1Active = ECFM_TRUE;
                EcfmCcUtilY1731EnableMep (pMepInfo);
                pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                          (tRBElem *) pMepInfo, NULL);
            }
            MipNode.u2PortNum = u2PortNum;
            pMipNode =
                (tEcfmCcMipInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                                  &MipNode, NULL);
            while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
            {
                pMipNode->b1Active = ECFM_TRUE;
                pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                          (tRBElem *) pMipNode, NULL);
            }
        }
        else
        {
            /* Enable Y1731 on MEPs associated to MPLS-TP OAM Dummy port */
            pMaNode = RBTreeGetFirst (ECFM_CC_MA_TABLE);
            while (pMaNode != NULL)
            {
                if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
                    (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
                {
                    TMO_DLL_Scan (&(pMaNode->MepTable), pMepInfo,
                                  tEcfmCcMepInfo *)
                    {
                        pMepInfo->b1Active = ECFM_TRUE;
                        EcfmCcUtilY1731EnableMep (pMepInfo);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
            }
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcY1731Disable                                 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable Y.1731 funtionality 
 *                       for CC Task.                                            
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcY1731Disable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Get CC Tasks current CONTEXT Id */
    /* Scan the ECFM enabled ports state machine for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_CC_MAX_PORT_INFO; u2LocalPortNum++)

    {
        if ((ECFM_CC_GET_PORT_INFO (u2LocalPortNum) != NULL) &&
            (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (u2LocalPortNum)))

        {
            EcfmCcY1731DisableForAPort (u2LocalPortNum);
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcY1731DisableForPort                                 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable Y.1731 funtionality 
 *                       for CC Task.                                            
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcY1731DisableForAPort (UINT2 u2PortNum)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfoNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    pPortInfoNode = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfoNode != NULL)

    {
        /* The following sequence for state machines is desired 
         * 1. Disable the CC related State Machines Y1731 module
         * 2. Disable Y1731 specific functionalities
         * 3. Set the default values of the variables.
         * 4. Enable the CC related State Machines for ECFM Module
         */
        if (u2PortNum < ECFM_CC_MAX_MEP_INFO)
        {
            gpEcfmCcMepNode->u2PortNum = u2PortNum;
            pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                                         gpEcfmCcMepNode, NULL);
            while ((pMepInfo != NULL) && (pMepInfo->u2PortNum == u2PortNum))
            {
                EcfmCcUtilY1731DisableMep (pMepInfo);
                pMepInfo = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                          (tRBElem *) pMepInfo, NULL);
            }
            ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_DISABLE;
            EcfmCcModuleEnableForAPort (u2PortNum);
        }
        else
        {
            /* Disable Y1731 on MEPs associated to MPLS-TP OAM Dummy port */
            pMaNode = RBTreeGetFirst (ECFM_CC_MA_TABLE);
            while (pMaNode != NULL)
            {
                if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
                    (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
                {
                    TMO_DLL_Scan (&(pMaNode->MepTable), pMepInfo,
                                  tEcfmCcMepInfo *)
                    {
                        EcfmCcUtilY1731DisableMep (pMepInfo);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
            }
            ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_DISABLE;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcModuleEnable                                  
 *                                                                          
 *    DESCRIPTION      : This function is called to enable ECFM module for CC
 *                       Task from disabled state.                           
 *
 *    INPUT            : None 
 *                                                                          
 *    w
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcModuleEnable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Get CC Tasks current CONTEXT Id */
    /* Scan the ECFM enabled ports state machine for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_CC_MAX_PORT_INFO; u2LocalPortNum++)

    {
        EcfmCcModuleEnableForAPort (u2LocalPortNum);
    }
    ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                 "EcfmModuleEnable: ECFM Module ENABLED \r\n");
    ECFM_CC_INCR_CFM_UP_COUNT ();
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcModuleDisable                                 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable ECFM module for  
 *                       CC Task.                                            
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : VOID
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmCcModuleDisable (VOID)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    ECFM_CC_TRC_FN_ENTRY ();

    /* Get CurrentContext for CC Task */
    /* Scan the ECFM enabled ports for all ECFM enabled ports. */
    for (u2LocalPortNum = ECFM_PORTS_PER_CONTEXT_MIN;
         u2LocalPortNum <= ECFM_CC_MAX_PORT_INFO; u2LocalPortNum++)

    {
        if ((ECFM_CC_GET_PORT_INFO (u2LocalPortNum) != NULL) &&
            (ECFM_CC_IS_PORT_MODULE_STS_ENABLED (u2LocalPortNum)))

        {
            EcfmCcModuleDisableForAPort (u2LocalPortNum);
        }
    }
    ECFM_CC_INCR_CFM_DOWN_COUNT ();
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcModuleEnableForAPort
 *
 *    DESCRIPTION      : This function is called to enable ECFM module at 
 *                       particular port.
 *
 *    INPUT            : portNum
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcModuleEnableForAPort (UINT2 u2PortNum)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pPortInfoNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipInfo      MipNode;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);
    pPortInfoNode = ECFM_CC_GET_PORT_INFO (u2PortNum);

    if (pPortInfoNode != NULL)
    {
        /* Update Port ModuleStatus in CC Task */
        pPortInfoNode->u1PortEcfmStatus = ECFM_ENABLE;

        if (u2PortNum < ECFM_CC_MAX_MEP_INFO)
        {
            /* For all MEPS configured on this port, send Port MODULE ENABLE
             * indication.
             */
            gpEcfmCcMepNode->u2PortNum = u2PortNum;
            pMepNode = (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                                         gpEcfmCcMepNode, NULL);
            while ((pMepNode != NULL) && (pMepNode->u2PortNum == u2PortNum))
            {
                pMepNode->b1Active = ECFM_TRUE;
                EcfmCcUtilModuleEnableForAMep (pMepNode);
                pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                          (tRBElem *) pMepNode, NULL);
            }
            MipNode.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MipNode.u2PortNum = u2PortNum;
            pMipNode =
                (tEcfmCcMipInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                                  &MipNode, NULL);
            while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
            {
                pMipNode->b1Active = ECFM_TRUE;
                pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                          (tRBElem *) pMipNode, NULL);
            }
        }
        else
        {
            /* MODULE ENABLE indication to MPLS-TP OAM Dummy port MEPs */
            ECFM_Y1731_PORT_OPER_STATUS (pPortInfoNode) = ECFM_ENABLE;

            pMaNode = RBTreeGetFirst (ECFM_CC_MA_TABLE);
            while (pMaNode != NULL)
            {
                if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
                    (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
                {
                    TMO_DLL_Scan (&(pMaNode->MepTable), pMepNode,
                                  tEcfmCcMepInfo *)
                    {
                        pMepNode->b1Active = ECFM_TRUE;
                        EcfmCcUtilModuleEnableForAMep (pMepNode);
                        /* For MPLS-TP OAM MEPs Y1731 is enabled by default */
                        EcfmCcUtilY1731EnableMep (pMepNode);
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
            }
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcModuleDisableForAPort
 *
 *    DESCRIPTION      : This function is called to disable ECFM module at 
 *                       particular port.
 *
 *    INPUT            : pPortInfoNode - Pointer to portInfo. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmCcModuleDisableForAPort (UINT2 u2PortNum)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pPortInfoNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipInfo      MipNode;

    ECFM_CC_TRC_FN_ENTRY ();
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    ECFM_MEMSET (&MipNode, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);
    pPortInfoNode = ECFM_CC_GET_PORT_INFO (u2PortNum);

    if (pPortInfoNode != NULL)
    {
        if (u2PortNum < ECFM_CC_MAX_MEP_INFO)
        {
            /* For all Meps configured on this port, send Port 
             * ModuleDisable Indication
             * to SM of all the MEPs on that port */
            gpEcfmCcMepNode->u2PortNum = u2PortNum;
            pMepNode =
                (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                                  gpEcfmCcMepNode, NULL);
            while ((pMepNode != NULL) && (pMepNode->u2PortNum == u2PortNum))

            {
                EcfmCcUtilModuleDisableForAMep (pMepNode);
                pMepNode->b1Active = ECFM_FALSE;
                /* Get next MEP Node */
                pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                          (tRBElem *) pMepNode, NULL);
            }
            MipNode.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MipNode.u2PortNum = u2PortNum;
            pMipNode =
                (tEcfmCcMipInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                                  &MipNode, NULL);
            while ((pMipNode != NULL) && (pMipNode->u2PortNum == u2PortNum))
            {
                pMipNode->b1Active = ECFM_FALSE;
                pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                                          (tRBElem *) pMipNode, NULL);
            }
        }
        else
        {
            pMaNode = RBTreeGetFirst (ECFM_CC_MA_TABLE);
            while (pMaNode != NULL)
            {
                if ((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
                    (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
                {
                    TMO_DLL_Scan (&(pMaNode->MepTable), pMepNode,
                                  tEcfmCcMepInfo *)
                    {
                        EcfmCcUtilModuleDisableForAMep (pMepNode);
                        pMepNode->b1Active = ECFM_FALSE;
                    }
                }
                pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
            }
        }

        /* Update Port ModuleStatus in CC Task  */
        pPortInfoNode->u1PortEcfmStatus = ECFM_DISABLE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : EcfmHandleCcAndLbLtStartModule
 *
 *    DESCRIPTION      : This function allocates memory pools for all tables
 *                       in CC Task . It also initalizes the global structure.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmHandleCcAndLbLtStartModule ()
{
    INT4                i4SysLogId = ECFM_INIT_VAL;
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmHandleCcAndLbLtStartModule: Starting CC and LBLT \r\n");

#ifdef L2RED_WANTED
    ECFM_MEMSET (&gEcfmRedGlobalInfo, ECFM_INIT_VAL,
                 sizeof (tEcfmRedGlobalInfo));
    gEcfmRedGlobalInfo.u2BulkUpdNextPort = ECFM_PORTS_PER_CONTEXT_MIN;
    ECFM_BULK_REQ_RECD () = ECFM_FALSE;
    ECFM_NODE_STATUS () = ECFM_NODE_IDLE;

#else /*  */
    ECFM_RM_GET_NODE_STATUS ();

#endif /*  */
    ECFM_RM_GET_NUM_PEERS_UP ();

    /* Register with RM module, to get event and messages from RM. */
    if (EcfmRedRegisterWithRM () != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: Registration"
                      "with RM \r\n");
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (&gFsEcfmSizingInfo, 0, sizeof (tFsModSizingInfo));
    ECFM_MEMCPY (gFsEcfmSizingInfo.ModName, "ECFM", STRLEN ("ECFM"));
    gFsEcfmSizingInfo.u4ModMemPreAllocated = 0;
    gFsEcfmSizingInfo.ModSizingParams = gFsEcfmSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsEcfmSizingInfo);
    if (EcfmSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return ECFM_FAILURE;
    }

    /*Assign respective mempools Id's */
    EcfmCcAssignMempoolIds ();
    EcfmLbltAssignMempoolIds ();

    /* Start the ECFM Module */
    if (EcfmCcInitGlobalInfo () != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: CC global info init "
                      "failed \r\n");
        return ECFM_FAILURE;
    }

    /* Initialize Timer List and Timer Mem Pool for CC Task */
    if (EcfmCcTmrInit () != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: CC timer failed \r\n");
        return ECFM_FAILURE;
    }
#ifdef SYSLOG_WANTED
    /* Register with SysLog */
    i4SysLogId = SYS_LOG_REGISTER (ECFM_NAME, SYSLOG_CRITICAL_LEVEL);
    if (i4SysLogId <= 0)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: Sys log failed \r\n");
        return ECFM_FAILURE;
    }
#endif /* SYSLOG_WANTED */

    /* Save the syslog id in the MIB */
    ECFM_SYSLOG_ID = (UINT4) (i4SysLogId);
    if (EcfmPortRegisterWithIp () != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: Register"
                      "with IP failure \r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_INITIALISED () = ECFM_TRUE;
    ECFM_TAKE_SEMAPHORE (ECFM_LBLT_SEM_ID);

    /* LBLT related informations */
    if (EcfmLbLtInitGlobalInfo () != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: Lblt global init "
                      "failed\r\n");
        ECFM_GIVE_SEMAPHORE (ECFM_LBLT_SEM_ID);
        return ECFM_FAILURE;
    }

    /* Initialize Timer List for LBLT task */
    if (EcfmLbLtTmrInit () != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmHandleCcAndLbLtStartModule: Lblt Timer init "
                      "failed\r\n");
        ECFM_GIVE_SEMAPHORE (ECFM_LBLT_SEM_ID);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_INITIALISED () = ECFM_TRUE;
    ECFM_GIVE_SEMAPHORE (ECFM_LBLT_SEM_ID);

    /*Note: CC context is already selected */
    if (EcfmUtilModuleStart () != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_CONTROL_PLANE_TRC,
                     "EcfmHandleCcAndLbLtStartModule: Module start failed \r\n");
        return ECFM_FAILURE;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmHandleCcAndLbLtStartModule: ...CC and LBLT started "
                  "successfully \r\n");
    return ECFM_SUCCESS;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : EcfmCcModuleStart
 *
 *    DESCRIPTION      : This function allocates memory pools for all tables
 *                       in CC Task . It also initalizes the global structure.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcModuleStart ()
{
    ECFM_CC_TRC_FN_ENTRY ();

    /* Creating Ports, Updating PortInfo Structure and Creating 
     * MEPTree per Port */

    if (EcfmL2IwfGetBridgeMode
        (ECFM_CC_CURR_CONTEXT_ID (),
         &(ECFM_CC_BRIDGE_MODE ())) != L2IWF_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmCcModuleStart:"
                     "EcfmL2IwfGetBridgeMode returned failure \r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_CC_BRIDGE_MODE () == ECFM_INVALID_BRIDGE_MODE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcModuleStart: Invalid Bridge Mode \r\n");
        return ECFM_FAILURE;
    }

    EcfmCcIfCreateAllPorts ();
    if (EcfmCcCreateGlobalRBTrees () != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcInitGlobalInfo: Protocol RBTree Creation "
                     "FAILED  !!!! \r\n");
        return ECFM_FAILURE;
    }

    /* MIP Dynamic evaluation status */
    ECFM_CC_MIP_DYNAMIC_EVALUATION_STATUS = ECFM_FALSE;

    /* ECFM Trap control */
    ECFM_CC_CURR_CONTEXT_INFO ()->u1TrapControl = ECFM_INIT_VAL;

    /* Y.1731 trap control */
    ECFM_CC_CURR_CONTEXT_INFO ()->u2Y1731TrapControl = ECFM_INIT_VAL;

    /* Set Cross Check Delay to Default Value */
    ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay = ECFM_INIT_VAL;

    /* Initialising Default Md Object scalars */
    ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION = ECFM_MHF_CRITERIA_NONE;
    ECFM_CC_DEF_MD_DEFAULT_SENDER_ID_PERMISSION = ECFM_SENDER_ID_NONE;
    ECFM_CC_DEF_MD_DEFAULT_LEVEL = ECFM_INIT_VAL;

    /* MIP CCM Database */
    ECFM_CC_MIP_CCM_DB_STATUS = ECFM_DISABLE;
    ECFM_CC_MIP_CCM_DB_HOLD_TIME = ECFM_MIP_CCM_DB_DEF_HOLD_TIME;
    ECFM_CC_MIP_CCM_DB_SIZE = ECFM_MIP_CCM_DB_DEF_SIZE;

    /* ECFM Error log */
    ECFM_CC_ERR_LOG_SEQ_NUM = ECFM_INIT_VAL;
    ECFM_CC_ERR_LOG_STATUS = ECFM_ENABLE;
    ECFM_CC_ERR_LOG_SIZE = ECFM_ERROR_LOG_DEF_SIZE;
    if (ECFM_CREATE_MEM_POOL
        (ECFM_CC_ERR_LOG_INFO_SIZE, ECFM_CC_ERR_LOG_SIZE,
         MEM_DEFAULT_MEMORY_TYPE, &ECFM_CC_ERR_LOG_POOL) == ECFM_MEM_FAILURE)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC, "EcfmCcModuleStart:"
                     "Mem pool creation for error-log failed \r\n");
        return ECFM_FAILURE;
    }

    /* ECFM frame loss buffer */
    ECFM_CC_FL_BUFFER_SIZE = ECFM_FL_BUFFER_DEF_SIZE;

    /* Create the memory pool for the Frame Loss Buffer */
    if (ECFM_CREATE_MEM_POOL
        (ECFM_CC_FRM_LOSS_INFO_SIZE, ECFM_CC_FL_BUFFER_SIZE,
         MEM_DEFAULT_MEMORY_TYPE, &ECFM_CC_FRM_LOSS_POOL) == ECFM_MEM_FAILURE)

    {
        return ECFM_FAILURE;
    }

    /* Set Global OFFLOAD status to DISABLE */
    ECFM_GLOBAL_CCM_OFFLOAD_STATUS = ECFM_FALSE;

#ifdef NPAPI_WANTED
#ifndef MBSM_WANTED
#ifndef RM_WANTED
    if ((EcfmFsMiEcfmHwInit (ECFM_CC_CURR_CONTEXT_ID ())) != FNP_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcModuleStart: HW init for ecfm failed \r\n");
        return ECFM_FAILURE;
    }
#endif /* RM_WANTED  */
#endif /* MBSM_WANTED */
#if defined (MBSM_WANTED) || defined (RM_WANTED)

    if(EcfmInitWithMBSMandRM ( ECFM_CC_CURR_CONTEXT_ID () != ECFM_SUCCESS))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcModuleStart: HW init for ecfm failed \r\n");
        return ECFM_FAILURE;
    }
#endif
#endif /* NPAPI_WANTED  */
    ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                 "EcfmCcModuleStart:"
                 "ECFM Module Successfully Started !!! \r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *                                                                            
 *    FUNCTION NAME    : EcfmCcModuleShutDown                                 
 *                                                                            
 *    DESCRIPTION      : This is function is called during system shutdown or 
 *                       when the manager wants to shut the ECFM module.      
 *                                                                            
 *    INPUT            : None.                                                
 *                                                                            
 *    OUTPUT           : None.                                                
 *                                                                            
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                            
 *                                                                            
 *****************************************************************************/
PUBLIC VOID
EcfmCcModuleShutDown ()
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepListInfo *pEcfmCcMepListInfo = NULL;
    INT1                i1MaNetRetVal;
    INT1                i1MaCompRetVal;


    UINT4               u4ErrorCode;
    ECFM_CC_TRC_FN_ENTRY ();

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return;
    }
    /* During module shutdown, the MEP's scanned from RBTree and deleted
     * in the Control plane. But inidication to delete the filter from
     * the control plane  was missing.  That indication now added. */

    pMepNode = RBTreeGetFirst(ECFM_CC_MEP_TABLE);
    while (pMepNode != NULL)
    {

       if(nmhTestv2Dot1agCfmMepRowStatus(&u4ErrorCode,pMepNode->u4MdIndex,
                   pMepNode->u4MaIndex, pMepNode->u2MepId,
                   ECFM_ROW_STATUS_DESTROY)== SNMP_FAILURE)
       {
           ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                   ECFM_MGMT_TRC,
                   "EcfmCcModuleDisable: ECFM_ROW_STATUS_DESTROY"
                   "cannot be destroyed\r\n");
       }
       nmhSetDot1agCfmMepRowStatus(pMepNode->u4MdIndex,pMepNode->u4MaIndex,
                   pMepNode->u2MepId,ECFM_ROW_STATUS_DESTROY);
       pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
    }

    pMipNode = RBTreeGetFirst (ECFM_CC_GLOBAL_MIP_TABLE);
    while (pMipNode != NULL)
    {
           if (nmhTestv2FsEcfmMipRowStatus (&u4ErrorCode,
                                   (INT4) pMipNode->u2PortNum,
                                   (INT4) pMipNode->u1MdLevel,
                                   (INT4) pMipNode->u4VlanIdIsid,
                                   ECFM_ROW_STATUS_DESTROY) == SNMP_FAILURE)
           {
                   ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                    ECFM_MGMT_TRC,
                    "EcfmCcModuleDisable: ECFM_ROW_STATUS_DESTROY Mip"
                    "cannot be destroyed\r\n");
           }
           nmhSetFsEcfmMipRowStatus ((INT4) pMipNode->u2PortNum,
                           (INT4) pMipNode->u1MdLevel,
                           (INT4) pMipNode->u4VlanIdIsid,
                           ECFM_ROW_STATUS_DESTROY);
           pMipNode = RBTreeGetNext (ECFM_CC_GLOBAL_MIP_TABLE,
                           pMipNode, NULL);
    }
    /* During module shutdown, the Crosscheck list created has to be
     * destroyed in the control plane for service deletion.
     * This code is now added. */
    pEcfmCcMepListInfo = RBTreeGetFirst (ECFM_CC_MA_MEP_LIST_TABLE);
    while (pEcfmCcMepListInfo != NULL)
    {
        if (nmhTestv2Dot1agCfmMaMepListRowStatus (&u4ErrorCode,
                    pEcfmCcMepListInfo->u4MdIndex,
                    pEcfmCcMepListInfo->u4MaIndex, pEcfmCcMepListInfo->u2MepId,
                    ECFM_ROW_STATUS_DESTROY) == SNMP_FAILURE)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                    ECFM_MGMT_TRC,
                    "EcfmCcModuleDisable: ECFM_ROW_STATUS_DESTROY MepList"
                    "cannot be destroyed\r\n");

        }
        nmhSetDot1agCfmMaMepListRowStatus (pEcfmCcMepListInfo->u4MdIndex,
                pEcfmCcMepListInfo->u4MaIndex,
                pEcfmCcMepListInfo->u2MepId, ECFM_ROW_STATUS_DESTROY);
        pEcfmCcMepListInfo = RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE,
                                    pEcfmCcMepListInfo, NULL);
    }


    /* Delete all the dynamic caches and top the corresponding timers */
    /* Error Log */
    if (ECFM_CC_ERR_LOG_POOL != 0)

    {
        RBTreeDrain (ECFM_CC_ERR_LOG_TABLE,
                     (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                     ECFM_CC_ERR_LOG_ENTRY);
        ECFM_DELETE_MEM_POOL (ECFM_CC_ERR_LOG_POOL);
        ECFM_CC_ERR_LOG_POOL = 0;
    }

    /* MIP CCM database */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcModuleShutDown:Stopping MIP CCM DB Timer"
                     "Failed \r\n");
    }
    if (ECFM_CC_MIP_DB_TABLE_POOL != 0)

    {
        RBTreeDrain (ECFM_CC_MIP_CCM_DB_TABLE,
                     (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                     ECFM_CC_MIP_CCM_DB_ENTRY);
        ECFM_DELETE_MEM_POOL (ECFM_CC_MIP_DB_TABLE_POOL);
        ECFM_CC_MIP_DB_TABLE_POOL = 0;
    }

    /* Frame loss buffer */
    if (ECFM_CC_FRM_LOSS_POOL != 0)

    {
        RBTreeDrain (ECFM_CC_FL_BUFFER_TABLE,
                     (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                     ECFM_CC_FL_BUFFER_ENTRY);
        ECFM_DELETE_MEM_POOL (ECFM_CC_FRM_LOSS_POOL);
        ECFM_CC_FRM_LOSS_POOL = 0;
    }
    /* Deletion of service was not done earlier. This will not affect
     * control plane since the RB Trees are destroyed.
     * But In HW, the service will remain as stale entries.
     * They are removed by deletion of services. */
    pMaNode = RBTreeGetFirst(ECFM_CC_MA_TABLE);
    while (pMaNode != NULL)
    {
           if (pMaNode->pMdInfo != NULL)
           {
                   i1MaNetRetVal =
                           nmhTestv2Dot1agCfmMaNetRowStatus (&u4ErrorCode,
                                           pMaNode->u4MdIndex,
                                           pMaNode->u4MaIndex,
                                           ECFM_ROW_STATUS_DESTROY);
                   if (SNMP_SUCCESS == i1MaNetRetVal)
                   {
                           i1MaNetRetVal =
                                   nmhSetDot1agCfmMaNetRowStatus (pMaNode->u4MdIndex,
                                                   pMaNode->u4MaIndex,
                                                   ECFM_ROW_STATUS_DESTROY);
                   }
                   i1MaCompRetVal =
                           nmhTestv2Ieee8021CfmMaCompRowStatus (&u4ErrorCode,
                                           pMaNode->pMdInfo->u4ContextId,
                                           pMaNode->u4MdIndex,
                                           pMaNode->u4MaIndex,
                                           ECFM_ROW_STATUS_DESTROY);
                   if (SNMP_SUCCESS == i1MaCompRetVal)
                   {
                           i1MaCompRetVal =
                                   nmhSetIeee8021CfmMaCompRowStatus (
                                                   pMaNode->pMdInfo->u4ContextId,
                                                   pMaNode->u4MdIndex,
                                                   pMaNode->u4MaIndex,
                                                   ECFM_ROW_STATUS_DESTROY);
                   }

           }
           pMaNode = RBTreeGetNext (ECFM_CC_MA_TABLE, pMaNode, NULL);
    }

#ifdef NPAPI_WANTED
    /* Hardware should be disabled only when the Node is Active */
    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)

    {
        if ((EcfmFsMiEcfmHwDeInit (ECFM_CC_CURR_CONTEXT_ID ())) != FNP_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcModuleShutDown: HW deinit Failed \r\n");
        }
    }

#endif /* NPAPI_WANTED */
    ECFM_GLOBAL_CCM_OFFLOAD_STATUS = ECFM_FALSE;
    EcfmCcDeleteAllPortsInfo ();

    /* Delete all Global RBTrees */
    EcfmCcDeleteGlobalRBTrees ();
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * 
 * FUNCTION NAME    : EcfmCcDeleteGlobalRBTrees
 *
 * DESCRIPTION      : Delete all RBTrees present in the GlobalInfo
 *                    structure
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ****************************************************************************/
PRIVATE VOID
EcfmCcDeleteGlobalRBTrees ()
{

    /* Freeing Memory Assigned to DefaultMd Table */
    if (ECFM_CC_DEF_MD_TABLE != NULL)
    {
        RBTreeDestroy (ECFM_CC_DEF_MD_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_DEF_MD_ENTRY);
        ECFM_CC_DEF_MD_TABLE = NULL;
    }
    /* Delete RBTree for Cc Error Log Table */
    if (ECFM_CC_ERR_LOG_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_ERR_LOG_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_ERR_LOG_ENTRY);
        ECFM_CC_ERR_LOG_TABLE = NULL;
    }

    /* Delete RBTree for Frame Loss Buffer */
    if (ECFM_CC_FL_BUFFER_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_FL_BUFFER_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_FL_BUFFER_ENTRY);
        ECFM_CC_FL_BUFFER_TABLE = NULL;
    }

    /* Delete RBTree for Mip CCM DB Table */
    if (ECFM_CC_MIP_CCM_DB_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_MIP_CCM_DB_TABLE, (tRBKeyFreeFn)
                       EcfmCcUtilFreeEntryFn, ECFM_CC_MIP_CCM_DB_ENTRY);
        ECFM_CC_MIP_CCM_DB_TABLE = NULL;
    }

    /* Delete RBTree for RMEP Db Table */
    if (ECFM_CC_RMEP_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_RMEP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_RMEP_DB_ENTRY);
        ECFM_CC_RMEP_TABLE = NULL;
    }

    /* Delete RBTree for MEP  */
    if (ECFM_CC_MEP_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_MEP_TABLE, (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_MEP_ENTRY);
        ECFM_CC_MEP_TABLE = NULL;
    }
    if (ECFM_CC_PORT_MEP_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_PORT_MEP_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_PORT_MEP_ENTRY);
        ECFM_CC_PORT_MEP_TABLE = NULL;
    }

    /* Delete RBTree for MEP List */
    if (ECFM_CC_MA_MEP_LIST_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_MA_MEP_LIST_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_MEPLIST_ENTRY);
        ECFM_CC_MA_MEP_LIST_TABLE = NULL;
    }

    /*Delete RBTree for VLAN */
    if (ECFM_CC_PRIMARY_VLAN_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_PRIMARY_VLAN_TABLE, NULL, 0);
        ECFM_CC_PRIMARY_VLAN_TABLE = NULL;
    }
    if (ECFM_CC_VLAN_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_VLAN_TABLE,
                       (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_VLAN_ENTRY);
        ECFM_CC_VLAN_TABLE = NULL;
    }

    /* Delete RBTree for Maintenance Association */
    if (ECFM_CC_MA_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_MA_TABLE, (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_MA_ENTRY);
        ECFM_CC_MA_TABLE = NULL;
    }

    /* Delete all  Mds */
    if (ECFM_CC_MD_TABLE != NULL)

    {
        RBTreeDestroy (ECFM_CC_MD_TABLE, (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                       ECFM_CC_MD_ENTRY);
        ECFM_CC_MD_TABLE = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCcCreateGlobalRBTrees
 *                                                                          
 *    DESCRIPTION      : This function creates all the Protocol RBTrees for the 
 *                       CC Task.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmCcCreateGlobalRBTrees ()
{
    ECFM_CC_TRC_FN_ENTRY ();

    /* Creating RBTree for MdTable Indexed by MdIndex */
    ECFM_CC_MD_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcMdInfo, MdTableGlobalNode),
                                  EcfmCcMdCmp);
    if (ECFM_CC_MD_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of MD RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for MaTableIndex Indexed by MdIndex, MaIndex */
    ECFM_CC_MA_TABLE =
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF
                                  (tEcfmCcMaInfo, MaTableGlobalNode),
                                  EcfmCcMaCmp);
    if (ECFM_CC_MA_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of MA RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for MaMepListInfo Indexed by MdIndex, MaIndex, MepId */
    ECFM_CC_MA_MEP_LIST_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcMepListInfo, MepListGlobalNode), EcfmMepListCmp);
    if (ECFM_CC_MA_MEP_LIST_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of MepList RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for Mep Table indexed by MdIndex, MaIndex, MepId */
    ECFM_CC_MEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcMepInfo, MepGlobalIndexNode), EcfmMepCmp);
    if (ECFM_CC_MEP_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of MEP RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for Context specific Mep Table 
     * indexed by VlanId, Mdlevel, Direction and PortNum */
    ECFM_CC_PORT_MEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcMepInfo, MepPortTableNode), EcfmPortMepCmp);
    if (ECFM_CC_PORT_MEP_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of Context MEP RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for RMep Db Table indexed by MdIndex, MaIndex, MepId,
     * RMepId */
    ECFM_CC_RMEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcRMepDbInfo, MepDbGlobalNode), EcfmRMepDbCmp);
    if (ECFM_CC_RMEP_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of Remote MEP DB RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for VLAN Table indexed by VID */
    ECFM_CC_VLAN_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcVlanInfo, VlanGlobalNode), EcfmVlanCmp);
    if (ECFM_CC_VLAN_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of VLAN RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    /* Creating RBTree for VLAN Table indexed by VID */
    ECFM_CC_PRIMARY_VLAN_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcVlanInfo, PrimaryVlanGlobalNode),
         EcfmPrimaryVlanCmp);
    if (ECFM_CC_PRIMARY_VLAN_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of Primary VLAN RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating and initialising Default Md Table indexed by Primary VID */
    ECFM_CC_DEF_MD_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcDefaultMdTableInfo, DefaultMdGlobalNode),
         EcfmCcDefaultMdCmp);
    if (ECFM_CC_DEF_MD_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of default MD RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Creating RBTree for MIP Table indexed by FID and Source MAC Address */
    ECFM_CC_MIP_CCM_DB_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcMipCcmDbInfo, MipCcmDbGlobalNode),
         EcfmMipCcmDbCmp);
    if (ECFM_CC_MIP_CCM_DB_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of MIP CCM DB RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /*Y.1731: Added for Cc Error Logging funtionality */
    /* RBTree for Cc Error Log Table */
    ECFM_CC_ERR_LOG_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcErrLogInfo, ErrLogGlobalNode), EcfmCcErrLogCmp);
    if (ECFM_CC_ERR_LOG_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of Cc Error Log RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Create RB Tree for Frame Loss Buffer */
    ECFM_CC_FL_BUFFER_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcFrmLossBuff, FrmLossGlobalNode),
         EcfmCcFrmLossBuffCmp);
    if (ECFM_CC_FL_BUFFER_TABLE == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of Frame Loss RBTree for CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating Memory to Hold the MEP's created for MPLSTP-OAM 
     * and this MEP's are identified based on the four tupples in 
     * case of LSP or PW ID in case of PW.
     */
    ECFM_CC_MPLSMEP_TABLE = EcfmRBTreeCreateEmbedded
        (FSAP_OFFSETOF (tEcfmCcMepInfo, MepTableMplsNode),
         EcfmMpTpCcMepMplsPathCmp);
    if (ECFM_CC_MPLSMEP_TABLE == NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcCreateGlobalRBTrees:"
                     "Creation of Mpls Path Tree for CC context FAILED \r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcErrLogCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Error Log Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MD Node1
 *                       *pE2 - pointer to MD Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcErrLogCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcErrLogInfo  *pEcfmEntryA = NULL;
    tEcfmCcErrLogInfo  *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcErrLogInfo *) pE1;
    pEcfmEntryB = (tEcfmCcErrLogInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4SeqNum != pEcfmEntryB->u4SeqNum)

    {
        if (pEcfmEntryA->u4SeqNum < pEcfmEntryB->u4SeqNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcMdCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MD RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MD Node1
 *                       *pE2 - pointer to MD Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcMdCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMdInfo      *pEcfmEntryA = NULL;
    tEcfmCcMdInfo      *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMdInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMdInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcMaCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MA RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MA Node1
 *                       *pE2 - pointer to MA Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PRIVATE INT4
EcfmCcMaCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMaInfo      *pEcfmEntryA = NULL;
    tEcfmCcMaInfo      *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMaInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMaInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmMepListCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MEP LIST RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MepList Node1
 *                       *pE2 - pointer to MepList Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmMepListCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMepListInfo *pEcfmEntryA = NULL;
    tEcfmCcMepListInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMepListInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMepListInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmMepCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MEP RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MEP Node1
 *                       *pE2 - pointer to MEP Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmMepCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMepInfo     *pEcfmEntryA = NULL;
    tEcfmCcMepInfo     *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMepInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMepInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmRMepDbCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Remote MEP DB RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to Remote MEP DB Node1
 *                       *pE2 - pointer to Remote MEP DB Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmRMepDbCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcRMepDbInfo  *pEcfmEntryA = NULL;
    tEcfmCcRMepDbInfo  *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcRMepDbInfo *) pE1;
    pEcfmEntryB = (tEcfmCcRMepDbInfo *) pE2;
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u2RMepId != pEcfmEntryB->u2RMepId)

    {
        if (pEcfmEntryA->u2RMepId < pEcfmEntryB->u2RMepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmPrimaryVlanCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two VLAN RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to VLAN Node1
 *                       *pE2 - pointer to VLAN Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmPrimaryVlanCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcVlanInfo    *pEcfmEntryA = NULL;
    tEcfmCcVlanInfo    *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcVlanInfo *) pE1;
    pEcfmEntryB = (tEcfmCcVlanInfo *) pE2;
    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)
    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4VidIsid != pEcfmEntryB->u4VidIsid)

    {
        if (pEcfmEntryA->u4VidIsid < pEcfmEntryB->u4VidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmVlanCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two VLAN RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to VLAN Node1
 *                       *pE2 - pointer to VLAN Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmVlanCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcVlanInfo    *pEcfmEntryA = NULL;
    tEcfmCcVlanInfo    *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcVlanInfo *) pE1;
    pEcfmEntryB = (tEcfmCcVlanInfo *) pE2;
    if (pEcfmEntryA->u4VidIsid != pEcfmEntryB->u4VidIsid)

    {
        if (pEcfmEntryA->u4VidIsid < pEcfmEntryB->u4VidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcDefaultMdCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two default MD RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to default MD Node1
 *                       *pE2 - pointer to default MD Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PUBLIC INT4
EcfmCcDefaultMdCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcDefaultMdTableInfo *pEcfmEntryA = NULL;
    tEcfmCcDefaultMdTableInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcDefaultMdTableInfo *) pE1;
    pEcfmEntryB = (tEcfmCcDefaultMdTableInfo *) pE2;
    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)

    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmMipCcmDbCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two MIP CCM DB RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MIP CCM DB Node1
 *                       *pE2 - pointer to MIP CCM DB Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 ****************************************************************************/
PUBLIC INT4
EcfmMipCcmDbCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMipCcmDbInfo *pEcfmEntryA = NULL;
    tEcfmCcMipCcmDbInfo *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMipCcmDbInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMipCcmDbInfo *) pE2;
    if (pEcfmEntryA->u2Fid != pEcfmEntryB->u2Fid)

    {
        if (pEcfmEntryA->u2Fid < pEcfmEntryB->u2Fid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (ECFM_MEMCMP
        (pEcfmEntryA->SrcMacAddr, pEcfmEntryB->SrcMacAddr,
         ECFM_MAC_ADDR_LENGTH) != 0)

    {
        if (ECFM_MEMCMP
            (pEcfmEntryA->SrcMacAddr, pEcfmEntryB->SrcMacAddr,
             ECFM_MAC_ADDR_LENGTH) < 0)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCcFrameLossBuffCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Frame Loss Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MD Node1
 *                       *pE2 - pointer to MD Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmCcFrmLossBuffCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcFrmLossBuff *pEcfmEntryA = NULL;
    tEcfmCcFrmLossBuff *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcFrmLossBuff *) pE1;
    pEcfmEntryB = (tEcfmCcFrmLossBuff *) pE2;

    /* First Index - MD Index of the MEP */
    if (pEcfmEntryA->u4MdIndex != pEcfmEntryB->u4MdIndex)

    {
        if (pEcfmEntryA->u4MdIndex < pEcfmEntryB->u4MdIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Second Index - MA Index */
    if (pEcfmEntryA->u4MaIndex != pEcfmEntryB->u4MaIndex)

    {
        if (pEcfmEntryA->u4MaIndex < pEcfmEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Third Index - MEP Identifier */
    if (pEcfmEntryA->u2MepId != pEcfmEntryB->u2MepId)

    {
        if (pEcfmEntryA->u2MepId < pEcfmEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Fourth Index - Transaction Identifier */
    if (pEcfmEntryA->u4TransId != pEcfmEntryB->u4TransId)

    {
        if (pEcfmEntryA->u4TransId < pEcfmEntryB->u4TransId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }

    /* Fifth Index - Sequence Identifier */
    if (pEcfmEntryA->u4SeqNum != pEcfmEntryB->u4SeqNum)

    {
        if (pEcfmEntryA->u4SeqNum < pEcfmEntryB->u4SeqNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmPortMepCmp
 *
 *    DESCRIPTION      : This s the compare function used for RBTree.
 *                       It compares the two Context Port related MEP RBTree Nodes. 
 *
 *    INPUT            : *pE1 - pointer to MEP Node1
 *                       *pE2 - pointer to MEP Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If not Equal, returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PRIVATE INT4
EcfmPortMepCmp (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMepInfo     *pEcfmEntryA = NULL;
    tEcfmCcMepInfo     *pEcfmEntryB = NULL;
    pEcfmEntryA = (tEcfmCcMepInfo *) pE1;
    pEcfmEntryB = (tEcfmCcMepInfo *) pE2;
    if (pEcfmEntryA->u2PortNum != pEcfmEntryB->u2PortNum)

    {
        if (pEcfmEntryA->u2PortNum < pEcfmEntryB->u2PortNum)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u4PrimaryVidIsid != pEcfmEntryB->u4PrimaryVidIsid)

    {
        if (pEcfmEntryA->u4PrimaryVidIsid < pEcfmEntryB->u4PrimaryVidIsid)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1MdLevel != pEcfmEntryB->u1MdLevel)

    {
        if (pEcfmEntryA->u1MdLevel < pEcfmEntryB->u1MdLevel)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    if (pEcfmEntryA->u1Direction != pEcfmEntryB->u1Direction)

    {
        if (pEcfmEntryA->u1Direction < pEcfmEntryB->u1Direction)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmCcHandleCreateContext                        */
/*                                                                           */
/*    Description         : This function creates context, and initalises the*/
/*                          per context information                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS / ECFM_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmCcHandleCreateContext (UINT4 u4ContextId)
{
    tEcfmCcContextInfo *pContextInfo = NULL;
    if (ECFM_CC_GET_CONTEXT_INFO (u4ContextId) != NULL)

    {
        return ECFM_SUCCESS;
    }

    /* Allocate memory to the pContextInfo */
    if (ECFM_ALLOC_MEM_BLOCK_CC_CONTEXT (pContextInfo) == NULL)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcHandleCreateContext :MEM Block Allocation Failed \r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pContextInfo, 0x00, ECFM_CC_CONTEXT_INFO_SIZE);

    /* Select the newly allocated context info as current context */
    ECFM_CC_CURR_CONTEXT_INFO () = pContextInfo;
    ECFM_CC_CURR_CONTEXT_ID () = u4ContextId;
    ECFM_CC_GET_CONTEXT_INFO (u4ContextId) = ECFM_CC_CURR_CONTEXT_INFO ();

    /* Register Rx Call back for Offloading */
    EcfmCcmOffHwRegisterCallBack (u4ContextId);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmCcHandleDeleteContext                        */
/*                                                                           */
/*    Description         : This function deletes given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            :  ECFM_SUCCESS / ECFM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmCcHandleDeleteContext (UINT4 u4ContextId)
{
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcHandleDeleteContext :Invalid ContextId \r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (ECFM_CC_CURR_CONTEXT_INFO (), 0, sizeof (tEcfmCcContextInfo));
    if (ECFM_CC_CONTEXT_POOL != 0)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_CONTEXT_POOL,
                             (UINT1 *) ECFM_CC_CURR_CONTEXT_INFO ());
    }
    ECFM_CC_CURR_CONTEXT_INFO () = NULL;
    ECFM_CC_GET_CONTEXT_INFO (u4ContextId) = ECFM_CC_CURR_CONTEXT_INFO ();
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmCcSelectContext                              */
/*                                                                           */
/*    Description         : This function switches to given context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS / ECFM_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmCcSelectContext (UINT4 u4ContextId)
{
    if (u4ContextId >= ECFM_MAX_CONTEXTS)

    {
        return ECFM_FAILURE;
    }

    ECFM_CC_CURR_CONTEXT_INFO () = ECFM_CC_GET_CONTEXT_INFO (u4ContextId);
    if (ECFM_CC_CURR_CONTEXT_INFO () == NULL)

    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmCcReleaseContext                             */
/*                                                                           */
/*    Description         : This function makes the switched context to NULL */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                       .                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmCcReleaseContext (VOID)
{
    ECFM_CC_CURR_CONTEXT_INFO () = NULL;
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmCcGetFirstActiveContext                      */
/*                                                                           */
/*    Description         : This function is used to get the first Active    */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu4ContextId - Context Id.                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS/ECFM_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmCcGetFirstActiveContext (UINT4 *pu4ContextId)
{
    UINT4               u4ContextId;
    for (u4ContextId = 0; u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_GET_CONTEXT_INFO (u4ContextId) != NULL)

        {
            *pu4ContextId = u4ContextId;
            return ECFM_SUCCESS;
        }
    }
    return ECFM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmCcGetNextActiveContext                         
 */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : ECFM_SUCCESS/ECFM_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmCcGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId;
    for (u4ContextId = u4CurrContextId + 1; u4ContextId < ECFM_MAX_CONTEXTS;
         u4ContextId++)

    {
        if (ECFM_CC_GET_CONTEXT_INFO (u4ContextId) != NULL)

        {
            *pu4NextContextId = u4ContextId;
            return ECFM_SUCCESS;
        }
    }
    return ECFM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EcfmCcIsContextExist                               */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        context exist or not.                              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : ECFM_TRUE/ECFM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
EcfmCcIsContextExist (UINT4 u4ContextId)
{
    if (u4ContextId >= ECFM_MAX_CONTEXTS)

    {
        return ECFM_FALSE;
    }
    if (ECFM_CC_GET_CONTEXT_INFO (u4ContextId) != NULL)

    {
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/******************************************************************************
 * Function           : EcfmInitWithMBSMandRM
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Ecfm_SUCCESS/Ecfm_FAILURE
 * Action             : This function will Program the EcfmHwInit and Default Table
 *                      Entries to the Hardware only if MBSM SelfAttach is over and
 *                      the RM mode is ACTIVE.
 *******************************************************************************/
#ifdef NPAPI_WANTED
PUBLIC         INT4
EcfmInitWithMBSMandRM ( UINT4 u4CurrContextId )
{
#ifdef MBSM_WANTED
    if (MbsmGetSelfAttachStatus () == MBSM_SUCCESS)
#endif
    {
#ifdef RM_WANTED
        if (ECFM_NODE_STATUS() == ECFM_NODE_ACTIVE)     
#endif
        {
            if ((EcfmFsMiEcfmHwInit (u4CurrContextId) != FNP_SUCCESS))
            {
                 ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                              "EcfmCcModuleStart: HW init for ecfm failed \r\n");
                 return ECFM_FAILURE;
            }
            /*Registration with HW for Offload functionality has to be done 
	     * when GO_ACTIVE event  is received from RM*/
            EcfmFsMiEcfmHwRegister (u4CurrContextId);
        }
    }
    return ECFM_SUCCESS;
}
#endif
/****************************************************************************
  End of File cfmccmod.c
 ****************************************************************************/
