/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmsla.c,v 1.12 2016/03/07 12:11:35 siva Exp $
 *
 * Description: This file contains:
 *              1. Processing of Y1564 and RFC2544 requests and
 *                 initiates Frame Delay, Availability tests
 *******************************************************************/

#include "cfminc.h"

#if defined (Y1564_WANTED) || (RFC2544_WANTED)

PRIVATE VOID
EcfmY1564TestInit PROTO ((tEcfmLbLtMsg *));
PRIVATE VOID
EcfmY1564TestStop PROTO ((tEcfmLbLtMsg *));
PRIVATE VOID
EcfmY1564TestResults PROTO ((tEcfmLbLtMsg *));
PRIVATE VOID
EcfmR2544TestInit PROTO ((tEcfmLbLtMsg *));
PRIVATE VOID
EcfmR2544TestStop PROTO ((tEcfmLbLtMsg *));
PRIVATE VOID
EcfmR2544TestResults PROTO ((tEcfmLbLtMsg *));

/*****************************************************************************
 * Function Name      : EcfmLbLtHandleY1564andRFC2544Events                  *
 *                                                                           *
 * Description        : This function is invoked to process the following    *
 *                      events.                                              *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg - Pointer to LBLT strcuture             *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Global Variables                                                          *
 * Referred           : None.                                                *
 *                                                                           *
 * Global Variables                                                          *
 * Modified           : None.                                                *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtHandleY1564andRFC2544Events (tEcfmLbLtMsg * pEcfmLbLtMsg)
{
    tEcfmLbLtMepInfo        *pMepNode = NULL;
    tEcfmMacAddr            RetMacAddr;
    tEcfmMacAddr            LocMacAddr;
    UINT4                   u4NextContextId;
    UINT4                   u4CurrentContextId;
    UINT4                   u4MdIndex;
    UINT4                   u4MaIndex;
    UINT4                   u4MepIdentifier;
    UINT4                   u4NextMepIdentifier;
    UINT4                   u4NextRMepIdentifier;
    UINT4                   u4RMepIdentifier;
    UINT4                   u4NextMdIndex;
    UINT4                   u4NextMaIndex;
    INT4                    i4RMepState;
    BOOL1                   b1RetMacFlag = ECFM_FALSE;

    ECFM_MEMSET (LocMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    ECFM_MEMSET (RetMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if (ECFM_LBLT_SELECT_CONTEXT (pEcfmLbLtMsg->ReqParams.u4ContextId) != ECFM_SUCCESS)
    {
        return;
    }

    ECFM_LBLT_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
            "EcfmLbLtHandleY1564andRFC2544Events:"
            "Received Transmit LbLt Message from SNMP \r\n");

    /* Check system status for the Context*/
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmLbLtMsg->ReqParams.u4ContextId))

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                "EcfmLbLtHandleY1564andRFC2544Events:"
                "ECFM Module is Shutdown\r\n");
        return;
    }
    pMepNode =  EcfmLbLtUtilGetMepEntryFrmGlob (pEcfmLbLtMsg->ReqParams.EcfmMepInfo.
                                                u4MEGId,
                                                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.
                                                u4MEId,
                                                (UINT2) pEcfmLbLtMsg->ReqParams.EcfmMepInfo.
                                                u4MEPId);

    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtHandleY1564andRFC2544Events:"
                       "No MEP Entry\r\n");
        return;
    }

    if (pMepNode->pCcMepInfo->EcfmSlaParams.u4SlaId
        != pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4SlaId)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtHandleY1564andRFC2544Events:"
                       "SLA Id is not associated with MEP Entry\r\n");
        return;
    }
    u4CurrentContextId = pEcfmLbLtMsg->ReqParams.u4ContextId;

    ECFM_CC_LOCK ();

    /*Check for remote MEP entry for a particular context */
    if (nmhGetNextIndexFsMIEcfmMepDbTable (u4CurrentContextId,
                                           &u4NextContextId, 0,
                                           &u4NextMdIndex, 0,
                                           &u4NextMaIndex, 0,
                                           &u4NextMepIdentifier, 0,
                                           &u4NextRMepIdentifier) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return;
    }
    do
    {
        u4MdIndex = u4NextMdIndex;
        u4MaIndex =  u4NextMaIndex;
        u4MepIdentifier = u4NextMepIdentifier;
        u4RMepIdentifier = u4NextRMepIdentifier;

        if ((pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId == u4MdIndex) &&
            (pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId == u4MaIndex) &&
            (pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId == u4MepIdentifier))
        {
            nmhGetFsMIEcfmMepDbRMepState (u4CurrentContextId, u4MdIndex,
                                          u4MaIndex, u4MepIdentifier,
                                          u4RMepIdentifier, &i4RMepState);

            if (i4RMepState == ECFM_RMEP_OK)
            {
                ECFM_MEMSET (RetMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
                nmhGetFsMIEcfmMepDbMacAddress (u4CurrentContextId,
                                               u4MdIndex, u4MaIndex, u4MepIdentifier,
                                               u4RMepIdentifier,&RetMacAddr);
                b1RetMacFlag = ECFM_TRUE;
            }
            else if ((i4RMepState == ECFM_RMEP_FAILED) &&
                     ((pEcfmLbLtMsg->MsgType == ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST) ||
                      (pEcfmLbLtMsg->MsgType == ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST)))
            {
                ECFM_MEMSET (RetMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
                nmhGetFsMIEcfmMepDbMacAddress (u4CurrentContextId,
                                               u4MdIndex, u4MaIndex, u4MepIdentifier,
                                               u4RMepIdentifier,&RetMacAddr);
                b1RetMacFlag = ECFM_TRUE;
            }

            break;
        }
    }while(nmhGetNextIndexFsMIEcfmMepDbTable (u4CurrentContextId,
                                              &u4NextContextId, u4MdIndex, &u4NextMdIndex,
                                              u4MaIndex, &u4NextMaIndex, u4MepIdentifier,
                                              &u4NextMepIdentifier, u4RMepIdentifier,
                                              &u4NextRMepIdentifier) != SNMP_FAILURE);

    ECFM_CC_UNLOCK ();

    if (ECFM_LBLT_SELECT_CONTEXT (pEcfmLbLtMsg->ReqParams.u4ContextId) != ECFM_SUCCESS)
    {
        return;
    }
    switch (pEcfmLbLtMsg->MsgType)

    {
        case ECFM_R2544_REQ_START_THROUGHPUT_TEST:
        case ECFM_R2544_REQ_START_FRAMELOSS_TEST:
        case ECFM_R2544_REQ_START_BACKTOBACK_TEST:
        case ECFM_R2544_REQ_START_LATENCY_TEST:
		   if (b1RetMacFlag == ECFM_TRUE)
            {
                EcfmR2544TestInit (pEcfmLbLtMsg);
	    }
            break;

        case ECFM_R2544_REQ_STOP_BENCHMARK_TEST:
		   if (b1RetMacFlag == ECFM_TRUE)
            {
                EcfmR2544TestStop (pEcfmLbLtMsg);
			}
            break;

        case ECFM_R2544_REQ_SLA_TEST_RESULT:
            EcfmR2544TestResults (pEcfmLbLtMsg);
            break;

        case ECFM_Y1564_REQ_SLA_CONF_TEST_START:
        case ECFM_Y1564_REQ_SLA_PERF_TEST_START:            
            if (b1RetMacFlag == ECFM_TRUE)
            {
                EcfmY1564TestInit (pEcfmLbLtMsg);
            }
            break;
        case ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST:
        case ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST:
           if (b1RetMacFlag == ECFM_TRUE)
            {
                EcfmY1564TestStop (pEcfmLbLtMsg);
            }
            break;
        case ECFM_Y1564_REQ_CFM_GET_TEST_REPORT:

            pEcfmLbLtMsg->ReqParams.u2EvcId = (UINT2) pMepNode->u4PrimaryVidIsid;

            EcfmY1564TestResults (pEcfmLbLtMsg);
            break;

        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Unknown event from Y1564 and RFC2544 =[%d]\n",
                          pEcfmLbLtMsg->MsgType);
            break;
    }
    return;
}


/*****************************************************************************
 * Function Name      : EcfmY1564TestInit                                    *
 *                                                                           *
 * Description        : The routine implements the Y1564 Test Initiator,     *
 *                      it calls up routine to transmit LM/DM frame for      *
 *                      the various events.                                  *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg - Pointer to the LbLT structure         *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns            : None                                                 *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PRIVATE VOID
EcfmY1564TestInit (tEcfmLbLtMsg * pEcfmLbLtMsg)
{
    UINT4                u4ErrorCode = ECFM_INIT_VAL;
    INT4                 i4AvailabilityStatus = ECFM_INIT_VAL;
    INT4                 i4RowStatus = ECFM_INIT_VAL;

    switch (pEcfmLbLtMsg->MsgType)

    {
        case ECFM_Y1564_REQ_SLA_CONF_TEST_START:
        case ECFM_Y1564_REQ_SLA_PERF_TEST_START:

#ifdef NPAPI_WANTED
            /* Call Np wrapper to Start the TX */
            if (EcfmSlaStartSlaTest (&pEcfmLbLtMsg->ReqParams,
                                      pEcfmLbLtMsg->MsgType) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmY1564TestInit:"
                               "Failed to configure NP\r\n");
                return;
            }
#endif
            if (nmhTestv2FsMIY1731MepTransmitDmDestMacAddress (&u4ErrorCode,
                                                               pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                               pEcfmLbLtMsg->ReqParams.TxDstMacAddr)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmDestMacAddress (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                            pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                            pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                            pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                            pEcfmLbLtMsg->ReqParams.TxDstMacAddr)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhTestv2FsMIY1731MepTransmitDmType (&u4ErrorCode,
                                                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                     (INT4) ECFM_LBLT_DM_TYPE_DMM)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmType (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                  pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                  pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                  pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                  (INT4) ECFM_LBLT_DM_TYPE_DMM)
                == SNMP_FAILURE)
            {
                return;
            }


            if (nmhTestv2FsMIY1731MepTransmitDmDeadline (&u4ErrorCode,
                                                         pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                         pEcfmLbLtMsg->ReqParams.TestParam.u4TestDuration)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmDeadline (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                      pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                      pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                      pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                      pEcfmLbLtMsg->ReqParams.TestParam.u4TestDuration)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhTestv2FsMIY1731MepTransmitDmStatus (&u4ErrorCode,
                                                       pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                       (INT4) ECFM_TX_STATUS_START)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmStatus (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                     (INT4) ECFM_TX_STATUS_START)
                == SNMP_FAILURE)
            {
                return;
            }
           
            if (pEcfmLbLtMsg->MsgType == ECFM_Y1564_REQ_SLA_PERF_TEST_START)
            {
                ECFM_CC_LOCK ();
                nmhGetFsMIY1731MepAvailabilityStatus 
                             (pEcfmLbLtMsg->ReqParams.u4ContextId,
                              pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                              pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                              pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                              &i4AvailabilityStatus);

                if (i4AvailabilityStatus == ECFM_TX_STATUS_NOT_READY) 
                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                if (nmhGetFsMIY1731MepAvailabilityRowStatus
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     &i4RowStatus) != SNMP_SUCCESS)
                {
                    /*Test and Create Availability Measurement Table */

                    if (nmhTestv2FsMIY1731MepAvailabilityRowStatus
                        (&u4ErrorCode, pEcfmLbLtMsg->ReqParams.u4ContextId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                         ECFM_ROW_STATUS_CREATE_AND_WAIT) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsMIY1731MepAvailabilityRowStatus
                            (pEcfmLbLtMsg->ReqParams.u4ContextId,
                             pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                             pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                             pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                             ECFM_ROW_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
                        {
                            ECFM_CC_UNLOCK ();
                            return;
                        }
                    }

                    /* If Availability is on-going */
                    else
                    {
                        ECFM_CC_UNLOCK ();
                        return;
                    }
                }
                /* If created already, change status to Not in Service to configure */
                else if (nmhTestv2FsMIY1731MepAvailabilityRowStatus
                         (&u4ErrorCode, pEcfmLbLtMsg->ReqParams.u4ContextId,
                          pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                          pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                          pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                          ECFM_ROW_STATUS_NOT_IN_SERVICE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsMIY1731MepAvailabilityRowStatus
                        (pEcfmLbLtMsg->ReqParams.u4ContextId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                         ECFM_ROW_STATUS_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        ECFM_CC_UNLOCK ();
                        return;
                    }

                }
                /* If Availability is on-going */
                else
                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                if (nmhTestv2FsMIY1731MepAvailabilityDestIsMepId
                    (&u4ErrorCode, 
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_SNMP_FALSE) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK ();
                    return;
                }
                if (nmhSetFsMIY1731MepAvailabilityDestIsMepId
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_SNMP_FALSE) == SNMP_FAILURE)
                {

                    ECFM_CC_UNLOCK (); 
                    return;
                }

                /* Set destination address */
                if (nmhTestv2FsMIY1731MepAvailabilityDestMacAddress
                    (&u4ErrorCode,
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     pEcfmLbLtMsg->ReqParams.TxDstMacAddr) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK (); 
                    return;
                }
                if (nmhSetFsMIY1731MepAvailabilityDestMacAddress
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     pEcfmLbLtMsg->ReqParams.TxDstMacAddr) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK ();
                    return;
                }
                
                /* Test Interval for LM PDU's*/
                if (nmhTestv2FsMIY1731MepAvailabilityInterval
                    (&u4ErrorCode,
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_CC_AVLBLTY_INTERVAL_10_S) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK ();
                    return;
                }
                if (nmhSetFsMIY1731MepAvailabilityInterval
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_CC_AVLBLTY_INTERVAL_10_S) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                pEcfmLbLtMsg->ReqParams.TestParam.u4TestDuration =
                    pEcfmLbLtMsg->ReqParams.TestParam.u4TestDuration - 12;

                /* Test Deadline before setting */
                if (nmhTestv2FsMIY1731MepAvailabilityDeadline
                    (&u4ErrorCode, 
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     pEcfmLbLtMsg->ReqParams.TestParam.u4TestDuration) == SNMP_FAILURE)
                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                if (nmhSetFsMIY1731MepAvailabilityDeadline
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     pEcfmLbLtMsg->ReqParams.TestParam.u4TestDuration)
                     == SNMP_FAILURE)
                {
                
                    ECFM_CC_UNLOCK ();
                    return;
                }

                /*Make  Availability Measurement Table as Active */
                if (nmhTestv2FsMIY1731MepAvailabilityRowStatus
                    (&u4ErrorCode, 
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_ROW_STATUS_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsMIY1731MepAvailabilityRowStatus
                        (pEcfmLbLtMsg->ReqParams.u4ContextId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                         ECFM_ROW_STATUS_ACTIVE) == SNMP_FAILURE)
                    {
                        ECFM_CC_UNLOCK ();
                        return;
                    }
                }

                else
                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                /* Initiate Availability Measurement */
                if (nmhTestv2FsMIY1731MepAvailabilityStatus
                    (&u4ErrorCode,
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_TX_STATUS_START) == SNMP_FAILURE)

                {
                    
                     ECFM_CC_UNLOCK ();
                     return;
                }

                if (nmhSetFsMIY1731MepAvailabilityStatus
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_TX_STATUS_START) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK ();
                    return;
                }
                ECFM_CC_UNLOCK (); 
            }
            break;

        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Unknown event from Y1564 =[%d]\n",
                          pEcfmLbLtMsg->MsgType);
            break;
    }
    return;
}

/*****************************************************************************
 * Function Name      : EcfmY1564TestStop                                    *
 *                                                                           *
 * Description        : The routine implements the Y1564 Test Stop,          *
 *                      it calls up routine to transmit DM/LM frame.         *
 *                      the various events.                                  *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg  - Ecfm LbLt Message                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns            : None                                                 *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PRIVATE VOID
EcfmY1564TestStop (tEcfmLbLtMsg *pEcfmLbLtMsg)
{
    UINT4            u4ErrorCode = ECFM_INIT_VAL;
    INT4             i4AvailabilityStatus = ECFM_INIT_VAL;

    switch (pEcfmLbLtMsg->MsgType)
    {
        case ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST:
        case ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST:
#ifdef NPAPI_WANTED
            /* Call Np wrapper to Stop the TX forcefully */
            if (EcfmSlaStopSlaTest (&pEcfmLbLtMsg->ReqParams,
                                     pEcfmLbLtMsg->MsgType) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmY1564TestStop :"
                               "Failed to configure NP\r\n");
                return;
            }
 #endif
            if (nmhTestv2FsMIY1731MepTransmitDmStatus (&u4ErrorCode,
                                                       pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                       (INT4) ECFM_TX_STATUS_STOP)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmStatus (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                    pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                    pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                    pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                    (INT4) ECFM_TX_STATUS_STOP)
                == SNMP_FAILURE)
            {
                return;
            }

            if (pEcfmLbLtMsg->MsgType == ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST)
            {
                ECFM_CC_LOCK ();
                nmhGetFsMIY1731MepAvailabilityStatus 
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     &i4AvailabilityStatus);

                if (i4AvailabilityStatus != ECFM_TX_STATUS_NOT_READY) 
                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                /* Initiate Availability Measurement */
                if (nmhTestv2FsMIY1731MepAvailabilityStatus
                    (&u4ErrorCode,
                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_TX_STATUS_STOP) == SNMP_FAILURE)

                {
                    ECFM_CC_UNLOCK ();
                    return;
                }

                if (nmhSetFsMIY1731MepAvailabilityStatus
                    (pEcfmLbLtMsg->ReqParams.u4ContextId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                     ECFM_TX_STATUS_STOP) == SNMP_FAILURE)
                {
                    ECFM_CC_UNLOCK ();
                    return;
                }
               ECFM_CC_UNLOCK ();
            }
            /* Get the partial result */
            EcfmPortHandleExtInteraction (pEcfmLbLtMsg);
            break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Unknown event from Y1564 =[%d]\n",
                          pEcfmLbLtMsg->MsgType);
            break;
    }
    return;
}

/*****************************************************************************
 * Function Name      : EcfmY1564TestResults                                 *
 *                                                                           *
 * Description        : The routine implements the Y15644 get Test results   *
 *                      calls up routine to get the results.                 *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg - Ecfm LBLT Message                     *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns            : None                                                 *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PRIVATE VOID
EcfmY1564TestResults (tEcfmLbLtMsg * pEcfmLbLtMsg)
{
    switch (pEcfmLbLtMsg->MsgType)
    {
        case ECFM_Y1564_REQ_CFM_GET_TEST_REPORT:
            EcfmPortHandleExtInteraction (pEcfmLbLtMsg);
            break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Unknown event from RFC2544 =[%d]\n",
                          pEcfmLbLtMsg->MsgType);
            break;
    }
    return;
}
/*****************************************************************************
 * Function Name      : EcfmR2544TestInit                                    *
 *                                                                           *
 * Description        : The routine implements the RFC2544 Test Initiator,   *
 *                      it calls up routine to transmit LB/TST frame.        *
 *                      the various events.                                  *
 *                                                                           *
 * Input(s)           : pMepInfo - Pointer to the structure that             *
 *                      stores the information regarding MEP info.           *
 *                      u1EventId - Event Id                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE                          *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PRIVATE VOID
EcfmR2544TestInit (tEcfmLbLtMsg * pEcfmLbLtMsg)
{
    UINT4               u4ErrorCode = ECFM_INIT_VAL;


    switch (pEcfmLbLtMsg->MsgType)

    {
        case ECFM_R2544_REQ_START_THROUGHPUT_TEST:
        case ECFM_R2544_REQ_START_FRAMELOSS_TEST:
        case ECFM_R2544_REQ_START_BACKTOBACK_TEST:

#ifdef NPAPI_WANTED
            /* Call Np wrapper to Start the TX */
            if (EcfmSlaStartSlaTest (&pEcfmLbLtMsg->ReqParams,
                                      pEcfmLbLtMsg->MsgType) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmR2544TestInit:"
                               "Failed to configure NP\r\n");
                return;
            }
#endif
            break;

        case ECFM_R2544_REQ_START_LATENCY_TEST:
#ifdef NPAPI_WANTED
            /* Call Np wrapper to Start the TX */
            if (EcfmSlaStartSlaTest (&pEcfmLbLtMsg->ReqParams,
                                      pEcfmLbLtMsg->MsgType) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmR2544TestInit:"
                               "Failed to configure NP\r\n");
                return;
            }
#endif

            if (nmhTestv2FsMIY1731MepTransmitDmDestMacAddress (&u4ErrorCode,
                                                               pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                               pEcfmLbLtMsg->ReqParams.TxDstMacAddr)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmDestMacAddress (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                            pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                            pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                            pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                            pEcfmLbLtMsg->ReqParams.TxDstMacAddr)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhTestv2FsMIY1731MepTransmitDmType (&u4ErrorCode,
                                                     pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                     pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                     (INT4) ECFM_LBLT_DM_TYPE_DMM)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmType (
                                                  pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                  pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                  pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                  pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                  (INT4) ECFM_LBLT_DM_TYPE_DMM)
                == SNMP_FAILURE)
            {
                return;
            }



            pEcfmLbLtMsg->ReqParams.LatencyParam.u4TrialDuration =
                pEcfmLbLtMsg->ReqParams.LatencyParam.u4TrialDuration - 5;

            if (nmhTestv2FsMIY1731MepTransmitDmDeadline (&u4ErrorCode,
                                                         pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                         pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                         pEcfmLbLtMsg->ReqParams.LatencyParam.u4TrialDuration)  
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsMIY1731MepTransmitDmDeadline (
                                                      pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                      pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                                      pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                      pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                      pEcfmLbLtMsg->ReqParams.LatencyParam.u4TrialDuration)
                == SNMP_FAILURE)
            {
                return;
            }

            if (nmhTestv2FsMIY1731MepTransmitDmStatus (&u4ErrorCode,
                                                       pEcfmLbLtMsg->ReqParams.u4ContextId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,

                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                                       pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                       (INT4) ECFM_TX_STATUS_START)
                == SNMP_FAILURE)
            {
                return;
            }       
            if (nmhSetFsMIY1731MepTransmitDmStatus (pEcfmLbLtMsg->ReqParams.u4ContextId,
                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                                               pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                                                     (INT4) ECFM_TX_STATUS_START)
                == SNMP_FAILURE)
            {
                return;
            }

            
            break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Unknown event from RFC2544 =[%d]\n",
                          pEcfmLbLtMsg->MsgType);


            break;
    }
    return;
}

/*****************************************************************************
 * Function Name      : EcfmR2544TestStop                                    *
 *                                                                           *
 * Description        : The routine implements the R2544 Test Stop,          *
 *                      it calls up routine to transmit LB/TST frame.        *
 *                      the various events.                                  *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg  - Ecfm LbLt Message                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE                          *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PRIVATE VOID
EcfmR2544TestStop (tEcfmLbLtMsg *pEcfmLbLtMsg)
{
    UINT4            u4ErrorCode = ECFM_INIT_VAL;


#ifdef NPAPI_WANTED
    /* Call Np wrapper to Stop the TX forcefully */
    if (EcfmSlaStopSlaTest (&pEcfmLbLtMsg->ReqParams,
                pEcfmLbLtMsg->MsgType) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                "EcfmY1564TestStop:"
                "Failed to configure NP\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();

        return;
    }
#endif

    if (nmhTestv2FsMIY1731MepTransmitDmStatus (&u4ErrorCode,
                pEcfmLbLtMsg->ReqParams.u4ContextId,
                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                (INT4) ECFM_TX_STATUS_STOP)
            == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsMIY1731MepTransmitDmStatus (
                pEcfmLbLtMsg->ReqParams.u4ContextId,
                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEGId,
                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEId,
                pEcfmLbLtMsg->ReqParams.EcfmMepInfo.u4MEPId,
                (INT4) ECFM_TX_STATUS_STOP)
            == SNMP_FAILURE)
    {

        return;
    }
    pEcfmLbLtMsg->MsgType = ECFM_R2544_REQ_SLA_TEST_RESULT; 
    EcfmPortHandleExtInteraction (pEcfmLbLtMsg);

    return;

}
/*****************************************************************************
 * Function Name      : EcfmSlaGetPerfTestParams                             *
 *                                                                           *
 * Description        : This function Gets the  Sla test parameters to       *
 *                      initiate the test                                    *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg - Pointer to LBLT strcuture             *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
tEcfmReqParams  * 
EcfmSlaGetPerfTestParams (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4MegId,
                  UINT4 u4MeId, UINT4 u4MepId)
{
    tEcfmLbLtMsg            *pLbLtMsg = NULL;  
    tEcfmCcMepInfo          *pMepNode = NULL;
    tEcfmReqParams          *pEcfmReqParams = NULL;
    tEcfmMacAddr            RetMacAddr;
    tEcfmMacAddr            LocMacAddr;
    tEcfmCfaIfInfo          CfaIfInfo;
    UINT4                   u4MdIndex = ECFM_INIT_VAL;
    UINT4                   u4MaIndex = ECFM_INIT_VAL;
    UINT4                   u4MepIdentifier = ECFM_INIT_VAL;
    UINT4                   u4RMepIdentifier = ECFM_INIT_VAL;
    UINT4                   u4NextMdIndex = ECFM_INIT_VAL;
    UINT4                   u4NextMaIndex = ECFM_INIT_VAL;
    UINT4                   u4NextMepIdentifier = ECFM_INIT_VAL;
    UINT4                   u4NextRMepIdentifier = ECFM_INIT_VAL;
    UINT4                   u4CurrentContextId = ECFM_INIT_VAL;
    UINT4                   u4NextContextId = ECFM_INIT_VAL;
    UINT2                   u2TagType = ECFM_INIT_VAL;
    INT4                    i4RMepState = ECFM_INIT_VAL;

    ECFM_MEMSET (LocMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    ECFM_MEMSET (RetMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    ECFM_MEMSET (&CfaIfInfo, ECFM_INIT_VAL, sizeof (tCfaIfInfo));
 
    ECFM_CC_LOCK();

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return NULL;
    }

    /* Check system status for the Context*/
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                "EcfmLbLtHandleY1564andRFC2544Events:"
                "ECFM Module is Shutdown\r\n");
        return NULL;
    }
    pMepNode =  EcfmCcUtilGetMepEntryFrmGlob (u4MegId, u4MeId, (UINT2)u4MepId);

    if (pMepNode == NULL)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtHandleY1564andRFC2544Events:"
                       "No MEP Entry\r\n");
        return NULL;
    }

    if (pMepNode->EcfmSlaParams.u4SlaId != u4SlaId)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtHandleY1564andRFC2544Events:"
                       "SLA Id is not associated with MEP Entry\r\n");
        return NULL;
    }

    /* Allocating MEM Block for tEcfmReqParams */

    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pLbLtMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmLbLtHandleY1564andRFC2544Events "
                      ": MEM Block Allocation Failed \r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return NULL;
    }

    ECFM_MEMSET (pLbLtMsg, ECFM_INIT_VAL, ECFM_LBLT_MSG_INFO_SIZE);
    
    u4CurrentContextId = u4ContextId;
    /* Update Interface Index details in the request params to
     * send the test frames */
    pLbLtMsg->ReqParams.u4IfIndex = pMepNode->u4IfIndex;


    if ((EcfmUtilGetIfInfo (pMepNode->u4IfIndex, &CfaIfInfo)) != CFA_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtIfHandleCreatePort: Getting Interface Info from "
                       "CFA FAILED \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtMsg);
        return NULL;
    }

    pLbLtMsg->ReqParams.u4PortSpeed = CfaIfInfo.u4IfSpeed;
    pLbLtMsg->ReqParams.u4IfMtu = CfaIfInfo.u4IfMtu;

    /* Update Tag Type  details for sending in the test frames */
    EcfmVlanGetPortEtherType (pLbLtMsg->ReqParams.u4IfIndex, &u2TagType);
    pLbLtMsg->ReqParams.u4TagType = (UINT4) u2TagType;


    /* Update Vlan details for sending in the test frames*/

    pLbLtMsg->ReqParams.OutVlanId = (UINT2) pMepNode->u4PrimaryVidIsid; 


    /*Update Source Mac Address for sending in the test frames */

    nmhGetFsMIEcfmMepMacAddress (u4CurrentContextId, u4MegId, u4MeId, u4MepId,
                                 &LocMacAddr);

    MEMCPY (pLbLtMsg->ReqParams.TxSrcMacAddr, LocMacAddr, sizeof(tMacAddr));

    /*Check for remote MEP entry for a particular context */

    if (nmhGetNextIndexFsMIEcfmMepDbTable (u4CurrentContextId,
                                           &u4NextContextId, 0,
                                           &u4NextMdIndex, 0,
                                           &u4NextMaIndex, 0,
                                           &u4NextMepIdentifier, 0,
                                           &u4NextRMepIdentifier) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtMsg);
        return NULL;
    }
    do
    {
        
        u4MdIndex = u4NextMdIndex;
        u4MaIndex =  u4NextMaIndex;
        u4MepIdentifier = u4NextMepIdentifier;
        u4RMepIdentifier = u4NextRMepIdentifier;

        if ((u4MegId == u4MdIndex) &&
            (u4MeId == u4MaIndex) &&
            (u4MepId == u4MepIdentifier))
        {
            nmhGetFsMIEcfmMepDbRMepState (u4CurrentContextId, u4MdIndex,
                                          u4MaIndex, u4MepIdentifier,
                                          u4RMepIdentifier, &i4RMepState);

            if (i4RMepState == ECFM_RMEP_OK)
            {
                ECFM_MEMSET (RetMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
                nmhGetFsMIEcfmMepDbMacAddress (u4CurrentContextId,
                                               u4MdIndex, u4MaIndex, u4MepIdentifier,
                                               u4RMepIdentifier,&RetMacAddr);


                /* Update Destination MAC details
                 * details before starting the test */
                MEMCPY (pLbLtMsg->ReqParams.TxDstMacAddr, RetMacAddr,
                        sizeof(tMacAddr));
            }
            break;
        }
    }while(nmhGetNextIndexFsMIEcfmMepDbTable (u4CurrentContextId,
                                              &u4NextContextId, u4MdIndex, &u4NextMdIndex,
                                              u4MaIndex, &u4NextMaIndex, u4MepIdentifier,
                                              &u4NextMepIdentifier, u4RMepIdentifier,
                                              &u4NextRMepIdentifier) != SNMP_FAILURE);

    ECFM_CC_UNLOCK ();
    pEcfmReqParams = &pLbLtMsg->ReqParams;
    return pEcfmReqParams;
}

/*****************************************************************************
 * Function Name      : EcfmR2544TestResults                                 *
 *                                                                           *
 * Description        : The routine implements the get the R2544 Test results*
 *                      calls up routine to get the results.                 *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg - Ecfm LBLT Message                     *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE                          *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/
PRIVATE VOID
EcfmR2544TestResults (tEcfmLbLtMsg * pEcfmLbLtMsg)
{
    EcfmPortHandleExtInteraction (pEcfmLbLtMsg);
    return;
}

/*****************************************************************************
 * Function                  : EcfmSlaStartSlaTest                           *
 *                                                                           *
 * Description               : This function programs the Hardware to start  *
 *                             the Traffic with the provided parameters for  *
 *                             the specified duration                        *
 *                                                                           *
 * Input(s)                  : pEcfmReqParams - ReqParamsStructure           *
 *                             u4MsgType - Message Type                      *
 *                                                                           *
 *                                                                           *
 * Output                    : None.                                         *
 *                                                                           *
 * Returns                   : ECFM_SUCCESS/ECFM_FAILURE.                    *
 *                                                                           *
 *****************************************************************************/
INT4
EcfmSlaStartSlaTest (tEcfmReqParams * pEcfmReqParams,
                      UINT4 u4MsgType)
{
#ifdef NPAPI_WANTED
    ECFM_LBLT_TRC_FN_ENTRY ();
    tEcfmBufChainHeader *pBuf = NULL;
    tVlanTagInfo        VlanInfo;
    tHwTestInfo         HwTestInfo;
    UINT2               u2VlanTagId = ECFM_INIT_VAL;
    UINT2               u2PduSize = ECFM_INIT_VAL;
    UINT1               u1DeiBitValue = (UINT1) ECFM_INIT_VAL;
    UINT1               au1TxBuf[SLA_FRAME_SIZE];
    UINT1               au1TxBuf1[SLA_FRAME_SIZE];
    UINT1               *pu1ExPduStart = NULL;
    UINT1               *pu1Buf = NULL;
    UINT1               u1PduLength = ECFM_INIT_VAL;
    UINT1               u1Priority = ECFM_FALSE;


    MEMSET(au1TxBuf, ECFM_INIT_VAL, SLA_FRAME_SIZE);

    MEMSET (&HwTestInfo, ECFM_INDEX_ZERO, sizeof(tHwTestInfo));
    ECFM_MEMSET (&VlanInfo, ECFM_INIT_VAL, sizeof (tVlanTagInfo));

    if (pEcfmReqParams == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmSlaStartSlaTest: Failed to start the SLA Test \r\n");
        return ECFM_FAILURE;
    }

    if ((u4MsgType == ECFM_Y1564_REQ_SLA_CONF_TEST_START)   ||
        (u4MsgType == ECFM_Y1564_REQ_SLA_PERF_TEST_START)   ||              
        (u4MsgType == ECFM_R2544_REQ_START_THROUGHPUT_TEST) ||
        (u4MsgType == ECFM_R2544_REQ_START_FRAMELOSS_TEST)  ||
        (u4MsgType == ECFM_R2544_REQ_START_LATENCY_TEST)  ||
        (u4MsgType == ECFM_R2544_REQ_START_BACKTOBACK_TEST))
    {
        pBuf = ECFM_ALLOC_CRU_BUF (pEcfmReqParams->TrafficProfileParam.u2FrameSize,
                                   ECFM_INIT_VAL);
        if (pBuf == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmSlaStartSlaTest: Buffer allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    else
    {
	 ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
	                "EcfmSlaStartSlaTest: received "
                        "Invalid message type\r\n");
	 return ECFM_FAILURE;
    }

    pu1ExPduStart = au1TxBuf;

    /*Copy Destination MAC in first 6 Bytes */
    MEMCPY (pu1ExPduStart, pEcfmReqParams->TxDstMacAddr, sizeof (tMacAddr));
    pu1ExPduStart = pu1ExPduStart + MAC_ADDR_LEN;

    /*Copy Source MAC in next 6 Bytes */
    MEMCPY (pu1ExPduStart, pEcfmReqParams->TxSrcMacAddr, sizeof (tMacAddr));
    pu1ExPduStart = pu1ExPduStart + MAC_ADDR_LEN;

    ECFM_PUT_2BYTE (pu1ExPduStart, (UINT2)pEcfmReqParams->u4TagType);

    /* code to include VLAN header */
    VlanInfo.u2VlanId = pEcfmReqParams->OutVlanId;

    VlanInfo.u1Priority = pEcfmReqParams->TrafficProfileParam.u1TrafProfPCP ;

    u1DeiBitValue = ECFM_INIT_VAL;

    u1Priority = ((VlanInfo.u1Priority) << 1);

    if ((u1DeiBitValue == ECFM_TRUE)
        && (VlanInfo.u1DropEligible == ECFM_TRUE))
    {
        u1Priority = (UINT1) (u1Priority | 0x0001);
    }
    u2VlanTagId = (UINT2) u1Priority;
    u2VlanTagId = (UINT2) (u2VlanTagId << ECFM_VLAN_TAG_PRIORITY_DEI_SHIFT);
    u2VlanTagId = (UINT2) (u2VlanTagId | VlanInfo.u2VlanId);

    ECFM_PUT_2BYTE (pu1ExPduStart, u2VlanTagId);

    /* Fill Y1564 Payload information */
    MEMCPY (pu1ExPduStart,
            pEcfmReqParams->TrafficProfileParam.au1TrafProfPayload,
            ECFM_Y1564_AND_RFC2544_PAYLOAD_SIZE);


    /*Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1ExPduStart, ECFM_INIT_VAL,
                                (UINT4) STRLEN (pu1ExPduStart)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "\n EcfmCciSmXmitCCM:Copying into Bufferfailed\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    u1PduLength = (UINT1) ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);
    u2PduSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (ECFM_COPY_FROM_CRU_BUF (pBuf, au1TxBuf1, 0, u2PduSize) ==
        ECFM_CRU_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "PDU Copy From CRU Buffer FAILED\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    pu1Buf = au1TxBuf1;

    /*Copy all the necessary parameters to start
     * the SLA Test */

    HwTestInfo.HwSlaInfo.pu1Pdu = pu1Buf;

    MEMCPY (HwTestInfo.HwSlaInfo.SlaTxSrcMacAddr, pEcfmReqParams->TxSrcMacAddr,
            sizeof(tMacAddr));
    MEMCPY (HwTestInfo.HwSlaInfo.SlaTxDstMacAddr, pEcfmReqParams->TxDstMacAddr,
            sizeof(tMacAddr));
    pEcfmReqParams->InVlanId = 10;
    HwTestInfo.HwSlaInfo.InVlanId = pEcfmReqParams->InVlanId;
    HwTestInfo.HwSlaInfo.OutVlanId = pEcfmReqParams->OutVlanId;

    HwTestInfo.HwSlaInfo.u4IfIndex = pEcfmReqParams->u4IfIndex;
    HwTestInfo.HwSlaInfo.u4ContextId = pEcfmReqParams->u4ContextId;
    HwTestInfo.HwSlaInfo.u4TagType = pEcfmReqParams->u4TagType;
    HwTestInfo.HwSlaInfo.u4Rate = pEcfmReqParams->TrafficProfileParam.u4Rate;
    HwTestInfo.HwSlaInfo.u4YellowFrRate = pEcfmReqParams->u4YellowFrRate;
    HwTestInfo.HwSlaInfo.u4CIRPps = pEcfmReqParams->u4CIRPps;
    HwTestInfo.HwSlaInfo.u4YellowFrPps = pEcfmReqParams->u4YellowFrPps;

    HwTestInfo.HwSlaInfo.u4SlaId = pEcfmReqParams->EcfmMepInfo.u4SlaId;

    HwTestInfo.HwSlaInfo.u4R2544SubTest = pEcfmReqParams->TrafficProfileParam.u1R2544SubTest;

    if ((u4MsgType == ECFM_Y1564_REQ_SLA_CONF_TEST_START)   ||
        (u4MsgType == ECFM_Y1564_REQ_SLA_PERF_TEST_START))
    {
        HwTestInfo.HwSlaInfo.u4SlaDuration = pEcfmReqParams->TestParam.u4TestDuration;
        HwTestInfo.HwSlaInfo.u4CIR = pEcfmReqParams->TestParam.u4CIR;
        HwTestInfo.HwSlaInfo.u4EIR = pEcfmReqParams->TestParam.u4EIR;
        HwTestInfo.HwSlaInfo.u2EvcId = pEcfmReqParams->OutVlanId;

        HwTestInfo.HwSlaInfo.u1InCos = pEcfmReqParams->TestParam.u1TrafProfInCos;
        HwTestInfo.HwSlaInfo.u1OutCos = pEcfmReqParams->TestParam.u1TrafProfOutCos;
        HwTestInfo.HwSlaInfo.u2StepId =
            pEcfmReqParams->TestParam.u2SlaCurrentStep;
        HwTestInfo.HwSlaInfo.u1TestMode = pEcfmReqParams->TestParam.u1TestMode;


    }
    if (u4MsgType == ECFM_R2544_REQ_START_THROUGHPUT_TEST)
    {
        HwTestInfo.HwSlaInfo.u4SlaDuration = pEcfmReqParams->ThroughputParam.u4TrialDuration;
        HwTestInfo.HwSlaInfo.u4Rate = pEcfmReqParams->ThroughputParam.u4Rate;
        HwTestInfo.HwSlaInfo.u4TrialCount = pEcfmReqParams->ThroughputParam.u4TrialCount;

    }
    if (u4MsgType == ECFM_R2544_REQ_START_FRAMELOSS_TEST)
    {
        HwTestInfo.HwSlaInfo.u4SlaDuration = pEcfmReqParams->FrameLossParam.u4TrialDuration;
        HwTestInfo.HwSlaInfo.u4Rate = pEcfmReqParams->FrameLossParam.u4Rate;
        HwTestInfo.HwSlaInfo.u4TrialCount = pEcfmReqParams->FrameLossParam.u4TrialCount;
    }
    if (u4MsgType == ECFM_R2544_REQ_START_LATENCY_TEST)
    {
        HwTestInfo.HwSlaInfo.u4SlaDuration = pEcfmReqParams->LatencyParam.u4TrialDuration;
        HwTestInfo.HwSlaInfo.u4Rate = pEcfmReqParams->LatencyParam.u4Rate;
        HwTestInfo.HwSlaInfo.u4TrialCount = pEcfmReqParams->LatencyParam.u4TrialCount;
    }

    if (u4MsgType == ECFM_R2544_REQ_START_BACKTOBACK_TEST)
    {
        HwTestInfo.HwSlaInfo.u4SlaDuration = pEcfmReqParams->BackToBackParam.u4TrialDuration;
        HwTestInfo.HwSlaInfo.u4Rate = pEcfmReqParams->BackToBackParam.u4Rate;
        HwTestInfo.HwSlaInfo.u4TrialCount = pEcfmReqParams->BackToBackParam.u4TrialCount;
    }

    HwTestInfo.HwSlaInfo.u2EvcId = pEcfmReqParams->u2EvcId;

    HwTestInfo.HwSlaInfo.u2EvcId = pEcfmReqParams->OutVlanId;

    HwTestInfo.HwSlaInfo.u1Dei = ECFM_INIT_VAL;

    HwTestInfo.HwSlaInfo.u2PktSize =
        pEcfmReqParams->TrafficProfileParam.u2FrameSize;
    HwTestInfo.HwSlaInfo.u1Pcp =
        pEcfmReqParams->TrafficProfileParam.u1TrafProfPCP;
    HwTestInfo.HwSlaInfo.u4DwellTime =
        pEcfmReqParams->TrafficProfileParam.u4TrafProfDwellTime;
    HwTestInfo.HwSlaInfo.u1SeqNoCheck =
        pEcfmReqParams->TrafficProfileParam.u1TrafProfSeqNoCheck;


    MEMCPY (HwTestInfo.HwSlaInfo.au1TrafProfPayload,
            pEcfmReqParams->TrafficProfileParam.au1TrafProfPayload,
            ECFM_Y1564_AND_RFC2544_PAYLOAD_SIZE);

    HwTestInfo.u4InfoType = ECFM_HW_START_SLA_TEST;

    if (EcfmFsMiEcfmSlaTest (&HwTestInfo) == FNP_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmSlaStartSlaTest: Failed to Program Hardware \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return  ECFM_FAILURE;
    }

    ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
#else
    UNUSED_PARAM (pEcfmReqParams);
    UNUSED_PARAM (u4MsgType);
#endif
    return ECFM_SUCCESS;
}
/*****************************************************************************
 * Function                  : EcfmSlaStopSlaTest                            *
 *                                                                           *
 * Description               : This function programs the Hardware to stop   *
 *                             the test                                      *
 *                             u4MsgType - Message Type                      *
 *                                                                           *
 * Input(s)                  : pEcfmReqParams - ReqParamsStructure           *
 *                                                                           *
 *                                                                           *
 * Output                    : None.                                         *
 *                                                                           *
 * Returns                   : ECFM_SUCCESS/ECFM_FAILURE.                    *
 *                                                                           *
 *****************************************************************************/
 INT4
 EcfmSlaStopSlaTest (tEcfmReqParams * pEcfmReqParams,
                        UINT4 u4MsgType)
{
#ifdef NPAPI_WANTED
    tHwTestInfo         HwTestInfo;

    if (pEcfmReqParams == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmSlaStartSlaTest: Failed to start the SLA Test \r\n");
        return ECFM_FAILURE;
    }

    /* Only Evc Id is enough to stop the test. So not copying all the
     * parameters */
    HwTestInfo.HwSlaInfo.u2EvcId = pEcfmReqParams->u2EvcId;
    HwTestInfo.HwSlaInfo.u4IfIndex = pEcfmReqParams->u4IfIndex;

    if ((u4MsgType == ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST) ||
        (u4MsgType == ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST) ||
        (u4MsgType == ECFM_R2544_REQ_STOP_BENCHMARK_TEST))
    {

        HwTestInfo.u4InfoType = ECFM_HW_STOP_SLA_TEST;
    }

    if (EcfmFsMiEcfmSlaTest (&HwTestInfo) == FNP_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmSlaStopSlaTest: Failed to Program Hardware \r\n");
        return ECFM_FAILURE;
    }

#else
    UNUSED_PARAM (pEcfmReqParams);
    UNUSED_PARAM (u4MsgType);
#endif
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function                  : EcfmSlaFetchSlaReport                         *
 *                                                                           *
 * Description               : This function programs the Hardware to stop   *
 *                             the Traffic                                   *
 *                                                                           *
 * Input(s)                  : pEcfmExtInParams -  ExtInParamStructure       *
 *                             u4MsgType - Message Type                      *
 *                             tEcfmReqParams - Req Param Structure          *
 *                                                                           *
 * Output                    : None.                                         *
 *                                                                           *
 * Returns                   : ECFM_SUCCESS/ECFM_FAILURE.                    *
 *                                                                           *
 *****************************************************************************/
 INT4
 EcfmSlaFetchSlaReport (UINT4 u4MsgType,
                               tEcfmReqParams * pEcfmReqParams,
                               tEcfmExtInParams * pEcfmExtInParams)
{

#ifdef NPAPI_WANTED
    tHwTestInfo         HwTestInfo;

    if (pEcfmExtInParams == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmSlaStartSlaTest: Failed to start the SLA Test \r\n");
        return ECFM_FAILURE;
    }


    /* Only Evc Id is enough to fetch the test result.
     * So not copying all the parameters */
    HwTestInfo.HwSlaInfo.u4SlaId =
        pEcfmReqParams->EcfmMepInfo.u4SlaId;
    HwTestInfo.HwSlaInfo.u4ContextId =
        pEcfmReqParams->u4ContextId;
    HwTestInfo.HwSlaInfo.u2EvcId =
        pEcfmReqParams->OutVlanId;
    HwTestInfo.HwSlaInfo.u4IfIndex =
        pEcfmReqParams->u4IfIndex;
    HwTestInfo.HwSlaInfo.u4R2544SubTest =
        pEcfmReqParams->TrafficProfileParam.u1R2544SubTest;
    HwTestInfo.HwSlaInfo.u4TrialCount =
        pEcfmReqParams->TrafficProfileParam.u4TrialCount;
    HwTestInfo.HwSlaInfo.u2PktSize =
        pEcfmReqParams->TrafficProfileParam.u2FrameSize;

    if ((u4MsgType == ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST) ||
        (u4MsgType == ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST) ||
        (u4MsgType == ECFM_Y1564_REQ_CFM_GET_TEST_REPORT) ||
        (u4MsgType == ECFM_R2544_REQ_SLA_TEST_RESULT))
    {
        HwTestInfo.u4InfoType = ECFM_HW_FETCH_SLA_REPORT;
    }

    if (EcfmFsMiEcfmSlaTest (&HwTestInfo) == FNP_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmSlaFetchSlaReport: Failed to Program Hardware \r\n");
        return ECFM_FAILURE;
    }
    if ((u4MsgType == ECFM_Y1564_REQ_CFM_GET_TEST_REPORT) ||
        (u4MsgType == ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST) ||
        (u4MsgType == ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST))
    {

#ifdef Y1564_WANTED
        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u4StatsTxPkts =
            HwTestInfo.HwTestFrameStats.u4TxPkts;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u4StatsRxPkts =
            HwTestInfo.HwTestFrameStats.u4RxPkts;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u8StatsTxBytes =
            HwTestInfo.HwTestFrameStats.u8TxBytes;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u8StatsRxBytes =
            HwTestInfo.HwTestFrameStats.u8RxBytes;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u4CIRPps =
            HwTestInfo.HwTestFrameStats.u4CIRPps;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u2FrameSize =
            HwTestInfo.HwTestFrameStats.u2PktSize;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u4YellowFrTxPkts =
            HwTestInfo.HwTestFrameStats.u4YellowFrTxPkts;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u4YellowFrRxPkts =
            HwTestInfo.HwTestFrameStats.u4YellowFrRxPkts;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u8YellowFrTxBytes =
            HwTestInfo.HwTestFrameStats.u8YellowFrTxBytes;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u8YellowFrRxBytes =
            HwTestInfo.HwTestFrameStats.u8YellowFrRxBytes;

        pEcfmExtInParams->Y1564ReqParams.ResultInfo.u4YellowFrPps =
            HwTestInfo.HwTestFrameStats.u4EIRPps; 

#endif
    }


    if (u4MsgType == ECFM_R2544_REQ_SLA_TEST_RESULT)
    {

        pEcfmExtInParams->R2544ReqParams.SlaInfo.u4SlaId =
            pEcfmReqParams->EcfmMepInfo.u4SlaId;
        pEcfmExtInParams->R2544ReqParams.u4ContextId =
            pEcfmReqParams->u4ContextId;
        pEcfmExtInParams->R2544ReqParams.u1SubTest =
            pEcfmReqParams->TrafficProfileParam.u1R2544SubTest;
        pEcfmExtInParams->R2544ReqParams.u4FrameSize =
            pEcfmReqParams->TrafficProfileParam.u2FrameSize;

        if (HwTestInfo.HwSlaInfo.u4R2544SubTest == RFC2544_THROUGHPUT_TEST)
        {
            pEcfmExtInParams->R2544ReqParams.u4ReqType =
                R2544_THROUGHPUT_TEST_RESULT;
            pEcfmExtInParams->R2544ReqParams.ThroughputResult.u4TrialCount =
                pEcfmReqParams->TrafficProfileParam.u4TrialCount;
            pEcfmExtInParams->R2544ReqParams.ThroughputResult.u4TxCount =
                HwTestInfo.HwTestFrameStats.u4TxPkts;
            pEcfmExtInParams->R2544ReqParams.ThroughputResult.u4RxCount =
                HwTestInfo.HwTestFrameStats.u4RxPkts;
            pEcfmExtInParams->R2544ReqParams.ThroughputResult.u4Rate =
                pEcfmReqParams->TrafficProfileParam.u4Rate;

            pEcfmExtInParams->R2544ReqParams.LatencyResult.u4TxCount =
                HwTestInfo.HwTestFrameStats.u4TxPkts;
            pEcfmExtInParams->R2544ReqParams.LatencyResult.u4RxCount =
                HwTestInfo.HwTestFrameStats.u4RxPkts;

        }
        else if (HwTestInfo.HwSlaInfo.u4R2544SubTest == RFC2544_FRAMELOSS_TEST)
        {
            pEcfmExtInParams->R2544ReqParams.u4ReqType =
                R2544_FRAMELOSS_TEST_RESULT;
            pEcfmExtInParams->R2544ReqParams.FrameLossResult.u4TrialCount =
                pEcfmReqParams->TrafficProfileParam.u4TrialCount;
            pEcfmExtInParams->R2544ReqParams.FrameLossResult.u4TxCount =
                HwTestInfo.HwTestFrameStats.u4TxPkts;
            pEcfmExtInParams->R2544ReqParams.FrameLossResult.u4RxCount =
                HwTestInfo.HwTestFrameStats.u4RxPkts;
        }


        else if (HwTestInfo.HwSlaInfo.u4R2544SubTest == RFC2544_BACKTOBACK_TEST)
        {
            pEcfmExtInParams->R2544ReqParams.u4ReqType =
                R2544_BACKTOBACK_TEST_RESULT;

            pEcfmExtInParams->R2544ReqParams.FrameLossResult.u4TrialCount =
                pEcfmReqParams->TrafficProfileParam.u4TrialCount;
            pEcfmExtInParams->R2544ReqParams.BackToBackResult.u4TxCount =
                HwTestInfo.HwTestFrameStats.u4TxPkts;
            pEcfmExtInParams->R2544ReqParams.BackToBackResult.u4RxCount =
                HwTestInfo.HwTestFrameStats.u4RxPkts;
        }

    }

#else
    UNUSED_PARAM (pEcfmReqParams);
    UNUSED_PARAM (u4MsgType);
    UNUSED_PARAM (pEcfmExtInParams);
#endif

    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function                  : EcfmCreateHwLoopback                          *
 *                                                                           *
 * Description               : This function programs the Hardware to Create *
 *                             and update the Loopback Status                *
 *                                                                           *
 * Input(s)                  : pMepNode - EcfmCcMepInfoStructure             *
 *                                                                           *
 *                                                                           *
 * Output                    : None.                                         *
 *                                                                           *
 * Returns                   : ECFM_SUCCESS/ECFM_FAILURE.                    *
 *                                                                           *
 *****************************************************************************/
INT4
EcfmCreateHwLoopback (tEcfmCcMepInfo * pMepNode)
{
#ifdef NPAPI_WANTED
    tHwLoopbackInfo     MepHwLoopbackInfo;
    tEcfmMacAddr        LocMacAddr;

    ECFM_MEMSET (LocMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if ((pMepNode == NULL) || (pMepNode->pPortInfo == NULL))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmCreateHwLoopback: Failed Program Hardware \r\n");
        return ECFM_FAILURE;
    }

    MepHwLoopbackInfo.u4ContextId = pMepNode->pPortInfo->u4ContextId;
    MepHwLoopbackInfo.u4IfIndex = pMepNode->u4IfIndex;
    MepHwLoopbackInfo.u4MdIndex = pMepNode->u4MdIndex;
    MepHwLoopbackInfo.u4MaIndex = pMepNode->u4MaIndex;
    MepHwLoopbackInfo.u4PrimaryVidIsid = pMepNode->u4PrimaryVidIsid;
    MepHwLoopbackInfo.i4LoopbackStatus = pMepNode->i4LoopbackStatus;
    MepHwLoopbackInfo.u2MepId = pMepNode->u2MepId;
    MepHwLoopbackInfo.u2PortNum = pMepNode->u2PortNum;
    MepHwLoopbackInfo.u1MdLevel = pMepNode->u1MdLevel;
    MepHwLoopbackInfo.u1Direction =  pMepNode->u1Direction;

    nmhGetFsMIEcfmMepMacAddress (MepHwLoopbackInfo.u4ContextId,
                                 MepHwLoopbackInfo.u4MdIndex,
                                 MepHwLoopbackInfo.u4MaIndex,
                                 (UINT4) MepHwLoopbackInfo.u2MepId,
                                 &LocMacAddr);

    MEMCPY (MepHwLoopbackInfo.MepMacAddr, LocMacAddr, sizeof(tMacAddr));

    /* context is being released in Get routine. So again
     * taking the context information */

    if (ECFM_CC_SELECT_CONTEXT (MepHwLoopbackInfo.u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    if (EcfmFsMiEcfmAddHwLoopbackInfo (&MepHwLoopbackInfo) == FNP_FAILURE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmCreateHwLoopback: Failed to Program Hardware \r\n");
        return ECFM_FAILURE;
    }

#else
    UNUSED_PARAM (pMepNode);
#endif

    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function                  : EcfmDeleteHwLoopback                          *
 *                                                                           *
 * Description               : This function programs the Hardware to Delete *
 *                             the Loopback Status                           *
 *                                                                           *
 * Input(s)                  : pMepNode - EcfmCcMepInfoStructure             *
 *                                                                           *
 *                                                                           *
 * Output                    : None.                                         *
 *                                                                           *
 * Returns                   : ECFM_SUCCESS/ECFM_FAILURE.                    *
 *                                                                           *
 *****************************************************************************/
INT4
EcfmDeleteHwLoopback (tEcfmCcMepInfo * pMepNode)
{
#ifdef NPAPI_WANTED
    tHwLoopbackInfo     MepHwLoopbackInfo;
    tEcfmMacAddr        LocMacAddr;

    ECFM_MEMSET (LocMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if ((pMepNode == NULL) || (pMepNode->pPortInfo == NULL))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmCreateHwLoopback: Failed Program Hardware \r\n");
        return ECFM_FAILURE;
    }

    MepHwLoopbackInfo.u4ContextId = pMepNode->pPortInfo->u4ContextId;
    MepHwLoopbackInfo.u4IfIndex = pMepNode->u4IfIndex;
    MepHwLoopbackInfo.u4MdIndex = pMepNode->u4MdIndex;
    MepHwLoopbackInfo.u4MaIndex = pMepNode->u4MaIndex;
    MepHwLoopbackInfo.u4PrimaryVidIsid = pMepNode->u4PrimaryVidIsid;
    MepHwLoopbackInfo.i4LoopbackStatus = pMepNode->i4LoopbackStatus;
    MepHwLoopbackInfo.u2MepId = pMepNode->u2MepId;
    MepHwLoopbackInfo.u2PortNum = pMepNode->u2PortNum;
    MepHwLoopbackInfo.u1MdLevel = pMepNode->u1MdLevel;
    MepHwLoopbackInfo.u1Direction =  pMepNode->u1Direction;

    nmhGetFsMIEcfmMepMacAddress (MepHwLoopbackInfo.u4ContextId,
                                 MepHwLoopbackInfo.u4MdIndex,
                                 MepHwLoopbackInfo.u4MaIndex,
                                 (UINT4) MepHwLoopbackInfo.u2MepId,
                                 &LocMacAddr);

    /* context is being released in Get routine. So again
     * taking the context information */

    if (ECFM_CC_SELECT_CONTEXT (MepHwLoopbackInfo.u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    MEMCPY (MepHwLoopbackInfo.MepMacAddr, LocMacAddr, sizeof(tMacAddr));

    if (EcfmFsMiEcfmDelHwLoopbackInfo (&MepHwLoopbackInfo) == FNP_FAILURE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                       "EcfmCreateHwLoopback: Failed to Program Hardware \r\n");
        return ECFM_FAILURE;
    }

#else
    UNUSED_PARAM (pMepNode);
#endif

    return ECFM_SUCCESS;
}

#endif
/*****************************************************************************
  End of File cfmsla.c
 ******************************************************************************/
