/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmutil.c,v 1.61 2016/05/25 10:13:04 siva Exp $
 *
 * Description: This file contains the Utility Procedures used for 
 *              CC and LBLT Task
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmFormatPduHdr PROTO ((tExternalPduMsg *, UINT1 **, UINT1));
PRIVATE VOID EcfmFormatEthHdr PROTO ((tExternalPduMsg *, UINT1 **, UINT2));

extern UINT4        gu4ModId;    /* Module Id that is used to identify
                                 * the modules that registered to
                                 * indicate the SF / SF clear
                                 * conditions. */

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmUtilEpochToDate
 *
 *    DESCRIPTION      : This function converts the Epoch to Date Format                        
 *
 *    INPUT            : u4sec  - Number of sec from 1st Jan 1970 
 *
 *    OUTPUT           : u1DateFormat- Formatted Date 
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmUtilEpochToDate (UINT4 u4sec, UINT1 *pu1DateFormat)
{
    tUtlTm              tm;
    UINT1              *au1Months[] = {
        (UINT1 *) "Jan", (UINT1 *) "Feb", (UINT1 *) "Mar",
        (UINT1 *) "Apr", (UINT1 *) "May", (UINT1 *) "Jun",
        (UINT1 *) "Jul", (UINT1 *) "Aug", (UINT1 *) "Sep",
        (UINT1 *) "Oct", (UINT1 *) "Nov", (UINT1 *) "Dec"
    };
    UtlGetTimeForTicks ((u4sec * ECFM_NUM_OF_TIME_UNITS_IN_A_SEC), &tm);
    SPRINTF ((CHR1 *) pu1DateFormat, "%u %s %u %02u:%02u:%02u",
             tm.tm_mday,
             au1Months[tm.tm_mon],
             tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : EcfmGetTimeFromEpoch                            */
/*                                                                          */
/*    Description         : This function returns the number of seconds     */
/*                          passed from epoch.                             */
/*    Input(s)            : None.                                           */
/*                                                                          */
/*    Output(s)           : None                                            */
/*                                                                          */
/*    Returns             : Number of seconds passed from epoch             */
/*                                                                          */
/* **************************************************************************/
PUBLIC UINT4
EcfmUtilGetTimeFromEpoch (VOID)
{
    tOsixSysTime        SysTime;
    ECFM_GET_SYS_TIME (&SysTime);
    return (SysTime / ECFM_NUM_OF_TIME_UNITS_IN_A_SEC);
}

/**************************************************************************/
/* Function Name       : EcfmGlobalTrace                                  */
/*                                                                        */
/* Description         : This function prints the trace message, In case  */
/*                       of MI u4ModTrace and u4Flags will be ignore and  */
/*                       u4Flags is set to 1, and will be AND with        */
/*                       ECFM_GLB_TRC_FLAG to decide toprint the buffer or*/
/*                       not                                              */
/*                                                                        */
/* Input(s)            : u4ContextId - context for whcich the trace is    */
/*                                     enabled                            */
/*                       u4ModTrace - Which module trace needs to be      */
/*                                    enabled, currently UNUSED           */
/*                       u4Flags - Which path trcaes needs to be enabled  */
/*                                 , UNUSED                               */
/*                       fmt - print string                               */
/*                                                                        */
/* Output(s)           :None                                              */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
PUBLIC VOID
EcfmGlobalTrace (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...)
{
#ifdef TRACE_WANTED
    va_list             ap;
    static CHR1         ac1Str[ECFM_TRC_BUF_SIZE];
    int                 i;

    if (ECFM_GLB_TRC_FLAG != ECFM_TRUE)
    {
        return;
    }
    u4Flags = ECFM_TRUE;
    if (u4ContextId == ECFM_INVALID_CONTEXT)
    {
        va_start (ap, fmt);
        vsprintf (&ac1Str[0], fmt, ap);
        va_end (ap);
        UtlTrcLog (ECFM_GLB_TRC_FLAG, u4Flags, ECFM_MOD_NAME, ac1Str);
    }
    else
    {
        va_start (ap, fmt);
        i = sprintf (&ac1Str[0], "Context-%u: ", u4ContextId);
        vsprintf (&ac1Str[i], fmt, ap);
        va_end (ap);
        UtlTrcLog (ECFM_GLB_TRC_FLAG, u4Flags, ECFM_MOD_NAME, ac1Str);
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Flags);
    UNUSED_PARAM (fmt);
    return;
#endif
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilModuleStart
 *                                                                          
 *    DESCRIPTION      : This function is used to start the ECFM module.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmUtilModuleStart ()
{
    ECFM_CC_TRC_FN_ENTRY ();
    /* start ecfm module for CC task */
    if (EcfmCcModuleStart () != ECFM_SUCCESS)
    {
        EcfmCcModuleShutDown ();
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleStart: ECFM module start for CC task"
                     " failed \r\n");
        return ECFM_FAILURE;
    }
    /* start ecfm module for LBLT task */
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleStart: Corresponding context info not"
                     " present in LBLT task \r\n");
        ECFM_LBLT_UNLOCK ();
        EcfmCcModuleShutDown ();
        return ECFM_FAILURE;
    }
    if (EcfmLbLtModuleStart () != ECFM_SUCCESS)
    {
        EcfmLbLtModuleShutDown ();
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleStart: ECFM module start for LBLT task"
                     " failed \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        EcfmCcModuleShutDown ();
        return ECFM_FAILURE;
    }
    /*Change module status to start */
    /* Make the System Control START */
    ECFM_SET_SYSTEM_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_START);
    /* Set Default Value to the Module Status */
    ECFM_SET_MODULE_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_DISABLE);
    /* Set Default(Disable) Value to the Y.1731 Status */
    ECFM_SET_Y1731_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_DISABLE);

    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilModuleShutDown
 *                                                                          
 *    DESCRIPTION      : This function is used to shutdown the ECFM module.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmUtilModuleShutDown ()
{
    ECFM_CC_TRC_FN_ENTRY ();
    /* Disable ecfm module */
    EcfmUtilModuleDisable ();
    /* shutdown ecfm module for CC task */
    EcfmCcModuleShutDown ();
    ECFM_LBLT_LOCK ();
    /* shutdown ecfm module for LBLT task */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleShutdown: Corresponding context info not"
                     " present in LBLT task \r\n");
    }
    else
    {
        EcfmLbLtModuleShutDown ();
    }
    /* Make the System Control SHUTDOWN */
    ECFM_SET_SYSTEM_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_SHUTDOWN);
    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilModuleEnable
 *                                                                          
 *    DESCRIPTION      : This function is used to enable the ECFM module 
 *                       funtionality.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmUtilModuleEnable ()
{
    ECFM_CC_TRC_FN_ENTRY ();
    /* enable ecfm module for CC task */
    EcfmCcModuleEnable ();
    /* enable ecfm module for LBLT task */
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleEnable: Corresponding context info not"
                     " present in LBLT task \r\n");
        ECFM_LBLT_UNLOCK ();
        EcfmCcModuleDisable ();
        return ECFM_FAILURE;
    }
    EcfmLbLtModuleEnable ();
    /* Change module status to enabled */
    ECFM_SET_MODULE_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_ENABLE);
    /* Change Y.1731 status to disabled */
    ECFM_SET_Y1731_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_DISABLE);

    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilModuleDisable
 *                                                                          
 *    DESCRIPTION      : This function is used to disable the ECFM module
 *                       funtionality.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmUtilModuleDisable ()
{
    ECFM_CC_TRC_FN_ENTRY ();
    /* Disable Y.1731 */
    EcfmUtilY1731Disable ();
    /* disable ecfm module for CC task */
    EcfmCcModuleDisable ();
    ECFM_LBLT_LOCK ();
    /* disable ecfm module for LBLT task */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleDisable: Corresponding context info not"
                     " present in LBLT task \r\n");
    }
    else
    {
        EcfmLbLtModuleDisable ();
    }
    /* Make the module status as disabled */
    ECFM_SET_MODULE_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_DISABLE);
    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilY1731Enable
 *                                                                          
 *    DESCRIPTION      : This function is used to enable the Y.1731 specific
 *                       funtionality.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmUtilY1731Enable ()
{
    INT4                i4RetVal = ECFM_FAILURE;
    ECFM_CC_TRC_FN_ENTRY ();
    do
    {
        /* Enable Y.1731 for CC task */
        EcfmCcY1731Enable ();
        /* Enable Y.1731 for LBLT task */
        ECFM_LBLT_LOCK ();
        if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                         ECFM_MGMT_TRC,
                         "EcfmUtilY1731Enable: Corresponding context info not"
                         " present in LBLT task \r\n");
            ECFM_LBLT_UNLOCK ();
            break;
        }
        EcfmLbLtY1731Enable ();
        /* Set Y.1731 Status to enabled */
        ECFM_SET_Y1731_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_ENABLE);
        /* Release the LBLT's task context and lock */
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);
    /* Execution thread will reach this place when we will have some failure
     * starting Y.1731 module, Disable Y.1731 module
     */
    if (i4RetVal != ECFM_SUCCESS)
    {
        EcfmUtilY1731Disable ();
    }
    ECFM_CC_TRC_FN_EXIT ();
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmUtilY1731Disable
 *                                                                          
 *    DESCRIPTION      : This function is used to disable the Y.1731 specific
 *                       funtionality.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmUtilY1731Disable ()
{
    ECFM_CC_TRC_FN_ENTRY ();
    /* Disable Y.1731 for CC task */
    EcfmCcY1731Disable ();
    ECFM_LBLT_LOCK ();
    /* Disable Y.1731 for LBLT task */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilY1731Disable: Corresponding context info not"
                     " present in LBLT task \r\n");
    }
    else
    {
        EcfmLbLtY1731Disable ();
    }
    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    /* Set Y.1731 Status to disabled */
    ECFM_SET_Y1731_STATUS (ECFM_CC_CURR_CONTEXT_ID (), ECFM_DISABLE);
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmUtilY1731EnableForAPort
 *
 *    DESCRIPTION      : This function is called to enable Y1731 module at 
 *                       particular port.
 *
 *    INPUT            : pPortInfoNode -> Pointer to portInfo
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmUtilY1731EnableForAPort (tEcfmCcPortInfo * pPortInfoNode)
{

    INT4                i4RetVal = ECFM_FAILURE;
    ECFM_CC_TRC_FN_ENTRY ();
    do
    {
        /* Enable Y.1731 for CC task */
        EcfmCcY1731EnableForAPort (pPortInfoNode->u2PortNum);
        ECFM_LBLT_LOCK ();
        /* Enable Y.1731 for LBLT task */
        if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                         ECFM_MGMT_TRC,
                         "EcfmY1731ModuleEnableForAPort: Corresponding context info not"
                         " present in LBLT task \r\n");

            ECFM_LBLT_UNLOCK ();
            break;
        }
        else
        {
            EcfmLbLtY1731EnableForAPort (pPortInfoNode->u2PortNum);
        }
        /* Release the LBLT's task context and lock */
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        ECFM_CC_TRC_FN_EXIT ();
        i4RetVal = ECFM_SUCCESS;
    }
    while (0);

    /* Execution thread will reach this place when we will have some failure
     * starting Y.1731 module on a port. Disable Y1731 on this port.
     */
    if (i4RetVal != ECFM_SUCCESS)
    {
        EcfmUtilY1731DisableForAPort (pPortInfoNode);
    }

    ECFM_CC_TRC_FN_EXIT ();
    return i4RetVal;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmUtilY1731DisableForAPort
 *
 *    DESCRIPTION      : This function is called to disable Y1731 module at 
 *                       particular port.
 *
 *    INPUT            : pPortInfoNode - Pointer to portInfo. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmUtilY1731DisableForAPort (tEcfmCcPortInfo * pPortInfoNode)
{
    ECFM_CC_TRC_FN_ENTRY ();
    /* Disable Y.1731 for CC task */
    EcfmCcY1731DisableForAPort (pPortInfoNode->u2PortNum);

    ECFM_LBLT_LOCK ();
    /* Disable Y.1731 for LBLT task */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmY1731ModuleDisableForAPort: Corresponding context info not"
                     " present in LBLT task \r\n");
    }
    else
    {
        EcfmLbLtY1731DisableForAPort (pPortInfoNode->u2PortNum);
    }
    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmUtilModuleEnableForAPort
 *
 *    DESCRIPTION      : This function is called to enable Ecfm module at 
 *                       particular port.
 *
 *    INPUT            : pPortInfoNode -> Pointer to portInfo
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmUtilModuleEnableForAPort (tEcfmCcPortInfo * pPortInfoNode)
{

    ECFM_CC_TRC_FN_ENTRY ();

    /* Enable Ecfm Module for CC task */
    EcfmCcModuleEnableForAPort (pPortInfoNode->u2PortNum);

    ECFM_LBLT_LOCK ();
    /* Enable Ecfm for LBLT task */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleEnableForAPort: Corresponding context info not"
                     " present in LBLT task \r\n");

        ECFM_LBLT_UNLOCK ();
        EcfmCcModuleDisableForAPort (pPortInfoNode->u2PortNum);
        return ECFM_FAILURE;
    }
    else
    {
        /* Reset ECFM Module for LBLT Task */
        EcfmLbLtModuleEnableForAPort (pPortInfoNode->u2PortNum);
    }

    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmUtilModuleDisableForAPort
 *
 *    DESCRIPTION      : This function is called to enable Ecfm module at 
 *                       particular port.
 *
 *    INPUT            : pPortInfoNode -> Pointer to portInfo
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmUtilModuleDisableForAPort (tEcfmCcPortInfo * pPortInfoNode)
{

    ECFM_CC_TRC_FN_ENTRY ();

    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPortInfoNode->u2PortNum))
    {
        EcfmUtilY1731DisableForAPort (pPortInfoNode);
    }

    /* Reset ECFM Module for CC Task */
    EcfmCcModuleDisableForAPort (pPortInfoNode->u2PortNum);

    ECFM_LBLT_LOCK ();
    /* Enable Ecfm for LBLT task */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_MGMT_TRC,
                     "EcfmUtilModuleDisableForAPort: Corresponding context info not"
                     " present in LBLT task \r\n");
    }
    else
    {
        /* Reset ECFM Module for LBLT Task */
        EcfmLbLtModuleDisableForAPort (pPortInfoNode->u2PortNum);
    }

    /* Release the LBLT's task context and lock */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmFormatPduHdr
 *
 * Description        : This rotuine is used to fill the  CFM PDU Header.
 *
 * Input              : pMsg - Pointer to the Messgae structure
 *                      opcode- Opcode of External API
 * 
 * Output(s)          : ppu1Pdu - Pointer to Pointer to the PDU.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmFormatPduHdr (tExternalPduMsg * pMsg, UINT1 **ppu1Pdu, UINT1 opcode)
{
    UINT1              *pu1Pdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pu1Pdu = *ppu1Pdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */

    /* If this is a RAPS packet, then fill the 
     * version given by ERPS.
     */
    if (opcode == ECFM_OPCODE_RAPS)
    {
        ECFM_SET_MDLEVEL (u1LevelVer, pMsg->u1MdLevel);
        u1LevelVer = (u1LevelVer | ((pMsg->u1Version) & 0x1F));
    }
    else
    {
        ECFM_SET_MDLEVEL (u1LevelVer, pMsg->u1MdLevel);
        ECFM_SET_VERSION (u1LevelVer);
    }

    ECFM_PUT_1BYTE (pu1Pdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Pdu, opcode);

    /* Fill the Next 1 byte with Flag In the PDU, */
    ECFM_PUT_1BYTE (pu1Pdu, pMsg->u1PduFlag);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Pdu, pMsg->u1TlvOffset);

    *ppu1Pdu = pu1Pdu;

    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFormatEthHdr
 *
 * Description        : This routine is used to fill the Ethernet and LLC
 *                      Header in the PDU frame.
 *
 * Input              : pMsg - Pointer to the PDU info
 *                      u2Length - Length of the CFM header formated so far
 *                      filled
 *
 * Output(s)          : ppu1Pdu - Pointer to Pointer to the Pdu.
 *
 * Return Value(s)    : None.
 *****************************************************************************/
PRIVATE VOID
EcfmFormatEthHdr (tExternalPduMsg * pMsg, UINT1 **ppu1Pdu, UINT2 u2Length)
{
    UINT1              *pu1Pdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    pu1Pdu = *ppu1Pdu;

    /* Fill in the Ethernet header fields */
    ECFM_MEMCPY (pu1Pdu, pMsg->TxDestMacAddr, ECFM_MAC_ADDR_LENGTH);

    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 6byte with source address as the address of the
     * Sending MEP
     */
    if (ECFM_LBLT_GET_PORT_INFO (pMsg->u2PortNum) == NULL)
    {
        return;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO (pMsg->u2PortNum)->
                               u4IfIndex, pu1Pdu);

    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 2 byte as Length of the CFM PDU if the LLC encap has to be
     * added else fill in as the CFM type */
    if ((ECFM_LBLT_PORT_INFO (pMsg->u2PortNum)->b1LLCEncapStatus) == ECFM_TRUE)
    {
        /* Filling LLC Snap header */
        ECFM_LBLT_SET_LLC_SNAP_HDR (pu1Pdu,
                                    (u2Length + ECFM_LLC_SNAP_HDR_SIZE));
    }
    else
    {
        /* Fill in the next 2 byte as the CFM type */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_PDU_TYPE_CFM);
    }
    *ppu1Pdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmNotifyProtocols                                  *
 *                                                                           *
 * Description        : Routine used to notify various applications of the   *
 *                      events for which they have registered                *
 *                                                                           *
 * Input(s)           : pEcfmEventNotification - Pointer to the information  *
 *                      which will be passed to the application              *
 *                      u1TaskId  - CC Task or LBLT Task                     *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
EcfmNotifyProtocols (tEcfmEventNotification * pEcfmEventNotification, UINT1
                     u1TaskId)
{
    tEcfmBufChainHeader *pDupBuf = NULL;
    tEcfmBufChainHeader *pTempBuf = NULL;
    tEcfmCcMepInfo     *pMepCcNode = NULL;
    tEcfmLbLtMepInfo   *pMepLbLtNode = NULL;
    tEcfmCcVlanInfo    *pVlanInfo = NULL;
    tEcfmCcVlanInfo     VlanInfo;
    UINT4               u4Event = ECFM_INIT_VAL;
    UINT4               u4EntId = ECFM_INIT_VAL;
    UINT4               u4PrimaryVid = ECFM_INIT_VAL;
    UINT4               u4ProcessEvt = ECFM_INIT_VAL;

    if (pEcfmEventNotification == NULL)
    {
        return;
    }

    MEMSET (&VlanInfo, 0, sizeof (tEcfmCcVlanInfo));

    pTempBuf = pEcfmEventNotification->PduInfo.pu1Data;

    /* Get the event ID for which the function is called.
     * Based on that the notification will be sent to the 
     * registered applications.
     */
    u4Event = pEcfmEventNotification->u4Event;
    if (u1TaskId == ECFM_CC_TASK_ID)
    {
        pMepCcNode =
            EcfmCcUtilGetMepEntryFrmGlob (pEcfmEventNotification->u4MdIndex,
                                          pEcfmEventNotification->u4MaIndex,
                                          pEcfmEventNotification->u2MepId);
        if (pMepCcNode == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pTempBuf, FALSE);
            return;
        }

        for (u4EntId = ECFM_RESERVE_APP_ID; u4EntId < ECFM_MAX_APPS; u4EntId++)
        {
            /* Check for the Module id registered with MEP or Context and 
             * indicate the module once, even if a module registers twice 
             * with ECFM, such as 1. For MEP and 2. For a Context, the 
             * indication is only given once to module.
             */
            if ((ECFM_GET_U4BIT (pMepCcNode->u4ModId, u4EntId) == 0) &&
                (ECFM_GET_U4BIT (ECFM_CC_REG_MOD_BMP (), u4EntId) == 0))
            {
                continue;
            }

            EcfmUtilIndicateProtocol (pEcfmEventNotification, u4EntId, u4Event);
            /* For ERPS, this function is always invoked with the primary
             * VID for the MA.
             * */
            u4PrimaryVid = pEcfmEventNotification->u4VlanIdIsid;

            VlanInfo.u4PrimaryVidIsid = u4PrimaryVid;
            VlanInfo.u4VidIsid = 0;

            if (ECFM_CC_GET_APP_INFO (u4EntId)->u4VlanInd !=
                ECFM_INDICATION_FOR_ALL_VLANS)
            {
                if (pTempBuf != NULL)
                {
                    CRU_BUF_Release_MsgBufChain (pTempBuf, FALSE);
                }
                return;
            }
            /* In case of ERPS, the indication needs to be provided as
             * part of all the vlans that are mapped to this particular
             * primary VLAN. i.e. scan the Primary VLAN table and provide
             * indication for each and every secondary vlan present in it
             * */
            pVlanInfo = (tEcfmCcVlanInfo *) RBTreeGetNext
                (ECFM_CC_PRIMARY_VLAN_TABLE, (tRBElem *) & VlanInfo, NULL);

            while (pVlanInfo != NULL)
            {
                if (pVlanInfo->u4PrimaryVidIsid == u4PrimaryVid)
                {
                    pEcfmEventNotification->u4VlanIdIsid = pVlanInfo->u4VidIsid;

                    EcfmUtilIndicateProtocol (pEcfmEventNotification,
                                              u4EntId, u4Event);

                }
                /* This notification is being done in CC thread. CC thread
                 * handles more granular timers and other events for PDU
                 * RX and TX. Hence relinquish this loop and process another
                 * event before fully finishing this loop, if it takes more
                 * time. Also, it is enough if we process the most 
                 * important events like PDU reception.
                 * */
                u4ProcessEvt = ECFM_EV_INT_PDU_IN_QUEUE |
                    ECFM_EV_CFM_PDU_IN_QUEUE | ECFM_EV_CFG_MSG_IN_QUEUE;

                EcfmCcUtilRelinquishLoop (u4ProcessEvt);

                pVlanInfo = (tEcfmCcVlanInfo *) RBTreeGetNext
                    (ECFM_CC_PRIMARY_VLAN_TABLE, (tRBElem *) pVlanInfo, NULL);
            }
        }
    }
    else
    {
        pMepLbLtNode =
            EcfmLbLtUtilGetMepEntryFrmGlob (pEcfmEventNotification->u4MdIndex,
                                            pEcfmEventNotification->u4MaIndex,
                                            pEcfmEventNotification->u2MepId);
        if (pMepLbLtNode != NULL)
        {
            for (u4EntId = ECFM_RESERVE_APP_ID; u4EntId < ECFM_MAX_APPS;
                 u4EntId++)
            {
                /* Check for the Module id registered with MEP or Context and 
                 * indicate the module once, even if a module registers twice 
                 * with ECFM, such as 1. For MEP and 2. For a Context, the 
                 * indication is only given once to module.
                 */
                if ((ECFM_GET_U4BIT (pMepLbLtNode->u4ModId, u4EntId) != 0) ||
                    (ECFM_GET_U4BIT (ECFM_LBLT_REG_MOD_BMP (), u4EntId) != 0))
                {
                    if ((ECFM_LBLT_GET_APP_INFO (u4EntId) != NULL) &&
                        (ECFM_LBLT_GET_APP_INFO (u4EntId)->pFnRcvPkt != NULL) &&
                        ((ECFM_LBLT_GET_APP_INFO (u4EntId)->
                          u4EventsId & u4Event) != ECFM_INIT_VAL))
                    {
                        if (pEcfmEventNotification->PduInfo.pu1Data != NULL)
                        {
                            pDupBuf = ECFM_DUPLICATE_CRU_BUF
                                (pEcfmEventNotification->PduInfo.pu1Data);

                            if (pDupBuf == NULL)
                            {
                                /* Duplicating CRU Buffer failed, hence 
                                 * cannot indicate this module, so continue 
                                 * with the indication to next module 
                                 */
                                continue;
                            }
                            else
                            {
                                pEcfmEventNotification->PduInfo.pu1Data =
                                    pDupBuf;
                            }

                        }
                        (*(ECFM_LBLT_GET_APP_INFO (u4EntId)->pFnRcvPkt))
                            (pEcfmEventNotification);
                    }
                }
            }
        }
    }

    if (pTempBuf != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pTempBuf, FALSE);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmGetRmepMacAddr
 *
 *    DESCRIPTION      : Routine used to get the MAC Address of the remote MEP
 *                       from the Remote MEP CCM Database
 *
 *    INPUT            : u4MdIndex - MD Index for the RMEP Info
 *                       u4MaIndex - MA Index for the RMEP Info
 *                       u2MepId   - MEP ID corresponding to which the RMEP info
 *                                   is required.
 *                       u2TargetRMepId  - Mep ID of the remote MEP whose
 *                                         MAC Address is required
 *
 *    OUTPUT           : RmepMacAddr
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmGetRmepMacAddr (UINT4 u4MdIndex, UINT4 u4MaIndex,
                    UINT2 u2MepId, UINT2 u2TargetRMepId, UINT1 *pu1MacAddr)
{
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    tEcfmCcMepInfo     *pCcMepInfo = NULL;
    tEcfmMacAddr        NullMacAddr = { ECFM_INIT_VAL };

    /* Get corresponding MepNode in CCTasks MepInfo */
    pCcMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pCcMepInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    /* Get Corresponding Remote MEP Info for the found CC Tasks
     * MepInfo */
    pRMepInfo = EcfmUtilGetRmepInfo (pCcMepInfo, u2TargetRMepId);
    if (pRMepInfo == NULL)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_MEMCMP (NullMacAddr,
                     pRMepInfo->RMepMacAddr, ECFM_MAC_ADDR_LENGTH) == 0)
    {
        return ECFM_FAILURE;
    }

    ECFM_MEMCPY (pu1MacAddr, pRMepInfo->RMepMacAddr, ECFM_MAC_ADDR_LENGTH);

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmXmitExPdu
 *
 * Description        : This routine formats and transmits the External PDUs.
 *
 * Input(s)           : pMsg- Pointer to the structure that stores the
 *                      information regarding External pdu.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntXmitExPdu (tExternalPduMsg * pMsg, UINT1 u1OpCode)
{
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1ExPduStart = NULL;
    UINT1              *pu1ExPduEnd = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT2               u2PduLength = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    /* Allocate CRU Buffer */
    pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_PDU_SIZE, ECFM_INIT_VAL);
    if (pBuf == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmXmitExPdu: " "Buffer allocation failed\r\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);

    pu1ExPduStart = ECFM_LBLT_PDU;
    pu1ExPduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;
    /* Format the  External PDU header */
    EcfmFormatPduHdr (pMsg, &pu1ExPduEnd, u1OpCode);
    if ((u1OpCode != ECFM_OPCODE_APS) && (u1OpCode != ECFM_OPCODE_RAPS))
    {
        ECFM_LBLT_SET_OUI (pu1ExPduEnd);
        /* Fill  1 byte with SubOpCode */
        ECFM_PUT_1BYTE (pu1ExPduEnd, pMsg->u1SubOpCode);
    }
    ECFM_MEMCPY (pu1ExPduEnd, pMsg->pu1Data, pMsg->u4DataLen);
    pu1ExPduEnd = pu1ExPduEnd + pMsg->u4DataLen;
    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1ExPduEnd, ECFM_END_TLV_TYPE);
    u2PduLength = (UINT2) (pu1ExPduEnd - pu1ExPduStart);

    /* Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1ExPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmXmitExPdu: " "Buffer copy operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    pu1ExPduStart = pu1EthLlcHdr;
    EcfmFormatEthHdr (pMsg, &pu1EthLlcHdr, u2PduLength);
    /* Prepend the Ethernet and the LLC header in the
     * CRU buffer */
    u2PduLength = (UINT2) (pu1EthLlcHdr - pu1ExPduStart);

    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1ExPduStart,
                              (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmXmitExPdu: " "Buffer prepend operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmXmitExPdu: "
                        "Sending out External PDU to lower layer...\r\n");

    if ((pTempLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (pMsg->u2PortNum)) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmXmitExPdu: " "No Port info available\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    if (EcfmLbLtCtrlTxTransmitPkt (pBuf, pMsg->u2PortNum, pMsg->u4VidIsid,
                                   ECFM_DEF_VLAN_PRIORITY, ECFM_TRUE,
                                   pMsg->u1Direction, u1OpCode, NULL,
                                   NULL) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmXmitExPdu: "
                       "PDU transmission to the lower layer failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        ECFM_LBLT_INCR_TX_FAILED_COUNT (pMsg->u2PortNum);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pTempLbLtPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmUtilSetSenderIdTlv
 * Description        : This routine is used to set sender-id in the PDU
 *                        
 * Input(s)           : u1SenderIdPermission - Sender Id Permission.
 * Output(s)          : ppu1Pdu   - Pointer to pointer to the PDU where
 *                      sender-id needs to be placed.
 * Return Value(s)    : NONE
 ******************************************************************************/
PUBLIC VOID
EcfmUtilSetSenderIdTlv (UINT1 u1SenderIdPermission, UINT1 **ppu1Pdu)
{
    UINT1              *pu1PduStart = *ppu1Pdu;
    UINT1              *pu1PduEnd = *ppu1Pdu;
    UINT1              *pu1TlvLen = NULL;
    UINT1              *pu1PduLenStart = NULL;
    UINT2               u2TlvLength = ECFM_INIT_VAL;

    ECFM_PUT_1BYTE (pu1PduEnd, ECFM_SENDER_ID_TLV_TYPE);
    /* store the pointer for the length field */
    pu1TlvLen = pu1PduEnd;
    pu1PduEnd += ECFM_TLV_LENGTH_FIELD_SIZE;
    pu1PduLenStart = pu1PduEnd;
    switch (u1SenderIdPermission)
    {
        case ECFM_SENDER_ID_CHASSIS:
            if (ECFM_CHASSISID_LEN == 0)
            {
                pu1PduEnd = pu1PduLenStart;
                break;
            }
            /* set chassis-d */
            ECFM_SET_CHASSIS_ID (pu1PduEnd);
            /* set 0 for management address domain */
            ECFM_PUT_1BYTE (pu1PduEnd, 0);
            break;
        case ECFM_SENDER_ID_MANAGE:
            if ((ECFM_MGMT_ADDRESS_LEN == 0) ||
                (ECFM_MGMT_ADDR_DOMAIN_CODE == 0))
            {
                pu1PduEnd = pu1PduLenStart;
                break;
            }
            /* set 0 chassis id length */
            ECFM_PUT_1BYTE (pu1PduEnd, 0);
            /* set management address domain */
            ECFM_SET_MGMT_ADDR_DOMAIN (pu1PduEnd);
            /* set management address */
            ECFM_SET_MGMT_ADDRESS (pu1PduEnd);
            break;
        case ECFM_SENDER_ID_CHASSID_MANAGE:
            if ((ECFM_CHASSISID_LEN == 0) ||
                (ECFM_MGMT_ADDRESS_LEN == 0) ||
                (ECFM_MGMT_ADDR_DOMAIN_CODE == 0))
            {
                pu1PduEnd = pu1PduLenStart;
                break;
            }
            /* set chassis-d */
            ECFM_SET_CHASSIS_ID (pu1PduEnd);
            /* set management address domain */
            ECFM_SET_MGMT_ADDR_DOMAIN (pu1PduEnd);
            /* set management address */
            ECFM_SET_MGMT_ADDRESS (pu1PduEnd);
            break;
        case ECFM_SENDER_ID_DEFER:
        case ECFM_SENDER_ID_NONE:
        default:
            pu1PduEnd = pu1PduLenStart;
            break;
    }
    u2TlvLength = (UINT2) (pu1PduEnd - pu1PduLenStart);
    if (u2TlvLength != 0)
    {
        ECFM_PUT_2BYTE (pu1TlvLen, u2TlvLength);
    }
    else
    {
        /* revert back and don't add sender-id tlv */
        pu1PduEnd = pu1PduStart;
    }
    *ppu1Pdu = pu1PduEnd;
    return;
}

/*******************************************************************************
 * Function Name      : EcfmUtilDecodeAsn1BER
 *
 * Description        : This routine is used decode the ASN1 encoded OID to
 *                      UINT4 array.
 *                        
 * Input(s)           : pu1EncodedOid - ASN1 BER encoded OID
 *                      u4EncodedOidLen - ASN1 BER encoded OID length
 *                      u4OidSize - OID size
 * Output(s)          : pu4Oid - resultant OID
 *                      pu4OidLen - resultant OID length
 *
 * Return Value(s)    : NONE
 ******************************************************************************/
VOID
EcfmUtilDecodeAsn1BER (UINT1 *pu1EncodedOid, UINT4 u4EncodedOidLen,
                       UINT4 u4OidSize, UINT4 *pu4Oid, UINT4 *pu4OidLen)
{
    UINT4               u4Oid = 0;
    UINT4               u4Counter = u4EncodedOidLen;
    UINT4               u4OidLen = 0;
    BOOL1               b1First = ECFM_FALSE;
    for (u4OidLen = 0; u4Counter-- > 0; pu1EncodedOid++)
    {
        u4Oid =
            (u4Oid << ECFM_SHIFT_7BITS) +
            (*pu1EncodedOid & ~ECFM_MASK_WITH_VAL_128);
        if (*pu1EncodedOid & ECFM_MASK_WITH_VAL_128)
        {
            continue;
        }
        if (b1First == ECFM_FALSE)
        {
            b1First = ECFM_TRUE;
            if (u4OidLen < u4OidSize)
            {
                pu4Oid[u4OidLen] = u4Oid / ECFM_VAL_40;
                if (pu4Oid[u4OidLen] > ECFM_VAL_2)
                {
                    pu4Oid[u4OidLen] = ECFM_VAL_2;
                }
            }
            u4Oid -= pu4Oid[u4OidLen] * ECFM_VAL_40;
            if (u4OidLen < u4OidSize)
            {
                (u4OidLen)++;
            }
        }
        if (u4OidLen < u4OidSize)
        {
            pu4Oid[(u4OidLen)++] = u4Oid;
        }
        u4Oid = 0;
    }
    *pu4OidLen = u4OidLen;
    return;
}

/*******************************************************************************
 * Function Name      : EcfmUtilEncodeAsn1BER
 *
 * Description        : This routine is used convert a OID into ASN1 BER OID
 *                      format.
 *                        
 * Input(s)           : pu4Oid - OID to be converted
 *                      u4OidLen - OID length to be converted
 * Output(s)          : pu1EncodedOid - resultant encoded OID
 *                      pu4EncodedOidLen - resultant encoded OID length
 *
 * Return Value(s)    : NONE
 ******************************************************************************/
VOID
EcfmUtilEncodeAsn1BER (UINT4 *pu4Oid, UINT4 u4OidLen,
                       UINT1 *pu1EncodedOid, UINT1 *pu1EncodedOidLen)
{
    UINT4               au4Masks[ECFM_ARRAY_SIZE_5] = {
        ECFM_MASK_WITH_VAL_4026531840,
        ECFM_MASK_WITH_VAL_266338304,
        ECFM_MASK_WITH_VAL_2043904,
        ECFM_MASK_WITH_VAL_16256,
        ECFM_MASK_WITH_VAL_127
    };
    UINT4               u4Octect = 0;
    UINT4               u4Oid = 0;
    UINT1               u1EncodedOidLen = 0;
    UINT1               au1Shifts[ECFM_ARRAY_SIZE_5] = {
        ECFM_VAL_28,
        ECFM_VAL_21,
        ECFM_VAL_14,
        ECFM_VAL_7,
        ECFM_VAL_0
    };                            /* shift registers */
    UINT1               u4Index = 0;
    UINT1               u1Counter;
    /* fill the OID indentier */
    pu1EncodedOid[u1EncodedOidLen++] = ECFM_VAL_18;
    /* fill the OID length */
    pu1EncodedOid[u1EncodedOidLen++] = ECFM_MAX_TRANSPORT_DOMAIN_LEN;
    while (u4Index < u4OidLen)
    {
        if (u4Index == 0)
        {
            u4Oid = pu4Oid[0] * ECFM_VAL_40;
            u4Oid = u4Oid + pu4Oid[1];
            u4Index++;
        }
        else
        {
            u4Oid = pu4Oid[u4Index];
        }
        u1Counter = 0;
        while (u1Counter < ECFM_VAL_5)
        {
            u4Octect = u4Oid & au4Masks[u1Counter];
            u4Octect = u4Octect >> au1Shifts[u1Counter];
            if (u4Octect != 0)
            {
                switch (u1Counter)
                {
                    case ECFM_VAL_0:
                        pu1EncodedOid[u1EncodedOidLen++] =
                            (UINT1) (u4Octect | ECFM_MASK_WITH_VAL_128);
                        u1Counter++;
                        break;
                    case ECFM_VAL_1:
                        u4Octect = u4Oid & au4Masks[u1Counter];
                        u4Octect = u4Octect >> au1Shifts[u1Counter];
                        pu1EncodedOid[u1EncodedOidLen++] =
                            (UINT1) (u4Octect | ECFM_MASK_WITH_VAL_128);
                        u1Counter++;
                        break;
                    case ECFM_VAL_2:
                        u4Octect = u4Oid & au4Masks[u1Counter];
                        u4Octect = u4Octect >> au1Shifts[u1Counter];
                        pu1EncodedOid[u1EncodedOidLen++] =
                            (UINT1) (u4Octect | ECFM_MASK_WITH_VAL_128);
                        u1Counter++;
                        break;
                    case ECFM_VAL_3:
                        u4Octect = u4Oid & au4Masks[u1Counter];
                        u4Octect = u4Octect >> au1Shifts[u1Counter];
                        pu1EncodedOid[u1EncodedOidLen++] =
                            (UINT1) (u4Octect | ECFM_MASK_WITH_VAL_128);
                        u1Counter++;
                        break;
                    case ECFM_VAL_4:
                        u4Octect = u4Oid & au4Masks[u1Counter];
                        u4Octect = u4Octect >> au1Shifts[u1Counter];
                        pu1EncodedOid[u1EncodedOidLen++] = (UINT1) u4Octect;
                        break;
                }
                break;
            }
            u1Counter++;
        }
        u4Index++;
    }
    *pu1EncodedOidLen = u1EncodedOidLen;
    return;
}

/*******************************************************************************
 * Function Name      : EcfmUtilPrepareNotification 
 *
 * Description        : This function prepares the format after which 
 *                      notifications are sent to the registered 
 *                      applications.
 *                        
 * Input(s)           : u4EventId - Event Id
 *                      u2PortNo - Port Number
 *                      u2Vlan - Vlan Id
 *                      u1Dir - Direction of MEP
 *                      MacAddr - Peer Mac address
 *                      u1RxMdLevel - Peer MD level
 *                      u4Var - Variable Info
 *                      u1Flag - Flags value
 *                      u1Offset - Offset
 *                      pu1DataPtr - Data Pointer to the recieved PDU
 *                      u4Datalength - Datalength
 *                      pu1OuiPtr - Pointer to OUI
 *                      u1SubOpcodeVal - Sub OpCode Value
 * Output(s)          : 
 *
 * Return Value(s)    : NONE
 ******************************************************************************/
PUBLIC VOID
EcfmUtilPrepareNotification (UINT4 u4EventId, tEcfmMepInfoParams * pMepInfo,
                             tMacAddr * pMacAddr, UINT4 u4Var, UINT1 u1Flag,
                             UINT1 u1Offset, tEcfmBufChainHeader * pu1DataPtr,
                             UINT1 u1CfmPduOffset, UINT1 *pu1OuiPtr,
                             UINT1 u1SubOpcodeVal, UINT1 u1TaskId)
{
    tEcfmEventNotification EcfmEventNotification;

    if (ECFM_NODE_STATUS () == ECFM_NODE_STANDBY)
    {
        return;
    }

    ECFM_MEMSET (&EcfmEventNotification, ECFM_INIT_VAL,
                 sizeof (tEcfmEventNotification));
    EcfmEventNotification.u4Event = u4EventId;
    if (pMacAddr != NULL)
    {
        ECFM_MEMCPY (EcfmEventNotification.SrcMacAddr,
                     *pMacAddr, ECFM_MAC_ADDR_LENGTH);
    }
    else
    {
        ECFM_MEMSET (EcfmEventNotification.SrcMacAddr,
                     ECFM_INIT_VAL, sizeof (tMacAddr));
    }
    EcfmEventNotification.u4VarInfo = u4Var;
    EcfmEventNotification.u4ContextId = pMepInfo->u4ContextId;
    ECFM_GET_TIMESTAMP (&(EcfmEventNotification.TimeStamp));
    EcfmEventNotification.PduInfo.u1Flags = u1Flag;
    EcfmEventNotification.PduInfo.u1TlvOffset = u1Offset;
    EcfmEventNotification.PduInfo.pu1Data = pu1DataPtr;
    EcfmEventNotification.PduInfo.u1CfmPduOffset = u1CfmPduOffset;
    EcfmEventNotification.PduInfo.pu1Oui = pu1OuiPtr;
    EcfmEventNotification.PduInfo.u1SubOpCode = u1SubOpcodeVal;

    EcfmEventNotification.u4IfIndex = pMepInfo->u4IfIndex;
    EcfmEventNotification.u4VlanIdIsid = pMepInfo->u4VlanIdIsid;
    EcfmEventNotification.u1MdLevel = pMepInfo->u1MdLevel;
    EcfmEventNotification.u1Direction = pMepInfo->u1Direction;

    EcfmEventNotification.u4MdIndex = pMepInfo->u4MdIndex;
    EcfmEventNotification.u4MaIndex = pMepInfo->u4MaIndex;
    EcfmEventNotification.u2MepId = pMepInfo->u2MepId;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
    EcfmEventNotification.u4SlaId = pMepInfo->u4SlaId;
#endif
    EcfmNotifyProtocols (&EcfmEventNotification, u1TaskId);

    return;
}

/****************************************************************************
 * Function Name      : EcfmFormatCcTaskPduEthHdr
 *
 * Description        : This routine is used to fill the Ethernet and LLC
 *                      Header in the PDU frame generated on CC Task.
 *
 * Input              : pMepInfo - Pointer to the MEP info
 *                      u2Length - Length of the CFM header formated so far
 *                      filled
 *                      Opcode - Opcode of the PDU to be transmitted
 *
 * Output(s)          : ppu1Pdu - Pointer to Pointer to the Pdu.
 *
 * Return Value(s)    : None.
 *****************************************************************************/
PUBLIC VOID
EcfmFormatCcTaskPduEthHdr (tEcfmCcMepInfo * pMepInfo, UINT1 **ppu1Pdu,
                           UINT2 u2Length, UINT1 u1Opcode)
{
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmCcLckInfo     *pLckInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcMdInfo      *pMdInfo = NULL;
    tMacAddr            MacAddr = { ECFM_INIT_VAL };
    tMacAddr            NullMacAddr = { ECFM_INIT_VAL };
    UINT1              *pu1Pdu = NULL;
    ECFM_CC_TRC_FN_ENTRY ();

    pu1Pdu = *ppu1Pdu;

    switch (u1Opcode)
    {
        case ECFM_OPCODE_CCM:

            pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
            UNUSED_PARAM (pMaInfo);
            pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

            /* Y.1731: 
             * Fill in the 6byte of Destination Address 
             * Destination Address is unicast if CC Role is Performance Monitoring 
             * and Y.1731 is enabled otherwise DA is multicast
             */
            ECFM_MEMSET (NullMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

            if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
                (ECFM_MEMCMP (pCcInfo->UnicastCcmMacAddr, NullMacAddr,
                              ECFM_MAC_ADDR_LENGTH) != ECFM_SUCCESS))
            {
                ECFM_MEMCPY (MacAddr, pCcInfo->UnicastCcmMacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            else
            {
                ECFM_GEN_MULTICAST_CLASS1_ADDR (MacAddr, pMepInfo->u1MdLevel);
            }

            /*Finally copying the Mac address to the pdu */
            ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);
            break;

        case ECFM_OPCODE_AIS:
            pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);
            pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);
            /* Fill in the Ethernet header fields */
            if (pAisInfo->b1AisDestIsMulticast != ECFM_TRUE)
            {
                /* Fill in the 6byte of Destination Address */
                ECFM_MEMCPY (pu1Pdu, pAisInfo->AisClientUnicastMacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            else
            {
                ECFM_MEMSET (&MacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
                ECFM_GEN_MULTICAST_CLASS1_ADDR
                    (MacAddr, (UINT1) pMdInfo->i1ClientLevel);
                ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);
            }
            break;
        case ECFM_OPCODE_LCK:
            pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);
            pLckInfo = ECFM_CC_GET_LCKINFO_FROM_MEP (pMepInfo);
            /* Fill in the Ethernet header fields */
            if (pLckInfo->b1LckDestIsMulticast != ECFM_TRUE)
            {
                /* Fill in the 6byte of Destination Address */
                ECFM_MEMCPY (pu1Pdu, pLckInfo->LckClientUnicastMacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            else
            {
                ECFM_MEMSET (&MacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
                ECFM_GEN_MULTICAST_CLASS1_ADDR
                    (MacAddr, (UINT1) pMdInfo->i1ClientLevel);
                ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);
            }
            break;
        case ECFM_OPCODE_LMM:
            pLmInfo = &(pMepInfo->LmInfo);
            /* Fill in the Ethernet header fields */
            if (pLmInfo->b1TxLmIsDestMepId != ECFM_TRUE)
            {
                /* Fill in the 6byte of Destination Address */
                ECFM_MEMCPY (pu1Pdu, pLmInfo->TxLmmDestMacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            else
            {
                ECFM_MEMSET (&MacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
                /* Get RMEP MAC Address for the Destination MEPID */
                if (EcfmGetRmepMacAddr
                    (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                     pMepInfo->u2MepId, pLmInfo->u2TxLmDestMepId,
                     MacAddr) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                                 ECFM_OS_RESOURCE_TRC,
                                 "EcfmFormatCcTaskPduEthHdr"
                                 "Rmep Mac Address not Found !! \r\n");
                    return;
                }
                ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);

                /* Store the RMEP's mac address in the LM Info */
                ECFM_MEMCPY (pLmInfo->TxLmmDestMacAddr, MacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            break;
        default:
            break;
    }

    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 6byte with source address as the address of the
     * Sending MEP
     */
    if (ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        return;
    }
    ECFM_GET_MAC_ADDR_OF_PORT
        (ECFM_CC_PORT_INFO (pMepInfo->u2PortNum)->u4IfIndex, pu1Pdu);

    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 2 byte as Length of the CFM PDU if the LLC encap has to be
     * added else fill in as the CFM type */
    if (pMepInfo->pPortInfo->b1LLCEncapStatus == ECFM_TRUE)
    {
        /* Filling LLC Snap header */
        ECFM_CC_SET_LLC_SNAP_HDR (pu1Pdu, (u2Length + ECFM_LLC_SNAP_HDR_SIZE));
    }
    else
    {
        /* Fill in the next 2 byte as the CFM type */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_PDU_TYPE_CFM);
    }

    *ppu1Pdu = pu1Pdu;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmFormatLbLtTaskPduEthHdr
 *
 * Description        : This routine is used to fill the Ethernet and LLC
 *                      Header in the PDU frame generated on LBLT Task.
 *
 * Input              : pMepInfo - Pointer to the MEP info
 *                      u2Length - Length of the CFM header formated so far
 *                      filled
 *                      Opcode - Opcode of the PDU to be transmitted
 *
 * Output(s)          : ppu1Pdu - Pointer to Pointer to the Pdu.
 *
 * Return Value(s)    : None.
 *****************************************************************************/
PUBLIC VOID
EcfmFormatLbLtTaskPduEthHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1Pdu,
                             UINT2 u2Length, UINT1 u1Opcode)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tMacAddr            MacAddr = { ECFM_INIT_VAL };
    UINT1              *pu1Pdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    pu1Pdu = *ppu1Pdu;

    switch (u1Opcode)
    {
        case ECFM_OPCODE_LBM:
            pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
            /* Fill in the 6byte of Destination Address */
            ECFM_MEMCPY (pu1Pdu, pLbInfo->TxLbmDestMacAddr,
                         ECFM_MAC_ADDR_LENGTH);
            break;

        case ECFM_OPCODE_LTM:
            /* Get MEP Info from PduSmInfo Structure */
            ECFM_MEMSET (MacAddr, 0, sizeof (tEcfmMacAddr));
            /* Generate LTM Multicast Address */
            ECFM_GEN_MULTICAST_CLASS2_ADDR (MacAddr, pMepInfo->u1MdLevel);
            /*Finally copying the Mac address to the pdu */
            ECFM_MEMCPY (pu1Pdu, MacAddr, ECFM_MAC_ADDR_LENGTH);
            break;

        case ECFM_OPCODE_TST:
            pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pMepInfo);
            /* Fill in the 6byte of Destination Address */
            ECFM_MEMCPY (pu1Pdu, pTstInfo->TstDestMacAddr,
                         ECFM_MAC_ADDR_LENGTH);
            break;

        case ECFM_OPCODE_DMM:
        case ECFM_OPCODE_1DM:
            pDmInfo = &(pMepInfo->DmInfo);
            /* Fill in the 6byte of Destination Address */
            ECFM_MEMCPY (pu1Pdu, pDmInfo->TxDmDestMacAddr,
                         ECFM_MAC_ADDR_LENGTH);
            break;
        default:
            break;
    }

    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 6byte with source address as the address of the 
     * Sending MEP
     */
    if (ECFM_LBLT_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        return;
    }
    ECFM_GET_MAC_ADDR_OF_PORT
        (ECFM_LBLT_PORT_INFO (pMepInfo->u2PortNum)->u4IfIndex, pu1Pdu);
    pu1Pdu = pu1Pdu + MAC_ADDR_LEN;

    /*Fill in the next 2 byte as Length of the CMF PDU if the LLC encap has to be
     * added else fill in as the CFM type */
    if (pMepInfo->pPortInfo->b1LLCEncapStatus == ECFM_TRUE)
    {
        /* Filling LLC Snap header */
        ECFM_LBLT_SET_LLC_SNAP_HDR (pu1Pdu,
                                    (u2Length + ECFM_LLC_SNAP_HDR_SIZE));
    }
    else
    {
        /* Fill in the next 2 byte as the CFM type */
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_PDU_TYPE_CFM);
    }
    *ppu1Pdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmMiIsVlanIsidMemberPort                           */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a member of the given Vlan/Isid in the given */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : ContextId, u4IsidIntr, Global IfIndex                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
PUBLIC              BOOL1
EcfmMiIsVlanIsidMemberPort (UINT4 u4ContextId, UINT4 u4IsidIntr, UINT2 u2Port)
{
    if (ECFM_CC_GET_PORT_INFO (u2Port) != NULL)
    {
        if (u4IsidIntr < ECFM_INTERNAL_ISID_MIN)
        {
            return (EcfmL2IwfMiIsVlanMemberPort (u4ContextId, u4IsidIntr,
                                                 ECFM_CC_PORT_INFO (u2Port)->
                                                 u4IfIndex));
        }
        else
        {
            if (EcfmPbbMiIsIsidMemberPort (u4ContextId, u4IsidIntr,
                                           u2Port) == PBB_SUCCESS)
            {
                return OSIX_TRUE;
            }
            return OSIX_FALSE;
        }
    }
    else
    {
        return OSIX_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : EcfmPbbGetFwdPortList                            */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the list of ports to which the pkt is fwded. */
/*                                                                           */
/*    Input(s)            : pFrame- Incoming frame                           */
/*                          u4ContextId - Context Identifier                 */
/*                          u2LocalPort - Port Identifier                    */
/*                          SrcAddr - Source MacAddress to be learnt         */
/*                          DstMacAddr - Destination Mac Address for lookup  */
/*                          u4Isid - Isid of the frame                       */
/*                                                                           */
/*    Output(s)           : FwdPortList- list of port(s) to which the pkt    */
/*                          has to be forwarded                              */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
EcfmGetFwdPortList (UINT4 u4ContextId, UINT2 u2LocalPort, tMacAddr SrcAddr,
                    tMacAddr DestAddr, UINT4 U4VidIsid, tLocalPortListExt
                    FwdPortList)
{
    if (U4VidIsid < ECFM_INTERNAL_ISID_MIN)
    {
        return (EcfmVlanGetFwdPortList (u4ContextId, u2LocalPort,
                                        SrcAddr, DestAddr, U4VidIsid,
                                        FwdPortList));
    }
    else
    {
        return VLAN_FAILURE;
    }

}

/*****************************************************************************/
/* Function Name      : EcfmGetPortState                                     */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Vlan Id/Isid and the Port Index.                     */
/*                                                                           */
/* Input(s)           : U4VidIsid - VlanId/ISID for which the port state is  */
/*                      to be   obtained.                                    */
/*                    : u2IfIndex - Global IfIndex of the port whose state   */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
PUBLIC UINT1
EcfmGetPortState (UINT4 U4VidIsid, UINT2 u2IfIndex)
{
    return (EcfmL2IwfGetVlanPortState (U4VidIsid, u2IfIndex));
}

/*****************************************************************************/
/* Function Name      : EcfmMiIsUntagMemberPort                              */
/*                                                                           */
/* Description        : This function calls the L2IWF/PBB module to get the  */
/*                      Port Membership status                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                    : U4VidIsid - Vlan/Isid Identifier                     */
/*                    : u4IfIndex - Actual interface number                  */
/*                                                                           */
/* Output(s)          : ECFM_TRUE - Port is Untagged member of Vlan          */
/*                    : ECFM_FALSE - Port is Tagged member of Vlan           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmMiIsUntagMemberPort (UINT4 u2ContextId, UINT4 u4VidIsid, UINT4 u4IfIndex)
{
    if (u4VidIsid < ECFM_INTERNAL_ISID_MIN)
    {
        return (EcfmL2IwfMiIsVlanUntagMemberPort (u2ContextId,
                                                  u4VidIsid, u4IfIndex));
    }
    else
    {
        return ECFM_FALSE;
    }
}

/*****************************************************************************/
/* Function Name      : EcfmAHCheckNValidateMepCreation                      */
/*                                                                           */
/* Description        : This function cheks the MEP configuration allowed on */
/*                      Port with given direction.                           */
/*                                                                           */
/* Input(s)           : u2LocalPort - localport Identifier                   */
/*                    : U4VidIsid - Vlan/Isid Identifier                     */
/*                    : u1Direction - MEP direction UP/DOWN                  */
/*                                                                           */
/* Output(s)          : ECFM_FAILURE                                         */
/*                    : ECFM_SUCCESS                                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
EcfmAHCheckNValidateMepCreation (UINT2 u2LocalPort, UINT4 u4VidIsid,
                                 UINT1 u1Direction)
{
    UINT1               u1PbbPortType;
    u1PbbPortType = ECFM_CC_GET_PORT_TYPE (u2LocalPort);
    switch (u1PbbPortType)
    {
        case ECFM_CNP_STAGGED_PORT:
        case ECFM_CNP_PORTBASED_PORT:
        case ECFM_CNP_CTAGGED_PORT:
            if (u4VidIsid < ECFM_INTERNAL_ISID_MIN)
            {
                if (u1Direction == ECFM_MP_DIR_UP)
                {
                    return ECFM_FAILURE;
                }
            }
            else
            {
                if (u1Direction == ECFM_MP_DIR_DOWN)
                {
                    return ECFM_FAILURE;
                }
            }
            break;
        case ECFM_PROVIDER_INSTANCE_PORT:
            if (u4VidIsid < ECFM_INTERNAL_ISID_MIN)
            {
                return ECFM_FAILURE;
            }
            else
            {
                if (u1Direction == ECFM_MP_DIR_UP)
                {
                    return ECFM_FAILURE;
                }
            }
            break;
        case ECFM_CUSTOMER_BACKBONE_PORT:
            if (u4VidIsid < ECFM_INTERNAL_ISID_MIN)
            {
                if (u1Direction == ECFM_MP_DIR_DOWN)
                {
                    return ECFM_FAILURE;
                }
            }
            break;

        case ECFM_PROVIDER_NETWORK_PORT:
            if (u4VidIsid >= ECFM_INTERNAL_ISID_MIN)
            {
                return ECFM_FAILURE;
            }

            break;
        default:
            return ECFM_FAILURE;

    }

    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    FUNCTION NAME    : EcfmValidateMdNameFormat
 *
 *    DESCRIPTION      : This function checks whether MD name is according
 *                       to format defined by the mib object 
 *                       "dot1agCfmMdFormat"
 *
 *    INPUT            : pu1TestName - Name that needs to be validated
 *                       i4TestLength - Length of the Md Name string
 *                       u1TestNameType - Type of Validation to be done
 *
 *    OUTPUT           : pu4ErrorCode - Type of error if any
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmValidateMdNameFormat (UINT1 *pu1TestName, INT4 i4TestLength,
                          UINT1 u1TestNameType, UINT4 *pu4ErrorCode)
{
    INT4                i4Count = 0;

    switch (u1TestNameType)
    {
        case ECFM_DOMAIN_NAME_TYPE_IEEE_RESERVED:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
            return ECFM_FAILURE;
        }
            /* Added a check for DOMAIN NAME TYPE AS NONE 
             * In case if the format type is none, then the 
             * MD Name should be equal to "none". 
             */
        case ECFM_DOMAIN_NAME_TYPE_NONE:
        {
            UINT1               u1Counter = ECFM_INIT_VAL;
            /*  Validate Length */
            if (i4TestLength > ECFM_MD_NAME_MAX_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return ECFM_FAILURE;
            }

            /* Check if MD name is between o to 31 decimal codes */
            for (u1Counter = ECFM_INIT_VAL; u1Counter < i4TestLength;
                 u1Counter++)
            {
                if ((*(pu1TestName + u1Counter) <= ECFM_VAL_31) ||
                    (*(pu1TestName + u1Counter) > ECFM_VAL_127))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return ECFM_FAILURE;
                }
            }
            break;
        }
        case ECFM_DOMAIN_NAME_TYPE_DNS_LIKE_NAME:
        {
            /*  Max Allowed length is 20 characters */
            if (i4TestLength > ECFM_MD_NAME_MAX_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return ECFM_FAILURE;
            }

            if (!(isalnum (*pu1TestName) &&
                  isalnum (*(pu1TestName + STRLEN (pu1TestName) - 1))))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return ECFM_FAILURE;
            }
            else
            {
                for (i4Count = 0; i4Count < i4TestLength - 1; i4Count++)
                {
                    pu1TestName++;
                    if (isalnum (*pu1TestName))
                    {
                        continue;
                    }
                    if (((*pu1TestName != '.') && (*pu1TestName != '-')) ||
                        ((*pu1TestName == '.') && (*(pu1TestName + 1) == '.'))
                        || ((*pu1TestName == '-')
                            && (*(pu1TestName + 1) == '-'))
                        || ((*pu1TestName == '-')
                            && (*(pu1TestName + 1) == '.'))
                        || ((*pu1TestName == '.')
                            && (*(pu1TestName + 1) == '-')))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                        return ECFM_FAILURE;
                    }
                }
            }
            break;
        }
        case ECFM_DOMAIN_NAME_TYPE_MAC_ADDR_AND_UINT:

            if ((i4TestLength < MAC_ADDR_LEN + ECFM_VAL_1) ||
                (i4TestLength > (MAC_ADDR_LEN + ECFM_VAL_2)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return ECFM_FAILURE;
            }

            break;

        case ECFM_DOMAIN_NAME_TYPE_CHAR_STRING:
        {
            UINT1               u1Counter = ECFM_INIT_VAL;
            /*  Validate Length */
            if (i4TestLength > ECFM_MD_NAME_MAX_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return ECFM_FAILURE;
            }

            /* Check if MD name is between o to 31 decimal codes */
            for (u1Counter = ECFM_INIT_VAL; u1Counter < i4TestLength;
                 u1Counter++)
            {
                if ((*(pu1TestName + u1Counter) <= ECFM_VAL_31) ||
                    (*(pu1TestName + u1Counter) > ECFM_VAL_127))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return ECFM_FAILURE;
                }
            }
            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
            return ECFM_FAILURE;
        }
    }

    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    FUNCTION NAME    : EcfmValidateMaNameFormat
 *
 *    DESCRIPTION      : This function checks whether MA name is according
 *                       to format defined by the mib Object 
 *                       "dot1agCfmMaFormat"
 *
 *    INPUT            : pu1TestName - Name that needs to be validated
 *                       i4TestLength - Length of the Name String
 *                       u1TestNameType - Name Type defined for the Domain
 *                       pMdInfo         - Pointer to MD Information 
 *
 *    OUTPUT           : pu1ErrorCode - Type of error in name format
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmValidateMaNameFormat (UINT1 *pu1TestName, INT4 i4TestLength,
                          UINT1 u1TestNameType, tEcfmCcMdInfo * pMdInfo,
                          UINT4 *pu4ErrorCode)
{
    UINT1               u1Counter = ECFM_INIT_VAL;

    switch (u1TestNameType)
    {
        case ECFM_ASSOC_NAME_IEEE_RESERVED:
            CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return ECFM_FAILURE;

        case ECFM_ASSOC_NAME_PRIMARY_VID:
            /*  Validate Length */
            if (i4TestLength > ECFM_ASSOC_NAME_PRIMARY_VID_LENGTH)
            {
                CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return ECFM_FAILURE;
            }
            break;

        case ECFM_ASSOC_NAME_CHAR_STRING:
        case ECFM_ASSOC_NAME_ICC:
            /*  Validate Length */
            if (pMdInfo->u1NameFormat == ECFM_DOMAIN_NAME_TYPE_NONE)
            {
                if ((i4TestLength + pMdInfo->u1NameLength) >
                    ECFM_NONE_DOM_MA_LEN)
                {
                    CLI_SET_ERR(CLI_ECFM_MA_NAME_LEN_EXCEED_MAID_LEN);
                    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                    return ECFM_FAILURE;
                }
            }
            else                /* Below case is not applicable to ECFM_ASSOC_NAME_ICC */
            {
                if ((i4TestLength + pMdInfo->u1NameLength) > ECFM_DOM_MA_LEN)
                {
                    CLI_SET_ERR(CLI_ECFM_MA_NAME_LEN_EXCEED_MAID_LEN);
                    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                    return ECFM_FAILURE;
                }
            }

            /* Check if MA name is between '0' to 'Z' decimal codes */
            for (u1Counter = ECFM_INIT_VAL; u1Counter < i4TestLength;
                 u1Counter++)
            {
                if ((*(pu1TestName + u1Counter) <= ECFM_VAL_31) ||
                    (*(pu1TestName + u1Counter) > ECFM_VAL_127))
                {
                    CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return ECFM_FAILURE;
                }
            }

            break;

        case ECFM_ASSOC_NAME_UNSIGNED_INT_16:

            if (i4TestLength > ECFM_VAL_5)
            {
                CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return ECFM_FAILURE;
            }
            break;

        case ECFM_ASSOC_NAME_RFC2865_VPN_ID:

            if (i4TestLength > ECFM_ASSOC_NAME_RFC2865_VPN_ID_LENGTH)
            {
                CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return ECFM_FAILURE;
            }
            break;

        default:
            CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmAHUtilChkPortFiltering
 *
 * Description        : This routine applies the port-filtering rules 
 *                      for PBB bridge
 *
 * Input(s)           : 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmAHUtilChkPortFiltering (UINT4 u4IfIndex, UINT1 u1IfOperStatus,
                            UINT4 u4RxVlanIdIsId, UINT1 u1PbbPortType,
                            tPbbTag * pPbbTag, UINT1 u1Direction)
{
    /*Check the port operational Status */
    if (u1IfOperStatus != CFA_IF_UP)

    {
        ECFM_GLB_TRC (ECFM_DEFAULT_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmAHUtilChkPortFiltering : CFM-PDU filtered, port is not UP \r\n");
        return ECFM_FAILURE;
    }
    /* PIP is not running spanning tree */
    if (u1PbbPortType != ECFM_PROVIDER_INSTANCE_PORT)
    {
        /* Check for Spanning tree port state */
        if (EcfmL2IwfGetVlanPortState (u4RxVlanIdIsId,
                                       u4IfIndex) != AST_PORT_STATE_FORWARDING)

        {
            ECFM_GLB_TRC (ECFM_DEFAULT_CONTEXT,
                          ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmAHUtilChkPortFiltering : CFM-PDU filtered, spanning tree"
                          " port state is not forwarding \r\n");
            return ECFM_FAILURE;
        }
    }
    if (u1Direction == ECFM_MP_DIR_UP)
    {
        /* PIP port needs to drop the CFM-PDUs with UCA-Bit as TRUE */
        if ((u1PbbPortType == ECFM_PROVIDER_INSTANCE_PORT) &&
            (pPbbTag->InnerIsidTag.u1UcaBitValue == ECFM_TRUE))
        {
            ECFM_GLB_TRC (ECFM_DEFAULT_CONTEXT,
                          ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmAHUtilChkPortFiltering : CFM-PDU filtered, UCA"
                          " bit is ON \r\n");
            return ECFM_FAILURE;
        }
        /* 
         * Check we are trying to forward a CFM-PDU from CBP, without an I-TAG
         */
        if (u1PbbPortType == ECFM_CUSTOMER_BACKBONE_PORT)
        {
            /* Check if we did received an I-TAG in the frame and we are not
             * using the customer mac address */
            if ((pPbbTag->InnerIsidTag.u1TagType == VLAN_UNTAGGED) &&
                (pPbbTag->InnerIsidTag.u1UcaBitValue != ECFM_TRUE))
            {
                ECFM_GLB_TRC (ECFM_DEFAULT_CONTEXT,
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmAHUtilChkPortFiltering : CFM-PDU filtered, frame"
                              " without I-TAG cannot be transmited on CBP \r\n");
                /* Drop this PDU */
                return ECFM_FAILURE;
            }
        }
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmRBTreeCreateEmbedded
 *
 * Description        : This routine is used to disable Semaphore created with 
 *                      RBTree
 *
 * Input(s)           : u4Offset - Offset 
 *                      Cmp      - Compare Function required for RBTree Creation
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to RBTree
 *****************************************************************************/
tRBTree
EcfmRBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp)
{
    tRBTree             T;
    T = RBTreeCreateEmbedded (u4Offset, Cmp);
    if (T == NULL)
    {
        return NULL;
    }
    RBTreeDisableMutualExclusion (T);
    return T;
}

/******************************************************************************
 * Function Name      : EcfmValidateMaNameStrFormat
 *
 * Description        : This routine is used to validate the given MA name based
 *                      on the specified format.
 *
 * Input(s)           : pu1MaStr - MA Name
 *                      u4MaNameType - MA name format
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_FAILURE/ECFM_SUCCESS
 *****************************************************************************/
PUBLIC INT4
EcfmValidateMaNameStrFormat (UINT1 *pu1MaStr, UINT4 u4MaNameType)
{
    UINT1               au1VpnOUI[ECFM_VAL_10];
    UINT1               au1VpnIndex[ECFM_VAL_10];
    UINT4               u4Counter = ECFM_INIT_VAL;
    UINT4               u4MaStrLen = ECFM_INIT_VAL;
    UINT4               u4MaValue = ECFM_INIT_VAL;
    UINT2               u2Index = ECFM_INIT_VAL;
    UINT2               u2Count = ECFM_INIT_VAL;
    UINT1               u1DotCount = ECFM_INIT_VAL;

    if ((u4MaNameType == ECFM_ASSOC_NAME_PRIMARY_VID) && (pu1MaStr == NULL))
    {
        return ECFM_SUCCESS;
    }
    else if (pu1MaStr == NULL)
    {
        return ECFM_FAILURE;
    }

    u4MaStrLen = STRLEN (pu1MaStr);

    switch (u4MaNameType)
    {
        case ECFM_ASSOC_NAME_UNSIGNED_INT_16:
            /* Strlen max 5 chars (65535) */
            if (u4MaStrLen > ECFM_VAL_5)
            {
                return ECFM_FAILURE;
            }
            /* All Chars should be digits */
            u4Counter = u4MaStrLen;
            while (u4Counter--)
            {
                if (!ISDIGIT (pu1MaStr[u4Counter]))
                {
                    return ECFM_FAILURE;
                }
            }
            /* Should not more than 2 bytes max */
            u4MaValue = ATOI (pu1MaStr);
            if ((u4MaValue < ECFM_UINT1_MIN) || (u4MaValue > ECFM_UINT2_MAX))
            {
                return ECFM_FAILURE;
            }
            break;

        case ECFM_ASSOC_NAME_CHAR_STRING:
        case ECFM_ASSOC_NAME_ICC:

            /* Validate Length */
            if (u4MaStrLen > ECFM_MA_NAME_MAX_LEN)
            {
                return ECFM_FAILURE;
            }
            break;

        case ECFM_ASSOC_NAME_PRIMARY_VID:

            /* Strlen max 4 chars (4095) */
            if (u4MaStrLen > ECFM_VAL_4)
            {
                return ECFM_FAILURE;
            }
            /* All Chars should be digits */
            u4Counter = u4MaStrLen;
            while (u4Counter--)
            {
                if (!ISDIGIT (pu1MaStr[u4Counter]))
                {
                    return ECFM_FAILURE;
                }
            }
            /* Should not more than 2 bytes max */
            u4MaValue = ATOI (pu1MaStr);
            if ((u4MaValue < ECFM_VLANID_MIN) || (u4MaValue > ECFM_VLANID_MAX))
            {
                return ECFM_FAILURE;
            }
            break;

        case ECFM_ASSOC_NAME_RFC2865_VPN_ID:
            /* [3 Bytes VPNOUI:4 Bytes VPNIndex] and Format should be in Hex */
            if (u4MaStrLen != ECFM_VAL_15)
            {
                return ECFM_FAILURE;
            }

            /* Check for any special characters present and return failure if
             * present */
            u4Counter = u4MaStrLen;
            for (u4Counter = ECFM_INIT_VAL; u4Counter < u4MaStrLen; u4Counter++)
            {
                if (pu1MaStr[u4Counter] == ':')
                {
                    u1DotCount++;
                    continue;
                }
                else if (!ISXDIGIT (pu1MaStr[u4Counter]))
                {
                    return ECFM_FAILURE;
                }
            }

            if (u1DotCount != ECFM_VAL_1)
            {
                return ECFM_FAILURE;
            }

            u1DotCount = ECFM_INIT_VAL;
            MEMSET (au1VpnOUI, ECFM_VAL_0, ECFM_VAL_10);
            MEMSET (au1VpnIndex, ECFM_VAL_0, ECFM_VAL_10);

            for (u4Counter = 0; u4Counter < u4MaStrLen; u4Counter++)
            {
                if (pu1MaStr[u4Counter] == ':')
                {
                    u1DotCount++;
                    continue;
                }

                if (u1DotCount != ECFM_VAL_1)
                {
                    if (u2Count < ECFM_VAL_10)
                    {
                        au1VpnOUI[u2Count] = pu1MaStr[u4Counter];
                    }
                    u2Count++;
                }
                else
                {
                    if (u2Index < ECFM_VAL_10)
                    {
                        au1VpnIndex[u2Index] = pu1MaStr[u4Counter];
                    }
                    u2Index++;
                }
            }
            if ((u2Index < ECFM_VAL_1) || (u2Count < ECFM_VAL_1) ||
                (u2Index > ECFM_VAL_8) || (u2Count > ECFM_VAL_6))
            {
                return ECFM_FAILURE;
            }
            break;

        default:
            return ECFM_FAILURE;
    }

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmUtilMaNameOctetStrToStr
 *
 * Description        : This routine is used to convert the Octet string MA 
 *                      Name into printable format.
 *
 * Input(s)           : pMaName - Octet string
 *                      u4MaFormat - MA Name format
 *
 * Output(s)          : pu1StrMaName - pointer to the string contains MA name
 *                      in printable format.
 *
 * Return Value(s)    : None
 *****************************************************************************/

PUBLIC VOID
EcfmUtilMaNameOctetStrToStr (tSNMP_OCTET_STRING_TYPE * pMaName,
                             INT4 i4MaFormat, UINT1 *pu1StrMaName)
{
    UINT2               u2MaVal = ECFM_INIT_VAL;
    UINT4               u4VpnOui = ECFM_INIT_VAL;
    UINT4               u4VpnIndex = ECFM_INIT_VAL;

    if (i4MaFormat == ECFM_ASSOC_NAME_UNSIGNED_INT_16)
    {
        MEMCPY (&u2MaVal, pMaName->pu1_OctetList, pMaName->i4_Length);
        u2MaVal = ECFM_HTONS (u2MaVal);
        SPRINTF ((CHR1 *) pu1StrMaName, "%d", u2MaVal);

    }
    else if (i4MaFormat == ECFM_ASSOC_NAME_PRIMARY_VID)
    {
        MEMCPY (&u2MaVal, pMaName->pu1_OctetList, pMaName->i4_Length);
        u2MaVal = ECFM_HTONS (u2MaVal);
        SPRINTF ((CHR1 *) pu1StrMaName, "%d", u2MaVal);
    }
    else if (i4MaFormat == ECFM_ASSOC_NAME_RFC2865_VPN_ID)
    {
        u4VpnOui = pMaName->pu1_OctetList[2] | u4VpnOui;
        u4VpnOui = (pMaName->pu1_OctetList[1] << 8) | u4VpnOui;
        u4VpnOui = (pMaName->pu1_OctetList[0] << 16) | u4VpnOui;

        MEMCPY (&u4VpnIndex, (pMaName->pu1_OctetList + ECFM_VAL_3), ECFM_VAL_4);
        u4VpnIndex = ECFM_HTONL (u4VpnIndex);

        SPRINTF ((CHR1 *) pu1StrMaName, "%06x:%08x", u4VpnOui, u4VpnIndex);
    }
    else if ((i4MaFormat == ECFM_ASSOC_NAME_CHAR_STRING) ||
             (i4MaFormat == ECFM_ASSOC_NAME_ICC))
    {
        MEMCPY (pu1StrMaName, pMaName->pu1_OctetList, pMaName->i4_Length);
        pu1StrMaName[(pMaName->i4_Length)] = '\0';
    }
}

/******************************************************************************
 * Function Name      : EcfmUtilMaNameOctetStrToMegId
 *
 * Description        : This routine is used to convert MA name to MEGID
 *
 * Input(s)           : pMaName - Octet string
 *                      u1MaLen - MA name length
 *                      u4MaFormat - MA Name format
 *
 * Output(s)          : pu1MegId - MEG-ID 
 *                      pu1MegIdLen - MEG-ID length.
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmUtilMaNameOctetStrToMegId (UINT1 *pu1MaName, UINT1 u1MaLen,
                               UINT1 u1MaFormat, UINT1 *pu1MegId,
                               UINT1 *pu1MegIdLen)
{
    UINT1               u1StrMaName[ECFM_MA_NAME_ARRAY_SIZE];
    UINT2               u2MaVal = ECFM_INIT_VAL;
    UINT2               u2StrLen = ECFM_INIT_VAL;
    UINT4               u4VpnOui = ECFM_INIT_VAL;
    UINT4               u4VpnIndex = ECFM_INIT_VAL;

    MEMSET (u1StrMaName, ECFM_INIT_VAL, ECFM_MA_NAME_ARRAY_SIZE);

    if ((u1MaFormat == ECFM_ASSOC_NAME_PRIMARY_VID) ||
        (u1MaFormat == ECFM_ASSOC_NAME_UNSIGNED_INT_16))
    {
        MEMCPY (&u2MaVal, pu1MaName, u1MaLen);
        u2MaVal = ECFM_HTONS (u2MaVal);
        SPRINTF ((CHR1 *) u1StrMaName, "%d", u2MaVal);
    }
    else if (u1MaFormat == ECFM_ASSOC_NAME_RFC2865_VPN_ID)
    {
        u4VpnOui = pu1MaName[2] | u4VpnOui;
        u4VpnOui = (pu1MaName[1] << 8) | u4VpnOui;
        u4VpnOui = (pu1MaName[0] << 16) | u4VpnOui;

        MEMCPY (&u4VpnIndex, (pu1MaName + ECFM_VAL_3), ECFM_VAL_4);
        u4VpnIndex = ECFM_HTONL (u4VpnIndex);

        SPRINTF ((CHR1 *) u1StrMaName, "%06x%08x", u4VpnOui, u4VpnIndex);
    }
    else if ((u1MaFormat == ECFM_ASSOC_NAME_CHAR_STRING) ||
             (u1MaFormat == ECFM_ASSOC_NAME_ICC))
    {
        MEMCPY (u1StrMaName, pu1MaName, u1MaLen);
    }

    if (u1MaLen < ECFM_MA_NAME_ARRAY_SIZE)
    {
        u1StrMaName[u1MaLen] = '\0';
    }
    else
    {
        u1StrMaName[ECFM_MA_NAME_ARRAY_SIZE - ECFM_VAL_1] = '\0';
    }

    u2StrLen = STRLEN (u1StrMaName);
    if (u2StrLen >= ECFM_MEG_ID_LEN)
    {
        *pu1MegIdLen = ECFM_MEG_ID_LEN;
    }
    else
    {
        *pu1MegIdLen = u2StrLen;
    }

    STRNCPY (pu1MegId, u1StrMaName, *pu1MegIdLen);
}

/******************************************************************************
 * Function Name      : EcfmUtilMdNameOctetStrToStr
 *
 * Description        : This routine is used to convert the Octet string MD 
 *                      Name into printable format.
 *
 * Input(s)           : pMdName - Octet string
 *                      u4MdFormat - MA Name format
 *
 * Output(s)          : pu1StrMaName - pointer to the string contains MD name
 *                      in printable format.
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmUtilMdNameOctetStrToStr (tSNMP_OCTET_STRING_TYPE * pMdName,
                             INT4 i4MdFormat, UINT1 *pu1StrMdName)
{
    UINT2               u2MdUint = ECFM_INIT_VAL;
    UINT1              *pu1Temp = pu1StrMdName;
    UINT1               u1Count = ECFM_INIT_VAL;

    /* Added check for the ECFM_DOMAIN_NAME_TYPE_NONE */
    if ((i4MdFormat == ECFM_DOMAIN_NAME_TYPE_DNS_LIKE_NAME) ||
        (i4MdFormat == ECFM_DOMAIN_NAME_TYPE_CHAR_STRING) ||
        (i4MdFormat == ECFM_DOMAIN_NAME_TYPE_NONE))
    {
        MEMCPY (pu1Temp, pMdName->pu1_OctetList, pMdName->i4_Length);
    }
    else if (i4MdFormat == ECFM_DOMAIN_NAME_TYPE_MAC_ADDR_AND_UINT)
    {
        for (u1Count = 0; u1Count < MAC_ADDR_LEN; u1Count++)
        {
            pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                                *(pMdName->pu1_OctetList + u1Count));
        }

        MEMCPY (&u2MdUint, (pMdName->pu1_OctetList + u1Count), sizeof (UINT2));
        SPRINTF ((CHR1 *) pu1Temp, "%u", u2MdUint);
    }
}

/*****************************************************************************
 *
 *    Function Name        : EcfmUtilGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid
 *                        CFA_FAILURE if u4IfIndex is Invalid
 *****************************************************************************/
PUBLIC INT4
EcfmUtilGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    tMacAddr            NullMacAddr = { 0 };
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;

    if (EcfmVcmGetContextInfoFromIfIndex
        (u4IfIndex, &u4ContextId, &u2LocalPortId) != VCM_SUCCESS)
    {
        return CFA_FAILURE;
    }
    if (EcfmCfaGetIfInfo (u4IfIndex, pIfInfo) != CFA_FAILURE)
    {
        if (MEMCMP (pIfInfo->au1MacAddr, NullMacAddr, sizeof (tMacAddr)) == 0)
        {
            /* Fill the bridge-mac address if we dont have any mac-address
             * configure for the port */

            EcfmCfaGetContextMacAddr (u4ContextId, pIfInfo->au1MacAddr);
        }
        return CFA_SUCCESS;
    }
    else
        return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : EcfmUtilQueryBitListTable
 *
 *    Description          : This function is used to get value of  BIT
 *                           from the given position.
 *
 *    Input(s)             : u1Value - Byte value
 *                           u1Position - Bit position (0-7)
 *
 *    Output(s)            : None
 *
 *    Returns              : Value of the BIT
 *****************************************************************************/
UINT1
EcfmUtilQueryBitListTable (UINT1 u1Value, UINT1 u1Position)
{
    return ECFM_BITLIST_SCAN_TABLE (u1Value, u1Position);
}

/*****************************************************************************
 *    Function Name        : EcfmUtilIndicateProtocol
 *
 *    Description          : This function is used to trigger the call back
 *                           functions registered by the higher layer protocols.
 *
 *    Input(s)             : pEcfmEventNotification - Pointer to ECFM Event
 *                                                    Notification info.
 *                           u4Evt                                
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.             
 *****************************************************************************/
PUBLIC VOID
EcfmUtilIndicateProtocol (tEcfmEventNotification * pEcfmEventNotification,
                          UINT4 u4EntId, UINT4 u4Event)
{
    tEcfmBufChainHeader *pDupBuf = NULL;

    if ((ECFM_CC_GET_APP_INFO (u4EntId) == NULL) ||
        (ECFM_CC_GET_APP_INFO (u4EntId)->pFnRcvPkt == NULL) ||
        ((ECFM_CC_GET_APP_INFO (u4EntId)->u4EventsId & u4Event)
         == ECFM_INIT_VAL))
    {
        return;
    }

    if (pEcfmEventNotification->PduInfo.pu1Data != NULL)
    {
        pDupBuf =
            ECFM_DUPLICATE_CRU_BUF (pEcfmEventNotification->PduInfo.pu1Data);
        if (pDupBuf == NULL)
        {
            /* Duplicating CRU Buffer failed, hence cannot indicate this 
             * module, so continue with the indication to next module 
             */
            return;
        }
    }
    pEcfmEventNotification->PduInfo.pu1Data = pDupBuf;

    (*(ECFM_CC_GET_APP_INFO (u4EntId)->pFnRcvPkt)) (pEcfmEventNotification);
}

/*****************************************************************************
 *    Function Name        : EcfmUtilGetMepInfoFromMaVlans
 *
 *    Description          : This function is used to obtain the MEP info from
 *                           the given vlan information. The MEP may be present 
 *                           in the given vlan itself or installed on any of the
 *                           Vlans that are configured as secondary vlan for 
 *                           this vlan or in the primary vlan of the give vlan
 *                           or in any of the secondary vlans of the primary 
 *                           vlans configured for this secondary vlan. This 
 *                           utility will be used by the ERPS entity during RAPS
 *                           PDU reception.
 *
 *    Input(s)             : pEcfmEventNotification - Pointer to ECFM Event
 *                                                    Notification info.
 *                           u4Evt                                
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.             
 *****************************************************************************/
PUBLIC INT4
EcfmUtilGetMepInfoFromMaVlans (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtVlanInfo   VlanInfo;
    tEcfmLbLtVlanInfo  *pVlanInfo = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;

    ECFM_MEMSET (&VlanInfo, 0, sizeof (tEcfmLbLtVlanInfo));

    /* Check whether the VLAN id is primary or secondary VLAN. */
    VlanInfo.u4VidIsid = pPduSmInfo->u4RxVlanIdIsId;

    pVlanInfo = (tEcfmLbLtVlanInfo *)
        RBTreeGet ((ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanTable,
                   (tRBElem *) & VlanInfo);
    if (pVlanInfo == NULL)
    {
        /* Receive VLAN is primary vlan. Hence try to find the stack info
         * for this primary vlan and if MEP exists process on the same
         * */
        pStackInfo = EcfmLbLtUtilGetMp (pPduSmInfo->pPortInfo->u2PortNum,
                                        pPduSmInfo->u1RxMdLevel,
                                        pPduSmInfo->u4RxVlanIdIsId,
                                        pPduSmInfo->u1RxDirection);
        if (pStackInfo != NULL)
        {
            /* Found the Mep used by rx vlan. Call OpCode Demultiplexer */
            if (EcfmLbLtCtrlRxOpCodeDeMux (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "Opcode Demultiplexer not able to process the "
                               "RAPS PDU\r\n");
                return ECFM_FAILURE;
            }
            else
            {
                return ECFM_SUCCESS;
            }
        }
        return ECFM_FAILURE;
    }
    else
    {
        /* Receive VLAN is the secondary vlan in this MA, get the primary vlan 
         * for this secondary vlan 
         * */
        pPduSmInfo->u4RxVlanIdIsId = pVlanInfo->u4PrimaryVidIsid;
    }

    /* Scan through the secondary vlan table */
    VlanInfo.u4PrimaryVidIsid = pPduSmInfo->u4RxVlanIdIsId;
    VlanInfo.u4VidIsid = 0;

    pVlanInfo = (tEcfmLbLtVlanInfo *)
        RBTreeGetNext ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable,
                       (tRBElem *) & VlanInfo, NULL);

    while ((pVlanInfo != NULL) &&
           (pVlanInfo->u4PrimaryVidIsid == pPduSmInfo->u4RxVlanIdIsId))
    {
        /* Find the MEP for the primary vlan. If the VLAN is primary vlan,
         * then the earlier RB Tree fetch in 
         * ECFM_LBLT_CURR_CONTEXT_INFO ())->VlanInfo would have failed.
         * */
        pStackInfo = EcfmLbLtUtilGetMp (pPduSmInfo->pPortInfo->u2PortNum,
                                        pPduSmInfo->u1RxMdLevel,
                                        pVlanInfo->u4VidIsid,
                                        pPduSmInfo->u1RxDirection);
        if (pStackInfo != NULL)
        {
            /* Found the Mep used by rx vlan. Call OpCode Demultiplexer */
            pPduSmInfo->u4RxVlanIdIsId = pVlanInfo->u4VidIsid;

            if (EcfmLbLtCtrlRxOpCodeDeMux (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "Opcode Demultiplexer not able to process the "
                               "RAPS PDU\r\n");
                return ECFM_FAILURE;
            }
            else
            {
                return ECFM_SUCCESS;
            }

        }

        pVlanInfo = (tEcfmLbLtVlanInfo *)
            RBTreeGetNext ((ECFM_LBLT_CURR_CONTEXT_INFO ())->PrimaryVlanTable,
                           (tRBElem *) pVlanInfo, NULL);
    }
    return ECFM_FAILURE;
}

/*****************************************************************************
 * Function     : EcfmMptpUtilGetY1731HdrOffSet
 *                                                                           
 * Description  : This utility is to fetch the Y.1731 header offset by 
 *                calculating the MPLS Header length including the GACH header
 *                                                                           
 * Input        : pBuf      - Pointer to the Y.1731 PDU encapsulated with
 *                            MPLS header.
 *                                                                           
 * Output       : pu4Offset - Pointer to Y.1731 offset value
 *                                                                           
 * Returns      : None
 *****************************************************************************/
PUBLIC VOID
EcfmMptpUtilGetY1731HdrOffSet (tEcfmBufChainHeader * pBuf, UINT4 *pu4Offset)
{
    UINT1               u1ExpSI = ECFM_INIT_VAL;
    UINT4               u4Offset = ECFM_INIT_VAL;

    do
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1ExpSI,
                                   (u4Offset + ECFM_VAL_2), ECFM_VAL_1);
        if ((u1ExpSI & 0x01) == 0x01)
        {
            /* Add the MPLS header len for the header being processed along 
             * with ACH header len which of 4 bytes (The ACH header is 
             * present at the end after all the MPLS Labels.)
             */
            *pu4Offset = u4Offset + MPLS_HDR_LEN + ECFM_VAL_4;
            break;
        }
        u4Offset += MPLS_HDR_LEN;
    }
    while (ECFM_VAL_1);

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmIsPSWInterface                                    */
/*                                                                           */
/* Description        : Indicates whether the given port is a Pseudo wire    */
/*                      Interface or not.                                    */
/*                                                                           */
/* Input(s)           : i4PortId - Local Port Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
EcfmIsPWInterface (INT4 u4IfIndex)
{
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1RetVal = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (EcfmCfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return u1RetVal;
    }

    if (CfaIfInfo.u1IfType == CFA_PSEUDO_WIRE)
    {
        u1RetVal = ECFM_TRUE;
    }
    return u1RetVal;

}

/*****************************************************************************/
/* Function Name      : EcfmCreatePwInterface                                */
/*                                                                           */
/* Description        : This function creates port in ECFM. Based            */
/*                      on the module started, it call EcfmHandleCreatePort. */
/*                                                                           */
/* Input(s)           : i4PortId - Local port Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmCreatePwInterface (INT4 i4PortId)
{

    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId;
    /* UINT4               u4IfIndex; */
    UINT2               u2LocalPort;

    if (EcfmVcmGetContextInfoFromIfIndex (i4PortId, &u4ContextId, &u2LocalPort)
        != VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmMapPort : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_MAP_PORT_MSG;
    pMsg->u4IfIndex = i4PortId;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;

    EcfmCcCfgQueueHandler (pMsg);
    ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
    return 0;
}

/*****************************************************************************/
/* Function Name      : AstDeletePswInterface                                */
/*                                                                           */
/* Description        : This function deletes port in RSTP/MSTP/PVRST. Based */
/*                      on the module started, it call RstDeletePort or      */
/*                      MstDeletePort or PvrstDeletePort.                    */
/*                                                                           */
/* Input(s)           : i4PortId - Local port Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
EcfmDeletePwInterface (INT4 i4PortId)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId;
    /*UINT4               u4IfIndex; */
    UINT2               u2LocalPort;

    if (EcfmVcmGetContextInfoFromIfIndex (i4PortId, &u4ContextId, &u2LocalPort)
        != VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmMapPort : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_UNMAP_PORT_MSG;
    pMsg->u4IfIndex = i4PortId;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;

    EcfmCcCfgQueueHandler (pMsg);
    ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
    return 0;

}

/****************************************************************************
 * Function Name      : EcfmUtilPutMEGID
 *
 * Description        : This rotuine is used to fill the MEGID in the PDU
 * Input(s)           : pu1Pdu - pointer to the PDU
 *                      pMepInfo - Pointer to the Mep structure that stores the 
 *                      information regarding MEP info.
 * Output(s)          : None

 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmUtilPutMEGID (tEcfmCcMepInfo * pMepInfo, UINT1 *pu1Pdu)
{
    tEcfmCcMaInfo      *pMaInfo = NULL;
    UINT1               u1IccLen = ECFM_INIT_VAL;
    UINT1               u1UmcLen = ECFM_INIT_VAL;
    UINT1               u1MegIdLen = ECFM_INIT_VAL;
    UINT1               au1MegId[ECFM_MEG_ID_LEN] = { 0 };

    ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                  ECFM_FN_ENTRY_TRC, "EcfmUtilPutMEGID: "
                  "Constructing the MEG ID \r\n");
    /* Get MA Information from MEP */
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);

    /* If Y.1731 is enabled on port then send the packet with
     * ICC based MEG ID format as follows.
     --------------------------
     | Reserved (01)          |
     --------------------------
     | MEG ID Format (32)     |
     --------------------------
     | MEG ID Length (13)     |
     --------------------------
     | 0 |MEG-ID Value [1-13] |
     --------------------------
     | 0 pad, If necessary    |
     -------------------------
     */
    /* Fill the MEGID Reserved Field (01) */
    ECFM_PUT_1BYTE (pu1Pdu, (ECFM_MEGID_RESERVED_VALUE));

    /* Fill the MEGID Format Field (32) */
    ECFM_PUT_1BYTE (pu1Pdu, (ECFM_MEGID_FORMAT_TYPE));

    /* Fill the MEGID Length Field (13) */
    ECFM_PUT_1BYTE (pu1Pdu, (ECFM_MEG_ID_LEN));

    u1IccLen = STRLEN (pMaInfo->au1IccCode);
    u1UmcLen = STRLEN (pMaInfo->au1UmcCode);
    if ((u1IccLen + u1UmcLen) == ECFM_INIT_VAL)
    {
        EcfmUtilMaNameOctetStrToMegId (pMaInfo->au1Name,
                                       pMaInfo->u1NameLength,
                                       pMaInfo->u1NameFormat,
                                       au1MegId, &(u1MegIdLen));
        /* ICC and UMC not provided, hence copying Short MA Name */
        ECFM_MEMCPY (pu1Pdu, au1MegId, ECFM_MEG_ID_LEN);
        pu1Pdu = pu1Pdu + ECFM_MEG_ID_LEN;
    }
    else
    {
        /* Fill the next bytes with Icc */
        ECFM_PUT_NBYTE (pu1Pdu, pMaInfo->au1IccCode, u1IccLen);
        /* Fill the next bytes with Umc. */
        ECFM_PUT_NBYTE (pu1Pdu, pMaInfo->au1UmcCode, u1UmcLen);
        /* If the MEG ID is less than 13 then put pad the
         * remaining bytes with zero.
         */
        if ((u1IccLen + u1UmcLen) < ECFM_MEG_ID_LEN)
        {
            ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL,
                         (ECFM_MEG_ID_LEN - (u1IccLen + u1UmcLen)));
            pu1Pdu = pu1Pdu + (ECFM_MEG_ID_LEN - (u1IccLen + u1UmcLen));
        }
    }
    /* Fill the next 32 byte as all zeros 0 to make MEGID total
     * of 48bytes*/
    ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, ECFM_CCM_MEGID_PADDING_SIZE);
    pu1Pdu = pu1Pdu + ECFM_CCM_MEGID_PADDING_SIZE;

    return;
}

/****************************************************************************
 * Function Name      : EcfmUtilPutMAID
 *
 * Description        : This rotuine is used to fill the MAID in the PDU
 * Input(s)           : pu1Pdu - pointer to the PDU
 *                      pMepInfo - Pointer to the Mep structure that stores the 
 *                      information regarding MEP info.
 * Output(s)          : None
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmUtilPutMAID (tEcfmCcMepInfo * pMepInfo, UINT1 *pu1Pdu)
{

    UINT1              *pu1MdMaLength = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMdInfo      *pMdInfo = NULL;
    UINT1               u1NameLength = ECFM_INIT_VAL;
    UINT1               u1IccLen = ECFM_INIT_VAL;
    UINT1               u1UmcLen = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    /* Fill in ther MAID */
    /* used at the time of adding paddinig if the length od the MAID is less than
     * 48 bytes*/
    pu1MdMaLength = pu1Pdu;

    pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);

    /* CCM MAID field format : Maintenance Domain Present then,
     * packet should be constructed as per the IEEE 802.1ag standard
     * section 21.6.5 and Table 21-17 as follows:
     -------------------------
     | MD Name Format (not 1)|
     -------------------------                                                         | MD Name Length        |
     -------------------------
     | MD Name               |
     -------------------------
     | Short MA Name Format  |
     -------------------------
     | Short MA Name Length  |
     -------------------------
     | Short MA Name         |
     -------------------------
     | 0 pad, If necessary   |
     -------------------------
     */
    /* CCM MAID field format : Maintenance Domain not present
     * ECFM_DOMAIN_NAME_TYPE_NONE (01) then, packet should be 
     * constructed as per the IEEE 802.1ag standard section 21.6.5.1 
     * Table 21-18 as follows:
     -----------------------
     | MD Name Format (1)  |
     -----------------------
     | Short MA Name Format|
     -----------------------
     | Short MA Name Length|
     -----------------------
     | Short MA Name       |
     -----------------------
     | 0 pad, If necessary |
     -----------------------
     */
    /* Fill the 1byte Md Format Type */
    ECFM_PUT_1BYTE (pu1Pdu, pMdInfo->u1NameFormat);

    if (pMdInfo->u1NameFormat != ECFM_DOMAIN_NAME_TYPE_NONE)
    {
        u1NameLength = (pMdInfo->u1NameLength);
        /* Fill the 1byte Md Name Length and then MD Name */
        ECFM_PUT_1BYTE (pu1Pdu, u1NameLength);

        ECFM_PUT_NBYTE (pu1Pdu, pMdInfo->au1Name, u1NameLength);
    }

    /* Fill the 1 byte Short Ma Name Format */
    ECFM_PUT_1BYTE (pu1Pdu, pMaInfo->u1NameFormat);

    /* If MA name is ITU-T format (32) */
    if (pMaInfo->u1NameFormat == ECFM_ASSOC_NAME_ICC)
    {
        /* Fill the 1 byte Ma Length */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_MEG_ID_LEN);

        u1IccLen = STRLEN (pMaInfo->au1IccCode);
        u1UmcLen = STRLEN (pMaInfo->au1UmcCode);

        /* Fill the next bytes with ICC */
        ECFM_PUT_NBYTE (pu1Pdu, pMaInfo->au1IccCode, u1IccLen);

        /* Fill the next bytes with Umc */
        ECFM_PUT_NBYTE (pu1Pdu, pMaInfo->au1UmcCode, u1UmcLen);

        /* Fill the remaining bytes with Zero */
        if ((u1IccLen + u1UmcLen) < ECFM_MEG_ID_LEN)
        {
            u1NameLength = ECFM_MEG_ID_LEN - (u1IccLen + u1UmcLen);
            ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, u1NameLength);
            pu1Pdu = pu1Pdu + u1NameLength;
        }
    }
    else
    {
        /* Fill the 1 byte Ma Length */
        u1NameLength = pMaInfo->u1NameLength;
        ECFM_PUT_1BYTE (pu1Pdu, u1NameLength);

        /* Fill the next bytes as MA Name  */
        ECFM_PUT_NBYTE (pu1Pdu, pMaInfo->au1Name, u1NameLength);
    }

    /* If total length is less than 48 then add the padding to make 
     * MAID total of 48bytes.
     */
    u1NameLength = (UINT1) (pu1Pdu - pu1MdMaLength);
    if (u1NameLength < ((UINT1) ECFM_MAID_FIELD_SIZE))
    {
        u1NameLength = (((UINT1) ECFM_MAID_FIELD_SIZE) - u1NameLength);
        ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, u1NameLength);
        pu1Pdu = pu1Pdu + u1NameLength;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 *    Function Name        : EcfmUtilCompareMEGID
 *
 *    Description          : This function is used to obtain the MEP info from
 *                           PDU reception.
 *
 *    Input(s)             : pu1MegId - Pointer to MegId
 *                           pMepInfo - Pointer to MepInfo
 *
 *    Output(s)            : None.
 *
 *    Returns              : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmUtilCompareMEGID (UINT1 *pu1MegId, tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcMaInfo      *pMaInfo = NULL;
    UINT1               au1PadVal[ECFM_MAID_FIELD_SIZE] = "\0";
    UINT1               u1Offset = ECFM_INIT_VAL;
    UINT1               u1IccLen = ECFM_INIT_VAL;
    UINT1               u1UmcLen = ECFM_INIT_VAL;
    UINT1               u1MegIdLen = ECFM_INIT_VAL;
    UINT1               u1UmcCmpLen = ECFM_INIT_VAL;

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);

    /* Verify the reserved field */
    if (pu1MegId[0] != ECFM_MEGID_RESERVED_VALUE)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC, "EcfmUtilCompareMEGID: "
                      "Reserved field does not match \r\n");
        return ECFM_FAILURE;
    }

    /* Check the MEGID Format */
    u1Offset = u1Offset + ECFM_INCR_VAL;

    /* Verify if the received MEGID format is same as the configured one */
    if (pu1MegId[u1Offset] != ECFM_MEGID_FORMAT_TYPE)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC, "EcfmUtilCompareMEGID: "
                      "MEGID format does not match \r\n");
        return ECFM_FAILURE;
    }

    /* Check the length of the MEGID */
    u1Offset = u1Offset + ECFM_INCR_VAL;
    if (pu1MegId[u1Offset] != ECFM_MEG_ID_LEN)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC, "EcfmUtilCompareMEGID: "
                      "MEGID Length does not match \r\n");
        return ECFM_FAILURE;
    }
    u1IccLen = STRLEN (pMaInfo->au1IccCode);
    u1UmcLen = STRLEN (pMaInfo->au1UmcCode);

    u1Offset = u1Offset + ECFM_INCR_VAL;
    if ((u1IccLen + u1UmcLen) == ECFM_INIT_VAL)
    {
        u1MegIdLen = ((pMaInfo->u1NameLength > ECFM_MEG_ID_LEN)
                      ? ECFM_MEG_ID_LEN : pMaInfo->u1NameLength);
        /* ICC and UMC not provided, hence compare Short MA Name */
        if (ECFM_MEMCMP ((pu1MegId + u1Offset), pMaInfo->au1Name,
                         u1MegIdLen) != ECFM_INIT_VAL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcmRcvdSmCcmCompareMEGID: ICC + UMC does not match\r\n");
            return ECFM_FAILURE;
        }
        /* Move the pointer to Next 13 bytes. */
        u1Offset = u1Offset + u1MegIdLen;
    }
    else
    {
        /* Compare the Icc */
        if (ECFM_MEMCMP ((pu1MegId + u1Offset), pMaInfo->au1IccCode, u1IccLen)
            != 0)
        {
            ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmUtilCompareMEGID: "
                          "ICC value does not match \r\n");
            return ECFM_FAILURE;
        }
        /* Verify the Umc */
        u1Offset = u1Offset + u1IccLen;
        u1UmcCmpLen = (((u1IccLen + u1UmcLen) > ECFM_MEG_ID_LEN) ?
                       (ECFM_MEG_ID_LEN - u1IccLen) : u1UmcLen);
        if (ECFM_MEMCMP ((pu1MegId + u1Offset), pMaInfo->au1UmcCode,
                         u1UmcCmpLen) != 0)
        {
            ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmUtilCompareMEGID: "
                          "UMC value does not match \r\n");
            return ECFM_FAILURE;
        }
        u1Offset = u1Offset + u1UmcCmpLen;
    }
    /* The below logic checks even whether the trailing NULL bytes are
     * zeros in addition to Un-Used 32 bytes at the end.
     */
    if (u1Offset < ECFM_MEGID_FIELD_SIZE)
    {
        if (ECFM_MEMCMP ((pu1MegId + u1Offset), (au1PadVal + u1Offset),
                         (ECFM_MEGID_FIELD_SIZE - u1Offset)) != 0)
        {
            ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                          ECFM_ALL_FAILURE_TRC, "EcfmUtilCompareMEGID: "
                          "Remaining MEGID is not same \r\n");
            return ECFM_FAILURE;
        }
    }

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmCcUtilRestartAisTimer
 *
 * Description        : The routine will stop and Re-start the AIS transmission.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                                 information regarding Md info.
 *
 * Output(s)          : None
 *
 * Returns            : None
 ******************************************************************************/

VOID
EcfmCcUtilRestartAisTimer (tEcfmCcMdInfo * pMdInfo)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get the MA Assocated with this MD */
    pMaNode = RBTreeGetFirst (pMdInfo->MaTable);
    while (pMaNode != NULL)
    {
        /* Scan through all MEPs */
        TMO_DLL_Scan (&(pMaNode->MepTable), pMepNode, tEcfmCcMepInfo *)
            if ((pMepNode->pPortInfo != NULL) &&
                (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE))
        {
            /* Stop the AIS Transmission */
            EcfmCcInitStopAisTx (pMepNode);
            /* Send Mep Active event to AIS Initiator */
            EcfmCcClntAisInitiator (pMepNode, ECFM_EV_MEP_BEGIN);
        }
        pMaNode = RBTreeGetNext (pMdInfo->MaTable, pMaNode, NULL);
    }
    return;
}

/*****************************************************************************
* Function Name      : EcfmAllocateLtrMemBlk
* Description        : This function is called from the WEB module to
*                      allocate the memory for the LTR cache Indices
* Input(s)           : None
* Output(s)          : pLtr - Pointer to the Mem Block
* Return Value(s)    : pLtr
*****************************************************************************/
PUBLIC tEcfmLtrCacheIndices *
EcfmAllocateLtrMemBlk (VOID)
{
    tEcfmLtrCacheIndices *pLtr = NULL;
    pLtr = MemAllocMemBlk (ECFM_LTR_CACHE_INDICES_POOLID);
    return pLtr;
}

/************************************************************************
*  Function Name   : EcfmFreeLtrMemBlk                                 
*  Description     : Releases a block back to a Pool     
*  Input(s)        :                                                    
*  Output(s)       : pu1Block - Pointer to block being released.        
*  Returns         : ECFM_SUCCESS/ECFM_FAILURE
************************************************************************/

PUBLIC VOID
EcfmFreeLtrMemBlk (UINT1 *pu1Block)
{
    ECFM_FREE_MEM_BLOCK (ECFM_LTR_CACHE_INDICES_POOLID, pu1Block);
}

/************************************************************************
 * *  Function Name   : EcfmGetMacAddrOfPort
 * *  Description     : Copies Interface MAC address
 * *  Input(s)        : u4IfIndex - Interface Index
 * *                    MacAddr - Points to sallers array to hold MAC address
 * *  Output(s)       : MacAddr
 * *  Returns         : None
 * ************************************************************************/

PUBLIC VOID
EcfmGetMacAddrOfPort (UINT4 u4IfIndex, UINT1 *MacAddr)
{
    tCfaIfInfo          CfaInfo;
    if ((EcfmUtilGetIfInfo (u4IfIndex, &CfaInfo) != CFA_FAILURE))
    {
        ECFM_MEMCPY (MacAddr, CfaInfo.au1MacAddr, (ECFM_MAC_ADDR_LENGTH));
    }
}

/******************************************************************************/
/*                           End  of cfmutil.c                                */
/******************************************************************************/
