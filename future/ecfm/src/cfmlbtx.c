/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbtx.c,v 1.39 2014/12/26 10:27:08 siva Exp $
 *
 * Description: This file contains the functionality of the
 *              tranmsitter for LBLT Task in Control Sub Module
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmLbLtCtrlTxParsePduHdrs PROTO ((tEcfmLbLtPduSmInfo *));
PRIVATE INT4 EcfmLbLtAHCtrlTxTransmitPkt PROTO ((tEcfmBufChainHeader *,
                                                 UINT2, tEcfmVlanTag *,
                                                 tEcfmPbbTag *, UINT1, UINT1));
PRIVATE tEcfmBufChainHeader *EcfmLbLtCtrlTxDupCruBuf
PROTO ((tEcfmBufChainHeader *));
PRIVATE             BOOL1
    EcfmLbLtTxCheckLckCondition PROTO ((UINT1, UINT2, UINT4, UINT1));

/******************************************************************************
 * Function Name      : EcfmLbLtADCtrlTxTransmitPkt
 *
 * Description        : This routine is used to transmit the CFM-PDU. It 
 *                      transmits the CRU buffer to the frame filtering or port
 *                      depending upon the Mp Direction
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Port number
 *                      u2VlanId - Vlan tag information of the CRU-Buffer
 *                      u1MpDirection - Direction of the transmitting MP UP/DOWN
 *                      u1OpCOde - OpCOde
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtADCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                             tEcfmVlanTag * pVlanTag, UINT1 u1MpDirection,
                             UINT1 u1OpCode)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;

    ECFM_LBLT_TRC_FN_ENTRY ();

    ECFM_BUF_SET_OPCODE (pBuf, u1OpCode);
    /* Clearing values of PduSmInfo structure */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmLbLtPduSmInfo));
    PduSmInfo.pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    PduSmInfo.pBuf = pBuf;
    PduSmInfo.u1RxDirection = u1MpDirection;
    ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo), pVlanTag,
                 sizeof (tEcfmVlanTag));
    EcfmLbLtCtrlTxParsePduHdrs (&PduSmInfo);

    /* If MP is Vlan aware */
    if (ECFM_IS_MEP_VLAN_AWARE (pVlanTag->OuterVlanTag.u2VlanId))
    {
        PduSmInfo.u4RxVlanIdIsId = pVlanTag->OuterVlanTag.u2VlanId;
        if (EcfmLbLtTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                         u2LocalPort,
                                         PduSmInfo.u4RxVlanIdIsId,
                                         u1MpDirection) == ECFM_TRUE)
        {
            EcfmLbLtCtrlRxPktFree (pBuf);
            pBuf = NULL;
            return ECFM_SUCCESS;
        }
        /* If Down MP is Vlan aware */
        if (u1MpDirection == ECFM_MP_DIR_DOWN)
        {
            /* Clear the LTI SAP Bit in the Reserved Field in the 
             * buffer */
            ECFM_LBLT_RESET_LTI_SAP_INDICATION (pBuf);
            if (EcfmLbLtADCtrlTxFwdToPort (pBuf, u2LocalPort, pVlanTag) ==
                ECFM_FAILURE)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtADCtrlTxTransmitPkt:"
                               "EcfmLbLtADCtrlTxFwdToPort returned"
                               "failure\r\n");
                return ECFM_FAILURE;
            }
        }
        else
        {
            /* If Up MP is Vlan aware then transmit the the CFM-PDU to the
             * frame-filtering 
             */

            /* In case the LTM is initated by a UP-MEP sent it to Linktrace 
               Responder through LTI SAP
             */
            switch (u1OpCode)
            {
                case ECFM_OPCODE_LTR:
                    /* Check if the LTR is in response to a LTM initiated from
                     * UP-MEP*/
                    if (ECFM_LBLT_IS_LTI_SAP_IND_PRESENT (pBuf))
                    {
                        EcfmLbLtCtrlRxLevelDeMux (&PduSmInfo);
                    }
                    else
                    {
                        if (EcfmLbLtADCtrlTxFwdToFf (&PduSmInfo) !=
                            ECFM_SUCCESS)
                        {
                            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                           ECFM_CONTROL_PLANE_TRC,
                                           "EcfmLbLtADCtrlTxTransmitPkt:"
                                           "EcfmLbLtADCtrlTxFwdToFf returned "
                                           "failure\r\n");
                            return ECFM_FAILURE;
                        }
                    }
                    break;
                case ECFM_OPCODE_LTM:
                    /* Set LTI Bit in the reserved */
                    ECFM_LBLT_SET_LTI_SAP_INDICATION (pBuf);
                    EcfmLbLtCtrlRxLevelDeMux (&PduSmInfo);
                   break;
                default:
                    /* For all other PDUs send them to frame filtering */
                    if (EcfmLbLtADCtrlTxFwdToFf (&PduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort,
                                                           u1OpCode);
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtADCtrlTxTransmitPkt:"
                                       "EcfmLbLtADCtrlTxFwdToFf returned "
                                       "failure\r\n");
                        return ECFM_FAILURE;
                    }

                    break;
            }
        }                        /* end of If up/Down MP is Vlan aware */
        EcfmLbLtCtrlRxPktFree (pBuf);
        pBuf = NULL;
    }
    else                        /* If MP is Vlan un-aware */
    {
        /* Routine posts the PDU to CFA task */
        UINT4               u4PduLength = ECFM_INIT_VAL;    /* No of bytes of PDU */
        u4PduLength = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

        if (EcfmLbLtTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                         u2LocalPort,
                                         PduSmInfo.u4RxVlanIdIsId,
                                         u1MpDirection) == ECFM_TRUE)
        {
            EcfmLbLtCtrlRxPktFree (pBuf);
            pBuf = NULL;
            return ECFM_SUCCESS;
        }

        if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "ECFM: NODE NOT ACTIVE- Dropping the "
                           "packet \r\n");
            EcfmLbLtCtrlRxPktFree (pBuf);
        }
        else
        {
            if ((ECFM_LBLT_GET_PORT_INFO (u2LocalPort) != NULL) &&
                (ECFM_LBLT_PORT_INFO (u2LocalPort)->u1IfOperStatus) !=
                CFA_IF_UP)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtCtrlTxTransmitPkt: Port is DOWN "
                               "Packet Transmission Failed \r\n");
                ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
                ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
                ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT
                    (PduSmInfo.pPortInfo->u4ContextId);
                return ECFM_FAILURE;
            }
            if (ECFM_LBLT_GET_PORT_INFO (u2LocalPort) == NULL)
            {
                return ECFM_FAILURE;
            }
            /* Routine posts the PDU to CFA task */
            if (EcfmL2IwfHandleOutgoingPktOnPort
                (pBuf, ECFM_LBLT_PORT_INFO (u2LocalPort)->u4IfIndex,
                 u4PduLength, ECFM_PROT_BPDU, ECFM_ENCAP_NONE) != L2IWF_SUCCESS)
            {
                ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtADCtrlTxTransmitPkt: Transmit CFMPDU to a"
                               "Port failed\r\n");
                ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
                ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT
                    (PduSmInfo.pPortInfo->u4ContextId);
                return ECFM_FAILURE;
            }
            ECFM_LBLT_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
            ECFM_LBLT_INCR_CTX_TX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->
                                                 u4ContextId);
            ECFM_LBLT_INCR_TX_COUNT (u2LocalPort, u1OpCode);
            ECFM_LBLT_INCR_CTX_TX_COUNT (PduSmInfo.pPortInfo->u4ContextId,
                                         u1OpCode);
        }
    }                            /* end of If MP is Vlan aware/Unaware */
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtADCtrlTxFwdToFf
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *                      which is received from frame filtering .
 *                      It applies the egress rules to the CFM-PDU before 
 *                      passing it to the port.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                                   structure holds the information about the 
 *                                   MP and the so far parsed CFM-PDU.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtADCtrlTxFwdToFf (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtPduSmInfo  DupPduSmInfo;
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmBufChainHeader *pCruBuf = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2Vid = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT1              *pu1FwdPortList = NULL;
    tEcfmMacAddr        DestAddr;
    tEcfmMacAddr        SrcAddr;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocating memory for FwdPortList */
    pu1FwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1FwdPortList == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToFf: Error in Allocating memory "
                       "for FwdPortList\r\n");
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pu1FwdPortList, 0, sizeof (tLocalPortListExt));
    pPortInfo = pPduSmInfo->pPortInfo;
    u2LocalPort = pPortInfo->u2PortNum;
    pCruBuf = pPduSmInfo->pBuf;
    /* Get the VID of the packet */
    u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
    ECFM_MEMSET (SrcAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (DestAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));

    ECFM_COPY_FROM_CRU_BUF (pCruBuf, DestAddr, 0, ECFM_MAC_ADDR_LENGTH);
    ECFM_COPY_FROM_CRU_BUF (pCruBuf, SrcAddr, ECFM_MAC_ADDR_LENGTH,
                            ECFM_MAC_ADDR_LENGTH);

    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLbLtADCtrlTxFwdToFf: local-port = %d vid = %d\r\n",
                        u2LocalPort, u2Vid);
    if (EcfmVlanGetFwdPortList
        (ECFM_LBLT_CURR_CONTEXT_ID (), u2LocalPort, SrcAddr, DestAddr, u2Vid,
         pu1FwdPortList) != VLAN_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
        /* Flood the packet if it is unicast as no forwarding information was 
         * found */
        if (ECFM_IS_MULTICAST_ADDR (DestAddr) != ECFM_TRUE)
        {
            if (EcfmL2IwfMiGetVlanEgressPorts
                (ECFM_LBLT_CURR_CONTEXT_ID (), u2Vid,
                 pu1FwdPortList) != L2IWF_SUCCESS)
            {
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
        else
        {
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
    }

    /* Disable the port on which the packet was received */
    ECFM_RESET_MEMBER_PORT (pu1FwdPortList, u2LocalPort);
    /*Get the egress Port and forward the packet */
    for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
         u2ByteInd++)
    {
        if (pu1FwdPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = pu1FwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {
            u2Port =
                (u2ByteInd * BITS_PER_BYTE) +
                EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
            /*Reset the port info */
            pPortInfo = NULL;
            /*Get the port info */
            pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2Port);
            if (pPortInfo != NULL)
            {
                ECFM_LBLT_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                    "EcfmLbLtADCtrlTxFwdToFf: egress-port = %d\r\n",
                                    u2Port);
                pDupCruBuf = EcfmLbLtCtrlTxDupCruBuf (pCruBuf);
                if (pDupCruBuf != NULL)
                {
                    /*Duplicate PDU-SM info */
                    ECFM_MEMCPY (&DupPduSmInfo, pPduSmInfo,
                                 sizeof (tEcfmLbLtPduSmInfo));
                    DupPduSmInfo.pBuf = pDupCruBuf;
                    DupPduSmInfo.u1RxDirection = ECFM_MP_DIR_UP;
                    DupPduSmInfo.pPortInfo = pPortInfo;
                    DupPduSmInfo.pMepInfo = NULL;
                    DupPduSmInfo.pMipInfo = NULL;
                    DupPduSmInfo.pStackInfo = NULL;
                    if (EcfmLbLtCtrlRxLevelDeMux (&DupPduSmInfo) !=
                        ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtADCtrlTxFwdToFf: Level DeMux"
                                       "returned failure\r\n");
                        ECFM_LBLT_INCR_DSRD_CFM_PDU_COUNT (u2Port);
                    }
                    /*Free the duplicated buffer */
                    EcfmLbLtCtrlRxPktFree (pDupCruBuf);
                    pDupCruBuf = NULL;
                }
                else
                {
                    /* Unable to duplicate the buffer */
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtADCtrlTxFwdToFf: Error occurred in"
                                   "duplication of CRU-Buffer\r\n");
                }
            }
        }
    }
    /* Releasing Memory for FwdPortList */
    UtilPlstReleaseLocalPortList (pu1FwdPortList);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtADCtrlTxFwdToPort
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *                      which is received from frame filtering .
 *                      It applies the egress rules to the CFM-PDU before 
 *                      passing it to the port.
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Index of Interface on which CFM-PDU needs 
 *                      to be transmitted.
 *                      pVlanInfo - Vlan Tag information of the CRU Buffer
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtADCtrlTxFwdToPort (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                           tEcfmVlanTag * pVlanTag)
{
    tEcfmBufChainHeader *pDupBuf = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT1               u1OpCode = ECFM_INIT_VAL;

    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToPort:"
                       "No Port Information  \r\n");
        return ECFM_FAILURE;
    }
    /* Get OpCode from the buffer */
    u1OpCode = ECFM_BUF_GET_OPCODE (pBuf);

    if (pPortInfo->u1IfOperStatus != CFA_IF_UP)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToPort:"
                       "Port is DOWN, Cannot Transmit Packet \r\n");
        ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
        ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }
    pDupBuf = ECFM_DUPLICATE_CRU_BUF (pBuf);
    if (pDupBuf == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToPort:"
                       "unable to duplicate CRU-BUFFER \r\n");
        ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
        return ECFM_FAILURE;
    }

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToPort: NODE NOT ACTIVE- Dropping the "
                       "packet \r\n");
        EcfmLbLtCtrlRxPktFree (pDupBuf);
        pDupBuf = NULL;
    }
    else if (EcfmVlanTransmitCfmFrame (pDupBuf,
                                       ECFM_LBLT_CURR_CONTEXT_ID (),
                                       u2LocalPort, *pVlanTag,
                                       VLAN_CFM_FRAME) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToPort:"
                       "VlanTransmitCfmFrame returned" "failure\r\n");
        EcfmLbLtCtrlRxPktFree (pDupBuf);
        pDupBuf = NULL;
        ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_ARG1 (ECFM_DATA_PATH_TRC,
                        "EcfmLbLtADCtrlTxFwdToPort:"
                        "CFM-PDU Out From Port [%d]\r\n", u2LocalPort);
    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
    {
        ECFM_LBLT_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_CFM_PDU_COUNT (pPortInfo->u4ContextId);
        ECFM_LBLT_INCR_TX_COUNT (u2LocalPort, ECFM_BUF_GET_OPCODE (pBuf));
        ECFM_LBLT_INCR_CTX_TX_COUNT (pPortInfo->u4ContextId,
                                     ECFM_BUF_GET_OPCODE (pBuf));
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmLbLtCtrlTxParsePduHdrs 
 *
 * Description        : This is called to parse the received CFM PDU by 
 *                      extracting the Header values from that and places then 
 *                      into the tEcfmLbLtPduSmInfo Structure.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                                   structure holds the information about the 
 *                                   MP and the so far parsed CFM-PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS /ECFM_FAILURE
 *
 *****************************************************************************/
PRIVATE VOID
EcfmLbLtCtrlTxParsePduHdrs (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    UINT4               u4Offset = ECFM_INIT_VAL;    /* Stores offset */
    UINT2               u2TypeLength = ECFM_INIT_VAL;    /* Store CFM PDU 
                                                           Type */
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;    /* Stores first 
                                                           byte of CFM HDR */

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Copy the 6byte destination address from the recived PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxDestMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    /* Copy the 6byte Received source address from the LBM PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxSrcMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        if (pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            u4Offset = u4Offset + ECFM_VLAN_HDR_SIZE;
        }
        /* Check for InnerVlan Tag */
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            u4Offset = u4Offset + ECFM_ISID_HDR_SIZE;
        }
    }

    /* Check for OuterVlan Tag */
    if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        u4Offset = u4Offset + ECFM_VLAN_HDR_SIZE;
    }
    /* Check for InnerVlan Tag */
    if (pPduSmInfo->VlanClassificationInfo.InnerVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        u4Offset = u4Offset + ECFM_VLAN_HDR_SIZE;
    }
    /* Get Type Length */
    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset, u2TypeLength);

    /* Move the pointer by the 2 byte to get the MD LEVEL */
    u4Offset = u4Offset + ECFM_VLAN_TYPE_LEN_FIELD_SIZE;

    /* Check if TypeLength value contains ECFM_PDU_TYPE_CFM type then LLC Snap  
     * Header is present then move offset by 8 bytes
     */
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)
    {
        u4Offset = u4Offset + ECFM_LLC_SNAP_HDR_SIZE;
    }

    pPduSmInfo->u1CfmPduOffset = (UINT1) u4Offset;
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1MdLvlAndVer);

    /* Read the 3MSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxMdLevel = (UINT1) (ECFM_GET_MDLEVEL (u1MdLvlAndVer));

    /* Read the 5LSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxVersion = (UINT1) (ECFM_GET_VERSION (u1MdLvlAndVer));
    u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flag from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset,
                         pPduSmInfo->u1RxFirstTlvOffset);
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  * Name               : EcfmLbLtCtrlTxDupCruBuf 
  *
  * Description        : This is used to duplicate the CRU-Buffer
  *
  * Input(s)           : pSrcCruBuf - Pointer to source CRU-Buffer which needs
  *                      to be duplicated
  * Output(s)          : None
  *
  * Return Value(s)    : tEcfmBufChainHeader*
  *
 *****************************************************************************/

PRIVATE
    tEcfmBufChainHeader * EcfmLbLtCtrlTxDupCruBuf (tEcfmBufChainHeader *
                                                   pSrcCruBuf)
{
    UINT4               u4BufSize = ECFM_INIT_VAL;
    tEcfmBufChainHeader *pCruBuf = NULL;
    UINT1              *pu1Frame = NULL;
    UINT1               u1OpCode = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    u4BufSize = ECFM_GET_CRU_VALID_BYTE_COUNT (pSrcCruBuf);
    if (u4BufSize == ECFM_INIT_VAL)
    {
        return NULL;
    }
    pCruBuf = ECFM_ALLOC_CRU_BUF (u4BufSize, ECFM_INIT_VAL);
    if (pCruBuf == NULL)
    {
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return NULL;
    }
    pu1Frame = ECFM_GET_DATA_PTR_IF_LINEAR (pSrcCruBuf, 0, u4BufSize);
    if (pu1Frame == NULL)
    {
        pu1Frame = ECFM_LBLT_PDU;
        ECFM_MEMSET (pu1Frame, ECFM_INIT_VAL, u4BufSize);
        ECFM_COPY_FROM_CRU_BUF (pSrcCruBuf, pu1Frame, 0, u4BufSize);
    }
    ECFM_COPY_OVER_CRU_BUF (pCruBuf, pu1Frame, 0, u4BufSize);
    u1OpCode = ECFM_BUF_GET_OPCODE (pSrcCruBuf);
    ECFM_BUF_SET_OPCODE (pCruBuf, u1OpCode);
    ECFM_LBLT_TRC_FN_EXIT ();
    return pCruBuf;
}

/******************************************************************************
 * Function Name      : EcfmLbLtAHCtrlTxFwdToPort
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *                      which is received from frame filtering .
 *                      It applies the egress rules to the CFM-PDU before 
 *                      passing it to the port.
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Index of Interface on which CFM-PDU needs 
 *                      to be transmitted.
 *                      pVlanInfo - Vlan Tag information of the CRU Buffer
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtAHCtrlTxFwdToPort (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                           tEcfmVlanTag * pVlanTag, tEcfmPbbTag * pPbbTag)
{
    tEcfmBufChainHeader *pDupBuf = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT1               u1OpCode = ECFM_INIT_VAL;
    BOOL1               b1MemFlag = ECFM_FALSE;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    if (pPortInfo == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlTxFwdToPort:"
                       " No Port Information present \r\n");
        return ECFM_FAILURE;
    }
    /* Get OpCode from the buffer */
    u1OpCode = ECFM_BUF_GET_OPCODE (pBuf);

    if (pPortInfo->u1IfOperStatus != CFA_IF_UP)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtCtrlTxFwdToPort:"
                       "Port is DOWN, Cannot Transmit Packet \r\n");
        ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
        ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }
    pDupBuf = ECFM_DUPLICATE_CRU_BUF (pBuf);
    if (pDupBuf == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtAHCtrlTxFwdToPort:"
                       "unable to duplicate CRU-BUFFER \r\n");
        ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        ECFM_LBLT_HANDLE_TRANSMIT_FAILURE (u2LocalPort, u1OpCode);
        return ECFM_FAILURE;
    }

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtAHCtrlTxFwdToPort: NODE NOT ACTIVE- Dropping the "
                       "packet \r\n");
        b1MemFlag = ECFM_TRUE;

    }
    else if (EcfmL2IwfTransmitCfmFrame (pDupBuf, pPortInfo->u4IfIndex,
                                        *pVlanTag, *pPbbTag,
                                        L2_ECFM_PACKET) == L2IWF_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtAHCtrlTxFwdToPort:"
                       "VlanTransmitCfmFrame returned" "failure\r\n");
        ECFM_LBLT_INCR_TX_FAILED_COUNT (u2LocalPort);
        ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (pPortInfo->u4ContextId);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_ARG1 (ECFM_DATA_PATH_TRC,
                        "EcfmLbLtAHCtrlTxFwdToPort:"
                        "CFM-PDU Out From Port [%d]\r\n", u2LocalPort);
    ECFM_LBLT_INCR_TX_CFM_PDU_COUNT (u2LocalPort);
    ECFM_LBLT_INCR_TX_COUNT (u2LocalPort, ECFM_BUF_GET_OPCODE (pBuf));
    ECFM_LBLT_INCR_CTX_TX_CFM_PDU_COUNT (pPortInfo->u4ContextId);
    ECFM_LBLT_INCR_CTX_TX_COUNT (pPortInfo->u4ContextId,
                                 ECFM_BUF_GET_OPCODE (pBuf));

    if (b1MemFlag == ECFM_TRUE)
    {
        ECFM_RELEASE_CRU_BUF (pDupBuf, FALSE);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtAHCtrlTxFwdToFf
 *
 * Description        : This routine is used to forward the CFM-PDU to the port 
 *                      which is received from frame filtering .
 *                      It applies the egress rules to the CFM-PDU before 
 *                      passing it to the port.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                                   structure holds the information about the 
 *                                   MP and the so far parsed CFM-PDU.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtAHCtrlTxFwdToFf (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtPduSmInfo  DupPduSmInfo;
    tEcfmBufChainHeader *pDupCruBuf = NULL;
    tEcfmBufChainHeader *pCruBuf = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2PipPort = ECFM_INIT_VAL;
    UINT2               u2VipPort = ECFM_INIT_VAL;
    UINT2               u2Vid = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    UINT1              *pu1FwdPortList = NULL;
    tEcfmMacAddr        DestAddr;
    tEcfmMacAddr        SrcAddr;

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocating memory for FwdPortList */
    pu1FwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));

    if (pu1FwdPortList == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                       "EcfmLbLtADCtrlTxFwdToFf: Error in Allocating memory "
                       "for FwdPortList\r\n");
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pu1FwdPortList, 0, sizeof (tLocalPortListExt));

    pPortInfo = pPduSmInfo->pPortInfo;
    u2LocalPort = pPortInfo->u2PortNum;
    pCruBuf = pPduSmInfo->pBuf;
    /* Get the VID of the packet */
    u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
    ECFM_MEMSET (SrcAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_MEMSET (DestAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));

    ECFM_COPY_FROM_CRU_BUF (pCruBuf, DestAddr, 0, ECFM_MAC_ADDR_LENGTH);
    ECFM_COPY_FROM_CRU_BUF (pCruBuf, SrcAddr, ECFM_MAC_ADDR_LENGTH,
                            ECFM_MAC_ADDR_LENGTH);

    ECFM_LBLT_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                        "EcfmLbLtAHCtrlTxFwdToFf: local-port = %d vid = %d\r\n",
                        u2LocalPort, u2Vid);
    if (pPduSmInfo->u1RxPbbPortType == ECFM_PROVIDER_NETWORK_PORT)
    {
        pPduSmInfo->u4RxVlanIdIsId =
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId;
        /* If we have ISID tagged-pdu we need to strip off the BDA, BDA, I-TAG
         * and B-TAG if any, so that they can be newly added
         */
        /* get the port list associated with B-vid this list includes 
         * the CBP ports and PNP port also */
        if (EcfmVlanGetFwdPortList (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    u2LocalPort, SrcAddr, DestAddr,
                                    pPduSmInfo->PbbClassificationInfo.
                                    OuterVlanTag.u2VlanId,
                                    pu1FwdPortList) != ECFM_SUCCESS)
        {
            if (EcfmL2IwfMiGetVlanEgressPorts
                (ECFM_LBLT_CURR_CONTEXT_ID (),
                 pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId,
                 pu1FwdPortList) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
    }
    else if (pPduSmInfo->u1RxPbbPortType == ECFM_CUSTOMER_BACKBONE_PORT)
    {
        /* Check if we already have a B-TAG in the frame, this is the case when
         * we are replying to LBMs
         */
        if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0)
        {
            if (EcfmPbbGetBvidForIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                       u2LocalPort,
                                       pPduSmInfo->PbbClassificationInfo.
                                       InnerIsidTag.u4Isid,
                                       &u2Vid) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtAHCtrlTxFwdToFf: Unable to B-vid for the Isid\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId = u2Vid;
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_UNTAGGED;
        }
        else
        {
            u2Vid = pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u2VlanId;
        }
        if (EcfmVlanGetFwdPortList
            (ECFM_LBLT_CURR_CONTEXT_ID (), u2LocalPort,
             SrcAddr, DestAddr, u2Vid, pu1FwdPortList) != ECFM_SUCCESS)
        {
            if (EcfmL2IwfMiGetVlanEgressPorts
                (ECFM_LBLT_CURR_CONTEXT_ID (), u2Vid,
                 pu1FwdPortList) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
        /* CBP information is lost at the PNP so we need to fill the BDA, and fill the
         * BSA here only.
         */
        /* for the PDU comming from PIP BDA is swapped at the ingress only */
        if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType !=
             VLAN_TAGGED) &&
            (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u4Isid != 0))
        {
            if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1UcaBitValue ==
                 OSIX_TRUE)
                &&
                (ECFM_IS_MULTICAST_ADDR
                 (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.CDAMacAddr) ==
                 ECFM_TRUE))
            {
                EcfmPbbGetBCompBDA (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    u2LocalPort,
                                    pPduSmInfo->PbbClassificationInfo.
                                    InnerIsidTag.u4Isid,
                                    pPduSmInfo->PbbClassificationInfo.
                                    BDAMacAddr, L2IWF_INGRESS, OSIX_TRUE);
            }
            else
            {
                ECFM_MEMCPY (pPduSmInfo->PbbClassificationInfo.BDAMacAddr,
                             pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                             CDAMacAddr, ECFM_MAC_ADDR_LENGTH);
            }

            if ((pTempLbLtPortInfo =
                 ECFM_LBLT_GET_PORT_INFO (u2LocalPort)) != NULL)
            {
                ECFM_GET_MAC_ADDR_OF_PORT (pTempLbLtPortInfo->u4IfIndex,
                                           pPduSmInfo->PbbClassificationInfo.
                                           BSAMacAddr);
            }
            else
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtAHCtrlTxFwdToFf: "
                               "Unable to get Port Info\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
        /* Demux at PNPs for the B-VID */
        pPduSmInfo->u4RxVlanIdIsId = u2Vid;
    }
    else if (pPduSmInfo->u1RxPbbPortType == ECFM_PROVIDER_INSTANCE_PORT)
    {
        if (EcfmPbbGetPipVipWithIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->PbbClassificationInfo.
                                      InnerIsidTag.u4Isid, &u2VipPort,
                                      &u2PipPort) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: "
                           "Unable to get VIP information for the ISID\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        /* Get the VIP Corresponding to ISID 
         * remove this VIP from the port list associated with the Vid
         * received from Vlan */
        u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        /* Get the VIP Corresponding to ISID 
         * remove this VIP from the port list associated with the Vid
         * received from Vlan */
        u2Vid = pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        /* Check the spanning tree port state of the VIP port */
        if (ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_LBLT_CURR_CONTEXT_ID (),
                                              u2VipPort,
                                              &u4IfIndex) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: Unable to IfIndex from local-port\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        if (EcfmL2IwfGetVlanPortState (u2Vid, u4IfIndex) !=
            AST_PORT_STATE_FORWARDING)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: spanning tree port state of VIP is not"
                           " forwarding\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
            ECFM_VLAN_TAGGED)
        {
            /* get the forward port list for this S/c vlan id */
            if (EcfmVlanGetFwdPortList
                (ECFM_LBLT_CURR_CONTEXT_ID (), u2VipPort,
                 SrcAddr, DestAddr, u2Vid, pu1FwdPortList) != ECFM_SUCCESS)
            {
                if (EcfmL2IwfMiGetVlanEgressPorts
                    (ECFM_LBLT_CURR_CONTEXT_ID (),
                     u2Vid, pu1FwdPortList) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtAHCtrlTxFwdToFf:"
                                   "Unable to get Egress Ports\r\n");
                    /* Releasing Memory for FwdPortList */
                    UtilPlstReleaseLocalPortList (pu1FwdPortList);
                    return ECFM_FAILURE;
                }
            }
        }
        else
        {
            /* First check for CNP port with the PISID, if np such CNP is found
             * the use the PVID of the VIP to get the CNP port*/
            if (EcfmPbbGetCnpMemberPortsForIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                                 pPduSmInfo->
                                                 PbbClassificationInfo.
                                                 InnerIsidTag.u4Isid,
                                                 pu1FwdPortList) !=
                ECFM_SUCCESS)
            {
                if (EcfmL2IwfMiGetVlanEgressPorts (ECFM_LBLT_CURR_CONTEXT_ID (),
                                                   u2Vid,
                                                   pu1FwdPortList) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtAHCtrlTxFwdToFf:"
                                   "Unable to get Egress Ports\r\n");
                    /* Releasing Memory for FwdPortList */
                    UtilPlstReleaseLocalPortList (pu1FwdPortList);
                    return ECFM_FAILURE;
                }
            }
            else
            {
                /* No C/S-VLAN present in the PDU */
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId = 0;
            }
        }
    }
    else
    {
        UINT2               u2SVlanId = ECFM_INIT_VAL;
        /* CFM pdu generated local */
        if (EcfmPbbGetPipVipWithIsid (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->PbbClassificationInfo.
                                      InnerIsidTag.u4Isid, &u2VipPort,
                                      &u2PipPort) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: Unable to get Egress Ports\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType =
            VLAN_UNTAGGED;
        /* Note: no need to send EcfmLbLtCtrlRxLevelDeMux, for this no UP-MEP or MIP 
         * configured on PIP, we need to send directly to the PIP port */
        /* Check the spanning-tree port state of the VIP, MEP's configured on
         * PIP send the CFM-PDUs to the VIP first and then they go to the PIP,
         * so spanning tree check needs to be placed here.*/

        /* We dont have VIPs created in ECFM, so we need to use VCM for getting
         * the If-Index of the VIP*/
        if (ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_LBLT_CURR_CONTEXT_ID (),
                                              u2VipPort,
                                              &u4IfIndex) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: Unable to IfIndex from local-port\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId !=
            ECFM_INIT_VAL)
        {
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1Priority =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1Priority;
            pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1DropEligible =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1DropEligible;
            u2SVlanId =
                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId;
        }
        else
        {
            /* Get the PVID of the CNP port */
            if (EcfmL2IwfGetVlanPortPvid (pPortInfo->u4IfIndex, &u2SVlanId) !=
                ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtAHCtrlTxFwdToFf: Unable to get the PVID of the VIP port\r\n");
                /* Releasing Memory for FwdPortList */
                UtilPlstReleaseLocalPortList (pu1FwdPortList);
                return ECFM_FAILURE;
            }
        }
        if (EcfmL2IwfGetVlanPortState
            (u2SVlanId, u4IfIndex) != AST_PORT_STATE_FORWARDING)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: spanning tree port state of VIP is not"
                           " forwarding\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        /* Note: there is no need to do the demux for all the member CNP ports,
         * this CFM-PDU will have UCA bit as ON can we cannot send PBBN CFM-PDU
         * to the PB/C network*/
        /* Transmit to the corresping VIP */
        pDupCruBuf = ECFM_DUPLICATE_CRU_BUF (pCruBuf);
        if (pDupCruBuf == NULL)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: unable to duplicate CRU buffer\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }

        if (EcfmL2IwfTransmitFrameOnVip (ECFM_LBLT_CURR_CONTEXT_ID (),
                                         u4IfIndex, pDupCruBuf,
                                         &(pPduSmInfo->VlanClassificationInfo),
                                         &(pPduSmInfo->PbbClassificationInfo),
                                         L2_ECFM_PACKET) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxFwdToFf: unable to Transmit "
                           "frame on VIP\r\n");
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_FAILURE;
        }
        else
        {
            /* Releasing Memory for FwdPortList */
            UtilPlstReleaseLocalPortList (pu1FwdPortList);
            return ECFM_SUCCESS;
        }
    }
    /* Disable the port on which the packet was received */
    ECFM_RESET_MEMBER_PORT (pu1FwdPortList, u2LocalPort);
    /*Get the egress Port and forward the packet */
    for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
         u2ByteInd++)
    {
        if (pu1FwdPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = pu1FwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE)
              && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex) != 0));
             u2BitIndex++)
        {
            u2Port =
                (u2ByteInd * BITS_PER_BYTE) +
                EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex);
            /*Reset the port info */
            pPortInfo = NULL;
            /*Get the port info */
            pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2Port);
            if (pPortInfo != NULL)
            {
                ECFM_LBLT_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                                    "EcfmLbLtAHCtrlTxFwdToFf: egress-port = %d\r\n",
                                    u2Port);
                pDupCruBuf = EcfmLbLtCtrlTxDupCruBuf (pCruBuf);
                if (pDupCruBuf != NULL)
                {
                    /*Duplicate PDU-SM info */
                    ECFM_MEMCPY (&DupPduSmInfo, pPduSmInfo,
                                 sizeof (tEcfmLbLtPduSmInfo));
                    DupPduSmInfo.pBuf = pDupCruBuf;
                    DupPduSmInfo.u1RxDirection = ECFM_MP_DIR_UP;
                    DupPduSmInfo.pPortInfo = pPortInfo;
                    DupPduSmInfo.pMepInfo = NULL;
                    DupPduSmInfo.pMipInfo = NULL;
                    DupPduSmInfo.pStackInfo = NULL;
                    DupPduSmInfo.u1RxPbbPortType = pPortInfo->u1PortType;
                    if (EcfmLbLtCtrlRxLevelDeMux (&DupPduSmInfo) !=
                        ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtAHCtrlTxFwdToFf: Level DeMux"
                                       "returned failure\r\n");
                        ECFM_LBLT_INCR_DSRD_CFM_PDU_COUNT (u2Port);
                    }
                    /*Free the duplicated buffer */
                    EcfmLbLtCtrlRxPktFree (pDupCruBuf);
                    pDupCruBuf = NULL;
                }
                else
                {
                    /* Unable to duplicate the buffer */
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtAHCtrlTxFwdToFf: Error occurred in"
                                   "duplication of CRU-Buffer\r\n");
                }
            }
        }
    }
    /* Releasing Memory for FwdPortList */
    UtilPlstReleaseLocalPortList (pu1FwdPortList);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtAHCtrlTxTransmitPkt
 *
 * Description        : This routine is used to transmit the CFM-PDU. It 
 *                      transmits the CRU buffer to the frame filtering or port
 *                      depending upon the Mp Direction
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2LocalPort - Port number
 *                      u2VlanId - Vlan tag information of the CRU-Buffer
 *                      u1MpDirection - Direction of the transmitting MP UP/DOWN
 *                      u1OpCOde - OpCOde
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtAHCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, UINT2 u2LocalPort,
                             tEcfmVlanTag * pVlanTag, tEcfmPbbTag * pPbbTag,
                             UINT1 u1MpDirection, UINT1 u1OpCode)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;

    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_BUF_SET_OPCODE (pBuf, u1OpCode);
    /* Clearing values of PduSmInfo structure */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmLbLtPduSmInfo));
    PduSmInfo.pPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    PduSmInfo.u1RxPbbPortType = ECFM_LBLT_GET_PORT_TYPE (u2LocalPort);
    PduSmInfo.pBuf = pBuf;
    PduSmInfo.u4RxVlanIdIsId =
        ECFM_ISID_TO_ISID_INTERNAL (pPbbTag->InnerIsidTag.u4Isid);
    PduSmInfo.u1RxDirection = u1MpDirection;
    ECFM_MEMCPY (&(PduSmInfo.VlanClassificationInfo), pVlanTag,
                 sizeof (tEcfmVlanTag));
    ECFM_MEMCPY (&(PduSmInfo.PbbClassificationInfo), pPbbTag,
                 sizeof (tEcfmPbbTag));
    EcfmLbLtCtrlTxParsePduHdrs (&PduSmInfo);

    if (EcfmLbLtTxCheckLckCondition (PduSmInfo.u1RxMdLevel,
                                     u2LocalPort,
                                     PduSmInfo.u4RxVlanIdIsId,
                                     u1MpDirection) == ECFM_TRUE)
    {
        EcfmLbLtCtrlRxPktFree (pBuf);
        pBuf = NULL;
        return ECFM_SUCCESS;
    }
    /* If Down MP is Vlan aware */
    if (u1MpDirection == ECFM_MP_DIR_DOWN)
    {
        /* Check id the PDU is orginated from CNP itself,
         * */
        if ((ECFM_LBLT_GET_PORT_TYPE (u2LocalPort) == ECFM_CNP_CTAGGED_PORT) ||
            (ECFM_LBLT_GET_PORT_TYPE (u2LocalPort) == ECFM_CNP_PORTBASED_PORT)
            || (ECFM_LBLT_GET_PORT_TYPE (u2LocalPort) == ECFM_CNP_STAGGED_PORT))
        {
            ECFM_MEMCPY (&(pVlanTag->OuterVlanTag), &(pPbbTag->OuterVlanTag),
                         sizeof (tVlanTagInfo));
        }
        /* Clear the LTI SAP Bit in the Reserved Field in the 
         * buffer */
        ECFM_LBLT_RESET_LTI_SAP_INDICATION (pBuf);
        if (EcfmLbLtAHCtrlTxFwdToPort (pBuf, u2LocalPort, pVlanTag, pPbbTag) ==
            ECFM_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtAHCtrlTxTransmitPkt:"
                           "EcfmLbLtADCtrlTxFwdToPort returned" "failure\r\n");
            return ECFM_FAILURE;
        }
    }
    else
    {
        /* If Up MP is Vlan aware then transmit the the CFM-PDU to the
         * frame-filtering 
         */

        /* In case the LTM is initated by a UP-MEP sent it to Linktrace 
           Responder through LTI SAP
         */
        switch (u1OpCode)
        {
            case ECFM_OPCODE_LTR:
                /* Check if the LTR is in response to a LTM initiated from
                 * UP-MEP*/
                if (ECFM_LBLT_IS_LTI_SAP_IND_PRESENT (pBuf))
                {
                    if (EcfmLbLtCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtAHCtrlTxTransmitPkt:"
                                       "EcfmLbLtCtrlRxLevelDeMux returned "
                                       "failure\r\n");
                        return ECFM_FAILURE;
                    }
                }
                else
                {
                    if (EcfmLbLtAHCtrlTxFwdToFf (&PduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC,
                                       "EcfmLbLtAHCtrlTxTransmitPkt:"
                                       "EcfmLbLtAHCtrlTxFwdToFf returned "
                                       "failure\r\n");
                        return ECFM_FAILURE;
                    }
                }
                break;
            case ECFM_OPCODE_LTM:
                /* Set LTI Bit in the reserved */
                ECFM_LBLT_SET_LTI_SAP_INDICATION (pBuf);
                if (pPbbTag->InnerIsidTag.u4Isid != 0)
                {
                    PduSmInfo.u4RxVlanIdIsId =
                        ECFM_ISID_TO_ISID_INTERNAL (pPbbTag->InnerIsidTag.
                                                    u4Isid);
                }
                else
                {
                    PduSmInfo.u4RxVlanIdIsId = pPbbTag->OuterVlanTag.u2VlanId;
                }
                if (EcfmLbLtCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtAHCtrlTxTransmitPkt:"
                                   "EcfmLbLtCtrlRxLevelDeMux returned "
                                   "failure\r\n");
                    return ECFM_FAILURE;
                }

                break;
            default:
                /* For all other PDUs send them to frame filtering */
                if (EcfmLbLtAHCtrlTxFwdToFf (&PduSmInfo) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC,
                                   "EcfmLbLtAHCtrlTxTransmitPkt:"
                                   "EcfmLbLtAHCtrlTxFwdToFf returned "
                                   "failure\r\n");
                    return ECFM_FAILURE;
                }

                break;
        }
    }                            /* end of If up/Down MP is Vlan aware */
    EcfmLbLtCtrlRxPktFree (pBuf);
    pBuf = NULL;
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtCtrlTxTransmitPkt
 *
 * Description        : This routine is used to transmit the CFM-PDU. It 
 *                      transmits the CRU buffer to the frame filtering or port
 *                      depending upon the Mp Direction
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet
 *                      u2PortNum - Port number
 *                      u4VlanIdIsid - VlanId or Isid of the MEP 
 *                      u1Priority - Vlan priority to be transmited in vlan tag
 *                      u1DropEligible - drop eligibility to be transmited in vlan 
 *                                       tag 
 *                      u1Direction - Direction of the MP
 *                      u1OpCode - OpCode of the CFM-PDU
 *                      pVlanInfo - VLAN tag/classification information
 *                      pPbbInfo -  PBB tag/classification information
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, UINT2 u2PortNum,
                           UINT4 u4VlanIdIsid, UINT1 u1Priority,
                           UINT1 u1DropEligible, UINT1 u1Direction,
                           UINT1 u1OpCode, tEcfmVlanTag * pVlanInfo,
                           tEcfmPbbTag * pPbbInfo)
{
    tEcfmVlanTag        VlanTag;
    INT4                i4RetVal = ECFM_SUCCESS;

    ECFM_LBLT_TRC_FN_ENTRY ();
    if (ECFM_LBLT_802_1AH_BRIDGE () == ECFM_FALSE)
    {
        if (pVlanInfo == NULL)
        {
            ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));
            /* Setting the value in Vlan Info structure */
            VlanTag.OuterVlanTag.u2VlanId = (UINT2) u4VlanIdIsid;
            VlanTag.OuterVlanTag.u1Priority = u1Priority;
            VlanTag.OuterVlanTag.u1DropEligible = u1DropEligible;
            VlanTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
            pVlanInfo = &VlanTag;
        }
        i4RetVal = EcfmLbLtADCtrlTxTransmitPkt (pBuf, u2PortNum,
                                                pVlanInfo, u1Direction,
                                                u1OpCode);
    }
    else
    {
        tEcfmPbbTag         PbbTag;
        if (pVlanInfo == NULL)
        {
            ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));
            VlanTag.OuterVlanTag.u2VlanId = ECFM_INIT_VAL;
            VlanTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
            pVlanInfo = &VlanTag;
        }
        if (pPbbInfo == NULL)
        {
            ECFM_MEMSET (&PbbTag, ECFM_INIT_VAL, sizeof (tEcfmPbbTag));
            PbbTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
            PbbTag.OuterVlanTag.u1Priority = u1Priority;
            PbbTag.OuterVlanTag.u1DropEligible = u1DropEligible;
            PbbTag.InnerIsidTag.u1TagType = ECFM_VLAN_UNTAGGED;
            PbbTag.InnerIsidTag.u1Priority = u1Priority;
            PbbTag.InnerIsidTag.u1DropEligible = u1DropEligible;
            if (ECFM_IS_MEP_ISID_AWARE (u4VlanIdIsid))
            {
                PbbTag.InnerIsidTag.u4Isid =
                    ECFM_ISID_INTERNAL_TO_ISID (u4VlanIdIsid);
                PbbTag.OuterVlanTag.u2VlanId = 0;
                /* UCA-Bit is only set for MEPs configured at CBP and PIP
                 * */
                if ((ECFM_LBLT_GET_PORT_TYPE (u2PortNum) ==
                     ECFM_CUSTOMER_BACKBONE_PORT)
                    || (ECFM_LBLT_GET_PORT_TYPE (u2PortNum) ==
                        ECFM_PROVIDER_INSTANCE_PORT))
                {
                    PbbTag.InnerIsidTag.u1UcaBitValue = OSIX_TRUE;
                }
            }
            else
            {
                PbbTag.OuterVlanTag.u2VlanId = u4VlanIdIsid;
                PbbTag.InnerIsidTag.u4Isid = 0;
            }
            pPbbInfo = &PbbTag;
        }

        ECFM_CRU_GET_STRING (pBuf, pPbbInfo->InnerIsidTag.CDAMacAddr, 0,
                             ECFM_MAC_ADDR_LENGTH);
        ECFM_CRU_GET_STRING (pBuf, pPbbInfo->InnerIsidTag.CSAMacAddr,
                             MAC_ADDR_LEN, ECFM_MAC_ADDR_LENGTH);
        i4RetVal = EcfmLbLtAHCtrlTxTransmitPkt (pBuf, u2PortNum,
                                                pVlanInfo, pPbbInfo,
                                                u1Direction, u1OpCode);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Name               : EcfmLbLtTxCheckLckCondition
 *
 * Description        : This routine is used to check is any lower level MEP
 *                      is locked.
 *
 * Input(s)           : u1MdLevel - MD Level of CFM PDU received.
 *                      u2LocalPort - Port on which CFM PDU received.
 *                      u4VlanIdIsid - Received CFM PDU's VlanId or ISID value  
 *                      u1MpDirection - Direction of MEP from which CFM PDU
 *                      received.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_TRUE/ECFM_FALSE
 *****************************************************************************/
PRIVATE             BOOL1
EcfmLbLtTxCheckLckCondition (UINT1 u1MdLevel, UINT2 u2LocalPort,
                             UINT4 u4VlanIdIsid, UINT1 u1MpDirection)
{
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;

    INT1                i1MdLevel = ECFM_INIT_VAL;

    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (u2LocalPort) != ECFM_TRUE)
    {
        return ECFM_FALSE;
    }

    for (i1MdLevel = u1MdLevel - 1;
         i1MdLevel >= ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
    {
        pStackInfo = EcfmLbLtUtilGetMp (u2LocalPort, i1MdLevel,
                                        u4VlanIdIsid, u1MpDirection);
        if (pStackInfo != NULL)
        {
            if (ECFM_LBLT_IS_MEP (pStackInfo))
            {
                pMepInfo = pStackInfo->pLbLtMepInfo;

                if (ECFM_LBLT_LCK_IS_CONFIGURED (pMepInfo) == ECFM_TRUE)
                {
                    return ECFM_TRUE;
                }
            }
        }
    }
    return ECFM_FALSE;
}

/*****************************************************************************/
/*                               end of file cfmlbtx.c                       */
/*****************************************************************************/
