/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfmstklw.c,v 1.14 2014/03/16 11:34:12 siva Exp $
*
* Description: This file contains the Protocol Low Level Routines
 *                for Stack Table of standard ECFM MIB.
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "fscfmmcli.h"
#include  "cfminc.h"

/* LOW LEVEL Routines for Table : Ieee8021CfmStackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmStackTable
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmStackTable (INT4 i4Ieee8021CfmStackifIndex,
                                               INT4
                                               i4Ieee8021CfmStackServiceSelectorType,
                                               UINT4
                                               u4Ieee8021CfmStackServiceSelectorOrNone,
                                               INT4 i4Ieee8021CfmStackMdLevel,
                                               INT4 i4Ieee8021CfmStackDirection)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }
    UNUSED_PARAM (i4VlanIdIsidOrNone);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get Stack entry corresponding to indices - IfIndex, VlanId, MdLevel, 
     * Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4Ieee8021CfmStackServiceSelectorOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No entry exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices - IfIndex, VlanId, MdLevel, 
     * Direction exists */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmStackTable
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmStackTable (INT4 *pi4Ieee8021CfmStackifIndex,
                                       INT4
                                       *pi4Ieee8021CfmStackServiceSelectorType,
                                       UINT4
                                       *pu4Ieee8021CfmStackServiceSelectorOrNone,
                                       INT4 *pi4Ieee8021CfmStackMdLevel,
                                       INT4 *pi4Ieee8021CfmStackDirection)
{
    return (nmhGetNextIndexIeee8021CfmStackTable
            (0, pi4Ieee8021CfmStackifIndex, 0,
             pi4Ieee8021CfmStackServiceSelectorType, 0,
             pu4Ieee8021CfmStackServiceSelectorOrNone, 0,
             pi4Ieee8021CfmStackMdLevel, 0, pi4Ieee8021CfmStackDirection));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmStackTable
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                nextIeee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                nextIeee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                nextIeee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                nextIeee8021CfmStackMdLevel
                Ieee8021CfmStackDirection
                nextIeee8021CfmStackDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmStackTable (INT4 i4Ieee8021CfmStackifIndex,
                                      INT4 *pi4NextIeee8021CfmStackifIndex,
                                      INT4
                                      i4Ieee8021CfmStackServiceSelectorType,
                                      INT4
                                      *pi4NextIeee8021CfmStackServiceSelectorType,
                                      UINT4
                                      u4Ieee8021CfmStackServiceSelectorOrNone,
                                      UINT4
                                      *pu4NextIeee8021CfmStackServiceSelectorOrNone,
                                      INT4 i4Ieee8021CfmStackMdLevel,
                                      INT4 *pi4NextIeee8021CfmStackMdLevel,
                                      INT4 i4Ieee8021CfmStackDirection,
                                      INT4 *pi4NextIeee8021CfmStackDirection)
{

    tEcfmCcStackInfo    StackInfo;
    tEcfmCcStackInfo   *pStackNextNode = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get Stack entry corresponding to indices  next to IfIndex, VlanId, 
     * MdLevel and Direction */
    ECFM_MEMSET (&StackInfo, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);

    StackInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    StackInfo.u4IfIndex = (UINT2) i4Ieee8021CfmStackifIndex;

    if (i4Ieee8021CfmStackifIndex > OSIX_FALSE)
    {
        pTempPortInfo = ECFM_CC_GET_PORT_INFO (i4Ieee8021CfmStackifIndex);
        if (pTempPortInfo != NULL)
        {
            StackInfo.u4IfIndex = (UINT2) pTempPortInfo->u4IfIndex;
        }
    }

    StackInfo.u4VlanIdIsid = u4VlanIdIsidOrNone;
    StackInfo.u1MdLevel = (UINT1) i4Ieee8021CfmStackMdLevel;
    StackInfo.u1Direction = (UINT1) i4Ieee8021CfmStackDirection;

    pStackNextNode = (tEcfmCcStackInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) & StackInfo, NULL);

    if (pStackNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices next to IfIndex, VlanId, MdLevel, 
     * Direction exists */
    /* Set the next indices from corresponding Stack entry */
    *pi4NextIeee8021CfmStackifIndex = (INT4) (pStackNextNode->u2PortNum);
    if (((INT4) (pStackNextNode->u4VlanIdIsid)) >= ECFM_INTERNAL_ISID_MIN)
    {
        *pu4NextIeee8021CfmStackServiceSelectorOrNone =
            ECFM_ISID_INTERNAL_TO_ISID ((INT4) (pStackNextNode->u4VlanIdIsid));
        *pi4NextIeee8021CfmStackServiceSelectorType =
            ECFM_SERVICE_SELECTION_ISID;

    }
    else
    {
        *pu4NextIeee8021CfmStackServiceSelectorOrNone =
            (INT4) (pStackNextNode->u4VlanIdIsid);
        *pi4NextIeee8021CfmStackServiceSelectorType =
            ECFM_SERVICE_SELECTION_VLAN;
    }

    *pi4NextIeee8021CfmStackMdLevel = (INT4) (pStackNextNode->u1MdLevel);
    *pi4NextIeee8021CfmStackDirection = (INT4) (pStackNextNode->u1Direction);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMdIndex
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMdIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMdIndex (INT4 i4Ieee8021CfmStackifIndex,
                               INT4 i4Ieee8021CfmStackServiceSelectorType,
                               UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                               INT4 i4Ieee8021CfmStackMdLevel,
                               INT4 i4Ieee8021CfmStackDirection,
                               UINT4 *pu4RetValIeee8021CfmStackMdIndex)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */

    /* Set MdIndex from correponding stack entry */
    if (pStackNode->pMepInfo != NULL)
    {

        *pu4RetValIeee8021CfmStackMdIndex =
            pStackNode->pMepInfo->pMaInfo->pMdInfo->u4MdIndex;
    }
    else
    {
        *pu4RetValIeee8021CfmStackMdIndex = pStackNode->pMipInfo->u4MdIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMaIndex
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMaIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMaIndex (INT4 i4Ieee8021CfmStackifIndex,
                               INT4 i4Ieee8021CfmStackServiceSelectorType,
                               UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                               INT4 i4Ieee8021CfmStackMdLevel,
                               INT4 i4Ieee8021CfmStackDirection,
                               UINT4 *pu4RetValIeee8021CfmStackMaIndex)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    /* Set the MaIndex from corresponding Stack entry */
    if (pStackNode->pMepInfo != NULL)
    {
        *pu4RetValIeee8021CfmStackMaIndex =
            pStackNode->pMepInfo->pMaInfo->u4MaIndex;
    }
    else
    {
        *pu4RetValIeee8021CfmStackMaIndex = pStackNode->pMipInfo->u4MaIndex;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMepId
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMepId (INT4 i4Ieee8021CfmStackifIndex,
                             INT4 i4Ieee8021CfmStackServiceSelectorType,
                             UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                             INT4 i4Ieee8021CfmStackMdLevel,
                             INT4 i4Ieee8021CfmStackDirection,
                             UINT4 *pu4RetValIeee8021CfmStackMepId)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    /* Set the StackMepId from corresponding Stack entry */
    if (pStackNode->pMepInfo != NULL)
    {
        *pu4RetValIeee8021CfmStackMepId = pStackNode->pMepInfo->u2MepId;
    }
    else
    {
        *pu4RetValIeee8021CfmStackMepId = 0;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMacAddress
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMacAddress (INT4 i4Ieee8021CfmStackifIndex,
                                  INT4 i4Ieee8021CfmStackServiceSelectorType,
                                  UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                                  INT4 i4Ieee8021CfmStackMdLevel,
                                  INT4 i4Ieee8021CfmStackDirection,
                                  tMacAddr * pRetValIeee8021CfmStackMacAddress)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    /* Set the MP MacAddress corresponding to MP's IfIndex */
    if (ECFM_CC_GET_PORT_INFO (pStackNode->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Port Information \n");
        return SNMP_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_CC_PORT_INFO
                               (pStackNode->u2PortNum)->u4IfIndex,
                               pRetValIeee8021CfmStackMacAddress);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1agCfmStackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmStackTable
 Input       :  The Indices
                Dot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                Dot1agCfmStackDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmStackTable (INT4 i4Dot1agCfmStackifIndex,
                                             INT4 i4Dot1agCfmStackVlanIdOrNone,
                                             INT4 i4Dot1agCfmStackMdLevel,
                                             INT4 i4Dot1agCfmStackDirection)
{
    UNUSED_PARAM (i4Dot1agCfmStackifIndex);
    UNUSED_PARAM (i4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (i4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (i4Dot1agCfmStackDirection);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmStackTable
 Input       :  The Indices
                Dot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                Dot1agCfmStackDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmStackTable (INT4 *pi4Dot1agCfmStackifIndex,
                                     INT4 *pi4Dot1agCfmStackVlanIdOrNone,
                                     INT4 *pi4Dot1agCfmStackMdLevel,
                                     INT4 *pi4Dot1agCfmStackDirection)
{
    UNUSED_PARAM (pi4Dot1agCfmStackifIndex);
    UNUSED_PARAM (pi4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (pi4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (pi4Dot1agCfmStackDirection);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmStackTable
 Input       :  The Indices
                Dot1agCfmStackifIndex
                nextDot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                nextDot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                nextDot1agCfmStackMdLevel
                Dot1agCfmStackDirection
                nextDot1agCfmStackDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmStackTable (INT4 i4Dot1agCfmStackifIndex,
                                    INT4 *pi4NextDot1agCfmStackifIndex,
                                    INT4 i4Dot1agCfmStackVlanIdOrNone,
                                    INT4 *pi4NextDot1agCfmStackVlanIdOrNone,
                                    INT4 i4Dot1agCfmStackMdLevel,
                                    INT4 *pi4NextDot1agCfmStackMdLevel,
                                    INT4 i4Dot1agCfmStackDirection,
                                    INT4 *pi4NextDot1agCfmStackDirection)
{
    UNUSED_PARAM (i4Dot1agCfmStackifIndex);
    UNUSED_PARAM (i4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (i4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (i4Dot1agCfmStackDirection);
    UNUSED_PARAM (pi4NextDot1agCfmStackifIndex);
    UNUSED_PARAM (pi4NextDot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (pi4NextDot1agCfmStackMdLevel);
    UNUSED_PARAM (pi4NextDot1agCfmStackDirection);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmStackMdIndex
 Input       :  The Indices
                Dot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                Dot1agCfmStackDirection

                The Object 
                retValDot1agCfmStackMdIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmStackMdIndex (INT4 i4Dot1agCfmStackifIndex,
                             INT4 i4Dot1agCfmStackVlanIdOrNone,
                             INT4 i4Dot1agCfmStackMdLevel,
                             INT4 i4Dot1agCfmStackDirection,
                             UINT4 *pu4RetValDot1agCfmStackMdIndex)
{
    UNUSED_PARAM (i4Dot1agCfmStackifIndex);
    UNUSED_PARAM (i4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (i4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (i4Dot1agCfmStackDirection);
    UNUSED_PARAM (pu4RetValDot1agCfmStackMdIndex);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmStackMaIndex
 Input       :  The Indices
                Dot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                Dot1agCfmStackDirection

                The Object 
                retValDot1agCfmStackMaIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmStackMaIndex (INT4 i4Dot1agCfmStackifIndex,
                             INT4 i4Dot1agCfmStackVlanIdOrNone,
                             INT4 i4Dot1agCfmStackMdLevel,
                             INT4 i4Dot1agCfmStackDirection,
                             UINT4 *pu4RetValDot1agCfmStackMaIndex)
{

    UNUSED_PARAM (i4Dot1agCfmStackifIndex);
    UNUSED_PARAM (i4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (i4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (i4Dot1agCfmStackDirection);
    UNUSED_PARAM (pu4RetValDot1agCfmStackMaIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmStackMepId
 Input       :  The Indices
                Dot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                Dot1agCfmStackDirection

                The Object 
                retValDot1agCfmStackMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmStackMepId (INT4 i4Dot1agCfmStackifIndex,
                           INT4 i4Dot1agCfmStackVlanIdOrNone,
                           INT4 i4Dot1agCfmStackMdLevel,
                           INT4 i4Dot1agCfmStackDirection,
                           UINT4 *pu4RetValDot1agCfmStackMepId)
{
    UNUSED_PARAM (i4Dot1agCfmStackifIndex);
    UNUSED_PARAM (i4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (i4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (i4Dot1agCfmStackDirection);
    UNUSED_PARAM (pu4RetValDot1agCfmStackMepId);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetDot1agCfmStackMacAddress
 Input       :  The Indices
                Dot1agCfmStackifIndex
                Dot1agCfmStackVlanIdOrNone
                Dot1agCfmStackMdLevel
                Dot1agCfmStackDirection

                The Object 
                retValDot1agCfmStackMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmStackMacAddress (INT4 i4Dot1agCfmStackifIndex,
                                INT4 i4Dot1agCfmStackVlanIdOrNone,
                                INT4 i4Dot1agCfmStackMdLevel,
                                INT4 i4Dot1agCfmStackDirection,
                                tMacAddr * pRetValDot1agCfmStackMacAddress)
{
    UNUSED_PARAM (i4Dot1agCfmStackifIndex);
    UNUSED_PARAM (i4Dot1agCfmStackVlanIdOrNone);
    UNUSED_PARAM (i4Dot1agCfmStackMdLevel);
    UNUSED_PARAM (i4Dot1agCfmStackDirection);
    UNUSED_PARAM (pRetValDot1agCfmStackMacAddress);
    return SNMP_FAILURE;

}
