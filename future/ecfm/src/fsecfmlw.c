/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
*
 * $Id: fsecfmlw.c,v 1.90 2017/09/22 12:24:47 siva Exp $
*
 * Description: This file contains the low level routines for 
 *              proprietary MIB.
 *******************************************************************/

# include  "cfminc.h"
# include  "fscfmmcli.h"
# include "cfmdef.h"
extern UINT4        fscfmm[8];
extern UINT4        fsmiy1[9];
extern UINT4        fscfme[9];
extern UINT4        stdecf[6];
extern UINT4        fsecfm[8];
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmSystemControl
 Input       :  The Indices

                The Object 
                retValFsEcfmSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmSystemControl (INT4 *pi4RetValFsEcfmSystemControl)
{
    /* Returns the current system status */
    *pi4RetValFsEcfmSystemControl =
        ECFM_SYSTEM_STATUS (ECFM_CC_CURR_CONTEXT_ID ());
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmModuleStatus
 Input       :  The Indices

                The Object 
                retValFsEcfmModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmModuleStatus (INT4 *pi4RetValFsEcfmModuleStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmModuleStatus = ECFM_DISABLE;
        return SNMP_SUCCESS;
    }

    /* Returns the current module status */
    *pi4RetValFsEcfmModuleStatus =
        ECFM_MODULE_STATUS (ECFM_CC_CURR_CONTEXT_ID ());

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmOui
 Input       :  The Indices

                The Object 
                retValFsEcfmOui
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmOui (tSNMP_OCTET_STRING_TYPE * pRetValFsEcfmOui)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_INITIALISED () == ECFM_FALSE)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC, "SNMP:ECFM module is Initialised\r\n");
        pRetValFsEcfmOui->pu1_OctetList[0] = ECFM_INIT_VAL;
        pRetValFsEcfmOui->i4_Length = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }

    /* Returns the current OUI string along with its length */
    ECFM_MEMCPY (pRetValFsEcfmOui->pu1_OctetList,
                 ECFM_CC_ORG_UNIT_ID, ECFM_OUI_LENGTH);
    pRetValFsEcfmOui->i4_Length = ECFM_OUI_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmTraceOption
 Input       :  The Indices

                The Object 
                retValFsEcfmTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmTraceOption (INT4 *pi4RetValFsEcfmTraceOption)
{
    /* Returns the current Trace option */
    *pi4RetValFsEcfmTraceOption =
        ECFM_TRACE_OPTION (ECFM_CC_CURR_CONTEXT_ID ());

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmLtrCacheStatus
 Input       :  The Indices

                The Object 
                retValFsEcfmLtrCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmLtrCacheStatus (INT4 *pi4RetValFsEcfmLtrCacheStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmLtrCacheStatus = ECFM_DISABLE;
        return SNMP_SUCCESS;
    }

    /* Returns the LTR cache status */
    *pi4RetValFsEcfmLtrCacheStatus = ECFM_LBLT_LTR_CACHE_STATUS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmLtrCacheClear
 Input       :  The Indices

                The Object 
                retValFsEcfmLtrCacheClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmLtrCacheClear (INT4 *pi4RetValFsEcfmLtrCacheClear)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmLtrCacheClear = ECFM_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    /* Returns the current LTR clear status */
    *pi4RetValFsEcfmLtrCacheClear = ECFM_SNMP_FALSE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmLtrCacheHoldTime
 Input       :  The Indices

                The Object 
                retValFsEcfmLtrCacheHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmLtrCacheHoldTime (INT4 *pi4RetValFsEcfmLtrCacheHoldTime)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmLtrCacheHoldTime = ECFM_LTR_CACHE_DEF_HOLD_TIME;
        return SNMP_SUCCESS;
    }

    /* Returns the LTR cache hold time */
    *pi4RetValFsEcfmLtrCacheHoldTime = ECFM_LBLT_LTR_CACHE_HOLD_TIME;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmLtrCacheSize
 Input       :  The Indices

                The Object 
                retValFsEcfmLtrCacheSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmLtrCacheSize (INT4 *pi4RetValFsEcfmLtrCacheSize)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmLtrCacheSize = ECFM_LTR_CACHE_DEF_SIZE;
        return SNMP_SUCCESS;
    }

    /* Returns the LTR cache size */
    *pi4RetValFsEcfmLtrCacheSize = ECFM_LBLT_LTR_CACHE_SIZE;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmSystemControl
 Input       :  The Indices

                The Object 
                setValFsEcfmSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmSystemControl (INT4 i4SetValFsEcfmSystemControl)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Check if ECFM is already start/shutdown before setting this
     * object */
    if (ECFM_SYSTEM_STATUS (ECFM_CC_CURR_CONTEXT_ID ()) ==
        (UINT1) i4SetValFsEcfmSystemControl)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP: ECFM already in the same "
                     "System control state\r\n");
        return SNMP_SUCCESS;
    }

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmSystemControl, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Start the CC and LBLT Module */
    if (i4SetValFsEcfmSystemControl == ECFM_START)
    {
        if (EcfmUtilModuleStart () != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                         "SNMP: ECFM Module start FAILED\r\n");
            EcfmUtilModuleShutDown ();
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                              ECFM_CC_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmSystemControl));
            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
            return SNMP_FAILURE;
        }
    }
    /* Stop the CC and LBLT Module */
    else
    {
        EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                       ECFM_RED_SHUTDOWN_CMD,
                                       ECFM_INIT_VAL,
                                       ECFM_INIT_VAL,
                                       ECFM_INIT_VAL,
                                       ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_TRUE);

        EcfmUtilModuleShutDown ();
        /* Notify MSR about the module shutdown */
        EcfmNotifyProtocolShutdownStatus (ECFM_CC_CURR_CONTEXT_ID ());
    }
    /* Sending Trigger to MSR */
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmSystemControl));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmModuleStatus
 Input       :  The Indices

                The Object 
                setValFsEcfmModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmModuleStatus (INT4 i4SetValFsEcfmModuleStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                   ECFM_RED_MOD_STS_CHNG_CMD,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_TRUE);

    /* Check if ECFM is already enabled/disabled before setting this
     * object */
    if (ECFM_MODULE_STATUS (ECFM_CC_CURR_CONTEXT_ID ()) ==
        (UINT1) i4SetValFsEcfmModuleStatus)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP: ECFM already in the same" "state\r\n");
        return SNMP_SUCCESS;
    }

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmModuleStatus, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Enabling ECFM module */
    if (i4SetValFsEcfmModuleStatus == ECFM_ENABLE)
    {
        if (EcfmUtilModuleEnable () != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                         "SNMP: ECFM Module Enable Failed\r\n");

            EcfmUtilModuleDisable ();
            EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                           ECFM_RED_MOD_STS_CHNG_CMD,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_FALSE);

            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                              ECFM_CC_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmModuleStatus));
            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

            return SNMP_FAILURE;
        }
    }
    /* Disabling ECFM module */
    else
    {
        EcfmUtilModuleDisable ();
    }
    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmModuleStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmOui
 Input       :  The Indices

                The Object 
                setValFsEcfmOui
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmOui (tSNMP_OCTET_STRING_TYPE * pSetValFsEcfmOui)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    EcfmRedSynchHwAuditCmdEvtInfo (ECFM_INIT_VAL,
                                   ECFM_RED_OUI_CHANGE_CMD,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_TRUE);

    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Set OUI for CC module */
    ECFM_MEMCPY (ECFM_CC_ORG_UNIT_ID, pSetValFsEcfmOui->pu1_OctetList,
                 pSetValFsEcfmOui->i4_Length);

    /* Set OUI for LBLT module */
    EcfmLbLtConfOui (pSetValFsEcfmOui->pu1_OctetList);
    EcfmOffloadHandleOUIChange ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmOui, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ZERO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsEcfmOui));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmTraceOption
 Input       :  The Indices

                The Object 
                setValFsEcfmTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmTraceOption (INT4 i4SetValFsEcfmTraceOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Set cc trace option */
    ECFM_TRACE_OPTION (ECFM_CC_CURR_CONTEXT_ID ()) = i4SetValFsEcfmTraceOption;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmTraceOption, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmTraceOption));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmLtrCacheStatus
 Input       :  The Indices

                The Object 
                setValFsEcfmLtrCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmLtrCacheStatus (INT4 i4SetValFsEcfmLtrCacheStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4LtrCacheHoldTime = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }

    /* Check if LtrCache status is already enabled */
    if (i4SetValFsEcfmLtrCacheStatus == ECFM_LBLT_LTR_CACHE_STATUS)
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmLtrCacheStatus, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* check if it is to be enabled */
    if (i4SetValFsEcfmLtrCacheStatus == ECFM_ENABLE)
    {
        /* convert default time value in to seconds */
        u4LtrCacheHoldTime =
            (UINT4) ECFM_LBLT_LTR_CACHE_HOLD_TIME *ECFM_NUM_OF_SEC_IN_A_MIN *
            ECFM_NUM_OF_MSEC_IN_A_SEC;
        /* Start Ltr Cache timer for which lTR entries are to be retained */
        if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL,
                                   u4LtrCacheHoldTime) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                           "\tSNMP: LTR Hold Timer cannot be started\n");
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                              ECFM_LBLT_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmLtrCacheStatus));
            if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_FAILURE;
        }
        /* Set Ltr cache hold time */
        ECFM_LBLT_LTR_CACHE_HOLD_TIME = ECFM_LTR_CACHE_DEF_HOLD_TIME;
        /* Creating Mempool for LTR Table */
        if (ECFM_CREATE_MEM_POOL (ECFM_LBLT_LTR_INFO_SIZE,
                                  ECFM_LTR_CACHE_DEF_SIZE,
                                  MEM_DEFAULT_MEMORY_TYPE,
                                  &ECFM_LBLT_LTR_TABLE_POOL) ==
            ECFM_MEM_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                           ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_MGMT_TRC, "Creation of Mem Pool for LTR Info"
                           "FAILED!\n");
            EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL);
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              ECFM_LBLT_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmLtrCacheStatus));
            if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_FAILURE;
        }

        if (ECFM_CREATE_MEM_POOL (ECFM_LBLT_LTR_INFO_SIZE,
                                  ECFM_LTR_CACHE_DEF_SIZE,
                                  MEM_DEFAULT_MEMORY_TYPE,
                                  &ECFM_LBLT_LTR_TABLE_POOL) ==
            ECFM_MEM_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                           ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_MGMT_TRC, "Creation of Mem Pool for LTR Info"
                           "FAILED!\n");
            EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL);
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              ECFM_LBLT_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmLtrCacheStatus));
            if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_FAILURE;
        }

        if (EcfmLbltCreateMemPoolsForLtrTlvInfo (ECFM_LTR_CACHE_DEF_SIZE)
            == ECFM_FAILURE)
        {
            ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                           ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_MGMT_TRC, "Creation of Mem Pool for LTR TLV "
                           "Info FAILED!\n");
            EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL);
            if (ECFM_LBLT_LTR_TABLE_POOL != 0)
            {
                ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_TABLE_POOL);
                ECFM_LBLT_LTR_TABLE_POOL = ECFM_INIT_VAL;
            }
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              ECFM_LBLT_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmLtrCacheStatus));
            if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* It is to be disabled */
        EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL);
        /* Delete the mempool for LTR cache */
        RBTreeDrain (ECFM_LBLT_LTR_TABLE,
                     (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                     ECFM_LBLT_LTR_ENTRY);
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_TABLE_POOL);
        ECFM_LBLT_LTR_TABLE_POOL = ECFM_INIT_VAL;
    }
    ECFM_LBLT_LTR_CACHE_STATUS = (UINT1) i4SetValFsEcfmLtrCacheStatus;

    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_LBLT_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmLtrCacheStatus));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmLtrCacheClear
 Input       :  The Indices

                The Object 
                setValFsEcfmLtrCacheClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmLtrCacheClear (INT4 i4SetValFsEcfmLtrCacheClear)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Clear the LTR Table */
    UNUSED_PARAM (i4SetValFsEcfmLtrCacheClear);

    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmLtrCacheClear, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }
    RBTreeDrain (ECFM_LBLT_LTR_TABLE,
                 (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LTR_ENTRY);

    /* Delete the required LTM entries */
    EcfmSnmpLwDelMepLtmReplyList ();
    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", ECFM_LBLT_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmLtrCacheClear));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmLtrCacheHoldTime
 Input       :  The Indices

                The Object 
                setValFsEcfmLtrCacheHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmLtrCacheHoldTime (INT4 i4SetValFsEcfmLtrCacheHoldTime)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmLtrCacheHoldTime, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Start Ltr Cache timer for which lTR entries are to be retained */
    if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL,
                               (UINT2) i4SetValFsEcfmLtrCacheHoldTime *
                               ECFM_NUM_OF_SEC_IN_A_MIN *
                               ECFM_NUM_OF_MSEC_IN_A_SEC) != ECFM_SUCCESS)
    {
        CLI_SET_ERR (CLI_ECFM_MAX_TIMER_VALUE_SUPPORTED);
        ECFM_LBLT_TRC (ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC | ECFM_MGMT_TRC,
                       "LTR Hold Timer Start FAILED\r\n");
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_LBLT_CURR_CONTEXT_ID (),
                          i4SetValFsEcfmLtrCacheHoldTime));
        if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LTR_CACHE_HOLD_TIME = (UINT2) i4SetValFsEcfmLtrCacheHoldTime;
    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_LBLT_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmLtrCacheHoldTime));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmLtrCacheSize
 Input       :  The Indices

                The Object 
                setValFsEcfmLtrCacheSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmLtrCacheSize (INT4 i4SetValFsEcfmLtrCacheSize)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }
    if (ECFM_LBLT_LTR_CACHE_STATUS != ECFM_ENABLE)

    {
        ECFM_LBLT_LTR_CACHE_SIZE = (UINT2) i4SetValFsEcfmLtrCacheSize;
        return SNMP_SUCCESS;
    }

    /* Check if pool is already created */
    if (ECFM_LBLT_LTR_TABLE_POOL != 0)
    {
        RBTreeDrain (ECFM_LBLT_LTR_TABLE,
                     (tRBKeyFreeFn) EcfmLbLtUtilFreeEntryFn,
                     ECFM_LBLT_LTR_ENTRY);
        EcfmLbltDeleteMemPoolsForLtrTlvInfo ();
        ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_TABLE_POOL);
        ECFM_LBLT_LTR_TABLE_POOL = ECFM_INIT_VAL;
    }

    /* Delete the required LTM entries */
    EcfmSnmpLwDelMepLtmReplyList ();

    /* Creating Mempool for LTR Table */
    if (ECFM_CREATE_MEM_POOL
        (ECFM_LBLT_LTR_INFO_SIZE, i4SetValFsEcfmLtrCacheSize,
         MEM_DEFAULT_MEMORY_TYPE,
         &ECFM_LBLT_LTR_TABLE_POOL) == ECFM_MEM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of Mem Pool for LTR Info"
                       "FAILED!\n");
        return SNMP_FAILURE;
    }

    if (EcfmLbltCreateMemPoolsForLtrTlvInfo (i4SetValFsEcfmLtrCacheSize)
        == ECFM_FAILURE)
    {
        ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_MGMT_TRC, "Creation of Mem Pool for LTR TLV Info "
                       "FAILED!\n");
        if (ECFM_LBLT_LTR_TABLE_POOL != 0)
        {
            ECFM_DELETE_MEM_POOL (ECFM_LBLT_LTR_TABLE_POOL);
            ECFM_LBLT_LTR_TABLE_POOL = ECFM_INIT_VAL;
        }
        return SNMP_FAILURE;
    }

    ECFM_LBLT_LTR_CACHE_SIZE = (UINT2) i4SetValFsEcfmLtrCacheSize;
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmLtrCacheSize, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_LBLT_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmLtrCacheSize));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmSystemControl
 Input       :  The Indices

                The Object 
                testValFsEcfmSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsEcfmSystemControl)
{

    UINT4               u4BridgeMode = ECFM_INVALID_BRIDGE_MODE;
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Validate System Control values */
    if ((i4TestValFsEcfmSystemControl != ECFM_START) &&
        (i4TestValFsEcfmSystemControl != ECFM_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsEcfmSystemControl == ECFM_START)
    {
        if (EcfmL2IwfGetBridgeMode
            (ECFM_CC_CURR_CONTEXT_ID (), &u4BridgeMode) != L2IWF_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (u4BridgeMode == ECFM_INVALID_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_ECFM_BRG_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (i4TestValFsEcfmSystemControl == ECFM_SHUTDOWN)
    {
        pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);

        while (pMepNode != NULL)
        {
            if (ECFM_TRUE == pMepNode->b1MepInUse)
            {
                CLI_SET_ERR (CLI_ECFM_Y1731_MEP_IN_USE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmModuleStatus
 Input       :  The Indices

                The Object 
                testValFsEcfmModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmModuleStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsEcfmModuleStatus)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Module Status values */
    if ((i4TestValFsEcfmModuleStatus != ECFM_ENABLE) &&
        (i4TestValFsEcfmModuleStatus != ECFM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsEcfmModuleStatus == ECFM_DISABLE)
    {
        pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);

        while (pMepNode != NULL)
        {
            if (ECFM_TRUE == pMepNode->b1MepInUse)
            {
                CLI_SET_ERR (CLI_ECFM_Y1731_MEP_IN_USE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
        }
        if (ECFM_ENABLE == ECFM_Y1731_STATUS (ECFM_CC_CURR_CONTEXT_ID ()))
        {
            CLI_SET_ERR (CLI_ECFM_Y1731_ENABLE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmOui
 Input       :  The Indices

                The Object 
                testValFsEcfmOui
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmOui (UINT4 *pu4ErrorCode,
                    tSNMP_OCTET_STRING_TYPE * pTestValFsEcfmOui)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_INITIALISED () == ECFM_FALSE)
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check OUI string and length to be set */
    if (pTestValFsEcfmOui->i4_Length != ECFM_OUI_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsEcfmOui->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmTraceOption
 Input       :  The Indices

                The Object 
                testValFsEcfmTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmTraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsEcfmTraceOption)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Trace options values */
    if (((i4TestValFsEcfmTraceOption < ECFM_MIN_TRC_VAL) ||
         (i4TestValFsEcfmTraceOption > ECFM_MAX_TRC_VAL)) &&
        (i4TestValFsEcfmTraceOption != ECFM_CLR_TRC_OPT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmLtrCacheStatus
 Input       :  The Indices

                The Object 
                testValFsEcfmLtrCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmLtrCacheStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsEcfmLtrCacheStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Ltr cache Status values */
    if ((i4TestValFsEcfmLtrCacheStatus != ECFM_ENABLE) &&
        (i4TestValFsEcfmLtrCacheStatus != ECFM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Check if LtrCache status is already same */
    if (i4TestValFsEcfmLtrCacheStatus == ECFM_LBLT_LTR_CACHE_STATUS)
    {
        return SNMP_SUCCESS;
    }
    /* Check if pool is already created */
    if ((i4TestValFsEcfmLtrCacheStatus == ECFM_ENABLE) &&
        (ECFM_LBLT_LTR_TABLE_POOL != 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmLtrCacheClear
 Input       :  The Indices

                The Object 
                testValFsEcfmLtrCacheClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmLtrCacheClear (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsEcfmLtrCacheClear)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate LtrClearStatus value */
    if (i4TestValFsEcfmLtrCacheClear != ECFM_SNMP_TRUE)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmLtrCacheHoldTime
 Input       :  The Indices

                The Object 
                testValFsEcfmLtrCacheHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmLtrCacheHoldTime (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsEcfmLtrCacheHoldTime)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate LtrCache hold time value */
    if ((i4TestValFsEcfmLtrCacheHoldTime < ECFM_LTR_HOLD_TIME_MIN) ||
        (i4TestValFsEcfmLtrCacheHoldTime > ECFM_LTR_HOLD_TIME_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmLtrCacheSize
 Input       :  The Indices

                The Object 
                testValFsEcfmLtrCacheSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmLtrCacheSize (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsEcfmLtrCacheSize)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate LtrCache size value */
    if ((i4TestValFsEcfmLtrCacheSize < ECFM_LTR_CACHE_SIZE_MIN) ||
        (i4TestValFsEcfmLtrCacheSize > ECFM_LTR_CACHE_SIZE_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmSystemControl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmModuleStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmOui
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmOui (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmLtrCacheStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmLtrCacheStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmLtrCacheClear
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmLtrCacheClear (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmLtrCacheHoldTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmLtrCacheHoldTime (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmLtrCacheSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmLtrCacheSize (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmPortTable
 Input       :  The Indices
                FsEcfmPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmPortTable (UINT4 u4FsEcfmPortIndex)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcPortInfo     PortIndex;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    ECFM_MEMSET (&PortIndex, 0, sizeof (tEcfmCcPortInfo));

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }

    /* Validate index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))
    {
        return SNMP_FAILURE;
    }

    if (EcfmVcmGetContextInfoFromIfIndex
        (u4FsEcfmPortIndex, &u4ContextId, &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    PortIndex.u4ContextId = u4ContextId;
    PortIndex.u4IfIndex = u4FsEcfmPortIndex;

    pPortInfo =
        (tEcfmCcPortInfo *) RBTreeGet (ECFM_CC_GLOBAL_PORT_TABLE, &PortIndex);

    if (pPortInfo == NULL)
    {
        /* If entry is not found */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmPortTable
 Input       :  The Indices
                FsEcfmPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmPortTable (UINT4 *pu4FsEcfmPortIndex)
{
    return (nmhGetNextIndexFsEcfmPortTable (0, pu4FsEcfmPortIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmPortTable
 Input       :  The Indices
                FsEcfmPortIndex
                nextFsEcfmPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmPortTable (UINT4 u4FsEcfmPortIndex,
                                UINT4 *pu4NextFsEcfmPortIndex)
{
    UINT2               u2LocalPortNum = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }

    /* Scanning the entire port table.
     * Last Port Number is reserved for MPLS-TP Dummy Port,
     * and it is not a actual physical or logical port to be 
     * displayed.
     */
    for (u2LocalPortNum = (UINT2) (u4FsEcfmPortIndex + 1);
         u2LocalPortNum < ECFM_CC_MAX_PORT_INFO; u2LocalPortNum++)
    {

        if (EcfmVcmGetContextInfoFromIfIndex
            ((UINT4) u2LocalPortNum, &u4ContextId,
             &u2LocalPortId) != VCM_FAILURE)
        {
            if (ECFM_CC_GET_PORT_INFO (u2LocalPortNum) != NULL)
            {
                *pu4NextFsEcfmPortIndex = u2LocalPortNum;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmPortLLCEncapStatus
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortLLCEncapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortLLCEncapStatus (UINT4 u4FsEcfmPortIndex,
                                INT4 *pi4RetValFsEcfmPortLLCEncapStatus)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    pPortInfo = (tEcfmCcPortInfo *) ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    /* Check whether the entry is present or not */
    if (pPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If found then return the LLCEncap status corresponding to port */
    if (pPortInfo->b1LLCEncapStatus == ECFM_FALSE)
    {
        *pi4RetValFsEcfmPortLLCEncapStatus = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValFsEcfmPortLLCEncapStatus = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortModuleStatus
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortModuleStatus (UINT4 u4FsEcfmPortIndex,
                              INT4 *pi4RetValFsEcfmPortModuleStatus)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;

    pPortInfo = (tEcfmCcPortInfo *) ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);

    /* Check whether the entry is present or not */
    if (pPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If entry is found then return the module status corresp to port */
    *pi4RetValFsEcfmPortModuleStatus = pPortInfo->u1PortEcfmStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxCfmPduCount (UINT4 u4FsEcfmPortIndex,
                               UINT4 *pu4RetValFsEcfmPortTxCfmPduCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;

    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    EcfmCcmOffLoadUpdateTxRxStats (u4FsEcfmPortIndex);
    *pu4RetValFsEcfmPortTxCfmPduCount =
        *pu4RetValFsEcfmPortTxCfmPduCount + pCcPortInfo->u4TxCfmPduCount;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortTxCfmPduCount =
        *pu4RetValFsEcfmPortTxCfmPduCount + pLbLtPortInfo->u4TxCfmPduCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxCcmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxCcmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxCcmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortTxCcmCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    EcfmCcmOffLoadUpdateTxRxStats (u4FsEcfmPortIndex);
    *pu4RetValFsEcfmPortTxCcmCount =
        *pu4RetValFsEcfmPortTxCcmCount + pCcPortInfo->u4TxCcmCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxLbmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxLbmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxLbmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortTxLbmCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;

    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortTxLbmCount =
        *pu4RetValFsEcfmPortTxLbmCount + pLbLtPortInfo->u4TxLbmCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxLbrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxLbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxLbrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortTxLbrCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortTxLbrCount =
        *pu4RetValFsEcfmPortTxLbrCount + pLbLtPortInfo->u4TxLbrCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxLtmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxLtmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxLtmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortTxLtmCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortTxLtmCount =
        *pu4RetValFsEcfmPortTxLtmCount + pLbLtPortInfo->u4TxLtmCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxLtrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxLtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxLtrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortTxLtrCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortTxLtrCount =
        *pu4RetValFsEcfmPortTxLtrCount + pLbLtPortInfo->u4TxLtrCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortTxFailedCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortTxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortTxFailedCount (UINT4 u4FsEcfmPortIndex,
                               UINT4 *pu4RetValFsEcfmPortTxFailedCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    EcfmCcmOffLoadUpdateTxRxStats (u4FsEcfmPortIndex);
    *pu4RetValFsEcfmPortTxFailedCount =
        *pu4RetValFsEcfmPortTxFailedCount + pCcPortInfo->u4TxFailedCount;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortTxFailedCount =
        *pu4RetValFsEcfmPortTxFailedCount + pLbLtPortInfo->u4TxFailedCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxCfmPduCount (UINT4 u4FsEcfmPortIndex,
                               UINT4 *pu4RetValFsEcfmPortRxCfmPduCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    EcfmCcmOffLoadUpdateTxRxStats (u4FsEcfmPortIndex);
    *pu4RetValFsEcfmPortRxCfmPduCount =
        *pu4RetValFsEcfmPortRxCfmPduCount + pCcPortInfo->u4RxCfmPduCount;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxCfmPduCount =
        *pu4RetValFsEcfmPortRxCfmPduCount + pLbLtPortInfo->u4RxCfmPduCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxCcmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxCcmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxCcmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortRxCcmCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    EcfmCcmOffLoadUpdateTxRxStats (u4FsEcfmPortIndex);
    *pu4RetValFsEcfmPortRxCcmCount =
        *pu4RetValFsEcfmPortRxCcmCount + pCcPortInfo->u4RxCcmCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxLbmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxLbmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxLbmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortRxLbmCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;

    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxLbmCount =
        *pu4RetValFsEcfmPortRxLbmCount + pLbLtPortInfo->u4RxLbmCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxLbrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxLbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxLbrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortRxLbrCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxLbrCount =
        *pu4RetValFsEcfmPortRxLbrCount + pLbLtPortInfo->u4RxLbrCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxLtmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxLtmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxLtmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortRxLtmCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;

    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxLtmCount =
        *pu4RetValFsEcfmPortRxLtmCount + pLbLtPortInfo->u4RxLtmCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxLtrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxLtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxLtrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 *pu4RetValFsEcfmPortRxLtrCount)
{
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxLtrCount =
        *pu4RetValFsEcfmPortRxLtrCount + pLbLtPortInfo->u4RxLtrCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortRxBadCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortRxBadCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortRxBadCfmPduCount (UINT4 u4FsEcfmPortIndex,
                                  UINT4 *pu4RetValFsEcfmPortRxBadCfmPduCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxBadCfmPduCount =
        *pu4RetValFsEcfmPortRxBadCfmPduCount + pCcPortInfo->u4RxBadCfmPduCount;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortRxBadCfmPduCount =
        *pu4RetValFsEcfmPortRxBadCfmPduCount +
        pLbLtPortInfo->u4RxBadCfmPduCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortFrwdCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortFrwdCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortFrwdCfmPduCount (UINT4 u4FsEcfmPortIndex,
                                 UINT4 *pu4RetValFsEcfmPortFrwdCfmPduCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortFrwdCfmPduCount =
        *pu4RetValFsEcfmPortFrwdCfmPduCount + pCcPortInfo->u4FrwdCfmPduCount;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortFrwdCfmPduCount =
        *pu4RetValFsEcfmPortFrwdCfmPduCount + pLbLtPortInfo->u4FrwdCfmPduCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmPortDsrdCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                retValFsEcfmPortDsrdCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmPortDsrdCfmPduCount (UINT4 u4FsEcfmPortIndex,
                                 UINT4 *pu4RetValFsEcfmPortDsrdCfmPduCount)
{
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortDsrdCfmPduCount =
        *pu4RetValFsEcfmPortDsrdCfmPduCount + pCcPortInfo->u4DsrdCfmPduCount;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmPortDsrdCfmPduCount =
        *pu4RetValFsEcfmPortDsrdCfmPduCount + pLbLtPortInfo->u4DsrdCfmPduCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmPortLLCEncapStatus
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortLLCEncapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortLLCEncapStatus (UINT4 u4FsEcfmPortIndex,
                                INT4 i4SetValFsEcfmPortLLCEncapStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;

    EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                   ECFM_RED_PORT_LLC_CMD,
                                   u4FsEcfmPortIndex,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_TRUE);

    /* Get Port entry corresponding to u4FsEcfmPortIndex */
    pCcPortInfo = (tEcfmCcPortInfo *) ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);

    /* Check whether the entry is present or not */
    if ((pCcPortInfo == NULL) ||
        (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ())
         != ECFM_TRUE))
    {
        return SNMP_FAILURE;
    }
    /* Mapping of ECFM_SNMP_TRUE and ECFM_SNMP_FALSE to ECFM_TRUE
     * and ECFM_FALSE resp */
    if (i4SetValFsEcfmPortLLCEncapStatus == ECFM_SNMP_TRUE)
    {
        i4SetValFsEcfmPortLLCEncapStatus = ECFM_TRUE;
    }
    else
    {
        i4SetValFsEcfmPortLLCEncapStatus = ECFM_FALSE;
    }

    /* Set LLC Encap for CC */
    pCcPortInfo->b1LLCEncapStatus = (UINT1) i4SetValFsEcfmPortLLCEncapStatus;
    EcfmCcmOffLoadUpdateLlc (u4FsEcfmPortIndex);
    /* Set LLC Encap for LBLT */
    EcfmLbLtConfPortLLCEncapStatus (ECFM_CC_CURR_CONTEXT_ID (),
                                    u4FsEcfmPortIndex,
                                    pCcPortInfo->b1LLCEncapStatus);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortLLCEncapStatus, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex for given Local Port for a Context */
    u4IfIndex = pCcPortInfo->u4IfIndex;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      (INT4) u4IfIndex, i4SetValFsEcfmPortLLCEncapStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortModuleStatus
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortModuleStatus (UINT4 u4FsEcfmPortIndex,
                              INT4 i4SetValFsEcfmPortModuleStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IsPWInterface = 0;

    if (EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                        (UINT2) u4FsEcfmPortIndex,
                                        &u4IfIndex) == ECFM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (EcfmIsPWInterface (u4IfIndex) == ECFM_TRUE)
    {
        u4IsPWInterface = 1;
    }

    /* Get Port entry corresponding to u4FsEcfmPortIndex */
    pCcPortInfo = (tEcfmCcPortInfo *) ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);

    /* Check whether the entry is present or not */
    if ((u4IsPWInterface == 0) ||
        ((u4IsPWInterface == 1)
         && (i4SetValFsEcfmPortModuleStatus == ECFM_DISABLE)))
    {

        if ((pCcPortInfo == NULL) ||
            (EcfmLbLtPortCreated (u4FsEcfmPortIndex,
                                  ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_TRUE))
        {
            return SNMP_FAILURE;
        }

    }

    /* Check if ECFM is already enabled/disabled, at this port * 
     * before setting this object */
    if ((pCcPortInfo != NULL) &&
        (pCcPortInfo->u1PortEcfmStatus == i4SetValFsEcfmPortModuleStatus))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM at this port is already in the "
                     "same state\r\n");
        return SNMP_SUCCESS;
    }

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortModuleStatus, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                   ECFM_RED_PORT_MOD_STS_CMD,
                                   u4FsEcfmPortIndex,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_TRUE);

    if (i4SetValFsEcfmPortModuleStatus == ECFM_ENABLE)
    {

        if (u4IsPWInterface == 1)
        {
            if (EcfmCreatePwInterface (u4IfIndex) == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                             "SNMP: EcfmCreatePwInterface function returned FAILURE \r\n");
                return SNMP_FAILURE;
            }
            pCcPortInfo =
                (tEcfmCcPortInfo *) ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
            if (pCcPortInfo == NULL)
            {
                return SNMP_FAILURE;
            }
        }

        /* Enable module at this port */
        if (EcfmUtilModuleEnableForAPort (pCcPortInfo) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                         "SNMP: ECFM Module Enable Failed at this port \r\n");

            EcfmUtilModuleDisableForAPort (pCcPortInfo);
            EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                           ECFM_RED_PORT_MOD_STS_CMD,
                                           u4FsEcfmPortIndex,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL,
                                           ECFM_INIT_VAL, ECFM_FALSE);
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                              ECFM_CC_CURR_CONTEXT_ID (),
                              (INT4) u4IfIndex,
                              i4SetValFsEcfmPortModuleStatus));
            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Disable module at this port */
        if (pCcPortInfo == NULL)
        {
            return SNMP_FAILURE;
        }

        EcfmUtilModuleDisableForAPort (pCcPortInfo);
        if (u4IsPWInterface == 1)
        {
            if (EcfmDeletePwInterface (u4IfIndex) == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                             "SNMP: EcfmDeletePwInterface function returned FAILURE \r\n");
                return SNMP_FAILURE;
            }
        }

    }

    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      (INT4) u4IfIndex, i4SetValFsEcfmPortModuleStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxCfmPduCount (UINT4 u4FsEcfmPortIndex,
                               UINT4 u4SetValFsEcfmPortTxCfmPduCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4TxCfmPduCount = u4SetValFsEcfmPortTxCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4TxCfmPduCount = u4SetValFsEcfmPortTxCfmPduCount;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxCfmPduCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxCfmPduCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxCcmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxCcmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxCcmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortTxCcmCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4TxCcmCount = u4SetValFsEcfmPortTxCcmCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxCcmCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxCcmCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxLbmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxLbmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxLbmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortTxLbmCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4TxLbmCount = u4SetValFsEcfmPortTxLbmCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxLbmCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxLbmCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxLbrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxLbrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxLbrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortTxLbrCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4TxLbrCount = u4SetValFsEcfmPortTxLbrCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxLbrCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxLbrCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxLtmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxLtmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxLtmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortTxLtmCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4TxLtmCount = u4SetValFsEcfmPortTxLtmCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxLtmCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxLtmCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxLtrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxLtrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxLtrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortTxLtrCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4TxLtrCount = u4SetValFsEcfmPortTxLtrCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxLtrCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxLtrCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortTxFailedCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortTxFailedCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortTxFailedCount (UINT4 u4FsEcfmPortIndex,
                               UINT4 u4SetValFsEcfmPortTxFailedCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4TxFailedCount = u4SetValFsEcfmPortTxFailedCount;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4TxFailedCount = u4SetValFsEcfmPortTxFailedCount;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortTxFailedCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortTxFailedCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxCfmPduCount (UINT4 u4FsEcfmPortIndex,
                               UINT4 u4SetValFsEcfmPortRxCfmPduCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4RxCfmPduCount = u4SetValFsEcfmPortRxCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4RxCfmPduCount = u4SetValFsEcfmPortRxCfmPduCount;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxCfmPduCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxCfmPduCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxCcmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxCcmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxCcmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortRxCcmCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4RxCcmCount = u4SetValFsEcfmPortRxCcmCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxCcmCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxCcmCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxLbmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxLbmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxLbmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortRxLbmCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4RxLbmCount = u4SetValFsEcfmPortRxLbmCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxLbmCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxLbmCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxLbrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxLbrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxLbrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortRxLbrCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4RxLbrCount = u4SetValFsEcfmPortRxLbrCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxLbrCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxLbrCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxLtmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxLtmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxLtmCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortRxLtmCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4RxLtmCount = u4SetValFsEcfmPortRxLtmCount;

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxLtmCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxLtmCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxLtrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxLtrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxLtrCount (UINT4 u4FsEcfmPortIndex,
                            UINT4 u4SetValFsEcfmPortRxLtrCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4RxLtrCount = u4SetValFsEcfmPortRxLtrCount;
    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxLtrCount, u4SeqNum,
                          FALSE, EcfmLbLtLock, EcfmLbLtUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_LBLT_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxLtrCount));
    if (ECFM_LBLT_SELECT_CONTEXT (u4CurrContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortRxBadCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortRxBadCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortRxBadCfmPduCount (UINT4 u4FsEcfmPortIndex,
                                  UINT4 u4SetValFsEcfmPortRxBadCfmPduCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4RxBadCfmPduCount = u4SetValFsEcfmPortRxBadCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4RxBadCfmPduCount = u4SetValFsEcfmPortRxBadCfmPduCount;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortRxBadCfmPduCount,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortRxBadCfmPduCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortFrwdCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortFrwdCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortFrwdCfmPduCount (UINT4 u4FsEcfmPortIndex,
                                 UINT4 u4SetValFsEcfmPortFrwdCfmPduCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4FrwdCfmPduCount = u4SetValFsEcfmPortFrwdCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4FrwdCfmPduCount = u4SetValFsEcfmPortFrwdCfmPduCount;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortFrwdCfmPduCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortFrwdCfmPduCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmPortDsrdCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                setValFsEcfmPortDsrdCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmPortDsrdCfmPduCount (UINT4 u4FsEcfmPortIndex,
                                 UINT4 u4SetValFsEcfmPortDsrdCfmPduCount)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pCcPortInfo == NULL)

    {
        return SNMP_FAILURE;
    }
    pCcPortInfo->u4DsrdCfmPduCount = u4SetValFsEcfmPortDsrdCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex);
    if (pLbLtPortInfo == NULL)

    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pLbLtPortInfo->u4DsrdCfmPduCount = u4SetValFsEcfmPortDsrdCfmPduCount;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmPortDsrdCfmPduCount, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                    (UINT2) u4FsEcfmPortIndex, &u4IfIndex);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                      (INT4) u4IfIndex, u4SetValFsEcfmPortDsrdCfmPduCount));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortLLCEncapStatus
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortLLCEncapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortLLCEncapStatus (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                                   INT4 i4TestValFsEcfmPortLLCEncapStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Validate LLCEncap status value */
    if ((i4TestValFsEcfmPortLLCEncapStatus != ECFM_SNMP_TRUE) &&
        (i4TestValFsEcfmPortLLCEncapStatus != ECFM_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)
    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortModuleStatus
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortModuleStatus (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                                 INT4 i4TestValFsEcfmPortModuleStatus)
{
    UINT4               u4IfIndex = 0;

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Validating ModuleStatus value */
    if ((i4TestValFsEcfmPortModuleStatus != ECFM_ENABLE) &&
        (i4TestValFsEcfmPortModuleStatus != ECFM_DISABLE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /*Check for Pseudo Interface, is so return success. */
    if (EcfmVcmGetIfIndexFromLocalPort (ECFM_CC_CURR_CONTEXT_ID (),
                                        (UINT2) u4FsEcfmPortIndex,
                                        &u4IfIndex) == ECFM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (EcfmIsPWInterface (u4IfIndex) == ECFM_TRUE)
    {
        return SNMP_SUCCESS;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxCfmPduCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                                  UINT4 u4TestValFsEcfmPortTxCfmPduCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxCfmPduCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxCcmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxCcmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxCcmCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortTxCcmCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxCcmCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxLbmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxLbmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxLbmCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortTxLbmCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxLbmCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxLbrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxLbrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxLbrCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortTxLbrCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxLbrCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxLtmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxLtmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxLtmCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortTxLtmCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxLtmCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxLtrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxLtrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxLtrCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortTxLtrCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxLtrCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortTxFailedCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortTxFailedCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortTxFailedCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                                  UINT4 u4TestValFsEcfmPortTxFailedCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortTxFailedCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxCfmPduCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                                  UINT4 u4TestValFsEcfmPortRxCfmPduCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxCfmPduCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxCcmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxCcmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxCcmCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortRxCcmCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxCcmCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxLbmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxLbmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxLbmCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortRxLbmCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxLbmCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxLbrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxLbrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxLbrCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortRxLbrCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxLbrCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxLtmCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxLtmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxLtmCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortRxLtmCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxLtmCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxLtrCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxLtrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxLtrCount (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmPortIndex,
                               UINT4 u4TestValFsEcfmPortRxLtrCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxLtrCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_LBLT_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_LBLT_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortRxBadCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortRxBadCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortRxBadCfmPduCount (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsEcfmPortIndex,
                                     UINT4 u4TestValFsEcfmPortRxBadCfmPduCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortRxBadCfmPduCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortFrwdCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortFrwdCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortFrwdCfmPduCount (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsEcfmPortIndex,
                                    UINT4 u4TestValFsEcfmPortFrwdCfmPduCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortFrwdCfmPduCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)

    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmPortDsrdCfmPduCount
 Input       :  The Indices
                FsEcfmPortIndex

                The Object 
                testValFsEcfmPortDsrdCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmPortDsrdCfmPduCount (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsEcfmPortIndex,
                                    UINT4 u4TestValFsEcfmPortDsrdCfmPduCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validating INPUT value */
    if (u4TestValFsEcfmPortDsrdCfmPduCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Index range */
    if ((u4FsEcfmPortIndex < ECFM_PORTS_PER_CONTEXT_MIN) ||
        (u4FsEcfmPortIndex > ECFM_CC_MAX_PORT_INFO))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ECFM_CC_GET_PORT_INFO (u4FsEcfmPortIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EcfmLbLtPortCreated (u4FsEcfmPortIndex, ECFM_CC_CURR_CONTEXT_ID ()) !=
        ECFM_TRUE)
    {
        CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmPortTable
 Input       :  The Indices
                FsEcfmPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmMipCcmDbStatus
 Input       :  The Indices

                The Object 
                retValFsEcfmMipCcmDbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipCcmDbStatus (INT4 *pi4RetValFsEcfmMipCcmDbStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmMipCcmDbStatus = ECFM_DISABLE;
        return SNMP_SUCCESS;
    }

    /* Returns the MipCcmDb status */
    *pi4RetValFsEcfmMipCcmDbStatus = ECFM_CC_MIP_CCM_DB_STATUS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMipCcmDbClear
 Input       :  The Indices

                The Object 
                retValFsEcfmMipCcmDbClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipCcmDbClear (INT4 *pi4RetValFsEcfmMipCcmDbClear)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmMipCcmDbClear = ECFM_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    /* Returns the current MipCcmDb clear status */
    *pi4RetValFsEcfmMipCcmDbClear = ECFM_SNMP_FALSE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMipCcmDbSize
 Input       :  The Indices

                The Object 
                retValFsEcfmMipCcmDbSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipCcmDbSize (INT4 *pi4RetValFsEcfmMipCcmDbSize)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmMipCcmDbSize = ECFM_MIP_CCM_DB_DEF_SIZE;
        return SNMP_SUCCESS;
    }

    /* Returns the MipCcmDb size */
    *pi4RetValFsEcfmMipCcmDbSize = ECFM_CC_MIP_CCM_DB_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMipCcmDbHoldTime
 Input       :  The Indices

                The Object 
                retValFsEcfmMipCcmDbHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipCcmDbHoldTime (INT4 *pi4RetValFsEcfmMipCcmDbHoldTime)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmMipCcmDbHoldTime = ECFM_MIP_CCM_DB_DEF_HOLD_TIME;
        return SNMP_SUCCESS;
    }

    /* Returns the MipCcmDb hold time */
    *pi4RetValFsEcfmMipCcmDbHoldTime = ECFM_CC_MIP_CCM_DB_HOLD_TIME;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMemoryFailureCount
 Input       :  The Indices

                The Object 
                retValFsEcfmMemoryFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMemoryFailureCount (UINT4 *pu4RetValFsEcfmMemoryFailureCount)
{
    *pu4RetValFsEcfmMemoryFailureCount =
        *pu4RetValFsEcfmMemoryFailureCount +
        gEcfmCcGlobalInfo.u4MemoryFailureCount;
    ECFM_LBLT_LOCK ();
    *pu4RetValFsEcfmMemoryFailureCount =
        *pu4RetValFsEcfmMemoryFailureCount +
        gEcfmLbLtGlobalInfo.u4MemoryFailureCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmBufferFailureCount
 Input       :  The Indices

                The Object 
                retValFsEcfmBufferFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmBufferFailureCount (UINT4 *pu4RetValFsEcfmBufferFailureCount)
{
    *pu4RetValFsEcfmBufferFailureCount =
        *pu4RetValFsEcfmBufferFailureCount +
        gEcfmCcGlobalInfo.u4BufferFailureCount;
    ECFM_LBLT_LOCK ();
    *pu4RetValFsEcfmBufferFailureCount =
        *pu4RetValFsEcfmBufferFailureCount +
        gEcfmLbLtGlobalInfo.u4BufferFailureCount;
    ECFM_LBLT_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmUpCount
 Input       :  The Indices

                The Object 
                retValFsEcfmUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmUpCount (UINT4 *pu4RetValFsEcfmUpCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmUpCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmUpCount =
        *pu4RetValFsEcfmUpCount + ECFM_CC_CURR_CONTEXT_INFO ()->u4EcfmUpCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmDownCount
 Input       :  The Indices

                The Object 
                retValFsEcfmDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmDownCount (UINT4 *pu4RetValFsEcfmDownCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmDownCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmDownCount =
        *pu4RetValFsEcfmDownCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4EcfmDownCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmNoDftCount
 Input       :  The Indices

                The Object 
                retValFsEcfmNoDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmNoDftCount (UINT4 *pu4RetValFsEcfmNoDftCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmNoDftCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmNoDftCount =
        *pu4RetValFsEcfmNoDftCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4NoDefectCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRdiDftCount
 Input       :  The Indices

                The Object 
                retValFsEcfmRdiDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRdiDftCount (UINT4 *pu4RetValFsEcfmRdiDftCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmRdiDftCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmRdiDftCount =
        *pu4RetValFsEcfmRdiDftCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4RdiDefectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMacStatusDftCount
 Input       :  The Indices

                The Object 
                retValFsEcfmMacStatusDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMacStatusDftCount (UINT4 *pu4RetValFsEcfmMacStatusDftCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmMacStatusDftCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmMacStatusDftCount =
        *pu4RetValFsEcfmMacStatusDftCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4MacStatusDefectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmRemoteCcmDftCount
 Input       :  The Indices

                The Object 
                retValFsEcfmRemoteCcmDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRemoteCcmDftCount (UINT4 *pu4RetValFsEcfmRemoteCcmDftCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmRemoteCcmDftCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmRemoteCcmDftCount =
        *pu4RetValFsEcfmRemoteCcmDftCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4RemoteCcmDefectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmErrorCcmDftCount
 Input       :  The Indices

                The Object 
                retValFsEcfmErrorCcmDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmErrorCcmDftCount (UINT4 *pu4RetValFsEcfmErrorCcmDftCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmErrorCcmDftCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmErrorCcmDftCount =
        *pu4RetValFsEcfmErrorCcmDftCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4ErrorCcmDefectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmXconDftCount
 Input       :  The Indices

                The Object 
                retValFsEcfmXconDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmXconDftCount (UINT4 *pu4RetValFsEcfmXconDftCount)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pu4RetValFsEcfmXconDftCount = ECFM_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pu4RetValFsEcfmXconDftCount =
        *pu4RetValFsEcfmXconDftCount +
        ECFM_CC_CURR_CONTEXT_INFO ()->u4XconnDefectCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmCrosscheckDelay
 Input       :  The Indices

                The Object 
                retValFsEcfmCrosscheckDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmCrosscheckDelay (INT4 *pi4RetValFsEcfmCrosscheckDelay)
{
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmCrosscheckDelay = 0;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsEcfmCrosscheckDelay =
        ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMipDynamicEvaluationStatus
 Input       :  The Indices

                The Object 
                retValFsEcfmMipDynamicEvaluationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipDynamicEvaluationStatus (INT4
                                        *pi4RetValFsEcfmMipDynamicEvaluationStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmMipDynamicEvaluationStatus = ECFM_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    /* Returns the current MipEvaluation status */
    *pi4RetValFsEcfmMipDynamicEvaluationStatus =
        ECFM_CONVERT_TO_SNMP_BOOL (ECFM_CC_MIP_DYNAMIC_EVALUATION_STATUS);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmMipCcmDbStatus
 Input       :  The Indices

                The Object 
                setValFsEcfmMipCcmDbStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipCcmDbStatus (INT4 i4SetValFsEcfmMipCcmDbStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4Duration = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Check the System Status */
    /* This check has been added to be in sync with the MSR, MSR will not call
     * TEST routine on startup*/
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }

    /* Check if MipCcmDb status is already enabled */
    if (i4SetValFsEcfmMipCcmDbStatus == ECFM_CC_MIP_CCM_DB_STATUS)
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipCcmDbStatus, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* check if it is to be enabled */
    if (i4SetValFsEcfmMipCcmDbStatus == ECFM_ENABLE)
    {
        if (ECFM_CC_MIP_CCM_DB_SIZE == ECFM_INIT_VAL)
        {
            ECFM_CC_MIP_CCM_DB_SIZE = ECFM_MIP_CCM_DB_DEF_SIZE;
        }
        if (ECFM_CC_MIP_CCM_DB_HOLD_TIME == ECFM_INIT_VAL)
        {
            ECFM_CC_MIP_CCM_DB_HOLD_TIME = ECFM_MIP_CCM_DB_DEF_HOLD_TIME;
        }
        /* Creating Mempool for MipCcmDb Table */
        if (ECFM_CREATE_MEM_POOL (ECFM_CC_MIP_CCM_DB_INFO_SIZE,
                                  ECFM_CC_MIP_CCM_DB_SIZE,
                                  MEM_DEFAULT_MEMORY_TYPE,
                                  &ECFM_CC_MIP_DB_TABLE_POOL)
            == ECFM_MEM_FAILURE)
        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                         ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_MGMT_TRC,
                         "nmhSetFsEcfmMipCcmDbStatus:Creation of"
                         "Mem Pool for MipCcmDb Info FAILED!\n");
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              ECFM_CC_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmMipCcmDbStatus));
            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
            return SNMP_FAILURE;
        }
        u4Duration = ECFM_CONVERT_HRS_TO_MSEC (ECFM_CC_MIP_CCM_DB_HOLD_TIME);
        /* Start Mip CcmDb Timer for which MipCcmDbentries are to be retained */
        if (EcfmCcTmrStartTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL,
                                 u4Duration) != ECFM_SUCCESS)
        {
            ECFM_DELETE_MEM_POOL (ECFM_CC_MIP_DB_TABLE_POOL);
            ECFM_CC_MIP_DB_TABLE_POOL = ECFM_INIT_VAL;
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                              ECFM_CC_CURR_CONTEXT_ID (),
                              i4SetValFsEcfmMipCcmDbStatus));
            ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
            return SNMP_FAILURE;
        }
        if (EcfmLbLtEnableMipCcmDb
            (ECFM_CC_CURR_CONTEXT_ID (),
             ECFM_CC_MIP_CCM_DB_SIZE) != ECFM_SUCCESS)

        {
            EcfmCcTmrStopTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL);
            ECFM_DELETE_MEM_POOL (ECFM_CC_MIP_DB_TABLE_POOL);
            ECFM_CC_MIP_DB_TABLE_POOL = ECFM_INIT_VAL;
            return SNMP_FAILURE;
        }
    }

    else

    {

        /* It is to be disabled */
        if (EcfmCcTmrStopTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL) != ECFM_SUCCESS)

        {
            ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                         ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "nmhSetFsEcfmMipCcmDbStatus: Stopping MipCcmDb hold "
                         "timer get FAILED!\n");
        }
        RBTreeDrain (ECFM_CC_MIP_CCM_DB_TABLE,
                     (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                     ECFM_CC_MIP_CCM_DB_ENTRY);

        /* Delete the mempool for MipCcmDb */
        ECFM_DELETE_MEM_POOL (ECFM_CC_MIP_DB_TABLE_POOL);
        ECFM_CC_MIP_DB_TABLE_POOL = ECFM_INIT_VAL;
        EcfmLbLtDisableMipCcmDb (ECFM_CC_CURR_CONTEXT_ID ());
    }
    ECFM_CC_MIP_CCM_DB_STATUS = (UINT1) i4SetValFsEcfmMipCcmDbStatus;

    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmMipCcmDbStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMipCcmDbClear
 Input       :  The Indices

                The Object 
                setValFsEcfmMipCcmDbClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipCcmDbClear (INT4 i4SetValFsEcfmMipCcmDbClear)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (i4SetValFsEcfmMipCcmDbClear);

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }

    /* Clear the MipCcmDb Table */
    RBTreeDrain (ECFM_CC_MIP_CCM_DB_TABLE,
                 (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                 ECFM_CC_MIP_CCM_DB_ENTRY);
    EcfmLbLtDrainMipCcmDb (ECFM_CC_CURR_CONTEXT_ID ());

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipCcmDbClear, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmMipCcmDbClear));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMipCcmDbSize
 Input       :  The Indices

                The Object 
                setValFsEcfmMipCcmDbSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipCcmDbSize (INT4 i4SetValFsEcfmMipCcmDbSize)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }
    if (ECFM_CC_MIP_CCM_DB_STATUS != ECFM_ENABLE)

    {
        ECFM_CC_MIP_CCM_DB_SIZE = (UINT2) i4SetValFsEcfmMipCcmDbSize;
        return SNMP_SUCCESS;
    }

    /* Check if pool is already created */
    if (ECFM_CC_MIP_DB_TABLE_POOL != 0)
    {
        /* Delete MIP CCM Db entries if any */
        RBTreeDrain (ECFM_CC_MIP_CCM_DB_TABLE,
                     (tRBKeyFreeFn) EcfmCcUtilFreeEntryFn,
                     ECFM_CC_MIP_CCM_DB_ENTRY);
        /* Delete MIP CCM Db POOL */
        ECFM_DELETE_MEM_POOL (ECFM_CC_MIP_DB_TABLE_POOL);
        ECFM_CC_MIP_DB_TABLE_POOL = ECFM_INIT_VAL;
        EcfmLbLtDisableMipCcmDb (ECFM_CC_CURR_CONTEXT_ID ());

    }
    /* Creating Mempool for MipCcmDb Table */
    if (ECFM_CREATE_MEM_POOL (ECFM_CC_MIP_CCM_DB_INFO_SIZE,
                              i4SetValFsEcfmMipCcmDbSize,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &ECFM_CC_MIP_DB_TABLE_POOL) == ECFM_MEM_FAILURE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_MGMT_TRC,
                     "nmhSetFsEcfmMipCcmDbSize:Creation of Mem Pool for MipCcmDb Info"
                     " FAILED at CC!\n");
        return SNMP_FAILURE;
    }
    if (EcfmLbLtEnableMipCcmDb
        (ECFM_CC_CURR_CONTEXT_ID (),
         i4SetValFsEcfmMipCcmDbSize) != ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "nmhSetFsEcfmMipCcmDbSize:Creation of Mem Pool for MipCcmDb Info"
                     " FAILED at LBLT!\n");
        return SNMP_FAILURE;
    }
    ECFM_CC_MIP_CCM_DB_SIZE = (UINT2) i4SetValFsEcfmMipCcmDbSize;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipCcmDbSize, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmMipCcmDbSize));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMipCcmDbHoldTime
 Input       :  The Indices

                The Object 
                setValFsEcfmMipCcmDbHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipCcmDbHoldTime (INT4 i4SetValFsEcfmMipCcmDbHoldTime)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4Duration = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        return SNMP_SUCCESS;
    }
    if (ECFM_CC_MIP_CCM_DB_STATUS != ECFM_ENABLE)

    {
        ECFM_CC_MIP_CCM_DB_HOLD_TIME = (UINT1) i4SetValFsEcfmMipCcmDbHoldTime;
        return SNMP_SUCCESS;
    }
    u4Duration = ECFM_CONVERT_HRS_TO_MSEC (i4SetValFsEcfmMipCcmDbHoldTime);

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipCcmDbHoldTime, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    /* Start Mip CcmDb Timer for which MipCcmDbentries are to be retained */
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL,
                             u4Duration) != ECFM_SUCCESS)
    {
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                          i4SetValFsEcfmMipCcmDbHoldTime));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
        return SNMP_FAILURE;
    }
    ECFM_CC_MIP_CCM_DB_HOLD_TIME = (UINT1) i4SetValFsEcfmMipCcmDbHoldTime;

    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmMipCcmDbHoldTime));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMemoryFailureCount
 Input       :  The Indices

                The Object 
                setValFsEcfmMemoryFailureCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMemoryFailureCount (UINT4 u4SetValFsEcfmMemoryFailureCount)
{
    gEcfmCcGlobalInfo.u4MemoryFailureCount = u4SetValFsEcfmMemoryFailureCount;
    ECFM_LBLT_LOCK ();
    gEcfmLbLtGlobalInfo.u4MemoryFailureCount = u4SetValFsEcfmMemoryFailureCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmBufferFailureCount
 Input       :  The Indices

                The Object 
                setValFsEcfmBufferFailureCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmBufferFailureCount (UINT4 u4SetValFsEcfmBufferFailureCount)
{
    gEcfmCcGlobalInfo.u4BufferFailureCount = u4SetValFsEcfmBufferFailureCount;
    ECFM_LBLT_LOCK ();
    gEcfmLbLtGlobalInfo.u4BufferFailureCount = u4SetValFsEcfmBufferFailureCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmUpCount
 Input       :  The Indices

                The Object 
                setValFsEcfmUpCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmUpCount (UINT4 u4SetValFsEcfmUpCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4EcfmUpCount = u4SetValFsEcfmUpCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmDownCount
 Input       :  The Indices

                The Object 
                setValFsEcfmDownCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmDownCount (UINT4 u4SetValFsEcfmDownCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4EcfmDownCount = u4SetValFsEcfmDownCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmNoDftCount
 Input       :  The Indices

                The Object 
                setValFsEcfmNoDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmNoDftCount (UINT4 u4SetValFsEcfmNoDftCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4NoDefectCount = u4SetValFsEcfmNoDftCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRdiDftCount
 Input       :  The Indices

                The Object 
                setValFsEcfmRdiDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRdiDftCount (UINT4 u4SetValFsEcfmRdiDftCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4RdiDefectCount = u4SetValFsEcfmRdiDftCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMacStatusDftCount
 Input       :  The Indices

                The Object 
                setValFsEcfmMacStatusDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMacStatusDftCount (UINT4 u4SetValFsEcfmMacStatusDftCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4MacStatusDefectCount =
        u4SetValFsEcfmMacStatusDftCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRemoteCcmDftCount
 Input       :  The Indices

                The Object 
                setValFsEcfmRemoteCcmDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRemoteCcmDftCount (UINT4 u4SetValFsEcfmRemoteCcmDftCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4RemoteCcmDefectCount =
        u4SetValFsEcfmRemoteCcmDftCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmErrorCcmDftCount
 Input       :  The Indices

                The Object 
                setValFsEcfmErrorCcmDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmErrorCcmDftCount (UINT4 u4SetValFsEcfmErrorCcmDftCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4ErrorCcmDefectCount =
        u4SetValFsEcfmErrorCcmDftCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmXconDftCount
 Input       :  The Indices

                The Object 
                setValFsEcfmXconDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmXconDftCount (UINT4 u4SetValFsEcfmXconDftCount)
{
    ECFM_CC_CURR_CONTEXT_INFO ()->u4XconnDefectCount =
        u4SetValFsEcfmXconDftCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmCrosscheckDelay
 Input       :  The Indices

                The Object 
                setValFsEcfmCrosscheckDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmCrosscheckDelay (INT4 i4SetValFsEcfmCrosscheckDelay)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay =
        (UINT2) i4SetValFsEcfmCrosscheckDelay;
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmCrosscheckDelay, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmCrosscheckDelay));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMipDynamicEvaluationStatus
 Input       :  The Indices

                The Object 
                setValFsEcfmMipDynamicEvaluationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipDynamicEvaluationStatus (INT4
                                        i4SetValFsEcfmMipDynamicEvaluationStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT1               u1Status = ECFM_FALSE;

    u1Status =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMipDynamicEvaluationStatus);
    if (ECFM_CC_MIP_DYNAMIC_EVALUATION_STATUS == u1Status)
    {
        return SNMP_SUCCESS;
    }
    /* Set the MIP dyanamic evaluation stautus */
    ECFM_CC_MIP_DYNAMIC_EVALUATION_STATUS =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMipDynamicEvaluationStatus);

    EcfmCcUtilEvaluateAndCreateMip (-1, -1, ECFM_TRUE);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipDynamicEvaluationStatus,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmMipDynamicEvaluationStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipCcmDbStatus
 Input       :  The Indices

                The Object 
                testValFsEcfmMipCcmDbStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipCcmDbStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsEcfmMipCcmDbStatus)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate MipCcmDb Status values */
    if ((i4TestValFsEcfmMipCcmDbStatus != ECFM_ENABLE) &&
        (i4TestValFsEcfmMipCcmDbStatus != ECFM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Check if MipCcmDb status is already same */
    if (i4TestValFsEcfmMipCcmDbStatus == ECFM_CC_MIP_CCM_DB_STATUS)
    {
        return SNMP_SUCCESS;
    }
    /* Check if pool for MipCcmDb is already created */
    if ((i4TestValFsEcfmMipCcmDbStatus == ECFM_ENABLE) &&
        (ECFM_CC_MIP_DB_TABLE_POOL != 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipCcmDbClear
 Input       :  The Indices

                The Object 
                testValFsEcfmMipCcmDbClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipCcmDbClear (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsEcfmMipCcmDbClear)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate MipCcmDb ClearStatus value */
    if (i4TestValFsEcfmMipCcmDbClear != ECFM_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipCcmDbSize
 Input       :  The Indices

                The Object 
                testValFsEcfmMipCcmDbSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipCcmDbSize (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsEcfmMipCcmDbSize)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate MipCache size value */
    if ((i4TestValFsEcfmMipCcmDbSize < ECFM_MIP_CCM_DB_SIZE_MIN) ||
        (i4TestValFsEcfmMipCcmDbSize > ECFM_MIP_CCM_DB_SIZE_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipCcmDbHoldTime
 Input       :  The Indices

                The Object 
                testValFsEcfmMipCcmDbHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipCcmDbHoldTime (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsEcfmMipCcmDbHoldTime)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate MipCcmDb hold time value */
    if ((i4TestValFsEcfmMipCcmDbHoldTime < ECFM_MIP_CCM_DB_HOLD_TIME_MIN) ||
        (i4TestValFsEcfmMipCcmDbHoldTime > ECFM_MIP_CCM_DB_HOLD_TIME_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMemoryFailureCount
 Input       :  The Indices

                The Object 
                testValFsEcfmMemoryFailureCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMemoryFailureCount (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsEcfmMemoryFailureCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmMemoryFailureCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmBufferFailureCount
 Input       :  The Indices

                The Object 
                testValFsEcfmBufferFailureCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmBufferFailureCount (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsEcfmBufferFailureCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmBufferFailureCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmUpCount
 Input       :  The Indices

                The Object 
                testValFsEcfmUpCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmUpCount (UINT4 *pu4ErrorCode, UINT4 u4TestValFsEcfmUpCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmUpCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmDownCount
 Input       :  The Indices

                The Object 
                testValFsEcfmDownCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmDownCount (UINT4 *pu4ErrorCode, UINT4 u4TestValFsEcfmDownCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmDownCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmNoDftCount
 Input       :  The Indices

                The Object 
                testValFsEcfmNoDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmNoDftCount (UINT4 *pu4ErrorCode, UINT4 u4TestValFsEcfmNoDftCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmNoDftCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRdiDftCount
 Input       :  The Indices

                The Object 
                testValFsEcfmRdiDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRdiDftCount (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsEcfmRdiDftCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmRdiDftCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMacStatusDftCount
 Input       :  The Indices

                The Object 
                testValFsEcfmMacStatusDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMacStatusDftCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsEcfmMacStatusDftCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmMacStatusDftCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRemoteCcmDftCount
 Input       :  The Indices

                The Object 
                testValFsEcfmRemoteCcmDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRemoteCcmDftCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsEcfmRemoteCcmDftCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmRemoteCcmDftCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmErrorCcmDftCount
 Input       :  The Indices

                The Object 
                testValFsEcfmErrorCcmDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmErrorCcmDftCount (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsEcfmErrorCcmDftCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmErrorCcmDftCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmXconDftCount
 Input       :  The Indices

                The Object 
                testValFsEcfmXconDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmXconDftCount (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsEcfmXconDftCount)
{

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsEcfmXconDftCount != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmCrosscheckDelay
 Input       :  The Indices

                The Object 
                testValFsEcfmCrosscheckDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmCrosscheckDelay (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsEcfmCrosscheckDelay)
{
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmCrosscheckDelay < ECFM_XCHK_DELAY_MIN_VAL)
        || (i4TestValFsEcfmCrosscheckDelay > ECFM_XCHK_DELAY_MAX_VAL))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipDynamicEvaluationStatus
 Input       :  The Indices

                The Object 
                testValFsEcfmMipDynamicEvaluationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipDynamicEvaluationStatus (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFsEcfmMipDynamicEvaluationStatus)
{
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Validate Evaluation status value, that is to be set */
    if ((i4TestValFsEcfmMipDynamicEvaluationStatus != ECFM_SNMP_FALSE) &&
        (i4TestValFsEcfmMipDynamicEvaluationStatus != ECFM_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMipCcmDbStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMipCcmDbStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMipCcmDbClear
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMipCcmDbClear (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMipCcmDbSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMipCcmDbSize (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMipCcmDbHoldTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMipCcmDbHoldTime (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMemoryFailureCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMemoryFailureCount (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmBufferFailureCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmBufferFailureCount (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmUpCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmUpCount (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmDownCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmDownCount (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmNoDftCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmNoDftCount (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmRdiDftCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmRdiDftCount (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMacStatusDftCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMacStatusDftCount (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmRemoteCcmDftCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmRemoteCcmDftCount (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmErrorCcmDftCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmErrorCcmDftCount (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmXconDftCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmXconDftCount (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmCrosscheckDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmCrosscheckDelay (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMipDynamicEvaluationStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMipDynamicEvaluationStatus (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmMipTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmMipTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmMipTable (INT4 i4FsEcfmMipIfIndex,
                                        INT4 i4FsEcfmMipMdLevel,
                                        INT4 i4FsEcfmMipVid)
{
    tEcfmCcMipInfo     *pMipNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }
    /* Get MIP entry corresponding to indices MdLevel, VlanId, IfIndex */
    pMipNode =
        EcfmCcUtilGetMipEntry (i4FsEcfmMipIfIndex, i4FsEcfmMipMdLevel,
                               i4FsEcfmMipVid);

    if (pMipNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmMipTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmMipTable (INT4 *pi4FsEcfmMipIfIndex,
                                INT4 *pi4FsEcfmMipMdLevel,
                                INT4 *pi4FsEcfmMipVid)
{
    return (nmhGetNextIndexFsEcfmMipTable (0, pi4FsEcfmMipIfIndex, 0,
                                           pi4FsEcfmMipMdLevel, 0,
                                           pi4FsEcfmMipVid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmMipTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                nextFsEcfmMipIfIndex
                FsEcfmMipMdLevel
                nextFsEcfmMipMdLevel
                FsEcfmMipVid
                nextFsEcfmMipVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmMipTable (INT4 i4FsEcfmMipIfIndex,
                               INT4 *pi4NextFsEcfmMipIfIndex,
                               INT4 i4FsEcfmMipMdLevel,
                               INT4 *pi4NextFsEcfmMipMdLevel,
                               INT4 i4FsEcfmMipVid, INT4 *pi4NextFsEcfmMipVid)
{
    tEcfmCcMipInfo      MipInfo;
    tEcfmCcMipInfo     *pMipNextNode = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;

    if (EcfmVcmGetContextInfoFromIfIndex
        ((UINT4) i4FsEcfmMipIfIndex, &u4ContextId,
         &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (&MipInfo, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);
    MipInfo.u4ContextId = u4ContextId;
    MipInfo.u2PortNum = i4FsEcfmMipIfIndex;
    MipInfo.u1MdLevel = (UINT1) i4FsEcfmMipMdLevel;
    MipInfo.u4VlanIdIsid = (UINT2) i4FsEcfmMipVid;

    /* Get MIP entry corresponding to indices next to IfIndex, Level, VlanId */
    pMipNextNode = (tEcfmCcMipInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_MIP_TABLE, (tRBElem *) (&MipInfo), NULL);

    if (pMipNextNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIP entry corresponding to indices next to IfIndex, Level, VlanId 
     */
    /* Set the next indices from corresponding MIP next node */
    *pi4NextFsEcfmMipIfIndex = (INT4) (pMipNextNode->u2PortNum);
    *pi4NextFsEcfmMipMdLevel = (INT4) (pMipNextNode->u1MdLevel);
    *pi4NextFsEcfmMipVid = (INT4) (pMipNextNode->u4VlanIdIsid);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmMipActive
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                retValFsEcfmMipActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipActive (INT4 i4FsEcfmMipIfIndex, INT4 i4FsEcfmMipMdLevel,
                       INT4 i4FsEcfmMipVid, INT4 *pi4RetValFsEcfmMipActive)
{
    tEcfmCcMipInfo     *pMipNode = NULL;

    /* Get MIP entry corresponding to IfIndex, level and vlanId */
    pMipNode =
        EcfmCcUtilGetMipEntry (i4FsEcfmMipIfIndex, i4FsEcfmMipMdLevel,
                               i4FsEcfmMipVid);
    if (pMipNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIP entry corresponding to IfIndex, level and vlanId exists */
    /* Set the MipActive from the corresponding MIP entry */
    if (pMipNode->b1Active == ECFM_FALSE)
    {
        *pi4RetValFsEcfmMipActive = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValFsEcfmMipActive = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMipRowStatus
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                retValFsEcfmMipRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipRowStatus (INT4 i4FsEcfmMipIfIndex, INT4 i4FsEcfmMipMdLevel,
                          INT4 i4FsEcfmMipVid,
                          INT4 *pi4RetValFsEcfmMipRowStatus)
{
    tEcfmCcMipInfo     *pMipNode = NULL;

    /* Get MIP entry corresponding to IfIndex, level and vlanId */
    pMipNode =
        EcfmCcUtilGetMipEntry (i4FsEcfmMipIfIndex, i4FsEcfmMipMdLevel,
                               i4FsEcfmMipVid);
    if (pMipNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIP entry corresponding to IfIndex, level and vlanId exists */
    *pi4RetValFsEcfmMipRowStatus = (INT4) (pMipNode->u1RowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmMipActive
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                setValFsEcfmMipActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipActive (INT4 i4FsEcfmMipIfIndex, INT4 i4FsEcfmMipMdLevel,
                       INT4 i4FsEcfmMipVid, INT4 i4SetValFsEcfmMipActive)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcMipInfo     *pMipNode = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    /* Get MIP entry corresponding to IfIndex, level and vlanId */
    pMipNode =
        EcfmCcUtilGetMipEntry (i4FsEcfmMipIfIndex, i4FsEcfmMipMdLevel,
                               i4FsEcfmMipVid);
    if (pMipNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIP entry corresponding to IfIndex, level and vlanId exists */
    /* Set the MipActive to corresponding MIP entry */
    /* Check if Mip Active's value is already same */
    if (((i4SetValFsEcfmMipActive == ECFM_SNMP_FALSE) &&
         (pMipNode->b1Active == ECFM_FALSE)) ||
        ((i4SetValFsEcfmMipActive == ECFM_SNMP_TRUE) &&
         (pMipNode->b1Active == ECFM_TRUE)))
    {
        /* MipActive is already set */
        return SNMP_SUCCESS;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (i4SetValFsEcfmMipActive == ECFM_SNMP_TRUE)
    {
        pMipNode->b1Active = ECFM_TRUE;
    }
    else
    {
        pMipNode->b1Active = ECFM_FALSE;
    }
    /* Update MipActive in LbLt task's MIP entry */
    EcfmLbLtConfMipActive (pMipNode->u2PortNum,
                           pMipNode->u1MdLevel,
                           pMipNode->u4VlanIdIsid,
                           pMipNode->b1Active, ECFM_CC_CURR_CONTEXT_ID ());
    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pMipNode->u4VlanIdIsid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipActive, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        /* Get IfIndex from Local Port for the current Context */
        ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_CC_CURR_CONTEXT_ID (),
                                          (UINT2) i4FsEcfmMipIfIndex,
                                          &u4IfIndex);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4IfIndex,
                          i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                          i4SetValFsEcfmMipActive));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMipRowStatus
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                setValFsEcfmMipRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMipRowStatus (INT4 i4FsEcfmMipIfIndex, INT4 i4FsEcfmMipMdLevel,
                          INT4 i4FsEcfmMipVid, INT4 i4SetValFsEcfmMipRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcStackInfo    StackInfo;
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcMipInfo     *pMipNewNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4MipStatus = 0;

    ECFM_MEMSET (&StackInfo, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);
    /* Get Mip entry corresponding to MdIndex, MaIndex, MepId */
    pMipNode = EcfmCcUtilGetMipEntry ((UINT2) i4FsEcfmMipIfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid);
    if (pMipNode != NULL)
    {
        /* Mip entry corresponding to IfIndex, MdLevel, VlanId exists and
         * its row status is same as user wants to set */
        if (pMipNode->u1RowStatus == (UINT1) i4SetValFsEcfmMipRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    else if (i4SetValFsEcfmMipRowStatus == ECFM_ROW_STATUS_DESTROY)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Mip Entry for the given Index \r\n");
        /* As per RFC1905, Deleting a non-existing row status should return success */
        return SNMP_SUCCESS;
    }

    else if ((i4SetValFsEcfmMipRowStatus != ECFM_ROW_STATUS_CREATE_AND_WAIT)
             && (i4SetValFsEcfmMipRowStatus != ECFM_ROW_STATUS_CREATE_AND_GO))

    {
        return SNMP_FAILURE;
    }

    if (!(ECFM_IS_MEP_ISID_AWARE (i4FsEcfmMipVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMipRowStatus, u4SeqNum,
                              TRUE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    }
    /* Get IfIndex from Local Port for the current Context */
    pPortInfo = ECFM_CC_GET_PORT_INFO (i4FsEcfmMipIfIndex);
    if (pPortInfo != NULL)
    {
        u4IfIndex = pPortInfo->u4IfIndex;
        /*MIP status can be made to active only when ECFM is
         * enabled in that interface*/
        if (pPortInfo->u1PortEcfmStatus == ECFM_ENABLE)
        {
            u4MipStatus = ECFM_TRUE;
        }
        else
        {
            u4MipStatus = ECFM_FALSE;
        }

    }

    switch (i4SetValFsEcfmMipRowStatus)
    {
        case ECFM_ROW_STATUS_ACTIVE:
            pMipNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
            /* Set MIP row status in LBLT task also */
            EcfmLbLtConfMipRowStatus ((UINT2) i4FsEcfmMipIfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                      pMipNode->u1RowStatus,
                                      ECFM_CC_CURR_CONTEXT_ID ());
            break;

        case ECFM_ROW_STATUS_NOT_IN_SERVICE:
            pMipNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;

            /* Set MIP row status in LBLT task also */
            EcfmLbLtConfMipRowStatus ((UINT2) i4FsEcfmMipIfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                      pMipNode->u1RowStatus,
                                      ECFM_CC_CURR_CONTEXT_ID ());
            break;

        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
        case ECFM_ROW_STATUS_CREATE_AND_GO:
            /* Create node for Mip */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MIP_TABLE (pMipNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "nmhSetFsEcfmMipRowStatus:"
                             "Row Status Create and Go"
                             "Allocation for Mip Node Failed \r\n");
                if (!(ECFM_IS_MEP_ISID_AWARE (i4FsEcfmMipVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                      (INT4) u4IfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                      i4SetValFsEcfmMipRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }
            ECFM_MEMSET (pMipNewNode, ECFM_INIT_VAL, ECFM_CC_MIP_INFO_SIZE);
            /* put values in MaMepList new node */
            pMipNewNode->u2PortNum = (UINT2) i4FsEcfmMipIfIndex;
            pMipNewNode->u1MdLevel = (UINT1) i4FsEcfmMipMdLevel;
            pMipNewNode->u4VlanIdIsid = i4FsEcfmMipVid;
            pMipNewNode->b1Active = (BOOL1) u4MipStatus;

            /* Add MdIndex and MaIndex if MIP is to be created with MA */
            ECFM_CC_GET_DEFAULT_MD_ENTRY (i4FsEcfmMipVid, pDefaultMdNode);
            /* Check MIP is be created with MA or default MD parmeters */
            if ((pDefaultMdNode != NULL) &&
                (((pDefaultMdNode->i1MdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL) &&
                  (ECFM_CC_DEF_MD_DEFAULT_LEVEL == (UINT1) i4FsEcfmMipMdLevel))
                 || ((pDefaultMdNode->i1MdLevel != ECFM_DEF_MD_LEVEL_DEF_VAL)
                     && ((UINT1) (pDefaultMdNode->i1MdLevel) ==
                         (UINT1) i4FsEcfmMipMdLevel))))
            {
                /* Check MIP is be created with MA or default MD parmeters */
                if (pDefaultMdNode->b1Status != ECFM_FALSE)
                {
                    pMaNode = NULL;
                }
                else
                {
                    /* MIP is to be created with MA */
                    pMaNode = EcfmCcUtilGetMaAssocWithVid (i4FsEcfmMipVid,
                                                           i4FsEcfmMipMdLevel);
                }
            }

            else

            {

                /* If user has not update Default MD level */
                /* MIP is to be created with MA */
                pMaNode =
                    EcfmCcUtilGetMaAssocWithVid (i4FsEcfmMipVid,
                                                 i4FsEcfmMipMdLevel);

                /* Not checking the UP MEP associated with MA, as it must
                 * have been checked in test routine (TestMipRowStatus) */
            }

            /* Add Mip new node in global Mip Table in 
             * both the tasks */
            if ((EcfmCcUtilAddMipEntry (pMipNewNode, pMaNode)) != ECFM_SUCCESS)
            {
                /*Deallocate memory assigned to new node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL,
                                     (UINT1 *) (pMipNewNode));
                pMipNewNode = NULL;
                if (!(ECFM_IS_MEP_ISID_AWARE (i4FsEcfmMipVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                      (INT4) u4IfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                      i4SetValFsEcfmMipRowStatus));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                return SNMP_FAILURE;
            }
            if (i4SetValFsEcfmMipRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT)
            {
                pMipNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
            }
            else
            {
                pMipNewNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
                /* Note: Workaround done to send create and wait and active instead of 
                 * create and go*/

                if (!(ECFM_IS_MEP_ISID_AWARE (i4FsEcfmMipVid)))
                {
                    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                      (INT4) u4IfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                      ECFM_ROW_STATUS_CREATE_AND_WAIT));
                    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                }
                i4SetValFsEcfmMipRowStatus = ECFM_ROW_STATUS_ACTIVE;

            }
            /* Set Mip's rowstatus in LBLT task */
            EcfmLbLtConfMipRowStatus (pMipNewNode->u2PortNum,
                                      pMipNewNode->u1MdLevel,
                                      pMipNewNode->u4VlanIdIsid,
                                      pMipNewNode->u1RowStatus,
                                      ECFM_CC_CURR_CONTEXT_ID ());
            ECFM_SET_DEMUX_PORT (pMipNewNode->u2PortNum);
            break;

        case ECFM_ROW_STATUS_DESTROY:
            /* Remove Mip node from Mip Table and 
             * from StackTable in Global info */
            EcfmCcUtilDeleteMipEntry (pMipNode);
            /* Release memory allocated to Mip node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL, (UINT1 *) (pMipNode));
            pMipNode = NULL;
            pPortInfo = ECFM_CC_GET_PORT_INFO (i4FsEcfmMipIfIndex);
            StackInfo.u1MdLevel = 0;
            StackInfo.u4VlanIdIsid = 0;
            StackInfo.u1Direction = 0;
            if ((pPortInfo != NULL) && RBTreeGetNext
                (pPortInfo->StackInfoTree, (tRBElem *) & StackInfo,
                 NULL) == NULL)
            {
                ECFM_RESET_DEMUX_PORT ((UINT2) i4FsEcfmMipIfIndex);
            }

            break;

    }

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (i4FsEcfmMipVid)))
    {
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                          (INT4) u4IfIndex,
                          i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                          i4SetValFsEcfmMipRowStatus));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipActive
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                testValFsEcfmMipActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipActive (UINT4 *pu4ErrorCode, INT4 i4FsEcfmMipIfIndex,
                          INT4 i4FsEcfmMipMdLevel, INT4 i4FsEcfmMipVid,
                          INT4 i4TestValFsEcfmMipActive)
{
    tEcfmCcMipInfo     *pMipNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Validate MipActive value, that is to be set */
    if ((i4TestValFsEcfmMipActive != ECFM_SNMP_FALSE) &&
        (i4TestValFsEcfmMipActive != ECFM_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Get MIP entry corresponding to IfIndex, MdLevel, VlanId */
    pMipNode = EcfmCcUtilGetMipEntry (i4FsEcfmMipIfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid);
    if (pMipNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* MIP entry corresponding to IfIndex, MdLevel, VlanId exists */
    /* Check whether row corresponding to MIP entry can be modified or not */
    if (pMipNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMipRowStatus
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                testValFsEcfmMipRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMipRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsEcfmMipIfIndex,
                             INT4 i4FsEcfmMipMdLevel, INT4 i4FsEcfmMipVid,
                             INT4 i4TestValFsEcfmMipRowStatus)
{
    tEcfmCcMipInfo     *pMipNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4ErrMsg = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Validate Row status value */
    if ((i4TestValFsEcfmMipRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValFsEcfmMipRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate index range */
    if (ECFM_VALIDATE_MIP_TABLE_INDICES
        (i4FsEcfmMipIfIndex, i4FsEcfmMipMdLevel, i4FsEcfmMipVid))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Get MIP entry corresponding to IfIndex, mdlevel, vlanId */
    pMipNode = EcfmCcUtilGetMipEntry (i4FsEcfmMipIfIndex,
                                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid);
    if (pMipNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Mip Entry for the given Index \r\n");

        /* MIP entry corresponding to IfIndex, mdlevel, vlanId does not exists,
         * and user wants to change its row status */
        if (i4TestValFsEcfmMipRowStatus == ECFM_ROW_STATUS_DESTROY)
        {

            /* As per RFC1905, Deleting a non-existing row status should return success */
            return SNMP_SUCCESS;
        }

        else if ((i4TestValFsEcfmMipRowStatus != ECFM_ROW_STATUS_CREATE_AND_GO)
                 && (i4TestValFsEcfmMipRowStatus !=
                     ECFM_ROW_STATUS_CREATE_AND_WAIT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Check Port corresponding to IfIndex should be created  */
        pPortInfo = ECFM_CC_GET_PORT_INFO ((UINT2) i4FsEcfmMipIfIndex);

        if ((pPortInfo == NULL) ||
            (EcfmLbLtPortCreated
             ((UINT2) i4FsEcfmMipIfIndex,
              ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_TRUE))
        {
            CLI_SET_ERR (CLI_ECFM_PORT_NOT_CREATED_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Port should not be part of LA port */
        if (pPortInfo->u2ChannelPortNum != 0)
        {
            CLI_SET_ERR (CLI_ECFM_MIP_CONFIG_LA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (ECFM_CC_ISPORT_SISP_LOG ((UINT2) i4FsEcfmMipIfIndex,
                                     pTempPortInfo) == ECFM_TRUE)
        {
            if (EcfmL2IwfMiIsVlanUntagMemberPort (ECFM_CC_CURR_CONTEXT_ID (),
                                                  (UINT2) i4FsEcfmMipVid,
                                                  pPortInfo->u4IfIndex)
                == OSIX_TRUE)
            {
                /* SISP logical interface is to be configured with MEP for vlan
                 * for which this port is untagged member port. This 
                 * configuration is invalid.
                 * */
                CLI_SET_ERR (CLI_ECFM_MEP_UNTAG_SISP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        /* Check whether MIP can be created for IfIndex, mdlevel, vlanId */
        if (EcfmSnmpLwIsInfoConfiguredForMip (i4FsEcfmMipIfIndex,
                                              i4FsEcfmMipMdLevel,
                                              i4FsEcfmMipVid,
                                              &u4ErrMsg) != ECFM_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (u4ErrMsg);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    /* MIP entry corresponding to IfIndex, mdlevel, vlanId exists,
     * and its row status is same as user wants to set */
    if (pMipNode->u1RowStatus == (UINT1) i4TestValFsEcfmMipRowStatus)
    {
        return SNMP_SUCCESS;
    }
    /* MIP entry corresponding to IfIndex, mdlevel, vlanId exists,
     * and user wants to create new MIP entry with same indices */
    if ((i4TestValFsEcfmMipRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT) ||
        (i4TestValFsEcfmMipRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMipTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMipTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmMipCcmDbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmMipCcmDbTable
 Input       :  The Indices
                FsEcfmMipCcmFid
                FsEcfmMipCcmSrcAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmMipCcmDbTable (UINT4 u4FsEcfmMipCcmFid,
                                             tMacAddr FsEcfmMipCcmSrcAddr)
{
    tEcfmCcMipCcmDbInfo *pMipDbNode = NULL;
    UINT1               au1TempMacAddr[ECFM_MAC_ADDR_LENGTH];

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (au1TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    ECFM_MEMCPY (au1TempMacAddr, FsEcfmMipCcmSrcAddr, ECFM_MAC_ADDR_LENGTH);

    /* Get MIPDb entry corresponding to indices MdLevel, VlanId, IfIndex */
    pMipDbNode = EcfmCcUtilGetMipCcmDbEntry (u4FsEcfmMipCcmFid, au1TempMacAddr);

    if (pMipDbNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmMipCcmDbTable
 Input       :  The Indices
                FsEcfmMipCcmFid
                FsEcfmMipCcmSrcAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmMipCcmDbTable (UINT4 *pu4FsEcfmMipCcmFid,
                                     tMacAddr * pFsEcfmMipCcmSrcAddr)
{
    tMacAddr            TempMacAddr;

    ECFM_MEMSET (&TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    return (nmhGetNextIndexFsEcfmMipCcmDbTable (0, pu4FsEcfmMipCcmFid,
                                                TempMacAddr,
                                                pFsEcfmMipCcmSrcAddr));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmMipCcmDbTable
 Input       :  The Indices
                FsEcfmMipCcmFid
                nextFsEcfmMipCcmFid
                FsEcfmMipCcmSrcAddr
                nextFsEcfmMipCcmSrcAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmMipCcmDbTable (UINT4 u4FsEcfmMipCcmFid,
                                    UINT4 *pu4NextFsEcfmMipCcmFid,
                                    tMacAddr FsEcfmMipCcmSrcAddr,
                                    tMacAddr * pNextFsEcfmMipCcmSrcAddr)
{
    tEcfmCcMipCcmDbInfo MipDbInfo;
    tEcfmCcMipCcmDbInfo *pMipDbNextNode = NULL;

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (&MipDbInfo, ECFM_INIT_VAL, ECFM_CC_MIP_CCM_DB_INFO_SIZE);
    MipDbInfo.u2Fid = (UINT2) u4FsEcfmMipCcmFid;
    ECFM_MEMCPY (MipDbInfo.SrcMacAddr, FsEcfmMipCcmSrcAddr,
                 ECFM_MAC_ADDR_LENGTH);

    /* Get MIPDb entry corresponding to indices next to Fid, source address */
    pMipDbNextNode = (tEcfmCcMipCcmDbInfo *) RBTreeGetNext
        (ECFM_CC_MIP_CCM_DB_TABLE, (tRBElem *) (&MipDbInfo), NULL);

    if (pMipDbNextNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIPDb entry corresponding to indices next to Fid, source address */
    /* Set the next indices from corresponding MIPDb next node */
    *pu4NextFsEcfmMipCcmFid = (UINT4) (pMipDbNextNode->u2Fid);
    ECFM_MEMCPY (pNextFsEcfmMipCcmSrcAddr, pMipDbNextNode->SrcMacAddr,
                 ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmMipCcmIfIndex
 Input       :  The Indices
                FsEcfmMipCcmFid
                FsEcfmMipCcmSrcAddr

                The Object 
                retValFsEcfmMipCcmIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMipCcmIfIndex (UINT4 u4FsEcfmMipCcmFid,
                           tMacAddr FsEcfmMipCcmSrcAddr,
                           INT4 *pi4RetValFsEcfmMipCcmIfIndex)
{
    tEcfmCcMipCcmDbInfo *pMipDbNode = NULL;
    UINT1               au1TempMacAddr[ECFM_MAC_ADDR_LENGTH];

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (au1TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    ECFM_MEMCPY (au1TempMacAddr, FsEcfmMipCcmSrcAddr, ECFM_MAC_ADDR_LENGTH);

    /* Get MIPDb entry corresponding to indices MdLevel, VlanId, IfIndex */
    pMipDbNode = EcfmCcUtilGetMipCcmDbEntry (u4FsEcfmMipCcmFid, au1TempMacAddr);

    if (pMipDbNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Set IfIndex from MipDb Entry corresponding to Fid and SrcAddress */
    *pi4RetValFsEcfmMipCcmIfIndex = (INT4) (pMipDbNode->u2PortNum);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmGlobalCcmOffload
 Input       :  The Indices

                The Object 
                retValFsEcfmGlobalCcmOffload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmGlobalCcmOffload (INT4 *pi4RetValFsEcfmGlobalCcmOffload)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "SNMP:ECFM module is Shutdown\r\n");
        *pi4RetValFsEcfmGlobalCcmOffload = ECFM_CCM_OFFLOAD_DISABLE;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsEcfmGlobalCcmOffload =
        ECFM_CONVERT_TO_SNMP_BOOL (ECFM_GLOBAL_CCM_OFFLOAD_STATUS);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmGlobalCcmOffload
 Input       :  The Indices

                The Object 
                setValFsEcfmGlobalCcmOffload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmGlobalCcmOffload (INT4 i4SetValFsEcfmGlobalCcmOffload)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
#endif
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
#ifdef NPAPI_WANTED
    UINT1               u1Type = ECFM_INIT_VAL;
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL, ECFM_HW_MEP_HANDLER_SIZE);
#endif
    ECFM_MEMSET (&SnmpNotifyInfo, ECFM_INIT_VAL, sizeof (tSnmpNotifyInfo));

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC,
                     "ECFM system control is shutdown No MEP exists. \r\n");
        return SNMP_SUCCESS;
    }

    /* Check if the value is already set. If yes, simply return */
    if (ECFM_GLOBAL_CCM_OFFLOAD_STATUS == i4SetValFsEcfmGlobalCcmOffload)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC,
                     "ECFM offload status is already set to given value. \r\n");
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (ECFM_HW_CCMOFFLOAD_SUPPORT () == ECFM_FALSE)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: CCM offloading not supported in HW\n");
        return SNMP_FAILURE;
    }
#endif

    EcfmRedSynchHwAuditCmdEvtInfo (ECFM_CC_CURR_CONTEXT_ID (),
                                   ECFM_RED_GLOBAL_OFF_CMD,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, ECFM_TRUE);

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    /* Check if CCMOffloading is already enabled then check for all the MEP in
     * system  */
    if (i4SetValFsEcfmGlobalCcmOffload == ECFM_CCM_OFFLOAD_ENABLE)
    {
        pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);
        while (pMepNode != NULL)
        {
            if (pMepNode->b1MepCcmOffloadHwStatus != ECFM_TRUE)
            {
                ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                             ECFM_CC_PDUSM_INFO_SIZE);
                PduSmInfo.pMepInfo = pMepNode;
#ifdef MBSM_WANTED
                /* If No port is present in the system for this
                 * MEP then don't call offloading NPAPI */
                if (EcfmMbsmIsPortPresent (pMepNode->pPortInfo->u4IfIndex,
                                           pMepNode->u4PrimaryVidIsid,
                                           pMepNode->u1Direction) == ECFM_FALSE)
                {

                    tEcfmCcMepInfo     *pMepInfo = NULL;
                    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;

                    pRmepInfo = RBTreeGetFirst (ECFM_CC_RMEP_TABLE);
                    while (pRmepInfo != NULL)
                    {
                        /* Get MEP Node with which this Remote
                         * MEP is associated */
                        pMepInfo = pRmepInfo->pMepInfo;

                        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;

                        ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                                     sizeof (tEcfmCcPduSmInfo));
                        /* Assign MepInfo and Remote MEP Info 
                         * in the PduSmInfo for calling the SM */
                        PduSmInfo.pRMepInfo = pRmepInfo;
                        PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                        /* Raise Connectivity Loss trap */
                        /* Call the Remote MEP State Machine for TIME_OUT event */
                        EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);

                        /* Get Next RMEP Node from the Global RBTRee maintained */
                        pRmepInfo =
                            RBTreeGetNext (ECFM_CC_RMEP_TABLE, pRmepInfo, NULL);
                    }
                }
                else
#endif
                {
#ifdef NPAPI_WANTED
                    if (ECFM_MEMCMP
                        (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
                         ECFM_HW_MEP_HANDLER_SIZE) != 0)
                    {
                        /* There is a change in offload status so MEP created in h/w
                         * must be deleted first, and h/w handler has valid value.
                         */
                        MEMSET (&EcfmHwInfo, ECFM_INIT_VAL,
                                sizeof (tEcfmHwParams));
                        EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);

                        EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                            pMepNode->u4IfIndex;
                        if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_DELETE,
                                                     &EcfmHwInfo) ==
                            FNP_FAILURE)
                        {
                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                         "nmhSetFsEcfmGlobalCcmOffload :MEP deletion"
                                         "indication to H/W failed. \r\n");
                            return SNMP_FAILURE;
                        }
                        MEMSET (pMepNode->au1HwMepHandler, ECFM_INIT_VAL,
                                ECFM_HW_MEP_HANDLER_SIZE);
                    }

                    /* Create MEP in h/w with updated b1MepCcmOffloadStatus */
                    MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
                    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                        pMepNode->u4IfIndex;
                    if (EcfmFsMiEcfmHwCallNpApi
                        (ECFM_NP_MEP_CREATE, &EcfmHwInfo) == FNP_FAILURE)
                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "nmhSetFsEcfmGlobalCcmOffload :MEP Creation"
                                     "indication to H/W failed. \r\n");
                        return SNMP_FAILURE;
                    }
                    if (pMepNode->b1Active == ECFM_TRUE)
                    {
                        MEMSET (&EcfmHwInfo, ECFM_INIT_VAL,
                                sizeof (tEcfmHwParams));
                        EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);
                        EcfmHwInfo.EcfmHwEvents = ECFM_HW_ALL_EVENT_BITS;
                        u1Type = FS_MI_ECFM_ENABLE_HW_EVENTS;
                        if (EcfmFsMiEcfmHwCallNpApi (u1Type,
                                                     &EcfmHwInfo) !=
                            FNP_SUCCESS)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "nmhSetFsEcfmGlobalCcmOffload :"
                                         "Enable MEP events returned Failure. \r\n");
                            return ECFM_FAILURE;
                        }
                    }
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "SNMP: b1MepCcmOffloadStatus is False \r\n");
                    /* Call the Create Tx/Rx call for Mep */
                    if (EcfmCcmOffCreateTxRxForMep (pMepNode) != ECFM_SUCCESS)
                    {
                        pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
                    }
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC, "SNMP:"
                                 "b1MepCcmOffloadStatus is Set TRUE \r\n");
                    pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
                    if ((pMepNode->u1Direction == ECFM_MP_DIR_UP) &&
                        (ECFM_HW_UPMEP_OFFLOAD_SUPPORT () == ECFM_TRUE))
                    {
                        pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
                    }
                    else
                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_CRITICAL_TRC,
                                     "b1MepCcmOffloadStatus is False for Up Mep \r\n");
                    }
#endif
                }
            }
            ECFM_CC_TRC_ARG1 (ECFM_MGMT_TRC, "SNMP:"
                              "OFFLOAD: OFFLOADING Start For MEPID %d",
                              pMepNode->u2MepId);
            if (pMepNode->u1Direction == ECFM_MP_DIR_UP)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_CRITICAL_TRC, "SNMP:"
                             "b1MepCcmOffloadStatus is False for Up Mep \r\n");
            }
            else
            {
                pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
            }
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
        }

        /* check for all meps in system & enable ccm offloading for all the
         * meps */
        ECFM_GLOBAL_CCM_OFFLOAD_STATUS = ECFM_TRUE;
    }
    if (i4SetValFsEcfmGlobalCcmOffload == ECFM_CCM_OFFLOAD_DISABLE)
    {
        pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);
        while (pMepNode != NULL)
        {
            if (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE)
            {
                /* Disable Tx & Rx of CCM From Offloaded Module */
                /* Get Stats from hardware */
                /* Start operation in software */
                /* Delete Infor from hardware */
                if (pMepNode->pMaInfo != NULL)
                {
                    if ((pMepNode->pMaInfo->u1CcmInterval ==
                         ECFM_CCM_INTERVAL_300Hz)
                        || (pMepNode->pMaInfo->u1CcmInterval ==
                            ECFM_CCM_INTERVAL_10_Ms))
                    {
                        pMepNode->pMaInfo->u1CcmInterval =
                            ECFM_CCM_INTERVAL_1_S;
                        RM_GET_SEQ_NUM (&u4SeqNum);

                        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                              FsMIEcfmMaCcmInterval, u4SeqNum,
                                              FALSE, EcfmCcLock, EcfmCcUnLock,
                                              ECFM_TABLE_INDICES_COUNT_THREE,
                                              SNMP_SUCCESS);

                        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i",
                                          ECFM_CC_CURR_CONTEXT_ID (),
                                          pMepNode->pMaInfo->u4MdIndex,
                                          pMepNode->pMaInfo->u4MaIndex,
                                          pMepNode->pMaInfo->u1CcmInterval));
                        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                    }
                }
                else
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "nmhSetFsEcfmGlobalCcmOffload :Set default cc interval failed \r\n");
                    return SNMP_FAILURE;
                }
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "SNMP: b1MepCcmOffloadStatus" "is True \r\n");
                EcfmCcmOffDeleteTxRxForMep (pMepNode);
                ECFM_CC_TRC_ARG2 (ECFM_MGMT_TRC, "SNMP:"
                                  "OFFLOAD: OFFLOADING Delete For MEPID %d"
                                  "on Interface %d\r\n", pMepNode->u2MepId,
                                  pMepNode->pPortInfo->u4IfIndex);
#ifdef NPAPI_WANTED
                if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
                                 ECFM_HW_MEP_HANDLER_SIZE) != 0)
                {
                    /* There is a change in offload status so MEP created in h/w
                     * must be deleted first if h/w handler has valid value.
                     */
                    MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
                    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);

                    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                        pMepNode->u4IfIndex;
                    if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_DELETE,
                                                 &EcfmHwInfo) == FNP_FAILURE)
                    {
                        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                     "nmhSetFsEcfmGlobalCcmOffload :MEP deletion"
                                     "indication to H/W failed. \r\n");
                        return SNMP_FAILURE;
                    }
                    MEMSET (pMepNode->au1HwMepHandler, ECFM_INIT_VAL,
                            ECFM_HW_MEP_HANDLER_SIZE);
                }
                if (RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode) !=
                    NULL)
                {
                    /* Remove the entry from global h/w MEP table */
                    RBTreeRem (ECFM_CC_GLOBAL_HW_MEP_TABLE, pMepNode);
                }
                /* Create MEP in h/w with updated b1MepCcmOffloadStatus */

                MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
                EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);

                EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                    pMepNode->u4IfIndex;
                pMepNode->b1MepCcmOffloadStatus = ECFM_FALSE;

                if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_CREATE,
                                             &EcfmHwInfo) == FNP_FAILURE)
                {
                    ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                 "nmhSetFsEcfmGlobalCcmOffload :MEP Creation"
                                 "indication to H/W failed. \r\n");
                    return SNMP_FAILURE;
                }
#endif
            }
            pMepNode->b1MepCcmOffloadStatus = ECFM_FALSE;
            pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
            ECFM_CC_TRC_ARG2 (ECFM_MGMT_TRC, "SNMP:"
                              "OFFLOAD: For MEPID %d Offloading Start in "
                              "Software on Interface %d\r\n",
                              pMepNode->u2MepId,
                              pMepNode->pPortInfo->u4IfIndex);
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
        }
        /* check for all mep in system & disable ccm offloading for all the
         * meps */
        ECFM_GLOBAL_CCM_OFFLOAD_STATUS = ECFM_FALSE;
    }

    /* Set the status of the Hardware offload Engine */
#ifdef NPAPI_WANTED
    if (ECFM_SUCCESS != EcfmCcmOffStatus (ECFM_GLOBAL_CCM_OFFLOAD_STATUS))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: CCM offloading state not set in HW\n");
        return SNMP_FAILURE;
    }
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);

    ECFM_MEMSET (&SnmpNotifyInfo, ECFM_INIT_VAL, sizeof (tSnmpNotifyInfo));
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmGlobalCcmOffload, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValFsEcfmGlobalCcmOffload));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmGlobalCcmOffload
 Input       :  The Indices

                The Object 
                testValFsEcfmGlobalCcmOffload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmGlobalCcmOffload (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsEcfmGlobalCcmOffload)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (ECFM_HW_CCMOFFLOAD_SUPPORT () == ECFM_FALSE)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: CCM offloading not supported in HW\n");
        return SNMP_FAILURE;
    }
#endif
    /* Validate CCMOffload input values */
    if ((i4TestValFsEcfmGlobalCcmOffload != ECFM_CCM_OFFLOAD_ENABLE) &&
        (i4TestValFsEcfmGlobalCcmOffload != ECFM_CCM_OFFLOAD_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid Value \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmGlobalCcmOffload
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmGlobalCcmOffload (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmDynMipPreventionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmDynMipPreventionTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmDynMipPreventionTable (INT4 i4FsEcfmMipIfIndex,
                                                     INT4 i4FsEcfmMipMdLevel,
                                                     INT4 i4FsEcfmMipVid)
{
    tEcfmCcMipPreventInfo *pMipNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }
    /* Get MIP entry corresponding to indices MdLevel, VlanId, IfIndex */
    pMipNode =
        EcfmCcUtilGetMipPreventEntry ((UINT2) i4FsEcfmMipIfIndex,
                                      (UINT1) i4FsEcfmMipMdLevel,
                                      (UINT4) i4FsEcfmMipVid);

    if (pMipNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmDynMipPreventionTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmDynMipPreventionTable (INT4 *pi4FsEcfmMipIfIndex,
                                             INT4 *pi4FsEcfmMipMdLevel,
                                             INT4 *pi4FsEcfmMipVid)
{
    return (nmhGetNextIndexFsEcfmDynMipPreventionTable
            (0, pi4FsEcfmMipIfIndex, 0, pi4FsEcfmMipMdLevel, 0,
             pi4FsEcfmMipVid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmDynMipPreventionTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                nextFsEcfmMipIfIndex
                FsEcfmMipMdLevel
                nextFsEcfmMipMdLevel
                FsEcfmMipVid
                nextFsEcfmMipVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmDynMipPreventionTable (INT4 i4FsEcfmMipIfIndex,
                                            INT4 *pi4NextFsEcfmMipIfIndex,
                                            INT4 i4FsEcfmMipMdLevel,
                                            INT4 *pi4NextFsEcfmMipMdLevel,
                                            INT4 i4FsEcfmMipVid,
                                            INT4 *pi4NextFsEcfmMipVid)
{
    tEcfmCcMipPreventInfo MipInfo;
    tEcfmCcMipPreventInfo *pMipNextNode = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;

    if (EcfmVcmGetContextInfoFromIfIndex
        ((UINT4) i4FsEcfmMipIfIndex, &u4ContextId,
         &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (&MipInfo, ECFM_INIT_VAL, ECFM_CC_MIP_PREVENT_INFO_SIZE);
    MipInfo.u4ContextId = u4ContextId;
    MipInfo.u4IfIndex = (UINT4) i4FsEcfmMipIfIndex;
    MipInfo.u1MdLevel = (UINT1) i4FsEcfmMipMdLevel;
    MipInfo.u4VlanIdIsid = (UINT4) i4FsEcfmMipVid;

    /* Get MIP entry corresponding to indices next to IfIndex, Level, VlanId */
    pMipNextNode = (tEcfmCcMipPreventInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE, (tRBElem *) (&MipInfo), NULL);

    if (pMipNextNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIP entry corresponding to indices next to IfIndex, Level, VlanId 
     */
    /* Set the next indices from corresponding MIP next node */
    *pi4NextFsEcfmMipIfIndex = (INT4) (pMipNextNode->u4IfIndex);
    *pi4NextFsEcfmMipMdLevel = (INT4) (pMipNextNode->u1MdLevel);
    *pi4NextFsEcfmMipVid = (INT4) (pMipNextNode->u4VlanIdIsid);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmDynMipPreventionRowStatus
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                retValFsEcfmDynMipPreventionRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsEcfmDynMipPreventionRowStatus
    (INT4 i4FsEcfmMipIfIndex,
     INT4 i4FsEcfmMipMdLevel,
     INT4 i4FsEcfmMipVid, INT4 *pi4RetValFsEcfmDynMipPreventionRowStatus)
{
    tEcfmCcMipPreventInfo *pMipNode = NULL;

    /* Get MIP Prevention entry corresponding to IfIndex, level and vlanId */
    pMipNode =
        EcfmCcUtilGetMipPreventEntry ((UINT2) i4FsEcfmMipIfIndex,
                                      (UINT1) i4FsEcfmMipMdLevel,
                                      (UINT4) i4FsEcfmMipVid);
    if (pMipNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MIP Prevention entry corresponding to IfIndex, level and vlanId exists */
    *pi4RetValFsEcfmDynMipPreventionRowStatus = (INT4) (pMipNode->u1RowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmDynMipPreventionRowStatus
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                setValFsEcfmDynMipPreventionRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmDynMipPreventionRowStatus (INT4 i4FsEcfmMipIfIndex,
                                       INT4 i4FsEcfmMipMdLevel,
                                       INT4 i4FsEcfmMipVid,
                                       INT4
                                       i4SetValFsEcfmDynMipPreventionRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcMipPreventInfo *pMipNode = NULL;
    tEcfmCcMipPreventInfo *pMipNewNode = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDynMipPreventionRowStatus,
                          u4SeqNum, TRUE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    /* Get IfIndex from Local Port for the current Context */
    ECFM_GET_IFINDEX_FROM_LOCAL_PORT (ECFM_CC_CURR_CONTEXT_ID (),
                                      (UINT2) i4FsEcfmMipIfIndex, &u4IfIndex);

    switch (i4SetValFsEcfmDynMipPreventionRowStatus)
    {
        case ECFM_ROW_STATUS_CREATE_AND_WAIT:    /*Intentional fallthrough MSR */
        case ECFM_ROW_STATUS_CREATE_AND_GO:
            /* Create node for Mip */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MIP_PREVENT_TABLE (pMipNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "nmhSetFsEcfmDynMipPreventionRowStatus:"
                             "Row Status Create and Go"
                             "Allocation for Mip Node Failed \r\n");
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  (INT4) u4IfIndex,
                                  i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                  i4SetValFsEcfmDynMipPreventionRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            ECFM_MEMSET (pMipNewNode, ECFM_INIT_VAL,
                         ECFM_CC_MIP_PREVENT_INFO_SIZE);

            pMipNewNode->u4ContextId = u4CurrContextId;
            pMipNewNode->u4IfIndex = u4IfIndex;
            pMipNewNode->u1MdLevel = (UINT1) i4FsEcfmMipMdLevel;
            pMipNewNode->u4VlanIdIsid = (UINT4) i4FsEcfmMipVid;

            pMipNewNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

            /* Add Mip new node in the MIP prevent table */
            if ((EcfmCcUtilAddMipPreventEntry (pMipNewNode)) != ECFM_SUCCESS)
            {
                /*Deallocate memory assigned to new node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_PREVENT_TABLE_POOL,
                                     (UINT1 *) (pMipNewNode));
                pMipNewNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  (INT4) u4IfIndex,
                                  i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                                  i4SetValFsEcfmDynMipPreventionRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* This should be called to delete the dynamically created 
             * MIPs in the following scenario 
             * - MEP was created before creating this table */

            EcfmCcUtilEvaluateAndCreateMip (u4IfIndex, i4FsEcfmMipVid,
                                            ECFM_TRUE);

            break;

        case ECFM_ROW_STATUS_DESTROY:
            /* Get Mip entry corresponding to MdIndex, MaIndex, MepId */
            pMipNode = EcfmCcUtilGetMipPreventEntry ((UINT2) i4FsEcfmMipIfIndex,
                                                     (UINT1) i4FsEcfmMipMdLevel,
                                                     (UINT4) i4FsEcfmMipVid);
            if (pMipNode != NULL)
            {
                /* Remove Mip node from Mip Table and 
                 * from StackTable in Global info */
                EcfmCcUtilDelMipPreventEntry (pMipNode);
                /* Release memory allocated to Mip node */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_PREVENT_TABLE_POOL,
                                     (UINT1 *) (pMipNode));
                pMipNode = NULL;
            }
            break;

    }

    /* Sending Trigger to MSR */

    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                      (INT4) u4IfIndex,
                      i4FsEcfmMipMdLevel, i4FsEcfmMipVid,
                      i4SetValFsEcfmDynMipPreventionRowStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmDynMipPreventionRowStatus
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid

                The Object 
                testValFsEcfmDynMipPreventionRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmDynMipPreventionRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsEcfmMipIfIndex,
                                          INT4 i4FsEcfmMipMdLevel,
                                          INT4 i4FsEcfmMipVid,
                                          INT4
                                          i4TestValFsEcfmDynMipPreventionRowStatus)
{
    tEcfmCcMipPreventInfo *pMipNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate Row status value */
    if ((i4TestValFsEcfmDynMipPreventionRowStatus !=
         ECFM_ROW_STATUS_CREATE_AND_GO) &&
        (i4TestValFsEcfmDynMipPreventionRowStatus != ECFM_ROW_STATUS_DESTROY))
    {
        CLI_SET_ERR (CLI_ECFM_INVALID_ROWSTATUS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate index range */
    if (ECFM_VALIDATE_MIP_TABLE_INDICES
        (i4FsEcfmMipIfIndex, i4FsEcfmMipMdLevel, i4FsEcfmMipVid))
    {
        CLI_SET_ERR (CLI_ECFM_INVALID_INDEX);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Get MIP entry corresponding to IfIndex, mdlevel, vlanId */
    pMipNode = EcfmCcUtilGetMipPreventEntry ((UINT2) i4FsEcfmMipIfIndex,
                                             (UINT1) i4FsEcfmMipMdLevel,
                                             (UINT4) i4FsEcfmMipVid);
    if (pMipNode != NULL)
    {
        /* MIP entry corresponding to IfIndex, mdlevel, vlanId exists,
         * and user wants to create new MIP entry with same indices */
        if (i4TestValFsEcfmDynMipPreventionRowStatus ==
            ECFM_ROW_STATUS_CREATE_AND_GO)
        {
            CLI_SET_ERR (CLI_ECFM_MIP_PREVENT_ENT_PRESENT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* User tries to delete an entry which is not created */
        if (i4TestValFsEcfmDynMipPreventionRowStatus == ECFM_ROW_STATUS_DESTROY)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmDynMipPreventionTable
 Input       :  The Indices
                FsEcfmMipIfIndex
                FsEcfmMipMdLevel
                FsEcfmMipVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmDynMipPreventionTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmRemoteMepDbExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable (UINT4 u4FsEcfmMdIndex,
                                                  UINT4 u4FsEcfmMaIndex,
                                                  UINT4 u4FsEcfmMepIdentifier,
                                                  UINT4 u4FsEcfmRMepIdentifier)
{
    tEcfmCcRMepDbInfo  *pCcRMepDbInfo = NULL;

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate the indices */
    if (ECFM_VALIDATE_RMEP_TABLE_INDICES (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Indexes\n");
        return SNMP_FAILURE;
    }

    /* Get the corresponding entry */
    pCcRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                            u4FsEcfmMaIndex,
                                            u4FsEcfmMepIdentifier,
                                            u4FsEcfmRMepIdentifier);

    /* Check whether the entry is present or not */
    if (pCcRMepDbInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmRemoteMepDbExTable (UINT4 *pu4FsEcfmMdIndex,
                                          UINT4 *pu4FsEcfmMaIndex,
                                          UINT4 *pu4FsEcfmMepIdentifier,
                                          UINT4 *pu4FsEcfmRMepIdentifier)
{

    return (nmhGetNextIndexFsEcfmRemoteMepDbExTable (0, pu4FsEcfmMdIndex,
                                                     0, pu4FsEcfmMaIndex,
                                                     0,
                                                     pu4FsEcfmMepIdentifier,
                                                     0,
                                                     pu4FsEcfmRMepIdentifier));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsEcfmMdIndex
                nextFsEcfmMdIndex
                FsEcfmMaIndex
                nextFsEcfmMaIndex
                FsEcfmMepIdentifier
                nextFsEcfmMepIdentifier
                FsEcfmRMepIdentifier
                nextFsEcfmRMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmRemoteMepDbExTable (UINT4 u4FsEcfmMdIndex,
                                         UINT4 *pu4NextFsEcfmMdIndex,
                                         UINT4 u4FsEcfmMaIndex,
                                         UINT4 *pu4NextFsEcfmMaIndex,
                                         UINT4 u4FsEcfmMepIdentifier,
                                         UINT4 *pu4NextFsEcfmMepIdentifier,
                                         UINT4 u4FsEcfmRMepIdentifier,
                                         UINT4 *pu4NextFsEcfmRMepIdentifier)
{
    tEcfmCcRMepDbInfo   CcRMepDbInfo;
    tEcfmCcRMepDbInfo  *pCcRMepDbNextInfo = NULL;

    ECFM_MEMSET (&CcRMepDbInfo, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Moduel is Shutdown\n");
        return SNMP_FAILURE;
    }

    CcRMepDbInfo.u4MdIndex = u4FsEcfmMdIndex;
    CcRMepDbInfo.u4MaIndex = u4FsEcfmMaIndex;
    CcRMepDbInfo.u2MepId = (UINT2) u4FsEcfmMepIdentifier;
    CcRMepDbInfo.u2RMepId = (UINT2) u4FsEcfmRMepIdentifier;

    /* Getting next node */
    pCcRMepDbNextInfo =
        (tEcfmCcRMepDbInfo *) RBTreeGetNext (ECFM_CC_RMEP_TABLE,
                                             (tRBElem *) & CcRMepDbInfo, NULL);

    /* Check whether the entry is present or not */
    if (pCcRMepDbNextInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No next Remote MEP DB Node"
                     " for given indices\n");
        return SNMP_FAILURE;
    }

    /* If the entry is found then get the corresponding values */
    *pu4NextFsEcfmMdIndex = pCcRMepDbNextInfo->u4MdIndex;
    *pu4NextFsEcfmMaIndex = pCcRMepDbNextInfo->u4MaIndex;
    *pu4NextFsEcfmMepIdentifier = (UINT4) (pCcRMepDbNextInfo->u2MepId);
    *pu4NextFsEcfmRMepIdentifier = (UINT4) (pCcRMepDbNextInfo->u2RMepId);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepCcmSequenceNum
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepCcmSequenceNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepCcmSequenceNum (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                UINT4 u4FsEcfmRMepIdentifier,
                                UINT4 *pu4RetValFsEcfmRMepCcmSequenceNum)
{
    tEcfmCcRMepDbInfo  *pCcRMepDbInfo = NULL;

    /* Get the entry corresponding to the indices */
    pCcRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                            u4FsEcfmMaIndex,
                                            u4FsEcfmMepIdentifier,
                                            u4FsEcfmRMepIdentifier);

    /* Check whether the entry is present or not */
    if (pCcRMepDbInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* If the entry is found then get the corresponding CCM seq no. */
    *pu4RetValFsEcfmRMepCcmSequenceNum = pCcRMepDbInfo->u4SeqNum;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepPortStatusDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepPortStatusDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepPortStatusDefect (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  INT4 *pi4RetValFsEcfmRMepPortStatusDefect)
{
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmRMepPortStatusDefect =
        ECFM_CONVERT_TO_SNMP_BOOL (pRMepDbInfo->b1RMepPortStatusDefect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepInterfaceStatusDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepInterfaceStatusDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepInterfaceStatusDefect (UINT4 u4FsEcfmMdIndex,
                                       UINT4 u4FsEcfmMaIndex,
                                       UINT4 u4FsEcfmMepIdentifier,
                                       UINT4 u4FsEcfmRMepIdentifier,
                                       INT4
                                       *pi4RetValFsEcfmRMepInterfaceStatusDefect)
{
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmRMepInterfaceStatusDefect =
        ECFM_CONVERT_TO_SNMP_BOOL (pRMepDbInfo->b1RMepInterfaceStatusDefect);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepCcmDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepCcmDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepCcmDefect (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 u4FsEcfmRMepIdentifier,
                           INT4 *pi4RetValFsEcfmRMepCcmDefect)
{
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmRMepCcmDefect =
        ECFM_CONVERT_TO_SNMP_BOOL (pRMepDbInfo->b1RMepCcmDefect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepRDIDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepRDIDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepRDIDefect (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 u4FsEcfmRMepIdentifier,
                           INT4 *pi4RetValFsEcfmRMepRDIDefect)
{
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmRMepRDIDefect =
        ECFM_CONVERT_TO_SNMP_BOOL (pRMepDbInfo->b1LastRdi);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepMacAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepMacAddress (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4FsEcfmRMepIdentifier,
                            tMacAddr * pRetValFsEcfmRMepMacAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set RMep Mac Address from corresponding RMep Entry */
    ECFM_MEMCPY (pRetValFsEcfmRMepMacAddress,
                 pRMepNode->RMepMacAddr, ECFM_MAC_ADDR_LENGTH);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepRdi
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepRdi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepRdi (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                     UINT4 u4FsEcfmMepIdentifier, UINT4 u4FsEcfmRMepIdentifier,
                     INT4 *pi4RetValFsEcfmRMepRdi)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Rdi bit status from corresponding RMep Entry */
    if (pRMepNode->b1LastRdi == ECFM_FALSE)
    {
        *pi4RetValFsEcfmRMepRdi = ECFM_SNMP_FALSE;
    }
    else
    {
        *pi4RetValFsEcfmRMepRdi = ECFM_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepPortStatusTlv
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepPortStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepPortStatusTlv (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 u4FsEcfmRMepIdentifier,
                               INT4 *pi4RetValFsEcfmRMepPortStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Port status Tlv from corresponding RMep Entry */
    *pi4RetValFsEcfmRMepPortStatusTlv = (INT4) (pRMepNode->u1LastPortStatus);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepInterfaceStatusTlv
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepInterfaceStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepInterfaceStatusTlv (UINT4 u4FsEcfmMdIndex,
                                    UINT4 u4FsEcfmMaIndex,
                                    UINT4 u4FsEcfmMepIdentifier,
                                    UINT4 u4FsEcfmRMepIdentifier,
                                    INT4 *pi4RetValFsEcfmRMepInterfaceStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Interface status Tlv from corresponding RMep Entry */
    *pi4RetValFsEcfmRMepInterfaceStatusTlv =
        (INT4) (pRMepNode->u1LastInterfaceStatus);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepChassisIdSubtype
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepChassisIdSubtype (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  INT4 *pi4RetValFsEcfmRMepChassisIdSubtype)
{
    return nmhGetDot1agCfmMepDbChassisIdSubtype (u4FsEcfmMdIndex,
                                                 u4FsEcfmMaIndex,
                                                 u4FsEcfmMepIdentifier,
                                                 u4FsEcfmRMepIdentifier,
                                                 pi4RetValFsEcfmRMepChassisIdSubtype);
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepDbChassisId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmMepDbChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepDbChassisId (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4FsEcfmRMepIdentifier,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsEcfmMepDbChassisId)
{
    return nmhGetDot1agCfmMepDbChassisId (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier,
                                          pRetValFsEcfmMepDbChassisId);

}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepManAddressDomain
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepManAddressDomain (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  tSNMP_OID_TYPE *
                                  pRetValFsEcfmRMepManAddressDomain)
{
    return nmhGetDot1agCfmMepDbManAddressDomain (u4FsEcfmMdIndex,
                                                 u4FsEcfmMaIndex,
                                                 u4FsEcfmMepIdentifier,
                                                 u4FsEcfmRMepIdentifier,
                                                 pRetValFsEcfmRMepManAddressDomain);
}

/****************************************************************************
 Function    :  nmhGetFsEcfmRMepManAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                retValFsEcfmRMepManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmRMepManAddress (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4FsEcfmRMepIdentifier,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsEcfmRMepManAddress)
{
    return nmhGetDot1agCfmMepDbManAddress (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                           u4FsEcfmMepIdentifier,
                                           u4FsEcfmRMepIdentifier,
                                           pRetValFsEcfmRMepManAddress);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepPortStatusDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepPortStatusDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepPortStatusDefect (UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  INT4 i4SetValFsEcfmRMepPortStatusDefect)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    UINT4               u4SeqNum = 0;

    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    pRMepDbInfo->b1RMepPortStatusDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmRMepPortStatusDefect);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepPortStatusDefect,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier,
                      i4SetValFsEcfmRMepPortStatusDefect));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepInterfaceStatusDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepInterfaceStatusDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepInterfaceStatusDefect (UINT4 u4FsEcfmMdIndex,
                                       UINT4 u4FsEcfmMaIndex,
                                       UINT4 u4FsEcfmMepIdentifier,
                                       UINT4 u4FsEcfmRMepIdentifier,
                                       INT4
                                       i4SetValFsEcfmRMepInterfaceStatusDefect)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    pRMepDbInfo->b1RMepInterfaceStatusDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmRMepInterfaceStatusDefect);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepInterfaceStatusDefect,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier,
                      i4SetValFsEcfmRMepInterfaceStatusDefect));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepCcmDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepCcmDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepCcmDefect (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 u4FsEcfmRMepIdentifier,
                           INT4 i4SetValFsEcfmRMepCcmDefect)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    UINT4               u4SeqNum = 0;

    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    pRMepDbInfo->b1RMepCcmDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmRMepCcmDefect);
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepCcmDefect, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, i4SetValFsEcfmRMepCcmDefect));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepRDIDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepRDIDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepRDIDefect (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 u4FsEcfmRMepIdentifier,
                           INT4 i4SetValFsEcfmRMepRDIDefect)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEcfmCcRMepDbInfo  *pRMepDbInfo = NULL;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    pRMepDbInfo = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                          u4FsEcfmMaIndex,
                                          u4FsEcfmMepIdentifier,
                                          u4FsEcfmRMepIdentifier);
    if (NULL == pRMepDbInfo)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    pRMepDbInfo->b1LastRdi =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmRMepRDIDefect);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepRDIDefect, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, i4SetValFsEcfmRMepRDIDefect));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepMacAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepMacAddress (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4FsEcfmRMepIdentifier,
                            tMacAddr SetValFsEcfmRMepMacAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set RMep Mac Address to corresponding RMep Entry */
    ECFM_MEMCPY (pRMepNode->RMepMacAddr, SetValFsEcfmRMepMacAddress,
                 ECFM_MAC_ADDR_LENGTH);
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepMacAddress, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %m",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, SetValFsEcfmRMepMacAddress));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepRdi
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepRdi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepRdi (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                     UINT4 u4FsEcfmMepIdentifier,
                     UINT4 u4FsEcfmRMepIdentifier, INT4 i4SetValFsEcfmRMepRdi)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    pRMepNode->b1LastRdi = (BOOL1) i4SetValFsEcfmRMepRdi;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepRdi, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, i4SetValFsEcfmRMepRdi));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepPortStatusTlv
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepPortStatusTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepPortStatusTlv (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 u4FsEcfmRMepIdentifier,
                               INT4 i4SetValFsEcfmRMepPortStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    pRMepNode->u1LastPortStatus = (UINT1) i4SetValFsEcfmRMepPortStatusTlv;
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepPortStatusTlv, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, i4SetValFsEcfmRMepPortStatusTlv));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepInterfaceStatusTlv
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepInterfaceStatusTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepInterfaceStatusTlv (UINT4 u4FsEcfmMdIndex,
                                    UINT4 u4FsEcfmMaIndex,
                                    UINT4 u4FsEcfmMepIdentifier,
                                    UINT4 u4FsEcfmRMepIdentifier,
                                    INT4 i4SetValFsEcfmRMepInterfaceStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    pRMepNode->u1LastInterfaceStatus = (UINT1)
        i4SetValFsEcfmRMepInterfaceStatusTlv;
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepInterfaceStatusTlv,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier,
                      i4SetValFsEcfmRMepInterfaceStatusTlv));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepChassisIdSubtype
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepChassisIdSubtype
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepChassisIdSubtype (UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  INT4 i4SetValFsEcfmRMepChassisIdSubtype)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    pSenderId = &(pRMepNode->LastSenderId);
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    pSenderId->u1ChassisIdSubType = (UINT1) i4SetValFsEcfmRMepChassisIdSubtype;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepChassisIdSubtype,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier,
                      i4SetValFsEcfmRMepChassisIdSubtype));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepDbChassisId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmMepDbChassisId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepDbChassisId (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4FsEcfmRMepIdentifier,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsEcfmMepDbChassisId)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set ChassisId from corresponding RMep Entry */
    pSenderId = &(pRMepNode->LastSenderId);
    /* Check if valid CCM recieved */
    if (pSetValFsEcfmMepDbChassisId->pu1_OctetList == NULL)
    {
        if (pSenderId->ChassisId.pu1Octets != NULL)
        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_CHASSIS_ID_POOL,
                                 pSenderId->ChassisId.pu1Octets);
            pSenderId->ChassisId.pu1Octets = NULL;
            pSenderId->ChassisId.u4OctLen = 0;
        }
    }
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepDbChassisId, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %s",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, pSetValFsEcfmMepDbChassisId));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepManAddressDomain
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepManAddressDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepManAddressDomain (UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  tSNMP_OID_TYPE *
                                  pSetValFsEcfmRMepManAddressDomain)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    /* Set Mgt Agddress domain to corresponding RMep Entry */
    pSenderId = &(pRMepNode->LastSenderId);
    if (pSetValFsEcfmRMepManAddressDomain->pu4_OidList == NULL)
    {
        if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)
        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL,
                                 pSenderId->MgmtAddressDomain.pu1Octets);
            pSenderId->MgmtAddressDomain.pu1Octets = NULL;
            pSenderId->MgmtAddressDomain.u4OctLen = 0;
        }
    }
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepManAddressDomain,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %o",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier,
                      pSetValFsEcfmRMepManAddressDomain));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmRMepManAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                setValFsEcfmRMepManAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmRMepManAddress (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4FsEcfmRMepIdentifier,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsEcfmRMepManAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    /* RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId exists */
    pSenderId = &(pRMepNode->LastSenderId);
    /* Check if valid CCM recieved */
    if (pSetValFsEcfmRMepManAddress->pu1_OctetList == NULL)
    {
        if (pSenderId->MgmtAddress.pu1Octets != NULL)
        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_POOL,
                                 pSenderId->MgmtAddress.pu1Octets);
            pSenderId->MgmtAddress.pu1Octets = NULL;
            pSenderId->MgmtAddress.u4OctLen = 0;
        }
    }

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmRMepManAddress, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FIVE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u %s",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4FsEcfmRMepIdentifier, pSetValFsEcfmRMepManAddress));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepPortStatusDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepPortStatusDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepPortStatusDefect (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                     UINT4 u4FsEcfmMaIndex,
                                     UINT4 u4FsEcfmMepIdentifier,
                                     UINT4 u4FsEcfmRMepIdentifier,
                                     INT4 i4TestValFsEcfmRMepPortStatusDefect)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4FsEcfmRMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepPortStatusDefect != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmRMepPortStatusDefect != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepInterfaceStatusDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepInterfaceStatusDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepInterfaceStatusDefect (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsEcfmMdIndex,
                                          UINT4 u4FsEcfmMaIndex,
                                          UINT4 u4FsEcfmMepIdentifier,
                                          UINT4 u4FsEcfmRMepIdentifier,
                                          INT4
                                          i4TestValFsEcfmRMepInterfaceStatusDefect)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4FsEcfmRMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepInterfaceStatusDefect != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmRMepInterfaceStatusDefect != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepCcmDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepCcmDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepCcmDefect (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                              UINT4 u4FsEcfmMaIndex,
                              UINT4 u4FsEcfmMepIdentifier,
                              UINT4 u4FsEcfmRMepIdentifier,
                              INT4 i4TestValFsEcfmRMepCcmDefect)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4FsEcfmRMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepCcmDefect != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmRMepCcmDefect != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepRDIDefect
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepRDIDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepRDIDefect (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                              UINT4 u4FsEcfmMaIndex,
                              UINT4 u4FsEcfmMepIdentifier,
                              UINT4 u4FsEcfmRMepIdentifier,
                              INT4 i4TestValFsEcfmRMepRDIDefect)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4FsEcfmRMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepRDIDefect != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmRMepRDIDefect != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepMacAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepMacAddress (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                               UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 u4FsEcfmRMepIdentifier,
                               tMacAddr TestValFsEcfmRMepMacAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4FsEcfmRMepIdentifier);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (TestValFsEcfmRMepMacAddress);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepRdi
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepRdi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepRdi (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                        UINT4 u4FsEcfmMaIndex, UINT4 u4FsEcfmMepIdentifier,
                        UINT4 u4FsEcfmRMepIdentifier,
                        INT4 i4TestValFsEcfmRMepRdi)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepRdi != ECFM_SNMP_TRUE) &&
        (i4TestValFsEcfmRMepRdi != ECFM_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepPortStatusTlv
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepPortStatusTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepPortStatusTlv (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4FsEcfmRMepIdentifier,
                                  INT4 i4TestValFsEcfmRMepPortStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepPortStatusTlv < ECFM_NO_PORT_STATUS_TLV) ||
        (i4TestValFsEcfmRMepPortStatusTlv > ECFM_PORT_IS_UP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP DB Entry\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepInterfaceStatusTlv
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepInterfaceStatusTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepInterfaceStatusTlv (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsEcfmMdIndex,
                                       UINT4 u4FsEcfmMaIndex,
                                       UINT4 u4FsEcfmMepIdentifier,
                                       UINT4 u4FsEcfmRMepIdentifier,
                                       INT4
                                       i4TestValFsEcfmRMepInterfaceStatusTlv)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepInterfaceStatusTlv <
         ECFM_IS_NO_INTERFACE_STATUS_TLV)
        || (i4TestValFsEcfmRMepInterfaceStatusTlv >
            ECFM_INTERFACE_IS_LOWER_LAYER_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP Entry\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepChassisIdSubtype
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepChassisIdSubtype
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepChassisIdSubtype (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                     UINT4 u4FsEcfmMaIndex,
                                     UINT4 u4FsEcfmMepIdentifier,
                                     UINT4 u4FsEcfmRMepIdentifier,
                                     INT4 i4TestValFsEcfmRMepChassisIdSubtype)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmRMepChassisIdSubtype <
         ECFM_CHASSIS_COMPONENT_CHASSIS_ID)
        || (i4TestValFsEcfmRMepChassisIdSubtype > ECFM_LOCAL_CHASSIS_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP Entry\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepDbChassisId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmMepDbChassisId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepDbChassisId (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                               UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 u4FsEcfmRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsEcfmMepDbChassisId)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsEcfmMepDbChassisId->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    if ((pTestValFsEcfmMepDbChassisId->i4_Length < 1) ||
        (pTestValFsEcfmMepDbChassisId->i4_Length > ECFM_CHASSIS_ID_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP Entry\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepManAddressDomain
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepManAddressDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepManAddressDomain (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                     UINT4 u4FsEcfmMaIndex,
                                     UINT4 u4FsEcfmMepIdentifier,
                                     UINT4 u4FsEcfmRMepIdentifier,
                                     tSNMP_OID_TYPE *
                                     pTestValFsEcfmRMepManAddressDomain)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pTestValFsEcfmRMepManAddressDomain);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmRMepManAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier

                The Object 
                testValFsEcfmRMepManAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmRMepManAddress (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                               UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 u4FsEcfmRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsEcfmRMepManAddress)
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsEcfmRMepManAddress->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    if ((pTestValFsEcfmRMepManAddress->i4_Length < 1) ||
        (pTestValFsEcfmRMepManAddress->i4_Length > ECFM_MAN_ADDRESS_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    /* Get RMep entry corresponding to  indices next to MdIndex,MaIndex,
     * MepId,RMepId */
    pRMepNode = EcfmSnmpLwGetRMepEntry (u4FsEcfmMdIndex,
                                        u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier,
                                        u4FsEcfmRMepIdentifier);
    if (pRMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Remote MEP Entry\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmRMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmRemoteMepDbExTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmLtmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmLtmTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmLtmSeqNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsEcfmLtmTable (UINT4 u4FsEcfmMdIndex,
                                        UINT4 u4FsEcfmMaIndex,
                                        UINT4 u4FsEcfmMepIdentifier,
                                        UINT4 u4FsEcfmLtmSeqNumber)
{
    tEcfmLbLtLtmReplyListInfo LtmReplyListInfo;
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNode = NULL;

    /* Get LtmReplyList entry based on LtrSeqNum */
    ECFM_MEMSET (&LtmReplyListInfo, ECFM_INIT_VAL,
                 ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);

    LtmReplyListInfo.u4MdIndex = u4FsEcfmMdIndex;
    LtmReplyListInfo.u4MaIndex = u4FsEcfmMaIndex;
    LtmReplyListInfo.u2MepId = u4FsEcfmMepIdentifier;
    LtmReplyListInfo.u4LtmSeqNum = u4FsEcfmLtmSeqNumber;
    pLtmReplyListNode = RBTreeGet (ECFM_LBLT_LTM_REPLY_LIST, &LtmReplyListInfo);

    /* Check if corresponding entry exists */
    if (pLtmReplyListNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No LTM Entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmLtmTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmLtmSeqNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsEcfmLtmTable (UINT4 *pu4FsEcfmMdIndex,
                                UINT4 *pu4FsEcfmMaIndex,
                                UINT4 *pu4FsEcfmMepIdentifier,
                                UINT4 *pu4FsEcfmLtmSeqNumber)
{
    return (nmhGetNextIndexFsEcfmLtmTable
            (0, pu4FsEcfmMdIndex, 0, pu4FsEcfmMaIndex, 0,
             pu4FsEcfmMepIdentifier, 0, pu4FsEcfmLtmSeqNumber));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmLtmTable
 Input       :  The Indices
                FsEcfmMdIndex
                nextFsEcfmMdIndex
                FsEcfmMaIndex
                nextFsEcfmMaIndex
                FsEcfmMepIdentifier
                nextFsEcfmMepIdentifier
                FsEcfmLtmSeqNumber
                nextFsEcfmLtmSeqNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmLtmTable (UINT4 u4FsEcfmMdIndex,
                               UINT4 *pu4NextFsEcfmMdIndex,
                               UINT4 u4FsEcfmMaIndex,
                               UINT4 *pu4NextFsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 *pu4NextFsEcfmMepIdentifier,
                               UINT4 u4FsEcfmLtmSeqNumber,
                               UINT4 *pu4NextFsEcfmLtmSeqNumber)
{
    tEcfmLbLtLtmReplyListInfo LtmReplyListInfo;
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNode = NULL;

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&LtmReplyListInfo, ECFM_INIT_VAL,
                 ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);
    LtmReplyListInfo.u4MdIndex = u4FsEcfmMdIndex;
    LtmReplyListInfo.u4MaIndex = u4FsEcfmMaIndex;
    LtmReplyListInfo.u2MepId = (UINT2) u4FsEcfmMepIdentifier;
    LtmReplyListInfo.u4LtmSeqNum = u4FsEcfmLtmSeqNumber;
    pLtmReplyListNode =
        RBTreeGetNext (ECFM_LBLT_LTM_REPLY_LIST, &LtmReplyListInfo, NULL);
    if (pLtmReplyListNode != NULL)
    {
        *pu4NextFsEcfmMdIndex = pLtmReplyListNode->u4MdIndex;
        *pu4NextFsEcfmMaIndex = pLtmReplyListNode->u4MaIndex;
        *pu4NextFsEcfmMepIdentifier = pLtmReplyListNode->u2MepId;
        *pu4NextFsEcfmLtmSeqNumber = pLtmReplyListNode->u4LtmSeqNum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmLtmTargetMacAddress
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmLtmSeqNumber

                The Object 
                retValFsEcfmLtmTargetMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmLtmTargetMacAddress (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                 UINT4 u4FsEcfmMepIdentifier,
                                 UINT4 u4FsEcfmLtmSeqNumber,
                                 tMacAddr * pRetValFsEcfmLtmTargetMacAddress)
{
    tEcfmLbLtLtmReplyListInfo LtmReplyListInfo;
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNode = NULL;

    /* Get LtmReplyList entry based on LtrSeqNum */
    ECFM_MEMSET (&LtmReplyListInfo, ECFM_INIT_VAL,
                 ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);

    LtmReplyListInfo.u4MdIndex = u4FsEcfmMdIndex;
    LtmReplyListInfo.u4MaIndex = u4FsEcfmMaIndex;
    LtmReplyListInfo.u2MepId = (UINT2) u4FsEcfmMepIdentifier;
    LtmReplyListInfo.u4LtmSeqNum = u4FsEcfmLtmSeqNumber;
    pLtmReplyListNode = RBTreeGet (ECFM_LBLT_LTM_REPLY_LIST, &LtmReplyListInfo);

    /* Check if corresponding entry exists */
    if (pLtmReplyListNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No LTM Entry\n");
        return SNMP_FAILURE;
    }
    /* Get Target Mac address */
    ECFM_MEMCPY (pRetValFsEcfmLtmTargetMacAddress,
                 pLtmReplyListNode->TargetMacAddr, ECFM_MAC_ADDR_LENGTH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmLtmTtl
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
                FsEcfmLtmSeqNumber

                The Object 
                retValFsEcfmLtmTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmLtmTtl (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                    UINT4 u4FsEcfmMepIdentifier, UINT4 u4FsEcfmLtmSeqNumber,
                    UINT4 *pu4RetValFsEcfmLtmTtl)
{
    tEcfmLbLtLtmReplyListInfo LtmReplyListInfo;
    tEcfmLbLtLtmReplyListInfo *pLtmReplyListNode = NULL;

    /* Get LtmReplyList entry based on LtrSeqNum */

    ECFM_MEMSET (&LtmReplyListInfo, ECFM_INIT_VAL,
                 ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE);

    LtmReplyListInfo.u4MdIndex = u4FsEcfmMdIndex;
    LtmReplyListInfo.u4MaIndex = u4FsEcfmMaIndex;
    LtmReplyListInfo.u2MepId = (UINT2) u4FsEcfmMepIdentifier;
    LtmReplyListInfo.u4LtmSeqNum = u4FsEcfmLtmSeqNumber;
    pLtmReplyListNode = RBTreeGet (ECFM_LBLT_LTM_REPLY_LIST, &LtmReplyListInfo);

    /* Check if corresponding entry exists */
    if (pLtmReplyListNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No LTM Entry\n");
        return SNMP_FAILURE;
    }

    /* Get TTL */
    *pu4RetValFsEcfmLtmTtl = (UINT4) pLtmReplyListNode->u2LtmTtl;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmMepExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmMepExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmMepExTable (UINT4 u4FsEcfmMdIndex,
                                          UINT4 u4FsEcfmMaIndex,
                                          UINT4 u4FsEcfmMepIdentifier)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MEP entry corresponding to indices MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmMepExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmMepExTable (UINT4 *pu4FsEcfmMdIndex,
                                  UINT4 *pu4FsEcfmMaIndex,
                                  UINT4 *pu4FsEcfmMepIdentifier)
{
    return (nmhGetNextIndexDot1agCfmMepTable (0, pu4FsEcfmMdIndex, 0,
                                              pu4FsEcfmMaIndex, 0,
                                              pu4FsEcfmMepIdentifier));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmMepExTable
 Input       :  The Indices
                FsEcfmMdIndex
                nextFsEcfmMdIndex
                FsEcfmMaIndex
                nextFsEcfmMaIndex
                FsEcfmMepIdentifier
                nextFsEcfmMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmMepExTable (UINT4 u4FsEcfmMdIndex,
                                 UINT4 *pu4NextFsEcfmMdIndex,
                                 UINT4 u4FsEcfmMaIndex,
                                 UINT4 *pu4NextFsEcfmMaIndex,
                                 UINT4 u4FsEcfmMepIdentifier,
                                 UINT4 *pu4NextFsEcfmMepIdentifier)
{
    tEcfmCcMepInfo     *pMepNextNode = NULL;

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    gpEcfmCcMepNode->u4MdIndex = u4FsEcfmMdIndex;
    gpEcfmCcMepNode->u4MaIndex = u4FsEcfmMaIndex;
    gpEcfmCcMepNode->u2MepId = (UINT2) u4FsEcfmMepIdentifier;

    /* Get MEP entry corresponding to indices next to MdIndex, MaIndex, MepId */
    pMepNextNode = (tEcfmCcMepInfo *) RBTreeGetNext
        (ECFM_CC_MEP_TABLE, (tRBElem *) (gpEcfmCcMepNode), NULL);

    if (pMepNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Next MEP Entry\n");
        return SNMP_FAILURE;
    }
    /* MEP entry corresponding to indices next to MdIndex, MaIndex, MepId exists */
    /* Set the next indices from corresponding MEP next node */
    *pu4NextFsEcfmMdIndex = pMepNextNode->u4MdIndex;
    *pu4NextFsEcfmMaIndex = pMepNextNode->u4MaIndex;
    *pu4NextFsEcfmMepIdentifier = (UINT4) (pMepNextNode->u2MepId);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmXconnRMepId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmXconnRMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmXconnRMepId (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                         UINT4 u4FsEcfmMepIdentifier,
                         UINT4 *pu4RetValFsEcfmXconnRMepId)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmXconnRMepId = pMepNode->CcInfo.u2XconnRMepId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmErrorRMepId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmErrorRMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmErrorRMepId (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                         UINT4 u4FsEcfmMepIdentifier,
                         UINT4 *pu4RetValFsEcfmErrorRMepId)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsEcfmErrorRMepId = pMepNode->CcInfo.u2ErrorRMepId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepDefectRDICcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepDefectRDICcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepDefectRDICcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                             UINT4 u4FsEcfmMepIdentifier,
                             INT4 *pi4RetValFsEcfmMepDefectRDICcm)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                             u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMepDefectRDICcm =
        ECFM_CONVERT_TO_SNMP_BOOL (pMepNode->FngInfo.b1SomeRdiDefect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepDefectMacStatus
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepDefectMacStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepDefectMacStatus (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                INT4 *pi4RetValFsEcfmMepDefectMacStatus)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                             u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMepDefectMacStatus =
        ECFM_CONVERT_TO_SNMP_BOOL (pMepNode->FngInfo.b1SomeMacStatusDefect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepDefectRemoteCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepDefectRemoteCcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepDefectRemoteCcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                INT4 *pi4RetValFsEcfmMepDefectRemoteCcm)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMepDefectRemoteCcm =
        ECFM_CONVERT_TO_SNMP_BOOL (pMepNode->FngInfo.b1SomeRMepCcmDefect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepDefectErrorCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepDefectErrorCcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepDefectErrorCcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               INT4 *pi4RetValFsEcfmMepDefectErrorCcm)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMepDefectErrorCcm =
        ECFM_CONVERT_TO_SNMP_BOOL (pMepNode->CcInfo.b1ErrorCcmDefect);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepDefectXconnCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepDefectXconnCcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepDefectXconnCcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               INT4 *pi4RetValFsEcfmMepDefectXconnCcm)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMepDefectXconnCcm =
        ECFM_CONVERT_TO_SNMP_BOOL (pMepNode->CcInfo.b1XconCcmDefect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepCcmOffload
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepCcmOffload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepCcmOffload (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           INT4 *pi4RetValFsEcfmMepCcmOffload)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                             u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    if (pMepNode->b1MepCcmOffloadStatus == ECFM_FALSE)
    {
        *pi4RetValFsEcfmMepCcmOffload = ECFM_CCM_OFFLOAD_DISABLE;
    }
    else
    {
        *pi4RetValFsEcfmMepCcmOffload = ECFM_CCM_OFFLOAD_ENABLE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepLbrIn
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepLbrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepLbrIn (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                      UINT4 u4FsEcfmMepIdentifier,
                      UINT4 *pu4RetValFsEcfmMepLbrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of valid in order Lbrs from the corresponding MEP 
       entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    *pu4RetValFsEcfmMepLbrIn = pLbInfo->u4LbrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepLbrInOutOfOrder
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepLbrInOutOfOrder
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepLbrInOutOfOrder (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                UINT4 *pu4RetValFsEcfmMepLbrInOutOfOrder)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of valid out-of- order LBRs from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    *pu4RetValFsEcfmMepLbrInOutOfOrder = pLbInfo->u4LbrInOutOfOrder;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepLbrBadMsdu
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepLbrBadMsdu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepLbrBadMsdu (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 *pu4RetValFsEcfmMepLbrBadMsdu)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the total no. of valid out-of- order Lbrs from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    *pu4RetValFsEcfmMepLbrBadMsdu = pLbInfo->u4LbrBadMsdu;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepUnexpLtrIn
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepUnexpLtrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepUnexpLtrIn (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 *pu4RetValFsEcfmMepUnexpLtrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the total no. of unexpected LTRs received from the
     * corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);
    *pu4RetValFsEcfmMepUnexpLtrIn = pLtInfo->u4UnexpLtrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepLbrOut
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepLbrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepLbrOut (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                       UINT4 u4FsEcfmMepIdentifier,
                       UINT4 *pu4RetValFsEcfmMepLbrOut)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of Lbrs transmitted, from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    *pu4RetValFsEcfmMepLbrOut = pLbInfo->u4LbrOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepCcmSequenceErrors
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepCcmSequenceErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepCcmSequenceErrors (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 *pu4RetValFsEcfmMepCcmSequenceErrors)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the Ccm Sequence errors from the corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    EcfmCcmOffLoadUpdateTxRxStats (pMepNode->pPortInfo->u2PortNum);
    *pu4RetValFsEcfmMepCcmSequenceErrors = pCcInfo->u4CcmSeqErrors;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcfmMepCciSentCcms
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                retValFsEcfmMepCciSentCcms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepCciSentCcms (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 *pu4RetValFsEcfmMepCciSentCcms)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Ccm Sequence errors from the corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    EcfmCcmOffLoadUpdateTxRxStats (pMepNode->pPortInfo->u2PortNum);
    *pu4RetValFsEcfmMepCciSentCcms = pCcInfo->u4CciCcmCount;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmXconnRMepId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmXconnRMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmXconnRMepId (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                         UINT4 u4FsEcfmMepIdentifier,
                         UINT4 u4SetValFsEcfmXconnRMepId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->CcInfo.u2XconnRMepId = (UINT2) u4SetValFsEcfmXconnRMepId;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmXconnRMepId, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4SetValFsEcfmXconnRMepId));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmErrorRMepId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmErrorRMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmErrorRMepId (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                         UINT4 u4FsEcfmMepIdentifier,
                         UINT4 u4SetValFsEcfmErrorRMepId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->CcInfo.u2ErrorRMepId = (UINT2) u4SetValFsEcfmErrorRMepId;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmErrorRMepId, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %u",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      u4SetValFsEcfmErrorRMepId));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepDefectRDICcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepDefectRDICcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepDefectRDICcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                             UINT4 u4FsEcfmMepIdentifier,
                             INT4 i4SetValFsEcfmMepDefectRDICcm)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->FngInfo.b1SomeRdiDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMepDefectRDICcm);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepDefectRDICcm, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      i4SetValFsEcfmMepDefectRDICcm));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepDefectMacStatus
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepDefectMacStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepDefectMacStatus (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                INT4 i4SetValFsEcfmMepDefectMacStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->FngInfo.b1SomeMacStatusDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMepDefectMacStatus);
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepDefectMacStatus, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      i4SetValFsEcfmMepDefectMacStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepDefectRemoteCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepDefectRemoteCcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepDefectRemoteCcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                INT4 i4SetValFsEcfmMepDefectRemoteCcm)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->FngInfo.b1SomeRMepCcmDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMepDefectRemoteCcm);
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepDefectRemoteCcm, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      i4SetValFsEcfmMepDefectRemoteCcm));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepDefectErrorCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepDefectErrorCcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepDefectErrorCcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               INT4 i4SetValFsEcfmMepDefectErrorCcm)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->CcInfo.b1ErrorCcmDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMepDefectErrorCcm);
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepDefectErrorCcm, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      i4SetValFsEcfmMepDefectErrorCcm));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepDefectXconnCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepDefectXconnCcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepDefectXconnCcm (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               INT4 i4SetValFsEcfmMepDefectXconnCcm)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MEP entry corresponding to indices MdIndex,
     * MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    pMepNode->CcInfo.b1XconCcmDefect =
        ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsEcfmMepDefectXconnCcm);

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepDefectXconnCcm, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                      ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                      u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                      i4SetValFsEcfmMepDefectXconnCcm));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepCcmOffload
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepCcmOffload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepCcmOffload (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           INT4 i4SetValFsEcfmMepCcmOffload)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
#endif
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4SeqNum = 0;
    UINT4               u4RetPrimaryVid = 0;
#ifdef NPAPI_WANTED
    UINT1               u1Type = ECFM_INIT_VAL;
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL, ECFM_HW_MEP_HANDLER_SIZE);
#endif
    ECFM_MEMSET (&SnmpNotifyInfo, ECFM_INIT_VAL, sizeof (tSnmpNotifyInfo));

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex,
                                             u4FsEcfmMaIndex,
                                             u4FsEcfmMepIdentifier);
    /* Check whether MEP entry exists or not */
    if (pMepNode == NULL)
    {
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    /* Offload status on a MEP is modified so delete the MEP
     * from the hardware first by checking it's h/w handler.
     * For this MEP Stop CCM Transmission and Stop receiving CCMs from 
     * remote MEPs is already infromed to hardware by setting it's 
     * Rowstatus to NIS.
     */
    if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
                     ECFM_HW_MEP_HANDLER_SIZE) != 0)
    {
        MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
        EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);

        EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepNode->u4IfIndex;
        if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_DELETE,
                                     &EcfmHwInfo) == FNP_FAILURE)
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "nmhSetFsEcfmMepCcmOffload :MEP deletion"
                         "indication to H/W failed. \r\n");
            return SNMP_FAILURE;
        }
        MEMSET (pMepNode->au1HwMepHandler, ECFM_INIT_VAL,
                ECFM_HW_MEP_HANDLER_SIZE);
    }
    if (RBTreeGet (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode) != NULL)
    {
        /* Remove the entry from global h/w MEP table */
        RBTreeRem (ECFM_CC_GLOBAL_HW_MEP_TABLE, pMepNode);
    }
#endif
    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */

    if (i4SetValFsEcfmMepCcmOffload == ECFM_CCM_OFFLOAD_DISABLE)
    {
        pMepNode->b1MepCcmOffloadStatus = ECFM_FALSE;
        if (pMepNode->pMaInfo != NULL)
        {
            if ((pMepNode->pMaInfo->u1CcmInterval == ECFM_CCM_INTERVAL_300Hz) ||
                (pMepNode->pMaInfo->u1CcmInterval == ECFM_CCM_INTERVAL_10_Ms))
            {
                pMepNode->pMaInfo->u1CcmInterval = ECFM_CCM_INTERVAL_1_S;
                u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
                RM_GET_SEQ_NUM (&u4SeqNum);

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaCcmInterval,
                                      u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                                      ECFM_TABLE_INDICES_COUNT_THREE,
                                      SNMP_SUCCESS);

                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  pMepNode->pMaInfo->u4MdIndex,
                                  pMepNode->pMaInfo->u4MaIndex,
                                  pMepNode->pMaInfo->u1CcmInterval));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
            }
        }
        else
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "nmhSetFsEcfmGlobalCcmOffload :Set default cc interval failed \r\n");
            return SNMP_FAILURE;
        }

    }
    else
    {
        pMepNode->b1MepCcmOffloadStatus = ECFM_TRUE;
    }

#ifdef NPAPI_WANTED
    /* Create MEP in the hardware with the modified
     * offload status.
     */
    MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));
    EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);

    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepNode->u4IfIndex;

    if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_CREATE,
                                 &EcfmHwInfo) == FNP_FAILURE)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "nmhSetFsEcfmMepCcmOffload :MEP Creation"
                     "indication to H/W failed. \r\n");
        return SNMP_FAILURE;
    }
    if (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE)
    {
        if (pMepNode->b1Active == ECFM_TRUE)
        {
            MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

            EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);
            EcfmHwInfo.EcfmHwEvents = ECFM_HW_ALL_EVENT_BITS;
            u1Type = FS_MI_ECFM_ENABLE_HW_EVENTS;

            if (EcfmFsMiEcfmHwCallNpApi (u1Type, &EcfmHwInfo) != FNP_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmMepUtlSetAgMepRowStatus :"
                             "Enable MEP events returned Failure. \r\n");
                return ECFM_FAILURE;
            }
        }
    }
#endif
    nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (ECFM_CC_CURR_CONTEXT_ID (),
                                                  u4FsEcfmMdIndex,
                                                  u4FsEcfmMaIndex,
                                                  &u4RetPrimaryVid);

    /* Sending Trigger to MSR */
    if (!(ECFM_IS_MEP_ISID_AWARE (u4RetPrimaryVid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        ECFM_MEMSET (&SnmpNotifyInfo, ECFM_INIT_VAL, sizeof (tSnmpNotifyInfo));
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepCcmOffload, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_FOUR, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %u %i",
                          ECFM_CC_CURR_CONTEXT_ID (), u4FsEcfmMdIndex,
                          u4FsEcfmMaIndex, u4FsEcfmMepIdentifier,
                          i4SetValFsEcfmMepCcmOffload));
    }
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepLbrIn
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepLbrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepLbrIn (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                      UINT4 u4FsEcfmMepIdentifier, UINT4 u4SetValFsEcfmMepLbrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of valid in order Lbrs from the corresponding MEP 
       entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    pLbInfo->u4LbrIn = u4SetValFsEcfmMepLbrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepLbrInOutOfOrder
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepLbrInOutOfOrder
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepLbrInOutOfOrder (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                UINT4 u4SetValFsEcfmMepLbrInOutOfOrder)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of valid out-of- order LBRs from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    pLbInfo->u4LbrInOutOfOrder = u4SetValFsEcfmMepLbrInOutOfOrder;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepLbrBadMsdu
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepLbrBadMsdu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepLbrBadMsdu (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 u4SetValFsEcfmMepLbrBadMsdu)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the total no. of valid out-of- order Lbrs from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    pLbInfo->u4LbrBadMsdu = u4SetValFsEcfmMepLbrBadMsdu;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepUnexpLtrIn
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepUnexpLtrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepUnexpLtrIn (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                           UINT4 u4FsEcfmMepIdentifier,
                           UINT4 u4SetValFsEcfmMepUnexpLtrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the total no. of unexpected LTRs received from the
     * corresponding MEP entry */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pMepNode);
    pLtInfo->u4UnexpLtrIn = u4SetValFsEcfmMepUnexpLtrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepLbrOut
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepLbrOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepLbrOut (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                       UINT4 u4FsEcfmMepIdentifier,
                       UINT4 u4SetValFsEcfmMepLbrOut)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Set the total no. of Lbrs transmitted, from the
     * corresponding MEP entry */
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepNode);
    pLbInfo->u4LbrOut = u4SetValFsEcfmMepLbrOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepCcmSequenceErrors
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepCcmSequenceErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepCcmSequenceErrors (UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  UINT4 u4SetValFsEcfmMepCcmSequenceErrors)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Ccm Sequence errors from the corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    pCcInfo->u4CcmSeqErrors = u4SetValFsEcfmMepCcmSequenceErrors;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcfmMepCciSentCcms
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                setValFsEcfmMepCciSentCcms
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepCciSentCcms (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                            UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4SetValFsEcfmMepCciSentCcms)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /*Set the Ccm Sequence errors from the corresponding MEP entry */
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);
    pCcInfo->u4CciCcmCount = u4SetValFsEcfmMepCciSentCcms;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmXconnRMepId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmXconnRMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmXconnRMepId (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                            UINT4 u4FsEcfmMaIndex, UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4TestValFsEcfmXconnRMepId)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4TestValFsEcfmXconnRMepId);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmErrorRMepId
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmErrorRMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmErrorRMepId (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                            UINT4 u4FsEcfmMaIndex, UINT4 u4FsEcfmMepIdentifier,
                            UINT4 u4TestValFsEcfmErrorRMepId)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    UNUSED_PARAM (u4TestValFsEcfmErrorRMepId);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepDefectRDICcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepDefectRDICcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepDefectRDICcm (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                UINT4 u4FsEcfmMaIndex,
                                UINT4 u4FsEcfmMepIdentifier,
                                INT4 i4TestValFsEcfmMepDefectRDICcm)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmMepDefectRDICcm != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmMepDefectRDICcm != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepDefectMacStatus
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepDefectMacStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepDefectMacStatus (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                   UINT4 u4FsEcfmMaIndex,
                                   UINT4 u4FsEcfmMepIdentifier,
                                   INT4 i4TestValFsEcfmMepDefectMacStatus)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmMepDefectMacStatus != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmMepDefectMacStatus != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepDefectRemoteCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepDefectRemoteCcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepDefectRemoteCcm (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                   UINT4 u4FsEcfmMaIndex,
                                   UINT4 u4FsEcfmMepIdentifier,
                                   INT4 i4TestValFsEcfmMepDefectRemoteCcm)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmMepDefectRemoteCcm != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmMepDefectRemoteCcm != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepDefectErrorCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepDefectErrorCcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepDefectErrorCcm (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  INT4 i4TestValFsEcfmMepDefectErrorCcm)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmMepDefectErrorCcm != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmMepDefectErrorCcm != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepDefectXconnCcm
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepDefectXconnCcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepDefectXconnCcm (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                  UINT4 u4FsEcfmMaIndex,
                                  UINT4 u4FsEcfmMepIdentifier,
                                  INT4 i4TestValFsEcfmMepDefectXconnCcm)
{
    UNUSED_PARAM (u4FsEcfmMdIndex);
    UNUSED_PARAM (u4FsEcfmMaIndex);
    UNUSED_PARAM (u4FsEcfmMepIdentifier);
    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmMepDefectXconnCcm != ECFM_SNMP_TRUE)
        && (i4TestValFsEcfmMepDefectXconnCcm != ECFM_SNMP_FALSE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepCcmOffload
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepCcmOffload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepCcmOffload (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                              UINT4 u4FsEcfmMaIndex,
                              UINT4 u4FsEcfmMepIdentifier,
                              INT4 i4TestValFsEcfmMepCcmOffload)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (ECFM_HW_CCMOFFLOAD_SUPPORT () == ECFM_FALSE)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: CCM offloading not supported in HW\n");
        return SNMP_FAILURE;
    }
#endif
    /* Validate CCiEnabled value, that is to be set */
    if ((i4TestValFsEcfmMepCcmOffload != ECFM_CCM_OFFLOAD_DISABLE) &&
        (i4TestValFsEcfmMepCcmOffload != ECFM_CCM_OFFLOAD_ENABLE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }

    /* MEP entry corresponding to MdIndex, MaIndex, MepId exists */
    /* Check CcmOffloading corresponding to MEP entry can be set or not */
    if (pMepNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: MEP entry already exists\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsEcfmMepCcmOffload == ECFM_CCM_OFFLOAD_ENABLE) &&
        (pMepNode->u1Direction == ECFM_MP_DIR_UP))
    {
#ifdef NPAPI_WANTED
        if (ECFM_HW_UPMEP_OFFLOAD_SUPPORT () == ECFM_TRUE)
        {
            return SNMP_SUCCESS;
        }
#endif
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Offload Enabled for Up Mep\n");
        return SNMP_FAILURE;
    }
    if ((pMepNode->pPortInfo != NULL)
        && (pMepNode->pPortInfo->u1IfType == CFA_LAGG))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Offload cannot be enabled for LAGG Ports\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepLbrIn
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepLbrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepLbrIn (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                         UINT4 u4FsEcfmMaIndex, UINT4 u4FsEcfmMepIdentifier,
                         UINT4 u4TestValFsEcfmMepLbrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepLbrIn != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No Mep Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepLbrInOutOfOrder
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepLbrInOutOfOrder
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepLbrInOutOfOrder (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                   UINT4 u4FsEcfmMaIndex,
                                   UINT4 u4FsEcfmMepIdentifier,
                                   UINT4 u4TestValFsEcfmMepLbrInOutOfOrder)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepLbrInOutOfOrder != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepLbrBadMsdu
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepLbrBadMsdu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepLbrBadMsdu (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                              UINT4 u4FsEcfmMaIndex,
                              UINT4 u4FsEcfmMepIdentifier,
                              UINT4 u4TestValFsEcfmMepLbrBadMsdu)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepLbrBadMsdu != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepUnexpLtrIn
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepUnexpLtrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepUnexpLtrIn (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                              UINT4 u4FsEcfmMaIndex,
                              UINT4 u4FsEcfmMepIdentifier,
                              UINT4 u4TestValFsEcfmMepUnexpLtrIn)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepUnexpLtrIn != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepLbrOut
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepLbrOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepLbrOut (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                          UINT4 u4FsEcfmMaIndex, UINT4 u4FsEcfmMepIdentifier,
                          UINT4 u4TestValFsEcfmMepLbrOut)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepLbrOut != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                        u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        ECFM_LBLT_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                       "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepCcmSequenceErrors
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepCcmSequenceErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepCcmSequenceErrors (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                     UINT4 u4FsEcfmMaIndex,
                                     UINT4 u4FsEcfmMepIdentifier,
                                     UINT4 u4TestValFsEcfmMepCcmSequenceErrors)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepCcmSequenceErrors != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepCciSentCcms
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier

                The Object 
                testValFsEcfmMepCciSentCcms
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepCciSentCcms (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                               UINT4 u4FsEcfmMaIndex,
                               UINT4 u4FsEcfmMepIdentifier,
                               UINT4 u4TestValFsEcfmMepCciSentCcms)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Validate CCiEnabled value, that is to be set */
    if (u4TestValFsEcfmMepCciSentCcms != ECFM_INIT_VAL)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }

    /* Get MEP entry corresponding to MdIndex, MaIndex, MepId */
    pMepNode =
        EcfmCcUtilGetMepEntryFrmGlob (u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                                      u4FsEcfmMepIdentifier);
    if (pMepNode == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MEP Entry\n");
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMepNode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMepExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
                FsEcfmMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMepExTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmMdExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmMdExTable
 Input       :  The Indices
                FsEcfmMdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmMdExTable (UINT4 u4FsEcfmMdIndex)
{
    tEcfmCcMdInfo      *pMdNode = NULL;
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4FsEcfmMdIndex);
    /*Check whether MD entry exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmMdExTable
 Input       :  The Indices
                FsEcfmMdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmMdExTable (UINT4 *pu4FsEcfmMdIndex)
{
    return (nmhGetNextIndexFsEcfmMdExTable (0, pu4FsEcfmMdIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmMdExTable
 Input       :  The Indices
                FsEcfmMdIndex
                nextFsEcfmMdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmMdExTable (UINT4 u4FsEcfmMdIndex,
                                UINT4 *pu4NextFsEcfmMdIndex)
{
    tEcfmCcMdInfo       MdInfo;
    tEcfmCcMdInfo      *pMdNextNode = NULL;

    /* First check System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MD entry next to index MdIndex */
    ECFM_MEMSET (&MdInfo, ECFM_INIT_VAL, ECFM_CC_MD_INFO_SIZE);
    MdInfo.u4MdIndex = u4FsEcfmMdIndex;

    pMdNextNode = (tEcfmCcMdInfo *) RBTreeGetNext
        (ECFM_CC_MD_TABLE, (tRBElem *) (&MdInfo), NULL);

    /* Check whether next MD entry exists or not */
    if (pMdNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Next MD Entry\n");
        return SNMP_FAILURE;
    }
    /* Next MD entry exists, return the next MD Index */
    *pu4NextFsEcfmMdIndex = pMdNextNode->u4MdIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmMepArchiveHoldTime
 Input       :  The Indices
                FsEcfmMdIndex

                The Object 
                retValFsEcfmMepArchiveHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMepArchiveHoldTime (UINT4 u4FsEcfmMdIndex,
                                INT4 *pi4RetValFsEcfmMepArchiveHoldTime)
{
    tEcfmCcMdInfo      *pMdNode = NULL;
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4FsEcfmMdIndex);
    /*Check whether MD entry exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM No MD Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMepArchiveHoldTime = pMdNode->u2MepArchiveHoldTime;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmMepArchiveHoldTime
 Input       :  The Indices
                FsEcfmMdIndex

                The Object 
                setValFsEcfmMepArchiveHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMepArchiveHoldTime (UINT4 u4FsEcfmMdIndex,
                                INT4 i4SetValFsEcfmMepArchiveHoldTime)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4FsEcfmMdIndex);
    /*Check whether MD entry exists or not */
    if (pMdNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry\n");
        return SNMP_FAILURE;
    }
    pMdNode->u2MepArchiveHoldTime = (UINT2) i4SetValFsEcfmMepArchiveHoldTime;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMepArchiveHoldTime, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4FsEcfmMdIndex, i4SetValFsEcfmMepArchiveHoldTime));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMepArchiveHoldTime
 Input       :  The Indices
                FsEcfmMdIndex

                The Object 
                testValFsEcfmMepArchiveHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMepArchiveHoldTime (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                   INT4 i4TestValFsEcfmMepArchiveHoldTime)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MD entry corresponding to MdIndex */
    pMdNode = EcfmSnmpLwGetMdEntry (u4FsEcfmMdIndex);

    /*Check whether MD entry exists or not */
    if (pMdNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MD Entry\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEcfmMepArchiveHoldTime < ECFM_MEP_ARCHIVE_DEF_HOLD_TIME)
        || (i4TestValFsEcfmMepArchiveHoldTime > ECFM_UINT2_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMdExTable
 Input       :  The Indices
                FsEcfmMdIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMdExTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEcfmMaExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEcfmMaExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEcfmMaExTable (UINT4 u4FsEcfmMdIndex,
                                         UINT4 u4FsEcfmMaIndex)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MA entry corresponding to indices MdIndex,
     * MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsEcfmMdIndex, u4FsEcfmMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEcfmMaExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEcfmMaExTable (UINT4 *pu4FsEcfmMdIndex,
                                 UINT4 *pu4FsEcfmMaIndex)
{
    return (nmhGetNextIndexFsEcfmMaExTable (0, pu4FsEcfmMdIndex, 0,
                                            pu4FsEcfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEcfmMaExTable
 Input       :  The Indices
                FsEcfmMdIndex
                nextFsEcfmMdIndex
                FsEcfmMaIndex
                nextFsEcfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEcfmMaExTable (UINT4 u4FsEcfmMdIndex,
                                UINT4 *pu4NextFsEcfmMdIndex,
                                UINT4 u4FsEcfmMaIndex,
                                UINT4 *pu4NextFsEcfmMaIndex)
{
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNextNode = NULL;

    /* First check System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get MA entry next to index MdIndex, MaIndex */
    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
    MaInfo.u4MdIndex = u4FsEcfmMdIndex;
    MaInfo.u4MaIndex = u4FsEcfmMaIndex;

    pMaNextNode = (tEcfmCcMaInfo *) RBTreeGetNext
        (ECFM_CC_MA_TABLE, (tRBElem *) (&MaInfo), NULL);

    /* Check whether next MA entry exists or not */
    if (pMaNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Next MA Entry\n");
        return SNMP_FAILURE;
    }
    /* Next MA entry exists, return the next MD and MA Index */
    *pu4NextFsEcfmMdIndex = pMaNextNode->u4MdIndex;
    *pu4NextFsEcfmMaIndex = pMaNextNode->u4MaIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmMaCrosscheckStatus
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex

                The Object 
                retValFsEcfmMaCrosscheckStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmMaCrosscheckStatus (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                INT4 *pi4RetValFsEcfmMaCrosscheckStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MA entry corresponding to indices MdIndex,
     * MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsEcfmMdIndex, u4FsEcfmMaIndex);

    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsEcfmMaCrosscheckStatus = (INT4) (pMaNode->u1CrossCheckStatus);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmMaCrosscheckStatus
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex

                The Object 
                setValFsEcfmMaCrosscheckStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmMaCrosscheckStatus (UINT4 u4FsEcfmMdIndex, UINT4 u4FsEcfmMaIndex,
                                INT4 i4SetValFsEcfmMaCrosscheckStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    UINT4               u4SeqNum = 0;

    /* Get MA entry corresponding to indices MdIndex,
     * MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsEcfmMdIndex, u4FsEcfmMaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry\n");
        return SNMP_FAILURE;
    }
    pMaNode->u1CrossCheckStatus = (UINT1) i4SetValFsEcfmMaCrosscheckStatus;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmMaCrosscheckStatus, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_THREE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4FsEcfmMdIndex, u4FsEcfmMaIndex,
                      i4SetValFsEcfmMaCrosscheckStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmMaCrosscheckStatus
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex

                The Object 
                testValFsEcfmMaCrosscheckStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmMaCrosscheckStatus (UINT4 *pu4ErrorCode, UINT4 u4FsEcfmMdIndex,
                                   UINT4 u4FsEcfmMaIndex,
                                   INT4 i4TestValFsEcfmMaCrosscheckStatus)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* First check System status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get MA entry corresponding to indices MdIndex,
     * MaIndex */
    pMaNode = EcfmSnmpLwGetMaEntry (u4FsEcfmMdIndex, u4FsEcfmMaIndex);
    if (pMaNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No MA Entry\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEcfmMaCrosscheckStatus != ECFM_ENABLE) &&
        (i4TestValFsEcfmMaCrosscheckStatus != ECFM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmMaExTable
 Input       :  The Indices
                FsEcfmMdIndex
                FsEcfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmMaExTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEcfmTrapControl
 Input       :  The Indices

                The Object 
                retValFsEcfmTrapControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmTrapControl (tSNMP_OCTET_STRING_TYPE * pRetValFsEcfmTrapControl)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        pRetValFsEcfmTrapControl->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    pRetValFsEcfmTrapControl->pu1_OctetList[0] = ECFM_CC_TRAP_CONTROL;
    pRetValFsEcfmTrapControl->i4_Length = 1;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEcfmTrapType
 Input       :  The Indices

                The Object 
                retValFsEcfmTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEcfmTrapType (INT4 *pi4RetValFsEcfmTrapType)
{
    UNUSED_PARAM (pi4RetValFsEcfmTrapType);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEcfmTrapControl
 Input       :  The Indices

                The Object 
                setValFsEcfmTrapControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEcfmTrapControl (tSNMP_OCTET_STRING_TYPE * pSetValFsEcfmTrapControl)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    ECFM_CC_TRAP_CONTROL = pSetValFsEcfmTrapControl->pu1_OctetList[0];

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmTrapControl, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %s", ECFM_CC_CURR_CONTEXT_ID (),
                      pSetValFsEcfmTrapControl));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEcfmTrapControl
 Input       :  The Indices

                The Object 
                testValFsEcfmTrapControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcfmTrapControl (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsEcfmTrapControl)
{
    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        CLI_SET_ERR (CLI_ECFM_NOT_STARTED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsEcfmTrapControl->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsEcfmTrapControl->pu1_OctetList[0] > ECFM_TRAP_CONTROL_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEcfmTrapControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEcfmTrapControl (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EcfmNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/
PUBLIC VOID
EcfmNotifyProtocolShutdownStatus (INT4 i4ContextId)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = 0;

    ECFM_MEMSET (&au1ObjectOid, 0, sizeof (SNMP_MAX_OID_LENGTH));
    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fscfmm, (sizeof (fscfmm) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    u2Objects = 2;
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, i4ContextId);

    SNMPGetOidString (fsmiy1, (sizeof (fsmiy1) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    /* Send a notification to MSR to process the vlan shutdown, with 
     * vlan oids and its system control object */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, i4ContextId);
    ECFM_MEMSET (&au1ObjectOid, 0, sizeof (SNMP_MAX_OID_LENGTH));
    SNMPGetOidString (fscfme, (sizeof (fscfme) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsMIEcfmSystemControl,
                      (sizeof (FsMIEcfmSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    /* Send a notification to MSR to process the vlan shutdown, with 
     * vlan oids and its system control object */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, i4ContextId);
#else
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}
