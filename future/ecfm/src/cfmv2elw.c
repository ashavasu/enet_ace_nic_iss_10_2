/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmv2elw.c,v 1.4 2012/01/20 13:11:25 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "fscfmmcli.h"
#include  "cfminc.h"

/* LOW LEVEL Routines for Table : Ieee8021CfmStackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmStackTable
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmStackTable (INT4 i4Ieee8021CfmStackifIndex,
                                               INT4
                                               i4Ieee8021CfmStackServiceSelectorType,
                                               UINT4
                                               u4Ieee8021CfmStackServiceSelectorOrNone,
                                               INT4 i4Ieee8021CfmStackMdLevel,
                                               INT4 i4Ieee8021CfmStackDirection)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    INT4                i4VlanIdIsidOrNone;

    i4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        i4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    /* Get Stack entry corresponding to indices - IfIndex, VlanId, MdLevel, 
     * Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4Ieee8021CfmStackServiceSelectorOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No entry exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices - IfIndex, VlanId, MdLevel, 
     * Direction exists */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmStackTable
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmStackTable (INT4 *pi4Ieee8021CfmStackifIndex,
                                       INT4
                                       *pi4Ieee8021CfmStackServiceSelectorType,
                                       UINT4
                                       *pu4Ieee8021CfmStackServiceSelectorOrNone,
                                       INT4 *pi4Ieee8021CfmStackMdLevel,
                                       INT4 *pi4Ieee8021CfmStackDirection)
{
    return (nmhGetNextIndexIeee8021CfmStackTable
            (0, pi4Ieee8021CfmStackifIndex, 0,
             pi4Ieee8021CfmStackServiceSelectorType, 0,
             pu4Ieee8021CfmStackServiceSelectorOrNone, 0,
             pi4Ieee8021CfmStackMdLevel, 0, pi4Ieee8021CfmStackDirection));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmStackTable
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                nextIeee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                nextIeee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                nextIeee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                nextIeee8021CfmStackMdLevel
                Ieee8021CfmStackDirection
                nextIeee8021CfmStackDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmStackTable (INT4 i4Ieee8021CfmStackifIndex,
                                      INT4 *pi4NextIeee8021CfmStackifIndex,
                                      INT4
                                      i4Ieee8021CfmStackServiceSelectorType,
                                      INT4
                                      *pi4NextIeee8021CfmStackServiceSelectorType,
                                      UINT4
                                      u4Ieee8021CfmStackServiceSelectorOrNone,
                                      UINT4
                                      *pu4NextIeee8021CfmStackServiceSelectorOrNone,
                                      INT4 i4Ieee8021CfmStackMdLevel,
                                      INT4 *pi4NextIeee8021CfmStackMdLevel,
                                      INT4 i4Ieee8021CfmStackDirection,
                                      INT4 *pi4NextIeee8021CfmStackDirection)
{

    tEcfmCcStackInfo    StackInfo;
    tEcfmCcStackInfo   *pStackNextNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get Stack entry corresponding to indices  next to IfIndex, VlanId, 
     * MdLevel and Direction */
    ECFM_MEMSET (&StackInfo, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);

    StackInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    StackInfo.u4IfIndex = (UINT2) i4Ieee8021CfmStackifIndex;
    StackInfo.u4VlanIdIsid = u4VlanIdIsidOrNone;
    StackInfo.u1MdLevel = (UINT1) i4Ieee8021CfmStackMdLevel;
    StackInfo.u1Direction = (UINT1) i4Ieee8021CfmStackDirection;

    pStackNextNode = (tEcfmCcStackInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) & StackInfo, NULL);

    if (pStackNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices next to IfIndex, VlanId, MdLevel, 
     * Direction exists */
    /* Set the next indices from corresponding Stack entry */
    *pi4NextIeee8021CfmStackifIndex = (INT4) (pStackNextNode->u2PortNum);
    if (((INT4) (pStackNextNode->u4VlanIdIsid)) >= ECFM_INTERNAL_ISID_MIN)
    {
        *pu4NextIeee8021CfmStackServiceSelectorOrNone =
            ECFM_ISID_INTERNAL_TO_ISID ((INT4) (pStackNextNode->u4VlanIdIsid));
        *pi4NextIeee8021CfmStackServiceSelectorType =
            ECFM_SERVICE_SELECTION_ISID;

    }
    else
    {
        *pu4NextIeee8021CfmStackServiceSelectorOrNone =
            (INT4) (pStackNextNode->u4VlanIdIsid);
        *pi4NextIeee8021CfmStackServiceSelectorType =
            ECFM_SERVICE_SELECTION_VLAN;
    }

    *pi4NextIeee8021CfmStackMdLevel = (INT4) (pStackNextNode->u1MdLevel);
    *pi4NextIeee8021CfmStackDirection = (INT4) (pStackNextNode->u1Direction);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMdIndex
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMdIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMdIndex (INT4 i4Ieee8021CfmStackifIndex,
                               INT4 i4Ieee8021CfmStackServiceSelectorType,
                               UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                               INT4 i4Ieee8021CfmStackMdLevel,
                               INT4 i4Ieee8021CfmStackDirection,
                               UINT4 *pu4RetValIeee8021CfmStackMdIndex)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */

    /* Set MdIndex from correponding stack entry */
    if (pStackNode->pMepInfo != NULL)
    {

        *pu4RetValIeee8021CfmStackMdIndex =
            pStackNode->pMepInfo->pMaInfo->pMdInfo->u4MdIndex;
    }
    else
    {
        *pu4RetValIeee8021CfmStackMdIndex = pStackNode->pMipInfo->u4MdIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMaIndex
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMaIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMaIndex (INT4 i4Ieee8021CfmStackifIndex,
                               INT4 i4Ieee8021CfmStackServiceSelectorType,
                               UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                               INT4 i4Ieee8021CfmStackMdLevel,
                               INT4 i4Ieee8021CfmStackDirection,
                               UINT4 *pu4RetValIeee8021CfmStackMaIndex)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    /* Set the MaIndex from corresponding Stack entry */
    if (pStackNode->pMepInfo != NULL)
    {
        *pu4RetValIeee8021CfmStackMaIndex =
            pStackNode->pMepInfo->pMaInfo->u4MaIndex;
    }
    else
    {
        *pu4RetValIeee8021CfmStackMaIndex = pStackNode->pMipInfo->u4MaIndex;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMepId
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMepId (INT4 i4Ieee8021CfmStackifIndex,
                             INT4 i4Ieee8021CfmStackServiceSelectorType,
                             UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                             INT4 i4Ieee8021CfmStackMdLevel,
                             INT4 i4Ieee8021CfmStackDirection,
                             UINT4 *pu4RetValIeee8021CfmStackMepId)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    /* Set the StackMepId from corresponding Stack entry */
    if (pStackNode->pMepInfo != NULL)
    {
        *pu4RetValIeee8021CfmStackMepId = pStackNode->pMepInfo->u2MepId;
    }
    else
    {
        *pu4RetValIeee8021CfmStackMepId = 0;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmStackMacAddress
 Input       :  The Indices
                Ieee8021CfmStackifIndex
                Ieee8021CfmStackServiceSelectorType
                Ieee8021CfmStackServiceSelectorOrNone
                Ieee8021CfmStackMdLevel
                Ieee8021CfmStackDirection

                The Object 
                retValIeee8021CfmStackMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmStackMacAddress (INT4 i4Ieee8021CfmStackifIndex,
                                  INT4 i4Ieee8021CfmStackServiceSelectorType,
                                  UINT4 u4Ieee8021CfmStackServiceSelectorOrNone,
                                  INT4 i4Ieee8021CfmStackMdLevel,
                                  INT4 i4Ieee8021CfmStackDirection,
                                  tMacAddr * pRetValIeee8021CfmStackMacAddress)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    UINT4               u4VlanIdIsidOrNone;

    u4VlanIdIsidOrNone = u4Ieee8021CfmStackServiceSelectorOrNone;

    /* convert the isid received into intrenal isid value */
    if (i4Ieee8021CfmStackServiceSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL
            (u4Ieee8021CfmStackServiceSelectorOrNone);
    }

    /* Get Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    pStackNode =
        EcfmSnmpLwGetStackEntry (i4Ieee8021CfmStackifIndex,
                                 u4VlanIdIsidOrNone,
                                 i4Ieee8021CfmStackMdLevel,
                                 i4Ieee8021CfmStackDirection);
    if (pStackNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry Exists for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Stack entry corresponding to indices IfIndex, VlanId, 
     * MdLevel and Direction exists */
    /* Set the MP MacAddress corresponding to MP's IfIndex */
    if (ECFM_CC_GET_PORT_INFO (pStackNode->u2PortNum) == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Port Information \n");
        return SNMP_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_CC_PORT_INFO
                               (pStackNode->u2PortNum)->u4IfIndex,
                               pRetValIeee8021CfmStackMacAddress);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CfmVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmVlanTable (UINT4
                                              u4Ieee8021CfmVlanComponentId,
                                              UINT4 u4Ieee8021CfmVlanSelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get Vlan entry corresponding to VlanSelector */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for the given Indices\n");
        return SNMP_FAILURE;
    }
    /* Vlan entry corresponding to VlanId exists */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmVlanTable (UINT4 *pu4Ieee8021CfmVlanComponentId,
                                      UINT4 *pu4Ieee8021CfmVlanSelector)
{
    return (nmhGetNextIndexIeee8021CfmVlanTable
            (0, pu4Ieee8021CfmVlanComponentId, 0, pu4Ieee8021CfmVlanSelector));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                nextIeee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
                nextIeee8021CfmVlanSelector
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmVlanTable (UINT4 u4Ieee8021CfmVlanComponentId,
                                     UINT4 *pu4NextIeee8021CfmVlanComponentId,
                                     UINT4 u4Ieee8021CfmVlanSelector,
                                     UINT4 *pu4NextIeee8021CfmVlanSelector)
{
    tEcfmCcVlanInfo     VlanInfo;
    tEcfmCcVlanInfo    *pVlanNextNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Get next VLAN entry corresponding to VlanId */
    ECFM_MEMSET (&VlanInfo, ECFM_INIT_VAL, ECFM_CC_VLAN_INFO_SIZE);
    VlanInfo.u4VidIsid = (UINT2) u4Ieee8021CfmVlanSelector;

    pVlanNextNode = (tEcfmCcVlanInfo *) RBTreeGetNext
        (ECFM_CC_VLAN_TABLE, (tRBElem *) & VlanInfo, NULL);

    if (pVlanNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Indices\n");
        return SNMP_FAILURE;
    }
    /* Next VLAN entry corresponding to VlanId exists */
    /* Set next index corresponding to VLAN entry */
    *pu4NextIeee8021CfmVlanSelector = (INT4) (pVlanNextNode->u4VidIsid);

    *pu4NextIeee8021CfmVlanComponentId = ECFM_CC_CURR_CONTEXT_ID ();
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmVlanPrimarySelector
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                retValIeee8021CfmVlanPrimarySelector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmVlanPrimarySelector (UINT4 u4Ieee8021CfmVlanComponentId,
                                      UINT4 u4Ieee8021CfmVlanSelector,
                                      UINT4
                                      *pu4RetValIeee8021CfmVlanPrimarySelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /*Get entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Indices\n");
        return SNMP_FAILURE;
    }
    /* VLAN entry corresponding to VlanId exists */
    /* Set PrimarySelector corresponding to VLAN entry */
    *pu4RetValIeee8021CfmVlanPrimarySelector =
        (INT4) (pVlanNode->u4PrimaryVidIsid);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmVlanRowStatus
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                retValIeee8021CfmVlanRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmVlanRowStatus (UINT4 u4Ieee8021CfmVlanComponentId,
                                UINT4 u4Ieee8021CfmVlanSelector,
                                INT4 *pi4RetValIeee8021CfmVlanRowStatus)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /*Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for Given Indices\n");
        return SNMP_FAILURE;
    }
    /* VLAN entry corresponding to VlanId exists */
    /* Set Row status corresponding to VLAN entry */
    *pi4RetValIeee8021CfmVlanRowStatus = (INT4) (pVlanNode->u1RowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CfmVlanPrimarySelector
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                setValIeee8021CfmVlanPrimarySelector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmVlanPrimarySelector (UINT4 u4Ieee8021CfmVlanComponentId,
                                      UINT4 u4Ieee8021CfmVlanSelector,
                                      UINT4
                                      u4SetValIeee8021CfmVlanPrimarySelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Indices\n");
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_NOT_READY)
    {
        pVlanNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    }
    /* VLAN entry corresponding to VlanId exists */
    /* Set PrimaryVid corresponding to VLAN entry */
    pVlanNode->u4PrimaryVidIsid =
        (UINT2) u4SetValIeee8021CfmVlanPrimarySelector;

    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    SnmpNotifyInfo.pu4ObjectId = FsMIEcfmVlanPrimaryVid;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIEcfmVlanPrimaryVid) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = EcfmCcLock;
    SnmpNotifyInfo.pUnLockPointer = EcfmCcUnLock;
    SnmpNotifyInfo.u4Indices = ECFM_TABLE_INDICES_COUNT_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Ieee8021CfmVlanSelector,
                      u4SetValIeee8021CfmVlanPrimarySelector));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmVlanRowStatus
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                setValIeee8021CfmVlanRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmVlanRowStatus (UINT4 u4Ieee8021CfmVlanComponentId,
                                UINT4 u4Ieee8021CfmVlanSelector,
                                INT4 i4SetValIeee8021CfmVlanRowStatus)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcVlanInfo    *pVlanNewNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode != NULL)
    {
        /* VLAN entry corresponding to VlanId exists and its row status is same 
         * as user wants to set */
        if (pVlanNode->u1RowStatus == (UINT1) i4SetValIeee8021CfmVlanRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValIeee8021CfmVlanRowStatus !=
             ECFM_ROW_STATUS_CREATE_AND_WAIT)
    {
        return SNMP_FAILURE;
    }

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIEcfmVlanRowStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIEcfmVlanRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = EcfmCcLock;
    SnmpNotifyInfo.pUnLockPointer = EcfmCcUnLock;
    SnmpNotifyInfo.u4Indices = ECFM_TABLE_INDICES_COUNT_TWO;

    switch (i4SetValIeee8021CfmVlanRowStatus)
    {
        case ECFM_ROW_STATUS_NOT_IN_SERVICE:

            RBTreeRem (ECFM_CC_PRIMARY_VLAN_TABLE, pVlanNode);
            pVlanNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNode);
            break;

        case ECFM_ROW_STATUS_ACTIVE:
            /* Check whether VLAN entry's row status can be changed to ACTIVE */
            if (pVlanNode->u4PrimaryVidIsid == 0)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Vlan Row status set to Active Failed\n");
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Ieee8021CfmVlanSelector,
                                  i4SetValIeee8021CfmVlanRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* Update NoOfVlanIds for MAs associated with VLAN entry's 
             * PrimarySelector*/
            pMaNode = (tEcfmCcMaInfo *) RBTreeGetFirst (ECFM_CC_MA_TABLE);
            while (pMaNode != NULL)
            {
                /* Get MA entry associated with PrimarySelector */
                if ((pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE) &&
                    (pMaNode->u4PrimaryVidIsid == pVlanNode->u4PrimaryVidIsid))
                {
                    pMaNode->u2NumberOfVids =
                        (pMaNode->u2NumberOfVids) + (UINT2) ECFM_INCR_VAL;

                    /* Evaluate MIP creation for this particular vlanId */
                    EcfmCcUtilEvaluateAndCreateMip (-1,
                                                    pMaNode->u4PrimaryVidIsid,
                                                    ECFM_TRUE);
                }
                pMaNode = (tEcfmCcMaInfo *) RBTreeGetNext
                    (ECFM_CC_MA_TABLE, (tRBElem *) pMaNode, NULL);
            }
            /* Add new VLAN node in primary VLAN table */
            if (RBTreeAdd (ECFM_CC_PRIMARY_VLAN_TABLE, pVlanNode) ==
                ECFM_RB_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Additon to primay VLAN Failed\n");
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            /* Set row status ACTIVE */
            pVlanNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNode);
            break;

        case ECFM_ROW_STATUS_CREATE_AND_WAIT:
            /* User wants to create new entry with VlanId */
            /* Create node for new VLAN entry in VLAN Table */
            if (ECFM_ALLOC_MEM_BLOCK_CC_VLAN_TABLE (pVlanNewNode) == NULL)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                             ECFM_OS_RESOURCE_TRC,
                             "nmhSetIeee8021CfmVlanRowStatus: Row Status Create and Go"
                             "Allocation for VLAN Node Failed \r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Ieee8021CfmVlanSelector,
                                  i4SetValIeee8021CfmVlanRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }
            ECFM_MEMSET (pVlanNewNode, ECFM_INIT_VAL, ECFM_CC_VLAN_INFO_SIZE);

            /* Put index in new VLAN node */
            pVlanNewNode->u4VidIsid = (UINT2) u4Ieee8021CfmVlanSelector;
            /* Put other default values */
            pVlanNewNode->u4PrimaryVidIsid = ECFM_INIT_VAL;
            /* Set VLAN entry's rowstatus */
            pVlanNewNode->u1RowStatus = ECFM_ROW_STATUS_NOT_READY;

            /* Add new VLAN node in VlanTable in global info */
            if (RBTreeAdd (ECFM_CC_VLAN_TABLE, pVlanNewNode) == ECFM_RB_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "\tSNMP: Allocation to Vlan Failed\n");
                /*Addition of node in VlanTable failed */
                ECFM_FREE_MEM_BLOCK (ECFM_CC_VLAN_TABLE_POOL,
                                     (UINT1 *) (pVlanNewNode));
                pVlanNode = NULL;
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                                  ECFM_CC_CURR_CONTEXT_ID (),
                                  u4Ieee8021CfmVlanSelector,
                                  i4SetValIeee8021CfmVlanRowStatus));
                ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
                return SNMP_FAILURE;
            }

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtAddVlanEntry (u4CurrContextId, pVlanNewNode);
            break;

        case ECFM_ROW_STATUS_DESTROY:
            /* Update NoOfVlanIds for all MAs with VLAN's PrimarySelector */
            if (pVlanNode->u4PrimaryVidIsid != 0)
            {
                pMaNode = (tEcfmCcMaInfo *) RBTreeGetFirst (ECFM_CC_MA_TABLE);
                while (pMaNode != NULL)
                {
                    /* Get MA entry associated with PrimarySelector */
                    if ((pMaNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE) &&
                        (pMaNode->u4PrimaryVidIsid ==
                         pVlanNode->u4PrimaryVidIsid))
                    {
                        pMaNode->u2NumberOfVids =
                            (pMaNode->u2NumberOfVids) - (UINT2) ECFM_DECR_VAL;
                    }
                    pMaNode = (tEcfmCcMaInfo *) RBTreeGetNext
                        (ECFM_CC_MA_TABLE, (tRBElem *) pMaNode, NULL);
                }
                /* Evaluate MIP creation for this particular
                 * vlanId */
                EcfmCcUtilEvaluateAndCreateMip (-1,
                                                pVlanNode->u4PrimaryVidIsid,
                                                ECFM_TRUE);
            }
            /* Remove VLAN node from VlanTable in Global info */
            RBTreeRem (ECFM_CC_VLAN_TABLE, (tRBElem *) pVlanNode);
            RBTreeRem (ECFM_CC_PRIMARY_VLAN_TABLE, (tRBElem *) pVlanNode);

            /* Synchronize the status in the LBLT Task */
            EcfmLbLtRemoveVlanEntry (u4CurrContextId, pVlanNode);

            /* Release the memory assigned to VLAN node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_VLAN_TABLE_POOL,
                                 (UINT1 *) (pVlanNode));
            pVlanNode = NULL;
            break;
        default:
            break;
    }

    /* Sending Trigger to MSR */
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", ECFM_CC_CURR_CONTEXT_ID (),
                      u4Ieee8021CfmVlanSelector,
                      i4SetValIeee8021CfmVlanRowStatus));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmVlanPrimarySelector
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                testValIeee8021CfmVlanPrimarySelector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmVlanPrimarySelector (UINT4 *pu4ErrorCode,
                                         UINT4 u4Ieee8021CfmVlanComponentId,
                                         UINT4 u4Ieee8021CfmVlanSelector,
                                         UINT4
                                         u4TestValIeee8021CfmVlanPrimarySelector)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNextNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }

    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);

    MaInfo.u4MdIndex = 0;
    MaInfo.u4MaIndex = 0;
    pMaNextNode = (tEcfmCcMaInfo *)
        RBTreeGetNext (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo, NULL);

    while (pMaNextNode != NULL)
    {
        if (pMaNextNode->u4PrimaryVidIsid == (UINT4) u4Ieee8021CfmVlanSelector)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:VID is primary VLAN for some other. Can not"
                         " be configured as secondary VLAN \n");
            return SNMP_FAILURE;
        }

        MaInfo.u4MdIndex = pMaNextNode->u4MdIndex;
        MaInfo.u4MaIndex = pMaNextNode->u4MaIndex;

        pMaNextNode = (tEcfmCcMaInfo *) RBTreeGetNext
            (ECFM_CC_MA_TABLE, (tRBElem *) & MaInfo, NULL);
    }

    /* Validating the value for PrimarySelector */
    if ((u4TestValIeee8021CfmVlanPrimarySelector < ECFM_VLANID_MIN) ||
        (u4TestValIeee8021CfmVlanPrimarySelector > ECFM_VLANID_MAX) ||
        (u4TestValIeee8021CfmVlanPrimarySelector == u4Ieee8021CfmVlanSelector))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: PrimarySelector validation failed\n");
        return SNMP_FAILURE;
    }

    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:No Entry for given Index\n");
        return SNMP_FAILURE;
    }

    /* VLAN entry corresponding to VlanId exists */
    /* Check whether PrimarySelector can be set */
    if (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Primary VID cannot be set as row"
                     " status is already ECFM_ROW_STATIS_ACTIVE\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmVlanRowStatus
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector

                The Object 
                testValIeee8021CfmVlanRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmVlanRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021CfmVlanComponentId,
                                   UINT4 u4Ieee8021CfmVlanSelector,
                                   INT4 i4TestValIeee8021CfmVlanRowStatus)
{
    tEcfmCcVlanInfo    *pVlanNode = NULL;

    UNUSED_PARAM (u4Ieee8021CfmVlanComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate row status value */
    if ((i4TestValIeee8021CfmVlanRowStatus < ECFM_ROW_STATUS_ACTIVE) ||
        (i4TestValIeee8021CfmVlanRowStatus > ECFM_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Invalid Row Status Value\n");
        return SNMP_FAILURE;
    }

    /*  validate index range */
    if ((u4Ieee8021CfmVlanSelector < ECFM_VLANID_MIN) ||
        (u4Ieee8021CfmVlanSelector > ECFM_VLANID_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:VlanSelector validation failed\n");
        return SNMP_FAILURE;
    }
    /* Get VLAN entry corresponding to VlanId */
    pVlanNode = EcfmSnmpLwGetVlanEntry (u4Ieee8021CfmVlanSelector);
    if (pVlanNode == NULL)
    {
        /* VLAN entry corresponding to VlanId does not exists and user wants 
         * to change its row status */
        if (i4TestValIeee8021CfmVlanRowStatus !=
            ECFM_ROW_STATUS_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Invalid Value\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    /* VLAN entry corresponding to VlanId exists and its row status is 
     *  same as user wants to set */
    if (pVlanNode->u1RowStatus == (UINT1) i4TestValIeee8021CfmVlanRowStatus)
    {
        return SNMP_SUCCESS;
    }
    /* VLAN entry corresponding to VlanId exists and user wants 
     *  to create new VLAN entry with same VlanId */
    if ((i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_CREATE_AND_WAIT)
        || (i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP:Vlan entry already exists\n");
        return SNMP_FAILURE;
    }
    /* If user wants to make the row status, corresponding to VLAN entry to 
     * ACTIVE */
    if ((i4TestValIeee8021CfmVlanRowStatus == ECFM_ROW_STATUS_ACTIVE) &&
        (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_NOT_IN_SERVICE))
    {
        /* check if PrimarySelector has been set or not */
        if (pVlanNode->u4PrimaryVidIsid == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP:Invalid Row Status Value\n");
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CfmVlanTable
 Input       :  The Indices
                Ieee8021CfmVlanComponentId
                Ieee8021CfmVlanSelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CfmVlanTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CfmDefaultMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmDefaultMdTable (UINT4
                                                   u4Ieee8021CfmDefaultMdComponentId,
                                                   INT4
                                                   i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                                   UINT4
                                                   u4Ieee8021CfmDefaultMdPrimarySelector)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {

        /* first validate index range */
        if ((u4Ieee8021CfmDefaultMdPrimarySelector < ECFM_ISID_MIN) ||
            (u4Ieee8021CfmDefaultMdPrimarySelector > ECFM_ISID_MAX))
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Isid Index range for PrimarySelector Default Domain Table\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* first validate index range */
        if ((u4Ieee8021CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (u4Ieee8021CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid VlanId Index range for PrimarySelector Default Domain Table\n");
            return SNMP_FAILURE;
        }

    }

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);

    /* Check if DefaultMd entry exists */
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmDefaultMdTable (UINT4
                                           *pu4Ieee8021CfmDefaultMdComponentId,
                                           INT4
                                           *pi4Ieee8021CfmDefaultMdPrimarySelectorType,
                                           UINT4
                                           *pu4Ieee8021CfmDefaultMdPrimarySelector)
{
    tEcfmCcDefaultMdTableInfo *pDefMdInfo = NULL;

    *pu4Ieee8021CfmDefaultMdComponentId = ECFM_CC_CURR_CONTEXT_ID ();

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    pDefMdInfo = (tEcfmCcDefaultMdTableInfo *) RBTreeGetFirst
        (ECFM_CC_DEF_MD_TABLE);
    if (pDefMdInfo != NULL)
    {
        if (pDefMdInfo->u4PrimaryVidIsid < ECFM_INTERNAL_ISID_MIN)
        {
            *pu4Ieee8021CfmDefaultMdPrimarySelector =
                pDefMdInfo->u4PrimaryVidIsid;
            *pi4Ieee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_VLAN;
        }
        else
        {
            *pu4Ieee8021CfmDefaultMdPrimarySelector =
                ECFM_ISID_INTERNAL_TO_ISID (pDefMdInfo->u4PrimaryVidIsid);
            *pi4Ieee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_ISID;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                nextIeee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                nextIeee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
                nextIeee8021CfmDefaultMdPrimarySelector
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmDefaultMdTable (UINT4
                                          u4Ieee8021CfmDefaultMdComponentId,
                                          UINT4
                                          *pu4NextIeee8021CfmDefaultMdComponentId,
                                          INT4
                                          i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                          INT4
                                          *pi4NextIeee8021CfmDefaultMdPrimarySelectorType,
                                          UINT4
                                          u4Ieee8021CfmDefaultMdPrimarySelector,
                                          UINT4
                                          *pu4NextIeee8021CfmDefaultMdPrimarySelector)
{
    tEcfmCcDefaultMdTableInfo DefMdInfo;
    tEcfmCcDefaultMdTableInfo *pDefMdInfo = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    *pu4NextIeee8021CfmDefaultMdComponentId = ECFM_CC_CURR_CONTEXT_ID ();

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Check if it is the last entry of DefaultMdTable */
    if (i4CfmDefaultMdPrimarySelector >= ECFM_INTERNAL_ISID_MAX)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Next Default Domain Entry\n");
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&DefMdInfo, ECFM_INIT_VAL, ECFM_CC_DEF_MD_INFO_SIZE);

    DefMdInfo.u4PrimaryVidIsid = i4CfmDefaultMdPrimarySelector;

    pDefMdInfo = (tEcfmCcDefaultMdTableInfo *) RBTreeGetNext
        (ECFM_CC_DEF_MD_TABLE, (tRBElem *) (&DefMdInfo), NULL);
    /* Set the next index to PrimaryVid from DefaultMd Table */
    if (pDefMdInfo != NULL)
    {
        if (pDefMdInfo->u4PrimaryVidIsid < ECFM_INTERNAL_ISID_MIN)
        {
            *pu4NextIeee8021CfmDefaultMdPrimarySelector =
                pDefMdInfo->u4PrimaryVidIsid;
            *pi4NextIeee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_VLAN;
        }
        else
        {
            *pu4NextIeee8021CfmDefaultMdPrimarySelector =
                ECFM_ISID_INTERNAL_TO_ISID (pDefMdInfo->u4PrimaryVidIsid);
            *pi4NextIeee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_ISID;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdStatus
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdStatus (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                  INT4
                                  i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                  UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                  INT4 *pi4RetValIeee8021CfmDefaultMdStatus)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }

    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the DefaultMdStatus from corresponding DefaultMd entry */
    *pi4RetValIeee8021CfmDefaultMdStatus =
        ECFM_CONVERT_TO_SNMP_BOOL (pDefaultMdNode->b1Status);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdLevel
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdLevel (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                 INT4 i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                 UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                 INT4 *pi4RetValIeee8021CfmDefaultMdLevel)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the MdLevel from corresponding DefaultMd entry */
    *pi4RetValIeee8021CfmDefaultMdLevel = (INT4) (pDefaultMdNode->i1MdLevel);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdMhfCreation
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdMhfCreation (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                       INT4
                                       i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                       UINT4
                                       u4Ieee8021CfmDefaultMdPrimarySelector,
                                       INT4
                                       *pi4RetValIeee8021CfmDefaultMdMhfCreation)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the MHfCreation criteria from corresponding DefaultMd entry */
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021CfmDefaultMdMhfCreation =
        (INT4) (pDefaultMdNode->u1MhfCreation);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdIdPermission
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdIdPermission (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                        INT4
                                        i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                        UINT4
                                        u4Ieee8021CfmDefaultMdPrimarySelector,
                                        INT4
                                        *pi4RetValIeee8021CfmDefaultMdIdPermission)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the SenderIdPermission from corresponding DefaultMd entry */
    *pi4RetValIeee8021CfmDefaultMdIdPermission =
        (INT4) (pDefaultMdNode->u1IdPermission);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CfmDefaultMdLevel
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                setValIeee8021CfmDefaultMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmDefaultMdLevel (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                 INT4 i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                 UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                 INT4 i4SetValIeee8021CfmDefaultMdLevel)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        /* No Default entry is found */
        /* case 1: user wants to set the default value, don't add the node */
        /* case 2: user wants to set something other than default value, 
         *         add a default MD node*/
        if (i4SetValIeee8021CfmDefaultMdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if ((pDefaultMdNode =
                 EcfmSnmpLwSetDefaultMdNode (i4CfmDefaultMdPrimarySelector)) ==
                NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Check if it is already same */
    if (pDefaultMdNode->i1MdLevel == (INT1) i4SetValIeee8021CfmDefaultMdLevel)

    {
        return SNMP_SUCCESS;
    }
    /* Set the MdLevel to corresponding DefaultMd entry */
    pDefaultMdNode->i1MdLevel = (INT1) i4SetValIeee8021CfmDefaultMdLevel;

    /* Get if there is any MA with vlanId u2Vid and level  u1Level */
    pMaNode =
        EcfmCcUtilGetMaAssocWithVid (i4CfmDefaultMdPrimarySelector,
                                     i4SetValIeee8021CfmDefaultMdLevel);
    if (pMaNode != NULL)

    {

        /* If UP MEP associated with this MA exists then only update 
         * default MdStatus */
        if (EcfmIsMepAssocWithMa
            (pMaNode->u4MdIndex, pMaNode->u4MaIndex, -1,
             ECFM_MP_DIR_UP) == ECFM_TRUE)

        {
            EcfmSnmpLwUpdateDefaultMdStatus (i4SetValIeee8021CfmDefaultMdLevel,
                                             i4CfmDefaultMdPrimarySelector,
                                             ECFM_FALSE);
        }
    }

    else

    {
        EcfmSnmpLwUpdateDefaultMdStatus (i4SetValIeee8021CfmDefaultMdLevel,
                                         i4CfmDefaultMdPrimarySelector,
                                         ECFM_TRUE);
    }

    /* Evaluate and create if MIPs can be created for all the ports */
    EcfmCcUtilEvaluateAndCreateMip (-1, i4CfmDefaultMdPrimarySelector,
                                    ECFM_TRUE);
    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.pu4ObjectId = FsMIEcfmDefaultMdLevel;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIEcfmDefaultMdLevel) / sizeof (UINT4);
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = EcfmCcLock;
        SnmpNotifyInfo.pUnLockPointer = EcfmCcUnLock;
        SnmpNotifyInfo.u4Indices = ECFM_TABLE_INDICES_COUNT_TWO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (),
                          i4CfmDefaultMdPrimarySelector,
                          i4SetValIeee8021CfmDefaultMdLevel));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* Check if all the values of the Node are default, if so then delete the
     * Node */
    EcfmSnmpLwProcessDefaultMdNode (i4CfmDefaultMdPrimarySelector);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmDefaultMdMhfCreation
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                setValIeee8021CfmDefaultMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmDefaultMdMhfCreation (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                       INT4
                                       i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                       UINT4
                                       u4Ieee8021CfmDefaultMdPrimarySelector,
                                       INT4
                                       i4SetValIeee8021CfmDefaultMdMhfCreation)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        /* No Default entry is found */
        /* case 1: user wants to set the default value, don't add the node */
        /* case 2: user wants to set something other than default value, 
         *         add a default MD node*/
        if (i4SetValIeee8021CfmDefaultMdMhfCreation == ECFM_MHF_CRITERIA_DEFER)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if ((pDefaultMdNode =
                 EcfmSnmpLwSetDefaultMdNode (i4CfmDefaultMdPrimarySelector)) ==
                NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Check if it is already same */
    if (pDefaultMdNode->u1MhfCreation ==
        (UINT1) i4SetValIeee8021CfmDefaultMdMhfCreation)
    {
        return SNMP_SUCCESS;
    }
    /* Set the Mhf Creation to corresponding DefaultMd entry */
    pDefaultMdNode->u1MhfCreation =
        (UINT1) i4SetValIeee8021CfmDefaultMdMhfCreation;

    /* Evaluate and create if Mips can be created for all the ports */
    EcfmCcUtilEvaluateAndCreateMip (-1, i4CfmDefaultMdPrimarySelector,
                                    ECFM_TRUE);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pDefaultMdNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.pu4ObjectId = FsMIEcfmDefaultMdMhfCreation;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIEcfmDefaultMdMhfCreation) / sizeof (UINT4);
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = EcfmCcLock;
        SnmpNotifyInfo.pUnLockPointer = EcfmCcUnLock;
        SnmpNotifyInfo.u4Indices = ECFM_TABLE_INDICES_COUNT_TWO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (),
                          i4CfmDefaultMdPrimarySelector,
                          i4SetValIeee8021CfmDefaultMdMhfCreation));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* Check if all the values of the Node are default, if so then delete the
     * Node */
    EcfmSnmpLwProcessDefaultMdNode (i4CfmDefaultMdPrimarySelector);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmDefaultMdIdPermission
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                setValIeee8021CfmDefaultMdIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmDefaultMdIdPermission (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                        INT4
                                        i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                        UINT4
                                        u4Ieee8021CfmDefaultMdPrimarySelector,
                                        INT4
                                        i4SetValIeee8021CfmDefaultMdIdPermission)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        /* No Default entry is found */
        /* case 1: user wants to set the default value, don't add the node */
        /* case 2: user wants to set something other than default value, 
         *         add a default MD node*/
        if (i4SetValIeee8021CfmDefaultMdIdPermission == ECFM_SENDER_ID_DEFER)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if ((pDefaultMdNode =
                 EcfmSnmpLwSetDefaultMdNode (i4CfmDefaultMdPrimarySelector)) ==
                NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* Check if it is already same */
    if (pDefaultMdNode->u1IdPermission ==
        (UINT1) i4SetValIeee8021CfmDefaultMdIdPermission)
    {
        return SNMP_SUCCESS;
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the SenderId Permission to corresponding DefaultMd entry */
    pDefaultMdNode->u1IdPermission =
        (UINT1) i4SetValIeee8021CfmDefaultMdIdPermission;

    /* Update Default MD at LBLT also */
    EcfmLbLtUpdateDefaultMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pDefaultMdNode);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pDefaultMdNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.pu4ObjectId = FsMIEcfmDefaultMdIdPermission;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIEcfmDefaultMdIdPermission) / sizeof (UINT4);
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = EcfmCcLock;
        SnmpNotifyInfo.pUnLockPointer = EcfmCcUnLock;
        SnmpNotifyInfo.u4Indices = ECFM_TABLE_INDICES_COUNT_TWO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (),
                          i4CfmDefaultMdPrimarySelector,
                          i4SetValIeee8021CfmDefaultMdIdPermission));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* Check if all the values of the Node are default, if so then delete the
     * Node */
    EcfmSnmpLwProcessDefaultMdNode (i4CfmDefaultMdPrimarySelector);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmDefaultMdLevel
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                testValIeee8021CfmDefaultMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmDefaultMdLevel (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                    INT4
                                    i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                    UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                    INT4 i4TestValIeee8021CfmDefaultMdLevel)
{
    INT4                i4LocalIsid = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        /* Validate index range */
        if ((i4CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (i4CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        i4LocalIsid =
            ECFM_ISID_INTERNAL_TO_ISID (i4CfmDefaultMdPrimarySelector);
        /* Validate index range */
        if ((i4LocalIsid < ECFM_ISID_MIN) || (i4LocalIsid > ECFM_ISID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Now validating the MdLevel, that is to be Test */
    if ((i4TestValIeee8021CfmDefaultMdLevel < ECFM_DEF_MD_LEVEL_DEF_VAL) ||
        (i4TestValIeee8021CfmDefaultMdLevel > ECFM_MD_LEVEL_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MDLevel for Default Domain \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmDefaultMdMhfCreation
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                testValIeee8021CfmDefaultMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmDefaultMdMhfCreation (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4Ieee8021CfmDefaultMdComponentId,
                                          INT4
                                          i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                          UINT4
                                          u4Ieee8021CfmDefaultMdPrimarySelector,
                                          INT4
                                          i4TestValIeee8021CfmDefaultMdMhfCreation)
{
    INT4                i4LocalIsid = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        /* Validate index range */
        if ((i4CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (i4CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        i4LocalIsid =
            ECFM_ISID_INTERNAL_TO_ISID (i4CfmDefaultMdPrimarySelector);
        /* Validate index range */
        if ((i4LocalIsid < ECFM_ISID_MIN) || (i4LocalIsid > ECFM_ISID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Now validating the MHfCreation, that is to be Test */
    if ((i4TestValIeee8021CfmDefaultMdMhfCreation < ECFM_MHF_CRITERIA_NONE) ||
        (i4TestValIeee8021CfmDefaultMdMhfCreation > ECFM_MHF_CRITERIA_DEFER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MHFCreation Value for"
                     "Default Domain entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmDefaultMdIdPermission
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                testValIeee8021CfmDefaultMdIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmDefaultMdIdPermission (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021CfmDefaultMdComponentId,
                                           INT4
                                           i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                           UINT4
                                           u4Ieee8021CfmDefaultMdPrimarySelector,
                                           INT4
                                           i4TestValIeee8021CfmDefaultMdIdPermission)
{
    INT4                i4LocalIsid = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        /* Validate index range */
        if ((i4CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (i4CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        i4LocalIsid =
            ECFM_ISID_INTERNAL_TO_ISID (i4CfmDefaultMdPrimarySelector);
        /* Validate index range */
        if ((i4LocalIsid < ECFM_ISID_MIN) || (i4LocalIsid > ECFM_ISID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Now validating the SenderId Permission, that is to be Test */
    if ((i4TestValIeee8021CfmDefaultMdIdPermission < ECFM_SENDER_ID_NONE) ||
        (i4TestValIeee8021CfmDefaultMdIdPermission > ECFM_SENDER_ID_DEFER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for SenderID Permission"
                     " for Default Domain\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CfmDefaultMdTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CfmConfigErrorListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable (INT4
                                                         i4Ieee8021CfmConfigErrorListSelectorType,
                                                         UINT4
                                                         u4Ieee8021CfmConfigErrorListSelector,
                                                         INT4
                                                         i4Ieee8021CfmConfigErrorListIfIndex)
{
    tEcfmCcConfigErrInfo *pCofigErrNode = NULL;
    UINT4               u4VlanIdIsidOrNone =
        u4Ieee8021CfmConfigErrorListSelector;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmConfigErrorListSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmConfigErrorListSelector);
    }

    /* Get Config Err entry corresponding to indices, VlanId, IfIndex */
    pCofigErrNode = EcfmSnmpLwGetConfigErrEntry (u4VlanIdIsidOrNone,
                                                 i4Ieee8021CfmConfigErrorListIfIndex);
    if (pCofigErrNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Config Error Entry found\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmConfigErrorListTable
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmConfigErrorListTable (INT4
                                                 *pi4Ieee8021CfmConfigErrorListSelectorType,
                                                 UINT4
                                                 *pu4Ieee8021CfmConfigErrorListSelector,
                                                 INT4
                                                 *pi4Ieee8021CfmConfigErrorListIfIndex)
{
    return (nmhGetNextIndexIeee8021CfmConfigErrorListTable (0,
                                                            pi4Ieee8021CfmConfigErrorListSelectorType,
                                                            0,
                                                            pu4Ieee8021CfmConfigErrorListSelector,
                                                            0,
                                                            pi4Ieee8021CfmConfigErrorListIfIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmConfigErrorListTable
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                nextIeee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                nextIeee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex
                nextIeee8021CfmConfigErrorListIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmConfigErrorListTable (INT4
                                                i4Ieee8021CfmConfigErrorListSelectorType,
                                                INT4
                                                *pi4NextIeee8021CfmConfigErrorListSelectorType,
                                                UINT4
                                                u4Ieee8021CfmConfigErrorListSelector,
                                                UINT4
                                                *pu4NextIeee8021CfmConfigErrorListSelector,
                                                INT4
                                                i4Ieee8021CfmConfigErrorListIfIndex,
                                                INT4
                                                *pi4NextIeee8021CfmConfigErrorListIfIndex)
{
    tEcfmCcConfigErrInfo ConfigErrInfo;
    tEcfmCcConfigErrInfo *pConfigErrNextNode = NULL;
    UINT4               u4VlanIdIsidOrNone =
        u4Ieee8021CfmConfigErrorListSelector;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmConfigErrorListSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmConfigErrorListSelector);
    }

    /* Get ConfigErrList entry corresponding to indices next to VlanId, IfIndex */
    ECFM_MEMSET (&ConfigErrInfo, ECFM_INIT_VAL, ECFM_CC_CONFIG_ERR_INFO_SIZE);
    ConfigErrInfo.u4IfIndex = (UINT4) i4Ieee8021CfmConfigErrorListIfIndex;
    ConfigErrInfo.u4VidIsid = u4VlanIdIsidOrNone;

    pConfigErrNextNode = (tEcfmCcConfigErrInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, (tRBElem *) & ConfigErrInfo, NULL);
    if (pConfigErrNextNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Config Error Entry found\n");
        return SNMP_FAILURE;
    }

    /* ConfigErrList entry corresponding to indices next to VlanId, IfIndex exists */
    /* Set the indices corresponding to ConfigErrList entry */
    if ((pConfigErrNextNode->u4VidIsid) >= ECFM_INTERNAL_ISID_MIN)
    {
        *pu4NextIeee8021CfmConfigErrorListSelector =
            ECFM_ISID_INTERNAL_TO_ISID ((pConfigErrNextNode->u4VidIsid));
        *pi4NextIeee8021CfmConfigErrorListSelectorType =
            ECFM_SERVICE_SELECTION_ISID;
    }
    else
    {
        *pu4NextIeee8021CfmConfigErrorListSelector =
            ((pConfigErrNextNode->u4VidIsid));
        *pi4NextIeee8021CfmConfigErrorListSelectorType =
            ECFM_SERVICE_SELECTION_VLAN;
    }

    *pi4NextIeee8021CfmConfigErrorListIfIndex =
        (INT4) pConfigErrNextNode->u4IfIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmConfigErrorListErrorType
 Input       :  The Indices
                Ieee8021CfmConfigErrorListSelectorType
                Ieee8021CfmConfigErrorListSelector
                Ieee8021CfmConfigErrorListIfIndex

                The Object 
                retValIeee8021CfmConfigErrorListErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmConfigErrorListErrorType (INT4
                                           i4Ieee8021CfmConfigErrorListSelectorType,
                                           UINT4
                                           u4Ieee8021CfmConfigErrorListSelector,
                                           INT4
                                           i4Ieee8021CfmConfigErrorListIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValIeee8021CfmConfigErrorListErrorType)
{
    tEcfmCcConfigErrInfo *pCofigErrNode = NULL;
    UINT4               u4VlanIdIsidOrNone =
        u4Ieee8021CfmConfigErrorListSelector;

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmConfigErrorListSelectorType == ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        u4VlanIdIsidOrNone =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmConfigErrorListSelector);
    }

    /* Get Config Err entry corresponding to indices, VlanId, IfIndex */
    pCofigErrNode = EcfmSnmpLwGetConfigErrEntry (u4VlanIdIsidOrNone,
                                                 i4Ieee8021CfmConfigErrorListIfIndex);
    if (pCofigErrNode == NULL)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Config Error Entry Found\n");
        return SNMP_FAILURE;
    }
    pRetValIeee8021CfmConfigErrorListErrorType->pu1_OctetList[0] =
        pCofigErrNode->u1ErrorType;
    pRetValIeee8021CfmConfigErrorListErrorType->i4_Length = 1;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ieee8021CfmMaCompTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmMaCompTable
 Input       :  The Indices
                Dot1agCfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmMaCompTable (UINT4
                                                u4Ieee8021CfmMaComponentId,
                                                UINT4 u4Dot1agCfmMdIndex,
                                                UINT4 u4Dot1agCfmMaIndex)
{
    return (EcfmUtlValIndexInstMaCompTable (u4Ieee8021CfmMaComponentId,
                                            u4Dot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmMaCompTable
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmMaCompTable (UINT4 *pu4Ieee8021CfmMaComponentId,
                                        UINT4 *pu4Dot1agCfmMdIndex,
                                        UINT4 *pu4Dot1agCfmMaIndex)
{
    return (nmhGetNextIndexIeee8021CfmMaCompTable
            (0, pu4Ieee8021CfmMaComponentId, 0, pu4Dot1agCfmMdIndex, 0,
             pu4Dot1agCfmMaIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmMaCompTable
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                nextIeee8021CfmMaComponentId
                Dot1agCfmMdIndex
                nextDot1agCfmMdIndex
                Dot1agCfmMaIndex
                nextDot1agCfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmMaCompTable (UINT4 u4Ieee8021CfmMaComponentId,
                                       UINT4 *pu4NextIeee8021CfmMaComponentId,
                                       UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 *pu4NextDot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       UINT4 *pu4NextDot1agCfmMaIndex)
{
    return (EcfmUtlGetNextIndexMaCompTable (u4Ieee8021CfmMaComponentId,
                                            pu4NextIeee8021CfmMaComponentId,
                                            u4Dot1agCfmMdIndex,
                                            pu4NextDot1agCfmMdIndex,
                                            u4Dot1agCfmMaIndex,
                                            pu4NextDot1agCfmMaIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompPrimarySelectorType
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompPrimarySelectorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompPrimarySelectorType (UINT4 u4Ieee8021CfmMaComponentId,
                                            UINT4 u4Dot1agCfmMdIndex,
                                            UINT4 u4Dot1agCfmMaIndex,
                                            INT4
                                            *pi4RetValIeee8021CfmMaCompPrimarySelectorType)
{
    return EcfmUtlGetMaCompPriSelType (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       pi4RetValIeee8021CfmMaCompPrimarySelectorType);
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompPrimarySelectorOrNone
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompPrimarySelectorOrNone
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (UINT4 u4Ieee8021CfmMaComponentId,
                                              UINT4 u4Dot1agCfmMdIndex,
                                              UINT4 u4Dot1agCfmMaIndex,
                                              UINT4
                                              *pu4RetValIeee8021CfmMaCompPrimarySelectorOrNone)
{
    return (EcfmUtlGetMaCompPriSelectOrNone (u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             pu4RetValIeee8021CfmMaCompPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompMhfCreation
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompMhfCreation (UINT4 u4Ieee8021CfmMaComponentId,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 *pi4RetValIeee8021CfmMaCompMhfCreation)
{
    return (EcfmUtlGetMaCompMhfCreation (u4Ieee8021CfmMaComponentId,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         pi4RetValIeee8021CfmMaCompMhfCreation));
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompIdPermission
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompIdPermission (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4
                                     *pi4RetValIeee8021CfmMaCompIdPermission)
{
    return (EcfmUtlGetMaCompIdPermission (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          pi4RetValIeee8021CfmMaCompIdPermission));

}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompNumberOfVids
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompNumberOfVids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompNumberOfVids (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4
                                     *pu4RetValIeee8021CfmMaCompNumberOfVids)
{
    return (EcfmUtlGetMaCompNumberOfVids (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          pu4RetValIeee8021CfmMaCompNumberOfVids));
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmMaCompRowStatus
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                retValIeee8021CfmMaCompRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmMaCompRowStatus (UINT4 u4Ieee8021CfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 *pi4RetValIeee8021CfmMaCompRowStatus)
{
    return (EcfmUtlGetMaCompRowStatus (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       pi4RetValIeee8021CfmMaCompRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompPrimarySelectorType
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompPrimarySelectorType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompPrimarySelectorType (UINT4 u4Ieee8021CfmMaComponentId,
                                            UINT4 u4Dot1agCfmMdIndex,
                                            UINT4 u4Dot1agCfmMaIndex,
                                            INT4
                                            i4SetValIeee8021CfmMaCompPrimarySelectorType)
{
    return EcfmUtlSetMaCompPriSelType (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       i4SetValIeee8021CfmMaCompPrimarySelectorType);
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompPrimarySelectorOrNone
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompPrimarySelectorOrNone
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompPrimarySelectorOrNone (UINT4 u4Ieee8021CfmMaComponentId,
                                              UINT4 u4Dot1agCfmMdIndex,
                                              UINT4 u4Dot1agCfmMaIndex,
                                              UINT4
                                              u4SetValIeee8021CfmMaCompPrimarySelectorOrNone)
{
    return (EcfmUtlSetMaCompPriSelorNone (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4SetValIeee8021CfmMaCompPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompMhfCreation
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompMhfCreation (UINT4 u4Ieee8021CfmMaComponentId,
                                    UINT4 u4Dot1agCfmMdIndex,
                                    UINT4 u4Dot1agCfmMaIndex,
                                    INT4 i4SetValIeee8021CfmMaCompMhfCreation)
{
    return (EcfmUtlSetMaCompMhfCreation (u4Ieee8021CfmMaComponentId,
                                         u4Dot1agCfmMdIndex,
                                         u4Dot1agCfmMaIndex,
                                         i4SetValIeee8021CfmMaCompMhfCreation));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompIdPermission
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompIdPermission (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4 i4SetValIeee8021CfmMaCompIdPermission)
{
    return (EcfmUtlSetMaCompIdPermission (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          i4SetValIeee8021CfmMaCompIdPermission));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompNumberOfVids
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompNumberOfVids
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompNumberOfVids (UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     UINT4
                                     u4SetValIeee8021CfmMaCompNumberOfVids)
{
    return (EcfmUtlSetMaCompNumberOfVids (u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          u4SetValIeee8021CfmMaCompNumberOfVids));
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmMaCompRowStatus
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                setValIeee8021CfmMaCompRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmMaCompRowStatus (UINT4 u4Ieee8021CfmMaComponentId,
                                  UINT4 u4Dot1agCfmMdIndex,
                                  UINT4 u4Dot1agCfmMaIndex,
                                  INT4 i4SetValIeee8021CfmMaCompRowStatus)
{
    return (EcfmUtlSetMaCompRowStatus (u4Ieee8021CfmMaComponentId,
                                       u4Dot1agCfmMdIndex,
                                       u4Dot1agCfmMaIndex,
                                       i4SetValIeee8021CfmMaCompRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompPrimarySelectorType
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompPrimarySelectorType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompPrimarySelectorType (UINT4 *pu4ErrorCode,
                                               UINT4 u4Ieee8021CfmMaComponentId,
                                               UINT4 u4Dot1agCfmMdIndex,
                                               UINT4 u4Dot1agCfmMaIndex,
                                               INT4
                                               i4TestValIeee8021CfmMaCompPrimarySelectorType)
{
    return EcfmUtlTestMaCompPriSelType (pu4ErrorCode,
                                        u4Ieee8021CfmMaComponentId,
                                        u4Dot1agCfmMdIndex,
                                        u4Dot1agCfmMaIndex,
                                        i4TestValIeee8021CfmMaCompPrimarySelectorType);
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompPrimarySelectorOrNone
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021CfmMaComponentId,
                                                 UINT4 u4Dot1agCfmMdIndex,
                                                 UINT4 u4Dot1agCfmMaIndex,
                                                 UINT4
                                                 u4TestValIeee8021CfmMaCompPrimarySelectorOrNone)
{
    return (EcfmUtlTestv2MaCompPriSelOrNone (pu4ErrorCode,
                                             u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4TestValIeee8021CfmMaCompPrimarySelectorOrNone));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompMhfCreation
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompMhfCreation (UINT4 *pu4ErrorCode,
                                       UINT4 u4Ieee8021CfmMaComponentId,
                                       UINT4 u4Dot1agCfmMdIndex,
                                       UINT4 u4Dot1agCfmMaIndex,
                                       INT4
                                       i4TestValIeee8021CfmMaCompMhfCreation)
{
    return (EcfmUtlTstv2CfmMaCompMhfCreation (pu4ErrorCode,
                                              u4Ieee8021CfmMaComponentId,
                                              u4Dot1agCfmMdIndex,
                                              u4Dot1agCfmMaIndex,
                                              i4TestValIeee8021CfmMaCompMhfCreation));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompIdPermission
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompIdPermission (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021CfmMaComponentId,
                                        UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        INT4
                                        i4TestValIeee8021CfmMaCompIdPermission)
{
    return (EcfmUtlTestv2MaCompIdPermission (pu4ErrorCode,
                                             u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             i4TestValIeee8021CfmMaCompIdPermission));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompNumberOfVids
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompNumberOfVids
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompNumberOfVids (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021CfmMaComponentId,
                                        UINT4 u4Dot1agCfmMdIndex,
                                        UINT4 u4Dot1agCfmMaIndex,
                                        UINT4
                                        u4TestValIeee8021CfmMaCompNumberOfVids)
{
    return (EcfmUtlTestv2MaCompNumberOfVids (pu4ErrorCode,
                                             u4Ieee8021CfmMaComponentId,
                                             u4Dot1agCfmMdIndex,
                                             u4Dot1agCfmMaIndex,
                                             u4TestValIeee8021CfmMaCompNumberOfVids));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmMaCompRowStatus
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex

                The Object 
                testValIeee8021CfmMaCompRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmMaCompRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021CfmMaComponentId,
                                     UINT4 u4Dot1agCfmMdIndex,
                                     UINT4 u4Dot1agCfmMaIndex,
                                     INT4 i4TestValIeee8021CfmMaCompRowStatus)
{

    return (EcfmUtlTestv2MaCompRowStatus (pu4ErrorCode,
                                          u4Ieee8021CfmMaComponentId,
                                          u4Dot1agCfmMdIndex,
                                          u4Dot1agCfmMaIndex,
                                          i4TestValIeee8021CfmMaCompRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CfmMaCompTable
 Input       :  The Indices
                Ieee8021CfmMaComponentId
                Dot1agCfmMdIndex
                Dot1agCfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CfmMaCompTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
