/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmthini.c,v 1.21 2014/05/05 12:47:21 siva Exp $
 *
 * Description: This file contains:
 *         1. The Functionality of the 1 way  and 2 way Throughput 
 *         Initiation , 
 *         2. TH VSM PDUs is initiation
 *              3. Processing of VSR PDUs.
 *******************************************************************/

#include "cfminc.h"
PRIVATE INT4 EcfmThInitXmit2Th PROTO ((tEcfmLbLtMepInfo *));
PRIVATE VOID EcfmThInitFormatThVsmPduHdr PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID        EcfmThInitPutThVsmInfo
PROTO ((tEcfmLbLtMepInfo *, UINT1 **, UINT1));
PRIVATE INT4        EcfmThInitFormatEthHdr
PROTO ((tEcfmLbLtMepInfo *, UINT1 **, UINT2));
PRIVATE INT4 EcfmThInitXmit1Th PROTO ((tEcfmLbLtMepInfo *));
PRIVATE UINT4 EcfmThInitBurst PROTO ((tEcfmLbLtPduSmInfo *));
PRIVATE INT4 EcfmThXmitThVsmPdu PROTO ((tEcfmLbLtMepInfo *, UINT1));
PRIVATE VOID EcfmSendThEventToCli PROTO ((tEcfmLbLtMepInfo *, UINT1));

/*******************************************************************************
 * Function           : EcfmLbLtClntThInitiator
 *
 * Description        : The routine implements the Throughput Initiator, it calls up
 *                      routine to transmit LB/TST frame.
 *                      the various events.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/

PUBLIC UINT4
EcfmLbLtClntThInitiator (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 u1EventID)
{

    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tMacAddr            MacAddr = { 0 };

    DBL8                d8IntervalMSec = ECFM_INIT_VAL;
    UINT4               u4GetTstRxCountTimer = ECFM_INIT_VAL;
    UINT4               u4RetVal = ECFM_SUCCESS;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pThInfo = &pMepInfo->ThInfo;
    /* Check for the event received */
    switch (u1EventID)
    {
        case ECFM_EV_MEP_BEGIN:
            /*Set TH Initiator related values to their defaults */
            pThInfo->u1ThStatus = ECFM_TX_STATUS_READY;
            break;
        case ECFM_EV_MEP_NOT_ACTIVE:
            EcfmThInitStopThTransaction (pMepInfo);
            break;
        case ECFM_EV_LB_BURST_COMPLETE:
            if ((pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY) &&
                (pThInfo->u1ThState != ECFM_TH_STATE_WAITING_LBR_TIMEOUT))
            {
                u4RetVal = ECFM_FAILURE;
                break;
            }
            if (pThInfo->u1ThStatus == ECFM_TX_STATUS_READY)
            {
                return ECFM_SUCCESS;
            }

            if (EcfmThInitBurst (pPduSmInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                               ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtClntThInitiator: "
                               "Burst cannot be initiated\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_EV_TST_BURST_COMPLETE:
            if ((pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY) &&
                (pThInfo->u1ThState != ECFM_TH_STATE_WAITING_TST_TIMEOUT))
            {
                u4RetVal = ECFM_FAILURE;
                break;
            }
            if (pThInfo->u1ThStatus == ECFM_TX_STATUS_READY)
            {
                return ECFM_SUCCESS;
            }

            d8IntervalMSec = ECFM_TH_TST_FETCH_RX_CNT_TMR;

            if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_MSGS)
            {
                u4GetTstRxCountTimer =
                    ECFM_OPERATE_WITH_VAL_TWO * (pThInfo->u2TxThBurstMessages *
                                                 d8IntervalMSec);
            }
            else
            {
                u4GetTstRxCountTimer =
                    ECFM_OPERATE_WITH_VAL_TWO * (pThInfo->u4TxThBurstDeadline);
            }

            /*Start the TH Get TstRxCount Timer */
            if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT,
                                       pMepInfo,
                                       (u4GetTstRxCountTimer)) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                               ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtClntThInitiator: "
                               "Get TstRxCount Timer has not started\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }

            pThInfo->u1ThState = ECFM_TH_STATE_WAITING_GET_TSTINFO;

            u4RetVal = ECFM_SUCCESS;
            break;

        case ECFM_LBLT_TH_GET_TSTINFO_EXPIRY:
            if ((pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY) &&
                (pThInfo->u1ThState != ECFM_TH_STATE_WAITING_GET_TSTINFO))
            {
                u4RetVal = ECFM_FAILURE;
                break;
            }

            /* Call the routine to transmit the VSM Packet to fetch the TST
             * Counter */
            if (EcfmThXmitThVsmPdu (pMepInfo,
                                    ECFM_LBLT_TH_REQCODE_TST_FETCH) !=
                ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                               ECFM_ALL_FAILURE_TRC,
                               "EcfmThInitXmit1Th: "
                               "Unable to transmit VSM Pdu\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }

            /*Start the Vsr Timeout Timer and it will wait until the VSR
             * PDU is received or the timer is expired  */
            if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_VSR_TIMEOUT,
                                       pMepInfo,
                                       (Y1731_VSR_TIMEOUT_DEF_VAL *
                                        ECFM_NUM_OF_MSEC_IN_A_SEC)) !=
                ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                               ECFM_ALL_FAILURE_TRC,
                               "EcfmThInitXmit1Th: "
                               "Vsr Timeout Timer has not started\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }

            /* Change the Throughput Statte to waiting for Vsr Timeout */
            pThInfo->u1ThState = ECFM_TH_STATE_WAITING_VSR_TIMEOUT;

            u4RetVal = ECFM_SUCCESS;
            break;

        case ECFM_LBLT_TH_VSR_TIMEOUT_EXPIRY:
            pThInfo->b1TxThResultOk = ECFM_FALSE;
            u4RetVal = ECFM_FAILURE;
        case ECFM_LBLT_TH_DEADLINE_EXPIRY:
        case ECFM_TH_STOP_TRANSACTION:

            /* Stop the TH Transaction */
            EcfmThInitStopThTransaction (pMepInfo);
            break;
        case ECFM_TH_START_TRANSACTION:
            pThInfo->u1ThStatus = ECFM_TX_STATUS_NOT_READY;

            ECFM_LBLT_CURR_LBR_CACHE_STATUS = ECFM_LBLT_LBR_CACHE_STATUS;
            ECFM_LBLT_LBR_CACHE_STATUS = ECFM_DISABLE;
            /* Check if Intifinte transmission is required */
            if (pThInfo->u2TxThMessages == ECFM_INIT_VAL)
            {
                /* Set MSB in the variable to let the TH transmitter know
                 * that infinite transmission is requied
                 */
                ECFM_SET_INFINITE_TX_STATUS (pThInfo->u2TxThMessages);
            }
            /*No need of initiating TH transaction at STANDBY side, it will be
             * initiated with the remaining deadline time or no of messages when
             * STANDBY will become ACTIVE*/
            if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
            {
                u4RetVal = ECFM_SUCCESS;
                break;
            }

            pThInfo->d8MeasuredThBps = ECFM_INIT_VAL;
            pThInfo->d8TxThTL = ECFM_LBLT_TH_MIN;
            pThInfo->d8TxThTU = pMepInfo->pPortInfo->u4MaxBps;
            pThInfo->d8TxThTH =
                (pThInfo->u4TxThPps *
                 (pThInfo->u2TxThFrameSize * ECFM_BITS_PER_BYTE));
            pThInfo->u4TxThInterval =
                (UINT4) (ECFM_NUM_OF_USEC_IN_A_SEC / pThInfo->u4TxThPps);

            if ((pThInfo->u4TxThInterval <
                 (ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT * ECFM_NUM_OF_USEC_IN_A_MSEC))
                && (ECFM_USEC_TIMER_SUPPORT () == ECFM_FALSE))

            {
                pThInfo->u4TxThInterval = ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT *
                    ECFM_NUM_OF_USEC_IN_A_MSEC;
            }

            /* Start the THDeadline Time if */
            if (pThInfo->u4TxThDeadline != ECFM_INIT_VAL)
            {
                /*Start the TH Deadline Timer */
                if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_TH_DEADLINE,
                                           pMepInfo,
                                           (pThInfo->u4TxThDeadline *
                                            ECFM_NUM_OF_MSEC_IN_A_SEC)) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntThInitiator: "
                                   "FDDeadLine Timer has not started\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            if (pThInfo->b1TxThIsDestMepId == ECFM_TRUE)
            {

                ECFM_MEMSET (&MacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));

                /* Get RMEP MAC Address for the Destination MEPID */
                if (EcfmLbLtUtilGetRMepMacAddr
                    (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                     pMepInfo->u2MepId, pThInfo->u2TxThDestMepId,
                     MacAddr) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC
                                   | ECFM_OS_RESOURCE_TRC,
                                   "EcfmLbLtClntThInitiator"
                                   "Rmep Mac Address not Found !! \r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
                ECFM_MEMCPY (pThInfo->TxThDestMacAddr, MacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            /* Send 1Way/2 Way */
            if (pThInfo->u1TxThType == ECFM_LBLT_TH_TYPE_2TH)
            {
                /* Call the routine to transmit the LBM Packet */
                if (EcfmThInitXmit2Th (pMepInfo) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntThInitiator: "
                                   "Unable to transmit LBM Pdu\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            else
            {
                /* Call the routine to transmit the TST Burst */
                if (EcfmThInitXmit1Th (pMepInfo) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntThInitiator: "
                                   "Unable to transmit TST Pdu\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            break;
        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtClntThInitiator: "
                           "Event Not Supported\r\n");
            u4RetVal = ECFM_FAILURE;
            break;
    }
    if (u4RetVal == ECFM_FAILURE)
    {
        /* Update Result OK to False */
        pThInfo->b1TxThResultOk = ECFM_FALSE;
        /* Stop the TH Transaction */
        EcfmThInitStopThTransaction (pMepInfo);
    }
    else
    {
        /* Update Result OK to True */
        pThInfo->b1TxThResultOk = ECFM_TRUE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmThInitXmit2Th
 *
 * Description        : This routine calls the LBInitiator for the 2 way
 *                      throughput measurement.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmThInitXmit2Th (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    DBL8                d8IntervalSec = ECFM_INIT_VAL;
    DBL8                d8BurstDeadlineSec = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pThInfo = &pMepInfo->ThInfo;
    pLbInfo = &pMepInfo->LbInfo;
    /*Check if Number Of Message to be transmitted have been reached */
    if (pThInfo->u2TxThMessages == ECFM_INIT_VAL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                       "EcfmThInitXmit2Th: "
                       "Stopping Throughput Transaction as total number of TH messages"
                       " have been transmitted\r\n");
        /*Stop TH Transaction */
        EcfmThInitStopThTransaction (pMepInfo);

        return ECFM_SUCCESS;
    }
    if (ECFM_GET_INFINITE_TX_STATUS (pThInfo->u2TxThMessages) == ECFM_FALSE)
    {
        /*Throughput messages left to send should not be less then then the no.
         * of burst messages */
        if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_MSGS)
        {
            if (pThInfo->u2TxThMessages < pThInfo->u2TxThBurstMessages)
            {
                pThInfo->u2TxThBurstMessages = pThInfo->u2TxThMessages;
            }
        }
        else
        {
            /*Throughput deadline should not be of less period then then the
             * burst period */
            if ((pThInfo->u2TxThMessages * pThInfo->u4TxThInterval) <
                pThInfo->u4TxThBurstDeadline)
            {
                pThInfo->u4TxThBurstDeadline =
                    pThInfo->u2TxThMessages * pThInfo->u4TxThInterval;
            }
        }
    }

    /*Frame size should not be greter then the max Pdu Size */
    if (pThInfo->u2TxThFrameSize >= ECFM_MAX_PDU_SIZE)
    {
        pThInfo->u2TxThFrameSize = ECFM_MAX_PDU_SIZE;
        pThInfo->d8TxThTU =
            (pThInfo->u4TxThPps *
             (pThInfo->u2TxThFrameSize * ECFM_BITS_PER_BYTE));
        pThInfo->d8TxThTH = pThInfo->d8TxThTU;
    }

    /*Frame size should not be less then the min Pdu Size */
    if (pThInfo->u2TxThFrameSize <= ECFM_LBLT_TH_FS_MIN)
    {
        pThInfo->u2TxThFrameSize = ECFM_LBLT_TH_FS_MIN;
        pThInfo->d8TxThTL =
            (pThInfo->u4TxThPps *
             (pThInfo->u2TxThFrameSize * ECFM_BITS_PER_BYTE));
        pThInfo->d8TxThTH = pThInfo->d8TxThTL;
    }

    pLbInfo->u4LbmOut = ECFM_INIT_VAL;
#ifdef NPAPI_WANTED
    if (ECFM_HW_LBM_SUPPORT () == ECFM_FALSE)
    {
        pLbInfo->u4LbrIn = ECFM_INIT_VAL;
    }
    else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        tEcfmMepInfoParams  EcfmMepInfoParams;
        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                     sizeof (tEcfmMepInfoParams));
        EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        EcfmMepInfoParams.u4IfIndex =
            ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
        EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;
        if (EcfmFsMiEcfmClearRcvdLbrCounter (&EcfmMepInfoParams) != FNP_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }
#else /*  */
    pLbInfo->u4LbrIn = ECFM_INIT_VAL;
#endif /*  */

    /* Call the Loopback StateMachine */
    pLbInfo->u1TxLbmMode = ECFM_LBLT_LB_BURST_TYPE;
    pLbInfo->u1TxLbmIntervalType = ECFM_LBLT_LB_INTERVAL_USEC;
    pLbInfo->u4TxLbmInterval = pThInfo->u4TxThInterval;
    pLbInfo->u2TxLbmMessages = pThInfo->u2TxThBurstMessages;
    pLbInfo->u1TxLbmTlvOrNone = ECFM_LBLT_LBM_WITH_TEST_TLV;
    pLbInfo->u1TxLbmTstPatternType = pThInfo->u1TxThTstPatternType;
    pLbInfo->u2TxLbmPatternSize = (UINT2) (pThInfo->u2TxThFrameSize -
                                           ECFM_LBLT_MIN_LBM_SIZE);
    pLbInfo->b1TxLbmVariableBytes = ECFM_FALSE;

    d8IntervalSec =
        ((DBL8) (pThInfo->u4TxThInterval) / ECFM_NUM_OF_USEC_IN_A_SEC);
    d8BurstDeadlineSec =
        ((DBL8) pThInfo->u4TxThBurstDeadline / ECFM_NUM_OF_MSEC_IN_A_SEC);
    /*Timeout period to wait fro the response is twice the burst period */
    if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_MSGS)
    {
        pLbInfo->u4RxLbrTimeout =
            ECFM_OPERATE_WITH_VAL_TWO * (pThInfo->u2TxThBurstMessages *
                                         (ECFM_NUM_OF_TIME_UNITS_IN_A_SEC *
                                          d8IntervalSec));
    }
    else
    {
        pLbInfo->u4RxLbrTimeout =
            ECFM_OPERATE_WITH_VAL_TWO * (d8BurstDeadlineSec *
                                         ECFM_NUM_OF_TIME_UNITS_IN_A_SEC);
    }

    if (pThInfo->b1TxThIsDestMepId == ECFM_TRUE)
    {
        pLbInfo->u1TxLbmDestType = ECFM_TX_DEST_TYPE_MEPID;
        pLbInfo->u2TxDestMepId = pThInfo->u2TxThDestMepId;
    }
    else
    {
        pLbInfo->u1TxLbmDestType = ECFM_TX_DEST_TYPE_UNICAST;
        ECFM_MEMCPY (pLbInfo->TxLbmDestMacAddr, pThInfo->TxThDestMacAddr,
                     ECFM_MAC_ADDR_LENGTH);
    }

    if (EcfmLbLtClntLbInitiator (pMepInfo, ECFM_LB_START_TRANSACTION) !=
        ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* If Number of Th Messages to send is configured then decrement it after
     * transmitting PDU */
    if (ECFM_GET_INFINITE_TX_STATUS (pThInfo->u2TxThMessages) == ECFM_FALSE)
    {
        pThInfo->u2TxThMessages =
            pThInfo->u2TxThMessages - pThInfo->u2TxThBurstMessages;
    }
    pThInfo->u1ThState = ECFM_TH_STATE_WAITING_LBR_TIMEOUT;
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmThInitStopThTransaction
 *
 * Description        : This routine is used to stop the on going TH transaction
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmThInitStopThTransaction (tEcfmLbLtMepInfo * pMepInfo)
{

    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pThInfo = &pMepInfo->ThInfo;
    pLbInfo = &pMepInfo->LbInfo;
    pTstInfo = &pMepInfo->TstInfo;

    /* Stop THDeadline Timer */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_TH_DEADLINE, pMepInfo);
    /* Stop THGetTstRxCounr Timer */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT, pMepInfo);
    /* Stop THVsrTimeout Timer */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_VSR_TIMEOUT, pMepInfo);

    /* Revert Back the Tx Status */
    pMepInfo->ThInfo.u1ThStatus = ECFM_TX_STATUS_READY;
    ECFM_CLEAR_INFINITE_TX_STATUS (pMepInfo->ThInfo.u2TxThMessages);

    if (pThInfo->u1TxThType == ECFM_LBLT_TH_TYPE_2TH)
    {
        EcfmLbLtClntLbInitiator (pMepInfo, ECFM_LB_STOP_TRANSACTION);
        pLbInfo->u1TxLbmTlvOrNone = ECFM_LBLT_LBM_WITHOUT_TLV;
        pLbInfo->u1TxLbmDestType = ECFM_TX_DEST_TYPE_UNICAST;
        pLbInfo->b1TxLbmVariableBytes = ECFM_FALSE;
        pLbInfo->u1TxLbmTstPatternType = ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC;
        pLbInfo->u2TxLbmMessages = Y1731_LB_MESG_DEF_VAL;
        pLbInfo->u1TxLbmIntervalType = ECFM_LBLT_LB_INTERVAL_SEC;
        pLbInfo->u4TxLbmInterval = Y1731_LB_INTERVAL_DEF_VAL;
        pLbInfo->u4TxLbmDeadline = Y1731_LB_DEADLINE_DEF_VAL;
        pLbInfo->u4RxLbrTimeout = ECFM_LBR_TIMEOUT_DEF_VAL;
        pLbInfo->u1TxLbmMode = ECFM_LBLT_LB_REQ_RES_TYPE;

    }
    else
    {
        EcfmLbLtClntTstInitiator (pMepInfo, ECFM_TST_STOP_TRANSACTION);
        pTstInfo->u4TstDeadLine = ECFM_TST_DEADLINE_DEF_VAL;
        pTstInfo->u4TstInterval = ECFM_TST_INTERVAL_DEF_VAL;
        pTstInfo->u4TstMessages = ECFM_TST_MESG_DEF_VAL;
        pTstInfo->u4TstsSent = ECFM_INIT_VAL;
        pTstInfo->u2TstPatternSize = ECFM_INIT_VAL;
        pTstInfo->u2TstLastPatternSize = ECFM_INIT_VAL;
        pTstInfo->u1TstCapability = ECFM_ENABLE;
        pTstInfo->u1TstPatternType = ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC;
        pTstInfo->u1TstStatus = ECFM_TX_STATUS_READY;
        pTstInfo->u1TxTstIntervalType = ECFM_LBLT_TST_INTERVAL_SEC;
        pTstInfo->u1TxTstDestType = ECFM_TX_DEST_TYPE_UNICAST;
        pTstInfo->b1TstResultOk = ECFM_TRUE;
        pTstInfo->b1TstVariableBytes = ECFM_FALSE;
    }

    if (pThInfo->u1ThStatus == ECFM_TX_STATUS_READY)
    {
        ECFM_LBLT_LBR_CACHE_STATUS = ECFM_LBLT_CURR_LBR_CACHE_STATUS;

        /*Setting the TH State to Default State when Transaction Complete */
        pThInfo->u1ThState = ECFM_TH_STATE_DEFAULT;
    }

    if (pThInfo->b1TxThResultOk == ECFM_FALSE)

    {
        EcfmSendThEventToCli (pMepInfo, CLI_EV_ECFM_TH_TRANS_ERROR);
    }

    EcfmSendThEventToCli (pMepInfo, CLI_EV_ECFM_TH_TRANS_STOP);
    pThInfo->CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;

    /* Sync the stop event to the standby node */
    EcfmRedSyncTransactionStopEvent (ECFM_RED_STOP_THROUGHPUT_TRANS,
                                     pMepInfo->u4MdIndex,
                                     pMepInfo->u4MaIndex, pMepInfo->u2MepId);
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmThInitXmit1Th
 *
 * Description        : This routine initiates the TH- VSM PDU and starts the VSR
 *                      Timeout Timer.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmThInitXmit1Th (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pThInfo = &pMepInfo->ThInfo;
    pTstInfo = &pMepInfo->TstInfo;
    /*Check if Number Of Message to be transmitted have been reached */
    if (pThInfo->u2TxThMessages == ECFM_INIT_VAL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                       "EcfmThInitXmit1Th: "
                       "Stopping Throughput Transaction as total number of TH messages"
                       " have been transmitted\r\n");
        /*Stop TH Transaction */
        EcfmThInitStopThTransaction (pMepInfo);

        return ECFM_SUCCESS;
    }
    if (ECFM_GET_INFINITE_TX_STATUS (pThInfo->u2TxThMessages) == ECFM_FALSE)
    {
        if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_MSGS)
        {
            /*Throughput messages left to send should not be less then then the no.
             * of burst messages */
            if (pThInfo->u2TxThMessages < pThInfo->u2TxThBurstMessages)
            {
                pThInfo->u2TxThBurstMessages = pThInfo->u2TxThMessages;
            }
        }
        else
        {
            /*Throughput deadline should not be of less period then then the
             * burst period */
            if ((pThInfo->u2TxThMessages * pThInfo->u4TxThInterval) <
                pThInfo->u4TxThBurstDeadline)
            {
                pThInfo->u4TxThBurstDeadline =
                    pThInfo->u2TxThMessages * pThInfo->u4TxThInterval;
            }
        }
    }

    /*Frame size should not be greter then the max Pdu Size */
    if (pThInfo->u2TxThFrameSize >= ECFM_MAX_PDU_SIZE)
    {
        pThInfo->u2TxThFrameSize = ECFM_MAX_PDU_SIZE;
        pThInfo->d8TxThTU =
            (pThInfo->u4TxThPps *
             (pThInfo->u2TxThFrameSize * ECFM_BITS_PER_BYTE));
        pThInfo->d8TxThTH = pThInfo->d8TxThTU;
    }

    /*Frame size should not be less then the min Pdu Size */
    if (pThInfo->u2TxThFrameSize <= ECFM_LBLT_TH_FS_MIN)
    {
        pThInfo->u2TxThFrameSize = ECFM_LBLT_TH_FS_MIN;
        pThInfo->d8TxThTL =
            (pThInfo->u4TxThPps *
             (pThInfo->u2TxThFrameSize * ECFM_BITS_PER_BYTE));
        pThInfo->d8TxThTH = pThInfo->d8TxThTL;
    }

    pTstInfo->u4TstsSent = ECFM_INIT_VAL;

    /* Call the routine to transmit the VSM Packet to clear the Tst Counter */
    if (EcfmThXmitThVsmPdu (pMepInfo,
                            ECFM_LBLT_TH_REQCODE_TST_CLEAR) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                       ECFM_ALL_FAILURE_TRC,
                       "EcfmThInitXmit1Th: " "Unable to transmit VSM Pdu\r\n");
        return ECFM_FAILURE;
    }

    /*Start the Vsr Timeout Timer */
    if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_VSR_TIMEOUT,
                               pMepInfo,
                               (Y1731_VSR_TIMEOUT_DEF_VAL *
                                ECFM_NUM_OF_MSEC_IN_A_SEC)) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                       ECFM_ALL_FAILURE_TRC,
                       "EcfmThInitXmit1Th: "
                       "Vsr Timeout Timer has not started\r\n");
        return ECFM_FAILURE;
    }

    /* Change the Throughput Statte to waiting for Vsr Timeout */
    pThInfo->u1ThState = ECFM_TH_STATE_WAITING_VSR_TIMEOUT;

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmThXmitThVsmPdu
 *
 * Description        : This routine used to transmit the VSM PDU to clear the 
 *                      Mep Counter or fetch the counter value
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *                      u1ReqCode - To clear Tst Counter or fetch the counter
 *                      value
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmThXmitThVsmPdu (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1ReqCode)
{
    tEcfmBufChainHeader *pBuf = NULL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1ThVsmPduStart = NULL;
    UINT1              *pu1ThVsmPduEnd = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocate CRU Buffer */
    pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_TH_VSM_PDU_SIZE, ECFM_INIT_VAL);
    if (pBuf == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmThXmitThVsmPdu: " "Buffer allocation failed\r\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_TH_VSM_PDU_SIZE);
    pu1ThVsmPduStart = ECFM_LBLT_PDU;
    pu1ThVsmPduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;
    /* Format the  Vsm PDU header */
    EcfmThInitFormatThVsmPduHdr (pMepInfo, &pu1ThVsmPduEnd);

    /* Fill out the Information like SubOpCode and Request Code */
    EcfmThInitPutThVsmInfo (pMepInfo, &pu1ThVsmPduEnd, u1ReqCode);
    u2PduLength = (UINT2) (pu1ThVsmPduEnd - pu1ThVsmPduStart);

    /* Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1ThVsmPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmThXmitThVsmPdu: "
                       "Buffer copy operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    pu1ThVsmPduStart = pu1EthLlcHdr;
    EcfmThInitFormatEthHdr (pMepInfo, &pu1EthLlcHdr, u2PduLength);

    /* Prepend the Ethernet and the LLC header in the
     * CRU buffer */
    u2PduLength = (UINT2) (pu1EthLlcHdr - pu1ThVsmPduStart);
    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1ThVsmPduStart,
                              (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmThXmitThVsmPdu: "
                       "Buffer prepend operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmThXmitThVsmPdu: "
                        "Sending out VSM-PDU to lower layer...\r\n");
    i4RetVal =
        EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                   pMepInfo->u4PrimaryVidIsid,
                                   ECFM_VLAN_PRIORITY_MAX, ECFM_FALSE,
                                   pMepInfo->u1Direction, ECFM_OPCODE_VSM, NULL,
                                   NULL);
    if (i4RetVal != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmThXmitThVsmPdu: "
                       "VSM transmission to the lower layer failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/*******************************************************************************
 * Function Name      : EcfmThInitFormatThVsmPduHdr
 *
 * Description        : This rotuine is used to fill the TH VSM CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1ThVsmPdu - Pointer to Pointer to the VSM PDU.
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmThInitFormatThVsmPduHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1ThVsmPdu)
{
    UINT1              *pu1ThVsmPdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pu1ThVsmPdu = *ppu1ThVsmPdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1ThVsmPdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1ThVsmPdu, ECFM_OPCODE_VSM);

    /* Fill the Next 1 byte with Flag In VSM, Flag Field is set to ZERO */
    ECFM_PUT_1BYTE (pu1ThVsmPdu, ECFM_INIT_VAL);

    /*Fill in the next 1 byte as TLV Offset, Set as Zero */
    ECFM_PUT_1BYTE (pu1ThVsmPdu, ECFM_TH_VSM_FIRST_TLV_OFFSET);

    *ppu1ThVsmPdu = pu1ThVsmPdu;

    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmThInitPutThVsmInfo
 *
 * Description        : This routine is used to fill the Request Code and End TLV
 *                      in the VSM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure that stores
 *                      the information regarding MEP info.
 * 
 * Output(s)          : u1ReqCode - Request Code to filled in the VSM pdu.
 *                      ppu1ThVsmPdu - Pointer to pointer to the VSM Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmThInitPutThVsmInfo (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1ThVsmPdu,
                        UINT1 u1ReqCode)
{
    UINT1              *pu1ThVsmPdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    UNUSED_PARAM (pMepInfo);
    pu1ThVsmPdu = *ppu1ThVsmPdu;

    /* Fill OUI */
    ECFM_LBLT_SET_OUI (pu1ThVsmPdu);

    /* Fill SubOpCode in VSM */
    ECFM_PUT_1BYTE (pu1ThVsmPdu, ECFM_LBLT_TH_VSM_SUBOPCODE);

    /* Fill Request Code in VSM */
    ECFM_PUT_1BYTE (pu1ThVsmPdu, u1ReqCode);

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1ThVsmPdu, ECFM_END_TLV_TYPE);

    *ppu1ThVsmPdu = pu1ThVsmPdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 * Function Name      : EcfmThInitFormatEthHdr
 *
 * Description        : This routine is used to fill the Ethernet and LLC 
 *                      Header in the TH VSM frame.
 * 
 * Input              : pMepInfo - Pointer to the MEP info
 *                      u1Length - Length of the CFM header formated so far
 *                      filled
 *                       
 * Output(s)          : ppu1ThVsmPdu - Pointer to Pointer to the VSM Pdu.
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmThInitFormatEthHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1ThVsmPdu, UINT2
                        u2Length)
{
    tEcfmLbLtThInfo    *pThInfo = NULL;
    UINT1              *pu1ThVsmPdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    pu1ThVsmPdu = *ppu1ThVsmPdu;
    pThInfo = &(pMepInfo->ThInfo);
    /* Fill in the Ethernet header fields */
    /* Fill in the 6byte of Destination Address */
    ECFM_MEMCPY (pu1ThVsmPdu, pThInfo->TxThDestMacAddr, ECFM_MAC_ADDR_LENGTH);
    pu1ThVsmPdu = pu1ThVsmPdu + MAC_ADDR_LEN;

    /*Fill in the next 6byte with source address as the address of the 
     * Sending MEP
     */
    if (ECFM_LBLT_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        return ECFM_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT
        (ECFM_LBLT_PORT_INFO (pMepInfo->u2PortNum)->u4IfIndex, pu1ThVsmPdu);

    pu1ThVsmPdu = pu1ThVsmPdu + MAC_ADDR_LEN;

    /*Fill in the next 2 byte as Length of the CMF PDU if the LLC encap has to be
     * added else fill in as the CFM type */
    if (pMepInfo->pPortInfo->b1LLCEncapStatus == ECFM_TRUE)
    {
        /* Filling LLC Snap header */
        ECFM_LBLT_SET_LLC_SNAP_HDR (pu1ThVsmPdu,
                                    (u2Length + ECFM_LLC_SNAP_HDR_SIZE));
    }
    else
    {
        /* Fill in the next 2 byte as the CFM type */
        ECFM_PUT_2BYTE (pu1ThVsmPdu, ECFM_PDU_TYPE_CFM);
    }
    *ppu1ThVsmPdu = pu1ThVsmPdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbltClntThProcessVsr
 *
 * Description        : This routine processes the received VSR PDU. 
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbltClntThProcessVsr (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtRxThVsrPduInfo *pThVsrPduInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };
    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pThVsrPduInfo = &(pPduSmInfo->uPduInfo.Vsr);
    pThInfo = &(pMepInfo->ThInfo);
    pTstInfo = &(pMepInfo->TstInfo);
    /* Get the MEP's MAC Address */
    if (ECFM_LBLT_GET_PORT_INFO (pMepInfo->u2PortNum) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntThProcessVsr: " "No Port Information \r\n");
        return ECFM_FAILURE;
    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pMepInfo->u2PortNum)->u4IfIndex, MepMacAddr);
    /* Check if the Mac Address received in the VSR is same as that of the
     * receiving MEP
     */
    if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                               MepMacAddr) != ECFM_SUCCESS)
    {
        /* Discard the VSR frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntThProcessVsr: "
                       "discarding the received VSR frame\r\n");
        return ECFM_FAILURE;
    }

    if (pThInfo->u1ThState != ECFM_TH_STATE_WAITING_VSR_TIMEOUT)
    {
        /* Discard the VSR frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntThProcessVsr: "
                       "discarding the received VSR frame\r\n");
        return ECFM_FAILURE;
    }

    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_VSR_TIMEOUT, pMepInfo);

    if (pThVsrPduInfo->u1RespCode == ECFM_LBLT_TH_REQCODE_TST_CLEAR)
    {
        /*Call the TST State Machine */
        pTstInfo->u1TxTstIntervalType = ECFM_LBLT_TST_INTERVAL_USEC;
        pTstInfo->u4TstInterval = pThInfo->u4TxThInterval;
        pTstInfo->u4TstMessages = pThInfo->u2TxThBurstMessages;
        pTstInfo->u1TstPatternType = pThInfo->u1TxThTstPatternType;
        pTstInfo->b1TstVariableBytes = ECFM_FALSE;
        pTstInfo->u2TstPatternSize = (UINT2) (pThInfo->u2TxThFrameSize -
                                              ECFM_LBLT_MIN_TST_SIZE);

        if (pThInfo->b1TxThIsDestMepId == ECFM_TRUE)
        {
            pTstInfo->u1TxTstDestType = ECFM_TX_DEST_TYPE_MEPID;
            pTstInfo->u2TstDestMepId = pThInfo->u2TxThDestMepId;
        }
        else
        {
            pTstInfo->u1TxTstDestType = ECFM_TX_DEST_TYPE_UNICAST;
            ECFM_MEMCPY (pTstInfo->TstDestMacAddr, pThInfo->TxThDestMacAddr,
                         ECFM_MAC_ADDR_LENGTH);
        }
        pThInfo->u1ThState = ECFM_TH_STATE_WAITING_TST_TIMEOUT;
        EcfmLbLtClntTstInitiator (pMepInfo, ECFM_TST_START_TRANSACTION);

        /* If Number of Th Messages to send is configured then decrement it after
         * transmitting PDU */
        if (ECFM_GET_INFINITE_TX_STATUS (pThInfo->u2TxThMessages) == ECFM_FALSE)
        {
            pThInfo->u2TxThMessages =
                pThInfo->u2TxThMessages - pThInfo->u2TxThBurstMessages;
        }
    }
    else
    {
        if (EcfmThInitBurst (pPduSmInfo) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntThProcessVsr: "
                           "Burst cannot be initiated\r\n");
            return ECFM_FAILURE;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmThInitBurst
 *
 * Description        : This routine used to compare the Transission and
 *                       Reception Counters, and Initiate the BurstTST. 
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE UINT4
EcfmThInitBurst (tEcfmLbLtPduSmInfo * pPduSmInfo)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtRxThVsrPduInfo *pThVsrPduInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    UINT1               u1CounterStatus = ECFM_INIT_VAL;
    DBL8                d8FrameSize = ECFM_INIT_VAL;
    UINT4               u4RetVal = ECFM_SUCCESS;
    UINT4               u4LbrIn = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pThVsrPduInfo = &(pPduSmInfo->uPduInfo.Vsr);
    pThInfo = &(pMepInfo->ThInfo);
    pLbInfo = &(pMepInfo->LbInfo);
    pTstInfo = &(pMepInfo->TstInfo);

    /* Send 1Way/2 Way */
    if (pThInfo->u1TxThType == ECFM_LBLT_TH_TYPE_2TH)
    {
#ifdef NPAPI_WANTED
        if (ECFM_HW_LBM_SUPPORT () == ECFM_FALSE)
        {
            u4LbrIn = pLbInfo->u4LbrIn;
        }
        else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
        {
            tEcfmMepInfoParams  EcfmMepInfoParams;
            ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                         sizeof (tEcfmMepInfoParams));
            EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
            EcfmMepInfoParams.u4IfIndex =
                ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
            EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
            EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
            EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

            if (EcfmFsMiEcfmGetRcvdLbrCounter (&EcfmMepInfoParams, &u4LbrIn) !=
                FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
        }
#else /*  */
        u4LbrIn = pLbInfo->u4LbrIn;
#endif /*  */
        if (pLbInfo->u4LbmOut == u4LbrIn)
        {
            u1CounterStatus = ECFM_TRUE;
        }
        else
        {
            u1CounterStatus = ECFM_FALSE;
        }
    }
    else
    {
        if (pTstInfo->u4TstsSent == pThVsrPduInfo->u4TstIn)
        {
            u1CounterStatus = ECFM_TRUE;
        }
        else
        {
            u1CounterStatus = ECFM_FALSE;
        }
    }

    /*CLI Event for Burst Complete */
    EcfmSendThEventToCli (pMepInfo, CLI_EV_ECFM_TH_BURST_COMPLETE);

    /*When Previous throuput supported */
    if (u1CounterStatus == ECFM_TRUE)
    {
        pThInfo->d8MeasuredThBps = pThInfo->d8TxThTH;
        pThInfo->u2TxThVerifiedFrameSize = pThInfo->u2TxThFrameSize;
        if ((pThInfo->d8TxThTU - pThInfo->d8TxThTH) > 1)
        {
            pThInfo->d8TxThTL = pThInfo->d8TxThTH;
            pThInfo->d8TxThTH += (pThInfo->d8TxThTU - pThInfo->d8TxThTH) /
                ECFM_OPERATE_WITH_VAL_TWO;
        }
        else
        {
            EcfmThInitStopThTransaction (pMepInfo);
            return ECFM_SUCCESS;
        }
    }
    else
    {
        /*When Previous throughput not supported */
        if ((pThInfo->d8TxThTH - pThInfo->d8TxThTL) > 1)
        {
            pThInfo->d8TxThTU = pThInfo->d8TxThTH;
            pThInfo->d8TxThTH -= (pThInfo->d8TxThTH - pThInfo->d8TxThTL) /
                ECFM_OPERATE_WITH_VAL_TWO;
        }
        else
        {
            EcfmThInitStopThTransaction (pMepInfo);
            return ECFM_SUCCESS;
        }
    }

    d8FrameSize = (pThInfo->d8TxThTH /
                   (ECFM_NUM_OF_USEC_IN_A_SEC * ECFM_BITS_PER_BYTE)) *
        ((UINT4) (ECFM_NUM_OF_USEC_IN_A_SEC / pThInfo->u4TxThPps));

    pThInfo->u2TxThFrameSize = (UINT2) d8FrameSize;

    /* Send 1Way/2 Way */
    if (pThInfo->u1TxThType == ECFM_LBLT_TH_TYPE_2TH)
    {
        /* Call the routine to transmit the LBM Packet */
        if (EcfmThInitXmit2Th (pMepInfo) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtClntThInitiator: "
                           "Unable to transmit LBM Pdu\r\n");
            u4RetVal = ECFM_FAILURE;
        }
    }
    else
    {
        /* Call the routine to transmit the TST Burst */
        if (EcfmThInitXmit1Th (pMepInfo) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtClntThInitiator: "
                           "Unable to transmit TST Pdu\r\n");
            u4RetVal = ECFM_FAILURE;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmLbLtClntThParseVsr
 *
 * Description        : This is called to parse the received VSR PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      pu1Pdu - Pointer to the received PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntThParseVsr (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRxThVsrPduInfo *pVsrPduInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pVsrPduInfo = &(pPduSmInfo->uPduInfo.Vsr);
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;

    ECFM_GET_1BYTE (pVsrPduInfo->au1OUI[ECFM_INDEX_ZERO], pu1Pdu);
    ECFM_GET_1BYTE (pVsrPduInfo->au1OUI[ECFM_INDEX_ONE], pu1Pdu);
    ECFM_GET_1BYTE (pVsrPduInfo->au1OUI[ECFM_INDEX_TWO], pu1Pdu);
    ECFM_GET_1BYTE (pVsrPduInfo->u1SubOpCode, pu1Pdu);
    ECFM_GET_1BYTE (pVsrPduInfo->u1RespCode, pu1Pdu);

    if (ECFM_MEMCMP
        (pVsrPduInfo->au1OUI, ECFM_LBLT_ORG_UNIT_ID, ECFM_OUI_FIELD_SIZE) != 0)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParserVsr: OUI received doesnot match the system's OUI"
                       "\r\n");
        return ECFM_FAILURE;
    }

    if (pVsrPduInfo->u1SubOpCode != ECFM_LBLT_TH_VSM_SUBOPCODE)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntThParseVsr: VSR frame is not the related to Throughput "
                       "\r\n");

        return ECFM_FAILURE;
    }

    if (pVsrPduInfo->u1RespCode == ECFM_LBLT_TH_REQCODE_TST_FETCH)
    {
        ECFM_GET_4BYTE (pVsrPduInfo->u4TstIn, pu1Pdu);
    }
    /* Check that First TLV Offset value received in the VSR PDU */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_TH_VSR_FIRST_TLV_OFFSET)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParserVsr: VSR frame is discarded as the"
                       " first tlv offset if wrong is\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmSendThEventToCli
 *
 * Description        : This routine will be called on the following events
 *                      by the Th Initiator
 *                        - At the burst completion
 *                        - At the exit of TH transaction
 *                        - Error occurred while sending TH
 *                        
 * Input(s)           :  pMepInfo - Info of the MEP which has initiated the
 *                       TH.
 *                       i4EventType - Event type
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : None.
 ******************************************************************************/
PRIVATE VOID
EcfmSendThEventToCli (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventType)
{
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtCliEvInfo *pCliEvent = NULL;
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);
    pCliEvent = ECFM_LBLT_CLI_EVENT_INFO (pThInfo->CurrentCliHandle);
    if (pCliEvent == NULL)
    {
        return;
    }
    switch (u1EventType)
    {
        case CLI_EV_ECFM_TH_TRANS_STOP:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_TH_TRANS_STOP);
            break;
        case CLI_EV_ECFM_TH_BURST_COMPLETE:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_TH_BURST_COMPLETE);
            pCliEvent->Msg.u4Msg1 = pThInfo->d8TxThTH;
            pCliEvent->Msg.u4Msg2 = pThInfo->u2TxThFrameSize;
            break;
        case CLI_EV_ECFM_TH_TRANS_ERROR:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_TH_TRANS_ERROR);
            break;
        default:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_TH_TRANS_ERROR);
            break;
    }
    ECFM_GIVE_SEMAPHORE (pCliEvent->SyncSemId);
    return;
}

/*****************************************************************************
  End of File cfmthini.c
 ******************************************************************************/
