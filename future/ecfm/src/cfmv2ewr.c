/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmv2ewr.c,v 1.2 2011/12/27 11:53:41 siva Exp $
 *
 * Description: Wrapper Routines for the ecfm mib
 *********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "cfmv2elw.h"
#include  "cfmv2ewr.h"
#include  "cfmv2edb.h"
#include  "ecfm.h"
#include  "cfmdef.h"

INT4
GetNextIndexIeee8021CfmStackTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CfmStackTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CfmStackTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmStackMdIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmStackMdIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmStackMaIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmStackMaIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmStackMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmStackMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmStackMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetIeee8021CfmStackMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].i4_SLongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIeee8021CfmVlanTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CfmVlanTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CfmVlanTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanPrimarySelectorGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmVlanTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmVlanPrimarySelector
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmVlanTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmVlanRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanPrimarySelectorSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmVlanPrimarySelector
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmVlanRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanPrimarySelectorTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmVlanPrimarySelector (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmVlanRowStatus (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmVlanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CfmVlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CfmDefaultMdTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CfmDefaultMdTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CfmDefaultMdTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmDefaultMdStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmDefaultMdLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdMhfCreationGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetIeee8021CfmDefaultMdMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdIdPermissionGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmDefaultMdIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmDefaultMdLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdMhfCreationSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmDefaultMdMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdIdPermissionSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmDefaultMdIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmDefaultMdLevel (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmDefaultMdMhfCreation (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  i4_SLongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmDefaultMdIdPermission (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ZERO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ONE].
                                                   i4_SLongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_TWO].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmDefaultMdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CfmDefaultMdTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CfmConfigErrorListTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CfmConfigErrorListTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CfmConfigErrorListTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmConfigErrorListErrorTypeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmConfigErrorListErrorType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIeee8021CfmMaCompTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CfmMaCompTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CfmMaCompTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompPrimarySelectorTypeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmMaCompPrimarySelectorType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompPrimarySelectorOrNoneGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmMaCompPrimarySelectorOrNone
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmMaCompMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompIdPermissionGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmMaCompIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompNumberOfVidsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmMaCompNumberOfVids
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceIeee8021CfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetIeee8021CfmMaCompRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompPrimarySelectorTypeSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmMaCompPrimarySelectorType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompPrimarySelectorOrNoneSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmMaCompPrimarySelectorOrNone
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmMaCompMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompIdPermissionSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmMaCompIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompNumberOfVidsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmMaCompNumberOfVids
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetIeee8021CfmMaCompRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompPrimarySelectorTypeTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmMaCompPrimarySelectorType (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompPrimarySelectorOrNoneTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [ECFM_INDEX_ZERO].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_ONE].
                                                         u4_ULongValue,
                                                         pMultiIndex->
                                                         pIndex[ECFM_INDEX_TWO].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmMaCompMhfCreation (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmMaCompIdPermission (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompNumberOfVidsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmMaCompNumberOfVids (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Ieee8021CfmMaCompRowStatus (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Ieee8021CfmMaCompTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CfmMaCompTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

VOID
RegisterCFMV2E ()
{
    SNMPRegisterMib (&Ieee8021CfmStackTableOID, &Ieee8021CfmStackTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Ieee8021CfmVlanTableOID, &Ieee8021CfmVlanTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Ieee8021CfmDefaultMdTableOID,
                     &Ieee8021CfmDefaultMdTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Ieee8021CfmConfigErrorListTableOID,
                     &Ieee8021CfmConfigErrorListTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Ieee8021CfmMaCompTableOID, &Ieee8021CfmMaCompTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&cfmv2eOID, (const UINT1 *) "cfmv2ext");
}

VOID
UnRegisterCFMV2E ()
{
    SNMPUnRegisterMib (&Ieee8021CfmStackTableOID, &Ieee8021CfmStackTableEntry);
    SNMPUnRegisterMib (&Ieee8021CfmVlanTableOID, &Ieee8021CfmVlanTableEntry);
    SNMPUnRegisterMib (&Ieee8021CfmDefaultMdTableOID,
                       &Ieee8021CfmDefaultMdTableEntry);
    SNMPUnRegisterMib (&Ieee8021CfmConfigErrorListTableOID,
                       &Ieee8021CfmConfigErrorListTableEntry);
    SNMPUnRegisterMib (&Ieee8021CfmMaCompTableOID,
                       &Ieee8021CfmMaCompTableEntry);
    SNMPDelSysorEntry (&cfmv2eOID, (const UINT1 *) "cfmv2ext");
}
