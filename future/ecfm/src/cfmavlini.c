/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmavlini.c,v 1.5 2016/03/31 10:50:41 siva Exp $
 *
 * Description: This file contains the Functionality of  
 *              Availability Mesaurement of Control Sub Module.
 *******************************************************************/
#include "cfminc.h"
PRIVATE VOID
     
     
     
     EcfmCcAvlbltySlidingMethod
PROTO ((tEcfmCcMepInfo * pMepInfo, FLT4 f4FrmLossRatio));
PRIVATE VOID        EcfmCcAvlbltyStaticMethod
PROTO ((tEcfmCcMepInfo * pMepInfo, FLT4 f4FrmLossRatio));

/*******************************************************************************
 * Function           : EcfmCcClntAvlbltyInitiator
 *
 * Description        : The routine implements the Availabiity Initiator, 
 *                      it calls up routine to trigger LM Module
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC UINT4
EcfmCcClntAvlbltyInitiator (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcAvlbltyInfo *pAvlbltyInfo = NULL;
    UINT4               u4RetVal = ECFM_SUCCESS;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepInfo);
    pAvlbltyInfo = ECFM_CC_GET_AVLBLTYINFO_FROM_MEP (pMepInfo);

    /* Check for the received event */
    switch (u1Event)

    {
        case ECFM_AVLBLTY_STOP_TRANSACTION:
            if ((EcfmCcClntLmInitiator
                 (pPduSmInfo,
                  ECFM_LM_STOP_TRANSACTION) == ECFM_FAILURE) ||
                (pLmInfo->b1ResultOk == ECFM_FALSE))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntAvlbltyInitiator: Terminating LM failed\r\n");
                u4RetVal = ECFM_FAILURE;

            }
            break;
        case ECFM_AVLBLTY_START_TRANSACTION:
            if (pLmInfo->u1TxLmmStatus == ECFM_TX_STATUS_NOT_READY)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntAvlbltyInitiator: "
                             "LM and Availability cannot be started together\r\n");
                u4RetVal = ECFM_FAILURE;
            }
            /* Set the transmission status as not ready 
             * so that no other transaction can happen
             * before this finishes
             */
            pAvlbltyInfo->u1TxAvlbltyStatus = ECFM_TX_STATUS_NOT_READY;
            EcfmCcAvlbltyResetAlgoInfo (pAvlbltyInfo);

            /* Initialise the LM elements to trigger the LM transactions */
            pLmInfo->b1TxLmByAvlbility = ECFM_TRUE;
            pLmInfo->u4TxLmmDeadline = pAvlbltyInfo->u4TxAvlbltyDeadline;
            pLmInfo->u2TxLmmInterval = pAvlbltyInfo->u2TxAvlbltyInterval;
            pLmInfo->u2TxLmDestMepId = pAvlbltyInfo->u2TxAvlbltyDestMepId;
            pLmInfo->u2TxLmmMessages = ECFM_INIT_VAL;
            pLmInfo->u1TxLmmPriority = pAvlbltyInfo->u1TxAvlbltyPriority;
            pLmInfo->u1LossMeasurementType = ECFM_CC_LM_TYPE_1LM;
            pLmInfo->b1LmmDropEnable = pAvlbltyInfo->b1AvlbltyDropEnable;
            pLmInfo->b1TxLmIsDestMepId = pAvlbltyInfo->b1TxAvlbltyIsDestMepId;
            pLmInfo->b1TxLmByAvlbility = ECFM_TRUE;
            ECFM_MEMCPY (pLmInfo->TxLmmDestMacAddr,
                         pAvlbltyInfo->TxAvlbltyDestMacAddr,
                         ECFM_MAC_ADDR_LENGTH);

            if ((EcfmCcClntLmInitiator
                 (pPduSmInfo,
                  ECFM_LM_START_TRANSACTION) == ECFM_FAILURE) ||
                (pLmInfo->b1ResultOk == ECFM_FALSE))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntAvlbltyInitiator: Initiating LM failed\r\n");
                u4RetVal = ECFM_FAILURE;

            }
            EcfmRedSyncAvlbltyInfo (pMepInfo);
            break;
        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntAvlbltyInitiator: Event Not Supported\r\n");
            u4RetVal = ECFM_FAILURE;
            break;
    }
    if (u4RetVal == ECFM_FAILURE)

    {
        /* Update Result OK to False */
        pAvlbltyInfo->b1ResultOk = ECFM_FALSE;
        pAvlbltyInfo->u1TxAvlbltyStatus = ECFM_TX_STATUS_READY;
    }

    else

    {
        /* Update Result OK to True */
        pAvlbltyInfo->b1ResultOk = ECFM_TRUE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmLmInitXmitLmmPdu
 *
 * Description        : This routine formats and transmits the LMM PDU.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that stores the
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *******************************************************************************/
PUBLIC VOID
EcfmCcMeasureAvlblty (tEcfmCcPduSmInfo * pPduSmInfo, FLT4 f4FrmLossRatio)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcAvlbltyInfo *pAvlbltyInfo = NULL;

    pMepInfo = pPduSmInfo->pMepInfo;
    pAvlbltyInfo = pMepInfo->pAvlbltyInfo;

    if (pAvlbltyInfo != NULL)
    {
        if (pAvlbltyInfo->u1AvlbltyType == ECFM_CC_AVLBLTY_STATIC_METHOD)
        {
            EcfmCcAvlbltyStaticMethod (pMepInfo, f4FrmLossRatio);
        }
        else
        {
            EcfmCcAvlbltySlidingMethod (pMepInfo, f4FrmLossRatio);
        }
    }
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcAvlbltyStaticMethod
 *
 * Description        : This rotuine is used to categorize avaiability 
 *                      of current window based on static window method
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                      f4FrmLossRatio - Ratio of Far end frame loss in percentage
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmCcAvlbltyStaticMethod (tEcfmCcMepInfo * pMepInfo, FLT4 f4FrmLossRatio)
{
    tEcfmCcAvlbltyStaticInfo *pAvlbltyStaticInfo = NULL;
    tEcfmCcAvlbltyInfo *pAvlbltyInfo = NULL;

    pAvlbltyInfo = pMepInfo->pAvlbltyInfo;

    pAvlbltyStaticInfo = &(pAvlbltyInfo->uAvlbltyOperInfo.StaticInfo);

    /* Check the frame loss ratio and count number of available 
     * interval in the current window */
    if (f4FrmLossRatio < pAvlbltyInfo->f4FrameLossThreshold)
    {
        pAvlbltyStaticInfo->u4CurWinAvailableCnt++;
    }

    pAvlbltyStaticInfo->u4CurWinIntervalCnt++;

    /* check for every window and catogorize the window 
     * as available or not */
    if (pAvlbltyStaticInfo->u4CurWinIntervalCnt ==
        pAvlbltyInfo->u4TxAvlbltyWindowSize)
    {
        pAvlbltyStaticInfo->u4WindowCount++;

        /* If Scheduled Down Time is present in the current window
         * Declare the window as available */
        if ((pAvlbltyStaticInfo->u4WindowCount >=
             pAvlbltyStaticInfo->u4SchdldDownInitWindow) &&
            (pAvlbltyStaticInfo->u4WindowCount <=
             pAvlbltyStaticInfo->u4SchdldDownEndWindow))
        {
            pAvlbltyStaticInfo->u4AvailabilityIndicator++;
            pAvlbltyStaticInfo->u4PreAvailabilityIndicator = 1;

        }
        /* If All the intervals in the current window are Available
         * Declare the window as Available */
        else if (pAvlbltyStaticInfo->u4CurWinAvailableCnt ==
                 pAvlbltyInfo->u4TxAvlbltyWindowSize)
        {
            pAvlbltyStaticInfo->u4AvailabilityIndicator++;
            pAvlbltyStaticInfo->u4PreAvailabilityIndicator = 1;
        }
        /* If  All the intervals in the current window are Unavailable
         * Declare the window as Unavailable */
        else if (pAvlbltyStaticInfo->u4CurWinAvailableCnt == 0)
        {
            pAvlbltyStaticInfo->u4PreAvailabilityIndicator = 0;
        }
        /* If All the intervals are neither Available nor Unavailable
         * Declare availability same as the previous window */
        else
        {
            pAvlbltyStaticInfo->u4AvailabilityIndicator =
                pAvlbltyStaticInfo->u4AvailabilityIndicator +
                pAvlbltyStaticInfo->u4PreAvailabilityIndicator;
        }

        pAvlbltyStaticInfo->u4CurWinAvailableCnt = 0;
        pAvlbltyStaticInfo->u4CurWinIntervalCnt = 0;
    }
    EcfmRedSyncAvlbltyInfo (pMepInfo);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcAvlbltySlidingMethod
 *
 * Description        : This rotuine is used to categorize avaiability 
 *                      of current interval based on sliding window method
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                      f4FrmLossRatio - Ratio of Far end frame loss in percentage
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmCcAvlbltySlidingMethod (tEcfmCcMepInfo * pMepInfo, FLT4 f4FrmLossRatio)
{
    tEcfmCcAvlbltySlideInfo *pAvlbltySlideInfo = NULL;
    tEcfmCcAvlbltyInfo *pAvlbltyInfo = NULL;

    pAvlbltyInfo = pMepInfo->pAvlbltyInfo;

    pAvlbltySlideInfo = &(pAvlbltyInfo->uAvlbltyOperInfo.SlideInfo);

    pAvlbltySlideInfo->u4CurIntCount++;
    /* If scheduled Down time is presnt in the current interval
     * Increment Availability count*/
    if ((pAvlbltySlideInfo->u4CurIntCount >=
         pAvlbltySlideInfo->u4SchdldDownInitIntCnt) &&
        (pAvlbltySlideInfo->u4CurIntCount <=
         pAvlbltySlideInfo->u4SchdldDownEndIntCnt))
    {
        pAvlbltySlideInfo->b1PreIntAvlbltyState = ECFM_TRUE;
        pAvlbltySlideInfo->u4WindowContinuity = 1;
        pAvlbltySlideInfo->u4AvlbltyCount++;
        EcfmRedSyncAvlbltyInfo (pMepInfo);
        return;
    }

    /* Check the Current interval Availability Status */
    if (f4FrmLossRatio < pAvlbltyInfo->f4FrameLossThreshold)
    {
        pAvlbltySlideInfo->b1IntAvlbltyState = ECFM_TRUE;
    }
    else
    {
        pAvlbltySlideInfo->b1IntAvlbltyState = ECFM_FALSE;
    }

    /* Compare with previous interval, if both are same 
     * increase continuity else restart count */
    if (pAvlbltySlideInfo->b1IntAvlbltyState ==
        pAvlbltySlideInfo->b1PreIntAvlbltyState)
    {
        pAvlbltySlideInfo->u4WindowContinuity++;
    }
    else
    {
        pAvlbltySlideInfo->u4WindowContinuity = 1;
    }

    /* If Network is in Available State, Increase
     * Available Interval Count */
    if (pAvlbltySlideInfo->b1AvlbltyState == ECFM_TRUE)
    {
        pAvlbltySlideInfo->u4AvlbltyCount++;
    }

    /* If Continuously N (Window Size) intervals are 
     * either Available or Unavailable , check for transition */
    if (pAvlbltySlideInfo->u4WindowContinuity ==
        pAvlbltyInfo->u4TxAvlbltyWindowSize)
    {
        /* Window Size intervals are continuosly Unavailable
         * and Current state shows Available, Do transition */
        if ((pAvlbltySlideInfo->b1IntAvlbltyState == ECFM_FALSE) &&
            (pAvlbltySlideInfo->b1AvlbltyState == ECFM_TRUE))
        {
            pAvlbltySlideInfo->b1AvlbltyState = ECFM_FALSE;
            pAvlbltySlideInfo->u4AvlbltyCount =
                pAvlbltySlideInfo->u4AvlbltyCount -
                pAvlbltyInfo->u4TxAvlbltyWindowSize;
        }
        /* Window Size intervals are continuosly Available
         * and Current state shows Unavailable, Do transition */
        else if ((pAvlbltySlideInfo->b1IntAvlbltyState == ECFM_TRUE) &&
                 (pAvlbltySlideInfo->b1AvlbltyState == ECFM_FALSE))
        {
            pAvlbltySlideInfo->b1AvlbltyState = ECFM_TRUE;
            pAvlbltySlideInfo->u4AvlbltyCount =
                pAvlbltySlideInfo->u4AvlbltyCount +
                pAvlbltyInfo->u4TxAvlbltyWindowSize;
        }
        pAvlbltySlideInfo->u4WindowContinuity--;
    }

    pAvlbltySlideInfo->b1PreIntAvlbltyState =
        pAvlbltySlideInfo->b1IntAvlbltyState;
    EcfmRedSyncAvlbltyInfo (pMepInfo);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcAvlbltyStopTransaction
 *
 * Description        : This rotuine is used to categorize avaiability 
 *                      of current interval based on sliding window method
 * 
 * Input              : pAvlbltyInfo - Pointer to the MEP Availability structure  
 *                      f4FrmLossRatio - Ratio of Far end frame loss in percentage
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcAvlbltyStopTransaction (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tEcfmCcAvlbltyInfo *pAvlbltyInfo = NULL;
    tEcfmCcAvlbltySlideInfo *pAvlbltySlideInfo = NULL;
    tEcfmCcAvlbltyStaticInfo *pAvlbltyStaticInfo = NULL;
    UINT4               u4TotalWindows = ECFM_INIT_VAL;
    UINT4               u4TotalIntervals = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    FLT4                f4Avlbltynumerator = ECFM_INIT_VAL;
    FLT4                f4Avlbltydenominator = ECFM_INIT_VAL;

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepInfo);
    pAvlbltyInfo = ECFM_CC_GET_AVLBLTYINFO_FROM_MEP (pMepInfo);

    if (pAvlbltyInfo != NULL)
    {
        ECFM_GET_AVLBLTY_INTERVAL (pAvlbltyInfo->u2TxAvlbltyInterval,
                                   u4Interval);

        if (ECFM_INIT_VAL == u4Interval)
        {
            return;
        }

        u4TotalIntervals = ((pAvlbltyInfo->u4TxAvlbltyDeadline *
                             ECFM_NUM_OF_MSEC_IN_A_SEC) / u4Interval);

        u4TotalWindows = (u4TotalIntervals /
                          pAvlbltyInfo->u4TxAvlbltyWindowSize);

        /* Perform Availability Percentage Calculation 
         * in Static Window Method */
        if (pAvlbltyInfo->u1AvlbltyType == ECFM_CC_AVLBLTY_STATIC_METHOD)
        {
            pAvlbltyStaticInfo = &(pAvlbltyInfo->uAvlbltyOperInfo.StaticInfo);
            f4Avlbltynumerator = (FLT4)
                (pAvlbltyStaticInfo->u4AvailabilityIndicator -
                 pAvlbltyStaticInfo->u4SchdldDownWindowCnt);
            f4Avlbltydenominator = (FLT4) (u4TotalWindows -
                                           pAvlbltyStaticInfo->
                                           u4SchdldDownWindowCnt);
        }
        /* Perform Availability Percentage Calculation 
         * in Sliding Window Method */
        else
        {
            pAvlbltySlideInfo = &(pAvlbltyInfo->uAvlbltyOperInfo.SlideInfo);
            f4Avlbltynumerator = (FLT4)
                (pAvlbltySlideInfo->u4AvlbltyCount -
                 pAvlbltySlideInfo->u4SchdldDownIntTotal);
            f4Avlbltydenominator = (FLT4)
                (u4TotalIntervals - pAvlbltySlideInfo->u4SchdldDownIntTotal);
        }

        /* Calculate Availability Percentage */
        pAvlbltyInfo->f4AvlbltyPercentage = (FLT4)
            ((f4Avlbltynumerator / f4Avlbltydenominator) * ECFM_FLT_VAL_100);

#if defined (Y1564_WANTED) && defined (NPAPI_WANTED)
        if (((UINT4) (f4Avlbltynumerator / f4Avlbltydenominator)) != 1)
        {
            pAvlbltyInfo->u4UnAvailCnt = (UINT4) (u4TotalIntervals -
                 (f4Avlbltynumerator * pAvlbltyInfo->u4TxAvlbltyWindowSize));
        }
        else if (((UINT4) (f4Avlbltynumerator / f4Avlbltydenominator)) == 1)
        {
            pAvlbltyInfo->u4UnAvailCnt = 0;
        }
#endif
 
        pAvlbltyInfo->u1TxAvlbltyStatus = ECFM_TX_STATUS_READY;
        pLmInfo->b1TxLmByAvlbility = ECFM_FALSE;

    }
    EcfmRedSyncAvlbltyInfo (pMepInfo);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcAvlbltyResetAlgoInfo
 *
 * Description        : Reset the Static and Sliding Info to initiate new 
 *                      Avaiablity Measurement
 * 
 * Input              : pAvlbltyInfo - Pointer to the MEP Availability structure  
 *                       
 * Output(s)          : None
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcAvlbltyResetAlgoInfo (tEcfmCcAvlbltyInfo * pAvlbltyInfo)
{
    tEcfmCcAvlbltyStaticInfo *pStaticInfo = NULL;
    tEcfmCcAvlbltySlideInfo *pSlidingInfo = NULL;
    UINT4               u4Interval = ECFM_INIT_VAL;

    ECFM_GET_AVLBLTY_INTERVAL (pAvlbltyInfo->u2TxAvlbltyInterval, u4Interval);

    if (pAvlbltyInfo->bTxAvlbltyModestType == ECFM_TRUE)
    {
        pAvlbltyInfo->f4FrameLossThreshold =
            pAvlbltyInfo->f4FrameLossUpperThreshold;
    }
    else
    {
        pAvlbltyInfo->f4FrameLossThreshold =
            pAvlbltyInfo->f4FrameLossLowerThreshold;
    }

    if (pAvlbltyInfo->u1AvlbltyType == ECFM_CC_AVLBLTY_STATIC_METHOD)
    {
        pStaticInfo = &(pAvlbltyInfo->uAvlbltyOperInfo.StaticInfo);
        ECFM_MEMSET (pStaticInfo, 0, sizeof (tEcfmCcAvlbltyStaticInfo));
        pStaticInfo->u4PreAvailabilityIndicator = ECFM_VAL_1;
        if (ECFM_INIT_VAL == u4Interval)
        {
            return;
        }
        pStaticInfo->u4SchdldDownInitWindow =
            (pAvlbltyInfo->u4TxAvlbltySchldDownInitTime / u4Interval) /
            pAvlbltyInfo->u4TxAvlbltyWindowSize;
        pStaticInfo->u4SchdldDownEndWindow =
            (pAvlbltyInfo->u4TxAvlbltySchldDownEndTime / u4Interval) /
            pAvlbltyInfo->u4TxAvlbltyWindowSize;
        pStaticInfo->u4SchdldDownWindowCnt =
            pStaticInfo->u4SchdldDownEndWindow -
            pStaticInfo->u4SchdldDownInitWindow;
    }
    else
    {
        pSlidingInfo = &(pAvlbltyInfo->uAvlbltyOperInfo.SlideInfo);
        ECFM_MEMSET (pSlidingInfo, 0, sizeof (tEcfmCcAvlbltySlideInfo));
        if (ECFM_INIT_VAL == u4Interval)
        {
            return;
        }
        pSlidingInfo->u4SchdldDownInitIntCnt =
            (pAvlbltyInfo->u4TxAvlbltySchldDownInitTime / u4Interval);
        pSlidingInfo->u4SchdldDownEndIntCnt =
            (pAvlbltyInfo->u4TxAvlbltySchldDownEndTime / u4Interval);
        pSlidingInfo->u4SchdldDownIntTotal =
            pSlidingInfo->u4SchdldDownEndIntCnt -
            pSlidingInfo->u4SchdldDownInitIntCnt;
        pSlidingInfo->b1PreIntAvlbltyState = ECFM_TRUE;
        pSlidingInfo->b1AvlbltyState = ECFM_TRUE;
    }
    return;
}
