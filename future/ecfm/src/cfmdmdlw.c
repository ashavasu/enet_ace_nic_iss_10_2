/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfmdmdlw.c,v 1.29 2015/12/29 12:01:16 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"

#include "cfminc.h"
#include  "fscfmmcli.h"

/* LOW LEVEL Routines for Table : Ieee8021CfmDefaultMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CfmDefaultMdTable (UINT4
                                                   u4Ieee8021CfmDefaultMdComponentId,
                                                   INT4
                                                   i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                                   UINT4
                                                   u4Ieee8021CfmDefaultMdPrimarySelector)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {

        /* first validate index range */
        if ((u4Ieee8021CfmDefaultMdPrimarySelector < ECFM_ISID_MIN) ||
            (u4Ieee8021CfmDefaultMdPrimarySelector > ECFM_ISID_MAX))
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Isid Index range for PrimarySelector Default Domain Table\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* first validate index range */
        if ((u4Ieee8021CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (u4Ieee8021CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid VlanId Index range for PrimarySelector Default Domain Table\n");
            return SNMP_FAILURE;
        }

    }

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);

    /* Check if DefaultMd entry exists */
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CfmDefaultMdTable (UINT4
                                           *pu4Ieee8021CfmDefaultMdComponentId,
                                           INT4
                                           *pi4Ieee8021CfmDefaultMdPrimarySelectorType,
                                           UINT4
                                           *pu4Ieee8021CfmDefaultMdPrimarySelector)
{
    tEcfmCcDefaultMdTableInfo *pDefMdInfo = NULL;

    *pu4Ieee8021CfmDefaultMdComponentId = ECFM_CC_CURR_CONTEXT_ID ();
    ECFM_CONVERT_CTXT_ID_TO_COMP_ID (*pu4Ieee8021CfmDefaultMdComponentId);

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    pDefMdInfo = (tEcfmCcDefaultMdTableInfo *) RBTreeGetFirst
        (ECFM_CC_DEF_MD_TABLE);
    if (pDefMdInfo != NULL)
    {
        if (pDefMdInfo->u4PrimaryVidIsid < ECFM_INTERNAL_ISID_MIN)
        {
            *pu4Ieee8021CfmDefaultMdPrimarySelector =
                pDefMdInfo->u4PrimaryVidIsid;
            *pi4Ieee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_VLAN;
        }
        else
        {
            *pu4Ieee8021CfmDefaultMdPrimarySelector =
                ECFM_ISID_INTERNAL_TO_ISID (pDefMdInfo->u4PrimaryVidIsid);
            *pi4Ieee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_ISID;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                nextIeee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                nextIeee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
                nextIeee8021CfmDefaultMdPrimarySelector
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CfmDefaultMdTable (UINT4
                                          u4Ieee8021CfmDefaultMdComponentId,
                                          UINT4
                                          *pu4NextIeee8021CfmDefaultMdComponentId,
                                          INT4
                                          i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                          INT4
                                          *pi4NextIeee8021CfmDefaultMdPrimarySelectorType,
                                          UINT4
                                          u4Ieee8021CfmDefaultMdPrimarySelector,
                                          UINT4
                                          *pu4NextIeee8021CfmDefaultMdPrimarySelector)
{
    tEcfmCcDefaultMdTableInfo DefMdInfo;
    tEcfmCcDefaultMdTableInfo *pDefMdInfo = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    *pu4NextIeee8021CfmDefaultMdComponentId = ECFM_CC_CURR_CONTEXT_ID ();
    ECFM_CONVERT_CTXT_ID_TO_COMP_ID (*pu4NextIeee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Check if it is the last entry of DefaultMdTable */
    if (i4CfmDefaultMdPrimarySelector >= ECFM_INTERNAL_ISID_MAX)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: No Next Default Domain Entry\n");
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&DefMdInfo, ECFM_INIT_VAL, ECFM_CC_DEF_MD_INFO_SIZE);

    DefMdInfo.u4PrimaryVidIsid = i4CfmDefaultMdPrimarySelector;

    pDefMdInfo = (tEcfmCcDefaultMdTableInfo *) RBTreeGetNext
        (ECFM_CC_DEF_MD_TABLE, (tRBElem *) (&DefMdInfo), NULL);
    /* Set the next index to PrimaryVid from DefaultMd Table */
    if (pDefMdInfo != NULL)
    {
        if (pDefMdInfo->u4PrimaryVidIsid < ECFM_INTERNAL_ISID_MIN)
        {
            *pu4NextIeee8021CfmDefaultMdPrimarySelector =
                pDefMdInfo->u4PrimaryVidIsid;
            *pi4NextIeee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_VLAN;
        }
        else
        {
            *pu4NextIeee8021CfmDefaultMdPrimarySelector =
                ECFM_ISID_INTERNAL_TO_ISID (pDefMdInfo->u4PrimaryVidIsid);
            *pi4NextIeee8021CfmDefaultMdPrimarySelectorType =
                ECFM_SERVICE_SELECTION_ISID;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdStatus
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdStatus (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                  INT4
                                  i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                  UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                  INT4 *pi4RetValIeee8021CfmDefaultMdStatus)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }

    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the DefaultMdStatus from corresponding DefaultMd entry */
    *pi4RetValIeee8021CfmDefaultMdStatus =
        ECFM_CONVERT_TO_SNMP_BOOL (pDefaultMdNode->b1Status);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdLevel
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdLevel (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                 INT4 i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                 UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                 INT4 *pi4RetValIeee8021CfmDefaultMdLevel)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the MdLevel from corresponding DefaultMd entry */
    *pi4RetValIeee8021CfmDefaultMdLevel = (INT4) (pDefaultMdNode->i1MdLevel);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdMhfCreation
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdMhfCreation (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                       INT4
                                       i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                       UINT4
                                       u4Ieee8021CfmDefaultMdPrimarySelector,
                                       INT4
                                       *pi4RetValIeee8021CfmDefaultMdMhfCreation)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the MHfCreation criteria from corresponding DefaultMd entry */
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021CfmDefaultMdMhfCreation =
        (INT4) (pDefaultMdNode->u1MhfCreation);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CfmDefaultMdIdPermission
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                retValIeee8021CfmDefaultMdIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CfmDefaultMdIdPermission (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                        INT4
                                        i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                        UINT4
                                        u4Ieee8021CfmDefaultMdPrimarySelector,
                                        INT4
                                        *pi4RetValIeee8021CfmDefaultMdIdPermission)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the SenderIdPermission from corresponding DefaultMd entry */
    *pi4RetValIeee8021CfmDefaultMdIdPermission =
        (INT4) (pDefaultMdNode->u1IdPermission);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CfmDefaultMdLevel
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                setValIeee8021CfmDefaultMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmDefaultMdLevel (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                 INT4 i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                 UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                 INT4 i4SetValIeee8021CfmDefaultMdLevel)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);

    if (pDefaultMdNode == NULL)
    {
        /* No Default entry is found */
        /* case 1: user wants to set the default value, don't add the node */
        /* case 2: user wants to set something other than default value, 
         *         add a default MD node*/
        if (i4SetValIeee8021CfmDefaultMdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL)
        {
            return SNMP_SUCCESS;
        }
        else
        {
                if ((pDefaultMdNode =
                 EcfmSnmpLwSetDefaultMdNode (i4CfmDefaultMdPrimarySelector)) ==
                NULL)
                { 
                        return SNMP_FAILURE;
                }
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Check if it is already same */
    if (pDefaultMdNode->i1MdLevel == (INT1) i4SetValIeee8021CfmDefaultMdLevel)

    {
        return SNMP_SUCCESS;
    }
    /* Set the MdLevel to corresponding DefaultMd entry */
    pDefaultMdNode->i1MdLevel = (INT1) i4SetValIeee8021CfmDefaultMdLevel;

    /* Get if there is any MA with vlanId u2Vid and level  u1Level */
    pMaNode =
        EcfmCcUtilGetMaAssocWithVid (i4CfmDefaultMdPrimarySelector,
                                     i4SetValIeee8021CfmDefaultMdLevel);
    if (pMaNode != NULL)

    {

        /* If UP MEP associated with this MA exists then only update 
         * default MdStatus */
        if (EcfmIsMepAssocWithMa
            (pMaNode->u4MdIndex, pMaNode->u4MaIndex, -1,
             ECFM_MP_DIR_UP) == ECFM_TRUE)

        {
            EcfmSnmpLwUpdateDefaultMdStatus (i4SetValIeee8021CfmDefaultMdLevel,
                                             i4CfmDefaultMdPrimarySelector,
                                             ECFM_FALSE);
        }
        else
        {
            /*No UP MEP exists on MA but if default-domain entry exists and 
             *md status is FALSE then revert it to TRUE */
            EcfmSnmpLwUpdateDefaultMdStatus ((UINT1) i4SetValIeee8021CfmDefaultMdLevel,
                                             (UINT4) i4CfmDefaultMdPrimarySelector,
                                             ECFM_TRUE);
         }

    }

    else

    {
        EcfmSnmpLwUpdateDefaultMdStatus (i4SetValIeee8021CfmDefaultMdLevel,
                                         i4CfmDefaultMdPrimarySelector,
                                         ECFM_TRUE);
    }

    /* Evaluate and create if MIPs can be created for all the ports */
    EcfmCcUtilEvaluateAndCreateMip (-1, i4CfmDefaultMdPrimarySelector,
                                    ECFM_TRUE);
    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDefaultMdLevel, u4SeqNum,
                              FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (),
                          i4CfmDefaultMdPrimarySelector,
                          i4SetValIeee8021CfmDefaultMdLevel));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* Check if all the values of the Node are default, if so then delete the
     * Node */
    EcfmSnmpLwProcessDefaultMdNode (i4CfmDefaultMdPrimarySelector);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmDefaultMdMhfCreation
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                setValIeee8021CfmDefaultMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmDefaultMdMhfCreation (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                       INT4
                                       i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                       UINT4
                                       u4Ieee8021CfmDefaultMdPrimarySelector,
                                       INT4
                                       i4SetValIeee8021CfmDefaultMdMhfCreation)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        /* No Default entry is found */
        /* case 1: user wants to set the default value, don't add the node */
        /* case 2: user wants to set something other than default value, 
         *         add a default MD node*/
        if (i4SetValIeee8021CfmDefaultMdMhfCreation == ECFM_MHF_CRITERIA_DEFER)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if ((pDefaultMdNode =
                 EcfmSnmpLwSetDefaultMdNode (i4CfmDefaultMdPrimarySelector)) ==
                NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Check if it is already same */
    if (pDefaultMdNode->u1MhfCreation ==
        (UINT1) i4SetValIeee8021CfmDefaultMdMhfCreation)
    {
        return SNMP_SUCCESS;
    }
    /* Set the Mhf Creation to corresponding DefaultMd entry */
    pDefaultMdNode->u1MhfCreation =
        (UINT1) i4SetValIeee8021CfmDefaultMdMhfCreation;

    /* Evaluate and create if Mips can be created for all the ports */
    EcfmCcUtilEvaluateAndCreateMip (-1, i4CfmDefaultMdPrimarySelector,
                                    ECFM_TRUE);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pDefaultMdNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDefaultMdMhfCreation,
                              u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (),
                          i4CfmDefaultMdPrimarySelector,
                          i4SetValIeee8021CfmDefaultMdMhfCreation));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* Check if all the values of the Node are default, if so then delete the
     * Node */
    EcfmSnmpLwProcessDefaultMdNode (i4CfmDefaultMdPrimarySelector);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CfmDefaultMdIdPermission
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                setValIeee8021CfmDefaultMdIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CfmDefaultMdIdPermission (UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                        INT4
                                        i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                        UINT4
                                        u4Ieee8021CfmDefaultMdPrimarySelector,
                                        INT4
                                        i4SetValIeee8021CfmDefaultMdIdPermission)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode =
        EcfmCcSnmpLwGetDefaultMdEntry (i4CfmDefaultMdPrimarySelector);
    if (pDefaultMdNode == NULL)
    {
        /* No Default entry is found */
        /* case 1: user wants to set the default value, don't add the node */
        /* case 2: user wants to set something other than default value, 
         *         add a default MD node*/
        if (i4SetValIeee8021CfmDefaultMdIdPermission == ECFM_SENDER_ID_DEFER)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if ((pDefaultMdNode =
                 EcfmSnmpLwSetDefaultMdNode (i4CfmDefaultMdPrimarySelector)) ==
                NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* Check if it is already same */
    if (pDefaultMdNode->u1IdPermission ==
        (UINT1) i4SetValIeee8021CfmDefaultMdIdPermission)
    {
        return SNMP_SUCCESS;
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Set the SenderId Permission to corresponding DefaultMd entry */
    pDefaultMdNode->u1IdPermission =
        (UINT1) i4SetValIeee8021CfmDefaultMdIdPermission;

    /* Update Default MD at LBLT also */
    EcfmLbLtUpdateDefaultMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pDefaultMdNode);

    /* Sending Trigger to MSR */

    if (!(ECFM_IS_MEP_ISID_AWARE (pDefaultMdNode->u4PrimaryVidIsid)))
    {
        u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDefaultMdIdPermission,
                              u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                              ECFM_TABLE_INDICES_COUNT_TWO, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i",
                          ECFM_CC_CURR_CONTEXT_ID (),
                          i4CfmDefaultMdPrimarySelector,
                          i4SetValIeee8021CfmDefaultMdIdPermission));
        ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    }
    /* Check if all the values of the Node are default, if so then delete the
     * Node */
    EcfmSnmpLwProcessDefaultMdNode (i4CfmDefaultMdPrimarySelector);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmDefaultMdLevel
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                testValIeee8021CfmDefaultMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmDefaultMdLevel (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021CfmDefaultMdComponentId,
                                    INT4
                                    i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                    UINT4 u4Ieee8021CfmDefaultMdPrimarySelector,
                                    INT4 i4TestValIeee8021CfmDefaultMdLevel)
{
    INT4                i4LocalIsid = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;
    UINT4 u4EntryCount=0;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        /* Validate index range */
        if ((i4CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (i4CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        i4LocalIsid =
            ECFM_ISID_INTERNAL_TO_ISID (i4CfmDefaultMdPrimarySelector);
        /* Validate index range */
        if ((i4LocalIsid < ECFM_ISID_MIN) || (i4LocalIsid > ECFM_ISID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
     /* Check if a new default-domain is being created */
    if(EcfmSnmpLwGetDefMdEntry((UINT4)i4CfmDefaultMdPrimarySelector) == NULL) 
    {
        if (i4TestValIeee8021CfmDefaultMdLevel == -1)
        {
            CLI_SET_ERR(CLI_ECFM_MAX_DEF_MD_NOT_EXIST_ERR);
            return SNMP_FAILURE;
        }
        else 
        {
            RBTreeCount (ECFM_CC_DEF_MD_TABLE,&u4EntryCount);
            if (u4EntryCount >= ECFM_MAX_DEF_MD)
            {
                CLI_SET_ERR(CLI_ECFM_MAX_DEF_MD_CREATION_REACHED);
                return SNMP_FAILURE;
            }
        }
    }

    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Now validating the MdLevel, that is to be Test */
    if ((i4TestValIeee8021CfmDefaultMdLevel < ECFM_DEF_MD_LEVEL_DEF_VAL) ||
        (i4TestValIeee8021CfmDefaultMdLevel > ECFM_MD_LEVEL_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MDLevel for Default Domain \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmDefaultMdMhfCreation
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                testValIeee8021CfmDefaultMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmDefaultMdMhfCreation (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4Ieee8021CfmDefaultMdComponentId,
                                          INT4
                                          i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                          UINT4
                                          u4Ieee8021CfmDefaultMdPrimarySelector,
                                          INT4
                                          i4TestValIeee8021CfmDefaultMdMhfCreation)
{
    INT4                i4LocalIsid = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        /* Validate index range */
        if ((i4CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (i4CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        i4LocalIsid =
            ECFM_ISID_INTERNAL_TO_ISID (i4CfmDefaultMdPrimarySelector);
        /* Validate index range */
        if ((i4LocalIsid < ECFM_ISID_MIN) || (i4LocalIsid > ECFM_ISID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Now validating the MHfCreation, that is to be Test */
    if ((i4TestValIeee8021CfmDefaultMdMhfCreation < ECFM_MHF_CRITERIA_NONE) ||
        (i4TestValIeee8021CfmDefaultMdMhfCreation > ECFM_MHF_CRITERIA_DEFER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MHFCreation Value for"
                     "Default Domain entry\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CfmDefaultMdIdPermission
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector

                The Object 
                testValIeee8021CfmDefaultMdIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CfmDefaultMdIdPermission (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021CfmDefaultMdComponentId,
                                           INT4
                                           i4Ieee8021CfmDefaultMdPrimarySelectorType,
                                           UINT4
                                           u4Ieee8021CfmDefaultMdPrimarySelector,
                                           INT4
                                           i4TestValIeee8021CfmDefaultMdIdPermission)
{
    INT4                i4LocalIsid = ECFM_INIT_VAL;
    INT4                i4CfmDefaultMdPrimarySelector =
        u4Ieee8021CfmDefaultMdPrimarySelector;

    UNUSED_PARAM (u4Ieee8021CfmDefaultMdComponentId);

    if (i4Ieee8021CfmDefaultMdPrimarySelectorType ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        /* convert the isid received into intrenal isid value */
        i4CfmDefaultMdPrimarySelector =
            ECFM_ISID_TO_ISID_INTERNAL (u4Ieee8021CfmDefaultMdPrimarySelector);

    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return SNMP_FAILURE;
    }
    if (!(ECFM_IS_MEP_ISID_AWARE (i4CfmDefaultMdPrimarySelector)))
    {
        /* Validate index range */
        if ((i4CfmDefaultMdPrimarySelector < ECFM_VLANID_MIN) ||
            (i4CfmDefaultMdPrimarySelector > ECFM_VLANID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        i4LocalIsid =
            ECFM_ISID_INTERNAL_TO_ISID (i4CfmDefaultMdPrimarySelector);
        /* Validate index range */
        if ((i4LocalIsid < ECFM_ISID_MIN) || (i4LocalIsid > ECFM_ISID_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                         "\tSNMP: Invalid Default Domain PrimarySelector Range\n");
            return SNMP_FAILURE;
        }
    }
    /* DefaultMd entry corresponding to PrimaryVid exists */
    /* Now validating the SenderId Permission, that is to be Test */
    if ((i4TestValIeee8021CfmDefaultMdIdPermission < ECFM_SENDER_ID_NONE) ||
        (i4TestValIeee8021CfmDefaultMdIdPermission > ECFM_SENDER_ID_DEFER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value for SenderID Permission"
                     " for Default Domain\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CfmDefaultMdTable
 Input       :  The Indices
                Ieee8021CfmDefaultMdComponentId
                Ieee8021CfmDefaultMdPrimarySelectorType
                Ieee8021CfmDefaultMdPrimarySelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CfmDefaultMdTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdDefLevel
 Input       :  The Indices

                The Object 
                retValDot1agCfmDefaultMdDefLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdDefLevel (INT4 *pi4RetValDot1agCfmDefaultMdDefLevel)
{
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pi4RetValDot1agCfmDefaultMdDefLevel = ECFM_INIT_VAL;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_SUCCESS;
    }
    /* Set the required value */
    *pi4RetValDot1agCfmDefaultMdDefLevel =
        (INT4) (ECFM_CC_DEF_MD_DEFAULT_LEVEL);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdDefMhfCreation
 Input       :  The Indices

                The Object 
                retValDot1agCfmDefaultMdDefMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdDefMhfCreation (INT4
                                        *pi4RetValDot1agCfmDefaultMdDefMhfCreation)
{
/* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pi4RetValDot1agCfmDefaultMdDefMhfCreation = ECFM_MHF_CRITERIA_NONE;
        ECFM_CC_TRC (ECFM_MGMT_TRC, "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_SUCCESS;
    }
    /* Set the required value */
    *pi4RetValDot1agCfmDefaultMdDefMhfCreation = (INT4)
        (ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdDefIdPermission
 Input       :  The Indices

                The Object 
                retValDot1agCfmDefaultMdDefIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdDefIdPermission (INT4
                                         *pi4RetValDot1agCfmDefaultMdDefIdPermission)
{
/* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pi4RetValDot1agCfmDefaultMdDefIdPermission = ECFM_SENDER_ID_NONE;
        ECFM_CC_TRC (ECFM_MGMT_TRC, "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_SUCCESS;
    }
    /* Set the required value */
    *pi4RetValDot1agCfmDefaultMdDefIdPermission = (INT4)
        (ECFM_CC_DEF_MD_DEFAULT_SENDER_ID_PERMISSION);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmDefaultMdDefLevel
 Input       :  The Indices

                The Object 
                setValDot1agCfmDefaultMdDefLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmDefaultMdDefLevel (INT4 i4SetValDot1agCfmDefaultMdDefLevel)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    UINT2               u2VlanCounter = ECFM_INIT_VAL;
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC, "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_SUCCESS;
    }
    if (ECFM_CC_DEF_MD_DEFAULT_LEVEL ==
        (UINT1) i4SetValDot1agCfmDefaultMdDefLevel)

    {
        return SNMP_SUCCESS;
    }

    /* Set default Md Level */
    ECFM_CC_DEF_MD_DEFAULT_LEVEL = (UINT1) i4SetValDot1agCfmDefaultMdDefLevel;

    /* Delete all previously implicitly configured MIPs */
    EcfmDeleteImplicitlyCreatedMips (-1, -1);

    /* Update Default MD Status for each Default MD entry having MD LEVEL as 
     * -1 */
    for (u2VlanCounter = ECFM_VLANID_MIN; u2VlanCounter <= ECFM_VLANID_MAX;
         u2VlanCounter++)

    {
        /* Get the DefaultMd entry corresponding to Vid */
        pDefaultMdNode = EcfmCcSnmpLwGetDefaultMdEntry (u2VlanCounter);
        /* Default MD-Level can only be assumed in the following cases
         * 1. We dont have a entry in the default MD table, it means all the
         * values are default
         * 2. We do have a entry in the default MD table with Default MD level.*/
        if ((pDefaultMdNode == NULL) ||
            (pDefaultMdNode->i1MdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL))

        {

            /* Get if there is any MA with vlanId u2Vid and level  u1Level */
            pMaNode =
                EcfmCcUtilGetMaAssocWithVid (u2VlanCounter,
                                             i4SetValDot1agCfmDefaultMdDefLevel);

            /* If UP MEP associated with this MA exists then update default 
             * MdStatus */
            if ((pMaNode != NULL) &&
                (EcfmIsMepAssocWithMa
                 (pMaNode->u4MdIndex, pMaNode->u4MaIndex, -1,
                  ECFM_MP_DIR_UP) == ECFM_TRUE))

            {
                if (pDefaultMdNode == NULL)
                {
                    /* Add a new default MD node */
                    if ((pDefaultMdNode =
                         EcfmSnmpLwSetDefaultMdNode (u2VlanCounter)) == NULL)
                    {
                        continue;
                    }
                }
                pDefaultMdNode->b1Status = ECFM_FALSE;
                /* Update Default MD at LBLT also */
                EcfmLbLtUpdateDefaultMdEntry (ECFM_CC_CURR_CONTEXT_ID (),
                                              pDefaultMdNode);
            }
            else
            {
                if (pDefaultMdNode != NULL)
                {
                    pDefaultMdNode->b1Status = ECFM_TRUE;
                }
            }
            EcfmSnmpLwProcessDefaultMdNode (u2VlanCounter);
            /* Evaluate and create if Mips can be created for the VLAN for which
             * default MD Level is -1 on all the ports */
            EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanCounter, ECFM_FALSE);
        }
    }
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDefaultMdDefLevel, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValDot1agCfmDefaultMdDefLevel));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmDefaultMdDefMhfCreation
 Input       :  The Indices

                The Object 
                setValDot1agCfmDefaultMdDefMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmDefaultMdDefMhfCreation (INT4
                                        i4SetValDot1agCfmDefaultMdDefMhfCreation)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    UINT2               u2VlanCounter = ECFM_INIT_VAL;
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC, "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_SUCCESS;
    }
    if (ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION ==
        (UINT1) i4SetValDot1agCfmDefaultMdDefMhfCreation)

    {
        return SNMP_SUCCESS;
    }

    /* Set default Md MhfCreation */
    ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION =
        (UINT1) i4SetValDot1agCfmDefaultMdDefMhfCreation;

    /* Delete all previously implicitly configured MIPs */
    EcfmDeleteImplicitlyCreatedMips (-1, -1);

    /* Evaluate Implicit MIP creation for each Default MD entry having MD LEVEL as 
     * -1 */
    for (u2VlanCounter = ECFM_VLANID_MIN; u2VlanCounter <= ECFM_VLANID_MAX;
         u2VlanCounter++)

    {
        /* Get the DefaultMd entry corresponding to Vid */
        ECFM_CC_GET_DEFAULT_MD_ENTRY (u2VlanCounter, pDefaultMdNode);
        if (pDefaultMdNode == NULL)
        {
            continue;
        }
        /* Check if it is the required entry */
        if (pDefaultMdNode->i1MdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL)
        {
            /* Evaluate and create if Mips can be created for the VLAN for which
             * default MD Level is -1 on all the ports */
            EcfmCcUtilEvaluateAndCreateMip (-1, u2VlanCounter, ECFM_FALSE);
        }
    }
    /* Sending Trigger to MSR */
    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDefaultMdDefMhfCreation,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValDot1agCfmDefaultMdDefMhfCreation));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmDefaultMdDefIdPermission
 Input       :  The Indices

                The Object 
                setValDot1agCfmDefaultMdDefIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmDefaultMdDefIdPermission (INT4
                                         i4SetValDot1agCfmDefaultMdDefIdPermission)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;

    ECFM_CC_DEF_MD_DEFAULT_SENDER_ID_PERMISSION =
        (UINT1) i4SetValDot1agCfmDefaultMdDefIdPermission;

    /* Update Default Def ID Permission at LBLT also */
    EcfmLbLtUpdateDefDefaultIdPerm (ECFM_CC_CURR_CONTEXT_ID (),
                                    i4SetValDot1agCfmDefaultMdDefIdPermission);
    /* Sending Trigger to MSR */

    u4CurrContextId = ECFM_CC_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmDefaultMdDefIdPermission,
                          u4SeqNum, FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ONE, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", ECFM_CC_CURR_CONTEXT_ID (),
                      i4SetValDot1agCfmDefaultMdDefIdPermission));
    ECFM_CC_SELECT_CONTEXT (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmDefaultMdDefLevel
 Input       :  The Indices

                The Object 
                testValDot1agCfmDefaultMdDefLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmDefaultMdDefLevel (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValDot1agCfmDefaultMdDefLevel)
{
    /* Check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Value that needs to be changed is same */
    if (i4TestValDot1agCfmDefaultMdDefLevel == ECFM_CC_DEF_MD_DEFAULT_LEVEL)
    {
        return SNMP_SUCCESS;
    }

    /* Validate test value */
    if ((i4TestValDot1agCfmDefaultMdDefLevel < ECFM_MD_LEVEL_MIN) ||
        (i4TestValDot1agCfmDefaultMdDefLevel > ECFM_MD_LEVEL_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid MDLevel for Default Domain\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmDefaultMdDefMhfCreation
 Input       :  The Indices

                The Object 
                testValDot1agCfmDefaultMdDefMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmDefaultMdDefMhfCreation (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValDot1agCfmDefaultMdDefMhfCreation)
{

    /* Check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Value that needs to be changed is same */
    if (i4TestValDot1agCfmDefaultMdDefMhfCreation ==
        ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION)
    {
        return SNMP_SUCCESS;
    }

    /* Validate value to be tested */
    if ((i4TestValDot1agCfmDefaultMdDefMhfCreation < ECFM_MHF_CRITERIA_NONE) ||
        (i4TestValDot1agCfmDefaultMdDefMhfCreation >
         ECFM_MHF_CRITERIA_EXPLICIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmDefaultMdDefIdPermission
 Input       :  The Indices

                The Object 
                testValDot1agCfmDefaultMdDefIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmDefaultMdDefIdPermission (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValDot1agCfmDefaultMdDefIdPermission)
{
    /* Check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM module is Shutdown\n");
        return SNMP_FAILURE;
    }
    /* Validate value to be tested */
    if ((i4TestValDot1agCfmDefaultMdDefIdPermission < ECFM_SENDER_ID_NONE) ||
        (i4TestValDot1agCfmDefaultMdDefIdPermission >
         ECFM_SENDER_ID_CHASSID_MANAGE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Invalid Value \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmDefaultMdDefLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmDefaultMdDefLevel (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmDefaultMdDefMhfCreation
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmDefaultMdDefMhfCreation (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmDefaultMdDefIdPermission
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmDefaultMdDefIdPermission (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1agCfmDefaultMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1agCfmDefaultMdTable
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1agCfmDefaultMdTable (UINT4
                                                 u4Dot1agCfmDefaultMdComponentId,
                                                 INT4
                                                 i4Dot1agCfmDefaultMdPrimaryVid)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1agCfmDefaultMdTable
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1agCfmDefaultMdTable (UINT4
                                         *pu4Dot1agCfmDefaultMdComponentId,
                                         INT4 *pi4Dot1agCfmDefaultMdPrimaryVid)
{
    UNUSED_PARAM (pu4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (pi4Dot1agCfmDefaultMdPrimaryVid);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1agCfmDefaultMdTable
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                nextDot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid
                nextDot1agCfmDefaultMdPrimaryVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1agCfmDefaultMdTable (UINT4 u4Dot1agCfmDefaultMdComponentId,
                                        UINT4
                                        *pu4NextDot1agCfmDefaultMdComponentId,
                                        INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                        INT4
                                        *pi4NextDot1agCfmDefaultMdPrimaryVid)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (pu4NextDot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (pi4NextDot1agCfmDefaultMdPrimaryVid);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdStatus
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                retValDot1agCfmDefaultMdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdStatus (UINT4 u4Dot1agCfmDefaultMdComponentId,
                                INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                INT4 *pi4RetValDot1agCfmDefaultMdStatus)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (pi4RetValDot1agCfmDefaultMdStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdLevel
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                retValDot1agCfmDefaultMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdLevel (UINT4 u4Dot1agCfmDefaultMdComponentId,
                               INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                               INT4 *pi4RetValDot1agCfmDefaultMdLevel)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (pi4RetValDot1agCfmDefaultMdLevel);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdMhfCreation
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                retValDot1agCfmDefaultMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdMhfCreation (UINT4 u4Dot1agCfmDefaultMdComponentId,
                                     INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                     INT4
                                     *pi4RetValDot1agCfmDefaultMdMhfCreation)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (pi4RetValDot1agCfmDefaultMdMhfCreation);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1agCfmDefaultMdIdPermission
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                retValDot1agCfmDefaultMdIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1agCfmDefaultMdIdPermission (UINT4 u4Dot1agCfmDefaultMdComponentId,
                                      INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                      INT4
                                      *pi4RetValDot1agCfmDefaultMdIdPermission)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (pi4RetValDot1agCfmDefaultMdIdPermission);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1agCfmDefaultMdLevel
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                setValDot1agCfmDefaultMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmDefaultMdLevel (UINT4 u4Dot1agCfmDefaultMdComponentId,
                               INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                               INT4 i4SetValDot1agCfmDefaultMdLevel)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (i4SetValDot1agCfmDefaultMdLevel);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmDefaultMdMhfCreation
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                setValDot1agCfmDefaultMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmDefaultMdMhfCreation (UINT4 u4Dot1agCfmDefaultMdComponentId,
                                     INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                     INT4 i4SetValDot1agCfmDefaultMdMhfCreation)
{

    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (i4SetValDot1agCfmDefaultMdMhfCreation);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1agCfmDefaultMdIdPermission
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                setValDot1agCfmDefaultMdIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1agCfmDefaultMdIdPermission (UINT4 u4Dot1agCfmDefaultMdComponentId,
                                      INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                      INT4
                                      i4SetValDot1agCfmDefaultMdIdPermission)
{
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (i4SetValDot1agCfmDefaultMdIdPermission);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmDefaultMdLevel
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                testValDot1agCfmDefaultMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmDefaultMdLevel (UINT4 *pu4ErrorCode,
                                  UINT4 u4Dot1agCfmDefaultMdComponentId,
                                  INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                  INT4 i4TestValDot1agCfmDefaultMdLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (i4TestValDot1agCfmDefaultMdLevel);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmDefaultMdMhfCreation
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                testValDot1agCfmDefaultMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmDefaultMdMhfCreation (UINT4 *pu4ErrorCode,
                                        UINT4 u4Dot1agCfmDefaultMdComponentId,
                                        INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                        INT4
                                        i4TestValDot1agCfmDefaultMdMhfCreation)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (i4TestValDot1agCfmDefaultMdMhfCreation);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1agCfmDefaultMdIdPermission
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid

                The Object 
                testValDot1agCfmDefaultMdIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1agCfmDefaultMdIdPermission (UINT4 *pu4ErrorCode,
                                         UINT4 u4Dot1agCfmDefaultMdComponentId,
                                         INT4 i4Dot1agCfmDefaultMdPrimaryVid,
                                         INT4
                                         i4TestValDot1agCfmDefaultMdIdPermission)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1agCfmDefaultMdComponentId);
    UNUSED_PARAM (i4Dot1agCfmDefaultMdPrimaryVid);
    UNUSED_PARAM (i4TestValDot1agCfmDefaultMdIdPermission);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1agCfmDefaultMdTable
 Input       :  The Indices
                Dot1agCfmDefaultMdComponentId
                Dot1agCfmDefaultMdPrimaryVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1agCfmDefaultMdTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/****************************************************************************
  End of File cfmdmdlw.c
 ****************************************************************************/
