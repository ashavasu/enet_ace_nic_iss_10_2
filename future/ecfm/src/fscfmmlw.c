/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmmlw.c,v 1.29 2014/03/01 10:59:04 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "cfminc.h"
#include  "fscfmmcli.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsMIEcfmGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmGlobalTrace (INT4 *pi4RetValFsMIEcfmGlobalTrace)
{
#ifdef TRACE_WANTED
    *pi4RetValFsMIEcfmGlobalTrace =
        ECFM_CONVERT_TO_SNMP_BOOL (ECFM_GLB_TRC_FLAG);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIEcfmGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmOui
 Input       :  The Indices

                The Object 
                retValFsMIEcfmOui
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmOui (tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmOui)
{
    return nmhGetFsEcfmOui (pRetValFsMIEcfmOui);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmGlobalTrace
 Input       :  The Indices

                The Object 
                setValFsMIEcfmGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmGlobalTrace (INT4 i4SetValFsMIEcfmGlobalTrace)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    if ((i4SetValFsMIEcfmGlobalTrace != ECFM_SNMP_TRUE) &&
        (i4SetValFsMIEcfmGlobalTrace != ECFM_SNMP_FALSE))
    {
        return SNMP_FAILURE;
    }

    ECFM_LBLT_LOCK ();
    ECFM_GLB_TRC_FLAG = ECFM_CONVERT_TO_ECFM_BOOL (i4SetValFsMIEcfmGlobalTrace);
    ECFM_LBLT_UNLOCK ();

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIEcfmGlobalTrace, u4SeqNum,
                          FALSE, EcfmCcLock, EcfmCcUnLock,
                          ECFM_TABLE_INDICES_COUNT_ZERO, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsMIEcfmGlobalTrace));

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIEcfmGlobalTrace);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmOui
 Input       :  The Indices

                The Object 
                setValFsMIEcfmOui
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmOui (tSNMP_OCTET_STRING_TYPE * pSetValFsMIEcfmOui)
{
    return (nmhSetFsEcfmOui (pSetValFsMIEcfmOui));

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsMIEcfmGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmGlobalTrace (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMIEcfmGlobalTrace)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIEcfmGlobalTrace != ECFM_SNMP_TRUE) &&
        (i4TestValFsMIEcfmGlobalTrace != ECFM_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIEcfmGlobalTrace);
    return SNMP_FAILURE;
#endif

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmOui
 Input       :  The Indices

                The Object 
                testValFsMIEcfmOui
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmOui (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValFsMIEcfmOui)
{
    if (nmhTestv2FsEcfmOui (pu4ErrorCode, pTestValFsMIEcfmOui) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmGlobalTrace (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmOui
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmOui (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmContextTable
 Input       :  The Indices
                FsMIEcfmContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmContextTable (UINT4 u4FsMIEcfmContextId)
{
    if (EcfmCcIsContextExist (u4FsMIEcfmContextId) != ECFM_TRUE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmContextTable
 Input       :  The Indices
                FsMIEcfmContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmContextTable (UINT4 *pu4FsMIEcfmContextId)
{
    if (EcfmCcGetFirstActiveContext (pu4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmContextTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmContextTable (UINT4 u4FsMIEcfmContextId,
                                     UINT4 *pu4NextFsMIEcfmContextId)
{
    if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                    pu4NextFsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmSystemControl
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
 store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIEcfmSystemControl (UINT4 u4FsMIEcfmContextId,
                             INT4 *pi4RetValFsMIEcfmSystemControl)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsEcfmSystemControl (pi4RetValFsMIEcfmSystemControl);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIEcfmModuleStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmModuleStatus (UINT4 u4FsMIEcfmContextId,
                            INT4 *pi4RetValFsMIEcfmModuleStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmModuleStatus (pi4RetValFsMIEcfmModuleStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdDefLevel
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmDefaultMdDefLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdDefLevel (UINT4
                                 u4FsMIEcfmContextId,
                                 INT4 *pi4RetValFsMIEcfmDefaultMdDefLevel)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmDefaultMdDefLevel (pi4RetValFsMIEcfmDefaultMdDefLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdDefMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmDefaultMdDefMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdDefMhfCreation (UINT4
                                       u4FsMIEcfmContextId,
                                       INT4
                                       *pi4RetValFsMIEcfmDefaultMdDefMhfCreation)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmDefaultMdDefMhfCreation
        (pi4RetValFsMIEcfmDefaultMdDefMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdDefIdPermission
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmDefaultMdDefIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdDefIdPermission (UINT4
                                        u4FsMIEcfmContextId,
                                        INT4
                                        *pi4RetValFsMIEcfmDefaultMdDefIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmDefaultMdDefIdPermission
        (pi4RetValFsMIEcfmDefaultMdDefIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdTableNextIndex
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMdTableNextIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdTableNextIndex (UINT4
                                u4FsMIEcfmContextId,
                                UINT4 *pu4RetValFsMIEcfmMdTableNextIndex)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMdTableNextIndex (pu4RetValFsMIEcfmMdTableNextIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrCacheStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmLtrCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrCacheStatus (UINT4 u4FsMIEcfmContextId,
                              INT4 *pi4RetValFsMIEcfmLtrCacheStatus)
{
    INT1                i1RetVal;

    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmLtrCacheStatus (pi4RetValFsMIEcfmLtrCacheStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrCacheClear
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmLtrClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrCacheClear (UINT4 u4FsMIEcfmContextId,
                             INT4 *pi4RetValFsMIEcfmLtrClear)
{
    INT1                i1RetVal;

    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmLtrCacheClear (pi4RetValFsMIEcfmLtrClear);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrCacheHoldTime
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmLtrCacheHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrCacheHoldTime (UINT4
                                u4FsMIEcfmContextId,
                                INT4 *pi4RetValFsMIEcfmLtrCacheHoldTime)
{
    INT1                i1RetVal;

    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmLtrCacheHoldTime (pi4RetValFsMIEcfmLtrCacheHoldTime);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrCacheSize
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmLtrCacheSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrCacheSize (UINT4 u4FsMIEcfmContextId,
                            INT4 *pi4RetValFsMIEcfmLtrCacheSize)
{
    INT1                i1RetVal;

    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmLtrCacheSize (pi4RetValFsMIEcfmLtrCacheSize);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipCcmDbStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMipCcmDbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipCcmDbStatus (UINT4 u4FsMIEcfmContextId,
                              INT4 *pi4RetValFsMIEcfmMipCcmDbStatus)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMipCcmDbStatus (pi4RetValFsMIEcfmMipCcmDbStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipCcmDbClear
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMipCcmDbClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipCcmDbClear (UINT4 u4FsMIEcfmContextId,
                             INT4 *pi4RetValFsMIEcfmMipCcmDbClear)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMipCcmDbClear (pi4RetValFsMIEcfmMipCcmDbClear);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipCcmDbSize
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMipCcmDbSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipCcmDbSize (UINT4 u4FsMIEcfmContextId,
                            INT4 *pi4RetValFsMIEcfmMipCcmDbSize)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMipCcmDbSize (pi4RetValFsMIEcfmMipCcmDbSize);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipCcmDbHoldTime
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMipCcmDbHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipCcmDbHoldTime (UINT4
                                u4FsMIEcfmContextId,
                                INT4 *pi4RetValFsMIEcfmMipCcmDbHoldTime)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMipCcmDbHoldTime (pi4RetValFsMIEcfmMipCcmDbHoldTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMemoryFailureCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMemoryFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMemoryFailureCount (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4 *pu4RetValFsMIEcfmMemoryFailureCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMemoryFailureCount
        (pu4RetValFsMIEcfmMemoryFailureCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmBufferFailureCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmBufferFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmBufferFailureCount (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4 *pu4RetValFsMIEcfmBufferFailureCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmBufferFailureCount
        (pu4RetValFsMIEcfmBufferFailureCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmUpCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmUpCount (UINT4 u4FsMIEcfmContextId,
                       UINT4 *pu4RetValFsMIEcfmUpCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmUpCount (pu4RetValFsMIEcfmUpCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDownCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDownCount (UINT4 u4FsMIEcfmContextId,
                         UINT4 *pu4RetValFsMIEcfmDownCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmDownCount (pu4RetValFsMIEcfmDownCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmNoDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmNoDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmNoDftCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmNoDftCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmNoDftCount (pu4RetValFsMIEcfmNoDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRdiDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRdiDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRdiDftCount (UINT4 u4FsMIEcfmContextId,
                           UINT4 *pu4RetValFsMIEcfmRdiDftCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRdiDftCount (pu4RetValFsMIEcfmRdiDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMacStatusDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMacStatusDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMacStatusDftCount (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4 *pu4RetValFsMIEcfmMacStatusDftCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMacStatusDftCount (pu4RetValFsMIEcfmMacStatusDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRemoteCcmDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRemoteCcmDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRemoteCcmDftCount (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4 *pu4RetValFsMIEcfmRemoteCcmDftCount)
{

    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmRemoteCcmDftCount (pu4RetValFsMIEcfmRemoteCcmDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmErrorCcmDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmErrorCcmDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmErrorCcmDftCount (UINT4
                                u4FsMIEcfmContextId,
                                UINT4 *pu4RetValFsMIEcfmErrorCcmDftCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmErrorCcmDftCount (pu4RetValFsMIEcfmErrorCcmDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmXconDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmXconDftCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmXconDftCount (UINT4 u4FsMIEcfmContextId,
                            UINT4 *pu4RetValFsMIEcfmXconDftCount)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmXconDftCount (pu4RetValFsMIEcfmXconDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmCrosscheckDelay
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmCrosscheckDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmCrosscheckDelay (UINT4
                               u4FsMIEcfmContextId,
                               INT4 *pi4RetValFsMIEcfmCrosscheckDelay)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmCrosscheckDelay (pi4RetValFsMIEcfmCrosscheckDelay);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipDynamicEvaluationStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmMipDynamicEvaluationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipDynamicEvaluationStatus (UINT4
                                          u4FsMIEcfmContextId,
                                          INT4
                                          *pi4RetValFsMIEcfmMipDynamicEvaluationStatus)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMipDynamicEvaluationStatus
        (pi4RetValFsMIEcfmMipDynamicEvaluationStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmContextName
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmContextName (UINT4 u4FsMIEcfmContextId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmContextName)
{
    UINT1               au1ContextName[ECFM_SWITCH_ALIAS_LEN];

    ECFM_MEMSET (au1ContextName, ECFM_INIT_VAL, ECFM_SWITCH_ALIAS_LEN);

    if (EcfmCcIsContextExist (u4FsMIEcfmContextId) != ECFM_TRUE)
    {
        return SNMP_FAILURE;
    }

    EcfmVcmGetAliasName (u4FsMIEcfmContextId, au1ContextName);

    ECFM_MEMCPY (pRetValFsMIEcfmContextName->pu1_OctetList,
                 au1ContextName, ECFM_STRLEN (au1ContextName));
    pRetValFsMIEcfmContextName->i4_Length = ECFM_STRLEN (au1ContextName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTrapControl
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTrapControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTrapControl (UINT4 u4FsMIEcfmContextId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmTrapControl)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmTrapControl (pRetValFsMIEcfmTrapControl);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTrapType
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTrapType (UINT4 u4FsMIEcfmContextId,
                        INT4 *pi4RetValFsMIEcfmTrapType)
{
    UNUSED_PARAM (u4FsMIEcfmContextId);
    UNUSED_PARAM (pi4RetValFsMIEcfmTrapType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTraceOption
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTraceOption (UINT4 u4FsMIEcfmContextId,
                           INT4 *pi4RetValFsMIEcfmTraceOption)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmTraceOption (pi4RetValFsMIEcfmTraceOption);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmGlobalCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmGlobalCcmOffload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmGlobalCcmOffload (UINT4 u4FsMIEcfmContextId,
                                INT4 *pi4RetValFsMIEcfmGlobalCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsEcfmGlobalCcmOffload (pi4RetValFsMIEcfmGlobalCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmSystemControl
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmSystemControl (UINT4
                             u4FsMIEcfmContextId,
                             INT4 i4SetValFsMIEcfmSystemControl)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmSystemControl (i4SetValFsMIEcfmSystemControl);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmModuleStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmModuleStatus (UINT4 u4FsMIEcfmContextId,
                            INT4 i4SetValFsMIEcfmModuleStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmModuleStatus (i4SetValFsMIEcfmModuleStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDefaultMdDefLevel
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmDefaultMdDefLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDefaultMdDefLevel (UINT4
                                 u4FsMIEcfmContextId,
                                 INT4 i4SetValFsMIEcfmDefaultMdDefLevel)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1agCfmDefaultMdDefLevel
        (i4SetValFsMIEcfmDefaultMdDefLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDefaultMdDefMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmDefaultMdDefMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDefaultMdDefMhfCreation (UINT4
                                       u4FsMIEcfmContextId,
                                       INT4
                                       i4SetValFsMIEcfmDefaultMdDefMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1agCfmDefaultMdDefMhfCreation
        (i4SetValFsMIEcfmDefaultMdDefMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDefaultMdDefIdPermission
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmDefaultMdDefIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDefaultMdDefIdPermission (UINT4
                                        u4FsMIEcfmContextId,
                                        INT4
                                        i4SetValFsMIEcfmDefaultMdDefIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1agCfmDefaultMdDefIdPermission
        (i4SetValFsMIEcfmDefaultMdDefIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmLtrCacheStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmLtrCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmLtrCacheStatus (UINT4
                              u4FsMIEcfmContextId,
                              INT4 i4SetValFsMIEcfmLtrCacheStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmLtrCacheStatus (i4SetValFsMIEcfmLtrCacheStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmLtrCacheClear
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmLtrClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmLtrCacheClear (UINT4 u4FsMIEcfmContextId,
                             INT4 i4SetValFsMIEcfmLtrClear)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmLtrCacheClear (i4SetValFsMIEcfmLtrClear);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmLtrCacheHoldTime
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmLtrCacheHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmLtrCacheHoldTime (UINT4
                                u4FsMIEcfmContextId,
                                INT4 i4SetValFsMIEcfmLtrCacheHoldTime)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmLtrCacheHoldTime (i4SetValFsMIEcfmLtrCacheHoldTime);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmLtrCacheSize
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmLtrCacheSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmLtrCacheSize (UINT4 u4FsMIEcfmContextId,
                            INT4 i4SetValFsMIEcfmLtrCacheSize)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmLtrCacheSize (i4SetValFsMIEcfmLtrCacheSize);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipCcmDbStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMipCcmDbStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipCcmDbStatus (UINT4
                              u4FsMIEcfmContextId,
                              INT4 i4SetValFsMIEcfmMipCcmDbStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmMipCcmDbStatus (i4SetValFsMIEcfmMipCcmDbStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipCcmDbClear
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMipCcmDbClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipCcmDbClear (UINT4
                             u4FsMIEcfmContextId,
                             INT4 i4SetValFsMIEcfmMipCcmDbClear)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmMipCcmDbClear (i4SetValFsMIEcfmMipCcmDbClear);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipCcmDbSize
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMipCcmDbSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipCcmDbSize (UINT4 u4FsMIEcfmContextId,
                            INT4 i4SetValFsMIEcfmMipCcmDbSize)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmMipCcmDbSize (i4SetValFsMIEcfmMipCcmDbSize);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipCcmDbHoldTime
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMipCcmDbHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipCcmDbHoldTime (UINT4
                                u4FsMIEcfmContextId,
                                INT4 i4SetValFsMIEcfmMipCcmDbHoldTime)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmMipCcmDbHoldTime (i4SetValFsMIEcfmMipCcmDbHoldTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMemoryFailureCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMemoryFailureCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMemoryFailureCount (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4SetValFsMIEcfmMemoryFailureCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMemoryFailureCount (u4SetValFsMIEcfmMemoryFailureCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmBufferFailureCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmBufferFailureCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmBufferFailureCount (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4SetValFsMIEcfmBufferFailureCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmBufferFailureCount (u4SetValFsMIEcfmBufferFailureCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmUpCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmUpCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmUpCount (UINT4 u4FsMIEcfmContextId, UINT4 u4SetValFsMIEcfmUpCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmUpCount (u4SetValFsMIEcfmUpCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDownCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmDownCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDownCount (UINT4 u4FsMIEcfmContextId,
                         UINT4 u4SetValFsMIEcfmDownCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmDownCount (u4SetValFsMIEcfmDownCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmNoDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmNoDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmNoDftCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmNoDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmNoDftCount (u4SetValFsMIEcfmNoDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRdiDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRdiDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRdiDftCount (UINT4 u4FsMIEcfmContextId,
                           UINT4 u4SetValFsMIEcfmRdiDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmRdiDftCount (u4SetValFsMIEcfmRdiDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMacStatusDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMacStatusDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMacStatusDftCount (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4SetValFsMIEcfmMacStatusDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMacStatusDftCount (u4SetValFsMIEcfmMacStatusDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRemoteCcmDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRemoteCcmDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRemoteCcmDftCount (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4SetValFsMIEcfmRemoteCcmDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmRemoteCcmDftCount (u4SetValFsMIEcfmRemoteCcmDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmErrorCcmDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmErrorCcmDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmErrorCcmDftCount (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4SetValFsMIEcfmErrorCcmDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmErrorCcmDftCount (u4SetValFsMIEcfmErrorCcmDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmXconDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmXconDftCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmXconDftCount (UINT4 u4FsMIEcfmContextId,
                            UINT4 u4SetValFsMIEcfmXconDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmXconDftCount (u4SetValFsMIEcfmXconDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmCrosscheckDelay
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmCrosscheckDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmCrosscheckDelay (UINT4
                               u4FsMIEcfmContextId,
                               INT4 i4SetValFsMIEcfmCrosscheckDelay)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmCrosscheckDelay (i4SetValFsMIEcfmCrosscheckDelay);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipDynamicEvaluationStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmMipDynamicEvaluationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipDynamicEvaluationStatus (UINT4
                                          u4FsMIEcfmContextId,
                                          INT4
                                          i4SetValFsMIEcfmMipDynamicEvaluationStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMipDynamicEvaluationStatus
        (i4SetValFsMIEcfmMipDynamicEvaluationStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTrapControl
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTrapControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTrapControl (UINT4 u4FsMIEcfmContextId,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsMIEcfmTrapControl)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmTrapControl (pSetValFsMIEcfmTrapControl);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTraceOption
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTraceOption (UINT4 u4FsMIEcfmContextId,
                           INT4 i4SetValFsMIEcfmTraceOption)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmTraceOption (i4SetValFsMIEcfmTraceOption);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmGlobalCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmGlobalCcmOffload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmGlobalCcmOffload (UINT4 u4FsMIEcfmContextId,
                                INT4 i4SetValFsMIEcfmGlobalCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmGlobalCcmOffload (i4SetValFsMIEcfmGlobalCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmSystemControl
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmSystemControl (UINT4 *pu4ErrorCode,
                                UINT4
                                u4FsMIEcfmContextId,
                                INT4 i4TestValFsMIEcfmSystemControl)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmSystemControl (pu4ErrorCode,
                                             i4TestValFsMIEcfmSystemControl);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmModuleStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmModuleStatus (UINT4 *pu4ErrorCode,
                               UINT4
                               u4FsMIEcfmContextId,
                               INT4 i4TestValFsMIEcfmModuleStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmModuleStatus (pu4ErrorCode,
                                            i4TestValFsMIEcfmModuleStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDefaultMdDefLevel
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmDefaultMdDefLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDefaultMdDefLevel (UINT4 *pu4ErrorCode,
                                    UINT4
                                    u4FsMIEcfmContextId,
                                    INT4 i4TestValFsMIEcfmDefaultMdDefLevel)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmDefaultMdDefLevel (pu4ErrorCode,
                                                    i4TestValFsMIEcfmDefaultMdDefLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDefaultMdDefMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmDefaultMdDefMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDefaultMdDefMhfCreation (UINT4
                                          *pu4ErrorCode,
                                          UINT4
                                          u4FsMIEcfmContextId,
                                          INT4
                                          i4TestValFsMIEcfmDefaultMdDefMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmDefaultMdDefMhfCreation (pu4ErrorCode,
                                                          i4TestValFsMIEcfmDefaultMdDefMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDefaultMdDefIdPermission
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmDefaultMdDefIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDefaultMdDefIdPermission (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4FsMIEcfmContextId,
                                           INT4
                                           i4TestValFsMIEcfmDefaultMdDefIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmDefaultMdDefIdPermission (pu4ErrorCode,
                                                           i4TestValFsMIEcfmDefaultMdDefIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmLtrCacheStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmLtrCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmLtrCacheStatus (UINT4 *pu4ErrorCode,
                                 UINT4
                                 u4FsMIEcfmContextId,
                                 INT4 i4TestValFsMIEcfmLtrCacheStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmLtrCacheStatus (pu4ErrorCode,
                                              i4TestValFsMIEcfmLtrCacheStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmLtrCacheClear
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmLtrClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmLtrCacheClear (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIEcfmContextId,
                                INT4 i4TestValFsMIEcfmLtrClear)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmLtrCacheClear (pu4ErrorCode,
                                             i4TestValFsMIEcfmLtrClear);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmLtrCacheHoldTime
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmLtrCacheHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmLtrCacheHoldTime (UINT4 *pu4ErrorCode,
                                   UINT4
                                   u4FsMIEcfmContextId,
                                   INT4 i4TestValFsMIEcfmLtrCacheHoldTime)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmLtrCacheHoldTime (pu4ErrorCode,
                                                i4TestValFsMIEcfmLtrCacheHoldTime);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmLtrCacheSize
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmLtrCacheSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmLtrCacheSize (UINT4 *pu4ErrorCode,
                               UINT4
                               u4FsMIEcfmContextId,
                               INT4 i4TestValFsMIEcfmLtrCacheSize)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmLtrCacheSize (pu4ErrorCode,
                                            i4TestValFsMIEcfmLtrCacheSize);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipCcmDbStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMipCcmDbStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipCcmDbStatus (UINT4 *pu4ErrorCode,
                                 UINT4
                                 u4FsMIEcfmContextId,
                                 INT4 i4TestValFsMIEcfmMipCcmDbStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMipCcmDbStatus (pu4ErrorCode,
                                              i4TestValFsMIEcfmMipCcmDbStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipCcmDbClear
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMipCcmDbClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipCcmDbClear (UINT4 *pu4ErrorCode,
                                UINT4
                                u4FsMIEcfmContextId,
                                INT4 i4TestValFsMIEcfmMipCcmDbClear)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMipCcmDbClear (pu4ErrorCode,
                                             i4TestValFsMIEcfmMipCcmDbClear);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipCcmDbSize
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMipCcmDbSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipCcmDbSize (UINT4 *pu4ErrorCode,
                               UINT4
                               u4FsMIEcfmContextId,
                               INT4 i4TestValFsMIEcfmMipCcmDbSize)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMipCcmDbSize (pu4ErrorCode,
                                            i4TestValFsMIEcfmMipCcmDbSize);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipCcmDbHoldTime
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMipCcmDbHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipCcmDbHoldTime (UINT4 *pu4ErrorCode,
                                   UINT4
                                   u4FsMIEcfmContextId,
                                   INT4 i4TestValFsMIEcfmMipCcmDbHoldTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMipCcmDbHoldTime (pu4ErrorCode,
                                                i4TestValFsMIEcfmMipCcmDbHoldTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMemoryFailureCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMemoryFailureCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMemoryFailureCount (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4TestValFsMIEcfmMemoryFailureCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMemoryFailureCount (pu4ErrorCode,
                                                  u4TestValFsMIEcfmMemoryFailureCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmBufferFailureCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmBufferFailureCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmBufferFailureCount (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4TestValFsMIEcfmBufferFailureCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmBufferFailureCount (pu4ErrorCode,
                                                  u4TestValFsMIEcfmBufferFailureCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmUpCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmUpCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmUpCount (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                          UINT4 u4TestValFsMIEcfmUpCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmUpCount (pu4ErrorCode, u4TestValFsMIEcfmUpCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDownCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmDownCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDownCount (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                            UINT4 u4TestValFsMIEcfmDownCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmDownCount (pu4ErrorCode,
                                         u4TestValFsMIEcfmDownCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmNoDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmNoDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmNoDftCount (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmNoDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmNoDftCount (pu4ErrorCode,
                                          u4TestValFsMIEcfmNoDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRdiDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRdiDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRdiDftCount (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                              UINT4 u4TestValFsMIEcfmRdiDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmRdiDftCount (pu4ErrorCode,
                                           u4TestValFsMIEcfmRdiDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMacStatusDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMacStatusDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMacStatusDftCount (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4TestValFsMIEcfmMacStatusDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMacStatusDftCount (pu4ErrorCode,
                                                 u4TestValFsMIEcfmMacStatusDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRemoteCcmDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRemoteCcmDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRemoteCcmDftCount (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4TestValFsMIEcfmRemoteCcmDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmRemoteCcmDftCount (pu4ErrorCode,
                                                 u4TestValFsMIEcfmRemoteCcmDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmErrorCcmDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmErrorCcmDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmErrorCcmDftCount (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4TestValFsMIEcfmErrorCcmDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmErrorCcmDftCount (pu4ErrorCode,
                                                u4TestValFsMIEcfmErrorCcmDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmXconDftCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmXconDftCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmXconDftCount (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                               UINT4 u4TestValFsMIEcfmXconDftCount)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmXconDftCount (pu4ErrorCode,
                                            u4TestValFsMIEcfmXconDftCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmCrosscheckDelay
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmCrosscheckDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmCrosscheckDelay (UINT4 *pu4ErrorCode,
                                  UINT4
                                  u4FsMIEcfmContextId,
                                  INT4 i4TestValFsMIEcfmCrosscheckDelay)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmCrosscheckDelay (pu4ErrorCode,
                                               i4TestValFsMIEcfmCrosscheckDelay);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipDynamicEvaluationStatus
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmMipDynamicEvaluationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipDynamicEvaluationStatus (UINT4
                                             *pu4ErrorCode,
                                             UINT4
                                             u4FsMIEcfmContextId,
                                             INT4
                                             i4TestValFsMIEcfmMipDynamicEvaluationStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMipDynamicEvaluationStatus (pu4ErrorCode,
                                                          i4TestValFsMIEcfmMipDynamicEvaluationStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTrapControl
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTrapControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTrapControl (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIEcfmContextId,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsMIEcfmTrapControl)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2FsEcfmTrapControl (pu4ErrorCode, pTestValFsMIEcfmTrapControl);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTraceOption
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTraceOption (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIEcfmContextId,
                              INT4 i4TestValFsMIEcfmTraceOption)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmTraceOption (pu4ErrorCode,
                                           i4TestValFsMIEcfmTraceOption);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmGlobalCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmGlobalCcmOffload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmGlobalCcmOffload (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   INT4 i4TestValFsMIEcfmGlobalCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2FsEcfmGlobalCcmOffload (pu4ErrorCode,
                                         i4TestValFsMIEcfmGlobalCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmContextTable
 Input       :  The Indices
                FsMIEcfmContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmContextTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmPortTable
 Input       :  The Indices
                FsMIEcfmPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmPortTable (INT4 i4FsMIEcfmPortIfIndex)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcPortInfo     PortIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmPortIfIndex,
                                                 &u4ContextId, &u2LocalPortId);
    PortIndex.u4ContextId = u4ContextId;
    PortIndex.u4IfIndex = i4FsMIEcfmPortIfIndex;
    pPortInfo =
        (tEcfmCcPortInfo *) RBTreeGet (ECFM_CC_GLOBAL_PORT_TABLE, &PortIndex);
    UNUSED_PARAM (i4RetVal);

    if (pPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsMIEcfmPortTable
Input       :  The Indices
               FsMIEcfmPortIfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmPortTable (INT4 *pi4FsMIEcfmPortIfIndex)
{
    return (nmhGetNextIndexFsMIEcfmPortTable (0, pi4FsMIEcfmPortIfIndex));

}

/****************************************************************************
Function    :  nmhGetNextIndexFsMIEcfmPortTable
Input       :  The Indices
FsMIEcfmPortIfIndex
nextFsMIEcfmPortIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmPortTable (INT4 i4FsMIEcfmPortIfIndex,
                                  INT4 *pi4NextFsMIEcfmPortIfIndex)
{
    tEcfmCcPortInfo    *pNextPortInfo = NULL;
    tEcfmCcPortInfo     PortIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&PortIndex, 0x00, sizeof (tEcfmCcPortInfo));

    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmPortIfIndex,
                                                 &u4ContextId, &u2LocalPortId);
    PortIndex.u4ContextId = u4ContextId;
    PortIndex.u4IfIndex = i4FsMIEcfmPortIfIndex;
    pNextPortInfo = (tEcfmCcPortInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_PORT_TABLE, (tRBElem *) & PortIndex, NULL);
    UNUSED_PARAM (i4RetVal);

    if (pNextPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIEcfmPortIfIndex = pNextPortInfo->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortLLCEncapStatus
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortLLCEncapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortLLCEncapStatus (INT4 i4FsMIEcfmPortIfIndex,
                                  INT4 *pi4RetValFsMIEcfmPortLLCEncapStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortLLCEncapStatus ((UINT4) u2LocalPort,
                                               pi4RetValFsMIEcfmPortLLCEncapStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortModuleStatus
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortModuleStatus (INT4 i4FsMIEcfmPortIfIndex,
                                INT4 *pi4RetValFsMIEcfmPortModuleStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortModuleStatus ((UINT4) u2LocalPort,
                                             pi4RetValFsMIEcfmPortModuleStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 *pu4RetValFsMIEcfmPortTxCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortTxCfmPduCount ((UINT4) u2LocalPort,
                                              pu4RetValFsMIEcfmPortTxCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxCcmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxCcmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxCcmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortTxCcmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsEcfmPortTxCcmCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortTxCcmCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxLbmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxLbmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxLbmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortTxLbmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsEcfmPortTxLbmCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortTxLbmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxLbrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxLbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxLbrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortTxLbrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortTxLbrCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortTxLbrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxLtmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxLtmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxLtmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortTxLtmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortTxLtmCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortTxLtmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxLtrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxLtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxLtrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortTxLtrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortTxLtrCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortTxLtrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortTxFailedCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortTxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortTxFailedCount (INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 *pu4RetValFsMIEcfmPortTxFailedCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortTxFailedCount ((UINT4) u2LocalPort,
                                              pu4RetValFsMIEcfmPortTxFailedCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 *pu4RetValFsMIEcfmPortRxCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxCfmPduCount ((UINT4) u2LocalPort,
                                              pu4RetValFsMIEcfmPortRxCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxCcmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxCcmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxCcmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortRxCcmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxCcmCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortRxCcmCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxLbmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxLbmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxLbmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortRxLbmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxLbmCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortRxLbmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxLbrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxLbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxLbrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortRxLbrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxLbrCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortRxLbrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxLtmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxLtmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxLtmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortRxLtmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxLtmCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortRxLtmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxLtrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxLtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxLtrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 *pu4RetValFsMIEcfmPortRxLtrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxLtrCount ((UINT4) u2LocalPort,
                                           pu4RetValFsMIEcfmPortRxLtrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortRxBadCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortRxBadCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortRxBadCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                    UINT4
                                    *pu4RetValFsMIEcfmPortRxBadCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortRxBadCfmPduCount ((UINT4) u2LocalPort,
                                                 pu4RetValFsMIEcfmPortRxBadCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortFrwdCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortFrwdCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortFrwdCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                   UINT4 *pu4RetValFsMIEcfmPortFrwdCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortFrwdCfmPduCount ((UINT4) u2LocalPort,
                                                pu4RetValFsMIEcfmPortFrwdCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmPortDsrdCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                retValFsMIEcfmPortDsrdCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmPortDsrdCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                   UINT4 *pu4RetValFsMIEcfmPortDsrdCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmPortDsrdCfmPduCount ((UINT4) u2LocalPort,
                                                pu4RetValFsMIEcfmPortDsrdCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortLLCEncapStatus
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortLLCEncapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortLLCEncapStatus (INT4 i4FsMIEcfmPortIfIndex,
                                  INT4 i4SetValFsMIEcfmPortLLCEncapStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortLLCEncapStatus ((UINT4) u2LocalPort,
                                               i4SetValFsMIEcfmPortLLCEncapStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortModuleStatus
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortModuleStatus (INT4 i4FsMIEcfmPortIfIndex,
                                INT4 i4SetValFsMIEcfmPortModuleStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortModuleStatus ((UINT4) u2LocalPort,
                                             i4SetValFsMIEcfmPortModuleStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4SetValFsMIEcfmPortTxCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortTxCfmPduCount ((UINT4) u2LocalPort,
                                              u4SetValFsMIEcfmPortTxCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxCcmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxCcmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxCcmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortTxCcmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortTxCcmCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortTxCcmCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxLbmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxLbmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxLbmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortTxLbmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortTxLbmCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortTxLbmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxLbrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxLbrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxLbrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortTxLbrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortTxLbrCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortTxLbrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxLtmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxLtmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxLtmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortTxLtmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmPortTxLtmCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortTxLtmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxLtrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxLtrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxLtrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortTxLtrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortTxLtrCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortTxLtrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortTxFailedCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortTxFailedCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortTxFailedCount (INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4SetValFsMIEcfmPortTxFailedCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortTxFailedCount ((UINT4) u2LocalPort,
                                              u4SetValFsMIEcfmPortTxFailedCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4SetValFsMIEcfmPortRxCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortRxCfmPduCount ((UINT4) u2LocalPort,
                                              u4SetValFsMIEcfmPortRxCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxCcmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxCcmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxCcmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortRxCcmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmPortRxCcmCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortRxCcmCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxLbmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxLbmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxLbmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortRxLbmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmPortRxLbmCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortRxLbmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxLbrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxLbrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxLbrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortRxLbrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortRxLbrCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortRxLbrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxLtmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxLtmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxLtmCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortRxLtmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmPortRxLtmCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortRxLtmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxLtrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxLtrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxLtrCount (INT4 i4FsMIEcfmPortIfIndex,
                              UINT4 u4SetValFsMIEcfmPortRxLtrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmPortRxLtrCount ((UINT4) u2LocalPort,
                                           u4SetValFsMIEcfmPortRxLtrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortRxBadCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortRxBadCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortRxBadCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                    UINT4 u4SetValFsMIEcfmPortRxBadCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortRxBadCfmPduCount ((UINT4) u2LocalPort,
                                                 u4SetValFsMIEcfmPortRxBadCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortFrwdCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortFrwdCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortFrwdCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                   UINT4 u4SetValFsMIEcfmPortFrwdCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortFrwdCfmPduCount ((UINT4) u2LocalPort,
                                                u4SetValFsMIEcfmPortFrwdCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmPortDsrdCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                setValFsMIEcfmPortDsrdCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmPortDsrdCfmPduCount (INT4 i4FsMIEcfmPortIfIndex,
                                   UINT4 u4SetValFsMIEcfmPortDsrdCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmPortDsrdCfmPduCount ((UINT4) u2LocalPort,
                                                u4SetValFsMIEcfmPortDsrdCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortLLCEncapStatus
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortLLCEncapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortLLCEncapStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIEcfmPortIfIndex,
                                     INT4 i4TestValFsMIEcfmPortLLCEncapStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortLLCEncapStatus (pu4ErrorCode,
                                                  (UINT4) u2LocalPort,
                                                  i4TestValFsMIEcfmPortLLCEncapStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortModuleStatus
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortModuleStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIEcfmPortIfIndex,
                                   INT4 i4TestValFsMIEcfmPortModuleStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortModuleStatus (pu4ErrorCode,
                                                (UINT4) u2LocalPort,
                                                i4TestValFsMIEcfmPortModuleStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxCfmPduCount (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIEcfmPortIfIndex,
                                    UINT4 u4TestValFsMIEcfmPortTxCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortTxCfmPduCount (pu4ErrorCode,
                                                 (UINT4) u2LocalPort,
                                                 u4TestValFsMIEcfmPortTxCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxCcmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxCcmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxCcmCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortTxCcmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortTxCcmCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortTxCcmCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxLbmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxLbmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxLbmCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortTxLbmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortTxLbmCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortTxLbmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxLbrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxLbrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxLbrCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortTxLbrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortTxLbrCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortTxLbrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxLtmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxLtmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxLtmCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortTxLtmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortTxLtmCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortTxLtmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxLtrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxLtrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxLtrCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortTxLtrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortTxLtrCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortTxLtrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortTxFailedCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortTxFailedCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortTxFailedCount (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIEcfmPortIfIndex,
                                    UINT4 u4TestValFsMIEcfmPortTxFailedCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortTxFailedCount (pu4ErrorCode,
                                                 (UINT4) u2LocalPort,
                                                 u4TestValFsMIEcfmPortTxFailedCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxCfmPduCount (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIEcfmPortIfIndex,
                                    UINT4 u4TestValFsMIEcfmPortRxCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortRxCfmPduCount (pu4ErrorCode,
                                                 (UINT4) u2LocalPort,
                                                 u4TestValFsMIEcfmPortRxCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxCcmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxCcmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxCcmCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortRxCcmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortRxCcmCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortRxCcmCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxLbmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxLbmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxLbmCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortRxLbmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortRxLbmCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortRxLbmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxLbrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxLbrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxLbrCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortRxLbrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortRxLbrCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortRxLbrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxLtmCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxLtmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxLtmCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortRxLtmCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortRxLtmCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortRxLtmCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxLtrCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxLtrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxLtrCount (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIEcfmPortIfIndex,
                                 UINT4 u4TestValFsMIEcfmPortRxLtrCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortRxLtrCount (pu4ErrorCode,
                                              (UINT4) u2LocalPort,
                                              u4TestValFsMIEcfmPortRxLtrCount);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortRxBadCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortRxBadCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortRxBadCfmPduCount (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIEcfmPortIfIndex,
                                       UINT4
                                       u4TestValFsMIEcfmPortRxBadCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortRxBadCfmPduCount (pu4ErrorCode,
                                                    (UINT4) u2LocalPort,
                                                    u4TestValFsMIEcfmPortRxBadCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortFrwdCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortFrwdCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortFrwdCfmPduCount (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIEcfmPortIfIndex,
                                      UINT4
                                      u4TestValFsMIEcfmPortFrwdCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmPortFrwdCfmPduCount (pu4ErrorCode,
                                                   (UINT4) u2LocalPort,
                                                   u4TestValFsMIEcfmPortFrwdCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmPortDsrdCfmPduCount
 Input       :  The Indices
                FsMIEcfmPortIfIndex

                The Object 
                testValFsMIEcfmPortDsrdCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmPortDsrdCfmPduCount (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIEcfmPortIfIndex,
                                      UINT4
                                      u4TestValFsMIEcfmPortDsrdCfmPduCount)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmPortIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmPortDsrdCfmPduCount (pu4ErrorCode,
                                                   (UINT4) u2LocalPort,
                                                   u4TestValFsMIEcfmPortDsrdCfmPduCount);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmPortTable
 Input       :  The Indices
                FsMIEcfmPortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmStackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmStackTable
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                FsMIEcfmStackDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmStackTable (INT4 i4FsMIEcfmStackIfIndex,
                                            INT4 i4FsMIEcfmStackVlanIdOrNone,
                                            INT4 i4FsMIEcfmStackMdLevel,
                                            INT4 i4FsMIEcfmStackDirection)
{
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmCcStackInfo    StackIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmStackIfIndex,
                                                 &u4ContextId, &u2LocalPortId);

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    StackIndex.u4ContextId = u4ContextId;
    StackIndex.u4IfIndex = i4FsMIEcfmStackIfIndex;
    StackIndex.u4VlanIdIsid = i4FsMIEcfmStackVlanIdOrNone;
    StackIndex.u1MdLevel = i4FsMIEcfmStackMdLevel;
    StackIndex.u1Direction = i4FsMIEcfmStackDirection;
    pStackInfo =
        (tEcfmCcStackInfo *) RBTreeGet (ECFM_CC_GLOBAL_STACK_TABLE,
                                        &StackIndex);
    UNUSED_PARAM (i4RetVal);

    if (pStackInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmStackTable
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                FsMIEcfmStackDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmStackTable (INT4 *pi4FsMIEcfmStackIfIndex,
                                    INT4 *pi4FsMIEcfmStackVlanIdOrNone,
                                    INT4 *pi4FsMIEcfmStackMdLevel,
                                    INT4 *pi4FsMIEcfmStackDirection)
{
    return (nmhGetNextIndexFsMIEcfmStackTable (0, pi4FsMIEcfmStackIfIndex,
                                               0,
                                               pi4FsMIEcfmStackVlanIdOrNone,
                                               0, pi4FsMIEcfmStackMdLevel,
                                               0, pi4FsMIEcfmStackDirection));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmStackTable
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                nextFsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                nextFsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                nextFsMIEcfmStackMdLevel
                FsMIEcfmStackDirection
                nextFsMIEcfmStackDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmStackTable (INT4 i4FsMIEcfmStackIfIndex,
                                   INT4 *pi4NextFsMIEcfmStackIfIndex,
                                   INT4 i4FsMIEcfmStackVlanIdOrNone,
                                   INT4 *pi4NextFsMIEcfmStackVlanIdOrNone,
                                   INT4 i4FsMIEcfmStackMdLevel,
                                   INT4 *pi4NextFsMIEcfmStackMdLevel,
                                   INT4 i4FsMIEcfmStackDirection,
                                   INT4 *pi4NextFsMIEcfmStackDirection)
{
    tEcfmCcStackInfo   *pNextStackInfo = NULL;
    tEcfmCcStackInfo    StackIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmStackIfIndex,
                                                 &u4ContextId, &u2LocalPortId);

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&StackIndex, 0x00, sizeof (tEcfmCcStackInfo));
    StackIndex.u4ContextId = u4ContextId;
    StackIndex.u4IfIndex = i4FsMIEcfmStackIfIndex;
    StackIndex.u4VlanIdIsid = i4FsMIEcfmStackVlanIdOrNone;
    StackIndex.u1MdLevel = i4FsMIEcfmStackMdLevel;
    StackIndex.u1Direction = i4FsMIEcfmStackDirection;

    pNextStackInfo = (tEcfmCcStackInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) & StackIndex, NULL);
    UNUSED_PARAM (i4RetVal);

    if (pNextStackInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIEcfmStackIfIndex = pNextStackInfo->u4IfIndex;
    *pi4NextFsMIEcfmStackVlanIdOrNone = pNextStackInfo->u4VlanIdIsid;
    *pi4NextFsMIEcfmStackMdLevel = pNextStackInfo->u1MdLevel;
    *pi4NextFsMIEcfmStackDirection = pNextStackInfo->u1Direction;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmStackMdIndex
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                FsMIEcfmStackDirection

                The Object 
                retValFsMIEcfmStackMdIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmStackMdIndex (INT4 i4FsMIEcfmStackIfIndex,
                            INT4 i4FsMIEcfmStackVlanIdOrNone,
                            INT4 i4FsMIEcfmStackMdLevel,
                            INT4 i4FsMIEcfmStackDirection,
                            UINT4 *pu4RetValFsMIEcfmStackMdIndex)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmStackServiceSelectorType;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmStackIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmStackVlanIdOrNone >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmStackVlanIdOrNone =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmStackVlanIdOrNone);
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_ISID;

    }
    else
    {
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_VLAN;
    }
    i1RetVal = nmhGetIeee8021CfmStackMdIndex ((INT4) u2LocalPort,
                                              i4FsMIEcfmStackServiceSelectorType,
                                              i4FsMIEcfmStackVlanIdOrNone,
                                              i4FsMIEcfmStackMdLevel,
                                              i4FsMIEcfmStackDirection,
                                              pu4RetValFsMIEcfmStackMdIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmStackMaIndex
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                FsMIEcfmStackDirection

                The Object 
                retValFsMIEcfmStackMaIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmStackMaIndex (INT4 i4FsMIEcfmStackIfIndex,
                            INT4 i4FsMIEcfmStackVlanIdOrNone,
                            INT4 i4FsMIEcfmStackMdLevel,
                            INT4 i4FsMIEcfmStackDirection,
                            UINT4 *pu4RetValFsMIEcfmStackMaIndex)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmStackServiceSelectorType;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmStackIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmStackVlanIdOrNone >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmStackVlanIdOrNone =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmStackVlanIdOrNone);
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_ISID;

    }
    else
    {
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_VLAN;
    }
    i1RetVal = nmhGetIeee8021CfmStackMaIndex ((INT4) u2LocalPort,
                                              i4FsMIEcfmStackServiceSelectorType,
                                              i4FsMIEcfmStackVlanIdOrNone,
                                              i4FsMIEcfmStackMdLevel,
                                              i4FsMIEcfmStackDirection,
                                              pu4RetValFsMIEcfmStackMaIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmStackMepId
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                FsMIEcfmStackDirection

                The Object 
                retValFsMIEcfmStackMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmStackMepId (INT4 i4FsMIEcfmStackIfIndex,
                          INT4 i4FsMIEcfmStackVlanIdOrNone,
                          INT4 i4FsMIEcfmStackMdLevel,
                          INT4 i4FsMIEcfmStackDirection,
                          UINT4 *pu4RetValFsMIEcfmStackMepId)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmStackServiceSelectorType;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmStackIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmStackVlanIdOrNone >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmStackVlanIdOrNone =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmStackVlanIdOrNone);
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_ISID;

    }
    else
    {
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_VLAN;
    }

    i1RetVal = nmhGetIeee8021CfmStackMepId ((INT4) u2LocalPort,
                                            i4FsMIEcfmStackServiceSelectorType,
                                            i4FsMIEcfmStackVlanIdOrNone,
                                            i4FsMIEcfmStackMdLevel,
                                            i4FsMIEcfmStackDirection,
                                            pu4RetValFsMIEcfmStackMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmStackMacAddress
 Input       :  The Indices
                FsMIEcfmStackIfIndex
                FsMIEcfmStackVlanIdOrNone
                FsMIEcfmStackMdLevel
                FsMIEcfmStackDirection

                The Object 
                retValFsMIEcfmStackMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmStackMacAddress (INT4 i4FsMIEcfmStackIfIndex,
                               INT4 i4FsMIEcfmStackVlanIdOrNone,
                               INT4 i4FsMIEcfmStackMdLevel,
                               INT4 i4FsMIEcfmStackDirection,
                               tMacAddr * pRetValFsMIEcfmStackMacAddress)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmStackServiceSelectorType;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmStackIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmStackVlanIdOrNone >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmStackVlanIdOrNone =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmStackVlanIdOrNone);
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_ISID;

    }
    else
    {
        i4FsMIEcfmStackServiceSelectorType = ECFM_SERVICE_SELECTION_VLAN;
    }
    i1RetVal = nmhGetIeee8021CfmStackMacAddress ((INT4) u2LocalPort,
                                                 i4FsMIEcfmStackServiceSelectorType,
                                                 i4FsMIEcfmStackVlanIdOrNone,
                                                 i4FsMIEcfmStackMdLevel,
                                                 i4FsMIEcfmStackDirection,
                                                 pRetValFsMIEcfmStackMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIEcfmVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmVlanTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmVlanTable (UINT4 u4FsMIEcfmContextId,
                                           INT4 i4FsMIEcfmVlanVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceIeee8021CfmVlanTable (u4FsMIEcfmContextId,
                                                      i4FsMIEcfmVlanVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmVlanTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmVlanTable (UINT4 *pu4FsMIEcfmContextId,
                                   INT4 *pi4FsMIEcfmVlanVid)
{
    return (nmhGetNextIndexFsMIEcfmVlanTable (0, pu4FsMIEcfmContextId, 0,
                                              pi4FsMIEcfmVlanVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmVlanTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmVlanVid
                nextFsMIEcfmVlanVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmVlanTable (UINT4 u4FsMIEcfmContextId,
                                  UINT4 *pu4NextFsMIEcfmContextId,
                                  INT4 i4FsMIEcfmVlanVid,
                                  INT4 *pi4NextFsMIEcfmVlanVid)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexIeee8021CfmVlanTable (u4FsMIEcfmContextId,
                                                 pu4NextFsMIEcfmContextId,
                                                 i4FsMIEcfmVlanVid,
                                                 (UINT4 *)
                                                 pi4NextFsMIEcfmVlanVid) ==
            SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexIeee8021CfmVlanTable (pu4NextFsMIEcfmContextId,
                                                 (UINT4 *)
                                                 pi4NextFsMIEcfmVlanVid) !=
           SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmVlanPrimaryVid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid

                The Object 
                retValFsMIEcfmVlanPrimaryVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmVlanPrimaryVid (UINT4 u4FsMIEcfmContextId, INT4 i4FsMIEcfmVlanVid,
                              INT4 *pi4RetValFsMIEcfmVlanPrimaryVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIeee8021CfmVlanPrimarySelector (u4FsMIEcfmContextId,
                                                     i4FsMIEcfmVlanVid,
                                                     (UINT4 *)
                                                     pi4RetValFsMIEcfmVlanPrimaryVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmVlanRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid

                The Object 
                retValFsMIEcfmVlanRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmVlanRowStatus (UINT4 u4FsMIEcfmContextId, INT4 i4FsMIEcfmVlanVid,
                             INT4 *pi4RetValFsMIEcfmVlanRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIeee8021CfmVlanRowStatus (u4FsMIEcfmContextId,
                                               i4FsMIEcfmVlanVid,
                                               pi4RetValFsMIEcfmVlanRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmVlanPrimaryVid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid

                The Object 
                setValFsMIEcfmVlanPrimaryVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmVlanPrimaryVid (UINT4 u4FsMIEcfmContextId,
                              INT4 i4FsMIEcfmVlanVid,
                              INT4 i4SetValFsMIEcfmVlanPrimaryVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetIeee8021CfmVlanPrimarySelector (u4FsMIEcfmContextId,
                                                     i4FsMIEcfmVlanVid,
                                                     i4SetValFsMIEcfmVlanPrimaryVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmVlanRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid

                The Object 
                setValFsMIEcfmVlanRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmVlanRowStatus (UINT4 u4FsMIEcfmContextId,
                             INT4 i4FsMIEcfmVlanVid,
                             INT4 i4SetValFsMIEcfmVlanRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetIeee8021CfmVlanRowStatus (u4FsMIEcfmContextId,
                                               i4FsMIEcfmVlanVid,
                                               i4SetValFsMIEcfmVlanRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmVlanPrimaryVid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid

                The Object 
                testValFsMIEcfmVlanPrimaryVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmVlanPrimaryVid (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                 INT4 i4FsMIEcfmVlanVid,
                                 INT4 i4TestValFsMIEcfmVlanPrimaryVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmVlanPrimarySelector (pu4ErrorCode,
                                                 u4FsMIEcfmContextId,
                                                 i4FsMIEcfmVlanVid,
                                                 i4TestValFsMIEcfmVlanPrimaryVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmVlanRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid

                The Object 
                testValFsMIEcfmVlanRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmVlanRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                INT4 i4FsMIEcfmVlanVid,
                                INT4 i4TestValFsMIEcfmVlanRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmVlanRowStatus (pu4ErrorCode, u4FsMIEcfmContextId,
                                           i4FsMIEcfmVlanVid,
                                           i4TestValFsMIEcfmVlanRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmVlanTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmVlanVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmVlanTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmDefaultMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmDefaultMdTable (UINT4 u4FsMIEcfmContextId,
                                                INT4
                                                i4FsMIEcfmDefaultMdPrimaryVid)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    i1RetVal =
        nmhValidateIndexInstanceIeee8021CfmDefaultMdTable
        (u4FsMIEcfmContextId,
         i4FsMIEcfmDefaultMdPrimarySelectorType, i4FsMIEcfmDefaultMdPrimaryVid);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmDefaultMdTable (UINT4 *pu4FsMIEcfmContextId,
                                        INT4 *pi4FsMIEcfmDefaultMdPrimaryVid)
{
    return (nmhGetNextIndexFsMIEcfmDefaultMdTable
            (0, pu4FsMIEcfmContextId, 0, pi4FsMIEcfmDefaultMdPrimaryVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid
                nextFsMIEcfmDefaultMdPrimaryVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmDefaultMdTable (UINT4 u4FsMIEcfmContextId,
                                       UINT4 *pu4NextFsMIEcfmContextId,
                                       INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                       INT4 *pi4NextFsMIEcfmDefaultMdPrimaryVid)
{
    UINT4               u4ContextId;
    INT4                i4NextFsMIEcfmDefaultMdPrimarySelectortype =
        ECFM_SERVICE_SELECTION_VLAN;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexIeee8021CfmDefaultMdTable
            (u4FsMIEcfmContextId,
             pu4NextFsMIEcfmContextId,
             i4FsMIEcfmDefaultMdPrimarySelectorType,
             &i4NextFsMIEcfmDefaultMdPrimarySelectortype,
             i4FsMIEcfmDefaultMdPrimaryVid,
             (UINT4 *) pi4NextFsMIEcfmDefaultMdPrimaryVid) == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            if (i4NextFsMIEcfmDefaultMdPrimarySelectortype ==
                ECFM_SERVICE_SELECTION_ISID)
            {
                *pi4NextFsMIEcfmDefaultMdPrimaryVid =
                    ECFM_ISID_TO_ISID_INTERNAL
                    (*pi4NextFsMIEcfmDefaultMdPrimaryVid);
            }
            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexIeee8021CfmDefaultMdTable
           (pu4NextFsMIEcfmContextId,
            &i4NextFsMIEcfmDefaultMdPrimarySelectortype,
            (UINT4 *) pi4NextFsMIEcfmDefaultMdPrimaryVid) != SNMP_SUCCESS);

    if (i4NextFsMIEcfmDefaultMdPrimarySelectortype ==
        ECFM_SERVICE_SELECTION_ISID)
    {
        *pi4NextFsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_TO_ISID_INTERNAL (*pi4NextFsMIEcfmDefaultMdPrimaryVid);
    }

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                retValFsMIEcfmDefaultMdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdStatus (UINT4 u4FsMIEcfmContextId,
                               INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                               INT4 *pi4RetValFsMIEcfmDefaultMdStatus)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmDefaultMdStatus (u4FsMIEcfmContextId,
                                          i4FsMIEcfmDefaultMdPrimarySelectorType,
                                          i4FsMIEcfmDefaultMdPrimaryVid,
                                          pi4RetValFsMIEcfmDefaultMdStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                retValFsMIEcfmDefaultMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdLevel (UINT4 u4FsMIEcfmContextId,
                              INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                              INT4 *pi4RetValFsMIEcfmDefaultMdLevel)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIeee8021CfmDefaultMdLevel (u4FsMIEcfmContextId,
                                                i4FsMIEcfmDefaultMdPrimarySelectorType,
                                                i4FsMIEcfmDefaultMdPrimaryVid,
                                                pi4RetValFsMIEcfmDefaultMdLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                retValFsMIEcfmDefaultMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdMhfCreation (UINT4 u4FsMIEcfmContextId,
                                    INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                    INT4 *pi4RetValFsMIEcfmDefaultMdMhfCreation)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetIeee8021CfmDefaultMdMhfCreation (u4FsMIEcfmContextId,
                                               i4FsMIEcfmDefaultMdPrimarySelectorType,
                                               i4FsMIEcfmDefaultMdPrimaryVid,
                                               pi4RetValFsMIEcfmDefaultMdMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDefaultMdIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                retValFsMIEcfmDefaultMdIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDefaultMdIdPermission (UINT4 u4FsMIEcfmContextId,
                                     INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                     INT4
                                     *pi4RetValFsMIEcfmDefaultMdIdPermission)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmDefaultMdIdPermission (u4FsMIEcfmContextId,
                                                i4FsMIEcfmDefaultMdPrimarySelectorType,
                                                i4FsMIEcfmDefaultMdPrimaryVid,
                                                pi4RetValFsMIEcfmDefaultMdIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDefaultMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                setValFsMIEcfmDefaultMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDefaultMdLevel (UINT4 u4FsMIEcfmContextId,
                              INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                              INT4 i4SetValFsMIEcfmDefaultMdLevel)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetIeee8021CfmDefaultMdLevel (u4FsMIEcfmContextId,
                                                i4FsMIEcfmDefaultMdPrimarySelectorType,
                                                i4FsMIEcfmDefaultMdPrimaryVid,
                                                i4SetValFsMIEcfmDefaultMdLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDefaultMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                setValFsMIEcfmDefaultMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDefaultMdMhfCreation (UINT4 u4FsMIEcfmContextId,
                                    INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                    INT4 i4SetValFsMIEcfmDefaultMdMhfCreation)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmDefaultMdMhfCreation (u4FsMIEcfmContextId,
                                               i4FsMIEcfmDefaultMdPrimarySelectorType,
                                               i4FsMIEcfmDefaultMdPrimaryVid,
                                               i4SetValFsMIEcfmDefaultMdMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDefaultMdIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                setValFsMIEcfmDefaultMdIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDefaultMdIdPermission (UINT4 u4FsMIEcfmContextId,
                                     INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                     INT4 i4SetValFsMIEcfmDefaultMdIdPermission)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmDefaultMdIdPermission (u4FsMIEcfmContextId,
                                                i4FsMIEcfmDefaultMdPrimarySelectorType,
                                                i4FsMIEcfmDefaultMdPrimaryVid,
                                                i4SetValFsMIEcfmDefaultMdIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDefaultMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                testValFsMIEcfmDefaultMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDefaultMdLevel (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                 INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                 INT4 i4TestValFsMIEcfmDefaultMdLevel)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Ieee8021CfmDefaultMdLevel
        (pu4ErrorCode,
         u4FsMIEcfmContextId,
         i4FsMIEcfmDefaultMdPrimarySelectorType,
         i4FsMIEcfmDefaultMdPrimaryVid, i4TestValFsMIEcfmDefaultMdLevel);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDefaultMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                testValFsMIEcfmDefaultMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDefaultMdMhfCreation (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIEcfmContextId,
                                       INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                       INT4
                                       i4TestValFsMIEcfmDefaultMdMhfCreation)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Ieee8021CfmDefaultMdMhfCreation
        (pu4ErrorCode,
         u4FsMIEcfmContextId,
         i4FsMIEcfmDefaultMdPrimarySelectorType,
         i4FsMIEcfmDefaultMdPrimaryVid, i4TestValFsMIEcfmDefaultMdMhfCreation);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDefaultMdIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid

                The Object 
                testValFsMIEcfmDefaultMdIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDefaultMdIdPermission (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIEcfmContextId,
                                        INT4 i4FsMIEcfmDefaultMdPrimaryVid,
                                        INT4
                                        i4TestValFsMIEcfmDefaultMdIdPermission)
{
    INT1                i1RetVal;
    INT4                i4FsMIEcfmDefaultMdPrimarySelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmDefaultMdPrimaryVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmDefaultMdPrimaryVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmDefaultMdPrimaryVid);
        i4FsMIEcfmDefaultMdPrimarySelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Ieee8021CfmDefaultMdIdPermission
        (pu4ErrorCode,
         u4FsMIEcfmContextId,
         i4FsMIEcfmDefaultMdPrimarySelectorType,
         i4FsMIEcfmDefaultMdPrimaryVid, i4TestValFsMIEcfmDefaultMdIdPermission);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmDefaultMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmDefaultMdPrimaryVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmDefaultMdTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmConfigErrorListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmConfigErrorListTable
 Input       :  The Indices
                FsMIEcfmConfigErrorListVid
                FsMIEcfmConfigErrorListIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmConfigErrorListTable (INT4
                                                      i4FsMIEcfmConfigErrorListVid,
                                                      INT4
                                                      i4FsMIEcfmConfigErrorListIfIndex)
{
    tEcfmCcConfigErrInfo *pConfigErrInfo = NULL;
    tEcfmCcConfigErrInfo ConfigErrIndex;
    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ConfigErrIndex.u4IfIndex = i4FsMIEcfmConfigErrorListIfIndex;
    ConfigErrIndex.u4VidIsid = i4FsMIEcfmConfigErrorListVid;
    pConfigErrInfo =
        (tEcfmCcConfigErrInfo *) RBTreeGet (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE,
                                            &ConfigErrIndex);
    if (pConfigErrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmConfigErrorListTable
 Input       :  The Indices
                FsMIEcfmConfigErrorListVid
                FsMIEcfmConfigErrorListIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmConfigErrorListTable (INT4
                                              *pi4FsMIEcfmConfigErrorListVid,
                                              INT4
                                              *pi4FsMIEcfmConfigErrorListIfIndex)
{
    return (nmhGetNextIndexFsMIEcfmConfigErrorListTable
            (0, pi4FsMIEcfmConfigErrorListVid, 0,
             pi4FsMIEcfmConfigErrorListIfIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmConfigErrorListTable
 Input       :  The Indices
                FsMIEcfmConfigErrorListVid
                nextFsMIEcfmConfigErrorListVid
                FsMIEcfmConfigErrorListIfIndex
                nextFsMIEcfmConfigErrorListIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmConfigErrorListTable (INT4 i4FsMIEcfmConfigErrorListVid,
                                             INT4
                                             *pi4NextFsMIEcfmConfigErrorListVid,
                                             INT4
                                             i4FsMIEcfmConfigErrorListIfIndex,
                                             INT4
                                             *pi4NextFsMIEcfmConfigErrorListIfIndex)
{
    tEcfmCcConfigErrInfo *pNextConfigErrInfo = NULL;
    tEcfmCcConfigErrInfo ConfigErrIndex;
    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&ConfigErrIndex, 0x00, sizeof (tEcfmCcConfigErrInfo));
    ConfigErrIndex.u4IfIndex = i4FsMIEcfmConfigErrorListIfIndex;
    ConfigErrIndex.u4VidIsid = i4FsMIEcfmConfigErrorListVid;
    pNextConfigErrInfo = (tEcfmCcConfigErrInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE, (tRBElem *) & ConfigErrIndex, NULL);
    if (pNextConfigErrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIEcfmConfigErrorListIfIndex = pNextConfigErrInfo->u4IfIndex;
    *pi4NextFsMIEcfmConfigErrorListVid = pNextConfigErrInfo->u4VidIsid;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmConfigErrorListErrorType
 Input       :  The Indices
                FsMIEcfmConfigErrorListVid
                FsMIEcfmConfigErrorListIfIndex

                The Object 
                retValFsMIEcfmConfigErrorListErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmConfigErrorListErrorType (INT4 i4FsMIEcfmConfigErrorListVid,
                                        INT4 i4FsMIEcfmConfigErrorListIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIEcfmConfigErrorListErrorType)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    INT4                i4FsMIEcfmConfigErrorListSelectorType =
        ECFM_SERVICE_SELECTION_VLAN;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX
        ((UINT4) i4FsMIEcfmConfigErrorListIfIndex, &u4ContextId,
         &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Ieee802.1ag Standard MIB has SelectorType and SelectorValue 
     * as Seperate Indices. So Internal Vlan Id is converted to 
     * ServiceSelectoType and ServiceSelectorValue
     */
    if (i4FsMIEcfmConfigErrorListVid >= ECFM_INTERNAL_ISID_MIN)
    {
        i4FsMIEcfmConfigErrorListVid =
            ECFM_ISID_INTERNAL_TO_ISID (i4FsMIEcfmConfigErrorListVid);
        i4FsMIEcfmConfigErrorListSelectorType = ECFM_SERVICE_SELECTION_ISID;
    }

    i1RetVal =
        nmhGetIeee8021CfmConfigErrorListErrorType
        (i4FsMIEcfmConfigErrorListSelectorType,
         i4FsMIEcfmConfigErrorListVid, (INT4) u2LocalPort,
         pRetValFsMIEcfmConfigErrorListErrorType);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMdTable (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1agCfmMdTable (u4FsMIEcfmMdIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMdTable (UINT4 *pu4FsMIEcfmContextId,
                                 UINT4 *pu4FsMIEcfmMdIndex)
{
    return (nmhGetNextIndexFsMIEcfmMdTable (0, pu4FsMIEcfmContextId, 0,
                                            pu4FsMIEcfmMdIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMdTable (UINT4 u4FsMIEcfmContextId,
                                UINT4 *pu4NextFsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 *pu4NextFsMIEcfmMdIndex)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexDot1agCfmMdTable (u4FsMIEcfmMdIndex,
                                             pu4NextFsMIEcfmMdIndex) ==
            SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexDot1agCfmMdTable (pu4NextFsMIEcfmMdIndex)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdFormat (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        INT4 *pi4RetValFsMIEcfmMdFormat)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMdFormat (u4FsMIEcfmMdIndex, pi4RetValFsMIEcfmMdFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdName (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmMdName)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMdName (u4FsMIEcfmMdIndex, pRetValFsMIEcfmMdName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdMdLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdMdLevel (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         INT4 *pi4RetValFsMIEcfmMdMdLevel)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMdMdLevel (u4FsMIEcfmMdIndex,
                                  pi4RetValFsMIEcfmMdMdLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdMhfCreation (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             INT4 *pi4RetValFsMIEcfmMdMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMdMhfCreation (u4FsMIEcfmMdIndex,
                                             pi4RetValFsMIEcfmMdMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdMhfIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdMhfIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdMhfIdPermission (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 INT4 *pi4RetValFsMIEcfmMdMhfIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMdMhfIdPermission (u4FsMIEcfmMdIndex,
                                                 pi4RetValFsMIEcfmMdMhfIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdMaTableNextIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdMaTableNextIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdMaTableNextIndex (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 *pu4RetValFsMIEcfmMdMaTableNextIndex)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMdMaNextIndex (u4FsMIEcfmMdIndex,
                                             pu4RetValFsMIEcfmMdMaTableNextIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMdRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMdRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMdRowStatus (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           INT4 *pi4RetValFsMIEcfmMdRowStatus)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMdRowStatus (u4FsMIEcfmMdIndex,
                                           pi4RetValFsMIEcfmMdRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMdFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMdFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMdFormat (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        INT4 i4SetValFsMIEcfmMdFormat)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMdFormat (u4FsMIEcfmMdIndex, i4SetValFsMIEcfmMdFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMdName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMdName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMdName (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsMIEcfmMdName)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMdName (u4FsMIEcfmMdIndex, pSetValFsMIEcfmMdName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMdMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMdMdLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMdMdLevel (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         INT4 i4SetValFsMIEcfmMdMdLevel)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMdMdLevel (u4FsMIEcfmMdIndex, i4SetValFsMIEcfmMdMdLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMdMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMdMhfCreation (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex,
                             INT4 i4SetValFsMIEcfmMdMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMdMhfCreation (u4FsMIEcfmMdIndex,
                                             i4SetValFsMIEcfmMdMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMdMhfIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMdMhfIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMdMhfIdPermission (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 INT4 i4SetValFsMIEcfmMdMhfIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMdMhfIdPermission (u4FsMIEcfmMdIndex,
                                                 i4SetValFsMIEcfmMdMhfIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMdRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMdRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMdRowStatus (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           INT4 i4SetValFsMIEcfmMdRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMdRowStatus (u4FsMIEcfmMdIndex,
                                    i4SetValFsMIEcfmMdRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMdFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMdFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMdFormat (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex,
                           INT4 i4TestValFsMIEcfmMdFormat)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMdFormat (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                           i4TestValFsMIEcfmMdFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMdName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMdName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMdName (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                         UINT4 u4FsMIEcfmMdIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMIEcfmMdName)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMdName (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                         pTestValFsMIEcfmMdName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMdMdLevel
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMdMdLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMdMdLevel (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                            UINT4 u4FsMIEcfmMdIndex,
                            INT4 i4TestValFsMIEcfmMdMdLevel)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMdMdLevel (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                            i4TestValFsMIEcfmMdMdLevel);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMdMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMdMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMdMhfCreation (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                INT4 i4TestValFsMIEcfmMdMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMdMhfCreation (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                         i4TestValFsMIEcfmMdMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMdMhfIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMdMhfIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMdMhfIdPermission (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    INT4 i4TestValFsMIEcfmMdMhfIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMdMhfIdPermission (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             i4TestValFsMIEcfmMdMhfIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMdRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMdRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMdRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              INT4 i4TestValFsMIEcfmMdRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMdRowStatus (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                       i4TestValFsMIEcfmMdRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMdTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMdTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMaTable (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmMaIndex)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* MaTable should display the entries even when 
     * MaComp Table is not created */
    i1RetVal = nmhValidateIndexInstanceDot1agCfmMaNetTable
        (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMaTable (UINT4 *pu4FsMIEcfmContextId,
                                 UINT4 *pu4FsMIEcfmMdIndex,
                                 UINT4 *pu4FsMIEcfmMaIndex)
{
    return (nmhGetNextIndexFsMIEcfmMaTable (0, pu4FsMIEcfmContextId, 0,
                                            pu4FsMIEcfmMdIndex, 0,
                                            pu4FsMIEcfmMaIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMaTable (UINT4 u4FsMIEcfmContextId,
                                UINT4 *pu4NextFsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 *pu4NextFsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 *pu4NextFsMIEcfmMaIndex)
{
    UINT4               u4PrevContextId = ECFM_INIT_VAL;

    /* MaTable should display the entries even when 
     * MaComp entries are not created*/
    do
    {
        if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
        {
            if (nmhGetNextIndexDot1agCfmMaNetTable (u4FsMIEcfmMdIndex,
                                                    pu4NextFsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    pu4NextFsMIEcfmMaIndex)
                == SNMP_SUCCESS)
            {
                *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;
                ECFM_CC_RELEASE_CONTEXT ();
                return SNMP_SUCCESS;
            }
        }

        u4PrevContextId = u4FsMIEcfmContextId;
        u4FsMIEcfmMdIndex = ECFM_INIT_VAL;
        u4FsMIEcfmMaIndex = ECFM_INIT_VAL;

    }
    while (EcfmCcGetNextActiveContext (u4PrevContextId,
                                       &u4FsMIEcfmContextId) == ECFM_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaPrimaryVlanId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaPrimaryVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaPrimaryVlanId (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               INT4 *pi4RetValFsMIEcfmMaPrimaryVlanId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmMaCompPrimarySelectorOrNone (u4FsMIEcfmContextId,
                                                      u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      (UINT4 *)
                                                      pi4RetValFsMIEcfmMaPrimaryVlanId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaFormat (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        UINT4 u4FsMIEcfmMaIndex,
                        INT4 *pi4RetValFsMIEcfmMaFormat)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaNetFormat (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                    pi4RetValFsMIEcfmMaFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaName (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                      UINT4 u4FsMIEcfmMaIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmMaName)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMaNetName (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                         pRetValFsMIEcfmMaName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaMhfCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaMhfCreation (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             INT4 *pi4RetValFsMIEcfmMaMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetIeee8021CfmMaCompMhfCreation (u4FsMIEcfmContextId,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            pi4RetValFsMIEcfmMaMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaIdPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaIdPermission (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              INT4 *pi4RetValFsMIEcfmMaIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmMaCompIdPermission (u4FsMIEcfmContextId,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             pi4RetValFsMIEcfmMaIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaCcmInterval
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaCcmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaCcmInterval (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             INT4 *pi4RetValFsMIEcfmMaCcmInterval)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaNetCcmInterval (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                         pi4RetValFsMIEcfmMaCcmInterval);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaNumberOfVids
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaNumberOfVids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaNumberOfVids (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              UINT4 *pu4RetValFsMIEcfmMaNumberOfVids)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetIeee8021CfmMaCompNumberOfVids (u4FsMIEcfmContextId,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             pu4RetValFsMIEcfmMaNumberOfVids);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaRowStatus (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           INT4 *pi4RetValFsMIEcfmMaRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1agCfmMaNetRowStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              pi4RetValFsMIEcfmMaRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaPrimaryVlanId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaPrimaryVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaPrimaryVlanId (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmMaIndex,
                               INT4 i4SetValFsMIEcfmMaPrimaryVlanId)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmMaCompPrimarySelectorOrNone (u4FsMIEcfmContextId,
                                                      u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      i4SetValFsMIEcfmMaPrimaryVlanId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaFormat (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        UINT4 u4FsMIEcfmMaIndex, INT4 i4SetValFsMIEcfmMaFormat)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMaNetFormat (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                    i4SetValFsMIEcfmMaFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaName (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                      UINT4 u4FsMIEcfmMaIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsMIEcfmMaName)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMaNetName (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                         pSetValFsMIEcfmMaName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaMhfCreation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaMhfCreation (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             INT4 i4SetValFsMIEcfmMaMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmMaCompMhfCreation (u4FsMIEcfmContextId,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            i4SetValFsMIEcfmMaMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaIdPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaIdPermission (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              INT4 i4SetValFsMIEcfmMaIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetIeee8021CfmMaCompIdPermission (u4FsMIEcfmContextId,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             i4SetValFsMIEcfmMaIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaCcmInterval
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaCcmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaCcmInterval (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             INT4 i4SetValFsMIEcfmMaCcmInterval)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMaNetCcmInterval (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                         i4SetValFsMIEcfmMaCcmInterval);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaRowStatus (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           INT4 i4SetValFsMIEcfmMaRowStatus)
{
    INT1                i1RetVal;
    INT1                i1MaCompRetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMaNetRowStatus (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       i4SetValFsMIEcfmMaRowStatus);
    i1MaCompRetVal =
        nmhSetIeee8021CfmMaCompRowStatus (u4FsMIEcfmContextId,
                                          u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                          i4SetValFsMIEcfmMaRowStatus);

    if ((SNMP_SUCCESS == i1RetVal) && (SNMP_SUCCESS == i1MaCompRetVal))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaPrimaryVlanId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaPrimaryVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaPrimaryVlanId (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  INT4 i4TestValFsMIEcfmMaPrimaryVlanId)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone (pu4ErrorCode,
                                                         u4FsMIEcfmContextId,
                                                         u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         i4TestValFsMIEcfmMaPrimaryVlanId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaFormat
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaFormat (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                           INT4 i4TestValFsMIEcfmMaFormat)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMaNetFormat (pu4ErrorCode,
                                              u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              i4TestValFsMIEcfmMaFormat);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaName
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaName (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                         UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMIEcfmMaName)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMaNetName (pu4ErrorCode,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            pTestValFsMIEcfmMaName);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaMhfCreation
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaMhfCreation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaMhfCreation (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                INT4 i4TestValFsMIEcfmMaMhfCreation)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmMaCompMhfCreation (pu4ErrorCode,
                                               u4FsMIEcfmContextId,
                                               u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmMaIndex,
                                               i4TestValFsMIEcfmMaMhfCreation);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaIdPermission
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaIdPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaIdPermission (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 INT4 i4TestValFsMIEcfmMaIdPermission)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Ieee8021CfmMaCompIdPermission (pu4ErrorCode,
                                                u4FsMIEcfmContextId,
                                                u4FsMIEcfmMdIndex,
                                                u4FsMIEcfmMaIndex,
                                                i4TestValFsMIEcfmMaIdPermission);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaCcmInterval
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaCcmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaCcmInterval (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                INT4 i4TestValFsMIEcfmMaCcmInterval)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMaNetCcmInterval (pu4ErrorCode,
                                            u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            i4TestValFsMIEcfmMaCcmInterval);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              INT4 i4TestValFsMIEcfmMaRowStatus)
{
    INT1                i1MaNetRetVal;
    INT1                i1MaCompRetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1MaNetRetVal =
        nmhTestv2Dot1agCfmMaNetRowStatus (pu4ErrorCode,
                                          u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          i4TestValFsMIEcfmMaRowStatus);
    i1MaCompRetVal =
        nmhTestv2Ieee8021CfmMaCompRowStatus (pu4ErrorCode,
                                             u4FsMIEcfmContextId,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             i4TestValFsMIEcfmMaRowStatus);
    if ((SNMP_SUCCESS == i1MaNetRetVal) && (SNMP_SUCCESS == i1MaCompRetVal))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_SUCCESS;
    }
    else
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return SNMP_FAILURE;

    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMaTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMaTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMaMepListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMaMepListTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMaMepListTable (UINT4 u4FsMIEcfmContextId,
                                                UINT4 u4FsMIEcfmMdIndex,
                                                UINT4 u4FsMIEcfmMaIndex,
                                                UINT4
                                                u4FsMIEcfmMaMepListIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1agCfmMaMepListTable (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMaMepListIdentifier);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMaMepListTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMaMepListTable (UINT4 *pu4FsMIEcfmContextId,
                                        UINT4 *pu4FsMIEcfmMdIndex,
                                        UINT4 *pu4FsMIEcfmMaIndex,
                                        UINT4 *pu4FsMIEcfmMaMepListIdentifier)
{
    return (nmhGetNextIndexFsMIEcfmMaMepListTable
            (0, pu4FsMIEcfmContextId, 0, pu4FsMIEcfmMdIndex, 0,
             pu4FsMIEcfmMaIndex, 0, pu4FsMIEcfmMaMepListIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMaMepListTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier
                nextFsMIEcfmMaMepListIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMaMepListTable (UINT4 u4FsMIEcfmContextId,
                                       UINT4 *pu4NextFsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 *pu4NextFsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 *pu4NextFsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMaMepListIdentifier,
                                       UINT4
                                       *pu4NextFsMIEcfmMaMepListIdentifier)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexDot1agCfmMaMepListTable (u4FsMIEcfmMdIndex,
                                                    pu4NextFsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    pu4NextFsMIEcfmMaIndex,
                                                    u4FsMIEcfmMaMepListIdentifier,
                                                    pu4NextFsMIEcfmMaMepListIdentifier)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexDot1agCfmMaMepListTable (pu4NextFsMIEcfmMdIndex,
                                                    pu4NextFsMIEcfmMaIndex,
                                                    pu4NextFsMIEcfmMaMepListIdentifier)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaMepListRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier

                The Object 
                retValFsMIEcfmMaMepListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaMepListRowStatus (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMaMepListIdentifier,
                                  INT4 *pi4RetValFsMIEcfmMaMepListRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMaMepListRowStatus (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMaMepListIdentifier,
                                           pi4RetValFsMIEcfmMaMepListRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaMepListRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier

                The Object 
                setValFsMIEcfmMaMepListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsMIEcfmMaMepListRowStatus (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMaMepListIdentifier,
                                  INT4 i4SetValFsMIEcfmMaMepListRowStatus)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMaMepListRowStatus (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMaMepListIdentifier,
                                           i4SetValFsMIEcfmMaMepListRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaMepListRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier

                The Object 
                testValFsMIEcfmMaMepListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaMepListRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMaMepListIdentifier,
                                     INT4 i4TestValFsMIEcfmMaMepListRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMaMepListRowStatus (pu4ErrorCode,
                                              u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMaMepListIdentifier,
                                              i4TestValFsMIEcfmMaMepListRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMaMepListTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMaMepListIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMaMepListTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMepTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMepTable (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1agCfmMepTable (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMepTable (UINT4 *pu4FsMIEcfmContextId,
                                  UINT4 *pu4FsMIEcfmMdIndex,
                                  UINT4 *pu4FsMIEcfmMaIndex,
                                  UINT4 *pu4FsMIEcfmMepIdentifier)
{
    return (nmhGetNextIndexFsMIEcfmMepTable (0, pu4FsMIEcfmContextId, 0,
                                             pu4FsMIEcfmMdIndex, 0,
                                             pu4FsMIEcfmMaIndex, 0,
                                             pu4FsMIEcfmMepIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                nextFsMIEcfmMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMepTable (UINT4 u4FsMIEcfmContextId,
                                 UINT4 *pu4NextFsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 *pu4NextFsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 *pu4NextFsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 *pu4NextFsMIEcfmMepIdentifier)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexDot1agCfmMepTable (u4FsMIEcfmMdIndex,
                                              pu4NextFsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              pu4NextFsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              pu4NextFsMIEcfmMepIdentifier)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexDot1agCfmMepTable (pu4NextFsMIEcfmMdIndex,
                                              pu4NextFsMIEcfmMaIndex,
                                              pu4NextFsMIEcfmMepIdentifier)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepIfIndex (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                          UINT4 u4FsMIEcfmMepIdentifier,
                          INT4 *pi4RetValFsMIEcfmMepIfIndex)
{
    INT1                i1RetVal;
    UINT4               u4LocalPort = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pPortInfo = NULL;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepIfIndex (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   (INT4 *) &u4LocalPort);
    pPortInfo = ECFM_CC_GET_PORT_INFO (u4LocalPort);
    if (pPortInfo != NULL)
    {
        *pi4RetValFsMIEcfmMepIfIndex = (INT4) pPortInfo->u4IfIndex;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDirection
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDirection (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            INT4 *pi4RetValFsMIEcfmMepDirection)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDirection (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     pi4RetValFsMIEcfmMepDirection);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepPrimaryVid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepPrimaryVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepPrimaryVid (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 *pu4RetValFsMIEcfmMepPrimaryVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepPrimaryVid (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      pu4RetValFsMIEcfmMepPrimaryVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepActive (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                         INT4 *pi4RetValFsMIEcfmMepActive)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepActive (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                  u4FsMIEcfmMepIdentifier,
                                  pi4RetValFsMIEcfmMepActive);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepFngState
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepFngState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepFngState (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4 u4FsMIEcfmMepIdentifier,
                           INT4 *pi4RetValFsMIEcfmMepFngState)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepFngState (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                    u4FsMIEcfmMepIdentifier,
                                    pi4RetValFsMIEcfmMepFngState);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepCciEnabled
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepCciEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepCciEnabled (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             INT4 *pi4RetValFsMIEcfmMepCciEnabled)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCciEnabled (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      pi4RetValFsMIEcfmMepCciEnabled);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepCcmLtmPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepCcmLtmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepCcmLtmPriority (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 *pu4RetValFsMIEcfmMepCcmLtmPriority)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCcmLtmPriority (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          pu4RetValFsMIEcfmMepCcmLtmPriority);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepMacAddress (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             tMacAddr * pRetValFsMIEcfmMepMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepMacAddress (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      pRetValFsMIEcfmMepMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepLowPrDef
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepLowPrDef
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepLowPrDef (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4 u4FsMIEcfmMepIdentifier,
                           INT4 *pi4RetValFsMIEcfmMepLowPrDef)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLowPrDef (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                    u4FsMIEcfmMepIdentifier,
                                    pi4RetValFsMIEcfmMepLowPrDef);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepFngAlarmTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepFngAlarmTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepFngAlarmTime (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               INT4 *pi4RetValFsMIEcfmMepFngAlarmTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepFngAlarmTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        pi4RetValFsMIEcfmMepFngAlarmTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepFngResetTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepFngResetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepFngResetTime (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               INT4 *pi4RetValFsMIEcfmMepFngResetTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepFngResetTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        pi4RetValFsMIEcfmMepFngResetTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepHighestPrDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepHighestPrDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepHighestPrDefect (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMepIdentifier,
                                  INT4 *pi4RetValFsMIEcfmMepHighestPrDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepHighestPrDefect (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           pi4RetValFsMIEcfmMepHighestPrDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDefects
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDefects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDefects (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                          UINT4 u4FsMIEcfmMaIndex,
                          UINT4 u4FsMIEcfmMepIdentifier,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMIEcfmMepDefects)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDefects (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   pRetValFsMIEcfmMepDefects);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepErrorCcmLastFailure
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepErrorCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepErrorCcmLastFailure (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmMaIndex,
                                      UINT4 u4FsMIEcfmMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIEcfmMepErrorCcmLastFailure)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepErrorCcmLastFailure (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      pRetValFsMIEcfmMepErrorCcmLastFailure);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepXconCcmLastFailure
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepXconCcmLastFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepXconCcmLastFailure (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIEcfmMepXconCcmLastFailure)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepXconCcmLastFailure (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     pRetValFsMIEcfmMepXconCcmLastFailure);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepCcmSequenceErrors
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepCcmSequenceErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepCcmSequenceErrors (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMIEcfmMepCcmSequenceErrors)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCcmSequenceErrors (u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             pu4RetValFsMIEcfmMepCcmSequenceErrors);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepCciSentCcms
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepCciSentCcms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepCciSentCcms (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              UINT4 u4FsMIEcfmMepIdentifier,
                              UINT4 *pu4RetValFsMIEcfmMepCciSentCcms)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepCciSentCcms (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       pu4RetValFsMIEcfmMepCciSentCcms);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepNextLbmTransId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepNextLbmTransId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepNextLbmTransId (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 *pu4RetValFsMIEcfmMepNextLbmTransId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepNextLbmTransId (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          pu4RetValFsMIEcfmMepNextLbmTransId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepLbrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepLbrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepLbrIn (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                        UINT4 *pu4RetValFsMIEcfmMepLbrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrIn (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 pu4RetValFsMIEcfmMepLbrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepLbrInOutOfOrder
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepLbrInOutOfOrder
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepLbrInOutOfOrder (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMepIdentifier,
                                  UINT4 *pu4RetValFsMIEcfmMepLbrInOutOfOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrInOutOfOrder (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           pu4RetValFsMIEcfmMepLbrInOutOfOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepLbrBadMsdu
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepLbrBadMsdu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepLbrBadMsdu (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 *pu4RetValFsMIEcfmMepLbrBadMsdu)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrBadMsdu (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      pu4RetValFsMIEcfmMepLbrBadMsdu);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepLtmNextSeqNumber
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepLtmNextSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepLtmNextSeqNumber (UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   UINT4 *pu4RetValFsMIEcfmMepLtmNextSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLtmNextSeqNumber (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            u4FsMIEcfmMepIdentifier,
                                            pu4RetValFsMIEcfmMepLtmNextSeqNumber);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepUnexpLtrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepUnexpLtrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepUnexpLtrIn (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 *pu4RetValFsMIEcfmMepUnexpLtrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepUnexpLtrIn (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      pu4RetValFsMIEcfmMepUnexpLtrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepLbrOut
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepLbrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepLbrOut (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                         UINT4 *pu4RetValFsMIEcfmMepLbrOut)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepLbrOut (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                  u4FsMIEcfmMepIdentifier,
                                  pu4RetValFsMIEcfmMepLbrOut);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmStatus (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    INT4 *pi4RetValFsMIEcfmMepTransmitLbmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlGetAgMepTxLbmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              pi4RetValFsMIEcfmMepTransmitLbmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmDestMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmDestMacAddress (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            tMacAddr *
                                            pRetValFsMIEcfmMepTransmitLbmDestMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLbmDestMacAddress (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     pRetValFsMIEcfmMepTransmitLbmDestMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmDestMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmDestMepId (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       *pu4RetValFsMIEcfmMepTransmitLbmDestMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmDestMepId (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       pu4RetValFsMIEcfmMepTransmitLbmDestMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmDestIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmDestIsMepId (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmMaIndex,
                                         UINT4 u4FsMIEcfmMepIdentifier,
                                         INT4
                                         *pi4RetValFsMIEcfmMepTransmitLbmDestIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmDestIsMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMepIdentifier,
                                                         pi4RetValFsMIEcfmMepTransmitLbmDestIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmMessages
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmMessages (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmMaIndex,
                                      UINT4 u4FsMIEcfmMepIdentifier,
                                      INT4
                                      *pi4RetValFsMIEcfmMepTransmitLbmMessages)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmMessages (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      pi4RetValFsMIEcfmMepTransmitLbmMessages);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmDataTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmDataTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmDataTlv (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIEcfmMepTransmitLbmDataTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmDataTlv (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     pRetValFsMIEcfmMepTransmitLbmDataTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmVlanPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmVlanPriority (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier,
                                          INT4
                                          *pi4RetValFsMIEcfmMepTransmitLbmVlanPriority)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmVlanPriority (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          pi4RetValFsMIEcfmMepTransmitLbmVlanPriority);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmVlanDropEnable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmVlanDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmVlanDropEnable (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            INT4
                                            *pi4RetValFsMIEcfmMepTransmitLbmVlanDropEnable)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1agCfmMepTransmitLbmVlanDropEnable (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     pi4RetValFsMIEcfmMepTransmitLbmVlanDropEnable);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmResultOK
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmResultOK (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmMaIndex,
                                      UINT4 u4FsMIEcfmMepIdentifier,
                                      INT4
                                      *pi4RetValFsMIEcfmMepTransmitLbmResultOK)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmResultOK (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      pi4RetValFsMIEcfmMepTransmitLbmResultOK);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLbmSeqNumber
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLbmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLbmSeqNumber (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       *pu4RetValFsMIEcfmMepTransmitLbmSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLbmSeqNumber (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       pu4RetValFsMIEcfmMepTransmitLbmSeqNumber);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmStatus (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    INT4 *pi4RetValFsMIEcfmMepTransmitLtmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = EcfmMepUtlGetAgMepTxLtmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              pi4RetValFsMIEcfmMepTransmitLtmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmFlags
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmFlags (UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIEcfmMepTransmitLtmFlags)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmFlags (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            u4FsMIEcfmMepIdentifier,
                                            pRetValFsMIEcfmMepTransmitLtmFlags);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmTargetMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmTargetMacAddress (UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmMaIndex,
                                              UINT4 u4FsMIEcfmMepIdentifier,
                                              tMacAddr *
                                              pRetValFsMIEcfmMepTransmitLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmTargetMacAddress (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       pRetValFsMIEcfmMepTransmitLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmTargetMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmTargetMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmTargetMepId (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmMaIndex,
                                         UINT4 u4FsMIEcfmMepIdentifier,
                                         UINT4
                                         *pu4RetValFsMIEcfmMepTransmitLtmTargetMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLtmTargetMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMepIdentifier,
                                                         pu4RetValFsMIEcfmMepTransmitLtmTargetMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmTargetIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmTargetIsMepId (UINT4 u4FsMIEcfmContextId,
                                           UINT4 u4FsMIEcfmMdIndex,
                                           UINT4 u4FsMIEcfmMaIndex,
                                           UINT4 u4FsMIEcfmMepIdentifier,
                                           INT4
                                           *pi4RetValFsMIEcfmMepTransmitLtmTargetIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmTargetIsMepId (u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    u4FsMIEcfmMepIdentifier,
                                                    pi4RetValFsMIEcfmMepTransmitLtmTargetIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmTtl (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 *pu4RetValFsMIEcfmMepTransmitLtmTtl)
{

    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmTtl (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          pu4RetValFsMIEcfmMepTransmitLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmResult
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmResult (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    INT4 *pi4RetValFsMIEcfmMepTransmitLtmResult)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmResult (u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             pi4RetValFsMIEcfmMepTransmitLtmResult);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmSeqNumber
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmSeqNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmSeqNumber (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       *pu4RetValFsMIEcfmMepTransmitLtmSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepTransmitLtmSeqNumber (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       pu4RetValFsMIEcfmMepTransmitLtmSeqNumber);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepTransmitLtmEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepTransmitLtmEgressIdentifier (UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmMaIndex,
                                              UINT4 u4FsMIEcfmMepIdentifier,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValFsMIEcfmMepTransmitLtmEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepTransmitLtmEgressIdentifier (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       pRetValFsMIEcfmMepTransmitLtmEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepRowStatus (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            INT4 *pi4RetValFsMIEcfmMepRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepRowStatus (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     pi4RetValFsMIEcfmMepRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepCcmOffload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepCcmOffload (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             INT4 *pi4RetValFsMIEcfmMepCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepCcmOffload (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   pi4RetValFsMIEcfmMepCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepIfIndex (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                          UINT4 u4FsMIEcfmMepIdentifier,
                          INT4 i4SetValFsMIEcfmMepIfIndex)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4SetValFsMIEcfmMepIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepIfIndex (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier, (INT4) u2LocalPort);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepDirection
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepDirection (UINT4 u4FsMIEcfmContextId,
                            UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            INT4 i4SetValFsMIEcfmMepDirection)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepDirection (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     i4SetValFsMIEcfmMepDirection);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepPrimaryVid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepPrimaryVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepPrimaryVid (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 u4SetValFsMIEcfmMepPrimaryVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepPrimaryVid (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      u4SetValFsMIEcfmMepPrimaryVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepActive (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmMaIndex,
                         UINT4 u4FsMIEcfmMepIdentifier,
                         INT4 i4SetValFsMIEcfmMepActive)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepActive (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                  u4FsMIEcfmMepIdentifier,
                                  i4SetValFsMIEcfmMepActive);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepCciEnabled
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepCciEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepCciEnabled (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             INT4 i4SetValFsMIEcfmMepCciEnabled)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepCciEnabled (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      i4SetValFsMIEcfmMepCciEnabled);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepCcmLtmPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepCcmLtmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepCcmLtmPriority (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 u4SetValFsMIEcfmMepCcmLtmPriority)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepCcmLtmPriority (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          u4SetValFsMIEcfmMepCcmLtmPriority);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepLowPrDef
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepLowPrDef
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepLowPrDef (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4 u4FsMIEcfmMepIdentifier,
                           INT4 i4SetValFsMIEcfmMepLowPrDef)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepLowPrDef (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                    u4FsMIEcfmMepIdentifier,
                                    i4SetValFsMIEcfmMepLowPrDef);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepFngAlarmTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepFngAlarmTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepFngAlarmTime (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               INT4 i4SetValFsMIEcfmMepFngAlarmTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepFngAlarmTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        i4SetValFsMIEcfmMepFngAlarmTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepFngResetTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepFngResetTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepFngResetTime (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               INT4 i4SetValFsMIEcfmMepFngResetTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepFngResetTime (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        i4SetValFsMIEcfmMepFngResetTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepCcmSequenceErrors
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepCcmSequenceErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepCcmSequenceErrors (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    UINT4 u4SetValFsMIEcfmMepCcmSequenceErrors)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmMepCcmSequenceErrors
        (u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4SetValFsMIEcfmMepCcmSequenceErrors);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepCciSentCcms
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepCciSentCcms
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepCciSentCcms (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4 u4FsMIEcfmMepIdentifier,
                              UINT4 u4SetValFsMIEcfmMepCciSentCcms)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepCciSentCcms (u4FsMIEcfmMdIndex,
                                    u4FsMIEcfmMaIndex,
                                    u4FsMIEcfmMepIdentifier,
                                    u4SetValFsMIEcfmMepCciSentCcms);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepLbrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepLbrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepLbrIn (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        UINT4 u4FsMIEcfmMaIndex,
                        UINT4 u4FsMIEcfmMepIdentifier,
                        UINT4 u4SetValFsMIEcfmMepLbrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrIn (u4FsMIEcfmMdIndex,
                              u4FsMIEcfmMaIndex,
                              u4FsMIEcfmMepIdentifier,
                              u4SetValFsMIEcfmMepLbrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepLbrInOutOfOrder
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepLbrInOutOfOrder
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepLbrInOutOfOrder (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMepIdentifier,
                                  UINT4 u4SetValFsMIEcfmMepLbrInOutOfOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrInOutOfOrder (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        u4SetValFsMIEcfmMepLbrInOutOfOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepLbrBadMsdu
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepLbrBadMsdu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepLbrBadMsdu (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 u4SetValFsMIEcfmMepLbrBadMsdu)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrBadMsdu (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   u4SetValFsMIEcfmMepLbrBadMsdu);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepUnexpLtrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepUnexpLtrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepUnexpLtrIn (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 u4SetValFsMIEcfmMepUnexpLtrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepUnexpLtrIn (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   u4SetValFsMIEcfmMepUnexpLtrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepLbrOut
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepLbrOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepLbrOut (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmMaIndex,
                         UINT4 u4FsMIEcfmMepIdentifier,
                         UINT4 u4SetValFsMIEcfmMepLbrOut)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepLbrOut (u4FsMIEcfmMdIndex,
                               u4FsMIEcfmMaIndex,
                               u4FsMIEcfmMepIdentifier,
                               u4SetValFsMIEcfmMepLbrOut);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmStatus (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    INT4 i4SetValFsMIEcfmMepTransmitLbmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlSetAgMepTxLbmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              i4SetValFsMIEcfmMepTransmitLbmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmDestMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmDestMacAddress (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            tMacAddr
                                            SetValFsMIEcfmMepTransmitLbmDestMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLbmDestMacAddress (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     SetValFsMIEcfmMepTransmitLbmDestMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmDestMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmDestMepId (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       u4SetValFsMIEcfmMepTransmitLbmDestMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmDestMepId (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       u4SetValFsMIEcfmMepTransmitLbmDestMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmDestIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmDestIsMepId (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmMaIndex,
                                         UINT4 u4FsMIEcfmMepIdentifier,
                                         INT4
                                         i4SetValFsMIEcfmMepTransmitLbmDestIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmDestIsMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMepIdentifier,
                                                         i4SetValFsMIEcfmMepTransmitLbmDestIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmMessages
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmMessages (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmMaIndex,
                                      UINT4 u4FsMIEcfmMepIdentifier,
                                      INT4
                                      i4SetValFsMIEcfmMepTransmitLbmMessages)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmMessages (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      i4SetValFsMIEcfmMepTransmitLbmMessages);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmDataTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmDataTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmDataTlv (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsMIEcfmMepTransmitLbmDataTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLbmDataTlv (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     pSetValFsMIEcfmMepTransmitLbmDataTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmVlanPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmVlanPriority (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier,
                                          INT4
                                          i4SetValFsMIEcfmMepTransmitLbmVlanPriority)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1agCfmMepTransmitLbmVlanPriority (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          i4SetValFsMIEcfmMepTransmitLbmVlanPriority);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLbmVlanDropEnable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLbmVlanDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLbmVlanDropEnable (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            INT4
                                            i4SetValFsMIEcfmMepTransmitLbmVlanDropEnable)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLbmVlanDropEnable (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     i4SetValFsMIEcfmMepTransmitLbmVlanDropEnable);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmStatus (UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    INT4 i4SetValFsMIEcfmMepTransmitLtmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlSetAgMepTxLtmStatus (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              i4SetValFsMIEcfmMepTransmitLtmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmFlags
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmFlags (UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMIEcfmMepTransmitLtmFlags)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmFlags (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            u4FsMIEcfmMepIdentifier,
                                            pSetValFsMIEcfmMepTransmitLtmFlags);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmTargetMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmTargetMacAddress (UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmMaIndex,
                                              UINT4 u4FsMIEcfmMepIdentifier,
                                              tMacAddr
                                              SetValFsMIEcfmMepTransmitLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmTargetMacAddress (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       SetValFsMIEcfmMepTransmitLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmTargetMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmTargetMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmTargetMepId (UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmMaIndex,
                                         UINT4 u4FsMIEcfmMepIdentifier,
                                         UINT4
                                         u4SetValFsMIEcfmMepTransmitLtmTargetMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1agCfmMepTransmitLtmTargetMepId (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMepIdentifier,
                                                         u4SetValFsMIEcfmMepTransmitLtmTargetMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmTargetIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmTargetIsMepId (UINT4 u4FsMIEcfmContextId,
                                           UINT4 u4FsMIEcfmMdIndex,
                                           UINT4 u4FsMIEcfmMaIndex,
                                           UINT4 u4FsMIEcfmMepIdentifier,
                                           INT4
                                           i4SetValFsMIEcfmMepTransmitLtmTargetIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmTargetIsMepId (u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    u4FsMIEcfmMepIdentifier,
                                                    i4SetValFsMIEcfmMepTransmitLtmTargetIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmTtl (UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 u4SetValFsMIEcfmMepTransmitLtmTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmTtl (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          u4SetValFsMIEcfmMepTransmitLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepTransmitLtmEgressIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepTransmitLtmEgressIdentifier (UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmMaIndex,
                                              UINT4 u4FsMIEcfmMepIdentifier,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValFsMIEcfmMepTransmitLtmEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepTransmitLtmEgressIdentifier (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       pSetValFsMIEcfmMepTransmitLtmEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepRowStatus (UINT4 u4FsMIEcfmContextId,
                            UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            INT4 i4SetValFsMIEcfmMepRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1agCfmMepRowStatus (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     i4SetValFsMIEcfmMepRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepCcmOffload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepCcmOffload (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             INT4 i4SetValFsMIEcfmMepCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsEcfmMepCcmOffload (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   i4SetValFsMIEcfmMepCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepIfIndex (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             INT4 i4TestValFsMIEcfmMepIfIndex)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4TestValFsMIEcfmMepIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMepIfIndex (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             (INT4) u2LocalPort);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepDirection
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepDirection (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               INT4 i4TestValFsMIEcfmMepDirection)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepDirection (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        i4TestValFsMIEcfmMepDirection);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepPrimaryVid
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepPrimaryVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepPrimaryVid (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 u4FsMIEcfmMepIdentifier,
                                UINT4 u4TestValFsMIEcfmMepPrimaryVid)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMepPrimaryVid (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                         u4FsMIEcfmMaIndex,
                                         u4FsMIEcfmMepIdentifier,
                                         u4TestValFsMIEcfmMepPrimaryVid);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepActive
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepActive (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                            UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            INT4 i4TestValFsMIEcfmMepActive)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMepActive (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            u4FsMIEcfmMepIdentifier,
                                            i4TestValFsMIEcfmMepActive);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepCciEnabled
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepCciEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepCciEnabled (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 u4FsMIEcfmMepIdentifier,
                                INT4 i4TestValFsMIEcfmMepCciEnabled)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepCciEnabled (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                         u4FsMIEcfmMaIndex,
                                         u4FsMIEcfmMepIdentifier,
                                         i4TestValFsMIEcfmMepCciEnabled);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepCcmLtmPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepCcmLtmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepCcmLtmPriority (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    UINT4 u4TestValFsMIEcfmMepCcmLtmPriority)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepCcmLtmPriority (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             u4TestValFsMIEcfmMepCcmLtmPriority);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepLowPrDef
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepLowPrDef
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepLowPrDef (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              UINT4 u4FsMIEcfmMepIdentifier,
                              INT4 i4TestValFsMIEcfmMepLowPrDef)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepLowPrDef (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                       u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       i4TestValFsMIEcfmMepLowPrDef);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepFngAlarmTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepFngAlarmTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepFngAlarmTime (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMepIdentifier,
                                  INT4 i4TestValFsMIEcfmMepFngAlarmTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepFngAlarmTime (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           i4TestValFsMIEcfmMepFngAlarmTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepFngResetTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepFngResetTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepFngResetTime (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMepIdentifier,
                                  INT4 i4TestValFsMIEcfmMepFngResetTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1agCfmMepFngResetTime (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           i4TestValFsMIEcfmMepFngResetTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepCcmSequenceErrors
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepCcmSequenceErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepCcmSequenceErrors (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       u4TestValFsMIEcfmMepCcmSequenceErrors)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepCcmSequenceErrors
        (pu4ErrorCode, u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4TestValFsMIEcfmMepCcmSequenceErrors);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepCciSentCcms
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepCciSentCcms
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepCciSentCcms (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 u4TestValFsMIEcfmMepCciSentCcms)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepCciSentCcms
        (pu4ErrorCode, u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4TestValFsMIEcfmMepCciSentCcms);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepLbrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepLbrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepLbrIn (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                           UINT4 u4FsMIEcfmMepIdentifier,
                           UINT4 u4TestValFsMIEcfmMepLbrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepLbrIn
        (pu4ErrorCode, u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex, u4FsMIEcfmMepIdentifier, u4TestValFsMIEcfmMepLbrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepLbrInOutOfOrder
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepLbrInOutOfOrder
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepLbrInOutOfOrder (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     UINT4 u4TestValFsMIEcfmMepLbrInOutOfOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepLbrInOutOfOrder
        (pu4ErrorCode, u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4TestValFsMIEcfmMepLbrInOutOfOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepLbrBadMsdu
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepLbrBadMsdu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepLbrBadMsdu (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 u4FsMIEcfmMepIdentifier,
                                UINT4 u4TestValFsMIEcfmMepLbrBadMsdu)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepLbrBadMsdu
        (pu4ErrorCode, u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4TestValFsMIEcfmMepLbrBadMsdu);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepUnexpLtrIn
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepUnexpLtrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepUnexpLtrIn (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 u4FsMIEcfmMepIdentifier,
                                UINT4 u4TestValFsMIEcfmMepUnexpLtrIn)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepUnexpLtrIn
        (pu4ErrorCode, u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4TestValFsMIEcfmMepUnexpLtrIn);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepLbrOut
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepLbrOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepLbrOut (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                            UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            UINT4 u4TestValFsMIEcfmMepLbrOut)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMepLbrOut (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                  u4FsMIEcfmMaIndex,
                                  u4FsMIEcfmMepIdentifier,
                                  u4TestValFsMIEcfmMepLbrOut);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       INT4
                                       i4TestValFsMIEcfmMepTransmitLbmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = EcfmMepUtlTestv2AgMepTxLbmStatus (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 i4TestValFsMIEcfmMepTransmitLbmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmDestMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmDestMacAddress (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMIEcfmContextId,
                                               UINT4 u4FsMIEcfmMdIndex,
                                               UINT4 u4FsMIEcfmMaIndex,
                                               UINT4 u4FsMIEcfmMepIdentifier,
                                               tMacAddr
                                               TestValFsMIEcfmMepTransmitLbmDestMacAddress)
{

    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmDestMacAddress (pu4ErrorCode,
                                                               u4FsMIEcfmMdIndex,
                                                               u4FsMIEcfmMaIndex,
                                                               u4FsMIEcfmMepIdentifier,
                                                               TestValFsMIEcfmMepTransmitLbmDestMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmDestMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmDestMepId (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier,
                                          UINT4
                                          u4TestValFsMIEcfmMepTransmitLbmDestMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmDestMepId (pu4ErrorCode,
                                                          u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          u4TestValFsMIEcfmMepTransmitLbmDestMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmDestIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmDestIsMepId (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            INT4
                                            i4TestValFsMIEcfmMepTransmitLbmDestIsMepId)
{

    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmDestIsMepId (pu4ErrorCode,
                                                            u4FsMIEcfmMdIndex,
                                                            u4FsMIEcfmMaIndex,
                                                            u4FsMIEcfmMepIdentifier,
                                                            i4TestValFsMIEcfmMepTransmitLbmDestIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmMessages
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmMessages (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMIEcfmContextId,
                                         UINT4 u4FsMIEcfmMdIndex,
                                         UINT4 u4FsMIEcfmMaIndex,
                                         UINT4 u4FsMIEcfmMepIdentifier,
                                         INT4
                                         i4TestValFsMIEcfmMepTransmitLbmMessages)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLbmMessages (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  u4FsMIEcfmMaIndex,
                                                  u4FsMIEcfmMepIdentifier,
                                                  i4TestValFsMIEcfmMepTransmitLbmMessages);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmDataTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmDataTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmDataTlv (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIEcfmContextId,
                                        UINT4 u4FsMIEcfmMdIndex,
                                        UINT4 u4FsMIEcfmMaIndex,
                                        UINT4 u4FsMIEcfmMepIdentifier,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsMIEcfmMepTransmitLbmDataTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLbmDataTlv (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 pTestValFsMIEcfmMepTransmitLbmDataTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmVlanPriority
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmVlanPriority (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMIEcfmContextId,
                                             UINT4 u4FsMIEcfmMdIndex,
                                             UINT4 u4FsMIEcfmMaIndex,
                                             UINT4 u4FsMIEcfmMepIdentifier,
                                             INT4
                                             i4TestValFsMIEcfmMepTransmitLbmVlanPriority)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmVlanPriority (pu4ErrorCode,
                                                             u4FsMIEcfmMdIndex,
                                                             u4FsMIEcfmMaIndex,
                                                             u4FsMIEcfmMepIdentifier,
                                                             i4TestValFsMIEcfmMepTransmitLbmVlanPriority);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLbmVlanDropEnable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLbmVlanDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLbmVlanDropEnable (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMIEcfmContextId,
                                               UINT4 u4FsMIEcfmMdIndex,
                                               UINT4 u4FsMIEcfmMaIndex,
                                               UINT4 u4FsMIEcfmMepIdentifier,
                                               INT4
                                               i4TestValFsMIEcfmMepTransmitLbmVlanDropEnable)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLbmVlanDropEnable (pu4ErrorCode,
                                                               u4FsMIEcfmMdIndex,
                                                               u4FsMIEcfmMaIndex,
                                                               u4FsMIEcfmMepIdentifier,
                                                               i4TestValFsMIEcfmMepTransmitLbmVlanDropEnable);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       INT4
                                       i4TestValFsMIEcfmMepTransmitLtmStatus)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = EcfmMepUtlTestv2AgMepTxLtmStatus (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 i4TestValFsMIEcfmMepTransmitLtmStatus);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmFlags
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmFlags (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmMaIndex,
                                      UINT4 u4FsMIEcfmMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMIEcfmMepTransmitLtmFlags)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmFlags (pu4ErrorCode,
                                               u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmMaIndex,
                                               u4FsMIEcfmMepIdentifier,
                                               pTestValFsMIEcfmMepTransmitLtmFlags);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmTargetMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmTargetMacAddress (UINT4 *pu4ErrorCode,
                                                 UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmMaIndex,
                                                 UINT4 u4FsMIEcfmMepIdentifier,
                                                 tMacAddr
                                                 TestValFsMIEcfmMepTransmitLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmTargetMacAddress (pu4ErrorCode,
                                                          u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          TestValFsMIEcfmMepTransmitLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmTargetMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmTargetMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmTargetMepId (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            UINT4
                                            u4TestValFsMIEcfmMepTransmitLtmTargetMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLtmTargetMepId (pu4ErrorCode,
                                                            u4FsMIEcfmMdIndex,
                                                            u4FsMIEcfmMaIndex,
                                                            u4FsMIEcfmMepIdentifier,
                                                            u4TestValFsMIEcfmMepTransmitLtmTargetMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmTargetIsMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmTargetIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmTargetIsMepId (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsMIEcfmContextId,
                                              UINT4 u4FsMIEcfmMdIndex,
                                              UINT4 u4FsMIEcfmMaIndex,
                                              UINT4 u4FsMIEcfmMepIdentifier,
                                              INT4
                                              i4TestValFsMIEcfmMepTransmitLtmTargetIsMepId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1agCfmMepTransmitLtmTargetIsMepId (pu4ErrorCode,
                                                              u4FsMIEcfmMdIndex,
                                                              u4FsMIEcfmMaIndex,
                                                              u4FsMIEcfmMepIdentifier,
                                                              i4TestValFsMIEcfmMepTransmitLtmTargetIsMepId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmTtl (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIEcfmContextId,
                                    UINT4 u4FsMIEcfmMdIndex,
                                    UINT4 u4FsMIEcfmMaIndex,
                                    UINT4 u4FsMIEcfmMepIdentifier,
                                    UINT4 u4TestValFsMIEcfmMepTransmitLtmTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmTtl (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             u4TestValFsMIEcfmMepTransmitLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepTransmitLtmEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepTransmitLtmEgressIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepTransmitLtmEgressIdentifier (UINT4 *pu4ErrorCode,
                                                 UINT4 u4FsMIEcfmContextId,
                                                 UINT4 u4FsMIEcfmMdIndex,
                                                 UINT4 u4FsMIEcfmMaIndex,
                                                 UINT4 u4FsMIEcfmMepIdentifier,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pTestValFsMIEcfmMepTransmitLtmEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepTransmitLtmEgressIdentifier (pu4ErrorCode,
                                                          u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          pTestValFsMIEcfmMepTransmitLtmEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepRowStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               INT4 i4TestValFsMIEcfmMepRowStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1agCfmMepRowStatus (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        i4TestValFsMIEcfmMepRowStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepCcmOffload
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepCcmOffload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepCcmOffload (UINT4 *pu4ErrorCode, UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 u4FsMIEcfmMepIdentifier,
                                INT4 i4TestValFsMIEcfmMepCcmOffload)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMepCcmOffload (pu4ErrorCode, u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             i4TestValFsMIEcfmMepCcmOffload);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMepTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMepTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmLtrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmLtrTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmLtrTable (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier,
                                          UINT4 u4FsMIEcfmLtrSeqNumber,
                                          UINT4 u4FsMIEcfmLtrReceiveOrder)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1agCfmLtrTable (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          u4FsMIEcfmLtrSeqNumber,
                                                          u4FsMIEcfmLtrReceiveOrder);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmLtrTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmLtrTable (UINT4 *pu4FsMIEcfmContextId,
                                  UINT4 *pu4FsMIEcfmMdIndex,
                                  UINT4 *pu4FsMIEcfmMaIndex,
                                  UINT4 *pu4FsMIEcfmMepIdentifier,
                                  UINT4 *pu4FsMIEcfmLtrSeqNumber,
                                  UINT4 *pu4FsMIEcfmLtrReceiveOrder)
{
    return (nmhGetNextIndexFsMIEcfmLtrTable (0, pu4FsMIEcfmContextId, 0,
                                             pu4FsMIEcfmMdIndex, 0,
                                             pu4FsMIEcfmMaIndex, 0,
                                             pu4FsMIEcfmMepIdentifier, 0,
                                             pu4FsMIEcfmLtrSeqNumber, 0,
                                             pu4FsMIEcfmLtrReceiveOrder));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmLtrTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                nextFsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                nextFsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder
                nextFsMIEcfmLtrReceiveOrder
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmLtrTable (UINT4 u4FsMIEcfmContextId,
                                 UINT4 *pu4NextFsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 *pu4NextFsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 *pu4NextFsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 *pu4NextFsMIEcfmMepIdentifier,
                                 UINT4 u4FsMIEcfmLtrSeqNumber,
                                 UINT4 *pu4NextFsMIEcfmLtrSeqNumber,
                                 UINT4 u4FsMIEcfmLtrReceiveOrder,
                                 UINT4 *pu4NextFsMIEcfmLtrReceiveOrder)
{
    UINT4               u4ContextId;

    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexDot1agCfmLtrTable (u4FsMIEcfmMdIndex,
                                              pu4NextFsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              pu4NextFsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              pu4NextFsMIEcfmMepIdentifier,
                                              u4FsMIEcfmLtrSeqNumber,
                                              pu4NextFsMIEcfmLtrSeqNumber,
                                              u4FsMIEcfmLtrReceiveOrder,
                                              pu4NextFsMIEcfmLtrReceiveOrder)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_LBLT_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        if (EcfmLbLtGetNextActiveContext (u4FsMIEcfmContextId,
                                          &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexDot1agCfmLtrTable (pu4NextFsMIEcfmMdIndex,
                                              pu4NextFsMIEcfmMaIndex,
                                              pu4NextFsMIEcfmMepIdentifier,
                                              pu4NextFsMIEcfmLtrSeqNumber,
                                              pu4NextFsMIEcfmLtrReceiveOrder)
           != SNMP_SUCCESS);

    ECFM_LBLT_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrTtl (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                      UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                      UINT4 u4FsMIEcfmLtrSeqNumber,
                      UINT4 u4FsMIEcfmLtrReceiveOrder,
                      UINT4 *pu4RetValFsMIEcfmLtrTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmLtrTtl (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      u4FsMIEcfmLtrSeqNumber,
                                      u4FsMIEcfmLtrReceiveOrder,
                                      pu4RetValFsMIEcfmLtrTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrForwarded
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrForwarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrForwarded (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            UINT4 u4FsMIEcfmLtrSeqNumber,
                            UINT4 u4FsMIEcfmLtrReceiveOrder,
                            INT4 *pi4RetValFsMIEcfmLtrForwarded)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrForwarded (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     u4FsMIEcfmLtrSeqNumber,
                                     u4FsMIEcfmLtrReceiveOrder,
                                     pi4RetValFsMIEcfmLtrForwarded);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrTerminalMep
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrTerminalMep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrTerminalMep (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              UINT4 u4FsMIEcfmMepIdentifier,
                              UINT4 u4FsMIEcfmLtrSeqNumber,
                              UINT4 u4FsMIEcfmLtrReceiveOrder,
                              INT4 *pi4RetValFsMIEcfmLtrTerminalMep)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1agCfmLtrTerminalMep (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       u4FsMIEcfmLtrSeqNumber,
                                       u4FsMIEcfmLtrReceiveOrder,
                                       pi4RetValFsMIEcfmLtrTerminalMep);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrLastEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrLastEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrLastEgressIdentifier (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4 u4FsMIEcfmLtrSeqNumber,
                                       UINT4 u4FsMIEcfmLtrReceiveOrder,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIEcfmLtrLastEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1agCfmLtrLastEgressIdentifier (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       u4FsMIEcfmLtrSeqNumber,
                                                       u4FsMIEcfmLtrReceiveOrder,
                                                       pRetValFsMIEcfmLtrLastEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrNextEgressIdentifier
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrNextEgressIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrNextEgressIdentifier (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4 u4FsMIEcfmLtrSeqNumber,
                                       UINT4 u4FsMIEcfmLtrReceiveOrder,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIEcfmLtrNextEgressIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmLtrNextEgressIdentifier (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       u4FsMIEcfmLtrSeqNumber,
                                                       u4FsMIEcfmLtrReceiveOrder,
                                                       pRetValFsMIEcfmLtrNextEgressIdentifier);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrRelay
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrRelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrRelay (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                        UINT4 u4FsMIEcfmLtrSeqNumber,
                        UINT4 u4FsMIEcfmLtrReceiveOrder,
                        INT4 *pi4RetValFsMIEcfmLtrRelay)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1agCfmLtrRelay (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 u4FsMIEcfmLtrSeqNumber,
                                 u4FsMIEcfmLtrReceiveOrder,
                                 pi4RetValFsMIEcfmLtrRelay);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrChassisIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrChassisIdSubtype (UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   UINT4 u4FsMIEcfmLtrSeqNumber,
                                   UINT4 u4FsMIEcfmLtrReceiveOrder,
                                   INT4 *pi4RetValFsMIEcfmLtrChassisIdSubtype)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrChassisIdSubtype (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            u4FsMIEcfmMepIdentifier,
                                            u4FsMIEcfmLtrSeqNumber,
                                            u4FsMIEcfmLtrReceiveOrder,
                                            pi4RetValFsMIEcfmLtrChassisIdSubtype);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrChassisId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrChassisId (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            UINT4 u4FsMIEcfmLtrSeqNumber,
                            UINT4 u4FsMIEcfmLtrReceiveOrder,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIEcfmLtrChassisId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrChassisId (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     u4FsMIEcfmLtrSeqNumber,
                                     u4FsMIEcfmLtrReceiveOrder,
                                     pRetValFsMIEcfmLtrChassisId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrManAddressDomain
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrManAddressDomain (UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   UINT4 u4FsMIEcfmLtrSeqNumber,
                                   UINT4 u4FsMIEcfmLtrReceiveOrder,
                                   tSNMP_OID_TYPE *
                                   pRetValFsMIEcfmLtrManAddressDomain)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrManAddressDomain (u4FsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            u4FsMIEcfmMepIdentifier,
                                            u4FsMIEcfmLtrSeqNumber,
                                            u4FsMIEcfmLtrReceiveOrder,
                                            pRetValFsMIEcfmLtrManAddressDomain);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrManAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrManAddress (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 u4FsMIEcfmLtrSeqNumber,
                             UINT4 u4FsMIEcfmLtrReceiveOrder,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIEcfmLtrManAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrManAddress (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      u4FsMIEcfmLtrSeqNumber,
                                      u4FsMIEcfmLtrReceiveOrder,
                                      pRetValFsMIEcfmLtrManAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrIngress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrIngress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrIngress (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                          UINT4 u4FsMIEcfmMaIndex,
                          UINT4 u4FsMIEcfmMepIdentifier,
                          UINT4 u4FsMIEcfmLtrSeqNumber,
                          UINT4 u4FsMIEcfmLtrReceiveOrder,
                          INT4 *pi4RetValFsMIEcfmLtrIngress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrIngress (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   u4FsMIEcfmLtrSeqNumber,
                                   u4FsMIEcfmLtrReceiveOrder,
                                   pi4RetValFsMIEcfmLtrIngress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrIngressMac
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrIngressMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrIngressMac (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4 u4FsMIEcfmMepIdentifier,
                             UINT4 u4FsMIEcfmLtrSeqNumber,
                             UINT4 u4FsMIEcfmLtrReceiveOrder,
                             tMacAddr * pRetValFsMIEcfmLtrIngressMac)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrIngressMac (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                      u4FsMIEcfmMepIdentifier,
                                      u4FsMIEcfmLtrSeqNumber,
                                      u4FsMIEcfmLtrReceiveOrder,
                                      pRetValFsMIEcfmLtrIngressMac);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrIngressPortIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrIngressPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrIngressPortIdSubtype (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4 u4FsMIEcfmLtrSeqNumber,
                                       UINT4 u4FsMIEcfmLtrReceiveOrder,
                                       INT4
                                       *pi4RetValFsMIEcfmLtrIngressPortIdSubtype)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmLtrIngressPortIdSubtype (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       u4FsMIEcfmLtrSeqNumber,
                                                       u4FsMIEcfmLtrReceiveOrder,
                                                       pi4RetValFsMIEcfmLtrIngressPortIdSubtype);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrIngressPortId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrIngressPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrIngressPortId (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4 u4FsMIEcfmMepIdentifier,
                                UINT4 u4FsMIEcfmLtrSeqNumber,
                                UINT4 u4FsMIEcfmLtrReceiveOrder,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIEcfmLtrIngressPortId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrIngressPortId (u4FsMIEcfmMdIndex,
                                         u4FsMIEcfmMaIndex,
                                         u4FsMIEcfmMepIdentifier,
                                         u4FsMIEcfmLtrSeqNumber,
                                         u4FsMIEcfmLtrReceiveOrder,
                                         pRetValFsMIEcfmLtrIngressPortId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrEgress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrEgress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrEgress (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                         UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                         UINT4 u4FsMIEcfmLtrSeqNumber,
                         UINT4 u4FsMIEcfmLtrReceiveOrder,
                         INT4 *pi4RetValFsMIEcfmLtrEgress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrEgress (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                  u4FsMIEcfmMepIdentifier,
                                  u4FsMIEcfmLtrSeqNumber,
                                  u4FsMIEcfmLtrReceiveOrder,
                                  pi4RetValFsMIEcfmLtrEgress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrEgressMac
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrEgressMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrEgressMac (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                            UINT4 u4FsMIEcfmMaIndex,
                            UINT4 u4FsMIEcfmMepIdentifier,
                            UINT4 u4FsMIEcfmLtrSeqNumber,
                            UINT4 u4FsMIEcfmLtrReceiveOrder,
                            tMacAddr * pRetValFsMIEcfmLtrEgressMac)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrEgressMac (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     u4FsMIEcfmLtrSeqNumber,
                                     u4FsMIEcfmLtrReceiveOrder,
                                     pRetValFsMIEcfmLtrEgressMac);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrEgressPortIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrEgressPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrEgressPortIdSubtype (UINT4 u4FsMIEcfmContextId,
                                      UINT4 u4FsMIEcfmMdIndex,
                                      UINT4 u4FsMIEcfmMaIndex,
                                      UINT4 u4FsMIEcfmMepIdentifier,
                                      UINT4 u4FsMIEcfmLtrSeqNumber,
                                      UINT4 u4FsMIEcfmLtrReceiveOrder,
                                      INT4
                                      *pi4RetValFsMIEcfmLtrEgressPortIdSubtype)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmLtrEgressPortIdSubtype (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      u4FsMIEcfmLtrSeqNumber,
                                                      u4FsMIEcfmLtrReceiveOrder,
                                                      pi4RetValFsMIEcfmLtrEgressPortIdSubtype);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrEgressPortId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrEgressPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrEgressPortId (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               UINT4 u4FsMIEcfmLtrSeqNumber,
                               UINT4 u4FsMIEcfmLtrReceiveOrder,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIEcfmLtrEgressPortId)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmLtrEgressPortId (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        u4FsMIEcfmLtrSeqNumber,
                                        u4FsMIEcfmLtrReceiveOrder,
                                        pRetValFsMIEcfmLtrEgressPortId);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtrOrganizationSpecificTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtrSeqNumber
                FsMIEcfmLtrReceiveOrder

                The Object 
                retValFsMIEcfmLtrOrganizationSpecificTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtrOrganizationSpecificTlv (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier,
                                          UINT4 u4FsMIEcfmLtrSeqNumber,
                                          UINT4 u4FsMIEcfmLtrReceiveOrder,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsMIEcfmLtrOrganizationSpecificTlv)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmLtrOrganizationSpecificTlv (u4FsMIEcfmMdIndex,
                                                          u4FsMIEcfmMaIndex,
                                                          u4FsMIEcfmMepIdentifier,
                                                          u4FsMIEcfmLtrSeqNumber,
                                                          u4FsMIEcfmLtrReceiveOrder,
                                                          pRetValFsMIEcfmLtrOrganizationSpecificTlv);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMepDbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMepDbTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMepDbTable (UINT4 u4FsMIEcfmContextId,
                                            UINT4 u4FsMIEcfmMdIndex,
                                            UINT4 u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier,
                                            UINT4 u4FsMIEcfmMepDbRMepIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1agCfmMepDbTable (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     u4FsMIEcfmMepDbRMepIdentifier);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMepDbTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMepDbTable (UINT4 *pu4FsMIEcfmContextId,
                                    UINT4 *pu4FsMIEcfmMdIndex,
                                    UINT4 *pu4FsMIEcfmMaIndex,
                                    UINT4 *pu4FsMIEcfmMepIdentifier,
                                    UINT4 *pu4FsMIEcfmMepDbRMepIdentifier)
{
    return (nmhGetNextIndexFsMIEcfmMepDbTable (0, pu4FsMIEcfmContextId, 0,
                                               pu4FsMIEcfmMdIndex, 0,
                                               pu4FsMIEcfmMaIndex, 0,
                                               pu4FsMIEcfmMepIdentifier, 0,
                                               pu4FsMIEcfmMepDbRMepIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMepDbTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                nextFsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
                nextFsMIEcfmMepDbRMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMepDbTable (UINT4 u4FsMIEcfmContextId,
                                   UINT4 *pu4NextFsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 *pu4NextFsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 *pu4NextFsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   UINT4 *pu4NextFsMIEcfmMepIdentifier,
                                   UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                                   UINT4 *pu4NextFsMIEcfmMepDbRMepIdentifier)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexDot1agCfmMepDbTable (u4FsMIEcfmMdIndex,
                                                pu4NextFsMIEcfmMdIndex,
                                                u4FsMIEcfmMaIndex,
                                                pu4NextFsMIEcfmMaIndex,
                                                u4FsMIEcfmMepIdentifier,
                                                pu4NextFsMIEcfmMepIdentifier,
                                                u4FsMIEcfmMepDbRMepIdentifier,
                                                pu4NextFsMIEcfmMepDbRMepIdentifier)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexDot1agCfmMepDbTable (pu4NextFsMIEcfmMdIndex,
                                                pu4NextFsMIEcfmMaIndex,
                                                pu4NextFsMIEcfmMepIdentifier,
                                                pu4NextFsMIEcfmMepDbRMepIdentifier)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbRMepState
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbRMepState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbRMepState (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              UINT4 u4FsMIEcfmMepIdentifier,
                              UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                              INT4 *pi4RetValFsMIEcfmMepDbRMepState)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDbRMepState (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       u4FsMIEcfmMepDbRMepIdentifier,
                                       pi4RetValFsMIEcfmMepDbRMepState);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbRMepFailedOkTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbRMepFailedOkTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbRMepFailedOkTime (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                                     UINT4
                                     *pu4RetValFsMIEcfmMepDbRMepFailedOkTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepDbRMepFailedOkTime (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     u4FsMIEcfmMepDbRMepIdentifier,
                                                     pu4RetValFsMIEcfmMepDbRMepFailedOkTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbMacAddress (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                               tMacAddr * pRetValFsMIEcfmMepDbMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDbMacAddress (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        u4FsMIEcfmMepDbRMepIdentifier,
                                        pRetValFsMIEcfmMepDbMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbRdi
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbRdi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbRdi (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                        UINT4 u4FsMIEcfmMaIndex, UINT4 u4FsMIEcfmMepIdentifier,
                        UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                        INT4 *pi4RetValFsMIEcfmMepDbRdi)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDbRdi (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 u4FsMIEcfmMepDbRMepIdentifier,
                                 pi4RetValFsMIEcfmMepDbRdi);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbPortStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbPortStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbPortStatusTlv (UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4FsMIEcfmMdIndex,
                                  UINT4 u4FsMIEcfmMaIndex,
                                  UINT4 u4FsMIEcfmMepIdentifier,
                                  UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                                  INT4 *pi4RetValFsMIEcfmMepDbPortStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDbPortStatusTlv (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           pi4RetValFsMIEcfmMepDbPortStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbInterfaceStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbInterfaceStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbInterfaceStatusTlv (UINT4 u4FsMIEcfmContextId,
                                       UINT4 u4FsMIEcfmMdIndex,
                                       UINT4 u4FsMIEcfmMaIndex,
                                       UINT4 u4FsMIEcfmMepIdentifier,
                                       UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                                       INT4
                                       *pi4RetValFsMIEcfmMepDbInterfaceStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepDbInterfaceStatusTlv (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       u4FsMIEcfmMepDbRMepIdentifier,
                                                       pi4RetValFsMIEcfmMepDbInterfaceStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbChassisIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbChassisIdSubtype (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                                     INT4
                                     *pi4RetValFsMIEcfmMepDbChassisIdSubtype)
{
    INT1                i1RetVal;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1agCfmMepDbChassisIdSubtype (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     u4FsMIEcfmMepDbRMepIdentifier,
                                                     pi4RetValFsMIEcfmMepDbChassisIdSubtype);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbChassisId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbChassisId (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                              UINT4 u4FsMIEcfmMepIdentifier,
                              UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMIEcfmMepDbChassisId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDbChassisId (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       u4FsMIEcfmMepDbRMepIdentifier,
                                       pRetValFsMIEcfmMepDbChassisId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbManAddressDomain
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbManAddressDomain (UINT4 u4FsMIEcfmContextId,
                                     UINT4 u4FsMIEcfmMdIndex,
                                     UINT4 u4FsMIEcfmMaIndex,
                                     UINT4 u4FsMIEcfmMepIdentifier,
                                     UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                                     tSNMP_OID_TYPE *
                                     pRetValFsMIEcfmMepDbManAddressDomain)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1agCfmMepDbManAddressDomain (u4FsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     u4FsMIEcfmMepDbRMepIdentifier,
                                                     pRetValFsMIEcfmMepDbManAddressDomain);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDbManAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmMepDbManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDbManAddress (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex, UINT4 u4FsMIEcfmMaIndex,
                               UINT4 u4FsMIEcfmMepIdentifier,
                               UINT4 u4FsMIEcfmMepDbRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIEcfmMepDbManAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1agCfmMepDbManAddress (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        u4FsMIEcfmMepDbRMepIdentifier,
                                        pRetValFsMIEcfmMepDbManAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FsMIEcfmMipTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMipTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMipTable (INT4 i4FsMIEcfmMipIfIndex,
                                          INT4 i4FsMIEcfmMipMdLevel,
                                          INT4 i4FsMIEcfmMipVid)
{
    tEcfmCcMipInfo     *pMipInfo = NULL;
    tEcfmCcMipInfo      MipIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmMipIfIndex,
                                                 &u4ContextId, &u2LocalPortId);
    MipIndex.u4ContextId = u4ContextId;
    MipIndex.u2PortNum = u2LocalPortId;
    MipIndex.u1MdLevel = i4FsMIEcfmMipMdLevel;
    MipIndex.u4VlanIdIsid = i4FsMIEcfmMipVid;
    /* Get MIP info from the global MIP tree */
    pMipInfo =
        (tEcfmCcMipInfo *) RBTreeGet (ECFM_CC_GLOBAL_MIP_TABLE, &MipIndex);
    UNUSED_PARAM (i4RetVal);
    if (pMipInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMipTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMipTable (INT4 *pi4FsMIEcfmMipIfIndex,
                                  INT4 *pi4FsMIEcfmMipMdLevel,
                                  INT4 *pi4FsMIEcfmMipVid)
{
    return (nmhGetNextIndexFsMIEcfmMipTable (0, pi4FsMIEcfmMipIfIndex, 0,
                                             pi4FsMIEcfmMipMdLevel, 0,
                                             pi4FsMIEcfmMipVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMipTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                nextFsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                nextFsMIEcfmMipMdLevel
                FsMIEcfmMipVid
                nextFsMIEcfmMipVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMipTable (INT4 i4FsMIEcfmMipIfIndex,
                                 INT4 *pi4NextFsMIEcfmMipIfIndex,
                                 INT4 i4FsMIEcfmMipMdLevel,
                                 INT4 *pi4NextFsMIEcfmMipMdLevel,
                                 INT4 i4FsMIEcfmMipVid,
                                 INT4 *pi4NextFsMIEcfmMipVid)
{
    tEcfmCcMipInfo     *pNextMipInfo = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    tEcfmCcMipInfo      MipIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&MipIndex, 0x00, sizeof (tEcfmCcMipInfo));

    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmMipIfIndex,
                                                 &u4ContextId, &u2LocalPortId);

    MipIndex.u4ContextId = u4ContextId;
    MipIndex.u2PortNum = u2LocalPortId;
    MipIndex.u1MdLevel = i4FsMIEcfmMipMdLevel;
    MipIndex.u4VlanIdIsid = i4FsMIEcfmMipVid;
    pNextMipInfo = (tEcfmCcMipInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_MIP_TABLE, (tRBElem *) & MipIndex, NULL);
    UNUSED_PARAM (i4RetVal);

    if (pNextMipInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (pNextMipInfo->u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIEcfmMipIfIndex = ECFM_CC_GET_PHY_PORT (pNextMipInfo->u2PortNum,
                                                       pTempPortInfo);
    *pi4NextFsMIEcfmMipMdLevel = pNextMipInfo->u1MdLevel;
    *pi4NextFsMIEcfmMipVid = pNextMipInfo->u4VlanIdIsid;

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipActive
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                retValFsMIEcfmMipActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipActive (INT4 i4FsMIEcfmMipIfIndex, INT4 i4FsMIEcfmMipMdLevel,
                         INT4 i4FsMIEcfmMipVid,
                         INT4 *pi4RetValFsMIEcfmMipActive)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMipActive ((INT4) u2LocalPort, i4FsMIEcfmMipMdLevel,
                               i4FsMIEcfmMipVid, pi4RetValFsMIEcfmMipActive);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipRowStatus
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                retValFsMIEcfmMipRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipRowStatus (INT4 i4FsMIEcfmMipIfIndex,
                            INT4 i4FsMIEcfmMipMdLevel, INT4 i4FsMIEcfmMipVid,
                            INT4 *pi4RetValFsMIEcfmMipRowStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMipRowStatus ((INT4) u2LocalPort,
                                  i4FsMIEcfmMipMdLevel, i4FsMIEcfmMipVid,
                                  pi4RetValFsMIEcfmMipRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipActive
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                setValFsMIEcfmMipActive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipActive (INT4 i4FsMIEcfmMipIfIndex, INT4 i4FsMIEcfmMipMdLevel,
                         INT4 i4FsMIEcfmMipVid, INT4 i4SetValFsMIEcfmMipActive)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMipActive ((INT4) u2LocalPort, i4FsMIEcfmMipMdLevel,
                               i4FsMIEcfmMipVid, i4SetValFsMIEcfmMipActive);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMipRowStatus
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                setValFsMIEcfmMipRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMipRowStatus (INT4 i4FsMIEcfmMipIfIndex,
                            INT4 i4FsMIEcfmMipMdLevel, INT4 i4FsMIEcfmMipVid,
                            INT4 i4SetValFsMIEcfmMipRowStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMipRowStatus ((INT4) u2LocalPort,
                                  i4FsMIEcfmMipMdLevel, i4FsMIEcfmMipVid,
                                  i4SetValFsMIEcfmMipRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipActive
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                testValFsMIEcfmMipActive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipActive (UINT4 *pu4ErrorCode, INT4 i4FsMIEcfmMipIfIndex,
                            INT4 i4FsMIEcfmMipMdLevel, INT4 i4FsMIEcfmMipVid,
                            INT4 i4TestValFsMIEcfmMipActive)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMipActive (pu4ErrorCode, (INT4) u2LocalPort,
                                         i4FsMIEcfmMipMdLevel,
                                         i4FsMIEcfmMipVid,
                                         i4TestValFsMIEcfmMipActive);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMipRowStatus
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                testValFsMIEcfmMipRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMipRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIEcfmMipIfIndex,
                               INT4 i4FsMIEcfmMipMdLevel, INT4 i4FsMIEcfmMipVid,
                               INT4 i4TestValFsMIEcfmMipRowStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmMipRowStatus (pu4ErrorCode, (INT4) u2LocalPort,
                                     i4FsMIEcfmMipMdLevel, i4FsMIEcfmMipVid,
                                     i4TestValFsMIEcfmMipRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMipTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMipTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmDynMipPreventionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmDynMipPreventionTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmDynMipPreventionTable (INT4
                                                       i4FsMIEcfmMipIfIndex,
                                                       INT4
                                                       i4FsMIEcfmMipMdLevel,
                                                       INT4 i4FsMIEcfmMipVid)
{
    tEcfmCcMipPreventInfo *pMipInfo = NULL;
    tEcfmCcMipPreventInfo MipIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmMipIfIndex,
                                                 &u4ContextId, &u2LocalPortId);

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&MipIndex, 0x00, sizeof (tEcfmCcMipPreventInfo));
    MipIndex.u4ContextId = u4ContextId;
    MipIndex.u4IfIndex = (UINT4) i4FsMIEcfmMipIfIndex;
    MipIndex.u1MdLevel = (UINT1) i4FsMIEcfmMipMdLevel;
    MipIndex.u4VlanIdIsid = (UINT4) i4FsMIEcfmMipVid;
    /* Get MIP info from the global MIP tree */
    pMipInfo =
        (tEcfmCcMipPreventInfo *) RBTreeGet (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE,
                                             (tRBElem *) & MipIndex);
    UNUSED_PARAM (i4RetVal);
    if (pMipInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmDynMipPreventionTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmDynMipPreventionTable (INT4 *pi4FsMIEcfmMipIfIndex,
                                               INT4 *pi4FsMIEcfmMipMdLevel,
                                               INT4 *pi4FsMIEcfmMipVid)
{
    return (nmhGetNextIndexFsMIEcfmDynMipPreventionTable
            (0, pi4FsMIEcfmMipIfIndex, 0, pi4FsMIEcfmMipMdLevel, 0,
             pi4FsMIEcfmMipVid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmDynMipPreventionTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                nextFsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                nextFsMIEcfmMipMdLevel
                FsMIEcfmMipVid
                nextFsMIEcfmMipVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmDynMipPreventionTable (INT4 i4FsMIEcfmMipIfIndex,
                                              INT4 *pi4NextFsMIEcfmMipIfIndex,
                                              INT4 i4FsMIEcfmMipMdLevel,
                                              INT4 *pi4NextFsMIEcfmMipMdLevel,
                                              INT4 i4FsMIEcfmMipVid,
                                              INT4 *pi4NextFsMIEcfmMipVid)
{
    tEcfmCcMipPreventInfo *pNextMipInfo = NULL;
    tEcfmCcMipPreventInfo MipIndex;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPortId = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    i4RetVal = EcfmVcmGetContextInfoFromIfIndex ((UINT4) i4FsMIEcfmMipIfIndex,
                                                 &u4ContextId, &u2LocalPortId);

    if (!ECFM_IS_SYSTEM_INITIALISED ())
    {
        return SNMP_FAILURE;
    }
    ECFM_MEMSET (&MipIndex, 0x00, sizeof (tEcfmCcMipPreventInfo));
    MipIndex.u4ContextId = u4ContextId;
    MipIndex.u4IfIndex = (UINT4) i4FsMIEcfmMipIfIndex;
    MipIndex.u1MdLevel = (UINT1) i4FsMIEcfmMipMdLevel;
    MipIndex.u4VlanIdIsid = (UINT4) i4FsMIEcfmMipVid;

    pNextMipInfo = (tEcfmCcMipPreventInfo *) RBTreeGetNext
        (ECFM_CC_GLOBAL_MIP_PREVENT_TABLE, (tRBElem *) & MipIndex, NULL);
    UNUSED_PARAM (i4RetVal);

    if (pNextMipInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIEcfmMipIfIndex = (INT4) (pNextMipInfo->u4IfIndex);
    *pi4NextFsMIEcfmMipMdLevel = (INT4) (pNextMipInfo->u1MdLevel);
    *pi4NextFsMIEcfmMipVid = (INT4) (pNextMipInfo->u4VlanIdIsid);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDynMipPreventionRowStatus
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                retValFsMIEcfmDynMipPreventionRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDynMipPreventionRowStatus (INT4 i4FsMIEcfmMipIfIndex,
                                         INT4 i4FsMIEcfmMipMdLevel,
                                         INT4 i4FsMIEcfmMipVid,
                                         INT4
                                         *pi4RetValFsMIEcfmDynMipPreventionRowStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmDynMipPreventionRowStatus ((INT4) u2LocalPort,
                                               i4FsMIEcfmMipMdLevel,
                                               i4FsMIEcfmMipVid,
                                               pi4RetValFsMIEcfmDynMipPreventionRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDynMipPreventionRowStatus
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                setValFsMIEcfmDynMipPreventionRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDynMipPreventionRowStatus (INT4 i4FsMIEcfmMipIfIndex,
                                         INT4 i4FsMIEcfmMipMdLevel,
                                         INT4 i4FsMIEcfmMipVid,
                                         INT4
                                         i4SetValFsMIEcfmDynMipPreventionRowStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmDynMipPreventionRowStatus (u2LocalPort,
                                               i4FsMIEcfmMipMdLevel,
                                               i4FsMIEcfmMipVid,
                                               i4SetValFsMIEcfmDynMipPreventionRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDynMipPreventionRowStatus
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid

                The Object 
                testValFsMIEcfmDynMipPreventionRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDynMipPreventionRowStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIEcfmMipIfIndex,
                                            INT4 i4FsMIEcfmMipMdLevel,
                                            INT4 i4FsMIEcfmMipVid,
                                            INT4
                                            i4TestValFsMIEcfmDynMipPreventionRowStatus)
{
    INT1                i1RetVal;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX ((UINT4) i4FsMIEcfmMipIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsEcfmDynMipPreventionRowStatus
        (pu4ErrorCode, u2LocalPort,
         i4FsMIEcfmMipMdLevel, i4FsMIEcfmMipVid,
         i4TestValFsMIEcfmDynMipPreventionRowStatus);

    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmDynMipPreventionTable
 Input       :  The Indices
                FsMIEcfmMipIfIndex
                FsMIEcfmMipMdLevel
                FsMIEcfmMipVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmDynMipPreventionTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMipCcmDbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMipCcmDbTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMipCcmFid
                FsMIEcfmMipCcmSrcAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIEcfmMipCcmDbTable (UINT4
                                               u4FsMIEcfmContextId,
                                               UINT4
                                               u4FsMIEcfmMipCcmFid,
                                               tMacAddr FsMIEcfmMipCcmSrcAddr)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsEcfmMipCcmDbTable (u4FsMIEcfmMipCcmFid,
                                                     FsMIEcfmMipCcmSrcAddr);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMipCcmDbTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMipCcmFid
                FsMIEcfmMipCcmSrcAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMipCcmDbTable (UINT4
                                       *pu4FsMIEcfmContextId,
                                       UINT4
                                       *pu4FsMIEcfmMipCcmFid,
                                       tMacAddr * pFsMIEcfmMipCcmSrcAddr)
{
    tMacAddr            TempMacAddr;

    ECFM_MEMSET (&TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    return (nmhGetNextIndexFsMIEcfmMipCcmDbTable
            (0, pu4FsMIEcfmContextId, 0, pu4FsMIEcfmMipCcmFid, TempMacAddr,
             pFsMIEcfmMipCcmSrcAddr));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMipCcmDbTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMipCcmFid
                nextFsMIEcfmMipCcmFid
                FsMIEcfmMipCcmSrcAddr
                nextFsMIEcfmMipCcmSrcAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMipCcmDbTable (UINT4
                                      u4FsMIEcfmContextId,
                                      UINT4
                                      *pu4NextFsMIEcfmContextId,
                                      UINT4
                                      u4FsMIEcfmMipCcmFid,
                                      UINT4
                                      *pu4NextFsMIEcfmMipCcmFid,
                                      tMacAddr
                                      FsMIEcfmMipCcmSrcAddr,
                                      tMacAddr * pNextFsMIEcfmMipCcmSrcAddr)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexFsEcfmMipCcmDbTable (u4FsMIEcfmMipCcmFid,
                                                pu4NextFsMIEcfmMipCcmFid,
                                                FsMIEcfmMipCcmSrcAddr,
                                                pNextFsMIEcfmMipCcmSrcAddr)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexFsEcfmMipCcmDbTable (pu4NextFsMIEcfmMipCcmFid,
                                                pNextFsMIEcfmMipCcmSrcAddr)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMipCcmIfIndex
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMipCcmFid
                FsMIEcfmMipCcmSrcAddr

                The Object 
                retValFsMIEcfmMipCcmIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMipCcmIfIndex (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMipCcmFid,
                             tMacAddr FsMIEcfmMipCcmSrcAddr,
                             INT4 *pi4RetValFsMIEcfmMipCcmIfIndex)
{
    INT1                i1RetVal;
    UINT4               u4LocalPort = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pPortInfo = NULL;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMipCcmIfIndex (u4FsMIEcfmMipCcmFid,
                                          FsMIEcfmMipCcmSrcAddr,
                                          (INT4 *) &u4LocalPort);

    /* Get the actual interface from local Port */
    pPortInfo = ECFM_CC_GET_PORT_INFO (u4LocalPort);
    if (pPortInfo != NULL)
    {
        *pi4RetValFsMIEcfmMipCcmIfIndex = (INT4) pPortInfo->u4IfIndex;
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIEcfmRemoteMepDbExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable (UINT4
                                                    u4FsMIEcfmContextId,
                                                    UINT4
                                                    u4FsMIEcfmMdIndex,
                                                    UINT4
                                                    u4FsMIEcfmMaIndex,
                                                    UINT4
                                                    u4FsMIEcfmMepIdentifier,
                                                    UINT4
                                                    u4FsMIEcfmMepDbRMepIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable
        (u4FsMIEcfmMdIndex,
         u4FsMIEcfmMaIndex,
         u4FsMIEcfmMepIdentifier, u4FsMIEcfmMepDbRMepIdentifier);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmRemoteMepDbExTable (UINT4
                                            *pu4FsMIEcfmContextId,
                                            UINT4
                                            *pu4FsMIEcfmMdIndex,
                                            UINT4
                                            *pu4FsMIEcfmMaIndex,
                                            UINT4
                                            *pu4FsMIEcfmMepIdentifier,
                                            UINT4
                                            *pu4FsMIEcfmMepDbRMepIdentifier)
{

    return (nmhGetNextIndexFsMIEcfmRemoteMepDbExTable
            (0, pu4FsMIEcfmContextId, 0, pu4FsMIEcfmMdIndex, 0,
             pu4FsMIEcfmMaIndex, 0, pu4FsMIEcfmMepIdentifier, 0,
             pu4FsMIEcfmMepDbRMepIdentifier));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                nextFsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
                nextFsMIEcfmMepDbRMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmRemoteMepDbExTable (UINT4
                                           u4FsMIEcfmContextId,
                                           UINT4
                                           *pu4NextFsMIEcfmContextId,
                                           UINT4
                                           u4FsMIEcfmMdIndex,
                                           UINT4
                                           *pu4NextFsMIEcfmMdIndex,
                                           UINT4
                                           u4FsMIEcfmMaIndex,
                                           UINT4
                                           *pu4NextFsMIEcfmMaIndex,
                                           UINT4
                                           u4FsMIEcfmMepIdentifier,
                                           UINT4
                                           *pu4NextFsMIEcfmMepIdentifier,
                                           UINT4
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           UINT4
                                           *pu4NextFsMIEcfmMepDbRMepIdentifier)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexFsEcfmRemoteMepDbExTable (u4FsMIEcfmMdIndex,
                                                     pu4NextFsMIEcfmMdIndex,
                                                     u4FsMIEcfmMaIndex,
                                                     pu4NextFsMIEcfmMaIndex,
                                                     u4FsMIEcfmMepIdentifier,
                                                     pu4NextFsMIEcfmMepIdentifier,
                                                     u4FsMIEcfmMepDbRMepIdentifier,
                                                     pu4NextFsMIEcfmMepDbRMepIdentifier)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexFsEcfmRemoteMepDbExTable (pu4NextFsMIEcfmMdIndex,
                                                     pu4NextFsMIEcfmMaIndex,
                                                     pu4NextFsMIEcfmMepIdentifier,
                                                     pu4NextFsMIEcfmMepDbRMepIdentifier)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepCcmSequenceNum
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepCcmSequenceNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepCcmSequenceNum (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  UINT4
                                  u4FsMIEcfmMepDbRMepIdentifier,
                                  UINT4 *pu4RetValFsMIEcfmRMepCcmSequenceNum)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepCcmSequenceNum (u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmMaIndex,
                                               u4FsMIEcfmMepIdentifier,
                                               u4FsMIEcfmMepDbRMepIdentifier,
                                               pu4RetValFsMIEcfmRMepCcmSequenceNum);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepPortStatusDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepPortStatusDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepPortStatusDefect (UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    INT4 *pi4RetValFsMIEcfmRMepPortStatusDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepPortStatusDefect (u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 pi4RetValFsMIEcfmRMepPortStatusDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepInterfaceStatusDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepInterfaceStatusDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepInterfaceStatusDefect (UINT4
                                         u4FsMIEcfmContextId,
                                         UINT4
                                         u4FsMIEcfmMdIndex,
                                         UINT4
                                         u4FsMIEcfmMaIndex,
                                         UINT4
                                         u4FsMIEcfmMepIdentifier,
                                         UINT4
                                         u4FsMIEcfmMepDbRMepIdentifier,
                                         INT4
                                         *pi4RetValFsMIEcfmRMepInterfaceStatusDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepInterfaceStatusDefect (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      u4FsMIEcfmMepDbRMepIdentifier,
                                                      pi4RetValFsMIEcfmRMepInterfaceStatusDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepCcmDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepCcmDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepCcmDefect (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4
                             u4FsMIEcfmMepIdentifier,
                             UINT4
                             u4FsMIEcfmMepDbRMepIdentifier,
                             INT4 *pi4RetValFsMIEcfmRMepCcmDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepCcmDefect (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          u4FsMIEcfmMepDbRMepIdentifier,
                                          pi4RetValFsMIEcfmRMepCcmDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepRDIDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepRDIDefect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepRDIDefect (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4
                             u4FsMIEcfmMepIdentifier,
                             UINT4
                             u4FsMIEcfmMepDbRMepIdentifier,
                             INT4 *pi4RetValFsMIEcfmRMepRDIDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepRDIDefect (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          u4FsMIEcfmMepDbRMepIdentifier,
                                          pi4RetValFsMIEcfmRMepRDIDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepMacAddress (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4
                              u4FsMIEcfmMepIdentifier,
                              UINT4
                              u4FsMIEcfmMepDbRMepIdentifier,
                              tMacAddr * pRetValFsMIEcfmRMepMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepMacAddress (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           pRetValFsMIEcfmRMepMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepRdi
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepRdi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepRdi (UINT4 u4FsMIEcfmContextId,
                       UINT4 u4FsMIEcfmMdIndex,
                       UINT4 u4FsMIEcfmMaIndex,
                       UINT4 u4FsMIEcfmMepIdentifier,
                       UINT4
                       u4FsMIEcfmMepDbRMepIdentifier,
                       INT4 *pi4RetValFsMIEcfmRMepRdi)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepRdi (u4FsMIEcfmMdIndex,
                                    u4FsMIEcfmMaIndex,
                                    u4FsMIEcfmMepIdentifier,
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    pi4RetValFsMIEcfmRMepRdi);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepPortStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepPortStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepPortStatusTlv (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 UINT4
                                 u4FsMIEcfmMepDbRMepIdentifier,
                                 INT4 *pi4RetValFsMIEcfmRMepPortStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepPortStatusTlv (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              u4FsMIEcfmMepDbRMepIdentifier,
                                              pi4RetValFsMIEcfmRMepPortStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepInterfaceStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepInterfaceStatusTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepInterfaceStatusTlv (UINT4
                                      u4FsMIEcfmContextId,
                                      UINT4
                                      u4FsMIEcfmMdIndex,
                                      UINT4
                                      u4FsMIEcfmMaIndex,
                                      UINT4
                                      u4FsMIEcfmMepIdentifier,
                                      UINT4
                                      u4FsMIEcfmMepDbRMepIdentifier,
                                      INT4
                                      *pi4RetValFsMIEcfmRMepInterfaceStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepInterfaceStatusTlv (u4FsMIEcfmMdIndex,
                                                   u4FsMIEcfmMaIndex,
                                                   u4FsMIEcfmMepIdentifier,
                                                   u4FsMIEcfmMepDbRMepIdentifier,
                                                   pi4RetValFsMIEcfmRMepInterfaceStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepChassisIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepChassisIdSubtype (UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    INT4 *pi4RetValFsMIEcfmRMepChassisIdSubtype)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepChassisIdSubtype (u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 pi4RetValFsMIEcfmRMepChassisIdSubtype);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepDbChassisId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepDbChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepDbChassisId (UINT4
                               u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmMaIndex,
                               UINT4
                               u4FsMIEcfmMepIdentifier,
                               UINT4
                               u4FsMIEcfmMepDbRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIEcfmRMepDbChassisId)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmMepDbChassisId (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           pRetValFsMIEcfmRMepDbChassisId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepManAddressDomain
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepManAddressDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepManAddressDomain (UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    tSNMP_OID_TYPE *
                                    pRetValFsMIEcfmRMepManAddressDomain)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepManAddressDomain (u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 pRetValFsMIEcfmRMepManAddressDomain);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRMepManAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                retValFsMIEcfmRMepManAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRMepManAddress (UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4
                              u4FsMIEcfmMepIdentifier,
                              UINT4
                              u4FsMIEcfmMepDbRMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMIEcfmRMepManAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmRMepManAddress (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           pRetValFsMIEcfmRMepManAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepPortStatusDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepPortStatusDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepPortStatusDefect (UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    INT4 i4SetValFsMIEcfmRMepPortStatusDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepPortStatusDefect (u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 i4SetValFsMIEcfmRMepPortStatusDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepInterfaceStatusDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepInterfaceStatusDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepInterfaceStatusDefect (UINT4
                                         u4FsMIEcfmContextId,
                                         UINT4
                                         u4FsMIEcfmMdIndex,
                                         UINT4
                                         u4FsMIEcfmMaIndex,
                                         UINT4
                                         u4FsMIEcfmMepIdentifier,
                                         UINT4
                                         u4FsMIEcfmMepDbRMepIdentifier,
                                         INT4
                                         i4SetValFsMIEcfmRMepInterfaceStatusDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepInterfaceStatusDefect (u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      u4FsMIEcfmMepDbRMepIdentifier,
                                                      i4SetValFsMIEcfmRMepInterfaceStatusDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepCcmDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepCcmDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepCcmDefect (UINT4
                             u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4
                             u4FsMIEcfmMepIdentifier,
                             UINT4
                             u4FsMIEcfmMepDbRMepIdentifier,
                             INT4 i4SetValFsMIEcfmRMepCcmDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepCcmDefect (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          u4FsMIEcfmMepDbRMepIdentifier,
                                          i4SetValFsMIEcfmRMepCcmDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepRDIDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepRDIDefect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepRDIDefect (UINT4
                             u4FsMIEcfmContextId,
                             UINT4 u4FsMIEcfmMdIndex,
                             UINT4 u4FsMIEcfmMaIndex,
                             UINT4
                             u4FsMIEcfmMepIdentifier,
                             UINT4
                             u4FsMIEcfmMepDbRMepIdentifier,
                             INT4 i4SetValFsMIEcfmRMepRDIDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepRDIDefect (u4FsMIEcfmMdIndex,
                                          u4FsMIEcfmMaIndex,
                                          u4FsMIEcfmMepIdentifier,
                                          u4FsMIEcfmMepDbRMepIdentifier,
                                          i4SetValFsMIEcfmRMepRDIDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepMacAddress (UINT4
                              u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4
                              u4FsMIEcfmMepIdentifier,
                              UINT4
                              u4FsMIEcfmMepDbRMepIdentifier,
                              tMacAddr SetValFsMIEcfmRMepMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepMacAddress (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           SetValFsMIEcfmRMepMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepRdi
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepRdi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepRdi (UINT4 u4FsMIEcfmContextId,
                       UINT4 u4FsMIEcfmMdIndex,
                       UINT4 u4FsMIEcfmMaIndex,
                       UINT4 u4FsMIEcfmMepIdentifier,
                       UINT4
                       u4FsMIEcfmMepDbRMepIdentifier,
                       INT4 i4SetValFsMIEcfmRMepRdi)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepRdi (u4FsMIEcfmMdIndex,
                                    u4FsMIEcfmMaIndex,
                                    u4FsMIEcfmMepIdentifier,
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    i4SetValFsMIEcfmRMepRdi);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepPortStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepPortStatusTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepPortStatusTlv (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 UINT4
                                 u4FsMIEcfmMepDbRMepIdentifier,
                                 INT4 i4SetValFsMIEcfmRMepPortStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepPortStatusTlv (u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              u4FsMIEcfmMepDbRMepIdentifier,
                                              i4SetValFsMIEcfmRMepPortStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepInterfaceStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepInterfaceStatusTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepInterfaceStatusTlv (UINT4
                                      u4FsMIEcfmContextId,
                                      UINT4
                                      u4FsMIEcfmMdIndex,
                                      UINT4
                                      u4FsMIEcfmMaIndex,
                                      UINT4
                                      u4FsMIEcfmMepIdentifier,
                                      UINT4
                                      u4FsMIEcfmMepDbRMepIdentifier,
                                      INT4
                                      i4SetValFsMIEcfmRMepInterfaceStatusTlv)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepInterfaceStatusTlv (u4FsMIEcfmMdIndex,
                                                   u4FsMIEcfmMaIndex,
                                                   u4FsMIEcfmMepIdentifier,
                                                   u4FsMIEcfmMepDbRMepIdentifier,
                                                   i4SetValFsMIEcfmRMepInterfaceStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepChassisIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepChassisIdSubtype
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepChassisIdSubtype (UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    INT4 i4SetValFsMIEcfmRMepChassisIdSubtype)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepChassisIdSubtype (u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 i4SetValFsMIEcfmRMepChassisIdSubtype);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepDbChassisId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepDbChassisId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepDbChassisId (UINT4
                               u4FsMIEcfmContextId,
                               UINT4
                               u4FsMIEcfmMdIndex,
                               UINT4
                               u4FsMIEcfmMaIndex,
                               UINT4
                               u4FsMIEcfmMepIdentifier,
                               UINT4
                               u4FsMIEcfmMepDbRMepIdentifier,
                               tSNMP_OCTET_STRING_TYPE
                               * pSetValFsMIEcfmRMepDbChassisId)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmMepDbChassisId (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           pSetValFsMIEcfmRMepDbChassisId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepManAddressDomain
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepManAddressDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepManAddressDomain (UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    tSNMP_OID_TYPE *
                                    pSetValFsMIEcfmRMepManAddressDomain)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepManAddressDomain (u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 pSetValFsMIEcfmRMepManAddressDomain);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRMepManAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                setValFsMIEcfmRMepManAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRMepManAddress (UINT4
                              u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4
                              u4FsMIEcfmMepIdentifier,
                              UINT4
                              u4FsMIEcfmMepDbRMepIdentifier,
                              tSNMP_OCTET_STRING_TYPE
                              * pSetValFsMIEcfmRMepManAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmRMepManAddress (u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4FsMIEcfmMepDbRMepIdentifier,
                                           pSetValFsMIEcfmRMepManAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepPortStatusDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepPortStatusDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepPortStatusDefect (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4FsMIEcfmContextId,
                                       UINT4
                                       u4FsMIEcfmMdIndex,
                                       UINT4
                                       u4FsMIEcfmMaIndex,
                                       UINT4
                                       u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       u4FsMIEcfmMepDbRMepIdentifier,
                                       INT4
                                       i4TestValFsMIEcfmRMepPortStatusDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepPortStatusDefect (pu4ErrorCode,
                                                    u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    u4FsMIEcfmMepIdentifier,
                                                    u4FsMIEcfmMepDbRMepIdentifier,
                                                    i4TestValFsMIEcfmRMepPortStatusDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepInterfaceStatusDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepInterfaceStatusDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepInterfaceStatusDefect (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4FsMIEcfmContextId,
                                            UINT4
                                            u4FsMIEcfmMdIndex,
                                            UINT4
                                            u4FsMIEcfmMaIndex,
                                            UINT4
                                            u4FsMIEcfmMepIdentifier,
                                            UINT4
                                            u4FsMIEcfmMepDbRMepIdentifier,
                                            INT4
                                            i4TestValFsMIEcfmRMepInterfaceStatusDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepInterfaceStatusDefect (pu4ErrorCode,
                                                         u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMepIdentifier,
                                                         u4FsMIEcfmMepDbRMepIdentifier,
                                                         i4TestValFsMIEcfmRMepInterfaceStatusDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepCcmDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepCcmDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepCcmDefect (UINT4 *pu4ErrorCode,
                                UINT4
                                u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4
                                u4FsMIEcfmMepIdentifier,
                                UINT4
                                u4FsMIEcfmMepDbRMepIdentifier,
                                INT4 i4TestValFsMIEcfmRMepCcmDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepCcmDefect (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             u4FsMIEcfmMepDbRMepIdentifier,
                                             i4TestValFsMIEcfmRMepCcmDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepRDIDefect
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepRDIDefect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepRDIDefect (UINT4 *pu4ErrorCode,
                                UINT4
                                u4FsMIEcfmContextId,
                                UINT4 u4FsMIEcfmMdIndex,
                                UINT4 u4FsMIEcfmMaIndex,
                                UINT4
                                u4FsMIEcfmMepIdentifier,
                                UINT4
                                u4FsMIEcfmMepDbRMepIdentifier,
                                INT4 i4TestValFsMIEcfmRMepRDIDefect)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepRDIDefect (pu4ErrorCode,
                                             u4FsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             u4FsMIEcfmMepDbRMepIdentifier,
                                             i4TestValFsMIEcfmRMepRDIDefect);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepMacAddress (UINT4 *pu4ErrorCode,
                                 UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 UINT4
                                 u4FsMIEcfmMepDbRMepIdentifier,
                                 tMacAddr TestValFsMIEcfmRMepMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepMacAddress (pu4ErrorCode,
                                              u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              u4FsMIEcfmMepDbRMepIdentifier,
                                              TestValFsMIEcfmRMepMacAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepRdi
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepRdi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepRdi (UINT4 *pu4ErrorCode,
                          UINT4 u4FsMIEcfmContextId,
                          UINT4 u4FsMIEcfmMdIndex,
                          UINT4 u4FsMIEcfmMaIndex,
                          UINT4 u4FsMIEcfmMepIdentifier,
                          UINT4
                          u4FsMIEcfmMepDbRMepIdentifier,
                          INT4 i4TestValFsMIEcfmRMepRdi)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepRdi (pu4ErrorCode,
                                       u4FsMIEcfmMdIndex,
                                       u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       u4FsMIEcfmMepDbRMepIdentifier,
                                       i4TestValFsMIEcfmRMepRdi);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepPortStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepPortStatusTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepPortStatusTlv (UINT4 *pu4ErrorCode,
                                    UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    UINT4
                                    u4FsMIEcfmMepDbRMepIdentifier,
                                    INT4 i4TestValFsMIEcfmRMepPortStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepPortStatusTlv (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 u4FsMIEcfmMepDbRMepIdentifier,
                                                 i4TestValFsMIEcfmRMepPortStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepInterfaceStatusTlv
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepInterfaceStatusTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepInterfaceStatusTlv (UINT4
                                         *pu4ErrorCode,
                                         UINT4
                                         u4FsMIEcfmContextId,
                                         UINT4
                                         u4FsMIEcfmMdIndex,
                                         UINT4
                                         u4FsMIEcfmMaIndex,
                                         UINT4
                                         u4FsMIEcfmMepIdentifier,
                                         UINT4
                                         u4FsMIEcfmMepDbRMepIdentifier,
                                         INT4
                                         i4TestValFsMIEcfmRMepInterfaceStatusTlv)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepInterfaceStatusTlv (pu4ErrorCode,
                                                      u4FsMIEcfmMdIndex,
                                                      u4FsMIEcfmMaIndex,
                                                      u4FsMIEcfmMepIdentifier,
                                                      u4FsMIEcfmMepDbRMepIdentifier,
                                                      i4TestValFsMIEcfmRMepInterfaceStatusTlv);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepChassisIdSubtype
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepChassisIdSubtype
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepChassisIdSubtype (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4FsMIEcfmContextId,
                                       UINT4
                                       u4FsMIEcfmMdIndex,
                                       UINT4
                                       u4FsMIEcfmMaIndex,
                                       UINT4
                                       u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       u4FsMIEcfmMepDbRMepIdentifier,
                                       INT4
                                       i4TestValFsMIEcfmRMepChassisIdSubtype)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepChassisIdSubtype (pu4ErrorCode,
                                                    u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    u4FsMIEcfmMepIdentifier,
                                                    u4FsMIEcfmMepDbRMepIdentifier,
                                                    i4TestValFsMIEcfmRMepChassisIdSubtype);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepDbChassisId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepDbChassisId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepDbChassisId (UINT4 *pu4ErrorCode,
                                  UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  UINT4
                                  u4FsMIEcfmMepDbRMepIdentifier,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pTestValFsMIEcfmRMepDbChassisId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepDbChassisId (pu4ErrorCode,
                                              u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              u4FsMIEcfmMepDbRMepIdentifier,
                                              pTestValFsMIEcfmRMepDbChassisId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepManAddressDomain
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepManAddressDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepManAddressDomain (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4FsMIEcfmContextId,
                                       UINT4
                                       u4FsMIEcfmMdIndex,
                                       UINT4
                                       u4FsMIEcfmMaIndex,
                                       UINT4
                                       u4FsMIEcfmMepIdentifier,
                                       UINT4
                                       u4FsMIEcfmMepDbRMepIdentifier,
                                       tSNMP_OID_TYPE *
                                       pTestValFsMIEcfmRMepManAddressDomain)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepManAddressDomain (pu4ErrorCode,
                                                    u4FsMIEcfmMdIndex,
                                                    u4FsMIEcfmMaIndex,
                                                    u4FsMIEcfmMepIdentifier,
                                                    u4FsMIEcfmMepDbRMepIdentifier,
                                                    pTestValFsMIEcfmRMepManAddressDomain);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRMepManAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier

                The Object 
                testValFsMIEcfmRMepManAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRMepManAddress (UINT4 *pu4ErrorCode,
                                 UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 UINT4
                                 u4FsMIEcfmMepDbRMepIdentifier,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pTestValFsMIEcfmRMepManAddress)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmRMepManAddress (pu4ErrorCode,
                                              u4FsMIEcfmMdIndex,
                                              u4FsMIEcfmMaIndex,
                                              u4FsMIEcfmMepIdentifier,
                                              u4FsMIEcfmMepDbRMepIdentifier,
                                              pTestValFsMIEcfmRMepManAddress);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmMepDbRMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmRemoteMepDbExTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList *
                                    pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmLtmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmLtmTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtmSeqNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmLtmTable (UINT4 u4FsMIEcfmContextId,
                                          UINT4 u4FsMIEcfmMdIndex,
                                          UINT4 u4FsMIEcfmMaIndex,
                                          UINT4 u4FsMIEcfmMepIdentifier,
                                          UINT4 u4FsMIEcfmLtmSeqNumber)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsEcfmLtmTable (u4FsMIEcfmMdIndex,
                                                       u4FsMIEcfmMaIndex,
                                                       u4FsMIEcfmMepIdentifier,
                                                       u4FsMIEcfmLtmSeqNumber);

    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmLtmTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtmSeqNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmLtmTable (UINT4 *pu4FsMIEcfmContextId,
                                  UINT4 *pu4FsMIEcfmMdIndex,
                                  UINT4 *pu4FsMIEcfmMaIndex,
                                  UINT4 *pu4FsMIEcfmMepIdentifier,
                                  UINT4 *pu4FsMIEcfmLtmSeqNumber)
{
    return (nmhGetNextIndexFsMIEcfmLtmTable (0, pu4FsMIEcfmContextId, 0,
                                             pu4FsMIEcfmMdIndex, 0,
                                             pu4FsMIEcfmMaIndex, 0,
                                             pu4FsMIEcfmMepIdentifier, 0,
                                             pu4FsMIEcfmLtmSeqNumber));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmLtmTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                nextFsMIEcfmMepIdentifier
                FsMIEcfmLtmSeqNumber
                nextFsMIEcfmLtmSeqNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmLtmTable (UINT4 u4FsMIEcfmContextId,
                                 UINT4 *pu4NextFsMIEcfmContextId,
                                 UINT4 u4FsMIEcfmMdIndex,
                                 UINT4 *pu4NextFsMIEcfmMdIndex,
                                 UINT4 u4FsMIEcfmMaIndex,
                                 UINT4 *pu4NextFsMIEcfmMaIndex,
                                 UINT4 u4FsMIEcfmMepIdentifier,
                                 UINT4 *pu4NextFsMIEcfmMepIdentifier,
                                 UINT4 u4FsMIEcfmLtmSeqNumber,
                                 UINT4 *pu4NextFsMIEcfmLtmSeqNumber)
{
    UINT4               u4ContextId;

    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexFsEcfmLtmTable (u4FsMIEcfmMdIndex,
                                           pu4NextFsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           pu4NextFsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           pu4NextFsMIEcfmMepIdentifier,
                                           u4FsMIEcfmLtmSeqNumber,
                                           pu4NextFsMIEcfmLtmSeqNumber)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_LBLT_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        if (EcfmLbLtGetNextActiveContext (u4FsMIEcfmContextId,
                                          &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexFsEcfmLtmTable (pu4NextFsMIEcfmMdIndex,
                                           pu4NextFsMIEcfmMaIndex,
                                           pu4NextFsMIEcfmMepIdentifier,
                                           pu4NextFsMIEcfmLtmSeqNumber)
           != SNMP_SUCCESS);

    ECFM_LBLT_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtmTargetMacAddress
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtmSeqNumber

                The Object 
                retValFsMIEcfmLtmTargetMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtmTargetMacAddress (UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4FsMIEcfmMdIndex,
                                   UINT4 u4FsMIEcfmMaIndex,
                                   UINT4 u4FsMIEcfmMepIdentifier,
                                   UINT4 u4FsMIEcfmLtmSeqNumber,
                                   tMacAddr *
                                   pRetValFsMIEcfmLtmTargetMacAddress)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmLtmTargetMacAddress (u4FsMIEcfmMdIndex,
                                                u4FsMIEcfmMaIndex,
                                                u4FsMIEcfmMepIdentifier,
                                                u4FsMIEcfmLtmSeqNumber,
                                                pRetValFsMIEcfmLtmTargetMacAddress);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmLtmTtl
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                FsMIEcfmLtmSeqNumber

                The Object 
                retValFsMIEcfmLtmTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmLtmTtl (UINT4 u4FsMIEcfmContextId, UINT4 u4FsMIEcfmMdIndex,
                      UINT4 u4FsMIEcfmMaIndex,
                      UINT4 u4FsMIEcfmMepIdentifier,
                      UINT4 u4FsMIEcfmLtmSeqNumber,
                      UINT4 *pu4RetValFsMIEcfmLtmTtl)
{
    INT1                i1RetVal;
    if (ECFM_LBLT_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsEcfmLtmTtl (u4FsMIEcfmMdIndex,
                                   u4FsMIEcfmMaIndex,
                                   u4FsMIEcfmMepIdentifier,
                                   u4FsMIEcfmLtmSeqNumber,
                                   pu4RetValFsMIEcfmLtmTtl);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FsMIEcfmMepExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMepExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMepExTable (UINT4
                                            u4FsMIEcfmContextId,
                                            UINT4
                                            u4FsMIEcfmMdIndex,
                                            UINT4
                                            u4FsMIEcfmMaIndex,
                                            UINT4 u4FsMIEcfmMepIdentifier)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsEcfmMepExTable (u4FsMIEcfmMdIndex,
                                                         u4FsMIEcfmMaIndex,
                                                         u4FsMIEcfmMepIdentifier);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMepExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMepExTable (UINT4
                                    *pu4FsMIEcfmContextId,
                                    UINT4
                                    *pu4FsMIEcfmMdIndex,
                                    UINT4
                                    *pu4FsMIEcfmMaIndex,
                                    UINT4 *pu4FsMIEcfmMepIdentifier)
{
    return (nmhGetNextIndexFsMIEcfmMepExTable (0, pu4FsMIEcfmContextId, 0,
                                               pu4FsMIEcfmMdIndex, 0,
                                               pu4FsMIEcfmMaIndex, 0,
                                               pu4FsMIEcfmMepIdentifier));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMepExTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
                nextFsMIEcfmMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMepExTable (UINT4
                                   u4FsMIEcfmContextId,
                                   UINT4
                                   *pu4NextFsMIEcfmContextId,
                                   UINT4
                                   u4FsMIEcfmMdIndex,
                                   UINT4
                                   *pu4NextFsMIEcfmMdIndex,
                                   UINT4
                                   u4FsMIEcfmMaIndex,
                                   UINT4
                                   *pu4NextFsMIEcfmMaIndex,
                                   UINT4
                                   u4FsMIEcfmMepIdentifier,
                                   UINT4 *pu4NextFsMIEcfmMepIdentifier)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexFsEcfmMepExTable (u4FsMIEcfmMdIndex,
                                             pu4NextFsMIEcfmMdIndex,
                                             u4FsMIEcfmMaIndex,
                                             pu4NextFsMIEcfmMaIndex,
                                             u4FsMIEcfmMepIdentifier,
                                             pu4NextFsMIEcfmMepIdentifier)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexFsEcfmMepExTable (pu4NextFsMIEcfmMdIndex,
                                             pu4NextFsMIEcfmMaIndex,
                                             pu4NextFsMIEcfmMepIdentifier)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmXconnRMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmXconnRMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmXconnRMepId (UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4
                           u4FsMIEcfmMepIdentifier,
                           UINT4 *pu4RetValFsMIEcfmXconnRMepId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmXconnRMepId (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 pu4RetValFsMIEcfmXconnRMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmErrorRMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmErrorRMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmErrorRMepId (UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4
                           u4FsMIEcfmMepIdentifier,
                           UINT4 *pu4RetValFsMIEcfmErrorRMepId)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmErrorRMepId (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 pu4RetValFsMIEcfmErrorRMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDefectRDICcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDefectRDICcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDefectRDICcm (UINT4
                               u4FsMIEcfmContextId,
                               UINT4 u4FsMIEcfmMdIndex,
                               UINT4 u4FsMIEcfmMaIndex,
                               UINT4
                               u4FsMIEcfmMepIdentifier,
                               INT4 *pi4RetValFsMIEcfmMepDefectRDICcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepDefectRDICcm (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     pi4RetValFsMIEcfmMepDefectRDICcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDefectMacStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDefectMacStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDefectMacStatus (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  INT4 *pi4RetValFsMIEcfmMepDefectMacStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepDefectMacStatus (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        pi4RetValFsMIEcfmMepDefectMacStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDefectRemoteCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDefectRemoteCcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDefectRemoteCcm (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  INT4 *pi4RetValFsMIEcfmMepDefectRemoteCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepDefectRemoteCcm (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        pi4RetValFsMIEcfmMepDefectRemoteCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDefectErrorCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDefectErrorCcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDefectErrorCcm (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 INT4 *pi4RetValFsMIEcfmMepDefectErrorCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepDefectErrorCcm (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       pi4RetValFsMIEcfmMepDefectErrorCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepDefectXconnCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                retValFsMIEcfmMepDefectXconnCcm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepDefectXconnCcm (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 INT4 *pi4RetValFsMIEcfmMepDefectXconnCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMepDefectXconnCcm (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       pi4RetValFsMIEcfmMepDefectXconnCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmXconnRMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmXconnRMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmXconnRMepId (UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4
                           u4FsMIEcfmMepIdentifier,
                           UINT4 u4SetValFsMIEcfmXconnRMepId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmXconnRMepId (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 u4SetValFsMIEcfmXconnRMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmErrorRMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmErrorRMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmErrorRMepId (UINT4 u4FsMIEcfmContextId,
                           UINT4 u4FsMIEcfmMdIndex,
                           UINT4 u4FsMIEcfmMaIndex,
                           UINT4
                           u4FsMIEcfmMepIdentifier,
                           UINT4 u4SetValFsMIEcfmErrorRMepId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmErrorRMepId (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                 u4FsMIEcfmMepIdentifier,
                                 u4SetValFsMIEcfmErrorRMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepDefectRDICcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepDefectRDICcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepDefectRDICcm (UINT4
                               u4FsMIEcfmContextId,
                               UINT4
                               u4FsMIEcfmMdIndex,
                               UINT4
                               u4FsMIEcfmMaIndex,
                               UINT4
                               u4FsMIEcfmMepIdentifier,
                               INT4 i4SetValFsMIEcfmMepDefectRDICcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMepDefectRDICcm (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                     u4FsMIEcfmMepIdentifier,
                                     i4SetValFsMIEcfmMepDefectRDICcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepDefectMacStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepDefectMacStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepDefectMacStatus (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  INT4 i4SetValFsMIEcfmMepDefectMacStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMepDefectMacStatus (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        i4SetValFsMIEcfmMepDefectMacStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepDefectRemoteCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepDefectRemoteCcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepDefectRemoteCcm (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  INT4 i4SetValFsMIEcfmMepDefectRemoteCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMepDefectRemoteCcm (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        u4FsMIEcfmMepIdentifier,
                                        i4SetValFsMIEcfmMepDefectRemoteCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepDefectErrorCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepDefectErrorCcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepDefectErrorCcm (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 INT4 i4SetValFsMIEcfmMepDefectErrorCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMepDefectErrorCcm (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       i4SetValFsMIEcfmMepDefectErrorCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepDefectXconnCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                setValFsMIEcfmMepDefectXconnCcm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepDefectXconnCcm (UINT4
                                 u4FsMIEcfmContextId,
                                 UINT4
                                 u4FsMIEcfmMdIndex,
                                 UINT4
                                 u4FsMIEcfmMaIndex,
                                 UINT4
                                 u4FsMIEcfmMepIdentifier,
                                 INT4 i4SetValFsMIEcfmMepDefectXconnCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsEcfmMepDefectXconnCcm (u4FsMIEcfmMdIndex, u4FsMIEcfmMaIndex,
                                       u4FsMIEcfmMepIdentifier,
                                       i4SetValFsMIEcfmMepDefectXconnCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmXconnRMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmXconnRMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmXconnRMepId (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4
                              u4FsMIEcfmMepIdentifier,
                              UINT4 u4TestValFsMIEcfmXconnRMepId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmXconnRMepId (pu4ErrorCode,
                                           u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4TestValFsMIEcfmXconnRMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmErrorRMepId
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmErrorRMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmErrorRMepId (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIEcfmContextId,
                              UINT4 u4FsMIEcfmMdIndex,
                              UINT4 u4FsMIEcfmMaIndex,
                              UINT4
                              u4FsMIEcfmMepIdentifier,
                              UINT4 u4TestValFsMIEcfmErrorRMepId)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmErrorRMepId (pu4ErrorCode,
                                           u4FsMIEcfmMdIndex,
                                           u4FsMIEcfmMaIndex,
                                           u4FsMIEcfmMepIdentifier,
                                           u4TestValFsMIEcfmErrorRMepId);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepDefectRDICcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepDefectRDICcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepDefectRDICcm (UINT4 *pu4ErrorCode,
                                  UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4
                                  u4FsMIEcfmMepIdentifier,
                                  INT4 i4TestValFsMIEcfmMepDefectRDICcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepDefectRDICcm (pu4ErrorCode,
                                               u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmMaIndex,
                                               u4FsMIEcfmMepIdentifier,
                                               i4TestValFsMIEcfmMepDefectRDICcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepDefectMacStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepDefectMacStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepDefectMacStatus (UINT4
                                     *pu4ErrorCode,
                                     UINT4
                                     u4FsMIEcfmContextId,
                                     UINT4
                                     u4FsMIEcfmMdIndex,
                                     UINT4
                                     u4FsMIEcfmMaIndex,
                                     UINT4
                                     u4FsMIEcfmMepIdentifier,
                                     INT4 i4TestValFsMIEcfmMepDefectMacStatus)
{

    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepDefectMacStatus (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  u4FsMIEcfmMaIndex,
                                                  u4FsMIEcfmMepIdentifier,
                                                  i4TestValFsMIEcfmMepDefectMacStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepDefectRemoteCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepDefectRemoteCcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepDefectRemoteCcm (UINT4
                                     *pu4ErrorCode,
                                     UINT4
                                     u4FsMIEcfmContextId,
                                     UINT4
                                     u4FsMIEcfmMdIndex,
                                     UINT4
                                     u4FsMIEcfmMaIndex,
                                     UINT4
                                     u4FsMIEcfmMepIdentifier,
                                     INT4 i4TestValFsMIEcfmMepDefectRemoteCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepDefectRemoteCcm (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  u4FsMIEcfmMaIndex,
                                                  u4FsMIEcfmMepIdentifier,
                                                  i4TestValFsMIEcfmMepDefectRemoteCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepDefectErrorCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepDefectErrorCcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepDefectErrorCcm (UINT4 *pu4ErrorCode,
                                    UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    INT4 i4TestValFsMIEcfmMepDefectErrorCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepDefectErrorCcm (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 i4TestValFsMIEcfmMepDefectErrorCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepDefectXconnCcm
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier

                The Object 
                testValFsMIEcfmMepDefectXconnCcm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepDefectXconnCcm (UINT4 *pu4ErrorCode,
                                    UINT4
                                    u4FsMIEcfmContextId,
                                    UINT4
                                    u4FsMIEcfmMdIndex,
                                    UINT4
                                    u4FsMIEcfmMaIndex,
                                    UINT4
                                    u4FsMIEcfmMepIdentifier,
                                    INT4 i4TestValFsMIEcfmMepDefectXconnCcm)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMepDefectXconnCcm (pu4ErrorCode,
                                                 u4FsMIEcfmMdIndex,
                                                 u4FsMIEcfmMaIndex,
                                                 u4FsMIEcfmMepIdentifier,
                                                 i4TestValFsMIEcfmMepDefectXconnCcm);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMepExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
                FsMIEcfmMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMepExTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList *
                            pSnmpIndexList, tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMdExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMdExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMdExTable (UINT4
                                           u4FsMIEcfmContextId,
                                           UINT4 u4FsMIEcfmMdIndex)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsEcfmMdExTable (u4FsMIEcfmMdIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMdExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMdExTable (UINT4
                                   *pu4FsMIEcfmContextId,
                                   UINT4 *pu4FsMIEcfmMdIndex)
{
    return (nmhGetNextIndexFsMIEcfmMdExTable (0, pu4FsMIEcfmContextId, 0,
                                              pu4FsMIEcfmMdIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMdExTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMdExTable (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  *pu4NextFsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4 *pu4NextFsMIEcfmMdIndex)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexFsEcfmMdExTable (u4FsMIEcfmMdIndex,
                                            pu4NextFsMIEcfmMdIndex) ==
            SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexFsEcfmMdExTable (pu4NextFsMIEcfmMdIndex)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMepArchiveHoldTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                retValFsMIEcfmMepArchiveHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMepArchiveHoldTime (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  INT4 *pi4RetValFsMIEcfmMepArchiveHoldTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsEcfmMepArchiveHoldTime (u4FsMIEcfmMdIndex,
                                               pi4RetValFsMIEcfmMepArchiveHoldTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMepArchiveHoldTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                setValFsMIEcfmMepArchiveHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMepArchiveHoldTime (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  INT4 i4SetValFsMIEcfmMepArchiveHoldTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsEcfmMepArchiveHoldTime (u4FsMIEcfmMdIndex,
                                               i4SetValFsMIEcfmMepArchiveHoldTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMepArchiveHoldTime
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex

                The Object 
                testValFsMIEcfmMepArchiveHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMepArchiveHoldTime (UINT4
                                     *pu4ErrorCode,
                                     UINT4
                                     u4FsMIEcfmContextId,
                                     UINT4
                                     u4FsMIEcfmMdIndex,
                                     INT4 i4TestValFsMIEcfmMepArchiveHoldTime)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsEcfmMepArchiveHoldTime (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  i4TestValFsMIEcfmMepArchiveHoldTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMdExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMdExTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList *
                           pSnmpIndexList, tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmMaExTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmMaExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmMaExTable (UINT4
                                           u4FsMIEcfmContextId,
                                           UINT4
                                           u4FsMIEcfmMdIndex,
                                           UINT4 u4FsMIEcfmMaIndex)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsEcfmMaExTable (u4FsMIEcfmMdIndex,
                                                        u4FsMIEcfmMaIndex);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmMaExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmMaExTable (UINT4
                                   *pu4FsMIEcfmContextId,
                                   UINT4
                                   *pu4FsMIEcfmMdIndex,
                                   UINT4 *pu4FsMIEcfmMaIndex)
{
    return (nmhGetNextIndexFsMIEcfmMaExTable (0, pu4FsMIEcfmContextId, 0,
                                              pu4FsMIEcfmMdIndex, 0,
                                              pu4FsMIEcfmMaIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmMaExTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
                FsMIEcfmMdIndex
                nextFsMIEcfmMdIndex
                FsMIEcfmMaIndex
                nextFsMIEcfmMaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmMaExTable (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  *pu4NextFsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  *pu4NextFsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  UINT4 *pu4NextFsMIEcfmMaIndex)
{
    UINT4               u4ContextId;

    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) == ECFM_SUCCESS)
    {
        if (nmhGetNextIndexFsEcfmMaExTable (u4FsMIEcfmMdIndex,
                                            pu4NextFsMIEcfmMdIndex,
                                            u4FsMIEcfmMaIndex,
                                            pu4NextFsMIEcfmMaIndex)
            == SNMP_SUCCESS)
        {
            *pu4NextFsMIEcfmContextId = u4FsMIEcfmContextId;

            ECFM_CC_RELEASE_CONTEXT ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        ECFM_CC_RELEASE_CONTEXT ();
        if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                        &u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIEcfmContextId = u4ContextId;
        *pu4NextFsMIEcfmContextId = u4ContextId;

    }
    while (nmhGetFirstIndexFsEcfmMaExTable (pu4NextFsMIEcfmMdIndex,
                                            pu4NextFsMIEcfmMaIndex)
           != SNMP_SUCCESS);

    ECFM_CC_RELEASE_CONTEXT ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmMaCrosscheckStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                retValFsMIEcfmMaCrosscheckStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmMaCrosscheckStatus (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  INT4 *pi4RetValFsMIEcfmMaCrosscheckStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsEcfmMaCrosscheckStatus (u4FsMIEcfmMdIndex,
                                        u4FsMIEcfmMaIndex,
                                        pi4RetValFsMIEcfmMaCrosscheckStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmMaCrosscheckStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                setValFsMIEcfmMaCrosscheckStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmMaCrosscheckStatus (UINT4
                                  u4FsMIEcfmContextId,
                                  UINT4
                                  u4FsMIEcfmMdIndex,
                                  UINT4
                                  u4FsMIEcfmMaIndex,
                                  INT4 i4SetValFsMIEcfmMaCrosscheckStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsEcfmMaCrosscheckStatus (u4FsMIEcfmMdIndex,
                                               u4FsMIEcfmMaIndex,
                                               i4SetValFsMIEcfmMaCrosscheckStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmMaCrosscheckStatus
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex

                The Object 
                testValFsMIEcfmMaCrosscheckStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmMaCrosscheckStatus (UINT4
                                     *pu4ErrorCode,
                                     UINT4
                                     u4FsMIEcfmContextId,
                                     UINT4
                                     u4FsMIEcfmMdIndex,
                                     UINT4
                                     u4FsMIEcfmMaIndex,
                                     INT4 i4TestValFsMIEcfmMaCrosscheckStatus)
{
    INT1                i1RetVal;
    if (ECFM_CC_SELECT_CONTEXT (u4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsEcfmMaCrosscheckStatus (pu4ErrorCode,
                                                  u4FsMIEcfmMdIndex,
                                                  u4FsMIEcfmMaIndex,
                                                  i4TestValFsMIEcfmMaCrosscheckStatus);
    ECFM_CC_RELEASE_CONTEXT ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmMaExTable
 Input       :  The Indices
                FsMIEcfmContextId
                FsMIEcfmMdIndex
                FsMIEcfmMaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmMaExTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList *
                           pSnmpIndexList, tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIEcfmStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEcfmStatsTable
 Input       :  The Indices
                FsMIEcfmContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEcfmStatsTable (UINT4 u4FsMIEcfmContextId)
{
    if (EcfmCcIsContextExist (u4FsMIEcfmContextId) != ECFM_TRUE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEcfmStatsTable
 Input       :  The Indices
                FsMIEcfmContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEcfmStatsTable (UINT4 *pu4FsMIEcfmContextId)
{
    if (EcfmCcGetFirstActiveContext (pu4FsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEcfmStatsTable
 Input       :  The Indices
                FsMIEcfmContextId
                nextFsMIEcfmContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEcfmStatsTable (UINT4 u4FsMIEcfmContextId,
                                   UINT4 *pu4NextFsMIEcfmContextId)
{
    if (EcfmCcGetNextActiveContext (u4FsMIEcfmContextId,
                                    pu4NextFsMIEcfmContextId) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxCfmPduCount (UINT4 u4FsMIEcfmContextId,
                             UINT4 *pu4RetValFsMIEcfmTxCfmPduCount)
{
    UINT4               u4TxCccount = ECFM_INIT_VAL;

    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmTxCfmPduCount,
                               ECFM_CTX_CFM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    u4TxCccount = *pu4RetValFsMIEcfmTxCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmTxCfmPduCount,
                                 ECFM_CTX_CFM_OUT) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEcfmTxCfmPduCount = u4TxCccount +
        *pu4RetValFsMIEcfmTxCfmPduCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxCcmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxCcmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxCcmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmTxCcmCount)
{
    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmTxCcmCount,
                               ECFM_CTX_CCM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxLbmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxLbmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxLbmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmTxLbmCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmTxLbmCount,
                                 ECFM_CTX_LBM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxLbrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxLbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxLbrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmTxLbrCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmTxLbrCount,
                                 ECFM_CTX_LBR_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxLtmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxLtmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxLtmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmTxLtmCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmTxLtmCount,
                                 ECFM_CTX_LTM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxLtrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxLtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxLtrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmTxLtrCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmTxLtrCount,
                                 ECFM_CTX_LTR_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmTxFailedCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmTxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmTxFailedCount (UINT4 u4FsMIEcfmContextId,
                             UINT4 *pu4RetValFsMIEcfmTxFailedCount)
{
    UINT4               u4TxCccount = ECFM_INIT_VAL;

    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmTxFailedCount,
                               ECFM_CTX_FAILED_COUNT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4TxCccount = *pu4RetValFsMIEcfmTxFailedCount;

    ECFM_LBLT_LOCK ();
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmTxFailedCount,
                                 ECFM_CTX_FAILED_COUNT) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEcfmTxFailedCount = u4TxCccount +
        *pu4RetValFsMIEcfmTxFailedCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxCfmPduCount (UINT4 u4FsMIEcfmContextId,
                             UINT4 *pu4RetValFsMIEcfmRxCfmPduCount)
{
    UINT4               u4RxCcCfmcount = ECFM_INIT_VAL;

    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmRxCfmPduCount,
                               ECFM_CTX_CFM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4RxCcCfmcount = *pu4RetValFsMIEcfmRxCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmRxCfmPduCount,
                                 ECFM_CTX_CFM_IN) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEcfmRxCfmPduCount = u4RxCcCfmcount +
        *pu4RetValFsMIEcfmRxCfmPduCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxCcmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxCcmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxCcmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmRxCcmCount)
{
    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmRxCcmCount,
                               ECFM_CTX_CCM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxLbmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxLbmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxLbmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmRxLbmCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmRxLbmCount,
                                 ECFM_CTX_LBM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxLbrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxLbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxLbrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmRxLbrCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmRxLbrCount,
                                 ECFM_CTX_LBR_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxLtmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxLtmCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxLtmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmRxLtmCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmRxLtmCount,
                                 ECFM_CTX_LTM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxLtrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxLtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxLtrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 *pu4RetValFsMIEcfmRxLtrCount)
{
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmRxLtrCount,
                                 ECFM_CTX_LTR_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmRxBadCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmRxBadCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmRxBadCfmPduCount (UINT4 u4FsMIEcfmContextId,
                                UINT4 *pu4RetValFsMIEcfmRxBadCfmPduCount)
{
    UINT4               u4RxCcBadCfmcount = ECFM_INIT_VAL;

    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmRxBadCfmPduCount,
                               ECFM_CTX_BAD_PDU) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4RxCcBadCfmcount = *pu4RetValFsMIEcfmRxBadCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmRxBadCfmPduCount,
                                 ECFM_CTX_BAD_PDU) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEcfmRxBadCfmPduCount = u4RxCcBadCfmcount +
        *pu4RetValFsMIEcfmRxBadCfmPduCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmFrwdCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmFrwdCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmFrwdCfmPduCount (UINT4 u4FsMIEcfmContextId,
                               UINT4 *pu4RetValFsMIEcfmFrwdCfmPduCount)
{
    UINT4               u4CcFrwdCfmCount = ECFM_INIT_VAL;

    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmFrwdCfmPduCount,
                               ECFM_CTX_FWD_PDU) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4CcFrwdCfmCount = *pu4RetValFsMIEcfmFrwdCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmFrwdCfmPduCount,
                                 ECFM_CTX_FWD_PDU) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEcfmFrwdCfmPduCount = u4CcFrwdCfmCount +
        *pu4RetValFsMIEcfmFrwdCfmPduCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcfmDsrdCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                retValFsMIEcfmDsrdCfmPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEcfmDsrdCfmPduCount (UINT4 u4FsMIEcfmContextId,
                               UINT4 *pu4RetValFsMIEcfmDsrdCfmPduCount)
{
    UINT4               u4TxCcDsrdCfmcount = ECFM_INIT_VAL;

    if (EcfmGetCcContextStats (u4FsMIEcfmContextId,
                               pu4RetValFsMIEcfmDsrdCfmPduCount,
                               ECFM_CTX_DSRD_PDU) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4TxCcDsrdCfmcount = *pu4RetValFsMIEcfmDsrdCfmPduCount;

    ECFM_LBLT_LOCK ();
    if (EcfmGetLbLtContextStats (u4FsMIEcfmContextId,
                                 pu4RetValFsMIEcfmDsrdCfmPduCount,
                                 ECFM_CTX_DSRD_PDU) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEcfmDsrdCfmPduCount = u4TxCcDsrdCfmcount +
        *pu4RetValFsMIEcfmDsrdCfmPduCount;
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxCfmPduCount (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4SetValFsMIEcfmTxCfmPduCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxCfmPduCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_CFM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_CFM_OUT) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxCcmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxCcmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxCcmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmTxCcmCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxCcmCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_CCM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxLbmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxLbmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxLbmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmTxLbmCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxLbmCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LBM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxLbrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxLbrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxLbrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmTxLbrCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxLbrCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LBR_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxLtmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxLtmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxLtmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmTxLtmCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxLtmCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LTM_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxLtrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxLtrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxLtrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmTxLtrCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxLtrCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LTR_OUT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmTxFailedCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmTxFailedCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmTxFailedCount (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4SetValFsMIEcfmTxFailedCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmTxFailedCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_FAILED_COUNT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_FAILED_COUNT) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxCfmPduCount (UINT4 u4FsMIEcfmContextId,
                             UINT4 u4SetValFsMIEcfmRxCfmPduCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxCfmPduCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_CFM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_CFM_IN) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxCcmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxCcmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxCcmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmRxCcmCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxCcmCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_CCM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxLbmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxLbmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxLbmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmRxLbmCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxLbmCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LBM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxLbrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxLbrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxLbrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmRxLbrCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxLbrCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LBR_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxLtmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxLtmCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxLtmCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmRxLtmCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxLtmCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LTM_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxLtrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxLtrCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxLtrCount (UINT4 u4FsMIEcfmContextId,
                          UINT4 u4SetValFsMIEcfmRxLtrCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxLtrCount);
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_LTR_IN) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmRxBadCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmRxBadCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmRxBadCfmPduCount (UINT4 u4FsMIEcfmContextId,
                                UINT4 u4SetValFsMIEcfmRxBadCfmPduCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmRxBadCfmPduCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_BAD_PDU) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_BAD_PDU) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmFrwdCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmFrwdCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmFrwdCfmPduCount (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4SetValFsMIEcfmFrwdCfmPduCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmFrwdCfmPduCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_FWD_PDU) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_FWD_PDU) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcfmDsrdCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                setValFsMIEcfmDsrdCfmPduCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEcfmDsrdCfmPduCount (UINT4 u4FsMIEcfmContextId,
                               UINT4 u4SetValFsMIEcfmDsrdCfmPduCount)
{
    UNUSED_PARAM (u4SetValFsMIEcfmDsrdCfmPduCount);
    if (EcfmClearCcContextStats (u4FsMIEcfmContextId,
                                 ECFM_CTX_DSRD_PDU) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmClearLbLtContextStats (u4FsMIEcfmContextId,
                                   ECFM_CTX_DSRD_PDU) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxCfmPduCount (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIEcfmContextId,
                                UINT4 u4TestValFsMIEcfmTxCfmPduCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmTxCfmPduCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmTxCfmPduCount) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxCcmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxCcmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxCcmCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmTxCcmCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmTxCcmCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxLbmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxLbmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxLbmCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmTxLbmCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmTxLbmCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxLbrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxLbrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxLbrCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmTxLbrCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmTxLbrCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxLtmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxLtmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxLtmCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmTxLtmCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmTxLtmCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxLtrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxLtrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxLtrCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmTxLtrCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmTxLtrCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmTxFailedCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmTxFailedCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmTxFailedCount (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIEcfmContextId,
                                UINT4 u4TestValFsMIEcfmTxFailedCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmTxFailedCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmTxFailedCount) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxCfmPduCount (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIEcfmContextId,
                                UINT4 u4TestValFsMIEcfmRxCfmPduCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmRxCfmPduCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmRxCfmPduCount) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxCcmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxCcmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxCcmCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmRxCcmCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmRxCcmCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxLbmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxLbmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxLbmCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmRxLbmCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmRxLbmCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxLbrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxLbrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxLbrCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmRxLbrCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmRxLbrCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxLtmCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxLtmCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxLtmCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmRxLtmCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmRxLtmCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxLtrCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxLtrCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxLtrCount (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIEcfmContextId,
                             UINT4 u4TestValFsMIEcfmRxLtrCount)
{
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmRxLtrCount) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmRxBadCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmRxBadCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmRxBadCfmPduCount (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIEcfmContextId,
                                   UINT4 u4TestValFsMIEcfmRxBadCfmPduCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmRxBadCfmPduCount) !=
        ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmRxBadCfmPduCount) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmFrwdCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmFrwdCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmFrwdCfmPduCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4TestValFsMIEcfmFrwdCfmPduCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmFrwdCfmPduCount) !=
        ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmFrwdCfmPduCount) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcfmDsrdCfmPduCount
 Input       :  The Indices
                FsMIEcfmContextId

                The Object 
                testValFsMIEcfmDsrdCfmPduCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEcfmDsrdCfmPduCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIEcfmContextId,
                                  UINT4 u4TestValFsMIEcfmDsrdCfmPduCount)
{
    if (EcfmTestCcContextStats (pu4ErrorCode,
                                u4FsMIEcfmContextId,
                                u4TestValFsMIEcfmDsrdCfmPduCount) !=
        ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (EcfmTestLbLtContextStats (pu4ErrorCode,
                                  u4FsMIEcfmContextId,
                                  u4TestValFsMIEcfmDsrdCfmPduCount) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEcfmStatsTable
 Input       :  The Indices
                FsMIEcfmContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEcfmStatsTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
