/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmrmpsm.c,v 1.41 2015/12/31 11:25:34 siva Exp $
 *
 * Description: This file contains the Functionality of the Remote 
 *              MEP State Machine.
 *******************************************************************/

#include "cfminc.h"
#include "cfmrmpsm.h"

/****************************************************************************
 * Function Name      : EcfmCcClntRmepSm
 *
 * Description        : This is the Remote MEP State Machine that traverses 
 *                      the state event matrix depending event given to it. 
 *
 * Input(s)           : u1Event - Specifying the event received by this 
 *                                state machine.
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntRmepSm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    ECFM_CC_TRC_FN_ENTRY ();
    pRmepInfo = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);
    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcClntRmepSm: Called with Event: %d, and Current "
                      "State: %d\r\n", u1Event,
                      ECFM_CC_RMEP_GET_STATE (pRmepInfo));

    if (ECFM_CC_RMEP_STATE_MACHINE (u1Event, pRmepInfo->u1State, pPduSmInfo)
        != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmCcClntRmepSm:RMEP State machine for Mep %u "
                          " does not function Correctly\r\n",
                          pRmepInfo->u2MepId);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepSmSetStateDefault
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      MepNotActive in which state machine changes its current 
 *                      state to DEFAULT state.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmRmepSmSetStateDefault (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pRmepInfo = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);

    /*intialize Y1731 receving counter value to zero */
    pRmepInfo->u1Y1731RemCCMRxCount = ECFM_INIT_VAL;

    /* Stop the running RMepWhile timer */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_RMEP_WHILE, pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmRmepSmSetStateDefault:Unale to stop timer\r\n");
        return ECFM_FAILURE;
    }

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    /* Set rMEPCCMdefect to False */
    /* Simulate the exit log in case the defect was already present */
    if (pRmepInfo->b1RMepCcmDefect == ECFM_TRUE)

    {
        tEcfmCcPduSmInfo    PduSmInfo;
        tEcfmCcErrLogInfo  *pCcErrLog = NULL;
        PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;
        PduSmInfo.pRMepInfo = pRmepInfo;

        /*Add Error Exit to Error Log Table */
        pCcErrLog = EcfmCcAddErrorLogEntry (&PduSmInfo, ECFM_LOC_DFCT_EXIT);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_LOC_TRAP_EX_VAL);
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pRmepInfo->pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT
            (pRmepInfo->pMepInfo->u2PortNum, pTempPortInfo);

        MepInfo.u4MdIndex = pRmepInfo->pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pRmepInfo->pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pRmepInfo->pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pRmepInfo->pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pRmepInfo->pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
        MepInfo.u4SlaId = pRmepInfo->pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL,
                               ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    }
    pRmepInfo->b1RMepCcmDefect = ECFM_FALSE;
    if (pRmepInfo->b1LastRdi == ECFM_TRUE)

    {
        tEcfmCcErrLogInfo  *pCcErrLog = NULL;

        /*Add Error Exit to Error Log Table */
        pCcErrLog = EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_RDI_CCM_DFCT_EXIT);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_RDI_CCM_EX_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pRmepInfo->pMepInfo->pMaInfo->u4PrimaryVidIsid;
        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT
            (pRmepInfo->pMepInfo->u2PortNum, pTempPortInfo);

        MepInfo.u4MdIndex = pRmepInfo->pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pRmepInfo->pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pRmepInfo->pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pRmepInfo->pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pRmepInfo->pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
        MepInfo.u4SlaId = pRmepInfo->pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_RDI_CONDITION_CLEARED,
                               &MepInfo, NULL,
                               ECFM_RDI_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    }

    /* Clear RDI and mac status defects as we are not receiving anyting from the
     * remote MEP*/
    pRmepInfo->b1LastRdi = ECFM_FALSE;
    pRmepInfo->b1RMepPortStatusDefect = ECFM_FALSE;
    pRmepInfo->b1RMepInterfaceStatusDefect = ECFM_FALSE;
   
      /* Set Mac Address to Default value 0x00 */
    ECFM_MEMSET (pRmepInfo->RMepMacAddr, 0x00, ECFM_MAC_ADDR_LENGTH);
    EcfmRedCheckAndSyncSmData (&pRmepInfo->u1State,
                               ECFM_RMEP_STATE_DEFAULT, pPduSmInfo);

    /*State set to Default */
    ECFM_CC_RMEP_SET_STATE (pRmepInfo, ECFM_RMEP_STATE_DEFAULT);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepSmSetStateDefault:RMEP SEM Moved to Default "
                 "state\r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepSmSetStateStart
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      ECFM_SM_EV_BEGIN,This routine is used to initialize the 
 *                      variables to default values and set the current state 
 *                      of the state machine to the START state
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmRmepSmSetStateStart (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT2               u2CrossCheckDelay = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pRmepInfo = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);

    /*Intialize the Y1731 receiving counter to zero, this value will increment
     * on every receiption of CCM in y1731 mode */
    pRmepInfo->u1Y1731RemCCMRxCount = ECFM_INIT_VAL;

    /* Start rMepwhile timer with  3.5 * CCMInterval which waitd for the
     * reception of the valid CCM with in time period before raising the 
     * fault alarm */

    ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4Interval);

    u2CrossCheckDelay = ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;

    /* If OperStatus is DOWN, then generate fault */

    if (pMepInfo->pPortInfo->u1IfOperStatus != CFA_IF_UP)
    {
        return EcfmRmepSmSetStateFailed (pPduSmInfo);
    }

    if (u2CrossCheckDelay != 0)
    {
        u4Interval = u4Interval * (u2CrossCheckDelay);
    }
    else
    {
        u4Interval = u4Interval * ECFM_XCHK_DELAY_DEF_VAL;
    }

    if (pMepInfo->b1MepCcmOffloadStatus == ECFM_FALSE)
    {
        if (EcfmCcTmrStartTimer (ECFM_CC_TMR_RMEP_WHILE, pPduSmInfo,
                                 u4Interval) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "EcfmRmepSmSetStateStart:Start Timer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }

    /* Set Mac Address to Default value 0x00 */
    ECFM_MEMSET (pRmepInfo->RMepMacAddr, 0x00, ECFM_MAC_ADDR_LENGTH);

    /* Updated RMEP MAC address at LBLT also */
    if (EcfmLbLtAddRMepDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pRmepInfo) !=
        ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }

    /* Set RDI bit to false */
    pRmepInfo->b1LastRdi = ECFM_FALSE;

    /* Ser Port Status defect to false */
    pRmepInfo->b1RMepPortStatusDefect = ECFM_FALSE;

    /* Ser Interface Status defect to false */
    pRmepInfo->b1RMepInterfaceStatusDefect = ECFM_FALSE;

    /* Set PortStatus to psNoPortStateTLV */
    pRmepInfo->u1LastPortStatus = ECFM_NO_PORT_STATUS_TLV;

    /* Set InterfaceStatus to isNoInterfaceStatusTLV */
    pRmepInfo->u1LastInterfaceStatus = ECFM_IS_NO_INTERFACE_STATUS_TLV;

#ifdef MBSM_WANTED
    /* Set Line Card Status */
    pRmepInfo->u1MepCcmLCStatus = ECFM_MBSM_LC_STATUS;
#endif

    EcfmRedCheckAndSyncSmData (&pRmepInfo->u1State,
                               ECFM_RMEP_STATE_START, pPduSmInfo);

    /* Set Rmepstate to ECFM_RMEP_STATE_START */
    ECFM_CC_RMEP_SET_STATE (pRmepInfo, ECFM_RMEP_STATE_START);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepSmSetStateStart: RMEP SEM Moved to start state\r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepSmSetStateOk
 *
 * Description        :This routine is used to handle the valid CCM received
 *                     from the Remote MEP and change the state to OK from START
 *                     state and OK state.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmRmepSmSetStateOk (tEcfmCcPduSmInfo * pPduSmInfo)
{
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
#endif
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcRxCcmPduInfo *pCcRxInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmSenderId      *pSenderId = NULL;
    tEcfmMepInfoParams  MepInfo;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT1               u1RCcmCounter = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepInfo);
    pRmepInfo = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);
    pCcRxInfo = ECFM_CC_GET_CCM_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);
    pSenderId = &(pCcRxInfo->RecvdSenderId);

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    /* Inccrement u1y1731RxCount when ever you recieve CCM */
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        pRmepInfo->u1Y1731RemCCMRxCount++;
#ifdef NPAPI_WANTED
        if ((pMepInfo->b1MepCcmOffloadHwStatus == ECFM_TRUE) &&
            (pRmepInfo->u1Y1731RemCCMRxCount < ECFM_Y1731_LOC_EXIT_MIN))
        {
            MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

            EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepInfo, NULL);
            if (EcfmFsMiEcfmHwCallNpApi (FS_MI_ECFM_SET_HIT_ME_ONCE,
                                         &EcfmHwInfo) != FNP_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcInterruptQueueHandler: NPAPI"
                             "for Hit-me-Once returned"
                             "FAILURE\r\n");
            }
        }
#endif
    }

    if (pRmepInfo->b1RMepCcmDefect == ECFM_TRUE)

    {
        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) ==
            ECFM_FALSE)
        {
            MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;
            MepInfo.u4IfIndex =
                ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

            ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                                   &MepInfo, NULL,
                                   ECFM_DEFECT_CONDITION_CLEARED, ECFM_INIT_VAL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, ECFM_CC_TASK_ID);
            MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
            /* Set rMEPCCMdefect to False */
            pRmepInfo->b1RMepCcmDefect = ECFM_FALSE;
        }
        else                    /* In Y1731 mode */
        {
            if (pRmepInfo->u1Y1731RemCCMRxCount >= ECFM_Y1731_LOC_EXIT_MIN)
            {
                tEcfmCcErrLogInfo  *pCcErrLog = NULL;

                /*Add Error Exit to Error Log Table */
                pCcErrLog =
                    EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_LOC_DFCT_EXIT);

                /* Generate the SNMP trap for the fault */
                Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_LOC_TRAP_EX_VAL);
                MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
                MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

                /*Reset the Port Info Ptr */
                pTempPortInfo = NULL;
                MepInfo.u4IfIndex =
                    ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
                MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
                MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
                MepInfo.u2MepId = pMepInfo->u2MepId;
                MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
                MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

                ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                                       &MepInfo, NULL,
                                       ECFM_DEFECT_CONDITION_CLEARED,
                                       ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                       ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                       ECFM_CC_TASK_ID);
                MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
                pRmepInfo->b1RMepCcmDefect = ECFM_FALSE;
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmRmepSmSetStateOk:"
                             " LOC defect cleared!!\r\n ");
            }
        }

    }

    /* Stop the running timer */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_RMEP_WHILE, pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmRmepSmSetStateOk:Stop Timer FAILED\r\n");
    }
    if (pMepInfo->b1MepCcmOffloadStatus == ECFM_FALSE)
    {
        ECFM_GET_CCM_INTERVAL (pMaInfo->u1CcmInterval, u4Interval);

        u1RCcmCounter = (UINT1) ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;
        if (u1RCcmCounter != 0)
        {
            u4Interval = u4Interval * u1RCcmCounter;
        }
        else
        {
            u4Interval = u4Interval * ECFM_XCHK_DELAY_DEF_VAL;
        }

        if (EcfmCcTmrStartTimer
            (ECFM_CC_TMR_RMEP_WHILE, pPduSmInfo, u4Interval) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "EcfmRmepSmSetStateOk:Start Timer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }
    if (ECFM_MEMCMP
        (pRmepInfo->RMepMacAddr, pPduSmInfo->RxSrcMacAddr,
         ECFM_MAC_ADDR_LENGTH) != 0)

    {

        /* Set the RMEP macAddress to received source mac address */
        ECFM_MEMCPY (pRmepInfo->RMepMacAddr, pPduSmInfo->RxSrcMacAddr,
                     ECFM_MAC_ADDR_LENGTH);

        /* Updated RMEP MAC address at LBLT also */
        if (EcfmLbLtAddRMepDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pRmepInfo)
            != ECFM_SUCCESS)

        {
            return ECFM_FAILURE;
        }

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex =
            ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    }
    EcfmRedSyncCCDataOnChng (ECFM_CC_CURR_CONTEXT_ID (),
                             pRmepInfo->u4MdIndex,
                             pRmepInfo->u4MaIndex,
                             pRmepInfo->u2MepId, pRmepInfo->u2RMepId);

    /* Check if RDI condition has entered */
    if ((pRmepInfo->b1LastRdi == ECFM_FALSE) &&
        (pRmepInfo->b1LastRdi != ECFM_CC_GET_RDI (pPduSmInfo->u1RxFlags)))

    {
        tEcfmCcErrLogInfo  *pCcErrLog = NULL;

        /*Add Error Entry to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_RDI_CCM_DFCT_ENTRY);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_RDI_CCM_EN_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pRmepInfo->pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT
            (pRmepInfo->pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pRmepInfo->pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pRmepInfo->pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pRmepInfo->pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pRmepInfo->pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pRmepInfo->pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_RDI_CONDITION_ENCOUNTERED,
                               &MepInfo, NULL,
                               ECFM_RDI_CONDITION_ENCOUNTERED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    }

    /* Check if RDI condition has exited */
    if ((pRmepInfo->b1LastRdi == ECFM_TRUE) &&
        (pRmepInfo->b1LastRdi != ECFM_CC_GET_RDI (pPduSmInfo->u1RxFlags)))

    {
        tEcfmCcErrLogInfo  *pCcErrLog = NULL;

        /*Add Error Exit to Error Log Table */
        pCcErrLog = EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_RDI_CCM_DFCT_EXIT);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_RDI_CCM_EX_TRAP_VAL);

        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pRmepInfo->pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex = ECFM_CC_GET_PHY_PORT
            (pRmepInfo->pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pRmepInfo->pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pRmepInfo->pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pRmepInfo->pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pRmepInfo->pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pRmepInfo->pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

        ECFM_NOTIFY_PROTOCOLS (ECFM_RDI_CONDITION_CLEARED,
                               &MepInfo, NULL,
                               ECFM_RDI_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    }

    /* Set RDI to RDI bit received in the Pdu */
    pRmepInfo->b1LastRdi = ECFM_CC_GET_RDI (pPduSmInfo->u1RxFlags);

    /* Set PortState to recvd PortState */
    pRmepInfo->u1LastPortStatus = pCcRxInfo->u1RecvdPortStatus;

    /* Set rMEPportStatusDefect = True if (rMEPlastPortStatus != psUp &&
     * rMEPlastPortStatus!=psNoPortStateTLV
     */
    if ((pRmepInfo->u1LastPortStatus != ECFM_PORT_IS_UP) &&
        (pRmepInfo->u1LastPortStatus != ECFM_NO_PORT_STATUS_TLV))

    {
        pRmepInfo->b1RMepPortStatusDefect = ECFM_TRUE;
    }

    else

    {
        pRmepInfo->b1RMepPortStatusDefect = ECFM_FALSE;
    }

    /* Set InterfaceStatus to recvd InterfaceStatus */
    pRmepInfo->u1LastInterfaceStatus = pCcRxInfo->u1RecvdIfStatus;

    /* Set rMEPInterfaceStatusDefect = True if (rMEPlastInterfaceStatus != isUp 
     * && rMEPlastInterfaceStatus!=psNoInterfaceStateTLV
     */
    if ((pRmepInfo->u1LastInterfaceStatus != ECFM_INTERFACE_UP) &&
        (pRmepInfo->u1LastInterfaceStatus != ECFM_IS_NO_INTERFACE_STATUS_TLV))

    {
        pRmepInfo->b1RMepInterfaceStatusDefect = ECFM_TRUE;
    }

    else

    {
        pRmepInfo->b1RMepInterfaceStatusDefect = ECFM_FALSE;
    }

    /* Set SenderId to recvd SenderId */
    /* Sender id consist of Chassis Id */
    if (pSenderId->ChassisId.u4OctLen != 0)

    {

        /* Copy the chassis id */
        if (pRmepInfo->LastSenderId.ChassisId.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_CHASSIS_ID_POOL,
                                 pRmepInfo->LastSenderId.ChassisId.pu1Octets);
            pRmepInfo->LastSenderId.ChassisId.pu1Octets = NULL;
        }

        ECFM_ALLOC_MEM_BLOCK_CC_RMEP_CHASSIS_ID
            (pRmepInfo->LastSenderId.ChassisId.pu1Octets);
        if (pRmepInfo->LastSenderId.ChassisId.pu1Octets == NULL)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmRmepSmSetStateOk:Meme alloc failed FAILED\r\n");
            return ECFM_FAILURE;
        }
        pRmepInfo->LastSenderId.u1ChassisIdSubType =
            pSenderId->u1ChassisIdSubType;
        pRmepInfo->LastSenderId.ChassisId.u4OctLen =
            pSenderId->ChassisId.u4OctLen;
        ECFM_MEMCPY (pRmepInfo->LastSenderId.ChassisId.pu1Octets,
                     pSenderId->ChassisId.pu1Octets,
                     pSenderId->ChassisId.u4OctLen);
    }

    /* Sender id consisit of management address domain */
    if (pSenderId->MgmtAddressDomain.u4OctLen != 0)

    {

        /* Copy the management address domain */
        if (pRmepInfo->LastSenderId.MgmtAddressDomain.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL,
                                 pRmepInfo->LastSenderId.MgmtAddressDomain.
                                 pu1Octets);
            pRmepInfo->LastSenderId.MgmtAddressDomain.pu1Octets = NULL;
        }

        ECFM_ALLOC_MEM_BLOCK_CC_RMEP_MGMT_ADDR_DOMAIN
            (pRmepInfo->LastSenderId.MgmtAddressDomain.pu1Octets);
        if (pRmepInfo->LastSenderId.MgmtAddressDomain.pu1Octets == NULL)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmRmepSmSetStateOk:Meme alloc failed FAILED\r\n");
            return ECFM_FAILURE;
        }
        pRmepInfo->LastSenderId.MgmtAddressDomain.u4OctLen =
            pSenderId->MgmtAddressDomain.u4OctLen;
        ECFM_MEMCPY (pRmepInfo->LastSenderId.MgmtAddressDomain.pu1Octets,
                     pSenderId->MgmtAddressDomain.pu1Octets,
                     pSenderId->MgmtAddressDomain.u4OctLen);
    }

    /* Sender id consisit of management address */
    if (pSenderId->MgmtAddress.u4OctLen != 0)

    {

        /* Copy the management address domain */
        if (pRmepInfo->LastSenderId.MgmtAddress.pu1Octets != NULL)

        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_POOL,
                                 pRmepInfo->LastSenderId.MgmtAddress.pu1Octets);
            pRmepInfo->LastSenderId.MgmtAddress.pu1Octets = NULL;
        }

        ECFM_ALLOC_MEM_BLOCK_CC_RMEP_MGMT_ADDR
            (pRmepInfo->LastSenderId.MgmtAddress.pu1Octets);
        if (pRmepInfo->LastSenderId.MgmtAddress.pu1Octets == NULL)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmRmepSmSetStateOk:Meme alloc failed FAILED\r\n");
            return ECFM_FAILURE;
        }
        ECFM_MEMCPY (pRmepInfo->LastSenderId.MgmtAddress.pu1Octets,
                     pSenderId->MgmtAddress.pu1Octets,
                     pSenderId->MgmtAddress.u4OctLen);
        pRmepInfo->LastSenderId.MgmtAddress.u4OctLen =
            pSenderId->MgmtAddress.u4OctLen;
    }

    /* Call the FNG to calulate someRMEPCCMdefect, someMACstatusDefect and
     * someRDIdefect
     */
    EcfmCcFngCalulateRemoteDefects (pPduSmInfo, ECFM_TRUE);

    EcfmRedCheckAndSyncSmData (&pRmepInfo->u1State,
                               ECFM_RMEP_STATE_OK, pPduSmInfo);

    /*Set Rmepstate to ECFM_RMEP_STATE_OK */
    ECFM_CC_RMEP_SET_STATE (pRmepInfo, ECFM_RMEP_STATE_OK);

    /*Store the current time */
    ECFM_GET_SYS_TIME (&(pRmepInfo->u4FailedOkTime));
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepSmSetStateOk: RMEP SEM Moved to Ok state\r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepSmSetStateFailed
 *
 * Description        : This routine is used to handle the Timeout event and
 *                      generate the fault for non receipt of the CCM within 
 *                      the time period 
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmRmepSmSetStateFailed (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmMepInfoParams  MepInfo;
    UINT2               u2CrossCheckDelay = 0;
    UINT4               u4Interval = 0;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;
    tEcfmCcMaInfo      *pMaInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pRmepInfo = ECFM_CC_GET_RMEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pCcInfo);
    u2CrossCheckDelay = ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;
 
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pRmepInfo->pMepInfo);

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    if ((ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) == ECFM_TRUE) &&
        (pMaInfo->u1CrossCheckStatus == ECFM_ENABLE))
    {
        if (pRmepInfo->b1RMepCcmDefect == ECFM_FALSE)
        {
            tEcfmCcErrLogInfo  *pCcErrLog = NULL;
            pRmepInfo->b1RMepCcmDefect = ECFM_TRUE;

            /*Add Error Entry to Error Log Table */
            pCcErrLog =
                EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_LOC_DFCT_ENTRY);

            /* Check if AIS or LCK condition is set , we need not generate a
             * trap for the error
             */
            if ((ECFM_CC_IS_AIS_CONDITION_TRUE (pMepInfo)) ||
                (ECFM_CC_IS_LCK_CONDITION_TRUE (pMepInfo)))
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                             "EcfmRmepSmSetStateFailed:"
                             " Loc Alarm SUPRESSED!!\r\n ");
            }
            else
            {
                /* Generate the SNMP trap for the fault */
                Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_LOC_TRAP_EN_VAL);
                MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
                MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

                /*Reset the Port Info Ptr */
                pTempPortInfo = NULL;
                MepInfo.u4IfIndex =
                    ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
                MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
                MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
                MepInfo.u2MepId = pMepInfo->u2MepId;
                MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
                MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

                ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                       &MepInfo, NULL,
                                       ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                       ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                       ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                       ECFM_CC_TASK_ID);

                MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                             "EcfmRmepSmSetStateFailed:"
                             " LOC defect occurred!!\r\n ");
            }
        }
    }                            /* In 802.1ag Mode */
    else if (pMaInfo->u1CrossCheckStatus == ECFM_ENABLE)
    {
        if (pRmepInfo->b1RMepCcmDefect == ECFM_FALSE)

        {
            MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
            MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

            /*Reset the Port Info Ptr */
            pTempPortInfo = NULL;
            MepInfo.u4IfIndex =
                ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
            MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
            MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
            MepInfo.u2MepId = pMepInfo->u2MepId;
            MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
            MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif

            ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   &MepInfo, NULL,
                                   ECFM_DEFECT_CONDITION_ENCOUNTERED,
                                   ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                                   ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                                   ECFM_CC_TASK_ID);

            MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
        }
        /* Set rMEPCCMdefect to True */
        pRmepInfo->b1RMepCcmDefect = ECFM_TRUE;

    }
    if (pMepInfo->b1MepCcmOffloadStatus == ECFM_FALSE)
    {

	    ECFM_GET_CCM_INTERVAL (pMepInfo->pMaInfo->u1CcmInterval, u4Interval);

	    if (u2CrossCheckDelay != 0)
	    {
		    u4Interval = u4Interval * u2CrossCheckDelay;
	    }
	    else
	    {
		    u4Interval = u4Interval * ECFM_XCHK_DELAY_DEF_VAL;
	    }

	    if (EcfmCcTmrStartTimer
			    (ECFM_CC_TMR_RMEP_WHILE, pPduSmInfo, u4Interval) != ECFM_SUCCESS)
	    {
		    ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC |
				    ECFM_ALL_FAILURE_TRC,
				    "EcfmRmepSmSetStateFailed: Start Timer FAILED\r\n");
		    return ECFM_FAILURE;
	    }
    }
    pRmepInfo->u1Y1731RemCCMRxCount = 0;

    if (pRmepInfo->b1LastRdi == ECFM_TRUE)

    {
        tEcfmCcErrLogInfo  *pCcErrLog = NULL;

        /*Add Error Exit to Error Log Table */
        pCcErrLog = EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_RDI_CCM_DFCT_EXIT);

        /* Generate the SNMP trap for the fault */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_RDI_CCM_EX_TRAP_VAL);
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex =
            ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
            MepInfo.u4SlaId = pMepInfo->EcfmSlaParams.u4SlaId;
#endif
        ECFM_NOTIFY_PROTOCOLS (ECFM_RDI_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_RDI_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    }

    /* Clear RDI and mac status defects as we are not receiving anyting from the
     * remote MEP*/
    pRmepInfo->b1LastRdi = ECFM_FALSE;
    pRmepInfo->b1RMepPortStatusDefect = ECFM_FALSE;
    pRmepInfo->b1RMepInterfaceStatusDefect = ECFM_FALSE;

    /* When CC is disabled in a Local MEP, remote node will not
     * receive CC packet. So after CC timer expiry in remote node,it
     * should not reset the Remote MEP Mac address in LBLT task */

    if (pMepInfo->pPortInfo->u1IfOperStatus != CFA_IF_UP)
    {
        /* Updated RMEP MAC address at LBLT also */
        if (EcfmLbLtAddRMepDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pRmepInfo) !=
            ECFM_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }
    /* fix for the cross check disable and then enable */
    if (pMaInfo->u1CrossCheckStatus == ECFM_ENABLE)
    {
	    /* Callculate for allRMEPdead also */
	EcfmCcFngCalulateRemoteDefects (pPduSmInfo, ECFM_FALSE);

	EcfmRedCheckAndSyncSmData (&pRmepInfo->u1State,
	    	    ECFM_RMEP_STATE_FAILED, pPduSmInfo);

	/* State set to Failed */
	ECFM_CC_RMEP_SET_STATE (pRmepInfo, ECFM_RMEP_STATE_FAILED);
    }

    /*Store the current time */
    ECFM_GET_SYS_TIME (&(pRmepInfo->u4FailedOkTime));
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmRmepSmSetStateFailed: RMEP SEM Moved to FAILED state\r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmRmepSmEvtImpossible
 *
 * Description        : This rotuine is used to handle the occurence of event
 *                      which canmt be possible in the current state of the
 *                      state machine.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmRmepSmEvtImpossible (tEcfmCcPduSmInfo * pPduSmInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();
    UNUSED_PARAM (pPduSmInfo);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmRmepSmEvtImpossible:"
                 "IMPOSSIBLE EVENT/STATE Combination Occurred in "
                 "Remote MEP SEM \r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmrmpsm.c
 *****************************************************************************/
