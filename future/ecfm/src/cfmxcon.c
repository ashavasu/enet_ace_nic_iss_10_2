/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmxcon.c,v 1.23 2016/07/25 07:25:12 siva Exp $
 *
 * Description: This file contains the Functionality of the Cross 
 *              Connected State Machine.
 *******************************************************************/

#include "cfminc.h"
#include "cfmxcon.h"

/****************************************************************************
 * Function Name      : EcfmCcClntXconSm
 *
 * Description        : This is the XCON State Machine that traverses 
 *                      the state event matrix depending event given to it. 
 *
 * Input(s)           : u1Event - Specifying the event received by this 
 *                                state machine.
 *                      pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding MP info, the PDU if received and 
 *                      other  state machine related  information.
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
EcfmCcClntXconSm (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    ECFM_CC_TRC_ARG2 (ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcClntXconSm: Called with Event: %d, and the current"
                      "State: %d\r\n",
                      u1Event, ECFM_CC_MEP_XCON_GET_STATE (pMepInfo));

    if (ECFM_CC_MEP_XCON_STATE_MACHINE (u1Event, pCcInfo->u1MepXconState,
                                        pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmCcClntXconSm:Xcon State machine for Mep %u "
                          " does not function Correctly\r\n",
                          pMepInfo->u2MepId);
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmXconSmSetStateDefault
 *
 * Description        : This routine is used to handle the occurrence of event
 *                      MepNotActive in which state machine changes its current 
 *                      state to DEFAULT state.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmXconSmSetStateDefault (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    /* if the timer was running then stop the corrosponding timer */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmXconSmSetStateDefault:Unable to stop timer\r\n");
        return ECFM_FAILURE;
    }

    /* XconConnected Variable is set to false */
    pCcInfo->b1XconCcmDefect = ECFM_FALSE;

    /* Simulate the exit log in case the defect was already present */
    if (pCcInfo->b1MisMergeDefect == ECFM_TRUE)
    {
        /* raise mismerge exit trap */
        /* Add Entry for Mismerge defect exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_MISMERGE_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_MISMERGE_EX_TRAP_VAL);
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex =
            ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

        pCcInfo->b1MisMergeDefect = ECFM_FALSE;
    }

    if (pCcInfo->b1UnExpectedLevelDefect == ECFM_TRUE)
    {
        /* raise unexpected level exit trap */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_MEG_LEVEL_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_LEVEL_EX_TRAP_VAL);
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex =
            ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

        pCcInfo->b1UnExpectedLevelDefect = ECFM_FALSE;
    }
    /* Check if pu1XconCcmLastFailure already has frame then free it */
    if (pCcInfo->XconCcmLastFailure.pu1Octets != NULL)
    {
        /* Free the variable */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_XCON_CCM_POOL,
                             pCcInfo->XconCcmLastFailure.pu1Octets);
        pCcInfo->XconCcmLastFailure.pu1Octets = NULL;
        pCcInfo->XconCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->CcInfo.u1MepXconState,
                               ECFM_MEP_XCON_STATE_DEFAULT, pPduSmInfo);

    /*State set to Default */
    ECFM_CC_MEP_XCON_SET_STATE (pMepInfo, ECFM_MEP_XCON_STATE_DEFAULT);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmXconSmSetStateDefault:XCON SEM Moved to Default "
                 "state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/****************************************************************************
 * Function Name      : EcfmXconSmSetStateNoDefect
 *
 * Description        : This routine set the state of SM to NO_DEFECT
 *                      state from the transient idle on receipt of Begin
 *                      event or from the Defect state on expiry of the 
 *                      timer indicating that no defected CCM has been
 *                      received.Bool variable for XconDefect is reset 
 *                      and PDU for last Xcon defect is freed.
 * 
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmXconSmSetStateNoDefect (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmMepInfoParams  MepInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcErrLogInfo  *pCcErrLog = NULL;
    tEcfmCcPortInfo    *pTempPortInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    /* Stop the xcon ccm while timer if running */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmXconSmSetStateNoDefect:Stop Timer FAILED\r\n");
        return ECFM_FAILURE;
    }

    /*Mismerge Exit Trap is raised and Error Log Entry is done when 
     * Y.1731 is enabled */
    if (pCcInfo->b1MisMergeDefect == ECFM_TRUE)
    {
        /*Add Entry for Mismerge defect exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_MISMERGE_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_MISMERGE_EX_TRAP_VAL);
        /* Mismerge Variable is set to false */
        pCcInfo->b1MisMergeDefect = ECFM_FALSE;
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex =
            ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    }

    /*Unexpected Level Exit Trap is raised and Error Log Entry is done when 
     * Y.1731 is enabled */
    if (pCcInfo->b1UnExpectedLevelDefect == ECFM_TRUE)
    {
        /*Add Entry for Unexpected Mep Level defect exit to Error Log Table */
        pCcErrLog =
            EcfmCcAddErrorLogEntry (pPduSmInfo, ECFM_UNEXP_MEG_LEVEL_DFCT_EXIT);
        /* Generate the SNMP trap for the exit condition */
        Y1731_CC_GENERATE_TRAP (pCcErrLog, Y1731_UNEXP_LEVEL_EX_TRAP_VAL);
        /* UnExpectedLevelDefect Variable is set to false */
        pCcInfo->b1UnExpectedLevelDefect = ECFM_FALSE;
        MepInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        MepInfo.u4VlanIdIsid = pMepInfo->pMaInfo->u4PrimaryVidIsid;

        /*Reset the Port Info Ptr */
        pTempPortInfo = NULL;
        MepInfo.u4IfIndex =
            ECFM_CC_GET_PHY_PORT (pMepInfo->u2PortNum, pTempPortInfo);
        MepInfo.u4MdIndex = pMepInfo->u4MdIndex;
        MepInfo.u4MaIndex = pMepInfo->u4MaIndex;
        MepInfo.u2MepId = pMepInfo->u2MepId;
        MepInfo.u1MdLevel = pMepInfo->u1MdLevel;
        MepInfo.u1Direction = pMepInfo->u1Direction;

        ECFM_NOTIFY_PROTOCOLS (ECFM_DEFECT_CONDITION_CLEARED,
                               &MepInfo, NULL, ECFM_DEFECT_CONDITION_CLEARED,
                               ECFM_INIT_VAL, ECFM_INIT_VAL, NULL,
                               ECFM_INIT_VAL, NULL, ECFM_INIT_VAL,
                               ECFM_CC_TASK_ID);

        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));
    }

    if (pCcErrLog != NULL)
    {
        pCcErrLog->u2RmepId = pCcInfo->u2XconnRMepId;
    }
    if (pCcInfo->b1XconCcmDefect == ECFM_TRUE)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmXconSmSetStateNoDefect: xConCcmDefect Cleared \r\n");
        /* XconConnected Variable is set to false */
        pCcInfo->b1XconCcmDefect = ECFM_FALSE;
        pCcInfo->u2XconnRMepId = ECFM_INIT_VAL;
        /* Send !MAdefectIndication to FNG */
        EcfmCcFngNoMaDefectIndication (pPduSmInfo);
    }
    /* Check if XconCcmLastFailure already has frame then free it */
    if (pCcInfo->XconCcmLastFailure.pu1Octets != NULL)
    {
        /* Free the variable */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_XCON_CCM_POOL,
                             pCcInfo->XconCcmLastFailure.pu1Octets);
        pCcInfo->XconCcmLastFailure.pu1Octets = NULL;
        pCcInfo->XconCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }

    EcfmRedCheckAndSyncSmData (&pMepInfo->CcInfo.u1MepXconState,
                               ECFM_MEP_XCON_STATE_NO_DEFECT, pPduSmInfo);

    /* Set state to NO_DEFFECT */
    ECFM_CC_MEP_XCON_SET_STATE (pMepInfo, ECFM_MEP_XCON_STATE_NO_DEFECT);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmXconSmSetStateNoDefect: Xcon SEM Moved to No Defect"
                 "State\r\n");

    /* Required in case of OFFLOADING CCM */
    /* Check if RDI is to be set/reset in Offloaded module */
    if (FsEcfmOffloadSetRDI (pMepInfo) == ECFM_FAILURE)
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmXconSmSetStateDefect
 *
 * Description        : This routine is called when Cross Connect CCM is
 *                      received.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmXconSmSetStateDefect (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    UINT1               u1ByteCount = ECFM_INIT_VAL;
    UINT1               u1RecvdIntervalCode = ECFM_INIT_VAL;
    UINT4               u4CcmInterval = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT2               u2CrossCheckDelay = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    /* get the received CCI Interval */
    u1RecvdIntervalCode = ECFM_CC_GET_CCM_INTERVAL (pPduSmInfo->u1RxFlags);
    ECFM_GET_CCM_INTERVAL (u1RecvdIntervalCode, u4CcmInterval);
    u2CrossCheckDelay = ECFM_CC_CURR_CONTEXT_INFO ()->u2CrossCheckDelay;

    if (u2CrossCheckDelay != 0)
    {
        u4CcmInterval = u4CcmInterval * u2CrossCheckDelay;
    }
    else
    {
        u4CcmInterval = u4CcmInterval * ECFM_XCHK_DELAY_DEF_VAL;
    }

    /* Get the remaing Time if timer is already running */
    ECFM_GET_REMAINING_TIME (ECFM_CC_TMRLIST_ID, &(pCcInfo->
                                                   XconCCMWhileTimer.
                                                   TimerNode),
                             &u4RemainingTime);

    if (u4RemainingTime != 0)
    {
        /*Convert Time Ticks into Msec */
        u4RemainingTime = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4RemainingTime);
        /* if remaing time is greater than the 3.5 * CciInterval received then
         * start the timer with the remaining time else start timer with the
         * received interval */
        if (u4RemainingTime < u4CcmInterval)
        {
            /* Stop the running Xcon CCM While timer */
            if (EcfmCcTmrStopTimer (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmXconSmSetStateDefect:Stop Timer FAILED\r\n");
                return ECFM_FAILURE;

            }                    /* stop timer */
            /* start the timer */
            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo,
                                     u4CcmInterval) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmXconSmSetStateDefect:Start Timer  FAILED\r\n");
                return ECFM_FAILURE;
            }
        }
        else
        {
            /* Stop the running Xcon CCM While Timer */
            if (EcfmCcTmrStopTimer (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo)
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmXconSmSetStateDefect:Stop Timer FAILED\r\n");
                return ECFM_FAILURE;
            }
            /* Start the Xcon CCM while timer with the remaining time */
            if (EcfmCcTmrStartTimer (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo,
                                     u4RemainingTime) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmXconSmSetStateDefect:Start Timer  FAILED\r\n");
                return ECFM_FAILURE;
            }
        }
    }                            /* check if timer can be stopped or not */
    else
    {

        /* start the timer */
        if (EcfmCcTmrStartTimer
            (ECFM_CC_TMR_XCON_CCM_WHILE, pPduSmInfo,
             u4CcmInterval) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmXconSmSetStateDefect:Start Timer FAILED\r\n");
            return ECFM_FAILURE;
        }
    }

    /* Copy the received Cross connected frame in CC Info */
    u1ByteCount = (UINT1) ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pBuf);
    if (pCcInfo->XconCcmLastFailure.pu1Octets != NULL)
    {
        /* Free the variable */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_XCON_CCM_POOL,
                             pCcInfo->XconCcmLastFailure.pu1Octets);
        pCcInfo->XconCcmLastFailure.pu1Octets = NULL;
        pCcInfo->XconCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }
    /* Allocate the memory to store the XCON CCM */
    ECFM_ALLOC_MEM_BLOCK_CC_MEP_XCON_CCM
        (pCcInfo->XconCcmLastFailure.pu1Octets);
    if (pCcInfo->XconCcmLastFailure.pu1Octets == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmXconSmSetStateDefect:Meme alloc failed FAILED\r\n");
        return ECFM_FAILURE;
    }

    ECFM_COPY_FROM_CRU_BUF (pPduSmInfo->pBuf,
                            pCcInfo->XconCcmLastFailure.pu1Octets,
                            ECFM_INIT_VAL, (UINT4) (u1ByteCount));
    /* Update length of the Cross Connect CCM PDU received */
    pCcInfo->XconCcmLastFailure.u4OctLen = u1ByteCount;

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmXconSmSetStateDefect: xConCcmDefect Occurred \r\n");
    /* Generate the SNMP trap for the fault */
    ECFM_CC_GENERATE_TRAP (pPduSmInfo, ECFM_XCONN_CCM_TRAP_VAL);
    /* Generate the fault fo the Cross connected CCM */
    pCcInfo->b1XconCcmDefect = ECFM_TRUE;
    /* Store the MEP-ID which caused the Cross Connect Error */
    pCcInfo->u2XconnRMepId = pPduSmInfo->uPduInfo.Ccm.u2MepId;
    /* Call Ma defect indiaction that generate the fault depending upon the
     * priority */
    EcfmCcFngMaDefectIndication (pPduSmInfo, ECFM_DEF_XCON_CCM);
    /* Increment the defect counter */
    ECFM_CC_INCR_XCONN_DEFECT_COUNT ();

    EcfmRedCheckAndSyncSmData (&pMepInfo->CcInfo.u1MepXconState,
                               ECFM_MEP_XCON_STATE_DEFECT, pPduSmInfo);

    /*Set state to Defect state */
    ECFM_CC_MEP_XCON_SET_STATE (pMepInfo, ECFM_MEP_XCON_STATE_DEFECT);

    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                 "EcfmXconSmSetStateDefect: XCON SEM Moved to Defect state\r\n");

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;

}

/****************************************************************************
 * Function Name      : EcfmXconSmSetStateDefFrmDef
 *
 * Description        : Set state to DEFECT, Timer is started for keeping track
 *                      for the next invalid CCM and defect is generated after 
 *                      copying the wrong CCM received in the MIB and
 *                      setting the BOOL variable for error CCM.
 *
 * Input(s)           :  pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmXconSmSetStateDefFrmDef (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    pMepInfo = ECFM_CC_GET_MEP_FROM_PDUSM (pPduSmInfo);
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    if (pCcInfo->XconCcmLastFailure.pu1Octets != NULL)
    {
    /* Free the variable */
    ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_XCON_CCM_POOL,
                         pCcInfo->XconCcmLastFailure.pu1Octets);
    pCcInfo->XconCcmLastFailure.pu1Octets = NULL;
    pCcInfo->XconCcmLastFailure.u4OctLen = ECFM_INIT_VAL;

    }
    if (EcfmXconSmSetStateDefect (pPduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmXconSmSetStateDefFrmDef:Fault geneartion failed\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmXconSmEvtImpossible
 *
 * Description        : This rotuine is used to handle the occurence of event
 *                      which canmt be possible in the current state of the
 *                      state machine.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores 
 *                      the information regarding MP info, the PDU if received 
 *                      and other  state machine related  information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ ECFM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmXconSmEvtImpossible (tEcfmCcPduSmInfo * pPduSmInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

    UNUSED_PARAM (pPduSmInfo);
    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmXconSmEvtImpossible:"
                 "IMPOSSIBLE EVENT/STATE Combination Occurred in "
                 "XCON SEM \r\n");
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/****************************************************************************
  End of File cfmxcon.c
 *****************************************************************************/
