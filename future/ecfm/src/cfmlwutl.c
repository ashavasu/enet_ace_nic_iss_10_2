/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlwutl.c,v 1.78 2017/02/09 13:51:25 siva Exp $
 *
 * Description: This file contains the Utility Procedures used by
 *              SNMP Low level routines.
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmCreateRMepEntryInMep PROTO ((tEcfmCcMepInfo *, UINT2));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetVlanEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Vlan entry
 *                       from the global structure, VlanTable.
 *
 *    INPUT            : u2VlanId - Index of Vlan Table for which the entry is
 *                                  required.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Vlan Entry`
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcVlanInfo *
EcfmSnmpLwGetVlanEntry (UINT4 u4VlanIdIsid)
{
    tEcfmCcVlanInfo     VlanInfo;
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    ECFM_MEMSET (&VlanInfo, ECFM_INIT_VAL, ECFM_CC_VLAN_INFO_SIZE);

    /* Get VLAN entry, based on u2VlanId */
    VlanInfo.u4VidIsid = u4VlanIdIsid;
    pVlanNode = RBTreeGet (ECFM_CC_VLAN_TABLE, (tRBElem *) & VlanInfo);
    return pVlanNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetStackEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Stack entry
 *                       from the global structure, StackTable.
 *
 *    INPUT            : u2PortNum - Local Port Number 
 *                       u2VlanId -  VlanId
 *                       u1MdLevel - MdLevel
 *                       u1Direction - Stack Direction
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Stack Entry
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcStackInfo *
EcfmSnmpLwGetStackEntry (UINT4 u4PortNum, UINT4 u4VlanIdIsid, UINT1 u1MdLevel,
                         UINT1 u1Direction)
{
    tEcfmCcStackInfo    StackInfo;
    tEcfmCcStackInfo   *pStackNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    UINT4               u4IfIndex = 0;
    ECFM_MEMSET (&StackInfo, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);

    pPortInfo = ECFM_CC_GET_PORT_INFO (u4PortNum);
    if (pPortInfo == NULL)
    {
        return NULL;
    }

    u4IfIndex = pPortInfo->u4IfIndex;

    /*Get Stack entry corresponding to IfIndex, VlanId, MdLevel, Direction */
    StackInfo.u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    StackInfo.u4IfIndex = u4IfIndex;
    StackInfo.u4VlanIdIsid = u4VlanIdIsid;
    StackInfo.u1MdLevel = u1MdLevel;
    StackInfo.u1Direction = u1Direction;
    pStackNode =
        RBTreeGet (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) & StackInfo);
    return pStackNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetY1731MplsTpRMepCfgEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the RMep entry
 *                       from the global structure, RMepDb Table, which is 
 *                       associated with the Y1731 MPLSTP based Service Types: 
 *                       (LSP/PW)
 *
 *    INPUT            : u4MegIndex - Maintenance Entity Group index
 *                       u4MeIndex - Maintenance Entity index
 *                       u2MepId   - Local Mep Identifier
 *                       u2RMepId  - Remote Mep Identifier
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to RMep Entry associated with LSP/PW based 
 *                       Service.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcRMepDbInfo *
EcfmSnmpLwGetY1731MplsTpRMepCfgEntry (UINT4 u4MegIndex, UINT4 u4MeIndex,
                                      UINT2 u2MepId, UINT2 u2RMepId)
{
    tEcfmCcRMepDbInfo  *pCcRMepEntry = NULL;
    tEcfmCcMaInfo      *pMeNode = NULL;

    pCcRMepEntry =
        EcfmSnmpLwGetRMepEntry (u4MegIndex, u4MeIndex, u2MepId, u2RMepId);

    if (pCcRMepEntry != NULL)
    {
        pMeNode = pCcRMepEntry->pMepInfo->pMaInfo;

        if ((pMeNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
            (pMeNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
        {
            /* If the RowStatus is zero, then the RMep entry is not configured
             * yet and it a auto-created entry on creation of Local MEP.
             */
            if (pCcRMepEntry->u1RowStatus != ECFM_INIT_VAL)
            {
                return pCcRMepEntry;
            }
        }
    }
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetY1731MplsTpRMepAutoEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the auto created
 *                       RMep entry (on configuration of Local Mep) from the 
 *                       global structure, RMepDb Table.
 *
 *    INPUT            : u4MegIndex - Maintenance Entity Group index
 *                       u4MeIndex - Maintenance Entity index
 *                       u2MepId   - Local Mep Identifier
 *                       u2RMepId  - Remote Mep Identifier
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to RMep Entry associated with LSP/PW based 
 *                       Service.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcRMepDbInfo *
EcfmSnmpLwGetY1731MplsTpRMepAutoEntry (UINT4 u4MegIndex, UINT4 u4MeIndex,
                                       UINT2 u2MepId, UINT2 u2RMepId)
{
    tEcfmCcRMepDbInfo  *pCcRMepEntry = NULL;
    tEcfmCcMaInfo      *pMeNode = NULL;

    pCcRMepEntry =
        EcfmSnmpLwGetRMepEntry (u4MegIndex, u4MeIndex, u2MepId, u2RMepId);

    if (pCcRMepEntry != NULL)
    {
        pMeNode = pCcRMepEntry->pMepInfo->pMaInfo;

        if ((pMeNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
            (pMeNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
        {
            return pCcRMepEntry;
        }
    }
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetConfigErrEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Config error
 *                       entry from the global structure, ConfigErrListTable.
 *
 *    INPUT            :  u4VlanIdIsid -  VlanId
 *                        u2PortNum - Local Port Number 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : Pointer to Config Error List entry.
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcConfigErrInfo *
EcfmSnmpLwGetConfigErrEntry (UINT4 u4VlanIdIsid, UINT2 u2PortNum)
{
    tEcfmCcConfigErrInfo ConfigErrInfo;
    tEcfmCcConfigErrInfo *pConfigErrNode = NULL;
    ECFM_MEMSET (&ConfigErrInfo, ECFM_INIT_VAL, ECFM_CC_CONFIG_ERR_INFO_SIZE);

    /*Get config error list entry corresponding to IfIndex, VlanId */
    if ((u2PortNum != ECFM_VAL_0) && (u2PortNum < ECFM_CC_MAX_PORT_INFO))
    {
        ConfigErrInfo.u4IfIndex = ECFM_CC_PORT_INFO (u2PortNum)->u4IfIndex;
    }

    ConfigErrInfo.u4VidIsid = u4VlanIdIsid;
    pConfigErrNode =
        RBTreeGet (ECFM_CC_GLOBAL_CONFIG_ERR_TABLE,
                   (tRBElem *) & ConfigErrInfo);
    return pConfigErrNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetMdEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Md entry
 *                       corresponding to MdIndex from the global structure, 
 *                       MdTableIndex.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index 
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Md Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcMdInfo *
EcfmSnmpLwGetMdEntry (UINT4 u4MdIndex)
{
    tEcfmCcMdInfo       MdInfo;
    tEcfmCcMdInfo      *pMdNode = NULL;
    ECFM_MEMSET (&MdInfo, ECFM_INIT_VAL, ECFM_CC_MD_INFO_SIZE);

    /* Get MD entry corresponding to MdIndex */
    MdInfo.u4MdIndex = u4MdIndex;
    pMdNode = RBTreeGet (ECFM_CC_MD_TABLE, (tRBElem *) & MdInfo);
    return pMdNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetDefMdEntry
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Def Md entry
 *                       corresponding to u4DefMdIndex from the global structure, 
 *                       DefMdTableIndex.
 *
 *    INPUT            : u4DefMdIndex - Default Maintenance Domain Index 
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Default Md Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcDefaultMdTableInfo *
EcfmSnmpLwGetDefMdEntry (UINT4 u4DefMdIndex)
{
    tEcfmCcDefaultMdTableInfo DefMdInfo;
    tEcfmCcDefaultMdTableInfo *pDefMdNode = NULL;

    ECFM_MEMSET (&DefMdInfo, ECFM_INIT_VAL, ECFM_CC_DEF_MD_INFO_SIZE);

    /* Get MD entry corresponding to MdIndex */
    DefMdInfo.u4PrimaryVidIsid = u4DefMdIndex;
    pDefMdNode = RBTreeGet (ECFM_CC_DEF_MD_TABLE, (tRBElem *) & DefMdInfo);

    return pDefMdNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwIsInfoConfiguredForMd 
 *                                                                          
 *    DESCRIPTION      : This function Checks for the uniqueness of MD name
 *                       with in all MDs configured on this bridge.
 *
 *    INPUT            : pMdNode, pointer to Md node
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwIsInfoConfiguredForMd (tEcfmCcMdInfo * pMdNode)
{
    tEcfmCcMdInfo      *pMdTempNode = NULL;
    UINT4               u4ErrorCode = ECFM_INIT_VAL;

    /* Check if any active MD node exists with pMdNode's name */
    pMdTempNode = (tEcfmCcMdInfo *) RBTreeGetFirst (ECFM_CC_MD_TABLE);
    while (pMdTempNode != NULL)
    {
        if (pMdTempNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
        {
            if (pMdTempNode->u1NameFormat != ECFM_DOMAIN_NAME_TYPE_NONE)
            {
                if ((pMdTempNode != pMdNode) &&
                    (ECFM_MEMCMP (pMdTempNode->au1Name,
                                  pMdNode->au1Name, ECFM_MD_NAME_MAX_LEN) == 0))
                {
                    return ECFM_FAILURE;
                }
            }
        }
        pMdTempNode = (tEcfmCcMdInfo *) RBTreeGetNext (ECFM_CC_MD_TABLE,
                                                       (tRBElem *) pMdTempNode,
                                                       NULL);
    }

    /* Check whether Md name is corresponding to its format */
    if ((EcfmValidateMdNameFormat
         (pMdNode->au1Name, pMdNode->u1NameLength, pMdNode->u1NameFormat,
          &u4ErrorCode)) != ECFM_SUCCESS)

    {
        CLI_SET_ERR (CLI_ECFM_MD_NAME_FORMAT_ERR);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwGetMaEntry
 *
 *    DESCRIPTION      : This function returns the pointer to the MA entry
 *                       corresponding to MdIndex, MaIndex from the global 
 *                       structure, MaTableIndex.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index, 
 *                       u4MaIndex - Maintenance Association Index
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to Ma Entry
 *
 ****************************************************************************/
PUBLIC tEcfmCcMaInfo *
EcfmSnmpLwGetMaEntry (UINT4 u4MdIndex, UINT4 u4MaIndex)
{
    tEcfmCcMaInfo       MaInfo;
    tEcfmCcMaInfo      *pMaNode = NULL;
    ECFM_MEMSET (&MaInfo, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);

    /* Get MA entry corresponding to indices - MdIndex and MaIndex */
    MaInfo.u4MdIndex = u4MdIndex;
    MaInfo.u4MaIndex = u4MaIndex;
    pMaNode = (tEcfmCcMaInfo *) RBTreeGet (ECFM_CC_MA_TABLE,
                                           (tRBElem *) & MaInfo);
    return pMaNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetRMepEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the RMep entry
 *                       from the global structure, RMepDb Table.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u2RMepId  - Remote Mep Identifier
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to RMep Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcRMepDbInfo *
EcfmSnmpLwGetRMepEntry (UINT4 u4MdIndex,
                        UINT4 u4MaIndex, UINT2 u2MepId, UINT2 u2RMepId)
{
    tEcfmCcRMepDbInfo   RMepInfo;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);

    /* Get Remote MEP entry corresponding to indices - MdIndex, 
     * MaIndex, MepId and RMepId*/
    RMepInfo.u4MdIndex = u4MdIndex;
    RMepInfo.u4MaIndex = u4MaIndex;
    RMepInfo.u2MepId = u2MepId;
    RMepInfo.u2RMepId = u2RMepId;
    pRMepNode = RBTreeGet (ECFM_CC_RMEP_TABLE, (tRBElem *) & RMepInfo);
    return pRMepNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetMaMepListEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the MepList entry
 *                       from the global structure, MepListTable.
 *
 *    INPUT            : u4MdIndex - Maintenance domain index
 *                       u4MaIndex - Maintenance association index
 *                       u2MepId   - Mep Identifier
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to MepList Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcMepListInfo *
EcfmSnmpLwGetMaMepListEntry (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmCcMepListInfo  MepListInfo;
    tEcfmCcMepListInfo *pMepListNode = NULL;
    ECFM_MEMSET (&MepListInfo, ECFM_INIT_VAL, ECFM_CC_MEP_LIST_SIZE);

    /*Get MEPList entry corresponding to indices, MdIndex, MaIndex and MepId */
    MepListInfo.u4MdIndex = u4MdIndex;
    MepListInfo.u4MaIndex = u4MaIndex;
    MepListInfo.u2MepId = u2MepId;
    pMepListNode = RBTreeGet (ECFM_CC_MA_MEP_LIST_TABLE, (tRBElem *)
                              & MepListInfo);
    return pMepListNode;
}

/* Frame Loss Buffer*/
/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwGetFrmLossEntry
 *
 *    DESCRIPTION      : This function returns the pointer to the Fl entry
 *                       from the global structure, FlTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to Lbm Entry
 *
 *
 ****************************************************************************/
PUBLIC tEcfmCcFrmLossBuff *
EcfmSnmpLwGetFrmLossEntry (UINT4 u4MdIndex,
                           UINT4 u4MaIndex,
                           UINT2 u2MepId, UINT4 u4TransId, UINT4 u4FrmCount)
{
    tEcfmCcFrmLossBuff  FrmLossBuffNode;
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    ECFM_MEMSET (&FrmLossBuffNode, ECFM_INIT_VAL, ECFM_CC_FRM_LOSS_INFO_SIZE);

    /* Get FD entry corresponding to indices -
     * MegIndex, MeIndex, MepId, u4TransId, u4SeqNumber */
    FrmLossBuffNode.u4MdIndex = u4MdIndex;
    FrmLossBuffNode.u4MaIndex = u4MaIndex;
    FrmLossBuffNode.u2MepId = u2MepId;
    FrmLossBuffNode.u4TransId = u4TransId;
    FrmLossBuffNode.u4SeqNum = u4FrmCount;
    pFrmLossBuffNode =
        RBTreeGet (ECFM_CC_FL_BUFFER_TABLE, (tRBElem *) & FrmLossBuffNode);
    return pFrmLossBuffNode;
}

/* Frame Loss Stats */
/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwGetFrmLossTransEntry
 *
 *    DESCRIPTION      : This function returns the pointer to the FL Buffer
 *                       entry corresponding to MdIndex, MaIndex, MepId,
 *                       FlTransId from the global structure,
 *                       FrmLossBuffer.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index
 *                       u4MaIndex - Maintenance Association Index
 *                       u2MepId - Mep Identifier
 *                       u4FlTransId - Fl Transaction identifier
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : pointer to a Fl Buffer Table Entry
 *
 ****************************************************************************/
PUBLIC tEcfmCcFrmLossBuff *
EcfmSnmpLwGetFrmLossTransEntry (UINT4 u4MdIndex, UINT4 u4MaIndex,
                                UINT2 u2MepId, UINT4 u4FlTransId)
{
    tEcfmCcFrmLossBuff *pFlBufferNode = NULL;
    pFlBufferNode = (tEcfmCcFrmLossBuff *)
        RBTreeGetFirst (ECFM_CC_FL_BUFFER_TABLE);
    while (pFlBufferNode != NULL)

    {

        /* Get entry corresponding to MdIndex, MaIndex, MepId, FdTransId */
        if ((pFlBufferNode->u4MdIndex == u4MdIndex) &&
            (pFlBufferNode->u4MaIndex == u4MaIndex) &&
            (pFlBufferNode->u2MepId == u2MepId) &&
            (pFlBufferNode->u4TransId == u4FlTransId))

        {
            return pFlBufferNode;
        }
        pFlBufferNode = RBTreeGetNext (ECFM_CC_FL_BUFFER_TABLE,
                                       (tRBElem *) pFlBufferNode, NULL);
    }
    return NULL;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwGetNextIndexFlStatsTbl
 *
 *    DESCRIPTION      : This function returns the next frame Loss stats
 *                       indices.
 *
 *    INPUT            : u4MdIndex - Md Index (MEG Index)
 *                       u4MaIndex - Ma Index (ME Index)
 *                       u4MepIdentifier - MepIdentifier
 *                       u4FlTransId - Transaction Identifier
 *
 *
 *    OUTPUT           : pu4NextMdIndex - Next Md Index
 *                       pu4NextMaIndex - Next Ma Index
 *                       pu4NextMepIdentifier - Next Mep Identifier
 *                       pu4NextFlTransId - Next Transaction Identifier
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwGetNextIndexFlStatsTbl (UINT4 u4MdIndex, UINT4 *pu4NextMdIndex,
                                  UINT4 u4MaIndex, UINT4 *pu4NextMaIndex,
                                  UINT4 u4MepIdentifier,
                                  UINT4 *pu4NextMepIdentifier,
                                  UINT4 u4FlTransId, UINT4 *pu4NextFlTransId)
{
    tEcfmCcFrmLossBuff *pFrmLossTempNode = NULL;
    pFrmLossTempNode = (tEcfmCcFrmLossBuff *)
        RBTreeGetFirst (ECFM_CC_FL_BUFFER_TABLE);

    /* Traverse the FL table for the next transaction indices */
    while (pFrmLossTempNode != NULL)

    {
        if ((pFrmLossTempNode->u4MdIndex > u4MdIndex) ||
            (pFrmLossTempNode->u4MaIndex > u4MaIndex) ||
            (pFrmLossTempNode->u2MepId > (UINT2) u4MepIdentifier))

        {

            /* Required node */
            *pu4NextMdIndex = pFrmLossTempNode->u4MdIndex;
            *pu4NextMaIndex = pFrmLossTempNode->u4MaIndex;
            *pu4NextMepIdentifier = (UINT4) (pFrmLossTempNode->u2MepId);
            *pu4NextFlTransId = pFrmLossTempNode->u4TransId;
            return ECFM_SUCCESS;
        }
        if ((pFrmLossTempNode->u4MdIndex >= u4MdIndex) &&
            (pFrmLossTempNode->u4MaIndex >= u4MaIndex) &&
            (pFrmLossTempNode->u2MepId >= (UINT2) u4MepIdentifier) &&
            (pFrmLossTempNode->u4TransId > u4FlTransId))

        {

            /* Required node */
            *pu4NextMdIndex = pFrmLossTempNode->u4MdIndex;
            *pu4NextMaIndex = pFrmLossTempNode->u4MaIndex;
            *pu4NextMepIdentifier = (UINT4) (pFrmLossTempNode->u2MepId);
            *pu4NextFlTransId = pFrmLossTempNode->u4TransId;
            return ECFM_SUCCESS;
        }

        /* Skip the same transaction and differet sequence no node */
        if ((pFrmLossTempNode->u4MdIndex == u4MdIndex) &&
            (pFrmLossTempNode->u4MaIndex == u4MaIndex) &&
            (pFrmLossTempNode->u2MepId == (UINT2) u4MepIdentifier) &&
            (pFrmLossTempNode->u4TransId == u4FlTransId))

        {

            /* Move to next node */
            pFrmLossTempNode = (tEcfmCcFrmLossBuff *) RBTreeGetNext
                (ECFM_CC_FL_BUFFER_TABLE, (tRBElem *) pFrmLossTempNode, NULL);
            continue;
        }

        /* Skip the node having TransId less than the required one */
        if ((pFrmLossTempNode->u4MdIndex >= u4MdIndex) &&
            (pFrmLossTempNode->u4MaIndex >= u4MaIndex) &&
            (pFrmLossTempNode->u2MepId >= (UINT2) u4MepIdentifier) &&
            (pFrmLossTempNode->u4TransId <= u4FlTransId))

        {

            /* Move to next node */
            pFrmLossTempNode = (tEcfmCcFrmLossBuff *) RBTreeGetNext
                (ECFM_CC_FL_BUFFER_TABLE, (tRBElem *) pFrmLossTempNode, NULL);
            continue;
        }

        /* Move to next node */
        pFrmLossTempNode = (tEcfmCcFrmLossBuff *) RBTreeGetNext
            (ECFM_CC_FL_BUFFER_TABLE, (tRBElem *) pFrmLossTempNode, NULL);
    } return ECFM_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwGetFirstIndexFlStatsTbl
 *
 *    DESCRIPTION      : This function returns the first Frame Loss Stats
 *                       Entry indices.
 *
 *    INPUT            : None
 *
 *
 *    OUTPUT           : pu4MdIndex - Firstt Md Index
 *                       pu4MaIndex - First Ma Index
 *                       pu4MepIdentifier - First Mep Identifier
 *                       pu4FlTransId - First Transaction Identifier
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwGetFirstIndexFlStatsTbl (UINT4 *pu4MdIndex, UINT4 *pu4MaIndex,
                                   UINT4 *pu4MepIdentifier, UINT4 *pu4FlTransId)
{
    return (EcfmSnmpLwGetNextIndexFlStatsTbl
            (0, pu4MdIndex, 0, pu4MaIndex, 0, pu4MepIdentifier, 0,
             pu4FlTransId));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwGetLmTransactionStats
 *
 *    DESCRIPTION      : This function calculates and returns the Fl transaction
 *                       statistics.
 *
 *    INPUT            : pFlBufferFirstNode - Frame Delay buffer node for the
 *                       same Transaction
 *
 *    OUTPUT           : pu4FlStatsMessagesIn - Total number of Fl Frames recieved 
 *                                              for this transaction
 *                       pu4FlStatsFarEndLossAverage - Average of a calculated Delay
 *                       pu4FlStatsNearEndLossAverage- Average of a calculated FDV 
 *                       (Delay Variation)
 *                       pu4FlStatsNearEndLossMin - Min Mear-end Loss value in
 *                       a transaction
 *                       pu4FlStatsNearEndLossMax - Max Mear-end Loss value in
 *                       a transaction
 *                       pu4FlStatsFarEndLossMin  - Min Far-end Loss value in a
 *                       transaction
 *                       pu4FlStatsFarEndLossMax  - Max Far-end Loss value in a
 *                                          transaction                      
 *
 *    RETURNS          : Pointer to Frame Delay buffer node for the
 *                       same Transaction.
 *
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwGetLmTransactionStats (tEcfmCcFrmLossBuff * pFlBufferFirstNode,
                                 UINT4 *pu4FlStatsMessagesIn,
                                 UINT4 *pu4FlStatsFarEndLossAverage,
                                 UINT4 *pu4FlStatsNearEndLossAverage,
                                 UINT4 *pu4FlStatsNearEndLossMin,
                                 UINT4 *pu4FlStatsNearEndLossMax,
                                 UINT4 *pu4FlStatsFarEndLossMin,
                                 UINT4 *pu4FlStatsFarEndLossMax,
                                 UINT4 *pu4FlStatsFarEndLossTotal,
                                 UINT4 *pu4FlStatsNearEndLossTotal)
{
    UINT4               u4FlStatsMessagesIn = ECFM_INIT_VAL;
    UINT4               u4FlStatsFarEndLossTotal = ECFM_INIT_VAL;
    UINT4               u4FlStatsNearEndLossTotal = ECFM_INIT_VAL;
    UINT4               u4FlStatsFarEndLossAverage = ECFM_INIT_VAL;
    UINT4               u4FlStatsNearEndLossAverage = ECFM_INIT_VAL;
    UINT4               u4NearEndLossMin = ECFM_INIT_VAL;
    UINT4               u4NearEndLossMax = ECFM_INIT_VAL;
    UINT4               u4FarEndLossMin = ECFM_INIT_VAL;
    UINT4               u4FarEndLossMax = ECFM_INIT_VAL;
    UINT4               u4Count = ECFM_INIT_VAL;
    tEcfmCcFrmLossBuff *pFrmLossBuffTempNode = NULL;
    pFrmLossBuffTempNode = pFlBufferFirstNode;
    while (pFrmLossBuffTempNode != NULL)

    {

        /* Count the Lm out  nodes for the same transaction */
        u4FlStatsMessagesIn = u4FlStatsMessagesIn + ECFM_INCR_VAL;

        /*Get the FlStats Far End Loss & Near End Loss for a transacation */
        u4FlStatsFarEndLossTotal =
            u4FlStatsFarEndLossTotal + pFrmLossBuffTempNode->u4FarEndLoss;
        u4FlStatsNearEndLossTotal =
            u4FlStatsNearEndLossTotal + pFrmLossBuffTempNode->u4NearEndLoss;

        /* Get the minimum and maximum near and far end loss values */
        u4Count = u4Count + ECFM_INCR_VAL;
        if (u4Count == 1)

        {
            u4NearEndLossMin = pFrmLossBuffTempNode->u4NearEndLoss;
            u4NearEndLossMax = pFrmLossBuffTempNode->u4NearEndLoss;
            u4FarEndLossMin = pFrmLossBuffTempNode->u4FarEndLoss;
            u4FarEndLossMax = pFrmLossBuffTempNode->u4FarEndLoss;
        }
        if (pFrmLossBuffTempNode->u4NearEndLoss < u4NearEndLossMin)

        {
            u4NearEndLossMin = pFrmLossBuffTempNode->u4NearEndLoss;
        }
        if (pFrmLossBuffTempNode->u4NearEndLoss > u4NearEndLossMax)

        {
            u4NearEndLossMax = pFrmLossBuffTempNode->u4NearEndLoss;
        }
        if (pFrmLossBuffTempNode->u4FarEndLoss < u4FarEndLossMin)

        {
            u4FarEndLossMin = pFrmLossBuffTempNode->u4FarEndLoss;
        }
        if (pFrmLossBuffTempNode->u4FarEndLoss > u4FarEndLossMax)

        {
            u4FarEndLossMax = pFrmLossBuffTempNode->u4FarEndLoss;
        }
        pFrmLossBuffTempNode =
            EcfmGetNextFrmLossNodeForATrans (pFrmLossBuffTempNode);
    }

    if ((pu4FlStatsFarEndLossTotal != NULL) &&
        (pu4FlStatsNearEndLossTotal != NULL))
    {
        *pu4FlStatsFarEndLossTotal = u4FlStatsFarEndLossTotal;
        *pu4FlStatsNearEndLossTotal = u4FlStatsNearEndLossTotal;
    }
    if (pu4FlStatsMessagesIn != NULL)

    {

        /*Update the  Lm In count */
        *pu4FlStatsMessagesIn = u4FlStatsMessagesIn;
    }

    if (0 == u4FlStatsMessagesIn)
    {
        return;
    }

    if (pu4FlStatsFarEndLossAverage != NULL)

    {
        u4FlStatsFarEndLossAverage =
            u4FlStatsFarEndLossTotal / (u4FlStatsMessagesIn);

        /*Get the Far End Loss Averaage for a Transaction */
        *pu4FlStatsFarEndLossAverage = u4FlStatsFarEndLossAverage;
    }
    if (pu4FlStatsNearEndLossAverage != NULL)

    {
        u4FlStatsNearEndLossAverage =
            u4FlStatsNearEndLossTotal / (u4FlStatsMessagesIn);

        /*Get the Near End Loss Averaage for a Transaction */
        *pu4FlStatsNearEndLossAverage = u4FlStatsNearEndLossAverage;
    }
    if (pu4FlStatsNearEndLossMin != NULL)

    {
        *pu4FlStatsNearEndLossMin = u4NearEndLossMin;
    }
    if (pu4FlStatsNearEndLossMax != NULL)

    {
        *pu4FlStatsNearEndLossMax = u4NearEndLossMax;
    }
    if (pu4FlStatsFarEndLossMin != NULL)

    {
        *pu4FlStatsFarEndLossMin = u4FarEndLossMin;
    }
    if (pu4FlStatsFarEndLossMax != NULL)

    {
        *pu4FlStatsFarEndLossMax = u4FarEndLossMax;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwAddMaMepListEntry
 *
 *    DESCRIPTION      : This function adds MaMepList entry
 *                       corresponding to indices in global info's MaMepList
 *                       and in its associated MA's MepList.      
 *
 *
 *    INPUT            : pMepListNewNode- pointer to MepList Node .
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwAddMaMepListEntry (tEcfmCcMepListInfo * pMepListNewNode)
{

    /* Add MaMepList node in MaMepLisTable in Global info */
    if (RBTreeAdd (ECFM_CC_MA_MEP_LIST_TABLE, pMepListNewNode) ==
        ECFM_RB_FAILURE)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSnmpLwAddMaMepListEntry:"
                     "MEPList node addition FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwDeleteMaMepListEntry
 *
 *    DESCRIPTION      : This function deletes MaMepList entry from
 *                       MaMepListTable present in Global info and 
 *                       from its associated MA's MepList .
 *
 *    INPUT            : pMepListNode - pointer to MepList entry that needs to 
 *                       be removed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUSSESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwDeleteMaMepListEntry (tEcfmCcMepListInfo * pMepListNode)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcRMepDbInfo  *pTempRMepNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, 0x00, ECFM_CC_PDUSM_INFO_SIZE);

    /*Get MEP's associated MA node */
    pMaNode = pMepListNode->pMaInfo;

    /*  First remove its corresponding RMEP node from all locally configured
     *  MEPs*/
    pMepNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));
    while (pMepNode != NULL)

    {
        PduSmInfo.pMepInfo = NULL;
        if (pMepNode->pPortInfo != NULL)

        {
            if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))

            {
                EcfmCcmOffDeleteTxRxForMep (pMepNode);
            }

            /* Get particular remote MEP node */
            pRMepNode =
                (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));

            while (pRMepNode != NULL)
            {
                pTempRMepNode = pRMepNode;
                pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_Next
                    (&(pMepNode->RMepDb), &(pTempRMepNode->MepDbDllNode));

                if (pTempRMepNode->u2RMepId == pMepListNode->u2MepId)
                {
                    EcfmDeleteRMepEntry (pMepNode, pTempRMepNode);
                }
                pTempRMepNode = NULL;
            }
            PduSmInfo.pMepInfo = pMepNode;
            if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))

            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_OS_RESOURCE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "CCMOFFLOAD: MAMEP List is updated \r\n");
                if (EcfmCcmOffCreateTxRxForMep (pMepNode) == ECFM_FAILURE)

                {
                    return ECFM_FAILURE;
                }

                /* Create Reception call for this RMEP */
            }

            /*Calculate Rdi, MacStatus and Remote CCM Defect */
            EcfmCcFngCalulateRemoteDefects (&PduSmInfo, ECFM_TRUE);
        }
        pMepNode = (tEcfmCcMepInfo *) TMO_DLL_Next
            (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));
    }

    /*Remove MepListNode from its associated MA's MepList */

    /*Remove it from Global info's MaMepListTable */
    RBTreeRem (ECFM_CC_MA_MEP_LIST_TABLE, (tRBElem *) pMepListNode);

    /*Clear MepListNode's back pointer */
    pMepListNode->pMaInfo = NULL;
    return ECFM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwUpdateAllMepRowStatus 
 *                                                                          
 *    DESCRIPTION      : This function updates Row status of all MEPs 
 *                       associated with MA .
 *
 *    INPUT            : pMaNode , pointer to MA.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwUpdateAllMepRowStatus (tEcfmCcMaInfo * pMaNode)
{
    tEcfmCcMepInfo     *pMepTempNode = NULL;

    /* Get all MEPs associated with pMaNode */
    pMepTempNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));
    while (pMepTempNode != NULL)

    {

        /* Call SNMP low level routine to set MEP's Rowstatus, 
         * with MEP indices */
        nmhSetDot1agCfmMepRowStatus (pMepTempNode->u4MdIndex,
                                     pMepTempNode->u4MaIndex,
                                     pMepTempNode->u2MepId,
                                     pMaNode->u1RowStatus);
        pMepTempNode =
            (tEcfmCcMepInfo *) TMO_DLL_Next (&(pMaNode->MepTable),
                                             &(pMepTempNode->MepTableDllNode));
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwUpdateAllMaRowStatus 
 *                                                                          
 *    DESCRIPTION      : This function updates row status of all MAs, 
 *                       associated with MD
 *
 *    INPUT            : pMdNode , pointer to MD.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwUpdateAllMaRowStatus (tEcfmCcMdInfo * pMdNode)
{
    tEcfmCcMaInfo      *pMaTempNode = NULL;

    /* Get all MAs associated with pMaNode */
    pMaTempNode = (tEcfmCcMaInfo *) RBTreeGetFirst (pMdNode->MaTable);
    while (pMaTempNode != NULL)

    {

        /* Call SNMP low level routine to set MA's Rowstatus, 
         * with MA indices */
        nmhSetDot1agCfmMaNetRowStatus (pMaTempNode->u4MdIndex,
                                       pMaTempNode->u4MaIndex,
                                       pMdNode->u1RowStatus);
        pMaTempNode =
            (tEcfmCcMaInfo *) RBTreeGetNext (pMdNode->MaTable,
                                             (tRBElem *) pMaTempNode, NULL);
    }
    return;
}

/****************************************************************************

 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwIsInfoConfiguredForMa 
 *                                                                          
 *    DESCRIPTION      : This function checks for uniqueness of
 *                       -- MA name in its associated MD
 *                       -- MA PrimaryVid with in all MAs at same MdLevel.
 *
 *    INPUT            : pMaNode, pointer to Ma node
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwIsInfoConfiguredForMa (tEcfmCcMaInfo * pMaNode)
{
    tEcfmCcMaInfo      *pMaCurrentNode = NULL;
    tEcfmCcMdInfo      *pMdCurrentNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pMaNode->u1SelectorType) == ECFM_TRUE)
    {
        /* ICC and UMC name must be configured before activation */
        if ((pMaNode->u1IccCodeLength == 0) || (pMaNode->u1UmcCodeLength == 0))
        {
            CLI_SET_ERR (CLI_ECFM_MA_CONFIG_ERR);
            return ECFM_FAILURE;
        }
    }

    /* Get MA's associated MD */
    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);

    /* Check for uniqueness of PrimaryVid with in all MAs configured at same
     * MdLevel, and uniqueness of MaName in all MAs with in its associated
     * MD */
    pMdCurrentNode = RBTreeGetFirst (ECFM_CC_MD_TABLE);
    while (pMdCurrentNode != NULL)

    {

        /* Check if there exists any MD with same MdLevel */
        if (pMdCurrentNode->u1Level == pMdNode->u1Level)
        {
            pMaCurrentNode = RBTreeGetFirst (pMdCurrentNode->MaTable);
            while (pMaCurrentNode != NULL)
            {
                if (pMaCurrentNode->u4MdIndex == pMaNode->u4MdIndex)
                {
                    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (pMaNode->u1SelectorType)
                        != ECFM_TRUE)
                    {
                        /* Check if there exists any Active MA with PrimaryVid */
                        if ((pMaCurrentNode != pMaNode) &&
                            (pMaCurrentNode->u1RowStatus ==
                             ECFM_ROW_STATUS_ACTIVE) &&
                            (pMaCurrentNode->u4PrimaryVidIsid ==
                             pMaNode->u4PrimaryVidIsid))
                        {
                            CLI_SET_ERR (CLI_ECFM_MA_CONFIG_ERR);
                            return ECFM_FAILURE;
                        }
                    }
                }
                /* Check if there exists any Active MA in its associated MD with 
                 * same MaName*/
                if ((pMaCurrentNode != pMaNode) &&
                    (pMaCurrentNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE) &&
                    (pMaCurrentNode->u4MdIndex == pMaNode->u4MdIndex) &&
		    (ECFM_MEMCMP (pMaCurrentNode->au1Name, pMaNode->au1Name,                                    ECFM_MA_NAME_ARRAY_SIZE) == ECFM_VAL_0))
                {
                    CLI_SET_ERR (CLI_ECFM_MA_CONFIG_ERR);
                    return ECFM_FAILURE;
                }

                /* Move to next MA in its associated MD's MaTable */
                pMaCurrentNode = RBTreeGetNext (pMdCurrentNode->MaTable,
                                                (tRBElem *) pMaCurrentNode,
                                                NULL);
            }
        }
        pMdCurrentNode = RBTreeGetNext (ECFM_CC_MD_TABLE,
                                        (tRBElem *) pMdCurrentNode, NULL);
    }

    /* Primary Vlan Id check is outside Validate routine to remove dependency of
     * asigning Primary VlanId before configuring MaName Format*/
    if (pMaNode->u1NameFormat == ECFM_ASSOC_NAME_PRIMARY_VID)

    {
        UINT2               u2VlanId = ECFM_INIT_VAL;

        /* Get the first Octet from MA Name */
        u2VlanId = pMaNode->au1Name[0];

        /* Shift to form complete Vlan Id */
        u2VlanId = u2VlanId << ECFM_SHIFT_8BITS;

        /* Get the second Octet from MA Name */
        u2VlanId |= pMaNode->au1Name[1];

        /*Mask Reserved fields in the Octet string */
        u2VlanId &= ECFM_MASK_WITH_VAL_4095;
        if (pMaNode->u4PrimaryVidIsid != u2VlanId)

        {
            CLI_SET_ERR (CLI_ECFM_MA_NAME_FORMAT_ERR);
            return ECFM_FAILURE;
        }
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwDefaultMaConfigAllowed 
 *                                                                          
 *    DESCRIPTION      : This function checks if there exists
 *                       -- MA with "DEFAULT" as a name or
 *                       -- MA with primary Vlan Id zero at same MD level.
 *
 *    INPUT            : u1MdLevel - MD Level of MA, for which default
 *                                   configuration is to be checked.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC              BOOL1
EcfmSnmpLwDefaultMaConfigAllowed (tEcfmCcMdInfo * pMdNode)
{
    tEcfmCcMaInfo      *pMaCurrentNode = NULL;
    tEcfmCcMdInfo      *pMdCurrentNode = NULL;

    /* Check for uniqueness of PrimaryVid as 0 with in all MAs configured at same
     * MdLevel, and uniqueness of MaName as "DEFAULT"in all MAs with in its associated
     * MD */
    pMdCurrentNode = RBTreeGetFirst (ECFM_CC_MD_TABLE);
    while (pMdCurrentNode != NULL)
    {
        if (pMdCurrentNode->u1Level == pMdNode->u1Level)
        {
            pMaCurrentNode = (tEcfmCcMaInfo *) RBTreeGetFirst
                (pMdCurrentNode->MaTable);
            while (pMaCurrentNode != NULL)
            {
                /* Check if there exists any Active MA with PrimaryVid as
                 * zero 
                 */
                if (pMaCurrentNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)
                {
                    if (pMaCurrentNode->u4PrimaryVidIsid == ECFM_INIT_VAL)
                    {
                        return ECFM_FALSE;
                    }
                    /* Check if there exists any MA in its associated MD 
                     * with DEFAULT MaName
                     */
                    if ((pMaCurrentNode->u4MdIndex == pMdNode->u4MdIndex) &&
                        (ECFM_STRCMP (pMaCurrentNode->au1Name, "DEFAULT") == 0))
                    {
                        return ECFM_FALSE;
                    }
                }
                /* Move to next MA in its associated MD's MaTable */
                pMaCurrentNode = (tEcfmCcMaInfo *)
                    RBTreeGetNext (pMdCurrentNode->MaTable,
                                   (tRBElem *) pMaCurrentNode, NULL);
            }
        }
        pMdCurrentNode = (tEcfmCcMdInfo *) RBTreeGetNext (ECFM_CC_MD_TABLE,
                                                          (tRBElem *)
                                                          pMdCurrentNode, NULL);
    }

    return ECFM_TRUE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwAddMaEntry
 *
 *    DESCRIPTION      : This function adds Ma entry
 *                       in MaTableIndex in Global info and in MaTable
 *                       in its associated MD.
 *
 *
 *    INPUT            : pMaNewNode- pointer to Ma Node, that needs to be added.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwAddMaEntry (tEcfmCcMaInfo * pMaNewNode)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MA's associated MD */
    pMdNode = pMaNewNode->pMdInfo;

    /* Add MA node in MaTableIndex in Global info */
    if (RBTreeAdd (ECFM_CC_MA_TABLE, pMaNewNode) == ECFM_RB_FAILURE)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmSnmpLwAddMaEntry: MA Node Addition in global info"
                     "Failed\r\n");
        return ECFM_FAILURE;
    }

    /* Add MA node in MaTable in its associated MD node */
    if (RBTreeAdd (pMdNode->MaTable, pMaNewNode) == ECFM_RB_FAILURE)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmSnmpLwAddMaEntry: MA Node Addition in MD's MaTable"
                     "Failed\r\n");

        /*Remove MA node from global info's MaTable index */
        RBTreeRem (ECFM_CC_MA_TABLE, pMaNewNode);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmSnmpLwDeleteMaEntry
 *
 *    DESCRIPTION      : This function deletes MA entry from RBTree MaTableIndex
 *                       present in Global info and from its associated MD's
 *                       MaTable.
 *
 *
 *    INPUT            : pMaNode - pointer to Ma entry that needs to be
 *                       removed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : None.
 *
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwDeleteMaEntry (tEcfmCcMaInfo * pMaNode)
{
    tEcfmCcMdInfo      *pMdNode = NULL;

    /* Get MA's associated MD */
    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);

    /* Remove MA from its associated MD's MaTable */
    RBTreeRem (pMdNode->MaTable, (tRBElem *) pMaNode);

    /* Now remove MA from Global info's MaTableIndex */
    RBTreeRem (ECFM_CC_MA_TABLE, (tRBElem *) pMaNode);

    /* Clear MA's backward pointer to MD */
    pMdNode = NULL;
    return;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmSnmpLwAddMepEntry
 *
 *    DESCRIPTION      : This function adds MEP entry
 *                       in MepTableIndex in Global info and in its
 *                       associated MA's MepTable. 
 *
 *    INPUT            : pMepNewNode- pointer to Mep Node that needs to added.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwAddMepEntry (tEcfmCcMepInfo * pMepNewNode)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /* Get MEP's associated MA */
    pMaNode = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNewNode);

    /* Add MEP node in GlobalInfo's RBTree MepTableIndex */
    if (RBTreeAdd (ECFM_CC_MEP_TABLE, pMepNewNode) == ECFM_RB_FAILURE)

    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSnmpLwAddMepEntry : RBTree Add in MepTableIndex"
                     "Failed!!\r\n");
        return ECFM_FAILURE;
    }

    /* Add MEP node in its associated MA's DLL MepTable */
    TMO_DLL_Add (&(pMaNode->MepTable),
                 (tTMO_DLL_NODE *) & (pMepNewNode->MepTableDllNode));
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmSnmpLwAddMepInAPort
 *  
 *    DESCRIPTION      : This function adds MEP entry in port Info's MepInfoTree,
 *                       based on IfIndex, MdLevel, PrimaryVid and Direction.
 *                            
 *   
 *    INPUT            : pMepNode- pointer to Mep Node .
 *   
 *    OUTPUT           : None.
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *   

 *******************************************************************************/
PUBLIC INT4
EcfmSnmpLwAddMepInAPort (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcPortInfo    *pPortEntry = NULL;

    /* Get PortInfo corresponding to MEP's IfIndex */
    pPortEntry = ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum);
    if (pPortEntry == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSnmpLwAddMepInAPort: No Port Information present\r\n");
        return ECFM_FAILURE;
    }

    pMepNode->pPortInfo = pPortEntry;
    pMepNode->u4IfIndex = pPortEntry->u4IfIndex;

    /* Mplstp Oam mep are not added into to ECFM_CC_PORT_MEP_TABLE
     * it is added only in ECFM_CC_MEP_TABLE.
     */
    if (!((pMepNode->pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMepNode->pMaInfo->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {
        /* Add MEP node in context's RBTree MepContextIndex */
        if (RBTreeAdd (ECFM_CC_PORT_MEP_TABLE, pMepNode) == ECFM_RB_FAILURE)

        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmSnmpLwAddMepInAPort: Mep addition to CC RBTree "
                         "Failed!!\r\n");
            return ECFM_FAILURE;
        }
    }

    /* Set backward pointer to PortInfo */
    pMepNode->pPortInfo = pPortEntry;

    /* Add MEP node in LbLt task's PortInfo's MepInfoTree */
    if (EcfmLbLtAddMepInAPort (pMepNode) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmSnmpLwAddMepInAPort: Mep addition in lblt "
                     "Failed!!\r\n");
        return ECFM_FAILURE;
    }

    EcfmCcUtilUpdatePortState (pMepNode, ECFM_INIT_VAL);

    return ECFM_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmSnmpLwAddMepInStack
 *  
 *    DESCRIPTION      : This function adds MEP entry
 *                       corresponding to indices in StackTable in GlobalInfo of
 *                       both the tasks. 
 *                            
 *   
 *    INPUT            : pMepNode- pointer to MEP Node .
 *   
 *    OUTPUT           : None.
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *   

 *******************************************************************************/
PUBLIC INT4
EcfmSnmpLwAddMepInStack (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcStackInfo   *pStackNewNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;

    /* Create Stack node corresponding to MEP */
    if (ECFM_ALLOC_MEM_BLOCK_CC_STACK_TABLE (pStackNewNode) == NULL)

    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmSnmpLwAddMepInStack:"
                     "MEM Block Allocation for Stack node Failed" "\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pStackNewNode, ECFM_INIT_VAL, ECFM_CC_STACK_INFO_SIZE);

    /* Put required parameters from MEP node */
    pStackNewNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
    pStackNewNode->pMepInfo = pMepNode;
    pStackNewNode->pMipInfo = NULL;
    pStackNewNode->u2PortNum = pMepNode->u2PortNum;
    pStackNewNode->u4VlanIdIsid = pMepNode->u4PrimaryVidIsid;
    pStackNewNode->u1MdLevel = pMepNode->u1MdLevel;
    pStackNewNode->u1Direction = pMepNode->u1Direction;
    pStackNewNode->u4IfIndex =
        ECFM_CC_PORT_INFO (pMepNode->u2PortNum)->u4IfIndex;

    if (RBTreeAdd (ECFM_CC_GLOBAL_STACK_TABLE, pStackNewNode) == RB_FAILURE)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackNewNode));
        pStackNewNode = NULL;
        return ECFM_FAILURE;
    }
    pPortInfo = ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum);
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    if (RBTreeAdd (pPortInfo->StackInfoTree, pStackNewNode) == RB_FAILURE)
    {
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) pStackNewNode);
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackNewNode));
        pStackNewNode = NULL;
        return ECFM_FAILURE;
    }
    /* Add Stack node in LBLT task's StackTable in global info */
    if (EcfmLbLtAddMepInStack
        (pMepNode->u4MdIndex, pMepNode->u4MaIndex, pMepNode->u2MepId,
         ECFM_CC_CURR_CONTEXT_ID ()) != ECFM_SUCCESS)

    {
        RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) pStackNewNode);
        RBTreeRem (pPortInfo->StackInfoTree, (tRBElem *) pStackNewNode);
        /* Release the memory allocated to Stack new node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                             (UINT1 *) (pStackNewNode));
        pStackNewNode = NULL;
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmDeleteStackEntryForMep
 *
 *    DESCRIPTION      : This function deletes particular stack entry from
 *                       Global info's StackTable . 
 *
 *    INPUT            : pMepNode - pointer to MEP entry, corresponding to which
 *                       stack entry needs to be deleted
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
EcfmDeleteStackEntryForMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcStackInfo   *pStackNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;

    /* Get Stack node corresponding to this MEP */
    pStackNode = EcfmCcUtilGetMp (pMepNode->u2PortNum,
                                  pMepNode->u1MdLevel,
                                  pMepNode->u4PrimaryVidIsid,
                                  pMepNode->u1Direction);

    pPortInfo = ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum);
    if (pStackNode != NULL)
    {
        if (pStackNode->pMepInfo != NULL)
        {
            if (pPortInfo != NULL)
            {
                RBTreeRem (pPortInfo->StackInfoTree, (tRBElem *) pStackNode);
            }
            RBTreeRem (ECFM_CC_GLOBAL_STACK_TABLE, (tRBElem *) pStackNode);
            /* Release memory allocated to stack node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_STACK_TABLE_POOL,
                                 (UINT1 *) (pStackNode));
            pStackNode = NULL;
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmUtilSnmpLwDeleteMepEntry
 *
 *    DESCRIPTION      : This function deletes particular MEP entry from
 *                       MEP's associated MA, Global info's MepTableIndex,
 *                       Port info's MepInfoTree , And corresponding stack 
 *                       entry of both the tasks. 
 *
 *    INPUT            : pMepNode - pointer to MEP entry that needs to be
 *                       removed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwDeleteMepEntry (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepCcInfo   *pCcInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcRMepDbInfo  *pTempRMepNode = NULL;
    tEcfmCcMepInfo     *pTempMepNode = NULL;
#ifdef NPAPI_WANTED
    tEcfmHwParams       EcfmHwInfo;
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL,
                 ECFM_HW_MEP_HANDLER_SIZE);
#endif
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepNode);

    /* Release memory allocated to CCM PDUs that
     *  have been stored in case of any CCM related error */
    if (pCcInfo->ErrorCcmLastFailure.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_ERR_CCM_POOL,
                             pCcInfo->ErrorCcmLastFailure.pu1Octets);
        pCcInfo->ErrorCcmLastFailure.pu1Octets = NULL;
        pCcInfo->ErrorCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }
    if (pCcInfo->XconCcmLastFailure.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_XCON_CCM_POOL,
                             pCcInfo->XconCcmLastFailure.pu1Octets);
        pCcInfo->XconCcmLastFailure.pu1Octets = NULL;
        pCcInfo->XconCcmLastFailure.u4OctLen = ECFM_INIT_VAL;
    }

    /* Check if MEP has been configured at particular Port */
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    if (pPortInfo != NULL)

    {

        /*Remove MEP node from MepInfoTree and from Mpstack */
        /* Indicate state machines before MEP deletion */
        EcfmCcUtilNotifySm (pMepNode, ECFM_IND_MEP_INACTIVE);
        EcfmCcUtilNotifyY1731 (pMepNode, ECFM_IND_MEP_INACTIVE);
        EcfmLbLtNotifySM (pMepNode->u4MdIndex, pMepNode->u4MaIndex,
                          pMepNode->u2MepId, ECFM_CC_CURR_CONTEXT_ID (),
                          ECFM_IND_MEP_INACTIVE);

#ifdef NPAPI_WANTED
        /* Stop CCM Tx and Stop CCM Rx is informed to offload
         * module above through EcfmCcUtilNotifySm.
         * Now delete the MEP from the hardware.
         */
        if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
                         ECFM_HW_MEP_HANDLER_SIZE) != 0)
        {
            MEMSET (&EcfmHwInfo, ECFM_INIT_VAL, sizeof (tEcfmHwParams));

            EcfmHwUpdateMaMepParams (&EcfmHwInfo, pMepNode, NULL);
            EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex = pMepNode->u4IfIndex;

            if (EcfmFsMiEcfmHwCallNpApi (ECFM_NP_MEP_DELETE,
                                         &EcfmHwInfo) == FNP_FAILURE)
            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmSnmpLwDeleteMepEntry :MEP deletion"
                             "indication to H/W failed. \r\n");
            }
        }
#endif

        /* Delete RMepDb associated with MEP node */
        pRMepNode = (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
        while (pRMepNode != NULL)
        {
            pTempRMepNode = pRMepNode;
            pRMepNode = (tEcfmCcRMepDbInfo *)
                TMO_DLL_Next (&(pMepNode->RMepDb),
                              &(pTempRMepNode->MepDbDllNode));

            EcfmDeleteRMepEntry (pMepNode, pTempRMepNode);
            pTempRMepNode = NULL;
        }
        ECFM_MEMSET (&(pMepNode->RMepDb), ECFM_INIT_VAL,
                     sizeof (pMepNode->RMepDb));
        /* Delete stack entry corresponding to MEP */
        if (pMepNode->pEcfmMplsParams == NULL)
        {
            EcfmDeleteStackEntryForMep (pMepNode);
            pMepNode->pPortInfo = NULL;
        }
    }
    if (EcfmMpTpDelMaEntry (pMepNode) == ECFM_FAILURE)
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: Unable to remove MA entry in RBTree for"
                     "LSP/PW status\n");
    }

    /* Remove MEP node from LBLT task's Global info's MepTableIndex, 
     * PortInfo's MepInfoTree, Stack Table*/
    EcfmLbLtDeleteMepEntry (pMepNode->u4MdIndex, pMepNode->u4MaIndex,
                            pMepNode->u2MepId, ECFM_CC_CURR_CONTEXT_ID ());

    /* Remove MEP node from its associated MA */
    pMaNode = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);
    TMO_DLL_Delete (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));
    pMepNode->pMaInfo = NULL;

    /* Remove MEP node from Global info's MepInfoTree */
    RBTreeRem (ECFM_CC_MEP_TABLE, (tRBElem *) pMepNode);

    pTempMepNode = RBTreeGet (ECFM_CC_PORT_MEP_TABLE, pMepNode);
    if ((pTempMepNode != NULL) &&
        (pTempMepNode->u4MdIndex == pMepNode->u4MdIndex) &&
        (pTempMepNode->u4MaIndex == pMepNode->u4MaIndex) &&
        (pTempMepNode->u2MepId == pMepNode->u2MepId))
    {
        /* Remove Port MEP node from Context info */
        RBTreeRem (ECFM_CC_PORT_MEP_TABLE, (tRBElem *) pMepNode);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwIsInfoConfiguredForMep 
 *                                                                          
 *    DESCRIPTION      : This function checks whether MEP can be configured
 *                       on specified port, with particular Mdlevel and 
 *                       primaryVid and Direction. 
 *                       
 *
 *    INPUT            : pMepNode , pointer to Mep node
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwIsInfoConfiguredForMep (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcMepInfo     *pMepTempNode = NULL;
    tEcfmCcMaInfo      *pMaTempNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo       MaNode;
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    tEcfmCcStackInfo   *pStackNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    UINT1               u1HighestMdLevel = ECFM_INIT_VAL;
    INT1                i1UnAwareHighestLevel = ECFM_INVALID_VALUE;
    UINT1               u1AwareLowestLevel = ECFM_MD_LEVEL_MAX + 1;
    BOOL1               b1IsMep = ECFM_INIT_VAL;

    ECFM_MEMSET (&MaNode, ECFM_INIT_VAL, ECFM_CC_MA_INFO_SIZE);
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    /* Check if IfIndex has been set or not */
    if (pMepNode->u2PortNum == 0)

    {
        return ECFM_FAILURE;
    }
    pMaNode = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);

    /* Check whether MEP is to be associated with VLAN un aware MA */
    if ((pMepNode->u4PrimaryVidIsid != 0) && (pMaNode->u2NumberOfVids == 0))

    {
        return ECFM_FAILURE;
    }

    /* Check whether MEP is to be associated with MA associated with only 
     * one VLAN*/
    if ((pMaNode->u2NumberOfVids == 1) &&
        (pMepNode->u4PrimaryVidIsid != pMaNode->u4PrimaryVidIsid))

    {
        return ECFM_FAILURE;
    }

    /* Check whether MEP is to be associated with MA associated with more than 
     *  one VLAN*/
    if ((pMaNode->u2NumberOfVids > 1) &&
        (pMepNode->u4PrimaryVidIsid != pMaNode->u4PrimaryVidIsid))

    {

        /* MEP PrimaryVid should be same as MA's PrimaryVid OR 
           Entry for MEP's PrimaryVid should present in VLAN Table */
        pVlanNode = EcfmSnmpLwGetVlanEntry (pMepNode->u4PrimaryVidIsid);
        if (pVlanNode == NULL)

        {
            return ECFM_FAILURE;
        }
    }
    if ((pMepNode->u4PrimaryVidIsid == 0) &&
        (pMepNode->u1Direction == ECFM_MP_DIR_UP))

    {
        return ECFM_FAILURE;
    }

    /* If MEP to be configured is UP MEP and MA associated with it shares
     * Primary VID with another MA, then UP MEP cannot be configured */
    if (pMepNode->u1Direction == ECFM_MP_DIR_UP)
    {

        /* Get all MA entries for this MD, and match Primary VID. If an Entry is
         * found, UP MEP creation should fail
         */
        pMdNode = EcfmSnmpLwGetMdEntry (pMaNode->u4MdIndex);
        if (pMdNode != NULL)
        {
            MaNode.u4MdIndex = pMaNode->u4MdIndex;
            MaNode.u4MaIndex = 0;
            pMaTempNode = RBTreeGetNext (pMdNode->MaTable, (tRBElem *) & MaNode,
                                         NULL);
            while (pMaTempNode != NULL)
            {
                if ((pMepNode->pMaInfo != pMaTempNode) &&
                    (pMepNode->pMaInfo->u4PrimaryVidIsid ==
                     pMaTempNode->u4PrimaryVidIsid))
                {
                    return ECFM_FAILURE;
                }
                pMaTempNode =
                    RBTreeGetNext (pMdNode->MaTable, pMaTempNode, NULL);
            }
        }
    }
    /* An MA can have either all UP MEPs or All Down MEPs on same switch */
    /* Scan DLL maintained per MA and Get all MEPs configured in an MA */
    pMepTempNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));
    while (pMepTempNode != NULL)
    {
        if (pMepTempNode->pPortInfo != NULL)
        {
            if ((pMepTempNode != pMepNode) &&
                (pMepNode->u1Direction != pMepTempNode->u1Direction))
            {
                return ECFM_FAILURE;
            }
        }
        pMepTempNode = (tEcfmCcMepInfo *)
            TMO_DLL_Next (&(pMaNode->MepTable),
                          &(pMepTempNode->MepTableDllNode));
    }

    /* Check if  VLAN unaware MEP can be configured on port */
    pPortInfo = ECFM_CC_GET_PORT_INFO (pMepNode->u2PortNum);
    if (pPortInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    if ((pPortInfo->u1IfType == CFA_LAGG) && (pMepNode->u4PrimaryVidIsid == 0))

    {
        return ECFM_FAILURE;
    }

    if (pPortInfo->u2ChannelPortNum != 0)
    {
        if (pMepNode->u4PrimaryVidIsid != 0)

        {
            return ECFM_FAILURE;
        }
    }

    /* Check if Port's MepInfoTree is empty and
     * and Check if MEP's MdLevel is higher or equal than any MIP's MdLevel
     * configured on same port..*/

    /*Get if any MP node with these indices already present */
    pStackNode = EcfmCcUtilGetMp (pMepNode->u2PortNum,
                                  pMepNode->u1MdLevel,
                                  pMepNode->u4PrimaryVidIsid,
                                  pMepNode->u1Direction);
    if (pStackNode != NULL)

    {
        if (pStackNode->pMepInfo != NULL)
        {
            /* MEP found */
            return ECFM_FAILURE;
        }
    }

    /* Check if there is any MIP at lower level than the level of MEP to be
     * configured */
    if (EcfmCcUtilGetHighestMpMdLevel
        (pMepNode->u2PortNum, pMepNode->u4PrimaryVidIsid, &u1HighestMdLevel,
         &b1IsMep) == ECFM_SUCCESS)
    {
        if (!b1IsMep)
        {
            if (u1HighestMdLevel < pMepNode->u1MdLevel)
            {
                return ECFM_FAILURE;
            }
            else
            {
                /* Check if MIP created is Implicitly created. If so, then delete
                 * MIP created and allow MEP creation 
                 */
                pMipNode = EcfmCcUtilGetMipEntry (pMepNode->u2PortNum,
                                                  u1HighestMdLevel,
                                                  pMepNode->u4PrimaryVidIsid);
                if (pMipNode != NULL)
                {
                    if (pMipNode->b1ImplicitlyCreated == ECFM_TRUE)
                    {
                        /* Delete Implicit MIP created */
                        EcfmCcUtilDeleteMipEntry (pMipNode);
                        /* Release memory allocated to Mip node */
                        ECFM_FREE_MEM_BLOCK (ECFM_CC_MIP_TABLE_POOL,
                                             (UINT1 *) (pMipNode));
                    }
                }
            }
        }
    }
    /* Get highest Mdlevel of unaware MEPs and lowest Mdlevel of aware
     * MEPs at this port */
    gpEcfmCcMepNode->u2PortNum = pMepNode->u2PortNum;
    gpEcfmCcMepNode->u4PrimaryVidIsid = pMepNode->u4PrimaryVidIsid;
    pMepTempNode = (tEcfmCcMepInfo *) RBTreeGetNext
        (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);

    while (pMepTempNode != NULL &&
           (pMepTempNode->u2PortNum == pMepNode->u2PortNum) &&
           (pMepTempNode->u4PrimaryVidIsid == pMepNode->u4PrimaryVidIsid))

    {
        if (pMepTempNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE)

        {

            /* Check for the highest unaware Mdlevel */
            if ((pMepTempNode->u4PrimaryVidIsid == 0) &&
                (pMepTempNode->u1MdLevel > i1UnAwareHighestLevel))

            {
                i1UnAwareHighestLevel = (INT1) pMepTempNode->u1MdLevel;
            }
            if ((pMepTempNode->u4PrimaryVidIsid != 0) &&
                ((pMepTempNode->u1MdLevel < u1AwareLowestLevel) &&
                 (pMepTempNode->u1Direction == ECFM_MP_DIR_DOWN)))

            {
                u1AwareLowestLevel = pMepTempNode->u1MdLevel;
            }
        }
        pMepTempNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                      pMepTempNode, NULL);
    }

    /* Check for VLAN unaware MEP */
    if (pMepNode->u4PrimaryVidIsid == 0)

    {

        /* Validate MEP Direction */
        if ((pMepNode->u1Direction == ECFM_MP_DIR_UP) ||
            (pMepNode->u1MdLevel >= u1AwareLowestLevel))

        {
            return ECFM_FAILURE;
        }
    }

    else
        /* Validate the level for VLAN aware MEP */
    {
        if ((pMepNode->u1MdLevel <= i1UnAwareHighestLevel) &&
            (pMepNode->u1Direction == ECFM_MP_DIR_DOWN))

        {
            return ECFM_FAILURE;
        }
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 *  FUNCTION NAME    : EcfmSnmpLwHandleMepAddFailure
 *
 *  DESCRIPTION      : This function  deletes entry from its associtated MA,
 *                     MepTableIndex in CC global info, from particular port,
 *                     Stack Table if its ifIndex has been configured.
 *                     
 *  INPUT            : pMepNode- pointer to Mep Node .
 *  
 *  OUTPUT           : None.
 *  RETURNS          : None
 *  
 ********************************************************************************/
PUBLIC VOID
EcfmSnmpLwHandleMepAddFailure (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcMaInfo      *pMaNode = NULL;

    /*Get MEP's associated MA node */
    pMaNode = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);

    /* Remove MEP from its associated MA node */
    TMO_DLL_Delete (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));

    /* Remove MEP from Global info MepTableIndex */
    RBTreeRem (ECFM_CC_MEP_TABLE, (tRBElem *) pMepNode);

    /* Remove Port MEP node from Context info */
    RBTreeRem (ECFM_CC_PORT_MEP_TABLE, (tRBElem *) pMepNode);

    /*Clear its back pointer to MA */
    pMaNode = NULL;

    /* Release the memory allocated to MEP node and MPLS-TP Params block */
    if (pMepNode->pEcfmMplsParams != NULL)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_MPTP_PARAMS_POOL,
                             (UINT1 *) pMepNode->pEcfmMplsParams);
    }

    ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_TABLE_POOL, (UINT1 *) pMepNode);
    ECFM_CC_TRC (ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                 "EcfmSnmpLwHandleMepAddFailure: Freeing MEP Node \r\n ");
    pMepNode = NULL;
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetNoOfEntriesForPVid 
 *                                                                          
 *    DESCRIPTION      : This function returns number of entries associated  
 *                       with PrimaryVid from Vlan Table. 
 *
 *    INPUT            : primaryVid - Primary vid for which the entries need to
 *                       be found.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : Number of entries corresponding to primaryVid.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC UINT2
EcfmSnmpLwGetNoOfEntriesForPVid (UINT2 u4PrimaryVidIsid)
{
    UINT2               u2NoOfEntries = ECFM_INIT_VAL;
    tEcfmCcVlanInfo    *pVlanNode = NULL;
    pVlanNode = (tEcfmCcVlanInfo *) RBTreeGetFirst (ECFM_CC_VLAN_TABLE);
    while (pVlanNode != NULL)

    {

        /*Check if VLAN entry has same PrimaryVid, and its correspondig row
         * status should be active*/
        if ((pVlanNode->u4PrimaryVidIsid == u4PrimaryVidIsid) &&
            (pVlanNode->u1RowStatus == ECFM_ROW_STATUS_ACTIVE))

        {

            /* VLAN entry corresponding to PrimaryVid found */
            u2NoOfEntries = u2NoOfEntries + (UINT2) ECFM_INCR_VAL;
        }
        pVlanNode = (tEcfmCcVlanInfo *) RBTreeGetNext
            (ECFM_CC_VLAN_TABLE, (tRBElem *) pVlanNode, NULL);
    } return u2NoOfEntries;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmMepCmpForMepListInMa
 *
 *    DESCRIPTION      : This routine is used to compare nodes of RBTree 
 *                       MepListTable (indexed by MepId) in a particular Ma 
 *                       node.  
 *
 *    INPUT            : *pE1 - pointer to MepList Node1
 *                       *pE2 - pointer to MepList Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if pE1 > pE2 else returns
 *                       -1      
 *
 ****************************************************************************/
PUBLIC INT4
EcfmMepCmpForMepListInMa (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMepListInfo *pEntryA = NULL;
    tEcfmCcMepListInfo *pEntryB = NULL;
    pEntryA = (tEcfmCcMepListInfo *) pE1;
    pEntryB = (tEcfmCcMepListInfo *) pE2;
    if (pEntryA->u2MepId != pEntryB->u2MepId)

    {
        if (pEntryA->u2MepId < pEntryB->u2MepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmMaCmpForMaTableInMd
 *
 *    DESCRIPTION      : This routine is used to compare nodes of RBTree
 *                       MaTable (indexed by MaIndex) in a particular Md 
 *                       node.
 *
 *    INPUT            : *pE1 - pointer to MA Node1
 *                       *pE2 - pointer to MA Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PUBLIC INT4
EcfmMaCmpForMaTableInMd (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcMaInfo      *pEntryA = NULL;
    tEcfmCcMaInfo      *pEntryB = NULL;
    pEntryA = (tEcfmCcMaInfo *) pE1;
    pEntryB = (tEcfmCcMaInfo *) pE2;
    if (pEntryA->u4MaIndex != pEntryB->u4MaIndex)

    {
        if (pEntryA->u4MaIndex < pEntryB->u4MaIndex)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmCmpForRMepDbInMep
 *
 *    DESCRIPTION      : This routine is used to compare nodes of RBTree
 *                       RMepTable (indexed by RMepId) in a particular Mep 
 *                       node.
 *
 *    INPUT            : *pE1 - pointer to RMep Node1
 *                       *pE2 - pointer to RMep Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCmpForRMepDbInMep (tRBElem * pE1, tRBElem * pE2)
{
    tEcfmCcRMepDbInfo  *pEntryA = NULL;
    tEcfmCcRMepDbInfo  *pEntryB = NULL;
    pEntryA = (tEcfmCcRMepDbInfo *) pE1;
    pEntryB = (tEcfmCcRMepDbInfo *) pE2;
    if (pEntryA->u2RMepId != pEntryB->u2RMepId)

    {
        if (pEntryA->u2RMepId < pEntryB->u2RMepId)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetLtrEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the LTR entry
 *                       from the global structure, LtrTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u4SeqNum  - Ltr sequence number
 *                       u4RcvOrder - Ltr receive order                         
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to LTR Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtLtrInfo *
EcfmSnmpLwGetLtrEntry (UINT4 u4MdIndex,
                       UINT4 u4MaIndex,
                       UINT2 u2MepId, UINT4 u4SeqNum, UINT4 u4RcvOrder)
{
    tEcfmLbLtLtrInfo    LtrInfo;
    tEcfmLbLtLtrInfo   *pLtrNode = NULL;
    ECFM_MEMSET (&LtrInfo, ECFM_INIT_VAL, ECFM_LBLT_LTR_INFO_SIZE);

    /* Get LTR entry corresponding to indices - 
     * MdIndex, MaIndex, MepId, LtmSeqNum, Ltr Recv order */
    LtrInfo.u4MdIndex = u4MdIndex;
    LtrInfo.u4MaIndex = u4MaIndex;
    LtrInfo.u2MepId = u2MepId;
    LtrInfo.u4SeqNum = u4SeqNum;
    LtrInfo.u4RcvOrder = u4RcvOrder;
    pLtrNode = RBTreeGet (ECFM_LBLT_LTR_TABLE, (tRBElem *) & LtrInfo);
    return pLtrNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwCreateRMepInAllMeps
 *                                                                          
 *    DESCRIPTION      : This function creates Remote MEP nodes for all locally
 *                       configured MEPs. 
 *
 *    INPUT            : pMepListNode - Pointer to MEPList entry, which is to 
 *                       be added as Remote MEP.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwCreateRMepInAllMeps (tEcfmCcMepListInfo * pMepListNode)
{
    tEcfmCcMaInfo       MaNode;
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMepListInfo  MepListTempNode;
    tEcfmCcMepListInfo *pMepListTempNode = NULL;
    tEcfmCcMepListInfo *pMepListNextNode = NULL;
    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEPLIST (pMepListNode);

    ECFM_MEMSET (&MaNode, 0x00, ECFM_CC_MA_INFO_SIZE);
    ECFM_MEMSET (&MepListTempNode, 0x00, ECFM_CC_MEP_LIST_SIZE);

    MepListTempNode.u4MdIndex = pMepListNode->u4MdIndex;
    MepListTempNode.u4MaIndex = pMepListNode->u4MaIndex;

    /* Check if it is first and only node */
    pMepListTempNode =
        RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE, &MepListTempNode, NULL);
    pMepListNextNode =
        RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE, pMepListTempNode, NULL);
    if ((pMepListTempNode != NULL)
        && (pMepListTempNode->u4MdIndex == pMepListNode->u4MdIndex)
        && (pMepListTempNode->u4MaIndex == pMepListNode->u4MaIndex))
    {

        if ((pMepListTempNode == pMepListNode) && (pMepListNextNode == NULL))

        {
            return ECFM_SUCCESS;
        }
    }
    /* Get all ACTIVE MEPs associated with MA and create MepList node as a
     * remote MEP */
    pMepNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaInfo->MepTable));
    while (pMepNode != NULL)

    {
        if (pMepNode->pPortInfo != NULL)

        {
            if (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE)

            {
                EcfmCcmOffDeleteTxRxForMep (pMepNode);
            }

            /* Create Remote MEP node in MEP node  */
            EcfmCreateRMepEntryInMep (pMepNode, pMepListNode->u2MepId);
            if (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE)

            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_OS_RESOURCE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "CCMOFFLOAD: MAMEP List is updated \r\n");
                if (EcfmCcmOffCreateTxRxForMep (pMepNode) == ECFM_FAILURE)

                {
                    return ECFM_FAILURE;
                }
            }
        }
        pMepNode = (tEcfmCcMepInfo *)
            TMO_DLL_Next (&(pMaInfo->MepTable), &(pMepNode->MepTableDllNode));
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmSnmpLwDeleteRMepInAllMeps
 *                                                                          
 *    DESCRIPTION      : This function deletes Remote MEP nodes for all locally
 *                       configured MEPs. 
 *
 *    INPUT            : pMepListNode - Pointer to MEPList entry, which is to 
 *                       be added as Remote MEP.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwDeleteRMepInAllMeps (tEcfmCcMepListInfo * pMepListNode)
{
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcRMepDbInfo  *pTempRMepNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    ECFM_MEMSET (&PduSmInfo, 0x00, ECFM_CC_PDUSM_INFO_SIZE);

    /*Get MEP's associated MA node */
    pMaNode = pMepListNode->pMaInfo;

    /*  First remove its corresponding RMEP node from all locally configured
     *  MEPs*/
    pMepNode = (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));
    while (pMepNode != NULL)

    {
        PduSmInfo.pMepInfo = NULL;
        if (pMepNode->pPortInfo != NULL)

        {
            if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))

            {
                EcfmCcmOffDeleteTxRxForMep (pMepNode);
            }

            /* Get particular remote MEP node */
            pRMepNode =
                (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
            while (pRMepNode != NULL)
            {
                pTempRMepNode = pRMepNode;
                pRMepNode = (tEcfmCcRMepDbInfo *)
                    TMO_DLL_Next (&(pMepNode->RMepDb),
                                  &(pTempRMepNode->MepDbDllNode));

                EcfmDeleteRMepEntry (pMepNode, pTempRMepNode);
                pTempRMepNode = NULL;
            }

            PduSmInfo.pMepInfo = pMepNode;
            if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))

            {
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_OS_RESOURCE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "CCMOFFLOAD: MAMEP List is updated \r\n");
                if (EcfmCcmOffCreateTxRxForMep (pMepNode) == ECFM_FAILURE)

                {
                    return ECFM_FAILURE;
                }

                /* Create Reception call for this RMEP */
            }
            /*Calculate Rdi, MacStatus and Remote CCM Defect */
            EcfmCcFngCalulateRemoteDefects (&PduSmInfo, ECFM_TRUE);
        }
        pMepNode = (tEcfmCcMepInfo *) TMO_DLL_Next
            (&(pMaNode->MepTable), &(pMepNode->MepTableDllNode));
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmDeleteRMepEntry
 *                                                                          
 *    DESCRIPTION      : This function deletes remote MEP node from MEPs
 *                       RMepDb and from global's Remote Mep Table. 
 *
 *    INPUT            : pMepTempNode - Pointer to MEP, from which remote MEP
 *                       is to be deleted.
 *                       pRMepNode - Pointer to remote MEP node which is to be
 *                       deleted.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmDeleteRMepEntry (tEcfmCcMepInfo * pMepTempNode,
                     tEcfmCcRMepDbInfo * pRMepNode)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmSenderId      *pSenderId = NULL;
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pRMepInfo = pRMepNode;

    /*Fill the pMepTempNode - Pointer to MEP, from which remote MEP
       is to be deleted, in the PduSmInfo */
    PduSmInfo.pMepInfo = pMepTempNode;

    /* Revert back Remote MEP state machine before removing 
     * remote MEP node */
    EcfmCcClntRmepSm (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);

    /* Delete memory allocated to SenderId tlv if present */
    pSenderId = &(pRMepNode->LastSenderId);
    if (pSenderId->ChassisId.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_CHASSIS_ID_POOL,
                             pSenderId->ChassisId.pu1Octets);
        pSenderId->ChassisId.pu1Octets = NULL;
    }
    if (pSenderId->MgmtAddress.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_POOL,
                             pSenderId->MgmtAddress.pu1Octets);
        pSenderId->MgmtAddress.pu1Octets = NULL;
    }
    if (pSenderId->MgmtAddressDomain.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL,
                             pSenderId->MgmtAddressDomain.pu1Octets);
        pSenderId->MgmtAddressDomain.pu1Octets = NULL;
    }

    /* Remove remote mep node from MEP node's RMepDb and from
     * global remote mep table */
    RBTreeRem (ECFM_CC_RMEP_TABLE, (tRBElem *) pRMepNode);
    TMO_DLL_Delete (&(pMepTempNode->RMepDb), &(pRMepNode->MepDbDllNode));

    /* Delete RMEP DB at LBLT also */
    EcfmLbLtRemoveRMepDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pRMepNode);

    /* Free memory allocated to Remote MEP node */
    ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_DB_TABLE_POOL, (UINT1 *) (pRMepNode));
    pRMepNode = NULL;
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwUpdateRMepDbEntries
 *                                                                          
 *    DESCRIPTION      : This function Removes this node from all other MEPs
 *                       RMepDb, and add its remote MEP nodes
 *
 *    INPUT            : pMepNode - Pointer to MEP, for which RMepDb is to 
 *                       be updated.
 *                       
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwUpdateRMepDbEntries (tEcfmCcMepInfo * pMepNode)
{
    tEcfmCcMaInfo      *pMaInfo = NULL;
    tEcfmCcMepListInfo *pMepListNode = NULL;
    tEcfmCcMepListInfo  MepListNode;

    pMaInfo = ECFM_CC_GET_MAINFO_FROM_MEP (pMepNode);
    UNUSED_PARAM (pMaInfo);

    ECFM_MEMSET (&MepListNode, ECFM_INIT_VAL, ECFM_CC_MEP_LIST_SIZE);

    MepListNode.u4MdIndex = pMepNode->u4MdIndex;
    MepListNode.u4MaIndex = pMepNode->u4MaIndex;

    pMepListNode =
        RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE, &MepListNode, NULL);
    while ((pMepListNode != NULL)
           && (pMepListNode->u4MdIndex == pMepNode->u4MdIndex)
           && (pMepListNode->u4MaIndex == pMepNode->u4MaIndex))

    {
        if (pMepListNode->u2MepId != pMepNode->u2MepId)

        {

            /* Create and add Remote MEP node */
            EcfmCreateRMepEntryInMep (pMepNode, pMepListNode->u2MepId);
        }
        pMepListNode =
            RBTreeGetNext (ECFM_CC_MA_MEP_LIST_TABLE, pMepListNode, NULL);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmCreateRMepEntryInMep
 *                                                                          
 *    DESCRIPTION      : This function adds remote MEP entry in particular
 *                       MEP RMepDb
 *
 *    INPUT            : pMepNode - Pointer to MEP, in which remote mep is
 *                       to be created and added.
 *                       u2RMepId - Remote MEP identifier with which new node
 *                       is to be created.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EcfmCreateRMepEntryInMep (tEcfmCcMepInfo * pMepNode, UINT2 u2RMepId)
{
#ifdef NPAPI_WANTED
    UINT1               au1TempHwMepHandler[ECFM_HW_MEP_HANDLER_SIZE];
#endif
    tEcfmCcRMepDbInfo  *pRMepNewNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;

    /* Create remote MEP node */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MEP_DB_TABLE (pRMepNewNode) == NULL)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC,
                     "EcfmCreateRMepEntryInMep:"
                     "Allocation for Remote MEP Node Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return;
    }
#ifdef NPAPI_WANTED
    ECFM_MEMSET (au1TempHwMepHandler, ECFM_INIT_VAL, ECFM_HW_MEP_HANDLER_SIZE);
#endif
    ECFM_MEMSET (pRMepNewNode, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);
    /* Set SenderId  */
    ECFM_MEMSET (&(pRMepNewNode->LastSenderId), ECFM_INIT_VAL,
                 sizeof (tEcfmSenderId));
    /* Set the global ChassisIdSubType as senderId SubType */
    pRMepNewNode->LastSenderId.u1ChassisIdSubType =
        gEcfmCcGlobalInfo.u1ChassisIdSubType;

    /* Set the required indices for new remote MEP node */
    pRMepNewNode->u4MdIndex = pMepNode->u4MdIndex;
    pRMepNewNode->u4MaIndex = pMepNode->u4MaIndex;
    pRMepNewNode->u2MepId = pMepNode->u2MepId;
    pRMepNewNode->u2RMepId = u2RMepId;

    /* Put backward pointer to MEP node */
    pRMepNewNode->pMepInfo = pMepNode;
    ECFM_CC_RMEP_SET_STATE (pRMepNewNode, ECFM_RMEP_STATE_DEFAULT);

    /* Add remote MEP new node */
    /* Adding remote MEP new node in MEP */

    TMO_DLL_Add (&(pMepNode->RMepDb),
                 (tTMO_DLL_NODE *) & (pRMepNewNode->MepDbDllNode));

    /* Adding remote MEP new node to global */
    if (RBTreeAdd (ECFM_CC_RMEP_TABLE, pRMepNewNode) != ECFM_RB_SUCCESS)

    {

        /* Remove the node added in the global */
        TMO_DLL_Delete (&(pMepNode->RMepDb), &(pRMepNewNode->MepDbDllNode));

        /* Release memory allocated to new remote MEP node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MEP_DB_TABLE_POOL,
                             (UINT1 *) (pRMepNewNode));
        pRMepNewNode = NULL;
        return;
    }

    /* Add RMEP-DB at LBLT also */
    if (EcfmLbLtAddRMepDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pRMepNewNode) !=
        ECFM_SUCCESS)

    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCreateRMepEntryInMep:"
                     "Failure adding RMEP-DB at LBLT \r\n");
        return;
    }
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, ECFM_CC_PDUSM_INFO_SIZE);
    PduSmInfo.pMepInfo = pMepNode;
#ifdef NPAPI_WANTED
    if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
                     ECFM_HW_MEP_HANDLER_SIZE) != 0)
#endif
    {
        EcfmCcClntNotifyRmep (&PduSmInfo, ECFM_EV_MEP_NOT_ACTIVE);
    }

    if ((pMepNode->b1Active == ECFM_TRUE) &&
        (pMepNode->pPortInfo->u1PortEcfmStatus == ECFM_ENABLE))
    {
#ifdef NPAPI_WANTED
        if (ECFM_MEMCMP (pMepNode->au1HwMepHandler, au1TempHwMepHandler,
            ECFM_HW_MEP_HANDLER_SIZE) != 0)
#endif
        {
            EcfmCcClntNotifyRmep (&PduSmInfo, ECFM_EV_MEP_BEGIN);
        }
    }

    return;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwInitiateLtmTx
 * 
 * DESCRIPTION      : This Function checks if LTM transmission is to be 
 *                    initiated and send event to corresponding state machine
 *                    for its initiation. 
 *                    
 * INPUT            : u4MdIndex - MD Index 
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 *******************************************************************************/
PUBLIC INT4
EcfmSnmpLwInitiateLtmTx (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtLTInfo    *pLtInfo = NULL;
    tMacAddr            TempMacAddr;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* Get MEP entry from MepTableIndex, Global */
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)
    {
        return ECFM_FAILURE;
    }

    /* Check if LTM transmission is to be initiated */
    /* Get LTInfo from Mep */
    pLtInfo = ECFM_LBLT_GET_LTINFO_FROM_MEP (pLbLtMepNode);

    /* Check if MepId or MacAddress of target Mep 
     *  has been set*/
    if (pLtInfo->b1TxLtmTargetIsMepId == ECFM_TRUE)

    {
        if (pLtInfo->u2TxLtmTargetMepId == 0)

        {

            /* Required parameters are not set */
            return ECFM_FAILURE;
        }
    }

    else

    {

        /* Check for Destination Mac Address has been set or not */
        if (ECFM_MEMCMP
            (pLtInfo->TxLtmTargetMacAddr, TempMacAddr,
             ECFM_MAC_ADDR_LENGTH) == 0)

        {
            return ECFM_FAILURE;
        }
    }

    /* Check if TTL is wrong */
    if (ECFM_LBLT_GET_LTM_TTL_MSB (pLtInfo->u2TxLtmTtl))
    {
        return ECFM_FAILURE;
    }

    /* Check if Ltm flags value is wrong */
    if (ECFM_LBLT_GET_LTM_FLAGS_LSB (pLtInfo->u1TxLtmFlags))

    {
        return ECFM_FAILURE;
    }

    /* Send event for LTM transmission initiation */
    if (EcfmLbLtUtilPostTransaction
        (pLbLtMepNode, ECFM_LT_START_TRANSACTION) == ECFM_FAILURE)

    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwTestTstTxParam
 *
 * DESCRIPTION      : Function checks if Tst transmission paramaters are
 *                    configured properly.
 *
 * INPUT            : u4MdIndex - MD Index
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 *******************************************************************************/
PUBLIC              BOOL1
EcfmSnmpLwTestTstTxParam (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tMacAddr            TempMacAddr;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)
    {
        return ECFM_FAILURE;
    }
    pTstInfo = ECFM_LBLT_GET_TSTINFO_FROM_MEP (pLbLtMepNode);

    /* Check if required parameters are configured properly */
    /* Check if MepId or MacAddress of Target Mep or Multicast(in case of Y1731
     * only has been set*/
    if (pTstInfo->u1TstCapability != ECFM_ENABLE)
    {
        return ECFM_FALSE;
    }

    if (pTstInfo->u1TxTstDestType == ECFM_TX_DEST_TYPE_MEPID)

    {
        if (pTstInfo->u2TstDestMepId == 0)

        {

            /* Target MEP ID is not set has not set */
            return ECFM_FALSE;
        }
    }
    if (pTstInfo->u1TxTstDestType == ECFM_TX_DEST_TYPE_UNICAST)

    {

        /* Check for Destination Mac Address has been set or not */
        if ((ECFM_MEMCMP
             (pTstInfo->TstDestMacAddr, TempMacAddr,
              ECFM_MAC_ADDR_LENGTH) == 0)
            || (ECFM_IS_MULTICAST_ADDR (pTstInfo->TstDestMacAddr)))

        {

            /* Unicast Mac address has not been set */
            return ECFM_FALSE;
        }
    }

    /* Checks if DestType is Microsec and Interval value is less than 10 
       or if DestType is Sec and Interval value is more than 600 */
    switch (pTstInfo->u1TxTstIntervalType)

    {
        case ECFM_LBLT_TST_INTERVAL_SEC:
            if (pTstInfo->u4TstInterval > ECFM_TST_INTERVAL_SEC_MAX)

            {

                /* Interval value is not accordance with Lbm Interval type */
                return ECFM_FALSE;
            }
            break;
        case ECFM_LBLT_TST_INTERVAL_MSEC:

            /* Check system time granuality */
            if (pTstInfo->u4TstInterval < ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT)

            {
                return ECFM_FALSE;
            }
            break;
        case ECFM_LBLT_TST_INTERVAL_USEC:
            if (pTstInfo->u4TstInterval < ECFM_TST_INTERVAL_IN_USEC_MIN)

            {

                /* Interval value is not accordance with TST Interval type */
                return ECFM_FALSE;
            }
            if (ECFM_USEC_TIMER_SUPPORT () == ECFM_FALSE)

            {
                return ECFM_FALSE;
            }
            break;
        default:
            return ECFM_FALSE;
    }
    return ECFM_TRUE;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwTestLbmTxParam
 * 
 * DESCRIPTION      : Function checks if LBM transmission paramaters are
 *                    configured properly. 
 *                    
 * INPUT            : u4MdIndex - MD Index 
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 *******************************************************************************/
PUBLIC              BOOL1
EcfmSnmpLwTestLbmTxParam (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tMacAddr            TempMacAddr;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)
    {
        return ECFM_FAILURE;
    }
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pLbLtMepNode);

    /* Check if required parameters are configured properly */
    /* Check if MepId or MacAddress of Target Mep or Multicast(in case of Y1731
     * only has been set*/
    if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MEPID)

    {
        if (pLbInfo->u2TxDestMepId == 0)

        {

            /* Target MEP ID is not set has not set */
            return ECFM_FALSE;
        }
    }

    else if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_UNICAST)

    {

        /* Check for Destination Mac Address has been set or not */
        if ((ECFM_MEMCMP
             (pLbInfo->TxLbmDestMacAddr, TempMacAddr,
              ECFM_MAC_ADDR_LENGTH) == 0)
            || (ECFM_IS_MULTICAST_ADDR (pLbInfo->TxLbmDestMacAddr)))

        {

            /* Unicast Mac address has not been set */
            return ECFM_FALSE;
        }
    }

    /*checks if DestType is Microsec and Interval value is less than 10 
       or if DestType is Sec and Interval value is more than 600 */
    switch (pLbInfo->u1TxLbmIntervalType)

    {
        case ECFM_LBLT_LB_INTERVAL_SEC:
            if (pLbInfo->u4TxLbmInterval > ECFM_LB_INTERVAL_SEC_MAX)

            {

                /* Interval value is not accordance with Lbm Interval type */
                return ECFM_FALSE;
            }
            break;
        case ECFM_LBLT_LB_INTERVAL_MSEC:

            /* Check system time granuality */
            if (pLbInfo->u4TxLbmInterval < ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT)

            {
                return ECFM_FALSE;
            }
            break;
        case ECFM_LBLT_LB_INTERVAL_USEC:
            if (pLbInfo->u4TxLbmInterval < ECFM_LB_INTERVAL_IN_USEC_MIN)

            {

                /* Interval value is not accordance with Lbm Interval type */
                return ECFM_FALSE;
            }
            if (ECFM_USEC_TIMER_SUPPORT () == ECFM_FALSE)

            {
                return ECFM_FALSE;
            }
            break;
        default:
            return ECFM_FALSE;
    }
    return ECFM_TRUE;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwTestDmTxParam
 * 
 * DESCRIPTION      : Function checks if DM transmission paramaters are
 *                    configured properly. 
 *                    
 * INPUT            : u4MdIndex - MD Index 
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 *******************************************************************************/
PUBLIC              BOOL1
EcfmSnmpLwTestDmTxParam (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tMacAddr            TempMacAddr;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)
    {
        return ECFM_FAILURE;
    }
    pDmInfo = &(pLbLtMepNode->DmInfo);

    /* Check if required parameters are configured properly */
    /* Check if MacAddress of Target Mep has been set */
    if (pDmInfo->b1TxDmIsDestMepId == ECFM_TRUE)

    {
        if (pDmInfo->u2TxDmDestMepId == 0)

        {

            /* Target MEP ID is not set has not set */
            return ECFM_FALSE;
        }
    }

    else

    {

        /* Check for Destination Mac Address has been set or not */
        if (ECFM_MEMCMP
            (pDmInfo->TxDmDestMacAddr, TempMacAddr, ECFM_MAC_ADDR_LENGTH) == 0)

        {

            /* Unicast Mac address has not been set */
            return ECFM_FALSE;
        }
    }
    return ECFM_TRUE;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwTestThTxParam
 * 
 * DESCRIPTION      : Function checks if TH transmission paramaters are
 *                    configured properly. 
 *                    
 * INPUT            : u4MdIndex - MD Index 
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 *******************************************************************************/
PUBLIC              BOOL1
EcfmSnmpLwTestThTxParam (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tMacAddr            TempMacAddr;
    DBL8                d8ThInterval = ECFM_INIT_VAL;
    DBL8                d8ThDeadline = ECFM_INIT_VAL;
    DBL8                d8ThBurstDeadline = ECFM_INIT_VAL;

    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    pLbLtMepNode =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pLbLtMepNode == NULL)
    {
        return ECFM_FAILURE;
    }
    pThInfo = &(pLbLtMepNode->ThInfo);

    /* Check if required parameters are configured properly */
    /* Check if MacAddress of Target Mep has been set */
    if (pThInfo->b1TxThIsDestMepId == ECFM_TRUE)
    {
        if (pThInfo->u2TxThDestMepId == 0)
        {
            /* Target MEP ID is not set has not set */
            return ECFM_FALSE;
        }
    }
    else
    {
        /* Check for Destination Mac Address has been set or not */
        if (ECFM_MEMCMP (pThInfo->TxThDestMacAddr,
                         TempMacAddr, ECFM_MAC_ADDR_LENGTH) == 0)
        {
            /* Unicast Mac address has not been set */
            return ECFM_FALSE;
        }
    }

    d8ThInterval = (DBL8) (ECFM_NUM_OF_USEC_IN_A_SEC / pThInfo->u4TxThPps);    /*In microsec */
    d8ThDeadline = pThInfo->u4TxThDeadline * ECFM_NUM_OF_USEC_IN_A_SEC;    /*In microsec */
    d8ThBurstDeadline = pThInfo->u4TxThBurstDeadline * ECFM_NUM_OF_MSEC_IN_A_SEC;    /*In microsec */
    if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_MSGS)
    {
        if ((pThInfo->u2TxThMessages != 0) && (pThInfo->u4TxThDeadline != 0))
        {
            if ((pThInfo->u2TxThMessages * d8ThInterval) < (d8ThDeadline))
            {
                if (pThInfo->u2TxThMessages < pThInfo->u2TxThBurstMessages)
                {
                    return ECFM_FALSE;
                }
            }
            else
            {
                if ((pThInfo->u2TxThBurstMessages * d8ThInterval) >
                    d8ThDeadline)
                {
                    return ECFM_FALSE;
                }
            }

            return ECFM_TRUE;
        }
        if (pThInfo->u2TxThMessages != 0)
        {
            if (pThInfo->u2TxThMessages < pThInfo->u2TxThBurstMessages)
            {
                return ECFM_FALSE;
            }
        }
        if (pThInfo->u4TxThDeadline != 0)
        {
            if ((pThInfo->u2TxThBurstMessages * d8ThInterval) > d8ThDeadline)
            {
                return ECFM_FALSE;
            }
        }

    }
    else
    {
        if ((pThInfo->u2TxThMessages != 0) && (pThInfo->u4TxThDeadline != 0))
        {
            if ((pThInfo->u2TxThMessages * d8ThInterval) < (d8ThDeadline))
            {
                if ((pThInfo->u2TxThMessages * d8ThInterval) <
                    d8ThBurstDeadline)
                {
                    return ECFM_FALSE;
                }
            }
            else
            {
                if (d8ThBurstDeadline > d8ThDeadline)
                {
                    return ECFM_FALSE;
                }
            }
            return ECFM_TRUE;
        }
        if (pThInfo->u2TxThMessages != 0)
        {
            if ((pThInfo->u2TxThMessages * d8ThInterval) < d8ThBurstDeadline)
            {
                return ECFM_FALSE;
            }
        }
        if (pThInfo->u4TxThDeadline != 0)
        {
            if (d8ThBurstDeadline > d8ThDeadline)
            {
                return ECFM_FALSE;
            }
        }
    }

    return ECFM_TRUE;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwTestLmTxParam
 *
 * DESCRIPTION      : Function checks if LM transmission paramaters are
 *                    configured properly.
 *
 * INPUT            : u4MdIndex - MD Index
 *                    u4MaIndex - MA Index
 *                    u2MepId   - MepIdentifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_TRUE/ECFM_FALSE
 *
 *******************************************************************************/
PUBLIC              BOOL1
EcfmSnmpLwTestLmTxParam (UINT4 u4MdIndex, UINT4 u4MaIndex, UINT2 u2MepId)
{
    tEcfmCcMepInfo     *pCcMepNode = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    tMacAddr            TempMacAddr;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    pCcMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pCcMepNode == NULL)
    {
        return ECFM_FAILURE;
    }
    pLmInfo = &(pCcMepNode->LmInfo);

    /* Check if required parameters are configured properly */
    /* Check if MacAddress of Target Mep has been set */
    if (pLmInfo->b1TxLmIsDestMepId == ECFM_TRUE)

    {
        if (pLmInfo->u2TxLmDestMepId == 0)

        {

            /* Target MEP ID is not set has not set */
            return ECFM_FALSE;
        }
    }

    else

    {

        /* Check for Destination Mac Address has been set or not */
        if (ECFM_MEMCMP
            (pLmInfo->TxLmmDestMacAddr, TempMacAddr, ECFM_MAC_ADDR_LENGTH) == 0)

        {

            /* Unicast Mac address has not been set */
            return ECFM_FALSE;
        }
    }
    return ECFM_TRUE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmGetNextFrmLossNodeForATrans
 *
 *    DESCRIPTION      : This function returns the next Frame Loss buffer node
 *                       for same transaction.
 *
 *    INPUT            : pFlBufferNode - Pointer to first FrameLoss buffer
 *                       node for required Transaction
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : Pointer to next Frame Loss buffer node for the same
 *                       Transaction.
 *
 ****************************************************************************/
PUBLIC tEcfmCcFrmLossBuff *
EcfmGetNextFrmLossNodeForATrans (tEcfmCcFrmLossBuff * pFlBufferTransNode)
{
    tEcfmCcFrmLossBuff *pFlTempNode = NULL;

    /* Get next node in the FL Table */
    pFlTempNode = (tEcfmCcFrmLossBuff *)
        RBTreeGetNext (ECFM_CC_FL_BUFFER_TABLE,
                       (tRBElem *) pFlBufferTransNode, NULL);
    if (pFlTempNode == NULL)

    {
        return NULL;
    }

    /* Check if next node belongs to same transaction */
    if ((pFlTempNode->u4MdIndex == pFlBufferTransNode->u4MdIndex) &&
        (pFlTempNode->u4MaIndex == pFlBufferTransNode->u4MaIndex) &&
        (pFlTempNode->u2MepId == pFlBufferTransNode->u2MepId) &&
        (pFlTempNode->u4TransId == pFlBufferTransNode->u4TransId))

    {

        /* Same transaction node */
        return pFlTempNode;
    }

    /* Node does not belongs to same transaction */
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmAnyLowerLevelMep
 *                                                                          
 *    DESCRIPTION      : This function checks if any lower level MEP configured 
 *                       at specified port and vid. 
 *                       
 *
 *    INPUT            : u2Port   - Local Port Num, where lower level MEP
 *                                   needs to find.
 *                       u1MdLevel - Md Level from which lower level MEP needs
 *                                   to find.
 *                       u4VidIsid - Vlan Identifier  
 *    
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE.
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE             BOOL1
EcfmAnyLowerLevelMep (UINT2 u2Port, UINT4 u4VidIsid, UINT1 u1Level)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pPortNode = NULL;

    pPortNode = ECFM_CC_GET_PORT_INFO (u2Port);
    UNUSED_PARAM (pPortNode);
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    gpEcfmCcMepNode->u2PortNum = u2Port;
    gpEcfmCcMepNode->u4PrimaryVidIsid = u4VidIsid;
    pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, gpEcfmCcMepNode, NULL);

    while ((pMepNode != NULL) &&
           (pMepNode->u2PortNum == u2Port) &&
           (pMepNode->u4PrimaryVidIsid == u4VidIsid))
    {

        /* Check for any MEP at lower MD level */
        if (pMepNode->u1MdLevel < u1Level)

        {
            return ECFM_TRUE;
        }
        pMepNode = RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE, pMepNode, NULL);
    }
    return ECFM_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmValidateMipCreation 
 *                                                                          
 *    DESCRIPTION      : This function checks whether MIP can be configured
 *                       on specified port, with particular Mdlevel and 
 *                       VlanId based on MhfCreation criteria. 
 *                       
 *
 *    INPUT            : u4IfIndex - Interface index with which MIP needs to
 *                                   be configured.
 *                       u1MdLevel - MdLevel at which MIP needs to configured
 *                       u4VidIsid     - VlanId with which MIP needs to be
 *                                   associated.
 *                       pu4ErrMsg - Error Message to be returned for CLI
 *                       interface.            
 *    
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_TRUE/ECFM_FALSE
 *    
 *                                                                          
 ****************************************************************************/
PRIVATE             BOOL1
EcfmValidateMipCreation (UINT4 u4VidIsid, UINT4 u4IfIndex, UINT1 u1Level,
                         UINT4 *pu4ErrMsg)
{
    UINT1               u1MhfCreation = ECFM_INIT_VAL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;

    /* Get the DefaultMd entry corresponding to PrimaryVid */
    ECFM_CC_GET_DEFAULT_MD_ENTRY (u4VidIsid, pDefaultMdNode);

    /* NULL check */
    if (pDefaultMdNode == NULL)
    {
        *pu4ErrMsg = CLI_ECFM_MIP_CONFIG_ERR;
        return ECFM_FALSE;
    }

    /* check default md entry's mdlevel */
    if (((pDefaultMdNode->i1MdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL) &&
         (ECFM_CC_DEF_MD_DEFAULT_LEVEL == u1Level)) ||
        ((pDefaultMdNode->i1MdLevel != ECFM_DEF_MD_LEVEL_DEF_VAL) &&
         ((UINT1) (pDefaultMdNode->i1MdLevel) == u1Level)))

    {

        /* Check MIP should be created with MA or default MD parameters */
        if (pDefaultMdNode->b1Status != ECFM_FALSE)
        {
            /* MIP can be created with Default MD parameters only */
            u1MhfCreation = pDefaultMdNode->u1MhfCreation;

            /*check default md entry's mhfcreation */
            if (pDefaultMdNode->u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
            {
                u1MhfCreation = ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION;
            }
        }
        else
        {
            /* Get if there is any MA with vlanId u4VidIsid and level  u1Level */
            pMaNode = EcfmCcUtilGetMaAssocWithVid (u4VidIsid, u1Level);

            /* If Ma asscoiated with VlanId exists */
            if (pMaNode != NULL)
            {
                u1MhfCreation = pMaNode->u1MhfCreation;
                if (u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
                {
                    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);
                    u1MhfCreation = pMdNode->u1MhfCreation;
                }
            }
            else
            {
                *pu4ErrMsg = CLI_ECFM_MIP_CONFIG_ERR;
                return ECFM_FALSE;
            }
        }
    }
    else
    {
        /* Case if user has not configured MD Level corresponding to the
         * VLAN in the default MD entry */
        /* Check if there is any MA, eligible for MIP creation */
        /* Get if there is any MA with vlanId u4VidIsid and level  u1Level */
        pMaNode = EcfmCcUtilGetMaAssocWithVid (u4VidIsid, u1Level);
        if (pMaNode != NULL)
        {
            /* MA asscoiated with VlanId exists */
            if (EcfmIsMepAssocWithMa (pMaNode->u4MdIndex, pMaNode->u4MaIndex,
                                      -1, ECFM_MP_DIR_UP) == ECFM_FALSE)
            {
                /* UP MEP is not present for this MA on any of the VLAN's ports.
                 * Since, MA is present, refer the MHF Creation Criteria of the
                 * MA. If the criteria equals DEFER, then user has not specified
                 * on the MA's object MHF Creation Criteria. Hence refer 
                 * MD's MHF Creation Criteria. 
                 * Refer point (l) of IEEE 802.1ag/D8.1, Section 22.2.3
                 */
                u1MhfCreation = pMaNode->u1MhfCreation;

                if (u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
                {
                    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);
                    u1MhfCreation = pMdNode->u1MhfCreation;
                }
                else
                {
                    *pu4ErrMsg = CLI_ECFM_MIP_CONFIG_ERR;
                    return ECFM_FALSE;
                }
            }
            else
            {
                /* UP MEP for this MA is present on any of the ports, then the
                 * enumeration should be of MA managed object MHF creation
                 * criteria. Refer point (k) of IEEE 802.1ag/D8.1,
                 * Section 22.2.3
                 */
                u1MhfCreation = pMaNode->u1MhfCreation;
                if (u1MhfCreation == ECFM_MHF_CRITERIA_DEFER)
                {
                    pMdNode = ECFM_CC_GET_MDINFO_FROM_MA (pMaNode);
                    u1MhfCreation = pMdNode->u1MhfCreation;
                }

            }
        }
        else
        {
            *pu4ErrMsg = CLI_ECFM_MIP_CONFIG_ERR;
            return ECFM_FALSE;
        }
    }

    /* Check if MIP creation can be possible with Mhf creation value */
    switch (u1MhfCreation)
    {
        case ECFM_MHF_CRITERIA_DEFAULT:
            /* Section 22.2.3 Creating MIPs.
             * 2) defMHFdefault: MHFs can be created for this VID(s) on any Bridge Port through which the
             *  VID(s) can pass where:
             *  i) There are no lower active MD levels; or
             *  ii) There is a MEP at the next lower active MD-level on the port. 
             */
            return ECFM_TRUE;
        case ECFM_MHF_CRITERIA_EXPLICIT:

            /* MHFs can be created for this VID(s) only on Bridge Ports through which
             * this VID(s) can pass, and only if there is a MEP at the next lower 
             * active MD-level on the port.
             */

            /* Check if any lower level MEP at same VlanId at same ifIndex
             * exists, then only MIP can be created */
            if (EcfmAnyLowerLevelMep ((UINT2) u4IfIndex, u4VidIsid, u1Level) ==
                ECFM_TRUE)
            {
                return ECFM_TRUE;
            }
            else
            {
                *pu4ErrMsg =
                    (pMaNode ==
                     NULL) ? CLI_ECFM_MIP_CONFIG_MHFCRITERIA_EXPLICIT_DMD_ERR :
                    CLI_ECFM_MIP_CONFIG_MHFCRITERIA_EXPLICIT_MA_ERR;
                return ECFM_FALSE;
            }
        case ECFM_MHF_CRITERIA_NONE:
            *pu4ErrMsg =
                (pMaNode ==
                 NULL) ? CLI_ECFM_MIP_CONFIG_MHFCRITERIA_NONE_DMD_ERR :
                CLI_ECFM_MIP_CONFIG_MHFCRITERIA_NONE_MA_ERR;
            return ECFM_FALSE;
        default:
            *pu4ErrMsg = CLI_ECFM_MIP_CONFIG_MHFCRITERIA_INVALID_ERR;
            return ECFM_FALSE;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwIsInfoConfiguredForMip 
 *                                                                          
 *    DESCRIPTION      : This function checks whether MIP can be configured
 *                       on specified port, with particular Mdlevel and 
 *                       VlanId. 
 *                       
 *
 *    INPUT            : u4IfIndex - Interface index at which MIP needs to be
 *                                   configured
 *                       u1MdLevel - MdLevel at which MIP needs to be
 *                                   configured
 *                       u4VidIsid     - VlanId which MIP needs to be
 *                                   associated.
 *                       pu4ErrMsg - Error Message to be returned for CLI
 *                       interface.            
 *    
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwIsInfoConfiguredForMip (UINT4 u4IfIndex, UINT1 u1MdLevel,
                                  UINT4 u4VidIsid, UINT4 *pu4ErrMsg)
{
    tEcfmCcStackInfo   *pMpUpNode = NULL;
    tEcfmCcStackInfo   *pMpDownNode = NULL;
    tEcfmCcMipInfo     *pMipNode = NULL;
    UINT1               u1HighestMdLevel = ECFM_INIT_VAL;
    BOOL1               b1IsMep = ECFM_FALSE;

    /* Check for any MP at these indices */
    pMpUpNode =
        EcfmCcUtilGetMp ((UINT2) u4IfIndex, u1MdLevel, u4VidIsid,
                         ECFM_MP_DIR_UP);
    pMpDownNode =
        EcfmCcUtilGetMp ((UINT2) u4IfIndex, u1MdLevel, u4VidIsid,
                         ECFM_MP_DIR_DOWN);
    if ((pMpUpNode != NULL) || (pMpDownNode != NULL))

    {
        *pu4ErrMsg = CLI_ECFM_MIP_CONFIG_EXIST_ERR;
        return ECFM_FAILURE;
    }

    /* Get Highest MP level on IfIndex, u1MdLevel, and vid  */
    if (EcfmCcUtilGetHighestMpMdLevel (u4IfIndex, u4VidIsid, &u1HighestMdLevel,
                                       &b1IsMep) == ECFM_SUCCESS)
    {

        /* Check if any MP is  already configured at higher level */
        if (u1MdLevel > u1HighestMdLevel)

        {

            /* Validate Mip creation with MhfCreation value either 
             * from MA if it exists or from default Md Table  */
            if (EcfmValidateMipCreation
                (u4VidIsid, u4IfIndex, u1MdLevel, pu4ErrMsg) == ECFM_TRUE)

            {

                /* check entity configured on highest MD level on this port and vid
                 * is MIP */
                if (b1IsMep == ECFM_FALSE)

                {

                    /* Restrict the user from creating MIPs at this u1MdLevel as
                       another MIP exists at the u4IfIndex */
                    pMipNode =
                        EcfmCcUtilGetMipEntry ((UINT2) u4IfIndex,
                                               u1HighestMdLevel, u4VidIsid);
                    if (pMipNode != NULL)
                    {
                       *pu4ErrMsg = CLI_ECFM_LOW_MIP_EXIST_ERR;
                       return ECFM_FAILURE;
                    }
                }
                return ECFM_SUCCESS;
            }
            return ECFM_FAILURE;
        }

        else

        {
	    *pu4ErrMsg = CLI_ECFM_HIGH_MP_EXIST_ERR;
            return ECFM_FAILURE;
        }
    }

    else
        /* No entity exists at ifIndex with vid */
    {

        /* Validate Mip creation with MhfCreation value either
         * from MA if it exists or from  default Md Table  */
        if (EcfmValidateMipCreation
            (u4VidIsid, u4IfIndex, u1MdLevel, pu4ErrMsg) == ECFM_TRUE)

        {
            return ECFM_SUCCESS;
        }
        return ECFM_FAILURE;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwUpdateDefaultMdStatus 
 *                                                                          
 *    DESCRIPTION      : This function checks whether MIP can be configured
 *                       on specified port, with particular Mdlevel and 
 *                       VlanId. 
 *                       
 *
 *    INPUT            : u1MdLevel - MdLevel 
 *                       u2VlanId -  Vlan Identifier 
 *                                  Level and VlanId, corresponding to which,
 *                                  DefaultMdStatus is to be updated
 *                       b1Status - Status to be updated.
 *    
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwUpdateDefaultMdStatus (UINT1 u1MdLevel, UINT4 u4VlanIdIsid,
                                 BOOL1 b1Status)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    /* Get the DefaultMd entry corresponding to PrimaryVid */
    pDefaultMdNode = EcfmCcSnmpLwGetDefaultMdEntry (u4VlanIdIsid);

    /* Set status if level matches with MA,s level */
    /* Check if default MD level is to checked */
    if (pDefaultMdNode == NULL)
    {
        if (b1Status == ECFM_TRUE)
        {
            return;
        }
        /* Add a new entry in the table */
        if ((pDefaultMdNode = EcfmSnmpLwSetDefaultMdNode (u4VlanIdIsid))
            == NULL)
        {
            return;
        }
    }
    if (pDefaultMdNode->i1MdLevel != -1)

    {
        if ((UINT1) pDefaultMdNode->i1MdLevel == u1MdLevel)

        {
            pDefaultMdNode->b1Status = b1Status;
        }
    }

    else

    {

        /* Check if scalar MdLevel matched */
        if (ECFM_CC_DEF_MD_DEFAULT_LEVEL == u1MdLevel)

        {
            pDefaultMdNode->b1Status = b1Status;
        }
    }

    /* Update Default MD at LBLT also */
    EcfmLbLtUpdateDefaultMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pDefaultMdNode);
    /* Process the default MD node */
    EcfmSnmpLwProcessDefaultMdNode (u4VlanIdIsid);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwDelMepLtmReplyList 
 *                                                                          
 *    DESCRIPTION      : This function removes the LTM reply List entries
 *                       after the LTRs are cleared
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *  RETURNS            : None
 * **************************************************************************/
PUBLIC VOID
EcfmSnmpLwDelMepLtmReplyList ()
{
    tEcfmLbLtLtmReplyListInfo *pLtmNode = NULL;
    tEcfmLbLtLtmReplyListInfo *pLtmTempNode = NULL;
    pLtmNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGetFirst
        (ECFM_LBLT_LTM_REPLY_LIST);

    /* Scanning each LTM node in MEP */
    while (pLtmNode != NULL)
    {
        pLtmTempNode = pLtmNode;
        RBTreeRem (ECFM_LBLT_LTM_REPLY_LIST, (tRBElem *) pLtmNode);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL,
                             (UINT1 *) pLtmNode);
        pLtmNode = NULL;

        /* Next LTM node */
        pLtmNode = (tEcfmLbLtLtmReplyListInfo *) RBTreeGetNext
            (ECFM_LBLT_LTM_REPLY_LIST, pLtmTempNode, NULL);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetErrorLogEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the CcError entry
 *                       from the global structure, CcErrorLogTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u4SeqNum  - Sequence number of the error log.
 *                       occured/Cleared                                                   
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to CcErrorLog Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmCcErrLogInfo *
EcfmSnmpLwGetErrorLogEntry (UINT4 u4MdIndex,
                            UINT4 u4MaIndex, UINT2 u2MepId, UINT4 u4SeqNum)
{
    tEcfmCcErrLogInfo   CcErrLog;
    tEcfmCcErrLogInfo  *pCcErrLogNode = NULL;
    ECFM_MEMSET (&CcErrLog, ECFM_INIT_VAL, ECFM_CC_ERR_LOG_INFO_SIZE);

    /* Get CcErrLog entry corresponding to indices - 
     * MegIndex, MeIndex, MepId, CcErrorTimeStamp */
    CcErrLog.u4MdIndex = u4MdIndex;
    CcErrLog.u4MaIndex = u4MaIndex;
    CcErrLog.u2MepId = u2MepId;
    CcErrLog.u4SeqNum = u4SeqNum;
    pCcErrLogNode = RBTreeGet (ECFM_CC_ERR_LOG_TABLE, (tRBElem *) & CcErrLog);
    return pCcErrLogNode;
}

/* LBM table */
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetLbmEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the LTR entry
 *                       from the global structure, LtrTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u4TransId  - Transaction Id
 *                       u4SeqNumber - Sequence Number                        
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Lbm Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtLbmInfo *
EcfmSnmpLwGetLbmEntry (UINT4 u4MdIndex,
                       UINT4 u4MaIndex,
                       UINT2 u2MepId, UINT4 u4TransId, UINT4 u4SeqNum)
{
    tEcfmLbLtLbmInfo    LbmInfo;
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    ECFM_MEMSET (&LbmInfo, ECFM_INIT_VAL, ECFM_LBLT_LBM_INFO_SIZE);

    /* Get LBINIT entry corresponding to indices - 
     * MegIndex, MeIndex, MepId, u4TransId, u4SeqNumber */
    LbmInfo.u4MdIndex = u4MdIndex;
    LbmInfo.u4MaIndex = u4MaIndex;
    LbmInfo.u2MepId = u2MepId;
    LbmInfo.u4TransId = u4TransId;
    LbmInfo.u4SeqNum = u4SeqNum;
    pLbmNode = RBTreeGet (ECFM_LBLT_LBM_TABLE, (tRBElem *) & LbmInfo);
    return pLbmNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetLbrEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the LTR entry
 *                       from the global structure, LtrTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u4TransId  - Transaction Id
 *                       u4SeqNumber - Sequence Number                        
 *                       u4ReceiveOrder
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Lbm Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtLbrInfo *
EcfmSnmpLwGetLbrEntry (UINT4 u4MdIndex,
                       UINT4 u4MaIndex,
                       UINT2 u2MepId,
                       UINT4 u4TransId, UINT4 u4SeqNum, UINT4 u4ReceiveOrder)
{
    tEcfmLbLtLbmInfo    LbmInfo;
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;

    ECFM_MEMSET (&LbmInfo, ECFM_INIT_VAL, ECFM_LBLT_LBM_INFO_SIZE);

    /* Get LBR entry corresponding to indices - 
     * MegIndex, MeIndex, MepId, u4TransId, u4SeqNumber and u4ReceiveOrder */
    LbmInfo.u4MdIndex = u4MdIndex;
    LbmInfo.u4MaIndex = u4MaIndex;
    LbmInfo.u2MepId = u2MepId;
    LbmInfo.u4TransId = u4TransId;
    LbmInfo.u4SeqNum = u4SeqNum;

    pLbmNode = RBTreeGet (ECFM_LBLT_LBM_TABLE, (tRBElem *) & LbmInfo);

    if (pLbmNode == NULL)
    {
        return NULL;
    }

    pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_First (&(pLbmNode->LbrList));

    while (pLbrNode != NULL)
    {
        if (pLbrNode->u4RcvOrder == u4ReceiveOrder)
        {
            return pLbrNode;
        }

        /* Get the next LBR
         * entry */
        pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_Next
            (&(pLbmNode->LbrList), &pLbrNode->LbrTableSllNode);
    }

    return NULL;
}

/* Frame delay Buffer*/
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetFrmDelayEntry
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the LTR entry
 *                       from the global structure, LtrTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain index
 *                       u4MaIndex - Maintenance Association index
 *                       u2MepId   - Mep Identifier
 *                       u4TransId  - Transaction Id
 *                       u4SeqNumber - Sequence Number                        
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Lbm Entry
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtFrmDelayBuff *
EcfmSnmpLwGetFrmDelayEntry (UINT4 u4MdIndex,
                            UINT4 u4MaIndex,
                            UINT2 u2MepId, UINT4 u4TransId, UINT4 u4SeqNum)
{
    tEcfmLbLtFrmDelayBuff FrmDelayBuffNode;
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;
    ECFM_MEMSET (&FrmDelayBuffNode, ECFM_INIT_VAL,
                 ECFM_LBLT_FRM_DELAY_INFO_SIZE);

    /* Get FD entry corresponding to indices - 
     * MegIndex, MeIndex, MepId, u4TransId, u4SeqNumber */
    FrmDelayBuffNode.u4MdIndex = u4MdIndex;
    FrmDelayBuffNode.u4MaIndex = u4MaIndex;
    FrmDelayBuffNode.u2MepId = u2MepId;
    FrmDelayBuffNode.u4TransId = u4TransId;
    FrmDelayBuffNode.u4SeqNum = u4SeqNum;
    pFrmDelayBuffNode =
        RBTreeGet (ECFM_LBLT_FD_BUFFER_TABLE, (tRBElem *) & FrmDelayBuffNode);
    return pFrmDelayBuffNode;
}

/* LoopBack Stats */
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetLbTransEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Lb Init 
 *                       entry corresponding to MdIndex, MaIndex, MepId,
 *                       LbmTransId from the global structure, 
 *                       LbmTable.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index
 *                       u4MaIndex - Maintenance Association Index
 *                       u2MepId - Mep Identifier
 *                       u4TransId - Lbm Transaction identifier
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to a Lbm Table Entry
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtLbmInfo *
EcfmSnmpLwGetLbTransEntry (UINT4 u4MdIndex,
                           UINT4 u4MaIndex, UINT2 u2MepId, UINT4 u4TransId)
{
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    pLbmNode = (tEcfmLbLtLbmInfo *) RBTreeGetFirst (ECFM_LBLT_LBM_TABLE);
    while (pLbmNode != NULL)

    {

        /* Get entry corresponding to MdIndex, MaIndex, MepId, LbmTransId */
        if ((pLbmNode->u4MdIndex == u4MdIndex) &&
            (pLbmNode->u4MaIndex == u4MaIndex) &&
            (pLbmNode->u2MepId == u2MepId) &&
            (pLbmNode->u4TransId == u4TransId))

        {
            return pLbmNode;
        }
        pLbmNode = RBTreeGetNext (ECFM_LBLT_LBM_TABLE,
                                  (tRBElem *) pLbmNode, NULL);
    }
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmGetNextLbmNodeForATrans 
 *                                                                          
 *    DESCRIPTION      : This function returns the next Lbm node  
 *                       for same transaction.
 *
 *    INPUT            : pLbmNode - Pointer to first Lbm node for 
 *                       required Transaction
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : Pointer to next Lbm node for the same Transaction.
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtLbmInfo *
EcfmGetNextLbmNodeForATrans (tEcfmLbLtLbmInfo * pLbmNode)
{
    tEcfmLbLtLbmInfo   *pLbmTempNode = NULL;

    /* Get next node in the LB Init Table */
    pLbmTempNode = (tEcfmLbLtLbmInfo *)
        RBTreeGetNext (ECFM_LBLT_LBM_TABLE, (tRBElem *) pLbmNode, NULL);
    if (pLbmTempNode == NULL)

    {
        return NULL;
    }

    /* Check if next node belongs to same transaction */
    if ((pLbmTempNode->u4MdIndex == pLbmNode->u4MdIndex) &&
        (pLbmTempNode->u4MaIndex == pLbmNode->u4MaIndex) &&
        (pLbmTempNode->u2MepId == pLbmNode->u2MepId) &&
        (pLbmTempNode->u4TransId == pLbmNode->u4TransId))

    {

        /* Same transaction node */
        return pLbmTempNode;
    }

    /* Node does not belongs to same transaction */
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetLbTransactionStats 
 *                                                                          
 *    DESCRIPTION      : This function calculate and return the LB transaction
 *                       statistics.
 *
 *    INPUT            : pLbmFirstNode - Pointer to first Lbm node for 
 *                       required Transaction
 *                                 
 *    OUTPUT           : pu4LbrIn - Total number of LBRs recieved for this 
 *                                  transaction
 *                       pi4LbrAverageRcvTime - Average of a LBR time to be
 *                                              received   
 *                       pi4LbrMinRcvTime - Minimum of  LBRs time to be
 *                                          received
 *                       pi4LbrMaxRcvTime - Maximum of  LBRs time to be
 *                       received
 *                       pu4LbmTotalResponders - Total number of LB Responders
 *                       pu4AvgLbrsPerResponder - Average number of LBRs per
 *                       Responder
 *                                                                          
 *    RETURNS          : Pointer to next Lbm node for the same Transaction.
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwGetLbTransactionStats (tEcfmLbLtLbmInfo * pLbmFirstNode,
                                 UINT4 *pu4LbrIn, INT4 *pi4LbrAverageRcvTime,
                                 INT4 *pi4LbrMinRcvTime,
                                 INT4 *pi4LbrMaxRcvTime,
                                 UINT4 *pu4LbmTotalResponders,
                                 UINT4 *pu4AvgLbrsPerResponder)
{
    tMacAddr            aSavedRespMacAddr[ECFM_MAX_RMEP_IN_MA];
    tEcfmLbLtLbmInfo   *pLbmTempNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrTempNode = NULL;
    UINT4               u4LbrIn = ECFM_INIT_VAL;
    UINT4               u4Count = ECFM_INIT_VAL;
    INT4                i4AvgTotalTime = ECFM_INIT_VAL;
    INT4                i4RetMinTime = ECFM_INIT_VAL;
    INT4                i4RetMaxTime = ECFM_INIT_VAL;
    INT4                i4NextMinTime = ECFM_INIT_VAL;
    INT4                i4NextMaxTime = ECFM_INIT_VAL;
    UINT4               u4TotalResponders = ECFM_INIT_VAL;
    UINT4               u4RespCount = ECFM_INIT_VAL;
    UINT4               u4Temp = ECFM_INIT_VAL;
    BOOL1               b1EntryFound = ECFM_FALSE;

    pLbmTempNode = pLbmFirstNode;

    while (pLbmTempNode != NULL)
    {
        pLbrTempNode =
            (tEcfmLbLtLbrInfo *) TMO_SLL_First (&(pLbmTempNode->LbrList));
        if (pLbrTempNode == NULL)
        {

            /* Move to next LBM sequence number */
            pLbmTempNode = EcfmGetNextLbmNodeForATrans (pLbmTempNode);
            continue;
        }

        u4Count = u4Count + ECFM_INCR_VAL;

        /* Initialise variable for calculation of Min and Max time */
        if (u4Count == 1)
        {
            i4RetMinTime = (INT4) pLbrTempNode->u4LbrRcvTime;
            i4RetMaxTime = (INT4) pLbrTempNode->u4LbrRcvTime;
        }
        else
        {
            i4RetMinTime = i4NextMinTime;
            i4RetMaxTime = i4NextMaxTime;
        }

        /* Scan LBR List for this sequence number */
        while (pLbrTempNode != NULL)
        {

            /* Calculate the required stats */
            u4LbrIn = u4LbrIn + ECFM_INCR_VAL;
            i4AvgTotalTime = i4AvgTotalTime + (INT4) pLbrTempNode->u4LbrRcvTime;

            if (pLbrTempNode->u4LbrRcvTime < (UINT4) i4RetMinTime)
            {
                i4RetMinTime = (INT4) pLbrTempNode->u4LbrRcvTime;
            }

            if (pLbrTempNode->u4LbrRcvTime > (UINT4) i4RetMaxTime)
            {
                i4RetMaxTime = (INT4) pLbrTempNode->u4LbrRcvTime;
            }

            /* For First LBR entry */
            if (u4TotalResponders == 0)
            {
                ECFM_MEMCPY (aSavedRespMacAddr[u4TotalResponders],
                             pLbrTempNode->LbrSrcMacAddr, ECFM_MAC_ADDR_LENGTH);
                u4TotalResponders = u4TotalResponders + ECFM_INCR_VAL;
            }
            /* If more than one LBR is received for the same sequence number,
             * then count the no. of responders */
            else
            {
                u4RespCount = u4TotalResponders;

                for (u4Temp = 0; u4Temp < u4RespCount;
                     u4Temp = u4Temp + ECFM_INCR_VAL)
                {
                    if (ECFM_MEMCMP
                        (pLbrTempNode->LbrSrcMacAddr,
                         aSavedRespMacAddr[u4Temp], ECFM_MAC_ADDR_LENGTH) == 0)
                    {
                        b1EntryFound = ECFM_TRUE;
                        break;
                    }
                }

                if (b1EntryFound != ECFM_TRUE)
                {
                    if (u4TotalResponders < ECFM_MAX_RMEP_IN_MA)
                    {
                        ECFM_MEMCPY (aSavedRespMacAddr[u4TotalResponders],
                                     pLbrTempNode->LbrSrcMacAddr,
                                     ECFM_MAC_ADDR_LENGTH);
                        u4TotalResponders = u4TotalResponders + ECFM_INCR_VAL;
                    }
                }
                else
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmSnmpLwGetLbTransactionStats:"
                                   "Total number of responders exceeded the"
                                   " allowed limit. \r\n");
                }
            }

            pLbrTempNode = (tEcfmLbLtLbrInfo *) TMO_SLL_Next
                (&(pLbmTempNode->LbrList), &pLbrTempNode->LbrTableSllNode);
        }
        /* Update for next LBM sequence number */
        i4NextMinTime = i4RetMinTime;
        i4NextMaxTime = i4RetMaxTime;

        /* Move to next LBM sequence number */
        pLbmTempNode = EcfmGetNextLbmNodeForATrans (pLbmTempNode);
    }
    /* Calculate and update required output variables */
    if (pu4LbrIn != NULL)
    {
        *pu4LbrIn = u4LbrIn;
    }

    if (pi4LbrAverageRcvTime != NULL)
    {
        if (u4LbrIn != ECFM_INIT_VAL)
        {
            *pi4LbrAverageRcvTime = (i4AvgTotalTime / (INT4) u4LbrIn);
        }
    }

    if (pi4LbrMinRcvTime != NULL)
    {
        *pi4LbrMinRcvTime = i4RetMinTime;
    }

    if (pi4LbrMaxRcvTime != NULL)
    {
        *pi4LbrMaxRcvTime = i4RetMaxTime;
    }

    if (pu4LbmTotalResponders != NULL)
    {
        *pu4LbmTotalResponders = u4TotalResponders;
    }

    if (pu4AvgLbrsPerResponder != NULL)
    {
        if (u4TotalResponders != ECFM_INIT_VAL)
        {
            *pu4AvgLbrsPerResponder =
                (INT4) (u4LbrIn / (INT4) u4TotalResponders);
        }
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetNextIndexLbStatsTbl 
 *                                                                          
 *    DESCRIPTION      : This function returns the next Loob back stats
 *                       indices.
 *
 *    INPUT            : u4MdIndex - Md Index (MEG Index)
 *                       u4MaIndex - Ma Index (ME Index)
 *                       u4MepIdentifier - MepIdentifier
 *                       u4LbTransId - Transaction Identifier
 *                       
 *                                 
 *    OUTPUT           : pu4NextMdIndex - Next Md Index
 *                       pu4NextMaIndex - Next Ma Index 
 *                       pu4NextMepIdentifier - Next Mep Identifier
 *                       pu4NextLbTransId - Next Transaction Identifier
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwGetNextIndexLbStatsTbl (UINT4 u4MdIndex, UINT4 *pu4NextMdIndex,
                                  UINT4 u4MaIndex, UINT4 *pu4NextMaIndex,
                                  UINT4 u4MepIdentifier,
                                  UINT4 *pu4NextMepIdentifier,
                                  UINT4 u4LbTransId, UINT4 *pu4NextLbTransId)
{
    tEcfmLbLtLbmInfo   *pLbmTempNode = NULL;

    /* Check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        return ECFM_FAILURE;
    }

    /* Get Stats entry corresponding to indices  next to MdIndex, MaIndex, MepId
     * TransId */
    pLbmTempNode = (tEcfmLbLtLbmInfo *) RBTreeGetFirst (ECFM_LBLT_LBM_TABLE);

    /* Traverse the LB init table for the next transaction indices */
    while (pLbmTempNode != NULL)

    {
        if ((pLbmTempNode->u4MdIndex > u4MdIndex) ||
            (pLbmTempNode->u4MaIndex > u4MaIndex) ||
            (pLbmTempNode->u2MepId > (UINT2) u4MepIdentifier))

        {

            /* Required node */
            *pu4NextMdIndex = pLbmTempNode->u4MdIndex;
            *pu4NextMaIndex = pLbmTempNode->u4MaIndex;
            *pu4NextMepIdentifier = (UINT4) (pLbmTempNode->u2MepId);
            *pu4NextLbTransId = pLbmTempNode->u4TransId;
            return ECFM_SUCCESS;
        }
        if ((pLbmTempNode->u4MdIndex >= u4MdIndex) &&
            (pLbmTempNode->u4MaIndex >= u4MaIndex) &&
            (pLbmTempNode->u2MepId >= (UINT2) u4MepIdentifier) &&
            (pLbmTempNode->u4TransId > u4LbTransId))

        {

            /* Required node */
            *pu4NextMdIndex = pLbmTempNode->u4MdIndex;
            *pu4NextMaIndex = pLbmTempNode->u4MaIndex;
            *pu4NextMepIdentifier = (UINT4) (pLbmTempNode->u2MepId);
            *pu4NextLbTransId = pLbmTempNode->u4TransId;
            return ECFM_SUCCESS;
        }

        /* Skip the same transaction and differet sequence no node */
        if ((pLbmTempNode->u4MdIndex == u4MdIndex) &&
            (pLbmTempNode->u4MaIndex == u4MaIndex) &&
            (pLbmTempNode->u2MepId == (UINT2) u4MepIdentifier) &&
            (pLbmTempNode->u4TransId == u4LbTransId))

        {

            /* Move to next node */
            pLbmTempNode = (tEcfmLbLtLbmInfo *) RBTreeGetNext
                (ECFM_LBLT_LBM_TABLE, (tRBElem *) pLbmTempNode, NULL);
            continue;
        }

        /* Skip the node having TransId less than the required one */
        if ((pLbmTempNode->u4MdIndex >= u4MdIndex) &&
            (pLbmTempNode->u4MaIndex >= u4MaIndex) &&
            (pLbmTempNode->u2MepId >= (UINT2) u4MepIdentifier) &&
            (pLbmTempNode->u4TransId <= u4LbTransId))

        {

            /* Move to next node */
            pLbmTempNode = (tEcfmLbLtLbmInfo *) RBTreeGetNext
                (ECFM_LBLT_LBM_TABLE, (tRBElem *) pLbmTempNode, NULL);
            continue;
        }

        /* Move to next node */
        pLbmTempNode = (tEcfmLbLtLbmInfo *) RBTreeGetNext
            (ECFM_LBLT_LBM_TABLE, (tRBElem *) pLbmTempNode, NULL);
    } return ECFM_FAILURE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetFirstIndexLbStatsTbl 
 *                                                                          
 *    DESCRIPTION      : This function returns the first Loopback Stats 
 *                       Entry indices.
 *
 *    INPUT            : None
 *                                 
 *    OUTPUT           : pu4MdIndex - Firstt Md Index
 *                       pu4MaIndex - First Ma Index 
 *                       pu4MepIdentifier - First Mep Identifier
 *                       pu4LbTransId - First Transaction Identifier
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwGetFirstIndexLbStatsTbl (UINT4 *pu4MdIndex, UINT4 *pu4MaIndex,
                                   UINT4 *pu4MepIdentifier, UINT4 *pu4LbTransId)
{
    return (EcfmSnmpLwGetNextIndexLbStatsTbl
            (0, pu4MdIndex, 0, pu4MaIndex, 0, pu4MepIdentifier, 0,
             pu4LbTransId));
}

/* Frame Delay Stats */
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetFrmDelayTransEntry 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the FD Buffer 
 *                       entry corresponding to MdIndex, MaIndex, MepId,
 *                       FdTransId from the global structure, 
 *                       FrmDelayBuffer.
 *
 *    INPUT            : u4MdIndex - Maintenance Domain Index
 *                       u4MaIndex - Maintenance Association Index
 *                       u2MepId - Mep Identifier
 *                       u4FdTransId - Lbm Transaction identifier
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to a Fd Buffer Table Entry
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtFrmDelayBuff *
EcfmSnmpLwGetFrmDelayTransEntry (UINT4 u4MdIndex, UINT4 u4MaIndex,
                                 UINT2 u2MepId, UINT4 u4FdTransId)
{
    tEcfmLbLtFrmDelayBuff *pFdBufferNode = NULL;
    pFdBufferNode = (tEcfmLbLtFrmDelayBuff *)
        RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);
    while (pFdBufferNode != NULL)

    {

        /* Get entry corresponding to MdIndex, MaIndex, MepId, FdTransId */
        if ((pFdBufferNode->u4MdIndex == u4MdIndex) &&
            (pFdBufferNode->u4MaIndex == u4MaIndex) &&
            (pFdBufferNode->u2MepId == u2MepId) &&
            (pFdBufferNode->u4TransId == u4FdTransId))

        {
            return pFdBufferNode;
        }
        pFdBufferNode = RBTreeGetNext (ECFM_LBLT_FD_BUFFER_TABLE,
                                       (tRBElem *) pFdBufferNode, NULL);
    }
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmGetNextFrmDelayNodeForATrans 
 *                                                                          
 *    DESCRIPTION      : This function returns the next Frame Delay buffer node  
 *                       for same transaction.
 *
 *    INPUT            : pFdBufferNode - Pointer to first FrameDelay buffer
 *                       node for required Transaction
 *                                 
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : Pointer to next Frame Delay buffer node for the same
 *                       Transaction.
 *                                                                          
 ****************************************************************************/
PUBLIC tEcfmLbLtFrmDelayBuff *
EcfmGetNextFrmDelayNodeForATrans (tEcfmLbLtFrmDelayBuff * pFdBufferTransNode)
{
    tEcfmLbLtFrmDelayBuff *pFdTempNode = NULL;

    /* Get next node in the LB Init Table */
    pFdTempNode = (tEcfmLbLtFrmDelayBuff *)
        RBTreeGetNext (ECFM_LBLT_FD_BUFFER_TABLE,
                       (tRBElem *) pFdBufferTransNode, NULL);
    if (pFdTempNode == NULL)

    {
        return NULL;
    }

    /* Check if next node belongs to same transaction */
    if ((pFdTempNode->u4MdIndex == pFdBufferTransNode->u4MdIndex) &&
        (pFdTempNode->u4MaIndex == pFdBufferTransNode->u4MaIndex) &&
        (pFdTempNode->u2MepId == pFdBufferTransNode->u2MepId) &&
        (pFdTempNode->u4TransId == pFdBufferTransNode->u4TransId))

    {

        /* Same transaction node */
        return pFdTempNode;
    }

    /* Node does not belongs to same transaction */
    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetDmTransactionStats 
 *                                                                          
 *    DESCRIPTION      : This function calculates and returns the Fd transaction
 *                       statistics.
 *
 *    INPUT            : pFdBufferFirstNode - Frame Delay buffer node for the
 *                       same Transaction
 *                                 
 *    OUTPUT           : pu4DmrIn        - Total number of DMRs recieved for
 *                                         this transaction
 *                       pFdDelayAverage - Average of a calculated Delay 
 *                       pFdFDVAverage - Average of a calculated FDV (Delay
 *                                        Variation)
 *                       pFdIFDVAverage - Average of a calculated IFDV (Delay
 *                                        Variation)
 *                       pFdDelayMin     - Minimum Delay Value
 *                       pFdDelayMax     - Maximum Delay Value
 *                                                                          
 *    RETURNS          : Pointer to Frame Delay buffer node for the
 *                       same Transaction.
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmSnmpLwGetDmTransactionStats (tEcfmLbLtFrmDelayBuff * pFdBufferFirstNode,
                                 UINT4 *pu4DmrIn,
                                 tSNMP_OCTET_STRING_TYPE * pFdDelayAverage,
                                 tSNMP_OCTET_STRING_TYPE * pFdFDVAverage,
                                 tSNMP_OCTET_STRING_TYPE * pFdIFDVAverage,
                                 tSNMP_OCTET_STRING_TYPE * pFdDelayMin,
                                 tSNMP_OCTET_STRING_TYPE * pFdDelayMax)
{
    UINT4               u4DmrIn = ECFM_INIT_VAL;
    UINT4               u4TempNanoSeconds = ECFM_INIT_VAL;
    UINT4               u4DelayMin = ECFM_INIT_VAL;
    UINT4               u4DelayMax = ECFM_INIT_VAL;
    UINT4               u4Count = ECFM_INIT_VAL;
    UINT1              *pu1DelayVal = NULL;
    UINT1              *pu1FDVVal = NULL;
    UINT1              *pu1IFDVVal = NULL;
    UINT1              *pu1DelayMin = NULL;
    UINT1              *pu1DelayMax = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffTempNode = NULL;
    FS_UINT8            u8Temp1;
    FS_UINT8            u8TotalDelaySeconds;
    FS_UINT8            u8TotalDelayNanoSeconds;
    FS_UINT8            u8Temp1Sec;
    FS_UINT8            u8Temp2Sec;
    FS_UINT8            u8Temp1NanoSec;
    FS_UINT8            u8Temp2NanoSec;
    FS_UINT8            u8PktCnt;
    FS_UINT8            u8TotalFDVSeconds;
    FS_UINT8            u8TotalFDVNanoSeconds;
    FS_UINT8            u8RetFDVSeconds;
    FS_UINT8            u8RetFDVNanoSeconds;
    FS_UINT8            u8Seconds;
    FS_UINT8            u8NanoSeconds;
    FS_UINT8            u8TotalIFDVSeconds;
    FS_UINT8            u8TotalIFDVNanoSeconds;
    FS_UINT8            u8RetIFDVSeconds;
    FS_UINT8            u8RetIFDVNanoSeconds;
    FSAP_U8_CLR (&u8Temp1);
    FSAP_U8_CLR (&u8TotalDelaySeconds);
    FSAP_U8_CLR (&u8TotalDelayNanoSeconds);
    FSAP_U8_CLR (&u8Temp1Sec);
    FSAP_U8_CLR (&u8Temp2Sec);
    FSAP_U8_CLR (&u8Temp1NanoSec);
    FSAP_U8_CLR (&u8Temp2NanoSec);
    FSAP_U8_CLR (&u8TotalFDVSeconds);
    FSAP_U8_CLR (&u8TotalFDVNanoSeconds);
    FSAP_U8_CLR (&u8RetFDVSeconds);
    FSAP_U8_CLR (&u8RetFDVNanoSeconds);
    FSAP_U8_CLR (&u8Seconds);
    FSAP_U8_CLR (&u8NanoSeconds);
    FSAP_U8_CLR (&u8TotalIFDVSeconds);
    FSAP_U8_CLR (&u8TotalIFDVNanoSeconds);
    FSAP_U8_CLR (&u8RetIFDVSeconds);
    FSAP_U8_CLR (&u8RetIFDVNanoSeconds);

    pFrmDelayBuffTempNode = pFdBufferFirstNode;

    while (pFrmDelayBuffTempNode != NULL)

    {
        if (pFrmDelayBuffTempNode->MeasurementTimeStamp != 0)

        {
            u4DmrIn = u4DmrIn + ECFM_INCR_VAL;

            /* Get the delay in nano seconds and seconds from a string */
            /* Add seconds first */

            FSAP_U8_ASSIGN_LO (&u8Temp1Sec,
                               pFrmDelayBuffTempNode->FrmDelayValue.u4Seconds);
            FSAP_U8_ADD (&u8TotalDelaySeconds, &u8Temp1Sec,
                         &u8TotalDelaySeconds);
            FSAP_U8_CLR (&u8Temp1Sec);

            /* Next add nano seconds */
            FSAP_U8_ASSIGN_LO (&u8Temp1NanoSec,
                               pFrmDelayBuffTempNode->FrmDelayValue.
                               u4NanoSeconds);
            FSAP_U8_ADD (&u8TotalDelayNanoSeconds, &u8Temp1NanoSec,
                         &u8TotalDelayNanoSeconds);
            FSAP_U8_CLR (&u8Temp1NanoSec);

            /* Get the FDV in nano seconds and seconds from a string */
            FSAP_U8_ASSIGN_LO (&u8Temp1Sec,
                               pFrmDelayBuffTempNode->FrameDelayVariation.
                               u4Seconds);
            FSAP_U8_ADD (&u8TotalFDVSeconds, &u8Temp1Sec, &u8TotalFDVSeconds);
            FSAP_U8_CLR (&u8Temp1Sec);

            FSAP_U8_ASSIGN_LO (&u8Temp1NanoSec,
                               pFrmDelayBuffTempNode->FrameDelayVariation.
                               u4NanoSeconds);
            FSAP_U8_ADD (&u8TotalFDVNanoSeconds, &u8Temp1NanoSec,
                         &u8TotalFDVNanoSeconds);
            FSAP_U8_CLR (&u8Temp1NanoSec);

            /* Get the PDV in nano seconds and seconds from a string */
            FSAP_U8_ASSIGN_LO (&u8Temp1Sec,
                               pFrmDelayBuffTempNode->InterFrmDelayVariation.
                               u4Seconds);
            FSAP_U8_ADD (&u8TotalIFDVSeconds, &u8Temp1Sec, &u8TotalIFDVSeconds);
            FSAP_U8_CLR (&u8Temp1Sec);

            FSAP_U8_ASSIGN_LO (&u8Temp1NanoSec,
                               pFrmDelayBuffTempNode->InterFrmDelayVariation.
                               u4NanoSeconds);
            FSAP_U8_ADD (&u8TotalIFDVNanoSeconds, &u8Temp1NanoSec,
                         &u8TotalIFDVNanoSeconds);
            FSAP_U8_CLR (&u8Temp1NanoSec);

            /* Get the minimum and maximum Frame Delay Value */
            u4TempNanoSeconds =
                (pFrmDelayBuffTempNode->FrmDelayValue.u4Seconds *
                 ECFM_NUM_OF_NSEC_IN_A_SEC) +
                pFrmDelayBuffTempNode->FrmDelayValue.u4NanoSeconds;
            u4Count = u4Count + ECFM_INCR_VAL;
            if (u4Count == 1)

            {
                u4DelayMax = u4TempNanoSeconds;
                u4DelayMin = u4TempNanoSeconds;
                if (pFdDelayMin != NULL)

                {
                    pu1DelayMin = pFdDelayMin->pu1_OctetList;
                    ECFM_PUT_4BYTE (pu1DelayMin,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4Seconds);
                    ECFM_PUT_4BYTE (pu1DelayMin,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4NanoSeconds);
                    pFdDelayMin->i4_Length = sizeof (tEcfmTimeRepresentation);
                }
                if (pFdDelayMax != NULL)

                {
                    pu1DelayMax = pFdDelayMax->pu1_OctetList;
                    ECFM_PUT_4BYTE (pu1DelayMax,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4Seconds);
                    ECFM_PUT_4BYTE (pu1DelayMax,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4NanoSeconds);
                    pFdDelayMax->i4_Length = sizeof (tEcfmTimeRepresentation);
                }
            }
            if (u4TempNanoSeconds < u4DelayMin)

            {
                u4DelayMin = u4TempNanoSeconds;
                if (pFdDelayMin != NULL)

                {
                    pu1DelayMin = pFdDelayMin->pu1_OctetList;
                    ECFM_PUT_4BYTE (pu1DelayMin,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4Seconds);
                    ECFM_PUT_4BYTE (pu1DelayMin,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4NanoSeconds);
                    pFdDelayMin->i4_Length = sizeof (tEcfmTimeRepresentation);
                }
            }
            if (u4TempNanoSeconds > u4DelayMax)

            {
                u4DelayMax = u4TempNanoSeconds;
                if (pFdDelayMax != NULL)

                {
                    pu1DelayMax = pFdDelayMax->pu1_OctetList;
                    ECFM_PUT_4BYTE (pu1DelayMax,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4Seconds);
                    ECFM_PUT_4BYTE (pu1DelayMax,
                                    pFrmDelayBuffTempNode->
                                    FrmDelayValue.u4NanoSeconds);
                    pFdDelayMax->i4_Length = sizeof (tEcfmTimeRepresentation);
                }
            }
        }

        /* Move to next DMM sequence number */
        pFrmDelayBuffTempNode = EcfmGetNextFrmDelayNodeForATrans
            (pFrmDelayBuffTempNode);
    }

    /* Calculate and update required output variables */
    if (pu4DmrIn != NULL)

    {

        /* Update Number of DMR received for a transaction */
        *pu4DmrIn = u4DmrIn;
    }
    if (pFdDelayAverage != NULL)

    {
        if (u4DmrIn != ECFM_INIT_VAL)

        {

            /* Calculate Frame Delay average in seconds and nano seconds */
            FSAP_U8_CLR (&u8PktCnt);
            FSAP_U8_ASSIGN_LO (&u8PktCnt, u4DmrIn);

            FSAP_U8_DIV (&u8Temp1Sec, &u8Temp1,
                         &u8TotalDelaySeconds, &u8PktCnt);
            FSAP_U8_CLR (&u8Temp1);

            FSAP_U8_DIV (&u8Temp1NanoSec, &u8Temp1,
                         &u8TotalDelayNanoSeconds, &u8PktCnt);
            FSAP_U8_CLR (&u8Temp1);

            u8Temp1.u4Lo = ECFM_NUM_OF_NSEC_IN_A_SEC;

            FSAP_U8_DIV (&u8Temp2Sec, &u8Temp2NanoSec,
                         &u8Temp1NanoSec, &u8Temp1);

            if ((u8Temp2Sec.u4Lo != ECFM_INIT_VAL)
                || (u8Temp2Sec.u4Hi != ECFM_INIT_VAL))

            {
                FSAP_U8_ADD (&u8Temp1Sec, &u8Temp1Sec, &u8Temp2Sec);
                FSAP_U8_ASSIGN (&u8Temp1NanoSec, &u8Temp2NanoSec);

            }
            pu1DelayVal = pFdDelayAverage->pu1_OctetList;

            /* Update Seconds in first four bytes */
            ECFM_PUT_4BYTE (pu1DelayVal, u8Temp1Sec.u4Lo);
            /* Update Nano seconds in next four bytes */
            ECFM_PUT_4BYTE (pu1DelayVal, u8Temp1NanoSec.u4Lo);
            pFdDelayAverage->i4_Length = sizeof (tEcfmTimeRepresentation);
        }
    }
    if (pFdFDVAverage != NULL)

    {
        if (u4DmrIn > ECFM_INCR_VAL)

        {

            /* Calculate  FDV average in seconds and nano seconds */
            FSAP_U8_CLR (&u8PktCnt);
            FSAP_U8_ASSIGN_LO (&u8PktCnt, (u4DmrIn-ECFM_INCR_VAL));

            FSAP_U8_DIV (&u8RetFDVSeconds, &u8Temp1,
                         &u8TotalFDVSeconds, &u8PktCnt);
            FSAP_U8_CLR (&u8Temp1);

            FSAP_U8_DIV (&u8RetFDVNanoSeconds, &u8Temp1,
                         &u8TotalFDVNanoSeconds, &u8PktCnt);
            FSAP_U8_CLR (&u8Temp1);

            u8Temp1.u4Lo = ECFM_NUM_OF_NSEC_IN_A_SEC;

            FSAP_U8_DIV (&u8Seconds, &u8Temp2NanoSec,
                         &u8RetFDVSeconds, &u8Temp1);

            FSAP_U8_DIV (&u8Temp2NanoSec, &u8NanoSeconds,
                         &u8RetFDVNanoSeconds, &u8Temp1);

            if ((u8Seconds.u4Lo != ECFM_INIT_VAL)
                || (u8Seconds.u4Hi != ECFM_INIT_VAL))
            {
                FSAP_U8_ADD (&u8RetFDVSeconds, &u8RetFDVSeconds, &u8Seconds);
                FSAP_U8_ASSIGN (&u8RetFDVNanoSeconds, &u8NanoSeconds);
            }
            pu1FDVVal = pFdFDVAverage->pu1_OctetList;

            /* Update Seconds in first four bytes */
            ECFM_PUT_4BYTE (pu1FDVVal, u8RetFDVSeconds.u4Lo);
            /* Update Nano seconds in next four bytes */
            ECFM_PUT_4BYTE (pu1FDVVal, u8RetFDVNanoSeconds.u4Lo);
            pFdFDVAverage->i4_Length = sizeof (tEcfmTimeRepresentation);
        }
    }
    if (pFdIFDVAverage != NULL)

    {
        if (u4DmrIn > ECFM_INCR_VAL)

        {

            /* calculate  ipdv average in seconds and nano seconds */
            FSAP_U8_CLR (&u8PktCnt);
            FSAP_U8_ASSIGN_LO (&u8PktCnt, (u4DmrIn-ECFM_INCR_VAL));

            FSAP_U8_DIV (&u8RetIFDVSeconds, &u8Temp1,
                         &u8TotalIFDVSeconds, &u8PktCnt);
            FSAP_U8_CLR (&u8Temp1);

            FSAP_U8_DIV (&u8RetIFDVNanoSeconds, &u8Temp1,
                         &u8TotalIFDVNanoSeconds, &u8PktCnt);
            FSAP_U8_CLR (&u8Temp1);

            u8Temp1.u4Lo = ECFM_NUM_OF_NSEC_IN_A_SEC;

            FSAP_U8_DIV (&u8Seconds, &u8Temp2NanoSec,
                         &u8RetIFDVSeconds, &u8Temp1);

            FSAP_U8_DIV (&u8Temp2NanoSec, &u8NanoSeconds,
                         &u8RetIFDVNanoSeconds, &u8Temp1);

            if ((u8Seconds.u4Lo != ECFM_INIT_VAL)
                || (u8Seconds.u4Hi != ECFM_INIT_VAL))
            {
                FSAP_U8_ADD (&u8RetIFDVSeconds, &u8RetIFDVSeconds, &u8Seconds);
                FSAP_U8_ASSIGN (&u8RetIFDVNanoSeconds, &u8NanoSeconds);
            }
            pu1IFDVVal = pFdIFDVAverage->pu1_OctetList;

            /* Update Seconds in first four bytes */
            ECFM_PUT_4BYTE (pu1IFDVVal, u8RetIFDVSeconds.u4Lo);

            /* Update Nano seconds in next four bytes */
            ECFM_PUT_4BYTE (pu1IFDVVal, u8RetIFDVNanoSeconds.u4Lo);
            pFdIFDVAverage->i4_Length = sizeof (tEcfmTimeRepresentation);
        }
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetNextIndexFdStatsTbl 
 *                                                                          
 *    DESCRIPTION      : This function returns the next frame delay stats
 *                       indices.
 *
 *    INPUT            : u4MdIndex - Md Index (MEG Index)
 *                       u4MaIndex - Ma Index (ME Index)
 *                       u4MepIdentifier - MepIdentifier
 *                       u4FdTransId - Transaction Identifier
 *                       
 *                                 
 *    OUTPUT           : pu4NextMdIndex - Next Md Index
 *                       pu4NextMaIndex - Next Ma Index 
 *                       pu4NextMepIdentifier - Next Mep Identifier
 *                       pu4NextFdTransId - Next Transaction Identifier
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwGetNextIndexFdStatsTbl (UINT4 u4MdIndex, UINT4 *pu4NextMdIndex,
                                  UINT4 u4MaIndex, UINT4 *pu4NextMaIndex,
                                  UINT4 u4MepIdentifier,
                                  UINT4 *pu4NextMepIdentifier,
                                  UINT4 u4FdTransId, UINT4 *pu4NextFdTransId)
{
    tEcfmLbLtFrmDelayBuff *pFrmDelayTempNode = NULL;

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        return ECFM_FAILURE;
    }
    pFrmDelayTempNode = (tEcfmLbLtFrmDelayBuff *)
        RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);

    /* Traverse the LB init table for the next transaction indices */
    while (pFrmDelayTempNode != NULL)

    {
        if ((pFrmDelayTempNode->u4MdIndex > u4MdIndex) ||
            (pFrmDelayTempNode->u4MaIndex > u4MaIndex) ||
            (pFrmDelayTempNode->u2MepId > (UINT2) u4MepIdentifier))

        {

            /* Required node */
            *pu4NextMdIndex = pFrmDelayTempNode->u4MdIndex;
            *pu4NextMaIndex = pFrmDelayTempNode->u4MaIndex;
            *pu4NextMepIdentifier = (UINT4) (pFrmDelayTempNode->u2MepId);
            *pu4NextFdTransId = pFrmDelayTempNode->u4TransId;
            return ECFM_SUCCESS;
        }
        if ((pFrmDelayTempNode->u4MdIndex >= u4MdIndex) &&
            (pFrmDelayTempNode->u4MaIndex >= u4MaIndex) &&
            (pFrmDelayTempNode->u2MepId >= (UINT2) u4MepIdentifier) &&
            (pFrmDelayTempNode->u4TransId > u4FdTransId))

        {

            /* Required node */
            *pu4NextMdIndex = pFrmDelayTempNode->u4MdIndex;
            *pu4NextMaIndex = pFrmDelayTempNode->u4MaIndex;
            *pu4NextMepIdentifier = (UINT4) (pFrmDelayTempNode->u2MepId);
            *pu4NextFdTransId = pFrmDelayTempNode->u4TransId;
            return ECFM_SUCCESS;
        }

        /* Skip the same transaction and differet sequence no node */
        if ((pFrmDelayTempNode->u4MdIndex == u4MdIndex) &&
            (pFrmDelayTempNode->u4MaIndex == u4MaIndex) &&
            (pFrmDelayTempNode->u2MepId == (UINT2) u4MepIdentifier) &&
            (pFrmDelayTempNode->u4TransId == u4FdTransId))

        {

            /* Move to next node */
            pFrmDelayTempNode = (tEcfmLbLtFrmDelayBuff *) RBTreeGetNext
                (ECFM_LBLT_FD_BUFFER_TABLE, (tRBElem *)
                 pFrmDelayTempNode, NULL);
            continue;
        }

        /* Skip the node having TransId less than the required one */
        if ((pFrmDelayTempNode->u4MdIndex >= u4MdIndex) &&
            (pFrmDelayTempNode->u4MaIndex >= u4MaIndex) &&
            (pFrmDelayTempNode->u2MepId >= (UINT2) u4MepIdentifier) &&
            (pFrmDelayTempNode->u4TransId <= u4FdTransId))

        {

            /* Move to next node */
            pFrmDelayTempNode = (tEcfmLbLtFrmDelayBuff *) RBTreeGetNext
                (ECFM_LBLT_FD_BUFFER_TABLE, (tRBElem *)
                 pFrmDelayTempNode, NULL);
            continue;
        }

        /* Move to next node */
        pFrmDelayTempNode = (tEcfmLbLtFrmDelayBuff *) RBTreeGetNext
            (ECFM_LBLT_FD_BUFFER_TABLE, (tRBElem *) pFrmDelayTempNode, NULL);
    } return ECFM_FAILURE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmSnmpLwGetFirstIndexFdStatsTbl 
 *                                                                          
 *    DESCRIPTION      : This function returns the first Frame Delay Stats 
 *                       Entry indices.
 *
 *    INPUT            : None
 *                       
 *                                 
 *    OUTPUT           : pu4MdIndex - Firstt Md Index
 *                       pu4MaIndex - First Ma Index 
 *                       pu4MepIdentifier - First Mep Identifier
 *                       pu4FdTransId - First Transaction Identifier
 *                                                                          
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmSnmpLwGetFirstIndexFdStatsTbl (UINT4 *pu4MdIndex, UINT4 *pu4MaIndex,
                                   UINT4 *pu4MepIdentifier, UINT4 *pu4FdTransId)
{
    return (EcfmSnmpLwGetNextIndexFdStatsTbl
            (0, pu4MdIndex, 0, pu4MaIndex, 0, pu4MepIdentifier, 0,
             pu4FdTransId));
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwConfDropEnable
 * 
 * DESCRIPTION      : API Function to set drop enable in CC task.
 *
 * INPUT            : u4MdIndex - MD index
 *                    b1DropEnable - Vlan Drop Eligibility
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmSnmpLwConfDropEnable (UINT4 u4MdIndex, BOOL1 b1DropEnable)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    gpEcfmCcMepNode->u4MdIndex = u4MdIndex;

    pMepInfo = RBTreeGetNext (ECFM_CC_MEP_TABLE,
                              (tRBElem *) gpEcfmCcMepNode, NULL);
    while ((pMepInfo != NULL) && (pMepInfo->u4MdIndex == u4MdIndex))
    {
        pMepInfo->CcInfo.b1CcmDropEligible = b1DropEnable;
        pMepInfo->LmInfo.b1LmmDropEnable = b1DropEnable;
        pMepInfo->AisInfo.b1AisDropEnable = b1DropEnable;
        pMepInfo->LckInfo.b1LckDropEnable = b1DropEnable;

        pMepInfo = RBTreeGetNext (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) pMepInfo, NULL);

    }
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwConfVlanPriority
 * 
 * DESCRIPTION      : API Function to set vlan priority in CC task.
 *
 * INPUT            : u4MdIndex - MD index
 *                    u1VlanPriority - Vlan priority
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmSnmpLwConfVlanPriority (UINT4 u4MdIndex, UINT1 u1VlanPriority)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    gpEcfmCcMepNode->u4MdIndex = u4MdIndex;

    pMepInfo = RBTreeGetNext (ECFM_CC_MEP_TABLE,
                              (tRBElem *) gpEcfmCcMepNode, NULL);
    while ((pMepInfo != NULL) && (pMepInfo->u4MdIndex == u4MdIndex))
    {
        pMepInfo->CcInfo.u1CcmPriority = u1VlanPriority;
        pMepInfo->LmInfo.u1TxLmmPriority = u1VlanPriority;
        pMepInfo->AisInfo.u1AisPriority = u1VlanPriority;
        pMepInfo->LckInfo.u1LckPriority = u1VlanPriority;

        pMepInfo = RBTreeGetNext (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) pMepInfo, NULL);
    }

}

/**************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwSetDefaultMdNode
 * 
 * DESCRIPTION      : This funtion is used to add a default MD table entry
 *
 * INPUT            : u4VlanIdIsid - VLAN or ISID
 *
 * OUTPUT           : None
 *
 * RETURNS          : Newly added default MD node
 *
 **************************************************************************/
PUBLIC tEcfmCcDefaultMdTableInfo *
EcfmSnmpLwSetDefaultMdNode (UINT4 u4VlanIdIsid)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    if (ECFM_ALLOC_MEM_BLOCK_CC_DEF_MD_TABLE (pDefaultMdNode) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return NULL;
    }
    ECFM_MEMSET (pDefaultMdNode, ECFM_INIT_VAL, ECFM_CC_DEF_MD_INFO_SIZE);
    pDefaultMdNode->u4PrimaryVidIsid = u4VlanIdIsid;
    pDefaultMdNode->b1Status = ECFM_TRUE;
    pDefaultMdNode->i1MdLevel = ECFM_DEF_MD_LEVEL_DEF_VAL;
    pDefaultMdNode->u1MhfCreation = ECFM_MHF_CRITERIA_DEFER;
    pDefaultMdNode->u1IdPermission = ECFM_SENDER_ID_DEFER;
    if (RBTreeAdd (ECFM_CC_DEF_MD_TABLE, pDefaultMdNode) == ECFM_RB_FAILURE)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_DEF_MD_TABLE_POOL,
                             (UINT1 *) (pDefaultMdNode));
        return NULL;

    }
    if (EcfmLbLtAddDefMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pDefaultMdNode) !=
        ECFM_SUCCESS)
    {
        RBTreeRem (ECFM_CC_DEF_MD_TABLE, (tRBElem *) pDefaultMdNode);
        ECFM_FREE_MEM_BLOCK (ECFM_CC_DEF_MD_TABLE_POOL,
                             (UINT1 *) (pDefaultMdNode));
        return NULL;
    }
    return pDefaultMdNode;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSnmpLwProcessDefaultMdNode
 * 
 * DESCRIPTION      : This funtion is used to check if all the objects of the 
 *                    defaultMD table are default if so then delete the ROW.
 *
 * INPUT            : u4VlanIdIsid - VLAN or ISID
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
EcfmSnmpLwProcessDefaultMdNode (UINT4 u4VlanIdIsid)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    pDefaultMdNode = EcfmCcSnmpLwGetDefaultMdEntry (u4VlanIdIsid);
    if (pDefaultMdNode == NULL)
    {
        return;
    }
    /* Check if all the paramter of the node are defauls */
    if ((pDefaultMdNode->i1MdLevel == ECFM_DEF_MD_LEVEL_DEF_VAL) &&
        (pDefaultMdNode->u1MhfCreation == ECFM_MHF_CRITERIA_DEFER) &&
        (pDefaultMdNode->u1IdPermission == ECFM_SENDER_ID_DEFER) &&
        (pDefaultMdNode->b1Status == ECFM_TRUE))
    {
        EcfmLbLtRemoveDefMdEntry (ECFM_CC_CURR_CONTEXT_ID (), pDefaultMdNode);
        RBTreeRem (ECFM_CC_DEF_MD_TABLE, (tRBElem *) pDefaultMdNode);
        /* Release memory block allocated to MD node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_DEF_MD_TABLE_POOL,
                             (UINT1 *) (pDefaultMdNode));
        pDefaultMdNode = NULL;
    }
    return;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmCcSnmpLwGetDefaultMdEntry
 * 
 * DESCRIPTION      : This funtion is used to add a default MD table entry
 *
 * INPUT            : u4VlanIdIsid - VLAN or ISID
 *
 * OUTPUT           : None
 *
 * RETURNS          : Newly added default MD node
 *
 **************************************************************************/
PUBLIC tEcfmCcDefaultMdTableInfo *
EcfmCcSnmpLwGetDefaultMdEntry (UINT4 u4VlanIdIsid)
{
    tEcfmCcDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmCcDefaultMdTableInfo DummyNode;
    DummyNode.u4PrimaryVidIsid = u4VlanIdIsid;
    pDefaultMdNode = (tEcfmCcDefaultMdTableInfo *) RBTreeGet
        (ECFM_CC_DEF_MD_TABLE, &DummyNode);
    return pDefaultMdNode;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmLbLtSnmpLwGetDefaultMdEntry
 * 
 * DESCRIPTION      : This funtion is used to add a default MD table entry
 *
 * INPUT            : u4VlanIdIsid - VLAN or ISID
 *
 * OUTPUT           : None
 *
 * RETURNS          : Newly added default MD node
 *
 **************************************************************************/
PUBLIC tEcfmLbLtDefaultMdTableInfo *
EcfmLbLtSnmpLwGetDefaultMdEntry (UINT4 u4VlanIdIsid)
{
    tEcfmLbLtDefaultMdTableInfo *pDefaultMdNode = NULL;
    tEcfmLbLtDefaultMdTableInfo DummyNode;
    DummyNode.u4PrimaryVidIsid = u4VlanIdIsid;
    pDefaultMdNode = (tEcfmLbLtDefaultMdTableInfo *) RBTreeGet
        (ECFM_LBLT_DEF_MD_TABLE, &DummyNode);
    return pDefaultMdNode;
}

/****************************************************************************
 Function    :  EcfmSnmpLwGetFirstIndexFsY1731MplstpExtRemoteMepTable
 Input       :  The Indices
                fsY1731MegIndex
                fsY1731MeIndex
                fsY1731MepIdentifier
                fsY1731MplstpExtRMepIdentifier
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  ECFM_SUCCESS or ECFM_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
EcfmSnmpLwGetFirstIndexFsY1731MplstpExtRemoteMepTable (UINT4
                                                       *pu4FsMIY1731MegIndex,
                                                       UINT4
                                                       *pu4FsMIY1731MeIndex,
                                                       UINT4
                                                       *pu4FsMIY1731MepIdentifier,
                                                       UINT4
                                                       *pu4FsMIY1731MplstpExtRMepIdentifier)
{
    return (EcfmSnmpLwGetNextIndexFsY1731MplstpExtRemoteMepTable
            (0, pu4FsMIY1731MegIndex, 0, pu4FsMIY1731MeIndex, 0,
             pu4FsMIY1731MepIdentifier, 0,
             pu4FsMIY1731MplstpExtRMepIdentifier));
}

/****************************************************************************
 Function    :  EcfmSnmpLwGetNextIndexFsEcfmRemoteMepDbExTable
 Input       :  The Indices
                FsY1731ContextId
                nextFsY1731ContextId
                FsY1731MegIndex
                nextFsY1731MegIndex
                FsY1731MeIndex
                nextFsY1731MeIndex
                FsY1731MepIdentifier
                nextFsY1731MepIdentifier
                FsY1731MplstpExtRMepIdentifier
                nextFsY1731MplstpExtRMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  ECFM_SUCCESS or ECFM_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
EcfmSnmpLwGetNextIndexFsY1731MplstpExtRemoteMepTable (UINT4 u4FsMIY1731MegIndex,
                                                      UINT4
                                                      *pu4NextFsMIY1731MegIndex,
                                                      UINT4 u4FsMIY1731MeIndex,
                                                      UINT4
                                                      *pu4NextFsMIY1731MeIndex,
                                                      UINT4
                                                      u4FsMIY1731MepIdentifier,
                                                      UINT4
                                                      *pu4NextFsMIY1731MepIdentifier,
                                                      UINT4
                                                      u4FsMIY1731MplstpExtRMepIdentifier,
                                                      UINT4
                                                      *pu4NextFsMIY1731MplstpExtRMepIdentifier)
{
    tEcfmCcRMepDbInfo   CcRMepEntry;
    tEcfmCcRMepDbInfo  *pCcRMepNextEntry = NULL;
    tEcfmCcMaInfo      *pMeNode = NULL;

    ECFM_MEMSET (&CcRMepEntry, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);

    /* Check the System Status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                     "\tSNMP: ECFM Module is Shutdown\n");
        return ECFM_FAILURE;
    }

    CcRMepEntry.u4MdIndex = u4FsMIY1731MegIndex;
    CcRMepEntry.u4MaIndex = u4FsMIY1731MeIndex;
    CcRMepEntry.u2MepId = (UINT2) u4FsMIY1731MepIdentifier;
    CcRMepEntry.u2RMepId = (UINT2) u4FsMIY1731MplstpExtRMepIdentifier;

    /* Getting next node */
    pCcRMepNextEntry =
        (tEcfmCcRMepDbInfo *) RBTreeGetNext (ECFM_CC_RMEP_TABLE,
                                             (tRBElem *) & CcRMepEntry, NULL);

    while (pCcRMepNextEntry != NULL)
    {
        pMeNode = pCcRMepNextEntry->pMepInfo->pMaInfo;

        if ((pMeNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
            (pMeNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW))
        {
            /* If the RowStatus object is equal to zero then the RMep
             * entry is not configured in Y.1731-MPLSTP RMep Table
             * Hence the entry can be skipped.
             */
            if (pCcRMepNextEntry->u1RowStatus != ECFM_INIT_VAL)
            {
                *pu4NextFsMIY1731MegIndex = pCcRMepNextEntry->u4MdIndex;
                *pu4NextFsMIY1731MeIndex = pCcRMepNextEntry->u4MaIndex;
                *pu4NextFsMIY1731MepIdentifier =
                    (UINT4) (pCcRMepNextEntry->u2MepId);
                *pu4NextFsMIY1731MplstpExtRMepIdentifier =
                    (UINT4) (pCcRMepNextEntry->u2RMepId);
                return ECFM_SUCCESS;
            }
        }
        pCcRMepNextEntry =
            (tEcfmCcRMepDbInfo *) RBTreeGetNext (ECFM_CC_RMEP_TABLE,
                                                 (tRBElem *) pCcRMepNextEntry,
                                                 NULL);
    }

    return ECFM_FAILURE;
}

/******************************************************************************/
/*                           End  of file cfmlwutl.c                          */
/******************************************************************************/
