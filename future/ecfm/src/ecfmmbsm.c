/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmmbsm.c,v 1.18 2014/10/03 10:22:41 siva Exp $
 * 
 * Description: This file contains functions for handling card insertion 
 *              and removal in a chassis system.
 ******************************************************************************/
#ifdef MBSM_WANTED
#include "cfminc.h"

PRIVATE INT4 EcfmMbsmUpdateOnCardInsert PROTO ((tMbsmSlotInfo *,
                                                tMbsmPortInfo *));
PRIVATE INT4 EcfmMbsmUpdateOnCardRemove PROTO ((tMbsmSlotInfo *,
                                                tMbsmPortInfo *));
PRIVATE INT4 EcfmMbsmHandleCardInsertForDownMep PROTO ((tMbsmPortInfo *));
PRIVATE INT4 EcfmMbsmHandleCardInsertForUpMep PROTO ((VOID));
PRIVATE INT4 EcfmMbsmHandleCardRmvForDownMep PROTO ((tMbsmPortInfo *));
PRIVATE INT4 EcfmMbsmHandleCardRmvForUpMep PROTO ((VOID));
PRIVATE INT4 EcfmMbsmStopOnDemandOprtLbLtTaskOnUpMep PROTO ((VOID));
PRIVATE INT4 EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep PROTO
    ((tMbsmPortInfo *));

PRIVATE INT4 EcfmMbsmPgmDownMepForPortInHw PROTO ((UINT4 u4PortNo));

PRIVATE INT4 EcfmMbsmSispPgmDownMepForPort PROTO ((UINT4 u4PortNo));

PRIVATE INT4 EcfmMbsmRemoveDownMepForPortFromHw PROTO ((UINT4 u4PortNo));

PRIVATE INT4        EcfmMbsmSispHandleCardRmvForDownMep (UINT4 u4PortNo);

PRIVATE INT4
       EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMepForPort (UINT4 u4PortNo);

PRIVATE INT4
       EcfmMbsmSispStopOnDemandOprtLbLtTaskOnDownMepForPort (UINT4 u4PortNo);

PRIVATE INT4 EcfmMbsmPgmDownMepForPortInHw PROTO ((UINT4 u4PortNo));

PRIVATE INT4 EcfmMbsmSispPgmDownMepForPort PROTO ((UINT4 u4PortNo));

PRIVATE INT4 EcfmMbsmRemoveDownMepForPortFromHw PROTO ((UINT4 u4PortNo));

PRIVATE INT4        EcfmMbsmSispHandleCardRmvForDownMep (UINT4 u4PortNo);

PRIVATE INT4
       EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMepForPort (UINT4 u4PortNo);

PRIVATE INT4
       EcfmMbsmSispStopOnDemandOprtLbLtTaskOnDownMepForPort (UINT4 u4PortNo);

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmMbsmHandleLcStatusChg 
 *                                                                          
 *    DESCRIPTION      : This function programs the HW with the ECFM  
 *                       software configuration. 
 *                       This function will be called from the ECFM module
 *                       when an indication for the Card Insertion/Removal   
 *                       is received from the MBSM. 
 *
 *    INPUT            : pBuf  - Buffer containing the protocol message 
 *                               information 
 *                       u4Cmd - Line card - Insert/Remove
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EcfmMbsmHandleLcStatusChg (tMbsmProtoMsg * pBuf, UINT4 u4Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    INT4                i4ProtoCookie = ECFM_INIT_VAL;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmProtoAckMsg    ProtoAckMsg;

    if (pBuf == NULL)
    {
        return;
    }

    i4ProtoCookie = pBuf->i4ProtoCookie;
    pSlotInfo = &(pBuf->MbsmSlotInfo);
    pPortInfo = &(pBuf->MbsmPortInfo);

    if (u4Cmd == MBSM_MSG_CARD_INSERT)
    {
        i4RetStatus = EcfmMbsmUpdateOnCardInsert (pSlotInfo, pPortInfo);
    }
    if (u4Cmd == MBSM_MSG_CARD_REMOVE)
    {
        i4RetStatus = EcfmMbsmUpdateOnCardRemove (pSlotInfo, pPortInfo);
    }
    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP
     */
    ProtoAckMsg.i4RetStatus = i4RetStatus;
    ProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
    ProtoAckMsg.i4ProtoCookie = i4ProtoCookie;
    MbsmSendAckFromProto (&ProtoAckMsg);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmMbsmUpdateOnCardInsert 
 *                                                                          
 *    DESCRIPTION      : This function used to perform the necessary actions 
 *                       in HW and SW  when an indication for the 
 *                       Card Insertion is received from the MBSM. 
 *
 *    INPUT            : pPortInfo - Inserted port list     
 *                       pSlotInfo - Information about inserted slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmUpdateOnCardInsert (tMbsmSlotInfo * pSlotInfo,
                            tMbsmPortInfo * pPortInfo)
{
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4PrevContextId = ECFM_INIT_VAL;
    UINT4               u4PortCount = ECFM_INIT_VAL;
    UINT4               u4PortNo = ECFM_INIT_VAL;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    /* Update the global Slot Info */
    ECFM_MEMSET (&gEcfmSlotInfo, ECFM_INIT_VAL, sizeof (tMbsmSlotInfo));

    MEMCPY (&gEcfmSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

    /* Update the global MBSM Event Handler boolean 
     * Set this to TRUE so that MBSM NPAPIs are called */
    gbEcfmMbsmEvtHndl = ECFM_TRUE;

    if (EcfmCcGetFirstActiveContext (&u4ContextId) == ECFM_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;
            if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
            {
                continue;
            }

            if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
            {
                ECFM_CC_RELEASE_CONTEXT ();
                continue;
            }
            if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
            {
                /*Check for port/card message */
                if (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                {
                    if (EcfmFsMiEcfmMbsmNpInitHw (u4ContextId, pSlotInfo) ==
                        FNP_FAILURE)
                    {
                        ECFM_GLB_TRC (u4ContextId,
                                      ECFM_INIT_SHUT_TRC |
                                      ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmMbsmUpdateOnCardInsert: MBSM Hw init"
                                      " NPAPI returned failure\n");
                        ECFM_CC_RELEASE_CONTEXT ();
                        gbEcfmMbsmEvtHndl = ECFM_FALSE;
                        return MBSM_FAILURE;
                    }
                }
            }
        }
        while (EcfmCcGetNextActiveContext (u4PrevContextId,
                                           &u4ContextId) == ECFM_SUCCESS);
    }
    /* If not Preconfigured program the defaults. */
    if (MBSM_SLOT_INFO_ISPRECONF (pSlotInfo) == MBSM_FALSE)
    {
        gbEcfmMbsmEvtHndl = ECFM_FALSE;
        return MBSM_SUCCESS;
    }

    /* Call routine for Down MEP to handle Card Insert Event */
    if (EcfmMbsmHandleCardInsertForDownMep (pPortInfo) == MBSM_FAILURE)
    {
        gbEcfmMbsmEvtHndl = ECFM_FALSE;
        return MBSM_FAILURE;
    }

    /* Call routine for UP MEP to handle Card Insert Event */
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (EcfmMbsmHandleCardInsertForUpMep () == MBSM_FAILURE)
        {
            gbEcfmMbsmEvtHndl = ECFM_FALSE;
            return MBSM_FAILURE;
        }
    }
    gbEcfmMbsmEvtHndl = ECFM_FALSE;
    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmMbsmUpdateOnCardRemove 
 *                                                                          
 *    DESCRIPTION      : This function programs the HW with the ECFM  
 *                       software configuration. 
 *                       This function will be called from the ECFM module
 *                       when an indication for the Card Removal is  
 *                       received from the MBSM. 
 *
 *    INPUT            : pPortInfo - Removed port list     
 *                       pSlotInfo - Information about removed slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmUpdateOnCardRemove (tMbsmSlotInfo * pSlotInfo,
                            tMbsmPortInfo * pPortInfo)
{
    /* Update global Slot Info */
    ECFM_MEMSET (&gEcfmSlotInfo, ECFM_INIT_VAL, sizeof (tMbsmSlotInfo));
    MEMCPY (&gEcfmSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

    /* Update the global MBSM Event Handler boolean 
     * Set this to TRUE so that MBSM NPAPIs are called */
    gbEcfmMbsmEvtHndl = ECFM_TRUE;

    /* Call routine for Down MEP to handle Card Remove Event */
    if (EcfmMbsmHandleCardRmvForDownMep (pPortInfo) == MBSM_FAILURE)
    {
        gbEcfmMbsmEvtHndl = ECFM_FALSE;
        return MBSM_FAILURE;
    }

    /* Call routine for UP MEP to handle Card Remove Event */
    if (EcfmMbsmHandleCardRmvForUpMep () == MBSM_FAILURE)
    {
        gbEcfmMbsmEvtHndl = ECFM_FALSE;
        return MBSM_FAILURE;
    }

    gbEcfmMbsmEvtHndl = ECFM_FALSE;
    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EcfmMbsmPostMessage 
 *                                                                          
 *    DESCRIPTION      : Allocates a CRU buffer and enqueues the Line card
 *                       change status to the ECFM Task
 *
 *    INPUT            : pProtoMsg - Contains the Slot and Port Information 
 *                       i4Event  - Line card Up/Down status          
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EcfmMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tEcfmCcMsg         *pQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    MEMSET (&(MbsmProtoAckMsg), 0, sizeof (tMbsmProtoAckMsg));

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        /* If the module is shutdown no need to proceed further, send the ack 
         * to MBSM so that other modules get the LcStatus Notification. 
         * The ack is sent only for hardware updation to be in
         * synchronization across the dependant modules. 
         */
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pQMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return MBSM_FAILURE;
    }

    ECFM_MEMSET (pQMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pQMsg->MsgType = (UINT4) i4Event;

    if (!(pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pQMsg);
        return MBSM_FAILURE;
    }
    MEMCPY (pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
            sizeof (tMbsmProtoMsg));

    if (EcfmCcCfgQueMsg (pQMsg) != ECFM_SUCCESS)
    {
        MEM_FREE (pQMsg->uMsg.MbsmCardUpdate.pMbsmProtoMsg);
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pQMsg);
        return MBSM_FAILURE;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmMbsmPostMessage: queued successfully to cc \r\n");
    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmHandleCardInsertForUpMep
 *
 *    Description      : This rotuine checks if upcoming port belongs to an UP
 *                       MEP in the system and it is not started in hardware
 *                       then start it in hardware.
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmHandleCardInsertForUpMep (VOID)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcPortInfo    *pLocalPortInfo = NULL;
    tEcfmCfaIfInfo      CfaIfInfo;
    UINT1              *pPortList = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4PrevContextId = ECFM_INIT_VAL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2PrevPort = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    BOOL1               b1PortPresent = ECFM_FALSE;

    ECFM_MEMSET (&CfaIfInfo, ECFM_INIT_VAL, sizeof (tCfaIfInfo));
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    if (EcfmCcGetFirstActiveContext (&u4ContextId) == ECFM_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;
            if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
            {
                continue;
            }

            if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
            {
                ECFM_CC_RELEASE_CONTEXT ();
                continue;
            }

            while (EcfmL2IwfGetNextValidPortForContext
                   (ECFM_CC_CURR_CONTEXT_ID (), u2PrevPort, &u2PortNum,
                    &u4IfIndex) == L2IWF_SUCCESS)
            {
                pPortInfo = NULL;
                pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
                if (pPortInfo != NULL)
                {
                    gpEcfmCcMepNode->u4IfIndex = u4IfIndex;
                    gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
                    pMepInfo = NULL;
                    pMepInfo = (tEcfmCcMepInfo *) RBTreeGetNext
                        (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, gpEcfmCcMepNode,
                         NULL);

                    while ((pMepInfo != NULL)
                           && (pMepInfo->u4IfIndex == u4IfIndex)
                           && (pMepInfo->u4ContextId ==
                               ECFM_CC_CURR_CONTEXT_ID ()))
                    {
                        if ((pMepInfo->u1Direction != ECFM_MP_DIR_UP) ||
                            (pMepInfo->b1MepCcmOffloadHwStatus != ECFM_FALSE))
                        {
                            pMepInfo =
                                (tEcfmCcMepInfo *)
                                RBTreeGetNext (ECFM_CC_PORT_MEP_TABLE,
                                               (tRBElem *) pMepInfo, NULL);
                            continue;
                        }
                        pPortList =
                            UtilPlstAllocLocalPortList (sizeof
                                                        (tLocalPortListExt));
                        if (pPortList == NULL)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "EcfmMbsmHandleCardInsertForUpMep: Error in allocating memory "
                                         "for pPortList\r\n");
                            ECFM_CC_RELEASE_CONTEXT ();
                            return MBSM_FAILURE;
                        }
                        ECFM_MEMSET (pPortList, ECFM_INIT_VAL,
                                     sizeof (tLocalPortListExt));

                        if (EcfmL2IwfMiGetVlanEgressPorts
                            (ECFM_CC_CURR_CONTEXT_ID (),
                             pMepInfo->u4PrimaryVidIsid,
                             pPortList) == ECFM_SUCCESS)
                        {
                            /* Disable the port on which Up MEP is created */
                            ECFM_RESET_MEMBER_PORT (pPortList,
                                                    pPortInfo->u2PortNum);

                            for (u2ByteInd = ECFM_INIT_VAL;
                                 u2ByteInd < ECFM_PORT_LIST_SIZE; u2ByteInd++)
                            {
                                if (pPortList[u2ByteInd] == 0)
                                {
                                    continue;
                                }
                                u1PortFlag = pPortList[u2ByteInd];
                                for (u2BitIndex = 0;
                                     ((u2BitIndex < BITS_PER_BYTE)
                                      &&
                                      (EcfmUtilQueryBitListTable
                                       (u1PortFlag, u2BitIndex) != 0));
                                     u2BitIndex++)
                                {
                                    u2Port =
                                        (UINT2) ((u2ByteInd *
                                                  BITS_PER_BYTE) +
                                                 EcfmUtilQueryBitListTable
                                                 (u1PortFlag, u2BitIndex));

                                    pLocalPortInfo = NULL;
                                    pLocalPortInfo =
                                        ECFM_CC_GET_PORT_INFO (u2Port);
                                    if (pLocalPortInfo != NULL)
                                    {
                                        if (EcfmUtilGetIfInfo
                                            (pLocalPortInfo->u4IfIndex,
                                             &CfaIfInfo) == CFA_SUCCESS)
                                        {
                                            if (CfaIfInfo.
                                                u1IfOperStatus != CFA_IF_NP)
                                            {
                                                b1PortPresent = ECFM_TRUE;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (b1PortPresent == ECFM_TRUE)
                                {
                                    break;
                                }
                            }
                        }
                        UtilPlstReleaseLocalPortList (pPortList);
                        if (b1PortPresent == ECFM_TRUE)
                        {
                            if (EcfmCcmOffCreateTxRxForMep (pMepInfo) ==
                                ECFM_FAILURE)
                            {
                                ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (),
                                              ECFM_INIT_SHUT_TRC |
                                              ECFM_CONTROL_PLANE_TRC |
                                              ECFM_ALL_FAILURE_TRC,
                                              "EcfmMbsmUpdateOnCardInsert:"
                                              " EcfmCcmOffCreateTxRxForMep func"
                                              " returned failure\n");
                                ECFM_CC_RELEASE_CONTEXT ();
                                return MBSM_FAILURE;
                            }

                        }

                        pMepInfo =
                            (tEcfmCcMepInfo *)
                            RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                           (tRBElem *) pMepInfo, NULL);
                    }
                }
                u2PrevPort = u2PortNum;
            }
            ECFM_CC_RELEASE_CONTEXT ();
        }
        while (EcfmCcGetNextActiveContext (u4PrevContextId,
                                           &u4ContextId) == ECFM_SUCCESS);
    }
    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmHandleCardInsertForDownMep
 *
 *    Description      : This rotuine checks if upcoming port belongs to Down 
 *                       MEP in the system and it is not started in hardware
 *                       then start it in hardware.
 *    INPUT            : pPortInfo - Inserted port list     
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmHandleCardInsertForDownMep (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount = ECFM_INIT_VAL;
    UINT4               u4PortNo = ECFM_INIT_VAL;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    while (u4PortCount != 0)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4PortNo,
                                 sizeof (tPortListExt), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4PortNo, sizeof (tPortListExt),
                                 u1IsSetInPortListStatus);
        /* Check for newly inserted ports */
        if ((OSIX_TRUE == u1IsSetInPortList)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {
            if (EcfmMbsmPgmDownMepForPortInHw (u4PortNo) == MBSM_FAILURE)
            {
                return MBSM_FAILURE;
            }

            if (EcfmMbsmSispPgmDownMepForPort (u4PortNo) == MBSM_FAILURE)
            {
                return MBSM_FAILURE;
            }

        }                        /* Port is present in attached slot */
        u4PortCount--;
        u4PortNo++;
    }
    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmHandleCardRmvForDownMep
 *
 *    Description      : This rotuine checks if port belonging to a Down MEP
 *                       in the system is removed then disable CCM offload 
 *                       for that MEP in hardware
 *    INPUT            : pPortInfo - Inserted port list     
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 *****************************************************************************/
PRIVATE INT4
EcfmMbsmHandleCardRmvForDownMep (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount = ECFM_INIT_VAL;
    UINT4               u4PortNo = ECFM_INIT_VAL;
    UINT1               u1Result = ECFM_INIT_VAL;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    while (u4PortCount != 0)
    {
        MBSM_IS_MEMBER_PORT (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                             u4PortNo, u1Result);

        if (u1Result == MBSM_TRUE)
        {
            if (EcfmMbsmRemoveDownMepForPortFromHw (u4PortNo) != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }

            if (EcfmMbsmSispHandleCardRmvForDownMep (u4PortNo) != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }
        }
        u4PortCount--;
        u4PortNo++;
    }

    /* Call routine for Down MEP to stop On Demand Operation for LBLT Task */
    if (EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep (pPortInfo) == MBSM_FAILURE)
    {
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmHandleCardRmvForUpMep
 *
 *    Description      : This rotuine checks if all the eggress ports belonging
 *                       to an Up MEP in the system are removed then disable
 *                       CCM offload for that MEP in hardware
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 *****************************************************************************/
PRIVATE INT4
EcfmMbsmHandleCardRmvForUpMep (VOID)
{
    tEcfmCcRMepDbInfo   RMepNode;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcPortInfo    *pLocalPortInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT1              *pPortList = NULL;
    tEcfmCfaIfInfo      CfaIfInfo;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4PrevContextId = ECFM_INIT_VAL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT2               u2PrevPort = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    BOOL1               b1PortPresent = ECFM_FALSE;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    ECFM_MEMSET (&RMepNode, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);
    ECFM_MEMSET (&CfaIfInfo, ECFM_INIT_VAL, sizeof (tCfaIfInfo));

    if (EcfmCcGetFirstActiveContext (&u4ContextId) == ECFM_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;
            if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
            {
                continue;
            }

            if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
            {
                ECFM_CC_RELEASE_CONTEXT ();
                continue;
            }
            while (EcfmL2IwfGetNextValidPortForContext
                   (ECFM_CC_CURR_CONTEXT_ID (), u2PrevPort, &u2PortNum,
                    &u4IfIndex) == L2IWF_SUCCESS)
            {
                pPortInfo = NULL;
                pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
                if (pPortInfo != NULL)
                {
                    gpEcfmCcMepNode->u4IfIndex = pPortInfo->u4IfIndex;
                    gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
                    pMepInfo =
                        (tEcfmCcMepInfo *)
                        RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                       gpEcfmCcMepNode, NULL);
                    while ((pMepInfo != NULL)
                           && (pMepInfo->u4IfIndex == u4IfIndex)
                           && (pMepInfo->u4ContextId ==
                               ECFM_CC_CURR_CONTEXT_ID ()))
                    {
                        if (pMepInfo->u1Direction != ECFM_MP_DIR_UP)
                        {
                            pMepInfo =
                                RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                               pMepInfo, NULL);
                            continue;
                        }
                        pRmepInfo =
                            (tEcfmCcRMepDbInfo *)
                            TMO_DLL_First (&(pMepInfo->RMepDb));

                        pPortList =
                            UtilPlstAllocLocalPortList (sizeof
                                                        (tLocalPortListExt));
                        if (pPortList == NULL)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "EcfmMbsmHandleCardRmvForUpMep: Error in allocating memory "
                                         "for pPortList\r\n");
                            ECFM_CC_RELEASE_CONTEXT ();
                            return MBSM_FAILURE;
                        }
                        ECFM_MEMSET (pPortList, ECFM_INIT_VAL,
                                     sizeof (tLocalPortListExt));

                        while (pRmepInfo != NULL)
                        {
                            /* Get MEP Node with which this Remote
                             * MEP is associated */
                            if (EcfmL2IwfMiGetVlanEgressPorts
                                (ECFM_CC_CURR_CONTEXT_ID (),
                                 pMepInfo->u4PrimaryVidIsid,
                                 pPortList) == ECFM_SUCCESS)
                            {
                                /* Disable the port on which Up MEP is created */
                                ECFM_RESET_MEMBER_PORT (pPortList,
                                                        pPortInfo->u2PortNum);

                                for (u2ByteInd = ECFM_INIT_VAL;
                                     u2ByteInd < ECFM_PORT_LIST_SIZE;
                                     u2ByteInd++)
                                {
                                    if (pPortList[u2ByteInd] == 0)
                                    {
                                        continue;
                                    }
                                    u1PortFlag = pPortList[u2ByteInd];
                                    for (u2BitIndex = 0;
                                         ((u2BitIndex < BITS_PER_BYTE)
                                          &&
                                          (EcfmUtilQueryBitListTable
                                           (u1PortFlag, u2BitIndex) != 0));
                                         u2BitIndex++)
                                    {
                                        u2Port =
                                            (UINT2) ((u2ByteInd *
                                                      BITS_PER_BYTE) +
                                                     EcfmUtilQueryBitListTable
                                                     (u1PortFlag, u2BitIndex));
                                        pLocalPortInfo = NULL;
                                        pLocalPortInfo =
                                            ECFM_CC_GET_PORT_INFO (u2Port);
                                        if (pLocalPortInfo != NULL)
                                        {
                                            if (EcfmUtilGetIfInfo
                                                (pLocalPortInfo->u4IfIndex,
                                                 &CfaIfInfo) == CFA_SUCCESS)
                                            {
                                                if (CfaIfInfo.
                                                    u1IfOperStatus != CFA_IF_NP)
                                                {
                                                    b1PortPresent = ECFM_TRUE;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (b1PortPresent == ECFM_FALSE)
                                {
                                    pMepInfo->b1MepCcmOffloadHwStatus =
                                        ECFM_FALSE;

                                    if (pMepInfo->u1Direction ==
                                        ECFM_MP_DIR_DOWN)
                                    {
                                        UtilPlstReleaseLocalPortList
                                            (pPortList);
                                        continue;
                                    }

                                    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                                                 sizeof (tEcfmCcPduSmInfo));
                                    /* Assign MepInfo and Remote MEP Info 
                                     * in the PduSmInfo for calling the SM */
                                    PduSmInfo.pRMepInfo = pRmepInfo;
                                    PduSmInfo.pMepInfo = pRmepInfo->pMepInfo;

                                    /* Call the Remote MEP State Machine for TIME_OUT event */
                                    EcfmCcClntRmepSm (&PduSmInfo,
                                                      ECFM_SM_EV_RMEP_TIMEOUT);

                                    /* Stop On Demand Operation for CC Task.
                                     * i.e. Stop Single Ended LM Transaction 
                                     * if running */
                                    if (pMepInfo->LmInfo.u1TxLmmStatus ==
                                        ECFM_TX_STATUS_NOT_READY)
                                    {
                                        if (EcfmCcUtilPostTransaction (pMepInfo,
                                                                       ECFM_LM_STOP_TRANSACTION)
                                            == ECFM_FAILURE)
                                        {
                                            ECFM_GLB_TRC
                                                (ECFM_CC_CURR_CONTEXT_ID (),
                                                 ECFM_INIT_SHUT_TRC |
                                                 ECFM_CONTROL_PLANE_TRC |
                                                 ECFM_ALL_FAILURE_TRC,
                                                 "EcfmMbsmHandleCardRmvForUpMep: Stopping On Demand"
                                                 " Operation on CC Task for Up MEP returned Failure\n");
                                            UtilPlstReleaseLocalPortList
                                                (pPortList);
                                            ECFM_CC_RELEASE_CONTEXT ();
                                            return MBSM_FAILURE;
                                        }
                                        pMepInfo->LmInfo.u1TxLmmStatus =
                                            ECFM_TX_STATUS_STOP;
                                    }
                                }
                            }

                            /* Get Next RMEP Node from the Global RBTRee maintained */
                            pRmepInfo =
                                (tEcfmCcRMepDbInfo *)
                                TMO_DLL_Next (&(pMepInfo->RMepDb),
                                              &(pRmepInfo->MepDbDllNode));

                        }
                        UtilPlstReleaseLocalPortList (pPortList);
                        pMepInfo =
                            RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                           pMepInfo, NULL);
                    }
                }
                u2PrevPort = u2PortNum;
            }
            ECFM_CC_RELEASE_CONTEXT ();
        }
        while (EcfmCcGetNextActiveContext (u4PrevContextId,
                                           &u4ContextId) == ECFM_SUCCESS);
    }
    /* Call routine for Up MEP to stop On Demand Operation for LBLT Task */
    if (EcfmMbsmStopOnDemandOprtLbLtTaskOnUpMep () == MBSM_FAILURE)
    {
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep
 *
 *    Description      : This rotuine checks if any on demand operation is 
 *                       running in LBLT Task on the port which is removed at
 *                       run time then it stops that operation 
 *    INPUT            : pPortInfo - Inserted port list     
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 *****************************************************************************/
PRIVATE INT4
EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount = ECFM_INIT_VAL;
    UINT4               u4PortNo = ECFM_INIT_VAL;
    UINT1               u1Result = ECFM_INIT_VAL;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    ECFM_LBLT_LOCK ();
    while (u4PortCount != 0)
    {
        MBSM_IS_MEMBER_PORT (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                             u4PortNo, u1Result);

        if (u1Result == MBSM_TRUE)
        {
            if (EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMepForPort (u4PortNo)
                != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }

            if (EcfmMbsmSispStopOnDemandOprtLbLtTaskOnDownMepForPort (u4PortNo)
                != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }
        }
        u4PortCount--;
        u4PortNo++;
    }
    ECFM_LBLT_UNLOCK ();
    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmStopOnDemandOprtLbLtTaskOnUpMep
 *
 *    Description      : This rotuine checks if any on demand operation is 
 *                       running on the port which is removed at run time 
 *                       then it stops that operation 
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *****************************************************************************/
PRIVATE INT4
EcfmMbsmStopOnDemandOprtLbLtTaskOnUpMep (VOID)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    tEcfmLbLtPortInfo  *pLocalPortInfo = NULL;
    UINT1              *pPortList = NULL;
    tEcfmCfaIfInfo      CfaIfInfo;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4PrevContextId = ECFM_INIT_VAL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;
    UINT2               u2PrevPort = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;
    BOOL1               b1PortPresent = ECFM_FALSE;

    ECFM_MEMSET (&CfaIfInfo, ECFM_INIT_VAL, sizeof (tCfaIfInfo));
    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);

    ECFM_LBLT_LOCK ();
    if (EcfmLbLtGetNextActiveContext (0, &u4ContextId) == ECFM_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;
            if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
            {
                continue;
            }

            if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))
            {
                ECFM_LBLT_RELEASE_CONTEXT ();
                continue;
            }
            while (EcfmL2IwfGetNextValidPortForContext
                   (ECFM_LBLT_CURR_CONTEXT_ID (), u2PrevPort, &u2PortNum,
                    &u4IfIndex) == L2IWF_SUCCESS)
            {
                pLbLtPortInfo = NULL;
                pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u2PortNum);
                if (pLbLtPortInfo != NULL)
                {
                    gpEcfmLbLtMepNode->u2PortNum = u2PortNum;
                    pMepInfo = NULL;
                    pMepInfo = (tEcfmLbLtMepInfo *) RBTreeGetNext
                        (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);

                    while ((pMepInfo != NULL)
                           && (pMepInfo->u2PortNum == u2PortNum))
                    {
                        if (pMepInfo->u1Direction != ECFM_MP_DIR_UP)
                        {

                            pMepInfo =
                                (tEcfmLbLtMepInfo *)
                                RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                               (tRBElem *) pMepInfo, NULL);
                            continue;

                        }
                        pPortList =
                            UtilPlstAllocLocalPortList (sizeof
                                                        (tLocalPortListExt));
                        if (pPortList == NULL)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "EcfmMbsmStopOnDemandOprtLbLtTaskOnUpMep: Error in allocating memory "
                                         "for pPortList\r\n");
                            ECFM_LBLT_RELEASE_CONTEXT ();
                            ECFM_LBLT_UNLOCK ();
                            return MBSM_FAILURE;
                        }
                        ECFM_MEMSET (pPortList, ECFM_INIT_VAL,
                                     sizeof (tLocalPortListExt));

                        if (EcfmL2IwfMiGetVlanEgressPorts
                            (ECFM_LBLT_CURR_CONTEXT_ID (),
                             pMepInfo->u4PrimaryVidIsid,
                             pPortList) == ECFM_SUCCESS)
                        {
                            /* Disable the port on which Up MEP is created */
                            ECFM_RESET_MEMBER_PORT (pPortList,
                                                    pLbLtPortInfo->u2PortNum);

                            for (u2ByteInd = ECFM_INIT_VAL;
                                 u2ByteInd < ECFM_PORT_LIST_SIZE; u2ByteInd++)
                            {
                                if (pPortList[u2ByteInd] == 0)
                                {
                                    continue;
                                }
                                u1PortFlag = pPortList[u2ByteInd];
                                for (u2BitIndex = 0;
                                     ((u2BitIndex < BITS_PER_BYTE)
                                      &&
                                      (EcfmUtilQueryBitListTable
                                       (u1PortFlag, u2BitIndex) != 0));
                                     u2BitIndex++)
                                {
                                    u2Port =
                                        (UINT2) ((u2ByteInd *
                                                  BITS_PER_BYTE) +
                                                 EcfmUtilQueryBitListTable
                                                 (u1PortFlag, u2BitIndex));
                                    pLocalPortInfo = NULL;
                                    pLocalPortInfo =
                                        ECFM_LBLT_GET_PORT_INFO (u2Port);
                                    if (pLocalPortInfo != NULL)
                                    {
                                        if (EcfmUtilGetIfInfo
                                            (pLocalPortInfo->u4IfIndex,
                                             &CfaIfInfo) == CFA_SUCCESS)
                                        {
                                            if (CfaIfInfo.
                                                u1IfOperStatus == CFA_IF_NP)
                                            {
                                                b1PortPresent = ECFM_TRUE;
                                            }
                                        }
                                    }
                                }
                            }
                            if (b1PortPresent == ECFM_FALSE)
                            {
                                /* Stopping on demand operation on LBLT Task */
                                /* Stop LB Transaction if running */
                                if (pMepInfo->LbInfo.u1TxLbmStatus ==
                                    ECFM_TX_STATUS_NOT_READY)
                                {
                                    if (EcfmLbLtUtilPostTransaction
                                        (pMepInfo,
                                         ECFM_LB_STOP_TRANSACTION) ==
                                        ECFM_FAILURE)
                                    {
                                        ECFM_GLB_TRC
                                            (ECFM_LBLT_CURR_CONTEXT_ID (),
                                             ECFM_INIT_SHUT_TRC |
                                             ECFM_CONTROL_PLANE_TRC |
                                             ECFM_ALL_FAILURE_TRC,
                                             "EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep: Stopping "
                                             "LBM transaction returned Failure\n");
                                        UtilPlstReleaseLocalPortList
                                            (pPortList);
                                        ECFM_LBLT_RELEASE_CONTEXT ();
                                        ECFM_LBLT_UNLOCK ();
                                        return MBSM_FAILURE;
                                    }
                                    pMepInfo->LbInfo.u1TxLbmStatus =
                                        ECFM_TX_STATUS_STOP;
                                }
                                /* Stop TST Transaction if running */
                                if (pMepInfo->TstInfo.u1TstStatus ==
                                    ECFM_TX_STATUS_NOT_READY)
                                {
                                    if (EcfmLbLtUtilPostTransaction
                                        (pMepInfo,
                                         ECFM_TST_STOP_TRANSACTION) ==
                                        ECFM_FAILURE)
                                    {
                                        ECFM_GLB_TRC
                                            (ECFM_LBLT_CURR_CONTEXT_ID (),
                                             ECFM_INIT_SHUT_TRC |
                                             ECFM_CONTROL_PLANE_TRC |
                                             ECFM_ALL_FAILURE_TRC,
                                             "EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep: Stopping "
                                             "TST transaction returned Failure\n");
                                        UtilPlstReleaseLocalPortList
                                            (pPortList);
                                        ECFM_LBLT_RELEASE_CONTEXT ();
                                        ECFM_LBLT_UNLOCK ();
                                        return MBSM_FAILURE;
                                    }
                                    pMepInfo->TstInfo.u1TstStatus =
                                        ECFM_TX_STATUS_STOP;
                                }
                                /* Stop DM Transaction if running */
                                if (pMepInfo->DmInfo.u1DmStatus ==
                                    ECFM_TX_STATUS_NOT_READY)
                                {
                                    if (EcfmLbLtUtilPostTransaction
                                        (pMepInfo,
                                         ECFM_DM_STOP_TRANSACTION) ==
                                        ECFM_FAILURE)
                                    {
                                        ECFM_GLB_TRC
                                            (ECFM_LBLT_CURR_CONTEXT_ID (),
                                             ECFM_INIT_SHUT_TRC |
                                             ECFM_CONTROL_PLANE_TRC |
                                             ECFM_ALL_FAILURE_TRC,
                                             "EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep: Stopping "
                                             "DM transaction returned Failure\n");
                                        UtilPlstReleaseLocalPortList
                                            (pPortList);
                                        ECFM_LBLT_RELEASE_CONTEXT ();
                                        ECFM_LBLT_UNLOCK ();
                                        return MBSM_FAILURE;
                                    }
                                    pMepInfo->DmInfo.u1DmStatus =
                                        ECFM_TX_STATUS_STOP;
                                }
                            }
                        }
                        UtilPlstReleaseLocalPortList (pPortList);

                        pMepInfo =
                            (tEcfmLbLtMepInfo *)
                            RBTreeGetNext (ECFM_LBLT_PORT_MEP_TABLE,
                                           (tRBElem *) pMepInfo, NULL);
                    }
                }
                u2PrevPort = u2PortNum;
            }
            ECFM_LBLT_RELEASE_CONTEXT ();
        }
        while (EcfmLbLtGetNextActiveContext (u4PrevContextId,
                                             &u4ContextId) == ECFM_SUCCESS);
    }

    ECFM_LBLT_UNLOCK ();
    return MBSM_SUCCESS;
}

/****************************************************************************
 * Function Name      : EcfmMbsmIsPortPresent
 *
 * Description        : This rotuine checks if any port belonging to the 
 *                      given MEP (Up/Down) in the system is present or not.
 *
 * Input(s)           : u4IfIndex- Interface Index
 *                      u4VlanId- Vlan Identifier            
 *                      u1Direction - Mep Direction
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_TRUE / ECFM_FALSE
 *****************************************************************************/
PUBLIC INT4
EcfmMbsmIsPortPresent (UINT4 u4IfIndex, UINT4 u4VlanId, UINT1 u1Direction)
{
    UINT1              *pPortList = NULL;
    tEcfmCfaIfInfo      CfaIfInfo;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4CurrIfIndex = ECFM_INIT_VAL;
    UINT2               u2ByteInd = ECFM_INIT_VAL;
    UINT2               u2BitIndex = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2Port = ECFM_INIT_VAL;
    UINT1               u1PortFlag = ECFM_INIT_VAL;

    ECFM_MEMSET (&CfaIfInfo, ECFM_INIT_VAL, sizeof (tCfaIfInfo));

    /* For MPLS-TP OAM, dummy ifIndices are reserved one per context */
    if ((u4IfIndex >= (CFA_MIN_IVR_IF_INDEX + ECFM_INIT_VAL)) ||
        (u4IfIndex < (CFA_MIN_IVR_IF_INDEX + ECFM_MAX_CONTEXTS)))
    {
        return ECFM_TRUE;
    }

    /* For Down MEP check this port alone */
    if (u1Direction == ECFM_MP_DIR_DOWN)
    {
        if (EcfmUtilGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_SUCCESS)
        {
            if (CfaIfInfo.u1IfOperStatus == CFA_IF_NP)
            {
                return ECFM_FALSE;
            }
        }
        return ECFM_TRUE;
    }
    else
    {
        if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX
            (u4IfIndex, &u4ContextId, &u2LocalPort) == VCM_SUCCESS)
        {
            pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortListExt));
            if (pPortList == NULL)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmMbsmIsPortPresent: Error in allocating memory "
                             "for pPortList\r\n");
                return ECFM_FALSE;
            }
            ECFM_MEMSET (pPortList, ECFM_INIT_VAL, sizeof (tLocalPortListExt));

            /* Up MEP: Check all the egress ports for this MEP */
            if (EcfmL2IwfMiGetVlanEgressPorts (u4ContextId,
                                               u4VlanId,
                                               pPortList) == ECFM_SUCCESS)
            {
                /* Disable the port on which Up MEP is created */
                ECFM_RESET_MEMBER_PORT (pPortList, u2LocalPort);

                for (u2ByteInd = ECFM_INIT_VAL; u2ByteInd < ECFM_PORT_LIST_SIZE;
                     u2ByteInd++)
                {
                    if (pPortList[u2ByteInd] == 0)
                    {
                        continue;
                    }
                    u1PortFlag = pPortList[u2ByteInd];
                    for (u2BitIndex = 0;
                         ((u2BitIndex < BITS_PER_BYTE)
                          && (EcfmUtilQueryBitListTable (u1PortFlag, u2BitIndex)
                              != 0)); u2BitIndex++)
                    {
                        u2Port =
                            (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                     EcfmUtilQueryBitListTable (u1PortFlag,
                                                                u2BitIndex));
                        if (ECFM_GET_IFINDEX_FROM_LOCAL_PORT
                            (u4ContextId, u2Port,
                             &u4CurrIfIndex) == VCM_SUCCESS)
                        {
                            if (EcfmUtilGetIfInfo (u4CurrIfIndex,
                                                   &CfaIfInfo) == CFA_SUCCESS)
                            {
                                if (CfaIfInfo.u1IfOperStatus != CFA_IF_NP)
                                {
                                    UtilPlstReleaseLocalPortList (pPortList);
                                    return ECFM_TRUE;
                                }
                            }
                        }
                    }
                }
            }
            UtilPlstReleaseLocalPortList (pPortList);
        }
        return ECFM_FALSE;
    }
}

/****************************************************************************
 *    Function Name    : EcfmMbsmPgmDownMepForPortInHw     
 *
 *    Description      : This rotuine checks if upcoming port belongs to Down 
 *                       MEP in the system and it is not started in hardware
 *                       then start it in hardware.
 *    INPUT            : u4PortNo - Port Number             
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmPgmDownMepForPortInHw (UINT4 u4PortNo)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4PortNo, &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return MBSM_SUCCESS;
    }
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) == ECFM_FAILURE)
    {
        return MBSM_SUCCESS;
    }

    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);

    if ((pCcPortInfo != NULL) && (pCcPortInfo->u2ChannelPortNum != 0))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return MBSM_SUCCESS;
    }

    /* Check if any MEP exists on this port */
    if (pCcPortInfo != NULL)
    {
        gpEcfmCcMepNode->u4IfIndex = pCcPortInfo->u4IfIndex;
        gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        pMepNode = NULL;
        pMepNode =
            (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                              gpEcfmCcMepNode, NULL);

        while ((pMepNode != NULL) &&
               (pMepNode->u4IfIndex == pCcPortInfo->u4IfIndex) &&
               (pMepNode->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ()))
        {
            /* Check if Offload is Enabled on the Down MEP and it 
             * is not running in Hardware then start it in H/W */
            if ((pMepNode->u1Direction == ECFM_MP_DIR_DOWN) &&
                (pMepNode->b1MepCcmOffloadHwStatus != ECFM_TRUE))
            {
                if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
                {
                    if (EcfmCcmOffCreateTxRxForMep (pMepNode) == ECFM_FAILURE)
                    {
                        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (),
                                      ECFM_INIT_SHUT_TRC |
                                      ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmMbsmHandleCardInsertForDownMep:"
                                      " EcfmCcmOffCreateTxRxForMep func"
                                      " returned failure\n");
                        ECFM_CC_RELEASE_CONTEXT ();
                        return MBSM_FAILURE;
                    }
                }
            }
            pMepNode = (tEcfmCcMepInfo *) RBTreeGetNext
                (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, (tRBElem *) pMepNode, NULL);
        }
    }
    ECFM_CC_RELEASE_CONTEXT ();

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name    : EcfmMbsmSispPgmDownMepForPort                        
 *                                                                           
 *    Description      : This rotuine checks if upcoming port belongs to Down 
 *                       MEP in the system and it is not started in hardware
 *                       then start it in hardware.
 *    INPUT            : u4PortNo - Port Number             
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmSispPgmDownMepForPort (UINT4 u4PortNo)
{
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortCnt = ECFM_INIT_VAL;
    UINT4               u4PortNum = ECFM_INIT_VAL;
    UINT4               u4PortId = ECFM_INIT_VAL;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    if (EcfmVcmSispGetSispPortsInfoOfPhysicalPort (u4PortNo, VCM_FALSE,
                                                   (VOID *) au4SispPorts,
                                                   &u4PortCnt)
        != ECFM_VCM_SUCCESS)
    {
        /* This port has no logical interfaces or secondary context
         * mapping
         * */
        return MBSM_SUCCESS;
    }

    for (; u4PortNum < u4PortCnt; u4PortNum++)
    {
        /* The following function will select the context and perform
         * based on the same internally. Hence after finishing the operations
         * the context should be restored */

        u4PortId = au4SispPorts[u4PortNum];

        if (EcfmMbsmPgmDownMepForPortInHw (u4PortId) != MBSM_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }                            /*for all the SISP logical ports */

    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmRemoveDownMepForPortFromHw
 *
 *    Description      : This rotuine checks if upcoming port belongs to Down 
 *                       MEP in the system and then disable CCM offload
 *                       for that MEP in hardware
 *
 *    INPUT            : u4PortNo - Port Number             
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmRemoveDownMepForPortFromHw (UINT4 u4PortNo)
{
    tEcfmCcRMepDbInfo   RMepNode;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pCcPortInfo = NULL;
    tEcfmCcRMepDbInfo  *pRmepInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4TempContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    ECFM_MEMSET (&RMepNode, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4PortNo, &u4TempContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return MBSM_SUCCESS;
    }

    if (ECFM_CC_SELECT_CONTEXT (u4TempContextId) == ECFM_FAILURE)
    {
        return MBSM_SUCCESS;
    }

    pCcPortInfo = ECFM_CC_GET_PORT_INFO (u2LocalPort);

    if ((pCcPortInfo != NULL) && (pCcPortInfo->u2ChannelPortNum != 0))
    {
        ECFM_CC_RELEASE_CONTEXT ();
        return MBSM_SUCCESS;
    }

    /* Check if any MEP exists on this port */
    if (pCcPortInfo != NULL)
    {
        gpEcfmCcMepNode->u4IfIndex = pCcPortInfo->u4IfIndex;
        gpEcfmCcMepNode->u4ContextId = ECFM_CC_CURR_CONTEXT_ID ();
        pMepNode =
            (tEcfmCcMepInfo *) RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE,
                                              gpEcfmCcMepNode, NULL);
        while ((pMepNode != NULL) &&
               (pMepNode->u4IfIndex == pCcPortInfo->u4IfIndex) &&
               (pMepNode->u4ContextId == ECFM_CC_CURR_CONTEXT_ID ()))
        {
            if (pMepNode->u1Direction != ECFM_MP_DIR_DOWN)
            {
                pMepNode =
                    RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode,
                                   NULL);
                continue;
            }
            pRmepInfo =
                (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(pMepNode->RMepDb));
            while (pRmepInfo != NULL)
            {
                /* Get MEP Node with which this Remote
                 * MEP is associated */
                pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
                /* For Down MEP raise connectivity loss trap.
                 * For Up MEP check if all LCs sends receives notification 
                 * for connectivity loss trap then generate trap */
                if (pMepNode->u1Direction == ECFM_MP_DIR_UP)
                {
                    continue;
                }

                ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                             sizeof (tEcfmCcPduSmInfo));
                /* Assign MepInfo and Remote MEP Info 
                 * in the PduSmInfo for calling the SM */
                PduSmInfo.pRMepInfo = pRmepInfo;
                PduSmInfo.pMepInfo = pMepNode;

                /* Raise Connectivity Loss trap */
                /* Call the Remote MEP State Machine for TIME_OUT event */
                EcfmCcClntRmepSm (&PduSmInfo, ECFM_SM_EV_RMEP_TIMEOUT);

                /* Stop running on demand operation for CC Task */
                /* i.e. Stop Single Ended LM Transaction if running */
                if (pMepNode->LmInfo.u1TxLmmStatus == ECFM_TX_STATUS_NOT_READY)
                {
                    if (EcfmCcUtilPostTransaction (pMepNode,
                                                   ECFM_LM_STOP_TRANSACTION)
                        == ECFM_FAILURE)
                    {
                        ECFM_GLB_TRC (ECFM_CC_CURR_CONTEXT_ID (),
                                      ECFM_INIT_SHUT_TRC |
                                      ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmMbsmHandleCardRmvForDownMep: Stopping On Demand"
                                      " Operation on CC Task for Down MEP returned Failure\n");
                        ECFM_CC_RELEASE_CONTEXT ();
                        return MBSM_FAILURE;
                    }
                    pMepNode->LmInfo.u1TxLmmStatus = ECFM_TX_STATUS_STOP;
                }
                /* Get Next RMEP Node from the Global RBTRee maintained */
                pRmepInfo =
                    (tEcfmCcRMepDbInfo *) TMO_DLL_Next (&(pMepNode->RMepDb),
                                                        &(pRmepInfo->
                                                          MepDbDllNode));
            }
            pMepNode =
                RBTreeGetNext (ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE, pMepNode,
                               NULL);
        }
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name    : EcfmMbsmSispHandleCardRmvForDownMep                  
 *                                                                           
 *    Description      : This rotuine checks if upcoming port belongs to Down 
 *                       MEP in the system and then disable CCM offload
 *                       for that MEP in hardware
 *
 *    INPUT            : u4PortNo - Port Number             
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EcfmMbsmSispHandleCardRmvForDownMep (UINT4 u4PortNo)
{
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortCnt = ECFM_INIT_VAL;
    UINT4               u4PortNum = ECFM_INIT_VAL;
    UINT4               u4PortId = ECFM_INIT_VAL;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    if (EcfmVcmSispGetSispPortsInfoOfPhysicalPort (u4PortNo, VCM_FALSE,
                                                   (VOID *) au4SispPorts,
                                                   &u4PortCnt)
        != ECFM_VCM_SUCCESS)
    {
        /* This port has no logical interfaces or secondary context
         * mapping
         * */
        return MBSM_SUCCESS;
    }

    for (; u4PortNum < u4PortCnt; u4PortNum++)
    {
        /* The following function will select the context and perform
         * based on the same internally. Hence after finishing the operations
         * the context should be restored */

        u4PortId = au4SispPorts[u4PortNum];

        if (EcfmMbsmRemoveDownMepForPortFromHw (u4PortId) != MBSM_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }                            /*for all the SISP logical ports */

    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMepForPort
 *
 *    Description      : This rotuine checks if any on demand operation is 
 *                       running in LBLT Task on the port which is removed at
 *                       run time then it stops that operation 
 *    INPUT            : u4PortNo - Port Number             
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 *****************************************************************************/
PRIVATE INT4
EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMepForPort (UINT4 u4PortNo)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtPortInfo  *pLbLtPortInfo = NULL;
    UINT4               u4TempContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmLbLtMepNode, ECFM_INIT_VAL, ECFM_LBLT_MEP_INFO_SIZE);

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4PortNo, &u4TempContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return MBSM_SUCCESS;
    }

    if (ECFM_LBLT_SELECT_CONTEXT (u4TempContextId) == ECFM_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return MBSM_SUCCESS;
    }

    pLbLtPortInfo = ECFM_LBLT_GET_PORT_INFO (u2LocalPort);
    if (pLbLtPortInfo != NULL && pLbLtPortInfo->u2ChannelPortNum != 0)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        return MBSM_SUCCESS;
    }

    if (pLbLtPortInfo != NULL)
    {
        gpEcfmLbLtMepNode->u2PortNum = u2LocalPort;
        pMepNode = NULL;
        pMepNode = (tEcfmLbLtMepInfo *) RBTreeGetNext
            (ECFM_LBLT_PORT_MEP_TABLE, gpEcfmLbLtMepNode, NULL);

        while ((pMepNode != NULL) && (pMepNode->u2PortNum == u2LocalPort))
        {
            if (pMepNode->u1Direction == ECFM_MP_DIR_DOWN)
            {
                /* Stop LB Transaction if running */
                if (pMepNode->LbInfo.u1TxLbmStatus == ECFM_TX_STATUS_NOT_READY)
                {
                    if (EcfmLbLtUtilPostTransaction (pMepNode,
                                                     ECFM_LB_STOP_TRANSACTION)
                        == ECFM_FAILURE)
                    {
                        ECFM_GLB_TRC (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      ECFM_INIT_SHUT_TRC |
                                      ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep: Stopping "
                                      "LBM transaction returned Failure\n");
                        ECFM_LBLT_RELEASE_CONTEXT ();
                        ECFM_LBLT_UNLOCK ();
                        return MBSM_FAILURE;
                    }
                    pMepNode->LbInfo.u1TxLbmStatus = ECFM_TX_STATUS_STOP;
                }
                /* Stop TST Transaction if running */
                if (pMepNode->TstInfo.u1TstStatus == ECFM_TX_STATUS_NOT_READY)
                {
                    if (EcfmLbLtUtilPostTransaction (pMepNode,
                                                     ECFM_TST_STOP_TRANSACTION)
                        == ECFM_FAILURE)
                    {
                        ECFM_GLB_TRC (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      ECFM_INIT_SHUT_TRC |
                                      ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep: Stopping "
                                      "TST transaction returned Failure\n");
                        ECFM_LBLT_RELEASE_CONTEXT ();
                        ECFM_LBLT_UNLOCK ();
                        return MBSM_FAILURE;
                    }
                    pMepNode->TstInfo.u1TstStatus = ECFM_TX_STATUS_STOP;
                }
                /* Stop DM Transaction if running */
                if (pMepNode->DmInfo.u1DmStatus == ECFM_TX_STATUS_NOT_READY)
                {
                    if (EcfmLbLtUtilPostTransaction (pMepNode,
                                                     ECFM_DM_STOP_TRANSACTION)
                        == ECFM_FAILURE)
                    {
                        ECFM_GLB_TRC (ECFM_LBLT_CURR_CONTEXT_ID (),
                                      ECFM_INIT_SHUT_TRC |
                                      ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMep: Stopping "
                                      "DM transaction returned Failure\n");
                        ECFM_LBLT_RELEASE_CONTEXT ();
                        ECFM_LBLT_UNLOCK ();
                        return MBSM_FAILURE;
                    }
                    pMepNode->DmInfo.u1DmStatus = ECFM_TX_STATUS_STOP;
                }
            }
            pMepNode = (tEcfmLbLtMepInfo *) RBTreeGetNext
                (ECFM_LBLT_PORT_MEP_TABLE, (tRBElem *) pMepNode, NULL);
        }
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return MBSM_SUCCESS;
}

/****************************************************************************
 *    Function Name    : EcfmMbsmSispStopOnDemandOprtLbLtTaskOnDownMepForPort
 *
 *    Description      : This rotuine checks if any on demand operation is 
 *                       running in LBLT Task on any of the SISP logical 
 *                       interface running over this port which is removed at
 *                       run time then it stops that operation 
 *
 *    INPUT            : u4PortNo - Port Number             
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 *****************************************************************************/
PRIVATE INT4
EcfmMbsmSispStopOnDemandOprtLbLtTaskOnDownMepForPort (UINT4 u4PortNo)
{
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortCnt = ECFM_INIT_VAL;
    UINT4               u4PortNum = ECFM_INIT_VAL;
    UINT4               u4PortId = ECFM_INIT_VAL;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    if (EcfmVcmSispGetSispPortsInfoOfPhysicalPort (u4PortNo, VCM_FALSE,
                                                   (VOID *) au4SispPorts,
                                                   &u4PortCnt)
        != ECFM_VCM_SUCCESS)
    {
        /* This port has no logical interfaces or secondary context
         * mapping
         * */
        return MBSM_SUCCESS;
    }

    for (; u4PortNum < u4PortCnt; u4PortNum++)
    {
        /* The following function will select the context and perform
         * based on the same internally. Hence after finishing the operations
         * the context should be restored */
        u4PortId = au4SispPorts[u4PortNum];

        if (EcfmMbsmStopOnDemandOprtLbLtTaskOnDownMepForPort (u4PortId)
            != MBSM_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }                            /*for all the SISP logical ports */

    return MBSM_SUCCESS;
}
#endif /*MBSM_WANTED */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ecfmmbsm.c                     */
/*-----------------------------------------------------------------------*/
