/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmnpwr.c,v 1.4 2016/02/08 10:30:19 siva Exp $
 *
 * Description: This file contains the NP wrappers for  ECFM module.
 *****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : EcfmNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tEcfmNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __ECFM_NP_WR_C
#define __ECFM_NP_WR_C

#include "cfminc.h"
#include "nputil.h"

PUBLIC UINT1
EcfmNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pEcfmNpModInfo = &(pFsHwNp->EcfmNpModInfo);

    if (NULL == pEcfmNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdLbrCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdLbrCounter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmClearRcvdLbrCounter (pEntry->pEcfmMepInfoParams);
            break;
        }
        case FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdTstCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdTstCounter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmClearRcvdTstCounter (pEntry->pEcfmMepInfoParams);
            break;
        }
        case FS_MI_ECFM_GET_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdLbrCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdLbrCounter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmGetRcvdLbrCounter (pEntry->pEcfmMepInfoParams,
                                           pEntry->pu4LbrIn);
            break;
        }
        case FS_MI_ECFM_GET_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdTstCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdTstCounter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmGetRcvdTstCounter (pEntry->pEcfmMepInfoParams,
                                           pEntry->pu4TstIn);
            break;
        }
        case FS_MI_ECFM_HW_CALL_NP_API:
        {
            tEcfmNpWrFsMiEcfmHwCallNpApi *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwCallNpApi;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwCallNpApi (pEntry->u1Type, pEntry->pEcfmHwInfo);
            break;
        }
        case FS_MI_ECFM_HW_DE_INIT:
        {
            tEcfmNpWrFsMiEcfmHwDeInit *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwDeInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmHwDeInit (pEntry->u4ContextId);
            break;
        }
        case FS_MI_ECFM_HW_GET_CAPABILITY:
        {
            tEcfmNpWrFsMiEcfmHwGetCapability *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCapability;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwGetCapability (pEntry->u4ContextId,
                                         pEntry->pu4HwCapability);
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS:
        {
            tEcfmNpWrFsMiEcfmHwGetCcmRxStatistics *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmRxStatistics;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwGetCcmRxStatistics (pEntry->u4ContextId,
                                              pEntry->u2RxFilterId,
                                              pEntry->pEcfmCcOffMepRxStats);
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS:
        {
            tEcfmNpWrFsMiEcfmHwGetCcmTxStatistics *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmTxStatistics;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwGetCcmTxStatistics (pEntry->u4ContextId,
                                              pEntry->u2TxFilterId,
                                              pEntry->pEcfmCcOffMepTxStats);
            break;
        }
        case FS_MI_ECFM_HW_GET_PORT_CC_STATS:
        {
            tEcfmNpWrFsMiEcfmHwGetPortCcStats *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetPortCcStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwGetPortCcStats (pEntry->u4ContextId,
                                          pEntry->u4IfIndex,
                                          pEntry->pEcfmCcOffPortStats);
            break;
        }
        case FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE:
        {
            tEcfmNpWrFsMiEcfmHwHandleIntQFailure *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwHandleIntQFailure;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwHandleIntQFailure (pEntry->u4ContextId,
                                             pEntry->pEcfmCcOffRxHandleInfo,
                                             pEntry->pu2RxHandle,
                                             pEntry->pb1More);
            break;
        }
        case FS_MI_ECFM_HW_INIT:
        {
            tEcfmNpWrFsMiEcfmHwInit *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmHwInit (pEntry->u4ContextId);
            break;
        }
        case FS_MI_ECFM_HW_REGISTER:
        {
            tEcfmNpWrFsMiEcfmHwRegister *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwRegister;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmHwRegister (pEntry->u4ContextId);
            break;
        }
        case FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE:
        {
            tEcfmNpWrFsMiEcfmHwSetVlanEtherType *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwSetVlanEtherType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmHwSetVlanEtherType (pEntry->u4ContextId,
                                            pEntry->u4IfIndex,
                                            pEntry->u2EtherTypeValue,
                                            pEntry->u1EtherType);
            break;
        }
        case FS_MI_ECFM_START_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartLbmTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLbmTransaction;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmStartLbmTransaction (pEntry->pEcfmMepInfoParams,
                                             pEntry->pEcfmConfigLbmInfo);
            break;
        }
        case FS_MI_ECFM_START_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartTstTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartTstTransaction;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmStartTstTransaction (pEntry->pEcfmMepInfoParams,
                                             pEntry->pEcfmConfigTstInfo);
            break;
        }
        case FS_MI_ECFM_STOP_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopLbmTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLbmTransaction;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmStopLbmTransaction (pEntry->pEcfmMepInfoParams);
            break;
        }
        case FS_MI_ECFM_STOP_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopTstTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopTstTransaction;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmStopTstTransaction (pEntry->pEcfmMepInfoParams);
            break;
        }
        case FS_MI_ECFM_TRANSMIT1_DM:
        {
            tEcfmNpWrFsMiEcfmTransmit1Dm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmit1Dm;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmTransmit1Dm (pEntry->u4ContextId, pEntry->u4IfIndex,
                                     pEntry->pu1DmPdu, pEntry->u2PduLength,
                                     pEntry->VlanTag, pEntry->u1Direction);
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMM:
        {
            tEcfmNpWrFsMiEcfmTransmitDmm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmm;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmTransmitDmm (pEntry->u4ContextId, pEntry->u4IfIndex,
                                     pEntry->pu1DmmPdu, pEntry->u2PduLength,
                                     pEntry->VlanTag, pEntry->u1Direction);
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMR:
        {
            tEcfmNpWrFsMiEcfmTransmitDmr *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmTransmitDmr (pEntry->u4ContextId, pEntry->u4IfIndex,
                                     pEntry->pu1DmrPdu, pEntry->u2PduLength,
                                     pEntry->VlanTag, pEntry->u1Direction);
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMM:
        {
            tEcfmNpWrFsMiEcfmTransmitLmm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmm;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmTransmitLmm (pEntry->u4ContextId, pEntry->u4IfIndex,
                                     pEntry->pu1LmmPdu, pEntry->u2PduLength,
                                     pEntry->VlanTag, pEntry->u1Direction);
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMR:
        {
            tEcfmNpWrFsMiEcfmTransmitLmr *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmTransmitLmr (pEntry->u4ContextId, pEntry->u4IfIndex,
                                     pEntry->pu1LmrPdu, pEntry->u2PduLength,
                                     pEntry->pVlanTag, pEntry->u1Direction);
            break;
        }
        case FS_MI_ECFM_START_LM:
        {
            tEcfmNpWrFsMiEcfmStartLm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLm;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmStartLm (pEntry->pHwLmInfo);
            break;
        }
        case FS_MI_ECFM_STOP_LM:
        {
            tEcfmNpWrFsMiEcfmStopLm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLm;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmStopLm (pEntry->pHwLmInfo);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_ECFM_MBSM_HW_CALL_NP_API:
        {
            tEcfmNpWrFsMiEcfmMbsmHwCallNpApi *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmMbsmHwCallNpApi;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmMbsmHwCallNpApi (pEntry->u1Type, pEntry->pEcfmHwInfo,
                                         pEntry->pSlotInfo);
            break;
        }
        case FS_MI_ECFM_MBSM_NP_INIT_HW:
        {
            tEcfmNpWrFsMiEcfmMbsmNpInitHw *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmMbsmNpInitHw;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmMbsmNpInitHw (pEntry->u4ContextId, pEntry->pSlotInfo);
            break;
        }
#endif
#if defined (Y1564_WANTED) || (RFC2544_WANTED)
        case FS_MI_ECFM_SLA_TEST:
        {
            tEcfmNpWrFsMiEcfmSlaTest  *pEntry = NULL;

            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmSlaTest;

            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiEcfmSlaTest (pEntry->pHwTestInfo);

            break;
        }
        case FS_MI_ECFM_ADD_HW_LOOPBACK_INFO:
        {
            tEcfmNpWrFsMiEcfmAddHwLoopbackInfo *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmAddHwLoopbackInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmAddHwLoopbackInfo (pEntry->pHwLoopbackInfo);
            break;
        }
        case FS_MI_ECFM_DEL_HW_LOOPBACK_INFO:
        {
            tEcfmNpWrFsMiEcfmDelHwLoopbackInfo *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmDelHwLoopbackInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiEcfmDelHwLoopbackInfo (pEntry->pHwLoopbackInfo);
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /*__ECFM_NP_WR_C */
