/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmthres.c,v 1.14 2014/03/16 11:34:12 siva Exp $
 *
 * Description: This file contains:
 *         1.Functionality to Process the TH-Vsm PDU Reception
 *         2.Functionality to Respond the TH-Vsm PDU with TH-VSR 
 *******************************************************************/
#include "cfminc.h"
PRIVATE UINT4 EcfmThResXmitVsrPdu PROTO ((tEcfmLbLtPduSmInfo *, tEcfmMacAddr));
PRIVATE VOID        EcfmThVsmRxPutVsrInfo
PROTO ((tEcfmLbLtRxThVsmPduInfo *, UINT1 **, UINT4));

/*******************************************************************************
 * Function Name      : EcfmLbLtClntParseThVsm
 *
 * Description        : This is called to parse the received TH VSM PDU and fill
 *                      required data structures
 * 
 *                      
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the state machine.
 *                      pu1Pdu - Pointer to the received PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntThParseVsm (tEcfmLbLtPduSmInfo * pPduSmInfo, UINT1 *pu1Pdu)
{
    tEcfmLbLtRxThVsmPduInfo *pThVsmInfo = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pThVsmInfo = &(pPduSmInfo->uPduInfo.Vsm);
    pu1Pdu = pu1Pdu + ECFM_CFM_HDR_SIZE;
    ECFM_GET_1BYTE (pThVsmInfo->au1OUI[ECFM_INDEX_ZERO], pu1Pdu);
    ECFM_GET_1BYTE (pThVsmInfo->au1OUI[ECFM_INDEX_ONE], pu1Pdu);
    ECFM_GET_1BYTE (pThVsmInfo->au1OUI[ECFM_INDEX_TWO], pu1Pdu);
    ECFM_GET_1BYTE (pThVsmInfo->u1SubOpCode, pu1Pdu);

    if (ECFM_MEMCMP
        (pThVsmInfo->au1OUI, ECFM_LBLT_ORG_UNIT_ID, ECFM_OUI_FIELD_SIZE) != 0)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParseVsm: OUI received doesn't match the system's OUI"
                       "\r\n");
        return ECFM_FAILURE;
    }

    if (pThVsmInfo->u1SubOpCode != ECFM_LBLT_TH_VSM_SUBOPCODE)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParseVsm: VSM frame is not the related to Throughput "
                       "\r\n");

        return ECFM_FAILURE;
    }

    ECFM_GET_1BYTE (pThVsmInfo->u1ReqCode, pu1Pdu);

    /* Check that First TLV Offset value received in the VSM PDU should be
     * 0 */
    if (pPduSmInfo->u1RxFirstTlvOffset != ECFM_TH_VSM_FIRST_TLV_OFFSET)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtClntParseVsm: VSM frame is discarded as the"
                       " first tlv offset if wrong is\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function           : EcfmLbltClntThProcessVsm
 *
 * Description        : This routine processes the received VSM PDU, and
 *                      trasnmits the VSR in response.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      pbFrwdVsm = Vsm Pdu is to be fwrded or not                
 * Output(s)          : None.
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbltClntThProcessVsm (tEcfmLbLtPduSmInfo * pPduSmInfo, BOOL1 * pbFrwdVsm)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
    tEcfmLbLtStackInfo *pStackInfo = NULL;
    tEcfmLbLtRxThVsmPduInfo *pThVsmInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    tEcfmMacAddr        MepMacAddr = { ECFM_INIT_VAL };

    ECFM_LBLT_TRC_FN_ENTRY ();
    pMepInfo = pPduSmInfo->pMepInfo;
    pTstInfo = &(pMepInfo->TstInfo);
    pStackInfo = pPduSmInfo->pStackInfo;
    pThVsmInfo = &(pPduSmInfo->uPduInfo.Vsm);

    if (ECFM_LBLT_GET_PORT_INFO (pStackInfo->u2PortNum) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessVsm: " "No Port Information \r\n");
        return ECFM_FAILURE;
    }
    /* Get the MEP's MAC Address */
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pStackInfo->u2PortNum)->u4IfIndex, MepMacAddr);
    /* Check if the Mac Address received in the VSM is same as that of the
     * receiving MEP
     */
    if (ECFM_COMPARE_MAC_ADDR (pPduSmInfo->RxDestMacAddr,
                               MepMacAddr) != ECFM_SUCCESS)
    {
        /* Discard the VSM frame */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessVsm: "
                       "discarding the received VSM frame\r\n");
        if (ECFM_LBLT_IS_MHF (pStackInfo))
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbltClntProcessVsm: "
                           "Vsm forwarded in case of MIP\r\n");
            *pbFrwdVsm = ECFM_TRUE;
        }
        return ECFM_FAILURE;
    }
    if (ECFM_LBLT_IS_MHF (pStackInfo))
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessVsm: "
                       "discarding the received VSM frame\r\n");
        return ECFM_FAILURE;
    }

    if (pThVsmInfo->u1ReqCode == ECFM_LBLT_TH_REQCODE_TST_CLEAR)
    {
#ifdef NPAPI_WANTED
        if (ECFM_HW_TST_SUPPORT () == ECFM_FALSE)
        {
            pTstInfo->u4ValidTstIn = ECFM_INIT_VAL;
        }
        else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
        {
            tEcfmMepInfoParams  EcfmMepInfoParams;
            ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                         sizeof (tEcfmMepInfoParams));
            EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
            EcfmMepInfoParams.u4IfIndex =
                ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
            EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
            EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
            EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;
            if (EcfmFsMiEcfmClearRcvdTstCounter (&EcfmMepInfoParams) !=
                FNP_SUCCESS)
            {
                return ECFM_FAILURE;
            }
        }
#else /*  */
        pTstInfo->u4ValidTstIn = ECFM_INIT_VAL;
#endif /*  */
    }

    /* Transmit the VSR */
    if (EcfmThResXmitVsrPdu (pPduSmInfo, MepMacAddr) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessVsm: "
                       "failure occurred while responding a VSR for VSM\r\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmThResXmitVsrPdu
 *
 * Description        : This routine formats and transmits the VSR PDUs.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 *                      MepMacAddr - Mac Address of the MEP receiving the VSM
 *                      frame.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE UINT4
EcfmThResXmitVsrPdu (tEcfmLbLtPduSmInfo * pPduSmInfo, tEcfmMacAddr MepMacAddr)
{

    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmLbLtRxThVsmPduInfo *pThVsmInfo = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmLbLtTstInfo   *pTstInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    INT4                i4RetVal = ECFM_INIT_VAL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4TstIn = ECFM_INIT_VAL;
    UINT2               u2PduLen = ECFM_INIT_VAL;
    UINT2               u2TypeLength = ECFM_INIT_VAL;
    UINT1               u1Flag = ECFM_INIT_VAL;
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT1              *pu1EthLlcHdr = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pMepInfo = pPduSmInfo->pMepInfo;
    pTstInfo = &pMepInfo->TstInfo;
#ifdef NPAPI_WANTED
    if (ECFM_HW_TST_SUPPORT () == ECFM_FALSE)
    {
        u4TstIn = pTstInfo->u4ValidTstIn;
    }
    else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
    {
        tEcfmMepInfoParams  EcfmMepInfoParams;
        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                     sizeof (tEcfmMepInfoParams));
        EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        EcfmMepInfoParams.u4IfIndex =
            ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
        EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;
        if (EcfmFsMiEcfmGetRcvdTstCounter (&EcfmMepInfoParams, &u4TstIn) !=
            FNP_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }
#else /*  */
    u4TstIn = pTstInfo->u4ValidTstIn;
#endif /*  */

    /* Allocates the CRU Buffer for formatting VSR */
    pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_PDU_SIZE, ECFM_INIT_VAL);
    if (pBuf == NULL)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC, "EcfmThResXmitVsrPdu:"
                       "CRU Buffer Allocation failed \r\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Get received LTM information from received PduSmInfo */
    pThVsmInfo = (&pPduSmInfo->uPduInfo.Vsm);
    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
    pu1PduStart = ECFM_LBLT_PDU;
    pu1PduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;

    /* Fill the CFM PDU Header */
    /* Set MD Level and Version in the first byte of CFM PDU Header */
    ECFM_SET_MDLEVEL (u1MdLvlAndVer, pPduSmInfo->pStackInfo->u1MdLevel);

    ECFM_SET_VERSION (u1MdLvlAndVer);
    ECFM_PUT_1BYTE (pu1PduEnd, u1MdLvlAndVer);

    /* Set in the next 1 byte OpCode */
    ECFM_PUT_1BYTE (pu1PduEnd, ECFM_OPCODE_VSR);

    /* Fill 1 byte of Flag field */
    u1Flag = pPduSmInfo->u1RxFlags;

    ECFM_PUT_1BYTE (pu1PduEnd, u1Flag);

    /* Fill 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1PduEnd, ECFM_TH_VSR_FIRST_TLV_OFFSET);

    /* Fill the other fields of VSR PDU */
    EcfmThVsmRxPutVsrInfo (pThVsmInfo, &pu1PduEnd, u4TstIn);

    /* Calculate the length of the formatted PDU */
    u2PduLen = (UINT2) (pu1PduEnd - pu1PduStart);

    /* copying the PDU in CRU Buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLen)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC |
                       ECFM_CONTROL_PLANE_TRC,
                       "EcfmThResXmitVsrPdu: Copying into Buffer failed \r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, FALSE);
        return ECFM_FAILURE;
    }
    /* Copy the Ethernet Header from VSM to VSR, as we also need the same VLAN
     * information in VSR also
     */
    ECFM_COPY_FROM_CRU_BUF (pPduSmInfo->pBuf, pu1EthLlcHdr, ECFM_INIT_VAL,
                            pPduSmInfo->u1CfmPduOffset);

    /* Copy 6byte Destination address as Original Address
     * Received in the LTM */
    ECFM_MEMCPY (pu1EthLlcHdr, pPduSmInfo->RxSrcMacAddr, ECFM_MAC_ADDR_LENGTH);
    pu1EthLlcHdr = pu1EthLlcHdr + ECFM_MAC_ADDR_LENGTH;

    ECFM_MEMCPY (pu1EthLlcHdr, MepMacAddr, ECFM_MAC_ADDR_LENGTH);
    pu1EthLlcHdr = pu1EthLlcHdr + ECFM_MAC_ADDR_LENGTH;

    /* Check if Outer Vlan Tag is Present */
    if (pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        pu1EthLlcHdr = pu1EthLlcHdr + ECFM_VLAN_HDR_SIZE;
    }
    /* Check if Inner Vlan Tag is Present */
    if (pPduSmInfo->VlanClassificationInfo.InnerVlanTag.u1TagType ==
        ECFM_VLAN_TAGGED)
    {
        pu1EthLlcHdr = pu1EthLlcHdr + ECFM_VLAN_HDR_SIZE;
    }
    /* Get TypeLength of CFM-PDU */
    ECFM_GET_2BYTE (u2TypeLength, pu1EthLlcHdr);
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)
    {
        pu1EthLlcHdr = pu1EthLlcHdr - ECFM_LLC_TYPE_LEN_FIELD_SIZE;
        /* Set LTR Pdu Length in LLC SNAP Header */
        ECFM_PUT_2BYTE (pu1EthLlcHdr, (u2PduLen + ECFM_LLC_SNAP_HDR_SIZE));
    }
    u2PduLen = pPduSmInfo->u1CfmPduOffset;

    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart, (UINT4) (u2PduLen))
        != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "EcfmThResXmitVsrPdu:Prepending into Buffer failed "
                       "\r\n");

        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmThResXmitVsrPdu: "
                        "Sending out VSR-PDU to lower layer...\r\n");

    pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
        (UINT2) pMepInfo->u4PrimaryVidIsid;

    i4RetVal = EcfmLbLtCtrlTxTransmitPkt (pBuf,
                                          pMepInfo->u2PortNum,
                                          pMepInfo->u4PrimaryVidIsid, 0, 0,
                                          pMepInfo->u1Direction,
                                          ECFM_OPCODE_VSR,
                                          &(pPduSmInfo->VlanClassificationInfo),
                                          &(pPduSmInfo->PbbClassificationInfo));

    if (i4RetVal != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbltClntProcessVsm: "
                       "failure occurred while formatting and transmitting VSR frame\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return i4RetVal;
}

/****************************************************************************
 * Function Name      : EcfmThVsmRxPutVsrInfo
 *
 * Description        : This rotuine is used to fill the VSR Info i.e. Req Code     
 *                      Tst Counter value
 *
 * Input(s)           : pThVsmInfo - pointer to strucuter containing VSM Pdu 
 *                      info 
 *                      ppu1PduEnd  - Pointer where Information needs to be 
 *                      filled
 *                      u4TstIn - Counter value of Tst Received
 *
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EcfmThVsmRxPutVsrInfo (tEcfmLbLtRxThVsmPduInfo * pThVsmInfo, UINT1 **ppu1PduEnd,
                       UINT4 u4TstIn)
{
    UINT1              *pu1Pdu = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *(ppu1PduEnd);

    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pThVsmInfo->au1OUI[ECFM_INDEX_ZERO]));
    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pThVsmInfo->au1OUI[ECFM_INDEX_ONE]));
    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pThVsmInfo->au1OUI[ECFM_INDEX_TWO]));

    /* Fill SubOpCode in VSR */
    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pThVsmInfo->u1SubOpCode));

    /* Fill next 1 byte as Request Code */
    ECFM_PUT_1BYTE (pu1Pdu, (UINT1) (pThVsmInfo->u1ReqCode));

    if (pThVsmInfo->u1ReqCode == ECFM_LBLT_TH_REQCODE_TST_FETCH)
    {
        ECFM_PUT_4BYTE (pu1Pdu, u4TstIn);
    }

    /* Fill end TLV to mark the end of the VSR PDU */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_END_TLV_TYPE);

    *(ppu1PduEnd) = pu1Pdu;

    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  End of File cfmdmres.c
 ******************************************************************************/
