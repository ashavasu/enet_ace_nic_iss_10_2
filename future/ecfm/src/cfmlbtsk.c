/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbtsk.c,v 1.34 2016/02/18 08:32:43 siva Exp $
 *
 * Description:  This file contains the functionality of the routine
 *               which is the entry point to the ECFM Module
 *******************************************************************/

#include "cfminc.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtMain
 *
 *    DESCRIPTION      : This function is the main routine for the LBLT task
 *                       that is waiting for a message and any event on the LBLT 
 *                       task.
 *
 *    INPUT            : pArg - Pointer to the Arguments
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtMain (INT1 *pArg)
{
    UINT4               u4Event = ECFM_INIT_VAL;
    tEcfmLbLtMsg       *pQMsg = NULL;
    UNUSED_PARAM (pArg);

    ECFM_SELF_TASK_ID (&ECFM_LBLT_TASK_ID);

    /* LBLT semaphore - created with initial value 0. */
    if (ECFM_CREATE_SEMAPHORE
        ((UINT1 *) ECFM_LBLT_SEM_NAME, &(ECFM_LBLT_SEM_ID)) != ECFM_SUCCESS)

    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    ECFM_GIVE_SEMAPHORE (ECFM_LBLT_SEM_ID);

    /* Indicate the Successful Intiation of LBLT Task to lrInitComplete */
    lrInitComplete (OSIX_SUCCESS);
    while (1)

    {
        if (ECFM_RECEIVE_EVENT
            (ECFM_LBLT_TASK_ID, ECFM_EV_ALL, OSIX_WAIT,
             &u4Event) == ECFM_SUCCESS)

        {

            /* Handle CFM PDUs received in the Packet Queue for CC Task */
            if (u4Event & ECFM_EV_CFM_PDU_IN_QUEUE)

            {
                if (ECFM_LBLT_PKT_QUEUE_ID != 0)
                {
                    while (ECFM_DEQUE_MSG
                           (ECFM_LBLT_PKT_QUEUE_ID, (UINT1 *) &pQMsg,
                            ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

                    {

                        /* Mutual exclusion  flag ON */
                        ECFM_LBLT_LOCK ();
                        EcfmLbLtPktQueueHandler (pQMsg);

                        /* Reslease Context */
                        ECFM_LBLT_RELEASE_CONTEXT ();

                        /* Release the buffer to pool */
                        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL,
                                             (UINT1 *) pQMsg);
                        pQMsg = NULL;

                        /* Mutual exclusion flag OFF */
                        ECFM_LBLT_UNLOCK ();
                    }
                }
            }

            /* Handle Configuration Messages received in the Configuration Queue
             * for LBLT Task */
            if (u4Event & ECFM_EV_CFG_MSG_IN_QUEUE)

            {

                /* Event received, dequeue messages for processing */
                if (ECFM_LBLT_CFG_QUEUE_ID != 0)
                {
                    while (ECFM_DEQUE_MSG
                           (ECFM_LBLT_CFG_QUEUE_ID, (UINT1 *) &pQMsg,
                            ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

                    {

                        /* Mutual exclusion  flag ON */
                        ECFM_LBLT_LOCK ();
                        EcfmLbLtCfgQueueHandler (pQMsg);

                        /* Reslease Context */
                        ECFM_LBLT_RELEASE_CONTEXT ();

                        /* Release the buffer to pool */
                        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL,
                                             (UINT1 *) pQMsg);
                        pQMsg = NULL;

                        /* Mutual exclusion flag OFF */
                        ECFM_LBLT_UNLOCK ();
                    }
                }
            }
            if (u4Event & ECFM_EV_TMR_EXP)

            {

                /* Mutual exclusion  flag ON */
                ECFM_LBLT_LOCK ();
                EcfmLbLtTmrExpHandler ();

                /* Mutual exclusion flag OFF */
                ECFM_LBLT_UNLOCK ();
            }
        }
    }
}

/****************************************************************************
 *    FUNCTION NAME    : EcfmLbLtTaskInit
 *
 *    DESCRIPTION      : This function creates the task queue, allocates
 *                       MemPools for task queue messages and creates LBLT
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS / ECFM_FAILURE
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtTaskInit ()
{

    /* Ecfm LBLT Task Packet Queue */
    if (ECFM_CREATE_MSG_QUEUE
        ((UINT1 *) ECFM_LBLT_PKT_QUEUE_NAME, ECFM_DEF_MSG_LEN,
         ECFM_LBLT_PKT_QUEUE_DEPTH, &ECFM_LBLT_PKT_QUEUE_ID) != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }

    /* Ecfm LBLT Task Configuration Queue */
    if (ECFM_CREATE_MSG_QUEUE
        ((UINT1 *) ECFM_LBLT_CFG_QUEUE_NAME, ECFM_DEF_MSG_LEN,
         ECFM_LBLT_CFG_QUEUE_DEPTH, &ECFM_LBLT_CFG_QUEUE_ID) != ECFM_SUCCESS)

    {
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtHandleTaskInitFailure
 *
 *    DESCRIPTION      : Function called when the initialization of LBLT Task
 *                       fails at any point during initialization.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC VOID
EcfmLbLtHandleTaskInitFailure ()
{
    tEcfmLbLtMsg       *ptEcfmLbLtMsg = NULL;

    /* Dequeue all CFM PDU received messages from LBLTs Packet Queue */
    if (ECFM_LBLT_PKT_QUEUE_ID != 0)

    {
        while (ECFM_DEQUE_MSG
               (ECFM_LBLT_PKT_QUEUE_ID, (UINT1 *) &ptEcfmLbLtMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {
            if (ptEcfmLbLtMsg == NULL)

            {
                continue;
            }

            /*  Release Memory */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) ptEcfmLbLtMsg);
        }
        /* Delete Q */
        ECFM_DELETE_MSG_QUEUE (ECFM_LBLT_PKT_QUEUE_ID);
        ECFM_LBLT_PKT_QUEUE_ID = ECFM_INIT_VAL;
    }

    /* Dequeue all Configuration Messages  from LBLTs Configuration Queue */
    if (ECFM_LBLT_CFG_QUEUE_ID != 0)

    {
        while (ECFM_DEQUE_MSG
               (ECFM_LBLT_CFG_QUEUE_ID, (UINT1 *) &ptEcfmLbLtMsg,
                ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)

        {
            if (ptEcfmLbLtMsg == NULL)

            {
                continue;
            }

            /*  Release Memory */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) ptEcfmLbLtMsg);
        }
        /* Delete Q */
        ECFM_DELETE_MSG_QUEUE (ECFM_LBLT_CFG_QUEUE_ID);
        ECFM_LBLT_CFG_QUEUE_ID = ECFM_INIT_VAL;
    }

    /* DeInitialize LBLT Task's Global Info */
    EcfmLbLtDeInitGlobalInfo ();

    /* DeInitalize LBLT Task's Timer MemPool and Timer List */
    EcfmLbLtTmrDeInit ();

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EcfmLbLtInitGlobalInfo
 *
 *    DESCRIPTION      : This function intializes the LBLT Global Info struct.
 *                       It also creates all the ports and port specific MEP
 *                       tree, Timer Intialization and Setting default state for
 *                       the LTM Receiver state machine and LTR Transmitter
 *                       state machine is also handled in this routine. 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmLbLtInitGlobalInfo ()
{
    tMacAddr            MacAddr;
    ECFM_MEMSET (&MacAddr, 0, ECFM_MAC_ADDR_LENGTH);

    /* Allocating MEM Block for the ContextInfo */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_CONTEXT (ECFM_LBLT_CURR_CONTEXT_INFO ())
        == NULL)

    {
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    /* Get Current Context */
    ECFM_LBLT_GET_CONTEXT_INFO (ECFM_DEFAULT_CONTEXT) =
        ECFM_LBLT_CURR_CONTEXT_INFO ();
    ECFM_MEMSET (ECFM_LBLT_CURR_CONTEXT_INFO (), 0,
                 ECFM_LBLT_CONTEXT_INFO_SIZE);
    ECFM_LBLT_CURR_CONTEXT_ID () = ECFM_DEFAULT_CONTEXT;

    /* Allocating memory for LBLT MEP Info global structure */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_TABLE (gpEcfmLbLtMepNode) == NULL)
    {
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_LBLT_PDU = gau1EcfmLbLtPdu;
    /* Initialize default OUI as System Mac Address */

    /* Get System MAC Address */
    CfaGetSysMacAddress (MacAddr);

    /* Set OUI for CC Task */
    ECFM_MEMCPY (ECFM_LBLT_ORG_UNIT_ID, &MacAddr, ECFM_OUI_LENGTH);

    /* Intialize the LBM DLL list */
    UTL_DLL_INIT (&(gEcfmLbLtGlobalInfo.LbmDllList),
                  ECFM_OFFSET (tEcfmLbLtLbmInfo, LbmDllNextNode));

    return ECFM_SUCCESS;
}

/******************************************************************************
 *                                                                            *
 *    FUNCTION NAME    : EcfmLbLtDeInitGlobalInfo                             *
 *                                                                            *
 *    DESCRIPTION      : This function DeInitalizes global structure, deletes *
 *                       all the RBTrees for LBLT Task                        *
 *                                                                            *
 *    INPUT            : None                                                 *
 *                                                                            *
 *    OUTPUT           : None                                                 *
 *                                                                            *
 *    RETURNS          : None                                                 *
 *                                                                            *
 *****************************************************************************/
PUBLIC VOID
EcfmLbLtDeInitGlobalInfo ()
{

    /* Delete the default context created */
    EcfmLbLtHandleDeleteContext (ECFM_DEFAULT_CONTEXT);

    if (gpEcfmLbLtMepNode != NULL)
    {
        /* Free the memory of LBLT MEP Info */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_TABLE_POOL,
                             (UINT1 *) (gpEcfmLbLtMepNode));
        gpEcfmLbLtMepNode = NULL;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmLbltAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function is called when ECFM Module is STARTED. */
/*                      Assign mempoolIds for LBLT mempools created in ECFM module*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
EcfmLbltAssignMempoolIds (VOID)
{
    /*tEcfmLbLtVlanInfo */
    ECFM_LBLT_VLAN_MEM_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_VLAN_INFO_SIZING_ID];

    /*tEcfmLbLtContextInfo */
    ECFM_LBLT_CONTEXT_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_CONTEXT_INFO_SIZING_ID];

    /*tEcfmLbLtDefaultMdTableInfo */
    ECFM_LBLT_DEF_MD_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_DEF_MD_INFO_SIZING_ID];

    /*tEcfmLbLtDelayQueueNod */
    ECFM_LBLT_DELAY_QUEUE_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_DELAY_QUEUE_NODES_SIZING_ID];

    /*tEcfmLbLtLbmInfo */
    ECFM_LBLT_LBM_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_LBM_INFO_SIZING_ID];

    /*tEcfmLbLtLtmReplyListInfo */
    ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZING_ID];

    /*tEcfmLbLtMaInfo */
    ECFM_LBLT_MA_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_MA_INFO_SIZING_ID];

    /*tEcfmLbLtMdInfo */
    ECFM_LBLT_MD_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_MD_INFO_SIZING_ID];

    /*tEcfmLbLtMepInfo */
    ECFM_LBLT_MEP_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_MEP_INFO_SIZING_ID];

    /*tEcfmLbLtMipInfo */
    ECFM_LBLT_MIP_TABLE_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_MIP_INFO_SIZING_ID];

    /*tEcfmLbLtPortInfo */
    ECFM_LBLT_PORT_INFO_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_PORT_INFO_SIZING_ID];

    /*tEcfmLbLtMsg */
    ECFM_LBLT_MSGQ_POOL = ECFMMemPoolIds[MAX_ECFM_LBLT_Q_MESG_SIZING_ID];

    /*tEcfmRegParams */
    ECFM_LBLT_APP_REG_MEM_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_REG_APPS_SIZING_ID];

    /*tEcfmLbLtRMepDbInfo */
    ECFM_LBLT_RMEP_DB_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_RMEP_DB_INFO_SIZING_ID];

    /*tEcfmLbLtStackInfo */
    ECFM_LBLT_STACK_TABLE_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBLT_STACK_INFO_SIZING_ID];

    /*tEcfmCcAvlbltyInfo */
    ECFM_AVLBLTY_INFO_POOLID =
        ECFMMemPoolIds[MAX_ECFM_AVLBLTY_INFO_SIZE_SIZING_ID];

    /*MAX_ECFM_LBM_DATA_TLV_LEN_MAX */
    ECFM_LBLT_MEP_LBM_DATA_TLV_POOL =
        ECFMMemPoolIds[MAX_ECFM_LBM_DATA_TLV_LEN_MAX_SIZING_ID];

    /*MAX_ECFM_MAX_LBM_PDU_SIZE */
    ECFM_LBLT_MEP_LBM_PDU_POOL =
        ECFMMemPoolIds[MAX_ECFM_MAX_LBM_PDU_SIZE_SIZING_ID];

    /*tMplsApiInInfo */
    ECFM_MPTP_LBLT_INPARAMS_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPTP_LBLT_INPARAMS_SIZE_SIZING_ID];

    /*tMplsApiOutInfo */
    ECFM_MPTP_LBLT_OUTPARAMS_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPTP_LBLT_OUTPARAMS_SIZE_SIZING_ID];

    /*tEcfmMplsTpPathInfo */
    ECFM_MPTP_LBLT_PATHINFO_POOLID =
        ECFMMemPoolIds[MAX_ECFM_MPTP_LBLT_PATHINFO_SIZE_SIZING_ID];

    /*tEcfmLtrCacheIndices */
    ECFM_LTR_CACHE_INDICES_POOLID =
        ECFMMemPoolIds[MAX_ECFM_LTR_CACHE_STRUCT_SIZE_SIZING_ID];

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmlbtsk.c                     */
/*-----------------------------------------------------------------------*/
