/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmv2wr.c,v 1.3 2011/12/28 12:58:44 siva Exp $
 *
 * Description: Wrapper Routines for the ecfm mib
 ********************************************************************/

#include  "lr.h"
#include  "fssnmp.h"
#include  "cfmv2lw.h"
#include  "cfmv2wr.h"
#include  "cfmv2db.h"
#include  "ecfm.h"
#include  "cfmdef.h"

INT4
GetNextIndexDot1agCfmStackTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmStackTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmStackTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmStackMdIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmStackMdIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmStackMaIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmStackMaIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmStackMepIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmStackMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         &(pMultiData->u4_ULongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmStackMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmStackTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmStackMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].i4_SLongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) ==
        SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1agCfmVlanTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmVlanTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmVlanTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmVlanPrimaryVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmVlanTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmVlanPrimaryVid
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
Dot1agCfmVlanRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmVlanTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmVlanRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
Dot1agCfmVlanPrimaryVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmVlanPrimaryVid
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiData->i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmVlanRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmVlanRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmVlanPrimaryVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmVlanPrimaryVid (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          i4_SLongValue,
                                          pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmVlanRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmVlanRowStatus (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                         i4_SLongValue,
                                         pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmVlanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmVlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Dot1agCfmDefaultMdDefLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhGetDot1agCfmDefaultMdDefLevel (&(pMultiData->i4_SLongValue)) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefMhfCreationGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhGetDot1agCfmDefaultMdDefMhfCreation (&(pMultiData->i4_SLongValue)) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefIdPermissionGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhGetDot1agCfmDefaultMdDefIdPermission (&(pMultiData->i4_SLongValue))
        != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhSetDot1agCfmDefaultMdDefLevel (pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefMhfCreationSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhSetDot1agCfmDefaultMdDefMhfCreation (pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefIdPermissionSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhSetDot1agCfmDefaultMdDefIdPermission (pMultiData->i4_SLongValue) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhTestv2Dot1agCfmDefaultMdDefLevel
        (pu4Error, pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhTestv2Dot1agCfmDefaultMdDefMhfCreation
        (pu4Error, pMultiData->i4_SLongValue) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefIdPermissionTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhTestv2Dot1agCfmDefaultMdDefIdPermission
        (pu4Error, pMultiData->i4_SLongValue) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdDefLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmDefaultMdDefLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Dot1agCfmDefaultMdDefMhfCreationDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmDefaultMdDefMhfCreation
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Dot1agCfmDefaultMdDefIdPermissionDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmDefaultMdDefIdPermission
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmDefaultMdTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmDefaultMdTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmDefaultMdTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmDefaultMdStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
Dot1agCfmDefaultMdLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmDefaultMdLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdMhfCreationGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmDefaultMdMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdIdPermissionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmDefaultMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmDefaultMdIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmDefaultMdLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdMhfCreationSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmDefaultMdMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
Dot1agCfmDefaultMdIdPermissionSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmDefaultMdIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmDefaultMdLevel (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          i4_SLongValue,
                                          pMultiData->i4_SLongValue) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
Dot1agCfmDefaultMdMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmDefaultMdMhfCreation (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmDefaultMdIdPermission (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmDefaultMdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmDefaultMdTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmConfigErrorListTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmConfigErrorListTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmConfigErrorListTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmConfigErrorListErrorTypeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmConfigErrorListTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmConfigErrorListErrorType
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].i4_SLongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].i4_SLongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_SUCCESS;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdTableNextIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    if (nmhGetDot1agCfmMdTableNextIndex (&(pMultiData->u4_ULongValue)) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_SUCCESS;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1agCfmMdTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmMdTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmMdTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMdFormat
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMdName
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMdLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMdMdLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMdMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMhfIdPermissionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMdMhfIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMaNextIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMdMaNextIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMdTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMdRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMdFormat
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMdName
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMdLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMdMdLevel
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMdMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMhfIdPermissionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMdMhfIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMdRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMdFormat (pu4Error,
                                    pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                    u4_ULongValue,
                                    pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMdName (pu4Error,
                                  pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                  u4_ULongValue,
                                  pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMdLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMdMdLevel (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     u4_ULongValue,
                                     pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMdMhfCreation (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdMhfIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMdMhfIdPermission (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMdRowStatus (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmMdTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmMaNetTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmMaNetTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmMaNetTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaNetTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaNetFormat
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaNetTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaNetName
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetCcmIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaNetTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaNetCcmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaNetTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaNetRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaNetFormat
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaNetName
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetCcmIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaNetCcmInterval
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaNetRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaNetFormat (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaNetName (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                     u4_ULongValue,
                                     pMultiData->pOctetStrValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetCcmIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaNetCcmInterval (pu4Error,
                                            pMultiIndex->
                                            pIndex[ECFM_INDEX_ZERO].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaNetRowStatus (pu4Error,
                                          pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                          u4_ULongValue,
                                          pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                          u4_ULongValue,
                                          pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaNetTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmMaNetTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmMaCompTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmMaCompTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmMaCompTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompPrimaryVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaCompPrimaryVlanId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompMhfCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaCompMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompIdPermissionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaCompIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompNumberOfVidsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaCompNumberOfVids
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaCompTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaCompRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompPrimaryVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaCompPrimaryVlanId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompMhfCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaCompMhfCreation
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompIdPermissionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaCompIdPermission
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompNumberOfVidsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaCompNumberOfVids
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaCompRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompPrimaryVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaCompPrimaryVlanId (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompMhfCreationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaCompMhfCreation (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompIdPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaCompIdPermission (pu4Error,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ZERO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ONE].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_TWO].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompNumberOfVidsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaCompNumberOfVids (pu4Error,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ZERO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ONE].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_TWO].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaCompRowStatus (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaCompTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmMaCompTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmMaMepListTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmMaMepListTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmMaMepListTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaMepListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMaMepListTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMaMepListRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaMepListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMaMepListRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaMepListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMaMepListRowStatus (pu4Error,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ZERO].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_ONE].
                                              u4_ULongValue,
                                              pMultiIndex->
                                              pIndex[ECFM_INDEX_TWO].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMaMepListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmMaMepListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmMepTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmMepTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmMepTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepIfIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepDirection
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepPrimaryVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepPrimaryVid
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepActive
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepFngState
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCciEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepCciEnabled
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCcmLtmPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepCcmLtmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmMepMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLowPrDefGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepLowPrDef
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngAlarmTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepFngAlarmTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngResetTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepFngResetTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepHighestPrDefectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepHighestPrDefect
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDefectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepDefects
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepErrorCcmLastFailureGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepErrorCcmLastFailure
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepXconCcmLastFailureGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepXconCcmLastFailure
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCcmSequenceErrorsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepCcmSequenceErrors
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCciSentCcmsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepCciSentCcms
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepNextLbmTransIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetDot1agCfmMepNextLbmTransId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLbrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepLbrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLbrInOutOfOrderGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepLbrInOutOfOrder
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLbrBadMsduGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetDot1agCfmMepLbrBadMsdu
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLtmNextSeqNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepLtmNextSeqNumber
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepUnexpLtrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepUnexpLtrIn
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLbrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetDot1agCfmMepLbrOut
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestMacAddressGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmMepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestMepIdGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestIsMepIdGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmMessagesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDataTlvGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmDataTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmVlanPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmVlanDropEnableGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmVlanDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmResultOKGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLbmResultOK
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmSeqNumberGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetDot1agCfmMepTransmitLbmSeqNumber
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmFlagsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmFlags
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetMacAddressGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmMepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetMepIdGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetIsMepIdGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmTtl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmResultGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();

    if (nmhGetDot1agCfmMepTransmitLtmResult
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmSeqNumberGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmSeqNumber
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_LBLT_LOCK ();
    if (nmhGetDot1agCfmMepTransmitLtmEgressIdentifier
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepIfIndex
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepDirection
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepPrimaryVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepPrimaryVid
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepActiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepActive
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCciEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepCciEnabled
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCcmLtmPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepCcmLtmPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLowPrDefSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepLowPrDef
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngAlarmTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepFngAlarmTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngResetTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepFngResetTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_SUCCESS;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestMacAddressSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmDestMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestMepIdSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmDestMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestIsMepIdSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmDestIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmMessagesSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmMessages
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDataTlvSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmDataTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmVlanPriority
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmVlanDropEnableSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLbmVlanDropEnable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_SUCCESS;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmFlagsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmFlags
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetMacAddressSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmTargetMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetMepIdSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmTargetMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetIsMepIdSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmTargetIsMepId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTtlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmTtl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->u4_ULongValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmEgressIdentifierSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhSetDot1agCfmMepTransmitLtmEgressIdentifier
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhSetDot1agCfmMepRowStatus
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepIfIndex (pu4Error,
                                      pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                      u4_ULongValue,
                                      pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                      u4_ULongValue,
                                      pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepDirection (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepPrimaryVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepPrimaryVid (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                         u4_ULongValue,
                                         pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepActiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepActive (pu4Error,
                                     pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                     u4_ULongValue,
                                     pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                     u4_ULongValue,
                                     pMultiData->i4_SLongValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCciEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepCciEnabled (pu4Error,
                                         pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                         u4_ULongValue,
                                         pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                         u4_ULongValue,
                                         pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepCcmLtmPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepCcmLtmPriority (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepLowPrDefTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepLowPrDef (pu4Error,
                                       pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                       u4_ULongValue,
                                       pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                       u4_ULongValue,
                                       pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngAlarmTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepFngAlarmTime (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepFngResetTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepFngResetTime (pu4Error,
                                           pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                           u4_ULongValue,
                                           pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestMacAddressTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhTestv2Dot1agCfmMepTransmitLbmDestMacAddress (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        (*(tMacAddr *)
                                                         pMultiData->
                                                         pOctetStrValue->
                                                         pu1_OctetList)) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestMepIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmDestMepId (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ZERO].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_ONE].
                                                   u4_ULongValue,
                                                   pMultiIndex->
                                                   pIndex[ECFM_INDEX_TWO].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDestIsMepIdTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmDestIsMepId (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmMessagesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmMessages (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ZERO].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_ONE].
                                                  u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[ECFM_INDEX_TWO].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmDataTlvTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmDataTlv (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ZERO].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_ONE].
                                                 u4_ULongValue,
                                                 pMultiIndex->
                                                 pIndex[ECFM_INDEX_TWO].
                                                 u4_ULongValue,
                                                 pMultiData->pOctetStrValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmVlanPriorityTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmVlanPriority (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ZERO].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_ONE].
                                                      u4_ULongValue,
                                                      pMultiIndex->
                                                      pIndex[ECFM_INDEX_TWO].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLbmVlanDropEnableTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLbmVlanDropEnable (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ZERO].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_ONE].
                                                        u4_ULongValue,
                                                        pMultiIndex->
                                                        pIndex[ECFM_INDEX_TWO].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLtmStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ZERO].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_ONE].
                                                u4_ULongValue,
                                                pMultiIndex->
                                                pIndex[ECFM_INDEX_TWO].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmFlagsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLtmFlags (pu4Error,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ZERO].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_ONE].
                                               u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[ECFM_INDEX_TWO].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetMacAddressTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhTestv2Dot1agCfmMepTransmitLtmTargetMacAddress (pu4Error,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_ZERO].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_ONE].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_TWO].
                                                          u4_ULongValue,
                                                          (*(tMacAddr *)
                                                           pMultiData->
                                                           pOctetStrValue->
                                                           pu1_OctetList)) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetMepIdTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLtmTargetMepId (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ZERO].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_ONE].
                                                     u4_ULongValue,
                                                     pMultiIndex->
                                                     pIndex[ECFM_INDEX_TWO].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTargetIsMepIdTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLtmTargetIsMepId (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ZERO].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_ONE].
                                                       u4_ULongValue,
                                                       pMultiIndex->
                                                       pIndex[ECFM_INDEX_TWO].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmTtlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLtmTtl (pu4Error,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ZERO].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_ONE].
                                             u4_ULongValue,
                                             pMultiIndex->
                                             pIndex[ECFM_INDEX_TWO].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTransmitLtmEgressIdentifierTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhTestv2Dot1agCfmMepTransmitLtmEgressIdentifier (pu4Error,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_ZERO].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_ONE].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex
                                                          [ECFM_INDEX_TWO].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          pOctetStrValue) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2Dot1agCfmMepRowStatus (pu4Error,
                                        pMultiIndex->pIndex[ECFM_INDEX_ZERO].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_ONE].
                                        u4_ULongValue,
                                        pMultiIndex->pIndex[ECFM_INDEX_TWO].
                                        u4_ULongValue,
                                        pMultiData->i4_SLongValue) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1agCfmMepTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1agCfmLtrTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    ECFM_LBLT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmLtrTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmLtrTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_LBLT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrTtl
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrForwardedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrForwarded
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrTerminalMepGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrTerminalMep
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrLastEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrLastEgressIdentifier
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrNextEgressIdentifierGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrNextEgressIdentifier
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrRelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrRelay
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrChassisIdSubtype
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrChassisId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrManAddressDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrManAddressDomain
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOidValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrManAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrManAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrIngressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrIngress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrIngressMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_LBLT_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmLtrIngressMac
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrIngressPortIdSubtypeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrIngressPortIdSubtype
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrIngressPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrIngressPortId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrEgressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrEgress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrEgressMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmLtrEgressMac
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrEgressPortIdSubtypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrEgressPortIdSubtype
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrEgressPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmLtrEgressPortId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmLtrOrganizationSpecificTlvGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmLtrTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmLtrOrganizationSpecificTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_FOUR].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1agCfmMepDbTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    ECFM_CC_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1agCfmMepDbTable
            (&(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1agCfmMepDbTable
            (pFirstMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue),
             pFirstMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
             &(pNextMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            ECFM_CC_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbRMepStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepDbRMepState
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbRMepFailedOkTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepDbRMepFailedOkTime
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->u4_ULongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhGetDot1agCfmMepDbMacAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList) !=
        SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbRdiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepDbRdi
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbPortStatusTlvGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1agCfmMepDbPortStatusTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbInterfaceStatusTlvGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepDbInterfaceStatusTlv
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbChassisIdSubtypeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepDbChassisIdSubtype
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         &(pMultiData->i4_SLongValue)) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepDbChassisId
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbManAddressDomainGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepDbManAddressDomain
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOidValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1agCfmMepDbManAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ECFM_CC_LOCK ();

    if (nmhValidateIndexInstanceDot1agCfmMepDbTable
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (nmhGetDot1agCfmMepDbManAddress
        (pMultiIndex->pIndex[ECFM_INDEX_ZERO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_ONE].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_TWO].u4_ULongValue,
         pMultiIndex->pIndex[ECFM_INDEX_THREE].u4_ULongValue,
         pMultiData->pOctetStrValue) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

VOID
RegisterCFMV2 ()
{
    SNMPRegisterMib (&Dot1agCfmStackTableOID, &Dot1agCfmStackTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmVlanTableOID, &Dot1agCfmVlanTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmDefaultMdTableOID, &Dot1agCfmDefaultMdTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmConfigErrorListTableOID,
                     &Dot1agCfmConfigErrorListTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMdTableOID, &Dot1agCfmMdTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMaNetTableOID, &Dot1agCfmMaNetTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMaCompTableOID, &Dot1agCfmMaCompTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMaMepListTableOID, &Dot1agCfmMaMepListTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMepTableOID, &Dot1agCfmMepTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmLtrTableOID, &Dot1agCfmLtrTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMepDbTableOID, &Dot1agCfmMepDbTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmDefaultMdDefLevelOID,
                     &Dot1agCfmDefaultMdDefLevelEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmDefaultMdDefMhfCreationOID,
                     &Dot1agCfmDefaultMdDefMhfCreationEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmDefaultMdDefIdPermissionOID,
                     &Dot1agCfmDefaultMdDefIdPermissionEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&Dot1agCfmMdTableNextIndexOID,
                     &Dot1agCfmMdTableNextIndexEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&cfmv2OID, (const UINT1 *) "cfmv2");
}

VOID
UnRegisterCFMV2 ()
{
    SNMPUnRegisterMib (&Dot1agCfmStackTableOID, &Dot1agCfmStackTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmVlanTableOID, &Dot1agCfmVlanTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmDefaultMdTableOID,
                       &Dot1agCfmDefaultMdTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmConfigErrorListTableOID,
                       &Dot1agCfmConfigErrorListTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmMdTableOID, &Dot1agCfmMdTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmMaNetTableOID, &Dot1agCfmMaNetTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmMaCompTableOID, &Dot1agCfmMaCompTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmMaMepListTableOID,
                       &Dot1agCfmMaMepListTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmMepTableOID, &Dot1agCfmMepTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmLtrTableOID, &Dot1agCfmLtrTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmMepDbTableOID, &Dot1agCfmMepDbTableEntry);
    SNMPUnRegisterMib (&Dot1agCfmDefaultMdDefLevelOID,
                       &Dot1agCfmDefaultMdDefLevelEntry);
    SNMPUnRegisterMib (&Dot1agCfmDefaultMdDefMhfCreationOID,
                       &Dot1agCfmDefaultMdDefMhfCreationEntry);
    SNMPUnRegisterMib (&Dot1agCfmDefaultMdDefIdPermissionOID,
                       &Dot1agCfmDefaultMdDefIdPermissionEntry);
    SNMPUnRegisterMib (&Dot1agCfmMdTableNextIndexOID,
                       &Dot1agCfmMdTableNextIndexEntry);
    SNMPDelSysorEntry (&cfmv2OID, (const UINT1 *) "cfmv2");
}
