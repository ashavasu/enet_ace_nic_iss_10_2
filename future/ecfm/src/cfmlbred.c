/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbred.c,v 1.22 2015/08/08 12:58:42 siva Exp $
 *
 * Description: This file contains l2red specific procedures for 
                lblt task.
 *************************************************************************/

#include "cfminc.h"

/* Prototypes for private routines */
PRIVATE VOID EcfmRedProcessSyncLbLtPdu PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLtrCacheHldTmr PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessDelayQueueExpiry PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmLbLtRedHandleValidMessage PROTO ((tRmMsg *, UINT2));
PRIVATE VOID        EcfmRedProcessLbLtMepTmrExp
PROTO ((tRmMsg *, UINT2, UINT2, UINT1));
PRIVATE VOID EcfmRedProcessStopLbTstTrans PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessDmStopTransaction PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcess1DmTransIntExp PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLbrCacheHldTmr PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedLbLtProcessSyncMsg PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLbLtTransSts PROTO ((tRmMsg *, UINT2, UINT2));

extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);

/*****************************************************************************/
/* Function Name      : EcfmLbLtRedHandleRmEvents                            */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pEcfmMsg - Pointer to the  input buffer.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmLbLtRedHandleRmEvents (tEcfmLbLtMsg * pEcfmMsg)
{
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    switch (pEcfmMsg->uMsg.RmFrame.u1Event)

    {
        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM ((tRmMsg *) (pEcfmMsg->uMsg.RmFrame.pFrame),
                               &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR ((tRmMsg *) (pEcfmMsg->uMsg.RmFrame.pFrame),
                                 pEcfmMsg->uMsg.RmFrame.u2Length);

            ProtoAck.u4AppId = RM_ECFM_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Event RM_MESSAGE \r\n");
            EcfmLbLtRedHandleValidMessage ((tRmMsg *) pEcfmMsg->uMsg.
                                           RmFrame.pFrame,
                                           pEcfmMsg->uMsg.RmFrame.u2Length);

            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (pEcfmMsg->uMsg.RmFrame.pFrame));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Received Unknown event from RM =[%d]\n",
                          pEcfmMsg->uMsg.RmFrame.u1Event);
            break;
    }
}

/*****************************************************************************/
/* Function Name      : EcfmLbLtRedHandleValidMessage.                       */
/*                                                                           */
/* Description        : This function stores the information present in the  */
/*                      message.                                             */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmLbLtRedHandleValidMessage (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT1               u1RedMsgType;
    UINT4               u4ByteCount = ECFM_INIT_VAL;

    /* No need to do null pointer check for pMsg as it is done
     * in EcfmRedRcvPktFromRm. */
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "LBLT Received Frame of Size =[%d] \r\n", u2Length);

    /* Get the Message type. */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1RedMsgType);
    u2Length = u2Length - ECFM_RED_TYPE_FIELD_SIZE;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pMsg);
    ECFM_GLB_PKT_DUMP (ECFM_DUMP_TRC, pMsg, u4ByteCount,
                       "EcfmLbLtRedHandleValidMessage: Dumping Received Frame \r\n");
    switch (u1RedMsgType)

    {
        case ECFM_RED_SYNC_LBLT_PDU:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_SYNC_LBLT_PDU \n");
            EcfmRedProcessSyncLbLtPdu (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LTR_CACHE_HLD_TMR:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_LTR_CACHE_HOLD_TIMER_EXPIRY \n");
            EcfmRedProcessLtrCacheHldTmr (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LBR_CACHE_HLD_TMR:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_LBR_CACHE_HOLD_TIMER_EXPIRY \n");
            EcfmRedProcessLbrCacheHldTmr (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_DELAY_QUEUE_TIMER_EXPIRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_DELAY_QUEUE_TIMER_EXPIRY \n");
            EcfmRedProcessDelayQueueExpiry (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LTT_TIMER_EXPIRY:
        case ECFM_RED_LBI_TIMER_EXPIRY:
        case ECFM_RED_LBI_STOP_TX:
        case ECFM_RED_TST_TIMER_EXPIRY:
        case ECFM_RED_TST_STOP_TX:
        case ECFM_RED_STOP_THROUGHPUT_TRANS:
            EcfmRedProcessLbLtMepTmrExp (pMsg, u2Offset, u2Length,
                                         u1RedMsgType);
            break;
        case ECFM_RED_SYNC_LB_TST_TRAN_TYPE:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_SYNC_STOP_TST_TRAN / "
                          "ECFM_RED_SYNC_STOP_TST_TRAN \n");
            EcfmRedProcessStopLbTstTrans (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_DM_STOP_TRANSACTION:
        case ECFM_RED_DM_DEADLINE_TIMER_EXPIRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_DM_DEADLINE_TIMER_EXPIRY "
                          "or ECFM_RED_DM_STOP_TRANSACTION \n");
            EcfmRedProcessDmStopTransaction (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_1DM_TRANS_INT_EXPIRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_1DM_TRANS_INT_EXPIRY\n");
            EcfmRedProcess1DmTransIntExp (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LBLT_SYNC_UPD_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_LBLT_SYNC_UPD_MSG\n");
            EcfmRedLbLtProcessSyncMsg (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LBLT_SYNC_TRANS_STS:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_RED_LBLT_SYNC_TRANS_STS\n");
            EcfmRedProcessLbLtTransSts (pMsg, u2Offset, u2Length);
            break;

        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBLT Invalid Message [%d] \n", u1RedMsgType);
            break;
    }
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbLtPdu                                   */
/*                                                                           */
/* Description        : This function sends the received Valid LbLt Pdu to   */
/*                      the stand-by Node.                                   */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbLtPdu (tEcfmBufChainHeader * pBuf, UINT4 u4IfIndex)
{
    tRmMsg             *pMsg;
    UINT1              *pu1Frame = NULL;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    UINT2               u2MsgSize = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " LBLT Pdu Sync Up message .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);
    u2MsgSize =
        (UINT2) (u4ByteCount + ECFM_RED_TYPE_FIELD_SIZE +
                 ECFM_RED_LEN_FIELD_SIZE + ECFM_PORT_ID_SIZE +
                 ECFM_CONTEXT_ID_SIZE);

    /* Check if the buffer size to be allocated is greater than the max
     * buffer size
     */
    if (u2MsgSize > ECFM_RED_MAX_MSG_SIZE)

    {
        return;
    }

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        return;
    }

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_RED_SYNC_LBLT_PDU);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset,
                        (u2MsgSize -
                         (ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE)));
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    pu1Frame = ECFM_GET_DATA_PTR_IF_LINEAR (pBuf, 0, u4ByteCount);
    if (pu1Frame == NULL)

    {
        pu1Frame = ECFM_LBLT_PDU;
        ECFM_MEMSET (pu1Frame, ECFM_INIT_VAL, u4ByteCount);
        ECFM_COPY_FROM_CRU_BUF (pBuf, pu1Frame, 0, u4ByteCount);
    }
    ECFM_RM_PUT_N_BYTE (pMsg, pu1Frame, &u4Offset, u4ByteCount);
    EcfmRedSendMsgToRm (pMsg, u2MsgSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLtrCacheHldTmr                            */
/*                                                                           */
/* Description        : This function sends LTR Cache Time to standby        */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLtrCacheHldTmr (UINT4 u4RemainingTime)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " Ltr cache hold timer expiry .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LTR_CACHE_HLD_TMR_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    u2SyncMsgLen = ECFM_LTR_CACHE_HLD_TMR_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_RED_LTR_CACHE_HLD_TMR);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4RemainingTime);

    u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT4);

    IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                (CHR1 *) "tEcfmLbLtContextInfo",
                                u4BulkUnitSize);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbrCacheHldTmr                            */
/*                                                                           */
/* Description        : This function sends LBR Cache Time to standby        */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbrCacheHldTmr (UINT4 u4RemainingTime)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4BulkUnitSize = 0;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " Ltr cache hold timer expiry .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LBR_CACHE_HLD_TMR_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    u2SyncMsgLen = ECFM_LBR_CACHE_HLD_TMR_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_RED_LBR_CACHE_HLD_TMR);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4RemainingTime);
    u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT4);
    IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                (CHR1 *) "tEcfmLbLtContextInfo",
                                u4BulkUnitSize);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbLtMepTmrExp                             */
/*                                                                           */
/* Description        : This function sends LTI/LBI  expiry to standby       */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbLtMepTmrExp (UINT1 u1MsgType, UINT4 u4MdIndex, UINT4 u4MaIndex,
                          UINT4 u4MepIndex)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " Delay Queue while timer expiry .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LBLT_MEP_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        return;
    }
    u2SyncMsgLen = ECFM_LBLT_MEP_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MaIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MepIndex);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncTransactionStopEvent                      */
/*                                                                           */
/* Description        : This function sends LTM/TST/TH transaction stop event*/
/*                      to STANDBY Node                                      */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncTransactionStopEvent (UINT1 u1TransType, UINT4 u4MdIndex,
                                 UINT4 u4MaIndex, UINT4 u4MepIndex)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " Ltf while timer expiry .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_STOP_TRANS_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        return;
    }
    u2SyncMsgLen = ECFM_STOP_TRANS_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1TransType);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MaIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MepIndex);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDelayQueueTmrExpiry                       */
/*                                                                           */
/* Description        : This function sends Delay Queue expiry to standby    */
/*                      Node                                                 */
/*                      standby Node.                                        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDelayQueueTmrExpiry (VOID)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " Delay Queue while timer expiry .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_DELAY_QUEUE_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        return;
    }
    u2SyncMsgLen = ECFM_DELAY_QUEUE_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_RED_DELAY_QUEUE_TIMER_EXPIRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLtrCacheHldTmr                         */
/*                                                                           */
/* Description        : This function process the received LbLt sync up msg  */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLtrCacheHldTmr (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tRmProtoEvt         ProtoEvt;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LBLT Sync LT cache Hold Tmr in case of standby Only .\n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "LBLT Data  corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4RemainingTime);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update LTR Hold Timer in stand-by\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    if (u4RemainingTime == 0)

    {
        RBTreeDrain (ECFM_LBLT_LTR_TABLE, (tRBKeyFreeFn)
                     EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LTR_ENTRY);
        ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLtrCacheHldTime = 0;
        ECFM_LBLT_CURR_CONTEXT_INFO ()->TimeStamp = 0;
    }

    else

    {

        /*Store the remaining time along with the time-stamp */
        ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLtrCacheHldTime =
            u4RemainingTime;
        ECFM_GET_SYS_TIME (&(ECFM_LBLT_CURR_CONTEXT_INFO ()->TimeStamp));
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "LTR-CACHE remaining time as = [%d]\r\n", u4RemainingTime);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLbrCacheHldTmr                         */
/*                                                                           */
/* Description        : This function process the received LbLt sync up msg  */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLbrCacheHldTmr (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tRmProtoEvt         ProtoEvt;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LBLT Sync LB Cache HoldTmr in case of Standby Only .\n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "LBLT Data  corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4RemainingTime);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update LB Cache Hold Timer in stand-by\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    if (u4RemainingTime == 0)

    {
        RBTreeDrain (ECFM_LBLT_LBM_TABLE, (tRBKeyFreeFn)
                     EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LBM_ENTRY);
        ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLbCacheHldTime = 0;
        ECFM_LBLT_CURR_CONTEXT_INFO ()->TimeStamp = 0;
    }

    else

    {

        /*Store the remaining time along with the time-stamp */
        ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLbCacheHldTime = u4RemainingTime;
        ECFM_GET_SYS_TIME (&(ECFM_LBLT_CURR_CONTEXT_INFO ()->TimeStamp));
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "LBR-CACHE remaining time as = [%d]\r\n", u4RemainingTime);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessSyncLbLtPdu                            */
/*                                                                           */
/* Description        : This function process the received LbLt sync up msg  */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessSyncLbLtPdu (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    UINT1              *pu1Frame = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LbLtPduLen = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;

    tEcfmBufChainHeader *pCruBuf = NULL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LBLT Sync PDU in case of standby Only .\n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "LBLT Data corruption =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4IfIndex);
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex, &u4ContextId, &u2PortNum)
        != VCM_SUCCESS)
    {
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update LBLT Sync PDU in stand-by\r\n");
        return;
    }
    u2LbLtPduLen = i2MsgSize - (ECFM_PORT_ID_SIZE + ECFM_CONTEXT_ID_SIZE);
    pu1Frame = ECFM_LBLT_PDU;
    ECFM_MEMSET (pu1Frame, ECFM_INIT_VAL, u2LbLtPduLen);
    ECFM_RM_GET_N_BYTE (pMsg, pu1Frame, &u2Offset, u2LbLtPduLen);
    pCruBuf = ECFM_ALLOC_CRU_BUF (u2LbLtPduLen, ECFM_INIT_VAL);
    if (pCruBuf == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "RM alloc  failed. unable to process LBLT PDU sync-up\n");
        ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
        return;
    }
    ECFM_COPY_OVER_CRU_BUF (pCruBuf, pu1Frame, 0, u2LbLtPduLen);
    EcfmLbLtCtrlRxPkt (pCruBuf, u4IfIndex, u2PortNum);

    /*Call LbLt Ctrl Receiver */
    EcfmLbLtCtrlRxPktFree (pCruBuf);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessDelayQueueExpiry                       */
/*                                                                           */
/* Description        : This function process Delay Queue Expiry Message     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessDelayQueueExpiry (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process Delay Queue timer  in case of standby Only .\n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "LBLT Data corruption =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update Delay Queue Timer in stand-by\r\n");
        return;
    }

    /*Trasmit the queued CFM PDUs */
    EcfmLbLtDelayQueueTimerTimeOut ();
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLbLtMepTmrExp                          */
/*                                                                           */
/* Description        : This function process the received LbLt mep timer    */
/*                      expiry                                               */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLbLtMepTmrExp (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length,
                             UINT1 u1MsgType)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4MepIndex = ECFM_INIT_VAL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LBLT MEP timer in case of standby Only .\n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "LBLT Data corruption =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MepIndex);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update MEP-INFO in stand-by\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return;
    }
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmLbLtPduSmInfo));
    PduSmInfo.pMepInfo =
        EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u4MepIndex);
    if (PduSmInfo.pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Mep Info not found for LBLT mep timer expiry\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return;
    }
    switch (u1MsgType)

    {
        case ECFM_RED_LTT_TIMER_EXPIRY:

        {
            EcfmLbLtClntLtInitSm (&PduSmInfo, ECFM_SM_EV_LTI_TX_LTM_TIMESOUT);
            break;
        }
        case ECFM_RED_LBI_TIMER_EXPIRY:
        case ECFM_RED_LBI_STOP_TX:

        {
            EcfmLbLtClntLbInitiator (PduSmInfo.pMepInfo,
                                     ECFM_LB_STOP_TRANSACTION);
            break;
        }
        case ECFM_RED_TST_TIMER_EXPIRY:
        case ECFM_RED_TST_STOP_TX:

        {
            EcfmLbLtClntLbInitiator (PduSmInfo.pMepInfo,
                                     ECFM_TST_STOP_TRANSACTION);
            break;
        }
        case ECFM_RED_STOP_THROUGHPUT_TRANS:

        {
            EcfmLbLtClntThInitiator (&PduSmInfo, ECFM_TH_STOP_TRANSACTION);
            break;
        }
        default:

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Invalid message type = %d\r\n", u1MsgType);
            break;
        }
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncStopLbTstTran                             */
/*                                                                           */
/* Description        : This function sends Stop event for LB/TST            */
/*                      transaction to the stand-by Node.                    */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncStopLbTstTran (UINT1 u1MsgType, tEcfmLbLtMsg * pMsg)
{
    tRmMsg             *pRmMsg;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2MsgSize = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " LBLT PDU Sync Up message .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2MsgSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNC_LB_TST_TRAN_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pRmMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        return;
    }

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pRmMsg, &u4Offset, ECFM_RED_SYNC_LB_TST_TRAN_TYPE);
    ECFM_RM_PUT_2_BYTE (pRmMsg, &u4Offset,
                        (u2MsgSize -
                         (ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE)));
    if (u1MsgType == ECFM_LB_STOP_TRANSACTION)

    {
        ECFM_RM_PUT_1_BYTE (pRmMsg, &u4Offset, ECFM_RED_SYNC_STOP_LB_TRAN);
    }

    else if (u1MsgType == ECFM_TST_STOP_TRANSACTION)

    {
        ECFM_RM_PUT_1_BYTE (pRmMsg, &u4Offset, ECFM_RED_SYNC_STOP_TST_TRAN);
    }
    ECFM_RM_PUT_4_BYTE (pRmMsg, &u4Offset, pMsg->u4ContextId);
    ECFM_RM_PUT_2_BYTE (pRmMsg, &u4Offset, pMsg->u2PortNum);
    ECFM_RM_PUT_2_BYTE (pRmMsg, &u4Offset, pMsg->uMsg.Mep.u4VidIsid);
    ECFM_RM_PUT_1_BYTE (pRmMsg, &u4Offset, pMsg->uMsg.Mep.u1MdLevel);
    ECFM_RM_PUT_1_BYTE (pRmMsg, &u4Offset, pMsg->uMsg.Mep.u1Direction);
    EcfmRedSendMsgToRm (pRmMsg, u2MsgSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessStopLbTstTrans                         */
/*                                                                           */
/* Description        : This function process the received Lb/Tst transaction*/
/*                      stop event                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
EcfmRedProcessStopLbTstTrans (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmLbLtMsg       *pLbLtMsg = NULL;
    UINT1               u1MsgType = ECFM_INIT_VAL;
    UNUSED_PARAM (u2Length);
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process RED LB/TST Stop in case of standby Only .\n");
        return;
    }
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pLbLtMsg) == NULL)

    {
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1MsgType);
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_LBLT_MSG_INFO_SIZE);
    switch (u1MsgType)

    {
        case ECFM_RED_SYNC_STOP_LB_TRAN:

        {
            pLbLtMsg->MsgType = (tEcfmMsgType) ECFM_LB_STOP_TRANSACTION;
            break;
        }
        case ECFM_RED_SYNC_STOP_TST_TRAN:

        {
            pLbLtMsg->MsgType = (tEcfmMsgType) ECFM_TST_STOP_TRANSACTION;
            break;
        }
        default:

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Invalid message type = %d\r\n", u1MsgType);
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtMsg);
            return;
        }
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pLbLtMsg->u4ContextId);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, pLbLtMsg->u2PortNum);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pLbLtMsg->uMsg.Mep.u4VidIsid);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pLbLtMsg->uMsg.Mep.u1MdLevel);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pLbLtMsg->uMsg.Mep.u1Direction);
    if (EcfmLbLtCfgQueMsg (pLbLtMsg) != ECFM_SUCCESS)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtMsg);
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmStopTransaction                         */
/*                                                                           */
/* Description        : This function will send DM Stop Transaction event    */
/*                      to STANDBY node when number of DMMs to be transmitted*/
/*                      becomes 0.                                           */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmStopTransaction (tEcfmLbLtMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "DM Deadline Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_DM_STOP_TRANSACTION_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_DM_STOP_TRANSACTION_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_DM_STOP_TRANSACTION);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmDeadlineTimerExp                        */
/*                                                                           */
/* Description        : This function will send DM Deadline Timeout event    */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmDeadlineTimerExp (tEcfmLbLtMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "DM Deadline Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_DM_DEADLINE_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_DM_DEADLINE_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_DM_DEADLINE_TIMER_EXPIRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessDmStopTransaction                      */
/*                                                                           */
/* Description        : This function process received DM Deadline Expiry    */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessDmStopTransaction (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process DM Deadline Timer Expiry in case"
                      "of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: LBLT Data corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Cannot Handle AIS Rx While Expiry in"
                      "stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }

    /* Stop DM Transaction for the MEP */
    EcfmDmInitStopDmTransaction (pMepNode);
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmTransIntrvalExp                         */
/*                                                                           */
/* Description        : This function will send 1DM Tramsaction Interval Exp */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmTransIntrvalExp (tEcfmLbLtMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "1DM Transaction Interval expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_1DM_TRANS_INT_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_1DM_TRANS_INT_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_1DM_TRANS_INT_EXPIRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcess1DmTransIntExp                         */
/*                                                                           */
/* Description        : This function process received 1DM Trans Interval Exp*/
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcess1DmTransIntExp (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process DM Deadline Timer Expiry in case"
                      "of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: LBLT Data corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Cannot Handle AIS Rx While Expiry in"
                      "stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }

    /* Set 1DM Status to Ready */
    pMepNode->DmInfo.u1DmStatus = ECFM_TX_STATUS_READY;
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedStartMepLbLtTimers                            */
/*                                                                           */
/* Description        : This function starts the LBLT Related Timer for all */
/*                      Active MEPs when Standby Node gets Active            */
/*                                                                           */
/* Input(s)           : u4CurrentTime - Current Time when the Timer needs    */
/*                                      to be started                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
EcfmRedStartMepLbLtTimers (UINT4 u4ContextId, UINT4 u4CurrentTime)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    UINT4               u4DiffTime = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT4               u4LtrCacheHoldTime = ECFM_INIT_VAL;
    INT4                i4LeftOutDuration = ECFM_INIT_VAL;
    INT4                i4OneDmTransInterval = ECFM_INIT_VAL;
    BOOL1               b1TimerStarted = ECFM_FALSE;
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Start LtrCache Hold Timer */
    if (ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLtrCacheHldTime != ECFM_INIT_VAL)

    {
        u4DiffTime = (u4CurrentTime -
                      (ECFM_LBLT_CURR_CONTEXT_INFO ()->TimeStamp));
        i4LeftOutDuration =
            ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLtrCacheHldTime - u4DiffTime;
        if (i4LeftOutDuration > 0)

        {
            UINT4               u4TimeInMsc = ECFM_INIT_VAL;

            /*Convert the time in Msec */
            u4TimeInMsc =
                ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration));
            EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL, u4TimeInMsc);

            /* set the Timer-Started flag to true */
            b1TimerStarted = ECFM_TRUE;
        }

        else

        {

            /* Clear LTR Cache */
            RBTreeDrain (ECFM_LBLT_LTR_TABLE, (tRBKeyFreeFn)
                         EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LTR_ENTRY);
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Ltr Cahce Left-Out Time = [%d] \r\n", i4LeftOutDuration);
    }

    /* Check is LTR-Cache is enabled, then start fresh timer */
    if ((ECFM_IS_LTR_CACHE_ENABLED () == ECFM_TRUE)
        && (b1TimerStarted == ECFM_FALSE))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Starting fresh LTR cache timer \r\n");
        u4LtrCacheHoldTime = ECFM_LBLT_LTR_CACHE_HOLD_TIME * 60;
        u4LtrCacheHoldTime = u4LtrCacheHoldTime * ECFM_NUM_OF_MSEC_IN_A_SEC;
        EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LTR_HOLD, NULL,
                               u4LtrCacheHoldTime);
    }
    ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLtrCacheHldTime = 0;

    /*Start Delay Queue Timer */
    EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_DELAY_QUEUE, NULL,
                           ECFM_DELAY_QUEUE_INTERVAL *
                           ECFM_NUM_OF_MSEC_IN_A_SEC);

    /* Start LBR Cache Hold Timer */
    if (ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLbCacheHldTime != ECFM_INIT_VAL)

    {
        u4DiffTime = (u4CurrentTime -
                      (ECFM_LBLT_CURR_CONTEXT_INFO ()->TimeStamp));
        i4LeftOutDuration =
            ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLbCacheHldTime - u4DiffTime;
        if (i4LeftOutDuration > 0)

        {
            UINT4               u4TimeInMsc = ECFM_INIT_VAL;

            /*Convert the time in Msec */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "LBR Cahce Left-Out Time = [%d] \r\n",
                          i4LeftOutDuration);
            u4TimeInMsc =
                ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration));
            EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBR_HOLD, NULL, u4TimeInMsc);
        }

        else

        {

            /* Handle LBR Cache Hold Timer Expiry */
            RBTreeDrain (ECFM_LBLT_LBM_TABLE, (tRBKeyFreeFn)
                         EcfmLbLtUtilFreeEntryFn, ECFM_LBLT_LBM_ENTRY);
        }
        ECFM_LBLT_CURR_CONTEXT_INFO ()->u4RecvdLbCacheHldTime = ECFM_INIT_VAL;
    }

    else

    {

        /* Start LBR Cache Hold Timer */
        EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBR_HOLD, NULL,
                               (UINT4) (ECFM_LBLT_LBR_CACHE_HOLD_TIME *
                                        ECFM_NUM_OF_SEC_IN_A_MIN *
                                        ECFM_NUM_OF_MSEC_IN_A_SEC));
    }
    /* Start Mep related Timers */
    pMepInfo = RBTreeGetFirst (ECFM_LBLT_MEP_TABLE);
    if (pMepInfo == NULL)

    {
        ECFM_LBLT_UNLOCK ();
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }
    u4DiffTime = (u4CurrentTime - (pMepInfo->TimeStamp));
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmLbLtPduSmInfo));
    while (pMepInfo != NULL)

    {
        PduSmInfo.pMepInfo = pMepInfo;

        /*Check is Interval Timer needs to be started */
        /*There is a possibility that interval timer is zero in such
         * case check if deadline timer is remaining or there are LB
         * message to be transmitted or infinite status is set*/

        /* LB Interval value can be sec/msec, and we need to start the timer 
         * accordingly 
         */
        if (pMepInfo->LbInfo.u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_MSEC)

        {
            u4Interval = pMepInfo->LbInfo.u4TxLbmInterval;
        }
        if (pMepInfo->LbInfo.u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_SEC)

        {
            u4Interval =
                ECFM_NUM_OF_MSEC_IN_A_SEC * pMepInfo->LbInfo.u4TxLbmInterval;
        }
        if (pMepInfo->LbInfo.u1TxLbmStatus == ECFM_TX_STATUS_NOT_READY)

        {
            EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBI_INTERVAL, pMepInfo,
                                   u4Interval);
            if (pMepInfo->LbInfo.u4TxLbmDeadline != 0)
            {
                /* Start Deadline Timer with configured value */
                EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBI_DEADLINE,
                                       pMepInfo,
                                       (ECFM_NUM_OF_MSEC_IN_A_SEC *
                                        pMepInfo->LbInfo.u4TxLbmDeadline));
            }
        }
        /*Check is Interval Timer needs to be started */
        /*There is a possibility that interval timer is zero in such
         * case check if deadline timer is remaining or there are TST
         * message to be transmitted or infinite status is set*/

        /* TST Interval value can be sec/msec, and we need to start the timer 
         * accordingly 
         */
        u4Interval = ECFM_INIT_VAL;
        if (pMepInfo->TstInfo.u1TxTstIntervalType ==
            ECFM_LBLT_TST_INTERVAL_MSEC)

        {
            u4Interval = (UINT4) (pMepInfo->TstInfo.u4TstInterval);
        }
        if (pMepInfo->TstInfo.u1TxTstIntervalType == ECFM_LBLT_TST_INTERVAL_SEC)

        {
            u4Interval =
                (UINT4) (ECFM_NUM_OF_MSEC_IN_A_SEC *
                         pMepInfo->TstInfo.u4TstInterval);
        }
        if (pMepInfo->TstInfo.u1TstStatus == ECFM_TX_STATUS_NOT_READY)

        {
            EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_TST_INTERVAL, pMepInfo,
                                   u4Interval);
            if (ECFM_GET_INFINITE_TX_STATUS (pMepInfo->TstInfo.u4TstMessages)
                == ECFM_TRUE)
            {
                if (pMepInfo->TstInfo.u4TstDeadLine != ECFM_INIT_VAL)
                {
                    EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_TST_DEADLINE, pMepInfo,
                                           ECFM_NUM_OF_MSEC_IN_A_SEC *
                                           pMepInfo->TstInfo.u4TstDeadLine);
                }
            }
        }

        if (pMepInfo->DmInfo.u1DmStatus == ECFM_TX_STATUS_NOT_READY)

        {
            EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_DM_INTERVAL, pMepInfo,
                                   ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC
                                   (pMepInfo->DmInfo.u2TxDmInterval));
            if (ECFM_GET_INFINITE_TX_STATUS (pMepInfo->DmInfo.u2TxNoOfMessages)
                == ECFM_TRUE)

            {
                if (pMepInfo->DmInfo.u4TxDmDeadline != ECFM_INIT_VAL)
                {
                    /* Start Deadline Timer with configured value */
                    EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_DM_DEADLINE,
                                           pMepInfo,
                                           (ECFM_NUM_OF_MSEC_IN_A_SEC *
                                            pMepInfo->DmInfo.u4TxDmDeadline));
                }
            }
            if (pMepInfo->DmInfo.u2OneDmTransInterval < ECFM_NUM_OF_TICKS_IN_A_MSEC)
            {
                i4OneDmTransInterval = 0;
            }
            else
    
            {
                i4OneDmTransInterval = pMepInfo->DmInfo.u2OneDmTransInterval / ECFM_NUM_OF_TICKS_IN_A_MSEC;
            }

            if ((pMepInfo->DmInfo.u1DmStatus == ECFM_TX_STATUS_NOT_READY) &&
                (pMepInfo->DmInfo.u2OneDmTransInterval != 0) &&
                (pMepInfo->DmInfo.u2TxNoOfMessages == 0))
            {
                EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_1DM_TRANS_INTERVAL,
                                       pMepInfo,
                                       ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC
                                       (i4OneDmTransInterval));
            }
        }

        if (pMepInfo->LtInfo.u1TxLtmStatus == ECFM_TX_STATUS_NOT_READY)
        {
            /* Start Deadline Timer with configured value */
            EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LTM_TX,
                                   pMepInfo,
                                   ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC
                                   (pMepInfo->LtInfo.u2LtrTimeOut));
        }
        /*Start TH transaction fresh */
        if (pMepInfo->ThInfo.u2TxThMessages != ECFM_INIT_VAL)
        {
            pMepInfo->ThInfo.u1ThStatus = ECFM_TX_STATUS_READY;
            /* Call TH Initiator to start transaction */
            if (EcfmLbLtUtilPostTransaction
                (pMepInfo, ECFM_TH_START_TRANSACTION) != ECFM_SUCCESS)
            {
                pMepInfo->ThInfo.b1TxThResultOk = ECFM_FALSE;
            }
        }

        PduSmInfo.pMepInfo = NULL;

        /* Get Next MEP from the Global RBTree maintained */
        pMepInfo = RBTreeGetNext (ECFM_LBLT_MEP_TABLE, pMepInfo, NULL);
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedFillLbLtEcfmMepInfo                           */
/*                                                                           */
/* Description        : This function fills the MEP Info in the RM Buffer    */
/*                                                                           */
/* Input(s)           : u4ContextId - Current context Id.                    */
/*                      pMsg     - Pointer to the buffer where information   */
/*                                 needs to be written.                      */
/*                      u2Offset - Offset from where the info is to be filled*/
/*                      MdIndex  - MdIndex for the MEP                       */
/*                      MaIndex  - MaIndex for the MEP                       */
/*                      MepId    - MepId for the MEP                         */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedFillLbLtEcfmMepInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT4 u4MdIndex,
                            UINT4 u4MaIndex, UINT2 u2MepId, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmAppTimer      *pEcfmAppTmr = NULL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    ECFM_LBLT_LOCK ();

    /* Select LBLT Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context Selection Failed \r\n");
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP from Global Node to get the information and fill in the 
     * RM Buffer */
    pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrOut);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrIn);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrBadMsdu);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, (pMepInfo->LbInfo.u4NextTransId));
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4NextLbmSeqNo);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4ExpectedLbrSeqNo);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u2NoOfLbrIn);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.b1TxLbmResultOk);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u2TxLbmMessages);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LtInfo.u4LtmNextSeqNum);

    /* Reset u4RemainigTime  for DM Deadline Timer */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->LtInfo.LtmTxTimer.TimerNode);

    /* Get the remaining time for RDI Period Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }

    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pLtmTxTimer = [%d] \n", u4RemainingTime);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

    /* Release LBLT Context */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedFillLbLtY1731MepInfo                          */
/*                                                                           */
/* Description        : This function fills the MEP Info in the RM Buffer    */
/*                                                                           */
/* Input(s)           : u4ContextId - Current context Id.                    */
/*                      pMsg     - Pointer to the buffer where information   */
/*                                 needs to be written.                      */
/*                      u2Offset - Offset from where the info is to be filled*/
/*                      MdIndex  - MdIndex for the MEP                       */
/*                      MaIndex  - MaIndex for the MEP                       */
/*                      MepId    - MepId for the MEP                         */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedFillLbLtY1731MepInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT4 u4MdIndex,
                             UINT4 u4MaIndex, UINT2 u2MepId, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    tEcfmAppTimer      *pEcfmAppTmr = NULL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    ECFM_LBLT_LOCK ();

    /* Select LBLT Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context Selection Failed \r\n");
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP from Global Node to get the information and fill in the 
     * RM Buffer */
    pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, (pMepInfo->DmInfo.u4CurrTransId));
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u2TxNoOfMessages);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u2NoOf1DmIn);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u2NoOfDmrIn);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.MinFrameDelayValue.u4NanoSeconds);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrBitError);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrInOutOfOrder);

    /*Start of Tst Transaction synch Up information */
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4TstSeqNumber);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4TstsSent);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4TstMessages);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4BitErroredTstIn);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4ValidTstIn);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.b1TstResultOk);

    ECFM_RM_PUT_N_BYTE (pMsg, &pMepInfo->ThInfo.d8MeasuredThBps, pu2Offset, 8);
    ECFM_RM_PUT_N_BYTE (pMsg, &pMepInfo->ThInfo.d8TxThTL, pu2Offset, 8);
    ECFM_RM_PUT_N_BYTE (pMsg, &pMepInfo->ThInfo.d8TxThTU, pu2Offset, 8);
    ECFM_RM_PUT_N_BYTE (pMsg, &pMepInfo->ThInfo.d8TxThTH, pu2Offset, 8);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->ThInfo.u2TxThMessages);

    /* Reset u4RemainigTime  for DM Deadline Timer */
    u4RemainingTime = ECFM_INIT_VAL;

    pEcfmAppTmr = &(pMepInfo->DmInfo.DmInitDeadlineTimer.TimerNode);

    /* Get the remaining time for RDI Period Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }

    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pDmInitDeadlineTimer = [%d] \n", u4RemainingTime);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

    /* Reset u4RemainigTime  for DM Interval timer */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->DmInfo.DmInitIntervalTimer.TimerNode);

    /* Get the remaining time for RDI Period Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pDmInitIntervalTimer = [%d] \n", u4RemainingTime);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

    /* Reset u4RemainigTime  for 1DM Transaction Interval Timer */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->DmInfo.OneDmTransIntervalTimer.TimerNode);

    /* Get the remaining time for RDI Period Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: p1DmTransIntervalTimer = [%d] \n", u4RemainingTime);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->LbInfo.LbiIntervalTimer.TimerNode);

    /* Get the remaining time for LBM Interval Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pLbiIntervalTimer = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->LbInfo.LbiDeadlineTimer.TimerNode);

    /* Get the remaining time for LBM Deadline Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pLbiDeadlineTimer = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->TstInfo.TstWhile.TimerNode);

    /* Get the remaining time for TstWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pTstWhile = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->TstInfo.TstDeadLine.TimerNode);

    /* Get the remaining time for Tst DeadLine Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pTstDeadLine = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->ThInfo.ThInitDeadlineTimer.TimerNode);

    /* Get the remaining time for Throughput DeadLine Timer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_LBLT_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pThInitDeadlineTimer = [%d] \n", u4RemainingTime);

    /* Release LBLT Context */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedLbLtUpdateEcfmMepInfo                         */
/*                                                                           */
/* Description        : This function Updates MEP Info from the RM Buffer    */
/*                                                                           */
/* Input(s)           : u4ContextId - Current context Id.                    */
/*                      pMsg     - Pointer to the buffer where information   */
/*                                 needs to be written.                      */
/*                      u2Offset - Offset from where the info is to be filled*/
/*                      MdIndex  - MdIndex for the MEP                       */
/*                      MaIndex  - MaIndex for the MEP                       */
/*                      MepId    - MepId for the MEP                         */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedLbLtUpdateEcfmMepInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT4 u4MdIndex,
                              UINT4 u4MaIndex, UINT2 u2MepId, UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    ECFM_LBLT_LOCK ();

    /* Select Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Select Context failed while Updating"
                      "LBLT MEP Info\n");
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP from the Global node to update the MEP Info */
    pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP not Found \n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Update the MEP Info received in the RM Buffer */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrOut);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrIn);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrBadMsdu);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, (pMepInfo->LbInfo.u4NextTransId));
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4NextLbmSeqNo);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4ExpectedLbrSeqNo);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u2NoOfLbrIn);
    ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.b1TxLbmResultOk);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u2TxLbmMessages);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LtInfo.u4LtmNextSeqNum);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->LtInfo.u4RemainingLtWhileTimer);

    /* Release Context */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedLbLtUpdateY1731MepInfo                        */
/*                                                                           */
/* Description        : This function Updates MEP Info from the RM Buffer    */
/*                                                                           */
/* Input(s)           : u4ContextId - Current context Id.                    */
/*                      pMsg     - Pointer to the buffer where information   */
/*                                 needs to be written.                      */
/*                      u2Offset - Offset from where the info is to be filled*/
/*                      MdIndex  - MdIndex for the MEP                       */
/*                      MaIndex  - MaIndex for the MEP                       */
/*                      MepId    - MepId for the MEP                         */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedLbLtUpdateY1731MepInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT4 u4MdIndex,
                               UINT4 u4MaIndex, UINT2 u2MepId,
                               UINT4 u4ContextId)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    ECFM_LBLT_LOCK ();

    /* Select Context */
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Select Context failed while Updating"
                      "LBLT MEP Info\n");
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Get MEP from the Global node to update the MEP Info */
    pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP not Found \n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return;
    }

    /* Update the MEP Info received in the RM Buffer */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u4CurrTransId);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u2TxNoOfMessages);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u2NoOf1DmIn);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->DmInfo.u2NoOfDmrIn);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.MinFrameDelayValue.u4Seconds);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.MinFrameDelayValue.u4NanoSeconds);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrBitError);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->LbInfo.u4LbrInOutOfOrder);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4TstSeqNumber);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4TstsSent);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4TstMessages);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4BitErroredTstIn);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.u4ValidTstIn);
    ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->TstInfo.b1TstResultOk);

    ECFM_RM_GET_N_BYTE (pMsg, &pMepInfo->ThInfo.d8MeasuredThBps, pu2Offset, 8);
    ECFM_RM_GET_N_BYTE (pMsg, &pMepInfo->ThInfo.d8TxThTL, pu2Offset, 8);
    ECFM_RM_GET_N_BYTE (pMsg, &pMepInfo->ThInfo.d8TxThTU, pu2Offset, 8);
    ECFM_RM_GET_N_BYTE (pMsg, &pMepInfo->ThInfo.d8TxThTH, pu2Offset, 8);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->ThInfo.u2TxThMessages);

    /* Store the remaining Timer intervals for the MEP */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.u4RemainingDmInitDeadlineTimer);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.u4RemainingDmInitIntervalTimer);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->DmInfo.u4RemainingDmTransIntervalTimer);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->LbInfo.u4RemainingLbIntervalTime);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->LbInfo.u4RemainingLbDeadLineTime);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->TstInfo.u4RemainingTstInterval);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->TstInfo.u4RemainingTstDeadLine);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                        pMepInfo->ThInfo.u4RemainingThDeadlineTimer);
    ECFM_GET_SYS_TIME (&(pMepInfo->TimeStamp));

    /* Release Context */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbCache                                   */
/*                                                                           */
/* Description        : This function  syncs the LB Cache at LBLT Task at    */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbCache ()
{
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;
    tRmMsg             *pMsg = NULL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen = ECFM_INIT_VAL;
    UINT2               u2SyncOffset = ECFM_INIT_VAL;
    UINT2               u2LbInfoLen = ECFM_INIT_VAL;
    UINT2               u2LbrNodeCount = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " LBLT PDU Sync Up message .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize = ECFM_RED_MAX_MSG_SIZE;

    /* Allocate memory to the pMsg for Maximum PDU Size */
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);
    if (pMsg == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM alloc  failed. Bulk updates not sent\n");
        return;
    }
    u2Offset = ECFM_INIT_VAL;

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LBLT_SYNC_UPD_MSG);

    /* Fill the number of size of  information to be synced up.
     * Here only zero will be filled. The following macro is called
     * to move the pointer by 2 bytes.*/
    u2SyncOffset = u2Offset;
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);

    /* Update LB Buffer Information */
    pLbmNode = (tEcfmLbLtLbmInfo *) RBTreeGetFirst (ECFM_LBLT_LBM_TABLE);

    /* Get LB Info and Fill in the RM Buffer to transmit Sync Up message */
    while (pLbmNode != NULL)

    {

        /* Get LBR node count for each LBM Node */
        u2LbrNodeCount = TMO_SLL_Count (&(pLbmNode->LbrList));
        u2LbInfoLen = ECFM_RED_LBM_BUFF_INFO_SIZE +
            (u2LbrNodeCount * ECFM_RED_LBR_BUFF_INFO_SIZE);

        /* There is no enough space to fill this port information into
         * buffer.
         * Hence send the existing message and construct new message to
         * fill the information of other MEPs.
         * */
        if ((u2BufSize - u2Offset) < u2LbInfoLen)

        {

            /* Fill the number of size of  information to be synced up.
             * Here only zero will be filled. The following macro is
             * called to move the pointer by 2 bytes.
             */
            /* u2Offset contains the number of bytes written in the
             * buffer, so can be used as no of bytes to transmit
             */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer for LB buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LBLT_SYNC_UPD_MSG);

            /* Offset of the Length field for update information is
             * stored in u4SyncLengthOffset. u2SyncMsgLen contains the size of
             * update information so far written. Hence update length field at
             * offset u4SyncLengthOffset with the value u2SyncMsgLen.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
        }

        /* Fill in Value field the LM Buffer Info */
        /* Fill TLV Type in the message and Increment the offset */
        if (pLbmNode->b1Synched == ECFM_FALSE)

        {
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LB_BUFF_INFO);

            /* Fill TLV length in the message and increment the offset */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2LbInfoLen);

            /* Fill the Value in the message and increment the offset */
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_LBLT_CURR_CONTEXT_ID ());
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4MdIndex);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4MaIndex);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pLbmNode->u2MepId);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4TransId);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4SeqNum);
            ECFM_RM_PUT_N_BYTE (pMsg, pLbmNode->TargetMacAddr, &u2Offset,
                                ECFM_MAC_ADDR_LENGTH);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4BytesSent);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4UnexpLbrIn);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4NumOfResponders);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbmNode->u4DupLbrIn);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pLbmNode->u2NoOfLBMSent);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pLbmNode->u2NoOfLBRRcvd);
            u4BulkUnitSize = ECFM_RED_LBM_BUFF_INFO_SIZE;
            IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                        (CHR1 *) "tEcfmLbLtLbmInfo",
                                        u4BulkUnitSize);

            /* Get LBR Node corresponding to this LBM and Fill LBR info */
            pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_First
                (&(pLbmNode->LbrList));
            while (pLbrNode != NULL)

            {
                ECFM_RM_PUT_N_BYTE (pMsg, pLbrNode->LbrSrcMacAddr, &u2Offset,
                                    ECFM_MAC_ADDR_LENGTH);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbrNode->u4RcvOrder);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pLbrNode->u4LbrRcvTime);
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                                    pLbrNode->u1LbrErrorStatus);
                u4BulkUnitSize = ECFM_RED_LBR_BUFF_INFO_SIZE;
                IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                            (CHR1 *) "tEcfmLbLtLbrInfo",
                                            u4BulkUnitSize);
                /* Get Next Lbr Node */
                pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_Next
                    (&(pLbmNode->LbrList),
                     (tEcfmSllNode *) & (pLbrNode->LbrTableSllNode));
            }

            /* Update Sync Msg Length */
            u2SyncMsgLen = u2SyncMsgLen + u2LbInfoLen;

            /* Set b1Synched for the LB Buffer as TRUE */
            pLbmNode->b1Synched = ECFM_TRUE;
        }

        /* Get Next LB Buffer Entry */
        pLbmNode =
            (tEcfmLbLtLbmInfo *) RBTreeGetNext (ECFM_LBLT_LBM_TABLE, pLbmNode,
                                                NULL);
    }
    if ((u2Offset - 2) != u2SyncOffset)

    {

        /* u2Offset contains the number of bytes written in the buffer,
         * so can be used as no of bytes to transmit */
        ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
        EcfmRedSendMsgToRm (pMsg, u2Offset);
    }

    else

    {

        /* Empty buffer created without any update messages filled */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Empty buffer created without any update"
                      "messages filled \r\n");
        RM_FREE (pMsg);
        pMsg = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncDmCache                                   */
/*                                                                           */
/* Description        : This function syncs the DM Cache at LBLT Task at     */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncDmCache ()
{
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;
    tEcfmMacAddr        TempMacAddr;
    tRmMsg             *pMsg = NULL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4TransId = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2Length = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen = ECFM_INIT_VAL;
    UINT2               u2SyncOffset = ECFM_INIT_VAL;
    UINT2               u2DmOffset = ECFM_INIT_VAL;
    UINT2               u2DmInfoLen = ECFM_INIT_VAL;
    BOOL1               b1BuffFull = ECFM_TRUE;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " LBLT PDU Sync Up message .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize = ECFM_RED_MAX_MSG_SIZE;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* Allocate memory to the pMsg for Maximum PDU Size */
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);
    if (pMsg == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM alloc  failed. Bulk updates not sent\n");
        return;
    }
    u2Offset = ECFM_INIT_VAL;

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LBLT_SYNC_UPD_MSG);

    /* Fill the number of size of  information to be synced up.
     * Here only zero will be filled. The following macro is called
     * to move the pointer by 2 bytes.*/
    u2SyncOffset = u2Offset;
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);

    /* Update DM Buffer Information */
    pFrmDelayBuffNode = (tEcfmLbLtFrmDelayBuff *)
        RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);

    /* Get DM Buffer Info and Fill in the RM Buffer to transmit Sync Up message */
    while (pFrmDelayBuffNode != NULL)

    {

        /* Check if the  node is for the same transaction or not. If not,
         * then we need to send the indexes too in the RM message otherwise,
         * just the DM info.
         */
        if ((u4MdIndex != pFrmDelayBuffNode->u4MdIndex) ||
            (u4MaIndex != pFrmDelayBuffNode->u4MaIndex) ||
            (u2MepId != pFrmDelayBuffNode->u2MepId) ||
            (u4TransId != pFrmDelayBuffNode->u4TransId) ||
            (ECFM_MEMCMP
             (pFrmDelayBuffNode->PeerMepMacAddress, TempMacAddr, 6) != 0))

        {
            u2Length = ECFM_RED_DM_BUFF_FULL_INFO_SIZE;
            b1BuffFull = ECFM_TRUE;
        }

        else

        {
            u2Length = ECFM_RED_DM_BUFF_INFO_SIZE;
            b1BuffFull = ECFM_FALSE;
        }

        /* There is no enough space to fill this port information into
         * buffer.
         * Hence send the existing message and construct new message to
         * fill the information of other MEPs.
         * */
        if ((u2BufSize - u2Offset) < u2Length)

        {

            /* Update Message Length */
            u2SyncMsgLen = u2SyncMsgLen + u2DmInfoLen;

            /* u2Offset contains the number of bytes written in the
             * buffer, so can be used as no of bytes to transmit
             */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer for DM buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;
            u2DmInfoLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LBLT_SYNC_UPD_MSG);

            /* Offset of the Length field for update information is
             * stored in u4SyncLengthOffset. u2SyncMsgLen contains the size of
             * update information so far written. Hence update length field at
             * offset u4SyncLengthOffset with the value u2SyncMsgLen.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            u4MdIndex = ECFM_INIT_VAL;
            u4MaIndex = ECFM_INIT_VAL;
            u2MepId = ECFM_INIT_VAL;
            u4TransId = ECFM_INIT_VAL;
            ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
            b1BuffFull = ECFM_TRUE;
        }

        /* Fill in Value field the LM Buffer Info */
        /* Fill TLV Type in the message and Increment the offset */
        if (pFrmDelayBuffNode->b1Synched == ECFM_FALSE)

        {
            if (b1BuffFull)

            {
                u2SyncMsgLen =
                    u2SyncMsgLen + u2DmInfoLen + ECFM_RED_TYPE_FIELD_SIZE +
                    ECFM_RED_LEN_FIELD_SIZE;

                /*New Transaction Buff started */
                u2DmInfoLen = ECFM_INIT_VAL;

                /* Fill Type */
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_DM_BUFF_INFO);
                u2DmOffset = u2Offset;

                /* Fill TLV length in the message and increment the offset */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2DmInfoLen);

                /* If New Node is for the same Transaction, then need not fill
                 * the indexes in the RM Buffer.
                 */
                /* Fill the Value in the message and increment the offset */
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    ECFM_LBLT_CURR_CONTEXT_ID ());
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmDelayBuffNode->u4MdIndex);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmDelayBuffNode->u4MaIndex);
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                    pFrmDelayBuffNode->u2MepId);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmDelayBuffNode->u4TransId);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmDelayBuffNode->u4SeqNum);
                ECFM_RM_PUT_N_BYTE (pMsg, pFrmDelayBuffNode->PeerMepMacAddress,
                                    &u2Offset, ECFM_MAC_ADDR_LENGTH);
                u4BulkUnitSize = ECFM_RED_DM_COMMON_INFO_SIZE;
                IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                            (CHR1 *) "tEcfmLbLtFrmDelayBuff",
                                            u4BulkUnitSize);

                /* Increment Length for DM Indexes + Peer Mac Address which
                 * is same in all DM Buffer for a same transaction */
                u2DmInfoLen = u2DmInfoLen + ECFM_RED_DM_COMMON_INFO_SIZE;
            }
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->TxTimeStampf.u4Seconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->TxTimeStampf.u4NanoSeconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->FrmDelayValue.u4Seconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->
                                InterFrmDelayVariation.u4Seconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->
                                InterFrmDelayVariation.u4NanoSeconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->
                                FrameDelayVariation.u4Seconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->
                                FrameDelayVariation.u4NanoSeconds);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->MeasurementTimeStamp);
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pFrmDelayBuffNode->u1DmType);

            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->u2NoOfDMMSent);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->u2NoOfDMRRcvd);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                pFrmDelayBuffNode->u2NoOf1DMRcvd);
            u4BulkUnitSize = ECFM_RED_DM_BUFF_INFO_SIZE;
            IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                        (CHR1 *) "tEcfmLbLtFrmDelayBuff",
                                        u4BulkUnitSize);

            /* Update Sync Msg Length */
            u2DmInfoLen = u2DmInfoLen + ECFM_RED_DM_BUFF_INFO_SIZE;

            /* Fill TLV length in the message and increment the offset */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2DmOffset, u2DmInfoLen);
            u2DmOffset -= 2;

            /* Set b1Synched for the LM Buffer as TRUE */
            pFrmDelayBuffNode->b1Synched = ECFM_TRUE;
        }

        /*Check required to handle the case where list is partially synched */
        else if (u4TransId == ECFM_INIT_VAL)

        {

            /* Get Next LM Buffer Entry */
            pFrmDelayBuffNode = (tEcfmLbLtFrmDelayBuff *)
                RBTreeGetNext (ECFM_LBLT_FD_BUFFER_TABLE, pFrmDelayBuffNode,
                               NULL);
            continue;
        }

        /* Store the last nodes Indexes to compare the new node indexes */
        u4MdIndex = pFrmDelayBuffNode->u4MdIndex;
        u4MaIndex = pFrmDelayBuffNode->u4MaIndex;
        u2MepId = pFrmDelayBuffNode->u2MepId;
        u4TransId = pFrmDelayBuffNode->u4TransId;
        ECFM_MEMCPY (TempMacAddr, pFrmDelayBuffNode->PeerMepMacAddress,
                     ECFM_MAC_ADDR_LENGTH);

        /* Get Next LM Buffer Entry */
        pFrmDelayBuffNode = (tEcfmLbLtFrmDelayBuff *)
            RBTreeGetNext (ECFM_LBLT_FD_BUFFER_TABLE, pFrmDelayBuffNode, NULL);
    }

    /* Update Message Length */
    u2SyncMsgLen = u2SyncMsgLen + u2DmInfoLen;
    if ((u2Offset - 2) != u2SyncOffset)

    {

        /* u2Offset contains the number of bytes written in the buffer,
         * so can be used as no of bytes to transmit */
        ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
        EcfmRedSendMsgToRm (pMsg, u2Offset);
    }

    else

    {

        /* Empty buffer created without any update messages filled */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Empty buffer created without any update"
                      "messages filled \r\n");
        RM_FREE (pMsg);
        pMsg = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateDmBuffInfo                              */
/*                                                                           */
/* Description        : This function updates the DM Buffer Info received in */
/*                      Sync  msg.                                           */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu2Offset - Offset from where info is to be read     */
/*                      u2InfoLen - Length for DM Info received in RM        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedUpdateDmBuffInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT2 u2InfoLen)
{
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;
    tEcfmLbLtFrmDelayBuff *pFirstFrmDelayBuffEntry = NULL;
    tEcfmMacAddr        TempMacAddr;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT4               u4TransId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update DM BUFF-INFO in stand-by\r\n");
        return;
    }

    /* Indexes for the LM Buffer Info */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2MepId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4TransId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4SeqNum);
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_RM_GET_N_BYTE (pMsg, TempMacAddr, pu2Offset, ECFM_MAC_ADDR_LENGTH);
    u2InfoLen = u2InfoLen - ECFM_RED_DM_COMMON_INFO_SIZE;
    while (u2InfoLen)

    {

        /* Allocate a memory block for frame loss buffer node */
        ECFM_ALLOC_MEM_BLOCK_LBLT_FD_BUFF_TABLE (pFrmDelayBuffNode);
        while (pFrmDelayBuffNode == NULL)

        {

            /* Memory allocation failed, so now check if the memory pool is
             * exhausted
             */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM : failure allocating memory for DM buffer\r\n");
            if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_FD_BUFFER_POOL)
                != ECFM_INIT_VAL)

            {

                /* memory failure failure has occured even when we have free
                 * memory in the pool
                 */
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "ECFM : DM memory pool corruption\r\n");
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return;
            }

            /* Memory pool is exhausted for sure, now we can delete the first
             * entry in the pool to have some free memory
             */
            pFirstFrmDelayBuffEntry = (tEcfmLbLtFrmDelayBuff *)
                RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);
            if (pFirstFrmDelayBuffEntry == NULL)

            {

                /* Memory pool is exhausted and we dont have anyting in the frame
                 * loss table to free.*/
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "ECFM RED: Frame Delay buffer table corruption \r\n");
                return;
            }

            /* Remove the first node from the table */
            RBTreeRem (ECFM_LBLT_FD_BUFFER_TABLE, pFirstFrmDelayBuffEntry);

            /* Free the memory for the node */
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_FD_BUFFER_POOL,
                                 (UINT1 *) pFirstFrmDelayBuffEntry);
            pFirstFrmDelayBuffEntry = NULL;

            /* Now we can again try for allocating memory */
            ECFM_ALLOC_MEM_BLOCK_LBLT_FD_BUFF_TABLE (pFrmDelayBuffNode);
        }
        ECFM_MEMSET (pFrmDelayBuffNode, ECFM_INIT_VAL,
                     sizeof (tEcfmLbLtFrmDelayBuff));
        /* Fill the Node */
        pFrmDelayBuffNode->u4MdIndex = u4MdIndex;
        pFrmDelayBuffNode->u4MaIndex = u4MaIndex;
        pFrmDelayBuffNode->u2MepId = u2MepId;
        pFrmDelayBuffNode->u4TransId = u4TransId;
        ECFM_MEMCPY (pFrmDelayBuffNode->PeerMepMacAddress, TempMacAddr,
                     ECFM_MAC_ADDR_LENGTH);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->TxTimeStampf.u4Seconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->TxTimeStampf.u4NanoSeconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->FrmDelayValue.u4Seconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->
                            InterFrmDelayVariation.u4Seconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->
                            InterFrmDelayVariation.u4NanoSeconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->FrameDelayVariation.u4Seconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->
                            FrameDelayVariation.u4NanoSeconds);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmDelayBuffNode->MeasurementTimeStamp);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pFrmDelayBuffNode->u1DmType);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pFrmDelayBuffNode->u2NoOfDMMSent);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pFrmDelayBuffNode->u2NoOfDMRRcvd);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pFrmDelayBuffNode->u2NoOf1DMRcvd);

        /* Get MEP info to update Seq Number field */
        pMepInfo =
            EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
        if (pMepInfo == NULL)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: MEP Not Found \r\n");
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_FD_BUFFER_POOL,
                                 (UINT1 *) pFrmDelayBuffNode);
            return;
        }

        /* Increment sequence number to add a new node in the Buffer */
        u4SeqNum++;
        pMepInfo->DmInfo.u4TxDmSeqNum = u4SeqNum;

        /* Increment the Sequence Number */
        pFrmDelayBuffNode->u4SeqNum = u4SeqNum;

        /* Add the node into the table */
        if (RBTreeAdd (ECFM_LBLT_FD_BUFFER_TABLE, pFrmDelayBuffNode)
            != ECFM_RB_SUCCESS)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: DM Node add failure \r\n");
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_FD_BUFFER_POOL,
                                 (UINT1 *) pFrmDelayBuffNode);
        }
        u2InfoLen = u2InfoLen - ECFM_RED_DM_BUFF_INFO_SIZE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateLbBuffInfo                              */
/*                                                                           */
/* Description        : This function updates the LB Buffer Info received in */
/*                      Sync  msg.                                           */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu2Offset - Offset from where info is to be read     */
/*                      u2InfoLen - Length for LB Info received in RM        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedUpdateLbBuffInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT2 u2InfoLen)
{
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmFirstNode = NULL;
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_LBLT_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update LB BUFF-INFO in stand-by\r\n");
        return;
    }

    /* Allocate Memory to the LBM Node to be addded in the LbInit Info */
    ECFM_ALLOC_MEM_BLOCK_LBLT_LBM_TABLE (pLbmNode);
    while (pLbmNode == NULL)

    {
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_LBM_TABLE_POOL) != 0)

        {
            ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED : Memory Block exists but Memory Allocation to "
                          "LBM Node Failed\r\n");
            return;
        }

        /* If the Addition of this LBM entry exceeds the resources allocated
         * to LbInit, then delete the first entry if any */
        pLbmFirstNode = (tEcfmLbLtLbmInfo *) RBTreeGetFirst
            (ECFM_LBLT_LBM_TABLE);
        if (pLbmFirstNode == NULL)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: Memory corrupted\r\n");
            return;
        }
        EcfmLbLtUtilRemoveLbEntry (pLbmFirstNode);
        ECFM_ALLOC_MEM_BLOCK_LBLT_LBM_TABLE (pLbmNode);
    }
    ECFM_MEMSET (pLbmNode, ECFM_INIT_VAL, ECFM_LBLT_LBM_INFO_SIZE);

    /* Indexes for the LM Buffer Info */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pLbmNode->u2MepId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4TransId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4SeqNum);

    /* Get MEP info to update Seq Number field */
    pMepInfo =
        EcfmLbLtUtilGetMepEntryFrmGlob (pLbmNode->u4MdIndex,
                                        pLbmNode->u4MaIndex, pLbmNode->u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: MEP Not Found \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LBM_TABLE_POOL, (UINT1 *) pLbmNode);
        return;
    }
    pMepInfo->LbInfo.u4NextLbmSeqNo = pLbmNode->u4SeqNum;
    ECFM_RM_GET_N_BYTE (pMsg, pLbmNode->TargetMacAddr, pu2Offset,
                        ECFM_MAC_ADDR_LENGTH);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4BytesSent);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4UnexpLbrIn);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4NumOfResponders);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbmNode->u4DupLbrIn);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pLbmNode->u2NoOfLBMSent);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pLbmNode->u2NoOfLBRRcvd);

    /* Add RBTree Node */
    if (RBTreeAdd (ECFM_LBLT_LBM_TABLE, pLbmNode) != ECFM_RB_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: LB Node in RBTree Add Failed!!\r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_LBM_TABLE_POOL, (UINT1 *) pLbmNode);
        return;
    }

    /* Initialise the LBR SLL for this LBM Node to store the LBRs that will be
     * received for this LBM sequence Number */
    TMO_SLL_Init (&(pLbmNode->LbrList));
    u2InfoLen -= ECFM_RED_LBM_BUFF_INFO_SIZE;
    while (u2InfoLen)

    {

        /* Allocate Memory to the LBR Node to be addded in the LbrTable */
        ECFM_ALLOC_MEM_BLOCK_LBLT_LBR_TABLE (pLbrNode);
        while (pLbrNode == NULL)

        {
            if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_LBR_TABLE_POOL) != 0)

            {
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "ECFM RED : Memory Block exists but Memory"
                              "Allocation to LBR Node Failed\r\n");
                return;
            }

            /* If the Addition of this LBR entry exceeds the resources allocated
             * to Lbr Table, then delete the first entry if any */
            pLbmFirstNode = (tEcfmLbLtLbmInfo *) RBTreeGetFirst
                (ECFM_LBLT_LBM_TABLE);
            if (pLbmFirstNode == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "ECFM RED : Memory corrupted\r\n");
                return;
            }
            EcfmLbLtUtilRemoveLbEntry (pLbmFirstNode);
            ECFM_ALLOC_MEM_BLOCK_LBLT_LBR_TABLE (pLbrNode);
        }
        ECFM_MEMSET (pLbrNode, ECFM_INIT_VAL, ECFM_LBLT_LBR_INFO_SIZE);

        /* Get LBR Info from the RM Message */
        ECFM_RM_GET_N_BYTE (pMsg, pLbrNode->LbrSrcMacAddr, pu2Offset,
                            ECFM_MAC_ADDR_LENGTH);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbrNode->u4RcvOrder);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pLbrNode->u4LbrRcvTime);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pLbrNode->u1LbrErrorStatus);

        /* Add LBR  node in the SLL maintained by MEP per Transaction and per LBM's
         * Seq number */
        TMO_SLL_Add (&(pLbmNode->LbrList), (tTMO_SLL_NODE *)
                     & (pLbrNode->LbrTableSllNode));
        u2InfoLen = u2InfoLen - ECFM_RED_LBR_BUFF_INFO_SIZE;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessSyncMsg.                               */
/*                                                                           */
/* Description        : This function process the received sync up msg       */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedLbLtProcessSyncMsg (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT2               u2InfoLen = ECFM_INIT_VAL;
    UINT1               u1InfoType = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {

        /* Not a Standby node, hence don't process the received sync message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Node Not in Stand-By Mode \n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Data corruption =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }

    /* Get Type (MepInfo, RmepDbInfo, MipCcmDbInfo, PortInfo, ContextInfo,
     * Global Info from the Value Field */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1InfoType);

    /* Decrement MesgSize with 1 byte Type Field */
    i2MsgSize = i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE;
    switch (u1InfoType)

    {
        case ECFM_RED_DM_BUFF_INFO:
            while (i2MsgSize > 0)

            {

                /* Get Length of the DM Buffer Info in the Message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next DM Buff Info from the buffer 
                 */
                i2MsgSize = i2MsgSize - (INT2)
                    (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen);

                /* Extract and update DM Buffer Info in the Node */
                EcfmRedUpdateDmBuffInfo (pMsg, &u2Offset, u2InfoLen);
                i2MsgSize = i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE;
                u2Offset = u2Offset + ECFM_RED_TYPE_FIELD_SIZE;
            } break;
        case ECFM_RED_LB_BUFF_INFO:
            while (i2MsgSize > 0)

            {

                /* Get Length of the LB Buffer Info in the Message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next LB Buff Info from the buffer 
                 */
                i2MsgSize = i2MsgSize - (INT2)
                    (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen);

                /* Extract and update LB Buffer Info in the Node */
                EcfmRedUpdateLbBuffInfo (pMsg, &u2Offset, u2InfoLen);
                i2MsgSize = i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE;
                u2Offset = u2Offset + ECFM_RED_TYPE_FIELD_SIZE;
            } break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Invalid Info in the buffer \r\n");
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedStartLBLTTimersOnActiveEvent                  */
/*                                                                           */
/* Description        : This function starts the LBR Hold Timer & LBLT Delay */
/*                      queue Timer on reception of GO ACTIVE event from RM  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedStartLBLTTimersOnActiveEvent ()
{
    UINT4               u4ContextId = ECFM_INIT_VAL;

    for (u4ContextId = 0; u4ContextId < LBLT_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_SYSTEM_STARTED (u4ContextId) != ECFM_START)

        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            continue;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Starting LBLT timers for context = [%d] \r\n",
                      u4ContextId);

        /* Check whether Timer Is already running or not */

        /* Start LBR Cache Hold Timer */
        if (EcfmLbLtTmrStartTimer
            (ECFM_LBLT_TMR_LBR_HOLD, NULL,
             ECFM_NUM_OF_MSEC_IN_A_SEC *
             (ECFM_LBLT_LBR_CACHE_HOLD_TIME * 60)) != ECFM_SUCCESS)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: LBR Cache Hold Timer Start FAILED\r\n");
            ECFM_LBLT_RELEASE_CONTEXT ();
            continue;
        }

        /* Check whether Timer Is already running or not */

        /* Start the free running LTF while timer */
        if (EcfmLbLtTmrStartTimer
            (ECFM_LBLT_TMR_DELAY_QUEUE, NULL,
             ECFM_NUM_OF_MSEC_IN_A_SEC *
             (ECFM_DELAY_QUEUE_INTERVAL)) != ECFM_SUCCESS)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: LTF While Timer Start FAILED\r\n");
            ECFM_LBLT_RELEASE_CONTEXT ();
            continue;
        }
        ECFM_LBLT_RELEASE_CONTEXT ();
    }
}

/*****************************************************************************/
/* Function Name      : EcfmRedClearDmSyncFlag                               */
/*                                                                           */
/* Description        : This function clears the Sync Flag maintained in DM  */
/*                      Buffer.                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedClearDmSyncFlag ()
{
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;

    /* Get DM Buffer First Node */
    pFrmDelayBuffNode = (tEcfmLbLtFrmDelayBuff *)
        RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);
    while (pFrmDelayBuffNode != NULL)

    {

        /* Clear the Sync Flag */
        pFrmDelayBuffNode->b1Synched = ECFM_FALSE;

        /* Get Next LM Buffer Entry */
        pFrmDelayBuffNode = (tEcfmLbLtFrmDelayBuff *)
            RBTreeGetNext (ECFM_LBLT_FD_BUFFER_TABLE, pFrmDelayBuffNode, NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedClearLbSyncFlag                               */
/*                                                                           */
/* Description        : This function clears the Sync Flag maintained in LB  */
/*                      Buffer.                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedClearLbSyncFlag ()
{
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;

    /* Get LB Buffer First Node */
    pLbmNode = (tEcfmLbLtLbmInfo *) RBTreeGetFirst (ECFM_LBLT_LBM_TABLE);
    while (pLbmNode != NULL)

    {

        /* Clear the Sync Flag */
        pLbmNode->b1Synched = ECFM_FALSE;

        /* Get Next LB Buffer Entry */
        pLbmNode =
            (tEcfmLbLtLbmInfo *) RBTreeGetNext (ECFM_LBLT_LBM_TABLE, pLbmNode,
                                                NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLbLtTransStatus                           */
/*                                                                           */
/* Description        : This function will send the LB, DM, TST Transmit     */
/*                      status to STANDBY node                               */
/*                                                                           */
/* Input(s)           : u4ContextId   - Current Context Id                   */
/*                      u4MdIndex     - MdIndex for the MEP                  */
/*                      u4MaIndex     - MaIndex for the MEP                  */
/*                      u2MepId       - MdIndex for the MEP                  */
/*                      u1LbTransSts  - LB Transaction Status for the MEP    */
/*                      u1TstTransSts - TST Transaction Status for the MEP   */
/*                      u1DmTransSts  - DM Transaction Status for the MEP    */
/*                      u1ThTransSts  - TH Transaction Status for the MEP    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLbLtTransStatus (UINT4 u4ContextId, UINT4 u4MdIndex, UINT4 u4MaIndex,
                            UINT2 u2MepId, UINT1 u1LbTransSts,
                            UINT1 u1TstTransSts, UINT1 u1DmTransSts,
                            UINT1 u1ThTranSts)
{
    tRmMsg             *pMsg;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT2               u2Offset = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the Trans Status \n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LBLT sync up messages"
                      "cannot be sent if the standby node is down.\n");
        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_SYNC_LBLT_TRANS_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_SYNC_LBLT_TRANS_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LBLT_SYNC_TRANS_STS);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2MepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, u1LbTransSts);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, u1TstTransSts);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, u1DmTransSts);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, u1ThTranSts);
    u4BulkUnitSize = ECFM_SYNC_LBLT_TRANS_MSG_SIZE;

    IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM", (CHR1 *) "tEcfmLbLtMepInfo",
                                u4BulkUnitSize);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLbLtTransSts                           */
/*                                                                           */
/* Description        : This function process the received LB, DM, TST       */
/*                      transaction Status Message.                          */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLbLtTransSts (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT1               u1LbTransSts = ECFM_INIT_VAL;
    UINT1               u1TstTransSts = ECFM_INIT_VAL;
    UINT1               u1DmTransSts = ECFM_INIT_VAL;
    UINT1               u1ThTransSts = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process AIS Condition Set"
                      "in case of standby Only .\n");
        return;
    }
    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: LBLT Data corruption " "=[%d][%d].\n",
                      u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1LbTransSts);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1TstTransSts);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1DmTransSts);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1ThTransSts);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        return;
    }

    /* Set LB Transaction Status for this MEP */
    if (u1LbTransSts == ECFM_TX_STATUS_NOT_READY)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_LB_START_TRANSACTION)
            != ECFM_SUCCESS)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Post Event for LB Start Transaction Failed"
                          "\r\n");
        }
    }

    /* Set TST Transaction Status for this MEP */
    if (u1TstTransSts == ECFM_TX_STATUS_NOT_READY)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_TST_START_TRANSACTION)
            != ECFM_SUCCESS)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Post Event for TST Start Transaction Failed"
                          "\r\n");
        }
    }

    /* Set DM Transaction Status for this MEP */
    if (u1DmTransSts == ECFM_TX_STATUS_NOT_READY)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_DM_START_TRANSACTION)
            != ECFM_SUCCESS)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Post Event for DM Start Transaction Failed"
                          "\r\n");
        }
    }

    /* Set Throughput Transaction Status for this MEP */
    if (u1ThTransSts == ECFM_TX_STATUS_NOT_READY)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_TH_START_TRANSACTION)
            != ECFM_SUCCESS)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Post Event for DM Start Transaction Failed"
                          "\r\n");
        }
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************
 *                          End of cfmlbred.c                                  *
 *****************************************************************************/
