/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccrx.c,v 1.53 2017/02/09 13:51:24 siva Exp $
 *
 * Description: This file contains the Functionality of the CC 
 *                        Receiver of the Control Sub Module.
 *******************************************************************/

#include "cfminc.h"

/* Prototypes for the routines only used in this file */
PRIVATE INT4        EcfmCcCtrlRxOpCodeDeMux
PROTO ((tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1DeMuxType));
PRIVATE INT4 EcfmCcCtrlRxParsePduHdrs PROTO ((tEcfmCcPduSmInfo *));
PRIVATE INT4 EcfmCcCtrlRxForwardCfmPdu PROTO ((tEcfmCcPduSmInfo * pPduSmInfo));
PRIVATE BOOL1 EcfmCcCheckLockedCondition PROTO ((tEcfmCcPduSmInfo *));
PRIVATE VOID        EcfmAddVlanTagToFrame
PROTO ((tEcfmBufChainHeader *, tEcfmVlanTag));
/*****************************************************************************
 * Name               : EcfmCcCtrlRxPkt 
 *
 * Description        : Receives the incoming frame from lower layer and Frame 
 *                      filtering, validates the CFM-PDU Header and checks for 
 *                      Link aggregated to selects the appropriate port if packet 
 *                      received from port for level de multiplexing. 
 *
 * Input(s)           : pBuf - Pointer to the received buffer
 *                      u4IfIndex - Index of Interface on which CFM-PDU was 
 *                      received.
 *                      u2PortNum -  Logical port number
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCES/ECFM_FAILURE
 *****************************************************************************/

PUBLIC INT4
EcfmCcCtrlRxPkt (tEcfmBufChainHeader * pBuf, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    static tEcfmCcPduSmInfo PduSmInfo;    /* To store information about the
                                         * MP and the so far parsed CFM-PDU
                                         */
    tEcfmCcPortInfo    *pPortInfo = NULL;    /* Pointer to Port specific info */
    BOOL1               b1PortInChannel = ECFM_FALSE;

    ECFM_CC_TRC_FN_ENTRY ();

    /*Reset the PDU-SM info to all zeros */
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));

    PduSmInfo.u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

    ECFM_CC_TRC_ARG2 (ECFM_DATA_PATH_TRC,
                      "EcfmCcCtrlRxPkt: "
                      "ECFM PDU received on Port,IfIndex =[%d] [%d]\r\n",
                      u2PortNum, u4IfIndex);
    ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, PduSmInfo.u4ByteCount,
                      "EcfmCcCtrlRxPkt: "
                      "Dumping CFMPDU received from lower layer...\r\n");

    pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);
    if (pPortInfo == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlRxPkt: " "No Port Information available \r\n");
        return ECFM_FAILURE;
    }

    if (pPortInfo->u2ChannelPortNum != 0)
    {
        pPortInfo = ECFM_CC_GET_PORT_INFO (pPortInfo->u2ChannelPortNum);
        if (pPortInfo != NULL)
        {
            b1PortInChannel = ECFM_TRUE;
        }
        else
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxPkt: "
                         "No Port Information available \r\n");
            return ECFM_FAILURE;
        }
    }

    PduSmInfo.pPortInfo = pPortInfo;
    PduSmInfo.pBuf = pBuf;
    PduSmInfo.pHeaderBuf = NULL;
    PduSmInfo.u1RxDirection = ECFM_MP_DIR_DOWN;
    PduSmInfo.u1NoFwd = OSIX_FALSE;

    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        PduSmInfo.u1RxPbbPortType = ECFM_CC_GET_PORT_TYPE (u2PortNum);
    }

    /* Routine to get VlanTag,MdLevel,opcode,flag,FirstTLVOffset 
     * from the PDU 
     */
    if (EcfmCcCtrlRxParsePduHdrs (&PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlRxPkt: "
                     "EcfmCcCtrlRxParsePduHdrs returned Failure\r\n");
        /* Increment the rx bad CFM-PDU counter */
        ECFM_CC_INCR_RX_BAD_CFM_PDU_COUNT (u2PortNum);
        ECFM_CC_INCR_CTX_RX_BAD_CFM_PDU_COUNT (PduSmInfo.pPortInfo->
                                               u4ContextId);
        return ECFM_FAILURE;
    }
    if (PduSmInfo.u1NoFwd == OSIX_TRUE)
    {
        return ECFM_SUCCESS;
    }
    if (b1PortInChannel)
    {
        /* Set the port as the one on which we have received the packet */
        /* Routine to get Port Info */
        pPortInfo = ECFM_CC_GET_PORT_INFO (u2PortNum);

        if (pPortInfo == NULL)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxPkt: "
                         "No Port Information available \r\n");
            return ECFM_FAILURE;
        }
        else
        {
            PduSmInfo.pPortInfo = pPortInfo;
        }
    }
    /* Every CFM-PDU must first be forwared to the unware MEP */
    /* Check if unware MEP is present on the port for the 
     * received untagged CFM-PDU*/
    if ((PduSmInfo.VlanClassificationInfo.OuterVlanTag.u1TagType ==
         ECFM_VLAN_UNTAGGED) &&
        (ECFM_CC_UNAWARE_MEP_PRESENT (u2PortNum) == ECFM_TRUE))
    {
        /* only untagged CFM-PDU are processed by VLAN unaware MEPs */
        PduSmInfo.u4RxVlanIdIsId = 0;
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlRxPkt: "
                     "untagged cfm-pdu received on a port with VLAN unaware MEP\r\n");
    }
    else
    {
        /* check if the port is part of a port-channel */
        if (pPortInfo->u2ChannelPortNum != 0)
        {
            /* switch the port with port channel */
            pPortInfo = ECFM_CC_GET_PORT_INFO (pPortInfo->u2ChannelPortNum);
            if (pPortInfo != NULL)
            {
                PduSmInfo.pPortInfo = pPortInfo;
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxPkt: "
                             "cfm-pdu received on a port which is a member of port channel\r\n");
            }
        }
    }
    /* Increment the per port pdu counters */

    ECFM_CC_INCR_RX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->u2PortNum);
    ECFM_CC_INCR_CTX_RX_CFM_PDU_COUNT (PduSmInfo.pPortInfo->u4ContextId);
    ECFM_CC_INCR_RX_COUNT (PduSmInfo.pPortInfo->u2PortNum,
                           PduSmInfo.u1RxOpcode);
    ECFM_CC_INCR_CTX_RX_COUNT (PduSmInfo.pPortInfo->u4ContextId,
                               PduSmInfo.u1RxOpcode);
    ECFM_BUF_SET_OPCODE (pBuf, PduSmInfo.u1RxOpcode);
#ifdef MBSM_WANTED
    /* For handling Multiple LC Indications 
     * Update interface index on which PDU is received from CFA */
    if (PduSmInfo.u1RxOpcode == ECFM_OPCODE_CCM)
    {
        PduSmInfo.u4InterfaceId = u4IfIndex;
    }
#else
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4IfIndex);
#endif

    /* Selects the MP on the Port for which CFM-PDU is received and passes the
     * Parsed PDU to that MP for further processing 
     */
    if (EcfmCcCtrlRxLevelDeMux (&PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlRxPkt: "
                     "EcfmCcCtrlRxLevelDeMux returned Failure\r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmCcCtrlRxLevelDeMux
 *
 * Description        : This routine selects the MP on the Port using VLAN-ID 
 *                      and MDLevel received in the CFM-PDU from the Stack 
 *                      Table in UP or DOWN direction. 
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this 
 *                      structure holds the information about the MP and the 
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmCcCtrlRxLevelDeMux (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcStackInfo   *pStackInfo = NULL;    /* Pointer to Stack info */
    tEcfmCcMepInfo     *pMepInfo = NULL;    /* Pointer to Mep info */
    tEcfmCcMipInfo     *pMipInfo = NULL;    /* Pointer to MIP Info */
    tMacAddr            TempMacAddr;
    UINT2               u2PortNum = ECFM_INIT_VAL;    /* Interface Index of the port */
    UINT1               u1MdLevel = ECFM_INIT_VAL;    /* Mdlevel at which MP is configured 
                                                     */
    INT4                i4RetVal = ECFM_FAILURE;
    BOOL1               bSrcTgtAddrSame = ECFM_FALSE;
    ECFM_CC_TRC_FN_ENTRY ();

    u2PortNum = pPduSmInfo->pPortInfo->u2PortNum;
    /* If the Received packet is a MPLSTP-OAM Packet then
     * process the packet with OpcodeDemux level as 
     * Equal.
     */
    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        if (pPduSmInfo->u1RxMdLevel ==
            pPduSmInfo->pMepInfo->pMaInfo->pMdInfo->u1Level)
        {
            if (EcfmCcCtrlRxOpCodeDeMux (pPduSmInfo, ECFM_CC_DEMUX_EQUAL) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_INCR_CTX_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u4ContextId);
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxLevelDeMux: "
                             "EcfmCcCtrlRxOpCodeDeMux returned Failure\r\n");
                return ECFM_FAILURE;
            }
        }
        else
        {
            if (EcfmCcCtrlRxOpCodeDeMux (pPduSmInfo, ECFM_CC_DEMUX_LOW)
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxLevelDeMux: "
                             "EcfmCcCtrlRxOpCodeDeMux returned " "Failure\r\n");
                return ECFM_FAILURE;
            }
        }
        return ECFM_SUCCESS;
    }
    /* Forward the CFM-PDU as normal data-packet in case ECFM is disabled
     * on this port*/
    if (ECFM_CC_IS_PORT_MODULE_STS_ENABLED (u2PortNum) == ECFM_FALSE)
    {
        return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
    }
    /* Apply the forwarding rule of CBP and PIP */
    switch (pPduSmInfo->u1RxPbbPortType)
    {
        case ECFM_CUSTOMER_BACKBONE_PORT:
            /* 
             * For the CBP port we need to do DeMux for B-VID for PDU w/o I-TAG and
             * DeMux for ISID for PDU with I-TAG.
             */
            if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
                VLAN_TAGGED)
            {
                /* for CBP port we only need to do DeMux for CFM-PDUs having the
                 * UCA-Bit as ON, else this PDU was intended for CN/PBN*/
                if (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.
                    u1UcaBitValue == OSIX_TRUE)
                {
                    /* Overwrite the B-VID with ISID for I-Tagged CFM-PDUs */
                    pPduSmInfo->u4RxVlanIdIsId =
                        ECFM_ISID_TO_ISID_INTERNAL (pPduSmInfo->
                                                    PbbClassificationInfo.
                                                    InnerIsidTag.u4Isid);
                }
                else
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcCtrlRxLevelDeMux: "
                                 "EcfmCcCtrlRxOpCodeDeMux UCA-Bit is OFF, no need to DeMux at CBP\r\n");

                    /* Check if any lower Level MEP is locked */
                    if (EcfmCcCheckLockedCondition (pPduSmInfo) == ECFM_TRUE)
                    {
                        return ECFM_SUCCESS;
                    }

                    return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
                }
            }
            break;
        default:
            break;

    }
    /* Check if any lower Level MEP is locked */
    if (EcfmCcCheckLockedCondition (pPduSmInfo) == ECFM_TRUE)
    {
        return ECFM_SUCCESS;
    }

    /* This is function is used to get entry from Stack table */
    pStackInfo = EcfmCcUtilGetMp (u2PortNum,
                                  pPduSmInfo->u1RxMdLevel,
                                  pPduSmInfo->u4RxVlanIdIsId,
                                  pPduSmInfo->u1RxDirection);
    /* If MP exists at same Level exists */
    if (pStackInfo != NULL)
    {
        pPduSmInfo->pStackInfo = pStackInfo;
        /* if Mep of same level exists */
        if (ECFM_CC_IS_MEP (pStackInfo))
        {
            /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
             * and the MD Level
             */
            pMepInfo = pStackInfo->pMepInfo;

            /* Fill adress of MepInfo (i.e pMepInfo) in PduSmInfo */
            pPduSmInfo->pMepInfo = pMepInfo;
        }
        else
        {
            /* Get the MIP Info  from the Stack Info indexed by Vlan Id 
             * and the MD Level
             */
            pMipInfo = pStackInfo->pMipInfo;
            /* Fill adress of MipInfo (i.e pMipInfo) in PduSmInfo */
            pPduSmInfo->pMipInfo = pMipInfo;

        }
        /* Call the OpCode Demultiplexer */
        if (EcfmCcCtrlRxOpCodeDeMux (pPduSmInfo, ECFM_CC_DEMUX_EQUAL) !=
            ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxLevelDeMux: "
                         "EcfmCcCtrlRxOpCodeDeMux returned Failure\r\n");
            return ECFM_FAILURE;
        }
        i4RetVal = ECFM_SUCCESS;
    }
    else
    {
        /* Get next higher level mp if any from the port belongs to same Vid and 
         * same direction 
         */
        for (u1MdLevel = pPduSmInfo->u1RxMdLevel + 1;
             u1MdLevel <= (UINT1) ECFM_MD_LEVEL_MAX; u1MdLevel = u1MdLevel + 1)
        {
            pStackInfo = EcfmCcUtilGetMp (u2PortNum, u1MdLevel,
                                          pPduSmInfo->u4RxVlanIdIsId,
                                          pPduSmInfo->u1RxDirection);
            /* If MP at Higer Level exists on Port */
            if (pStackInfo != NULL)
            {
                pPduSmInfo->pStackInfo = pStackInfo;

                /* if Mep at high level exists then Pdu is processed */
                if (ECFM_CC_IS_MEP (pStackInfo))
                {
                    /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                     * and the MD Level
                     */
                    pMepInfo = pStackInfo->pMepInfo;
                    /* Fill MepInfo in PduSmInfo */
                    pPduSmInfo->pMepInfo = pMepInfo;

                    /* MEP Is present at the high level, validate the Pkt 
                     * on the basis of the OpCode(CCM)only
                     */
                    /* make sure that we also respond with tagged/untagged packet */
                    pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u2VlanId =
                        (UINT2) pPduSmInfo->u4RxVlanIdIsId;
                    if (EcfmCcCtrlRxOpCodeDeMux (pPduSmInfo, ECFM_CC_DEMUX_LOW)
                        != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlRxLevelDeMux: "
                                     "EcfmCcCtrlRxOpCodeDeMux returned "
                                     "Failure\r\n");
                        return ECFM_FAILURE;
                    }
                    /* Stop Processing as MEP is found at higher level */
                    return ECFM_SUCCESS;
                }
                else
                {
                    /*MIP forwards the CFM-PDU in all cases */
                    break;
                }
            }
        }                        /* end of for Loop */

        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pPduSmInfo->pPortInfo->u2PortNum)
            == ECFM_TRUE)
        {
            ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
            ECFM_GET_MAC_ADDR_OF_PORT
                (pPduSmInfo->pPortInfo->u4IfIndex, TempMacAddr);

            if (ECFM_MEMCMP (pPduSmInfo->RxSrcMacAddr, TempMacAddr,
                             ECFM_MAC_ADDR_LENGTH) == ECFM_INIT_VAL)
            {
                bSrcTgtAddrSame = ECFM_TRUE;
            }
            if (!
                ((bSrcTgtAddrSame)
                 && (pPduSmInfo->u1RxOpcode == ECFM_OPCODE_LCK)))
            {
                /* In case Mep is configured Out of service then block data 
                 * traffic through it */
                INT1                i1MdLevel = ECFM_INIT_VAL;
                for (i1MdLevel = pPduSmInfo->u1RxMdLevel - 1;
                     i1MdLevel >= ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
                {
                    pStackInfo = EcfmCcUtilGetMp (u2PortNum, i1MdLevel,
                                                  pPduSmInfo->u4RxVlanIdIsId,
                                                  ECFM_MP_DIR_DOWN);
                    /* If MP at Lower Level exists on Port */
                    if (pStackInfo != NULL)
                    {
                        pPduSmInfo->pStackInfo = pStackInfo;

                        /* if Mep at Low level exists */
                        if (ECFM_CC_IS_MEP (pStackInfo))
                        {
                            /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                             * and the MD Level
                             */
                            pMepInfo = pStackInfo->pMepInfo;

                            if (ECFM_CC_LCK_IS_CONFIGURED (pMepInfo) ==
                                ECFM_TRUE)
                            {
                                /* Stop Processing as MEP is configured Out of Service */
                                return ECFM_SUCCESS;
                            }
                        }
                    }
                }
                for (i1MdLevel = pPduSmInfo->u1RxMdLevel - 1;
                     i1MdLevel >= (INT1) ECFM_MD_LEVEL_MIN;
                     i1MdLevel = i1MdLevel - 1)
                {
                    pStackInfo = EcfmCcUtilGetMp (u2PortNum, i1MdLevel,
                                                  pPduSmInfo->u4RxVlanIdIsId,
                                                  ECFM_MP_DIR_UP);
                    /* If MP at Lower Level exists on Port */
                    if (pStackInfo != NULL)
                    {
                        pPduSmInfo->pStackInfo = pStackInfo;

                        /* if Mep at Low level exists */
                        if (ECFM_CC_IS_MEP (pStackInfo))
                        {
                            /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                             * and the MD Level
                             */
                            pMepInfo = pStackInfo->pMepInfo;

                            if (ECFM_CC_LCK_IS_CONFIGURED (pMepInfo) ==
                                ECFM_TRUE)
                            {
                                /* Stop Processing as MEP is configured Out of Service */
                                return ECFM_SUCCESS;
                            }
                        }
                    }
                }
            }
        }
        /* No MP at the same level or at any higher level is found */
        /* check if level-demux happened for unaware MEP */
        if (pPduSmInfo->u4RxVlanIdIsId == 0)
        {
            /* switch to the classified PVID */
            pPduSmInfo->u4RxVlanIdIsId = pPduSmInfo->VlanClassificationInfo.
                OuterVlanTag.u2VlanId;
            /* If receiving port is part of a port channel then we need to level de-mux
             * again for the port channel, with the received vlan-id
             */
            ECFM_CC_TRC_ARG1 (ECFM_CONTROL_PLANE_TRC,
                              "EcfmCcCtrlRxLevelDeMux: "
                              "VLAN unware MEP not found with the level %d \r\n",
                              pPduSmInfo->u1RxMdLevel);
            if (pPduSmInfo->pPortInfo->u2ChannelPortNum != 0)
            {
                /* switch the port with port channel */
                pPduSmInfo->pPortInfo =
                    ECFM_CC_GET_PORT_INFO (pPduSmInfo->pPortInfo->
                                           u2ChannelPortNum);
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxLevelDeMux: "
                             "switching port info to port channel's info \r\n");
            }
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxLevelDeMux: "
                         "level demux again ... \r\n");
            if (pPduSmInfo->u4RxVlanIdIsId != 0)
            {
                i4RetVal = EcfmCcCtrlRxLevelDeMux (pPduSmInfo);
            }
            else
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                             ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxLevelDeMux: "
                             "VLAN-ID as zero received \r\n");

                i4RetVal = ECFM_FAILURE;
            }
        }
        /* Check if MEP for Opposite direction (opposite of which received in the
         * PDU) is configured on the port. In that case PDU has to be dropped as per
         * specification.
         */
        else if (EcfmCcUtilGetMepEntryFrmPort (pPduSmInfo->u1RxMdLevel,
                                               pPduSmInfo->u4RxVlanIdIsId,
                                               u2PortNum,
                                               ECFM_REVERSE_MEP_DIR
                                               (pPduSmInfo->u1RxDirection)) !=
                 NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmCcCtrlRxLevelDeMux: "
                         "CFM PDU is dropped as Same Level BUT Opposite direction"
                         "MEP as received in PDU exists on the port\r\n");
            /* Discard CFM PDU */
            return ECFM_SUCCESS;
        }
        else
        {
            /* Forward the CFM-PDU */
            i4RetVal = EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Name               : EcfmCcCtrlRxOpCodeDeMux
 *
 * Description        : This routine is invoked whenever a MEP at the same 
 *                      level as that of the CFM-PDU received is found on 
 *                      the port. It validate the OpCode received in the 
 *                      CFM-PDU for CCM only, this routine also calls the 
 *                      ECFM client routine to parse the CFM-PDU.and calls the 
 *                      State machine of the Client.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU.
 *                      u1DeMuxType - Type of the Level DeMultiplexer Equal/Low
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmCcCtrlRxOpCodeDeMux (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1DeMuxType)
{
    UINT1              *pu1EcfmPdu = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2StripLen = ECFM_INIT_VAL;
    BOOL1               bFwrdPdu = ECFM_FALSE;

    ECFM_CC_TRC_FN_ENTRY ();

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: NODE NOT ACTIVE- Dropping the packet \r\n");
        return i4RetVal;
    }

    /* CCM PDU is Parsed and CCM RX SM is called 
     * if and only if that MEP/MHF is in Active State 
     */
    if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
    {
        /* Incase of MPLSTP-OAM check is done only to verify is this 
         * Active MEP, check is not made to verify MEP */
        if (ECFM_CC_IS_MEP_ACTIVE (pPduSmInfo->pMepInfo) == ECFM_FALSE)
        {
            return i4RetVal;
        }
    }
    else
    {
        if (ECFM_CC_IS_MEP (pPduSmInfo->pStackInfo))
        {
            if (ECFM_CC_IS_MEP_ACTIVE (pPduSmInfo->pMepInfo) == ECFM_FALSE)
            {
                return i4RetVal;
            }
        }
        else
        {
            if (ECFM_CC_IS_MIP_ACTIVE (pPduSmInfo->pMipInfo) == ECFM_FALSE)
            {
                return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
            }
        }
    }
    /* In case Y1731 Specific PDUs are received & Y1731 is disabled on the
     * receiving port then do not process further */
    switch (pPduSmInfo->u1RxOpcode)
    {
        case ECFM_OPCODE_AIS:
        case ECFM_OPCODE_LCK:
        case ECFM_OPCODE_LMM:
        case ECFM_OPCODE_LMR:
            if ((u1DeMuxType == ECFM_CC_DEMUX_LOW) ||
                (ECFM_CC_IS_Y1731_DISABLED_ON_PORT
                 (pPduSmInfo->pPortInfo->u2PortNum) == ECFM_TRUE))
            {
                /* Either a lower level PDU has been received by a higher layer MEP.
                 * OR
                 * Y1731 is disabled on this port.
                 * This PDU should be forwarded
                 */
                return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            break;
        default:
            break;
    }

    pBuf = pPduSmInfo->pBuf;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /*Remove the ethernet header size from the pdu length */
    u4ByteCount = u4ByteCount - pPduSmInfo->u1CfmPduOffset;
    pu1EcfmPdu = ECFM_GET_DATA_PTR_IF_LINEAR (pBuf,
                                              pPduSmInfo->u1CfmPduOffset,
                                              u4ByteCount);
    if (pu1EcfmPdu == NULL)
    {
        pu1EcfmPdu = ECFM_CC_PDU;
        ECFM_MEMSET (pu1EcfmPdu, ECFM_INIT_VAL, ECFM_MAX_PDU_SIZE);
        if (ECFM_COPY_FROM_CRU_BUF (pBuf, pu1EcfmPdu,
                                    pPduSmInfo->u1CfmPduOffset, u4ByteCount)
            == ECFM_CRU_FAILURE)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxOpCodeDeMux: "
                         "Received BPDU Copy From CRU Buffer FAILED\r\n");
            i4RetVal = ECFM_FAILURE;
            return i4RetVal;
        }
    }
    /* Incase we have the UCA bit as ON then we need to use the CDA and BDA
     * for CFM funtionality as the CFM-PDU was generated from the PBBN only*/

    if ((pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType ==
         VLAN_TAGGED)
        && (pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1UcaBitValue ==
            OSIX_TRUE))
    {
        ECFM_MEMCPY (pPduSmInfo->RxSrcMacAddr,
                     pPduSmInfo->PbbClassificationInfo.InnerIsidTag.CSAMacAddr,
                     sizeof (tMacAddr));
        ECFM_MEMCPY (pPduSmInfo->RxDestMacAddr,
                     pPduSmInfo->PbbClassificationInfo.InnerIsidTag.CDAMacAddr,
                     sizeof (tMacAddr));
        /* Strip off the BDA, BSA, B-TAG and I-TAG if present, we will add
         * new BDA, BSA, B-TAG and I-TAG for the response PDU, ECFM must not
         * change anyting in the BDA, BSA, I-TAG for the B-TAG*/
        /*BDA + BSA */
        u2StripLen = 2 * ECFM_MAC_ADDR_LENGTH;
        /*ISID length + Ether Type */
        u2StripLen += PBB_ISID_TAG_PID_LEN;
        pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType =
            VLAN_UNTAGGED;
        if (pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType ==
            VLAN_TAGGED)
        {
            /*B-VLAN Tag length */
            u2StripLen += ECFM_VLAN_HDR_SIZE;
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_UNTAGGED;
        }
        pPduSmInfo->pHeaderBuf = pPduSmInfo->pBuf;
        if (ECFM_FRAGMENT_CRU_BUF
            (pPduSmInfo->pHeaderBuf, u2StripLen,
             &pPduSmInfo->pBuf) != ECFM_CRU_SUCCESS)
        {
            i4RetVal = ECFM_FAILURE;
            return i4RetVal;
        }
        pPduSmInfo->u1CfmPduOffset -= u2StripLen;
    }
    switch (pPduSmInfo->u1RxOpcode)
    {
        case ECFM_OPCODE_CCM:
            if (EcfmCcClntParseCcm (pPduSmInfo, pu1EcfmPdu) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxOpCodeDeMux: "
                             "Could not Parse LBR PDU\r\n");
                /* Increment the rx bad CFM-PDU counter */
                ECFM_CC_INCR_RX_BAD_CFM_PDU_COUNT
                    (pPduSmInfo->pPortInfo->u2PortNum);
                ECFM_CC_INCR_CTX_RX_BAD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->
                                                       u4ContextId);
                i4RetVal = ECFM_FAILURE;
                break;
            }
            if (pPduSmInfo->pPortInfo->u1IfType == CFA_MPLS)
            {
                if (u1DeMuxType == ECFM_CC_DEMUX_EQUAL)
                {
                    if (EcfmCcClntProcessEqCcm (pPduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlRxOpCodeDeMux: "
                                     "Could not process "
                                     "the equal CC RX for MPLS-TP-OAM \r\n");
                        i4RetVal = ECFM_FAILURE;
                        break;
                    }
                }
                else
                {
                    if (EcfmCcClntProcessLowCcm (pPduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlRxOpCodeDeMux: "
                                     "Could not Process "
                                     "the low CCM RX \r\n");
                        i4RetVal = ECFM_FAILURE;
                        break;
                    }
                }

                /* If the received CCM is for Performance monitorig,
                 * Calculate the Frame Loss from the received Info
                 */
                if (pPduSmInfo->pMepInfo->pMaInfo->u1CcRole == ECFM_CC_ROLE_PM)
                {
                    if (EcfmCcClntCalcFrameLoss
                        (pPduSmInfo, ECFM_CC_LM_TYPE_2LM) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                                     ECFM_ALL_FAILURE_TRC,
                                     "EcfmCcCtrlRxOpCodeDeMux: Not able "
                                     "to calculate Frame Loss\r\n");
                        i4RetVal = ECFM_FAILURE;
                        break;
                    }
                }
            }
            else
            {
                if (ECFM_CC_IS_MEP (pPduSmInfo->pStackInfo))
                {
                    if (u1DeMuxType == ECFM_CC_DEMUX_EQUAL)
                    {
                        if (EcfmCcClntProcessEqCcm (pPduSmInfo) != ECFM_SUCCESS)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "EcfmCcCtrlRxOpCodeDeMux: "
                                         "Could not process "
                                         "the equal CC RX \r\n");
                            i4RetVal = ECFM_FAILURE;
                            break;
                        }
                    }
                    else
                    {
                        if (EcfmCcClntProcessLowCcm (pPduSmInfo) !=
                            ECFM_SUCCESS)
                        {
                            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                         ECFM_CONTROL_PLANE_TRC,
                                         "EcfmCcCtrlRxOpCodeDeMux: "
                                         "Could not Process "
                                         "the low CCM RX \r\n");
                            i4RetVal = ECFM_FAILURE;
                            break;
                        }
                    }
                    /* If the received CCM is for Performance monitorig,
                     * Calculate the Frame Loss from the received Info
                     */
                    if (pPduSmInfo->pMepInfo->pMaInfo->u1CcRole ==
                        ECFM_CC_ROLE_PM)
                    {
                        if (EcfmCcClntCalcFrameLoss
                            (pPduSmInfo, ECFM_CC_LM_TYPE_2LM) != ECFM_SUCCESS)
                        {
                            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                                         ECFM_ALL_FAILURE_TRC,
                                         "EcfmCcCtrlRxOpCodeDeMux: Not able "
                                         "to calculate Frame Loss\r\n");
                            i4RetVal = ECFM_FAILURE;
                            break;
                        }
                    }
                }
                else
                {
                    if (EcfmCcClntMhfProcessCcm (pPduSmInfo) != ECFM_SUCCESS)
                    {
                        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC |
                                     ECFM_CONTROL_PLANE_TRC,
                                     "EcfmCcCtrlRxOpCodeDeMux: "
                                     "Could not process "
                                     "the equal CC RX \r\n");
                    }
                    /*forward the CCM */
                    return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
                }
            }
            break;
        case ECFM_OPCODE_LBM:
        case ECFM_OPCODE_LBR:
        case ECFM_OPCODE_LTM:
        case ECFM_OPCODE_LTR:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxOpCodeDeMux: "
                         "LBLT PDU Received on CC Task\r\n");
            pBuf = ECFM_DUPLICATE_CRU_BUF (pPduSmInfo->pBuf);
            /*Signal the packet to LBLT Task */
            if (pBuf != NULL)
            {
                if (EcfmLbLtHandleInFrameFromPort
                    (pBuf, pPduSmInfo->pPortInfo->u4IfIndex) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                 "EcfmCcCtrlRxOpCodeDeMux: Unable to Signal CFM-PDU to"
                                 "LBLT Task \r\n");
                    EcfmCcCtrlRxPktFree (pBuf);
                    pBuf = NULL;
                }
            }
            else
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxOpCodeDeMux: Unable to duplicate"
                             "CRM-Buffer \r\n");
                i4RetVal = ECFM_FAILURE;
                break;
            }
            break;

            /* Y.1731 : AIS opcode type */
        case ECFM_OPCODE_AIS:
            /* Call the AIS Processor */
            if (EcfmCcClntProcessAis (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxOpCodeDeMux: "
                             "Could not process the AIS frame received\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                i4RetVal = ECFM_FAILURE;
                break;
            }
            break;

            /*Y.1731: LCK opcode type */
        case ECFM_OPCODE_LCK:
            /* Call the LCK Processor */
            if (EcfmCcClntProcessLck (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxOpCodeDeMux: "
                             "Could not process the LCK frame received\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                i4RetVal = ECFM_FAILURE;
            }
            break;
        case ECFM_OPCODE_LMM:
            /* Routine to parse and process LMM PDU */
            if (EcfmCcClntProcessLmm (pPduSmInfo, pu1EcfmPdu, &bFwrdPdu)
                != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxOpCodeDeMux: "
                             "Could not Parse LMM PDU\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                i4RetVal = ECFM_FAILURE;
            }
            break;
        case ECFM_OPCODE_LMR:
            /* Routine to parse LMR PDU */
            EcfmCcClntParseLmr (pPduSmInfo, pu1EcfmPdu);
            /* Process the received LMR PDU */
            if (EcfmCcClntProcessLmr (pPduSmInfo, &bFwrdPdu) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxOpCodeDeMux: "
                             "Could not Parse LMR PDU\r\n");
                if (bFwrdPdu == ECFM_TRUE)
                {
                    return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
                }
                i4RetVal = ECFM_FAILURE;
            }
            break;
        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxOpCodeDeMux: " "Invalid OpCode\r\n");
            /*forwarded all other CFM-PDUs in case of MHF */
            if (ECFM_CC_IS_MHF (pPduSmInfo->pStackInfo))
            {
                return EcfmCcCtrlRxForwardCfmPdu (pPduSmInfo);
            }
            else
            {
                /* Discard the PDU */
                i4RetVal = ECFM_FAILURE;
            }
            break;
    }
    /* Check if we did the buffer fragmentation */
    if (pPduSmInfo->pHeaderBuf != NULL)
    {
        /* Check what all tags were fragmented */
        pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType = VLAN_TAGGED;
        if (u2StripLen > ((2 * ECFM_MAC_ADDR_LENGTH) + PBB_ISID_TAG_PID_LEN))
        {
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_TAGGED;
        }
        /* Concat the fragmant with the main buffer */
        ECFM_CONCAT_CRU_BUF (pPduSmInfo->pHeaderBuf, pPduSmInfo->pBuf);
        pPduSmInfo->pBuf = pPduSmInfo->pHeaderBuf;
        pPduSmInfo->pHeaderBuf = NULL;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Name               : EcfmCcCtrlRxParsePduHdrs
 *
 * Description        : This is called to parse the received CFM PDU by 
 *                      extracting the Header values from that and places then 
 *                      into the tEcfmCcPduSmInfo Structure.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS /ECFM_FAILURE
 *
 *****************************************************************************/
PRIVATE INT4
EcfmCcCtrlRxParsePduHdrs (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmVlanTag        VlanTag;    /* Struture containing VlanId, Priority, DE */
    tEcfmPbbTag         PbbTag;
    UINT4               u4Offset = ECFM_INIT_VAL;    /* Stores offset */
    UINT2               u2LocalPort = ECFM_INIT_VAL;    /* Interface Index of the port */
    UINT2               u2TypeLength = ECFM_INIT_VAL;
    UINT1               u1IngressAction = ECFM_INIT_VAL;    /* Stores Ingress Action */
    UINT1               u1IsSend = OSIX_FALSE;
    UINT1               u1MdLvlAndVer = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();

    u2LocalPort = pPduSmInfo->pPortInfo->u2PortNum;
    ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));
    ECFM_MEMSET (&PbbTag, ECFM_INIT_VAL, sizeof (tEcfmPbbTag));

    /* Copy the 6byte destination address from the recived PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxDestMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    /* Copy the 6byte Received source address from the LBM PDU */
    ECFM_CRU_GET_STRING (pPduSmInfo->pBuf, pPduSmInfo->RxSrcMacAddr,
                         u4Offset, ECFM_MAC_ADDR_LENGTH);
    u4Offset = u4Offset + ECFM_MAC_ADDR_LENGTH;

    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        pPduSmInfo->u4ITagOffset = ECFM_INIT_VAL;

        if (EcfmL2IwfGetTagInfoFromFrame (ECFM_CC_CURR_CONTEXT_ID (),
                                          pPduSmInfo->pPortInfo->u4IfIndex,
                                          pPduSmInfo->pBuf, &VlanTag, &PbbTag,
                                          &u1IngressAction,
                                          &pPduSmInfo->u4ITagOffset,
                                          &u4Offset, L2_ECFM_PACKET,
                                          &u1IsSend) == L2IWF_FAILURE)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxParsePduHdrs: "
                         "Unable to get PBB/VLAN information of the frame\r\n");
            return ECFM_FAILURE;
        }

        if (u1IsSend == OSIX_TRUE)
        {
            if (EcfmL2IwfPbbTxFrameToIComp (ECFM_CC_CURR_CONTEXT_ID (),
                                            pPduSmInfo->pPortInfo->u4IfIndex,
                                            pPduSmInfo->pBuf, &VlanTag,
                                            &PbbTag,
                                            &pPduSmInfo->u4ITagOffset,
                                            L2_ECFM_PACKET,
                                            u2LocalPort) == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxParsePduHdrs: "
                             "EcfmL2IwfPbbTxFrameToIComp returned failure\r\n");
                return L2IWF_FAILURE;
            }

            pPduSmInfo->u1NoFwd = OSIX_TRUE;
            return ECFM_SUCCESS;
        }

        pPduSmInfo->u4PduOffset = u4Offset;
        /* copy the Classified VLAN-Information in PDU-SM */
        ECFM_MEMCPY (&pPduSmInfo->VlanClassificationInfo, &VlanTag,
                     sizeof (tEcfmVlanTag));
        /* copy the Classified VLAN-Information in PDU-SM */
        ECFM_MEMCPY (&pPduSmInfo->PbbClassificationInfo, &PbbTag,
                     sizeof (tEcfmPbbTag));
        /* In case we have the UCA Bit sent the I-TAG then we need to use the */
    }
    else
    {
        /*Get the frame VLAN information */
        if (EcfmVlanGetVlanInfoFromFrame (ECFM_CC_CURR_CONTEXT_ID (),
                                          u2LocalPort,
                                          pPduSmInfo->pBuf, &VlanTag,
                                          &u1IngressAction,
                                          &u4Offset) != ECFM_SUCCESS)
        {

            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxParsePduHdrs: "
                         "Unable to get VLAN information of the frame\r\n");
            return ECFM_FAILURE;
        }
        /* Process for CEP */
        if (EcfmVlanProcessPktForCep (ECFM_CC_CURR_CONTEXT_ID (),
                                      u2LocalPort,
                                      pPduSmInfo->pBuf,
                                      &VlanTag) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxParsePduHdrs: "
                         "Unable to process the packet for CEP.\r\n");
        }
        /* copy the Classified VLAN-Information in PDU-SM */
        ECFM_MEMCPY (&pPduSmInfo->VlanClassificationInfo, &VlanTag,
                     sizeof (tEcfmVlanTag));
    }

    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        switch (pPduSmInfo->u1RxPbbPortType)
        {
            case ECFM_CNP_STAGGED_PORT:
            case ECFM_CNP_CTAGGED_PORT:
            case ECFM_CNP_PORTBASED_PORT:
                pPduSmInfo->u4RxVlanIdIsId = VlanTag.OuterVlanTag.u2VlanId;
                break;
            case ECFM_PROVIDER_NETWORK_PORT:
                pPduSmInfo->u4RxVlanIdIsId = PbbTag.OuterVlanTag.u2VlanId;
                break;
            case ECFM_PROVIDER_INSTANCE_PORT:
            case ECFM_CUSTOMER_BACKBONE_PORT:
                /* convert isid to ECFM internal isid */
                pPduSmInfo->u4RxVlanIdIsId =
                    ECFM_ISID_TO_ISID_INTERNAL (PbbTag.InnerIsidTag.u4Isid);
                break;
            default:
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcCtrlRxParsePduHdrs: "
                             "Unable to get VLAN information of the frame\r\n");
                return ECFM_FAILURE;
        }
    }
    else
    {
        pPduSmInfo->u4RxVlanIdIsId = VlanTag.OuterVlanTag.u2VlanId;
    }

    /* Check if the port in CC packet is a member of service VLAN */
    if (ECFM_IS_MEP_VLAN_AWARE (VlanTag.OuterVlanTag.u2VlanId))
    {
       if (EcfmVlanIsMemberPort (ECFM_CC_CURR_CONTEXT_ID (), u2LocalPort,
		VlanTag)!= ECFM_SUCCESS)
       {
	   ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
		       "EcfmCcCtrlRxParsePduHdrs: Failed in Vlan Member"
		       "port validation \r\n");

	   return ECFM_FAILURE;
       }
    }
    /* Get Type Length */
    ECFM_CRU_GET_2_BYTE (pPduSmInfo->pBuf, u4Offset, u2TypeLength);

    /* Move the pointer by the 2 byte to get the MD LEVEL */
    u4Offset = u4Offset + ECFM_VLAN_TYPE_LEN_FIELD_SIZE;

    /* Check if TypeLength value contains ECFM_PDU_TYPE_CFM type then LLC Snap  
     * Header is present then move offset by 8 bytes
     */
    if (u2TypeLength != ECFM_PDU_TYPE_CFM)
    {
        u4Offset = u4Offset + ECFM_LLC_SNAP_HDR_SIZE;
    }

    /* IEEE 802.1ag, Section 20.46.4.1
     * If the following test fails, the receiving MP Level Demultiplexer
     * (19.2.6) shall consider the CFM PDU
     * invalid and discard it:
     *  a) The length of the mac_service_data_unit is long enough to 
     *  contain a complete MD Level field.
     */
    if (pPduSmInfo->u4ByteCount <= u4Offset)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlRxParsePduHdrs: "
                     "Unable to retrieve information of MD Level field\r\n");
        return ECFM_FAILURE;
    }

    pPduSmInfo->u1CfmPduOffset = (UINT1) u4Offset;
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, u1MdLvlAndVer);

    /* Read the 3MSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxMdLevel = (UINT1) (ECFM_GET_MDLEVEL (u1MdLvlAndVer));

    /* Read the 5LSB from the received first byte of Cfm header */
    pPduSmInfo->u1RxVersion = (UINT1) (ECFM_GET_VERSION (u1MdLvlAndVer));
    u4Offset = u4Offset + (UINT4) (ECFM_MDLEVEL_VER_FIELD_SIZE);

    /* Get Opcode from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxOpcode);
    u4Offset = u4Offset + ECFM_OPCODE_FIELD_SIZE;

    /* Get Flag from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset, pPduSmInfo->u1RxFlags);
    u4Offset = u4Offset + ECFM_FLAGS_FIELD_SIZE;

    /* Get First TLV offset from Received PDU */
    ECFM_CRU_GET_1_BYTE (pPduSmInfo->pBuf, u4Offset,
                         pPduSmInfo->u1RxFirstTlvOffset);

    /* If recvd packet DA is CFM group destination multicast then based on the
     * opcode the level from the DestMac and the CFM header are compared
     * to check if both are in sync */
    if (ECFM_IS_GROUP_DMAC_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_TRUE)
    {
        if (ECFM_IS_MULTICAST_CLASS1_ADDR (pPduSmInfo->RxDestMacAddr,
                                           pPduSmInfo->u1RxMdLevel) !=
            ECFM_TRUE)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxParsePduHdrs: "
                         "Received wrong multicast class1 address for CCM\r\n");
            return ECFM_FAILURE;
        }

	/* If received CFM packet contains tunnel MAC address.
 	   This packet need not to be processed */
	if(ECFM_IS_TUNNEL_MAC_ADDR (pPduSmInfo->RxDestMacAddr) == ECFM_TRUE)
	{ 
	     ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                         "EcfmCcCtrlRxParsePduHdrs: "
                         "Received wrong multicast class1 address for CCM\r\n");
            return ECFM_FAILURE;
        }

    }

    /* Validate source MAC address. If source MAC is multicast then
     * we should discard packet*/
    if (FS_UTIL_IS_MCAST_MAC (pPduSmInfo->RxSrcMacAddr) == OSIX_TRUE)
    {

        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcCtrlRxParsePduHdrs: "
                     "Received wrong source address for CCM\r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcCtlRxPktFree
 *
 * Description        : Free CRU Buffer memory
 *
 * Input(s)           : pBuf - pointer to CRU Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EcfmCcCtrlRxPktFree (tEcfmBufChainHeader * pBuf)
{
    ECFM_RELEASE_CRU_BUF (pBuf, FALSE);
}

/*****************************************************************************
 * Name               : EcfmCcCtrlRxForwardCfmPdu
 *
 * Description        : This routine is used to forward the CFM-PDU to the port
 *                      or frame filtering entity, it also applies the 
 *                      port-filtering rules the CFM-PDU.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PRIVATE INT4
EcfmCcCtrlRxForwardCfmPdu (tEcfmCcPduSmInfo * pPduSmInfo)
{
    UINT1               u1OpCode = ECFM_INIT_VAL;
    /* Check if the CFM-PDU was destined for this port if so, then discard the 
     * the CFM-PDU*/
    /* Check if we did the buffer fragmentation */
    if (pPduSmInfo->pHeaderBuf != NULL)
    {
        /* Check what all tags were fragmented */
        pPduSmInfo->PbbClassificationInfo.InnerIsidTag.u1TagType = VLAN_TAGGED;
        if (ECFM_GET_CRU_VALID_BYTE_COUNT (pPduSmInfo->pHeaderBuf) >
            ((2 * ECFM_MAC_ADDR_LENGTH) + PBB_ISID_TAG_PID_LEN))
        {
            pPduSmInfo->PbbClassificationInfo.OuterVlanTag.u1TagType =
                VLAN_TAGGED;
        }
        /* Concat the fragmant with the main buffer */
        ECFM_CONCAT_CRU_BUF (pPduSmInfo->pHeaderBuf, pPduSmInfo->pBuf);
        pPduSmInfo->pBuf = pPduSmInfo->pHeaderBuf;
        pPduSmInfo->pHeaderBuf = NULL;
    }
    if (ECFM_CC_802_1AH_BRIDGE () == ECFM_TRUE)
    {
        if ((pPduSmInfo->u1RxOpcode != ECFM_OPCODE_AIS) &&
            (pPduSmInfo->u1RxOpcode != ECFM_OPCODE_LCK))
        {
            /*Apply the Port-Filtering Rules */
            if (EcfmAHUtilChkPortFiltering (pPduSmInfo->pPortInfo->u4IfIndex,
                                            pPduSmInfo->pPortInfo->
                                            u1IfOperStatus,
                                            pPduSmInfo->u4RxVlanIdIsId,
                                            pPduSmInfo->u1RxPbbPortType,
                                            &pPduSmInfo->PbbClassificationInfo,
                                            pPduSmInfo->u1RxDirection) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCtrlRxForwardCfmPdu:"
                                  "CFM-PDU Filtered by EcfmAHUtilChkPortFiltering, Port =[%d],"
                                  "Vid=[%d]\r\n",
                                  pPduSmInfo->pPortInfo->u2PortNum,
                                  pPduSmInfo->u4RxVlanIdIsId);
                return ECFM_FAILURE;
            }
        }
        if (pPduSmInfo->u1RxDirection == ECFM_MP_DIR_UP)
        {
            return EcfmCcAHCtrlTxFwdToPort (pPduSmInfo->pBuf,
                                            pPduSmInfo->pPortInfo->
                                            u2PortNum,
                                            &(pPduSmInfo->
                                              VlanClassificationInfo),
                                            &(pPduSmInfo->
                                              PbbClassificationInfo));
        }
        /* else if the PDU  is received from the port then forward it to frame 
         * filtering
         */
        else
        {
            /* Check if we need to the forward the CFM-PDU */
            ECFM_CC_INCR_FRWD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->u2PortNum);
            ECFM_CC_INCR_CTX_FRWD_CFM_PDU_COUNT
                (pPduSmInfo->pPortInfo->u4ContextId);
            return EcfmCcAHCtrlTxFwdToFf (pPduSmInfo);
        }
    }
    else
    {
        if ((pPduSmInfo->u1RxOpcode != ECFM_OPCODE_AIS) &&
            (pPduSmInfo->u1RxOpcode != ECFM_OPCODE_LCK))
        {
            /*Apply the Port-Filtering Rules */
            if (EcfmCcADUtilChkPortFiltering (pPduSmInfo->pPortInfo->u2PortNum,
                                              pPduSmInfo->
                                              VlanClassificationInfo.
                                              OuterVlanTag.u2VlanId) !=
                ECFM_SUCCESS)
            {
                ECFM_CC_TRC_ARG2 (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                                  "EcfmCcCtrlRxForwardCfmPdu:"
                                  "CFM-PDU Filtered by"
                                  "EcfmCcADUtilChkPortFiltering, Port =[%d],Vid=[%d]\r\n",
                                  pPduSmInfo->pPortInfo->u2PortNum,
                                  pPduSmInfo->VlanClassificationInfo.
                                  OuterVlanTag.u2VlanId);
                return ECFM_FAILURE;
            }
        }
        if (pPduSmInfo->u1RxDirection == ECFM_MP_DIR_UP)
        {
            /* If AIS or LCK PDU, then it should be transmitted on all the 
             * CVLANs in case of Provider Bridges */
            u1OpCode = ECFM_BUF_GET_OPCODE (pPduSmInfo->pBuf);

            if ((u1OpCode == ECFM_OPCODE_AIS) || (u1OpCode == ECFM_OPCODE_LCK))
            {
                if (ECFM_CC_802_1AD_BRIDGE () == ECFM_TRUE)
                {
                    if (pPduSmInfo->pPortInfo->u1PortType ==
                        L2IWF_CUSTOMER_EDGE_PORT)

                    {
                        return EcfmSendAisLckOnAllCVlan (pPduSmInfo);
                    }
                }
            }
            return EcfmCcADCtrlTxFwdToPort (pPduSmInfo->pBuf,
                                            pPduSmInfo->pPortInfo->u2PortNum,
                                            &(pPduSmInfo->
                                              VlanClassificationInfo));
        }
        /*else if the PDU  is received from the port then forward it to frame 
         * filtering
         */
        else
        {
            ECFM_CC_INCR_FRWD_CFM_PDU_COUNT (pPduSmInfo->pPortInfo->u2PortNum);
            ECFM_CC_INCR_CTX_FRWD_CFM_PDU_COUNT
                (pPduSmInfo->pPortInfo->u4ContextId);
            return EcfmCcADCtrlTxFwdToFf (pPduSmInfo);
        }
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmCcCheckLockedCondition
 *
 * Description        : This routine is used to check is any lower level MEP
 *                      is locked.
 *
 * Input(s)           : pPduSmInfo - Pointer to pdusm structure, this
 *                      structure holds the information about the MP and the
 *                      so far parsed CFM-PDU.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_TRUE/ECFM_FALSE
 *****************************************************************************/
PRIVATE             BOOL1
EcfmCcCheckLockedCondition (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmCcStackInfo   *pStackInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT2               u2PortNum = ECFM_INIT_VAL;
    tMacAddr            TempMacAddr;
    BOOL1               bSrcTgtAddrSame = ECFM_FALSE;

    u2PortNum = pPduSmInfo->pPortInfo->u2PortNum;

    if ((ECFM_CC_IS_PORT_MODULE_STS_ENABLED (u2PortNum) == ECFM_FALSE) ||
        ECFM_CC_IS_Y1731_ENABLED_ON_PORT (u2PortNum) != ECFM_TRUE)
    {
        return ECFM_FALSE;
    }
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    ECFM_GET_MAC_ADDR_OF_PORT (pPduSmInfo->pPortInfo->u4IfIndex, TempMacAddr);

    if (ECFM_MEMCMP (pPduSmInfo->RxSrcMacAddr, TempMacAddr,
                     ECFM_MAC_ADDR_LENGTH) == ECFM_INIT_VAL)
    {
        bSrcTgtAddrSame = ECFM_TRUE;
    }
    if (!((bSrcTgtAddrSame) && (pPduSmInfo->u1RxOpcode == ECFM_OPCODE_LCK)))
    {
        /* In case Mep is configured Out of service then block data 
         * traffic through it */
        INT1                i1MdLevel = ECFM_INIT_VAL;
        for (i1MdLevel = pPduSmInfo->u1RxMdLevel - 1;
             i1MdLevel >= ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
        {
            pStackInfo = EcfmCcUtilGetMp (u2PortNum, i1MdLevel,
                                          pPduSmInfo->u4RxVlanIdIsId,
                                          ECFM_MP_DIR_DOWN);
            /* If MP at Lower Level exists on Port */
            if (pStackInfo != NULL)
            {
                pPduSmInfo->pStackInfo = pStackInfo;

                /* if Mep at Low level exists */
                if (ECFM_CC_IS_MEP (pStackInfo))
                {
                    /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                     * and the MD Level
                     */
                    pMepInfo = pStackInfo->pMepInfo;
                    if (ECFM_CC_LCK_IS_CONFIGURED (pMepInfo) == ECFM_TRUE)
                    {
                        /* Stop Processing as MEP is configured Out of Service */
                        return ECFM_TRUE;
                    }
                }
            }
        }
        for (i1MdLevel = pPduSmInfo->u1RxMdLevel - 1;
             i1MdLevel >= (INT1) ECFM_MD_LEVEL_MIN; i1MdLevel = i1MdLevel - 1)
        {
            pStackInfo = EcfmCcUtilGetMp (u2PortNum, i1MdLevel,
                                          pPduSmInfo->u4RxVlanIdIsId,
                                          ECFM_MP_DIR_UP);
            /* If MP at Lower Level exists on Port */
            if (pStackInfo != NULL)
            {
                pPduSmInfo->pStackInfo = pStackInfo;

                /* if Mep at Low level exists */
                if (ECFM_CC_IS_MEP (pStackInfo))
                {
                    /* Get the MEP Info  from the Stack Info indexed by Vlan Id 
                     * and the MD Level
                     */
                    pMepInfo = pStackInfo->pMepInfo;
                    if (ECFM_CC_LCK_IS_CONFIGURED (pMepInfo) == ECFM_TRUE)
                    {
                        /* Stop Processing as MEP is configured Out of Service */
                        return ECFM_TRUE;
                    }
                }
            }
        }
    }
    return ECFM_FALSE;
}

/*****************************************************************************
 * Name               : EcfmSendAisLckOnAllCVlan
 *
 * Description        : This routine is used to forward the AIS and LCK PDU
 *                      for MEPs configured on SVLAN to all the CVLANs mapped 
 *                      with it.
 *
 * Input(s)           : pPduSmInfo - Pointer to CC PDU SM Information structure 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE 
 *****************************************************************************/
PUBLIC INT4
EcfmSendAisLckOnAllCVlan (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tEcfmVlanTag        VlanTag;
    UINT1              *pCVlanList = NULL;
    tEcfmBufChainHeader *pDupBuf = NULL;
    UINT4               u4ByteIndex = ECFM_INIT_VAL;
    UINT4               u4BitIndex = ECFM_INIT_VAL;
    UINT2               u2CurrVid = ECFM_INIT_VAL;
    UINT2               u2VlanFlag = ECFM_INIT_VAL;

    tSNMP_OCTET_STRING_TYPE VlanListAll;

    pCVlanList = UtilVlanAllocVlanListSize (sizeof (tVlanListExt));
    if (pCVlanList == NULL)
    {
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pCVlanList, ECFM_INIT_VAL, ECFM_CVLAN_LIST_SIZE);
    VlanListAll.pu1_OctetList = pCVlanList;
    VlanListAll.i4_Length = ECFM_CVLAN_LIST_SIZE;

    /* Check if port is CEP MEP is configured on S-VLAN,get a list
     * of CVLAN ID and transmit AIS and LCK PDU to all the CVLANs */
    if (EcfmVlanGetCVlanIdList (pPduSmInfo->pPortInfo->u4IfIndex,
                                pPduSmInfo->VlanClassificationInfo.OuterVlanTag.
                                u2VlanId, pCVlanList) != ECFM_SUCCESS)
    {
        UtilVlanReleaseVlanListSize (pCVlanList);
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmSendAisLckOnAllCVlan:"
                     "Getting CVLAN for a SVLAN returned Failure \r\n");
        return ECFM_FAILURE;
    }
    for (u4ByteIndex = 0; u4ByteIndex < (UINT4) VlanListAll.i4_Length;
         u4ByteIndex++)
    {
        if (VlanListAll.pu1_OctetList[u4ByteIndex] == 0)
        {
            continue;
        }
        u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];
        for (u4BitIndex = 0;
             ((u4BitIndex < BITS_PER_BYTE)
              && (ECFM_BITLIST_SCAN_TABLE (u2VlanFlag, u4BitIndex) != 0));
             u4BitIndex++)
        {
            u2CurrVid =
                (u4ByteIndex * BITS_PER_BYTE) +
                ECFM_BITLIST_SCAN_TABLE (u2VlanFlag, u4BitIndex);
            /* Transmit AIS and LCK PDU for all CVLANs */
            pDupBuf = EcfmCcUtilDupCruBuf (pPduSmInfo->pBuf);
            if (pDupBuf == NULL)
            {
                UtilVlanReleaseVlanListSize (pCVlanList);
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmSendAisLckOnAllCVlan:"
                             "Cannot Duplicate CRU Buffer \r\n");
                return ECFM_FAILURE;
            }
            ECFM_MEMCPY (&VlanTag, &pPduSmInfo->VlanClassificationInfo,
                         sizeof (tEcfmVlanTag));
            VlanTag.InnerVlanTag.u2VlanId = u2CurrVid;
            VlanTag.InnerVlanTag.u1Priority = VlanTag.OuterVlanTag.u1Priority;
            VlanTag.InnerVlanTag.u1DropEligible =
                VlanTag.OuterVlanTag.u1DropEligible;
            VlanTag.InnerVlanTag.u1TagType = ECFM_VLAN_TAGGED;

            /* Add VLAN Tag  to the Frame */
            EcfmAddVlanTagToFrame (pDupBuf, VlanTag);

            if (EcfmCcADCtrlTxFwdToPort (pDupBuf,
                                         pPduSmInfo->pPortInfo->u2PortNum,
                                         &VlanTag) != ECFM_SUCCESS)
            {
                UtilVlanReleaseVlanListSize (pCVlanList);
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmSendAisLckOnAllCVlan:"
                             "Forward to Port returned Failure \r\n");
                EcfmCcCtrlRxPktFree (pDupBuf);
                pDupBuf = NULL;
                return ECFM_FAILURE;
            }
            EcfmCcCtrlRxPktFree (pDupBuf);
            pDupBuf = NULL;
        }
    }
    UtilVlanReleaseVlanListSize (pCVlanList);
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Name               : EcfmAddVlanTagToFrame
 *
 * Description        : This routine is used to format VLAN Tag for AIS and LCK 
 *                      PDU transmission from CEP ports.
 *
 * Input(s)           : pBuf - Pointer to Buffer on which the VLAN Tag needs to
 *                             be appended.
 *                      VlanTag - Vlan Classification Info        
 *
 * Output(s)          : None
 *
 * Return Value(s)    : NONE
 *****************************************************************************/
PRIVATE VOID
EcfmAddVlanTagToFrame (tEcfmBufChainHeader * pBuf, tEcfmVlanTag VlanTag)
{
    UINT2               u2Tag = ECFM_INIT_VAL;
    UINT2               u2Protocol;
    UINT1               au1Buf[ECFM_VLAN_TAGGED_HEADER_SIZE];

    /* Put VLAN Protocol ether type as Customer 0x8100 */
    u2Protocol = ECFM_HTONS (ECFM_VLAN_CUSTOMER_PROTOCOL_ID);

    /* copying source and dest mac address */
    ECFM_COPY_FROM_CRU_BUF (pBuf, au1Buf, ECFM_INIT_VAL, ECFM_VLAN_TAG_OFFSET);

    u2Tag = (UINT2) VlanTag.InnerVlanTag.u1Priority;
    u2Tag = (UINT2) (u2Tag << ECFM_VLAN_TAG_PRIORITY_SHIFT);
    u2Tag =
        (UINT2) (u2Tag | (VlanTag.InnerVlanTag.u2VlanId & ECFM_VLAN_ID_MASK));
    u2Tag = (UINT2) (ECFM_HTONS (u2Tag));

    MEMCPY (&au1Buf[ECFM_VLAN_TAG_OFFSET], (UINT1 *) &u2Protocol,
            ECFM_VLAN_TYPE_OR_LEN_SIZE);
    MEMCPY (&au1Buf[VLAN_TAG_VLANID_OFFSET], (UINT1 *) &u2Tag,
            ECFM_VLAN_TYPE_OR_LEN_SIZE);

    ECFM_CRU_BUF_MOVE_VALID_OFFSET (pBuf, ECFM_VLAN_TAG_OFFSET);
    ECFM_PREPEND_CRU_BUF (pBuf, au1Buf, VLAN_TAGGED_HEADER_SIZE);

    return;
}

/*****************************************************************************
  End of File cfmccrx.c
 ******************************************************************************/
