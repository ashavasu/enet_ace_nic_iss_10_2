/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmlbtsm.c,v 1.44 2016/05/19 10:55:39 siva Exp $
 *
 * Description: This file contains the functionality of the MEP 
 *              Loopback Initiator Transmit.
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmLbiTxFormatLbmPduHdr PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmLbiTxPutLbInfo PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmLbiTxPutDataTlv PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmLbiTxPutTestTlv PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmLbiTxAdjustPattern PROTO ((UINT1 *, UINT2, UINT2, UINT1 **));
PRIVATE VOID EcfmSendLbmEventToCli PROTO ((tEcfmLbLtMepInfo *, UINT1));
PRIVATE tEcfmLbLtLbmInfo *EcfmLbiTxAddLbmEntry
PROTO ((tEcfmLbLtLBInfo *, tEcfmLbLtMepInfo *));
PRIVATE INT4 EcfmCheckForLbrs PROTO ((tEcfmLbLtMepInfo *));
PRIVATE INT4 EcfmLbiTxXmitLbmPdu PROTO ((tEcfmLbLtMepInfo *));
PRIVATE VOID EcfmLbiTxSetRMepDbSeqNum PROTO ((tEcfmLbLtMepInfo *, UINT4));

/*******************************************************************************
 * Function Name      : EcfmLbLtClntLbInitiator  
 *
 * Description        : This is the interface routine of LB module, which 
 *                      calls the various routines to handle the occurrence of 
 *                      the various events.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmLbLtClntLbInitiator (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventId)
{
    tEcfmLbLtPduSmInfo  PduSmInfo;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    UINT4               u4DeadlineTime = ECFM_INIT_VAL;
    UINT4               u4RetVal = ECFM_FAILURE;
    UINT4               u4LbrTimeout = ECFM_INIT_VAL;
    UINT1               u1SelectorType = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    PduSmInfo.pMepInfo = pMepInfo;

    /* Calling Functions acc. to the event received */
    switch (u1EventId)
    {
        case ECFM_EV_MEP_BEGIN:
            pLbInfo->u1TxLbmStatus = ECFM_TX_STATUS_READY;
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_LBLT_LBI_DEADLINE_EXPIRY:
            /*When Lbm Type is Burst Mode then after the deadline timer expires
             * Lbr Timeout timer is started to wait for the recption of all the 
             * LBRs*/
            if ((ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
                (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_BURST_TYPE))
            {
                /* Stop Timer LBIwhile if it is activated */
                EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBI_INTERVAL, pMepInfo);

                /*Converting LbrTimeout to MSEC */
                u4LbrTimeout =
                    ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (pLbInfo->
                                                          u4RxLbrTimeout);

                /* Y.1731 : Start the Lbr Timeout Timer to wait for all the LBR
                 * Reception*/
                if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBR_TIMEOUT,
                                           pMepInfo,
                                           (u4LbrTimeout)) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbiTxXmitLbmPdu: "
                                   "LbrTimeout Timer has not started\r\n");
                    return ECFM_FAILURE;
                }
                u4RetVal = ECFM_SUCCESS;
                break;
            }
            /*
             * When Lbm Type is Request Response Type Wait for one more extra unit 
             * for timing out the last response
             * There is a very much possibility that the deadline timer expired
             * before timing out the response of the last request, in this case
             * the response will be received after the expiry of the
             * state-machine and thus the response will be treated as invalid
             * and which is not correct.
             */

            if ((ECFM_GET_EXTRA_DEADLINE_STATUS (pLbInfo->u4TxLbmDeadline) ==
                 ECFM_FALSE)
                && (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) &&
                    (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_REQ_RES_TYPE)))
            {
                UINT4               u4LbInterval = ECFM_INIT_VAL;
                if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_MSEC)

                {
                    u4LbInterval = pLbInfo->u4TxLbmInterval;
                }
                if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_SEC)

                {
                    u4LbInterval =
                        ECFM_NUM_OF_MSEC_IN_A_SEC * pLbInfo->u4TxLbmInterval;
                }
                ECFM_SET_EXTRA_DEADLINE_STATUS (pLbInfo->u4TxLbmDeadline);

                /* from now on we have to mask the Interval timers expiry 
                 * events
                 */
                if (EcfmLbLtTmrStartTimer
                    (ECFM_LBLT_TMR_LBI_DEADLINE, pMepInfo,
                     u4LbInterval) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntLbInitiator: "
                                   "Extra time DeadLine Timer has not started\r\n");
                    break;
                }
                u4RetVal = ECFM_SUCCESS;
            }
            /* If LBM type is Request Response Type and LBI dead line 
             * timer expiry event is called with extra dead line value
             * after successful completion of LB,pLbInfo->b1TxLbmResultOk
             * is set to false.Becasue of this, observed an error message.
             * To avoid the above problem, check is added such that
             * If there is a extra dead line value, 
             * call the StopXmitPdu after setting pLbInfo->b1TxLbmResultOk 
             * to true and stop sending LBM pdu */
            else if ((ECFM_GET_EXTRA_DEADLINE_STATUS (pLbInfo->u4TxLbmDeadline)
                      == ECFM_TRUE)
                     &&
                     (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)
                      && (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_REQ_RES_TYPE)))
            {

                u4RetVal = ECFM_SUCCESS;
                pLbInfo->b1TxLbmResultOk = ECFM_TRUE;

                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbLtClntLbInitiator"
                               "Stopping LBM Transaction as total "
                               "number of LBM(s) are sent\r\n");
                EcfmLbiTxStopXmitLbmPdu (pMepInfo);

                /* Sync the stop event to the standby node */
                EcfmRedSyncTransactionStopEvent (ECFM_RED_LBI_STOP_TX,
                                                 pMepInfo->u4MdIndex,
                                                 pMepInfo->u4MaIndex,
                                                 pMepInfo->u2MepId);

                /* Sync LB Cache at STANDBY Node also */
                EcfmRedSyncLbCache ();
            }
            break;
        case ECFM_LBLT_LBR_TIMEOUT_EXPIRY:
            /* Stop the running transaction */
            EcfmLbiTxStopXmitLbmPdu (pMepInfo);
            /*To notify the Throughput Initiator that Burst is Complete */
            EcfmLbLtClntThInitiator (&(PduSmInfo), ECFM_EV_LB_BURST_COMPLETE);
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_LB_STOP_TRANSACTION:

            /* Sync the stop event to the standby node */
            EcfmRedSyncTransactionStopEvent (ECFM_RED_LBI_TIMER_EXPIRY,
                                             pMepInfo->u4MdIndex,
                                             pMepInfo->u4MaIndex,
                                             pMepInfo->u2MepId);
            if (((u1EventId == ECFM_LBLT_LBI_DEADLINE_EXPIRY)
                 || (u1EventId == ECFM_LB_STOP_TRANSACTION))
                && (pMepInfo->u2PortNum != ECFM_INIT_VAL)
                && (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)))

            {

                /* Sync LB Cache at STAND BY Node */
                EcfmRedSyncLbCache ();
            }
            break;
        case ECFM_EV_MEP_NOT_ACTIVE:

            /* Stop the running transaction */
            EcfmLbiTxStopXmitLbmPdu (pMepInfo);
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_LB_START_TRANSACTION:

            /* Increment the Transaction Id for the current transaction */
            ECFM_LBLT_INCR_NEXT_LBM_TRANS_ID (pLbInfo);

            /* In case of ECFM, all the LBR are expected to be received in order
             * and after or in b/w transamission of all the LBMs*/
            /* Status set to indicate no other transaction possible till this
             * completion */
            pLbInfo->u1TxLbmStatus = ECFM_TX_STATUS_NOT_READY;

            pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);

            /*In case Throughput SM in Not Ready State, it means throughput
             * transaction running and deadline is to be started as Burst
             * Deadline in milliseconds*/
            if (pThInfo->u1ThStatus == ECFM_TX_STATUS_NOT_READY)
            {
                if (pThInfo->u1TxThBurstType == ECFM_LBLT_TH_BURST_TYPE_DL)
                {
                    /* Start the Timer with u4DeadlineTime milliseconds */
                    if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBI_DEADLINE,
                                               pMepInfo,
                                               pThInfo->u4TxThBurstDeadline) !=
                        ECFM_SUCCESS)
                    {
                        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                       ECFM_ALL_FAILURE_TRC,
                                       "EcfmLbLtClntLbInitiator: "
                                       "LBIDeadLine Timer has not started\r\n");

                        u4RetVal = ECFM_FAILURE;
                        break;
                    }
                }
            }

            /* Y.1731 : Start Timer LBIDeadLine, if configured */
            if ((ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
                (pLbInfo->u4TxLbmDeadline != 0))
            {
                u4DeadlineTime =
                    ECFM_NUM_OF_MSEC_IN_A_SEC * pLbInfo->u4TxLbmDeadline;

                /* Start the Timer with u4DeadlineTime milliseconds */
                if (EcfmLbLtTmrStartTimer
                    (ECFM_LBLT_TMR_LBI_DEADLINE, pMepInfo,
                     u4DeadlineTime) != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntLbInitiator: "
                                   "LBIDeadLine Timer has not started\r\n");
                    break;
                }
            }

            EcfmRedSynchHwAuditCmdEvtInfo (ECFM_LBLT_CURR_CONTEXT_ID (),
                                           ECFM_RED_LB_NPAPI,
                                           ECFM_INIT_VAL,
                                           pMepInfo->u4MdIndex,
                                           pMepInfo->u4MaIndex,
                                           pMepInfo->u2MepId,
                                           ECFM_INIT_VAL, ECFM_TRUE);

#ifdef NPAPI_WANTED
            if ((ECFM_HW_LBM_SUPPORT () == ECFM_TRUE) &&
                (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE))
            {
                tEcfmMepInfoParams  EcfmMepInfoParams;
                tEcfmConfigLbmInfo  EcfmConfigLbmInfo;
                ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                             sizeof (tEcfmMepInfoParams));
                ECFM_MEMSET (&EcfmConfigLbmInfo, ECFM_INIT_VAL,
                             sizeof (tEcfmConfigLbmInfo));
                EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
                EcfmMepInfoParams.u4IfIndex =
                    ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum,
                                            pTempLbLtPortInfo);
                EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
                EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
                EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;
                EcfmConfigLbmInfo.u1Status = pLbInfo->u1TxLbmStatus;
                EcfmConfigLbmInfo.u1TstTlvPatterType =
                    pLbInfo->u1TxLbmTstPatternType;
                EcfmConfigLbmInfo.u2TstTlvPatterSize =
                    pLbInfo->u2TxLbmPatternSize;
                EcfmConfigLbmInfo.u1TlvOrNone = pLbInfo->u1TxLbmTlvOrNone;
                EcfmConfigLbmInfo.b1VariableByte =
                    pLbInfo->b1TxLbmVariableBytes;
                EcfmConfigLbmInfo.b1DropEnable = pLbInfo->b1TxLbmDropEligible;
                EcfmConfigLbmInfo.u2TxLbmMessages = pLbInfo->u2TxLbmMessages;
                EcfmConfigLbmInfo.u4LbmInterval = pLbInfo->u4TxLbmInterval;
                EcfmConfigLbmInfo.u4Deadline = pLbInfo->u4TxLbmDeadline;
                EcfmConfigLbmInfo.u1LbmMode = pLbInfo->u1TxLbmMode;

                ECFM_MEMCPY (EcfmConfigLbmInfo.DestMacAddr,
                             pLbInfo->TxLbmDestMacAddr, ECFM_MAC_ADDR_LENGTH);
                EcfmConfigLbmInfo.pu1DataTlvData =
                    pLbInfo->TxLbmDataTlv.pu1Octets;
                EcfmConfigLbmInfo.u4DataTlvSize =
                    pLbInfo->TxLbmDataTlv.u4OctLen;

                if (EcfmFsMiEcfmStartLbmTransaction
                    (&EcfmMepInfoParams, &EcfmConfigLbmInfo) != FNP_SUCCESS)
                {
                    return ECFM_FAILURE;
                }
                return ECFM_SUCCESS;
            }
#endif /*  */

            /* Y.1731 : Check this u2TxLbmMessages variable if configured 
             * then send the configured no. of LBMs else infinite send LBMs */
            if (pLbInfo->u2TxLbmMessages == 0)
            {
                if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
                {
                    /* Set MSB in the variable to indicate infinite transmission */
                    ECFM_SET_INFINITE_TX_STATUS (pLbInfo->u2TxLbmMessages);
                }
                else
                {
                    pLbInfo->u2TxLbmMessages = ECFM_LB_MESG_DEF_VAL;
                }
            }

            ECFM_LBLT_RESET_LBM_SEQ_NUM (pLbInfo);

            pLbInfo->u4TxLbmSeqNumber = pLbInfo->u4NextLbmSeqNo;
            if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum) ||
                (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) &&
                 (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_BURST_TYPE)))
            {
                /*If LBM Initiated is Multicast and Mode is burst then 
                 * Expected LbrSeqNum is set for all the RMEPs of teh MEP*/
                if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MULTICAST)
                {
                    EcfmLbiTxSetRMepDbSeqNum (pMepInfo,
                                              pLbInfo->u4NextLbmSeqNo);
                }
                else
                {
                    /* Copy the expected Seq Number for the LBR PDU */
                    pLbInfo->u4ExpectedLbrSeqNo = pLbInfo->u4NextLbmSeqNo;
                }
            }

            u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

            if ((pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MEPID) &&
                (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) != ECFM_TRUE))
            {
                /* Get RMEP MAC Address for the Destination MEPID */
                if (EcfmLbLtUtilGetRMepMacAddr (pMepInfo->u4MdIndex,
                                                pMepInfo->u4MaIndex,
                                                pMepInfo->u2MepId,
                                                pLbInfo->u2TxDestMepId,
                                                pLbInfo->TxLbmDestMacAddr)
                    != ECFM_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                   ECFM_CONTROL_PLANE_TRC
                                   | ECFM_OS_RESOURCE_TRC,
                                   "EcfmLbLtClntLbInitiator"
                                   "Rmep Mac Address not Found !! \r\n");

                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            else if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MULTICAST)
            {
                /* Y.1731 : Multicast LBM can only be send when Y1731 is enabled */
                if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
                {
                    ECFM_GEN_MULTICAST_CLASS1_ADDR (pLbInfo->TxLbmDestMacAddr,
                                                    pMepInfo->u1MdLevel);
                }
                else
                {
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }

            /* Call the routine to format and send the LBM PDU */
            if (EcfmLbiTxXmitLbmPdu (pMepInfo) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtClntLbInitiator: "
                               "LBM PDU cannot be transmitted\r\n");
                break;
            }
            u4RetVal = ECFM_SUCCESS;
            break;
        case ECFM_LBLT_LBI_INTERVAL_EXPIRY:

            /* ignore the interval expiry event in case we are waiting for the
             * last reponse
             */
            if (ECFM_GET_EXTRA_DEADLINE_STATUS (pLbInfo->u4TxLbmDeadline) ==
                ECFM_FALSE)

            {

                /* For CLI ping */
                EcfmSendLbmEventToCli (pMepInfo, CLI_EV_ECFM_LBR_TIMEOUT);

                /* Call the routine to format and send the LBM PDU */
                if (EcfmLbiTxXmitLbmPdu (pMepInfo) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntLbInitiator: "
                                   "LBM PDU cannot be transmitted\r\n");
                    break;
                }
            }
            else
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbLtClntLbInitiator: "
                               "ECFM_LBLT_LBI_INTERVAL_EXPIRY ignored\r\n");
            }
            u4RetVal = ECFM_SUCCESS;
            break;
        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbLtClntLbInitiator: "
                           "Event not SUPPORTED!\r\n");
    }
    if (u4RetVal == ECFM_FAILURE)

    {

        /* Update Result OK to False */
        pLbInfo->b1TxLbmResultOk = ECFM_FALSE;

        /* Stop the running LBM Transaction */
        EcfmLbiTxStopXmitLbmPdu (pMepInfo);
        return ECFM_FAILURE;
    }

    else

    {

        /* Update Result OK to True */
        pLbInfo->b1TxLbmResultOk = ECFM_TRUE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxXmitLbmPdu
 *
 * Description        : This routine formats and transmits the LBM PDUs.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmLbiTxXmitLbmPdu (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4LbInterval = ECFM_INIT_VAL;
    UINT4               u4LbrTimeout = ECFM_INIT_VAL;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1               u1DropEligible = ECFM_FALSE;
    UINT1               u1SelectorType = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);

    /* Y.1731 : Check the Loopback capability, it should
     * be enabled for LBM transmission */
    if ((ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum)) &&
        (ECFM_LBLT_IS_LB_DISABLED (pMepInfo)))
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbiTxXmitLbmPdu: "
                       "LBM PDU cannot be send as Loopback capability is "
                       "disabled\r\n");
        return ECFM_FAILURE;
    }

    /* Y.1731 : Check the Number of LBMs to send, if it is zero stop the
     * transmission */
    if (pLbInfo->u2TxLbmMessages == 0)
    {
        /* In case of ECFM the LB transaction has to run for a worst time of 5
         * seconds, but in case of Y.1731 we need to stop the transaction if all
         * the configured LBMs have been transmitted
         */
        if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepInfo->u2PortNum) ||
            (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) &&
             (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_BURST_TYPE)))
        {
            /* Stop Timer LBIwhile if it is activated */
            EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBI_INTERVAL, pMepInfo);

            /* Y.1731 : Stop Timer LBIDeadLine if it is activated */
            EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBI_DEADLINE, pMepInfo);

            /*Converting LbrTimeout to MSEC */
            if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
            {
                u4LbrTimeout =
                    ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (pLbInfo->
                                                          u4RxLbrTimeout);
            }
            else
            {
                u4LbrTimeout =
                    ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC
                    (ECFM_LBR_TIMEOUT_DEF_VAL);
            }

            /* Y.1731 : Start the Lbr Timeout Timer to wait for all the LBR
             * Reception*/
            if (EcfmLbLtTmrStartTimer (ECFM_LBLT_TMR_LBR_TIMEOUT,
                                       pMepInfo,
                                       (u4LbrTimeout)) != ECFM_SUCCESS)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                               ECFM_ALL_FAILURE_TRC,
                               "EcfmLbiTxXmitLbmPdu: "
                               "LbrTimeout Timer has not started\r\n");
                return ECFM_FAILURE;
            }
        }
        else
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbiTxXmitLbmPdu: "
                           "Stopping LBM Transaction as total "
                           "number of LBM(s) are sent\r\n");
            EcfmLbiTxStopXmitLbmPdu (pMepInfo);

            /* Sync the stop event to the standby node */
            EcfmRedSyncTransactionStopEvent (ECFM_RED_LBI_STOP_TX,
                                             pMepInfo->u4MdIndex,
                                             pMepInfo->u4MaIndex,
                                             pMepInfo->u2MepId);

            /* Sync LB Cache at STANDBY Node also */
            EcfmRedSyncLbCache ();
        }
        return ECFM_SUCCESS;
    }

    u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
    {
        /*Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_LBM_PDU_SIZE,
                                   ECFM_MPLSTP_CTRL_PKT_OFFSET);
        if (pBuf == NULL)
        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiTxXmitLbmPdu: Buffer Allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        /* Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_LBM_PDU_SIZE, ECFM_INIT_VAL);
        if (pBuf == NULL)
        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbiTxXmitLbmPdu: Buffer Allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }

    /* Y.1731 : Check for the varying bytes, if it is TRUE then format the LBM
     * PDU everytime else format the LBM PDU once and keep changing the
     * sequnce number for the rest of the PDUs to send */
    if ((pLbInfo->LbmPdu.pu1Octets == NULL) ||
        (pLbInfo->b1TxLbmVariableBytes == ECFM_TRUE))
    {
        /* Free the memory allocated to the last transmitted LBM PDU, if any */ 
        if (pLbInfo->LbmPdu.pu1Octets != NULL) 
 
        { 
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL, 
                                 pLbInfo->LbmPdu.pu1Octets); 
            pLbInfo->LbmPdu.pu1Octets = NULL; 
        } 

        ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_LBM_PDU_SIZE);
        pu1PduStart = ECFM_LBLT_PDU;
        pu1PduEnd = ECFM_LBLT_PDU;
        pu1EthLlcHdr = ECFM_LBLT_PDU;

        /* Format the LBM PDU header */
        EcfmLbiTxFormatLbmPduHdr (pMepInfo, &pu1PduEnd);

        /* Fill in the LBM PDU info like Sequence Number and other optional
         * tlvs */
        /* Routine called to format the LBM PDU */
        EcfmLbiTxPutLbInfo (pMepInfo, &pu1PduEnd);
        u2PduLength = (UINT2) (pu1PduEnd - pu1PduStart);

        if ((u2PduLength > 0) && (u2PduLength <= ECFM_MAX_LBM_PDU_SIZE))
        {

            /* Copy the formatted CFM LBM PDU into the mib */
            ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_LBM_PDU
                (pMepInfo->LbInfo.LbmPdu.pu1Octets);
            if (pMepInfo->LbInfo.LbmPdu.pu1Octets == NULL)

            {
                ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                               ECFM_OS_RESOURCE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbiTxXmitLbmPdu: "
                               "Memory allocation for Data FAILED!\r\n"
                               "MAX LB Sessions crossed, tune the macro\r\n"
                               "ECFM_MAX_LB_SESSIONS to expected value\r\n");
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                return ECFM_FAILURE;
            }

            /* Copy the size of the LBM PDU */
            pMepInfo->LbInfo.LbmPdu.u4OctLen = u2PduLength;

            /* Copy the transmitted LBM PDU */
            ECFM_MEMCPY (pMepInfo->LbInfo.LbmPdu.pu1Octets, pu1PduStart,
                         u2PduLength);
        }
    }
    else
    {
        pu1PduStart = pLbInfo->LbmPdu.pu1Octets;
        pu1PduEnd = pLbInfo->LbmPdu.pu1Octets;
        pu1EthLlcHdr = ECFM_LBLT_PDU;
        u2PduLength = (UINT2) pLbInfo->LbmPdu.u4OctLen;

        /* move the ptr by 4 byte of CFM header */
        pu1PduEnd = pu1PduEnd + ECFM_CFM_HDR_SIZE;

        /* Finally update the Sequence No. in the ptr */
        ECFM_PUT_4BYTE (pu1PduEnd, pLbInfo->u4NextLbmSeqNo);
    }

    /* Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_ALL_FAILURE_TRC,
                       "EcfmLbiTxXmitLbmPdu: "
                       "Copying into Buffer failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                             pLbInfo->LbmPdu.pu1Octets);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                             pMepInfo->LbInfo.LbmPdu.pu1Octets);
        pLbInfo->LbmPdu.pu1Octets = NULL;
        pLbInfo->LbmPdu.u4OctLen = 0;
        pMepInfo->LbInfo.LbmPdu.pu1Octets = NULL;
        pMepInfo->LbInfo.LbmPdu.u4OctLen = 0;
        return ECFM_FAILURE;
    }

    u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) != ECFM_TRUE)
    {
        pu1PduStart = pu1EthLlcHdr;
        /* Format the Ethernet and the LLC header */
        EcfmFormatLbLtTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr,
                                     u2PduLength, ECFM_OPCODE_LBM);
        /* Prepend the Ethernet and the LLC header in the CRU buffer */
        u2PduLength = (UINT2) (pu1EthLlcHdr - pu1PduStart);

        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1PduStart,
                                  (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiTxXmitLbmPdu: "
                           "Prepending into Buffer failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                                 pLbInfo->LbmPdu.pu1Octets);
            pLbInfo->LbmPdu.pu1Octets = NULL;
            pLbInfo->LbmPdu.u4OctLen = 0;
            return ECFM_FAILURE;
        }
    }

    /* Start Timer LB Interval for the configured value */
    if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_MSEC)
    {
        u4LbInterval = pLbInfo->u4TxLbmInterval;
    }
    else if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_SEC)
    {
        u4LbInterval = ECFM_NUM_OF_MSEC_IN_A_SEC * pLbInfo->u4TxLbmInterval;
    }
    else if (pLbInfo->u1TxLbmIntervalType == ECFM_LBLT_LB_INTERVAL_USEC)
    {
        u4LbInterval = pLbInfo->u4TxLbmInterval / ECFM_NUM_OF_USEC_IN_A_MSEC;
    }

    /* Start the Timer with u4LbInterval milliseconds */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_LBI_INTERVAL, pMepInfo, u4LbInterval) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmLbiTxXmitLbmPdu: "
                       "LBIWhile Timer has not started\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                             pLbInfo->LbmPdu.pu1Octets);
        pLbInfo->LbmPdu.pu1Octets = NULL;
        pLbInfo->LbmPdu.u4OctLen = 0;
        return ECFM_FAILURE;
    }

    /* Y.1731 expects the LBR to be received before transmission of next LBM */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum) &&
        (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_REQ_RES_TYPE))
    {
        /*If LBM Initiated is Multicast and Mode is burst then 
         * Expected LbrSeqNum is set for all the RMEPs of teh MEP*/
        if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MULTICAST)
        {
            EcfmLbiTxSetRMepDbSeqNum (pMepInfo, pLbInfo->u4NextLbmSeqNo);
        }
        /* Copy the expected Seq Number for the LBR PDU */
        pLbInfo->u4ExpectedLbrSeqNo = pLbInfo->u4NextLbmSeqNo;
    }

    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only Active node needs to add Buffer. \r\n");

        /* Y.1731/ECFM : Make entry of the transmitted LBM in the LBInit Table 
         * only if LBR Cache Enabled*/
        if (ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE)
        {
            pLbmNode = EcfmLbiTxAddLbmEntry (pLbInfo, pMepInfo);
            if (pLbmNode == NULL)
            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbiTxXmitLbmPdu: "
                               "LBM cannot be added in the LBM Table\r\n");
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                                     pLbInfo->LbmPdu.pu1Octets);
                pLbInfo->LbmPdu.pu1Octets = NULL;
                pLbInfo->LbmPdu.u4OctLen = 0;
                return ECFM_FAILURE;
            }
            pLbmNode->u4BytesSent = u4PduSize;
        }
    }

    /* Increment the Sequence Number for the next LBM */
    ECFM_LBLT_INCR_LBM_NEXT_SEQ_NUM (pLbInfo);

    /* If Number of LBMs to send is configured then decrement it after
     * transmitting one LBM PDU */
    if (ECFM_GET_INFINITE_TX_STATUS (pLbInfo->u2TxLbmMessages) == ECFM_FALSE)
    {
        ECFM_LBLT_DECR_TRANSMIT_LBM_MSG (pLbInfo);
    }

    if (pLbInfo->u1TxLbmDestType != ECFM_TX_DEST_TYPE_MULTICAST)
    {
        u1DropEligible = (UINT1) pLbInfo->b1TxLbmDropEligible;
    }

    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmLbiTxXmitLbmPdu: "
                        "Sending out CFM PDU to lower layer...\r\n");

    /* Transmit the LBM over MPLS-TP path */
    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
    {
        if (EcfmMplsTpLbLtTxPacket (pBuf, pMepInfo,
                                    ECFM_OPCODE_LBM) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbiTxXmitLbmPdu: Transmit LBM PDU failed\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                                 pLbInfo->LbmPdu.pu1Octets);
            pLbInfo->LbmPdu.pu1Octets = NULL;
            pLbInfo->LbmPdu.u4OctLen = 0;
            return ECFM_FAILURE;
        }
    }
    else                        /* Transmit LBM to Ethernet */
    {
        if (EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                       pMepInfo->u4PrimaryVidIsid,
                                       pLbInfo->u1TxLbmVlanPriority,
                                       u1DropEligible,
                                       pMepInfo->u1Direction,
                                       ECFM_OPCODE_LBM, NULL,
                                       NULL) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbiTxXmitLbmPdu: "
                           "Transmit CFM PDU failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                                 pLbInfo->LbmPdu.pu1Octets);
            pLbInfo->LbmPdu.pu1Octets = NULL;
            pLbInfo->LbmPdu.u4OctLen = 0;
            return ECFM_FAILURE;
        }
    }

    ECFM_LBLT_INCR_LBM_OUT (pLbInfo);
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxFormatLbmPduHdr
 *
 * Description        : This rotuine is used to fill the CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1LbPdu - Pointer to the Pdu     
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLbiTxFormatLbmPduHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    UINT1              *pu1Pdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Pdu = *ppu1LbPdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1Pdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_OPCODE_LBM);

    /* Fill the Next 1 byte with Flag In LBM, Flag Field is set to ZERO */
    ECFM_PUT_1BYTE (pu1Pdu, 0);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_LBM_FIRST_TLV_OFFSET);
    *ppu1LbPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxPutLbInfo
 *
 * Description        : This routine is used to fill the Sequence number and
 *                      TLVs required to be send in the LBM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure that stores
 *                      the information regarding MEP info.
 * 
 * Output(s)          : ppu1LbPdu - Pointer to the LBM Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLbiTxPutLbInfo (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1SelectorType = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pu1Pdu = *ppu1LbPdu;

    /* Fill the Next 4 bytes with Sequence Number */
    ECFM_PUT_4BYTE (pu1Pdu, pLbInfo->u4NextLbmSeqNo);

    /* Y.1731 : Check the Y.1731 status, if it is enabled then send Y1731 TLVs 
     * depending on the configuration else send 802.1ag TLVs */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        /* If the selector Type LSP/PW, fill the Y.1731 based 
         * MPLS-TP OAM TLVs
         */
        u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

        if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbiTxPutLbInfo: "
                           "Fill the Target MEP/MIP ID TLV "
                           "and Requesting MEP ID TLV\r\n");

            if (pLbInfo->u1TxLbmDestType == ECFM_TX_DEST_TYPE_MEPID)
            {
                /* Put Target MEP ID TLV */
                EcfmMpTpLbPutTrgtMepIdTlv (pMepInfo, &pu1Pdu);
            }
            else                /* ECFM_TX_DEST_TYPE_MIPID */
            {
                /* Put Target MIP ID TLV */
                EcfmMpTpLbPutTrgtMipIdTlv (pMepInfo, &pu1Pdu);
            }

            /* Put Requesting MEP ID TLV, if not for diagnostic test */
            if (pLbInfo->u1TxLbmTlvOrNone != ECFM_LBLT_LBM_WITH_TEST_TLV)
            {
                EcfmMpTpLbPutRqstMepIdTlv (pMepInfo, &pu1Pdu);
            }
        }

        /* Y.1731 : Check the u1LbmTlvTypeOrNone variable, send the TLV
         * accordingly. If varying bytes is ON and no TLV is configured then
         * send data TLV */
        switch (pLbInfo->u1TxLbmTlvOrNone)
        {
            case ECFM_LBLT_LBM_WITH_DATA_TLV:
                EcfmLbiTxPutDataTlv (pMepInfo, &pu1Pdu);
                break;
            case ECFM_LBLT_LBM_WITH_TEST_TLV:
                EcfmLbiTxPutTestTlv (pMepInfo, &pu1Pdu);
                break;
            case ECFM_LBLT_LBM_WITHOUT_TLV:
                if (pLbInfo->b1TxLbmVariableBytes == ECFM_TRUE)
                {
                    EcfmLbiTxPutDataTlv (pMepInfo, &pu1Pdu);
                }
                break;
            default:
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC,
                               "EcfmLbiTxPutLbInfo: "
                               "TLV type not SUPPORTED!\r\n");
        }
    }
    else                        /* IEEE 802.1ag */
    {
        UINT2               u2DataLen = ECFM_INIT_VAL;

        /* Fill the Sender Id TLV */
        ECFM_LBLT_SET_SENDER_ID_TLV (pMepInfo, pu1Pdu);

        /* Fill the Data TLV if TxLbmDataTlv is Non-Zero */
        u2DataLen = (UINT2) pLbInfo->TxLbmDataTlv.u4OctLen;
        if (u2DataLen != 0)

        {
            ECFM_PUT_1BYTE (pu1Pdu, ECFM_DATA_TLV_TYPE);
            ECFM_PUT_2BYTE (pu1Pdu, u2DataLen);
            ECFM_MEMCPY (pu1Pdu, pLbInfo->TxLbmDataTlv.pu1Octets, u2DataLen);
            pu1Pdu = pu1Pdu + u2DataLen;
        }

        /* Fill the Organisation-specific TLV 
         * Sending Subtype field as ZERO */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_TLV_TYPE);
        ECFM_PUT_2BYTE (pu1Pdu, ECFM_ORG_SPEC_MIN_LENGTH);
        ECFM_LBLT_SET_OUI (pu1Pdu);

        /* Fill in the subtype as 0 */
        ECFM_PUT_1BYTE (pu1Pdu, ECFM_ORG_SPEC_SUBTYPE_VALUE);

        /* Set value depending upon the subtype */
    }

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_END_TLV_TYPE);
    *ppu1LbPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxPutDataTlv
 *
 * Description        : This routine is used to fill the Data TLV, if required
 *                      to be send in the LBM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure
 *                      that stores the information regarding MEP info.
 *
 * Output(s)          : ppu1LbPdu - pointer to the Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLbiTxPutDataTlv (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT2               u2LbmDataSize = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pu1Pdu = *ppu1LbPdu;

    /* Put 1 byte as the Data TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_DATA_TLV_TYPE);

    /* If variable bytes to send is ON then send PDU of varying size, 
     * else send the data of the configured size else default size */
    if (pLbInfo->b1TxLbmVariableBytes == ECFM_TRUE)

    {

        /* If it is the first PDU or the last PDU of size 9K, 
         * then add data to make LBM PDU of 32 bytes, else
         * send LBM PDU of 64, 128, 256, 512, 1024, 2048, 4096, 
         * 8192, 9000 bytes */
        ECFM_LBLT_GET_DATA_TO_SEND_SIZE (pLbInfo->LbmPdu.u4OctLen,
                                         u2LbmDataSize);
    }

    else

    {
        if (pLbInfo->u2TxLbmPatternSize != 0)

        {
            u2LbmDataSize = pLbInfo->u2TxLbmPatternSize;
        }

        else

        {

            /* Default size of the data to send */
            u2LbmDataSize = ECFM_LBM_DATA_PATTERN_SIZE_MIN;
        }
    }

    /* Put the next 2 byte as the length of the TLV */
    ECFM_PUT_2BYTE (pu1Pdu, u2LbmDataSize);

    /* Put the next u2LbmDataSize bytes as the value part of the TLV */
    /* If data is configured then send the configured value else send default 
     * all-1 sequence */
    if (pLbInfo->TxLbmDataTlv.pu1Octets != NULL)

    {
        EcfmLbiTxAdjustPattern (pLbInfo->TxLbmDataTlv.pu1Octets,
                                pLbInfo->TxLbmDataTlv.u4OctLen,
                                u2LbmDataSize, &pu1Pdu);
        pu1Pdu = pu1Pdu + u2LbmDataSize;
    }

    else

    {
        ECFM_MEMSET (pu1Pdu, ECFM_LBM_DEF_DATA_PATTERN, u2LbmDataSize);
        pu1Pdu = pu1Pdu + u2LbmDataSize;
    }
    *ppu1LbPdu = pu1Pdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxPutTestTlv
 *
 * Description        : This routine is used to fill the Test TLV, if required
 *                      to be send in the LBM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure
 *                      that stores the information regarding MEP info.
 *
 * Output(s)          : ppu1LbPdu - pointer to the Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLbiTxPutTestTlv (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1LbPdu)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1              *pu1TlvStart = NULL;
    UINT4               u4Crc32val = ECFM_INIT_VAL;
    UINT2               u2LbmDataSize = ECFM_INIT_VAL;
    UINT2               u2TlvLen = ECFM_INIT_VAL;
    UINT1               au1RandBytes[ECFM_LBLT_PRBS_SIZE];
    ECFM_LBLT_TRC_FN_ENTRY ();
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pu1Pdu = *ppu1LbPdu;
    pu1TlvStart = *ppu1LbPdu;
    ECFM_MEMSET (au1RandBytes, ECFM_INIT_VAL, ECFM_LBLT_PRBS_SIZE);

   /*----------------------TEST TLV---------------------------*/
    /* Type(1) | Length(2) | Pat-Type(1) | Value (N) | CRC (4) */
   /*---------------------------------------------------------*/
    /* "Length" field contains size of the data in "Value + CRC" 
     * fields */

    /* Put 1 byte as the Test TLV Type */
    ECFM_PUT_1BYTE (pu1Pdu, ECFM_TEST_TLV_TYPE);

    /* If variable bytes to send is ON then send PDU of varying size,
     * else send the data of the configured size */
    if (pLbInfo->b1TxLbmVariableBytes == ECFM_TRUE)

    {

        /* If it is the first PDU or the last PDU is of 9K, 
         * then add data to make LBM PDU of 64 bytes, else
         * send LBM PDU of 128, 256, 512, 1024, 2048, 4096, 
         * 8192, 9000 bytes*/
        ECFM_LBLT_GET_PATTERN_TO_SEND_SIZE (pLbInfo->LbmPdu.u4OctLen,
                                            u2LbmDataSize);
    }

    else

    {
        if (pLbInfo->u2TxLbmPatternSize != 0)

        {
            u2LbmDataSize = pLbInfo->u2TxLbmPatternSize;
        }

        else

        {

            /* Default size to send in TLV */
            u2LbmDataSize = ECFM_LBM_TEST_PATTERN_SIZE_MIN;
        }
    }

    /* Put the next 2 byte as the length of the TLV */
    ECFM_PUT_2BYTE (pu1Pdu, (u2LbmDataSize + ECFM_TST_PATTERN_TYPE_FIELD_SIZE));

    /* Put the next 1 byte as the Test TLV Pattern Type */
    ECFM_PUT_1BYTE (pu1Pdu, pLbInfo->u1TxLbmTstPatternType);
    u2TlvLen =
        (UINT2) (ECFM_TLV_TYPE_FIELD_SIZE + ECFM_TLV_LENGTH_FIELD_SIZE +
                 ECFM_TST_PATTERN_TYPE_FIELD_SIZE + (u2LbmDataSize -
                                                     ECFM_LBLT_TEST_TLV_CRC_SIZE));

    /* Put the next u2LbmDataSize bytes as the value part of the TLV. */
    switch (pLbInfo->u1TxLbmTstPatternType)

    {
        case ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITHOUT_CRC:
            ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL, u2LbmDataSize);
            pu1Pdu = pu1Pdu + u2LbmDataSize;
            *ppu1LbPdu = pu1Pdu;
            break;
        case ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC:

            /* If Pattern size is less than 4 bytes then donot append any tlv */
            if (u2LbmDataSize > ECFM_LBLT_TEST_TLV_CRC_SIZE)

            {

                /* Pattern to send is of the size provided by the user minus 
                 * crc field size */
                ECFM_MEMSET (pu1Pdu, ECFM_INIT_VAL,
                             u2LbmDataSize - ECFM_LBLT_TEST_TLV_CRC_SIZE);
                pu1Pdu = pu1Pdu + (u2LbmDataSize - ECFM_LBLT_TEST_TLV_CRC_SIZE);
                u4Crc32val = EcfmLbLtUtilCalculateCrc32 (pu1TlvStart, u2TlvLen);
                ECFM_PUT_4BYTE (pu1Pdu, u4Crc32val);
                *ppu1LbPdu = pu1Pdu;
                break;
            }
            *ppu1LbPdu = pu1TlvStart;
            break;
        case ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC:
            EcfmGenerateRandPseudoBytes (au1RandBytes);
            EcfmLbiTxAdjustPattern (au1RandBytes, ECFM_LBLT_PRBS_SIZE,
                                    u2LbmDataSize, &pu1Pdu);
            pu1Pdu = pu1Pdu + u2LbmDataSize;
            *ppu1LbPdu = pu1Pdu;
            break;
        case ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC:

            /* If Pattern size is less than 4 bytes then donot append any tlv */
            if (u2LbmDataSize > ECFM_LBLT_TEST_TLV_CRC_SIZE)

            {
                EcfmGenerateRandPseudoBytes (au1RandBytes);

                /* Pattern to send is of the size provided by the user minus crc
                 * field size */
                EcfmLbiTxAdjustPattern (au1RandBytes, ECFM_LBLT_PRBS_SIZE,
                                        u2LbmDataSize -
                                        ECFM_LBLT_TEST_TLV_CRC_SIZE, &pu1Pdu);
                pu1Pdu = pu1Pdu + (u2LbmDataSize - ECFM_LBLT_TEST_TLV_CRC_SIZE);
                u4Crc32val = EcfmLbLtUtilCalculateCrc32 (pu1TlvStart, u2TlvLen);
                ECFM_PUT_4BYTE (pu1Pdu, u4Crc32val);
                *ppu1LbPdu = pu1Pdu;
                break;
            }
            *ppu1LbPdu = pu1TlvStart;
            break;
        default:
            *ppu1LbPdu = pu1TlvStart;
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbiTxPutTestTlv: "
                           "Pattern type not SUPPORTED!\r\n");
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxStopXmitLbmPdu
 *
 * Description        : This routine is used to stop the LBM PDU transmission
 *                      when either deadline timer has expired or when numbers 
 *                      of PDUs to send are transmitted or when any interrupt 
 *                      like CTRL-C from CLI or Stop from SNMP is received.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmLbiTxStopXmitLbmPdu (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif

    ECFM_LBLT_TRC_FN_ENTRY ();
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pLbInfo->u1TxLbmStatus = ECFM_TX_STATUS_READY;
    ECFM_CLEAR_INFINITE_TX_STATUS (pLbInfo->u2TxLbmMessages);
    ECFM_CLEAR_EXTRA_DEADLINE_STATUS (pLbInfo->u4TxLbmDeadline);

    /* Stop Timer LBIwhile if it is activated */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBI_INTERVAL, pMepInfo);

    /* Y.1731 : Stop Timer LBIDeadLine if it is activated */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBI_DEADLINE, pMepInfo);

    /* Y.1731 : Stop the Lbr Timeout if it is activated */
    EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_LBR_TIMEOUT, pMepInfo);

    /* Free the memory allocated to the last transmitted LBM PDU, if any */
    if (pLbInfo->LbmPdu.pu1Octets != NULL)

    {
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MEP_LBM_PDU_POOL,
                             pLbInfo->LbmPdu.pu1Octets);
        pLbInfo->LbmPdu.pu1Octets = NULL;
        pLbInfo->LbmPdu.u4OctLen = 0;
    }
    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmLbiTxStopXmitLbmPdu: "
                   "LBM PDU Transmission is stopped\r\n");

    /* For CLI ping */
    if (pLbInfo->b1TxLbmResultOk == ECFM_FALSE)

    {
        EcfmSendLbmEventToCli (pMepInfo, CLI_EV_ECFM_LB_TRANS_ERROR);
    }
    EcfmSendLbmEventToCli (pMepInfo, CLI_EV_ECFM_LB_TRANS_STOP);
    pLbInfo->CurrentCliHandle = ECFM_CLI_MAX_SESSIONS;
#ifdef NPAPI_WANTED
    if ((ECFM_HW_LBM_SUPPORT () == ECFM_TRUE) &&
        (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE))
    {
        tEcfmMepInfoParams  EcfmMepInfoParams;
        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                     sizeof (tEcfmMepInfoParams));
        EcfmMepInfoParams.u4ContextId = ECFM_LBLT_CURR_CONTEXT_ID ();
        EcfmMepInfoParams.u4IfIndex =
            ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum, pTempLbLtPortInfo);
        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
        EcfmMepInfoParams.u4VlanIdIsid = pMepInfo->u4PrimaryVidIsid;
        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

        EcfmFsMiEcfmStopLbmTransaction (&EcfmMepInfoParams);
        return;
    }
#endif /*  */
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function Name      : EcfmLbiTxAddLbmEntry 
 *
 * Description        : This routine is used to add the entry of the current
 *                      initiated LBM message in the Loopback initiation Table.
 *
 * Input(s)           : pLbInfo - Pointer to the structure that stores the
 *                      information regarding LB info.
 *                      pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *                        
 * Output(s)          : None.
 *
 * Returns            : tEcfmLbLtLbmInfo - Pointer to the inserted node.
 ******************************************************************************/
PRIVATE tEcfmLbLtLbmInfo *
EcfmLbiTxAddLbmEntry (tEcfmLbLtLBInfo * pLbInfo, tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLbmInfo   *pLbmNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmDllCurNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmDllNextNode = NULL;
    tEcfmLbLtLbmInfo   *pLbmRbTreeDelNode = NULL;

    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocate Memory to the LBM Node to be addded in the LbInit Info */
    ECFM_ALLOC_MEM_BLOCK_LBLT_LBM_TABLE (pLbmNode);
    while (pLbmNode == NULL)

    {
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_LBM_TABLE_POOL) != 0)

        {
            ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_OS_RESOURCE_TRC,
                           "EcfmLbiTxAddLbmEntry: "
                           "Memory Block exists but Memory Allocation to "
                           "LBM Node Failed\r\n");
            return NULL;
        }

        /* If the addition of this LBM entry exceeds the resources
         * allocated to Lbm table, then delete the oldest LBM entry if any
         * from the LbmDllList.
         */
        UTL_DLL_OFFSET_SCAN (&(gEcfmLbLtGlobalInfo.LbmDllList),
                             pLbmDllCurNode, pLbmDllNextNode,
                             tEcfmLbLtLbmInfo *)
        {
            /* Break after getting the first node which is also the
             * oldest LBM entry.
             */
            break;
        }
        /* Get the correponding node from the RB Tree and delete it */
        pLbmRbTreeDelNode =
            (tEcfmLbLtLbmInfo *) RBTreeGet (ECFM_LBLT_LBM_TABLE,
                                            pLbmDllCurNode);
        if (pLbmRbTreeDelNode == NULL)
        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiTxAddLbmEntry: " "Memory corrupted\r\n");
            return NULL;
        }

        EcfmLbLtUtilRemoveLbEntry (pLbmRbTreeDelNode);
        ECFM_ALLOC_MEM_BLOCK_LBLT_LBM_TABLE (pLbmNode);
    }
    ECFM_MEMSET (pLbmNode, ECFM_INIT_VAL, ECFM_LBLT_LBM_INFO_SIZE);

    /* Add MegIndex field */
    pLbmNode->u4MdIndex = pMepInfo->u4MdIndex;

    /* Add MeIndex field */
    pLbmNode->u4MaIndex = pMepInfo->u4MaIndex;

    /* Add MegId field */
    pLbmNode->u2MepId = pMepInfo->u2MepId;

    /* Add Sequence Number field  */
    pLbmNode->u4SeqNum = pLbInfo->u4NextLbmSeqNo;

    /* Add TransId field  */
    pLbmNode->u4TransId = pLbInfo->u4NextTransId;

    /* Add Target MAC Address */
    ECFM_MEMCPY (pLbmNode->TargetMacAddr, pLbInfo->TxLbmDestMacAddr,
                 ECFM_MAC_ADDR_LENGTH);

    /* Add Type of TLV send in the LBM PDU  */
    if (ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))

    {
        switch (pLbInfo->u1TxLbmTlvOrNone)

        {
            case ECFM_LBLT_LBM_WITH_DATA_TLV:
                pLbmNode->u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN;
                break;
            case ECFM_LBLT_LBM_WITH_TEST_TLV:
                if ((pLbInfo->u1TxLbmTstPatternType ==
                     ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC)
                    || (pLbInfo->u1TxLbmTstPatternType ==
                        ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC))

                {
                    pLbmNode->u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN_CRC;
                }

                else

                {
                    pLbmNode->u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN;
                }
                break;
            case ECFM_LBLT_LBM_WITHOUT_TLV:
                pLbmNode->u1TlvStatus = ECFM_LBLT_LBM_WITHOUT_PATTERN;
                break;
            default:
                break;
        }
    }

    else

    {

        /* TLVs will be present as per 802.1g */
        pLbmNode->u1TlvStatus = ECFM_LBLT_LBM_WITH_PATTERN;
    }

    pLbmNode->u1DestType = pLbInfo->u1TxLbmDestType;
    pLbmNode->u2DestMepId = pLbInfo->u2TxDestMepId;
    ECFM_MEMCPY (pLbmNode->au1LbmIcc, pLbInfo->au1LbmIcc,
                 ECFM_CARRIER_CODE_ARRAY_SIZE);
    pLbmNode->u4NodeId = pLbInfo->u4LbmNodeId;
    pLbmNode->u4IfNum = pLbInfo->u4LbmIfNum;
    pLbmNode->u1Ttl = pLbInfo->u1LbmTtl;
    /* Update the Number of LBM sent counter */
    pLbmNode->u2NoOfLBMSent = pLbmNode->u4SeqNum + ECFM_INCR_VAL;

    /* Initialize the LBR In counter for the new transaction */
    if (pLbmNode->u4SeqNum == ECFM_INIT_VAL)

    {
        pLbInfo->u2NoOfLbrIn = ECFM_INIT_VAL;
    }
    /* Add RBTree Node */
    if (RBTreeAdd (ECFM_LBLT_LBM_TABLE, pLbmNode) != ECFM_RB_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC |
                       ECFM_OS_RESOURCE_TRC,
                       "EcfmLbiTxAddLbmEntry: " "RBTree Add Failed!!\r\n");
        return pLbmNode;
    }

    /* Initialise the LBR SLL for this LBM Node to store the LBRs that will be 
     * received for this LBM sequence Number */
    TMO_SLL_Init (&(pLbmNode->LbrList));

    /* Store the LBM entry into tail of the LbmDllList */
    TMO_DLL_Init_Node (&(pLbmNode->LbmDllNextNode));
    TMO_DLL_Add (&(gEcfmLbLtGlobalInfo.LbmDllList),
                 &(pLbmNode->LbmDllNextNode));

    ECFM_LBLT_TRC_FN_EXIT ();
    return pLbmNode;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxAdjustPattern
 *
 * Description        : This routine is used to adjust the data till the
 *                      given size.
 *                        
 * Input(s)           : pu1Pattern    - Pattern to be adjusted.
 *                      u2PatternLen -  Pattern length 
 *                      u2PatternSize - Size till which pattern is repeated.
 *
 * Output(s)          : ppu1LbPdu - Pointer to the PDU. 
 *
 * Return Value(s)    : None.
 ******************************************************************************/
PRIVATE VOID
EcfmLbiTxAdjustPattern (UINT1 *pu1Pattern, UINT2 u2PatternLen,
                        UINT2 u2PatternSize, UINT1 **ppu1LbPdu)
{
    UINT1              *pu1Pdu = NULL;
    UINT2               u2PatternNewSize = ECFM_INIT_VAL;
    UINT2               u2SizeCounter = ECFM_INIT_VAL;
    pu1Pdu = *ppu1LbPdu;
    while (u2PatternNewSize < u2PatternSize)

    {
        for (u2SizeCounter = 0; u2SizeCounter < u2PatternLen; u2SizeCounter++)

        {
            pu1Pdu[u2PatternNewSize] = pu1Pattern[u2SizeCounter];
            u2PatternNewSize = u2PatternNewSize + ECFM_INCR_VAL;
            if (u2PatternNewSize == u2PatternSize)

            {
                break;
            }
        }
    }
    *ppu1LbPdu = pu1Pdu;
    return;
}

/*******************************************************************************
 * Function Name      : EcfmSendLbmEventToCli
 *
 * Description        : This routine will be called on the following events
 *                      by the LB Initiator and Initiator receiver.     
 *                        - At the exit of LBM transaction
 *                        - Time out before LBR is received
 *                        - Error occurred while sending LBM
 *                        
 * Input(s)           :  pMepInfo - Info of the MEP which has initiated the
 *                       LBM.
 *                       i4EventType - Event type
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : None.
 ******************************************************************************/
PRIVATE VOID
EcfmSendLbmEventToCli (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventType)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtThInfo    *pThInfo = NULL;
    tEcfmLbLtCliEvInfo *pCliEvent = NULL;
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    pThInfo = ECFM_LBLT_GET_THINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pThInfo);
    pCliEvent = ECFM_LBLT_CLI_EVENT_INFO (pLbInfo->CurrentCliHandle);
    if (pCliEvent == NULL)

    {
        return;
    }
    switch (u1EventType)

    {
        case CLI_EV_ECFM_LB_TRANS_STOP:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_LB_TRANS_STOP);
            break;
        case CLI_EV_ECFM_LBR_TIMEOUT:
            if ((ECFM_IS_LBR_CACHE_ENABLED () == ECFM_TRUE) &&
                (pLbInfo->u1TxLbmMode == ECFM_LBLT_LB_REQ_RES_TYPE) &&
                (EcfmCheckForLbrs (pMepInfo) != ECFM_TRUE))
            {
                pCliEvent->Msg.u4Msg1 = pLbInfo->u4ExpectedLbrSeqNo;
                pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_LBR_TIMEOUT);
            }
            else
            {
                return;
            }
            break;
        case CLI_EV_ECFM_LB_TRANS_ERROR:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_LB_TRANS_ERROR);
            break;
        default:
            pCliEvent->u1Events |= (0x01 << CLI_EV_ECFM_LB_TRANS_ERROR);
            break;
    }
    ECFM_GIVE_SEMAPHORE (pCliEvent->SyncSemId);
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCheckForLbrs
 *
 * Description        : This routine is called to find if any LBR is received 
 *                      for the sent LBM.
 *                        
 * Input(s)           :  pMepInfo - Info of the MEP which has initiated the
 *                       LBM.
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ECFM_TRUE / ECFM_FALSE.
 ******************************************************************************/
PRIVATE INT4
EcfmCheckForLbrs (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtLBInfo    *pLbInfo = NULL;
    tEcfmLbLtLbmInfo   *pLbmEntry = NULL;
    tEcfmLbLtLbrInfo   *pLbrNode = NULL;
    tEcfmLbLtLbmInfo    LbmNode;
    pLbInfo = ECFM_LBLT_GET_LBINFO_FROM_MEP (pMepInfo);
    ECFM_MEMSET (&LbmNode, ECFM_INIT_VAL, ECFM_LBLT_LBM_INFO_SIZE);
    LbmNode.u4MdIndex = pMepInfo->u4MdIndex;
    LbmNode.u4MaIndex = pMepInfo->u4MaIndex;
    LbmNode.u2MepId = pMepInfo->u2MepId;
    LbmNode.u4TransId = pLbInfo->u4NextTransId;
    LbmNode.u4SeqNum = pLbInfo->u4ExpectedLbrSeqNo;

    /* Get LBM entry for the Sequence Number and the
     * Transaction id from the LbInit tree maintained */
    pLbmEntry = (tEcfmLbLtLbmInfo *) RBTreeGet (ECFM_LBLT_LBM_TABLE, &LbmNode);
    if (pLbmEntry == NULL)

    {
        return ECFM_FALSE;
    }
    /* First node of LBR corresponding to the LBM entry */
    pLbrNode = (tEcfmLbLtLbrInfo *) TMO_SLL_First (&(pLbmEntry->LbrList));
    if (pLbrNode != NULL)

    {
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/*******************************************************************************
 * Function Name      : EcfmLbiTxSetRMepDbSeqNum
 *
 * Description        : This routine is used to set the Expected Lbr Sequence
 *                      Number for the Specific RMep of the Mep initiating
 *                      Multicast LBMs as it will expect mutiple LBR
 *                        
 * Input(s)           :  pMepInfo  - Pointer to the MEP info
 *                       u4SeqNum  - Expected sequence number in LBR
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmLbiTxSetRMepDbSeqNum (tEcfmLbLtMepInfo * pMepInfo, UINT4 u4LocalSeqNum)
{

    tEcfmLbLtRMepDbInfo RMepInfo;
    tEcfmLbLtRMepDbInfo *pRMepNode = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_LBLT_RMEP_DB_INFO_SIZE);

    /* Get Remote MEP entry corresponding to indices - MdIndex, 
     * MaIndex, MepId*/
    RMepInfo.u4MdIndex = pMepInfo->u4MdIndex;
    RMepInfo.u4MaIndex = pMepInfo->u4MaIndex;
    RMepInfo.u2MepId = pMepInfo->u2MepId;

    pRMepNode =
        RBTreeGetNext (ECFM_LBLT_RMEP_DB_TABLE, (tRBElem *) & RMepInfo, NULL);

    while (pRMepNode != NULL)
    {

        pRMepNode->u4ExpectedLbrSeqNo = u4LocalSeqNum;

        pRMepNode = RBTreeGetNext (ECFM_LBLT_RMEP_DB_TABLE, pRMepNode, NULL);

        if ((pRMepNode == NULL) ||
            (pRMepNode->u4MdIndex != pMepInfo->u4MdIndex) ||
            (pRMepNode->u4MaIndex != pMepInfo->u4MaIndex) ||
            (pRMepNode->u2MepId != pMepInfo->u2MepId))
        {
            break;
        }
    }

    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/******************************************************************************/
/*                           End  of cfmlbtsm.c                               */
/******************************************************************************/
