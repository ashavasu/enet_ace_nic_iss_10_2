/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmapi.c,v 1.69 2015/12/14 11:13:08 siva Exp $
 *
 * Description: This file contains the procedures called by other
 *              modules to access te functionality of this module
 *******************************************************************/

#include "cfminc.h"
#include "cfmglb.h"
#include "cfmccglb.h"
#include "cfmlbglb.h"
/* Prototypes of the Private Routines */
PRIVATE INT4 EcfmCcPktQueMsg PROTO ((tEcfmCcMsg *));
PRIVATE INT4 EcfmLbLtPktQueMsg PROTO ((tEcfmLbLtMsg *));
PRIVATE INT1 EcfmGetOpCodeValue PROTO ((tEcfmBufChainHeader *, UINT4));
/*****************************************************************************
 * Function Name      : EcfmCreatePort                                       *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module        *
 *                      to indicate the creation of a Port.                  *
 *                                                                           *
 * Input(s)           : u4ContextId - Current Context of ECFM CC Task        *
 *                      u4IfIndex - The Port Index of the port to be created *
 *                      u2LocalPortId - LocalPortId                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC INT4
EcfmCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCreatePort : ECFM MODULE - not Initialised \r\n");
        UNUSED_PARAM (pMsg);
        return ECFM_FAILURE;
    }
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCreatePort : Invalid IfIndex \n");
        UNUSED_PARAM (pMsg);
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCreatePort : ECFM MODULE -  not Started \r\n");
        UNUSED_PARAM (pMsg);
        return ECFM_FAILURE;
    }

    /* Verify the Interface Index for which Port Create indication is
     * received  */
    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmCreatePort : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_CREATE_PORT_MSG;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPortId;

    /* Sending Message and Event to own tasks */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      " EcfmCreatePort : Message Queuing Failed \n");

        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmDeletePort                                        *
 *                                                                            *
 * Description        : This function is called from the L2IWF Module         *
 *                      to indicate the creation of a Port.                   *
 *                                                                            *
 * Input(s)           : u4IfIndex - The IfIndex of the port to be deleted     *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmDeletePort (UINT4 u4IfIndex)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmDeletePort : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Verify the Interface Index for which deletion indication is received */
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmDeletePort : Invalid IfIndex \r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmDeletePort : ECFM MODULE - not Started \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmDeletePort : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Information */
    pMsg->MsgType = (tEcfmMsgType) ECFM_DELETE_PORT_MSG;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;
    pMsg->u4IfIndex = u4IfIndex;
    /* Sending Message and Event to own task */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmDeletePort : Message sending and Event"
                      "Notification failed \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmUpdatePortStatus                                  *
 *                                                                            *
 * Description        : This function is called from the L2IWF Module         *
 *                      to indicate the Port operstatus change.               *
 *                                                                            *
 * Input(s)           : u4IfIndex   - The IfIndex  of the port for which the  *
 *                                    indication belongs.                     *
 *                      u1PortState  - New State of the Port.                 *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1PortState)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUpdatePortStatus : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Verify the index for which the Port State Change is received */
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUpdatePortStatus : Invalid IfIndex \n");
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUpdatePortStatus: ECFM is not started \n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC | ECFM_OS_RESOURCE_TRC,
                      "EcfmUpdatePortStatus : MemBlock Allocation Failed"
                      "\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Node */
    pMsg->MsgType = (tEcfmMsgType) ECFM_OPER_STATUS_CHG_MSG;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;
    pMsg->uMsg.u1PortOperState = u1PortState;

    /* Sending Message and Event to own task */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUpdatePortStatus : Message Queuing Failed \n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcHandleInFrameFromPort                          *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module to     *
 *                      handover the CFMPDUs to CC task.                     *
 *                      OBSOLETED                                             *
 *                                                                           *
 * Input(s)           : pCruBuf - Pointer to the CFMPDU CRU Buffer           *
 *                      u4IfIndex - Interface Index of the Port              *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *                                                                           *
 ******************************************************************************/
PUBLIC INT4
EcfmCcHandleInFrameFromPort (tEcfmBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    return EcfmHandleInFrameFromPort (pCruBuf, u4IfIndex);
}

/******************************************************************************
 * Function Name      : EcfmHandleInFrameFromPort                             *
 *                                                                            *
 * Description        : This function is called from the L2IWF Module to      *
 *                      handover the CFMPDUs to CC task.                      *
 *                                                                            *
 * Input(s)           : pCruBuf - Pointer to the CFMPDU CRU Buffer            *
 *                      u4IfIndex - Interface Index of the Port               *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
EcfmHandleInFrameFromPort (tEcfmBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    tEcfmInterface      InterfaceId;
    tEcfmCfaIfInfo      CfaIfInfo;
    tEcfmCcMsg         *pCcMsg = NULL;
    tEcfmLbLtMsg       *pLbLtMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    ECFM_MEMSET (&InterfaceId, ECFM_INIT_VAL, sizeof (InterfaceId));
    ECFM_MEMSET (&CfaIfInfo, ECFM_INIT_VAL, sizeof (tCfaIfInfo));
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmHandleInFrameFromPort : ECFM MODULE - not Initialised \r\n");
        ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if ((ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE) ||
        (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId)))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcHandleInFrameFromPort: ECFM MODULE not started"
                      "\r\n");
        ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
        return ECFM_FAILURE;
    }

    /* Getting Interface Info from CFA */
    if (EcfmUtilGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC, "EcfmCcHandleInFrameFromPort:"
                          "Cannot Get IfType for Port %u \r\n", u4IfIndex);

        ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
        return ECFM_FAILURE;
    }

    /* Setting the Interface Type from CFA */
    InterfaceId.u1_InterfaceType = CfaIfInfo.u1IfType;

    InterfaceId.u4IfIndex = u4IfIndex;

    /* Set the interface id in the buffer */
    ECFM_BUF_SET_INTERFACEID (pCruBuf, InterfaceId);

    /*Check for the PDU OpCode Type */
    switch (EcfmGetOpCodeValue (pCruBuf, u4IfIndex))
    {
        case ECFM_OPCODE_CCM:
        case ECFM_OPCODE_AIS:
        case ECFM_OPCODE_LCK:
        case ECFM_OPCODE_LMM:
        case ECFM_OPCODE_LMR:
            /* CFM pdu is for CC Task, signal to CC task */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pCcMsg) == NULL)
            {
                ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            ECFM_MEMSET (pCcMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

            /* Set the Message type in the Message */
            pCcMsg->MsgType = (tEcfmMsgType) ECFM_PDU_RCV_FRM_CFA;

            /* Updating Message Node */
            pCcMsg->u4IfIndex = u4IfIndex;
            pCcMsg->u4ContextId = u4ContextId;
            pCcMsg->u2PortNum = u2LocalPort;
            pCcMsg->uMsg.pEcfmPdu = pCruBuf;
            /* Sending message and Event to own task */
            if (EcfmCcPktQueMsg (pCcMsg) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (u4ContextId,
                              ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmHandleInFrameFromPort:Posting to Message Queue "
                              "& sending Event to CC task  FAILED !!! \r\n");

                ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
                pCcMsg->uMsg.pEcfmPdu = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pCcMsg);
                return ECFM_FAILURE;
            }
            break;
        case ECFM_OPCODE_LBR:
        case ECFM_OPCODE_LBM:
        case ECFM_OPCODE_LTR:
        case ECFM_OPCODE_LTM:
        case ECFM_OPCODE_TST:
        case ECFM_OPCODE_APS:
        case ECFM_OPCODE_RAPS:
        case ECFM_OPCODE_MCC:
        case ECFM_OPCODE_1DM:
        case ECFM_OPCODE_DMM:
        case ECFM_OPCODE_DMR:
        case ECFM_OPCODE_EXM:
        case ECFM_OPCODE_EXR:
        case ECFM_OPCODE_VSM:
        case ECFM_OPCODE_VSR:
            /* CFM pdu is for LBLT Task, signal to LBLT task */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pLbLtMsg) == NULL)
            {
                ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
                ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            ECFM_MEMSET (pLbLtMsg, ECFM_INIT_VAL, sizeof (tEcfmLbLtMsg));

            /* Set the Message type in the Message */
            pLbLtMsg->MsgType = (tEcfmMsgType) ECFM_PDU_RCV_FRM_CFA;

            /* Updating Message Node */
            pLbLtMsg->u4IfIndex = u4IfIndex;
            pLbLtMsg->u4ContextId = u4ContextId;
            pLbLtMsg->u2PortNum = u2LocalPort;
            pLbLtMsg->uMsg.pEcfmPdu = pCruBuf;
            /* Sending message and Event to own task */
            if (EcfmLbLtPktQueMsg (pLbLtMsg) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (u4ContextId,
                              ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmHandleInFrameFromPort:Posting to Message Queue "
                              "& sending Event to LBLT task  FAILED !!! \r\n");

                ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
                pLbLtMsg->uMsg.pEcfmPdu = NULL;
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtMsg);
                return ECFM_FAILURE;
            }
            break;
        default:
            ECFM_GLB_TRC (u4ContextId,
                          ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                          "EcfmHandleInFrameFromPort: unknown OpCode !!! \r\n");
            ECFM_RELEASE_CRU_BUF (pCruBuf, FALSE);
            return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmCcPktQueMsg                                         *       
 *                                                                            *       
 * DESCRIPTION      : Function to post CFMPDU message and events to           *       
 *                    ECFM's CC Task.                                         *       
 *                                                                            *       
 * INPUT            : pMsg - Pointer to msg to be posted                      *       
 *                                                                            *       
 * OUTPUT           : None                                                    *       
 *                                                                            *       
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *       
 *                                                                            *       
 *****************************************************************************/
PRIVATE INT4
EcfmCcPktQueMsg (tEcfmCcMsg * pMsg)
{
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmCcPktQueMsg:" "ECFM is not initialised \r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_ENQUE_MSG (ECFM_CC_PKT_QUEUE_ID, (UINT1 *) (&pMsg),
                        OSIX_DEF_MSG_LEN) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId, ECFM_OS_RESOURCE_TRC |
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmCcPktQueMsg:Enqueue CFM PDU Message FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Sending Event */
    if (ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_CFM_PDU_IN_QUEUE)
        != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId, ECFM_OS_RESOURCE_TRC |
                      ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcPktQueMsg: Sending Event for CFM PDU to"
                      "CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmCcCfgQueMsg                                         *
 *                                                                            *
 * DESCRIPTION      : Function to post configuration message and events to    * 
 *                    ECFM's CC Task.                                         *
 *                                                                            *
 * INPUT            : pMsg - Pointer to msg to be posted                      *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
EcfmCcCfgQueMsg (tEcfmCcMsg * pMsg)
{
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmCcCfgQueMsg:" "ECFM is not initialised \r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_ENQUE_MSG (ECFM_CC_CFG_QUEUE_ID, (UINT1 *) (&pMsg),
                        OSIX_DEF_MSG_LEN) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId, ECFM_OS_RESOURCE_TRC |
                      ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCcCfgQueMsg:Enqueue Configuration Message FAILED "
                      "\r\n");
        return ECFM_FAILURE;
    }

    /* Sending Event */
    if (ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_CFG_MSG_IN_QUEUE)
        != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId, ECFM_OS_RESOURCE_TRC |
                      ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcCfgQueMsg: Sending Event for condifurgation"
                      " message to CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmCcInterruptQueMsg                                   *
 *                                                                            *
 * DESCRIPTION      : Function to post interrupt message and events to        * 
 *                    ECFM's CC Task.                                         *
 *                                                                            *
 * INPUT            : pMsg - Pointer to msg to be posted                      *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
EcfmCcInterruptQueMsg (tEcfmCcMsg * pMsg)
{
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmCcInterruptQueMsg:" "ECFM is not initialised \r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_ENQUE_MSG (ECFM_CC_INT_QUEUE_ID, (UINT1 *) (&pMsg),
                        OSIX_DEF_MSG_LEN) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId, ECFM_OS_RESOURCE_TRC |
                      ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCcInterruptQueMsg:Enqueue Interrupt Message FAILED "
                      "\r\n");
        return ECFM_FAILURE;
    }

    /* Sending Event */
    if (ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_INT_PDU_IN_QUEUE)
        != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId, ECFM_OS_RESOURCE_TRC |
                      ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcInterruptQueMsg: Sending Event for Interrupt"
                      " message to CC Task FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*******************************************************************************
 *    FUNCTION NAME    : EcfmCcLock                                            *
 *                                                                             *
 *    DESCRIPTION      : Function to take the mutual exclusion protocol        *
 *                       semaphore.                                            *
 *                                                                             *
 *    INPUT            : None                                                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                             *
 *                                                                             *
 ******************************************************************************/
INT4
EcfmCcLock ()
{

    if (ECFM_TAKE_SEMAPHORE (ECFM_CC_SEM_ID) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /*Select Default Context */
    ECFM_CC_SELECT_CONTEXT (ECFM_DEFAULT_CONTEXT);
    return SNMP_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : EcfmCcUnLock                                         *
 *                                                                            *
 *    DESCRIPTION      : Function to give the mutual exclusion protocol       *
 *                       semaphore.                                           *
 *                                                                            *
 *    INPUT            : None                                                 *
 *                                                                            *
 *    OUTPUT           : None                                                 *
 *                                                                            *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                            *
 *****************************************************************************/
INT4
EcfmCcUnLock ()
{
    /* Release Default Context */
    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_GIVE_SEMAPHORE (ECFM_CC_SEM_ID);
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtCreatePort                                   *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module        *
 *                      to indicate the creation of a Port.                  *
 *                      OBSOLETED                                             *
 *                                                                           *
 * Input(s)           : u4ConetxtId - Current Context Id                     *
 *                      u4IfIndex - The IfIndex of the port to be created    *
 *                      u2LocalPortId - Port Number                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *****************************************************************************/

PUBLIC INT4
EcfmLbLtCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalPortId);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtDeletePort                                   *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module        *
 *                      to indicate the creation of a Port.                  *
 *                      OBSOLETED                                             *
 *                                                                           *
 * Input(s)           : u4IfIndex - The IfIndex of the port to be deleted     *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *****************************************************************************/

PUBLIC INT4
EcfmLbLtDeletePort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtUpdatePortStatus                              *
 *                                                                            *
 * Description        : This function is called from the CFA Module           *
 *                      to indicate the Port operstatus change.               *
 *                      OBSOLETED                                             *
 *                                                                            *
 * Input(s)           : u4IfIndex - The IfIndex of the port for which the     *
 *                                  indication belongs.                       *
 *                      u1PortState - New State of the Port to be updated     * 
 *                                                                            *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1PortState)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PortState);
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmPortDeiBitChangeIndication                        *
 *                                                                            *
 * Description        : This function is called by the L2IWF Module when Dei  *
 *                      Bit is changed. If DEI Bit is set to true, then encode*
 *                      drop eligible parameter in S-Tag. This is done by     *
 *                      setting the fourth bit of VLAN ID.                    *
 *                                                                            *
 * Input(s)           : u4IfIndex - Context Identifier                        *
 *                      u1DeiBitValue - Dei Bit Value                         *
 *                                                                            * 
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmPortDeiBitChangeIndication (UINT4 u4IfIndex, UINT1 u1DeiBitValue)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = (UINT2) ECFM_INIT_VAL;

    UNUSED_PARAM (u1DeiBitValue);
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmPortDeiBitChangeIndication : Invalid IfIndex \n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmPortDeiBitChangeIndication : ECFM MODULE -"
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX
        (u4IfIndex, &u4ContextId, &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* Checking System Status for ECFM */
    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_VLAN_UPDATE_DEI_BIT;

    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmCcmOffloadCreateVlanIndication                    *
 *                                                                            *
 * Description        : This function is called by the VLAN Module            *
 *                      when a VLAN is created.                               *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                      VlanId      - VLAN ID to be created                   * 
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffloadCreateVlanIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCcmOffloadCreateVlanIndication : ECFM MODULE -"
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_CREATE_VLAN;
    pMsg->u4ContextId = u4ContextId;
    pMsg->uMsg.Indications.u4VlanIdIsid = VlanId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmCcmOffloadDeleteVlanIndication                    *
 *                                                                            *
 * Description        : This function is called by the VLAN Module            *
 *                      when a VLAN is Deleted.                               *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                      VlanId      - VLAN ID to be created                   * 
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffloadDeleteVlanIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCcmOffloadDeleteVlanIndication : ECFM MODULE "
                      " - not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_DELETE_VLAN;
    pMsg->uMsg.Indications.u4VlanIdIsid = VlanId;
    pMsg->u4ContextId = u4ContextId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmPortStateChangeIndication                   *
 *                                                                            *
 * Description        : This function is called by the VLAN Module            *
 *                      when Port State is changed                            *
 *  Input(s)           : u2MstInst  - InstanceID for which the port state is 
 *                                    to be updated
 *                      u4IfIndex -  Interface Index of the port, whose port
 *                                   state is to be updated .
 *                      u1PortState - Port state to be set 
 *                                                                           
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmPortStateChangeIndication (UINT2 u2MstInst, UINT4 u4IfIndex,
                               UINT1 u1PortState)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmPortStateChangeIndication : ECFM MODULE - not "
                      "Initialised \r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_PORT_STATE_CHANGE;
    pMsg->uMsg.Indications.u1PortState = u1PortState;
    pMsg->uMsg.Indications.u2MstInst = u2MstInst;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/***************************************************************************
* Function Name     : EcfmPbPortStateChangeIndication                      *
*                                                                          *
* Description      : This function is called by RSTP Module for port state *
*                    change Indication.                                    *
* Input (s)        : u4CepIfIndex- IfIndex                                 *
*                    SVlanId - svlan ID                                    *
*                    u1PortState - Port state to be set                    *
*                                                                          *
* Output (s)       : None                                                  *
*                                                                          *
* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                         *
****************************************************************************/
PUBLIC INT4
EcfmPbPortStateChangeIndication (UINT4 u4CepIfIndex, tVlanId SVlanId,
                                 UINT1 u1PortState)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmPbPortStateChangeIndication : ECFM MODULE - not "
                      "Initialised \r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_VALID_INTERFACE (u4CepIfIndex) == ECFM_FALSE)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4CepIfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_PB_PORT_STATE_CHANGE;
    pMsg->uMsg.Indications.u1PortState = u1PortState;
    pMsg->u4IfIndex = u4CepIfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;
    pMsg->uMsg.Mep.u4VidIsid = SVlanId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmHandleVlanPortMembershipIndication               *
 *                                                                            *
 * Description        : This function is called by the VLAN Module            *
 *                      when a VLAN Port List is updated                      *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                      VlanId      - VLAN ID to be created                   * 
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmVlanPortMembershipIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmVlanPortMembershipIndication : ECFM MODULE - "
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_VLAN_MEMBER_CHANGE;
    pMsg->uMsg.Indications.u4VlanIdIsid = VlanId;
    pMsg->u4ContextId = u4ContextId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmCcmOffloadMstpEnableIndication                    *
 *                                                                            *
 * Description        : This function is called by the MSTP  Module           *
 *                      when MSTP is Enabled.                                 *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmMstpEnableIndication (UINT4 u4ContextId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMstpEnableIndication : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_MSTP_ENABLE;
    pMsg->u4ContextId = u4ContextId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmVlanEtherTypeChngInd                              *
 *                                                                            *
 * Description        : This function is called by the VLAN Module            *
 *                      when ether Type for VLAN is changed                   *
 *                                                                            *
 * Input(s)           : u4ContextId - Current Context Id                      *
 *                      u4IfIndex   - IfIndex of the port for which Ether Type*
 *                                    updated.                                *
 *                      u1EtherType  - Ingress of Egress Ether Type           * 
 *                      u2EtherTypeValue - Ether Type Value                   * 
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmVlanEtherTypeChngInd (UINT4 u4ContextId, UINT4 u4IfIndex,
                          UINT1 u1IngressorEgress, UINT2 u2EtherTypeValue)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmVlanEtherTypeChngInd : ECFM MODULE - "
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4IfIndex = u4IfIndex;

    pMsg->MsgType = (tEcfmMsgType) ECFM_VLAN_ETHER_TYPE_CHANGE;
    pMsg->uMsg.Indications.u2EtherTypeValue = u2EtherTypeValue;
    pMsg->uMsg.Indications.u1EtherType = u1IngressorEgress;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmCcmOffloadMstpDisableIndication                   *
 *                                                                            *
 * Description        : This function is called by the MSTP  Module           *
 *                      when MSTP is Disabled.                                *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmMstpDisableIndication (UINT4 u4ContextId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMstpDisableIndication : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_MSTP_DISABLE;
    pMsg->u4ContextId = u4ContextId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtLock                                            *
 *                                                                            *
 * DESCRIPTION      : Function to take the mutual exclusion protocol          *
 *                    semaphore.                                              *
 *                                                                            *
 * INPUT            : None                                                    *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtLock ()
{
    if (!ECFM_LBLT_SEM_ID)
    {
        return SNMP_FAILURE;
    }
    if (ECFM_TAKE_SEMAPHORE (ECFM_LBLT_SEM_ID) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Select Default Context */
    if (ECFM_LBLT_SELECT_CONTEXT (ECFM_DEFAULT_CONTEXT) != ECFM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtUnLock                                          *
 *                                                                            *
 * DESCRIPTION      : Function to give the mutual exclusion protocol          *
 *                    semaphore.                                              *
 *                                                                            *
 * INPUT            : None                                                    *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *
 *                                                                            *
 *****************************************************************************/
INT4
EcfmLbLtUnLock ()
{
    if (!ECFM_LBLT_SEM_ID)
    {
        return SNMP_FAILURE;
    }
    /* Release Default Context */
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_GIVE_SEMAPHORE (ECFM_LBLT_SEM_ID);
    return SNMP_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtCfgQueMsg                                       *
 *                                                                            *
 * DESCRIPTION      : Function to post Configuration message and events to    *
 *                    LBLT task.                                              *
 *                                                                            *
 * INPUT            : pMsg - Pointer to msg to be posted                      *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtCfgQueMsg (tEcfmLbLtMsg * pMsg)
{
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtCfgQueMsg:" "ECFM is not initialised\r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_ENQUE_MSG (ECFM_LBLT_CFG_QUEUE_ID, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtCfgQueMsg:"
                      "ECFM Enqueue Configuration Message to LBLT Task FAILED"
                      "\r\n");
        return ECFM_FAILURE;
    }
    /* Sending Event to LBLT Task */
    if (ECFM_SEND_EVENT (ECFM_LBLT_TASK_ID, ECFM_EV_CFG_MSG_IN_QUEUE)
        != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtCfgQueMsg: Send Configuration Event to LBLT Task "
                      "FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : EcfmLbLtPktQueMsg                                       *
 *                                                                            *
 * DESCRIPTION      : Function to post CFMPDU message and events to LBLT task.*
 *                                                                            *
 * INPUT            : pMsg - Pointer to msg to be posted                      *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE                               *
 *                                                                            *
 *****************************************************************************/
PRIVATE INT4
EcfmLbLtPktQueMsg (tEcfmLbLtMsg * pMsg)
{
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtPktQueMsg:" "ECFM is not initialised\r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_ENQUE_MSG (ECFM_LBLT_PKT_QUEUE_ID, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtPktQueMsg:"
                      "ECFM Enqueue CFMPDU Message to LBLT Task FAILED \r\n");
        return ECFM_FAILURE;
    }

    /* Sending Event to LBLT Task */
    if (ECFM_SEND_EVENT (ECFM_LBLT_TASK_ID, ECFM_EV_CFM_PDU_IN_QUEUE)
        != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pMsg->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC,
                      "EcfmLbLtPktQueMsg: Send CFMPDU Event to LBLT Task FAILED"
                      "\r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmLbLtHandleInFrameFromPort                        *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module to     *
 *                      handover the CFMPDUs to LBLT task.                   *
 *                      OBSOLETED                                             *
 *                                                                           *
 * Input(s)           : pCruBuf - Pointer to the CFMPDU CRU Buffer           *
 *                      u4IfIndex - Interface Index of the Port              *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtHandleInFrameFromPort (tEcfmBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    return EcfmHandleInFrameFromPort (pCruBuf, u4IfIndex);
}

/******************************************************************************
 *    Function Name       : EcfmCreateContext                                 *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context created, this *
 *                          function post a message to ECFM for creating a    *
 *                          context                                           *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/

PUBLIC INT4
EcfmCreateContext (UINT4 u4ContextId)
{
    tEcfmCcMsg         *pMsg = NULL;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCreateContext : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCreateContext : MEM Block Allocation Failed \r\n");
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);
    pMsg->MsgType = (tEcfmMsgType) ECFM_CREATE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    /* Sending Message and Event to own tasks */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCreateContext : Message Queuing Failed \n");

        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2MI_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    Function Name       : EcfmDeleteContext                                 *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context deleted, this *
 *                          function ports a Delete context Message to ECFM   *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/

PUBLIC INT4
EcfmDeleteContext (UINT4 u4ContextId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmDeleteContext : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                     ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmDeleteContext : MEM Block Allocation Failed \r\n");
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_DELETE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    /* Sending Message and Event to own tasks */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmDeleteContext : Message Queuing Failed \n");

        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2MI_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    Function Name       : EcfmMapPort                                       *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever some port is mapped to  *
 *                          any  context                                      *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                          u2IfIndex    - mapped Port number                 *
 *    Input(s)            : u2IfIndex    - local Port number                  *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
EcfmMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMapPort : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Verify the Interface Index for which Port Create indication is
     * received  */
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC, "EcfmMapPort : Invalid IfIndex \n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMapPort : ECFM MODULE -  not Started \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmMapPort : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_MAP_PORT_MSG;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPortId;

    /* Sending Message and Event to own tasks */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMapPort : Message Queuing Failed \n");

        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2MI_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;

}

/******************************************************************************
 *    Function Name       : EcfmUnMapPort                                     *
 *                                                                            *
 *    Description         : Invoked by L2IWF  whenever any port is unmapped   *
 *                          from a context                                    *
 *                                                                            *
 *    Input(s)            : u2IfIndex - port that is unmapped .               *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Use of Recursion    : None.                                             *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
EcfmUnMapPort (UINT4 u4IfIndex)
{

    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUnMapPort : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Verify the Interface Index for which deletion indication is received */
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmUnMapPort : Invalid IfIndex \r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmUnMapPort : ECFM MODULE - not Started \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmUnMapPort : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Information */
    pMsg->MsgType = (tEcfmMsgType) ECFM_UNMAP_PORT_MSG;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;

    /* Sending Message and Event to own task */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUnMapPort : Message sending and Event Notification"
                      "failed \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2MI_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;

}

/****************************************************************************** 
 *                                                                            *
 *    Function Name       : EcfmLbLtCreateContext                             *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context created, this *
 *                          function post a message to ECFM for creating a    *
 *                          context                                           *
 *                          OBSOLETED                                         *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS / ECFM_FAILURE                        *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtCreateContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return ECFM_SUCCESS;
}

/*******************************************************************************
 *                                                                             *
 *    Function Name       : EcfmLbLtDeleteContext                              *
 *                                                                             *
 *    Description         : Invoked by L2IWF whenever a context deleted, this  *
 *                          function ports a Delete context Message to ECFM    *
 *                                                                             *
 *    Input(s)            : u4ContextId  - context Id                          *
 *                          OBSOLETED                                          *
 *                                                                             *
 *    Output(s)           : None                                               *
 *                                                                             *
 *    Exceptions or Operating                                                  *
 *    System Error Handling    : None.                                         *
 *                                                                             *
 *    Use of Recursion        : None.                                          *
 *                                                                             *
 *    Returns            : ECFM_SUCCESS / ECFM_FAILURE                         *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
EcfmLbLtDeleteContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    Function Name       : EcfmLbLtMapPort                                   *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever some port is mapped to  *
 *                          any  context                                      *
 *                          OBSOLETED                                         *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                          u2IfIndex    - mapped Port number                 *
 *    Input(s)            : u2IfIndex    - local Port number                  *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS / ECFM_FAILURE                        *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalPortId);
    return ECFM_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : EcfmLbLtUnMapPort                                 *
 *                                                                            *
 *    Description         : Invoked by L2IWF  whenever any port is unmapped   *
 *                          from a context                                    *
 *                          OBSOLETED                                         *
 *                                                                            *
 *    Input(s)            : u2IfIndex - port that is unmapped .               *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtUnMapPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmCcCreatePort                                      *
 *                                                                            *
 * Description        : This function is called from the L2IWF Module         *
 *                      to indicate the creation of a Port.                   *
 *                      OBSOLETED                                             *
 *                                                                            *
 * Input(s)           : u4ContextId - Current Context of ECFM CC Task         *
 *                      u4IfIndex - The Port Index of the port to be created  *
 *                      u2LocalPortId - LocalPortId                           *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmCcCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    return EcfmCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
}

/******************************************************************************
 * Function Name      : EcfmCcDeletePort                                      *
 *                                                                            *
 * Description        : This function is called from the L2IWF Module         *
 *                      to indicate the creation of a Port.                   *
 *                      OBSOLETED                                             *
 *                                                                            *
 * Input(s)           : u4IfIndex - The IfIndex of the port to be deleted     *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
EcfmCcDeletePort (UINT4 u4IfIndex)
{
    return EcfmDeletePort (u4IfIndex);
}

/******************************************************************************
 * Function Name      : EcfmCcUpdatePortStatus                                *
 *                                                                            *
 * Description        : This function is called from the L2IWF Module         *
 *                      to indicate the Port operstatus change.               *
 *                      OBSOLETED                                             *
 *                                                                            *
 * Input(s)           : u4IfIndex   - The IfIndex  of the port for which the  *
 *                                    indication belongs.                     *
 *                      u1PortState  - New State of the Port.                 *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmCcUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1PortState)
{
    return EcfmUpdatePortStatus (u4IfIndex, u1PortState);
}

/******************************************************************************
 *    Function Name       : EcfmCcCreateContext                               *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context created, this *
 *                          function post a message to ECFM for creating a    *
 *                          context                                           *
 *                          OBSOLETED                                         *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/

PUBLIC INT4
EcfmCcCreateContext (UINT4 u4ContextId)
{
    return EcfmCreateContext (u4ContextId);
}

/******************************************************************************
 *    Function Name       : EcfmCcDeleteContext                               *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context deleted, this *
 *                          function ports a Delete context Message to ECFM   *
 *                          OBSOLETED                                         *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/

PUBLIC INT4
EcfmCcDeleteContext (UINT4 u4ContextId)
{
    return EcfmDeleteContext (u4ContextId);
}

/******************************************************************************
 *    Function Name       : EcfmCcMapPort                                     *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever some port is mapped to  *
 *                          any  context                                      *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                          u2IfIndex    - mapped Port number                 *
 *    Input(s)            : u2IfIndex    - local Port number                  *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
EcfmCcMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    return EcfmMapPort (u4ContextId, u4IfIndex, u2LocalPortId);
}

/******************************************************************************
 *    Function Name       : EcfmCcUnMapPort                                   *
 *                                                                           *
 *    Description         : Invoked by L2IWF  whenever any port is unmapped  *
 *                          from a context                                   *
 *                                                                           *
 *    Input(s)            : u2IfIndex - port that is unmapped .              *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Use of Recursion        : None.                                        *
 *                                                                           *
 *    Returns            : ECFM_SUCCESS                                      *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
EcfmCcUnMapPort (UINT4 u4IfIndex)
{
    return EcfmUnMapPort (u4IfIndex);
}

/******************************************************************************
 *    Function Name       : EcfmGetOpCodeValue                                *
 *                                                                            *
 *    Description         : This routine gets the OpCode value from the CFM   *
 *                          PDU received from lower layer.                    *
 *                          from a context                                    *
 *                                                                            *
 *    Input(s)            : pCruBuf - Pointer to the CRU-Buffer received      *
 *                          u4IfIndex - Index of interface in which the cfm   *
 *                          frame was received.                               *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns             : OpCode Value                                      *
 *****************************************************************************/
PRIVATE INT1
EcfmGetOpCodeValue (tEcfmBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    UINT1               u1OpCode = ECFM_INIT_VAL;
    UINT4               u4VlanOffset = ECFM_VLAN_TAG_OFFSET;
    UINT2               u2LenOrType = 0;
    if (EcfmVlanGetTagLenInFrame (pCruBuf, u4IfIndex, &u4VlanOffset) ==
        VLAN_SUCCESS)
    {
        ECFM_CRU_GET_2_BYTE (pCruBuf, u4VlanOffset, u2LenOrType);
    }

    /* To classify the llc encapsulated frames */
    if (u2LenOrType != ECFM_PDU_TYPE_CFM)
    {
        /*8 is 2 byte of typelength in VLAN hdr and 6byte of the LLC */
        u4VlanOffset = u4VlanOffset + ECFM_LLC_SNAP_HDR_SIZE;
        ECFM_CRU_GET_2_BYTE (pCruBuf, u4VlanOffset, u2LenOrType);
    }
    /*Received CFM frames contains the Type */
    if (u2LenOrType == ECFM_PDU_TYPE_CFM)
    {
        /*Skip the CFM-PDU Type, MD-Level and Version Field */
        u4VlanOffset = u4VlanOffset + ECFM_CFM_PDU_TYPE_FIELD_SIZE
            + ECFM_MDLEVEL_VER_FIELD_SIZE;
        /*Get the OpCode */
        ECFM_CRU_GET_1_BYTE (pCruBuf, u4VlanOffset, u1OpCode);
    }
    return u1OpCode;
}

/*****************************************************************************
 * Function Name      : EcfmRegisterProtcols                                  *
 *                                                                            *
 * Description        : Routine used by protocols to register with ECFM.      *
 *                                                                            *
 * Input(s)           : tRmRegParams - Reg. params to be provided by protocols*
 *                      u4ContextId - Current ContextId of the application    *
 *                      that wants to get reqistered.                         *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmRegisterProtocols (tEcfmRegParams * pEcfmReg, UINT4 u4ContextId)
{

    UINT4               u4ModId = ECFM_INIT_VAL;
    UINT4               u4EventsRegd = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmRegisterProtocols : ECFM MODULE - "
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    if (pEcfmReg == NULL)
    {
        ECFM_GLB_TRC (u4ContextId, ECFM_ALL_FAILURE_TRC,
                      "EcfmRegisterProtocols: "
                      "Invalid Registration params from application\n");
        return ECFM_FAILURE;

    }

    /* Validate the AppId given by application */
    u4ModId = pEcfmReg->u4ModId;

    u4EventsRegd = pEcfmReg->u4EventsId;

    if ((u4ModId == 0) || (u4ModId >= ECFM_MAX_APPS))
    {
        ECFM_GLB_TRC (u4ContextId, ECFM_ALL_FAILURE_TRC,
                      "EcfmRegisterProtocols:Invalid application id "
                      "during registration\n");
        return ECFM_FAILURE;

    }

    if (u4EventsRegd == ECFM_INIT_VAL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRegisterProtocols: No events provided "
                      "for regisration \n");
        return ECFM_FAILURE;
    }

    ECFM_CC_LOCK ();

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Set the Per Context CC DataBase Module registration bit map */
    ECFM_SET_U4BIT (ECFM_CC_REG_MOD_BMP (), u4ModId);

    /* Allocate memory for the new entry */
    if (ECFM_CC_GET_APP_INFO (u4ModId) == NULL)
    {
        if (ECFM_ALLOC_MEM_BLOCK_CC_REG_APP (ECFM_CC_GET_APP_INFO (u4ModId)) ==
            NULL)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                         "Unable to allocate memory for the new registration entry\n");
            ECFM_CLEAR_U4BIT (ECFM_CC_REG_MOD_BMP (), u4ModId);
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
    }

    /* Update the application specific info */
    ECFM_CC_GET_APP_INFO (u4ModId)->pFnRcvPkt = pEcfmReg->pFnRcvPkt;

    ECFM_CC_GET_APP_INFO (u4ModId)->u4ModId = u4ModId;

    ECFM_CC_GET_APP_INFO (u4ModId)->u4EventsId = u4EventsRegd;

    ECFM_CC_GET_APP_INFO (u4ModId)->u4VlanInd = pEcfmReg->u4VlanInd;
    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();

    /* Registration per context in CC DB is successfull !! */

    ECFM_LBLT_LOCK ();

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "No events provided for regisration \n");
        ECFM_LBLT_UNLOCK ();
        i4RetVal = ECFM_FAILURE;
    }

    if (i4RetVal == ECFM_SUCCESS)
    {
        /* Set the Per Context LBLT DataBase Module registration bit map */
        ECFM_SET_U4BIT (ECFM_LBLT_REG_MOD_BMP (), u4ModId);

        /* Allocate memory for the new entry */
        if (ECFM_LBLT_GET_APP_INFO (u4ModId) == NULL)
        {
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_REG_APP
                (ECFM_LBLT_GET_APP_INFO (u4ModId)) == NULL)
            {
                ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                               "Unable to allocate memory for the new "
                               "registration entry\n");
                ECFM_CLEAR_U4BIT (ECFM_LBLT_REG_MOD_BMP (), u4ModId);
                i4RetVal = ECFM_FAILURE;
            }
            else
            {
                /* Update the application specific info */
                ECFM_LBLT_GET_APP_INFO (u4ModId)->pFnRcvPkt =
                    pEcfmReg->pFnRcvPkt;

                ECFM_LBLT_GET_APP_INFO (u4ModId)->u4ModId = u4ModId;

                ECFM_LBLT_GET_APP_INFO (u4ModId)->u4EventsId = u4EventsRegd;

                ECFM_LBLT_GET_APP_INFO (u4ModId)->u4VlanInd =
                    pEcfmReg->u4VlanInd;
            }
        }
        else
        {
            /* Update the application specific info */
            ECFM_LBLT_GET_APP_INFO (u4ModId)->pFnRcvPkt = pEcfmReg->pFnRcvPkt;

            ECFM_LBLT_GET_APP_INFO (u4ModId)->u4ModId = u4ModId;

            ECFM_LBLT_GET_APP_INFO (u4ModId)->u4EventsId = u4EventsRegd;

            ECFM_LBLT_GET_APP_INFO (u4ModId)->u4VlanInd = pEcfmReg->u4VlanInd;
        }

        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        /* Registration per context in LBLT DB is successfull !! */
    }

    if (i4RetVal == ECFM_FAILURE)
    {
        /* LBLT Registration Failed, so handle it by resetting the Module bit in 
         * CC DB Bitmap 
         */
        ECFM_CC_LOCK ();

        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }

        ECFM_CLEAR_U4BIT (ECFM_CC_REG_MOD_BMP (), u4ModId);

        /* Application Information memory in CC DB is not released as it is a 
         * global memory and not per context, the data in it overwritten when 
         * a module registers again 
         */

        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
    }

    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmDeRegisterProtcols                               *
 *                                                                           *
 * Description        : Routine used by protocols to de-register from ECFM.  *
 *                                                                           *
 * Input(s)           : u4EntId - App ID of the application                  *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
EcfmDeRegisterProtocols (UINT4 u4ModId, UINT4 u4ContextId)
{
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmDeRegisterProtocols : ECFM MODULE - "
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (ECFM_CC_GET_APP_INFO (u4ModId) == NULL)
    {

        ECFM_CC_TRC_ARG1 (ECFM_ALL_FAILURE_TRC,
                          "EcfmDeRegistration failed for AppId %d\n", u4ModId);
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;

    }

    /* Reset the global registration bit map */
    ECFM_CLEAR_U4BIT (ECFM_CC_REG_MOD_BMP (), u4ModId);

    /* Application Information memory in CC DB is not released as it is a 
     * global memory and not per context, the data in it overwritten when 
     * a module registers again 
     */
    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();

    /* De-Registration at CC DB is successfull !! */

    ECFM_LBLT_LOCK ();

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (ECFM_LBLT_GET_APP_INFO (u4ModId) == NULL)

    {
        ECFM_LBLT_TRC_ARG1 (ECFM_ALL_FAILURE_TRC,
                            "EcfmDeRegistration failed for AppId %d\n",
                            u4ModId);
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;

    }

    /* Reset the global registration bit map */
    ECFM_CLEAR_U4BIT (ECFM_LBLT_REG_MOD_BMP (), u4ModId);

    /* Application Information memory in LBLT DB is not released as it is a 
     * global memory and not per context, the data in it overwritten when 
     * a module registers again 
     */

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function Name      : EcfmMepRegisterAndGetFltStatus                        *
 *                                                                            *
 * Description        : Routine used by protocols to register with ECFM.      *
 *                                                                            *
 * Input(s)           : tRmRegParams - Reg. params to be provided by protocols*
 *                      tEcfmMepInfoParams - Pointer to Mep information       * 
 *                      that needs to be monitored by the ECFM.               *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmMepRegisterAndGetFltStatus (tEcfmRegParams * pEcfmReg,
                                tEcfmEventNotification * pMepInfo)
{
    tEcfmCcMepInfo     *pMepCcNode = NULL;
    tEcfmLbLtMepInfo   *pMepLbLtNode = NULL;
    UINT4               u4ModId = ECFM_INIT_VAL;
    UINT4               u4EventsRegd = ECFM_INIT_VAL;

    if (pMepInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    pMepInfo->u4Event = 0;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMepRegisterAndGetFltStatus : ECFM MODULE - "
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    if (pEcfmReg == NULL)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_ALL_FAILURE_TRC,
                      "EcfmMepRegisterAndGetFltStatus : "
                      "Invalid Registration params from application\n");
        return ECFM_FAILURE;

    }
    /* Validate the AppId given by application */
    u4ModId = pEcfmReg->u4ModId;

    u4EventsRegd = pEcfmReg->u4EventsId;

    if ((u4ModId == 0) || (u4ModId >= ECFM_MAX_APPS))
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId, ECFM_ALL_FAILURE_TRC,
                      "EcfmMepRegisterAndGetFltStatus:Invalid application id "
                      "during registration\n");
        return ECFM_FAILURE;

    }

    if (u4EventsRegd == ECFM_INIT_VAL)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmMepRegisterAndGetFltStatus : No events provided "
                      "for regisration \n");
        return ECFM_FAILURE;
    }

    ECFM_LBLT_LOCK ();

    if (ECFM_LBLT_SELECT_CONTEXT (pMepInfo->u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* We are having the MdIndex, MaIndex and MepId that given by 
     * external modules so get the MepNode from the LbLt info */
    if ((pMepInfo->u4MdIndex != 0) && (pMepInfo->u4MaIndex != 0) &&
        (pMepInfo->u2MepId != 0))
    {
        pMepLbLtNode =
            EcfmLbLtUtilGetMepEntryFrmGlob (pMepInfo->u4MdIndex,
                                            pMepInfo->u4MaIndex,
                                            pMepInfo->u2MepId);

    }

    if (pMepLbLtNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmMepRegisterAndGetFltStatus : Mep Is not present "
                       "in CFM\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;

    }
    ECFM_SET_U4BIT (pMepLbLtNode->u4ModId, u4ModId);

    /* If module is already regitered do not allocate allocate memory again */
    if (ECFM_LBLT_GET_APP_INFO (u4ModId) == NULL)
    {
        /* Allocate memory for the new entry */
        if (ECFM_ALLOC_MEM_BLOCK_LBLT_REG_APP (ECFM_LBLT_GET_APP_INFO (u4ModId))
            == NULL)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                           "EcfmMepRegisterAndGetFltStatus : Unable to allocate"
                           " memory for the new registration entry\n");
            ECFM_CLEAR_U4BIT (pMepLbLtNode->u4ModId, u4ModId);
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }

        /* Update the application specific info */
        ECFM_LBLT_GET_APP_INFO (u4ModId)->pFnRcvPkt = pEcfmReg->pFnRcvPkt;

        ECFM_LBLT_GET_APP_INFO (u4ModId)->u4ModId = u4ModId;

        ECFM_LBLT_GET_APP_INFO (u4ModId)->u4EventsId = u4EventsRegd;
    }

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    ECFM_CC_LOCK ();

    if (ECFM_CC_SELECT_CONTEXT (pMepInfo->u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* We are having the MdIndex, MaIndex and MepId that given by 
     * external modules so get the MepNode from the CC info */

    if ((pMepInfo->u4MdIndex != 0) && (pMepInfo->u4MaIndex != 0) &&
        (pMepInfo->u2MepId != 0))
    {
        pMepCcNode =
            EcfmCcUtilGetMepEntryFrmGlob (pMepInfo->u4MdIndex,
                                          pMepInfo->u4MaIndex,
                                          pMepInfo->u2MepId);

    }

    if (pMepCcNode == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMepRegisterAndGetFltStatus : Mep Is not present "
                     "in CFM\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Set the Module id in the MEP specific bit map */
    ECFM_SET_U4BIT (pMepCcNode->u4ModId, u4ModId);

    /* If module is already regitered do not allocate allocate memory again */
    if (ECFM_CC_GET_APP_INFO (u4ModId) == NULL)
    {
        /* Allocate memory for the new entry */
        if (ECFM_ALLOC_MEM_BLOCK_CC_REG_APP (ECFM_CC_GET_APP_INFO (u4ModId))
            == NULL)
        {
            ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                         "EcfmMepRegisterAndGetFltStatus : Unable to allocate "
                         "memory for the new registration entry\n");
            ECFM_CLEAR_U4BIT (pMepCcNode->u4ModId, u4ModId);
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }

        /* Update the application specific info */
        ECFM_CC_GET_APP_INFO (u4ModId)->pFnRcvPkt = pEcfmReg->pFnRcvPkt;

        ECFM_CC_GET_APP_INFO (u4ModId)->u4ModId = u4ModId;

        ECFM_CC_GET_APP_INFO (u4ModId)->u4EventsId = u4EventsRegd;
    }

    /*Set the MEP usage to TRUE since this MEP is used by the
     * module ID configured in u4ModId*/

     pMepCcNode->b1MepInUse = ECFM_TRUE;

     if ((u4ModId == ECFM_Y1564_APP_ID ) ||
         (u4ModId ==  ECFM_R2544_APP_ID))
     {
         pMepCcNode->EcfmSlaParams.u4SlaId = pMepInfo->u4SlaId;
     }


    /* If any fault present on that MEP then give SF indication */
    if ((pMepCcNode->FngInfo.b1SomeRMepCcmDefect != 0) ||
        (pMepCcNode->CcInfo.b1LocalLinkFailure != 0) ||
        (pMepCcNode->CcInfo.b1MisMergeDefect != 0) ||
        (pMepCcNode->CcInfo.b1XconCcmDefect != 0) ||
        (pMepCcNode->CcInfo.b1UnExpectedLevelDefect != 0) ||
        (pMepCcNode->CcInfo.b1UnExpectedPeriodDefect != 0) ||
        (pMepCcNode->CcInfo.b1InternalHwFailure != 0))
    {
        pMepInfo->u4Event = ECFM_DEFECT_CONDITION_ENCOUNTERED;
        pMepInfo->u1Direction = pMepCcNode->u1Direction;
        if ((u4ModId == ECFM_Y1564_APP_ID ) ||
            (u4ModId ==  ECFM_R2544_APP_ID))
        {
            pMepInfo->u4SlaId = pMepCcNode->EcfmSlaParams.u4SlaId;
        }
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_SUCCESS;
    }

    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmMepDeRegister                                     
 *                                                                            
 * Description        : Routine used by protocols to de-register from ECFM.   
 *                                                                            
 * Input(s)           : tRmRegParams - Reg. params to be provided by protocols
 *                      tEcfmMepInfoParams - Pointer to Mep information        
 *                      that needs to be monitored by the ECFM.
 *                      pMepInfo - Mep information. Meg, Me and Mep Id are
 *                      passed through this structure for de-registration.
 *                                                                           
 * Output(s)          : None
 *                                                                           
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          
 *                                                                           
 *****************************************************************************/
PUBLIC INT4
EcfmMepDeRegister (tEcfmRegParams * pEcfmReg, tEcfmEventNotification * pMepInfo)
{
    tEcfmCcMepInfo     *pMepCcNode = NULL;
    tEcfmLbLtMepInfo   *pMepLbLtNode = NULL;

    if ((pEcfmReg == NULL) || (pMepInfo == NULL))
    {
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmMepDeRegister : ECFM MODULE - "
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }
    if ((pEcfmReg->u4ModId == 0) || (pEcfmReg->u4ModId >= ECFM_MAX_APPS))
    {
        ECFM_GLB_TRC (pMepInfo->u4ContextId,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmMepDeRegister : Invalid application id "
                      "during registration\n");
        return ECFM_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (pMepInfo->u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if ((pMepInfo->u4MdIndex != 0) || (pMepInfo->u4MaIndex != 0) ||
        (pMepInfo->u2MepId != 0))
    {
        pMepLbLtNode =
            EcfmLbLtUtilGetMepEntryFrmGlob (pMepInfo->u4MdIndex,
                                            pMepInfo->u4MaIndex,
                                            pMepInfo->u2MepId);
    }

    if (pMepLbLtNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmMepDeRegister : Mep Is not present ");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;

    }
    ECFM_CLEAR_U4BIT (pMepLbLtNode->u4ModId, pEcfmReg->u4ModId);

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();

    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (pMepInfo->u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    if ((pMepInfo->u4MdIndex != 0) || (pMepInfo->u4MaIndex != 0) ||
        (pMepInfo->u2MepId != 0))
    {
        pMepCcNode =
            EcfmCcUtilGetMepEntryFrmGlob (pMepInfo->u4MdIndex,
                                          pMepInfo->u4MaIndex,
                                          pMepInfo->u2MepId);
    }

    if (pMepCcNode == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmMepDeRegister: Mep Is not present in CFM\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    if ((pMepCcNode->u4ModId == ECFM_Y1564_APP_ID ) ||
        (pMepCcNode->u4ModId ==  ECFM_R2544_APP_ID))
    {
        pMepCcNode->EcfmSlaParams.u4SlaId = 0;
    }

    ECFM_CLEAR_U4BIT (pMepCcNode->u4ModId, pEcfmReg->u4ModId);

    /*Reset the MEP usage to ECFM_FALSE*/
    pMepCcNode->b1MepInUse = ECFM_FALSE;

    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();

    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    :  EcfmSetRdiCapability
 * 
 * DESCRIPTION      : API Function to set RDI capability in CCM frames.
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    u1Enable - Enable/Disable RDI capability
 *                    u1Interval - Interval between frames
 *                    u4Period - Period for which RDI capability be present
 *
 * OUTPUT           : None 
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
EcfmSetRdiCapability (tEcfmMepInfoParams * pEcfmMepInfoParams, UINT1 u1Enable,
                      UINT1 u1Interval, UINT4 u4Period)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    pMaNode = EcfmSnmpLwGetMaEntry (pEcfmMepInfoParams->u4MdIndex,
                                    pEcfmMepInfoParams->u4MaIndex);
    if (pMaNode == NULL)
    {
        return ECFM_FAILURE;
    }

    if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {

        if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                                &u4ContextId,
                                                &u2LocalPort) !=
            ECFM_VCM_SUCCESS)
        {
            return ECFM_FAILURE;
        }
    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {
        pMepNode = EcfmCcUtilGetMepEntryFrmPort (pEcfmMepInfoParams->u1MdLevel,
                                                 pEcfmMepInfoParams->
                                                 u4VlanIdIsid, u2LocalPort,
                                                 pEcfmMepInfoParams->
                                                 u1Direction);
    }
    else
    {
        pMepNode = EcfmCcUtilGetMepEntryFrmGlob (pEcfmMepInfoParams->u4MdIndex,
                                                 pEcfmMepInfoParams->u4MaIndex,
                                                 pEcfmMepInfoParams->u2MepId);
    }
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetRdiCapability: MEP does not exist at CC Task \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetRdiCapability: Y.1731 is not enabled for"
                     "this port \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Validate Rdi Period value, to be set */
    if (u4Period > ECFM_CC_RDI_PERIOD_MAX)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetRdiCapability: Incorrect RDI Period\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Setting CC Interval to default value when passed as optional. */
    if (u1Interval == ECFM_INIT_VAL)
    {
        u1Interval = ECFM_CCM_INTERVAL_1_S;
    }

    /* Configure the values for setting the RDI Capability */
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    pMepNode->CcInfo.u1RdiCapability = u1Enable;
    pMepNode->pMaInfo->u1CcmInterval = u1Interval;
    pMepNode->CcInfo.u4RdiCapPeriod = u4Period;
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmGetLmCounters 
 * 
 * DESCRIPTION      : API Function to get Loss measurement counters
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    pu4TxFCf - Value of the local counter TxFCl at the time of 
 *                    transmission of the LM frame.
 *                    pu4TxFCb - Value of TxFCb in the last received LM frame 
 *                    from the peer MEP.
 *                    pu4RxFCb - Value of the local counter RxFCl at the time of
 *                    reception of the last CCM frame from the peer MEP.
 *
 * OUTPUT           : None 
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
EcfmGetLmCounters (tEcfmMepInfoParams * pEcfmMepInfoParams, UINT4 *pu4TxFCf,
                   UINT4 *pu4TxFCb, UINT4 *pu4RxFCb)
{
    tEcfmCcMepInfo     *pCcMepNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }
    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        return ECFM_FAILURE;
    }
    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Get MEP node from portInfo's MepInfoTree */
    pCcMepNode = EcfmCcUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);

    if (pCcMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcGetLmCounters: MEP does not exist at CC Task \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Fetch the Loss Measurement Counters */
    *pu4TxFCf = pCcMepNode->LmInfo.u4PreTxFCf;
    *pu4TxFCb = pCcMepNode->LmInfo.u4PreTxFCb;
    *pu4RxFCb = pCcMepNode->LmInfo.u4PreRxFCb;
    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmConfigLm 
 * 
 * DESCRIPTION      : API Function to configure Loss measurement parameters
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    u1Status - Status of Loss Measurement
 *                    DestMacAddr - Address of the remote MEP
 *                    u1Interval - Interval between 2 LM messages
 *                    u2NumObservation - No of LM frames that should be sent
 *                    u4Deadline - Duration for LM to take place
 *
 * OUTPUT           : None 
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 **************************************************************************/
INT4
EcfmConfigLm (tEcfmMepInfoParams * pEcfmMepInfoParams, tMacAddr DestMacAddr,
              UINT1 u1Interval, UINT2 u2NumObservation, UINT4 u4Deadline)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPortInfo    *pPortInfo = NULL;
    tEcfmCcLmInfo      *pLmInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmConfigLm: MEP does not exist at CC Task \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmConfigLm: Y.1731 is not enabled for"
                     "this port \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP is in a state to Initiate or Stop a transaction */
    if (pMepNode->b1Active != ECFM_TRUE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmConfigLm: MEP is not Active\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP's Port is in a state to transmit LM(s) */
    pPortInfo = ECFM_CC_GET_PORTINFO_FROM_MEP (pMepNode);
    if (ECFM_CC_IS_PORT_MODULE_STS_ENABLED (pPortInfo->u2PortNum) == ECFM_FALSE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmConfigLm: Port cant transmit LM frames\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    pLmInfo = ECFM_CC_GET_LMINFO_FROM_MEP (pMepNode);
    if (pLmInfo->u1TxLmmStatus != ECFM_TX_STATUS_READY)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmConfigLm: Already one transaction is going on \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Set Lmm interval to default value, if sent as optional from outside */
    if (u1Interval == ECFM_INIT_VAL)
    {
        u1Interval = ECFM_CC_LMM_INTERVAL_100_Ms;
    }

    /* Set the desired parameters */
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    ECFM_MEMCPY (pLmInfo->TxLmmDestMacAddr,
                 DestMacAddr, (ECFM_MAC_ADDR_LENGTH));
    pLmInfo->u2TxLmmInterval = u1Interval;
    pLmInfo->u2TxLmmMessages = u2NumObservation;
    pLmInfo->u4TxLmmDeadline = u4Deadline;
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

    /* Initiate LM transaction and send event to LM Initiator for the same */
    if (EcfmCcUtilPostTransaction (pMepNode, ECFM_LM_START_TRANSACTION) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmConfigLm: Not able to post the msg in CC queue\r\n");
        pLmInfo->b1ResultOk = ECFM_FALSE;
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSetLmThreshold 
 * 
 * DESCRIPTION      : API Function to configure Threshold for Loss measurement
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    u4NearEndThreshold - Threshold value for LM for ingress 
 *                    frames.
 *                    u4FarEndThreshold - Threshold value for LM for egress 
 *                    frames
 *
 * OUTPUT           : None 
 *
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
EcfmSetLmThreshold (tEcfmMepInfoParams * pEcfmMepInfoParams,
                    UINT4 u4NearEndThreshold, UINT4 u4FarEndThreshold)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetLmThreshold: MEP does not exist at CC Task \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetLmThreshold: Y.1731 is not enabled "
                     "for this port \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /*Check the LmmTransaction Status */
    if (pMepNode->LmInfo.u1TxLmmStatus == ECFM_TX_STATUS_NOT_READY)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetLmThreshold: A transaction is going on \r\n");
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Configure the threshold value for Loss Measurement */
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    pMepNode->LmInfo.u4NearEndFrmLossThreshold = u4NearEndThreshold;
    pMepNode->LmInfo.u4FarEndFrmLossThreshold = u4FarEndThreshold;
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSetAisCapability 
 * 
 * DESCRIPTION      : This API is called by the modules that want to configure 
 *                    AIS capability.
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    u1Enable -  indicating whether to enable or disable AIS
 *                    u1Interval - Time Interval between succcessive AIS frames
 *                    u4Period - Duration for which AIS capability is ON
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCESS/ECFM_FAILURE 
 *
 **************************************************************************/
PUBLIC INT4
EcfmSetAisCapability (tEcfmMepInfoParams * pEcfmMepInfoParams, UINT1 u1Enable,
                      UINT1 u1Interval, UINT4 u4Period)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    ECFM_CC_LOCK ();
    pMaNode = EcfmSnmpLwGetMaEntry (pEcfmMepInfoParams->u4MdIndex,
                                    pEcfmMepInfoParams->u4MaIndex);
    if (pMaNode == NULL)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {
        if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                                &u4ContextId,
                                                &u2LocalPort) !=
            ECFM_VCM_SUCCESS)
        {
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
    }

    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (ECFM_CC_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (!((pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
          (pMaNode->u1SelectorType == ECFM_SERVICE_SELECTION_PW)))
    {
        pMepNode = EcfmCcUtilGetMepEntryFrmPort (pEcfmMepInfoParams->u1MdLevel,
                                                 pEcfmMepInfoParams->
                                                 u4VlanIdIsid, u2LocalPort,
                                                 pEcfmMepInfoParams->
                                                 u1Direction);
    }
    else
    {
        pMepNode = EcfmCcUtilGetMepEntryFrmGlob (pEcfmMepInfoParams->u4MdIndex,
                                                 pEcfmMepInfoParams->u4MaIndex,
                                                 pEcfmMepInfoParams->u2MepId);
    }
    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmSetAisCapability: MEP does not exist at CC Task \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmSetAisCapability: Y.1731 is not enabled "
                     "for this port \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepNode);

    /* Setting u1Interval as default value when sent as optional */

    if (u1Interval == ECFM_INIT_VAL)
    {
        u1Interval = ECFM_CC_AIS_LCK_INTERVAL_1_SEC;
    }

    if ((u1Interval != ECFM_CC_AIS_LCK_INTERVAL_1_SEC) &&
        (u1Interval != ECFM_CC_AIS_LCK_INTERVAL_1_MIN))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmSetAisCapability: Incorrect AIS Interval\r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (u1Enable == ECFM_ENABLE)
    {
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        pAisInfo->u1AisCapability = u1Enable;
        pAisInfo->u1AisInterval = u1Interval;
        pAisInfo->u4AisPeriod = u4Period;
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
        if (EcfmCcAisCapabilityConfiguration (pMepNode) != ECFM_SUCCESS)
        {
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        pAisInfo->u1AisCapability = u1Enable;
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
        if (EcfmCcUtilPostTransaction (pMepNode, ECFM_AIS_STOP_TRANSACTION) ==
            ECFM_FAILURE)
        {
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
    }

    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSetOutOfService 
 * 
 * DESCRIPTION      : This API is called by the modules that want to configure 
 *                    a MEP to perform Out of Service operations.
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    b1Enable - Enable/Disable Out of service config for MEP
 *                    u1Interval - Interval between successive LCK frames
 *                    u4Period - Duration for which MEP remains in this condition
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCESS/ECFM_FAILURE 
 *
 **************************************************************************/
PUBLIC INT4
EcfmSetOutOfService (tEcfmMepInfoParams * pEcfmMepInfoParams, BOOL1 b1Enable,
                     UINT1 u1Interval, UINT4 u4Period)
{

    tEcfmCcMepInfo     *pMepNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        return ECFM_FAILURE;
    }

    ECFM_CC_LOCK ();
    if (ECFM_CC_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);

    if (pMepNode == NULL)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetOutOfService: MEP does not exist at CC Task \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_CC_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC,
                     "EcfmCcSetOutOfService: Y.1731 is not enabled "
                     "for this port \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Setting LckInterval to default value, if called as optional field */
    if (u1Interval == ECFM_INIT_VAL)
    {
        u1Interval = ECFM_CC_AIS_LCK_INTERVAL_1_SEC;
    }

    if (b1Enable == ECFM_FALSE)
    {
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        pMepNode->LckInfo.b1LckCondition = b1Enable;
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
        /* Stop Transmitting LCK PDUs */
        if (EcfmCcUtilPostTransaction (pMepNode, ECFM_LCK_STOP_TRANSACTION) ==
            ECFM_FAILURE)
        {
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        pMepNode->LckInfo.b1LckCondition = b1Enable;
        pMepNode->LckInfo.u1LckInterval = u1Interval;
        pMepNode->LckInfo.u4LckPeriod = u4Period;
        pMepNode->LckInfo.b1OutOfService = ECFM_TRUE;
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
        /* Start Transmitting LCK PDUs */
        if (EcfmCcUtilPostTransaction (pMepNode, ECFM_LCK_START_TRANSACTION) ==
            ECFM_FAILURE)
        {
            ECFM_CC_RELEASE_CONTEXT ();
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
    }

    ECFM_CC_RELEASE_CONTEXT ();
    ECFM_CC_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmInitiateExPdu 
 * 
 * DESCRIPTION      : This API is called by the modules that want to initiate
 *                    External PDUs
 *
 * INPUT            : pMepInfo - Pointer to structure having 
 *                    information to select a MEP node
 *                    u1TlvOffset - TLV offset for the PDU
 *                    u1SubOpCode - SubOpCode for the External PDU
 *                    pu1PduData - Pointer to External data
 *                    u4PduDataLen - Length of External data
 *                    TxDestMacAddr- Destination Mac Address
 *                    u1OpCode - Opcode of the PDU to be sent
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC INT4
EcfmInitiateExPdu (tEcfmMepInfoParams * pMepInfo, UINT1 u1TlvOffset,
                   UINT1 u1SubOpCode, UINT1 *pu1PduData, UINT4 u4PduDataLen,
                   tMacAddr TxDestMacAddr, UINT1 u1OpCode)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtRMepDbInfo *pRMepNode = NULL;
    tEcfmLbLtRMepDbInfo RMepInfo;
    tExternalPduMsg     Msg;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    const tMacAddr      NullMac = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }
    ECFM_MEMSET (&Msg, ECFM_INIT_VAL, sizeof (tExternalPduMsg));

    if (pMepInfo == NULL)
    {
        return ECFM_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (pMepInfo->u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pMepInfo->u4ContextId))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmInitiateExPdu: System is not Up\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* if this API is called from ELPS then we need to check these three 
     * values */
    if ((pMepInfo->u4MdIndex != 0) && (pMepInfo->u4MaIndex != 0) &&
        (pMepInfo->u2MepId != 0))
    {
        pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (pMepInfo->u4MdIndex,
                                                   pMepInfo->u4MaIndex,
                                                   pMepInfo->u2MepId);
        if (pMepNode != NULL)
        {
            if (MEMCMP (TxDestMacAddr, NullMac, ECFM_MAC_ADDR_LENGTH) != 0)
            {
                ECFM_MEMCPY (&Msg.TxDestMacAddr, TxDestMacAddr,
                             ECFM_MAC_ADDR_LENGTH);
            }
            else
            {
                RMepInfo.u4MdIndex = pMepInfo->u4MdIndex;
                RMepInfo.u4MaIndex = pMepInfo->u4MaIndex;
                RMepInfo.u2MepId = pMepInfo->u2MepId;
                RMepInfo.u2RMepId = 0;

                pRMepNode = RBTreeGetNext (ECFM_LBLT_RMEP_DB_TABLE,
                                           (tRBElem *) & RMepInfo, NULL);
                if (pRMepNode != NULL)
                {
                    if (MEMCMP (pRMepNode->RMepMacAddr, NullMac,
                                ECFM_MAC_ADDR_LENGTH) == 0)
                    {
                        /* Remote MEP MAC address is not learnt yet */
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                                       "EcfmInitiateExPdu: Remote MEP Mac"
                                       "address is not learnt\r\n");
                        ECFM_LBLT_RELEASE_CONTEXT ();
                        ECFM_LBLT_UNLOCK ();
                        return ECFM_FAILURE;
                    }

                    ECFM_MEMCPY (&Msg.TxDestMacAddr, pRMepNode->RMepMacAddr,
                                 ECFM_MAC_ADDR_LENGTH);
                }
                else
                {
                    ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                                   "EcfmInitiateExPdu: Remote MEP is"
                                   "not Active\r\n");
                    ECFM_LBLT_RELEASE_CONTEXT ();
                    ECFM_LBLT_UNLOCK ();
                    return ECFM_FAILURE;
                }
            }
        }
    }
    else
    {
        if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pMepInfo->u4IfIndex,
                                                &u4ContextId,
                                                &u2LocalPort)
            != ECFM_VCM_SUCCESS)
        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
        /* Get MEP node from portInfo's MepInfoTree */
        pMepNode = EcfmLbLtUtilGetMepEntryFrmPort (pMepInfo->u1MdLevel,
                                                   pMepInfo->u4VlanIdIsid,
                                                   u2LocalPort,
                                                   pMepInfo->u1Direction);
        ECFM_MEMCPY (Msg.TxDestMacAddr, TxDestMacAddr, ECFM_MAC_ADDR_LENGTH);
    }
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmInitiateExPdu: MEP does not exist at LBLT Task \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmInitiateExPdu: Y.1731 is not enabled for"
                       "this port \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP is in a state to Initiate or Stop a transaction */
    if (pMepNode->b1Active != ECFM_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmInitiateExPdu: MEP is not Active\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    Msg.u2PortNum = pMepNode->u2PortNum;
    Msg.u4VidIsid = pMepNode->u4PrimaryVidIsid;
    Msg.u1MdLevel = pMepNode->u1MdLevel;
    Msg.u1Direction = pMepNode->u1Direction;
    Msg.pu1Data = pu1PduData;
    Msg.u4DataLen = u4PduDataLen;
    Msg.u1TlvOffset = u1TlvOffset;
    Msg.u1SubOpCode = u1SubOpCode;
    switch (u1OpCode)
    {
        case ECFM_OPCODE_RAPS:

            Msg.u4VidIsid = pMepInfo->u4VlanIdIsid;
            Msg.u1Version = pMepInfo->u1Version;

            /* Intentional Fall through */
        case ECFM_OPCODE_MCC:
        case ECFM_OPCODE_APS:
        case ECFM_OPCODE_EXM:
        case ECFM_OPCODE_EXR:
        case ECFM_OPCODE_VSM:
        case ECFM_OPCODE_VSR:

            if (EcfmLbLtClntXmitExPdu (&Msg, u1OpCode) != ECFM_SUCCESS)
            {
                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();
                return ECFM_FAILURE;
            }
            break;
        default:
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                           "EcfmInitiateExPdu: Incorrect Opcode Rx\r\n");
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmConfigDm 
 * 
 * DESCRIPTION      : This API is called by the modules that want to configure 
 *                    Delay Measurement Capability.
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    u1Status - Status of Delay Measurement
 *                    DestMacAddr - Detination MAC address
 *                    u1Type - Type of Delay Measurement (1DM/2DM)
 *                    u2Interval - Interval for DM
 *                    u4NumObservation - No of DM PDU
 *                    u4Deadline - Deadline for DM
 *                    u1VlanPriority - Vlan priority for DM 
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCESS/ECFM_FAILURE 
 *
 **************************************************************************/
PUBLIC INT4
EcfmConfigDm (tEcfmMepInfoParams * pEcfmMepInfoParams, UINT1 u1Status,
              tMacAddr DestMacAddr, UINT1 u1Type, UINT2 u2Interval,
              UINT4 u4NumObservation, UINT4 u4Deadline, UINT1 u1VlanPriority)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigDm: System is not Up\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigDm: MEP does not exist at LBLT Task \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigDm: Y.1731 is not enabled for"
                       "this port \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP is in a state to Initiate or Stop a transaction */
    if (pMepNode->b1Active != ECFM_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigDm: MEP is not Active\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP's Port is in a state to transmit LM(s) */
    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pMepNode);
    if (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (pPortInfo->u2PortNum) ==
        ECFM_FALSE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigDm: Port cant transmit DM frames\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    pDmInfo = ECFM_LBLT_GET_DMINFO_FROM_MEP (pMepNode);
    if (pDmInfo->u1DmStatus != ECFM_TX_STATUS_READY)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigDm: Already one transaction is going on \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Setting u1Type and u2Interval to default value if passed as optional */
    if (u1Type == ECFM_INIT_VAL)
    {
        u1Type = ECFM_LBLT_DM_TYPE_DMM;
    }

    if (u2Interval == ECFM_INIT_VAL)
    {
        u1Type = ECFM_DM_INTERVAL_DEF_VAL;
    }

    if (u1Status == ECFM_TX_STATUS_STOP)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_DM_STOP_TRANSACTION) ==
            ECFM_FAILURE)
        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
        pMepNode->DmInfo.u1DmStatus = u1Status;
    }
    else
    {
        ECFM_MEMCPY (pMepNode->DmInfo.TxDmDestMacAddr,
                     DestMacAddr, (ECFM_MAC_ADDR_LENGTH));
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        pDmInfo->u1TxDmType = u1Type;
        pMepNode->DmInfo.u1DmStatus = u1Status;
        pDmInfo->u2TxDmInterval = u2Interval;
        pDmInfo->u2TxNoOfMessages = (UINT2) u4NumObservation;
        pDmInfo->u4TxDmDeadline = (UINT2) u4Deadline;
        if (u1Type == ECFM_LBLT_DM_TYPE_1DM)
        {
            pDmInfo->u1Tx1DmVlanPriority = u1VlanPriority;
        }
        else
        {
            pDmInfo->u1TxDmmVlanPriority = u1VlanPriority;
        }
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_DM_START_TRANSACTION) !=
            ECFM_SUCCESS)
        {
            pDmInfo->b1ResultOk = ECFM_FALSE;
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
    }
    pDmInfo->b1ResultOk = ECFM_TRUE;
    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmSetDmThreshold 
 * 
 * DESCRIPTION      : This API is called by the modules that want to configure 
 *                    Threshold value for Delay measurement
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having information to
 *                    select a MEP node
 *                    u4Threshold - Threshold value of DM
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCESS/ECFM_FAILURE 
 *
 **************************************************************************/
PUBLIC INT4
EcfmSetDmThreshold (tEcfmMepInfoParams * pEcfmMepInfoParams, UINT4 u4Threshold)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }

    if (pEcfmMepInfoParams == NULL)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmSetDmThreshold: System is not Up\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmSetDmThreshold: MEP does not exist at LBLT Task \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmSetDmThreshold: Y.1731 is not enabled for"
                       "this port \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Configure the Threshold Valur for Delay Measurement */
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
    pMepNode->DmInfo.u4FrmDelayThreshold = u4Threshold;
    pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmConfigTst 
 * 
 * DESCRIPTION      : This API is called by the modules that want to configure 
 *                    TST capability.
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    pEcfmConfigTstInfo - Structure having information for 
 *                    configuring TST Parameters
 *
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCESS/ECFM_FAILURE 
 *
 **************************************************************************/
PUBLIC INT4
EcfmConfigTst (tEcfmMepInfoParams * pEcfmMepInfoParams,
               tEcfmConfigTstInfo * pEcfmConfigTstInfo)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tMacAddr            TempMacAddr;
    tMacAddr            DestMacAddr;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT4               u4NumMessages = ECFM_INIT_VAL;
    UINT4               u4Deadline = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2TstPatterSize = ECFM_INIT_VAL;
    UINT1               u1Status = ECFM_INIT_VAL;
    UINT1               u1TstPatterType = ECFM_INIT_VAL;
    BOOL1               b1VariableByte = ECFM_FALSE;
    BOOL1               b1DropEnable = ECFM_FALSE;

    if ((pEcfmMepInfoParams == NULL) || (pEcfmConfigTstInfo == NULL))
    {
        return ECFM_FAILURE;
    }
    u1Status = pEcfmConfigTstInfo->u1Status;
    ECFM_MEMCPY (DestMacAddr, pEcfmConfigTstInfo->DestMacAddr,
                 (ECFM_MAC_ADDR_LENGTH));
    u1TstPatterType = pEcfmConfigTstInfo->u1TstTlvPatterType;
    u2TstPatterSize = pEcfmConfigTstInfo->u2TstTlvPatterSize;
    u4Deadline = pEcfmConfigTstInfo->u4Deadline;
    b1VariableByte = pEcfmConfigTstInfo->b1VariableByte;
    u4NumMessages = pEcfmConfigTstInfo->u4TxTstMessages;
    b1DropEnable = pEcfmConfigTstInfo->b1DropEnable;
    u4Interval = pEcfmConfigTstInfo->u4TstInterval;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtConfigTst: System is not Up\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: MEP does not exist at LBLT Task \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Y.1731 is not enabled for"
                       "this port \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP is in a state to Initiate or Stop a transaction */
    if (pMepNode->b1Active != ECFM_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: MEP is not Active\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP's Port is in a state to transmit TST */
    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pMepNode);
    if (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (pPortInfo->u2PortNum) ==
        ECFM_FALSE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Port cant transmit TST frames\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (u1TstPatterType > ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect Pattern Type\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (u2TstPatterSize > ECFM_LBM_TEST_PATTERN_SIZE_MAX)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect Pattern Size \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Validate MacAddress value that is to be set */
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if (ECFM_MEMCMP (DestMacAddr, TempMacAddr, ECFM_MAC_ADDR_LENGTH) ==
        ECFM_INIT_VAL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect Destination address\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* It should be the unicast mac address */
    if (ECFM_IS_MULTICAST_ADDR (DestMacAddr) == ECFM_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect Destination Type\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (u4Interval == ECFM_INIT_VAL)
    {
        u4Interval = ECFM_TST_INTERVAL_DEF_VAL;
    }

    if (u4NumMessages > ECFM_TST_MESSAGE_MAX)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect No of Messages \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (u1Status == ECFM_TX_STATUS_STOP)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_TST_STOP_TRANSACTION)
            == ECFM_FAILURE)
        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
        pMepNode->TstInfo.u1TstStatus = u1Status;
    }
    else
    {
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        pMepNode->TstInfo.u1TstPatternType = u1TstPatterType;
        pMepNode->TstInfo.u2TstPatternSize = u2TstPatterSize;
        pMepNode->TstInfo.u1TstStatus = u1Status;
        pMepNode->TstInfo.b1TstVariableBytes = b1VariableByte;
        pMepNode->TstInfo.u4TstInterval = u4Interval;
        pMepNode->TstInfo.u4TstMessages = u4NumMessages;
        pMepNode->TstInfo.u4TstDeadLine = u4Deadline;
        pMepNode->TstInfo.b1TstDropEligible = b1DropEnable;
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;

        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_TST_START_TRANSACTION)
            != ECFM_SUCCESS)
        {
            pMepNode->TstInfo.b1TstResultOk = ECFM_FALSE;
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
    }
    pMepNode->TstInfo.b1TstResultOk = ECFM_TRUE;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : EcfmConfigLbm 
 * 
 * DESCRIPTION      : This API is called by the modules that want to configure 
 *                   LBcapability.
 *
 * INPUT            : pEcfmMepInfoParams - Pointer to structure having 
 *                    information to select a MEP node
 *                    pEcfmConfigLbmInfo - Pointer to structure having information for 
 *                    configuring LB Parameters
 * OUTPUT           : None
 *
 * RETURNS          : ECFM_SUCESS/ECFM_FAILURE 
 *
 **************************************************************************/
PUBLIC INT4
EcfmConfigLbm (tEcfmMepInfoParams * pEcfmMepInfoParams,
               tEcfmConfigLbmInfo * pEcfmConfigLbmInfo)
{

    tEcfmLbLtMepInfo   *pMepNode = NULL;
    tMacAddr            TempMacAddr;
    tEcfmLbLtPortInfo  *pPortInfo = NULL;
    tMacAddr            DestMacAddr;
    UINT4               u4DataTlvSize = ECFM_INIT_VAL;
    UINT4               u4Deadline = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;
    UINT2               u2TstTlvPatterSize = ECFM_INIT_VAL;
    UINT2               u2NumMessages = ECFM_INIT_VAL;
    UINT1             **ppu1DataTlvData = NULL;
    UINT1               u1Status = ECFM_INIT_VAL;
    UINT1               u1TstTlvPatterType = ECFM_INIT_VAL;
    UINT1               u1TlvOrNone = ECFM_INIT_VAL;
    BOOL1               b1VariableByte = ECFM_FALSE;
    BOOL1               b1DropEnable = ECFM_FALSE;

    if ((pEcfmMepInfoParams == NULL) || (pEcfmConfigLbmInfo == NULL))
    {
        return ECFM_FAILURE;
    }
    /* Store the Configured LBM Parameters */
    u1Status = pEcfmConfigLbmInfo->u1Status;
    ECFM_MEMCPY (DestMacAddr, pEcfmConfigLbmInfo->DestMacAddr,
                 (ECFM_MAC_ADDR_LENGTH));
    u1TstTlvPatterType = pEcfmConfigLbmInfo->u1TstTlvPatterType;
    u2TstTlvPatterSize = pEcfmConfigLbmInfo->u2TstTlvPatterSize;
    u1TlvOrNone = pEcfmConfigLbmInfo->u1TlvOrNone;
    ppu1DataTlvData = &(pEcfmConfigLbmInfo->pu1DataTlvData);
    u4DataTlvSize = pEcfmConfigLbmInfo->u4DataTlvSize;
    u4Deadline = pEcfmConfigLbmInfo->u4Deadline;
    b1VariableByte = pEcfmConfigLbmInfo->b1VariableByte;
    u2NumMessages = pEcfmConfigLbmInfo->u2TxLbmMessages;
    b1DropEnable = pEcfmConfigLbmInfo->b1DropEnable;
    u4Interval = pEcfmConfigLbmInfo->u4LbmInterval;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        return ECFM_FAILURE;
    }
    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (pEcfmMepInfoParams->u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }

    ECFM_LBLT_LOCK ();
    if (ECFM_LBLT_SELECT_CONTEXT (pEcfmMepInfoParams->u4ContextId) !=
        ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* First check system status */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmMepInfoParams->u4ContextId))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmLbLtConfigLb: System is not Up\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Get MEP node from portInfo's MepInfoTree */
    pMepNode = EcfmLbLtUtilGetMepEntryFrmPort
        (pEcfmMepInfoParams->u1MdLevel,
         pEcfmMepInfoParams->u4VlanIdIsid,
         u2LocalPort, pEcfmMepInfoParams->u1Direction);
    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: MEP does not exist at LBLT Task \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check the Y.1731 Status */
    if (ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT (pMepNode->u2PortNum))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Y.1731 is not enabled for"
                       "this port \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP is in a state to Initiate or Stop a transaction */
    if (pMepNode->b1Active != ECFM_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: MEP is not Active\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check if MEP's Port is in a state to transmit TST */
    pPortInfo = ECFM_LBLT_GET_PORTINFO_FROM_MEP (pMepNode);
    if (ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED (pPortInfo->u2PortNum) ==
        ECFM_FALSE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Port cant transmit TST frames\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (u1TstTlvPatterType > ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect Pattern Type\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    if (u2TstTlvPatterSize > ECFM_LBM_TEST_PATTERN_SIZE_MAX)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigTst: Incorrect Pattern Size \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    /* Setting u2TstTlvPatternSize to default value, if passed as */
    /* optional parameter */
    if (u2TstTlvPatterSize == ECFM_INIT_VAL)
    {
        u2TstTlvPatterSize = ECFM_LBM_TEST_PATTERN_SIZE_MIN;
    }
    /* Setting u4Interval to default value(1 s) if passed as */
    /* optional parameter */
    if (u4Interval == ECFM_INIT_VAL)
    {
        u4Interval = ECFM_LBLT_LB_INTERVAL_USEC;
    }

    /* Validate MacAddress value that is to be set */
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    if (ECFM_MEMCMP (DestMacAddr, TempMacAddr, ECFM_MAC_ADDR_LENGTH) ==
        ECFM_INIT_VAL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect Destination address\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* It should be the unicast mac address */
    if (ECFM_IS_MULTICAST_ADDR (DestMacAddr) == ECFM_TRUE)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect Destination Type\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* It should not be MEP's own Mac Addreess */
    if (ECFM_LBLT_GET_PORT_INFO (pMepNode->u2PortNum) == NULL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: port info not found\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;

    }
    ECFM_GET_MAC_ADDR_OF_PORT (ECFM_LBLT_PORT_INFO
                               (pMepNode->u2PortNum)->u4IfIndex, TempMacAddr);
    if (ECFM_MEMCMP
        (TempMacAddr, DestMacAddr, ECFM_MAC_ADDR_LENGTH) == ECFM_INIT_VAL)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect Destination Address\r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Check for the validity of Data pattern Size */
    if (u4DataTlvSize > ECFM_LBM_DATA_PATTERN_SIZE_MAX)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect Data Pattern Size \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Test for No of LBM PDUs to be transmitted */
    if (u2NumMessages > Y1731_LBM_MESSAGE_MAX)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect Number of LBMs to send \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Test for the configured interval */
    if ((u4Interval < ECFM_LB_INTERVAL_MIN) ||
        (u4Interval > ECFM_LB_INTERVAL_MAX))
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect LBM interval \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    /* Test for LBM deadline */
    if (u4Deadline > ECFM_LB_DEADLINE_MAX)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect LBM deadline \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (u1TlvOrNone > ECFM_LBLT_LBM_WITH_TEST_TLV)
    {
        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                       "EcfmConfigLbm: Incorrect LBM type to be include in LBB frame \r\n");
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }

    if (u1Status == ECFM_TX_STATUS_STOP)
    {
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_LB_STOP_TRANSACTION) ==
            ECFM_FAILURE)
        {
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
        pMepNode->LbInfo.u1TxLbmStatus = u1Status;
    }
    else
    {
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_NOT_IN_SERVICE;
        ECFM_MEMCPY (pMepNode->LbInfo.TxLbmDestMacAddr,
                     DestMacAddr, (ECFM_MAC_ADDR_LENGTH));
        pMepNode->LbInfo.u1TxLbmTstPatternType = u1TstTlvPatterType;
        pMepNode->LbInfo.u1TxLbmStatus = u1Status;
        pMepNode->LbInfo.u1TxLbmTlvOrNone = u1TlvOrNone;
        pMepNode->LbInfo.u2TxLbmPatternSize = u2TstTlvPatterSize;
        pMepNode->LbInfo.TxLbmDataTlv.pu1Octets = *ppu1DataTlvData;
        pMepNode->LbInfo.TxLbmDataTlv.u4OctLen = u4DataTlvSize;
        pMepNode->LbInfo.b1TxLbmVariableBytes = b1VariableByte;
        pMepNode->LbInfo.u4TxLbmInterval = u4Interval;
        pMepNode->LbInfo.u2TxLbmMessages = u2NumMessages;
        pMepNode->LbInfo.u4TxLbmDeadline = u4Deadline;
        pMepNode->LbInfo.b1TxLbmDropEligible = b1DropEnable;
        pMepNode->u1RowStatus = ECFM_ROW_STATUS_ACTIVE;
        if (EcfmLbLtUtilPostTransaction (pMepNode, ECFM_LB_START_TRANSACTION) !=
            ECFM_SUCCESS)
        {
            pMepNode->LbInfo.b1TxLbmResultOk = ECFM_FALSE;
            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            return ECFM_FAILURE;
        }
    }
    pMepNode->LbInfo.b1TxLbmResultOk = ECFM_TRUE;

    ECFM_LBLT_RELEASE_CONTEXT ();
    ECFM_LBLT_UNLOCK ();
    return ECFM_SUCCESS;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : EcfmCcAndLbLtStartModule
 *
 *    DESCRIPTION      : This is an API to start the CC and LBLT modules
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcAndLbLtStartModule ()
{
    if (ECFM_TAKE_SEMAPHORE (ECFM_CC_SEM_ID) == OSIX_FAILURE)
    {
        return ECFM_FAILURE;
    }
    if (EcfmHandleCcAndLbLtStartModule () == ECFM_FAILURE)
    {
        ECFM_GIVE_SEMAPHORE (ECFM_CC_SEM_ID);
        return ECFM_FAILURE;
    }
    ECFM_GIVE_SEMAPHORE (ECFM_CC_SEM_ID);
    return ECFM_SUCCESS;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : EcfmCcAndLbLtShutDownModule
 *
 *    DESCRIPTION      : This function deallocates memory pools for all tables
 *                       in CC and LBLT Task . It also deinitalizes the 
 *                       global structure.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EcfmCcAndLbLtShutDownModule ()
{
    tEcfmCcMsg         *pCcQMsg = NULL;
    tEcfmLbLtMsg       *pLbLtQMsg = NULL;
    UINT4               u4ContextId = 0;
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmCcAndLbLtShutDownModule: Shuting down ECFM .... \r\n");
    ECFM_CC_LOCK ();
    ECFM_CC_INITIALISED () = ECFM_FALSE;
    ECFM_LBLT_INITIALISED () = ECFM_FALSE;
    /* Delete all context, leaving default context default context will be
     * deleted after the loop*/
    for (u4ContextId = ECFM_DEFAULT_CONTEXT + 1;
         u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)
    {
        if (EcfmCcIsContextExist (u4ContextId) != ECFM_TRUE)
        {
            continue;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            continue;
        }
        EcfmUtilModuleShutDown ();
        ECFM_CC_RELEASE_CONTEXT ();

        EcfmCcHandleDeleteContext (u4ContextId);
        ECFM_LBLT_HANDLE_DELETE_CONTEXT (u4ContextId);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcAndLbLtShutDownModule: .... ECFM shutdown for context %d completed \r\n",
                      u4ContextId);
    }
    /* Delete the default context */
    u4ContextId = ECFM_DEFAULT_CONTEXT;
    ECFM_CC_SELECT_CONTEXT (u4ContextId);
    EcfmUtilModuleShutDown ();
    ECFM_CC_RELEASE_CONTEXT ();

    EcfmCcHandleDeleteContext (u4ContextId);
    ECFM_LBLT_HANDLE_DELETE_CONTEXT (u4ContextId);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmCcAndLbLtShutDownModule: .... ECFM shutdown for context %d completed \r\n",
                  u4ContextId);
    while (ECFM_DEQUE_MSG (ECFM_CC_PKT_QUEUE_ID, (UINT1 *) &pCcQMsg,
                           ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)
    {
        EcfmCcCtrlRxPktFree (pCcQMsg->uMsg.pEcfmPdu);
        pCcQMsg->uMsg.pEcfmPdu = NULL;
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pCcQMsg);
        pCcQMsg = NULL;
    }
    while (ECFM_DEQUE_MSG (ECFM_CC_CFG_QUEUE_ID, (UINT1 *) &pCcQMsg,
                           ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)
    {
        switch (pCcQMsg->MsgType)
        {
            case ECFM_CREATE_PORT_MSG:
                L2_SYNC_GIVE_SEM ();
                break;
            case ECFM_MAP_PORT_MSG:
            case ECFM_UNMAP_PORT_MSG:
            case ECFM_CREATE_CONTEXT_MSG:
            case ECFM_DELETE_CONTEXT_MSG:
                L2MI_SYNC_GIVE_SEM ();
                break;
            default:
                break;
        }
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pCcQMsg);
    }
    while (ECFM_DEQUE_MSG (ECFM_LBLT_PKT_QUEUE_ID, (UINT1 *) &pLbLtQMsg,
                           ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)
    {
        ECFM_LBLT_LOCK ();
        EcfmLbLtCtrlRxPktFree (pLbLtQMsg->uMsg.pEcfmPdu);
        pLbLtQMsg->uMsg.pEcfmPdu = NULL;
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtQMsg);
        pLbLtQMsg = NULL;
        ECFM_LBLT_UNLOCK ();

    }
    /* Event received, dequeue messages for processing */
    while (ECFM_DEQUE_MSG (ECFM_LBLT_CFG_QUEUE_ID, (UINT1 *) &pLbLtQMsg,
                           ECFM_DEF_MSG_LEN) == ECFM_SUCCESS)
    {
        ECFM_LBLT_LOCK ();
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtQMsg);
        pLbLtQMsg = NULL;
        ECFM_LBLT_UNLOCK ();
    }

    EcfmSizingMemDeleteMemPools ();
    EcfmCcTmrDeInit ();
    SYS_LOG_DEREGISTER (ECFM_SYSLOG_ID);
    if (EcfmPortDeRegisterWithIp () != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcAndLbLtShutDownModule: de-register with IP failure \r\n");
    }
    /* Save the syslog id in the MIB */
    ECFM_SYSLOG_ID = 0;

    ECFM_LBLT_LOCK ();

    ECFM_LBLT_PDU = NULL;

    /* Initialize Timer List for LBLT task */
    EcfmLbLtTmrDeInit ();
    ECFM_LBLT_UNLOCK ();
#ifdef L2RED_WANTED
    ECFM_MEMSET (&gEcfmRedGlobalInfo, ECFM_INIT_VAL,
                 sizeof (tEcfmRedGlobalInfo));
    gEcfmRedGlobalInfo.u2BulkUpdNextPort = 0;
    gEcfmRedGlobalInfo.u4BulkUpdNextContext = 0;
    ECFM_BULK_REQ_RECD () = ECFM_FALSE;
    ECFM_NODE_STATUS () = ECFM_NODE_IDLE;
    ECFM_NUM_STANDBY_NODES () = 0;
#else
    ECFM_RM_GET_NODE_STATUS ();
#endif

    if (EcfmRedDeRegisterWithRM () != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmCcAndLbLtShutDownModule: RM deregistration failed \r\n");
        ECFM_CC_UNLOCK ();
        return ECFM_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmCcAndLbLtShutDownModule: ... ECFM shutdown completed \r\n");
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    Function Name       : EcfmUpdateLocalSysInfo                            *
 *                                                                            *
 *    Description         : This routine is called by the LLDP module         *
 *                          whenever there is any change in port-id,          *
 *                          port-id subtype, chassis-id or                    *
 *                          chassis-id subtype.                               *
 *                                                                            *
 *    Input(s)            : u1InfoType - Type of information to be updated    *
 *                                       port-id or chassis-id                *
 *                          u4IfIndex - Interface index of the port.          *
 *                          u1IdSubType - Port-id/Chassis-id subtype.         *
 *                          pu1Id - Pointer to Port-id/chassis-id             *
 *                          u2IdLen - Port-id/chassis-id length.              *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns             : ECFM_SUCCESS/ECFM_FAILURE                         * 
 *****************************************************************************/

PUBLIC INT4
EcfmUpdateLocalSysInfo (UINT1 u1InfoType,
                        UINT4 u4IfIndex,
                        UINT1 u1IdSubType, UINT1 *pu1Id, UINT2 u2IdLen)
{
    tEcfmCcMsg         *pMsg = NULL;
    /* Check for System Status */
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        UNUSED_PARAM (pMsg);
        return ECFM_FAILURE;
    }
    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Information */
    pMsg->MsgType = (tEcfmMsgType) ECFM_UPDATE_LOC_SYS_INFO;
    pMsg->MsgSubType = (tEcfmMsgType) u1InfoType;
    switch (pMsg->MsgSubType)
    {
        case ECFM_UPDATE_CHASSISID:
            if (u2IdLen > ECFM_MAX_CHASSISID_LEN)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
                return ECFM_FAILURE;
            }
            else
            {
                pMsg->uMsg.ChassisId.u1SubType = u1IdSubType;
                pMsg->uMsg.ChassisId.u2IdLen = u2IdLen;
                ECFM_MEMCPY (pMsg->uMsg.ChassisId.au1Id, pu1Id, u2IdLen);
                break;
            }
        case ECFM_UPDATE_PORTID:
            if (u2IdLen > ECFM_MAX_PORTID_LEN)
            {
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
                return ECFM_FAILURE;
            }
            else
            {
                pMsg->uMsg.PortId.u1SubType = u1IdSubType;
                pMsg->uMsg.PortId.u2IdLen = u2IdLen;
                pMsg->u4IfIndex = u4IfIndex;
                ECFM_MEMCPY (pMsg->uMsg.PortId.au1Id, pu1Id, u2IdLen);
                break;
            }
        default:
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
            return ECFM_FAILURE;
    }
    /* Sending Message and Event to CC task */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 *                                                                            *
 *    Function Name    : EcfmNotifyIpv4IfStatusChange                         *
 *                                                                            *
 *    Description      : This Callback function is registered with ipv4       *
 *                       module(using NetIpv4RegisterHigherLayerProtocol()    *
 *                       API) to get the notification about the If status     *
 *                       change.                                              *
 *                       Whenever status of any ip interface changed this     *
 *                       callback routine is invoked.                         *
 *                       After getting this                                   *
 *                       notification ECFM module updates its local           *
 *                       Management Address database.                         *
 *                       These Ip addresses are used by ECFM module           *
 *                       to construct the sender-id TLV           .           *
 *                                                                            *
 *    Input(s)         : pNetIpIfInfo - pointer to if Config Record Structure *
 *                       u4Mask - Type of notification add/delete/change      *
 *                                                                            *
 *    Output(s)        : None                                                 *
 *                                                                            *
 *    Returns          : None                                                 *
 *****************************************************************************/
PUBLIC VOID
EcfmNotifyIpv4IfStatusChange (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4Mask)
{
    tEcfmCcMsg         *pMsg = NULL;

    /* Check for System Status */
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmNotifyIpv4IfStatusChange : ecfm not initialised yet\r\n");
        return;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmNotifyIpv4IfStatusChange : Rcvd Event mask = %08x\r\n",
                  u4Mask);
    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmNotifyIpv4IfStatusChange : memory allocation failure\r\n");
        return;
    }
    if ((u4Mask & OPER_STATE) || (u4Mask & IFACE_DELETED))
        /* Process Oper Status Change/Ip Interface delete Notification */
    {
        ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));
        pMsg->MsgType = ECFM_IP4_MAN_ADDR_CHG_MSG;
        ECFM_MEMCPY (&(pMsg->uMsg.Ip4ManAddr.NetIpIfInfo), pNetIpIfInfo,
                     sizeof (tNetIpv4IfInfo));
        pMsg->uMsg.Ip4ManAddr.u4BitMap = u4Mask;
        /* Sending Message and Event to CC task */
        if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
        {
            ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "EcfmNotifyIpv4IfStatusChange : message queuing failure\r\n");
        }
    }

    else
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
    }
    return;
}

/*******************************************************************************
 *                                                                             *
 *    Function Name    : EcfmApiNotifyIpv6IfStatusChange                       *
 *                                                                             *
 *    Description      : This Callback function is registered with ipv6        *
 *                       module(using NetIpv6RegisterHigherLayerProtocol()     *
 *                       API)to get the notification about the If status       *
 *                       change.                                               *
 *                       Whenever status of any ip interface changed this      *
 *                       callback routine is invoked.                          *
 *                       After getting this notification.                      *
 *                       notification ECFM module updates its local Management *
 *                       Address database.                                     *
 *                       These Ip addresses are used by ECFM module            *
 *                       to construct the sender-id TLV.                       *
 *                                                                             *
 *    Input(s)         : pNetIpv6HlParams - Pointer to If Config Record        *
 *                                          Structure                          *
 *                                                                             *
 *    Output(s)        : None                                                  *
 *                                                                             *
 *    Returns          : None                                                  *
 ******************************************************************************/
PUBLIC VOID
EcfmNotifyIpv6IfStatusChange (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4Mask = 0;

    u4Mask = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask;
    /* Check for System Status */
    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmNotifyIpv6IfStatusChange: ecfm not initialised yet\r\n");
        return;
    }

    if (pNetIpv6HlParams->u4Command != NETIPV6_ADDRESS_CHANGE)
    {
        return;
    }
    if ((u4Mask != NETIPV6_ADDRESS_ADD) && (u4Mask != NETIPV6_ADDRESS_DELETE))
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmiNotifyIpv6IfStatusChange: Process only the IP "
                      "address Addition and Deletion Event. Ignoring all other "
                      "events\r\n");
        /* Process only the IP address Addition And Deletion Notification */
        return;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmNotifyIpv6IfStatusChange: memory allocation failure\r\n");
        return;
    }
    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));
    pMsg->MsgType = ECFM_IP6_MAN_ADDR_CHG_MSG;
    ECFM_MEMCPY (&(pMsg->uMsg.Ip6ManAddr), pNetIpv6HlParams,
                 sizeof (tNetIpv6AddrChange));
    pMsg->uMsg.Ip4ManAddr.u4BitMap = u4Mask;
    /* Sending Message and Event to CC task */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmNotifyIpv6IfStatusChange: message queuing failure\r\n");
        return;
    }
    return;
}

/*******************************************************************************
 *                                                                             *
 *    Function Name    : EcfmConfigureCliSession                               *
 *                                                                             *
 *    Description      : This routine is used to configure the cli session     *
 *                       identifier in a MEP for any ecfm cli session which    *
 *                       can block the cli thread till the end of execution of *
 *                       command by the protocol thread.                       *
 *                                                                             *
 *    Input(s)         : u4ContextId - Context identifier                      *
 *                       u4MdIndex - Maintenance  domain index                 *
 *                       u4MaIndex - Maintenance  association index            *
 *                       u2MepId - MEP Identifier                              *
 *                       CliHandle - Cli session handle                        *
 *                       u1SessionType - Type of cli session (LB/LT/TST)       *
 *                                                                             *
 *    Output(s)        : None                                                  *
 *                                                                             *
 *    Returns          : ECFM_SUCCESS/ECFM_FAILURE                             *
 ******************************************************************************/
PUBLIC INT4
EcfmConfigureCliSession (UINT4 u4ContextId,
                         UINT4 u4MdIndex,
                         UINT4 u4MaIndex,
                         UINT2 u2MepId,
                         tCliHandle CliHandle, UINT1 u1SessionType)
{
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    INT4                i4RetVal = ECFM_SUCCESS;

    if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)
    {
        ECFM_LBLT_RELEASE_CONTEXT ();
        return ECFM_FAILURE;
    }
    switch (u1SessionType)
    {
        case ECFM_CLI_SESSION_LBM:
            /* Check is we have already a transaction going on */
            if (pMepInfo->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
            {
                i4RetVal = ECFM_FAILURE;
                break;
            }
            pMepInfo->LbInfo.CurrentCliHandle = CliHandle;
            break;
        case ECFM_CLI_SESSION_LTM:
            /* Check is we have already a transaction going on */
            if (pMepInfo->LtInfo.u1TxLtmStatus != ECFM_TX_STATUS_READY)
            {
                i4RetVal = ECFM_FAILURE;
                break;
            }
            pMepInfo->LtInfo.CurrentCliHandle = CliHandle;
            break;
        case ECFM_CLI_SESSION_TST:
            /* Check is we have already a transaction going on */
            if (pMepInfo->TstInfo.u1TstStatus != ECFM_TX_STATUS_READY)
            {
                i4RetVal = ECFM_FAILURE;
                break;
            }
            pMepInfo->TstInfo.CurrentCliHandle = CliHandle;
            break;
        case ECFM_CLI_SESSION_TH:
            /* Check is we have already a transaction going on */
            if (pMepInfo->ThInfo.u1ThStatus != ECFM_TX_STATUS_READY)
            {
                i4RetVal = ECFM_FAILURE;
                break;
            }
            pMepInfo->ThInfo.CurrentCliHandle = CliHandle;
            break;

        default:
            i4RetVal = ECFM_FAILURE;
            break;
    }
    ECFM_LBLT_RELEASE_CONTEXT ();
    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : EcfmCcmOffloadCreateIsidIndication                    *
 *                                                                            *
 * Description        : This function is called by the l2iwf/PBB Module       *
 *                      when a Isid is created.                               *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                      u4Isid      - Isid to be created                      *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/
PUBLIC INT4
EcfmCcmOffloadCreateIsidIndication (UINT4 u4ContextId, UINT4 u4Isid)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCcmOffloadCreateIsidIndication : ECFM MODULE -"
                      " not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_CREATE_ISID;
    pMsg->u4ContextId = u4ContextId;
    pMsg->uMsg.Indications.u4VlanIdIsid = u4Isid;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmCcmOffloadDeleteIsidIndication                    *
 *                                                                            *
 * Description        : This function is called by the l2iwf/PBB Module       *
 *                      when a Isid is Deleted.                               *
 *                                                                            *
 * Input(s)           : u4ContextId - Context Identifier                      *
 *                      u4Isid      - Isid to be created                      *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *****************************************************************************/

PUBLIC INT4
EcfmCcmOffloadDeleteIsidIndication (UINT4 u4ContextId, UINT4 u4Isid)
{
    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmCcmOffloadDeleteIsidIndication : ECFM MODULE "
                      " - not Initialised \r\n");
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {

        return ECFM_FAILURE;
    }

    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_DELETE_ISID;
    pMsg->uMsg.Indications.u4VlanIdIsid = u4Isid;
    pMsg->u4ContextId = u4ContextId;

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/******************************************************************************
 *    Function Name       : EcfmUpdateIntfType                                *
 *                                                                            *
 *    Description         : Invoked by L2IWF  whenever any port is unmapped   *
 *                          from a context                                    *
 *                                                                            *
 *    Input(s)            : u2IfIndex - port that is unmapped .               *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Use of Recursion    : None.                                             *
 *                                                                            *
 *    Returns            : ECFM_SUCCESS                                       *
 *                         ECFM_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
EcfmUpdateIntfType (UINT4 u4ContextId, UINT1 u1IntfType)
{

    tEcfmCcMsg         *pMsg = NULL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUpdateIntfType : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Check for System Status for Context */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmUpdateIntfType : ECFM MODULE - not Started \r\n");
        return ECFM_FAILURE;
    }
    if ((u1IntfType != L2IWF_C_INTERFACE_TYPE) &&
        (u1IntfType != L2IWF_S_INTERFACE_TYPE))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_CONTROL_PLANE_TRC,
                      "EcfmUpdateIntfType : ECFM MODULE - Invalid Interface Type \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmUpdateIntfType : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));

    /* Updating Message Information */
    pMsg->MsgType = (tEcfmMsgType) ECFM_INTF_TYPE_CHANGE;
    pMsg->u4ContextId = u4ContextId;
    pMsg->uMsg.u1IntfType = u1IntfType;

    /* Sending Message and Event to own task */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmUpdateIntfType : Message sending and Event Notification"
                      "failed \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmAddPortToPortChannel                             *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module        *
 *                      to indicate the addition of a port to a              *
 *                      port channel.                                        *
 *                                                                           *
 * Input(s)           : u4IfIndex - The Port Index of the port to be created *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC INT4
EcfmAddPortToPortChannel (UINT4 u4IfIndex)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmAddPortToPortChannel : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Verify the Interface Index for which Port Create indication is
     * received  */
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmAddPortToPortChannel : Invalid IfIndex \n");
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmAddPortToPortChannel : ECFM MODULE -  not Started \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmAddPortToPortChannel : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_ADD_TO_PORT_CHANNEL;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;
    /* Sending Message and Event to own tasks */

    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmAddPortToPortChannel : Message Queuing Failed \n");

        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmRemovePortFromPortChannel                        *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module        *
 *                      to indicate the removal of a port from a             *
 *                      port channel.                                        *
 *                                                                           *
 * Input(s)           : u4IfIndex - The Port Index of the port to be created *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC INT4
EcfmRemovePortFromPortChannel (UINT4 u4IfIndex)
{
    tEcfmCcMsg         *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2LocalPort = ECFM_INIT_VAL;

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmRemovePortFromPortChannel : ECFM MODULE - not Initialised \r\n");
        return ECFM_FAILURE;
    }
    /* Verify the Interface Index for which Port Create indication is
     * received  */
    if (ECFM_IS_VALID_INTERFACE (u4IfIndex) == ECFM_FALSE)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmRemovePortFromPortChannel : Invalid IfIndex \n");
        return ECFM_FAILURE;
    }

    if (ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex,
                                            &u4ContextId,
                                            &u2LocalPort) != ECFM_VCM_SUCCESS)
    {
        return ECFM_FAILURE;
    }
    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmRemovePortFromPortChannel : ECFM MODULE -  not Started \r\n");
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pMsg) == NULL)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "EcfmRemovePortFromPortChannel : MEM Block Allocation Failed \r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);

    pMsg->MsgType = (tEcfmMsgType) ECFM_REMOVE_FROM_PORT_CHANNEL;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2PortNum = u2LocalPort;

    /* Sending Message and Event to own tasks */
    if (EcfmCcCfgQueMsg (pMsg) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmRemovePortFromPortChannel : Message Queuing Failed \n");

        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pMsg);
        return ECFM_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmStartedInContext                                 *
 *                                                                           *
 * Description        : This function is called from the VLAN module to      *
 *                      check whether ECFM is started in a context or not.   *
 *                                                                           *
 * Input(s)           : u4ContextId - Context indentifier                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_TRUE/ECFM_FALSE                                 *
 ****************************************************************************/
INT4
EcfmStartedInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= ECFM_MAX_CONTEXTS)
    {
        return ECFM_FALSE;
    }
    if ((ECFM_IS_SYSTEM_INITIALISED ()) &&
        (ECFM_IS_SYSTEM_STARTED (u4ContextId)))
    {
        return ECFM_TRUE;
    }
    return ECFM_FALSE;
}

/*****************************************************************************
 * Function Name      : EcfmApiMplsCallBack                                  *
 *                                                                           *
 * Description        : This is the call back function provided by the ECFM. *
 *                      This API needs to be invoked for all the             *
 *                      notifications regarding the MPLS TP network. This    *
 *                      needs to be invoked during the following conditions. *
 *                      (1) Y.1731 PDU received over MPLS TP networks. The   *
 *                          pBuf, Opcode and u2Event will be the fields of   *
 *                          interest.                                        *
 *                          ECFM_OPCODE_CCM      1  - OpCode for CCM PDUs    *
 *                          ECFM_OPCODE_LBR      2  - OpCode for LBR PDUs    *
 *                          ECFM_OPCODE_LBM      3  - OpCode for LBM PDUs    *
 *                      (2) LSP/PW Path status changes. Only the PathId and  *
 *                          u2Event will be the fields of interest.          *
 *                                                                           *
 * Input(s)           : pMplsEventNotif - Pointer to the structure consisting*
 *                                        of information that needs to be    *
 *                                        notified to the ECFM module.       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            *
 ****************************************************************************/
PUBLIC INT4
EcfmApiMplsCallBack (UINT4 u4ModId, VOID *pMplsEventNotif)
{
    tEcfmCcMsg         *pCcMsg = NULL;
    tEcfmLbLtMsg       *pLbLtMsg = NULL;
    tMplsEventNotif    *pMplsEvent = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1RxOpcode = 0;

    pMplsEvent = (tMplsEventNotif *) pMplsEventNotif;
    ECFM_GLB_TRC (pMplsEvent->u4ContextId, ECFM_CONTROL_PLANE_TRC |
                  ECFM_FN_ENTRY_TRC, "EcfmApiMplsCallBack: Y.1731 "
                  "PDU or LSP/PW Path status change message received "
                  "from MPLS TP networks \r\n");
    if (u4ModId != MPLS_Y1731_APP_ID)
    {
        ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmApiMplsCallBack: ECFM MODULE - "
                      "Invalid Module Id \r\n");
        if (pMplsEvent->pBuf != NULL)
        {
            ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
        }
        return ECFM_FAILURE;
    }

    if ((ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE) &&
        (ECFM_IS_SYSTEM_STARTED ((pMplsEvent->u4ContextId))))
    {
        /* ECFM module itself is not initialised. Return safely without
         * further processing.
         */
        ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "EcfmApiMplsCallBack: ECFM MODULE - not Initialised \r\n");
        ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
        return ECFM_FAILURE;
    }

    /* ECFM module expects only two events from MPLS currently (For more
     * information, please refer function description). Decode the message
     * type and post the event/message appropriately
     */
    switch (pMplsEvent->u2Event)
    {
            /* Based on the Opcode, packet is queued to the CC (for CCM) or
             * LBLT Task(for LBM/LBR) task. 
             */
        case MPLS_Y1731_PACKET:
            /* Determine the Y.1731 header offset by calculating the MPLS 
             * header size 
             */
            EcfmMptpUtilGetY1731HdrOffSet (pMplsEvent->pBuf, &u4Offset);
            /* Move the offset to point to the Opcode field */
            u4Offset = u4Offset + ECFM_MDLEVEL_VER_FIELD_SIZE;
            /* Get Opcode from Received PDU */
            ECFM_CRU_GET_1_BYTE (pMplsEvent->pBuf, u4Offset, u1RxOpcode);

            switch (u1RxOpcode)
            {
                case ECFM_OPCODE_CCM:
                case ECFM_OPCODE_LMM:
                case ECFM_OPCODE_LMR:
                    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pCcMsg) == NULL)
                    {
                        ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC
                                      | ECFM_OS_RESOURCE_TRC |
                                      ECFM_CONTROL_PLANE_TRC,
                                      "EcfmApiMplsCallBack: Unable to allocate "
                                      "Memory for Y1731 CCM PDU Received "
                                      "Q Message!!!!!!!!!!!!\r\n");
                        ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                        return ECFM_FAILURE;
                    }

                    ECFM_MEMSET (pCcMsg, ECFM_INIT_VAL, sizeof (tEcfmCcMsg));
                    pCcMsg->MsgType =
                        (tEcfmMsgType) ECFM_EV_MPLSTP_CC_PDU_IN_QUEUE;
                    pCcMsg->u4IfIndex = pMplsEvent->u4InIfIndex;
                    pCcMsg->u4ContextId = pMplsEvent->u4ContextId;
                    pCcMsg->uMsg.pEcfmPdu = pMplsEvent->pBuf;

                    /* Sending message and Event to own task */
                    if (EcfmCcPktQueMsg (pCcMsg) != ECFM_SUCCESS)
                    {
                        ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                                      "EcfmApiMplsCallBack: Posting to Message"
                                      "Queue & sending Event to CC task  FAILED"
                                      "!!! \r\n");
                        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL,
                                             (UINT1 *) pCcMsg);
                        return ECFM_FAILURE;
                    }

                    break;
                case ECFM_OPCODE_LBM:
                case ECFM_OPCODE_LBR:
                case ECFM_OPCODE_1DM:
                case ECFM_OPCODE_DMM:
                case ECFM_OPCODE_DMR:
                    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pLbLtMsg) == NULL)
                    {
                        ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC
                                      | ECFM_OS_RESOURCE_TRC |
                                      ECFM_CONTROL_PLANE_TRC,
                                      "EcfmApiMplsCallBack: Unable to allocate "
                                      "Memory for Y1731 (LBM/LBR) PDU Received!!!\r\n");
                        ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
                        return ECFM_FAILURE;
                    }

                    ECFM_MEMSET (pLbLtMsg, ECFM_INIT_VAL,
                                 sizeof (tEcfmLbLtMsg));

                    pLbLtMsg->MsgType =
                        (tEcfmMsgType) ECFM_EV_MPLSTP_LBLT_PDU_IN_QUE;
                    pLbLtMsg->u4IfIndex = pMplsEvent->u4InIfIndex;
                    pLbLtMsg->u4ContextId = pMplsEvent->u4ContextId;
                    pLbLtMsg->uMsg.pEcfmPdu = pMplsEvent->pBuf;

                    /* Sending message and Event to own task */
                    if (EcfmLbLtPktQueMsg (pLbLtMsg) != ECFM_SUCCESS)
                    {
                        ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                                      "EcfmApiMplsCallBack: Posting to "
                                      "Message Queue & sending Event to LBLT task "
                                      "FAILED !!! \r\n");
                        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL,
                                             (UINT1 *) pLbLtMsg);
                        return ECFM_FAILURE;
                    }
                    break;
                default:
                    ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                                  ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                                  "EcfmApiMplsCallBack: Invalid Opcode!!! \r\n");
                    return ECFM_FAILURE;
            }                    /* end of switch - Opcode */

            break;
        case MPLS_TNL_UP_EVENT:
        case MPLS_TNL_DOWN_EVENT:
        case MPLS_PW_UP_EVENT:
        case MPLS_PW_DOWN_EVENT:
            /* Allocating MEM Block for the Message */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pCcMsg) == NULL)
            {
                ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                              ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                              ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "EcfmApiMplsCallBack: Unable to allocate "
                              "Memory for Q Message!!!!!!!!!!!!\r\n");
                ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            ECFM_MEMSET (pCcMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);
            pCcMsg->MsgType = (tEcfmMsgType) ECFM_MPLS_PATH_STATUS_CHG;
            pCcMsg->u4IfIndex = pMplsEvent->u4InIfIndex;
            pCcMsg->u4ContextId = pMplsEvent->u4ContextId;

            if ((pMplsEvent->u2Event == MPLS_TNL_UP_EVENT) ||
                (pMplsEvent->u2Event == MPLS_PW_UP_EVENT))
            {
                pCcMsg->u1MplsTnlPwOperState = ECFM_MPLSTP_PATH_UP;
            }
            else if ((pMplsEvent->u2Event == MPLS_TNL_DOWN_EVENT) ||
                     (pMplsEvent->u2Event == MPLS_PW_DOWN_EVENT))
            {
                pCcMsg->u1MplsTnlPwOperState = ECFM_MPLSTP_PATH_DOWN;
            }
            if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_TUNNEL)
            {
                pCcMsg->uMsg.EcfmMplsParams.u1MplsPathType =
                    MPLS_PATH_TYPE_TUNNEL;
                pCcMsg->uMsg.EcfmMplsParams.MplsPathParams.MplsLspParams.
                    u4TunnelId = pMplsEvent->PathId.TnlId.u4SrcTnlNum;
                pCcMsg->uMsg.EcfmMplsParams.MplsPathParams.MplsLspParams.
                    u4TunnelInst = pMplsEvent->PathId.TnlId.u4LspNum;
                pCcMsg->uMsg.EcfmMplsParams.MplsPathParams.MplsLspParams.
                    u4SrcLer =
                    pMplsEvent->PathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0];
                pCcMsg->uMsg.EcfmMplsParams.MplsPathParams.MplsLspParams.
                    u4DstLer =
                    pMplsEvent->PathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0];
            }
            else if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_PW)
            {
                pCcMsg->uMsg.EcfmMplsParams.u1MplsPathType = MPLS_PATH_TYPE_PW;
                pCcMsg->uMsg.EcfmMplsParams.MplsPathParams.u4PswId =
                    pMplsEvent->PathId.unPathId.MplsPwId.u4PwIndex;
            }

            /* Sending Message and Event to own tasks */
            if (EcfmCcPktQueMsg (pCcMsg) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                              ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC,
                              "EcfmApiMplsCallBack: Unable to allocate "
                              "Memory for Q Message!!!!!!!!!!!!\r\n");
                ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pCcMsg);
                return ECFM_FAILURE;
            }
            break;
            /* case to handle the Bulk Path Status Records */
        case ECFM_MPLS_BULK_PATH_STATUS_IND:

            if (pMplsEvent->pBuf == NULL)
            {
                ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "MPLS Bulk Path Status information is NULL \r\n");
                ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                return ECFM_FAILURE;
            }

            /* Allocating MEM Block for the Message */
            if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pCcMsg) == NULL)
            {
                ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                              ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                              ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                              "EcfmApiMplsCallBack: Unable to allocate "
                              "Memory for Q Message MPLS Bulk Path Status"
                              "Record update !!!!!!!!!!!!\r\n");
                ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return ECFM_FAILURE;
            }

            ECFM_MEMSET (pCcMsg, ECFM_INIT_VAL, ECFM_CC_MSG_INFO_SIZE);
            pCcMsg->MsgType = (tEcfmMsgType) ECFM_MPLS_BULK_PATH_STATUS_IND;
            pCcMsg->u4ContextId = pMplsEvent->u4ContextId;
            pCcMsg->uMsg.pEcfmPdu = pMplsEvent->pBuf;

            /* Sending Message and Event to own tasks */
            if (EcfmCcCfgQueMsg (pCcMsg) != ECFM_SUCCESS)
            {
                ECFM_GLB_TRC (pMplsEvent->u4ContextId,
                              ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                              ECFM_ALL_FAILURE_TRC,
                              "EcfmApiMplsCallBack: Failed to Process the"
                              "Bulk Path Status Record !!!!!!!\r\n");
                ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pCcMsg);
                return ECFM_FAILURE;
            }
            break;
        default:
            /* Unexpected event occured */
            ECFM_GLB_TRC (pMplsEvent->u4ContextId, ECFM_CONTROL_PLANE_TRC,
                          "EcfmApiMplsCallBack: Invalid event !!! \r\n");
            ECFM_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
            return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EcfmApiValidateEvcToMepMapping                       *
 *                                                                           *
 * Description        : This function is called from the MEF and Vlan        *
 *                      module for the validation of MEP to EVC/Vlan         *
 *                      association when EVC/Vlan is going to be deleted.    *
 *                                                                           *
 * Input(s)           : u4VlanId - The Vlan id to which the MEPs need to be  *
 *                                 searched.                                 *
 *                      It returns ECFM_FAILURE if atleast one MEP is created*
 *                      for the input Vlan id else ECFM_SUCCESS.             *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *****************************************************************************/

PUBLIC INT4
EcfmApiValidateEvcToMepMapping (UINT4 u4VlanId)
{
    tEcfmCcMepInfo     *pMepNode = NULL;

    ECFM_CC_LOCK();

    pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);

    while (pMepNode != NULL)
    {
        if (pMepNode->u4PrimaryVidIsid == u4VlanId)
        {
            /* MEP created for the V-lan id received,
             * return failure so that EVC cannot be deleted.
             */
            ECFM_CC_UNLOCK ();
            return ECFM_FAILURE;
        }
        pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
    }
   
    ECFM_CC_UNLOCK ();

    /* No MEPs are created for the received V-lan */
    return ECFM_SUCCESS;
}

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)

/*****************************************************************************
 * Function Name      : Y1731ApiHandleExtRequest                             *
 *                                                                           *
 * Description        : This function is called from the Y1564 and RFC2544   *
 *                      module to indicate the creation of a Port.           *
 *                                                                           *
 * Input(s)           : pEcfmReqParams - pointer to ReqParams                *
 *                      pEcfmRespParams - pointer to ResParams               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/
PUBLIC INT4
Y1731ApiHandleExtRequest (tEcfmReqParams * pEcfmReqParams,
                          tEcfmRespParams * pEcfmRespParams)

{
    tEcfmLbLtMsg         *pLbLtMsg = NULL;
    INT4                  i4RetVal = ECFM_FAILURE;

    UNUSED_PARAM (pEcfmRespParams);

    if (ECFM_IS_SYSTEM_INITIALISED () != ECFM_TRUE)
    {
        ECFM_GLB_TRC (pEcfmReqParams->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "Y1731ApiHandleExtRequest"
                      ": ECFM MODULE - not Initialised \r\n");
        UNUSED_PARAM (pLbLtMsg);
        return ECFM_FAILURE;
    }

    /* Checking System Status for ECFM */
    if (ECFM_IS_SYSTEM_SHUTDOWN (pEcfmReqParams->u4ContextId))
    {
        ECFM_GLB_TRC (pEcfmReqParams->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC |
                      ECFM_ALL_FAILURE_TRC,
                      "Y1731ApiHandleExtRequest"
                      ": ECFM MODULE -  not Started \r\n");
        UNUSED_PARAM (pLbLtMsg);
        return ECFM_FAILURE;
    }

    /* Allocating MEM Block for the Message */
    /* CFM pdu is for LBLT Task, signal to LBLT task */
    if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pLbLtMsg) == NULL)
    {
        ECFM_GLB_TRC (pEcfmReqParams->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC |
                      ECFM_OS_RESOURCE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "Y1731ApiHandleExtRequest "
                      ": MEM Block Allocation Failed \r\n");
        ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }

    ECFM_MEMSET (pLbLtMsg, ECFM_INIT_VAL, ECFM_LBLT_MSG_INFO_SIZE);

    ECFM_MEMCPY(&pLbLtMsg->ReqParams, pEcfmReqParams, sizeof(tEcfmReqParams));

      pLbLtMsg->MsgType = pEcfmReqParams->u4MsgType ;
    /*
    else
    {
        pLbLtMsg->MsgType = pEcfmReqParams.RFC2544EcfmReqParams.u4MsgType ;
    } */
    /* Sending message and Event to own task */
    i4RetVal = EcfmLbLtCfgQueMsg (pLbLtMsg);

    if (i4RetVal != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (pEcfmReqParams->u4ContextId,
                      ECFM_INIT_SHUT_TRC | ECFM_ALL_FAILURE_TRC,
                      "Y1731ApiHandleExtRequest : "
                      "Posting toMessage Queue & sending Event to LBLT task "
                      "FAILED !!! \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL,
                             (UINT1 *) pLbLtMsg);

        i4RetVal = ECFM_FAILURE;
    }
    i4RetVal = ECFM_SUCCESS;

    return (((i4RetVal == ECFM_SUCCESS) ? OSIX_SUCCESS : OSIX_FAILURE));
}

/*******************************************************************************
 * Function Name      : EcfmApiValMepIndex                                      *
 *  *                                                                           *
 *  * Description        : This function is called from the ERPS Module         *
 *  *                      to validate the domain and service and MEP id        *
 *  *                                                                           *
 *  *                                                                           *
 *  *                                                                           *
 *  * Input(s)           : pMepInfo -Structure of MEP informaion in ERPS        *
 *  *                                                                           *
 *  * Output(s)          : None                                                 *
 *  *                                                                           *
 *  * Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          *
 *  ***************************************************************************/

PUBLIC INT4 EcfmApiValMepIndex(tEcfmMepInfoParams * pMepInfo)
{
    tEcfmLbLtMepInfo   *pMepNode = NULL;
    ECFM_LBLT_LOCK ();

    if (ECFM_LBLT_SELECT_CONTEXT (pMepInfo->u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }
    pMepNode = EcfmLbLtUtilGetMepEntryFrmGlob (pMepInfo->u4MdIndex,pMepInfo->u4MaIndex,
                                               pMepInfo->u2MepId);

    if (pMepNode == NULL)
    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "Mep is not present in CFM for the given domain"
                       "and service in CFM\n");
        ECFM_LBLT_UNLOCK ();
        return ECFM_FAILURE;
    }


    ECFM_LBLT_UNLOCK ();

    return ECFM_SUCCESS;

}
#endif
/*****************************************************************************
                            End of File cfmapi.c
 *****************************************************************************/
