/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccrdst.c,v 1.10 2012/03/26 12:33:58 siva Exp $
 *
 * Description: This file contains l2red specific stubs for cc task.
 *************************************************************************/

#include "cfminc.h"

/*****************************************************************************/
/* Function Name      : EcfmRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers ECFM with RM by providing an application   */
/*                      ID for ECFM and a call back function to be called    */
/*                      whenever RM needs to send an event to ECFM.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then ECFM_SUCCESS         */
/*                      Otherwise ECFM_FAILURE                               */
/*****************************************************************************/
INT4
EcfmRedRegisterWithRM (VOID)
{
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedDeRegisterWithRM                              */
/*                                                                           */
/* Description        : Deregisters ECFM with RM.                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then ECFM_SUCCESS       */
/*                      Otherwise ECFM_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
EcfmRedDeRegisterWithRM (VOID)
{
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncUpPortOperStatus                          */
/*                                                                           */
/* Description        : This function sends the port oper status to the      */
/*                      Standby.                                             */
/*                                                                           */
/* Input(s)           : u2PortIndex   - Port Id.                             */
/*                      u1PortOperStatus - Port Oper Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE.                         */
/*****************************************************************************/
PUBLIC INT4
EcfmRedSyncUpPortOperStatus (UINT4 u4IfIndex, UINT1 u1PortOperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PortOperStatus);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncMipDbHldTmr                               */
/*                                                                           */
/* Description        : This function Sends MIB DB Remaing time to           */
/*                      the standby Node.                                    */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

PUBLIC VOID
EcfmRedSyncMipDbHldTmr (UINT4 u4RemaimingTime)
{
    UNUSED_PARAM (u4RemaimingTime);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncErrorLogEntry                             */
/*                                                                           */
/* Description        : This function will send the Error Log Entry event to */
/*                      STANDBY node                                         */
/*                                                                           */
/* Input(s)           : pErrorNode - Pointer to the error Node               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncTmr (UINT1 u1TmrEvnt, UINT1 u1TimerType,
                tEcfmCcPduSmInfo * pPduSmInfo, UINT4 u4Interval)
{
    UNUSED_PARAM (u1TmrEvnt);
    UNUSED_PARAM (u1TimerType);
    UNUSED_PARAM (pPduSmInfo);
    UNUSED_PARAM (u4Interval);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncErrorLogEntry                             */
/*                                                                           */
/* Description        : This function will send the RDI expiry event to      */
/*                      STANDBY node                                         */
/*                                                                           */
/* Input(s)           : u4MdIndex - MD Index for the MEP                     */
/*                      u4MaIndex - MA Index for the MEP                     */
/*                      u2MepId   - MepId for the MEP                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncErrorLogEntry (tEcfmCcErrLogInfo * pErrorNode)
{
    UNUSED_PARAM (pErrorNode);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncSmData                                    */
/*                                                                           */
/* Description        : This function will send the RDI expiry event to      */
/*                      STANDBY node                                         */
/*                                                                           */
/* Input(s)           : u4MdIndex - MD Index for the MEP                     */
/*                      u4MaIndex - MA Index for the MEP                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
EcfmRedSyncSmData (tEcfmCcPduSmInfo * pPduSmInfo)
{
    UNUSED_PARAM (pPduSmInfo);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLckPeriodTimeout                          */
/*                                                                           */
/* Description        : This function will send the LCK Period Timeout event */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLckPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLckCondition                              */
/*                                                                           */
/* Description        : This function will send the set LCK Condition event  */
/*                      to  STANDBY node                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info for which LCK cond    */
/*                                 is to be set.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLckCondition (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLckStatus                                 */
/*                                                                           */
/* Description        : This function will send the set LCK Status event     */
/*                      to  STANDBY node                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info for which LCK cond    */
/*                                 is to be set.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLckStatus (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncAisPeriodTimeout                          */
/*                                                                           */
/* Description        : This function will send the AIS Period Timeout event */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncAisPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncAisCondition                              */
/*                                                                           */
/* Description        : This function will send the set AIS Condition event  */
/*                      to  STANDBY node                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info for which AIS cond    */
/*                                 is to be set.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncAisCondition (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmDeadlineTmrExp                         */
/*                                                                           */
/* Description        : This function will send LM DeadLine Timeout event    */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLmDeadlineTmrExp (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmStopTransaction                         */
/*                                                                           */
/* Description        : This function will send LM Stop Transaction to       */
/*                      STANDBY Node when number of LMM to be transmitted    */
/*                      becomes 0.                                           */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLmStopTransaction (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmCache                                   */
/*                                                                           */
/* Description        : This function  syncs the LM Cache at CC Task at      */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLmCache ()
{
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncAvlbltyInfo                               */
/*                                                                           */
/* Description        : This function  syncs the Availablity Info at CC      */
/*                      Task at standby node.                                */
/*                                                                           */
/* Input(s)           : pMepInfo - Ponter to the MEP Table                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncAvlbltyInfo (tEcfmCcMepInfo * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchHwAuditCmdEvtInfo                        */
/*                                                                           */
/* Description        : This function synchs Command/Event information under */
/*                      execution to execute HW Audit after switchoverobject */
/*                      maintained for MEP to                                */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4CmdEvnt - Command/Event to synch to STANDBY        */
/*                      u4IfIndex - Actual Interface Index                   */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      VlanId    - VLAn Idenetifier                         */
/*                      bError    - Error Condition to be set at STANDBY     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchHwAuditCmdEvtInfo (UINT4 u4ContextId,
                               UINT4 u4CmdEvnt,
                               UINT4 u4IfIndex,
                               UINT4 u4MdId,
                               UINT4 u4MaId,
                               UINT4 u4MepId, tVlanId VlanId, BOOL1 bError)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4CmdEvnt);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MdId);
    UNUSED_PARAM (u4MaId);
    UNUSED_PARAM (u4MepId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (bError);

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncCCDataOnChng                              */
/*                                                                           */
/* Description        : This function synchs information which needs to be   */
/*                      synched immediately when the value is updated on     */
/*                      ACTIVE                                               */
/*                      u2MepId - Maintenance End Poind ID                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedSyncCCDataOnChng (UINT4 u4ContextId, UINT4 u4MdIndex, UINT4 u4MaIndex,
                         UINT2 u2MepId, UINT2 u2RMepId)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MdIndex);
    UNUSED_PARAM (u4MaIndex);
    UNUSED_PARAM (u2MepId);
    UNUSED_PARAM (u2RMepId);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchRmepCounter                              */
/*                                                                           */
/* Description        : This function synchs counter maintained in RMEO for  */
/*                      detecting LOC error                                  */
/* Input(s)           : pPduSmInfo - Context/Mep/RMEP/Mip information        */
/*                      u1Counter - Counter value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchRmepCounter (tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Counter)
{
    UNUSED_PARAM (pPduSmInfo);
    UNUSED_PARAM (u1Counter);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedCheckAndSyncSmData                            */
/*                                                                           */
/* Description        : This function checks if the state of the state       */
/*                      machine is changed. If the state is changed then     */
/*                      State Mahine data will be synchupdates with standby  */
/*                      node                                                 */
/*                                                                           */
/* Input(s)           : pu1SmState    - Pointer to Current SM state          */
/*                      u1StateToSet  - State to be set for the state machine*/
/*                      pPduSmInfo - Context/Mep/RMEP/Mip information        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
VOID
EcfmRedCheckAndSyncSmData (UINT1 *pu1SmState, UINT1 u1StateToSet,
                           tEcfmCcPduSmInfo * pPduSmInfo)
{
    UNUSED_PARAM (pu1SmState);
    UNUSED_PARAM (u1StateToSet);
    UNUSED_PARAM (pPduSmInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchOffTxFilterId                            */
/*                                                                           */
/* Description        : This function synchs Offload module generated        */
/*                      Transmit Filter  id to STANDBY node                  */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      u4TxFilterId   - CCM Transmission filter identifier  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchOffTxFilterId (UINT4 u4ContextId,
                           UINT4 u4MdId,
                           UINT4 u4MaId, UINT4 u4MepId, UINT4 u4TxFilterId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MdId);
    UNUSED_PARAM (u4MaId);
    UNUSED_PARAM (u4MepId);
    UNUSED_PARAM (u4TxFilterId);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchOffRxFilterId                            */
/*                                                                           */
/* Description        : This function synchs Offload module generated        */
/*                      Recevie Filter  id to STANDBY node                   */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      u4RxFilterId   - CCM Reception filter identifier  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchOffRxFilterId (UINT4 u4ContextId,
                           UINT4 u4MdId,
                           UINT4 u4MaId,
                           UINT4 u4MepId, UINT4 u4RMepId, UINT4 u4RxFilterId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MdId);
    UNUSED_PARAM (u4MaId);
    UNUSED_PARAM (u4MepId);
    UNUSED_PARAM (u4RMepId);
    UNUSED_PARAM (u4RxFilterId);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchHwRxHandler                              */
/*                                                                           */
/* Description        : This function synchs Offload module generated        */
/*                      Recevie Filter  id to STANDBY node                   */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      u4RxFilterId   - CCM Reception filter identifier  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchHwRxHandler (UINT4 u4ContextId, UINT4 u4MdId, UINT4 u4MaId,
                         UINT4 u4MepId, UINT4 u4RMepId,
                         UINT1 *pau1HwRMepHandler)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MdId);
    UNUSED_PARAM (u4MaId);
    UNUSED_PARAM (u4MepId);
    UNUSED_PARAM (u4RMepId);
    UNUSED_PARAM (pau1HwRMepHandler);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchHwTxHandler                              */
/*                                                                           */
/* Description        : This function synchs Hw Handler generated            */
/*                      id to STANDBY node                                   */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      pau1HwHandler - CCM Transmission Handler */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchHwTxHandler (UINT4 u4ContextId, UINT4 u4MdId, UINT4 u4MaId,
                         UINT4 u4MepId, UINT1 *pau1HwMepHandler)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MdId);
    UNUSED_PARAM (u4MaId);
    UNUSED_PARAM (u4MepId);
    UNUSED_PARAM (pau1HwMepHandler);
    return;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : EcfmRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      ECFM to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
EcfmRedHRProcStdyStPktReq (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
EcfmRedHRSendStdyStTailMsg (VOID)
{
    return ECFM_SUCCESS;
}

/******************************************************************************
 * Function           : EcfmRedGetHRFlag
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Hitless restart flag value.
 * Action             : This API returns the hitless restart flag value.
 ******************************************************************************/
UINT1
EcfmRedGetHRFlag (VOID)
{
    return ECFM_SUCCESS;

}
