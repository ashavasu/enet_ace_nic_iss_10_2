/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmaisin.c,v 1.19 2014/12/09 12:45:07 siva Exp $
 *
 * Description: This file contains the Functionality of the AIS 
 *              Control Sub Module.
 *******************************************************************/

#include "cfminc.h"
PRIVATE VOID EcfmCcInitFormatAisPdu PROTO ((tEcfmCcMepInfo *, UINT1 **));
PRIVATE INT4 EcfmCcInitXmitAisPdu PROTO ((tEcfmCcMepInfo *));
PRIVATE INT4 EcfmCcInitConstructAis PROTO ((tEcfmBufChainHeader *,
                                            tEcfmCcMepInfo *));

/*******************************************************************************
 * Function           : EcfmCcClntAisInitiator
 *
 * Description        : The routine implements the AIS Initiator, it calls up
 *                      routine to format and transmit AIS frame depending upon
 *                      the various events.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/

PUBLIC UINT4
EcfmCcClntAisInitiator (tEcfmCcMepInfo * pMepInfo, UINT1 u1EventID)
{
#ifdef NPAPI_WANTED
    tEcfmMplstpAisOffParams AisOffParams;
#endif
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4RetVal = ECFM_SUCCESS;

    ECFM_CC_TRC_FN_ENTRY ();

    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;

    /* Check for the event received */
    switch (u1EventID)
    {
        case ECFM_EV_MEP_BEGIN:
            /* Check if AIS Capability is enabled or disabled */
            if (ECFM_CC_IS_AIS_DISABLED (pMepInfo) == ECFM_TRUE)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntAisInitiator: "
                             "Ais Capability is disabled - Cannot Trigger AIS"
                             "Transmission \r\n");
                break;
            }

            /* Check if any defect exists at this interface then transmit AIS Pdu */
            if (EcfmCcAisCapabilityConfiguration (pMepInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntAisInitiator: "
                             "Ais Transmission Failed \r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            break;
        case ECFM_EV_MEP_NOT_ACTIVE:
            /* Stop the AIS Transmission */
            EcfmCcInitStopAisTx (pMepInfo);
            /* Stop Ais Period Timer */
            EcfmCcTmrStopTimer (ECFM_CC_TMR_AIS_PERIOD, &PduSmInfo);

            break;
        case ECFM_AIS_START_TRANSACTION:
            if (pAisInfo->b1AisTsmting == ECFM_TRUE)
            {
                break;
            }
            /* Y.1731 : Check that Ais capability should be enabled for MEP 
             * to Transmit the AIS PDU */
            if (ECFM_CC_IS_AIS_DISABLED (pMepInfo) == ECFM_TRUE)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcClntAisInitiator"
                             "Ais capability is disabled, So MEP cannot "
                             "send AIS PDU \r\n");
                return ECFM_SUCCESS;
            }
#ifdef L2RED_WANTED
            ECFM_GET_SYS_TIME (&pMepInfo->AisInfo.AisIntervalTimeStamp);
#endif

            if (pMepInfo->b1MepAisOffloadStatus == ECFM_TRUE)
            {
                /* User has enabled the Offloading capability for the MEPs to
                 * transmit AIS PDUs. Hence indicate the hardware regarding the
                 * same
                 */
                ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC,
                                  "Offloading has been enabled for the MDIndex: %u "
                                  "MAID: %u & MEPID: %u. Indicating Offloaded Module "
                                  "to transmit AIS.............\r\n",
                                  pMepInfo->pMaInfo->pMdInfo->u4MdIndex,
                                  pMepInfo->pMaInfo->u4MaIndex,
                                  pMepInfo->u2MepId);
#ifdef NPAPI_WANTED
                ECFM_MEMSET (&AisOffParams, 0x0,
                             sizeof (tEcfmMplstpAisOffParams));

                AisOffParams.u4ContextId = pMepInfo->u4ContextId;
                AisOffParams.pSlotInfo = NULL;    /* Currently Unused */
                MEMCPY (&AisOffParams.EcfmMplsParams, pMepInfo->pEcfmMplsParams,
                        sizeof (tEcfmMplsParams));
                AisOffParams.u4Period = pMepInfo->AisInfo.u4AisPeriod;
                AisOffParams.u4AisInterval = pMepInfo->AisInfo.u1AisInterval;
                AisOffParams.u1MdLevel =
                    (UINT1) pMepInfo->pMaInfo->pMdInfo->i1ClientLevel;
                AisOffParams.u1PhbValue = pMepInfo->AisInfo.u1AisPriority;
                AisOffParams.b1AisCondition = TRUE;

                if (EcfmMplstpOffSetMepAisStatus (&AisOffParams) !=
                    ECFM_SUCCESS)
                {
                    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmCcClntAisInitiator: Unable to intimate the AIS "
                                      "Entry condition to Offloaded Module for MDIndex: %u MAID: %u "
                                      "MEPID: %u ....\r\n",
                                      pMepInfo->pMaInfo->pMdInfo->u4MdIndex,
                                      pMepInfo->pMaInfo->u4MaIndex,
                                      pMepInfo->u2MepId);
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
#endif /* NPAPI_WANTED */
            }
            else
            {
                /* Call the routine to transmit the AIS Packet */
                if (EcfmCcInitXmitAisPdu (pMepInfo) != ECFM_SUCCESS)
                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcClntAisInitiator: "
                                 "Unable to transmit AIS Pdu\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            pAisInfo->b1AisTsmting = ECFM_TRUE;
            break;

        case ECFM_AIS_STOP_TRANSACTION:
            if (pAisInfo->b1AisTsmting != ECFM_TRUE)
            {
                break;
            }
#ifdef L2RED_WANTED
            pMepInfo->AisInfo.AisIntervalTimeStamp = ECFM_INIT_VAL;
#endif
            if (pMepInfo->b1MepAisOffloadStatus == ECFM_TRUE)
            {
                /* User has enabled the Offloading capability for the MEPs to
                 * transmit AIS PDUs. Hence indicate the hardware to stop
                 * AIS Tx.
                 */
                ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC,
                                  "Offloading has been enabled for the MDIndex: %u "
                                  "MAID: %u & MEPID: %u. Indicating Offloaded Module "
                                  "to transmit AIS.............\r\n",
                                  pMepInfo->pMaInfo->pMdInfo->u4MdIndex,
                                  pMepInfo->pMaInfo->u4MaIndex,
                                  pMepInfo->u2MepId);
#ifdef NPAPI_WANTED
                ECFM_MEMSET (&AisOffParams, 0x0,
                             sizeof (tEcfmMplstpAisOffParams));

                AisOffParams.u4ContextId = pMepInfo->u4ContextId;
                AisOffParams.pSlotInfo = NULL;    /* Currently Unused */
                MEMCPY (&AisOffParams.EcfmMplsParams, pMepInfo->pEcfmMplsParams,
                        sizeof (tEcfmMplsParams));
                AisOffParams.u4Period = pMepInfo->AisInfo.u4AisPeriod;
                AisOffParams.u4AisInterval = pMepInfo->AisInfo.u1AisInterval;
                AisOffParams.u1MdLevel =
                    (UINT1) pMepInfo->pMaInfo->pMdInfo->i1ClientLevel;
                AisOffParams.u1PhbValue = pMepInfo->AisInfo.u1AisPriority;
                AisOffParams.b1AisCondition = FALSE;

                if (EcfmMplstpOffSetMepAisStatus (&AisOffParams) !=
                    ECFM_SUCCESS)
                {
                    ECFM_CC_TRC_ARG3 (ECFM_CONTROL_PLANE_TRC |
                                      ECFM_ALL_FAILURE_TRC,
                                      "EcfmCcClntAisInitiator: Unable to intimate the AIS "
                                      "Exit condition to Offloaded Module for MDIndex: %u MAID: %u "
                                      "MEPID: %u ....\r\n",
                                      pMepInfo->pMaInfo->pMdInfo->u4MdIndex,
                                      pMepInfo->pMaInfo->u4MaIndex,
                                      pMepInfo->u2MepId);
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
#endif /* NPAPI_WANTED */
            }
            /* EcfmCcInitStopAisTx is invoked even when Offloading is enabled,
             * to clear the Ais Condition in DS which is done within
             * EcfmCcClearAisCondition.
             */

            /* Call the routine to stop AIS transmission */
            EcfmCcInitStopAisTx (pMepInfo);
            break;
        case ECFM_EV_AIS_INTERVAL_EXPIRY:
            /* Y.1731 : Check that Ais capability should be enabled for MEP 
             * to Transmit the AIS PDU */
            if (ECFM_CC_IS_AIS_DISABLED (pMepInfo) == ECFM_TRUE)
            {
                ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                             "EcfmCcClntAisInitiator"
                             "Ais capability is disabled, So MEP cannot "
                             "send AIS PDU \r\n");
                /* Stop the AIS Transaction */
                EcfmCcInitStopAisTx (pMepInfo);
                return ECFM_SUCCESS;
            }
            /* Call the routine to transmit the AIS Packet */
            if (EcfmCcInitXmitAisPdu (pMepInfo) != ECFM_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcClntAisInitiator: "
                             "Unable to transmit AIS Pdu\r\n");
                u4RetVal = ECFM_FAILURE;
                break;
            }
            break;
        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcClntAisInitiator: " "Event Not Supported\r\n");
            u4RetVal = ECFM_FAILURE;
            break;
    }
    if (u4RetVal == ECFM_FAILURE)
    {
        /* Stop the AIS Transaction */
        EcfmCcInitStopAisTx (pMepInfo);
    }
    ECFM_CC_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitXmitAisPdu
 *
 * Description        : This routine formats and transmits the AIS PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmCcInitXmitAisPdu (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcAisInfo     *pAisInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;
    PduSmInfo.pPortInfo = ECFM_CC_GET_PORT_INFO (pMepInfo->u2PortNum);

    /* Y.1731 : Check that Ais capability should be enabled for MEP 
     * to Transmit the AIS PDU */
    if (ECFM_CC_IS_AIS_DISABLED (pMepInfo) == ECFM_TRUE)
    {
        ECFM_CC_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                     "EcfmCcInitXmitAisPdu: "
                     "Ais capability is disabled, So MEP cannot "
                     "send AIS PDU \r\n");
        return ECFM_FAILURE;
    }
    do
    {
        pBuf = CRU_BUF_Allocate_MsgBufChain (ECFM_MAX_AIS_PDU_SIZE, 0);
        if (pBuf == NULL)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcInitXmitAisPdu: "
                         "Buffer allocation failed\r\n");
            ECFM_CC_INCR_BUFFER_FAILURE_COUNT ();
            break;
        }
        if (pMepInfo->AisInfo.SavedAisPdu.pu1Octets != NULL)
        {
            if (ECFM_COPY_OVER_CRU_BUF
                (pBuf, pMepInfo->AisInfo.SavedAisPdu.pu1Octets, ECFM_INIT_VAL,
                 (UINT4) (pMepInfo->AisInfo.SavedAisPdu.u4OctLen)) !=
                ECFM_CRU_SUCCESS)
            {
                ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                             "EcfmCcInitXmitAisPdu: "
                             "Buffer copy operation failed\r\n");
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                break;
            }
        }
        else
        {
            i4RetVal = EcfmCcInitConstructAis (pBuf, pMepInfo);
            if (i4RetVal == ECFM_FAILURE)
            {
                ECFM_CC_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                             ALL_FAILURE_TRC,
                             "EcfmCcInitXmitAisPdu: Unable to construct AIS\r\n");
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                break;
            }
        }
        ECFM_BUF_SET_LEVEL (pBuf, pMepInfo->u1MdLevel);
        u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        ECFM_CC_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                          "EcfmCcInitXmitAisPdu: "
                          "Sending out AIS-PDU to lower layer...\r\n");
        if (EcfmCcCtrlTxTransmitPkt
            (pBuf, pMepInfo->u2PortNum, pMepInfo->u4PrimaryVidIsid,
             pAisInfo->u1AisPriority, pAisInfo->b1AisDropEnable,
             pMepInfo->u1Direction, ECFM_OPCODE_AIS, NULL,
             NULL) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcInitXmitAisPdu: "
                         "AIS transmission to the lower layer failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            break;
        }
        if (pMepInfo->u1Direction == ECFM_MP_DIR_DOWN)
        {
            ECFM_CC_INCR_TX_CFM_PDU_COUNT (pMepInfo->u2PortNum);
            ECFM_CC_INCR_TX_COUNT (pMepInfo->u2PortNum, 
                                       ECFM_BUF_GET_OPCODE (pBuf));
            ECFM_CC_INCR_CTX_TX_COUNT (PduSmInfo.pPortInfo->u4ContextId,
                                       ECFM_BUF_GET_OPCODE (pBuf));
        }
    }
    while (0);
    ECFM_GET_AIS_LCK_INTERVAL (pAisInfo->u1AisInterval, u4Interval);
    /* Start AIS Interval Timer */
    if (EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_INTERVAL, &PduSmInfo,
                             u4Interval) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitXmitAisPdu: "
                     "Failure starting AIS Interval timer\r\n");
        return ECFM_FAILURE;
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitConstructAis
 *
 * Description        : This rotuine is used to construct the new AIS CFM PDU.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : pBuf - Ais Pdu Buffer
 * 
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmCcInitConstructAis (tEcfmBufChainHeader * pBuf, tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcAisInfo     *pAisInfo = NULL;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1AisPduStart = NULL;
    UINT1              *pu1AisPduEnd = NULL;
    UINT1              *pu1AisPdu = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);
    UNUSED_PARAM (pAisInfo);

    ECFM_MEMSET (ECFM_CC_PDU, ECFM_INIT_VAL, ECFM_MAX_AIS_PDU_SIZE);
    pu1AisPdu = (UINT1 *) MemAllocMemBlk (ECFM_CC_AIS_PDU_POOL);

    if (pu1AisPdu == NULL)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitConstructAis: "
                     "Mempool allocation failed\r\n");
        ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
        return ECFM_FAILURE;
    }
    pu1AisPduStart = ECFM_CC_PDU;
    pu1AisPduEnd = ECFM_CC_PDU;
    pu1EthLlcHdr = ECFM_CC_PDU;

    /* Format the  Ais PDU */
    EcfmCcInitFormatAisPdu (pMepInfo, &pu1AisPduEnd);

    u2PduLength = (UINT2) (pu1AisPduEnd - pu1AisPduStart);
    /* Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1AisPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitConstructAis: "
                     "Buffer copy operation failed\r\n");
        MemReleaseMemBlock (ECFM_CC_AIS_PDU_POOL, (UINT1 *) pu1AisPdu);
        return ECFM_FAILURE;
    }
    pu1AisPduStart = pu1EthLlcHdr;
    EcfmFormatCcTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr, u2PduLength,
                               ECFM_OPCODE_AIS);
    /* Prepend the Ethernet and the LLC header in the
     * CRU buffer */
    u2PduLength = (UINT2) (pu1EthLlcHdr - pu1AisPduStart);
    if (ECFM_PREPEND_CRU_BUF (pBuf, pu1AisPduStart,
                              (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                     "EcfmCcInitConstructAis: "
                     "Buffer prepend operation failed\r\n");
        MemReleaseMemBlock (ECFM_CC_AIS_PDU_POOL, (UINT1 *) pu1AisPdu);
        return ECFM_FAILURE;
    }
    /* Set MD Level of Transmitting MEP in the Reserved fields of the Buffer as
     * it is required while transmitting AIS PDU to all VLANs */
    ECFM_BUF_SET_LEVEL (pBuf, pMepInfo->u1MdLevel);

    u2PduLength = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CRU_BUF_Copy_FromBufChain (pBuf, pu1AisPdu, 0, u2PduLength);

    pMepInfo->AisInfo.SavedAisPdu.pu1Octets = pu1AisPdu;
    pMepInfo->AisInfo.SavedAisPdu.u4OctLen = u2PduLength;

    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitFormatAisPdu
 *
 * Description        : This rotuine is used to fill the AIS CFM PDU.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1AisPdu - Pointer to Pointer to the AIS PDU.
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmCcInitFormatAisPdu (tEcfmCcMepInfo * pMepInfo, UINT1 **ppu1AisPdu)
{
    tEcfmCcMdInfo      *pMdInfo = NULL;
    UINT1              *pu1AisPdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;

    ECFM_CC_TRC_FN_ENTRY ();
    pu1AisPdu = *ppu1AisPdu;
    pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, (UINT1) pMdInfo->i1ClientLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1AisPdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1AisPdu, ECFM_OPCODE_AIS);

    /* Fill the Next 1 byte with Flag In AIS, Flag Field is set corresponding 
     * to interval */
    if (pMepInfo->AisInfo.u1AisInterval == ECFM_CC_AIS_LCK_INTERVAL_1_SEC)
    {
        ECFM_PUT_1BYTE (pu1AisPdu, ECFM_AIS_LCK_INTERVAL_1_S_FLAG);
    }
    else
    {
        ECFM_PUT_1BYTE (pu1AisPdu, ECFM_AIS_LCK_INTERVAL_1_MIN_FLAG);
    }

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1AisPdu, ECFM_INIT_VAL);

    /*Fill in the next 1 byte as End TLV */
    ECFM_PUT_1BYTE (pu1AisPdu, ECFM_INIT_VAL);
    *ppu1AisPdu = pu1AisPdu;

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcInitStopAisTx
 *
 * Description        : This routine is used to stop the on going AIS Tx 
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcInitStopAisTx (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPduSmInfo    PduSmInfo;

    ECFM_CC_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;

    /* Clear AIS Condition before stopping the RxWhile Timer */
    if (pMepInfo->AisInfo.b1AisCondition == ECFM_TRUE)
    {
        EcfmCcClearAisCondition (pMepInfo);
    }
    /* Stop Ais RxWhile Timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_AIS_RXWHILE, &PduSmInfo);

    /* Stop Ais Interval Timer */
    EcfmCcTmrStopTimer (ECFM_CC_TMR_AIS_INTERVAL, &PduSmInfo);
    pMepInfo->AisInfo.b1AisTsmting = ECFM_FALSE;

    if (pMepInfo->AisInfo.SavedAisPdu.pu1Octets != NULL)
    {
        MemReleaseMemBlock (ECFM_CC_AIS_PDU_POOL,
                            (UINT1 *) pMepInfo->AisInfo.SavedAisPdu.pu1Octets);
    }
    pMepInfo->AisInfo.SavedAisPdu.pu1Octets = NULL;
    pMepInfo->AisInfo.SavedAisPdu.u4OctLen = 0;
    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmCcAisCapabilityConfiguration
 *
 * Description        : This routine is used to set the AIS Capability
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcAisCapabilityConfiguration (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcMepCcInfo   *pCcInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();
    pCcInfo = ECFM_CC_GET_CCINFO_FROM_MEP (pMepInfo);

    /* Check if any Defect is present then start Ais Tx */
    if ((pCcInfo->b1UnExpectedMepDefect == ECFM_TRUE) ||
        (pCcInfo->b1UnExpectedPeriodDefect == ECFM_TRUE) ||
        (pCcInfo->b1MisMergeDefect == ECFM_TRUE) ||
        (pCcInfo->b1UnExpectedLevelDefect == ECFM_TRUE) ||
        (pMepInfo->FngInfo.b1SomeRMepCcmDefect == ECFM_TRUE) ||
        ((pMepInfo->AisInfo.b1AisCondition == ECFM_TRUE) &&
         (pMepInfo->CcInfo.b1CciEnabled == ECFM_FALSE)) ||
        (pMepInfo->LckInfo.b1LckCondition == ECFM_TRUE))
    {

        if (EcfmCcUtilPostTransaction (pMepInfo, ECFM_AIS_START_TRANSACTION) ==
            ECFM_FAILURE)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcAisCapabilityConfiguration: "
                         "AIS transmission failed\r\n");
            return ECFM_FAILURE;
        }
    }

    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcAisPeriodConfiguration
 *
 * Description        : This routine is used to set the AIS Period
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE
 ******************************************************************************/
PUBLIC INT4
EcfmCcAisPeriodConfiguration (tEcfmCcMepInfo * pMepInfo)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcAisInfo     *pAisInfo = NULL;

    ECFM_CC_TRC_FN_ENTRY ();

    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepInfo;
    pAisInfo = ECFM_CC_GET_AISINFO_FROM_MEP (pMepInfo);

    /* Stop Ais Period Timer */
    if (EcfmCcTmrStopTimer (ECFM_CC_TMR_AIS_PERIOD, &PduSmInfo) != ECFM_SUCCESS)
    {
        ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                     ECFM_ALL_FAILURE_TRC,
                     "EcfmCcAisPeriodConfiguration: "
                     "Ais Period Timer has not stopped\r\n");
        return ECFM_FAILURE;
    }

    /* Start the AIS Period Timer only if it is non-zero value */
    if (pAisInfo->u4AisPeriod != 0)
    {
        if (EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_PERIOD, &PduSmInfo,
                                 (pAisInfo->u4AisPeriod *
                                  ECFM_NUM_OF_MSEC_IN_A_SEC)) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC |
                         ECFM_ALL_FAILURE_TRC,
                         "EcfmCcAisPeriodConfiguration: "
                         "Ais Period Timer has not started\r\n");
            return ECFM_FAILURE;
        }
    }
    ECFM_CC_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmCcAisPeriodTimeout
 *
 * Description        : This routine is used to toggle the AIS capability on the
 *                      expiry of Period Timer
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmCcAisPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
    ECFM_CC_TRC_FN_ENTRY ();

    /* Sync AIS Timer Expiry in STAND BY node */
    EcfmRedSyncAisPeriodTimeout (pMepInfo);

    /* Reset AIS Period Timeout */
    pMepInfo->AisInfo.u4AisPeriod = ECFM_INIT_VAL;

    if (pMepInfo->AisInfo.u1AisCapability == ECFM_ENABLE)
    {
        EcfmCcClntAisInitiator (pMepInfo, ECFM_AIS_STOP_TRANSACTION);
        /* Disable AIS Capability */
        pMepInfo->AisInfo.u1AisCapability = ECFM_DISABLE;
    }
    else
    {
        /* Enable AIS Capability */
        pMepInfo->AisInfo.u1AisCapability = ECFM_ENABLE;
        if (EcfmCcAisCapabilityConfiguration (pMepInfo) != ECFM_SUCCESS)
        {
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcAisPeriodTimeout" "Unable to start AIS Tx\r\n");
            return;
        }
    }

    ECFM_CC_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  End of File cfmaisini.c
 ******************************************************************************/
