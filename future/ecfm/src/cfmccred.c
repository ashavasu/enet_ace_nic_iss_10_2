/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmccred.c,v 1.56 2015/03/06 10:20:15 siva Exp $
 *
 * Description: This file contains l2red specific proceduresf for cc task.
 *************************************************************************/

#include "cfminc.h"

/* Prototypes for private routines */
PRIVATE VOID EcfmRedStartMepCcTimer PROTO ((UINT4));
PRIVATE VOID EcfmRedUpdateMepInfo PROTO ((tRmMsg *, UINT2 *));
PRIVATE VOID EcfmRedUpdateRMepInfo PROTO ((tRmMsg *, UINT2 *));
PRIVATE VOID EcfmRedUpdateMipDbInfo PROTO ((tRmMsg *, UINT2 *));
PRIVATE VOID        EcfmRedFillMepInfo
PROTO ((UINT4, tEcfmCcMepInfo *, tRmMsg *, UINT2 *));
PRIVATE VOID        EcfmRedFillRMepInfo
PROTO ((UINT4, tEcfmCcRMepDbInfo *, tRmMsg *, UINT2 *));
PRIVATE VOID EcfmRedSendPeriodicUpdates PROTO ((VOID));
PRIVATE INT4 EcfmRedMakeNodeStandbyFromIdle PROTO ((VOID));
PRIVATE INT4 EcfmRedMakeNodeActive PROTO ((VOID));
PRIVATE VOID EcfmRedStartProtocolTimers PROTO ((VOID));
PRIVATE VOID EcfmRedProcessSyncMsg PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessPortOperStatus PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID        EcfmRedProcessLbrCacheBckUpStatus (tRmMsg *, UINT2, UINT2);
PRIVATE VOID EcfmRedProcessMipDbHldTmr PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedHandleGoStandbyEvent PROTO ((VOID));
PRIVATE VOID EcfmRedHandleStandByUpEvent PROTO ((VOID *));
PRIVATE VOID EcfmRedHandleBulkReqEvent PROTO ((VOID));
PRIVATE VOID EcfmCcRedHandleValidMessage PROTO ((tRmMsg *, UINT2));
PRIVATE INT4 EcfmRedSendBulkReq PROTO ((VOID));
PRIVATE INT4 EcfmRedSendBulkUpdateTailMsg PROTO ((VOID));
PRIVATE VOID EcfmRedTrigHigherLayer PROTO ((UINT1));
PRIVATE VOID EcfmRedProcessErrorLogEntry PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLckCondition PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLckStatus PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLckPeriodTmrExpiry PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessAisCondition PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessAisPeriodTmrExpiry PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessLmStopTransaction PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedUpdateLmBuffInfo PROTO ((tRmMsg *, UINT2 *, UINT2));
PRIVATE VOID EcfmRedClearLmSyncFlag PROTO ((VOID));
PRIVATE VOID        EcfmRedSyncLmTransStatus
PROTO ((UINT4, UINT4, UINT4, UINT2, UINT1));
PRIVATE VOID        EcfmRedProcessLmTransSts
PROTO ((tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length));
PRIVATE VOID EcfmRedUpdateErrLog PROTO ((tRmMsg *, UINT2 *));
PRIVATE VOID EcfmRedSyncErrLog PROTO ((VOID));
PRIVATE VOID EcfmRedSendRMEPUpdates PROTO ((VOID));
PRIVATE VOID EcfmCcRedHandleStandbyDown PROTO ((VOID));
PRIVATE INT4 EcfmRedProcessSynchSmData PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE INT4 EcfmRedProcessSyncCcDataOnChng PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedHandleTmrEvnt PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE INT4 EcfmRedProcessCcTxSeqNoMsg PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE INT4 EcfmRedProcessHwAuditInfo PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedHwAudit PROTO ((VOID));
PRIVATE VOID EcfmRedAuditMain PROTO ((INT1 *pi1Param));
#ifdef NPAPI_WANTED
PRIVATE VOID EcfmRedStartAudit PROTO ((VOID));
#endif
PRIVATE INT4 EcfmRedProcessOffTxFilterId PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE INT4 EcfmRedProcessOffRxFilterId PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE VOID EcfmRedProcessAvlbltyInfo PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE INT4 EcfmRedProcessHwTxHandler PROTO ((tRmMsg *, UINT2, UINT2));
PRIVATE INT4 EcfmRedProcessHwRxHandler PROTO ((tRmMsg *, UINT2, UINT2));

static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);

/*****************************************************************************/
/* Function Name      : EcfmRedSyncMipDbHldTmr                               */
/*                                                                           */
/* Description        : This function Sends MIB DB Remaing time to           */
/*                      the standby Node.                                    */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncMipDbHldTmr (UINT4 u4RemaimingTime)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node"
                      " needs to send the Mip Db Hold timer expiry. \r\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red sync up messages can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_MIB_DB_HLD_TMR_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    u2SyncMsgLen = ECFM_MIB_DB_HLD_TMR_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_RED_MIP_DB_HLD_TMR);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4RemaimingTime);

    if (u1MemEstFlag == ECFM_PORT_OPER_STATUS_MSG)
    {
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT4);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                    (CHR1 *) "tEcfmCcContextInfo",
                                    u4BulkUnitSize);
        u1MemEstFlag = ECFM_RED_MIP_DB_HLD_TMR;
    }

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncUpPortOperStatus                          */
/*                                                                           */
/* Description        : This function sends the port oper status to the      */
/*                      Standby.                                             */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Interface Index                        */
/*                      u1PortOperStatus - Port Oper Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE.                         */
/*****************************************************************************/
PUBLIC INT4
EcfmRedSyncUpPortOperStatus (UINT4 u4IfIndex, UINT1 u1PortOperStatus)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs"
                      " to send the SyncUp message. \r\n");

        /*Only the Active node needs to send the SyncUp message */
        return ECFM_SUCCESS;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red sync up messages can not"
                      "be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return ECFM_SUCCESS;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_PORT_OPER_STATUS_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        return ECFM_FAILURE;
    }
    u2SyncMsgLen = ECFM_PORT_OPER_STATUS_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_PORT_OPER_STATUS_MSG);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the port information to be synced up. */
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PortOperStatus);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSendMsgToRm                                   */
/*                                                                           */
/* Description        : This function constructs the RM message from the     */
/*                      given linear buf and sends it to Redundancy Manager. */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
EcfmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    /* Call the API provided by RM to send the data to RM */
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pMsg);
    UNUSED_PARAM (u4ByteCount);
    ECFM_GLB_PKT_DUMP (ECFM_DUMP_TRC, pMsg, u2BufSize,
                       "EcfmRedSendMsgToRm: Dumping Frame \r\n");
    if (EcfmRmEnqMsgToRmFromAppl
        (pMsg, u2BufSize, RM_ECFM_APP_ID, RM_ECFM_APP_ID) == RM_FAILURE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC
                      | ECFM_CONTROL_PLANE_TRC,
                      "Enqueue to RM from Appl failed\n");

        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmCcRedHandleRmEvents                              */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pEcfmMsg - Pointer to the  input buffer.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmCcRedHandleRmEvents (tEcfmCcMsg * pEcfmMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tEcfmNodeStatus     EcfmPrevNodeState = ECFM_NODE_IDLE;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;
#ifdef NPAPI_WANTED
    UINT4               u4CurrContextId = ECFM_INIT_VAL;
#endif
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    switch (pEcfmMsg->uMsg.RmFrame.u1Event)

    {
        case GO_ACTIVE:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event GO_ACTIVE \r\n");
            if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)
            {
                break;
            }
            EcfmPrevNodeState = ECFM_NODE_STATUS ();
            EcfmRedMakeNodeActive ();
            if (ECFM_BULK_REQ_RECD () == ECFM_TRUE)
            {
                ECFM_BULK_REQ_RECD () = ECFM_FALSE;
                gEcfmRedGlobalInfo.u2BulkUpdNextPort = 0;
                gEcfmRedGlobalInfo.u4BulkUpdNextContext = 0;
                EcfmRedHandleBulkUpdateEvent ();
            }

            if (EcfmPrevNodeState == ECFM_NODE_IDLE)
            {
                if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
                {
#ifdef NPAPI_WANTED
                    if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)

                    {
                         EcfmInitWithMBSMandRM ( u4CurrContextId );
                    }

#endif /* NPAPI_WANTED  */

                }

                else
                {
                    /*Check if any Standby Node is UP. */
                    if (ECFM_IS_STANDBY_UP () == ECFM_TRUE)

                    {
                        /* When the system comes up, EcfmCcModuleStart will be called.
                         * At this time the Node status may not be in Active State.
                         * So hw programming to create filters for ECFM packet might
                         * not have done.
                         * Hence when the node moves from IDLE to Active program the
                         * filters for ECFM packets. */

                        /* In case of MBSM filters are installed on card insertion
                         * event */
#ifndef MBSM_WANTED
#ifdef NPAPI_WANTED
                        if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)

                        {
                             EcfmInitWithMBSMandRM ( u4CurrContextId );
                        }

#endif /* NPAPI_WANTED  */
#endif /*MBSM_WANTED */
			}
			else
			{
#ifndef MBSM_WANTED
#ifdef NPAPI_WANTED
				if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
				{                                                                                                                                                                 EcfmInitWithMBSMandRM ( u4CurrContextId );
				}
#endif /* NPAPI_WANTED  */
#endif /*MBSM_WANTED */
			}
		}
		ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;

            }
            else

            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }
            EcfmRmHandleProtocolEvent (&ProtoEvt);
            break;
        case GO_STANDBY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event GO_STANDBY \r\n");
            if (ECFM_NODE_STATUS () == ECFM_NODE_STANDBY)
            {
                break;
            }
#ifdef NPAPI_WANTED
            if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
            {
                /* In Dual Unit Stacking , Hardware Initializations has to
                   be done even in Stand-by Node */
                if (EcfmPrevNodeState == ECFM_NODE_IDLE)
                {
                    EcfmFsMiEcfmHwInit (ECFM_DEFAULT_CONTEXT);
                }
            }
#endif
            if (ECFM_NODE_STATUS () == ECFM_NODE_IDLE)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Ignoring this event, Node will become"
                              "Standby once Static Configuration is complete\r\n");
                return;
            }

            else if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)

            {
                tEcfmCcPduSmInfo    PduSmInfo;
                ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                             sizeof (tEcfmCcPduSmInfo));

                EcfmRedHandleGoStandbyEvent ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                EcfmRmHandleProtocolEvent (&ProtoEvt);
            }
            ECFM_BULK_REQ_RECD () = ECFM_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            /* Hw Audit is not implemented for ECFM */
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event RM_CONFIG_RESTORE_COMPLETE \r\n");
            if (ECFM_NODE_STATUS () == ECFM_NODE_IDLE)

            {
                if (EcfmRmGetNodeState () == RM_STANDBY)

                {
                    ECFM_LBLT_LOCK ();
                    ECFM_NODE_STATUS () = ECFM_NODE_STANDBY;
                    ECFM_LBLT_UNLOCK ();
                    EcfmRedMakeNodeStandbyFromIdle ();
                }
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                EcfmRmHandleProtocolEvent (&ProtoEvt);
            }
            break;
        case RM_STANDBY_UP:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event RM_STANDBY_UP \r\n");
            pData = (tRmNodeInfo *) pEcfmMsg->uMsg.RmFrame.pFrame;
            ECFM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            EcfmRmReleaseMemoryForMsg ((UINT1 *) pData);
            EcfmRedHandleStandByUpEvent (NULL);
            break;
        case RM_STANDBY_DOWN:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event RM_STANDBY_DOWN \r\n");

            {
                tEcfmCcPduSmInfo    PduSmInfo;
                ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL,
                             sizeof (tEcfmCcPduSmInfo));
                pData = (tRmNodeInfo *) pEcfmMsg->uMsg.RmFrame.pFrame;
                ECFM_NUM_STANDBY_NODES () = pData->u1NumStandby;

                EcfmRmReleaseMemoryForMsg ((UINT1 *) pData);
                EcfmCcRedHandleStandbyDown ();
                break;
            }
        case RM_COMPLETE_SYNC_UP:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event RM_COMPLETE_SYNC_UP \r\n");

#if defined (CFA_WANTED) || defined (EOAM_WANTED) || defined (PNAC_WANTED) ||\
                defined (LA_WANTED) || defined (RSTP_WANTED) || (VLAN_WANTED)
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Ignoring RM_COMPLETE_SYNC_UP event"
                          "Waiting for L2_COMPLETE_SYNC_UP event from lower layer"
                          "\r\n");
            break;

#else
            EcfmRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;
#endif
        case L2_COMPLETE_SYNC_UP:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event L2_COMPLETE_SYNC_UP \r\n");
            EcfmRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;
        case L2_INITIATE_BULK_UPDATES:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event L2_INITIATE_BULK_UPDATES \r\n");
            EcfmRedSendBulkReq ();
            break;
        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM ((tRmMsg *) (pEcfmMsg->uMsg.RmFrame.pFrame),
                               &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR ((tRmMsg *) (pEcfmMsg->uMsg.RmFrame.pFrame),
                                 pEcfmMsg->uMsg.RmFrame.u2Length);

            ProtoAck.u4AppId = RM_ECFM_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Event RM_MESSAGE \r\n");
            EcfmCcRedHandleValidMessage ((tRmMsg *) pEcfmMsg->uMsg.
                                         RmFrame.pFrame,
                                         pEcfmMsg->uMsg.RmFrame.u2Length);

            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (pEcfmMsg->uMsg.RmFrame.pFrame));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Unknown event from RM =[%d]\n",
                          pEcfmMsg->uMsg.RmFrame.u1Event);
            break;
    }
}

/*****************************************************************************/
/* Function Name      : ECFMRedMakeNodeStandbyFromIdle.                      */
/*                                                                           */
/* Description        : This function makes the node Standby from Idle state */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS /ECFM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
EcfmRedMakeNodeStandbyFromIdle (VOID)
{
    ECFM_INIT_NUM_PEERS ();
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "ECFM Node made STANDBY from IDLE state.\n");
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedMakeNodeActive.                               */
/*                                                                           */
/* Description        : This function brings up the standby card to the      */
/*                      same state as the ACTIVE card. It will Enable ECFM   */
/*                      Module                                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          */
/*****************************************************************************/
PRIVATE INT4
EcfmRedMakeNodeActive (VOID)
{
    UINT4               u4ContextId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)

    {
        return ECFM_SUCCESS;
    }
    if (!ECFM_IS_SYSTEM_INITIALISED ())

    {
        ECFM_LBLT_LOCK ();
        ECFM_NODE_STATUS () = ECFM_NODE_ACTIVE;
        ECFM_LBLT_UNLOCK ();
        ECFM_RM_GET_NUM_PEERS_UP ();
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC
                      | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedMakeNodeActive : ECFM MODULE -  not Started \r\n");
        return ECFM_SUCCESS;
    }
    if (ECFM_NODE_STATUS () == ECFM_NODE_IDLE)

    {
        ECFM_RED_AUDIT_FLAG () = ECFM_RED_AUDIT_COMPLETED;    /* No need for audit */
        for (u4ContextId = 0; u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

        {
            if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

            {
                continue;
            }
            if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))

            {
                ECFM_CC_RELEASE_CONTEXT ();
                continue;
            }

            /* If ECFM Module is enabled then also enable it in this node */
            if (ECFM_IS_MODULE_ENABLED (u4ContextId))

            {
                if (EcfmUtilModuleEnable () != ECFM_SUCCESS)

                {
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                                  ECFM_CONTROL_PLANE_TRC |
                                  ECFM_ALL_FAILURE_TRC,
                                  "EcfmRedMakeNodeActive:ECFM MODULE cannot be Enabled"
                                  "\r\n");
                }
            }
            ECFM_CC_RELEASE_CONTEXT ();
        }

        /* Make the node as active. */
        ECFM_LBLT_LOCK ();
        ECFM_NODE_STATUS () = ECFM_NODE_ACTIVE;

        /* Start LBR Hold Timer & LBLT Delay queue Timer */
        EcfmRedStartLBLTTimersOnActiveEvent ();
        ECFM_LBLT_UNLOCK ();
        ECFM_RM_GET_NUM_PEERS_UP ();
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedMakeNodeActive:ECFM Node made ACTIVE \r\n");
        return ECFM_SUCCESS;
    }
    if (ECFM_NODE_STATUS () == ECFM_NODE_STANDBY)

    {
        ECFM_LBLT_LOCK ();
        ECFM_NODE_STATUS () = ECFM_NODE_ACTIVE;
        ECFM_LBLT_UNLOCK ();
        ECFM_RM_GET_NUM_PEERS_UP ();

        if (ECFM_RED_AUDIT_FLAG () != ECFM_RED_AUDIT_STARTED)
        {
            ECFM_RED_AUDIT_FLAG () = ECFM_RED_AUDIT_NOT_STARTED;    /* AUDIT yet to be
                                                                       started */
        }
        /* Start Protocol Timers for every context */
        EcfmRedStartProtocolTimers ();
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedMakeNodeActive:ECFM Node made ACTIVE \r\n");
        /*Audit will take place if ECFM is started in atleast one context */
        EcfmRedHwAudit ();
        return ECFM_SUCCESS;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedStartProtocolTimers                           */
/*                                                                           */
/* Description        : This function starts the protocol timers when        */
/*                      Standby Nodes gets Active                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
EcfmRedStartProtocolTimers ()
{
    UINT4               u4CurrentTime = ECFM_INIT_VAL;
    UINT4               u4DiffTime = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    INT4                i4LeftOutDuration = ECFM_INIT_VAL;
    BOOL1               b1TimerStarted = ECFM_FALSE;
    ECFM_GET_SYS_TIME (&u4CurrentTime);
    for (u4ContextId = 0; u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            continue;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Starting protocol timers for context = [%d] \r\n",
                      u4ContextId);

        /* Start CC/LCK/AIS MEP Timers */
        EcfmRedStartMepCcTimer (u4CurrentTime);
        b1TimerStarted = ECFM_FALSE;

        /*Start MIP-DB Hold Timer */
        if (ECFM_CC_CURR_CONTEXT_INFO ()->u4RecvdMipDbHldTime != ECFM_INIT_VAL)

        {
            u4DiffTime = (u4CurrentTime -
                          (ECFM_CC_CURR_CONTEXT_INFO ()->TimeStamp));
            i4LeftOutDuration =
                ECFM_CC_CURR_CONTEXT_INFO ()->u4RecvdMipDbHldTime - u4DiffTime;
            if (i4LeftOutDuration > 0)

            {
                EcfmCcTmrStartTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL,
                                     ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4)
                                                                      (i4LeftOutDuration)));

                /* set the Timer-Started flag to true */
                b1TimerStarted = ECFM_TRUE;
            }
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Mip Ccm DB Left-Out Time = [%d] \r\n",
                          i4LeftOutDuration);
        }

        /* Check is MIP CCM-DB is enabled, then start fresh timer */
        if ((ECFM_IS_MIP_CCM_DB_ENABLED () == ECFM_TRUE)
            && (b1TimerStarted == ECFM_FALSE))

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Starting fresh MIP-CCM-DB timer \r\n");
            EcfmCcTmrStartTimer (ECFM_CC_TMR_MIP_DB_HOLD, NULL,
                                 ECFM_CONVERT_HRS_TO_MSEC
                                 (ECFM_CC_MIP_CCM_DB_HOLD_TIME));
        }
        ECFM_CC_CURR_CONTEXT_INFO ()->u4RecvdMipDbHldTime = 0;

        /*Start Ltr Cache Hold Timer */
        /* Start LBLT MEP Spoecific Timers */
        EcfmRedStartMepLbLtTimers (u4ContextId, u4CurrentTime);
        ECFM_CC_RELEASE_CONTEXT ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedStartMepCcTimer                               */
/*                                                                           */
/* Description        : This function starts the CC Related Timer for all the*/
/*                      Active MEPs when Standby Node gets Active            */
/*                                                                           */
/* Input(s)           : u4CurrentTime - Current Time when the Timer needs    */
/*                                      to be started                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
EcfmRedStartMepCcTimer (UINT4 u4CurrentTime)
{
    tEcfmCcPduSmInfo    PduSmInfo;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcMdInfo      *pMdInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    INT4                i4LeftOutDuration = ECFM_INIT_VAL;

    /* Start Mep related Timers */
    pMepInfo = RBTreeGetFirst (ECFM_CC_MEP_TABLE);
    if (pMepInfo == NULL)

    {
        return;
    }
    pMdInfo = ECFM_CC_GET_MDINFO_FROM_MEP (pMepInfo);

    /* Check if Archive hold timer is not running */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, &(pMdInfo->MepArchiveHoldTimer.TimerNode),
         &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }

    if (u4RemainingTime == 0)

    {
        UINT4               u4TimeTick = ECFM_INIT_VAL;
        UINT4               u4TimeInMsc = ECFM_INIT_VAL;

        /* Start Mep Archive Hold Timer */
        u4TimeTick =
            ECFM_CONVERT_SEC_TO_TIME_TICKS (pMdInfo->u2MepArchiveHoldTime * 60);
        u4TimeInMsc = ECFM_CONVERT_TIME_TICKS_TO_MSEC (u4TimeTick);
        PduSmInfo.pMdInfo = pMdInfo;
        EcfmCcTmrStartTimer (ECFM_CC_TMR_MEP_ARCHIVE_HOLD, &PduSmInfo,
                             u4TimeInMsc);
        PduSmInfo.pMdInfo = NULL;
    }
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    while (pMepInfo != NULL)

    {
        PduSmInfo.pMepInfo = pMepInfo;
        if (pMepInfo->CcInfo.XConTimeStamp != ECFM_INIT_VAL)

        {
            if (u4CurrentTime >= pMepInfo->CcInfo.XConTimeStamp)
            {
                ECFM_CC_MEP_XCON_SET_STATE (pMepInfo,
                                            ECFM_MEP_XCON_STATE_NO_DEFECT);
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Xcon Ccm expired XCON defect cleared \r\n");
            }
            else
            {
                i4LeftOutDuration =
                    (pMepInfo->CcInfo.XConTimeStamp - u4CurrentTime);

                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_XCON_CCM_WHILE,
                                     &PduSmInfo,
                                     ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4)
                                                                      (i4LeftOutDuration)));
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Xcon Ccm While Left-Out Time = [%d] \r\n",
                              i4LeftOutDuration);
            }
            pMepInfo->CcInfo.XConTimeStamp = 0;
        }

        /* Check if Error CCM While Timer is running or not */
        if (pMepInfo->CcInfo.ErrTimeStamp != ECFM_INIT_VAL)

        {
            if (u4CurrentTime >= pMepInfo->CcInfo.ErrTimeStamp)
            {
                ECFM_CC_RMEP_ERR_SET_STATE (pMepInfo,
                                            ECFM_RMEP_ERR_STATE_NO_DEFFECT);
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Err Ccm expired ERR defect cleared \r\n");
            }
            else
            {
                i4LeftOutDuration =
                    (pMepInfo->CcInfo.ErrTimeStamp - u4CurrentTime);

                /*  Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_ERR_CCM_WHILE, &PduSmInfo,
                                     ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4)
                                                                      (i4LeftOutDuration)));
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Err Ccm While Left-Out Time = [%d] \r\n",
                              i4LeftOutDuration);
            }
            pMepInfo->CcInfo.ErrTimeStamp = 0;
        }

        /* Check if CCI While Timer is running or not */
        if (pMepInfo->CcInfo.b1CciEnabled == ECFM_TRUE)

        {
            UINT4               u4CcmInterval = ECFM_INIT_VAL;

            /* get the received CCI Interval */
            ECFM_GET_CCM_INTERVAL (pMepInfo->pMaInfo->u1CcmInterval,
                                   u4CcmInterval);
            u4CcmInterval = ECFM_CONVERT_MSEC_TO_TIME_TICKS (u4CcmInterval);

            if (u4CurrentTime > pMepInfo->CcInfo.CciTimeStamp)
            {
                tEcfmCcMepCcInfo   *pCcInfo = NULL;
                UINT4               u4TimeDIff;

                u4TimeDIff = u4CurrentTime - pMepInfo->CcInfo.CciTimeStamp;
                pCcInfo = &(pMepInfo->CcInfo);

                if (u4TimeDIff > u4CcmInterval)
                {
                    i4LeftOutDuration = (u4TimeDIff) % u4CcmInterval;
                    i4LeftOutDuration = u4CcmInterval - i4LeftOutDuration;
                    ECFM_INCR ((pCcInfo->u4CciSentCcms),
                               (u4TimeDIff / u4CcmInterval),
                               (ECFM_UINT4_MAX), 1);
                    ECFM_CC_INCR_CCM_SEQ_NUMBER (pCcInfo);
                    pCcInfo->u4CciCcmCount++;
                }
                else
                {
                    i4LeftOutDuration = u4CcmInterval - u4TimeDIff;
                    ECFM_CC_INCR_CCM_SEQ_NUMBER (pCcInfo);
                    pCcInfo->u4CciCcmCount++;
                }
            }
            else
            {
                i4LeftOutDuration =
                    (pMepInfo->CcInfo.CciTimeStamp - u4CurrentTime);
            }

            if (i4LeftOutDuration > 0)
            {
                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_CCI_WHILE, &PduSmInfo,
                                     ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4)
                                                                      (i4LeftOutDuration)));
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Cci While Left-Out Time = [%d] \r\n",
                              i4LeftOutDuration);
            }
            else

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Start Fresh CCI-While Timer \r\n");
                ECFM_GET_CCM_INTERVAL (pMepInfo->pMaInfo->u1CcmInterval,
                                       u4CcmInterval);

                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_CCI_WHILE, &PduSmInfo,
                                     u4CcmInterval);

                pMepInfo->CcInfo.CciTimeStamp = 0;
            }
        }
        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
        {

            /* Start RDI Period Timer */
            if (pMepInfo->CcInfo.RdiTimeStamp != ECFM_INIT_VAL)
            {
                if (u4CurrentTime >= pMepInfo->CcInfo.RdiTimeStamp)
                {
                    i4LeftOutDuration = 0;
                }
                else
                {
                    i4LeftOutDuration =
                        pMepInfo->CcInfo.RdiTimeStamp - u4CurrentTime;
                }
                if (i4LeftOutDuration > 0)
                {

                    /* Start Timer */
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_RDI_PERIOD,
                                         &PduSmInfo,
                                         ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration)));
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                                  "RDI Capability Timer Left-Out Time = [%d] \r\n",
                                  i4LeftOutDuration);
                }

                else
                {
                    /* RDI Period is Timeout while Switchover period */
                    EcfmCcRdiPeriodTimeout (pMepInfo);
                }
                pMepInfo->CcInfo.RdiTimeStamp = 0;
            }

            else if (pMepInfo->CcInfo.u4RdiCapPeriod != 0)
            {
                /* Start the RDI Period Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_RDI_PERIOD, &PduSmInfo,
                                     pMepInfo->CcInfo.u4RdiCapPeriod * 1000);
            }

            /* Check for Out-Of-Service MEP, and set Delay Expired accordingly */
            if (pMepInfo->LckInfo.b1OutOfService == ECFM_TRUE)

            {
                /* Call the Lck Delay Timeout */
                EcfmCcUpdateLckStatus (pMepInfo);
            }
            /* Start LCK Period Timer */
            if (pMepInfo->LckInfo.LckPrdTimeStamp != ECFM_INIT_VAL)
            {
                if (u4CurrentTime >= pMepInfo->LckInfo.LckPrdTimeStamp)
                {
                    i4LeftOutDuration = 0;
                }
                else
                {
                    i4LeftOutDuration =
                        pMepInfo->LckInfo.LckPrdTimeStamp - u4CurrentTime;
                }
                if (i4LeftOutDuration > 0)
                {
                    /* Start LCK Period Timer */
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_PERIOD,
                                         &PduSmInfo,
                                         ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration)));
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                                  "LCK Period Timer Left-Out Time = [%d] \r\n",
                                  i4LeftOutDuration);
                }

                else
                {
                    /* Timeout while Switchover, handle Timeout */
                    EcfmCcLckPeriodTimeout (pMepInfo);
                }
                pMepInfo->LckInfo.LckPrdTimeStamp = 0;
            }
            else if ((pMepInfo->LckInfo.u4LckPeriod != 0) &&
                     (pMepInfo->LckInfo.b1OutOfService == ECFM_TRUE))

            {
                EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_PERIOD, &PduSmInfo,
                                     (UINT4) (pMepInfo->LckInfo.u4LckPeriod
                                              * ECFM_NUM_OF_MSEC_IN_A_SEC));
            }

            /* Start LCK Interval Timer */
            if (pMepInfo->LckInfo.LckIntervalTimeStamp != ECFM_INIT_VAL)
            {
                ECFM_GET_AIS_LCK_INTERVAL (pMepInfo->LckInfo.u1LckInterval,
                                           u4Interval);
                u4Interval = ECFM_CONVERT_MSEC_TO_TIME_TICKS (u4Interval);

                if (u4CurrentTime > pMepInfo->LckInfo.LckIntervalTimeStamp)
                {
                    if ((u4CurrentTime -
                         (pMepInfo->LckInfo.LckIntervalTimeStamp)) > u4Interval)
                    {
                        i4LeftOutDuration = (u4CurrentTime -
                                             (pMepInfo->LckInfo.
                                              LckIntervalTimeStamp)) %
                            u4Interval;
                        i4LeftOutDuration = u4Interval - i4LeftOutDuration;
                    }
                    else
                    {
                        i4LeftOutDuration = u4Interval -
                            (u4CurrentTime -
                             pMepInfo->LckInfo.LckIntervalTimeStamp);
                    }
                }
                else

                {
                    i4LeftOutDuration =
                        (pMepInfo->LckInfo.LckIntervalTimeStamp -
                         u4CurrentTime);
                }

                if (i4LeftOutDuration > 0)
                {

                    /* Start Timer */
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_INTERVAL,
                                         &PduSmInfo,
                                         ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration)));
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                                  "LCK Rx Interval Timer Left-Out Time = [%d] \r\n",
                                  i4LeftOutDuration);
                }

                else

                {
                    if (pMepInfo->LckInfo.b1OutOfService != ECFM_FALSE)

                    {
                        /* Call the Lck Interval Timeout */
                        EcfmCcClntLckInitiator (pMepInfo,
                                                ECFM_EV_LCK_INTERVAL_EXPIRY);
                    }
                }
                pMepInfo->LckInfo.LckIntervalTimeStamp = ECFM_INIT_VAL;
            }
            else
            {
                if (pMepInfo->LckInfo.b1OutOfService == ECFM_TRUE)

                {
                    u4Interval = ECFM_INIT_VAL;

                    /* Start Timer */
                    ECFM_GET_AIS_LCK_INTERVAL (pMepInfo->LckInfo.
                                               u1LckInterval, u4Interval);
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_INTERVAL, &PduSmInfo,
                                         u4Interval);
                }
            }

            /* Start LCK Rx While Timer */
            if (pMepInfo->LckInfo.b1LckCondition == ECFM_TRUE)
            {

                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_LCK_RXWHILE,
                                     &PduSmInfo,
                                     pMepInfo->LckInfo.u4LckRcvdRxWhileValue);
            }

            /* Start AIS Period Timer */
            if (pMepInfo->AisInfo.AisPrdTimeStamp != ECFM_INIT_VAL)
            {
                if (u4CurrentTime >= pMepInfo->AisInfo.AisPrdTimeStamp)
                {
                    i4LeftOutDuration = 0;
                }
                else
                {
                    i4LeftOutDuration =
                        pMepInfo->AisInfo.AisPrdTimeStamp - u4CurrentTime;
                }
                if (i4LeftOutDuration > 0)
                {
                    /* Start AIS Period Timer */
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_PERIOD,
                                         &PduSmInfo,
                                         ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration)));
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                                  "AIS Period Timer Left-Out Time = [%d] \r\n",
                                  i4LeftOutDuration);
                }

                else
                {
                    /* Time out while switch over, handle timeout */
                    EcfmCcAisPeriodTimeout (pMepInfo);
                }
                pMepInfo->AisInfo.AisPrdTimeStamp = 0;
            }

            else if ((pMepInfo->AisInfo.u4AisPeriod != 0) &&
                     (pMepInfo->AisInfo.u1AisCapability == ECFM_ENABLE))
            {
                EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_PERIOD, &PduSmInfo,
                                     (pMepInfo->AisInfo.u4AisPeriod *
                                      ECFM_NUM_OF_MSEC_IN_A_SEC));
            }

            /* Start AIS Interval Timer */
            if (pMepInfo->AisInfo.AisIntervalTimeStamp != ECFM_INIT_VAL)

            {
                ECFM_GET_AIS_LCK_INTERVAL (pMepInfo->AisInfo.u1AisInterval,
                                           u4Interval);
                u4Interval = ECFM_CONVERT_MSEC_TO_TIME_TICKS (u4Interval);

                if (u4CurrentTime > pMepInfo->AisInfo.AisIntervalTimeStamp)
                {
                    if ((u4CurrentTime -
                         (pMepInfo->AisInfo.AisIntervalTimeStamp)) > u4Interval)
                    {
                        i4LeftOutDuration = (u4CurrentTime -
                                             (pMepInfo->AisInfo.
                                              AisIntervalTimeStamp)) %
                            u4Interval;
                        i4LeftOutDuration = u4Interval - i4LeftOutDuration;
                    }
                    else
                    {
                        i4LeftOutDuration = u4Interval -
                            (u4CurrentTime -
                             pMepInfo->AisInfo.AisIntervalTimeStamp);
                    }
                }
                else
                {
                    i4LeftOutDuration =
                        (pMepInfo->AisInfo.AisIntervalTimeStamp -
                         u4CurrentTime);
                }
                if (i4LeftOutDuration > 0)
                {
                    /* Start Timer */
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_INTERVAL,
                                         &PduSmInfo,
                                         ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration)));
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                                  "AIS  Interval Timer Left-Out Time = [%d] \r\n",
                                  i4LeftOutDuration);
                }
                else
                {
                    if (pMepInfo->AisInfo.u1AisCapability == ECFM_ENABLE)

                    {
                        /* Call the Ais Interval Timeout */
                        EcfmCcClntAisInitiator (pMepInfo,
                                                ECFM_EV_AIS_INTERVAL_EXPIRY);
                    }
                }
                pMepInfo->AisInfo.AisIntervalTimeStamp = ECFM_INIT_VAL;
            }
            else
            {
                if (ECFM_CC_IS_AIS_ENABLED (pMepInfo) == ECFM_TRUE)

                {
                    u4Interval = ECFM_INIT_VAL;

                    /* Start Timer */
                    ECFM_GET_AIS_LCK_INTERVAL (pMepInfo->AisInfo.
                                               u1AisInterval, u4Interval);
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_INTERVAL, &PduSmInfo,
                                         u4Interval);
                }
            }

            /* Start AIS Rx While Timer */
            if (ECFM_CC_IS_AIS_ENABLED (pMepInfo) == ECFM_TRUE)
            {
                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_AIS_RXWHILE,
                                     &PduSmInfo,
                                     pMepInfo->AisInfo.u4AisRcvdRxWhileValue);
            }

            /* Start LM Init While Timer */
            if (pMepInfo->LmInfo.u1TxLmmStatus == ECFM_TX_STATUS_NOT_READY)

            {
                u4Interval = ECFM_INIT_VAL;

                /* Convert time interval code into duration in msec */
                ECFM_GET_LMM_INTERVAL (pMepInfo->LmInfo.u2TxLmmInterval,
                                       u4Interval);
                EcfmCcTmrStartTimer (ECFM_CC_TMR_LM_WHILE, &PduSmInfo,
                                     u4Interval);
                if (ECFM_GET_INFINITE_TX_STATUS
                    (pMepInfo->LmInfo.u2TxLmmMessages) == ECFM_TRUE)
                {
                    if (pMepInfo->LmInfo.u4TxLmmDeadline != ECFM_INIT_VAL)
                    {

                        /* Start Deadline Timer with configured value */
                        EcfmCcTmrStartTimer (ECFM_CC_TMR_LM_DEADLINE,
                                             &PduSmInfo,
                                             (ECFM_NUM_OF_MSEC_IN_A_SEC *
                                              pMepInfo->LmInfo.
                                              u4TxLmmDeadline));
                    }
                }
            }
        }

        /* Check if FNG Alarm While Timer is running or not */
        if (pMepInfo->FngInfo.FngWhileTimeStamp != ECFM_INIT_VAL)
        {
            if (u4CurrentTime >= pMepInfo->FngInfo.FngWhileTimeStamp)
            {
                i4LeftOutDuration = 0;
            }
            else
            {
                i4LeftOutDuration =
                    pMepInfo->FngInfo.FngWhileTimeStamp - u4CurrentTime;
            }
            if (i4LeftOutDuration > 0)

            {

                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_FNG_WHILE, &PduSmInfo,
                                     ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4)
                                                                      (i4LeftOutDuration)));
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "Fng Alarm Left-Out Time = [%d] \r\n",
                              i4LeftOutDuration);
            }
            pMepInfo->FngInfo.FngWhileTimeStamp = 0;
        }

        if (pMepInfo->FngInfo.FngRstWhileTimeStamp != ECFM_INIT_VAL)
        {
            if (u4CurrentTime >= pMepInfo->FngInfo.FngRstWhileTimeStamp)
            {
                i4LeftOutDuration = 0;
            }
            else

            {
                i4LeftOutDuration =
                    pMepInfo->FngInfo.FngRstWhileTimeStamp - u4CurrentTime;
            }
            if (i4LeftOutDuration > 0)
            {

                /* Start Timer */
                EcfmCcTmrStartTimer (ECFM_CC_TMR_FNG_RESET_WHILE,
                                     &PduSmInfo,
                                     ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4)
                                                                      (i4LeftOutDuration)));
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "FNG Reset Left-Out Time = [%d] \r\n",
                              i4LeftOutDuration);
            }
            pMepInfo->FngInfo.FngRstWhileTimeStamp = 0;
        }

        pRMepInfo =
            (tEcfmCcRMepDbInfo *) TMO_DLL_First (&(PduSmInfo.pMepInfo->RMepDb));
        while (pRMepInfo != NULL)
        {
            /*Reset Seq Number to ignore first PDU received */
            pRMepInfo->u4SeqNum = ECFM_INIT_VAL;

            /* Start RMEP While Timer */
            if (pRMepInfo->RMepTimeStamp != ECFM_INIT_VAL)
            {
                if (u4CurrentTime >= pRMepInfo->RMepTimeStamp)
                {
                    i4LeftOutDuration = 0;
                }
                else
                {
                    i4LeftOutDuration =
                        pRMepInfo->RMepTimeStamp - u4CurrentTime;
                }
                if (i4LeftOutDuration > 0)
                {
                    /* Start Timer */
                    PduSmInfo.pRMepInfo = pRMepInfo;
                    EcfmCcTmrStartTimer (ECFM_CC_TMR_RMEP_WHILE,
                                         &PduSmInfo,
                                         ECFM_CONVERT_TIME_TICKS_TO_MSEC ((UINT4) (i4LeftOutDuration)));
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                                  "FNG Reset Left-Out Time = [%d] \r\n",
                                  i4LeftOutDuration);
                }
                pRMepInfo->RMepTimeStamp = 0;
            }
            /* Get the next node form the tree */
            pRMepInfo =
                (tEcfmCcRMepDbInfo *)
                TMO_DLL_Next (&(PduSmInfo.pMepInfo->RMepDb),
                              &(pRMepInfo->MepDbDllNode));

        }

        PduSmInfo.pMepInfo = NULL;

        /* Get Next MEP from the Global RBTree maintained */
        pMepInfo = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepInfo, NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedRcvPktFromRm                                  */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to ECFM    */
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event sent then ECFM_SUCCESS  */
/*                      Otherwise ECFM_FAILURE                               */
/****************************************************************************/
PUBLIC INT4
EcfmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tEcfmCcMsg         *pCcMsg = NULL;
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmRedRcvPktFromRm: event-id = %d \r\n", u1Event);
    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP)
         || (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))

    {

        /* Message absent and hence no need to process and no
         * need to send anything to ECFM tasks. */
        return RM_FAILURE;
    }
    if (!ECFM_IS_SYSTEM_INITIALISED ())

    {

        /* ECFM is not started and hence the mempool for interface
         * messages and the queue will not be present. Hence return
         * from this place. */
        if (u1Event == RM_MESSAGE)

        {
            RM_FREE (pData);
        }

        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))

        {
            EcfmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    /*Check if the messege needs to be sent to LBLT Module */
    if (u1Event == RM_MESSAGE)
    {
        UINT1               u1MsgType = ECFM_INIT_VAL;
        UINT2               u2Offset = ECFM_INIT_VAL;

        u2Offset = RM_HDR_LENGTH;

        ECFM_RM_GET_1_BYTE (pData, &u2Offset, u1MsgType);
        if ((u1MsgType == ECFM_RED_SYNC_LBLT_PDU) ||
            (u1MsgType == ECFM_RED_LTR_CACHE_HLD_TMR) ||
            (u1MsgType == ECFM_RED_LBR_CACHE_HLD_TMR) ||
            (u1MsgType == ECFM_RED_DELAY_QUEUE_TIMER_EXPIRY) ||
            (u1MsgType == ECFM_RED_LTT_TIMER_EXPIRY) ||
            (u1MsgType == ECFM_RED_LBI_TIMER_EXPIRY) ||
            (u1MsgType == ECFM_RED_LBI_STOP_TX) ||
            (u1MsgType == ECFM_RED_DM_STOP_TRANSACTION) ||
            (u1MsgType == ECFM_RED_DM_DEADLINE_TIMER_EXPIRY) ||
            (u1MsgType == ECFM_RED_1DM_TRANS_INT_EXPIRY) ||
            (u1MsgType == ECFM_RED_LBLT_SYNC_UPD_MSG) ||
            (u1MsgType == ECFM_RED_LBLT_SYNC_TRANS_STS) ||
            (u1MsgType == ECFM_RED_TST_TIMER_EXPIRY) ||
            (u1MsgType == ECFM_RED_TST_STOP_TX) ||
            (u1MsgType == ECFM_RED_STOP_THROUGHPUT_TRANS))

        {
            tEcfmLbLtMsg       *pLbLtMsg = NULL;

            /*Allocate Msg-Block From LBLT Pool */
            if (ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ (pLbLtMsg) == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC
                              | ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmRedRcvPktFromRm: LBLT Message ALLOC_MEM_BLOCK FAILED"
                              "for event \r\n");
                RM_FREE (pData);
                return RM_FAILURE;
            }
            ECFM_MEMSET (pLbLtMsg, 0, sizeof (tEcfmLbLtMsg));
            pLbLtMsg->MsgType = (tEcfmMsgType) ECFM_RM_FRAME;
            pLbLtMsg->uMsg.RmFrame.u1Event = u1Event;
            pLbLtMsg->uMsg.RmFrame.pFrame = pData;
            pLbLtMsg->uMsg.RmFrame.u2Length = u2DataLen;
            if (ECFM_ENQUE_MSG (ECFM_LBLT_CFG_QUEUE_ID,
                                (UINT1 *) (&pLbLtMsg), OSIX_DEF_MSG_LEN)
                != ECFM_SUCCESS)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmRedRcvPktFromRm:RM LBLT Message Enqueue FAILED \r\n");
                ECFM_FREE_MEM_BLOCK (ECFM_LBLT_MSGQ_POOL, (UINT1 *) pLbLtMsg);
                RM_FREE (pData);
                return RM_FAILURE;
            }

            /* Sent the event to ECFM CC Task. */
            if (ECFM_SEND_EVENT
                (ECFM_LBLT_TASK_ID, ECFM_EV_CFG_MSG_IN_QUEUE) != ECFM_SUCCESS)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                              ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                              "EcfmRedRcvPktFromRm: RM Event send FAILED \r\n");
                return RM_FAILURE;
            }
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "EcfmRedRcvPktFromRm: message-id queued successfully to LBLT = %d \r\n",
                          u1MsgType);
            return RM_SUCCESS;
        }
    }
    if (ECFM_ALLOC_MEM_BLOCK_CC_MSGQ (pCcMsg) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC
                      | ECFM_BUFFER_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedRcvPktFromRm: Message ALLOC_MEM_BLOCK FAILED"
                      "for event \r\n");
        if (u1Event == RM_MESSAGE)

        {
            RM_FREE (pData);
        }

        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))

        {
            EcfmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }
    ECFM_MEMSET (pCcMsg, 0, sizeof (tEcfmCcMsg));
    pCcMsg->MsgType = (tEcfmMsgType) ECFM_RM_FRAME;
    pCcMsg->uMsg.RmFrame.u1Event = u1Event;
    pCcMsg->uMsg.RmFrame.pFrame = pData;
    pCcMsg->uMsg.RmFrame.u2Length = u2DataLen;
    if (ECFM_ENQUE_MSG
        (ECFM_CC_CFG_QUEUE_ID, (UINT1 *) (&pCcMsg),
         OSIX_DEF_MSG_LEN) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedRcvPktFromRm:RM Message enqueue FAILED \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_MSGQ_POOL, (UINT1 *) pCcMsg);
        if (u1Event == RM_MESSAGE)

        {
            RM_FREE (pData);
        }

        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))

        {
            EcfmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    /* Sent the event to ECFM CC Task. */
    if (ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_CFG_MSG_IN_QUEUE)
        != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedRcvPktFromRm: RM Event send FAILED \r\n");
        return RM_FAILURE;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "EcfmRedRcvPktFromRm: queued successfully to cc \r\n");
    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers ECFM with RM by providing an application   */
/*                      ID for ECFM and a call back function to be called    */
/*                      whenever RM needs to send an event to ECFM.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then ECFM_SUCCESS         */
/*                      Otherwise ECFM_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
EcfmRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;
    RmRegParams.u4EntId = RM_ECFM_APP_ID;
    RmRegParams.pFnRcvPkt = EcfmRedRcvPktFromRm;
    if (EcfmRmRegisterProtocols (&RmRegParams) == RM_FAILURE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedRegisterWithRM: Registration with RM FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

 /*****************************************************************************/
/* Function Name      : EcfmRedDeRegisterWithRM                              */
/*                                                                           */
/* Description        : Deregisters ECFM with RM.                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then ECFM_SUCCESS       */
/*                      Otherwise ECFM_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
EcfmRedDeRegisterWithRM (VOID)
{
    if (EcfmRmDeRegisterProtocols () == RM_FAILURE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                      "EcfmRedDeregisterWithRM: Registration with RM FAILED \r\n");
        return ECFM_FAILURE;
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmCcRedHandleValidMessage.                         */
/*                                                                           */
/* Description        : This function stores the information present in the  */
/*                      message.                                             */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmCcRedHandleValidMessage (tRmMsg * pMsg, UINT2 u2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT1               u1RedMsgType;
    UINT4               u4ByteCount = ECFM_INIT_VAL;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* No need to do null pointer check for pMsg as it is done
     * in EcfmRedRcvPktFromRm. */
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "Received Frame of Size =[%d] \n", u2Length);

    /* Get the Message type. */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1RedMsgType);
    u2Length = u2Length - ECFM_RED_TYPE_FIELD_SIZE;
    u4ByteCount = ECFM_GET_CRU_VALID_BYTE_COUNT (pMsg);
    ECFM_GLB_PKT_DUMP (ECFM_DUMP_TRC, pMsg, u4ByteCount,
                       "EcfmRedHandleValidMessage: Dumping Received Frame \r\n");
    switch (u1RedMsgType)

    {
        case ECFM_BULK_REQ_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_BULK_REQ_MSG \n");
            if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

            {

                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                ECFM_BULK_REQ_RECD () = ECFM_TRUE;
                break;
            }
            ECFM_BULK_REQ_RECD () = ECFM_FALSE;

            /* On recieving ECFM_BULK_REQ_MSG, Bulk updation process should be
             * restarted.
             */
            gEcfmRedGlobalInfo.u2BulkUpdNextPort = 0;
            gEcfmRedGlobalInfo.u4BulkUpdNextContext = 0;
            EcfmRedHandleBulkUpdateEvent ();
            break;
        case ECFM_BULK_UPD_TAIL_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Message ECFM_BULK_UPD_TAIL_MSG \n");

            /* On receiving ECFM_BULK_UPD_TAIL_MSG, give indication to RM.
             * RM gives the trigger to higher 
             * layers inorder to send the Bulk Req msg. \n");
             */
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            EcfmRmHandleProtocolEvent (&ProtoEvt);
            break;
        case ECFM_PORT_OPER_STATUS_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_PORT_OPER_STATUS_MSG \n");
            EcfmRedProcessPortOperStatus (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LBR_CACHE_BACKUP_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_LBR_CACHE_BACKUP_MSG \n");
            EcfmRedProcessLbrCacheBckUpStatus (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNC_UPD_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNC_UPD_MSG \n");
            EcfmRedProcessSyncMsg (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_MIP_DB_HLD_TMR:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_MIP_DB_AGE_OUT \n");
            EcfmRedProcessMipDbHldTmr (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_ERROR_LOG_ENTRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_ERROR_LOG_ENTRY" "\n");
            EcfmRedProcessErrorLogEntry (pMsg, u2Offset, u2Length);
            break;
        case ECFM_STATE_MACHINE_DATA_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_STATE_MACHINE_DATA_MSG" "\n");
            EcfmRedProcessSynchSmData (pMsg, u2Offset, u2Length);
            break;
        case ECFM_IMMEDIATE_SYNCH_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_IMMEDIATE_SYNCH_MSG" "\n");
            EcfmRedProcessSyncCcDataOnChng (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNCH_TX_SEQ_NO_MSG:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNCH_TX_SEQ_NO_MSG" "\n");
            EcfmRedProcessCcTxSeqNoMsg (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNCH_HW_ADT_INFO:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNCH_TX_SEQ_NO_MSG" "\n");
            EcfmRedProcessHwAuditInfo (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNCH_TX_FILTER_ID:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNCH_TX_FILTER_ID" "\n");
            EcfmRedProcessOffTxFilterId (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNCH_RX_FILTER_ID:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNCH_RX_FILTER_ID" "\n");
            EcfmRedProcessOffRxFilterId (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNCH_HW_RX_HANDLER:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNCH_HW_RX_HANDLER" "\n");
            EcfmRedProcessHwRxHandler (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNCH_HW_TX_HANDLER:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNCH_HW_TX_HANDLER" "\n");
            EcfmRedProcessHwTxHandler (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNC_TMR:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNC_TMR" "\n");
            EcfmRedHandleTmrEvnt (pMsg, u2Offset, u2Length);
            break;

        case ECFM_RED_LCK_CONDITION:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_LCK_CONDITION" "\n");
            EcfmRedProcessLckCondition (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LCK_STATUS:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_LCK_STATUS" "\n");
            EcfmRedProcessLckStatus (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LCK_PERIOD_TIMER_EXPIRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_LCK_PERIOD_TIMER_EXPIRY"
                          "\n");
            EcfmRedProcessLckPeriodTmrExpiry (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_AIS_CONDITION:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_AIS_CONDITION" "\n");
            EcfmRedProcessAisCondition (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_AIS_PERIOD_TIMER_EXPIRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_AIS_PERIOD_TIMER_EXPIRY"
                          "\n");
            EcfmRedProcessAisPeriodTmrExpiry (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_LM_STOP_TRANSACTION:
        case ECFM_RED_LM_DEADLINE_TIMER_EXPIRY:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_LM_DEADLINE_TIMER_EXPIRY"
                          "or ECFM_RED_LM_STOP_TRANSACTION\n");
            EcfmRedProcessLmStopTransaction (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNC_LM_TRANS_STS:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNC_LM_TRANS_STS" "\n");
            EcfmRedProcessLmTransSts (pMsg, u2Offset, u2Length);
            break;
        case ECFM_RED_SYNC_AVLBLTY_INFO:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Messege ECFM_RED_SYNC_AVLBLTY_INFO" "\n");
            EcfmRedProcessAvlbltyInfo (pMsg, u2Offset, u2Length);
            break;

            /* HITLESS RESTART */
        case ECFM_HR_STDY_ST_PKT_REQ:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Received Steady State Pkt Request msg\n");
            EcfmRedHRProcStdyStPktReq ();
            break;

        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Invalid Messege [%d] \n", u1RedMsgType);
            break;
    }
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessSyncMsg.                               */
/*                                                                           */
/* Description        : This function process the received sync up msg       */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessSyncMsg (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT2               u2InfoLen = ECFM_INIT_VAL;
    UINT1               u1InfoType = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {

        /* Not a Standby node, hence don't process the received 
         * sync message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Node Not in Stand-By Mode \n");
        return;
    }

    /* Format of the Message .
       Type        Length   <----------------- Value---------------------------->
       ***************************************************************************
       * Sync  *    Length   * Type  *  Len      *  Value  * Type * Len    * Val
       * Msg   *             *       *           *         *      *        *
       ***************************************************************************
       <-1Byte-><--2 Byte---><-1byte-><--2 Byte---> */

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = (UINT2) (u2Length - ECFM_RED_LEN_FIELD_SIZE);
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Data corruption =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }

    /* Get Type (MepInfo, RmepDbInfo, MipCcmDbInfo, PortInfo, ContextInfo,
     * Global Info from the Value Field */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1InfoType);

    /* Decrement MesgSize with 1 byte Type Field */
    i2MsgSize = (INT2) (i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE);
    switch (u1InfoType)

    {
        case ECFM_RED_MEP_INFO:
            while (i2MsgSize > 0)

            {

                /* Get Length of the MepInfo in the message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next MepInfo from the buffer */
                i2MsgSize =
                    (INT2) (i2MsgSize - (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen));

                /* Extract and update MepInfo in the Node */
                EcfmRedUpdateMepInfo (pMsg, &u2Offset);
                i2MsgSize = (INT2) (i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE);
                u2Offset = (UINT2) (u2Offset + ECFM_RED_TYPE_FIELD_SIZE);
            }
            break;
        case ECFM_RED_RMEP_INFO:
            while (i2MsgSize > 0)

            {

                /* Get Length of the MepInfo in the message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next MepInfo from the buffer */
                i2MsgSize =
                    (INT2) (i2MsgSize - (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen));

                /* Extract and update RMepInfo in the Node */
                EcfmRedUpdateRMepInfo (pMsg, &u2Offset);
                i2MsgSize = (INT2) (i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE);
                u2Offset = (UINT2) (u2Offset + ECFM_RED_TYPE_FIELD_SIZE);
            }
            break;
        case ECFM_RED_MIP_DB_INFO:
            while (i2MsgSize > 0)

            {

                /* Get Length of the Mip Ccm Db Info in the message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next Mip Ccm Db Info from the buffer 
                 */
                i2MsgSize =
                    (INT2) (i2MsgSize - (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen));

                /* Extract and update Mip Ccm DB Info in the Node */
                EcfmRedUpdateMipDbInfo (pMsg, &u2Offset);
                i2MsgSize = (INT2) (i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE);
                u2Offset = (UINT2) (u2Offset + ECFM_RED_TYPE_FIELD_SIZE);
            } break;
        case ECFM_RED_LM_BUFF_INFO:
            while (i2MsgSize > 0)

            {

                /* Get Length of the LM Buffer Info in the Message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next LM Buff Info from the buffer 
                 */
                i2MsgSize =
                    (INT2) (i2MsgSize - (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen));

                /* Extract and update LM Buffer Info in the Node */
                EcfmRedUpdateLmBuffInfo (pMsg, &u2Offset, u2InfoLen);
                i2MsgSize = (INT2) (i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE);
                u2Offset = (UINT2) (u2Offset + ECFM_RED_TYPE_FIELD_SIZE);
            } break;
        case ECFM_RED_ERR_LOG_INFO:
            while (i2MsgSize > 0)
            {
                /* Get Length of the Mip Ccm Db Info in the message */
                ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2InfoLen);

                /* Update MesgSize to read next Mip Ccm Db Info from the buffer 
                 */
                i2MsgSize =
                    (INT2) (i2MsgSize - (ECFM_RED_LEN_FIELD_SIZE + u2InfoLen));

                /* Extract and update Error Log Info in the Node */
                EcfmRedUpdateErrLog (pMsg, &u2Offset);
                i2MsgSize = (INT2) (i2MsgSize - ECFM_RED_TYPE_FIELD_SIZE);
                u2Offset = (UINT2) (u2Offset + ECFM_RED_TYPE_FIELD_SIZE);
            } break;
        default:
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Invalid Info in the buffer \r\n");
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateMepInfo                               */
/*                                                                           */
/* Description        : This function updates the MepInfo received in the    */
/*                      Sync  msg.                                           */
/*                                                                           */
/* Input(s)           : pMsg     - Pointer to the sync up message.          */
/*                      pu2Offset - Offset from where info is to be read     */
/*                      u2InfoLen - Length of MepInfo to be updated.          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedUpdateMepInfo (tRmMsg * pMsg, UINT2 *pu2Offset)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4CurrentTime = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;

    ECFM_GET_SYS_TIME (&u4CurrentTime);

    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "context-id %d not valid in stand-by\r\n", u4ContextId);
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update MEP-INFO in stand-by -- module disabled for context = %d\r\n",
                      u4ContextId);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2MepId);
    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);
    gpEcfmCcMepNode->u4MdIndex = u4MdIndex;
    gpEcfmCcMepNode->u4MaIndex = u4MaIndex;
    gpEcfmCcMepNode->u2MepId = u2MepId;

    /* Get MEP from Global RBTree with received MdIndex, MaIndex, u2MepId */
    pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE, (tRBElem *) gpEcfmCcMepNode);
    if (pMepInfo != NULL)

    {
        /* Update CC Info Maintained per MEP */
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u4CciSentCcms);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u4CcmSeqErrors);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.b1ErrorCcmDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.b1XconCcmDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1MacStatusChanged);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->FngInfo.b1SomeRMepCcmDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->FngInfo.b1SomeMacStatusDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.b1SomeRdiDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.u1FngDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.u1FngState);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u2ErrorRMepId);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u2XconnRMepId);

        /* Get Offloading MEP Specific Information  */
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->b1MepCcmOffloadStatus);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pMepInfo->b1MepCcmOffloadHwStatus);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pMepInfo->u2OffloadMepTxHandle);
        ECFM_RM_GET_N_BYTE (pMsg, pMepInfo->au1HwMepHandler, pu2Offset,
                            ECFM_HW_MEP_HANDLER_SIZE);

        /* Get LBLT MEP Info */
        EcfmRedLbLtUpdateEcfmMepInfo (pMsg, pu2Offset, pMepInfo->u4MdIndex,
                                      pMepInfo->u4MaIndex,
                                      pMepInfo->u2MepId,
                                      ECFM_CC_CURR_CONTEXT_ID ());
        if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))

        {
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1UnExpectedMepDefect);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1UnExpectedPeriodDefect);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1MisMergeDefect);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1UnExpectedLevelDefect);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1LocalLinkFailure);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1InternalHwFailure);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.b1InternalSwFailure);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->CcInfo.u1RdiCapability);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->AisInfo.b1AisTsmting);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->LckInfo.b1LckCondition);
            ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                                pMepInfo->AisInfo.b1AisCondition);
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pMepInfo->pMaInfo->u4TransId);
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->CcInfo.RdiTimeStamp = u4CurrentTime + u4RemainingTime;
            }
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->LckInfo.LckPrdTimeStamp =
                    u4CurrentTime + u4RemainingTime;
            }
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->LckInfo.LckIntervalTimeStamp =
                    u4CurrentTime + u4RemainingTime;
            }
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->LckInfo.u4LckRcvdRxWhileValue = u4RemainingTime;
            }
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->AisInfo.AisPrdTimeStamp =
                    u4CurrentTime + u4RemainingTime;
            }
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->AisInfo.AisIntervalTimeStamp =
                    u4CurrentTime + u4RemainingTime;
            }
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
            if (u4RemainingTime != ECFM_INIT_VAL)
            {
                pMepInfo->AisInfo.u4AisRcvdRxWhileValue = u4RemainingTime;
            }

            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                                pMepInfo->LmInfo.u4RemainingInitWhileTimer);
            ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                                pMepInfo->LmInfo.u4RemainingDeadlineTimer);

            /* Get LBLT MEP Info */
            EcfmRedLbLtUpdateY1731MepInfo (pMsg, pu2Offset,
                                           pMepInfo->u4MdIndex,
                                           pMepInfo->u4MaIndex,
                                           pMepInfo->u2MepId,
                                           ECFM_CC_CURR_CONTEXT_ID ());
        }

        /* Store the Remaining Time Interval for the Timer Maintained */
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
        if (u4RemainingTime != ECFM_INIT_VAL)
        {
            pMepInfo->CcInfo.CciTimeStamp = u4CurrentTime + u4RemainingTime;
        }
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
        if (u4RemainingTime != ECFM_INIT_VAL)
        {
            pMepInfo->CcInfo.XConTimeStamp = u4CurrentTime + u4RemainingTime;
        }
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
        if (u4RemainingTime != ECFM_INIT_VAL)
        {
            pMepInfo->CcInfo.ErrTimeStamp = u4CurrentTime + u4RemainingTime;
        }
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
        if (u4RemainingTime != ECFM_INIT_VAL)
        {
            pMepInfo->FngInfo.FngWhileTimeStamp =
                u4CurrentTime + u4RemainingTime;
        }
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
        if (u4RemainingTime != ECFM_INIT_VAL)
        {
            pMepInfo->FngInfo.FngRstWhileTimeStamp =
                u4CurrentTime + u4RemainingTime;
        }
    }

    else

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "MEP-INFO Not found at STAND-BY NODE"
                      "=[%d][%d][%d]\r\n", u4MdIndex, u4MaIndex, u2MepId);
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateRMepInfo                                */
/*                                                                           */
/* Description        : This function updates the MepInfo received in the    */
/*                      Sync  msg.                                           */
/*                                                                           */
/* Input(s)           : pMsg     - Pointer to the sync up message.           */
/*                      pu2Offset - Offset from where info is to be read     */
/*                      u2InfoLen - Length of MepInfo to be updated.         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedUpdateRMepInfo (tRmMsg * pMsg, UINT2 *pu2Offset)
{
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2RMepId = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT4               u4CurrentTime = ECFM_INIT_VAL;
    tMacAddr            RMepMacAddr = {
        0
    };
    ECFM_GET_SYS_TIME (&u4CurrentTime);

    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "context-id %d not valid in stand-by\r\n", u4ContextId);
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update MEP-INFO in stand-by -- module disabled for context = %d\r\n",
                      u4ContextId);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2MepId);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2RMepId);

    /* Get MEP from Global RBTree with received MdIndex, MaIndex, u2MepId */
    pRMepInfo =
        EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u2MepId, u2RMepId);
    if (pRMepInfo != NULL)

    {
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pRMepInfo->u4SeqNum);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1State);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->b1RMepCcmDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1LastPortStatus);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1LastInterfaceStatus);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->b1RMepPortStatusDefect);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                            pRMepInfo->b1RMepInterfaceStatusDefect);
        ECFM_RM_GET_N_BYTE (pMsg, RMepMacAddr, pu2Offset, ECFM_MAC_ADDR_LENGTH);
        ECFM_MEMCPY (pRMepInfo->RMepMacAddr, RMepMacAddr, ECFM_MAC_ADDR_LENGTH);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: u4SeqNum = [%d] \n", pRMepInfo->u4SeqNum);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: u1State = [%d] \n", pRMepInfo->u1State);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: u1State = [%d] \n", pRMepInfo->b1RMepCcmDefect);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: u1LastPortStatus = [%d] \n",
                      pRMepInfo->u1LastPortStatus);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: u1LastInterfaceStatus = [%d] \n",
                      pRMepInfo->u1LastInterfaceStatus);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: b1RMepPortStatusDefect = [%d] \n",
                      pRMepInfo->b1RMepPortStatusDefect);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RCVD: b1RMepInterfaceStatusDefect = [%d] \n",
                      pRMepInfo->b1RMepPortStatusDefect);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RMEP-INFO received mac address = [%02x:%02x:%02x:%02x:%02x:%02x]\r\n",
                      RMepMacAddr[0], RMepMacAddr[1], RMepMacAddr[2],
                      RMepMacAddr[3], RMepMacAddr[4], RMepMacAddr[5]);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pRMepInfo->u2OffloadRMepRxHandle);
        ECFM_RM_GET_N_BYTE (pMsg, pRMepInfo->au1HwRMepHandler, pu2Offset,
                            ECFM_HW_MEP_HANDLER_SIZE);
        /* Store the Remaining Time Interval for the Timer Maintained */
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
        if (u4RemainingTime != ECFM_INIT_VAL)
        {
            pRMepInfo->RMepTimeStamp = u4CurrentTime + u4RemainingTime;
        }
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->b1LastRdi);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1MepCcmLCStatus);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1Y1731RemCCMRxCount);
        if (EcfmLbLtAddRMepDbEntry (u4ContextId, pRMepInfo) != ECFM_SUCCESS)
        {
            return;
        }
    }

    else

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RMEP-INFO Not found at STAND-BY NODE"
                      "=[%d][%d][%d][%d]\r\n", u4MdIndex, u4MaIndex, u2MepId,
                      u2RMepId);
    }
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateMipDbInfo                             */
/*                                                                           */
/* Description        : This function updates the Mip Db Info received in    */
/*                      Sync  msg.                                           */
/*                                                                           */
/* Input(s)           : pMsg     - Pointer to the sync up message.          */
/*                      pu2Offset - Offset from where info is to be read     */
/*                      u2InfoLen - Length of Mip Db Info to be updated.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedUpdateMipDbInfo (tRmMsg * pMsg, UINT2 *pu2Offset)
{
    tEcfmCcMipCcmDbInfo MipDbInfo;
    tEcfmMacAddr        MacAddr;
    tEcfmCcMipCcmDbInfo *pMipDbInfo = NULL;
    UINT4               u4TimeStamp = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2Fid = ECFM_INIT_VAL;
    UINT4               u4PortNum = ECFM_INIT_VAL;
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update MEP-INFO in stand-by\r\n");
        return;
    }

    /* Get Info from message */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4PortNum);
    ECFM_RM_GET_N_BYTE (pMsg, MacAddr, pu2Offset, ECFM_MAC_ADDR_LENGTH);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2Fid);
    ECFM_MEMSET (&MipDbInfo, ECFM_INIT_VAL, ECFM_CC_MIP_CCM_DB_INFO_SIZE);
    ECFM_MEMCPY (MipDbInfo.SrcMacAddr, MacAddr, ECFM_MAC_ADDR_LENGTH);
    MipDbInfo.u2Fid = u2Fid;

    /* Get MIP CCM DB Node from the Global RBTree */
    pMipDbInfo = RBTreeGet (ECFM_CC_MIP_CCM_DB_TABLE, (tRBElem *) & MipDbInfo);
    if (pMipDbInfo != NULL)

    {

        /* Update MIP CCM DB Info in the Node */
        pMipDbInfo->TimeStamp = u4TimeStamp;
        pMipDbInfo->u2PortNum = (UINT2) u4PortNum;
        ECFM_MEMCPY (pMipDbInfo->SrcMacAddr, MacAddr, ECFM_MAC_ADDR_LENGTH);
        EcfmLbLtAddMipCcmDbEntry (u4ContextId, pMipDbInfo);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessPortOperStatus.                        */
/*                                                                           */
/* Description        : This function process the received Port Oper Status  */
/*                      SyncUp  msg.                                         */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessPortOperStatus (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    UINT4               u4IfIndex = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2MsgSize = ECFM_INIT_VAL;
    UINT2               u2PortNum = ECFM_INIT_VAL;
    UINT1               u1RedMsgType = ECFM_INIT_VAL;
    UINT1               u1PortOperStatus = ECFM_INIT_VAL;
    INT2                i2Length = ECFM_INIT_VAL;
    tEcfmCcPortInfo    *pPortEntry = NULL;

    i2Length = u2Length;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {

        /* Not a Standby node, hence don't process the received 
         * sync message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      " NODE IS ACTIVE- Messege Ignored\r\n");
        return;
    }

    /* Get the Message size encoded by the ECFM module in Active node. */
    while (i2Length > 0)

    {
        ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MsgSize);
        i2Length = (i2Length) - ECFM_RED_LEN_FIELD_SIZE;
        if (u2MsgSize != ECFM_PORT_OPER_STATUS_MSG_SIZE)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          " Invalid Length = [%d]\r\n", i2Length);
            return;
        }
        ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4IfIndex);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1PortOperStatus);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Received Port Operation Status Message\r\n");
        ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (u4IfIndex, &u4ContextId,
                                            &u2PortNum);

        /* Check the validity of the port. */
        if ((u2PortNum < ECFM_PORTS_PER_CONTEXT_MIN)
            || (u2PortNum > ECFM_CC_MAX_PORT_INFO))

        {

            /* Invalid port. */
            return;
        }
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            return;
        }
        if (ECFM_IS_SYSTEM_STARTED (ECFM_CC_CURR_CONTEXT_ID ()))

        {
            pPortEntry = ECFM_CC_GET_PORT_INFO ((UINT2) u2PortNum);
            if (pPortEntry != NULL)

            {
                pPortEntry->u1IfOperStatus = u1PortOperStatus;
                ECFM_LBLT_HANDLE_PORT_OPER_CHG (u2PortNum, u1PortOperStatus);

            }
        }
        i2Length = i2Length - u2MsgSize;
        if (i2Length > 0)

        {
            ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1RedMsgType);
            i2Length = i2Length - ECFM_RED_TYPE_FIELD_SIZE;
        }

        else

        {
            break;
        }
        ECFM_CC_RELEASE_CONTEXT ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLbrCacheBckUpStatus                    */
/*                                                                           */
/* Description        : This function process the received Lbr Cache Backup  */
/*                      Status SyncUp  msg.                                  */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLbrCacheBckUpStatus (tRmMsg * pMsg, UINT2 u2Offset,
                                   UINT2 u2Length)
{
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2MsgSize = ECFM_INIT_VAL;
    INT2                i2Length = ECFM_INIT_VAL;
    UINT1               u1LbrCacheBckUpStatus = ECFM_INIT_VAL;

    i2Length = u2Length;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {

        /* Not a Standby node, hence don't process the received 
         * sync message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      " NODE IS ACTIVE- Messege Ignored\r\n");
        return;
    }

    /* Get the Message size encoded by the ECFM module in Active node. */
    while (i2Length > 0)

    {
        ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MsgSize);
        i2Length = (i2Length) - ECFM_RED_LEN_FIELD_SIZE;
        if (u2MsgSize != ECFM_LBR_CACHE_STATUS_BCKUP_MSG_SIZE)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          " Invalid Length = [%d]\r\n", i2Length);
            return;
        }
        ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1LbrCacheBckUpStatus);
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Received Lbr Cache BckUp Status Message\r\n");

        ECFM_LBLT_LOCK ();
        if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            ECFM_LBLT_UNLOCK ();
            return;
        }

        ECFM_LBLT_CURR_CONTEXT_INFO ()->u1LbrCacheStatusBckup =
            u1LbrCacheBckUpStatus;

        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessMipDbHldTmr                            */
/*                                                                           */
/* Description        : This function process the received MIP DB Timer      */
/*                      expiry messege.                                      */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessMipDbHldTmr (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT2               u2MsgSize = ECFM_INIT_VAL;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {

        /* Not a Standby node, hence don't process the received 
         * sync message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      " NODE IS ACTIVE- Messege Ignored\r\n");
        return;
    }

    /* Get the Message size encoded by the ECFM module in Active node. */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MsgSize);
    u2Length = (u2Length) - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != u2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      " Data corruption\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    if (u2Length != ECFM_MIB_DB_HLD_TMR_MSG_SIZE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      " Invalid Length\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4RemainingTime);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        return;
    }
    if (u4RemainingTime == 0)

    {

        /* Age Out the MIP DB Entries */
        EcfmCcAgeoutMipDbEntry ();
        ECFM_CC_CURR_CONTEXT_INFO ()->u4RecvdMipDbHldTime = 0;
        ECFM_CC_CURR_CONTEXT_INFO ()->TimeStamp = 0;
    }

    else

    {

        /*Store the remaining time along with the time-stamp */
        ECFM_CC_CURR_CONTEXT_INFO ()->u4RecvdMipDbHldTime = u4RemainingTime;
        ECFM_GET_SYS_TIME (&(ECFM_CC_CURR_CONTEXT_INFO ()->TimeStamp));
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "MIP-CCM-DM remaining time as = [%d]\r\n", u4RemainingTime);
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedHandleGoStandbyEvent.                         */
/*                                                                           */
/* Description        : This function handles the GO_STANDBY event from RM.  */
/*                      If node is active, then restart ECFM module,else     */
/*                      update the node status.                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedHandleGoStandbyEvent (VOID)
{
    ECFM_NODE_STATUS () = ECFM_NODE_STANDBY;
    EcfmRestartModule ();
    ECFM_INIT_NUM_PEERS ();
}

/*****************************************************************************/
/* Function Name      : EcfmRedHandleStandByUpEvent.                         */
/*                                                                           */
/* Description        : This function handles the STANDBY UP event from RM.  */
/*                      If node is active, then it increments the            */
/*                      u1NumPeersUp and calls EcfmRedHandleBulkUpdateEvent  */
/*                                                                           */
/* Input(s)           : pvPeerId - PeerId to which the BulkUpdate has to be  */
/*                      sent.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedHandleStandByUpEvent (VOID *pvPeerId)
{
    UNUSED_PARAM (pvPeerId);
    if (ECFM_BULK_REQ_RECD () == ECFM_TRUE)

    {

#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED) \
            &&!defined (RSTP_WANTED) && !defined (VLAN_WANTED)
        ECFM_BULK_REQ_RECD () = ECFM_FALSE;

        /* Bulk request msg is recieved before RM_STANDBY_UP event.
         * So we are sending bulk updates now.
         */
        gEcfmRedGlobalInfo.u2BulkUpdNextPort = 0;
        gEcfmRedGlobalInfo.u4BulkUpdNextContext = 0;
        EcfmRedHandleBulkUpdateEvent ();

#endif /*  */
    }
    ECFM_CC_SELECT_CONTEXT (ECFM_DEFAULT_CONTEXT);

    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSendBulkReq.                                  */
/*                                                                           */
/* Description        : This function sends a bulk request to the active     */
/*                      node in the system.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
PRIVATE INT4
EcfmRedSendBulkReq (VOID)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2BufSize;
    u2BufSize = ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE;
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "ECFM RED: Send Bulk Req \r\n");

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM allocation failed \r\n");
        return ECFM_FAILURE;
    }

    /* Fill the message type. */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_BULK_REQ_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, ECFM_BULK_REQ_MSG_SIZE);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmCcRedHandleStandbyDown                           */
/*                                                                           */
/* Description        : This function clears synched flags maintained in     */
/*                      all the Cache Nodes                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmCcRedHandleStandbyDown ()
{
    UINT4               u4ContextId = ECFM_INIT_VAL;

    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "ECFM RED: Red Standby Down \n");
    for (u4ContextId = ECFM_DEFAULT_CONTEXT;
         u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_MODULE_DISABLED (u4ContextId))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            continue;
        }

        /* Sync LM Cache at CC Task */
        EcfmRedClearLmSyncFlag ();

        /* Sync LB and DM Cache at LBLT Task */
        ECFM_LBLT_LOCK ();
        if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            EcfmRedClearDmSyncFlag ();
            EcfmRedClearLbSyncFlag ();
            ECFM_LBLT_UNLOCK ();
            ECFM_CC_RELEASE_CONTEXT ();
            return;
        }

        /* Sync DM Cache */
        EcfmRedClearDmSyncFlag ();

        /* Sync LB Cache */
        EcfmRedClearLbSyncFlag ();
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        ECFM_CC_RELEASE_CONTEXT ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSendPeriodicUpdates                           */
/*                                                                           */
/* Description        : This function sends the periodic updates to the      */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedSendPeriodicUpdates ()
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmCcMipCcmDbInfo *pMipDbNode = NULL;
    tEcfmMacAddr        TempMacAddr;
    tRmMsg             *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen = ECFM_INIT_VAL;
    UINT2               u2SyncOffset = ECFM_INIT_VAL;
    UINT2               u2MepInfoSize = ECFM_INIT_VAL;
    UINT4               u4TmpPortNum = ECFM_INIT_VAL;

    u2BufSize = ECFM_RED_MAX_MSG_SIZE;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* Allocate memory to the pMsg for Maximum PDU Size */
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);
    if (pMsg == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM alloc  failed. Bulk updates not sent\n");
        return;
    }
    u2Offset = ECFM_INIT_VAL;

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

    /* Fill the number of size of  information to be synced up. 
     * Here only zero will be filled. The following macro is called
     * to move the pointer by 2 bytes.*/
    u2SyncOffset = u2Offset;
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    for (u4ContextId = ECFM_DEFAULT_CONTEXT;
         u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_MODULE_DISABLED (u4ContextId))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            continue;
        }
        pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);

        /* Get MepInfo and Fill in the RM Buffer to transmit Sync Up message */
        while (pMepNode != NULL)

        {

            /* Calculate the length of the MepInfo to be transmitted as it is of
             * variable length */
            if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepNode->u2PortNum))

            {
                u2MepInfoSize =
                    ECFM_RED_MEP_INFO_SIZE + ECFM_RED_MEP_Y1731_INFO_SIZE;
            }

            else

            {
                u2MepInfoSize = ECFM_RED_MEP_INFO_SIZE;
            }

            /* There is no enough space to fill this port information into 
             * buffer.Hence send the existing message and construct new message 
             to fill 
             * the information of other MEPs.*/
            if (u2BufSize - u2Offset < u2MepInfoSize)

            {

                /* Offset of the Length field for update information is stored in
                 * u4SyncLengthOffset.u2SyncMsgLen contains the size of update
                 * information so far written.Hence update length field at offset
                 * u4SyncLengthOffset with the value u2SyncMsgLen.*/

                /* u2Offset contains the number of bytes written in the buffer, 
                 * so can be used as no of bytes to transmit */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
                EcfmRedSendMsgToRm (pMsg, u2Offset);
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "RM: Alloc One More Buffer for MEP \n");
                pMsg = RM_ALLOC_TX_BUF (u2BufSize);
                if (pMsg == NULL)

                {
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                                  "RM alloc  failed. Bulk updates not sent\n");
                    return;
                }
                u2Offset = ECFM_INIT_VAL;
                u2SyncMsgLen = ECFM_INIT_VAL;

                /* Fill TLV Type in the message and Increment the offset */
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

                /* Fill the number of size of  information to be synced up. 
                 * Here only zero will be filled. The following macro is called
                 * to move the pointer by 2 bytes.*/
                u2SyncOffset = u2Offset;
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            }

            /* Fill the Value in the message and increment the offset */
            EcfmRedFillMepInfo (u4ContextId, pMepNode, pMsg, &u2Offset);

            /* Update Sync Msg Length */
            u2SyncMsgLen = u2SyncMsgLen + u2MepInfoSize;

            /* Get Next MEP from the Global RBTree maintained */
            pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
        }
        if ((u2Offset - 2) != u2SyncOffset)

        {

            /* u2Offset contains the number of bytes written in the buffer, 
             * so can be used as no of bytes to transmit */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);

            /* Allocate memory to the pMsg for Maximum PDU Size */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

            /* Fill the number of size of  information to be synced up. 
             * Here only zero will be filled. The following macro is called
             * to move the pointer by 2 bytes.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
        }

        /* Updated RMEP DB Information */
        pRMepNode = RBTreeGetFirst (ECFM_CC_RMEP_TABLE);

        /* Get MepInfo and Fill in the RM Buffer to transmit Sync Up message */
        while (pRMepNode != NULL)

        {

            /* Calculate the length of the MepInfo to be transmitted as it is of
             * variable length */

            /* There is no enough space to fill this port information into 
             * buffer.Hence send the existing message and construct new message 
             to fill 
             * the information of other MEPs.*/
            if (u2BufSize - u2Offset < ECFM_RED_RMEP_INFO_SIZE)

            {

                /* Offset of the Length field for update information is stored in
                 * u4SyncLengthOffset.u2SyncMsgLen contains the size of update
                 * information so far written.Hence update length field at offset
                 * u4SyncLengthOffset with the value u2SyncMsgLen.*/

                /* u2Offset contains the number of bytes written in the buffer, 
                 * so can be used as no of bytes to transmit */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
                EcfmRedSendMsgToRm (pMsg, u2Offset);
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "RM: Alloc One More Buffer for MEP \n");
                pMsg = RM_ALLOC_TX_BUF (u2BufSize);
                if (pMsg == NULL)

                {
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                                  "RM alloc  failed. Bulk updates not sent\n");
                    return;
                }
                u2Offset = ECFM_INIT_VAL;
                u2SyncMsgLen = ECFM_INIT_VAL;

                /* Fill TLV Type in the message and Increment the offset */
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

                /* Fill the number of size of  information to be synced up. 
                 * Here only zero will be filled. The following macro is called
                 * to move the pointer by 2 bytes.*/
                u2SyncOffset = u2Offset;
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            }

            /* Fill the Value in the message and increment the offset */
            EcfmRedFillRMepInfo (u4ContextId, pRMepNode, pMsg, &u2Offset);

            /* Update Sync Msg Length */
            u2SyncMsgLen = u2SyncMsgLen + ECFM_RED_RMEP_INFO_SIZE;

            /* Get Next MEP from the Global RBTree maintained */
            pRMepNode = RBTreeGetNext (ECFM_CC_RMEP_TABLE, pRMepNode, NULL);
        }
        if ((u2Offset - 2) != u2SyncOffset)

        {

            /* u2Offset contains the number of bytes written in the buffer, 
             * so can be used as no of bytes to transmit */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);

            /* Allocate memory to the pMsg for Maximum PDU Size */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

            /* Fill the number of size of  information to be synced up. 
             * Here only zero will be filled. The following macro is called
             * to move the pointer by 2 bytes.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
        }

        /* Update MIP DB Information */
        pMipDbNode = RBTreeGetFirst (ECFM_CC_MIP_CCM_DB_TABLE);

        /* Get MepInfo and Fill in the RM Buffer to transmit Sync Up message */
        while (pMipDbNode != NULL)

        {

            /* There is no enough space to fill this port information into 
               buffer.
               * Hence send the existing message and construct new message to 
               fill 
               * the information of other MEPs. */
            if (u2BufSize - u2Offset < ECFM_RED_MIP_DB_INFO_SIZE)

            {

                /* Fill the number of size of  information to be synced up. 
                 * Here only zero will be filled. The following macro is 
                 * called to move the pointer by 2 bytes.
                 */
                /* u2Offset contains the number of bytes written in the 
                 * buffer, so can be used as no of bytes to transmit 
                 */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
                EcfmRedSendMsgToRm (pMsg, u2Offset);
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "RM: Alloc One More Buffer for MIP-CCMDB \n");
                pMsg = RM_ALLOC_TX_BUF (u2BufSize);
                if (pMsg == NULL)

                {
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                                  "RM alloc  failed. Bulk updates not sent\n");
                    return;
                }
                u2Offset = ECFM_INIT_VAL;
                u2SyncMsgLen = ECFM_INIT_VAL;

                /* Fill TLV Type in the message and Increment the offset */
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

                /* Offset of the Length field for update information is 
                 * stored in u4SyncLengthOffset.u2SyncMsgLen contains the size of 
                 * update information so far written.Hence update length field at 
                 * offset u4SyncLengthOffset with the value u2SyncMsgLen.*/
                u2SyncOffset = u2Offset;
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            }

            /* Fill in Value field the MIP DB Info */
            /* Fill TLV Type in the message and Increment the offset */
            if (pMipDbNode->b1Changed == ECFM_TRUE)

            {
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_MIP_DB_INFO);

                /* Fill TLV length in the message and increment the offset */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                    (ECFM_RED_MIP_DB_INFO_SIZE
                                     - (ECFM_RED_TYPE_FIELD_SIZE
                                        + ECFM_RED_LEN_FIELD_SIZE)));

                u4TmpPortNum = (UINT4) pMipDbNode->u2PortNum;

                /* Fill the Value in the message and increment the offset */

                /* Store the current context Id */
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4TmpPortNum);
                ECFM_RM_PUT_N_BYTE (pMsg, pMipDbNode->SrcMacAddr, &u2Offset,
                                    ECFM_MAC_ADDR_LENGTH);
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMipDbNode->u2Fid);

                /* Update Sync Msg Length */
                u2SyncMsgLen = u2SyncMsgLen + ECFM_RED_MIP_DB_INFO_SIZE;
            }

            /* Get Next MIP DB Count from the Global RBTree maintained */
            pMipDbNode =
                RBTreeGetNext (ECFM_CC_MIP_CCM_DB_TABLE, pMipDbNode, NULL);
        } ECFM_CC_RELEASE_CONTEXT ();
    } if ((u2Offset - 2) != u2SyncOffset)

    {

        /* u2Offset contains the number of bytes written in the buffer, 
         * so can be used as no of bytes to transmit */
        ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
        EcfmRedSendMsgToRm (pMsg, u2Offset);
    }

    else

    {

        /* Empty buffer created without any update messages filled */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Empty buffer created without any update"
                      "messages filled \r\n");
        RM_FREE (pMsg);
        pMsg = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedFillMepInfo                                 */
/*                                                                           */
/* Description        : This function fills the MEP Info in the RM Buffer    */
/*                                                                           */
/* Input(s)           : u4ContextId - Current context Id.                    */
/*                      pMepNode - Pointer to the MEP Node.                  */
/*                      pMsg     - Pointer to the buffer where information   */
/*                                 needs to be written.                      */
/*                      u2Offset - Offset from where the info is to be filled*/
/*                      u2MepInfoLen - Length of one MEP Length              */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedFillMepInfo (UINT4 u4ContextId, tEcfmCcMepInfo * pMepInfo,
                    tRmMsg * pMsg, UINT2 *pu2Offset)
{
    tEcfmAppTimer      *pEcfmAppTmr = NULL;
    UINT2               u2MepInfoSize = ECFM_RED_MEP_INFO_SIZE;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, ECFM_RED_MEP_INFO);

    /* Length of the message will be diffrent for 802.1ag and Y1731 */
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))
    {
        u2MepInfoSize = u2MepInfoSize + ECFM_RED_MEP_Y1731_INFO_SIZE;
    }

    /* Fill TLV length in the message and increment the offset */
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, (u2MepInfoSize
                                          - (ECFM_RED_TYPE_FIELD_SIZE +
                                             ECFM_RED_LEN_FIELD_SIZE)));

    /* Store the current context Id */
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4ContextId);

    /* Store the MEP related info in the buffer */
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->u2MepId);

    /* Store CC related Info maintained per MEP */
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u4CciSentCcms);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u4CcmSeqErrors);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.b1ErrorCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.b1XconCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.b1MacStatusChanged);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.b1SomeRMepCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                        pMepInfo->FngInfo.b1SomeMacStatusDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.b1SomeRdiDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.u1FngDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->FngInfo.u1FngState);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u2ErrorRMepId);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u2XconnRMepId);

    /* Fill Offloading Specific MEP Info */
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->b1MepCcmOffloadStatus);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->b1MepCcmOffloadHwStatus);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pMepInfo->u2OffloadMepTxHandle);
    ECFM_RM_PUT_N_BYTE (pMsg, pMepInfo->au1HwMepHandler, pu2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);
    pMepInfo->CcInfo.u1CciState = ECFM_CCI_STATE_WAITING;
    EcfmRedFillLbLtEcfmMepInfo (pMsg, pu2Offset, pMepInfo->u4MdIndex,
                                pMepInfo->u4MaIndex, pMepInfo->u2MepId,
                                ECFM_CC_CURR_CONTEXT_ID ());

    /* Fill Y1731 specific information */
    if (ECFM_CC_IS_Y1731_ENABLED_ON_PORT (pMepInfo->u2PortNum))

    {
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1UnExpectedMepDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1UnExpectedPeriodDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.b1MisMergeDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1UnExpectedLevelDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1LocalLinkFailure);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1InternalHwFailure);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                            pMepInfo->CcInfo.b1InternalSwFailure);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->CcInfo.u1RdiCapability);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->AisInfo.b1AisTsmting);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->LckInfo.b1LckCondition);
        ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pMepInfo->AisInfo.b1AisCondition);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pMepInfo->pMaInfo->u4TransId);

        /* Reset u4RemainigTime  for RDI Capability */
        u4RemainingTime = ECFM_INIT_VAL;
        pEcfmAppTmr = &(pMepInfo->CcInfo.RdiPeriodTimer.TimerNode);

        /* Get the remaining time for RDI Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }

        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: pRdiPeriodTimer = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for LCK Capability */
        u4RemainingTime = ECFM_INIT_VAL;
        pEcfmAppTmr = &(pMepInfo->LckInfo.LckPeriod.TimerNode);

        /* Get the remaining time for LCK Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: LckPeriod = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for LCK Interval */
        u4RemainingTime = ECFM_INIT_VAL;

        pEcfmAppTmr = &(pMepInfo->LckInfo.LckInterval.TimerNode);

        /* Get the remaining time for LCK Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: LckInterval = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for LCK Rx While Timer */
        u4RemainingTime = ECFM_INIT_VAL;

        pEcfmAppTmr = &(pMepInfo->LckInfo.LckRxWhile.TimerNode);

        /* Get the remaining time for LCK Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }

        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: LckRxWhile = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for AIS Period */
        u4RemainingTime = ECFM_INIT_VAL;

        pEcfmAppTmr = &(pMepInfo->AisInfo.AisPeriod.TimerNode);

        /* Get the remaining time for LCK Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }

        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: AisPeriod = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for LCK Interval */
        u4RemainingTime = ECFM_INIT_VAL;
        pEcfmAppTmr = &(pMepInfo->AisInfo.AisInterval.TimerNode);

        /* Get the remaining time for LCK Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: AisInterval = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for AIS Rx While Timer */
        u4RemainingTime = ECFM_INIT_VAL;
        pEcfmAppTmr = &(pMepInfo->AisInfo.AisRxWhile.TimerNode);

        /* Get the remaining time for LCK Period Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }

        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: AisRxWhile = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for LM InitWhile Timer */
        u4RemainingTime = ECFM_INIT_VAL;

        pEcfmAppTmr = &(pMepInfo->LmInfo.LmmInitWhileTimer.TimerNode);

        /* Get the remaining time for LM InitWhile Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: pLmmInitWhileTimer = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Reset u4RemainigTime  for LM Deadline Timer */
        u4RemainingTime = ECFM_INIT_VAL;
        pEcfmAppTmr = &(pMepInfo->LmInfo.LmInitDeadlineTimer.TimerNode);

        /* Get the remaining time for LM Deadline Timer */
        if (ECFM_GET_REMAINING_TIME
            (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

        {
            u4RemainingTime = ECFM_INIT_VAL;
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RM: pLmInitDeadlineTimer = [%d] \n", u4RemainingTime);
        ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

        /* Fill LBLT Mep Info */
        EcfmRedFillLbLtY1731MepInfo (pMsg, pu2Offset, pMepInfo->u4MdIndex,
                                     pMepInfo->u4MaIndex,
                                     pMepInfo->u2MepId,
                                     ECFM_CC_CURR_CONTEXT_ID ());
    }

    /* Reset u4RemainigTime for CC While Timer */
    u4RemainingTime = ECFM_INIT_VAL;

    pEcfmAppTmr = &(pMepInfo->CcInfo.CciWhileTimer.TimerNode);

    /* Get the remaining time for ErrCCMWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)
    {
        u4RemainingTime = ECFM_INIT_VAL;

    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: CciWhileTimer = [%d] \n", u4RemainingTime);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;

    /* fill remaining Timer Value for XconCCMWhileTimer & ErrCCMWhileTimer */
    pEcfmAppTmr = &(pMepInfo->CcInfo.XconCCMWhileTimer.TimerNode);

    /* Get the remaining Time for XconCCMWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }

    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: XconCCMWhileTimer = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;

    pEcfmAppTmr = &(pMepInfo->CcInfo.ErrCCMWhileTimer.TimerNode);

    /* Get the remaining time for ErrCCMWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pErrCCMWhileTimer = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;

    /* fill FngAlarmWhileTimer and ResetWhileTimer remaining time value */
    pEcfmAppTmr = &(pMepInfo->FngInfo.FngAlarmWhileTimer.TimerNode);

    /* Get the remaining time for ErrCCMWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }

    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pFngAlarmWhileTimer = [%d] \n", u4RemainingTime);

    /* Reset u4RemainigTime */
    u4RemainingTime = ECFM_INIT_VAL;
    pEcfmAppTmr = &(pMepInfo->FngInfo.FngResetWhileTimer.TimerNode);

    /* Get the remaining time for ErrCCMWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }

    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: pFngResetWhileTimer = [%d] \n", u4RemainingTime);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedFillRMepInfo                                  */
/*                                                                           */
/* Description        : This function fills the MEP Info in the RM Buffer    */
/*                                                                           */
/* Input(s)           : u4ContextId - Current context Id.                    */
/*                      pRMepInfo - Pointer to the RMEPinfo.                 */
/*                      pMsg     - Pointer to the buffer where information   */
/*                                 needs to be written.                      */
/*                      u2Offset - Offset from where the info is to be filled*/
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedFillRMepInfo (UINT4 u4ContextId, tEcfmCcRMepDbInfo * pRMepInfo,
                     tRmMsg * pMsg, UINT2 *pu2Offset)
{
    tEcfmAppTimer      *pEcfmAppTmr = NULL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, ECFM_RED_RMEP_INFO);

    /* Fill TLV length in the message and increment the offset */
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, (ECFM_RED_RMEP_INFO_SIZE
                                          - (ECFM_RED_TYPE_FIELD_SIZE +
                                             ECFM_RED_LEN_FIELD_SIZE)));

    /* Store the current context Id */
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4ContextId);

    /* Store the MEP related info in the buffer */
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pRMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pRMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pRMepInfo->u2MepId);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pRMepInfo->u2RMepId);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, pRMepInfo->u4SeqNum);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: u4SeqNum = [%d] \n", pRMepInfo->u4SeqNum);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1State);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: u1State = [%d] \n", pRMepInfo->u1State);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->b1RMepCcmDefect);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: b1RMepCcmDefect = [%d] \n", pRMepInfo->b1RMepCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1LastPortStatus);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: u1LastPortStatus = [%d] \n",
                  pRMepInfo->u1LastPortStatus);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1LastInterfaceStatus);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: u1LastInterfaceStatus = [%d] \n",
                  pRMepInfo->u1LastInterfaceStatus);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->b1RMepPortStatusDefect);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: b1RMepPortStatusDefect = [%d] \n",
                  pRMepInfo->b1RMepPortStatusDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset,
                        pRMepInfo->b1RMepInterfaceStatusDefect);
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: b1RMepInterfaceStatusDefect = [%d] \n",
                  pRMepInfo->b1RMepPortStatusDefect);
    ECFM_RM_PUT_N_BYTE (pMsg, pRMepInfo->RMepMacAddr, pu2Offset,
                        ECFM_MAC_ADDR_LENGTH);
    ECFM_RM_PUT_2_BYTE (pMsg, pu2Offset, pRMepInfo->u2OffloadRMepRxHandle);
    ECFM_RM_PUT_N_BYTE (pMsg, pRMepInfo->au1HwRMepHandler, pu2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);
    /* Reset u4RemainigTime for CC While Timer */
    u4RemainingTime = ECFM_INIT_VAL;

    pEcfmAppTmr = &(pRMepInfo->RMepWhileTimer.TimerNode);

    /* Get the remaining time for ErrCCMWhileTimer */
    if (ECFM_GET_REMAINING_TIME
        (ECFM_CC_TMRLIST_ID, pEcfmAppTmr, &u4RemainingTime) != ECFM_SUCCESS)

    {
        u4RemainingTime = ECFM_INIT_VAL;
    }
    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "RM: RMepWhileTimer = [%d] \n", u4RemainingTime);
    ECFM_RM_PUT_4_BYTE (pMsg, pu2Offset, u4RemainingTime);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->b1LastRdi);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1MepCcmLCStatus);
    ECFM_RM_PUT_1_BYTE (pMsg, pu2Offset, pRMepInfo->u1Y1731RemCCMRxCount);

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedGetNodeStateFromRm.                           */
/*                                                                           */
/* Description        : This function gets Node status from RM and stores in */
/*                      gEcfmRedGlobalInfo.NodeStatus.                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedGetNodeStateFromRm (VOID)
{
    UINT4               u4NodeState;
    u4NodeState = EcfmRmGetNodeState ();
    ECFM_LBLT_LOCK ();
    switch (u4NodeState)

    {
        case RM_ACTIVE:
            ECFM_NODE_STATUS () = ECFM_NODE_ACTIVE;
            break;
        case RM_STANDBY:
            ECFM_NODE_STATUS () = ECFM_NODE_STANDBY;
            break;
        default:
            ECFM_NODE_STATUS () = ECFM_NODE_IDLE;
            break;
    }
    ECFM_LBLT_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedHandleBulkUpdateEvent.                        */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      EcfmRedHandleBulkReqEvent is triggered.              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedHandleBulkUpdateEvent (VOID)
{
    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)

    {
        EcfmRedHandleBulkReqEvent ();
    }
    else
    {
        /* Only Active node can process bulk request and can
         * send the bulk update.*/
        ECFM_BULK_REQ_RECD () = ECFM_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : EcfmRedSendBulkUpdateTailMsg                         */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE.                         */
/*****************************************************************************/
PRIVATE INT4
EcfmRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2BufSize;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Node is not active. Bulk Update tail msg"
                      "not sent\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return ECFM_SUCCESS;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return ECFM_SUCCESS;
    }
    u2BufSize = ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT,
                      ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                      "RM Allocation failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return ECFM_FAILURE;
    }

    /* Form a bulk update tail message. 

     *        <--1 Byte-><----2 Byte----->
     ************************************** 
     *        *          *                *
     * RM Hdr * TAIL_MSG * Msg Length     *
     *        *          *                * 
     **************************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, ECFM_BULK_UPD_TAIL_MSG);
    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedHandleBulkReqEvent                            */
/*                                                                           */
/* Description        : This function sends bulk updates to the standby node.*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedHandleBulkReqEvent (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmMsg             *pCacheMsg = NULL;
    tEcfmCcPortInfo    *pPortEntry = NULL;
    tEcfmLbLtMepInfo   *pLbLtMepNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4BulkUpdPortCnt = ECFM_INIT_VAL;
    UINT4               u4PortNo = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4RemainingTime = ECFM_INIT_VAL;
    UINT2               u2PortUpdLen = ECFM_INIT_VAL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT2               u2OffSet = ECFM_INIT_VAL;
    UINT2               u2CacheOffSet = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;

    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    if (!ECFM_IS_SYSTEM_INITIALISED ())

    {

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        EcfmRedSendBulkUpdateTailMsg ();

        /* VLANGARP completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        EcfmRmSetBulkUpdatesStatus (RM_ECFM_APP_ID);
        return;
    }

    /* Send RMEP update to STANDBY before On-Demand Operations are started */
    EcfmRedSendRMEPUpdates ();

    u4BulkUpdPortCnt = ECFM_RED_NO_OF_PORTS_PER_SUB_UPDATE;
    for (u4ContextId = gEcfmRedGlobalInfo.u4BulkUpdNextContext;
         u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            gEcfmRedGlobalInfo.u4BulkUpdNextContext++;
            continue;
        }
        if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            gEcfmRedGlobalInfo.u4BulkUpdNextContext++;
            continue;
        }

        u2PortUpdLen =
            ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
            ECFM_PORT_ID_SIZE + ECFM_PORT_STATUS_SIZE;
        u2BufSize = ECFM_RED_MAX_MSG_SIZE;
        if (pMsg == NULL)

        {
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. unable to handle bulk req event\n");

                /* "RM alloc failed. Bulk updates not sent\n");
                 */
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                EcfmRmHandleProtocolEvent (&ProtoEvt);
                ECFM_CC_RELEASE_CONTEXT ();
                return;
            }
        }

        /*
         *    PORT Update message
         *    
         *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 1 bytes ->|
         *    -----------------------------------------------------
         *    | Msg. Type |   Length    | PortId     | Oper Status |
         *    |     (1)   |1 + 2 + 4 + 1|            |             |
         *    |----------------------------------------------------
         *
         */
        for (u4PortNo = gEcfmRedGlobalInfo.u2BulkUpdNextPort;
             u4PortNo <= ECFM_CC_MAX_PORT_INFO && u4BulkUpdPortCnt > 0;
             u4PortNo++)

        {
            gEcfmRedGlobalInfo.u2BulkUpdNextPort++;
            pPortEntry = ECFM_CC_GET_PORT_INFO (u4PortNo);
            if (pPortEntry == NULL)

            {
                continue;
            }
            if ((u2BufSize - u2OffSet) < u2PortUpdLen)

            {

                /* no room for the current message */
                if (EcfmRedSendMsgToRm (pMsg, u2OffSet) == ECFM_FAILURE)

                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    EcfmRmHandleProtocolEvent (&ProtoEvt);
                }
                pMsg = RM_ALLOC_TX_BUF (u2BufSize);
                if (pMsg == NULL)

                {
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                                  "RM alloc  failed. unable to handle bulk req event\n");
                    ECFM_CC_RELEASE_CONTEXT ();
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    EcfmRmHandleProtocolEvent (&ProtoEvt);
                    return;
                }
                u2OffSet = 0;
            }
            ECFM_RM_PUT_1_BYTE (pMsg, &u2OffSet, ECFM_PORT_OPER_STATUS_MSG);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2PortUpdLen -
                                (ECFM_RED_TYPE_FIELD_SIZE +
                                 ECFM_RED_LEN_FIELD_SIZE));
            ECFM_RM_PUT_4_BYTE (pMsg, &u2OffSet, pPortEntry->u4IfIndex);
            ECFM_RM_PUT_1_BYTE (pMsg, &u2OffSet, pPortEntry->u1IfOperStatus);
            if ((u1MemEstFlag == 1)
                && (ECFM_HR_STATUS () != ECFM_HR_STATUS_DISABLE))
            {
                u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT1);
                IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                            (CHR1 *) "tEcfmCcPortInfo",
                                            u4BulkUnitSize);
                u1MemEstFlag = ECFM_PORT_OPER_STATUS_MSG;
            }

            u4BulkUpdPortCnt--;
        }
        if (u4BulkUpdPortCnt == 0)

        {
            break;
        }

        else if (ECFM_CC_GET_PORT_INFO (u4PortNo) == NULL)

        {
            gEcfmRedGlobalInfo.u2BulkUpdNextPort = 0;
            gEcfmRedGlobalInfo.u4BulkUpdNextContext++;
            /*Sync MIB-DB Hold timer */
            if (ECFM_GET_REMAINING_TIME
                (ECFM_CC_TMRLIST_ID,
                 &(ECFM_CC_TMR_MIP_DB_HOLD_TMR.TimerNode),
                 &u4RemainingTime) == ECFM_SUCCESS)

            {

                /* Send the Remaining time of MIP-CCDB Hold timer */
                EcfmRedSyncMipDbHldTmr (u4RemainingTime);
            }

            /*Sync LTR-Cache Hold Timer */
            ECFM_LBLT_LOCK ();
            if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
            {
                ECFM_LBLT_UNLOCK ();
                RM_FREE (pMsg);
                return;
            }
            if (ECFM_GET_REMAINING_TIME
                (ECFM_LBLT_TMRLIST_ID,
                 &(ECFM_LBLT_LTR_HOLD_TIMER.TimerNode),
                 &u4RemainingTime) == ECFM_SUCCESS)

            {

                /* Send the Remaining time of LTR Cache timer */
                EcfmRedSyncLtrCacheHldTmr (u4RemainingTime);
            }

            if (ECFM_GET_REMAINING_TIME
                (ECFM_LBLT_TMRLIST_ID,
                 &(ECFM_LBLT_LBR_HOLD_TIMER.TimerNode),
                 &u4RemainingTime) == ECFM_SUCCESS)

            {

                /* Send the Remaining time of LBR Cache timer */
                EcfmRedSyncLbrCacheHldTmr (u4RemainingTime);
            }

            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();

            /* Sync Error Log Entries in STANDBY Node for each Context */
            if ((ECFM_CC_IS_ERR_LOG_ENABLED () == ECFM_TRUE) &&
                (ECFM_IS_CC_ERR_LOG_EMPTY () == ECFM_FALSE))
            {
                EcfmRedSyncErrLog ();
            }

            /* Get All MEPs configured in the system and check its LB, LM, DM , TST 
             * status. If any transaction status is not ready sync that status in the 
             * STANDBY Node 
             */

            pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);
            while (pMepNode != NULL)
            {
                /* Get LM Transmission status */
                if (pMepNode->LmInfo.u1TxLmmStatus != ECFM_TX_STATUS_READY)
                {
                    /* Send Loopback transmit Status sync Message to STANDBY Node */
                    EcfmRedSyncLmTransStatus (ECFM_CC_CURR_CONTEXT_ID (),
                                              pMepNode->u4MdIndex,
                                              pMepNode->u4MaIndex,
                                              pMepNode->u2MepId,
                                              pMepNode->LmInfo.u1TxLmmStatus);
                }
                /* Get Next MEP from the Global RBTree maintained */
                pMepNode = RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
            }
            ECFM_LBLT_LOCK ();
            if (ECFM_LBLT_SELECT_CONTEXT (ECFM_CC_CURR_CONTEXT_ID ()) !=
                ECFM_SUCCESS)
            {
                ECFM_LBLT_UNLOCK ();
                RM_FREE (pMsg);
                return;
            }

            pLbLtMepNode = RBTreeGetFirst (ECFM_LBLT_MEP_TABLE);
            while (pLbLtMepNode != NULL)
            {
                if ((pLbLtMepNode->LbInfo.u1TxLbmStatus != ECFM_TX_STATUS_READY)
                    || (pLbLtMepNode->TstInfo.u1TstStatus !=
                        ECFM_TX_STATUS_READY)
                    || (pLbLtMepNode->DmInfo.u1DmStatus != ECFM_TX_STATUS_READY)
                    || (pLbLtMepNode->ThInfo.u1ThStatus !=
                        ECFM_TX_STATUS_READY))
                {
                    /* Send Loopback transmit Status sync Message to STANDBY Node */
                    EcfmRedSyncLbLtTransStatus (ECFM_LBLT_CURR_CONTEXT_ID (),
                                                pLbLtMepNode->u4MdIndex,
                                                pLbLtMepNode->u4MaIndex,
                                                pLbLtMepNode->u2MepId,
                                                pLbLtMepNode->LbInfo.
                                                u1TxLbmStatus,
                                                pLbLtMepNode->TstInfo.
                                                u1TstStatus,
                                                pLbLtMepNode->DmInfo.u1DmStatus,
                                                pLbLtMepNode->ThInfo.
                                                u1ThStatus);
                }
                /* Get Next MEP from the Global RBTree maintained */
                pLbLtMepNode =
                    RBTreeGetNext (ECFM_LBLT_MEP_TABLE, pLbLtMepNode, NULL);
            }

            if ((pCacheMsg = RM_ALLOC_TX_BUF (ECFM_RED_TYPE_FIELD_SIZE +
                                              ECFM_RED_LEN_FIELD_SIZE + 5)) ==
                NULL)
            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. unable to handle bulk req event\n");

                /* "RM alloc failed. Bulk updates not sent\n");
                 */
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                EcfmRmHandleProtocolEvent (&ProtoEvt);
                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();
                ECFM_CC_RELEASE_CONTEXT ();
                RM_FREE (pMsg);
                return;
            }

            u2CacheOffSet = 0;
            /*Send LB Cache Status Backup, this is required to be synched in case a
             * throughput transaction is running and switchover takes place*/
            ECFM_RM_PUT_1_BYTE (pCacheMsg, &u2CacheOffSet,
                                ECFM_RED_LBR_CACHE_BACKUP_MSG);
            ECFM_RM_PUT_2_BYTE (pCacheMsg, &u2CacheOffSet,
                                ECFM_LBR_CACHE_STATUS_BCKUP_MSG_SIZE);
            ECFM_RM_PUT_4_BYTE (pCacheMsg, &u2CacheOffSet,
                                ECFM_LBLT_CURR_CONTEXT_ID ());
            ECFM_RM_PUT_1_BYTE (pCacheMsg, &u2CacheOffSet,
                                ECFM_LBLT_CURR_CONTEXT_INFO ()->
                                u1LbrCacheStatusBckup);

            if (u1MemEstFlag == ECFM_RED_SYNC_LM_TRANS_STS)
            {
                u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT1);

                IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                            (CHR1 *) "tEcfmLbLtContextInfo",
                                            u4BulkUnitSize);
                u1MemEstFlag = ECFM_RED_LBR_CACHE_BACKUP_MSG;
            }

            ECFM_LBLT_RELEASE_CONTEXT ();
            ECFM_LBLT_UNLOCK ();
            /* Send the buffer out */
            if (EcfmRedSendMsgToRm (pCacheMsg, u2CacheOffSet) == ECFM_FAILURE)

            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                EcfmRmHandleProtocolEvent (&ProtoEvt);
                RM_FREE (pMsg);
                return;
            }
        }
        ECFM_CC_RELEASE_CONTEXT ();
    }
    if (u2OffSet != 0)

    {

        /* Send the buffer out */
        if (EcfmRedSendMsgToRm (pMsg, u2OffSet) == ECFM_FAILURE)

        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            EcfmRmHandleProtocolEvent (&ProtoEvt);
        }
    }

    else if (pMsg)

    {

        /* Empty buffer created without any update messages filled */
        RM_FREE (pMsg);
    }
    if (gEcfmRedGlobalInfo.u4BulkUpdNextContext < ECFM_MAX_CONTEXTS)

    {

        /* Send an event to start the next sub bulk update. */
        if (ECFM_SEND_EVENT (ECFM_CC_TASK_ID, ECFM_EV_RED_BULK_UPD)
            == OSIX_FAILURE)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                          "Unable to send event\n");
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            EcfmRmHandleProtocolEvent (&ProtoEvt);
        }
        return;
    }

    else

    {

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        EcfmRedSendBulkUpdateTailMsg ();

        /* VLANGARP completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        EcfmRmSetBulkUpdatesStatus (RM_ECFM_APP_ID);
    }
    EcfmRedSendPeriodicUpdates ();
    for (u4ContextId = ECFM_DEFAULT_CONTEXT;
         u4ContextId <= ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_MODULE_DISABLED (u4ContextId))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            continue;
        }

        /* Sync LM Cache at CC Task */
        EcfmRedSyncLmCache ();

        /* Sync LB and DM Cache at LBLT Task */
        ECFM_LBLT_LOCK ();
        if (ECFM_LBLT_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
        {
            EcfmRedSyncDmCache ();
            EcfmRedSyncLbCache ();
            ECFM_LBLT_UNLOCK ();
            ECFM_CC_RELEASE_CONTEXT ();
            return;
        }
        /* Sync DM Cache */
        EcfmRedSyncDmCache ();

        /* Sync LB Cache */
        EcfmRedSyncLbCache ();
        ECFM_LBLT_RELEASE_CONTEXT ();
        ECFM_LBLT_UNLOCK ();
        ECFM_CC_RELEASE_CONTEXT ();
    }
}

/*****************************************************************************/
/* Function Name      : EcfmRestartModule                                    */
/*                                                                           */
/* Description        : This function will restart ECFM module.              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE.                         */
/*****************************************************************************/
PUBLIC INT4
EcfmRestartModule (VOID)
{
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4BackUpY1731Status = ECFM_INIT_VAL;
    UINT4               u4BackUpModuleStatus = ECFM_INIT_VAL;

    for (u4ContextId = 0; u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)

    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_SYSTEM_SHUTDOWN (u4ContextId))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            continue;
        }
        u4BackUpY1731Status = ECFM_Y1731_STATUS (ECFM_CC_CURR_CONTEXT_ID ());
        u4BackUpModuleStatus = ECFM_MODULE_STATUS (ECFM_CC_CURR_CONTEXT_ID ());

        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM Module getting restarted .... \r\n");
        if (u4BackUpModuleStatus == ECFM_ENABLE)
        {
            EcfmUtilModuleDisable ();
            EcfmUtilModuleEnable ();
        }
        if (u4BackUpY1731Status == ECFM_ENABLE)
        {
            EcfmUtilY1731Enable ();
        }
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "module resatrt for context \r\n");
        ECFM_CC_RELEASE_CONTEXT ();
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedTrigHigherLayer                               */
/*                                                                           */
/* Description        : This function will send the given event to the next  */
/*                      higher layer (can be ELMI or SNOOP.                  */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type. L2_INITIATE_BULK_UPDATES  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedTrigHigherLayer (UINT1 u1TrigType)
{

#if defined (ELMI_WANTED)
    EcfmElmRedRcvPktFromRm (u1TrigType, NULL, 0);

#elif defined (IGS_WANTED) || defined (MLDS_WANTED)    /* SNOOP_APPROACH */
    EcfmSnoopRedRcvPktFromRm (u1TrigType, NULL, 0);

#elif defined (LLDP_WANTED)
    EcfmLldpRedRcvPktFromRm (u1TrigType, NULL, 0);

#else /*  */
    if ((ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))

    {

        /* Send an event to RM to notify that
         * sync up is completed */
        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFMNode send event L2_SYNC_UP_COMPLETED"
                          "to RM failed \r\n");
        }
    }

#endif /*  */
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncErrorLogEntry                             */
/*                                                                           */
/* Description        : This function will send the Error Log Entry event to */
/*                      STANDBY node                                         */
/*                                                                           */
/* Input(s)           : pErrorNode - Pointer to the error Node               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncErrorLogEntry (tEcfmCcErrLogInfo * pErrorNode)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the Sync Log Entry Message\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_ERROR_LOG_ENTRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_ERROR_LOG_ENTRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_ERROR_LOG_ENTRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrorNode->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrorNode->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pErrorNode->u2MepId);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pErrorNode->u2RmepId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrorNode->u4SeqNum);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrorNode->TimeStamp);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pErrorNode->u2LogType);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessErrorLogEntry                          */
/*                                                                           */
/* Description        : This function process the received Error Log Entry   */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessErrorLogEntry (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcErrLogInfo  *pErrorNode = NULL;
    tEcfmCcErrLogInfo  *pFirstCcErrorNode = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessErrorLogEntry: Process Error Log Entry"
                      "in case of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessErrorLogEntry: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessErrorLogEntry: "
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessErrorLogEntry: "
                      "Cannot Handle RDI Timer Expiry in stand-by\r\n");
        return;
    }
    if (ECFM_CC_IS_ERR_LOG_DISABLED () == ECFM_TRUE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessErrorLogEntry: "
                      "Error Log Caching is disabled in STAND-BY\r\n");
        return;
    }

    /* Allocate a memory block for CC Error node */
    ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE (pErrorNode);
    while (pErrorNode == NULL)
    {

        /* Memory allocation failed, so now check if the memory pool is 
         * exhausted
         */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessErrorLogEntry: "
                      "failure allocating memory for Cc Error buffer node\r\n");
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_CC_ERR_LOG_POOL) != ECFM_INIT_VAL)

        {

            /* memory failure failure has occured even when we have free 
             * memory in the pool
             */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "EcfmRedProcessErrorLogEntry: "
                          "Cc Error memory pool corruption\r\n");
            ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
            return;
        }

        /* Memory pool is exhausted for sure, now we can delete the first entry
         * in the pool to have some free memory
         */
        pFirstCcErrorNode = (tEcfmCcErrLogInfo *)
            RBTreeGetFirst (ECFM_CC_ERR_LOG_TABLE);
        if (pFirstCcErrorNode == NULL)

        {

            /* Memory pool is exhausted and we dont have anyting in the CC
             * Error table to free.*/
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "EcfmRedProcessErrorLogEntry:"
                          "Cc Error buffer table corruption \r\n");
            return;
        }

        /* Remove the first node from the table */
        RBTreeRem (ECFM_CC_ERR_LOG_TABLE, pFirstCcErrorNode);

        /* Free the memory for the node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL, (UINT1 *) pFirstCcErrorNode);
        pFirstCcErrorNode = NULL;

        /* Now we can again try for allocating memory */
        ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE (pErrorNode);
    }
    ECFM_MEMSET (pErrorNode, ECFM_INIT_VAL, sizeof (tEcfmCcErrLogInfo));

    /* Get Error Log Information */
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pErrorNode->u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pErrorNode->u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, pErrorNode->u2MepId);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, pErrorNode->u2RmepId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pErrorNode->u4SeqNum);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pErrorNode->TimeStamp);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, pErrorNode->u2LogType);

    /* Add RBTree Node */
    if (RBTreeAdd (ECFM_CC_ERR_LOG_TABLE, pErrorNode) != ECFM_RB_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "RBTree Add Failed!! \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL, (UINT1 *) pErrorNode);
        return;
    }
    ECFM_CC_ERR_LOG_SEQ_NUM = ECFM_CC_ERR_LOG_SEQ_NUM + ECFM_INCR_VAL;

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (pErrorNode->u4MdIndex,
                                             pErrorNode->u4MaIndex,
                                             pErrorNode->u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: MEP Not Found \r\n");
        return;
    }
    if (pErrorNode->u2LogType == ECFM_MISMERGE_DFCT_EXIT)
    {
        pMepInfo->CcInfo.XConTimeStamp = 0;
    }
    else if (pErrorNode->u2LogType == ECFM_UNEXP_MEP_DFCT_EXIT)
    {
        pMepInfo->CcInfo.ErrTimeStamp = 0;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLckCondition                              */
/*                                                                           */
/* Description        : This function will send the set LCK Condition event  */
/*                      to  STANDBY node                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info for which LCK cond    */
/*                                 is to be set.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLckCondition (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedSyncLckCondition: Only the Active node needs to"
                      "send the set LCK Condition \n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedSyncLckCondition: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LCK_CONDITION_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_LCK_CONDITION_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LCK_CONDITION);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->LckInfo.b1LckCondition);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        pMepInfo->LckInfo.u4LckRcvdRxWhileValue);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLckStatus                                 */
/*                                                                           */
/* Description        : This function will send the set LCK Status event     */
/*                      to  STANDBY node                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info for which LCK cond    */
/*                                 is to be set.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLckStatus (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedSyncLckStatus: Only the Active node needs to"
                      "send the set LCK Condition \n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedSyncLckCondition: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LCK_STATUS_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_LCK_STATUS_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LCK_STATUS);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLckCondition                           */
/*                                                                           */
/* Description        : This function process the received LCK Codition      */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLckCondition (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    BOOL1               b1LckCond = ECFM_FALSE;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process Error Log Entry"
                      "in case of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessLckCondition: CC Data corruption "
                      "=[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessLckCondition: Context-Id not"
                      "valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessLckCondition: Cannot Handle"
                      "Set LCK Condition in stand-by\r\n");
        return;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessLckCondition : " "MEP Not Found \r\n");
        return;
    }

    /* Set LOCK condition for this MEP */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, b1LckCond);
    pMepNode->LckInfo.b1LckCondition = b1LckCond;

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        pMepNode->LckInfo.u4LckRcvdRxWhileValue);

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLckStatus                              */
/*                                                                           */
/* Description        : This function process the received LCK Codition      */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLckStatus (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LCK Status "
                      "in case of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM  Process LCK Status : CC Data corruption "
                      "=[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: ProcessLckStatus : Context-Id not"
                      "valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: ProcessLckStatus : Cannot Handle"
                      "Process LCK STATUS  in stand-by\r\n");
        return;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LCK STATUS : " "MEP Not Found \r\n");
        return;
    }

    /* Set LOCK Status for this MEP */
    EcfmCcUpdateLckStatus (pMepNode);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLckPeriodTimeout                          */
/*                                                                           */
/* Description        : This function will send the LCK Period Timeout event */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLckPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "LCK Period Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LCK_PERIOD_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_LCK_PERIOD_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LCK_PERIOD_TIMER_EXPIRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLckPeriodTmrExpiry                     */
/*                                                                           */
/* Description        : This function process the received LCK Period Expiry */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLckPeriodTmrExpiry (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LCK Period Timer Expiry in case"
                      "of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: CC Data corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Cannot Handle LCK Timer Expiry in stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }

    /* Call Timer Expiry Handle for LCK Period Timer */
    EcfmCcLckPeriodTimeout (pMepNode);
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncAisCondition                              */
/*                                                                           */
/* Description        : This function will send the set AIS Condition event  */
/*                      to  STANDBY node                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info for which AIS cond    */
/*                                 is to be set.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncAisCondition (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the set AIS Condition \n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_AIS_CONDITION_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_AIS_CONDITION_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_AIS_CONDITION);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->AisInfo.b1AisCondition);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        pMepInfo->AisInfo.u4AisRcvdRxWhileValue);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessAisCondition                           */
/*                                                                           */
/* Description        : This function process the received AIS Codition      */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessAisCondition (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    BOOL1               b1AisCond = ECFM_FALSE;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process AIS Condition Set"
                      "in case of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: : CC Data corruption " "=[%d][%d].\n",
                      u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM : Can't Handle Set AIS Condition in stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }

    /* Set AIS condition for this MEP */
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, b1AisCond);
    pMepNode->AisInfo.b1AisCondition = b1AisCond;
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        pMepNode->AisInfo.u4AisRcvdRxWhileValue);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncAisPeriodTimeout                          */
/*                                                                           */
/* Description        : This function will send the AIS Period Timeout event */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncAisPeriodTimeout (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "AIS Period Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_AIS_PERIOD_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_AIS_PERIOD_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_AIS_PERIOD_TIMER_EXPIRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessAisPeriodTmrExpiry                     */
/*                                                                           */
/* Description        : This function process the received LCK Period Expiry */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessAisPeriodTmrExpiry (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process AIS Period Timer Expiry in case"
                      "of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: CC Data corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Cannot Handle AIS Timer Expiry in stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM : MEP Not Found \r\n");
        return;
    }

    /* Call Timer Expiry Handle for LCK Period Timer */
    EcfmCcAisPeriodTimeout (pMepNode);
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedClearLmSyncFlag                               */
/*                                                                           */
/* Description        : This function clears the Sync Flag maintained in LM  */
/*                      Buffer.                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedClearLmSyncFlag ()
{
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;

    /* Get LM Buffer First Node */
    pFrmLossBuffNode = (tEcfmCcFrmLossBuff *)
        RBTreeGetFirst (ECFM_CC_FL_BUFFER_TABLE);
    while (pFrmLossBuffNode != NULL)

    {

        /* Clear the Sync Flag */
        pFrmLossBuffNode->b1Synched = ECFM_FALSE;

        /* Get Next LM Buffer Entry */
        pFrmLossBuffNode = (tEcfmCcFrmLossBuff *)
            RBTreeGetNext (ECFM_CC_FL_BUFFER_TABLE, pFrmLossBuffNode, NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmCache                                   */
/*                                                                           */
/* Description        : This function  syncs the LM Cache at CC Task at      */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLmCache ()
{
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    tEcfmMacAddr        TempMacAddr;
    tRmMsg             *pMsg = NULL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2Length = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT4               u4TransId = ECFM_INIT_VAL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen = ECFM_INIT_VAL;
    UINT2               u2SyncOffset = ECFM_INIT_VAL;
    UINT2               u2LmOffset = ECFM_INIT_VAL;
    UINT2               u2LmInfoLen = ECFM_INIT_VAL;
    BOOL1               b1BuffFull = ECFM_TRUE;
    UINT4               u4BulkUnitSize = 0;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      " LbLt Pdu SyncUp message .\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red LbLt sync up msgs can not be sent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize = ECFM_RED_MAX_MSG_SIZE;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);

    /* Allocate memory to the pMsg for Maximum PDU Size */
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);
    if (pMsg == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM alloc  failed. Bulk updates not sent\n");
        return;
    }
    u2Offset = ECFM_INIT_VAL;

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

    /* Fill the number of size of  information to be synced up.
     * Here only zero will be filled. The following macro is called
     * to move the pointer by 2 bytes.*/
    u2SyncOffset = u2Offset;
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);

    /* Update LM Buffer Information */
    pFrmLossBuffNode = (tEcfmCcFrmLossBuff *)
        RBTreeGetFirst (ECFM_CC_FL_BUFFER_TABLE);

    /* Get LM Buffer Info and Fill in the RM Buffer to transmit Sync Up message */
    while (pFrmLossBuffNode != NULL)

    {

        /* Check if the  node is for the same transaction or not. If not,
         * then we need to send the indexes too in the RM message otherwise,
         * just the LM buffer LM info.
         */
        if ((u4MdIndex != pFrmLossBuffNode->u4MdIndex) ||
            (u4MaIndex != pFrmLossBuffNode->u4MaIndex) ||
            (u2MepId != pFrmLossBuffNode->u2MepId) ||
            (u4TransId != pFrmLossBuffNode->u4TransId) ||
            (ECFM_MEMCMP
             (pFrmLossBuffNode->PeerMepMacAddress, TempMacAddr, 6) != 0))

        {
            u2Length = ECFM_RED_LM_BUFF_FULL_INFO_SIZE;
            b1BuffFull = ECFM_TRUE;
        }

        else

        {
            u2Length = ECFM_RED_LM_BUFF_INFO_SIZE;
            b1BuffFull = ECFM_FALSE;
        }

        /* There is no enough space to fill this port information into
         * buffer.
         * Hence send the existing message and construct new message to
         * fill the information of other MEPs.
         * */
        if ((u2BufSize - u2Offset) < u2Length)

        {

            /* Update Message Length */
            u2SyncMsgLen = u2SyncMsgLen + u2LmInfoLen;

            /* u2Offset contains the number of bytes written in the
             * buffer, so can be used as no of bytes to transmit
             */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer for LM buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;
            u2LmInfoLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

            /* Offset of the Length field for update information is
             * stored in u4SyncLengthOffset. u2SyncMsgLen contains the size of
             * update information so far written. Hence update length field at
             * offset u4SyncLengthOffset with the value u2SyncMsgLen.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            u4MdIndex = ECFM_INIT_VAL;
            u4MaIndex = ECFM_INIT_VAL;
            u2MepId = ECFM_INIT_VAL;
            u4TransId = ECFM_INIT_VAL;
            ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
            b1BuffFull = ECFM_TRUE;
        }

        /* Fill in Value field the LM Buffer Info */
        /* Fill TLV Type in the message and Increment the offset */
        if (pFrmLossBuffNode->b1Synched == ECFM_FALSE)

        {
            if (b1BuffFull)

            {
                u2SyncMsgLen =
                    u2SyncMsgLen + u2LmInfoLen + ECFM_RED_TYPE_FIELD_SIZE +
                    ECFM_RED_LEN_FIELD_SIZE;

                /*New Transaction Buff started */
                u2LmInfoLen = 0;

                /* Fill Type */
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LM_BUFF_INFO);
                u2LmOffset = u2Offset;

                /* Fill TLV length in the message and increment the offset */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2LmInfoLen);

                /* If New Node is for the same Transaction, then need not fill
                 * the indexes in the RM Buffer.
                 */
                /* Fill the Value in the message and increment the offset */
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    ECFM_CC_CURR_CONTEXT_ID ());
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmLossBuffNode->u4MdIndex);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmLossBuffNode->u4MaIndex);
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pFrmLossBuffNode->u2MepId);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmLossBuffNode->u4TransId);
                ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                    pFrmLossBuffNode->u4SeqNum);
                ECFM_RM_PUT_N_BYTE (pMsg, pFrmLossBuffNode->PeerMepMacAddress,
                                    &u2Offset, ECFM_MAC_ADDR_LENGTH);

                /* Increment Length for LM Indexes + Peer Mac Address which
                 * is same in all LM Buffer for a same transaction */
                u2LmInfoLen = u2LmInfoLen + ECFM_RED_LM_COMMON_INFO_SIZE;
            }
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pFrmLossBuffNode->RxTimeStamp);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmLossBuffNode->u4MeasurementInterval);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmLossBuffNode->u4NearEndLoss);
            ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                                pFrmLossBuffNode->u4FarEndLoss);
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                                pFrmLossBuffNode->u1LossMeasurementType);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                pFrmLossBuffNode->u2NoOfLMMSent);
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                                pFrmLossBuffNode->u2NoOfLMRRcvd);

            if (u1MemEstFlag == ECFM_RED_LBR_CACHE_BACKUP_MSG)
            {
                u4BulkUnitSize =
                    ECFM_RED_LM_COMMON_INFO_SIZE + ECFM_RED_LM_BUFF_INFO_SIZE;
                IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                            (CHR1 *) "tEcfmCcFrmLossBuff",
                                            u4BulkUnitSize);
                u1MemEstFlag = ECFM_RED_LM_BUFF_INFO;
            }

            /* Update Sync Msg Length */
            u2LmInfoLen = u2LmInfoLen + ECFM_RED_LM_BUFF_INFO_SIZE;

            /* Fill TLV length in the message and increment the offset */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2LmOffset, u2LmInfoLen);
            u2LmOffset -= 2;

            /* Set b1Synched for the LM Buffer as TRUE */
            pFrmLossBuffNode->b1Synched = ECFM_TRUE;
        }

        /*Check required to handle the case where list is partially synched */
        else if (u4TransId == ECFM_INIT_VAL)

        {

            /* Get Next LM Buffer Entry */
            pFrmLossBuffNode = (tEcfmCcFrmLossBuff *)
                RBTreeGetNext (ECFM_CC_FL_BUFFER_TABLE, pFrmLossBuffNode, NULL);
            continue;
        }

        /* Store the last nodes Indexes to compare the new node indexes */
        u4MdIndex = pFrmLossBuffNode->u4MdIndex;
        u4MaIndex = pFrmLossBuffNode->u4MaIndex;
        u2MepId = pFrmLossBuffNode->u2MepId;
        u4TransId = pFrmLossBuffNode->u4TransId;
        ECFM_MEMCPY (TempMacAddr, pFrmLossBuffNode->PeerMepMacAddress,
                     ECFM_MAC_ADDR_LENGTH);

        /* Get Next LM Buffer Entry */
        pFrmLossBuffNode = (tEcfmCcFrmLossBuff *)
            RBTreeGetNext (ECFM_CC_FL_BUFFER_TABLE, pFrmLossBuffNode, NULL);
    }

    /* Update Message Length */
    u2SyncMsgLen = u2SyncMsgLen + u2LmInfoLen;
    if ((u2Offset - 2) != u2SyncOffset)

    {

        /* u2Offset contains the number of bytes written in the buffer,
         * so can be used as no of bytes to transmit */
        ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
        EcfmRedSendMsgToRm (pMsg, u2Offset);
    }

    else

    {

        /* Empty buffer created without any update messages filled */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Empty buffer created without any update"
                      "messages filled \r\n");
        RM_FREE (pMsg);
        pMsg = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmDeadlineTmrExp                         */
/*                                                                           */
/* Description        : This function will send LM DeadLine Timeout event    */
/*                      to STANDBY node.                                     */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLmDeadlineTmrExp (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "LM Deadline Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LM_DEADLINE_EXPIRY_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_LM_DEADLINE_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LM_DEADLINE_TIMER_EXPIRY);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmStopTransaction                         */
/*                                                                           */
/* Description        : This function will send LM Stop Transaction to       */
/*                      STANDBY Node when number of LMM to be transmitted    */
/*                      becomes 0.                                           */
/*                                                                           */
/* Input(s)           : pMepInfo - Pointer to MEP Info whose timer is to be  */
/*                                 synched.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncLmStopTransaction (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "AIS RxWhile Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_LM_STOP_TRANSACTION_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_LM_DEADLINE_EXPIRY_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_LM_STOP_TRANSACTION);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLmStopTransaction                      */
/*                                                                           */
/* Description        : This function process received LM Deadline Tmr Expiry*/
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLmStopTransaction (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmCcPduSmInfo    PduSmInfo;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process LM Deadline Timer Expiry in case"
                      "of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: CC Data corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Cannot Handle LM Deadline Tmr Expiry in"
                      "stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }
    ECFM_MEMSET (&PduSmInfo, ECFM_INIT_VAL, sizeof (tEcfmCcPduSmInfo));
    PduSmInfo.pMepInfo = pMepNode;

    /* Call Stop Lm Transaction for LM Deadline Timer Expiry */
    EcfmLmInitStopLmTransaction (&PduSmInfo);
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateLmBuffInfo                              */
/*                                                                           */
/* Description        : This function updates the LM Buffer Info received in */
/*                      Sync  msg.                                           */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu2Offset - Offset from where info is to be read     */
/*                      u2InfoLen - Length for LM Info received in RM        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedUpdateLmBuffInfo (tRmMsg * pMsg, UINT2 *pu2Offset, UINT2 u2InfoLen)
{
    tEcfmCcFrmLossBuff *pFrmLossBuffNode = NULL;
    tEcfmCcFrmLossBuff *pFirstFrmLossBuffEntry = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmMacAddr        TempMacAddr;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT4               u4TransId = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update LM BUFF-INFO in stand-by\r\n");
        return;
    }

    /* Indexes for the LM Buffer Info */
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2MepId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4TransId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4SeqNum);
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, sizeof (tEcfmMacAddr));
    ECFM_RM_GET_N_BYTE (pMsg, TempMacAddr, pu2Offset, ECFM_MAC_ADDR_LENGTH);
    u2InfoLen = u2InfoLen - ECFM_RED_LM_COMMON_INFO_SIZE;
    while (u2InfoLen)

    {

        /* Allocate a memory block for frame loss buffer node */
        ECFM_ALLOC_MEM_BLOCK_CC_FL_BUFF_TABLE (pFrmLossBuffNode);
        while (pFrmLossBuffNode == NULL)

        {

            /* Memory allocation failed, so now check if the memory pool is
             * exhausted
             */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM : failure allocating memory for LM buffer node\r\n");
            if (ECFM_GET_FREE_MEM_UNITS (ECFM_CC_FRM_LOSS_POOL) !=
                ECFM_INIT_VAL)

            {

                /* memory failure failure has occured even when we have free
                 * memory in the pool
                 */
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "ECFM : Frame Loss memory pool corruption\r\n");
                ECFM_CC_INCR_MEMORY_FAILURE_COUNT ();
                return;
            }

            /* Memory pool is exhausted for sure, now we can delete the first entry
             * in the pool to have some free memory
             */
            pFirstFrmLossBuffEntry = (tEcfmCcFrmLossBuff *)
                RBTreeGetFirst (ECFM_CC_FL_BUFFER_TABLE);
            if (pFirstFrmLossBuffEntry == NULL)

            {

                /* Memory pool is exhausted and we dont have anyting in the frame
                 * loss table to free.*/
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "ECFM RED: Frame loss buffer table corruption \r\n");
                return;
            }

            /* Remove the first node from the table */
            RBTreeRem (ECFM_CC_FL_BUFFER_TABLE, pFirstFrmLossBuffEntry);

            /* Free the memory for the node */
            ECFM_FREE_MEM_BLOCK (ECFM_CC_FRM_LOSS_POOL,
                                 (UINT1 *) pFirstFrmLossBuffEntry);
            pFirstFrmLossBuffEntry = NULL;

            /* Now we can again try for allocating memory */
            ECFM_ALLOC_MEM_BLOCK_CC_FL_BUFF_TABLE (pFrmLossBuffNode);
        } ECFM_MEMSET (pFrmLossBuffNode, ECFM_INIT_VAL,
                       sizeof (tEcfmCcFrmLossBuff));

        /* Fill the Node */
        pFrmLossBuffNode->u4MdIndex = u4MdIndex;
        pFrmLossBuffNode->u4MaIndex = u4MaIndex;
        pFrmLossBuffNode->u2MepId = u2MepId;
        pFrmLossBuffNode->u4TransId = u4TransId;
        ECFM_MEMCPY (pFrmLossBuffNode->PeerMepMacAddress, TempMacAddr,
                     ECFM_MAC_ADDR_LENGTH);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pFrmLossBuffNode->RxTimeStamp);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset,
                            pFrmLossBuffNode->u4MeasurementInterval);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pFrmLossBuffNode->u4NearEndLoss);
        ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, pFrmLossBuffNode->u4FarEndLoss);
        ECFM_RM_GET_1_BYTE (pMsg, pu2Offset,
                            pFrmLossBuffNode->u1LossMeasurementType);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pFrmLossBuffNode->u2NoOfLMMSent);
        ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, pFrmLossBuffNode->u2NoOfLMRRcvd);

        /* Fill Port Number in the LM Buffer */
        pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
        if (pMepInfo == NULL)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: MEP Not Found \r\n");
            ECFM_FREE_MEM_BLOCK (ECFM_CC_FRM_LOSS_POOL,
                                 (UINT1 *) pFrmLossBuffNode);
            return;
        }
        pMepInfo->pMaInfo->u4SeqNum = u4SeqNum;

        /* Increment the Sequence Number */
        pFrmLossBuffNode->u4SeqNum = u4SeqNum;

        /* Increment sequence number to add a new node in the Buffer */
        u4SeqNum++;

        /* Add the node into the table */
        if (RBTreeAdd (ECFM_CC_FL_BUFFER_TABLE, pFrmLossBuffNode) !=
            ECFM_RB_SUCCESS)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: LM Node add failure \r\n");
            ECFM_FREE_MEM_BLOCK (ECFM_CC_FRM_LOSS_POOL,
                                 (UINT1 *) pFrmLossBuffNode);
        }
        u2InfoLen = u2InfoLen - ECFM_RED_LM_BUFF_INFO_SIZE;
    } ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncLmTransStatus                             */
/*                                                                           */
/* Description        : This function will send the LM transmit              */
/*                      status to STANDBY node                               */
/*                                                                           */
/* Input(s)           : u4ContextId  - Current Context Id                    */
/*                      u4MdIndex    - MdIndex for the MEP                   */
/*                      u4MaIndex    - MaIndex for the MEP                   */
/*                      u2MepId      - MdIndex for the MEP                   */
/*                      u1LmTransSts - LM Transaction Status for the MEP     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedSyncLmTransStatus (UINT4 u4ContextId, UINT4 u4MdIndex, UINT4 u4MaIndex,
                          UINT2 u2MepId, UINT1 u1LmTransSts)
{
    tRmMsg             *pMsg;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the Trans Status \n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");
        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_SYNC_LM_TRANS_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_SYNC_LM_TRANS_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_LM_TRANS_STS);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2MepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, u1LmTransSts);

    if (u1MemEstFlag == ECFM_RED_ERR_LOG_INFO)
    {
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4) +
            sizeof (UINT2) + sizeof (UINT1);

        IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                    (CHR1 *) "tEcfmCcMepInfo", u4BulkUnitSize);
        u1MemEstFlag = ECFM_RED_SYNC_LM_TRANS_STS;
    }

    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessLmTransSts                             */
/*                                                                           */
/* Description        : This function process the received LM Transaction    */
/*                      Status Message.                                      */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessLmTransSts (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT1               u1LmTransSts = ECFM_INIT_VAL;
    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process AIS Condition Set"
                      "in case of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: : CC Data corruption " "=[%d][%d].\n",
                      u2Length, i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM : Can't Handle Sync LM transaction status"
                      "in stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1LmTransSts);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepNode == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: MEP Not Found \r\n");
        return;
    }
    /* Notify SM to start LM transaction */
    if (u1LmTransSts == ECFM_TX_STATUS_NOT_READY)
    {
        if (EcfmCcUtilPostTransaction (pMepNode, ECFM_LM_START_TRANSACTION)
            != ECFM_SUCCESS)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Post Event LM Start Transaction Failed\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncErrLog                                    */
/*                                                                           */
/* Description        : This function sync  Error Log Entries in STANDBY     */
/*                      Node.                                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedSyncErrLog ()
{
    tEcfmCcErrLogInfo  *pErrLogNode = NULL;
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncOffset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen = ECFM_INIT_VAL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT4               u4BulkUnitSize = 0;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to Sync Error"
                      "Log Cache.\n");
        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");
        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }

    u2BufSize = ECFM_RED_MAX_MSG_SIZE;

    /* Allocate memory to the pMsg for Maximum PDU Size */
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);
    if (pMsg == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM alloc failed. Bulk updates not sent\n");
        return;
    }

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

    /* Fill the number of size of  information to be synced up.
     * Here only zero will be filled. The following macro is called
     * to move the pointer by 2 bytes.*/
    u2SyncOffset = u2Offset;
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);

    /* Update LM Buffer Information */
    pErrLogNode = (tEcfmCcErrLogInfo *) RBTreeGetFirst (ECFM_CC_ERR_LOG_TABLE);

    /* Get ERROR Log Info and Fill in the RM Buffer to transmit Sync Up message */
    while (pErrLogNode != NULL)
    {
        /* There is no enough space to fill this port information into
         * buffer.
         * Hence send the existing message and construct new message to
         * fill the information of other MEPs.
         * */
        if ((u2BufSize - u2Offset) < ECFM_RED_ERR_LOG_INFO_SIZE)
        {
            /* Fill the number of size of  information to be synced up.
             * Here only zero will be filled. The following macro is
             * called to move the pointer by 2 bytes.
             */
            /* u2Offset contains the number of bytes written in the
             * buffer, so can be used as no of bytes to transmit
             */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);

            EcfmRedSendMsgToRm (pMsg, u2Offset);
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer for LB buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)
            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            /*ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_ERR_LOG_MSG); */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

            /* Offset of the Length field for update information is
             * stored in u4SyncLengthOffset. u2SyncMsgLen contains the size of
             * update information so far written. Hence update length field at
             * offset u4SyncLengthOffset with the value u2SyncMsgLen.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
        }

        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_ERR_LOG_INFO);

        /* Fill TLV length in the message and increment the offset */
        ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, ECFM_RED_ERR_LOG_INFO_SIZE);

        /* Fill the Value in the message and increment the offset */
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrLogNode->u4MdIndex);
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrLogNode->u4MaIndex);
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrLogNode->u4SeqNum);
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pErrLogNode->TimeStamp);
        ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pErrLogNode->u2RmepId);
        ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pErrLogNode->u2LogType);
        ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pErrLogNode->u2MepId);

        if (u1MemEstFlag == ECFM_RED_MIP_DB_HLD_TMR)
        {
            u4BulkUnitSize = ECFM_RED_ERR_LOG_INFO_SIZE;

            IssSzUpdateSizingInfoForHR ((CHR1 *) "ECFM",
                                        (CHR1 *) "tEcfmCcErrLogInfo",
                                        u4BulkUnitSize);
            u1MemEstFlag = ECFM_RED_ERR_LOG_INFO;
        }

        /* Update Sync Msg Length */
        u2SyncMsgLen = u2SyncMsgLen + ECFM_RED_ERR_LOG_INFO_SIZE;

        /* Get Next Error Log Entry */
        pErrLogNode =
            (tEcfmCcErrLogInfo *) RBTreeGetNext (ECFM_CC_ERR_LOG_TABLE,
                                                 pErrLogNode, NULL);
    }
    if ((u2Offset - 2) != u2SyncOffset)
    {
        /* u2Offset contains the number of bytes written in the buffer,
         * so can be used as no of bytes to transmit */
        ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
        EcfmRedSendMsgToRm (pMsg, u2Offset);
    }
    else
    {
        /* Empty buffer created without any update messages filled */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Empty buffer created without any update"
                      "messages filled \r\n");
        RM_FREE (pMsg);
        pMsg = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedUpdateErrLog                                  */
/*                                                                           */
/* Description        : This function Updates Error Log Entries in STANDBY   */
/*                      Node.                                                */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu2Offset - Offset from where info is to be read     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedUpdateErrLog (tRmMsg * pMsg, UINT2 *pu2Offset)
{
    tEcfmCcErrLogInfo  *pErrNode = NULL;
    tEcfmCcErrLogInfo   ErrNode;
    tEcfmCcErrLogInfo  *pTempErrNode = NULL;
    tEcfmCcErrLogInfo  *pFirstCcErrorNode = NULL;
    tEcfmSysTime        TimeStamp;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4SeqNum = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2RMepId = ECFM_INIT_VAL;
    UINT2               u2LogType = ECFM_INIT_VAL;

    ECFM_MEMSET (&ErrNode, 0, sizeof (tEcfmCcErrLogInfo));

    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4MaIndex);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, u4SeqNum);
    ECFM_RM_GET_4_BYTE (pMsg, pu2Offset, TimeStamp);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2RMepId);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2LogType);
    ECFM_RM_GET_2_BYTE (pMsg, pu2Offset, u2MepId);

    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "context-id %d not valid in stand-by\r\n", u4ContextId);
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "Cannot Update Error Log in stand-by"
                      "-- module disabled for context = %d\r\n", u4ContextId);
        return;
    }

    /* Get ERROR Log entry */
    ErrNode.u4MdIndex = u4MdIndex;
    ErrNode.u4MaIndex = u4MaIndex;
    ErrNode.u2MepId = u2MepId;
    ErrNode.u4SeqNum = u4SeqNum;

    pTempErrNode =
        (tEcfmCcErrLogInfo *) RBTreeGet (ECFM_CC_ERR_LOG_TABLE, &ErrNode);

    if (pTempErrNode != NULL)
    {
        /* Node already exists, no need to add */
        return;
    }

    /* Allocate a memory block for CC Error node */
    ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE (pErrNode);
    while (pErrNode == NULL)
    {
        /* Memory allocation failed, so now check if the memory pool is
         * exhausted */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Failure allocating memory for Cc Error buffer"
                      "node\r\n");
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_CC_ERR_LOG_POOL) != ECFM_INIT_VAL)
        {
            /* memory failure failure has occured even when we have free
             * memory in the pool
             */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Cc Error memory pool corruption\r\n");
            return;
        }

        /* Memory pool is exhausted for sure, now we can delete the first entry
         * in the pool to have some free memory
         */
        pFirstCcErrorNode = (tEcfmCcErrLogInfo *)
            RBTreeGetFirst (ECFM_CC_ERR_LOG_TABLE);
        if (pFirstCcErrorNode == NULL)
        {
            /* Memory pool is exhausted and we dont have anyting in the CC
             * Error table to free.*/
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM: Cc Error buffer table corruption \r\n");
            return;
        }
        /* Remove the first node from the table */
        RBTreeRem (ECFM_CC_ERR_LOG_TABLE, pFirstCcErrorNode);

        /* Free the memory for the node */
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL, (UINT1 *) pFirstCcErrorNode);
        pFirstCcErrorNode = NULL;

        /* Now we can again try for allocating memory */
        ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE (pErrNode);
    }

    pErrNode->u4MdIndex = u4MdIndex;
    pErrNode->u4MaIndex = u4MaIndex;
    pErrNode->u2MepId = u2MepId;
    pErrNode->u4SeqNum = u4SeqNum;
    pErrNode->TimeStamp = TimeStamp;
    pErrNode->u2RmepId = u2RMepId;
    pErrNode->u2LogType = u2LogType;

    /* Add RBTree Node */
    if (RBTreeAdd (ECFM_CC_ERR_LOG_TABLE, pErrNode) != ECFM_RB_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: RBTree Add Failed for Error Log Node!! \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_CC_ERR_LOG_POOL, (UINT1 *) pErrNode);
        return;
    }
    ECFM_CC_ERR_LOG_SEQ_NUM = ECFM_CC_ERR_LOG_SEQ_NUM + ECFM_INCR_VAL;
    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncCCDataOnChng                              */
/*                                                                           */
/* Description        : This function synchs information which needs to be   */
/*                      synched immediately when the value is updated on     */
/*                      ACTIVE                                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Id                     */
/*                      u4MdIndex - Domain Index                             */
/*                      u4MaIndex - Maintenance Association ID               */
/*                      u2MepId - Maintenance End Poind ID                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedSyncCCDataOnChng (UINT4 u4ContextId, UINT4 u4MdIndex, UINT4 u4MaIndex,
                         UINT2 u2MepId, UINT2 u2RMepId)
{

    tRmMsg             *pMsg;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    tMacAddr            RMepMacAddr = {
        0
    };

    UNUSED_PARAM (u4ContextId);

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the immediate data Sync Messages\n");

        /*Only the Active node needs to send the SyncUp message */
        return ECFM_FAILURE;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return ECFM_SUCCESS;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_IMMEDIATE_SYNCH_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return ECFM_FAILURE;
    }
    u2SyncMsgLen = ECFM_IMMEDIATE_SYNCH_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_IMMEDIATE_SYNCH_MSG);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2MepId);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2RMepId);

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : MEP not found\n");
        RM_FREE (pMsg);
        return ECFM_FAILURE;
    }

    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1LocalLinkFailure);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1InternalHwFailure);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1InternalSwFailure);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1MacStatusChanged);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u1PortState);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u1IfStatus);

    if (u2RMepId != ECFM_INIT_VAL)
    {
        /* Get MEP from Global RBTree with received MdIndex, MaIndex, u2MepId */
        pRMepInfo =
            EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u2MepId, u2RMepId);
        if (pRMepInfo == NULL)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                          "ECFM : RMEP not found\n");
            RM_FREE (pMsg);
            return ECFM_FAILURE;
        }

        ECFM_RM_PUT_N_BYTE (pMsg, pRMepInfo->RMepMacAddr, &u2Offset,
                            ECFM_MAC_ADDR_LENGTH);
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pRMepInfo->u4FailedOkTime);
    }
    else
    {
        ECFM_RM_PUT_N_BYTE (pMsg, RMepMacAddr, &u2Offset, ECFM_MAC_ADDR_LENGTH);
        ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
    }

    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessSyncCcDataOnChng                       */
/*                                                                           */
/* Description        : This function updates information received           */
/*                      in  immediate synch message from ACTIVE              */
/*                                                                           */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessSyncCcDataOnChng (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{

    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    tEcfmCcMepInfo     *pMepInfo = NULL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2RMepId = ECFM_INIT_VAL;
    BOOL1               b1RcvdMacStatus = ECFM_FALSE;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSyncCcDataOnChng: Process Error Log Entry"
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSyncCcDataOnChng: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_SUCCESS;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSyncCcDataOnChng: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSyncCcDataOnChng: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2RMepId);

    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1LocalLinkFailure);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1InternalHwFailure);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1InternalSwFailure);

    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, b1RcvdMacStatus);
    if ((b1RcvdMacStatus != pMepInfo->CcInfo.b1MacStatusChanged) &&
        (pMepInfo->pMaInfo->u1CcmInterval >= ECFM_CCM_INTERVAL_10_S) &&
        (b1RcvdMacStatus == FALSE))
    {
        pMepInfo->CcInfo.u4CciSentCcms = pMepInfo->CcInfo.u4CciSentCcms + 1;
        pMepInfo->CcInfo.u4CciCcmCount++;
    }

    pMepInfo->CcInfo.b1MacStatusChanged = b1RcvdMacStatus;
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u1PortState);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u1IfStatus);

    if (u2RMepId != ECFM_INIT_VAL)
    {
        /* Get MEP from Global RBTree with received MdIndex, MaIndex, u2MepId */
        pRMepInfo =
            EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u2MepId, u2RMepId);
        if (pRMepInfo == NULL)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: Remote MEP Not Found \r\n");
            return ECFM_FAILURE;
        }

        ECFM_RM_GET_N_BYTE (pMsg, pRMepInfo->RMepMacAddr, &u2Offset,
                            ECFM_MAC_ADDR_LENGTH);
        /* Updated RMEP MAC address at LBLT also */
        if (EcfmLbLtAddRMepDbEntry (ECFM_CC_CURR_CONTEXT_ID (), pRMepInfo)
            != ECFM_SUCCESS)
        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: Remote MEP addition in LBLT failed\r\n");
            return ECFM_FAILURE;
        }

        ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pRMepInfo->u4FailedOkTime);
    }
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncSmData                                    */
/*                                                                           */
/* Description        : This function will send the CC State Machine data to */
/*                      STANDBY node                                         */
/*                                                                           */
/* Input(s)           : pPduSmInfo - Context/Mep/RMEP/Mip information        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
EcfmRedSyncSmData (tEcfmCcPduSmInfo * pPduSmInfo)
{

    tRmMsg             *pMsg;
    tEcfmCcRMepDbInfo   RMepInfo;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT2               u2RMepId = ECFM_INIT_VAL;

    ECFM_MEMSET (&RMepInfo, ECFM_INIT_VAL, ECFM_CC_RMEP_CCM_DB_SIZE);

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the State Machine information Message\n");

        /*Only the Active node needs to send the SyncUp message */
        return ECFM_FAILURE;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return ECFM_SUCCESS;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_STATE_MACHINE_DATA_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return ECFM_FAILURE;
    }
    u2SyncMsgLen = ECFM_STATE_MACHINE_DATA_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_STATE_MACHINE_DATA_MSG);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->u2MepId);

    if (pPduSmInfo->pRMepInfo != NULL)
    {
        u2RMepId = pPduSmInfo->pRMepInfo->u2RMepId;
    }
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2RMepId);

    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.u4CcmSeqErrors);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.u2ErrorRMepId);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.u2XconnRMepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.b1UnExpectedMepDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.b1UnExpectedPeriodDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.b1MisMergeDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.b1UnExpectedLevelDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.b1ErrorCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.b1XconCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.u1MepXconState);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.u1RMepErrState);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->b1MaDefectIndication);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->b1AllRMepsDead);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.b1SomeRMepCcmDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.b1SomeMacStatusDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.b1SomeRdiDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.u1HighestDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.u1HighestDefectPri);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->b1PresentRdi);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.u1FngPriority);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.u1FngDefect);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->FngInfo.u1FngState);

    if (pPduSmInfo->pRMepInfo != NULL)
    {
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                            pPduSmInfo->pRMepInfo->b1RMepPortStatusDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                            pPduSmInfo->pRMepInfo->b1RMepInterfaceStatusDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                            pPduSmInfo->pRMepInfo->b1RMepCcmDefect);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pPduSmInfo->pRMepInfo->u1State);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                            pPduSmInfo->pRMepInfo->u1LastPortStatus);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset,
                            pPduSmInfo->pRMepInfo->u1LastInterfaceStatus);
    }
    else
    {
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
        ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_INIT_VAL);
    }

    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4NoDefectCount);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4RdiDefectCount);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4MacStatusDefectCount);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4RemoteCcmDefectCount);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4ErrorCcmDefectCount);

    EcfmRedSendMsgToRm (pMsg, u2BufSize);
    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessSyncSmData                             */
/*                                                                           */
/* Description        : This function updates STATE MAchine information      */
/*                      in  MEP and Remote MEP                               */
/*                                                                           */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessSynchSmData (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2RMepId = ECFM_INIT_VAL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSynchSmData: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }
    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSynchSmData: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSynchSmData: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSynchErrWhile: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2RMepId);

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u4CcmSeqErrors);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u2ErrorRMepId);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u2XconnRMepId);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset,
                        pMepInfo->CcInfo.b1UnExpectedMepDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset,
                        pMepInfo->CcInfo.b1UnExpectedPeriodDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1MisMergeDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset,
                        pMepInfo->CcInfo.b1UnExpectedLevelDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1ErrorCcmDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.b1XconCcmDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u1MepXconState);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u1RMepErrState);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->b1MaDefectIndication);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->b1AllRMepsDead);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.b1SomeRMepCcmDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset,
                        pMepInfo->FngInfo.b1SomeMacStatusDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.b1SomeRdiDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.u1HighestDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.u1HighestDefectPri);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->b1PresentRdi);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.u1FngPriority);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.u1FngDefect);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepInfo->FngInfo.u1FngState);

    if (u2RMepId != 0)
    {
        /* Get MEP from Global RBTree with received MdIndex, MaIndex, u2MepId */
        pRMepInfo =
            EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u2MepId, u2RMepId);
        if (pRMepInfo == NULL)

        {
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "ECFM RED: Remote MEP Not Found \r\n");
            return ECFM_FAILURE;
        }

        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pRMepInfo->b1RMepPortStatusDefect);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset,
                            pRMepInfo->b1RMepInterfaceStatusDefect);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pRMepInfo->b1RMepCcmDefect);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pRMepInfo->u1State);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pRMepInfo->u1LastPortStatus);
        ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pRMepInfo->u1LastInterfaceStatus);
    }
    else
    {
        u2Offset = u2Offset + 6;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4NoDefectCount);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4RdiDefectCount);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4MacStatusDefectCount);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4RemoteCcmDefectCount);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        ECFM_CC_CURR_CONTEXT_INFO ()->u4ErrorCcmDefectCount);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncTmr                                       */
/*                                                                           */
/* Description        : This function Sends timer start/stop/expiry to       */
/*                      the standby Node.                                    */
/* Input(s)           : u4TmrEvnt - Event occured for the timer              */
/*                      u1TimerType - Timer Type                             */
/*                      pPduSmInfo - Context/Mep/RMEP/Mip information        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncTmr (UINT1 u1TmrEvnt, UINT1 u1TimerType,
                tEcfmCcPduSmInfo * pPduSmInfo, UINT4 u4Interval)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2BufSize;
    ProtoEvt.u4AppId = RM_ECFM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node"
                      " needs to send the Timer event information. \r\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red sync up msgs can not besent if"
                      " the standby node is down.\n");

        /* Standby node is not present, so no need to send the
         * sync up message. */
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_TMR_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_RED_SYNC_TMR);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, ECFM_RED_SYNCH_TMR_SIZE);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, pPduSmInfo->pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, pPduSmInfo->pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, pPduSmInfo->pMepInfo->u2MepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1TmrEvnt);
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1TimerType);
    ECFM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Interval);

    if (u1TimerType == ECFM_CC_TMR_RMEP_WHILE)
    {
        ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, pPduSmInfo->pRMepInfo->u2RMepId);
    }

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedHandleTmrEvnt                                 */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
EcfmRedHandleTmrEvnt (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4Interval = ECFM_INIT_VAL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;
    UINT2               u2RMepId = ECFM_INIT_VAL;
    UINT1               u1TmrEvnt = ECFM_INIT_VAL;
    UINT1               u1TimerType = ECFM_INIT_VAL;

    ECFM_MEMSET (gpEcfmCcMepNode, ECFM_INIT_VAL, ECFM_CC_MEP_INFO_SIZE);

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessSynchSmData: Process Error while update "
                      "in case of standby Only .\n");
        return;
    }
    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedHandleTmrEvnt: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1TmrEvnt);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, u1TimerType);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4Interval);
    if (u1TimerType == ECFM_CC_TMR_RMEP_WHILE)
    {
        ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2RMepId);
    }

    gpEcfmCcMepNode->u4MdIndex = u4MdIndex;
    gpEcfmCcMepNode->u4MaIndex = u4MaIndex;
    gpEcfmCcMepNode->u2MepId = u2MepId;

    switch (u1TimerType)
    {
        case ECFM_CC_TMR_FNG_WHILE:
            /* Get MEP from Global RBTree with received MdIndex,
             * MaIndex, u2MepId 
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&
                                           (pMepInfo->FngInfo.
                                            FngWhileTimeStamp));
                        pMepInfo->FngInfo.FngWhileTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        pMepInfo->FngInfo.FngWhileTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;

        case ECFM_CC_TMR_FNG_RESET_WHILE:
            /* Get MEP from Global RBTree with received MdIndex, 
             * MaIndex, u2MepId
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&
                                           (pMepInfo->FngInfo.
                                            FngRstWhileTimeStamp));
                        pMepInfo->FngInfo.FngRstWhileTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        pMepInfo->FngInfo.FngRstWhileTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;
        case ECFM_CC_TMR_RDI_PERIOD:
            /* Get MEP from Global RBTree with received MdIndex, 
             * MaIndex, u2MepId
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&(pMepInfo->CcInfo.RdiTimeStamp));
                        pMepInfo->CcInfo.RdiTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                        pMepInfo->CcInfo.RdiTimeStamp = ECFM_INIT_VAL;
                        break;
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        EcfmCcRdiPeriodTimeout (pMepInfo);
                        pMepInfo->CcInfo.RdiTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;
        case ECFM_CC_TMR_ERR_CCM_WHILE:
            /* Get MEP from Global RBTree with received MdIndex,
             * MaIndex, u2MepId 
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&(pMepInfo->CcInfo.ErrTimeStamp));
                        pMepInfo->CcInfo.ErrTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        pMepInfo->CcInfo.ErrTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;
        case ECFM_CC_TMR_XCON_CCM_WHILE:
            /* Get MEP from Global RBTree with received MdIndex, 
             * MaIndex, u2MepId 
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&(pMepInfo->CcInfo.XConTimeStamp));
                        pMepInfo->CcInfo.XConTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        pMepInfo->CcInfo.XConTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;
        case ECFM_CC_TMR_LCK_PERIOD:
            /* Get MEP from Global RBTree with received MdIndex, 
             * MaIndex, u2MepId 
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&
                                           (pMepInfo->LckInfo.LckPrdTimeStamp));
                        pMepInfo->LckInfo.LckPrdTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        pMepInfo->LckInfo.LckPrdTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;
        case ECFM_CC_TMR_RMEP_WHILE:
            if (u2RMepId != ECFM_INIT_VAL)
            {
                /* Get MEP from Global RBTree with received MdIndex, MaIndex, u2MepId */
                pRMepInfo =
                    EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u2MepId,
                                            u2RMepId);
                if (pRMepInfo != NULL)

                {
                    switch (u1TmrEvnt)
                    {
                        case ECFM_RED_SYNC_START_TMR:
                            ECFM_GET_SYS_TIME (&(pRMepInfo->RMepTimeStamp));
                            pRMepInfo->RMepTimeStamp += u4Interval;
                            break;
                        case ECFM_RED_SYNC_STOP_TMR:
                        case ECFM_RED_SYNC_EXPIRY_TMR:
                            pRMepInfo->RMepTimeStamp = ECFM_INIT_VAL;
                            break;
                        default:
                            break;
                    }
                }
            }
            break;

        case ECFM_CC_TMR_AIS_PERIOD:
            /* Get MEP from Global RBTree with received MdIndex, 
             * MaIndex, u2MepId 
             */
            pMepInfo = RBTreeGet (ECFM_CC_MEP_TABLE,
                                  (tRBElem *) gpEcfmCcMepNode);
            if (pMepInfo != NULL)
            {
                switch (u1TmrEvnt)
                {
                    case ECFM_RED_SYNC_START_TMR:
                        ECFM_GET_SYS_TIME (&
                                           (pMepInfo->AisInfo.AisPrdTimeStamp));
                        pMepInfo->AisInfo.AisPrdTimeStamp += u4Interval;
                        break;
                    case ECFM_RED_SYNC_STOP_TMR:
                    case ECFM_RED_SYNC_EXPIRY_TMR:
                        pMepInfo->AisInfo.AisPrdTimeStamp = ECFM_INIT_VAL;
                        break;
                    default:
                        break;
                }
            }
            break;
        default:
            ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                         "EcfmCcTmrStartTimer:Invalid timer type \n");
            break;
    }
}

/*****************************************************************************/
/* Function Name      : EcfmSynchRedCcSeqCounter                             */
/*                                                                           */
/* Description        : This function synchs object maintained for MEP to    */
/*                      transmit sequence number in CCM PDU                  */
/* Input(s)           : pPduSmInfo - Context/Mep/RMEP/Mip information        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncCcSeqCounter (tEcfmCcPduSmInfo * pPduSmInfo)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the synch CC Seq number Message\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_TX_SEQ_NO_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_RED_SYNCH_TX_SEQ_NO_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNCH_TX_SEQ_NO_MSG);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pPduSmInfo->pMepInfo->u2MepId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset,
                        pPduSmInfo->pMepInfo->CcInfo.u4CciSentCcms);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessCcTxSeqNoMsg                           */
/*                                                                           */
/* Description        : This function updates CC Transmit Sequence number    */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessCcTxSeqNoMsg (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{

    tEcfmCcMepInfo     *pMepInfo = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessCcTxSeqNoMsg: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessCcTxSeqNoMsg: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessCcTxSeqNoMsg: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessRMepCntrValue: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pMepInfo->CcInfo.u4CciSentCcms);
    ECFM_GET_SYS_TIME (&pMepInfo->CcInfo.CciTimeStamp);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSendRMEPUpdates                               */
/*                                                                           */
/* Description        : This function sends learned Remote MEP Status to     */
/*                      STANDBY in bulk update, so that if any on-demand     */
/*                      operation should be started then remote MEP          */
/*                      information is present to start the transaction      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedSendRMEPUpdates ()
{
    tEcfmCcRMepDbInfo  *pRMepNode = NULL;
    tEcfmMacAddr        TempMacAddr;
    tRmMsg             *pMsg = NULL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT2               u2BufSize = ECFM_INIT_VAL;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen = ECFM_INIT_VAL;
    UINT2               u2SyncOffset = ECFM_INIT_VAL;
    u2BufSize = ECFM_RED_MAX_MSG_SIZE;
    ECFM_MEMSET (TempMacAddr, ECFM_INIT_VAL, ECFM_MAC_ADDR_LENGTH);
    /* Allocate memory to the pMsg for Maximum PDU Size */
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);
    if (pMsg == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "RM alloc  failed. Bulk updates not sent\n");
        return;
    }
    u2Offset = ECFM_INIT_VAL;

    /* Fill TLV Type in the message and Increment the offset */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

    /* Fill the number of size of  information to be synced up. 
     * Here only zero will be filled. The following macro is called
     * to move the pointer by 2 bytes.*/
    u2SyncOffset = u2Offset;
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    for (u4ContextId = ECFM_DEFAULT_CONTEXT;
         u4ContextId < ECFM_MAX_CONTEXTS; u4ContextId++)
    {
        if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

        {
            continue;
        }
        if (ECFM_IS_MODULE_DISABLED (u4ContextId))

        {
            ECFM_CC_RELEASE_CONTEXT ();
            continue;
        }

        /* Updated RMEP DB Information */
        pRMepNode = RBTreeGetFirst (ECFM_CC_RMEP_TABLE);

        /* Get MepInfo and Fill in the RM Buffer to transmit Sync Up message */
        while (pRMepNode != NULL)

        {

            /* Calculate the length of the MepInfo to be transmitted as it is of
             * variable length */

            /* There is no enough space to fill this port information into 
             * buffer.Hence send the existing message and construct new message 
             to fill 
             * the information of other MEPs.*/
            if (u2BufSize - u2Offset < ECFM_RED_RMEP_INFO_SIZE)

            {

                /* Offset of the Length field for update information is stored in
                 * u4SyncLengthOffset.u2SyncMsgLen contains the size of update
                 * information so far written.Hence update length field at offset
                 * u4SyncLengthOffset with the value u2SyncMsgLen.*/

                /* u2Offset contains the number of bytes written in the buffer, 
                 * so can be used as no of bytes to transmit */
                ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
                EcfmRedSendMsgToRm (pMsg, u2Offset);
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                              "RM: Alloc One More Buffer for MEP \n");
                pMsg = RM_ALLOC_TX_BUF (u2BufSize);
                if (pMsg == NULL)

                {
                    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                                  "RM alloc  failed. Bulk updates not sent\n");
                    return;
                }
                u2Offset = ECFM_INIT_VAL;
                u2SyncMsgLen = ECFM_INIT_VAL;

                /* Fill TLV Type in the message and Increment the offset */
                ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

                /* Fill the number of size of  information to be synced up. 
                 * Here only zero will be filled. The following macro is called
                 * to move the pointer by 2 bytes.*/
                u2SyncOffset = u2Offset;
                ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            }

            /* Fill the Value in the message and increment the offset */
            EcfmRedFillRMepInfo (u4ContextId, pRMepNode, pMsg, &u2Offset);

            /* Update Sync Msg Length */
            u2SyncMsgLen = u2SyncMsgLen + ECFM_RED_RMEP_INFO_SIZE;

            /* Get Next MEP from the Global RBTree maintained */
            pRMepNode = RBTreeGetNext (ECFM_CC_RMEP_TABLE, pRMepNode, NULL);
        }
        if ((u2Offset - 2) != u2SyncOffset)

        {

            /* u2Offset contains the number of bytes written in the buffer, 
             * so can be used as no of bytes to transmit */
            ECFM_RM_PUT_2_BYTE (pMsg, &u2SyncOffset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);

            /* Allocate memory to the pMsg for Maximum PDU Size */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "RM: Alloc One More Buffer \n");
            pMsg = RM_ALLOC_TX_BUF (u2BufSize);
            if (pMsg == NULL)

            {
                ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                              "RM alloc  failed. Bulk updates not sent\n");
                return;
            }
            u2Offset = ECFM_INIT_VAL;
            u2SyncMsgLen = ECFM_INIT_VAL;

            /* Fill TLV Type in the message and Increment the offset */
            ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_UPD_MSG);

            /* Fill the number of size of  information to be synced up. 
             * Here only zero will be filled. The following macro is called
             * to move the pointer by 2 bytes.*/
            u2SyncOffset = u2Offset;
            ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
            EcfmRedSendMsgToRm (pMsg, u2Offset);
        }
        else
        {
            /* Empty buffer created without any update messages filled */
            ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                          "Empty buffer created without any update"
                          "messages filled \r\n");
            ECFM_CC_RELEASE_CONTEXT ();
            RM_FREE (pMsg);
            pMsg = NULL;
            return;
        }
        ECFM_CC_RELEASE_CONTEXT ();
        return;
    }
    RM_FREE (pMsg);
    pMsg = NULL;
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchHwAuditCmdEvtInfo                        */
/*                                                                           */
/* Description        : This function synchs Command/Event information under */
/*                      execution to execute HW Audit after switchoverobject */
/*                      maintained for MEP to                                */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4CmdEvnt - Command/Event to synch to STANDBY        */
/*                      u4IfIndex - Actual Interface Index                   */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      VlanId    - VLAn Idenetifier                         */
/*                      bError    - Error Condition to be set at STANDBY     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchHwAuditCmdEvtInfo (UINT4 u4ContextId,
                               UINT4 u4CmdEvnt,
                               UINT4 u4IfIndex,
                               UINT4 u4MdId,
                               UINT4 u4MaId,
                               UINT4 u4MepId, tVlanId VlanId, BOOL1 bError)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send the HW Audit synch Message\n");

        /*Called at STANDBY Node clear the Audit Flag */
        gEcfmRedGlobalInfo.HwAuditInfo.bError = ECFM_FALSE;

        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_HW_ADT_INFO_MSG_SIZE;
    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_RED_SYNCH_HW_ADT_INFO_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNCH_HW_ADT_INFO);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4CmdEvnt);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4IfIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, VlanId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, bError);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessHwAuditInfo                            */
/*                                                                           */
/* Description        : This function updates Hardware Audit Info            */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessHwAuditInfo (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwAuditInfo: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwAuditInfo: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        gEcfmRedGlobalInfo.HwAuditInfo.u4CmdEvnt);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        gEcfmRedGlobalInfo.HwAuditInfo.u4IfIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, gEcfmRedGlobalInfo.HwAuditInfo.u4MdId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, gEcfmRedGlobalInfo.HwAuditInfo.u4MaId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset,
                        gEcfmRedGlobalInfo.HwAuditInfo.u4MepId);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, gEcfmRedGlobalInfo.HwAuditInfo.VlanId);
    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, gEcfmRedGlobalInfo.HwAuditInfo.bError);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedHwAudit                                       */
/*                                                                           */
/* Description        : This function will be invoked when STANDBY node      */
/*                      becomes active. This function will spawn a new task  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
EcfmRedHwAudit (VOID)
{
    INT4                i4RetVal;
    UINT4               u4Context;
    UINT1               u1IsEcfmEnable = ECFM_FALSE;

    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "Sending event to AUDIT " "task to start the audit. \n");

    /*
     * Check in all context, if Ecfm is shutdown remove configuration
     * from Hw. If any context has Ecfm up Spawn VlanAudit task
     * to do Audit in Hw.
     * */
    for (u4Context = 0; u4Context < ECFM_MAX_CONTEXTS; u4Context++)
    {
        if (ECFM_CC_SELECT_CONTEXT (u4Context) != ECFM_SUCCESS)
        {
            continue;
        }
        if (ECFM_IS_SYSTEM_STARTED (u4Context) == ECFM_FALSE)
        {
            /*
             * ECFM Is disabled.
             * No information in software to compare with hardware. Hence DeINIT
             * hardware line cards completely.
             */
            /* Hardware should be disabled only when the Node is Active */
#ifdef NPAPI_WANTED
            if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)

            {
                if ((EcfmFsMiEcfmHwDeInit (ECFM_CC_CURR_CONTEXT_ID ())) !=
                    FNP_SUCCESS)

                {
                    ECFM_CC_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                                 "EcfmCcModuleShutDown: HW deinit Failed \r\n");
                }
            }
#endif
        }
        else
        {
            u1IsEcfmEnable = ECFM_TRUE;
        }

        ECFM_CC_RELEASE_CONTEXT ();
        continue;
    }

    if (u1IsEcfmEnable == ECFM_TRUE)
    {
        /* ECFM Is enabled. Start performing the audit */
        ECFM_RED_AUDIT_FLAG () = ECFM_RED_AUDIT_STARTED;    /* audit started here */

        if (ECFM_AUDIT_TASK_ID == 0)
        {
            /* Doing audit for the first time */
            i4RetVal = ECFM_SPAWN_TASK (ECFM_AUDIT_TASK,
                                        ECFM_AUDIT_TASK_PRIORITY,
                                        OSIX_DEFAULT_STACK_SIZE,
                                        (OsixTskEntry) EcfmRedAuditMain,
                                        0, &ECFM_AUDIT_TASK_ID);
        }

        if (ECFM_AUDIT_TASK_ID != 0)
        {
            ECFM_SEND_EVENT (ECFM_AUDIT_TASK_ID, ECFM_RED_AUDIT_START_EVENT);
        }
    }

    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "Current number of standby node count - %d \n",
                  ECFM_NUM_STANDBY_NODES ());
    UNUSED_PARAM (i4RetVal);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : EcfmRedAuditMain                                        */
/*                                                                            */
/*  Description     : This the main routine for the ECFM Audit submodule.     */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
EcfmRedAuditMain (INT1 *pi1Param)
{
    UINT4               u4Events;

    UNUSED_PARAM (pi1Param);
    OsixTskIdSelf (&ECFM_AUDIT_TASK_ID);
    while (1)
    {
        if ((ECFM_RECEIVE_EVENT
             (ECFM_AUDIT_TASK_ID,
              (ECFM_RED_AUDIT_START_EVENT), OSIX_WAIT,
              &u4Events)) == OSIX_SUCCESS)
        {

            if (u4Events & ECFM_RED_AUDIT_START_EVENT)
            {
#ifdef NPAPI_WANTED
                ECFM_CC_LOCK ();
                EcfmRedStartAudit ();
                ECFM_CC_UNLOCK ();
#endif
            }
        }
    }
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : EcfmRedStartAudit                                       */
/*                                                                            */
/*  Description     : This function performs audit of the ECFM parameters     */
/*                    between the hardware and the software. This function    */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
EcfmRedStartAudit (VOID)
{
    tEcfmCfaIfInfo      CfaIfInfo;
    tEcfmCcMdInfo      *pMdNode = NULL;
    tEcfmCcMaInfo      *pMaNode = NULL;
    tEcfmCcMepInfo     *pMepNode = NULL;
    tEcfmLbLtMepInfo   *pMepInfo = NULL;
    UINT4               u4Context;
    UINT2               u2LocalPortId;

    if (ECFM_RED_AUDIT_FLAG () != ECFM_RED_AUDIT_STARTED)
    {
        return;
    }
    if (gEcfmRedGlobalInfo.HwAuditInfo.bError == ECFM_TRUE)
    {
        if (ECFM_CC_SELECT_CONTEXT (gEcfmRedGlobalInfo.HwAuditInfo.u4ContextId)
            != ECFM_SUCCESS)
        {
            return;
        }

        ECFM_GET_CONTEXT_INFO_FROM_IFINDEX (gEcfmRedGlobalInfo.HwAuditInfo.
                                            u4IfIndex, &u4Context,
                                            &u2LocalPortId);

        switch (gEcfmRedGlobalInfo.HwAuditInfo.u4CmdEvnt)
        {
            case ECFM_DELETE_PORT_MSG:
            case ECFM_UNMAP_PORT_MSG:
                EcfmCcIfHandleDeletePort (u2LocalPortId);
                break;
            case ECFM_OPER_STATUS_CHG_MSG:

                if ((EcfmCfaGetIfInfo
                     (gEcfmRedGlobalInfo.HwAuditInfo.u4IfIndex,
                      &CfaIfInfo)) != CFA_SUCCESS)
                {
                    ECFM_LBLT_TRC (ECFM_INIT_SHUT_TRC | ECFM_CONTROL_PLANE_TRC,
                                   "ECFM Hardware Audit: Getting Interface Info from "
                                   "CFA FAILED \r\n");
                    return;
                }
                EcfmCcIfHandlePortOperChg (u2LocalPortId,
                                           CfaIfInfo.u1IfOperStatus);
                break;
                break;
            case ECFM_DELETE_VLAN:
                EcfmHandleDeleteVlan (gEcfmRedGlobalInfo.HwAuditInfo.
                                      u4ContextId,
                                      gEcfmRedGlobalInfo.HwAuditInfo.VlanId);
                break;
            case ECFM_RED_MD_ROW_STS_CMD:

                pMdNode =
                    EcfmSnmpLwGetMdEntry (gEcfmRedGlobalInfo.HwAuditInfo.
                                          u4MdId);
                if (pMdNode != NULL)
                {
                    /* Get all MAs associated with pMdNode */
                    pMaNode =
                        (tEcfmCcMaInfo *) RBTreeGetFirst (pMdNode->MaTable);
                    while (pMaNode != NULL)

                    {
                        /* Get all MEPs associated with pMaNode */
                        pMepNode =
                            (tEcfmCcMepInfo *)
                            TMO_DLL_First (&(pMaNode->MepTable));
                        while (pMepNode != NULL)
                        {
                            if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))
                            {
                                if (pMepNode->b1MepCcmOffloadHwStatus ==
                                    ECFM_TRUE)
                                {
                                    /* First Disable MEP transmission from this MEP */
                                    /* Update Tx/Rx parameters for this MEP */
                                    EcfmCcmOffDeleteTxRxForMep (pMepNode);

                                    if (EcfmCcmOffCreateTxRxForMep (pMepNode) ==
                                        ECFM_FAILURE)
                                    {
                                        return;
                                    }
                                }
                                else
                                {
                                    EcfmCcmOffDeleteTxRxForMep (pMepNode);
                                }
                                /* Enable Tx/Rx for this MEP */
                            }
                            else
                            {
                                EcfmCcmOffDeleteTxRxForMep (pMepNode);
                            }
                            pMepNode =
                                (tEcfmCcMepInfo *)
                                TMO_DLL_Next (&(pMaNode->MepTable),
                                              &(pMepNode->MepTableDllNode));
                        }
                        pMaNode =
                            (tEcfmCcMaInfo *) RBTreeGetNext (pMdNode->MaTable,
                                                             (tRBElem *)
                                                             pMaNode, NULL);
                    }
                }
                break;
            case ECFM_RED_MA_ROW_STS_CMD:
                /* Get all MAs associated with pMdNode */
                pMaNode =
                    EcfmSnmpLwGetMaEntry (gEcfmRedGlobalInfo.HwAuditInfo.u4MdId,
                                          gEcfmRedGlobalInfo.HwAuditInfo.
                                          u4MaId);
                if (pMaNode != NULL)
                {
                    /* Get all MEPs associated with pMaNode */
                    pMepNode =
                        (tEcfmCcMepInfo *) TMO_DLL_First (&(pMaNode->MepTable));
                    while (pMepNode != NULL)
                    {
                        if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))
                        {
                            if (pMepNode->b1MepCcmOffloadHwStatus == ECFM_TRUE)
                            {
                                /* First Disable MEP transmission from this MEP */
                                /* Update Tx/Rx parameters for this MEP */
                                EcfmCcmOffDeleteTxRxForMep (pMepNode);

                                if (EcfmCcmOffCreateTxRxForMep (pMepNode) ==
                                    ECFM_FAILURE)
                                {
                                    return;
                                }
                            }
                            else
                            {
                                EcfmCcmOffDeleteTxRxForMep (pMepNode);
                            }
                            /* Enable Tx/Rx for this MEP */
                        }
                        else
                        {
                            EcfmCcmOffDeleteTxRxForMep (pMepNode);
                        }
                        pMepNode =
                            (tEcfmCcMepInfo *)
                            TMO_DLL_Next (&(pMaNode->MepTable),
                                          &(pMepNode->MepTableDllNode));
                    }
                }
                break;
            case ECFM_RED_MEP_ROW_STS_CMD:
                pMepNode = EcfmCcUtilGetMepEntryFrmGlob
                    (gEcfmRedGlobalInfo.HwAuditInfo.u4MdId,
                     gEcfmRedGlobalInfo.HwAuditInfo.u4MaId,
                     gEcfmRedGlobalInfo.HwAuditInfo.u4MepId);
                if (pMepNode != NULL)
                {
                    if ((pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE))
                    {
                        if (pMepNode->b1MepCcmOffloadHwStatus == ECFM_TRUE)
                        {
                            /* First Disable MEP transmission from this MEP */
                            /* Update Tx/Rx parameters for this MEP */
                            EcfmCcmOffDeleteTxRxForMep (pMepNode);

                            if (EcfmCcmOffCreateTxRxForMep (pMepNode) ==
                                ECFM_FAILURE)
                            {
                                return;
                            }
                        }
                        else
                        {
                            EcfmCcmOffDeleteTxRxForMep (pMepNode);
                        }
                        /* Enable Tx/Rx for this MEP */
                    }
                    else
                    {
                        EcfmCcmOffDeleteTxRxForMep (pMepNode);
                    }
                }
                break;
            case ECFM_RED_SHUTDOWN_CMD:
            case ECFM_RED_MOD_STS_CHNG_CMD:
            case ECFM_RED_GLOBAL_OFF_CMD:
            case ECFM_RED_Y1731_MOD_STS_CMD:
                pMepNode = RBTreeGetFirst (ECFM_CC_MEP_TABLE);

                while (pMepNode != NULL)

                {

                    if (pMepNode->b1MepCcmOffloadStatus == ECFM_TRUE)
                    {
                        if (ECFM_IS_MODULE_ENABLED
                            (gEcfmRedGlobalInfo.HwAuditInfo.u4ContextId))
                        {

                            ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                                         "SNMP: b1MepCcmOffloadStatus is True \r\n");

                            /*First Delete in case already existing in H/W */
                            EcfmCcmOffDeleteTxRxForMep (pMepNode);

                            /* Call the Create Tx/Rx call for Mep */
                            if (EcfmCcmOffCreateTxRxForMep (pMepNode) !=
                                ECFM_SUCCESS)

                            {
                                pMepNode->b1MepCcmOffloadHwStatus = ECFM_FALSE;
                            }
                            ECFM_CC_TRC_ARG2 (ECFM_MGMT_TRC, "SNMP:"
                                              "OFFLOAD: OFFLOADING Start For MEPID %d"
                                              "on Interface %d\r\n",
                                              pMepNode->u2MepId,
                                              pMepNode->pPortInfo->u4IfIndex);
                        }
                        else
                        {
                            EcfmCcmOffDeleteTxRxForMep (pMepNode);
                        }
                    }
                    else
                    {
                        EcfmCcmOffDeleteTxRxForMep (pMepNode);
                    }

                    pMepNode =
                        RBTreeGetNext (ECFM_CC_MEP_TABLE, pMepNode, NULL);
                }
                break;
            case ECFM_RED_PORT_LLC_CMD:
            case ECFM_RED_PORT_MOD_STS_CMD:
                EcfmCcmOffLoadUpdateLlc (gEcfmRedGlobalInfo.HwAuditInfo.
                                         u4IfIndex);
                break;
            case ECFM_RED_OUI_CHANGE_CMD:
                EcfmOffloadHandleOUIChange ();
                break;
            case ECFM_RED_LB_NPAPI:
                ECFM_LBLT_LOCK ();

                if (ECFM_LBLT_SELECT_CONTEXT
                    (gEcfmRedGlobalInfo.HwAuditInfo.u4ContextId) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_UNLOCK ();
                    break;
                }

                pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob
                    (gEcfmRedGlobalInfo.HwAuditInfo.u4MdId,
                     gEcfmRedGlobalInfo.HwAuditInfo.u4MaId,
                     gEcfmRedGlobalInfo.HwAuditInfo.u4MepId);
                if (pMepInfo != NULL)
                {
                    if (pMepInfo->LbInfo.u1TxLbmStatus ==
                        ECFM_TX_STATUS_NOT_READY)
                    {
                        tEcfmMepInfoParams  EcfmMepInfoParams;
                        tEcfmConfigLbmInfo  EcfmConfigLbmInfo;
                        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                                     sizeof (tEcfmMepInfoParams));
                        ECFM_MEMSET (&EcfmConfigLbmInfo, ECFM_INIT_VAL,
                                     sizeof (tEcfmConfigLbmInfo));
                        EcfmMepInfoParams.u4ContextId =
                            ECFM_LBLT_CURR_CONTEXT_ID ();
                        EcfmMepInfoParams.u4IfIndex =
                            (UINT4) pMepInfo->u2PortNum;
                        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
                        EcfmMepInfoParams.u4VlanIdIsid =
                            pMepInfo->u4PrimaryVidIsid;
                        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

                        /*First stop LB transaction if running in HW */
                        EcfmFsMiEcfmStopLbmTransaction (&EcfmMepInfoParams);

                        EcfmConfigLbmInfo.u1Status =
                            pMepInfo->LbInfo.u1TxLbmStatus;
                        EcfmConfigLbmInfo.u1TstTlvPatterType =
                            pMepInfo->LbInfo.u1TxLbmTstPatternType;
                        EcfmConfigLbmInfo.u2TstTlvPatterSize =
                            pMepInfo->LbInfo.u2TxLbmPatternSize;
                        EcfmConfigLbmInfo.u1TlvOrNone =
                            pMepInfo->LbInfo.u1TxLbmTlvOrNone;
                        EcfmConfigLbmInfo.b1VariableByte =
                            pMepInfo->LbInfo.b1TxLbmVariableBytes;
                        EcfmConfigLbmInfo.b1DropEnable =
                            pMepInfo->LbInfo.b1TxLbmDropEligible;
                        EcfmConfigLbmInfo.u2TxLbmMessages =
                            pMepInfo->LbInfo.u2TxLbmMessages;
                        EcfmConfigLbmInfo.u4LbmInterval =
                            pMepInfo->LbInfo.u4TxLbmInterval;
                        EcfmConfigLbmInfo.u4Deadline =
                            pMepInfo->LbInfo.u4TxLbmDeadline;
                        EcfmConfigLbmInfo.u1LbmMode =
                            pMepInfo->LbInfo.u1TxLbmMode;

                        ECFM_MEMCPY (EcfmConfigLbmInfo.DestMacAddr,
                                     pMepInfo->LbInfo.TxLbmDestMacAddr,
                                     ECFM_MAC_ADDR_LENGTH);
                        EcfmConfigLbmInfo.pu1DataTlvData =
                            pMepInfo->LbInfo.TxLbmDataTlv.pu1Octets;
                        EcfmConfigLbmInfo.u4DataTlvSize =
                            pMepInfo->LbInfo.TxLbmDataTlv.u4OctLen;

                        /*Now restart LB transaction */
                        if (EcfmFsMiEcfmStartLbmTransaction
                            (&EcfmMepInfoParams,
                             &EcfmConfigLbmInfo) != FNP_SUCCESS)
                        {
                            return;
                        }
                    }
                    else
                    {
                        tEcfmMepInfoParams  EcfmMepInfoParams;
                        tEcfmConfigLbmInfo  EcfmConfigLbmInfo;
                        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                                     sizeof (tEcfmMepInfoParams));
                        ECFM_MEMSET (&EcfmConfigLbmInfo, ECFM_INIT_VAL,
                                     sizeof (tEcfmConfigLbmInfo));
                        EcfmMepInfoParams.u4ContextId =
                            ECFM_LBLT_CURR_CONTEXT_ID ();
                        EcfmMepInfoParams.u4IfIndex = pMepInfo->u2PortNum;
                        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
                        EcfmMepInfoParams.u4VlanIdIsid =
                            pMepInfo->u4PrimaryVidIsid;
                        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

                        /*First stop LB transaction if running in HW */
                        EcfmFsMiEcfmStopLbmTransaction (&EcfmMepInfoParams);
                    }
                }

                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();

                break;
            case ECFM_RED_TST_NPAPI:
                ECFM_LBLT_LOCK ();

                if (ECFM_LBLT_SELECT_CONTEXT
                    (gEcfmRedGlobalInfo.HwAuditInfo.u4ContextId) !=
                    ECFM_SUCCESS)
                {
                    ECFM_LBLT_UNLOCK ();
                    break;
                }

                pMepInfo = EcfmLbLtUtilGetMepEntryFrmGlob
                    (gEcfmRedGlobalInfo.HwAuditInfo.u4MdId,
                     gEcfmRedGlobalInfo.HwAuditInfo.u4MaId,
                     gEcfmRedGlobalInfo.HwAuditInfo.u4MepId);
                if (pMepInfo != NULL)
                {
                    if (pMepInfo->TstInfo.u1TstStatus ==
                        ECFM_TX_STATUS_NOT_READY)
                    {
                        tEcfmMepInfoParams  EcfmMepInfoParams;
                        tEcfmConfigTstInfo  EcfmConfigTstInfo;
                        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                                     sizeof (tEcfmMepInfoParams));
                        ECFM_MEMSET (&EcfmConfigTstInfo, ECFM_INIT_VAL,
                                     sizeof (tEcfmConfigTstInfo));
                        EcfmMepInfoParams.u4ContextId =
                            ECFM_LBLT_CURR_CONTEXT_ID ();
                        EcfmMepInfoParams.u4IfIndex = pMepInfo->u2PortNum;
                        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
                        EcfmMepInfoParams.u4VlanIdIsid =
                            pMepInfo->u4PrimaryVidIsid;
                        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

                        /*First delete the TST trasaction if already running */
                        EcfmFsMiEcfmStopTstTransaction (&EcfmMepInfoParams);

                        EcfmConfigTstInfo.u1Status =
                            pMepInfo->TstInfo.u1TstStatus;
                        EcfmConfigTstInfo.u1TstTlvPatterType =
                            pMepInfo->TstInfo.u1TstPatternType;
                        EcfmConfigTstInfo.b1VariableByte =
                            pMepInfo->TstInfo.b1TstVariableBytes;
                        EcfmConfigTstInfo.b1DropEnable =
                            pMepInfo->TstInfo.b1TstDropEligible;
                        EcfmConfigTstInfo.u4TxTstMessages =
                            pMepInfo->TstInfo.u4TstMessages;
                        EcfmConfigTstInfo.u4TstInterval =
                            pMepInfo->TstInfo.u4TstInterval;
                        EcfmConfigTstInfo.u2TstTlvPatterSize =
                            pMepInfo->TstInfo.u2TstPatternSize;
                        EcfmConfigTstInfo.u4Deadline =
                            pMepInfo->TstInfo.u4TstDeadLine;
                        ECFM_MEMCPY (EcfmConfigTstInfo.DestMacAddr,
                                     pMepInfo->TstInfo.TstDestMacAddr,
                                     ECFM_MAC_ADDR_LENGTH);

                        /*Restart the transaction */
                        if (EcfmFsMiEcfmStartTstTransaction
                            (&EcfmMepInfoParams,
                             &EcfmConfigTstInfo) != FNP_SUCCESS)
                        {
                            return;
                        }
                    }
                    else
                    {
                        tEcfmMepInfoParams  EcfmMepInfoParams;
                        ECFM_MEMSET (&EcfmMepInfoParams, ECFM_INIT_VAL,
                                     sizeof (tEcfmMepInfoParams));
                        EcfmMepInfoParams.u4ContextId =
                            ECFM_LBLT_CURR_CONTEXT_ID ();
                        EcfmMepInfoParams.u4IfIndex = pMepInfo->u2PortNum;
                        EcfmMepInfoParams.u1MdLevel = pMepInfo->u1MdLevel;
                        EcfmMepInfoParams.u4VlanIdIsid =
                            pMepInfo->u4PrimaryVidIsid;
                        EcfmMepInfoParams.u1Direction = pMepInfo->u1Direction;

                        EcfmFsMiEcfmStopTstTransaction (&EcfmMepInfoParams);
                    }
                }

                ECFM_LBLT_RELEASE_CONTEXT ();
                ECFM_LBLT_UNLOCK ();

                break;
            default:
                ECFM_CC_TRC (ECFM_MGMT_TRC | ECFM_ALL_FAILURE_TRC,
                             "SNMP: Invalid synch Event/Command \r\n");
                break;
        }
    }
    return;
}
#endif
/*****************************************************************************/
/* Function Name      : EcfmRedSynchOffTxFilterId                            */
/*                                                                           */
/* Description        : This function synchs Offload module generated        */
/*                      Transmit Filter  id to STANDBY node                  */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      u4TxFilterId   - CCM Transmission filter identifier  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchOffTxFilterId (UINT4 u4ContextId,
                           UINT4 u4MdId,
                           UINT4 u4MaId, UINT4 u4MepId, UINT4 u4TxFilterId)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send Filter sync Message\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_TX_FILTER_ID_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_RED_SYNCH_TX_FILTER_ID_MSG_SIZE;

    /* Fill the message type */

    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNCH_TX_FILTER_ID);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4TxFilterId);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessOffTxFilterId                          */
/*                                                                           */
/* Description        : This function updates Hardware Audit Info            */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessOffTxFilterId (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4MepId = ECFM_INIT_VAL;
    UINT4               u2OffloadMepTxHandle = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffTxFilterId: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffTxFilterId: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffTxFilterId: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffTxFilterId: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MepId);

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u4MepId);
    if (pMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u2OffloadMepTxHandle);
    pMepInfo->u2OffloadMepTxHandle = u2OffloadMepTxHandle;
    ECFM_RM_GET_N_BYTE (pMsg, pMepInfo->au1HwMepHandler, &u2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchOffRxFilterId                            */
/*                                                                           */
/* Description        : This function synchs Offload module generated        */
/*                      Recevie Filter  id to STANDBY node                   */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      u4RxFilterId   - CCM Reception filter identifier  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchOffRxFilterId (UINT4 u4ContextId,
                           UINT4 u4MdId,
                           UINT4 u4MaId,
                           UINT4 u4MepId, UINT4 u4RMepId, UINT4 u4RxFilterId)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to"
                      "send Filter sync Message\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs"
                      "cannot be sent if the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_RX_FILTER_ID_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM : RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_RED_SYNCH_RX_FILTER_ID_MSG_SIZE;

    /* Fill the message type */

    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNCH_RX_FILTER_ID);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4RMepId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4RxFilterId);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)

    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessOffRxFilterId                          */
/*                                                                           */
/* Description        : This function updates Hardware Audit Info            */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessOffRxFilterId (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4MepId = ECFM_INIT_VAL;
    UINT4               u4RMepId = ECFM_INIT_VAL;
    UINT4               u2OffloadRMepRxHandle = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffRxFilterId: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffRxFilterId: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffTxFilterId: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }
    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessOffTxFilterId: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4RMepId);

    pRMepInfo =
        EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u4MepId, u4RMepId);
    if (pRMepInfo == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: Remote MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u2OffloadRMepRxHandle);
    /*ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, pRMepInfo->u2OffloadRMepRxHandle); */
    pRMepInfo->u2OffloadRMepRxHandle = u2OffloadRMepRxHandle;
    ECFM_RM_GET_N_BYTE (pMsg, pRMepInfo->au1HwRMepHandler, &u2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedCheckAndSyncSmData                            */
/*                                                                           */
/* Description        : This function checks if the state of the state       */
/*                      machine is changed. If the state is changed then     */
/*                      State Mahine data will be synchupdates with standby  */
/*                      node                                                 */
/*                                                                           */
/* Input(s)           : pu1SmState    - Pointer to Current SM state          */
/*                      u1StateToSet  - State to be set for the state machine*/
/*                      pPduSmInfo - Context/Mep/RMEP/Mip information        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
VOID
EcfmRedCheckAndSyncSmData (UINT1 *pu1SmState, UINT1 u1StateToSet,
                           tEcfmCcPduSmInfo * pPduSmInfo)
{
    if (*pu1SmState != u1StateToSet)
    {
        *pu1SmState = u1StateToSet;
        EcfmRedSyncSmData (pPduSmInfo);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchHwTxHandler                              */
/*                                                                           */
/* Description        : This function synchs Hw Handler generated            */
/*                      id to STANDBY node                                   */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      pau1HwHandler - CCM Transmission Handler */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchHwTxHandler (UINT4 u4ContextId,
                         UINT4 u4MdId,
                         UINT4 u4MaId, UINT4 u4MepId, UINT1 *pau1HwMepHandler)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INVALID_CONTEXT | ECFM_CONTROL_PLANE_TRC,
                          "Func [%s]: Only the Active node needs to"
                          "send Filter sync Message\n", __FUNCTION__);
        /*Only the Active node needs to send the SyncUp message */
        return;
    }

    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INVALID_CONTEXT | ECFM_CONTROL_PLANE_TRC,
                          "Func [%s]: Red CC sync up msgs"
                          "cannot be sent if the standby node is down.\n",
                          __FUNCTION__);
        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }

    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_HW_TX_HANDLER_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INVALID_CONTEXT | ECFM_ALL_FAILURE_TRC,
                          "Func [%s]: RM Alloc Failure \n", __FUNCTION__);
        return;
    }

    u2SyncMsgLen = ECFM_RED_SYNCH_HW_TX_HANDLER_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNCH_HW_TX_HANDLER);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_PUT_N_BYTE (pMsg, pau1HwMepHandler, &u2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSynchHwRxHandler                              */
/*                                                                           */
/* Description        : This function synchs Offload module generated        */
/*                      Recevie Filter  id to STANDBY node                   */
/* Input(s)           : u4ContextId  - Context Identifier                    */
/*                      u4MdId    - Maintenance Domain Identifier            */
/*                      u4MaId    - Maintenance Association Identifier       */
/*                      u4MepId   - Maintenance end Point identifier         */
/*                      u4RxFilterId   - CCM Reception filter identifier  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSynchHwRxHandler (UINT4 u4ContextId,
                         UINT4 u4MdId,
                         UINT4 u4MaId,
                         UINT4 u4MepId, UINT4 u4RMepId,
                         UINT1 *pau1HwRMepHandler)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INVALID_CONTEXT | ECFM_CONTROL_PLANE_TRC,
                          "Func [%s]: Only the Active node needs to"
                          "send Filter sync Message\n", __FUNCTION__);
        /*Only the Active node needs to send the SyncUp message */
        return;
    }

    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INVALID_CONTEXT | ECFM_CONTROL_PLANE_TRC,
                          "Func [%s]: Red CC sync up msgs"
                          "cannot be sent if the standby node is down.\n",
                          __FUNCTION__);
        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }

    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_RED_SYNCH_HW_RX_HANDLER_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ECFM_CC_TRC_ARG1 (ECFM_INVALID_CONTEXT | ECFM_ALL_FAILURE_TRC,
                          "Func [%s]: RM Alloc Failure \n", __FUNCTION__);
        return;
    }
    u2SyncMsgLen = ECFM_RED_SYNCH_HW_RX_HANDLER_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNCH_HW_RX_HANDLER);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MdId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MaId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, u4RMepId);
    ECFM_RM_PUT_N_BYTE (pMsg, pau1HwRMepHandler, &u2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        EcfmRmHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessHwRxHandler                            */
/*                                                                           */
/* Description        : This function updates Hardware Audit Info            */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessHwRxHandler (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcRMepDbInfo  *pRMepInfo = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4MepId = ECFM_INIT_VAL;
    UINT4               u4RMepId = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwRxHandler: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwRxHandler: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwRxHandler: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwRxHandler: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MepId);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4RMepId);

    pRMepInfo =
        EcfmSnmpLwGetRMepEntry (u4MdIndex, u4MaIndex, u4MepId, u4RMepId);
    if (pRMepInfo == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM RED: Remote MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_N_BYTE (pMsg, pRMepInfo->au1HwRMepHandler, &u2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessHwTxHandler                            */
/*                                                                           */
/* Description        : This function updates Hardware Audit Info            */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ECFM_SUCCESS/ECFM_FAILURE                            */
/*****************************************************************************/
INT4
EcfmRedProcessHwTxHandler (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepInfo = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT4               u4MepId = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwTxHandler: Process Error while update "
                      "in case of standby Only .\n");
        return ECFM_FAILURE;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwTxHandler: CC Data corruption"
                      " =[%d][%d].\n", u2Length, i2MsgSize);
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwTxHandler: "
                      "Context-Id not valid in stand-by\r\n");
        return ECFM_FAILURE;
    }

    if (ECFM_IS_SYSTEM_SHUTDOWN (ECFM_CC_CURR_CONTEXT_ID ()))
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwTxHandler: "
                      "Cannot Handle Immediate synchup in stand-by\r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MepId);

    pMepInfo = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u4MepId);
    if (pMepInfo == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "EcfmRedProcessHwTxHandler: MEP Not Found \r\n");
        return ECFM_FAILURE;
    }

    ECFM_RM_GET_N_BYTE (pMsg, pMepInfo->au1HwMepHandler, &u2Offset,
                        ECFM_HW_MEP_HANDLER_SIZE);

    return ECFM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EcfmRedSyncAvlbltyInfo                               */
/*                                                                           */
/* Description        : This function  syncs the Availablity Info at CC      */
/*                      Task at standby node.                                */
/*                                                                           */
/* Input(s)           : pMepInfo - Ponter to the MEP Table                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PUBLIC VOID
EcfmRedSyncAvlbltyInfo (tEcfmCcMepInfo * pMepInfo)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = ECFM_INIT_VAL;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node needs to send the"
                      "AIS Period Timer expiry\n");

        /*Only the Active node needs to send the SyncUp message */
        return;
    }
    if (ECFM_IS_STANDBY_UP () == ECFM_FALSE)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Red CC sync up msgs cannot be sent if"
                      "the standby node is down.\n");

        /* Standby node is not present, so no need to send the sync
         * up message.*/
        return;
    }
    u2BufSize =
        ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE +
        ECFM_AVLBLTY_INFO_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: RM Alloc Failure \n");
        return;
    }
    u2SyncMsgLen = ECFM_AVLBLTY_INFO_MSG_SIZE;

    /* Fill the message type */
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, ECFM_RED_SYNC_AVLBLTY_INFO);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SyncMsgLen);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, ECFM_CC_CURR_CONTEXT_ID ());
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MdIndex);
    ECFM_RM_PUT_4_BYTE (pMsg, &u2Offset, pMepInfo->u4MaIndex);
    ECFM_RM_PUT_2_BYTE (pMsg, &u2Offset, pMepInfo->u2MepId);
    ECFM_RM_PUT_1_BYTE (pMsg, &u2Offset, pMepInfo->LmInfo.b1TxLmByAvlbility);
    if (pMepInfo->pAvlbltyInfo != NULL)
    {
        ECFM_RM_PUT_N_BYTE (pMsg, &pMepInfo->pAvlbltyInfo->f4FrameLossThreshold,
                            &u2Offset, ECFM_VAL_4);
        ECFM_RM_PUT_N_BYTE (pMsg,
                            &pMepInfo->pAvlbltyInfo->uAvlbltyOperInfo.
                            StaticInfo, &u2Offset,
                            sizeof (tEcfmCcAvlbltyStaticInfo));
        EcfmRedSendMsgToRm (pMsg, u2BufSize);
    }
    else
    {
        RM_FREE (pMsg);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : EcfmRedProcessAvlbltyInfo                            */
/*                                                                           */
/* Description        : This function process the received LCK Period Expiry */
/*                      Message                                              */
/*                                                                           */
/* Input(s)           : pMsg    - Pointer to the sync up message.            */
/*                      u4Offset - offset in the msg.                        */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
EcfmRedProcessAvlbltyInfo (tRmMsg * pMsg, UINT2 u2Offset, UINT2 u2Length)
{
    tEcfmCcMepInfo     *pMepNode = NULL;
    INT2                i2MsgSize = ECFM_INIT_VAL;
    UINT4               u4ContextId = ECFM_INIT_VAL;
    UINT4               u4MdIndex = ECFM_INIT_VAL;
    UINT4               u4MaIndex = ECFM_INIT_VAL;
    UINT2               u2MepId = ECFM_INIT_VAL;

    if (ECFM_NODE_STATUS () != ECFM_NODE_STANDBY)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Process AIS Period Timer Expiry in case"
                      "of standby Only .\n");
        return;
    }

    /* Get the Message size */
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, i2MsgSize);
    u2Length = u2Length - ECFM_RED_LEN_FIELD_SIZE;
    if (u2Length != i2MsgSize)

    {

        /* Data corruption, hence ignore the whole sync up message. */
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: CC Data corruption =[%d][%d].\n", u2Length,
                      i2MsgSize);
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4ContextId);
    if (ECFM_CC_SELECT_CONTEXT (u4ContextId) != ECFM_SUCCESS)

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Context-Id not valid in stand-by\r\n");
        return;
    }
    if (ECFM_IS_MODULE_DISABLED (ECFM_CC_CURR_CONTEXT_ID ()))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Cannot Handle AIS Timer Expiry in stand-by\r\n");
        return;
    }
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MdIndex);
    ECFM_RM_GET_4_BYTE (pMsg, &u2Offset, u4MaIndex);
    ECFM_RM_GET_2_BYTE (pMsg, &u2Offset, u2MepId);

    /* Get MEP from Global RBTree */
    pMepNode = EcfmCcUtilGetMepEntryFrmGlob (u4MdIndex, u4MaIndex, u2MepId);
    if ((pMepNode == NULL) || (pMepNode->pAvlbltyInfo == NULL))

    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM : MEP or Availability Table Not Found \r\n");
        return;
    }

    ECFM_RM_GET_1_BYTE (pMsg, &u2Offset, pMepNode->LmInfo.b1TxLmByAvlbility);
    ECFM_RM_GET_N_BYTE (pMsg, &pMepNode->pAvlbltyInfo->f4FrameLossThreshold,
                        &u2Offset, ECFM_VAL_4);
    ECFM_RM_GET_N_BYTE (pMsg,
                        &pMepNode->pAvlbltyInfo->uAvlbltyOperInfo.StaticInfo,
                        &u2Offset, sizeof (tEcfmCcAvlbltyStaticInfo));

    ECFM_CC_RELEASE_CONTEXT ();
    return;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : EcfmRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      ECFM to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
EcfmRedHRProcStdyStPktReq (VOID)
{
    EcfmRedHRSendStdyStTailMsg ();
}

/*****************************************************************************/
/* Function Name      : EcfmRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
EcfmRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ECFM_INIT_VAL;
    UINT2               u2BufSize = 0;

    if (ECFM_NODE_STATUS () != ECFM_NODE_ACTIVE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Only the Active node"
                      " needs to send the Steady state Tail Msg. \r\n");

        /*Only the Active node needs to send the SyncUp message */

        return ECFM_SUCCESS;
    }
    if (ECFM_HR_STATUS () == ECFM_HR_STATUS_DISABLE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                      "ECFM: Hitless restart is not enabled. Steady state "
                      "tail msg is not sent to RM.\n");
        return ECFM_SUCCESS;
    }

    ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_CONTROL_PLANE_TRC,
                  "ECFM: sending steady state tail msg to RM.\n");

    u2BufSize = ECFM_RED_TYPE_FIELD_SIZE + ECFM_RED_LEN_FIELD_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     *      * by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "Rm alloc failed\n");
        return (ECFM_FAILURE);
    }

    /* Form a steady state tail message.
     *
     *                    <--------1 Byte--------><---2 Byte--->
     *           _________________________________________________
     *          |        |                          |             |
     *          | RM Hdr | ECFM_HR_STDY_ST_PKT_TAIL | Msg Length  |
     *          |________|__________________________|_____________|
     *
     *       The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    ECFM_RM_PUT_1_BYTE (pMsg, &u4Offset, ECFM_HR_STDY_ST_PKT_TAIL);
    ECFM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (EcfmRedSendMsgToRm (pMsg, u2BufSize) == ECFM_FAILURE)
    {
        ECFM_GLB_TRC (ECFM_INVALID_CONTEXT, ECFM_ALL_FAILURE_TRC,
                      "ECFM: steady state tail msg sending is " "failed\n");
    }
    return ECFM_SUCCESS;
}

/* HITLESS RESTART */
/******************************************************************************
 * Function           : EcfmRedGetHRFlag
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Hitless restart flag value.
 * Action             : This API returns the hitless restart flag value.
 ******************************************************************************/
UINT1
EcfmRedGetHRFlag (VOID)
{
    UINT1               u1HRFlag = 0;
    u1HRFlag = RmGetHRFlag ();
    return (u1HRFlag);
}

/*****************************************************************************
 *                          End of cfmccred.c                                  *
 *****************************************************************************/
