/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmdmini.c,v 1.20 2015/09/10 12:07:29 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way 
 *              and 2 way Delay Mesaurement of Control Sub Module.
 *******************************************************************/

#include "cfminc.h"
PRIVATE INT4 EcfmDmInitXmitDmmPdu PROTO ((tEcfmLbLtMepInfo *));
PRIVATE VOID EcfmDmInitFormatDmmPduHdr PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID        EcfmDmInitPutDmmInfo
PROTO ((tEcfmLbLtMepInfo *, UINT1 **, tEcfmTimeRepresentation *));
PRIVATE INT4 EcfmDmInitXmit1DmPdu PROTO ((tEcfmLbLtMepInfo *));
PRIVATE VOID EcfmDmInitFormat1DmPduHdr PROTO ((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmDmInitPut1DmInfo PROTO ((UINT1 **));

/*******************************************************************************
 * Function           : EcfmLbLtClntDmInitiator
 *
 * Description        : The routine implements the DM Initiator, it calls up
 *                      routine to format and transmit 1DM/DMM frame.
 *                      the various events.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that 
 *                      stores the information regarding MEP info.
 *                      u1EventId - Event Id
 *
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PUBLIC UINT4
EcfmLbLtClntDmInitiator (tEcfmLbLtMepInfo * pMepInfo, UINT1 u1EventID)
{
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    UINT4               u4RetVal = ECFM_SUCCESS;
    UINT1               u1SelectorType = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pDmInfo = &pMepInfo->DmInfo;

    /* Check for the event received */
    switch (u1EventID)

    {
        case ECFM_EV_MEP_BEGIN:

            /*Set DM Initiator related values to their defaults */
            pDmInfo->u1DmStatus = ECFM_TX_STATUS_READY;
            break;
        case ECFM_EV_MEP_NOT_ACTIVE:

            EcfmDmInitStopDmTransaction (pMepInfo);
            /* If the 1 Dm Transaction Interval Timer is runnig, stop it */
            EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_1DM_TRANS_INTERVAL, pMepInfo);
            break;
        case ECFM_LBLT_DM_DEADLINE_EXPIRY:

            /*
             * Wait for one more extra unit for timing out the last 
             * response
             * There is a very much possibility that the deadline timer expired
             * before timing out the response of the last request, in this case
             * the response will be received after the expiry of the
             * state-machine and thus the response will be treated as invalid
             * and which is not correct.
             */
            if (ECFM_GET_EXTRA_DEADLINE_STATUS (pDmInfo->u4TxDmDeadline) ==
                ECFM_FALSE)

            {
                UINT4               u4DmInterval = ECFM_INIT_VAL;
                u4DmInterval =
                    ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (pDmInfo->
                                                          u2TxDmInterval);
                ECFM_SET_EXTRA_DEADLINE_STATUS (pDmInfo->u4TxDmDeadline);

                /* from now on we have to mask the Interval timers expiry 
                 * events
                 */
                if (EcfmLbLtTmrStartTimer
                    (ECFM_LBLT_TMR_DM_DEADLINE, pMepInfo,
                     u4DmInterval) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntDmInitiator: "
                                   "Extra time DeadLine Timer has not started\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
                break;
            }

            /* Sync DM Deadline timer to STANDBY node */
            EcfmRedSyncDmDeadlineTimerExp (pMepInfo);
        case ECFM_DM_STOP_TRANSACTION:

            /* Stop the DM Transaction */
            EcfmDmInitStopDmTransaction (pMepInfo);

            /* Sync DM Cache at STAND BY node */
            EcfmRedSyncDmCache ();
            break;
        case ECFM_DM_START_TRANSACTION:

            /* For the case of 1DM transaction, A new transaction
             * cannot start if the 1DM transaction interval timer is running
             */
            if (pDmInfo->u1TxDmType == ECFM_LBLT_DM_TYPE_1DM)

            {
                if (pMepInfo->DmInfo.u1DmStatus == ECFM_TX_STATUS_NOT_READY)

                {

                    /* Timer is running */
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntDmInitiator: "
                                   " 1DM transaction interval timer running."
                                   " Cant start a new 1DM transaction\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            pDmInfo->u1DmStatus = ECFM_TX_STATUS_NOT_READY;

            /*Increment the TransactionID */
            ECFM_LBLT_INCR_DM_TRANS_ID (pMepInfo);

            /* Start the DMDeadline Time if */
            if (pDmInfo->u4TxDmDeadline != ECFM_INIT_VAL)

            {

                /*Start the DM Deadline Timer */
                if (EcfmLbLtTmrStartTimer
                    (ECFM_LBLT_TMR_DM_DEADLINE, pMepInfo,
                     (pDmInfo->u4TxDmDeadline *
                      ECFM_NUM_OF_MSEC_IN_A_SEC)) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntDmInitiator: "
                                   "FDDeadLine Timer has not started\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }

            /* Check if Intifinte transmission is required */
            if (pDmInfo->u2TxNoOfMessages == ECFM_INIT_VAL)

            {

                /* Set MSB in the variable to let the 1DM/DMM transmitter know
                 * that infinite transmission is requied
                 */
                ECFM_SET_INFINITE_TX_STATUS (pDmInfo->u2TxNoOfMessages);
            }

            /* Reset the locally maintained sequence number counter */
            ECFM_LBLT_RESET_DM_SEQ_NUM (pMepInfo);

            u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

            if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) != ECFM_TRUE)
            {
                if (pDmInfo->b1TxDmIsDestMepId == ECFM_TRUE)

                {

                    /* Get RMEP MAC Address for the Destination MEPID */
                    if (EcfmLbLtUtilGetRMepMacAddr
                        (pMepInfo->u4MdIndex, pMepInfo->u4MaIndex,
                         pMepInfo->u2MepId, pDmInfo->u2TxDmDestMepId,
                         pDmInfo->TxDmDestMacAddr) != ECFM_SUCCESS)

                    {
                        ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC |
                                       ECFM_CONTROL_PLANE_TRC
                                       | ECFM_OS_RESOURCE_TRC,
                                       "EcfmLbLtClntDmInitiator: "
                                       "Rmep Mac Address not Found !! \r\n");
                        u4RetVal = ECFM_FAILURE;
                        break;
                    }
                }
            }

            /* Send 1DM/DMM */
            if (pDmInfo->u1TxDmType == ECFM_LBLT_DM_TYPE_DMM)

            {

                /* Call the routine to transmit the DMM Packet */
                if (EcfmDmInitXmitDmmPdu (pMepInfo) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntDmInitiator: "
                                   "Unable to transmit DMM Pdu\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }

            else

            {
                /*Resetting the 1DM In to Zero in order to avoid cumulative */
                /*Increase in "packets received" in Frame Delay Buffer */
                pMepInfo->DmInfo.u2NoOf1DmIn = ECFM_INIT_VAL;

                /* Call the routine to transmit the 1DM Packet */
                if (EcfmDmInitXmit1DmPdu (pMepInfo) != ECFM_SUCCESS)

                {
                    ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                   ECFM_ALL_FAILURE_TRC,
                                   "EcfmLbLtClntDmInitiator: "
                                   "Unable to transmit 1DM PDU\r\n");
                    u4RetVal = ECFM_FAILURE;
                    break;
                }
            }
            break;
        case ECFM_LBLT_DM_INTERVAL_EXPIRY:

            /* ignore the interval expiry event in case we are waiting
             * for the last reponse
             */
            /* Send 1DM/DMM */
            if (ECFM_GET_EXTRA_DEADLINE_STATUS (pDmInfo->u4TxDmDeadline) ==
                ECFM_FALSE)

            {
                if (pDmInfo->u1TxDmType == ECFM_LBLT_DM_TYPE_DMM)

                {

                    /* Call the routine to transmit the DMM Packet */
                    if (EcfmDmInitXmitDmmPdu (pMepInfo) != ECFM_SUCCESS)

                    {
                        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                       ECFM_ALL_FAILURE_TRC,
                                       "EcfmLbLtClntDmInitiator: "
                                       "Unable to transmit DMM PDU\r\n");
                        u4RetVal = ECFM_FAILURE;
                        break;
                    }
                }

                else

                {

                    /* Call the routine to transmit the 1DM Packet */
                    if (EcfmDmInitXmit1DmPdu (pMepInfo) != ECFM_SUCCESS)

                    {
                        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC |
                                       ECFM_ALL_FAILURE_TRC,
                                       "EcfmLbLtClntDmInitiator: "
                                       "Unable to transmit 1DM PDU\r\n");
                        u4RetVal = ECFM_FAILURE;
                        break;
                    }
                }
            }

            else

            {
                ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                               "EcfmLbLtClntDmInitiator: ECFM_LBLT_DM_INTERVAL_EXPIRY ignored\r\n");
            }
            u4RetVal = ECFM_SUCCESS;
            break;
        default:
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmLbLtClntDmInitiator: "
                           "Event Not Supported\r\n");
            u4RetVal = ECFM_FAILURE;
            break;
    }
    if (u4RetVal == ECFM_FAILURE)

    {

        /* Update Result OK to False */
        pDmInfo->b1ResultOk = ECFM_FALSE;

        /* Stop the DM Transaction */
        EcfmDmInitStopDmTransaction (pMepInfo);
    }

    else

    {

        /* Update Result OK to True */
        pDmInfo->b1ResultOk = ECFM_TRUE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitXmitDmmPdu
 *
 * Description        : This routine formats and transmits the DMM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmDmInitXmitDmmPdu (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmTimeRepresentation TxTimeStampf;
    tEcfmVlanTag        VlanTag;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelayBuffNode = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1DmmPduStart = NULL;
    UINT1              *pu1DmmPduEnd = NULL;
    UINT1               u1SelectorType = ECFM_INIT_VAL;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT4 		u4DmInterval = ECFM_INIT_VAL;
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT2               u2PduLength = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pDmInfo = &pMepInfo->DmInfo;

    /*Check if Number Of Message to be transmitted have been reached */
    if (pDmInfo->u2TxNoOfMessages == ECFM_INIT_VAL)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmDmInitXmitDmmPdu: "
                       "Stopping DM Transaction as total number of 1DM/DMM"
                       " have been transmitted\r\n");

        /*Stop DM Transaction */
        EcfmDmInitStopDmTransaction (pMepInfo);

        /* Stop DM Transaction at STANDBY Node also */
        EcfmRedSyncDmStopTransaction (pMepInfo);

        /* Sync DM Cache at STANDBY node also */
        EcfmRedSyncDmCache ();
        return ECFM_SUCCESS;
    }

    /* Allocate CRU Buffer */
    u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
    {
        /*Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_DMM_PDU_SIZE,
                                   ECFM_MPLSTP_CTRL_PKT_OFFSET);
        if (pBuf == NULL)
        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiTxXmitDmmPdu: Buffer Allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_DMM_PDU_SIZE, ECFM_INIT_VAL);
        if (pBuf == NULL)

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmitDmmPdu: "
                           "Buffer allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }

    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_DMM_PDU_SIZE);
    pu1DmmPduStart = ECFM_LBLT_PDU;
    pu1DmmPduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;

    /* Format the  Dmm PDU header */
    EcfmDmInitFormatDmmPduHdr (pMepInfo, &pu1DmmPduEnd);

    /* Fill out the Information like TimeStamps into the DMM PDU */
    EcfmDmInitPutDmmInfo (pMepInfo, &pu1DmmPduEnd, &TxTimeStampf);
    u2PduLength = (UINT2) (pu1DmmPduEnd - pu1DmmPduStart);

    /* Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1DmmPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitXmitDmmPdu: "
                       "Buffer copy operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) != ECFM_TRUE)
    {
        pu1DmmPduStart = pu1EthLlcHdr;
        EcfmFormatLbLtTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr,
                                     u2PduLength, ECFM_OPCODE_DMM);
        /* Prepend the Ethernet and the LLC header in the
         * CRU buffer */
        u2PduLength = (UINT2) (pu1EthLlcHdr - pu1DmmPduStart);
        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1DmmPduStart,
                                  (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmitDmmPdu: "
                           "Buffer prepend operation failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
        ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));

        /* Setting the value in Vlan Info structure */
        VlanTag.OuterVlanTag.u2VlanId = (UINT2) pMepInfo->u4PrimaryVidIsid;
        VlanTag.OuterVlanTag.u1Priority = pDmInfo->u1TxDmmVlanPriority;
        VlanTag.OuterVlanTag.u1DropEligible =
            (UINT1) pDmInfo->b1TxDmmDropEligible;
        VlanTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
    }

    u4DmInterval =  ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (pDmInfo->u2TxDmInterval);

   /* If DM interval is less than 100 ms, Add extra 10 ms in DMWhile timer for getting the DM response */
    if (u4DmInterval < ECFM_LM_INTERVAL_DEF_VAL)
    {
	u4DmInterval = u4DmInterval + ECFM_NUM_OF_TICKS_IN_A_MSEC;
    }

  /* Start DMWhile Timer */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_DM_INTERVAL, pMepInfo, u4DmInterval) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitXmitDmmPdu: "
                       "Failure starting DM While timer\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }
    if (ECFM_NODE_STATUS () == ECFM_NODE_ACTIVE)

    {

        /*Add Entry in the DM Buffer */
        pFrmDelayBuffNode = EcfmDmInitAddDmEntry (pMepInfo);
        if (pFrmDelayBuffNode == NULL)

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmitDmmPdu: "
                           "Failure adding node in the frame delay buffer\r\n");
        }

        else

        {

            /* Store the Transmit time of the DMM into the Frame delay buffer */
            ECFM_COPY_TIME_REPRESENTATION (&
                                           (pFrmDelayBuffNode->
                                            TxTimeStampf), &TxTimeStampf);

            /* Reset the frame delay value and the measurement timestamp */
            pFrmDelayBuffNode->FrmDelayValue.u4Seconds = ECFM_INIT_VAL;
            pFrmDelayBuffNode->FrmDelayValue.u4NanoSeconds = ECFM_INIT_VAL;
            pFrmDelayBuffNode->MeasurementTimeStamp = ECFM_INIT_VAL;
        }
    }

    /* Check if Infinite DMM transmission is required */
    if (ECFM_GET_INFINITE_TX_STATUS (pDmInfo->u2TxNoOfMessages) == ECFM_FALSE)

    {

        /* Decrement the number of DM Message to be transmitted */
        ECFM_LBLT_DECR_TRANSMIT_DM_MSG (pMepInfo);
    }
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmDmInitXmitDmmPdu: "
                        "Sending out DMM-PDU to lower layer...\r\n");

    /* Transmit the DMM over MPLS-TP path */
    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
    {
        if (EcfmMplsTpLbLtTxPacket (pBuf, pMepInfo,
                                    ECFM_OPCODE_DMM) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbiTxXmitDmmPdu: Transmit DMM PDU failed\n");
            i4RetVal = ECFM_FAILURE;
        }
    }
    else                        /* Transmit DMM to Ethernet */
    {
#ifdef NPAPI_WANTED
        if (ECFM_HW_DMM_SUPPORT () == ECFM_FALSE)
        {
            i4RetVal = EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                                  pMepInfo->u4PrimaryVidIsid,
                                                  pDmInfo->u1TxDmmVlanPriority,
                                                  pDmInfo->b1TxDmmDropEligible,
                                                  pMepInfo->u1Direction,
                                                  ECFM_OPCODE_DMM, NULL, NULL);

        }
        else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
        {
            UINT1              *pu1DmmPdu = ECFM_LBLT_PDU;

            /*Copy the CRU-BUFF into linear buffer */
            ECFM_COPY_FROM_CRU_BUF (pBuf, pu1DmmPdu, ECFM_INIT_VAL, u4PduSize);
            if (EcfmFsMiEcfmTransmitDmm
                (ECFM_LBLT_CURR_CONTEXT_ID (),
                 ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum,
                                         pTempLbLtPortInfo),
                 pu1DmmPdu, (UINT2) u4PduSize, VlanTag,
                 pMepInfo->u1Direction) != FNP_SUCCESS)

            {
                ECFM_LBLT_INCR_TX_FAILED_COUNT (pMepInfo->u2PortNum);
                ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT (ECFM_LBLT_CURR_CONTEXT_ID
                                                    ());
                i4RetVal = ECFM_FAILURE;
            }

            else
            {
                ECFM_LBLT_INCR_TX_COUNT (pMepInfo->u2PortNum, ECFM_OPCODE_DMM);
                ECFM_LBLT_INCR_CTX_TX_COUNT (ECFM_LBLT_CURR_CONTEXT_ID (),
                                             ECFM_OPCODE_DMM);
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                pBuf = NULL;
                i4RetVal = ECFM_SUCCESS;
            }
        }
#else /*  */
        i4RetVal = EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                              pMepInfo->u4PrimaryVidIsid,
                                              pDmInfo->u1TxDmmVlanPriority,
                                              pDmInfo->b1TxDmmDropEligible,
                                              pMepInfo->u1Direction,
                                              ECFM_OPCODE_DMM, NULL, NULL);
#endif /*  */
    }
    if (i4RetVal != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitXmitDmmPdu: "
                       "DMM transmission to the lower layer failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Increment the number of DMM frames transmitted */
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitFormatDmmPduHdr
 *
 * Description        : This rotuine is used to fill the DMM CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1DmmPdu - Pointer to Pointer to the DMM PDU.
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmDmInitFormatDmmPduHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1DmmPdu)
{
    UINT1              *pu1DmmPdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1DmmPdu = *ppu1DmmPdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1DmmPdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1DmmPdu, ECFM_OPCODE_DMM);

    /* Fill the Next 1 byte with Flag In DMM, Flag Field is set to ZERO */
    ECFM_PUT_1BYTE (pu1DmmPdu, 0);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1DmmPdu, ECFM_DMM_FIRST_TLV_OFFSET);
    *ppu1DmmPdu = pu1DmmPdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitPutDmmInfo
 *
 * Description        : This routine is used to fill the timestamp and End TLV
 *                      in the DMM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure that stores
 *                      the information regarding MEP info.
 * 
 * Output(s)          : pTxTimeStampf - Pointer to the TxTimeStampf filled in
 *                      the DMM pdu.
 *                      ppu1DmmPdu - Pointer to pointer to the DMM Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmDmInitPutDmmInfo (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1DmmPdu,
                      tEcfmTimeRepresentation * pTxTimeStampf)
{
    UINT1              *pu1DmmPdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    UNUSED_PARAM (pMepInfo);
    pu1DmmPdu = *ppu1DmmPdu;

    /* Call the API to get the Current TimeStamp */
    EcfmLbLtUtilDmGetCurrentTime (pTxTimeStampf);

    /* Fill the 4 bytes with Time Units in Seconds */
    ECFM_PUT_4BYTE (pu1DmmPdu, pTxTimeStampf->u4Seconds);

    /* Fill the next 4 bytes with Time Units in nano Seconds */
    ECFM_PUT_4BYTE (pu1DmmPdu, pTxTimeStampf->u4NanoSeconds);

    /* Fill the next 24 bytes with ZERO as it will be used by 
     * the end receving DMM/DMR frame 
     */
    ECFM_MEMSET (pu1DmmPdu, ECFM_RESERVE_VALUE, ECFM_DMM_RESERVED_SIZE);
    pu1DmmPdu = pu1DmmPdu + ECFM_DMM_RESERVED_SIZE;

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1DmmPdu, ECFM_END_TLV_TYPE);
    *ppu1DmmPdu = pu1DmmPdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function           : EcfmDmInitAddDmEntry
 *
 * Description        : This routine is used to add the entry of the transmitted
 *                      DMM frame into the frame delay buffer.
 *
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *                        
 * Output(s)          : None
 *
 * Returns            : tEcfmLbLtFrmDelayBuff * - Pointer to the node added
 ******************************************************************************/
PUBLIC tEcfmLbLtFrmDelayBuff *
EcfmDmInitAddDmEntry (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtFrmDelayBuff *pFrmDelyBuffNode = NULL;
    tEcfmLbLtFrmDelayBuff *pFirstFrmDelyBuffEntry = NULL;
    tEcfmLbLtFrmDelayBuff *pFrmDelyBuffEntry = NULL;
    tEcfmLbLtFrmDelayBuff FrmDelyBuffNode;
    ECFM_MEMSET (&FrmDelyBuffNode, ECFM_INIT_VAL,
                 sizeof (tEcfmLbLtFrmDelayBuff));
    FrmDelyBuffNode.u4MdIndex = pMepInfo->u4MdIndex;
    FrmDelyBuffNode.u4MaIndex = pMepInfo->u4MaIndex;
    FrmDelyBuffNode.u2MepId = pMepInfo->u2MepId;
    FrmDelyBuffNode.u4TransId = pMepInfo->DmInfo.u4CurrTransId;
    FrmDelyBuffNode.u4SeqNum = pMepInfo->DmInfo.u4TxDmSeqNum - ECFM_DECR_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();

    /* Allocate a memory block for frame delay buffer node */
    ECFM_ALLOC_MEM_BLOCK_LBLT_FD_BUFF_TABLE (pFrmDelyBuffNode);
    while (pFrmDelyBuffNode == NULL)

    {

        /* Memory allocation failed, so now check if the memory pool is 
         * exhausted
         */
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitAddDmEntry: "
                       "failure allocating memory for frame delay buffer node\r\n");
        if (ECFM_GET_FREE_MEM_UNITS (ECFM_LBLT_FD_BUFFER_POOL) != ECFM_INIT_VAL)

        {

            /* memory failure failure has occured even when we have free 
             * memory in the pool
             */
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitAddDmEntry: "
                           "Frame delay memory pool corruption\r\n");
            ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT ();
            return NULL;
        }

        /* Memory pool is exhausted for sure, now we can delete the first entry
         * in the pool to have some free memory
         */
        pFirstFrmDelyBuffEntry = (tEcfmLbLtFrmDelayBuff *)
            RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE);
        if (pFirstFrmDelyBuffEntry == NULL)

        {

            /* Memory pool is exhausted and we dont have anyting in the frame
             * delay table to free.*/
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitAddDmEntry: "
                           "Frame delay buffer table corruption \r\n");
            return NULL;
        }

        /* Remove the first node from the table */
        RBTreeRem (ECFM_LBLT_FD_BUFFER_TABLE, pFirstFrmDelyBuffEntry);

        /* Free the memory for the node */
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_FD_BUFFER_POOL,
                             (UINT1 *) pFirstFrmDelyBuffEntry);
        pFirstFrmDelyBuffEntry = NULL;

        /* Now we can again try for allocating memory */
        ECFM_ALLOC_MEM_BLOCK_LBLT_FD_BUFF_TABLE (pFrmDelyBuffNode);
    } ECFM_MEMSET (pFrmDelyBuffNode, ECFM_INIT_VAL,
                   sizeof (tEcfmLbLtFrmDelayBuff));

    /* Fill up the index in the frame delay buffer node */
    pFrmDelyBuffNode->u4MdIndex = pMepInfo->u4MdIndex;
    pFrmDelyBuffNode->u4MaIndex = pMepInfo->u4MaIndex;
    pFrmDelyBuffNode->u2MepId = pMepInfo->u2MepId;

    /* In case of 1DM, the buffer is maintained at the reception end.
     * The reception end has no idea of Transaction ID. The transation ID is
     * set to 1 in case the transaction ID is zero.
     * After this the transaction ID will be taken care by One Dm Transaction
     * timer
     */
    if ((pMepInfo->DmInfo.u1TxDmType == ECFM_LBLT_DM_TYPE_1DM) &&
        (pMepInfo->DmInfo.u4CurrTransId == ECFM_INIT_VAL))

    {
        pMepInfo->DmInfo.u4CurrTransId = ECFM_LBLT_1DM_DEF_TRANS_ID;
    }
    pFrmDelyBuffNode->u4TransId = pMepInfo->DmInfo.u4CurrTransId;
    pFrmDelyBuffNode->u4SeqNum = pMepInfo->DmInfo.u4TxDmSeqNum;

    /* Increment the locally maintained Sequence number */
    ECFM_LBLT_INCR_DM_SEQ_NUM (pMepInfo);

    /* Update the Number of DMM sent counter */
    if (pMepInfo->DmInfo.u1TxDmType == ECFM_LBLT_DM_TYPE_DMM)

    {
        pFrmDelyBuffNode->u2NoOfDMMSent =
            pFrmDelyBuffNode->u4SeqNum + ECFM_INCR_VAL;
        if (pFrmDelyBuffNode->u4SeqNum == ECFM_INIT_VAL)

        {

            /* Initializing DmrIn for the new transaction */
            pMepInfo->DmInfo.u2NoOfDmrIn = ECFM_INIT_VAL;
        }
    }
    if (pMepInfo->DmInfo.u1TxDmType == ECFM_LBLT_DM_TYPE_1DM)
    {
        /* Update the Number of 1DM received counter */
        ECFM_LBLT_INCR_1DM_RCVD_COUNT (pMepInfo);
        pFrmDelyBuffNode->u2NoOf1DMRcvd = pMepInfo->DmInfo.u2NoOf1DmIn;
        pFrmDelyBuffEntry =
            (tEcfmLbLtFrmDelayBuff *) RBTreeGet (ECFM_LBLT_FD_BUFFER_TABLE,
                                                 (tRBElem *) & FrmDelyBuffNode);
        if (pFrmDelyBuffEntry != NULL)
        {
            if (pFrmDelyBuffEntry->u1DmType == ECFM_LBLT_DM_TYPE_DMM)
            {
                /*If previous transaction type is 2DM and Current transaction */
                /*is 1DM then for this 1DM trasaction assign a */
                /*new transaction ID */
                ECFM_LBLT_INCR_DM_TRANS_ID (pMepInfo);
                pMepInfo->DmInfo.u4TxDmSeqNum = ECFM_INIT_VAL;
                /*For this new 1DM transaction initialise packet */
                /*recieved count as 1 */
                pMepInfo->DmInfo.u2NoOf1DmIn = ECFM_VAL_1;
                pFrmDelyBuffNode->u4TransId = pMepInfo->DmInfo.u4CurrTransId;
                pFrmDelyBuffNode->u4SeqNum = pMepInfo->DmInfo.u4TxDmSeqNum;
                pFrmDelyBuffNode->u2NoOf1DMRcvd = pMepInfo->DmInfo.u2NoOf1DmIn;
                ECFM_LBLT_INCR_DM_SEQ_NUM (pMepInfo);
            }
        }
    }
    /* Add the node into the table */
    if (RBTreeAdd (ECFM_LBLT_FD_BUFFER_TABLE, pFrmDelyBuffNode) !=
        ECFM_RB_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitAddDmEntry: "
                       "failure adding node into the frame delay table \r\n");
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_FD_BUFFER_POOL,
                             (UINT1 *) pFrmDelyBuffNode);
        return NULL;
    }
    ECFM_MEMCPY (pFrmDelyBuffNode->PeerMepMacAddress,
                 pMepInfo->DmInfo.TxDmDestMacAddr, ECFM_MAC_ADDR_LENGTH);

    /* Store the current DM type (1DM/DMM) into the frame delay buffer */
    pFrmDelyBuffNode->u1DmType = pMepInfo->DmInfo.u1TxDmType;
    ECFM_LBLT_TRC_FN_EXIT ();
    return pFrmDelyBuffNode;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitStopDmTransaction
 *
 * Description        : This routine is used to stop the on going DM transaction
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
EcfmDmInitStopDmTransaction (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    INT4  i4OneDmTransInterval  = ECFM_INIT_VAL; 
     ECFM_LBLT_TRC_FN_ENTRY ();
     pDmInfo = &pMepInfo->DmInfo;
 
     /* Stop DMWhile Timer */
     EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_DM_INTERVAL, pMepInfo);
 
     /* Stop DMDeadline Timer */
     EcfmLbLtTmrStopTimer (ECFM_LBLT_TMR_DM_DEADLINE, pMepInfo);

    /* If the deadline timer has expired for 1 DM transaction,
     * Start the 1DM Transaction interval timer. 
     */
    if ((pDmInfo->u1TxDmType == ECFM_LBLT_DM_TYPE_1DM) &&
        (pDmInfo->u2OneDmTransInterval != ECFM_INIT_VAL) &&
        (pDmInfo->u1DmStatus == ECFM_TX_STATUS_NOT_READY))

    {

        /* While the timer is running new transaction should not be
         * initiated till its expiry
         */
        pMepInfo->DmInfo.u1DmStatus = ECFM_TX_STATUS_NOT_READY;
        if (pMepInfo->DmInfo.u2OneDmTransInterval < ECFM_NUM_OF_TICKS_IN_A_MSEC)
        {
            i4OneDmTransInterval = 0;
        }
        else

        {
            i4OneDmTransInterval = pMepInfo->DmInfo.u2OneDmTransInterval / ECFM_NUM_OF_TICKS_IN_A_MSEC;
        }

        if (EcfmLbLtTmrStartTimer
            (ECFM_LBLT_TMR_1DM_TRANS_INTERVAL, pMepInfo,
             ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (i4OneDmTransInterval)) !=
            ECFM_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitStopDmTransaction:"
                           "Not able to start 1DM Transaction Timer\r\n");
            pMepInfo->DmInfo.u1DmStatus = ECFM_TX_STATUS_READY;
        }
    }

    else

    {
        pMepInfo->DmInfo.u1DmStatus = ECFM_TX_STATUS_READY;
    }
    ECFM_CLEAR_INFINITE_TX_STATUS (pMepInfo->DmInfo.u2TxNoOfMessages);
    ECFM_CLEAR_EXTRA_DEADLINE_STATUS (pMepInfo->DmInfo.u4TxDmDeadline);
    pMepInfo->DmInfo.u2TxNoOfMessages = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitXmit1DmPdu
 *
 * Description        : This routine formats and transmits the 1DM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the structure that stores the 
 *                      information regarding MEP info.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
PRIVATE INT4
EcfmDmInitXmit1DmPdu (tEcfmLbLtMepInfo * pMepInfo)
{
    tEcfmVlanTag        VlanTag;
    tEcfmLbLtDmInfo    *pDmInfo = NULL;
    tEcfmBufChainHeader *pBuf = NULL;
#ifdef NPAPI_WANTED
    tEcfmLbLtPortInfo  *pTempLbLtPortInfo = NULL;
#endif
    INT4                i4RetVal = ECFM_SUCCESS;
    UINT4               u4PduSize = ECFM_INIT_VAL;
    UINT2               u2PduLength = ECFM_INIT_VAL;
    UINT1              *pu1EthLlcHdr = NULL;
    UINT1              *pu1Dm1wPduStart = NULL;
    UINT1              *pu1Dm1wPduEnd = NULL;
    UINT1               u1SelectorType = ECFM_INIT_VAL;

    ECFM_LBLT_TRC_FN_ENTRY ();
    pDmInfo = &pMepInfo->DmInfo;

    /*Check if Number Of Message to be transmitted have been reached */
    if (pDmInfo->u2TxNoOfMessages == ECFM_INIT_VAL)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC, "EcfmDmInitXmitDm1wPdu: "
                       "Stopping DM Transaction as total number of 1DM"
                       " have been transmitted\r\n");

        /*Stop 1DM Transaction */
        EcfmDmInitStopDmTransaction (pMepInfo);

        /* Stop DM Transaction at STANDBY Node also */
        EcfmRedSyncDmStopTransaction (pMepInfo);

        /* Sync DM Cache at STANDBY node also */
        EcfmRedSyncDmCache ();
        return ECFM_SUCCESS;
    }

    /* Allocate CRU Buffer */
    u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
    {
        /*Allocates the CRU Buffer */
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_1DM_PDU_SIZE,
                                   ECFM_MPLSTP_CTRL_PKT_OFFSET);
        if (pBuf == NULL)
        {
            ECFM_LBLT_TRC (ECFM_BUFFER_TRC | ECFM_CONTROL_PLANE_TRC |
                           ECFM_ALL_FAILURE_TRC,
                           "EcfmLbiTxXmit1DmPdu: Buffer Allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    else
    {
        pBuf = ECFM_ALLOC_CRU_BUF (ECFM_MAX_1DM_PDU_SIZE, ECFM_INIT_VAL);
        if (pBuf == NULL)

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmit1DmPdu: "
                           "Buffer allocation failed\r\n");
            ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT ();
            return ECFM_FAILURE;
        }
    }
    ECFM_MEMSET (ECFM_LBLT_PDU, ECFM_INIT_VAL, ECFM_MAX_1DM_PDU_SIZE);
    pu1Dm1wPduStart = ECFM_LBLT_PDU;
    pu1Dm1wPduEnd = ECFM_LBLT_PDU;
    pu1EthLlcHdr = ECFM_LBLT_PDU;

    /* Format the  1Dm PDU header */
    EcfmDmInitFormat1DmPduHdr (pMepInfo, &pu1Dm1wPduEnd);

    /* Fill out the Information like TimeStamps into the 1DM PDU */
    EcfmDmInitPut1DmInfo (&pu1Dm1wPduEnd);
    u2PduLength = (UINT2) (pu1Dm1wPduEnd - pu1Dm1wPduStart);

    /* Copying PDU over CRU buffer */
    if (ECFM_COPY_OVER_CRU_BUF (pBuf, pu1Dm1wPduStart, ECFM_INIT_VAL,
                                (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitXmit1DmPdu: "
                       "Buffer copy operation failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    u1SelectorType = pMepInfo->pMaInfo->u1SelectorType;

    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) != ECFM_TRUE)
    {
        pu1Dm1wPduStart = pu1EthLlcHdr;
        EcfmFormatLbLtTaskPduEthHdr (pMepInfo, &pu1EthLlcHdr,
                                     u2PduLength, ECFM_OPCODE_1DM);

        /* Prepend the Ethernet and the LLC header in the
         * CRU buffer 
         */
        u2PduLength = (UINT2) (pu1EthLlcHdr - pu1Dm1wPduStart);
        if (ECFM_PREPEND_CRU_BUF (pBuf, pu1Dm1wPduStart,
                                  (UINT4) (u2PduLength)) != ECFM_CRU_SUCCESS)

        {
            ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                           "EcfmDmInitXmit1DmPdu: "
                           "Buffer prepend operation failed\r\n");
            ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
            return ECFM_FAILURE;
        }
        ECFM_MEMSET (&VlanTag, ECFM_INIT_VAL, sizeof (tEcfmVlanTag));

        /* Setting the value in Vlan Info structure */
        VlanTag.OuterVlanTag.u2VlanId = (UINT2) pMepInfo->u4PrimaryVidIsid;
        VlanTag.OuterVlanTag.u1Priority = pDmInfo->u1Tx1DmVlanPriority;
        VlanTag.OuterVlanTag.u1DropEligible =
            (UINT1) pDmInfo->b1Tx1DmDropEligible;
        VlanTag.OuterVlanTag.u1TagType = ECFM_VLAN_UNTAGGED;
    }

    /* Start DMWhile Timer */
    if (EcfmLbLtTmrStartTimer
        (ECFM_LBLT_TMR_DM_INTERVAL, pMepInfo,
         ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC (pDmInfo->
                                               u2TxDmInterval)) != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitXmit1DmPdu: "
                       "Failure starting DM While timer\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        return ECFM_FAILURE;
    }

    /* Check if Infinite 1DM transmission is required */
    if (ECFM_GET_INFINITE_TX_STATUS (pDmInfo->u2TxNoOfMessages) == ECFM_FALSE)

    {

        /* Decrement the number of DM Message to be transmitted */
        ECFM_LBLT_DECR_TRANSMIT_DM_MSG (pMepInfo);
    }
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ECFM_LBLT_PKT_DUMP (ECFM_DUMP_TRC, pBuf, u4PduSize,
                        "EcfmDmInitXmit1DmPdu: "
                        "Sending out 1DM-PDU to lower layer...\r\n");

    /* Transmit the 1DM over MPLS-TP path */
    if (ECFM_IS_SELECTOR_TYPE_MPLS_TP (u1SelectorType) == ECFM_TRUE)
    {
        if (EcfmMplsTpLbLtTxPacket (pBuf, pMepInfo,
                                    ECFM_OPCODE_1DM) != ECFM_SUCCESS)
        {
            ECFM_LBLT_TRC (ECFM_ALL_FAILURE_TRC | ECFM_CONTROL_PLANE_TRC,
                           "EcfmLbiTxXmit1DmPdu: Transmit 1DM PDU failed\n");
            i4RetVal = ECFM_FAILURE;
        }
    }
    else                        /* Transmit 1DM to Ethernet */
    {

#ifdef NPAPI_WANTED
        if (ECFM_HW_1DM_SUPPORT () == ECFM_FALSE)
        {
            i4RetVal =
                EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                           pMepInfo->u4PrimaryVidIsid,
                                           pDmInfo->u1Tx1DmVlanPriority,
                                           pDmInfo->b1Tx1DmDropEligible,
                                           pMepInfo->u1Direction,
                                           ECFM_OPCODE_1DM, NULL, NULL);
        }
        else if (ECFM_IS_NP_PROGRAMMING_ALLOWED () == ECFM_TRUE)
        {
            UINT1              *pu1OneDmPdu = ECFM_LBLT_PDU;

            /*Copy the CRU-BUFF into linear buffer */
            ECFM_COPY_FROM_CRU_BUF (pBuf, pu1OneDmPdu, ECFM_INIT_VAL,
                                    u4PduSize);
            if (EcfmFsMiEcfmTransmit1Dm
                (ECFM_LBLT_CURR_CONTEXT_ID (),
                 ECFM_LBLT_GET_PHY_PORT (pMepInfo->u2PortNum,
                                         pTempLbLtPortInfo), pu1OneDmPdu,
                 (UINT2) u4PduSize, VlanTag,
                 pMepInfo->u1Direction) != FNP_SUCCESS)

            {
                i4RetVal = ECFM_FAILURE;
            }

            else
            {
                ECFM_LBLT_INCR_TX_COUNT (pMepInfo->u2PortNum, ECFM_OPCODE_1DM);
                ECFM_LBLT_INCR_CTX_TX_COUNT (ECFM_LBLT_CURR_CONTEXT_ID (),
                                             ECFM_OPCODE_1DM);
                ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
                pBuf = NULL;
                i4RetVal = ECFM_SUCCESS;
            }
        }
#else /*  */
        i4RetVal =
            EcfmLbLtCtrlTxTransmitPkt (pBuf, pMepInfo->u2PortNum,
                                       pMepInfo->u4PrimaryVidIsid,
                                       pDmInfo->u1Tx1DmVlanPriority,
                                       pDmInfo->b1Tx1DmDropEligible,
                                       pMepInfo->u1Direction, ECFM_OPCODE_1DM,
                                       NULL, NULL);
#endif /*  */
    }
    if (i4RetVal != ECFM_SUCCESS)

    {
        ECFM_LBLT_TRC (ECFM_CONTROL_PLANE_TRC | ECFM_ALL_FAILURE_TRC,
                       "EcfmDmInitXmitDmmPdu: "
                       "1DM transmission to the lower layer failed\r\n");
        ECFM_RELEASE_CRU_BUF (pBuf, ECFM_FALSE);
        ECFM_LBLT_INCR_TX_FAILED_COUNT (pMepInfo->u2PortNum);
        return ECFM_FAILURE;
    }
    ECFM_LBLT_TRC_FN_EXIT ();
    return ECFM_SUCCESS;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitFormat1DmPduHdr
 *
 * Description        : This rotuine is used to fill the 1DM CFM PDU Header.
 * 
 * Input              : pMepInfo - Pointer to the MEP structure  
 *                       
 * Output(s)          : ppu1Dm1wPdu - Pointer to Pointer to the 1DM PDU.
 * 
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmDmInitFormat1DmPduHdr (tEcfmLbLtMepInfo * pMepInfo, UINT1 **ppu1Dm1wPdu)
{
    UINT1              *pu1Dm1wPdu = NULL;
    UINT1               u1LevelVer = ECFM_INIT_VAL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Dm1wPdu = *ppu1Dm1wPdu;

    /* Fill in the 4 byte as CFM header */
    /* Fill the 1byte as Md Level(3MSB) and Version */
    ECFM_SET_MDLEVEL (u1LevelVer, pMepInfo->u1MdLevel);
    ECFM_SET_VERSION (u1LevelVer);
    ECFM_PUT_1BYTE (pu1Dm1wPdu, u1LevelVer);

    /*Fill in the next 1 byte as OpCode */
    ECFM_PUT_1BYTE (pu1Dm1wPdu, ECFM_OPCODE_1DM);

    /* Fill the Next 1 byte with Flag In DMM, Flag Field is set to ZERO */
    ECFM_PUT_1BYTE (pu1Dm1wPdu, ECFM_INIT_VAL);

    /*Fill in the next 1 byte as First TLV Offset */
    ECFM_PUT_1BYTE (pu1Dm1wPdu, ECFM_1DM_FIRST_TLV_OFFSET);
    *ppu1Dm1wPdu = pu1Dm1wPdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 * Function Name      : EcfmDmInitPut1DmInfo
 *
 * Description        : This routine is used to fill the timestamp and End TLV
 *                      in the 1DM PDU.
 *                        
 * Input(s)           : pMepInfo - Pointer to the Pdu Info structure that stores
 *                      the information regarding MEP info.
 * 
 * Output(s)          : pTxTimeStampf - Pointer to the TxTimeStampf filled in
 *                      the 1DM pdu.
 *                      ppu1Dm1wPdu - Pointer to pointer to the 1DM Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
EcfmDmInitPut1DmInfo (UINT1 **ppu1Dm1wPdu)
{
    tEcfmTimeRepresentation TxTimeStampf;
    UINT1              *pu1Dm1wPdu = NULL;
    ECFM_LBLT_TRC_FN_ENTRY ();
    pu1Dm1wPdu = *ppu1Dm1wPdu;

    /* Call the API to get the Current TimeStamp */
    EcfmLbLtUtilDmGetCurrentTime (&TxTimeStampf);

    /* Fill the 4 bytes with Time Units in Seconds */
    ECFM_PUT_4BYTE (pu1Dm1wPdu, TxTimeStampf.u4Seconds);

    /* Fill the next 4 bytes with Time Units in nano Seconds */
    ECFM_PUT_4BYTE (pu1Dm1wPdu, TxTimeStampf.u4NanoSeconds);

    /* Fill the next 8 bytes with ZERO */
    ECFM_MEMSET (pu1Dm1wPdu, ECFM_RESERVE_VALUE, ECFM_1DM_RESERVED_SIZE);
    pu1Dm1wPdu = pu1Dm1wPdu + ECFM_1DM_RESERVED_SIZE;

    /* Fill the End TLV */
    ECFM_PUT_1BYTE (pu1Dm1wPdu, ECFM_END_TLV_TYPE);
    *ppu1Dm1wPdu = pu1Dm1wPdu;
    ECFM_LBLT_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************
  End of File cfmdmini.c
 ******************************************************************************/
