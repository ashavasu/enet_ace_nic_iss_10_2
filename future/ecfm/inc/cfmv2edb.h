/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfmv2edb.h,v 1.2 2011/12/27 11:53:40 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _CFMV2EDB_H
#define _CFMV2EDB_H

UINT1 Ieee8021CfmStackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021CfmVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021CfmDefaultMdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021CfmConfigErrorListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021CfmMaCompTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 cfmv2e [] ={1,3,111,2,802,1,1,7};
tSNMP_OID_TYPE cfmv2eOID = {8, cfmv2e};


/* Generated OID's for tables */
UINT4 Ieee8021CfmStackTable [] ={1,3,111,2,802,1,1,8,1,1,2};
tSNMP_OID_TYPE Ieee8021CfmStackTableOID = {11, Ieee8021CfmStackTable};


UINT4 Ieee8021CfmVlanTable [] ={1,3,111,2,802,1,1,8,1,3,2};
tSNMP_OID_TYPE Ieee8021CfmVlanTableOID = {11, Ieee8021CfmVlanTable};


UINT4 Ieee8021CfmDefaultMdTable [] ={1,3,111,2,802,1,1,8,1,2,5};
tSNMP_OID_TYPE Ieee8021CfmDefaultMdTableOID = {11, Ieee8021CfmDefaultMdTable};


UINT4 Ieee8021CfmConfigErrorListTable [] ={1,3,111,2,802,1,1,8,1,4,2};
tSNMP_OID_TYPE Ieee8021CfmConfigErrorListTableOID = {11, Ieee8021CfmConfigErrorListTable};


UINT4 Ieee8021CfmMaCompTable [] ={1,3,111,2,802,1,1,8,1,6,4};
tSNMP_OID_TYPE Ieee8021CfmMaCompTableOID = {11, Ieee8021CfmMaCompTable};




UINT4 Ieee8021CfmStackifIndex [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,1};
UINT4 Ieee8021CfmStackServiceSelectorType [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,2};
UINT4 Ieee8021CfmStackServiceSelectorOrNone [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,3};
UINT4 Ieee8021CfmStackMdLevel [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,4};
UINT4 Ieee8021CfmStackDirection [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,5};
UINT4 Ieee8021CfmStackMdIndex [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,6};
UINT4 Ieee8021CfmStackMaIndex [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,7};
UINT4 Ieee8021CfmStackMepId [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,8};
UINT4 Ieee8021CfmStackMacAddress [ ] ={1,3,111,2,802,1,1,8,1,1,2,1,9};
UINT4 Ieee8021CfmVlanComponentId [ ] ={1,3,111,2,802,1,1,8,1,3,2,1,1};
UINT4 Ieee8021CfmVlanSelector [ ] ={1,3,111,2,802,1,1,8,1,3,2,1,3};
UINT4 Ieee8021CfmVlanPrimarySelector [ ] ={1,3,111,2,802,1,1,8,1,3,2,1,5};
UINT4 Ieee8021CfmVlanRowStatus [ ] ={1,3,111,2,802,1,1,8,1,3,2,1,6};
UINT4 Ieee8021CfmDefaultMdComponentId [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,1};
UINT4 Ieee8021CfmDefaultMdPrimarySelectorType [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,2};
UINT4 Ieee8021CfmDefaultMdPrimarySelector [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,3};
UINT4 Ieee8021CfmDefaultMdStatus [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,4};
UINT4 Ieee8021CfmDefaultMdLevel [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,5};
UINT4 Ieee8021CfmDefaultMdMhfCreation [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,6};
UINT4 Ieee8021CfmDefaultMdIdPermission [ ] ={1,3,111,2,802,1,1,8,1,2,5,1,7};
UINT4 Ieee8021CfmConfigErrorListSelectorType [ ] ={1,3,111,2,802,1,1,8,1,4,2,1,1};
UINT4 Ieee8021CfmConfigErrorListSelector [ ] ={1,3,111,2,802,1,1,8,1,4,2,1,2};
UINT4 Ieee8021CfmConfigErrorListIfIndex [ ] ={1,3,111,2,802,1,1,8,1,4,2,1,3};
UINT4 Ieee8021CfmConfigErrorListErrorType [ ] ={1,3,111,2,802,1,1,8,1,4,2,1,4};
UINT4 Ieee8021CfmMaComponentId [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,1};
UINT4 Ieee8021CfmMaCompPrimarySelectorType [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,2};
UINT4 Ieee8021CfmMaCompPrimarySelectorOrNone [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,3};
UINT4 Ieee8021CfmMaCompMhfCreation [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,4};
UINT4 Ieee8021CfmMaCompIdPermission [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,5};
UINT4 Ieee8021CfmMaCompNumberOfVids [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,6};
UINT4 Ieee8021CfmMaCompRowStatus [ ] ={1,3,111,2,802,1,1,8,1,6,4,1,7};




tMbDbEntry Ieee8021CfmStackTableMibEntry[]= {

{{13,Ieee8021CfmStackifIndex}, GetNextIndexIeee8021CfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackServiceSelectorType}, GetNextIndexIeee8021CfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackServiceSelectorOrNone}, GetNextIndexIeee8021CfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackMdLevel}, GetNextIndexIeee8021CfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackDirection}, GetNextIndexIeee8021CfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackMdIndex}, GetNextIndexIeee8021CfmStackTable, Ieee8021CfmStackMdIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackMaIndex}, GetNextIndexIeee8021CfmStackTable, Ieee8021CfmStackMaIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackMepId}, GetNextIndexIeee8021CfmStackTable, Ieee8021CfmStackMepIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021CfmStackMacAddress}, GetNextIndexIeee8021CfmStackTable, Ieee8021CfmStackMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Ieee8021CfmStackTableINDEX, 5, 0, 0, NULL},
};
tMibData Ieee8021CfmStackTableEntry = { 9, Ieee8021CfmStackTableMibEntry };

tMbDbEntry Ieee8021CfmDefaultMdTableMibEntry[]= {

{{13,Ieee8021CfmDefaultMdComponentId}, GetNextIndexIeee8021CfmDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmDefaultMdPrimarySelectorType}, GetNextIndexIeee8021CfmDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmDefaultMdPrimarySelector}, GetNextIndexIeee8021CfmDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmDefaultMdStatus}, GetNextIndexIeee8021CfmDefaultMdTable, Ieee8021CfmDefaultMdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmDefaultMdLevel}, GetNextIndexIeee8021CfmDefaultMdTable, Ieee8021CfmDefaultMdLevelGet, Ieee8021CfmDefaultMdLevelSet, Ieee8021CfmDefaultMdLevelTest, Ieee8021CfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, "-1"},

{{13,Ieee8021CfmDefaultMdMhfCreation}, GetNextIndexIeee8021CfmDefaultMdTable, Ieee8021CfmDefaultMdMhfCreationGet, Ieee8021CfmDefaultMdMhfCreationSet, Ieee8021CfmDefaultMdMhfCreationTest, Ieee8021CfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmDefaultMdIdPermission}, GetNextIndexIeee8021CfmDefaultMdTable, Ieee8021CfmDefaultMdIdPermissionGet, Ieee8021CfmDefaultMdIdPermissionSet, Ieee8021CfmDefaultMdIdPermissionTest, Ieee8021CfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmDefaultMdTableINDEX, 3, 0, 0, NULL},
};
tMibData Ieee8021CfmDefaultMdTableEntry = { 7, Ieee8021CfmDefaultMdTableMibEntry };

tMbDbEntry Ieee8021CfmVlanTableMibEntry[]= {

{{13,Ieee8021CfmVlanComponentId}, GetNextIndexIeee8021CfmVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmVlanTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021CfmVlanSelector}, GetNextIndexIeee8021CfmVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmVlanTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021CfmVlanPrimarySelector}, GetNextIndexIeee8021CfmVlanTable, Ieee8021CfmVlanPrimarySelectorGet, Ieee8021CfmVlanPrimarySelectorSet, Ieee8021CfmVlanPrimarySelectorTest, Ieee8021CfmVlanTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CfmVlanTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021CfmVlanRowStatus}, GetNextIndexIeee8021CfmVlanTable, Ieee8021CfmVlanRowStatusGet, Ieee8021CfmVlanRowStatusSet, Ieee8021CfmVlanRowStatusTest, Ieee8021CfmVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmVlanTableINDEX, 2, 0, 1, NULL},
};
tMibData Ieee8021CfmVlanTableEntry = { 4, Ieee8021CfmVlanTableMibEntry };

tMbDbEntry Ieee8021CfmConfigErrorListTableMibEntry[]= {

{{13,Ieee8021CfmConfigErrorListSelectorType}, GetNextIndexIeee8021CfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CfmConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmConfigErrorListSelector}, GetNextIndexIeee8021CfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmConfigErrorListIfIndex}, GetNextIndexIeee8021CfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CfmConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmConfigErrorListErrorType}, GetNextIndexIeee8021CfmConfigErrorListTable, Ieee8021CfmConfigErrorListErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021CfmConfigErrorListTableINDEX, 3, 0, 0, NULL},
};
tMibData Ieee8021CfmConfigErrorListTableEntry = { 4, Ieee8021CfmConfigErrorListTableMibEntry };

tMbDbEntry Ieee8021CfmMaCompTableMibEntry[]= {

{{13,Ieee8021CfmMaComponentId}, GetNextIndexIeee8021CfmMaCompTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CfmMaCompTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmMaCompPrimarySelectorType}, GetNextIndexIeee8021CfmMaCompTable, Ieee8021CfmMaCompPrimarySelectorTypeGet, Ieee8021CfmMaCompPrimarySelectorTypeSet, Ieee8021CfmMaCompPrimarySelectorTypeTest, Ieee8021CfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmMaCompTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmMaCompPrimarySelectorOrNone}, GetNextIndexIeee8021CfmMaCompTable, Ieee8021CfmMaCompPrimarySelectorOrNoneGet, Ieee8021CfmMaCompPrimarySelectorOrNoneSet, Ieee8021CfmMaCompPrimarySelectorOrNoneTest, Ieee8021CfmMaCompTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CfmMaCompTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmMaCompMhfCreation}, GetNextIndexIeee8021CfmMaCompTable, Ieee8021CfmMaCompMhfCreationGet, Ieee8021CfmMaCompMhfCreationSet, Ieee8021CfmMaCompMhfCreationTest, Ieee8021CfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmMaCompTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmMaCompIdPermission}, GetNextIndexIeee8021CfmMaCompTable, Ieee8021CfmMaCompIdPermissionGet, Ieee8021CfmMaCompIdPermissionSet, Ieee8021CfmMaCompIdPermissionTest, Ieee8021CfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmMaCompTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmMaCompNumberOfVids}, GetNextIndexIeee8021CfmMaCompTable, Ieee8021CfmMaCompNumberOfVidsGet, Ieee8021CfmMaCompNumberOfVidsSet, Ieee8021CfmMaCompNumberOfVidsTest, Ieee8021CfmMaCompTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CfmMaCompTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021CfmMaCompRowStatus}, GetNextIndexIeee8021CfmMaCompTable, Ieee8021CfmMaCompRowStatusGet, Ieee8021CfmMaCompRowStatusSet, Ieee8021CfmMaCompRowStatusTest, Ieee8021CfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CfmMaCompTableINDEX, 3, 0, 1, NULL},
};
tMibData Ieee8021CfmMaCompTableEntry = { 7, Ieee8021CfmMaCompTableMibEntry };

#endif /* _CFMV2EDB_H */

