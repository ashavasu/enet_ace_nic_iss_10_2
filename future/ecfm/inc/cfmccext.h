/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmccext.h,v 1.12 2012/02/20 12:52:57 siva Exp $
 * 
 * Description: This file contains various extern variables declared 
 *              used in cc task.
 *********************************************************************/
#ifndef _CFM_CC_EXTN_H
#define _CFM_CC_EXTN_H
/******************************************************************************/
extern tEcfmCcGlobalInfo  gEcfmCcGlobalInfo;
extern tEcfmCcContextInfo *gpEcfmCcContextInfo;
extern const UINT4 au4CCTimer[ECFM_MAX_CCM_INTERVALS];
extern const UINT4 au4AisLckTimer[ECFM_MAX_AIS_LCK_INTERVALS];
extern const UINT4 au4LmmTimer[ECFM_MAX_LMM_INTERVALS];
extern const UINT4 au4AvlbltyTimer[ECFM_MAX_AVLBLTY_INTERVALS];
extern const UINT1 au1BlockedOperType[ECFM_MAX_INTF_OPER_TYPES];
extern UINT1 gu1EcfmCcInitialised;
extern UINT1              *gpu1ActiveMdLevels;
extern tEcfmCcMaInfo      **gppActiveMaNodes;
extern tEcfmCcMepInfo      *gpEcfmCcMepNode;
extern UINT1               gau1EcfmCCPdu[ECFM_MAX_PDU_SIZE];
#endif /* _CFM_CC_EXTN_H*/
/*******************************************************************************
                            End of File cfmccext.h                              
 ******************************************************************************/

