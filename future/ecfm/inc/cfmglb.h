/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmglb.h,v 1.26 2015/07/18 09:56:47 siva Exp $
 * 
 * Description: This file contains various common global variables.
 *********************************************************************/
#ifndef _CFM_GLOB_H
#define _CFM_GLOB_H
/*****************************************************************************/
/* IEEE 802.1ag Table: 8-9 and Table: 8-10 */
tMacAddr gu1CfmMacAddr =
  {0x01, 0x80, 0xC2, 0x00, 0x00, 0x3f};

tMacAddr gu1CfmTunnelMacAddr =
  {0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd9};

UINT4               gu4CfmCrcTable[256] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba,
    0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
    0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
    0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,
    0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
    0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,
    0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116,
    0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
    0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
    0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a,
    0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,
    0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c,
    0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
    0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
    0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086,
    0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4,
    0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
    0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
    0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe,
    0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
    0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252,
    0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60,
    0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
    0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
    0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,
    0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a,
    0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
    0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e,
    0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
    0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
    0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0,
    0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,
    0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
    0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};


UINT1 gau1EcfmSystemStatus[SYS_DEF_MAX_NUM_CONTEXTS];
UINT1 gau1EcfmModuleStatus[SYS_DEF_MAX_NUM_CONTEXTS];
UINT1 gau1EcfmY1731Status[SYS_DEF_MAX_NUM_CONTEXTS];
UINT4 gau4EcfmTraceOption[SYS_DEF_MAX_NUM_CONTEXTS];
UINT4 gu4EcfmGlobalTrace = ECFM_INIT_VAL;
tEcfmNodeStatus    gEcfmNodeStatus; /* Can be ECFM_NODE_IDLE /
                                     * ECFM_NODE_ACTIVE /ECFM_NODE_STANDBY
                                     * */
UINT4 gaau4TransportDomain[ECFM_MAX_TRANSPORT_DOMAIN][ECFM_MAX_TRANSPORT_DOMAIN_LEN] =
{
   {1,3,6,1,2,1,100,1,1},   /* transportDomainUdpIpv4 */   
   {1,3,6,1,2,1,100,1,2},   /* transportDomainUdpIpv6 */  
   {1,3,6,1,2,1,100,1,3},   /* transportDomainUdpIpv4z */ 
   {1,3,6,1,2,1,100,1,4},   /* transportDomainUdpIpv6z */ 
   {1,3,6,1,2,1,100,1,5},   /* transportDomainTcpIpv4 */  
   {1,3,6,1,2,1,100,1,6},   /* transportDomainTcpIpv6 */  
   {1,3,6,1,2,1,100,1,7},   /* transportDomainTcpIpv4z */ 
   {1,3,6,1,2,1,100,1,8},   /* transportDomainTcpIpv6z */ 
   {1,3,6,1,2,1,100,1,9},   /* transportDomainSctpIpv4 */ 
   {1,3,6,1,2,1,100,1,10},  /* transportDomainSctpIpv6 */ 
   {1,3,6,1,2,1,100,1,11},  /* transportDomainSctpIpv4z */
   {1,3,6,1,2,1,100,1,12},  /* transportDomainSctpIpv6z */
   {1,3,6,1,2,1,100,1,13},  /* transportDomainLocal */    
   {1,3,6,1,2,1,100,1,14},  /* transportDomainUdpDns */   
   {1,3,6,1,2,1,100,1,15},  /* transportDomainTcpDns */   
   {1,3,6,1,2,1,100,1,16}   /* transportDomainSctpDns */  
};
/* Y.1731 : Array of MEP prompt in CLI */
tEcfmMepPromptInfo  gaEcfmMepPromptInfo[ECFM_CLI_MAX_SESSIONS];
#ifdef L2RED_WANTED
tEcfmRedGlobalInfo gEcfmRedGlobalInfo;
#endif
#ifdef MBSM_WANTED
tMbsmSlotInfo gEcfmSlotInfo; /* Used to store the Slot Info for processing in 
                              * ECFM module */
BOOLEAN       gbEcfmMbsmEvtHndl = ECFM_FALSE; 
                                 /* Used to check whether to call MBSM related
                                  * NPAPI or not. MBSM NPAPIs are called if 
                                  * this is TRUR else not*/
#endif
#ifdef NPAPI_WANTED
UINT4         gu4HwCapability=0;
#endif

/* bit-list scanning mask 
 *
 * Byte    [       1       ][           2          ][           3           ]
 *
 * BitList [7|6|5|4|3|2|1|0][7|6 |5 |4 |3 |2 |1 |0 ][7 |6 |5 |4 |3 |2 |1 |0 ]
 *
 * Port     1 2 3 4 5 6 7 8  9 10 11 12 13 14 15 16  17 18 19 20 21 22 24 24
 *
 * This bitmask stores the ports for a give value of byte. eg 
 * if byte = 0x1E/[00011110], this means that ports 4,5,6,7 will be set as ON.
 * */
UINT1 gaau1EcfmBitListScanMask[256][8] =  
{
 /* 0 */ {0, 0, 0, 0, 0, 0, 0, 0},
 /* 1 */ {8, 0, 0, 0, 0, 0, 0, 0},
 /* 2 */ {7, 0, 0, 0, 0, 0, 0, 0},
 /* 3 */ {7, 8, 0, 0, 0, 0, 0, 0},
 /* 4 */ {6, 0, 0, 0, 0, 0, 0, 0},
 /* 5 */ {6, 8, 0, 0, 0, 0, 0, 0},
 /* 6 */ {6, 7, 0, 0, 0, 0, 0, 0},
 /* 7 */ {6, 7, 8, 0, 0, 0, 0, 0},
 /* 8 */ {5, 0, 0, 0, 0, 0, 0, 0},
 /* 9 */ {5, 8, 0, 0, 0, 0, 0, 0},
 /* 10 */ {5, 7, 0, 0, 0, 0, 0, 0},
 /* 11 */ {5, 7, 8, 0, 0, 0, 0, 0},
 /* 12 */ {5, 6, 0, 0, 0, 0, 0, 0},
 /* 13 */ {5, 6, 8, 0, 0, 0, 0, 0},
 /* 14 */ {5, 6, 7, 0, 0, 0, 0, 0},
 /* 15 */ {5, 6, 7, 8, 0, 0, 0, 0},
 /* 16 */ {4, 0, 0, 0, 0, 0, 0, 0},
 /* 17 */ {4, 8, 0, 0, 0, 0, 0, 0},
 /* 18 */ {4, 7, 0, 0, 0, 0, 0, 0},
 /* 19 */ {4, 7, 8, 0, 0, 0, 0, 0},
 /* 20 */ {4, 6, 0, 0, 0, 0, 0, 0},
 /* 21 */ {4, 6, 8, 0, 0, 0, 0, 0},
 /* 22 */ {4, 6, 7, 0, 0, 0, 0, 0},
 /* 23 */ {4, 6, 7, 8, 0, 0, 0, 0},
 /* 24 */ {4, 5, 0, 0, 0, 0, 0, 0},
 /* 25 */ {4, 5, 8, 0, 0, 0, 0, 0},
 /* 26 */ {4, 5, 7, 0, 0, 0, 0, 0},
 /* 27 */ {4, 5, 7, 8, 0, 0, 0, 0},
 /* 28 */ {4, 5, 6, 0, 0, 0, 0, 0},
 /* 29 */ {4, 5, 6, 8, 0, 0, 0, 0},
 /* 30 */ {4, 5, 6, 7, 0, 0, 0, 0},
 /* 31 */ {4, 5, 6, 7, 8, 0, 0, 0},
 /* 32 */ {3, 0, 0, 0, 0, 0, 0, 0},
 /* 33 */ {3, 8, 0, 0, 0, 0, 0, 0},
 /* 34 */ {3, 7, 0, 0, 0, 0, 0, 0},
 /* 35 */ {3, 7, 8, 0, 0, 0, 0, 0},
 /* 36 */ {3, 6, 0, 0, 0, 0, 0, 0},
 /* 37 */ {3, 6, 8, 0, 0, 0, 0, 0},
 /* 38 */ {3, 6, 7, 0, 0, 0, 0, 0},
 /* 39 */ {3, 6, 7, 8, 0, 0, 0, 0},
 /* 40 */ {3, 5, 0, 0, 0, 0, 0, 0},
 /* 41 */ {3, 5, 8, 0, 0, 0, 0, 0},
 /* 42 */ {3, 5, 7, 0, 0, 0, 0, 0},
 /* 43 */ {3, 5, 7, 8, 0, 0, 0, 0},
 /* 44 */ {3, 5, 6, 0, 0, 0, 0, 0},
 /* 45 */ {3, 5, 6, 8, 0, 0, 0, 0},
 /* 46 */ {3, 5, 6, 7, 0, 0, 0, 0},
 /* 47 */ {3, 5, 6, 7, 8, 0, 0, 0},
 /* 48 */ {3, 4, 0, 0, 0, 0, 0, 0},
 /* 49 */ {3, 4, 8, 0, 0, 0, 0, 0},
 /* 50 */ {3, 4, 7, 0, 0, 0, 0, 0},
 /* 51 */ {3, 4, 7, 8, 0, 0, 0, 0},
 /* 52 */ {3, 4, 6, 0, 0, 0, 0, 0},
 /* 53 */ {3, 4, 6, 8, 0, 0, 0, 0},
 /* 54 */ {3, 4, 6, 7, 0, 0, 0, 0},
 /* 55 */ {3, 4, 6, 7, 8, 0, 0, 0},
 /* 56 */ {3, 4, 5, 0, 0, 0, 0, 0},
 /* 57 */ {3, 4, 5, 8, 0, 0, 0, 0},
 /* 58 */ {3, 4, 5, 7, 0, 0, 0, 0},
 /* 59 */ {3, 4, 5, 7, 8, 0, 0, 0},
 /* 60 */ {3, 4, 5, 6, 0, 0, 0, 0},
 /* 61 */ {3, 4, 5, 6, 8, 0, 0, 0},
 /* 62 */ {3, 4, 5, 6, 7, 0, 0, 0},
 /* 63 */ {3, 4, 5, 6, 7, 8, 0, 0},
 /* 64 */ {2, 0, 0, 0, 0, 0, 0, 0},
 /* 65 */ {2, 8, 0, 0, 0, 0, 0, 0},
 /* 66 */ {2, 7, 0, 0, 0, 0, 0, 0},
 /* 67 */ {2, 7, 8, 0, 0, 0, 0, 0},
 /* 68 */ {2, 6, 0, 0, 0, 0, 0, 0},
 /* 69 */ {2, 6, 8, 0, 0, 0, 0, 0},
 /* 70 */ {2, 6, 7, 0, 0, 0, 0, 0},
 /* 71 */ {2, 6, 7, 8, 0, 0, 0, 0},
 /* 72 */ {2, 5, 0, 0, 0, 0, 0, 0},
 /* 73 */ {2, 5, 8, 0, 0, 0, 0, 0},
 /* 74 */ {2, 5, 7, 0, 0, 0, 0, 0},
 /* 75 */ {2, 5, 7, 8, 0, 0, 0, 0},
 /* 76 */ {2, 5, 6, 0, 0, 0, 0, 0},
 /* 77 */ {2, 5, 6, 8, 0, 0, 0, 0},
 /* 78 */ {2, 5, 6, 7, 0, 0, 0, 0},
 /* 79 */ {2, 5, 6, 7, 8, 0, 0, 0},
 /* 80 */ {2, 4, 0, 0, 0, 0, 0, 0},
 /* 81 */ {2, 4, 8, 0, 0, 0, 0, 0},
 /* 82 */ {2, 4, 7, 0, 0, 0, 0, 0},
 /* 83 */ {2, 4, 7, 8, 0, 0, 0, 0},
 /* 84 */ {2, 4, 6, 0, 0, 0, 0, 0},
 /* 85 */ {2, 4, 6, 8, 0, 0, 0, 0},
 /* 86 */ {2, 4, 6, 7, 0, 0, 0, 0},
 /* 87 */ {2, 4, 6, 7, 8, 0, 0, 0},
 /* 88 */ {2, 4, 5, 0, 0, 0, 0, 0},
 /* 89 */ {2, 4, 5, 8, 0, 0, 0, 0},
 /* 90 */ {2, 4, 5, 7, 0, 0, 0, 0},
 /* 91 */ {2, 4, 5, 7, 8, 0, 0, 0},
 /* 92 */ {2, 4, 5, 6, 0, 0, 0, 0},
 /* 93 */ {2, 4, 5, 6, 8, 0, 0, 0},
 /* 94 */ {2, 4, 5, 6, 7, 0, 0, 0},
 /* 95 */ {2, 4, 5, 6, 7, 8, 0, 0},
 /* 96 */ {2, 3, 0, 0, 0, 0, 0, 0},
 /* 97 */ {2, 3, 8, 0, 0, 0, 0, 0},
 /* 98 */ {2, 3, 7, 0, 0, 0, 0, 0},
 /* 99 */ {2, 3, 7, 8, 0, 0, 0, 0},
 /* 100 */ {2, 3, 6, 0, 0, 0, 0, 0},
 /* 101 */ {2, 3, 6, 8, 0, 0, 0, 0},
 /* 102 */ {2, 3, 6, 7, 0, 0, 0, 0},
 /* 103 */ {2, 3, 6, 7, 8, 0, 0, 0},
 /* 104 */ {2, 3, 5, 0, 0, 0, 0, 0},
 /* 105 */ {2, 3, 5, 8, 0, 0, 0, 0},
 /* 106 */ {2, 3, 5, 7, 0, 0, 0, 0},
 /* 107 */ {2, 3, 5, 7, 8, 0, 0, 0},
 /* 108 */ {2, 3, 5, 6, 0, 0, 0, 0},
 /* 109 */ {2, 3, 5, 6, 8, 0, 0, 0},
 /* 110 */ {2, 3, 5, 6, 7, 0, 0, 0},
 /* 111 */ {2, 3, 5, 6, 7, 8, 0, 0},
 /* 112 */ {2, 3, 4, 0, 0, 0, 0, 0},
 /* 113 */ {2, 3, 4, 8, 0, 0, 0, 0},
 /* 114 */ {2, 3, 4, 7, 0, 0, 0, 0},
 /* 115 */ {2, 3, 4, 7, 8, 0, 0, 0},
 /* 116 */ {2, 3, 4, 6, 0, 0, 0, 0},
 /* 117 */ {2, 3, 4, 6, 8, 0, 0, 0},
 /* 118 */ {2, 3, 4, 6, 7, 0, 0, 0},
 /* 119 */ {2, 3, 4, 6, 7, 8, 0, 0},
 /* 120 */ {2, 3, 4, 5, 0, 0, 0, 0},
 /* 121 */ {2, 3, 4, 5, 8, 0, 0, 0},
 /* 122 */ {2, 3, 4, 5, 7, 0, 0, 0},
 /* 123 */ {2, 3, 4, 5, 7, 8, 0, 0},
 /* 124 */ {2, 3, 4, 5, 6, 0, 0, 0},
 /* 125 */ {2, 3, 4, 5, 6, 8, 0, 0},
 /* 126 */ {2, 3, 4, 5, 6, 7, 0, 0},
 /* 127 */ {2, 3, 4, 5, 6, 7, 8, 0},
 /* 128 */ {1, 0, 0, 0, 0, 0, 0, 0},
 /* 129 */ {1, 8, 0, 0, 0, 0, 0, 0},
 /* 130 */ {1, 7, 0, 0, 0, 0, 0, 0},
 /* 131 */ {1, 7, 8, 0, 0, 0, 0, 0},
 /* 132 */ {1, 6, 0, 0, 0, 0, 0, 0},
 /* 133 */ {1, 6, 8, 0, 0, 0, 0, 0},
 /* 134 */ {1, 6, 7, 0, 0, 0, 0, 0},
 /* 135 */ {1, 6, 7, 8, 0, 0, 0, 0},
 /* 136 */ {1, 5, 0, 0, 0, 0, 0, 0},
 /* 137 */ {1, 5, 8, 0, 0, 0, 0, 0},
 /* 138 */ {1, 5, 7, 0, 0, 0, 0, 0},
 /* 139 */ {1, 5, 7, 8, 0, 0, 0, 0},
 /* 140 */ {1, 5, 6, 0, 0, 0, 0, 0},
 /* 141 */ {1, 5, 6, 8, 0, 0, 0, 0},
 /* 142 */ {1, 5, 6, 7, 0, 0, 0, 0},
 /* 143 */ {1, 5, 6, 7, 8, 0, 0, 0},
 /* 144 */ {1, 4, 0, 0, 0, 0, 0, 0},
 /* 145 */ {1, 4, 8, 0, 0, 0, 0, 0},
 /* 146 */ {1, 4, 7, 0, 0, 0, 0, 0},
 /* 147 */ {1, 4, 7, 8, 0, 0, 0, 0},
 /* 148 */ {1, 4, 6, 0, 0, 0, 0, 0},
 /* 149 */ {1, 4, 6, 8, 0, 0, 0, 0},
 /* 150 */ {1, 4, 6, 7, 0, 0, 0, 0},
 /* 151 */ {1, 4, 6, 7, 8, 0, 0, 0},
 /* 152 */ {1, 4, 5, 0, 0, 0, 0, 0},
 /* 153 */ {1, 4, 5, 8, 0, 0, 0, 0},
 /* 154 */ {1, 4, 5, 7, 0, 0, 0, 0},
 /* 155 */ {1, 4, 5, 7, 8, 0, 0, 0},
 /* 156 */ {1, 4, 5, 6, 0, 0, 0, 0},
 /* 157 */ {1, 4, 5, 6, 8, 0, 0, 0},
 /* 158 */ {1, 4, 5, 6, 7, 0, 0, 0},
 /* 159 */ {1, 4, 5, 6, 7, 8, 0, 0},
 /* 160 */ {1, 3, 0, 0, 0, 0, 0, 0},
 /* 161 */ {1, 3, 8, 0, 0, 0, 0, 0},
 /* 162 */ {1, 3, 7, 0, 0, 0, 0, 0},
 /* 163 */ {1, 3, 7, 8, 0, 0, 0, 0},
 /* 164 */ {1, 3, 6, 0, 0, 0, 0, 0},
 /* 165 */ {1, 3, 6, 8, 0, 0, 0, 0},
 /* 166 */ {1, 3, 6, 7, 0, 0, 0, 0},
 /* 167 */ {1, 3, 6, 7, 8, 0, 0, 0},
 /* 168 */ {1, 3, 5, 0, 0, 0, 0, 0},
 /* 169 */ {1, 3, 5, 8, 0, 0, 0, 0},
 /* 170 */ {1, 3, 5, 7, 0, 0, 0, 0},
 /* 171 */ {1, 3, 5, 7, 8, 0, 0, 0},
 /* 172 */ {1, 3, 5, 6, 0, 0, 0, 0},
 /* 173 */ {1, 3, 5, 6, 8, 0, 0, 0},
 /* 174 */ {1, 3, 5, 6, 7, 0, 0, 0},
 /* 175 */ {1, 3, 5, 6, 7, 8, 0, 0},
 /* 176 */ {1, 3, 4, 0, 0, 0, 0, 0},
 /* 177 */ {1, 3, 4, 8, 0, 0, 0, 0},
 /* 178 */ {1, 3, 4, 7, 0, 0, 0, 0},
 /* 179 */ {1, 3, 4, 7, 8, 0, 0, 0},
 /* 180 */ {1, 3, 4, 6, 0, 0, 0, 0},
 /* 181 */ {1, 3, 4, 6, 8, 0, 0, 0},
 /* 182 */ {1, 3, 4, 6, 7, 0, 0, 0},
 /* 183 */ {1, 3, 4, 6, 7, 8, 0, 0},
 /* 184 */ {1, 3, 4, 5, 0, 0, 0, 0},
 /* 185 */ {1, 3, 4, 5, 8, 0, 0, 0},
 /* 186 */ {1, 3, 4, 5, 7, 0, 0, 0},
 /* 187 */ {1, 3, 4, 5, 7, 8, 0, 0},
 /* 188 */ {1, 3, 4, 5, 6, 0, 0, 0},
 /* 189 */ {1, 3, 4, 5, 6, 8, 0, 0},
 /* 190 */ {1, 3, 4, 5, 6, 7, 0, 0},
 /* 191 */ {1, 3, 4, 5, 6, 7, 8, 0},
 /* 192 */ {1, 2, 0, 0, 0, 0, 0, 0},
 /* 193 */ {1, 2, 8, 0, 0, 0, 0, 0},
 /* 194 */ {1, 2, 7, 0, 0, 0, 0, 0},
 /* 195 */ {1, 2, 7, 8, 0, 0, 0, 0},
 /* 196 */ {1, 2, 6, 0, 0, 0, 0, 0},
 /* 197 */ {1, 2, 6, 8, 0, 0, 0, 0},
 /* 198 */ {1, 2, 6, 7, 0, 0, 0, 0},
 /* 199 */ {1, 2, 6, 7, 8, 0, 0, 0},
 /* 200 */ {1, 2, 5, 0, 0, 0, 0, 0},
 /* 201 */ {1, 2, 5, 8, 0, 0, 0, 0},
 /* 202 */ {1, 2, 5, 7, 0, 0, 0, 0},
 /* 203 */ {1, 2, 5, 7, 8, 0, 0, 0},
 /* 204 */ {1, 2, 5, 6, 0, 0, 0, 0},
 /* 205 */ {1, 2, 5, 6, 8, 0, 0, 0},
 /* 206 */ {1, 2, 5, 6, 7, 0, 0, 0},
 /* 207 */ {1, 2, 5, 6, 7, 8, 0, 0},
 /* 208 */ {1, 2, 4, 0, 0, 0, 0, 0},
 /* 209 */ {1, 2, 4, 8, 0, 0, 0, 0},
 /* 210 */ {1, 2, 4, 7, 0, 0, 0, 0},
 /* 211 */ {1, 2, 4, 7, 8, 0, 0, 0},
 /* 212 */ {1, 2, 4, 6, 0, 0, 0, 0},
 /* 213 */ {1, 2, 4, 6, 8, 0, 0, 0},
 /* 214 */ {1, 2, 4, 6, 7, 0, 0, 0},
 /* 215 */ {1, 2, 4, 6, 7, 8, 0, 0},
 /* 216 */ {1, 2, 4, 5, 0, 0, 0, 0},
 /* 217 */ {1, 2, 4, 5, 8, 0, 0, 0},
 /* 218 */ {1, 2, 4, 5, 7, 0, 0, 0},
 /* 219 */ {1, 2, 4, 5, 7, 8, 0, 0},
 /* 220 */ {1, 2, 4, 5, 6, 0, 0, 0},
 /* 221 */ {1, 2, 4, 5, 6, 8, 0, 0},
 /* 222 */ {1, 2, 4, 5, 6, 7, 0, 0},
 /* 223 */ {1, 2, 4, 5, 6, 7, 8, 0},
 /* 224 */ {1, 2, 3, 0, 0, 0, 0, 0},
 /* 225 */ {1, 2, 3, 8, 0, 0, 0, 0},
 /* 226 */ {1, 2, 3, 7, 0, 0, 0, 0},
 /* 227 */ {1, 2, 3, 7, 8, 0, 0, 0},
 /* 228 */ {1, 2, 3, 6, 0, 0, 0, 0},
 /* 229 */ {1, 2, 3, 6, 8, 0, 0, 0},
 /* 230 */ {1, 2, 3, 6, 7, 0, 0, 0},
 /* 231 */ {1, 2, 3, 6, 7, 8, 0, 0},
 /* 232 */ {1, 2, 3, 5, 0, 0, 0, 0},
 /* 233 */ {1, 2, 3, 5, 8, 0, 0, 0},
 /* 234 */ {1, 2, 3, 5, 7, 0, 0, 0},
 /* 235 */ {1, 2, 3, 5, 7, 8, 0, 0},
 /* 236 */ {1, 2, 3, 5, 6, 0, 0, 0},
 /* 237 */ {1, 2, 3, 5, 6, 8, 0, 0},
 /* 238 */ {1, 2, 3, 5, 6, 7, 0, 0},
 /* 239 */ {1, 2, 3, 5, 6, 7, 8, 0},
 /* 240 */ {1, 2, 3, 4, 0, 0, 0, 0},
 /* 241 */ {1, 2, 3, 4, 8, 0, 0, 0},
 /* 242 */ {1, 2, 3, 4, 7, 0, 0, 0},
 /* 243 */ {1, 2, 3, 4, 7, 8, 0, 0},
 /* 244 */ {1, 2, 3, 4, 6, 0, 0, 0},
 /* 245 */ {1, 2, 3, 4, 6, 8, 0, 0},
 /* 246 */ {1, 2, 3, 4, 6, 7, 0, 0},
 /* 247 */ {1, 2, 3, 4, 6, 7, 8, 0},
 /* 248 */ {1, 2, 3, 4, 5, 0, 0, 0},
 /* 249 */ {1, 2, 3, 4, 5, 8, 0, 0},
 /* 250 */ {1, 2, 3, 4, 5, 7, 0, 0},
 /* 251 */ {1, 2, 3, 4, 5, 7, 8, 0},
 /* 252 */ {1, 2, 3, 4, 5, 6, 0, 0},
 /* 253 */ {1, 2, 3, 4, 5, 6, 8, 0},
 /* 254 */ {1, 2, 3, 4, 5, 6, 7, 0},
 /* 255 */ {1, 2, 3, 4, 5, 6, 7, 8},
};

tFsModSizingParams gFsEcfmSizingParams [] =
{
    {"ECFM_CC_MSG_INFO_SIZE", "ECFM_CC_MSG_QUEUE_DEPTH", 
      ECFM_CC_MSG_INFO_SIZE, ECFM_CC_MSG_QUEUE_DEPTH, 
      ECFM_CC_MSG_QUEUE_DEPTH, 0},

    {"ECFM_CC_CONTEXT_INFO_SIZE", "ECFM_MAX_CONTEXTS", 
      ECFM_CC_CONTEXT_INFO_SIZE, SYS_DEF_MAX_NUM_CONTEXTS, 
      SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"ECFM_CC_VLAN_INFO_SIZE", "ECFM_MAX_VLAN_IN_SYSTEM", 
      ECFM_CC_VLAN_INFO_SIZE, ECFM_MAX_VLAN_IN_SYSTEM, 
      ECFM_MAX_VLAN, 0},

    {"ECFM_CC_PORT_INFO_SIZE", "ECFM_MAX_PORTS_IN_SYSTEM", 
      ECFM_CC_PORT_INFO_SIZE, ECFM_MAX_PORTS_IN_SYSTEM, 
      ECFM_MAX_PORTS, 0},

    {"ECFM_CC_STACK_INFO_SIZE", "ECFM_MAX_STACK", 
      ECFM_CC_STACK_INFO_SIZE, ECFM_MAX_STACK, 
      ECFM_MAX_STACK, 0},

    {"ECFM_CC_MD_INFO_SIZE", "ECFM_MAX_MD", 
      ECFM_CC_MD_INFO_SIZE, ECFM_MAX_MD, 
      ECFM_MAX_MD, 0},

    {"ECFM_CC_DEF_MD_INFO_SIZE", "ECFM_MAX_DEF_MD", 
      ECFM_CC_DEF_MD_INFO_SIZE, ECFM_MAX_DEF_MD, 
      ECFM_MAX_DEF_MD, 0},

    {"ECFM_CC_MA_INFO_SIZE", "ECFM_MAX_MA_IN_SYSTEM", 
      ECFM_CC_MA_INFO_SIZE, ECFM_MAX_MA_IN_SYSTEM, 
      ECFM_MAX_MA, 0},

    {"ECFM_CC_MEP_LIST_SIZE", "ECFM_MAX_MA_MEP_LIST_IN_SYSTEM", 
      ECFM_CC_MEP_LIST_SIZE, ECFM_MAX_MA_MEP_LIST_IN_SYSTEM, 
      ECFM_MAX_MA_MEP_LIST, 0},

    {"ECFM_CC_MEP_INFO_SIZE", "ECFM_MAX_MEP_IN_SYSTEM", 
      ECFM_CC_MEP_INFO_SIZE, ECFM_MAX_MEP_IN_SYSTEM, 
      ECFM_MAX_MEP, 0},

    {"ECFM_CC_MEP_MPTP_PARAMS_SIZE", "ECFM_MAX_MPTP_MEP_IN_SYSTEM", 
      ECFM_CC_MEP_MPTP_PARAMS_SIZE, ECFM_MAX_MPTP_MEP_IN_SYSTEM, 
      ECFM_MAX_MPTP_MEP_IN_SYSTEM, 0},

    {"ECFM_CC_MIP_INFO_SIZE", "ECFM_MAX_MIP_IN_SYSTEM", 
      ECFM_CC_MIP_INFO_SIZE, ECFM_MAX_MIP_IN_SYSTEM, 
      ECFM_MAX_MIP, 0},

    {"ECFM_CC_MIP_PREVENT_INFO_SIZE", "ECFM_MAX_MIP_IN_SYSTEM", 
      ECFM_CC_MIP_PREVENT_INFO_SIZE, ECFM_MAX_MIP_IN_SYSTEM, 
      ECFM_MAX_MIP, 0},

    {"ECFM_CC_RMEP_CCM_DB_SIZE", "ECFM_MAX_RMEP_DB",
      ECFM_CC_RMEP_CCM_DB_SIZE, ECFM_MAX_RMEP_DB,
      ECFM_MAX_RMEP_DB, 0},

    {"ECFM_CC_CONFIG_ERR_INFO_SIZE", "ECFM_MAX_CONFIG_ERR_LIST",
      ECFM_CC_CONFIG_ERR_INFO_SIZE, ECFM_MAX_CONFIG_ERR_LIST,
      ECFM_MAX_CONFIG_ERR_LIST, 0},

    {"ECFM_APP_REG_INFO_SIZE", "ECFM_MAX_APPS", 
      ECFM_APP_REG_INFO_SIZE, ECFM_MAX_APPS, 
      ECFM_MAX_APPS, 0},

    {"ECFM_MAX_PORTS_PER_CONTEXT * sizeof (UINT4)", "1", 
      ECFM_MAX_PORTS_PER_CONTEXT * sizeof (UINT4), 2, 
      2, 0},

    {"ECFM_LBLT_MSG_INFO_SIZE", "ECFM_LBLT_MSG_QUEUE_DEPTH", 
      ECFM_LBLT_MSG_INFO_SIZE, ECFM_LBLT_MSG_QUEUE_DEPTH, 
      ECFM_LBLT_MSG_QUEUE_DEPTH, 0},

    {"ECFM_LBLT_CONTEXT_INFO_SIZE", "ECFM_MAX_CONTEXTS", 
      ECFM_LBLT_CONTEXT_INFO_SIZE, SYS_DEF_MAX_NUM_CONTEXTS, 
      SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"ECFM_LBLT_PORT_INFO_SIZE", "ECFM_MAX_PORTS_IN_SYSTEM", 
      ECFM_LBLT_PORT_INFO_SIZE, ECFM_MAX_PORTS_IN_SYSTEM, 
      ECFM_MAX_PORTS, 0},

    {"ECFM_LBLT_MEP_INFO_SIZE", "ECFM_MAX_MEP_IN_SYSTEM", 
      ECFM_LBLT_MEP_INFO_SIZE, ECFM_MAX_MEP_IN_SYSTEM,
      ECFM_MAX_MEP, 0},

    {"ECFM_LBLT_MIP_INFO_SIZE", "ECFM_MAX_MIP_IN_SYSTEM", 
      ECFM_LBLT_MIP_INFO_SIZE, ECFM_MAX_MIP_IN_SYSTEM, 
      ECFM_MAX_MIP, 0},

    {"ECFM_LBLT_STACK_INFO_SIZE", "ECFM_MAX_STACK", 
      ECFM_LBLT_STACK_INFO_SIZE, ECFM_MAX_STACK, 
      ECFM_MAX_STACK, 0},

    {"ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE", "ECFM_MAX_LTM", 
      ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE, ECFM_MAX_LTM, 
      ECFM_MAX_LTM, 0},

    {"ECFM_LBLT_LBM_INFO_SIZE", "ECFM_MAX_LBM", 
      ECFM_LBLT_LBM_INFO_SIZE, ECFM_MAX_LBM, 
      ECFM_MAX_LBM, 0},

    {"ECFM_LBLT_RMEP_DB_INFO_SIZE", "ECFM_MAX_RMEP_DB",
      ECFM_LBLT_RMEP_DB_INFO_SIZE, ECFM_MAX_RMEP_DB,
      ECFM_MAX_RMEP_DB, 0},

    {"ECFM_LBLT_MA_INFO_SIZE", "ECFM_MAX_MA_IN_SYSTEM", 
      ECFM_LBLT_MA_INFO_SIZE, ECFM_MAX_MA_IN_SYSTEM, 
      ECFM_MAX_MA, 0},

    {"ECFM_LBLT_MD_INFO_SIZE", "ECFM_MAX_MD", 
      ECFM_LBLT_MD_INFO_SIZE, ECFM_MAX_MD, 
      ECFM_MAX_MD, 0},

    {"ECFM_LBLT_DELAY_Q_NODE_SIZE", "ECFM_MAX_DELAY_QUEUE", 
      ECFM_LBLT_DELAY_Q_NODE_SIZE, ECFM_MAX_DELAY_QUEUE, 
      ECFM_MAX_DELAY_QUEUE, 0},

    {"ECFM_LBLT_DEF_MD_INFO_SIZE", "ECFM_MAX_DEF_MD", 
      ECFM_LBLT_DEF_MD_INFO_SIZE, ECFM_MAX_DEF_MD, 
      ECFM_MAX_DEF_MD, 0},

    {"ECFM_APP_REG_INFO_SIZE", "ECFM_MAX_APPS", 
      ECFM_APP_REG_INFO_SIZE, ECFM_MAX_APPS,
      ECFM_MAX_APPS, 0},
 
    {"ECFM_MAX_AIS_PDU_SIZE", "ECFM_MAX_MEP_IN_SYSTEM", 
      ECFM_MAX_AIS_PDU_SIZE, ECFM_MAX_MEP_IN_SYSTEM,
      ECFM_MAX_MEP, 0},

    {"ECFM_MAX_LCK_PDU_SIZE", "ECFM_MAX_MEP_IN_SYSTEM", 
      ECFM_MAX_LCK_PDU_SIZE,  ECFM_MAX_MEP_IN_SYSTEM,
      ECFM_MAX_MEP, 0},

    {"ECFM_MPLSTP_OUTPARAMS_SIZE", "ECFM_MPLSTP_MAX_OUTPARAMS",
      ECFM_MPLSTP_OUTPARAMS_SIZE, ECFM_MPLSTP_MAX_OUTPARAMS,
      ECFM_MPLSTP_OUTPARAMS_SIZE, 0},

    {"ECFM_MPLSTP_INPARAMS_SIZE", "ECFM_MPLSTP_MAX_INPARAMS",
      ECFM_MPLSTP_INPARAMS_SIZE, ECFM_MPLSTP_MAX_INPARAMS,
      ECFM_MPLSTP_INPARAMS_SIZE, 0},

    {"ECFM_MPLSTP_PATHINFO_SIZE", "ECFM_MPLSTP_MAX_PATH_IN_SYSTEM",
      ECFM_MPLSTP_PATHINFO_SIZE, ECFM_MPLSTP_MAX_PATH_IN_SYSTEM,
      ECFM_MPLSTP_PATHINFO_SIZE, 0},

    {"ECFM_MPLSTP_SERVICE_PTR_SIZE", "ECFM_MPLSTP_MAX_SERVICE_PTRS_IN_SYSTEM",
      ECFM_MPLSTP_SERVICE_PTR_SIZE, ECFM_MPLSTP_MAX_SERVICE_PTRS_IN_SYSTEM,
      ECFM_MPLSTP_MAX_SERVICE_PTRS_IN_SYSTEM, 0},

    {"ECFM_MPTP_LBLT_PATHINFO_SIZE", "ECFM_MPLSTP_MAX_PATH_IN_SYSTEM", 
      ECFM_MPTP_LBLT_PATHINFO_SIZE, ECFM_MPLSTP_MAX_PATH_IN_SYSTEM,
      ECFM_MPTP_LBLT_PATHINFO_SIZE, 0},

     {"ECFM_MPTP_LBLT_OUTPARAMS_SIZE", "ECFM_MPLSTP_MAX_OUTPARAMS",
      ECFM_MPTP_LBLT_OUTPARAMS_SIZE, ECFM_MPLSTP_MAX_OUTPARAMS,
      ECFM_MPTP_LBLT_OUTPARAMS_SIZE, 0},

    {"ECFM_MPTP_LBLT_INPARAMS_SIZE", "ECFM_MPLSTP_MAX_INPARAMS",
      ECFM_MPTP_LBLT_INPARAMS_SIZE, ECFM_MPLSTP_MAX_INPARAMS,
      ECFM_MPTP_LBLT_INPARAMS_SIZE, 0},

    {"ECFM_MAX_CHASSISID_LEN", "ECFM_MAX_RMEP_DB",
      ECFM_MAX_CHASSISID_LEN, ECFM_MAX_RMEP_DB,
      ECFM_MAX_RMEP_DB, 0},

    {"ECFM_MAX_MGMT_DOMAIN_LEN", "ECFM_MAX_RMEP_DB",
      ECFM_MAX_MGMT_DOMAIN_LEN, ECFM_MAX_RMEP_DB,
      ECFM_MAX_RMEP_DB, 0},

    {"ECFM_MAX_MAN_ADDR_LEN", "ECFM_MAX_RMEP_DB",
      ECFM_MAX_MAN_ADDR_LEN, ECFM_MAX_RMEP_DB,
      ECFM_MAX_RMEP_DB, 0},

    {"ECFM_MAX_LBM_PDU_SIZE", "ECFM_MAX_LB_SESSIONS",
      ECFM_MAX_LBM_PDU_SIZE, ECFM_MAX_LB_SESSIONS,
      ECFM_MAX_LB_SESSIONS, 0},

    {"ECFM_LBM_DATA_TLV_LEN_MAX", "ECFM_MAX_LB_SESSIONS",
      ECFM_LBM_DATA_TLV_LEN_MAX, ECFM_MAX_LB_SESSIONS,
      ECFM_MAX_LB_SESSIONS, 0},
    {"ECFM_LTR_CACHE_STRUCT_SIZE", "ECFM_MAX_LTR_CACHE_INDEX",
      ECFM_LTR_CACHE_STRUCT_SIZE, ECFM_MAX_LTR_CACHE_INDEX,
      ECFM_MAX_LTR_CACHE_INDEX, 0},

    {"ECFM_AVLBLTY_INFO_SIZE", "ECFM_MAX_RMEP_DB",
      ECFM_AVLBLTY_INFO_SIZE, ECFM_MAX_AVLBLTY_SESSION,
      ECFM_MAX_AVLBLTY_SESSION, 0},

    {"ECFM_CC_MEP_ERR_CCM_SIZE", "ECFM_MAX_CC_MEP_ERRORS",
     ECFM_CC_MEP_ERR_CCM_SIZE, ECFM_MAX_CC_MEP_ERRORS,
     ECFM_MAX_CC_MEP_ERRORS , 0},

    {"ECFM_CC_MEP_XCON_CCM_SIZE", "ECFM_MAX_CC_MEP_XCONS",
      ECFM_CC_MEP_XCON_CCM_SIZE, ECFM_MAX_CC_MEP_XCONS,
      ECFM_MAX_CC_MEP_XCONS, 0},

    {"ECFM_LBR_RCVD_INFO_SIZE", "ECFM_MAX_LBR_RCVD_INFO",
      ECFM_LBR_RCVD_INFO_SIZE, ECFM_MAX_LBR_RCVD_INFO,
      ECFM_MAX_LBR_RCVD_INFO, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsEcfmSizingInfo;
#endif /* _CFM_GLOB_H */
/*******************************************************************************
                            End of File cfmglb.h                              
 ******************************************************************************/
