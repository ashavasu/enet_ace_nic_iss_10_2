/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdecflw.h,v 1.6 2012/02/10 10:14:51 siva Exp $
 * 
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Dot1agCfmStackTable. */
INT1
nmhValidateIndexInstanceDot1agCfmStackTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmStackTable  */

INT1
nmhGetFirstIndexDot1agCfmStackTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmStackTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmStackMdIndex ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDot1agCfmStackMaIndex ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDot1agCfmStackMepId ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDot1agCfmStackMacAddress ARG_LIST((INT4  , INT4  , INT4  , INT4 ,tMacAddr * ));

/* Proto Validate Index Instance for Dot1agCfmVlanTable. */
INT1
nmhValidateIndexInstanceDot1agCfmVlanTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmVlanTable  */

INT1
nmhGetFirstIndexDot1agCfmVlanTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmVlanTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmVlanPrimaryVid ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetDot1agCfmVlanRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmVlanPrimaryVid ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetDot1agCfmVlanRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmVlanPrimaryVid ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmVlanRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmDefaultMdDefLevel ARG_LIST((INT4 *));

INT1
nmhGetDot1agCfmDefaultMdDefMhfCreation ARG_LIST((INT4 *));

INT1
nmhGetDot1agCfmDefaultMdDefIdPermission ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmDefaultMdDefLevel ARG_LIST((INT4 ));

INT1
nmhSetDot1agCfmDefaultMdDefMhfCreation ARG_LIST((INT4 ));

INT1
nmhSetDot1agCfmDefaultMdDefIdPermission ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmDefaultMdDefLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1agCfmDefaultMdDefMhfCreation ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1agCfmDefaultMdDefIdPermission ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmDefaultMdDefLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1agCfmDefaultMdDefMhfCreation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1agCfmDefaultMdDefIdPermission ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1agCfmDefaultMdTable. */
INT1
nmhValidateIndexInstanceDot1agCfmDefaultMdTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmDefaultMdTable  */

INT1
nmhGetFirstIndexDot1agCfmDefaultMdTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmDefaultMdTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmDefaultMdStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetDot1agCfmDefaultMdLevel ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetDot1agCfmDefaultMdMhfCreation ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetDot1agCfmDefaultMdIdPermission ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmDefaultMdLevel ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetDot1agCfmDefaultMdMhfCreation ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetDot1agCfmDefaultMdIdPermission ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmDefaultMdLevel ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmDefaultMdMhfCreation ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmDefaultMdIdPermission ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmDefaultMdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for Dot1agCfmConfigErrorListTable. */
INT1
nmhValidateIndexInstanceDot1agCfmConfigErrorListTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmConfigErrorListTable  */

INT1
nmhGetFirstIndexDot1agCfmConfigErrorListTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmConfigErrorListTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmConfigErrorListErrorType ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMdTableNextIndex ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot1agCfmMdTable. */
INT1
nmhValidateIndexInstanceDot1agCfmMdTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmMdTable  */

INT1
nmhGetFirstIndexDot1agCfmMdTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmMdTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMdFormat ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMdName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMdMdLevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMdMhfCreation ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMdMhfIdPermission ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMdMaNextIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMdRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmMdFormat ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMdName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1agCfmMdMdLevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMdMhfCreation ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMdMhfIdPermission ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMdRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmMdFormat ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMdName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1agCfmMdMdLevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMdMhfCreation ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMdMhfIdPermission ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMdRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmMdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1agCfmMaNetTable. */
INT1
nmhValidateIndexInstanceDot1agCfmMaNetTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmMaNetTable  */

INT1
nmhGetFirstIndexDot1agCfmMaNetTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmMaNetTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMaNetFormat ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMaNetName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMaNetCcmInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMaNetRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmMaNetFormat ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMaNetName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1agCfmMaNetCcmInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMaNetRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmMaNetFormat ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMaNetName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1agCfmMaNetCcmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMaNetRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmMaNetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1agCfmMaCompTable. */
INT1
nmhValidateIndexInstanceDot1agCfmMaCompTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmMaCompTable  */

INT1
nmhGetFirstIndexDot1agCfmMaCompTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmMaCompTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMaCompPrimaryVlanId ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMaCompMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMaCompIdPermission ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMaCompNumberOfVids ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMaCompRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmMaCompPrimaryVlanId ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMaCompMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMaCompIdPermission ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMaCompNumberOfVids ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot1agCfmMaCompRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmMaCompPrimaryVlanId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMaCompMhfCreation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMaCompIdPermission ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMaCompNumberOfVids ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1agCfmMaCompRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmMaCompTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1agCfmMaMepListTable. */
INT1
nmhValidateIndexInstanceDot1agCfmMaMepListTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmMaMepListTable  */

INT1
nmhGetFirstIndexDot1agCfmMaMepListTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmMaMepListTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMaMepListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmMaMepListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmMaMepListRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmMaMepListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1agCfmMepTable. */
INT1
nmhValidateIndexInstanceDot1agCfmMepTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmMepTable  */

INT1
nmhGetFirstIndexDot1agCfmMepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmMepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMepIfIndex ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDirection ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepPrimaryVid ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepActive ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepFngState ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepCcmLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot1agCfmMepLowPrDef ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepFngAlarmTime ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepFngResetTime ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepHighestPrDefect ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDefects ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepErrorCcmLastFailure ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepXconCcmLastFailure ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepNextLbmTransId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepLtmNextSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot1agCfmMepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmDataTlv ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepTransmitLbmVlanPriority ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmVlanDropEnable ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmResultOK ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLbmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot1agCfmMepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmResult ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepTransmitLtmEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1agCfmMepIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepPrimaryVid ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot1agCfmMepActive ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepCcmLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot1agCfmMepLowPrDef ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepFngAlarmTime ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepFngResetTime ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetDot1agCfmMepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot1agCfmMepTransmitLbmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLbmDataTlv ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1agCfmMepTransmitLbmVlanPriority ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLbmVlanDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1agCfmMepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetDot1agCfmMepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot1agCfmMepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1agCfmMepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot1agCfmMepTransmitLtmEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1agCfmMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1agCfmMepIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepDirection ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepPrimaryVid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1agCfmMepActive ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepCciEnabled ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepCcmLtmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1agCfmMepLowPrDef ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepFngAlarmTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepFngResetTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmDataTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmVlanPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLbmVlanDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmFlags ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmTargetMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmTargetMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmTargetIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmTtl ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1agCfmMepTransmitLtmEgressIdentifier ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1agCfmMepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1agCfmMepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1agCfmLtrTable. */
INT1
nmhValidateIndexInstanceDot1agCfmLtrTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmLtrTable  */

INT1
nmhGetFirstIndexDot1agCfmLtrTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmLtrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmLtrTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmLtrForwarded ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrTerminalMep ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrLastEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmLtrNextEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmLtrRelay ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmLtrManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDot1agCfmLtrManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmLtrIngress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrIngressMac ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot1agCfmLtrIngressPortIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrIngressPortId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmLtrEgress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrEgressMac ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot1agCfmLtrEgressPortIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmLtrEgressPortId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmLtrOrganizationSpecificTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot1agCfmMepDbTable. */
INT1
nmhValidateIndexInstanceDot1agCfmMepDbTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1agCfmMepDbTable  */

INT1
nmhGetFirstIndexDot1agCfmMepDbTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1agCfmMepDbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1agCfmMepDbRMepState ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDbRMepFailedOkTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1agCfmMepDbMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot1agCfmMepDbRdi ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDbPortStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDbInterfaceStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDbChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1agCfmMepDbChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1agCfmMepDbManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDot1agCfmMepDbManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

