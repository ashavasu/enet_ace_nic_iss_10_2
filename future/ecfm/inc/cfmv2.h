/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: cfmv2.h,v 1.3 2011/12/28 14:00:53 siva Exp $
*
* Description: Protocol Mib Tanslation Table
*********************************************************************/

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */
#ifdef SNMP_3_WANTED
static struct MIB_OID orig_mib_oid_table[] = {
    {"ccitt",        "0"},
    {"iso",        "1"},
    {"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
    {"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000"},
    {"org",        "1.3"},
    {"dod",        "1.3.6"},
    {"internet",        "1.3.6.1"},
    {"directory",        "1.3.6.1.1"},
    {"mgmt",        "1.3.6.1.2"},
    {"mib-2",        "1.3.6.1.2.1"},
    {"ip",        "1.3.6.1.2.1.4"},
    {"transmission",        "1.3.6.1.2.1.10"},
    {"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
    {"rmon",        "1.3.6.1.2.1.16"},
    {"statistics",        "1.3.6.1.2.1.16.1"},
    {"history",        "1.3.6.1.2.1.16.2"},
    {"hosts",        "1.3.6.1.2.1.16.4"},
    {"matrix",        "1.3.6.1.2.1.16.6"},
    {"filter",        "1.3.6.1.2.1.16.7"},
    {"tokenRing",        "1.3.6.1.2.1.16.10"},
    {"dot1dBridge",        "1.3.6.1.2.1.17"},
    {"dot1dStp",        "1.3.6.1.2.1.17.2"},
    {"dot1dTp",        "1.3.6.1.2.1.17.4"},
    {"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
    {"dns",        "1.3.6.1.2.1.32"},
    {"experimental",        "1.3.6.1.3"},
    {"private",        "1.3.6.1.4"},
    {"enterprises",        "1.3.6.1.4.1"},
    {"ARICENT-MPLS-TP-MIB",        "1.3.6.1.4.1.2076.13.4"},
    {"issExt",        "1.3.6.1.4.1.2076.81.8"},
    {"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
    {"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
    {"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
    {"security",        "1.3.6.1.5"},
    {"snmpV2",        "1.3.6.1.6"},
    {"snmpDomains",        "1.3.6.1.6.1"},
    {"snmpProxys",        "1.3.6.1.6.2"},
    {"snmpModules",        "1.3.6.1.6.3"},
    {"snmpTraps",        "1.3.6.1.6.3.1.1.5"},
    {"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
    {"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
    {"ieee802dot1mibs",        "1.3.111.2.802.1.1"},
    {"ieee8021CfmMib",        "1.3.111.2.802.1.1.8"},
    {"ieee8021CfmV2Mib",        "1.3.111.2.802.1.1.8"},
    {"dot1agNotifications",        "1.3.111.2.802.1.1.8.0"},
    {"dot1agMIBObjects",        "1.3.111.2.802.1.1.8.1"},
    {"dot1agCfmStack",        "1.3.111.2.802.1.1.8.1.1"},
    {"dot1agCfmStackTable",        "1.3.111.2.802.1.1.8.1.1.1"},
    {"dot1agCfmStackEntry",        "1.3.111.2.802.1.1.8.1.1.1.1"},
    {"dot1agCfmStackifIndex",        "1.3.111.2.802.1.1.8.1.1.1.1.1"},
    {"dot1agCfmStackVlanIdOrNone",        "1.3.111.2.802.1.1.8.1.1.1.1.2"},
    {"dot1agCfmStackMdLevel",        "1.3.111.2.802.1.1.8.1.1.1.1.3"},
    {"dot1agCfmStackDirection",        "1.3.111.2.802.1.1.8.1.1.1.1.4"},
    {"dot1agCfmStackMdIndex",        "1.3.111.2.802.1.1.8.1.1.1.1.5"},
    {"dot1agCfmStackMaIndex",        "1.3.111.2.802.1.1.8.1.1.1.1.6"},
    {"dot1agCfmStackMepId",        "1.3.111.2.802.1.1.8.1.1.1.1.7"},
    {"dot1agCfmStackMacAddress",        "1.3.111.2.802.1.1.8.1.1.1.1.8"},
    {"ieee8021CfmStackTable",        "1.3.111.2.802.1.1.8.1.1.2"},
    {"ieee8021CfmStackEntry",        "1.3.111.2.802.1.1.8.1.1.2.1"},
    {"ieee8021CfmStackifIndex",        "1.3.111.2.802.1.1.8.1.1.2.1.1"},
    {"ieee8021CfmStackServiceSelectorType",        "1.3.111.2.802.1.1.8.1.1.2.1.2"},
    {"ieee8021CfmStackServiceSelectorOrNone",        "1.3.111.2.802.1.1.8.1.1.2.1.3"},
    {"ieee8021CfmStackMdLevel",        "1.3.111.2.802.1.1.8.1.1.2.1.4"},
    {"ieee8021CfmStackDirection",        "1.3.111.2.802.1.1.8.1.1.2.1.5"},
    {"ieee8021CfmStackMdIndex",        "1.3.111.2.802.1.1.8.1.1.2.1.6"},
    {"ieee8021CfmStackMaIndex",        "1.3.111.2.802.1.1.8.1.1.2.1.7"},
    {"ieee8021CfmStackMepId",        "1.3.111.2.802.1.1.8.1.1.2.1.8"},
    {"ieee8021CfmStackMacAddress",        "1.3.111.2.802.1.1.8.1.1.2.1.9"},
    {"dot1agCfmDefaultMd",        "1.3.111.2.802.1.1.8.1.2"},
    {"ieee8021CfmDefaultMdTable",        "1.3.111.2.802.1.1.8.1.2.5"},
    {"ieee8021CfmDefaultMdEntry",        "1.3.111.2.802.1.1.8.1.2.5.1"},
    {"ieee8021CfmDefaultMdComponentId",        "1.3.111.2.802.1.1.8.1.2.5.1.1"},
    {"ieee8021CfmDefaultMdPrimarySelectorType",        "1.3.111.2.802.1.1.8.1.2.5.1.2"},
    {"ieee8021CfmDefaultMdPrimarySelector",        "1.3.111.2.802.1.1.8.1.2.5.1.3"},
    {"ieee8021CfmDefaultMdStatus",        "1.3.111.2.802.1.1.8.1.2.5.1.4"},
    {"ieee8021CfmDefaultMdLevel",        "1.3.111.2.802.1.1.8.1.2.5.1.5"},
    {"ieee8021CfmDefaultMdMhfCreation",        "1.3.111.2.802.1.1.8.1.2.5.1.6"},
    {"ieee8021CfmDefaultMdIdPermission",        "1.3.111.2.802.1.1.8.1.2.5.1.7"},
    {"dot1agCfmDefaultMdDefLevel",        "1.3.111.2.802.1.1.8.1.2.1"},
    {"dot1agCfmDefaultMdDefMhfCreation",        "1.3.111.2.802.1.1.8.1.2.2"},
    {"dot1agCfmDefaultMdDefIdPermission",        "1.3.111.2.802.1.1.8.1.2.3"},
    {"dot1agCfmDefaultMdTable",        "1.3.111.2.802.1.1.8.1.2.4"},
    {"dot1agCfmDefaultMdEntry",        "1.3.111.2.802.1.1.8.1.2.4.1"},
    {"dot1agCfmDefaultMdComponentId",        "1.3.111.2.802.1.1.8.1.2.4.1.1"},
    {"dot1agCfmDefaultMdPrimaryVid",        "1.3.111.2.802.1.1.8.1.2.4.1.2"},
    {"dot1agCfmDefaultMdStatus",        "1.3.111.2.802.1.1.8.1.2.4.1.3"},
    {"dot1agCfmDefaultMdLevel",        "1.3.111.2.802.1.1.8.1.2.4.1.4"},
    {"dot1agCfmDefaultMdMhfCreation",        "1.3.111.2.802.1.1.8.1.2.4.1.5"},
    {"dot1agCfmDefaultMdIdPermission",        "1.3.111.2.802.1.1.8.1.2.4.1.6"},
    {"dot1agCfmVlan",        "1.3.111.2.802.1.1.8.1.3"},
    {"ieee8021CfmVlanTable",        "1.3.111.2.802.1.1.8.1.3.2"},
    {"ieee8021CfmVlanEntry",        "1.3.111.2.802.1.1.8.1.3.2.1"},
    {"ieee8021CfmVlanComponentId",        "1.3.111.2.802.1.1.8.1.3.2.1.1"},
    {"ieee8021CfmVlanSelector",        "1.3.111.2.802.1.1.8.1.3.2.1.3"},
    {"ieee8021CfmVlanPrimarySelector",        "1.3.111.2.802.1.1.8.1.3.2.1.5"},
    {"ieee8021CfmVlanRowStatus",        "1.3.111.2.802.1.1.8.1.3.2.1.6"},
    {"dot1agCfmVlanTable",        "1.3.111.2.802.1.1.8.1.3.1"},
    {"dot1agCfmVlanEntry",        "1.3.111.2.802.1.1.8.1.3.1.1"},
    {"dot1agCfmVlanComponentId",        "1.3.111.2.802.1.1.8.1.3.1.1.1"},
    {"dot1agCfmVlanVid",        "1.3.111.2.802.1.1.8.1.3.1.1.2"},
    {"dot1agCfmVlanPrimaryVid",        "1.3.111.2.802.1.1.8.1.3.1.1.3"},
    {"dot1agCfmVlanRowStatus",        "1.3.111.2.802.1.1.8.1.3.1.1.4"},
    {"dot1agCfmConfigErrorList",        "1.3.111.2.802.1.1.8.1.4"},
    {"ieee8021CfmConfigErrorListTable",        "1.3.111.2.802.1.1.8.1.4.2"},
    {"ieee8021CfmConfigErrorListEntry",        "1.3.111.2.802.1.1.8.1.4.2.1"},
    {"ieee8021CfmConfigErrorListSelectorType",        "1.3.111.2.802.1.1.8.1.4.2.1.1"},
    {"ieee8021CfmConfigErrorListSelector",        "1.3.111.2.802.1.1.8.1.4.2.1.2"},
    {"ieee8021CfmConfigErrorListIfIndex",        "1.3.111.2.802.1.1.8.1.4.2.1.3"},
    {"ieee8021CfmConfigErrorListErrorType",        "1.3.111.2.802.1.1.8.1.4.2.1.4"},
    {"dot1agCfmConfigErrorListTable",        "1.3.111.2.802.1.1.8.1.4.1"},
    {"dot1agCfmConfigErrorListEntry",        "1.3.111.2.802.1.1.8.1.4.1.1"},
    {"dot1agCfmConfigErrorListVid",        "1.3.111.2.802.1.1.8.1.4.1.1.1"},
    {"dot1agCfmConfigErrorListIfIndex",        "1.3.111.2.802.1.1.8.1.4.1.1.2"},
    {"dot1agCfmConfigErrorListErrorType",        "1.3.111.2.802.1.1.8.1.4.1.1.3"},
    {"dot1agCfmMd",        "1.3.111.2.802.1.1.8.1.5"},
    {"dot1agCfmMdTableNextIndex",        "1.3.111.2.802.1.1.8.1.5.1"},
    {"dot1agCfmMdTable",        "1.3.111.2.802.1.1.8.1.5.2"},
    {"dot1agCfmMdEntry",        "1.3.111.2.802.1.1.8.1.5.2.1"},
    {"dot1agCfmMdIndex",        "1.3.111.2.802.1.1.8.1.5.2.1.1"},
    {"dot1agCfmMdFormat",        "1.3.111.2.802.1.1.8.1.5.2.1.2"},
    {"dot1agCfmMdName",        "1.3.111.2.802.1.1.8.1.5.2.1.3"},
    {"dot1agCfmMdMdLevel",        "1.3.111.2.802.1.1.8.1.5.2.1.4"},
    {"dot1agCfmMdMhfCreation",        "1.3.111.2.802.1.1.8.1.5.2.1.5"},
    {"dot1agCfmMdMhfIdPermission",        "1.3.111.2.802.1.1.8.1.5.2.1.6"},
    {"dot1agCfmMdMaNextIndex",        "1.3.111.2.802.1.1.8.1.5.2.1.7"},
    {"dot1agCfmMdRowStatus",        "1.3.111.2.802.1.1.8.1.5.2.1.8"},
    {"dot1agCfmMa",        "1.3.111.2.802.1.1.8.1.6"},
    {"dot1agCfmMaNetTable",        "1.3.111.2.802.1.1.8.1.6.1"},
    {"dot1agCfmMaNetEntry",        "1.3.111.2.802.1.1.8.1.6.1.1"},
    {"dot1agCfmMaIndex",        "1.3.111.2.802.1.1.8.1.6.1.1.1"},
    {"dot1agCfmMaNetFormat",        "1.3.111.2.802.1.1.8.1.6.1.1.2"},
    {"dot1agCfmMaNetName",        "1.3.111.2.802.1.1.8.1.6.1.1.3"},
    {"dot1agCfmMaNetCcmInterval",        "1.3.111.2.802.1.1.8.1.6.1.1.4"},
    {"dot1agCfmMaNetRowStatus",        "1.3.111.2.802.1.1.8.1.6.1.1.5"},
    {"dot1agCfmMaCompTable",        "1.3.111.2.802.1.1.8.1.6.2"},
    {"dot1agCfmMaCompEntry",        "1.3.111.2.802.1.1.8.1.6.2.1"},
    {"ieee8021CfmMaCompTable",        "1.3.111.2.802.1.1.8.1.6.4"},
    {"ieee8021CfmMaCompEntry",        "1.3.111.2.802.1.1.8.1.6.4.1"},
    {"ieee8021CfmMaComponentId",        "1.3.111.2.802.1.1.8.1.6.4.1.1"},
    {"ieee8021CfmMaCompPrimarySelectorType",        "1.3.111.2.802.1.1.8.1.6.4.1.2"},
    {"ieee8021CfmMaCompPrimarySelectorOrNone",        "1.3.111.2.802.1.1.8.1.6.4.1.3"},
    {"ieee8021CfmMaCompMhfCreation",        "1.3.111.2.802.1.1.8.1.6.4.1.4"},
    {"ieee8021CfmMaCompIdPermission",        "1.3.111.2.802.1.1.8.1.6.4.1.5"},
    {"ieee8021CfmMaCompNumberOfVids",        "1.3.111.2.802.1.1.8.1.6.4.1.6"},
    {"ieee8021CfmMaCompRowStatus",        "1.3.111.2.802.1.1.8.1.6.4.1.7"},
    {"dot1agCfmMaComponentId",        "1.3.111.2.802.1.1.8.1.6.2.1.1"},
    {"dot1agCfmMaCompPrimaryVlanId",        "1.3.111.2.802.1.1.8.1.6.2.1.2"},
    {"dot1agCfmMaCompMhfCreation",        "1.3.111.2.802.1.1.8.1.6.2.1.3"},
    {"dot1agCfmMaCompIdPermission",        "1.3.111.2.802.1.1.8.1.6.2.1.4"},
    {"dot1agCfmMaCompNumberOfVids",        "1.3.111.2.802.1.1.8.1.6.2.1.5"},
    {"dot1agCfmMaCompRowStatus",        "1.3.111.2.802.1.1.8.1.6.2.1.6"},
    {"dot1agCfmMaMepListTable",        "1.3.111.2.802.1.1.8.1.6.3"},
    {"dot1agCfmMaMepListEntry",        "1.3.111.2.802.1.1.8.1.6.3.1"},
    {"dot1agCfmMaMepListIdentifier",        "1.3.111.2.802.1.1.8.1.6.3.1.1"},
    {"dot1agCfmMaMepListRowStatus",        "1.3.111.2.802.1.1.8.1.6.3.1.2"},
    {"dot1agCfmMep",        "1.3.111.2.802.1.1.8.1.7"},
    {"dot1agCfmMepTable",        "1.3.111.2.802.1.1.8.1.7.1"},
    {"dot1agCfmMepEntry",        "1.3.111.2.802.1.1.8.1.7.1.1"},
    {"dot1agCfmMepIdentifier",        "1.3.111.2.802.1.1.8.1.7.1.1.1"},
    {"dot1agCfmMepIfIndex",        "1.3.111.2.802.1.1.8.1.7.1.1.2"},
    {"dot1agCfmMepDirection",        "1.3.111.2.802.1.1.8.1.7.1.1.3"},
    {"dot1agCfmMepPrimaryVid",        "1.3.111.2.802.1.1.8.1.7.1.1.4"},
    {"dot1agCfmMepActive",        "1.3.111.2.802.1.1.8.1.7.1.1.5"},
    {"dot1agCfmMepFngState",        "1.3.111.2.802.1.1.8.1.7.1.1.6"},
    {"dot1agCfmMepCciEnabled",        "1.3.111.2.802.1.1.8.1.7.1.1.7"},
    {"dot1agCfmMepCcmLtmPriority",        "1.3.111.2.802.1.1.8.1.7.1.1.8"},
    {"dot1agCfmMepMacAddress",        "1.3.111.2.802.1.1.8.1.7.1.1.9"},
    {"dot1agCfmMepLowPrDef",        "1.3.111.2.802.1.1.8.1.7.1.1.10"},
    {"dot1agCfmMepFngAlarmTime",        "1.3.111.2.802.1.1.8.1.7.1.1.11"},
    {"dot1agCfmMepFngResetTime",        "1.3.111.2.802.1.1.8.1.7.1.1.12"},
    {"dot1agCfmMepHighestPrDefect",        "1.3.111.2.802.1.1.8.1.7.1.1.13"},
    {"dot1agCfmMepDefects",        "1.3.111.2.802.1.1.8.1.7.1.1.14"},
    {"dot1agCfmMepErrorCcmLastFailure",        "1.3.111.2.802.1.1.8.1.7.1.1.15"},
    {"dot1agCfmMepXconCcmLastFailure",        "1.3.111.2.802.1.1.8.1.7.1.1.16"},
    {"dot1agCfmMepCcmSequenceErrors",        "1.3.111.2.802.1.1.8.1.7.1.1.17"},
    {"dot1agCfmMepCciSentCcms",        "1.3.111.2.802.1.1.8.1.7.1.1.18"},
    {"dot1agCfmMepNextLbmTransId",        "1.3.111.2.802.1.1.8.1.7.1.1.19"},
    {"dot1agCfmMepLbrIn",        "1.3.111.2.802.1.1.8.1.7.1.1.20"},
    {"dot1agCfmMepLbrInOutOfOrder",        "1.3.111.2.802.1.1.8.1.7.1.1.21"},
    {"dot1agCfmMepLbrBadMsdu",        "1.3.111.2.802.1.1.8.1.7.1.1.22"},
    {"dot1agCfmMepLtmNextSeqNumber",        "1.3.111.2.802.1.1.8.1.7.1.1.23"},
    {"dot1agCfmMepUnexpLtrIn",        "1.3.111.2.802.1.1.8.1.7.1.1.24"},
    {"dot1agCfmMepLbrOut",        "1.3.111.2.802.1.1.8.1.7.1.1.25"},
    {"dot1agCfmMepTransmitLbmStatus",        "1.3.111.2.802.1.1.8.1.7.1.1.26"},
    {"dot1agCfmMepTransmitLbmDestMacAddress",        "1.3.111.2.802.1.1.8.1.7.1.1.27"},
    {"dot1agCfmMepTransmitLbmDestMepId",        "1.3.111.2.802.1.1.8.1.7.1.1.28"},
    {"dot1agCfmMepTransmitLbmDestIsMepId",        "1.3.111.2.802.1.1.8.1.7.1.1.29"},
    {"dot1agCfmMepTransmitLbmMessages",        "1.3.111.2.802.1.1.8.1.7.1.1.30"},
    {"dot1agCfmMepTransmitLbmDataTlv",        "1.3.111.2.802.1.1.8.1.7.1.1.31"},
    {"dot1agCfmMepTransmitLbmVlanPriority",        "1.3.111.2.802.1.1.8.1.7.1.1.32"},
    {"dot1agCfmMepTransmitLbmVlanDropEnable",        "1.3.111.2.802.1.1.8.1.7.1.1.33"},
    {"dot1agCfmMepTransmitLbmResultOK",        "1.3.111.2.802.1.1.8.1.7.1.1.34"},
    {"dot1agCfmMepTransmitLbmSeqNumber",        "1.3.111.2.802.1.1.8.1.7.1.1.35"},
    {"dot1agCfmMepTransmitLtmStatus",        "1.3.111.2.802.1.1.8.1.7.1.1.36"},
    {"dot1agCfmMepTransmitLtmFlags",        "1.3.111.2.802.1.1.8.1.7.1.1.37"},
    {"dot1agCfmMepTransmitLtmTargetMacAddress",        "1.3.111.2.802.1.1.8.1.7.1.1.38"},
    {"dot1agCfmMepTransmitLtmTargetMepId",        "1.3.111.2.802.1.1.8.1.7.1.1.39"},
    {"dot1agCfmMepTransmitLtmTargetIsMepId",        "1.3.111.2.802.1.1.8.1.7.1.1.40"},
    {"dot1agCfmMepTransmitLtmTtl",        "1.3.111.2.802.1.1.8.1.7.1.1.41"},
    {"dot1agCfmMepTransmitLtmResult",        "1.3.111.2.802.1.1.8.1.7.1.1.42"},
    {"dot1agCfmMepTransmitLtmSeqNumber",        "1.3.111.2.802.1.1.8.1.7.1.1.43"},
    {"dot1agCfmMepTransmitLtmEgressIdentifier",        "1.3.111.2.802.1.1.8.1.7.1.1.44"},
    {"dot1agCfmMepRowStatus",        "1.3.111.2.802.1.1.8.1.7.1.1.45"},
    {"dot1agCfmLtrTable",        "1.3.111.2.802.1.1.8.1.7.2"},
    {"dot1agCfmLtrEntry",        "1.3.111.2.802.1.1.8.1.7.2.1"},
    {"dot1agCfmLtrSeqNumber",        "1.3.111.2.802.1.1.8.1.7.2.1.1"},
    {"dot1agCfmLtrReceiveOrder",        "1.3.111.2.802.1.1.8.1.7.2.1.2"},
    {"dot1agCfmLtrTtl",        "1.3.111.2.802.1.1.8.1.7.2.1.3"},
    {"dot1agCfmLtrForwarded",        "1.3.111.2.802.1.1.8.1.7.2.1.4"},
    {"dot1agCfmLtrTerminalMep",        "1.3.111.2.802.1.1.8.1.7.2.1.5"},
    {"dot1agCfmLtrLastEgressIdentifier",        "1.3.111.2.802.1.1.8.1.7.2.1.6"},
    {"dot1agCfmLtrNextEgressIdentifier",        "1.3.111.2.802.1.1.8.1.7.2.1.7"},
    {"dot1agCfmLtrRelay",        "1.3.111.2.802.1.1.8.1.7.2.1.8"},
    {"dot1agCfmLtrChassisIdSubtype",        "1.3.111.2.802.1.1.8.1.7.2.1.9"},
    {"dot1agCfmLtrChassisId",        "1.3.111.2.802.1.1.8.1.7.2.1.10"},
    {"dot1agCfmLtrManAddressDomain",        "1.3.111.2.802.1.1.8.1.7.2.1.11"},
    {"dot1agCfmLtrManAddress",        "1.3.111.2.802.1.1.8.1.7.2.1.12"},
    {"dot1agCfmLtrIngress",        "1.3.111.2.802.1.1.8.1.7.2.1.13"},
    {"dot1agCfmLtrIngressMac",        "1.3.111.2.802.1.1.8.1.7.2.1.14"},
    {"dot1agCfmLtrIngressPortIdSubtype",        "1.3.111.2.802.1.1.8.1.7.2.1.15"},
    {"dot1agCfmLtrIngressPortId",        "1.3.111.2.802.1.1.8.1.7.2.1.16"},
    {"dot1agCfmLtrEgress",        "1.3.111.2.802.1.1.8.1.7.2.1.17"},
    {"dot1agCfmLtrEgressMac",        "1.3.111.2.802.1.1.8.1.7.2.1.18"},
    {"dot1agCfmLtrEgressPortIdSubtype",        "1.3.111.2.802.1.1.8.1.7.2.1.19"},
    {"dot1agCfmLtrEgressPortId",        "1.3.111.2.802.1.1.8.1.7.2.1.20"},
    {"dot1agCfmLtrOrganizationSpecificTlv",        "1.3.111.2.802.1.1.8.1.7.2.1.21"},
    {"dot1agCfmMepDbTable",        "1.3.111.2.802.1.1.8.1.7.3"},
    {"dot1agCfmMepDbEntry",        "1.3.111.2.802.1.1.8.1.7.3.1"},
    {"dot1agCfmMepDbRMepIdentifier",        "1.3.111.2.802.1.1.8.1.7.3.1.1"},
    {"dot1agCfmMepDbRMepState",        "1.3.111.2.802.1.1.8.1.7.3.1.2"},
    {"dot1agCfmMepDbRMepFailedOkTime",        "1.3.111.2.802.1.1.8.1.7.3.1.3"},
    {"dot1agCfmMepDbMacAddress",        "1.3.111.2.802.1.1.8.1.7.3.1.4"},
    {"dot1agCfmMepDbRdi",        "1.3.111.2.802.1.1.8.1.7.3.1.5"},
    {"dot1agCfmMepDbPortStatusTlv",        "1.3.111.2.802.1.1.8.1.7.3.1.6"},
    {"dot1agCfmMepDbInterfaceStatusTlv",        "1.3.111.2.802.1.1.8.1.7.3.1.7"},
    {"dot1agCfmMepDbChassisIdSubtype",        "1.3.111.2.802.1.1.8.1.7.3.1.8"},
    {"dot1agCfmMepDbChassisId",        "1.3.111.2.802.1.1.8.1.7.3.1.9"},
    {"dot1agCfmMepDbManAddressDomain",        "1.3.111.2.802.1.1.8.1.7.3.1.10"},
    {"dot1agCfmMepDbManAddress",        "1.3.111.2.802.1.1.8.1.7.3.1.11"},
    {"dot1agCfmConformance",        "1.3.111.2.802.1.1.8.2"},
    {"dot1agCfmCompliances",        "1.3.111.2.802.1.1.8.2.1"},
    {"dot1agCfmGroups",        "1.3.111.2.802.1.1.8.2.2"},
    {"joint-iso-ccitt",        "2"},
    {0,0}
};
#endif
#endif /* _SNMP_MIB_H */
