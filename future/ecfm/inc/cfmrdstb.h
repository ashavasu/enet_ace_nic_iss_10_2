/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmrdstb.h,v 1.7 2012/03/26 12:33:57 siva Exp $
 * 
 * Description: This file contains the constant definations for both 
 *              Redunancy manager for ECFM.
 *********************************************************************/

#ifndef _CFM_RSTB_H
#define _CFM_RSTB_H

#define ECFM_RM_GET_NODE_STATUS()  (gEcfmNodeStatus = ECFM_NODE_ACTIVE)

#define ECFM_RM_GET_NUM_PEERS_UP()

#define ECFM_INIT_NUM_PEERS()

#define ECFM_IS_STANDBY_UP()       ECFM_FALSE

#define ECFM_NODE_STATUS()         ECFM_NODE_ACTIVE
#define ECFM_RED_LTT_TIMER_EXPIRY      0
#define ECFM_RED_LBI_TIMER_EXPIRY      0
#define ECFM_RED_LBI_STOP_TX           0
#define ECFM_RED_TST_TIMER_EXPIRY      0
#define ECFM_RED_TST_STOP_TX           0
#define ECFM_RED_STOP_THROUGHPUT_TRANS 0
#define  ECFM_IS_NP_PROGRAMMING_ALLOWED() ECFM_TRUE
#define ECFM_HR_STDY_ST_REQ_RCVD()     OSIX_FALSE
/*****************************************************************************/
#endif /*_CFM_RSTB_H*/
/******************************************************************************
  End of File cfrdstbm.h
 *****************************************************************************/
