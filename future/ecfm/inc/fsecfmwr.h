
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsecfmwr.h,v 1.10 2011/09/24 06:53:35 siva Exp $
 * 
 * Description: This file contains prototypes for wrapper routines 
 *              for the protocols proprietary MIB Data Base.
 *********************************************************************/
#ifndef _FSECFMWR_H
#define _FSECFMWR_H

VOID RegisterFSECFM(VOID);

VOID UnRegisterFSECFM(VOID);
INT4 FsEcfmSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmOuiGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheClearGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmOuiSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheClearSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheHoldTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmOuiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheClearTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheHoldTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtrCacheSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmOuiDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmLtrCacheStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmLtrCacheClearDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmLtrCacheHoldTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmLtrCacheSizeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsEcfmPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmPortLLCEncapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxCfmPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxCcmCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLbmCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLbrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLtmCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLtrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxFailedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxCfmPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxCcmCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLbmCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLbrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLtmCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLtrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxBadCfmPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortFrwdCfmPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortDsrdCfmPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortLLCEncapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxCfmPduCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxCcmCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLbmCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLbrCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLtmCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLtrCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxFailedCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxCfmPduCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxCcmCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLbmCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLbrCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLtmCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLtrCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxBadCfmPduCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortFrwdCfmPduCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortDsrdCfmPduCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortLLCEncapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxCfmPduCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxCcmCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLbmCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLbrCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLtmCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxLtrCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTxFailedCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxCfmPduCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxCcmCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLbmCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLbrCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLtmCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxLtrCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortRxBadCfmPduCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortFrwdCfmPduCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortDsrdCfmPduCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsEcfmMipCcmDbStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbClearGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMemoryFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmBufferFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmNoDftCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRdiDftCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMacStatusDftCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRemoteCcmDftCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmErrorCcmDftCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmXconDftCountGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmCrosscheckDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipDynamicEvaluationStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbClearSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbHoldTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMemoryFailureCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmBufferFailureCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmUpCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmDownCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmNoDftCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRdiDftCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMacStatusDftCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRemoteCcmDftCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmErrorCcmDftCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmXconDftCountSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmCrosscheckDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipDynamicEvaluationStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbClearTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbHoldTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMemoryFailureCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmBufferFailureCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmUpCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmDownCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmNoDftCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRdiDftCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMacStatusDftCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRemoteCcmDftCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmErrorCcmDftCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmXconDftCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmCrosscheckDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipDynamicEvaluationStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipCcmDbStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmMipCcmDbClearDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmMipCcmDbSizeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmMipCcmDbHoldTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmMemoryFailureCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmBufferFailureCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmUpCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmDownCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmNoDftCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmRdiDftCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmMacStatusDftCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmRemoteCcmDftCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmErrorCcmDftCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmXconDftCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmCrosscheckDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEcfmMipDynamicEvaluationStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsEcfmMipTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmMipActiveGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipActiveSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipActiveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMipTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT4 GetNextIndexFsEcfmMipCcmDbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmMipCcmIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmGlobalCcmOffloadGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmGlobalCcmOffloadSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmGlobalCcmOffloadTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmGlobalCcmOffloadDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsEcfmDynMipPreventionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmDynMipPreventionRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmDynMipPreventionRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmDynMipPreventionRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmDynMipPreventionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT4 GetNextIndexFsEcfmRemoteMepDbExTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmRMepCcmSequenceNumGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepPortStatusDefectGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepInterfaceStatusDefectGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepCcmDefectGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepRDIDefectGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepRdiGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepPortStatusTlvGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepInterfaceStatusTlvGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepChassisIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDbChassisIdGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepManAddressDomainGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepManAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepPortStatusDefectSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepInterfaceStatusDefectSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepCcmDefectSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepRDIDefectSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepMacAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepRdiSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepPortStatusTlvSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepInterfaceStatusTlvSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepChassisIdSubtypeSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDbChassisIdSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepManAddressDomainSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepManAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepPortStatusDefectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepInterfaceStatusDefectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepCcmDefectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepRDIDefectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepMacAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepRdiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepPortStatusTlvTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepInterfaceStatusTlvTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepChassisIdSubtypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDbChassisIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepManAddressDomainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRMepManAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmRemoteMepDbExTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT4 GetNextIndexFsEcfmLtmTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmLtmTargetMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmLtmTtlGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsEcfmMepExTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmXconnRMepIdGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmErrorRMepIdGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectRDICcmGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectMacStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectRemoteCcmGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectErrorCcmGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectXconnCcmGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCcmOffloadGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrInGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrInOutOfOrderGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrBadMsduGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepUnexpLtrInGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrOutGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCcmSequenceErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCciSentCcmsGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmXconnRMepIdSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmErrorRMepIdSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectRDICcmSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectMacStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectRemoteCcmSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectErrorCcmSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectXconnCcmSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCcmOffloadSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrInSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrInOutOfOrderSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrBadMsduSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepUnexpLtrInSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrOutSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCcmSequenceErrorsSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCciSentCcmsSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmXconnRMepIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmErrorRMepIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectRDICcmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectMacStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectRemoteCcmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectErrorCcmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepDefectXconnCcmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCcmOffloadTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrInTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrInOutOfOrderTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrBadMsduTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepUnexpLtrInTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepLbrOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCcmSequenceErrorsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepCciSentCcmsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepExTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT4 GetNextIndexFsEcfmMdExTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmMepArchiveHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepArchiveHoldTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMepArchiveHoldTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMdExTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT4 GetNextIndexFsEcfmMaExTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEcfmMaCrosscheckStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMaCrosscheckStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmMaCrosscheckStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmMaExTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT4 FsEcfmTrapControlGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmTrapControlSet(tSnmpIndex *, tRetVal *);
INT4 FsEcfmTrapControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEcfmTrapControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

#endif /* _FSECFMWR_H */
