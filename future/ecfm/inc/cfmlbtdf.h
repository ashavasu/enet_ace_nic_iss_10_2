/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlbtdf.h,v 1.50 2016/02/18 08:32:27 siva Exp $
 * 
 * Description: This file contains the data structure for LBLT Task. 
 *********************************************************************/

#ifndef _CFM_LB_TDF_H
#define _CFM_LB_TDF_H
/****************************************************************************/
/* Node info for Md Table */
typedef struct EcfmLbLtMdInfo
{
   tEcfmRBNodeEmbd  MdTableGlobalNode; /* Embedded RBTree node for MD table 
                                        * indexed by MDIndex in GlobalInfo 
                                        * structure.
                                        */
   UINT4            u4MdIndex;         /* Index of the MD Table*/
   UINT1            u1IdPermission;    /* Indicates if anything is to be 
                                        * included in sender ID TLV transmitted 
                                        * by the MPs configured in this MD.
                                        */
   UINT1            u1Level;           /* MD level */
   UINT1            au1Pad[2];
} tEcfmLbLtMdInfo;

/****************************************************************************/
/*Node info for Ma Table*/
typedef struct EcfmLbLtMaInfo
{
   tEcfmRBNodeEmbd  MaTableGlobalNode; /* Ma Table node for MA tree in 
                                        * GlobalInfo, indexed by MdIndex, 
                                        * MaIndex
                                        */
   tEcfmLbLtMdInfo  *pMdInfo;          /* Back Pointer to the MD Table Entry
                                        * used to get the MD with which this MA
                                        * is associated.
                                        */
   UINT4            u4MdIndex;         /* MD index of the associated MD*/
   UINT4            u4MaIndex;         /* MA index of the MA*/
   UINT4            u4PrimaryVidIsid;  /* Primary VID assigned to this MA */
   UINT1            u1IdPermission;    /* Indicates if anything is to be 
                                        * included in sender ID TLV transmitted 
                                        * by the MP configured in this MA.
                                        */
   UINT1            u1SelectorType;    /* VLAN(1), ISID(2), LSP(3), PW(4) */
   UINT1            au1Pad[2];
} tEcfmLbLtMaInfo;
/****************************************************************************/
/* Node info for DefaultMdTable*/
typedef struct EcfmLbLtDefaultMdTableInfo
{
   tEcfmRBNodeEmbd  DefaultMdGlobalNode;
   UINT4            u4PrimaryVidIsid;   /* Primary VID of VLAN, used as an 
                                         * index
                                         */
   UINT1            u1IdPermission;     /* Indicating if anything is to be 
                                         * included in SenderID TLV, transmitted 
                                         * by the MHFs created by default 
                                         * maintenance domain
                                         */
   BOOL1            b1Status;           /* True if, there is a MA at same 
                                         * MdLevel associated with same vlanId 
                                         * and an upMep defined with the same MA
                                         */
   UINT1           au1Pad[2];
} tEcfmLbLtDefaultMdTableInfo;
/****************************************************************************/
/*Node info for RMepDb Table*/
typedef struct EcfmLbLtRMepDbInfo
{
   tEcfmRBNodeEmbd  MepDbGlobalNode; /* Embedded MepDbTable tree node for tree 
                                      * in Global Info
                                      */
   UINT4            u4MdIndex;       /* Md index of the MD this MEP belongs 
                                      * to.
                                      */
   UINT4            u4MaIndex;       /* Ma index of the MA with which this 
                                      * MEP is associated to.
                                      */
   UINT4            u4ExpectedLbrSeqNo;   /* Value expected in   
                                           * sequence no of next LBR
                                           * received
                                           */
   UINT2            u2MepId;         /* Mep id of this MEP*/
   UINT2            u2RMepId;        /* Remote Mep Identifier*/
   tEcfmMacAddr     RMepMacAddr;     /* Remote Mep's Mac address*/
   UINT1            au1Pad[2];
} tEcfmLbLtRMepDbInfo;
/******************************************************************************/
/* Info related to MIP CCM Database */
typedef struct EcfmLbLtMipCcmDbInfo
{
   tEcfmRBNodeEmbd  MipCcmDbGlobalNode; /* Embedded node for MIP CCM Db Table*/
   UINT2            u2Fid;              /* Filtetring identifier used as an 
                                         * index of this table
                                         */
   UINT2            u2PortNum;          /* Local Port Num */
   tEcfmMacAddr     SrcMacAddr;         /* Mac address received in CCM PDU for 
                                         * this VID
                                         */
   UINT1            au1Pad[2];
} tEcfmLbLtMipCcmDbInfo;
/****************************************************************************/
/* Contains Information for the Lbr Rx or Stop LB Transaction Command
 * in CLI */
typedef struct EcfmLbrRcvdInfo
{
   tEcfmSllNode  SllNode;
   UINT4         u4LbrSeqNumber;      /* Rcvd Lbr Sequence Number */
   UINT4         u4LbrRcvTime;        /* Time taken to rcv Lbr in ms */ 
   UINT2         u2LbrPduSize;        /* Size of rcvd Lbr Pdu */
   UINT1         u1LbrType   ;        /* Various possible LBR error types
                                         - Unexpected LBR 
                                         - Bit Error in LBR
                                         - Bad Msdu in LBR
                                         - Valid LBR
                                         - Error faced while processing LBR */
   tEcfmMacAddr  RxLbrSrcMacAddr;     /* Rcvd Lbr Source Mac Address */
   UINT1         au1Pad[3];

}tEcfmLbrRcvdInfo;
/****************************************************************************/
typedef struct EcfmLbLtCliEvInfo
{
   struct
   {
       tEcfmSll         List;
       UINT4            u4Msg1;
       UINT4            u4Msg2;
   }Msg;
   tOsixSemId           SyncSemId; /* Semaphore to have synchronizarion between 
                                    * protocol and cli thread.
                                    */
   /* Used for Ctrl+C */
   UINT4    u4ContextId;
   UINT4    u4VidIsid;
   UINT4    u4MdIndex;    /* Used for MPLS-TP based MEPs */
   UINT4    u4MaIndex;    /* Used for MPLS-TP based MEPs */
   UINT4    u4LocalMepId; /* Used for MPLS-TP based MEPs */
   UINT2    u2PortNum;
   UINT1    u1MdLevel;
   UINT1    u1Direction;
   UINT1    u1Events;     /* Various Possible event types
                                - Stop transaction
                                - PDU Received
                                - TimeOut
                                - Transaction error 
                           */
   UINT1    u1SelectorType;  /* Used to identify MPLS-TP based MEPs */
   UINT1    au1Pad[2];
}tEcfmLbLtCliEvInfo;
/****************************************************************************/
/*Dmm Pdu related required info*/
typedef struct EcfmLbLtRxDmmPduInfo
{
   tEcfmTimeRepresentation   TxTimeStampf; /* Carries the value of the 
                                            * TimeStamp inserted in the  
                                            * DMM frame
                                            */
   tEcfmTimeRepresentation   RxTimeStampf; /* Carries the value of the 
                                            * TimeStamp inserted in the  
                                            * DMM frame on reception of 
                                            * DMM by the HW
                                            */

}tEcfmLbLtRxDmmPduInfo;
/****************************************************************************/
/*TH Vsr Pdu related required info*/
typedef struct EcfmLbLtRxThVsmPduInfo
{

   UINT1           au1OUI[ECFM_OUI_FIELD_SIZE];  /*Specifies the OUI in Vsm Pdu
                                                   Rcvd  */ 
   UINT1           u1SubOpCode;                  /*Specifies the SupOpCode in 
                                                   Vsm Pdu Rcvd  */ 
   UINT1           u1ReqCode;                    /*Specifies the Request Code in 
                                                   Vsm Pdu Rcvd  */ 

   UINT1           au1Pad[3];
}tEcfmLbLtRxThVsmPduInfo;

/****************************************************************************/
/*TH Vsr Pdu related required info*/
typedef struct EcfmLbLtRxThVsrPduInfo
{

   UINT1           au1OUI[ECFM_OUI_FIELD_SIZE]; /* Specifies the OUI in Vsr 
                                                   Pdu Rcvd */ 
   UINT1           u1SubOpCode;                 /* Specifies the SupOpCode 
                                                   in Vsr Pdu Rcvd  */ 
   UINT4           u4TstIn;                     /* Specifies the Tst Counter value 
                                                   in Vsr Pdu Rcvd  */ 
   UINT1           u1RespCode;                  /* Specifies the Request Code 
                                                   in Vsr Pdu Rcvd  */ 

   UINT1           au1Pad[3];
}tEcfmLbLtRxThVsrPduInfo;
/****************************************************************************/
/*Dmr Pdu related required info*/
typedef struct EcfmLbLtRxDmrPduInfo
{
   tEcfmTimeRepresentation   TxTimeStampf; /* Carries the value copied 
                                            * from DMM PDU.
                                            */
   tEcfmTimeRepresentation   RxTimeStampf; /* Carries the value of the time 
                                            * at point of reception of DMM 
                                            * PDU.
                                            */
   tEcfmTimeRepresentation   TxTimeStampb; /* Carries the value of the time 
                                            * at point of transmission of 
                                            * DMR PDU.
                                            */
   tEcfmTimeRepresentation   RxTimeStampb; /* Carries the value of the time 
                                            * at point of reception of 
                                            * DMR PDU inserted by the HW.
                                            */
}tEcfmLbLtRxDmrPduInfo;
/****************************************************************************/
/*1Dm Pdu related required info*/
typedef struct EcfmLbLtRx1DmPduInfo
{
   tEcfmTimeRepresentation   TxTimeStampf; /* Carries the value of the 
                                            * TimeStamp inserted in the  
                                            * 1DM frame
                                            */
   tEcfmTimeRepresentation   RxTimeStampf; /* This field is reserved for 
                                            * storing the RxTimeStampf for
                                            * 1DM receiving equipment.
                                            */
}tEcfmLbLtRx1DmPduInfo;
/****************************************************************************/
/*Tst Pdu related required info*/
typedef struct EcfmLbLtRxTstPduInfo  
{
   UINT4           u4Checksum;             /* Value of the CRC 32 if received in the TST PDU 
                                            */
   UINT1           *pu1TstTlvData;         /* Pointer to store Test TLV Data
                                            */
   UINT2           u2TstTlvLength;         /* size of Test TLV that includes type length pattern type 
                                            */
   UINT1           u1TstTlvType;           /* Type of Test TLV
                                            */
   UINT1           u1TstTlvPatternType;    /* Pattern Type of Test TLV
                                            */
}tEcfmLbLtRxTstPduInfo;
/****************************************************************************/
typedef struct EcfmLbLtRxLtrPduInfo
{
   tEcfmSenderId     SenderId;          /* Sender Id*/
   tEcfmOrgSpecific  OrgSpecific;       /* Organization Specific Data*/
   UINT4             u4TransId;         /* Transaction ID in received LTR PDU*/
   struct 
   {
       UINT1          au1Last [ECFM_EGRESS_ID_LENGTH];/* Identifies Linktrace 
                                                       * intiator or Linktrace 
                                                       * responder
                                                       */
       UINT1         au1Next [ECFM_EGRESS_ID_LENGTH];/* Identifies  LinkTrace 
                                                      * Responder that can 
                                                      * forward LTM
                                                      */
   }EgressId;
   struct 
   {
       tEcfmString   PortId;            /* Ingress MP PortId*/
       UINT1         u1Action;          /* Ingress Action*/      
       UINT1         u1PortIdSubType;   /* Ingress MP PortId Sub-Type*/
       tEcfmMacAddr  MacAddr;           /* Ingress MP Mac Address*/
   }ReplyIngress;
   struct 
   {
       tEcfmString   PortId;            /* Ingress MP PortId*/
       UINT1         u1Action;          /* Ingress Action*/      
       UINT1         u1PortIdSubType;   /* Ingress MP PortId Sub-Type*/
       tEcfmMacAddr  MacAddr;           /* Ingress MP Mac Address*/
   }ReplyEgress;
   UINT1             u1ReplyTtl;        /* TTL in received LTR*/
   UINT1             u1RelayAction;     /* Relay action in received LTR (RlyHit, 
                                         * RlyFdb, RlyMpdb)
                                         */
   UINT1             au1Pad[2];
} tEcfmLbLtRxLtrPduInfo;
/*Mep Throuput Measurement Info*/
/****************************************************************************/
typedef struct EcfmLbLtThInfo
{

   tTmrBlk                ThInitDeadlineTimer; /* Timer node for Throughput
                                                   * deadline timer
                                                   */
   tTmrBlk                GetTstRxCounterTimer; /* Timer node to send the TH VSM PDU 
                                                    * to fetch the TstRx Count 
                                                   */
   tTmrBlk                ThVsrTimeoutTimer; /* Timer node for Throughput
                                                 * Vsr Timeout timer
                                                 */
   tCliHandle              CurrentCliHandle;    /* Current CLI session ID */
   DBL8                    d8MeasuredThBps;     /* Verified Throughput in bits
                                                 * per sec */
   DBL8                    d8TxThTL;            /* Lower Throughput limit in
                                                 *  bits per sec */
   DBL8                    d8TxThTU;            /* Upper Throughput limit in bits
                                                 * per sec */
   DBL8                    d8TxThTH;            /* Throughput under verification 
                                                   in bits per sec */

   UINT4                   u4TxThDeadline;     /* Specify  a  timeout, in seconds, 
                                                * before Throughput Measurement operation 
                                                * exits. Throughput Measurement operation exits, 
                                                * if configured number of messages are sent 
                                                * before timeout.
                                                */
   UINT4                   u4TxThPps;          /* Packets per seconds to be transmitted for 
                                                  ETH-TH transmission*/
   UINT4                   u4TxThBurstDeadline;     /* Specify  a  timeout, in seconds, before a 
                                                     * Throughput Burst  exits
                                                     */
   UINT4                   u4TxThInterval;     /* Inter frame gap with which 
                                                *  pkt to be transmitted */
#ifdef L2RED_WANTED
   UINT4                   u4RemainingThDeadlineTimer; /* Remaining Time Value for 
                                                        * the Throughput Deadline Timer */
#endif 
   UINT2                   u2TxThVerifiedFrameSize;  /* Verified Frame size */
   UINT2                   u2TxThDestMepId;    /* Destination MEP-ID of the MEP with which Throughput
                                                * needs to be measure
                                                */
   UINT2                   u2TxThFrameSize;    /* Frame Size of Packets to be sent while
                                                *  measuring Throughput
                                                */
   UINT2                   u2TxThMessages;   /* Number of Throughput messages 
                                              * to be transmitted.
                                              */
   UINT2                   u2TxThBurstMessages;   /* Number of Throughput messages to be transmitted.
                                                   * in a burst 
                                                   */
   UINT1                   u1ThStatus;         /* An Enumerated value set to 'start' to 
                                                * initiate TH transmission 
                                                * and 'stop' to stop TH transmission.
                                                * 'ready' and 'notReady' indicates whether 
                                                * TH can be initiated or no
                                                */
   UINT1                   u1TxThType;         /* Throughput Measurement type 1way/2way 
                                                */
   UINT1                   u1TxThBurstType;    /* Burst Type (Message/deadline) 
                                                */
   UINT1                   u1ThState;          /* State of Throughput Initiator 
                                                */
   UINT1                   u1TxThTstPatternType; /* Pattern type to be transmitted in TST-TLV 
                                                  * in TH */
   BOOL1                   b1TxThIsDestMepId;  /* Indicates Destination MP is
                                                * identified through MepId.
                                                */
   BOOL1                   b1TxThResultOk;    /* Indicates the result of ThroughPut 
                                               * transmission. */
   tEcfmMacAddr            TxThDestMacAddr;    /* Target Mac address, where the LB/TST 
                                                * is to be  transmitted
                                                */
   UINT1                   u1Pad;
}tEcfmLbLtThInfo;
/*Mep Delay Measurement Info*/
/****************************************************************************/
typedef struct EcfmLbLtDmInfo
{
   tTmrBlk                DmInitIntervalTimer;    /* Timer node for DM While timer*/   

   tTmrBlk                DmInitDeadlineTimer; /* Timer node for DM 
                                                   * deadline timer
                                                   */
   tTmrBlk                OneDmTransIntervalTimer; /* Timer node for 1DM
                                                     * transaction interval
                                                     */
   tEcfmTimeRepresentation MinFrameDelayValue; /* Minimum Frame Delay Value */
#ifdef L2RED_WANTED
   UINT4         u4RemainingDmInitDeadlineTimer; /* Remaining Time Value for 
                                                 * the DM Deadline Timer */
   UINT4         u4RemainingDmTransIntervalTimer;  /* Remaining Time Value for 
                                                   * the 1DM TransInterval Timer */
   UINT4         u4RemainingDmInitIntervalTimer;  /* Remaining Time Value for 
                                               * the DM InitInterval Timer */
#endif 

   UINT4                   u4CurrTransId; 
   /* Identifies current transaction 
    * that is being executed.
    */
   UINT4                   u4FrmDelayThreshold; 
   /* Notification is sent to 
    * Management Application if 
    * calculated frame delay 
    * exceeds configured threshold in usec. 
    */
   UINT4          u4TxDmSeqNum;       /* Sequence Number of the last 1DM/DMM frame 
                                       * tranmitted
                                       */
   UINT4          u4TxDmDeadline;     /* Specify  a  timeout, in seconds, before Delay 
                                       * Measurement operation exits.
                                       * Delay Measurement operation exits, 
                                       * if configured number of messages are sent 
                                       * before timeout.
                                       */
   UINT2          u2TxDmInterval;     /* Interval for MEP for transmitting 
                                       * 1DM or DMMs
                                       */
   UINT2          u2TxNoOfMessages;   /* Number of 1DMs or DMMs to be transmitted.*/
   UINT2          u2NoOf1DmIn;        /* Number of 1DMs receieved.*/
   UINT2          u2NoOfDmrIn;        /* Number of DMRs receieved.*/
   UINT2          u2OneDmTransInterval; /* Time interval between two
                                         * successive DM transactions
                                         */
   UINT2          u2TxDmDestMepId;    /* Destination MEP-ID of the MEP with which Frame 
                                       * Delay needs to be measure
                                       */
   UINT1          u1DmStatus;         /* An Enumerated value set to 'start' to 
                                       * initiate 1DM or DMM transmission 
                                       * and 'stop' to stop 1DM or DMM transmission.
                                       * 'ready' and 'notReady' indicates whether 
                                       * 1DM or DMM can be initiated 
                                       * or no
                                       */
   UINT1          u1TxDmType;         /* Delay Measurement operation to be 
                                       * initiated
                                       */
   UINT1          u1Rcv1DmCapability; /* Indicates that MEP is enabled to process 
                                       * 1DM frame
                                       */
   UINT1          u1TxDmmVlanPriority; /* 3 bit value to be used in the VLAN tag, if present
                                        * in the transmitted DMM frame
                                        */
   UINT1          u1Tx1DmVlanPriority; /* 3 bit value to be used in the VLAN tag, if present
                                        * in the transmitted 1DM frame
                                        */
   BOOL1          b1ResultOk;          /* Indicates the result of the operation:
                                        * - true    The Delay measurement Message 
                                        * (1DM or DMM) will be (or has been) sent.
                                        * - false   The Delay measurement Message 
                                        * (1DM  or DMM) will not be sent.
                                        */
   BOOL1          b1TxDmrOpFields;     /* Indicates if optional fields are to be 
                                        * inserted in DMR frame.
                                        */
   BOOL1          b1TxDmmDropEligible; /* Drop Eligible bit value to be used in the 
                                        * VLAN tag, if present in the transmitted 
                                        * DMM frame.
                                        */
   BOOL1          b1Tx1DmDropEligible; /* Drop Eligible bit value to be used in the 
                                        * VLAN tag, if present in the transmitted 
                                        * 1DM frame.
                                        */
   BOOL1          b1TxDmIsDestMepId;  /* Indicates Destination MP is
                                       * identified through MepId.
                                       */
   tEcfmMacAddr   TxDmDestMacAddr;    /* Target Mac address, where the 1DM or DMM 
                                       * is to be  transmitted
                                       */
}tEcfmLbLtDmInfo;

/****************************************************************************/
/* Per Context Statistics for LBLT task */
typedef struct EcfmLbLtStats
{
   UINT4    u4TxTstCount;      /* Number of TST-PDus Transmitted */ 
   UINT4    u4RxTstCount;      /* Number of TST-PDus Received    */ 
   UINT4    u4Tx1DmCount;      /* Number of 1DM-PDus Transmitted */ 
   UINT4    u4Rx1DmCount;      /* Number of 1DM-PDus Received    */ 
   UINT4    u4TxDmmCount;      /* Number of DMM-PDus Transmitted */ 
   UINT4    u4RxDmmCount;      /* Number of DMM-PDus Received    */ 
   UINT4    u4TxDmrCount;      /* Number of DMR-PDus Transmitted */ 
   UINT4    u4RxDmrCount;      /* Number of DMR-PDus Received    */ 
   UINT4    u4TxApsCount;      /* Number of APS-PDus Transmitted */ 
   UINT4    u4RxApsCount;      /* Number of APS-PDus Received    */ 
   UINT4    u4TxMccCount;      /* Number of MCC-PDus Transmitted */ 
   UINT4    u4RxMccCount;      /* Number of MCC-PDus Received    */ 
   UINT4    u4TxVsmCount;      /* Number of VSM-PDus Transmitted */ 
   UINT4    u4RxVsmCount;      /* Number of VSM-PDus Received    */ 
   UINT4    u4TxVsrCount;      /* Number of VSR-PDus Transmitted */ 
   UINT4    u4RxVsrCount;      /* Number of VSR-PDus Received    */ 
   UINT4    u4TxExmCount;      /* Number of EXM-PDus Transmitted */ 
   UINT4    u4RxExmCount;      /* Number of EXM-PDus Received    */ 
   UINT4    u4TxExrCount;      /* Number of EXR-PDus Transmitted */ 
   UINT4    u4RxExrCount;      /* Number of EXR-PDus Received    */ 
   UINT4    u1LastCfmTxFailOpcode; /* Opcode of last Transmitted CFM
                                    * PDU which was failed.
                                    */ 
   UINT4    u4TxCfmPduCount;   /* Number of CFM-PDUs Transmitted in 
                                * LBLT Task
                                */
   UINT4    u4TxLbmCount;      /* Number of LBMs Transmitted in
                                * LBLT Task
                                */
   UINT4    u4TxLbrCount;      /* Number of LBRs Transmitted in 
                                * LBLT Task
                                */
   UINT4    u4TxLtmCount;      /* Number of LTMs Transmitted in 
                                * LBLT Task
                                */
   UINT4    u4TxLtrCount;      /* Number of LTRs Transmitted in 
                                * LBLT Task
                                */
   UINT4    u4TxFailedCount;   /* Number of failed transmission in 
                                * LBLT Task
                                */
   UINT4    u4RxCfmPduCount;   /* Number of CFM-PDUs received in 
                                * LBLT Task
                                */
   UINT4    u4RxLbmCount;      /* Number of LBMs received in 
                                * LBLT Task
                                */
   UINT4    u4RxLbrCount;      /* Number of LBRs received in 
                                * LBLT Task
                                */
   UINT4    u4RxLtmCount;      /* Number of LTMs received in 
                                * LBLT Task
                                */
   UINT4    u4RxLtrCount;      /* Number of LTRs received in 
                                * LBLT Task
                                */
   UINT4    u4RxBadCfmPduCount; /* Number of bad CFM-PDUs received 
                                 * in LBLT Task
                                 */
   UINT4    u4FrwdCfmPduCount;  /* Number of CFM-PDUs forwarded */
   UINT4    u4DsrdCfmPduCount;  /* Number of CFM-PDus discarded */
}tEcfmLbLtStats;

/****************************************************************************/
/* LB related information, maintained per Mep */
typedef struct EcfmLbLtLBInfo
{
   tTmrBlk          LbiIntervalTimer;    /* Timer used to timeout the 
                                          * reception of expected LBRs
                                          */
   tTmrBlk          LbiDeadlineTimer;    /* Timer used to timeout the 
                                          * transmission of LBMs
                                          */
   tTmrBlk          LbrTimeout;          /* Timer used to timeout the 
                                          * reception of LBRs
                                          */
   tEcfmString       LbmPdu;             /* Last transmitted 
                                          * LBM PDU
                                          */
   tEcfmString       TxLbmDataTlv;       /* An arbitrary amount of data to be 
                                          * included in Data TLV
                                          */
   UINT4             u4LbrOut;           /* Number of LBR's Transmitted by the 
                                          * MEP
                                          */
   UINT4             u4NextLbmSeqNo;     /* Next sequence number to  be sent 
                                          * in LBM
                                          */
   UINT4             u4NextTransId;      /* Next Transaction-Id
                                          */
   UINT4             u4LbrIn;            /* Total no. of valid in-order LBRs 
                                          * received
                                          */
   UINT4             u4LbmOut;           /* Total no. of Lbm Transmitted by
                                          * MEP
                                          */
   UINT4             u4LbrInOutOfOrder;  /* Total no. of valid out-of-order
                                          * LBRs received
                                          */
   UINT4             u4LbrBadMsdu;       /* Total no. of LBRs received, whose 
                                          * mac_Service_data_unit did not 
                                          * match with LBM sent
                                          */
   UINT4             u4LbrBitError;      /* The Count of LBR(s) received with
                                          * bit errors (incorrect CRC). 
                                          */
   UINT4             u4ExpectedLbrSeqNo; /* Value expected in   
                                          * sequence no of next LBR
                                          * received
                                          */
   UINT4             u4TxLbmSeqNumber;   /* LBM transaction identifier*/
   UINT4             u4TxLbmDeadline;    /* The Timeout to exit from the LBM transmission
                                          * regardless of how many LBM(s) have been sent
                                          * or LBR(s) received.
                                          */
   UINT4             u4RxLbrTimeout;     /* The Timeout for the Lbr Reception time
                                          * after timeout no LBR will be accepted 
                                          * as valid PDU
                                          */
   UINT4             u4TxLbmInterval;    /* The Time Interval between transmission of
                                          * two successive LBMs 
                                          */
   UINT4             u4LbmNodeId;        /* Node Id of the specific MIP node
                                          * in MPLS-TP network
                                          */
   UINT4             u4LbmIfNum;         /* The Interface Number of the
                                          * specific MIP node in MPLS-TP
                                          * network
                                          */
   tCliHandle        CurrentCliHandle;   /* Current CLI session ID*/

#ifdef L2RED_WANTED   
   UINT4             u4RemainingLbIntervalTime; /*Remaining Interval Time of an ongoing
                                                 * LB transaction
                                                 */
   UINT4             u4RemainingLbDeadLineTime; /*Remaining Deadline Time of an ongoing
                                                 * LB transaction
                                                 */
#endif   

   UINT2             u2TxLbmPatternSize; /* The Size of the pattern to be included
                                          * in Data 0r Test TLV.   
                                          */
   UINT2             u2TxLbmMessages;    /* Number of LBM messages to be 
                                          * sent
                                          */
   UINT2             u2NoOfLbrIn;        /* Number of LBRs receieved per transaction.*/
   UINT2             u2TxDestMepId;      /* MEPID of target MEP*/
   UINT1             u1TxLbmTstPatternType; /* Pattern type to be transmitted in TST-TLV 
                                             * in LBM
                                             */
   UINT1             u1TxLbmIntervalType;/* LBM Tx Interval Type (uSec/mSec/Sec)*/
   UINT1             u1TxLbmTlvOrNone;   /* The TLV to be included or not include any
                                          * TLV 
                                          */
   BOOL1             b1TxLbmVariableBytes;/* The Varying number of data bytes to send in
                                           * the LBM. In this case, LBMs with data size of
                                           * 0 bytes, 32 bytes, 64 bytes, 128 bytes, 256 bytes,
                                           * 512 bytes, 1024 bytes, 2048 bytes, 4096 bytes,
                                           * 8192 bytes, are send one after the other. 
                                           */
   UINT1             u1TxLbmDestType;     /* Indicates the type of destination
                                           * MEPID/Unicast or Multicast MacAddress 
                                           */
   BOOL1             b1TxLbmResultOk;    /* Indicates the result of Loopback 
                                          * transmission i.e LBM 
                                          * have been or will be transmitted or 
                                          * not.
                                          */
   BOOL1             b1TxLbmDropEligible;/* Drop Eligible bit value to be used in 
                                          * VLAN tag
                                          */
   UINT1             u1TxLbmVlanPriority; /* 3-bit value to be included in Vlan 
                                          * tag. This variable also used in 
                                          * case of MPLS-TP to set the priority
                                          * for the outgoing packet
                                          */
   tEcfmMacAddr      TxLbmDestMacAddr;   /* Target Mac address field, to 
                                          * be transmitted
                                          */
   UINT1             u1RxMCastLbmCap;    /* The Capability to process the Multicast
                                          * LBM frames. 
                                          */
   UINT1             u1TxLbmMode;        /* Set to specify the Lbm Type as Busrt or 
                                          * Request-Response. 
                                          */
   UINT1             au1LbmIcc[ECFM_CARRIER_CODE_ARRAY_SIZE];
                                         /* The ICC(ITU Carrier Code)
                                          * of the MIP node in ICC based
                                          * MPLS-TP network for which
                                          * loopback connectivity
                                          * verification (ping) or
                                          * diagnostic tests are performed
                                          */
   UINT1             u1LbmTtl;           /* The time-to-live(TTL) value to be
                                          * used in the MPLS outer label
                                          * header when loopback connectivity
                                          * verification (ping) or diagnostic
                                          * tests are  performed for a MEP/MIP
                                          *  in the MPLS-TP network.
                                          */
   UINT1             u1TxLbmStatus;      /* Set to TRANSMIT to indicate that 
                                          * another set of LBMs can be 
                                          * transmitted on administrators 
                                          * command.
                                          */
   UINT1             u1LbCapability;     /* The Capability to initiate and process the LBM
                                          * frames. 
                                          */
   UINT1      au1Pad[2];
} tEcfmLbLtLBInfo;
/****************************************************************************/
/* LT related information, maintained per mep */
typedef struct EcfmLbLtLTInfo
{
   tTmrBlk           LtmTxTimer;    /* Timer(5sec) after which Transaction Id 
                                        * is incremented which can be 
                                        * transmitted in the next LTM 
                                        * transmission.
                                        */
   UINT4              u4LtmNextSeqNum; /* Next Sequence number to be transmitted 
                                        * in LTM
                                        */
   UINT4              u4UnexpLtrIn;    /* Total number of unexpected LTRs 
                                        * received
                                        */
   UINT4              u4TxLtmSeqNum;    /* Transaction Id of transmitted LTM */
#ifdef L2RED_WANTED
   UINT4              u4RemainingLtWhileTimer; /* Remaining time for LT
                                                * transaction expiry*/
#endif
   tCliHandle         CurrentCliHandle; /* Current CLI session ID*/
   UINT2              u2TxLtmTargetMepId;  /* MEPID of target MEP*/
   UINT2              u2TxLtmTtl;      /* Indicates number of hops LTM needs to 
                                        * pass, default value  is 64
                                        */
   UINT2              u2LtrTimeOut;  /* The timeout to receive LTR in response
                                      * to the LTM.
                                      */
   BOOL1              b1TxLtmDropEligible;/* The Drop eligibility bit value to be used in
                                           * VLAN tag.
                                           */
   BOOL1              b1TxLtmTargetIsMepId; /* Indicates if MEPID is used as the 
                                             * identification of target MEP. 
                                             */
   BOOL1              b1TxLtmResult;   /* Indicates LTM will/have beeb or 
                                        * will/have not ben  transmitted.
                                        */
   UINT1              u1TxLtmStatus;   /* set to TRANSMIT to indicate that 
                                        * another LTM can be transmitted
                                        */
   UINT1              u1TxLtmFlags;    /* Flag in LTM PDU to be transmitted 
                                        * contains  UseFDBOnly
                                        */
   UINT1              u1LtInitSmState; /* Current state of the Linktrace 
                                        * Initiator SM 
                                        */
   UINT1              u1TxLtmVlanPriority; /* The 3-bit value to be included in Vlan tag. */
   UINT1              au1TxLtmEgressId[ECFM_EGRESS_ID_LENGTH];/* identifies 
                                                               * Linktrace 
                                                               * Initiator that 
                                                               * is originating
                                                               * or Linktrace 
                                                               * Responder that 
                                                               * is forwarding 
                                                               * this LTM
                                                               */
   tEcfmMacAddr       TxLtmTargetMacAddr;  /* Target Mac address field to be 
                                            * transmitted in the LTM.
                                            */
   UINT1              u1Pad;
} tEcfmLbLtLTInfo;

/****************************************************************************/
/* Tst related information, maintained per mep */
typedef struct EcfmLbLtTstInfo
{
   tTmrBlk                 TstWhile;         /* Timer used to transmit TST(s) 
                                                 * after its expiration 
                                                 */
   tTmrBlk                 TstDeadLine;      /* Timer used to stop sending TST(s) 
                                                 * after its expiration regardless 
                                                 * of how many packets have been sent
                                                 */
   UINT4                    u4TstDeadLine;      /* Timeout to exit from the TST transmission 
                                                 * regardless of how many TST(s) have been sent 
                                                 */
   UINT4                    u4TstInterval;      /* Timeinterval between transmission of 
                                                 * two successive TST frames
                                                 */
#ifdef L2RED_WANTED
   UINT4                    u4RemainingTstDeadLine;      /* Remaining time for the TST transmission 
                                                 * regardless of how many TST(s) have been sent 
                                                 */
   UINT4                    u4RemainingTstInterval;      /* Remaining Timeinterval between transmission of 
                                                 * two successive TST frames
                                                 * while switchover
                                                 */
#endif

   UINT4                    u4TstSeqNumber;     /* The Sequence number to be sent in the TST frame 
                                                 */
   UINT4                    u4TstMessages;      /* The number of TST messsage to be send  
                                                 */
   UINT4                    u4TstsSent;         /* Number of TST messages sent 
                                                 */
   UINT4                    u4BitErroredTstIn;  /* Count of TST(s) with bit errors (incorrect CRC)
                                                 */
   UINT4                    u4ValidTstIn;       /* Number of correct TST message received 
                                                 */
   tCliHandle               CurrentCliHandle;   /* Current CLI session ID*/
   UINT2                    u2TstDestMepId;     /* Destination Mepid of the TST message 
                                                 */
   UINT2                    u2TstPatternSize;   /* Size of pattern to be included in TEST Tlv
                                                 */
   UINT2                    u2TstLastPatternSize;   /* Size of pattern to be included in TEST Tlv
                                                     */
   UINT1                    u1TstCapability;     /* The Capability to initiate and process the TST
                                                 * frames. 
                                                 */
   UINT1                    u1MulticastTstRxCap; /* Capability to process the Multicast TST messages 
                                                  */
   UINT1                    u1TstPatternType;   /* Type of pattern to be included in Test TLV 
                                                 */
   UINT1                    u1TstVlanPriority;  /* The 3 bit vlaue to be included in the VLAN tag 
                                                 */
   UINT1                    u1TstStatus;        /* Indicates whethers TSTs can be transmitted or 
                                                 * not on administrator's command 
                                                 */
   UINT1                   u1TxTstIntervalType;  /* Tst Tx Interval Type (uSec/mSec/Sec)*/
   UINT1                   u1TxTstDestType;       /* Indicates the type of destination
                                                   * MEPID/Unicast or Multicast MacAddress 
                                                   */
   BOOL1                    b1TstDropEligible;  /* The Drop eligibility bit value to be used in VLAN tag
                                                 */ 
   BOOL1                    b1TstResultOk;      /* Result of TST transmission ie TST have been or will be
                                                 * transmitted or not
                                                 */
   BOOL1                    b1TstVariableBytes;  /*The varying number of data bytes to be included in the Test TLV.
                                                  * The TST messages will be sent one after the other with the
                                                  * varying size  of 64 bytes, 128 bytes, 256 bytes, 512 bytes,
                                                  * 1024 bytes, 2048 bytes, 4096 bytes 8192 bytes and 9000 bytes and repeat.
                                                  */
   tEcfmMacAddr             TstDestMacAddr;      /* Destination MAC address of the TST message 
                                                  */
   UINT1                    au1Pad[2];            /* Padding to keep the structure byte aligned */
} tEcfmLbLtTstInfo;

/****************************************************************************/
/*Port related information*/
typedef struct EcfmLbLtPortInfo
{

   tEcfmRBTree      StackInfoTree;  /* RBTree for tEcfmLbLtStackInfo ndexed 
                                     * by MDLevel, VID and direction
                                     */
#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
   tEcfmRBNodeEmbd  LocalPortTableNode; /* RB Node for port table*/
#endif
   UINT4            u4IfIndex;    /* Physical Interface index of the Port*/
   UINT4            u4ContextId;  /* Context Identifier*/
   UINT4            u4PhyPortNum; /* Physical port no. in case this is SISP
         logical interface.
         */
   UINT4            u4MaxBps;     /* Maximum Bps value supported by interface*/
   UINT4            u4TxCfmPduCount;    /* Number of CFM-PDU tranmitted on LBLT Task
                                         */
   UINT4            u4TxLbmCount;       /* Number of LBMs transmitted on
                                         * LBLT Task
                                         */
   UINT4            u4TxLbrCount;       /* Number of LBRs transmitted on 
                                         * LBLT Task
                                         */
   UINT4            u4TxLtmCount;       /* Number of LTMs transmitted on 
                                         * LBLT Task
                                         */
   UINT4            u4TxLtrCount;       /* Number of LTRs transmitted on 
                                         * LBLT Task
                                         */
   UINT4            u4TxDmmCount;       /* Number of DMMs transmitted on 
                                         * LBLT Task
                                         */
   UINT4            u4TxDmrCount;       /* Number of DMrs transmitted on 
                                         * LBLT Task
                                         */
   UINT4            u4TxFailedCount;    /* Number of failed transmissions on 
                                         * LBLT Task
                                         */
   UINT4            u4Tx1DmCount;       /* Number of 1DMs transmitted on 
                                         * LBLT Task
                                         */
   UINT4            u4RxCfmPduCount;    /* Number of CFM-PDUs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxLbmCount;       /* Number of LBMs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxLbrCount;       /* Number of LBRs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxLtmCount;       /* Number of LTMs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxLtrCount;       /* Number of LTRs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxDmmCount;       /* Number of DMMs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxDmrCount;       /* Number of DMRs received on 
                                         * LBLT Task
                                         */
   UINT4            u4Rx1DmCount;       /* Number of 1DMs received on 
                                         * LBLT Task
                                         */
   UINT4            u4RxBadCfmPduCount ;/* Number of bad CFM-PDUs received on 
                                         * LBLT Task
                                         */
   UINT4            u4FrwdCfmPduCount;/* Number of CFM-PDUs forwarded*/
   UINT4            u4DsrdCfmPduCount;/* Number of CFM-PDus discarded*/

   UINT4            u4TxApsCount;
   UINT4            u4TxMccCount;
   UINT4            u4TxExmCount;
   UINT4            u4TxExrCount;
   UINT4            u4TxVsmCount;
   UINT4            u4TxVsrCount;
   UINT4            u4RxApsCount;
   UINT4            u4RxMccCount;
   UINT4            u4RxExmCount;
   UINT4            u4RxExrCount;
   UINT4            u4RxVsmCount;
   UINT4            u4RxVsrCount;
   UINT4            u4TxTstCount;
   UINT4            u4RxTstCount;
   UINT2            u2PortNum;    /* Logoical Port Number*/
   UINT2            u2ChannelPortNum;  /* logical port number for port channel 
                                          * if applicable
                                          */
   UINT1            u1LastCfmTxFailOpcode;  /* To store the opcode value of 
                                             * last Tx CFM PDU*/
   UINT1            u1PortType;         /* CNP-s/c/portbased, PNP,PIP,CBP */ 
   UINT1            u1IfOperStatus;   /* Operational Status of the Port 
                                         * (Up/Down)
                                         */
   UINT1            u1IfType;         /* Identifies the Port Type i.e either 
                                         * Ethernet port or Aggregated Port.
                                         */
   UINT1            u1PortEcfmStatus; /* To enable/disable the ECFM module 
                                         * for 
                                         * this port only.
                                         */
   UINT1            u1PortY1731Status;  /* To enable/disable the Y.1731
                                         *  functionality for 
                                         * this port only.
                                         */
   UINT1            u1PortIdSubType;  /* Port-Indentifier type*/
   BOOL1            b1UnawareMepPresent;/* boolean to indicate whether a unware MEP is 
                                         * present on the port
                                         */
   UINT1            au1PortId[ECFM_MAX_PORTID_LEN]; /* Port-Identifier*/

   BOOL1            b1LLCEncapStatus; /* If true, then LLC SNAP header is to 
                                         * added with the PDU transmitted by the 
                                         * MPs configured on this port.
                                         */
} tEcfmLbLtPortInfo;
/****************************************************************************/
/*Node info for Mep Table related to LBLT task*/
typedef struct EcfmLbLtMepInfo 
{
   tEcfmRBNodeEmbd    MepGlobalIndexNode; /* MEP Table tree node for table in 
                                           * GlobalInfo for MIB walk
                                           */
   tEcfmRBNodeEmbd    MepPortTableNode;  
   tEcfmRBNodeEmbd    MepTableMplsNode;   /* RB-Tree Node pointer of the MPLS-TP
                                           * MEP node in the MPLS-TP MEP Table
                                           * (RB Tree based). 
                                           */
   tEcfmLbLtLBInfo    LbInfo;       /* Loopback functionality related Info*/
   tEcfmLbLtLTInfo    LtInfo;       /* Linktrace  functionality related Info*/
   tEcfmLbLtDmInfo    DmInfo;       /* Delay Measurement functionality related info*/
   tEcfmLbLtTstInfo   TstInfo;      /* Test functionality related info*/
   tEcfmLbLtThInfo    ThInfo;       /* Throughput functionality related info*/
   tEcfmCcMepInfo     *pCcMepInfo;  /* Pointer to CC MepInfo structure */
   tEcfmLbLtMaInfo    *pMaInfo;     /* Backward pointer to MA with which this
                                     * MEP is associated.
                                     */
   tEcfmLbLtPortInfo  *pPortInfo;   /* Pointer to the PortInfo on which this MEP 
                                     * is configured.
                                     */
   tEcfmMplsParams *pEcfmMplsParams; /* Pointer to the MPLS Parameters. If MEP 
                                      * is MPLS-TP based, then this pointer is
                                      * valid, otherwise this pointer is set 
                                      * to NULL. The MPLS paramter block 
                                      * pointed by this field is shared 
                                      * between CC and LBLT tasks.
                                      */
   UINT4              u4MdIndex;    /* Md Index of the associated MD with which 
                                     * this MEP belongs to.
                                     */
   UINT4              u4MaIndex;    /* Ma Index of the associated MA with which 
                                     * this MEP is associated to.
                                     */
   UINT4              u4IfIndex;    /* Physical Interface index of the Port*/
#ifdef L2RED_WANTED
   tEcfmSysTime       TimeStamp;    /* Time Stamp used for remaining Timer 
                                     * values for STANDBY Node. This is used 
                                     * by DM, LB, TST 
                                     */
#endif
   UINT4              u4PrimaryVidIsid; /* VLAN ID or ISID od the MEP */
   UINT4            u4ModId;          /* Module Id that is used to identify 
                                         the modules that registered to 
                                         indicate the SF / SF clear 
                                         conditions that the MEP is received */
   UINT2              u2MepId;      /* MEP Identifier, also an index for the Mep 
                                     * Info
                                     */
   UINT2              u2PortNum;    /* Local Port Number for the IfIndex */
   BOOL1              b1Active;     /* Identifies the MEP status. When SET, LB 
                                     * and LT related state machines of this MEP 
                                     * are intialised; on RESET MEP stops 
                                     * functioning (Reset all the state machines 
                                     * of this MEP).
                                     */
   UINT1              u1MdLevel;
   UINT1              u1CcmLtmPriority; /* Priority parameter for CCM and LTM, 
                                         * (default is 7 - * highest priority).
                                         */
   UINT1              u1Direction;      /* Mep Direction*/
   UINT1              u1RowStatus;      /* Row Status for the Mep. */
   BOOL1              b1LckDelayExp;    /* Denotes whether the LCK delay timer
                                         * is expired or not */
   UINT1              u1EcfmSlaParams; /* Pointer to RFC2544 and Y1564 Parameters.
                                            If the RFC2544 or Y1564 is enabled then
                                            this pointer is valid otherwise this
                                            pointer is set to NULL.
                                            The parameters block pointed is
                                            shared between CC and LBLT tasks */
   UINT1            u1Pad[1];

} tEcfmLbLtMepInfo;
/****************************************************************************/

/* Node for MIP Table*/
typedef struct EcfmLbLtMipInfo
{
   tEcfmRBNodeEmbd  MipGlobalNode;     /* Mip Table embedded node for a  tree 
                                        * in Global info indexed by IfIndex, 
                                        * VlanId, MdLevel
                                        */
   UINT4            u4MdIndex;         /* Md Indentifier */
   UINT4            u4MaIndex;         /* Ma Indentifier */
   UINT4            u4VlanIdIsid;          /* Vlan Identifier*/
   UINT2            u2PortNum;         /* Local Port Num of the IfIndex */
   BOOL1            b1Active;          /* MIP Active/In-Active*/
   UINT1            u1MdLevel;         /* MD-Level of MIP*/
   UINT1            u1RowStatus;       /* Row Status*/
   UINT1            au1Pad[3];
}tEcfmLbLtMipInfo;
/****************************************************************************/
/* Node for Stack Table, per interface index */
typedef struct EcfmLbLtStackInfo
{
   tEcfmRBNodeEmbd  StackPortInfoNode;       /* MEP Info node for tree in PortInfo
                                              * indexed by MDLevel and VID and
                                              * direction.
                                              */
   tEcfmLbLtMepInfo  *pLbLtMepInfo;          /* Pointer to MepInfo */
   tEcfmLbLtMipInfo  *pLbLtMipInfo;          /* Pointer to MipInfo */

   UINT4             u4VlanIdIsid;           /* VlanId of the MP or zero, if it 
                                              * is not attached to any vlan
                                              */
   UINT2             u2PortNum;              /* Local Port Number */ 
   UINT1             u1MdLevel;              /* Md Level at which this MP 
                                              * configured
                                              */
   UINT1             u1Direction;            /* MP Direction (UP/DOWN)*/  
} tEcfmLbLtStackInfo;
/****************************************************************************/
/*Ltm Pdu related required info*/
typedef struct EcfmLbLtRxLtmPduInfo
{
   tEcfmSenderId     SenderId;         /* Sender Id*/
   tEcfmOrgSpecific  OrgSpecific;      /* Organization Specific Data*/
   tEcfmLbLtStackInfo *pEgressStackInfo; 
   UINT4             u4TransId;        /* Transaction ID received in LTM PDU */
   UINT2             u2LtmInPortNum;   /* Port from where the LTM is Received */
   UINT2             u2LtrOutPortNum;  /* Port from where the LTR is to be
                                        * transmitted */
   UINT1             au1EgressId[ECFM_EGRESS_ID_LENGTH];  
                     /* Egress Identifier
                      * from the received LTM PDU
                      */
   UINT1             u1LtrOutDirection;   /* Physical Interface index of the Port
                                           * from where the LTR is to be
                                           * transmitted
                                           */
   UINT1             u1RxLtmTtl;        /* TTL in received LTM*/
   UINT1             u1RelayAction;     /* Relay Action to be sent in LTR */
   tEcfmMacAddr      TargetMacAddr;    /* Target Mac address in received LTM */
   tEcfmMacAddr      OriginalMacAddr;  /* Original Mac Address*/
   UINT1             u1Pad;
} tEcfmLbLtRxLtmPduInfo;

/****************************************************************************/
/*Used for storing information from CFMPDU parsed by OpCode DeMux */
typedef struct EcfmLbLtPduSmInfo
{
   union
   { 
       tEcfmLbLtRxLtmPduInfo    Ltm;  /* Contains LTM PDU info (part of the 
                                       * union uPduInfo)
                                       */
       tEcfmLbLtRxLtrPduInfo    Ltr;  /* Contains LTR PDU (part of the union 
                                       * uPduInfo)info
                                       */
       tEcfmLbLtRxDmmPduInfo    Dmm;  /* Contains DMM PDU (part of the union
                                       * uPduInfo)info
                                       */
       tEcfmLbLtRxDmrPduInfo    Dmr;   /* Contains DMR PDU (part of the union
                                        * uPduInfo)info
                                        */
       tEcfmLbLtRx1DmPduInfo    OneDm; /* Contains 1DM PDU (part of union
                                        * uPduInfo) info
                                        */
       tEcfmLbLtRxTstPduInfo    Tst;   /* Contains Tst PDU (part of the union
                                        * uPduInfo)info
                                        */
       tEcfmLbLtRxThVsmPduInfo  Vsm;   /* Contains VSM PDU (part of the union
                                        * uPduInfo)info
                                        */
       tEcfmLbLtRxThVsrPduInfo  Vsr;   /* Contains VSR PDU (part of the union
                                        * uPduInfo)info
                                        */

       /* Transaction Id in the received LBR PDU*/
       UINT4                    u4LbrSeqNumber; 
   } uPduInfo;
   tEcfmLbLtStackInfo           *pStackInfo;  /* Pointer to Stack Info*/
   tEcfmLbLtMepInfo             *pMepInfo;    /* Pointer to MEP Info*/
   tEcfmLbLtMipInfo             *pMipInfo;    /* Pointer to MIP Info*/
   tEcfmLbLtPortInfo            *pPortInfo;   /* Pointer to the LbLt Port Info 
                                               * for getting port related 
                                               * information
                                               */
   tEcfmBufChainHeader          *pBuf;        /* Pointer to CRU Buffer 
                                               * containing the CFM PDU
                                               */
   tEcfmBufChainHeader          *pHeaderBuf;  /* Pointer to CRU Buffer 
                                               * containing the CFM PDU
                                               */
   tEcfmVlanTag          VlanClassificationInfo;/* Vlan Classification 
                                                 * Information of the 
                                                 * Received PDU
                                                 */
   tEcfmPbbTag       PbbClassificationInfo;
   UINT4                 u4RxVlanIdIsId;     /* ISID recieved in CFM PDU*/
   UINT4                 u4ITagOffset; /* offset from where next tag to 
                                           * be deocoded */
   UINT4                 u4PduOffset;     /* CFM PDU offset in the recvd buf*/

   UINT1                 u1NoFwd;  /*port type of incomming port*/
   UINT1                 u1RxPbbPortType;  /*port type of incomming port
                                            *CNP/PIP/CBP/PNP */
   UINT1                 u1RxDirection;/* Direction of the MP which 
                                        * received the  CFMPDU
                                        */
   UINT1                 u1RxMdLevel;  /* MD Level received in PDU*/
   UINT1                 u1RxOpcode;   /* Opcode of the received 
                                        * CFM-PDU
                                        */
   UINT1                 u1RxFlags;    /* Flags Field from the PDU*/
   UINT1                 u1RxFirstTlvOffset;  /* First TLV Offset in the 
                                               * PDU Value  varies 
                                               * according to OpCode
                                               */
   UINT1                 u1CfmPduOffset;    /* CFM Pdu offset*/
   UINT1                 u1RxVersion;       /* OpCode of the received
                                             * CFMPDU 
                                             */
   tEcfmMacAddr          RxDestMacAddr;/* Destination address received in 
                                        * PDU
                                        */
   tEcfmMacAddr          RxSrcMacAddr; /* Source address received in 
                                        * PDU
                                        */
   UINT1                 au1Pad[3];
} tEcfmLbLtPduSmInfo;
/****************************************************************************/
/* Message to be queued in the message queue*/
typedef struct  EcfmLbLtMsg
{
   tEcfmMsgType                 MsgType;    /* Message type which can be 
                                             * recieved 
                                             * by the LBLT Task
                                             * - CRU Buffer chains
                                             * - Port Indications
                                             * - SNMP Configurations
                                             */
   UINT4                        u4IfIndex;  /* Interface Index*/
   UINT4                        u4ContextId;/* Context Identifier*/
   union 
   {
       tEcfmBufChainHeader      *pEcfmPdu;  /* CFM PDU received from CFA*/
       struct
       {          
           UINT4                u4VidIsid;  /* VLAN ID of the MEP*/       
           UINT4                u4MdIndex;      /* MdIndex of the MEP */
           UINT4                u4MaIndex;      /* MaIndex of the MEP */ 
           UINT2                u2MepId;        /* MEP ID */
           UINT1                u1Direction;/* Direction of the MEP*/
           UINT1                u1MdLevel;  /* MD Level of the MEP*/
           /* In MPLSTP-OAM, MEPs is not added to the ECFM_LBLT_PORT_MEP_TABLE 
            * as the MPLSTP MEPs are not associated to port.
            * Inorder to the get the information about the MPLSTP MEPs from
            * ECFM_LBLT_MEP_TABLE, the MEP related information is added here.
            */
           UINT1                u1SelectorType; /* Selector Type */
           UINT1                au1Pad[3];
       }Mep;
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
       tEcfmReqParams           EcfmReqParams;
#endif

#ifdef L2RED_WANTED
       tEcfmRedRmFrame   RmFrame;
#endif
   }uMsg;
   UINT2                        u2PortNum;
   UINT1                        au1Pad[2];
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
#define ReqParams  uMsg.EcfmReqParams
#endif
} tEcfmLbLtMsg;
/****************************************************************************/
typedef struct EcfmLbLtDelayQueueNode
{
   tEcfmSllNode    SllNode;  /* Pointer to CRU Buffer 
                              * containing the CFM PDU
                              */
 
   tEcfmBufChainHeader   *pBuf;    /* Pointer to CRU Buffer 
                                    * containing the CFM PDU
                                    */
   tEcfmVlanTag    VlanClassificationInfo;
   tEcfmPbbTag     PbbClassificationInfo;
   UINT4           u4RxVlanIdIsId;
   UINT2           u2PortNum;
   UINT1           u1Direction;
   UINT1           u1OpCode;
   UINT2           au1Pad[2];
}tEcfmLbLtDelayQueueNode;
/****************************************************************************/
/* Information related to Delay Queue */
typedef struct EcfmLbLtDelayQueueInfo
{
   tEcfmSll          CfmPduSll;
   tTmrBlk           DelayQueueTimer; /* Delay Queue timer for CFM PDU 
                                         * Transmission i.e. after  expiry of 
                                         * this timer all the CFM PDUs in the 
                                         * queue are transmitted.
                                         */
} tEcfmLbLtDelayQueueInfo;
/****************************************************************************/
/*Ltr node information*/
typedef struct EcfmLbLtLtrInfo
{
   tEcfmDllNode      LtrTableMepDllNode; /* DLL Node maintained per  Mep per LTM 
                                          * sequence number
                                          */
   tEcfmRBNodeEmbd   LtrTableGlobalNode; /* LTR tree node for table in 
                                          * GlobalInfo
                                          */    
   tEcfmString       EgressPortId;  /* Egress port Identifier, Max length 255, 
                                     * depends on its Subtype
                                     */
   tEcfmString       IngressPortId; /* Ingress port Identifier, Max length 255
                                     */
   tEcfmSenderId     SenderId;      /* Sender ID TLV Values received in the LTR
                                     */
   tEcfmOrgSpecific  OrgSpecTlv;    /* Orgnaization Specfic TLV Values received 
                                     * in the LTR
                                     */
   UINT4             u4RcvOrder;    /* For Maintiaining the reception orders of 
                                     * the LTRs. Also an index to distinguish 
                                     * among  LTRs with same LTR Transaction Id
                                     */
   UINT4             u4MdIndex;     /* MD Index of Mep, also used as an index 
                                     * for LTR table in  global info for mibwalk
                                     */
   UINT4             u4MaIndex;     /* MA Index of Mep, used as an index for LTR 
                                     * table in global info for mibwalk
                                     */
   UINT4             u4SeqNum;      /* It identifies the LTRs to which LTM this 
                                     * LTR is a response.
                                     */
   UINT2             u2TimeTakenToRcvLtr;  /* The time taken to receive LTRs
                                            * in response to the LTM.
                                            */
   UINT2             u2MepId;       /* MepId of Mep, used as an index for LTR 
                                     * table in global info for MIB Walk
                                     */
   UINT1             au1LastEgressId[ECFM_EGRESS_ID_LENGTH];/* Linktrace 
                                                             * initiator or 
                                                             * Linktrace 
                                                             * responder 
                                                             * identifier 
                                                             * to which this 
                                                             * LTR is a 
                                                             * response
                                                             */
   UINT1             au1NextEgressId[ECFM_EGRESS_ID_LENGTH];/*Identifies 
                                                             * link trace 
                                                             * responder 
                                                             * that  
                                                             * transmitted 
                                                             * this LTR
                                                             */
   BOOL1             b1LtmForwarded;  /* Indicates whether LTM was forwarded or 
                                       * not
                                       */
   BOOL1             b1TerminalMep;   /* Indicates whether LTR transmitted by 
                                       * the MP was terminal MP or not
                                       */
   UINT1             u1LtrTtl;        /* TTL field value returned in LTR (one 
                                       * less than the value in the LTM for 
                                       * which this LTR is a response)
                                       */
   UINT1             u1RelayAction;   /* Value returned in the Relay Action 
                                       * field
                                       */
   UINT1             u1IngressAction; /* Value returned in the ngress Action 
                                       * field
                                       */
   UINT1             u1EgressAction;  /* Value returned in the Egress Action 
                                       * field
                                       */
   UINT1             u1IngressPortIdSubType; /* format of Ingress portID 
                                              * -PORT_COMPONENT 
                                              * -MAC_ADDRESS    
                                              * -NETWORK_ADDRESS
                                              * -INTERFACE_NAME
                                              * -AGENT_CIRCUIT_ID
                                              *-LOCAL
                                              */
   UINT1             u1EgressPortIdSubType;  /* format of Egress portID
                                              */
   tEcfmMacAddr      IngressMacAddr;/* Mac address returned in ingress MAC 
                                     * address field
                                     */
   tEcfmMacAddr      EgressMacAddr;   /* Mac address returned in egress MAC 
                                       * address field
                                       */
} tEcfmLbLtLtrInfo;
/****************************************************************************/
/*LTM Reply List*/
typedef struct EcfmLbLtLtmReplyListInfo
{
   tEcfmRBNodeEmbd   LtmReplyListGlobalNode; /* Embedded RBTree node of LTM
                                              * reply List
                                              */
   tEcfmDll          LtrList;                /* DLL of type tEcfmLbLtLtrInfo 
                                              * maintains the LTRs received 
                                              * based on the Sequence Number.
                                              */
   UINT4             u4MdIndex;              /* The MEG Index of MEP. Also used as an
                                                * index for LTM Reply List Table in
                                              * Global Info for MIB walk
                                              */
   UINT4             u4MaIndex;              /* The ME Index of MEP. Also used as an
                                              * index for LTM Reply List Table in Global
                                              * Info for MIB walk.
                                              */
 
   UINT4             u4LtmSeqNum;            /* Ltm Sequence number*/
   UINT2             u2LtmTtl;               /* Ttl of LTM transaction for 
                                             */ 

   UINT2             u2MepId;               /* The MepId of MEP. Also used as an index for
                                             * LTM Reply List Table in Global Info for MIB
                                             * Walk.
                                             */

   tEcfmMacAddr      TargetMacAddr;          /* Target Mac address of LTM
                                                transaction for u4LtmSeqNum
                                              */
   UINT1             au1Pad[2];
} tEcfmLbLtLtmReplyListInfo;
/****************************************************************************/
/* LBM Information for LBLT task */
typedef struct EcfmLbLtLbmInfo
{
   tEcfmRBNodeEmbd      LbmTableGlobalNode;       /* The Embedded RBTree node of
                                                   * the LBM Initiation Table
                                                   */
   tEcfmSll             LbrList;               /* The Single LL of type 
                                               * tEcfmLbLtLbrInfo maintained 
                                               * for every valid LBR received
                                               * based on the Transaction Id 
                                               * and Sequence Number.
                                               */
   tEcfmDllNode         LbmDllNextNode;       /* Points to next entry in
                                               * LbmDllList maintained
                                               * in tEcfmLbLtGlobalInfo.
                                                */
   UINT4                u4MdIndex;      /* The MEG Index of MEP. Also used as an
                                         * index for LBM Initiation table in
                                         * Global Info for MIB walk
                                         */
   UINT4                u4MaIndex;      /* The ME Index of MEP. Also used as an
                                         * index for LBM Initiation table in Global
                                         * Info for MIB walk.
                                         */
   UINT4                u4SeqNum;       /* The Sequence number of the LBM(s).
                                         * Also used as an index for LBM Initiation
                                         * table in Global Info for MIB Walk.
                                         */
   UINT4                u4TransId;      /* The Transaction Id of the LBM. Also used
                                         * as an index for LBM Initiation table in Global
                                         * Info for MIB Walk.
                                         */
   UINT4                u4BytesSent;    /* The Size of the LBM PDU transmitted.*/
   UINT4                u4UnexpLbrIn;   /* The Number of unexpected LBRs
                                           received */
   UINT4                u4NumOfResponders;  /* The Number of LB Responders per
                                               LBM */
   UINT4                u4DupLbrIn;     /* The Number of duplicate LBRs received */
   UINT4                u4NodeId;       /* Specifies the Node Id */ 
   UINT4                u4IfNum;        /* Interface Number of the MIP Node */ 
   UINT2                u2NoOfLBMSent;  /* Number of LBM sent Counter */
   UINT2                u2NoOfLBRRcvd;   /* Number of LBR received Counter */
   UINT2                u2MepId;        /* The MepId of MEP. Also used as an index for
                                         * LBM Initiation table in Global Info for MIB
                                         * Walk.
                                         */
   UINT2                u2DestMepId;       /* Target MEP ID */ 
   UINT1                au1LbmIcc[ECFM_CARRIER_CODE_ARRAY_SIZE];
                                        /* ICC of the MIP node in ICC 
                                         * based MPLS-TP network.
                                         */
   UINT1                u1Ttl;          /* Time-to-live value used in the
                                         * header in the LBM which was sent
                                         */
#ifdef L2RED_WANTED
   tEcfmMacAddr         TargetMacAddr;  /* The Destination MAC Address of the LBM.*/
   BOOL1                b1Synched;      /* TRUE- Indicates Entry is synched at
                                         * STANDBY node.
                                         */ 
#else
   tEcfmMacAddr         TargetMacAddr;  /* The Destination MAC Address of the LBM.*/
   UINT1                au1Pad[1];
#endif   
   UINT1                u1TlvStatus;    /* TLV if any transmitted in LBM*/   
   UINT1                u1DestType;     /* Indicates the destination type 
                                         * MEP/MIP/UNICAST/MULTICAST.
                                         */
   UINT1                au1Paddding[3];
}tEcfmLbLtLbmInfo;
/****************************************************************************/
/* LBR Information for LBLT task */
typedef struct EcfmLbLtLbrInfo
{
   tEcfmSllNode         LbrTableSllNode; /* The Single LL node maintained 
                                          * for every LBR received.
                                          */
   UINT4                u4RcvOrder;      /* The order in which LBR(s) are 
                                          * received in response to an initiated
                                          * LBM.
                                          */
   UINT4                u4LbrNodeId;      /* Node Id */  
   UINT4                u4LbrIfNum;       /* Interface Number of the 
                                           * MIP Node.
                                           */
   UINT4                u4LbrRcvTime;    /* The Time to receive LBR in 
                                          * response to the LBM.
                                          */
   UINT2                u2LbrDestMepId;   /* MEP which has sent the response 
                                           * for the LBM
                                           */
   UINT1                au1LbrIcc[ECFM_CARRIER_CODE_ARRAY_SIZE]; 
 
   UINT1                u1LbrDestType;    /* Indicates the destination type
                                           * MEP/MIP/UNICAST/MULTICAST.
                                           */
   tEcfmMacAddr         LbrSrcMacAddr;   /* The MAC Address of the 
                                          * responder.
                                          */
   UINT1                u1LbrErrorStatus; /* The Bit indicates that received LBR 
                                           * is with bad msdu (0) and bit
                                           * error (1).
                                           */  
   UINT1                au1Pad[3];
}tEcfmLbLtLbrInfo;
/****************************************************************************/
/* Frame Delay Buffer Information */
typedef struct EcfmLbLtFrmDelayBuff
{
   tEcfmTimeRepresentation  TxTimeStampf; /* Same as Timestamp field in  DMM or 
                                           * 1DM, also used as an index for 
                                           * FrameDelay Table
                                           */
   tEcfmTimeRepresentation  FrmDelayValue;/* Actual delay value determined 
                                           * in units of time
                                           */
   tEcfmTimeRepresentation  InterFrmDelayVariation; /* Inter Frame delay Variation */
   tEcfmTimeRepresentation  FrameDelayVariation;    /* Frame Delay Variation */
   tEcfmSysTime             MeasurementTimeStamp;  /* Time at which measurement 
                                                    * took place
                                                    */
   tEcfmRBNodeEmbd          FrmDelayBuffGlobalNode;/* Frame Delay tree node for 
                                                    * Table in Context Info
                                                    */
   UINT4                    u4MdIndex; /* MD Index of MEP, also used as an index 
                                        * for FrameDelay Buffer Table.
                                        */
   UINT4                    u4MaIndex; /* ME Index of MEP, also used as an index 
                                        * for FrameDelay BufferTable
                                        */
   UINT4                    u4TransId; /* Identifies current transaction for 
                                        * which measurement takes place.
                                        */  
   UINT4                    u4SeqNum;  /* Locally Maintained Sequence number of the 
                                        * transmitted 1DM/DMM frame
                                        */
   UINT2                    u2MepId;   /* MEP Identifier, also used as an index 
                                        * for FrameDelay Table
                                        */
   UINT2                    u2NoOfDMMSent;     /* Number of DMMs sent.*/
   UINT2                    u2NoOfDMRRcvd;     /* Number of DMMs received.*/
   UINT2                    u2NoOf1DMRcvd;     /* Number of 1DMs received.*/
   UINT1                    u1DmType;  /* Type of Delay measurement
                                        * One-way (1) or Two-way (2)
                                        */
#ifdef L2RED_WANTED
   BOOL1                    b1Synched;  /* TRUE - indicates Entry is synched at
                                        * STANDBY node
                                        */
   tEcfmMacAddr             PeerMepMacAddress; /* MacAddress of source MEP in 
                                                * case of 1DM or MacAddress of 
                                                * target MEP in case of DMM 
                                                */
#else
   tEcfmMacAddr             PeerMepMacAddress; /* MacAddress of source MEP in 
                                                * case of 1DM or MacAddress of 
                                                * target MEP in case of DMM 
                                                */
   UINT1                    u1pad; 
#endif
}tEcfmLbLtFrmDelayBuff;

/****************************************************************************/
/********* VLAN Information containing Primary VLAN to secondary ************
 *                               VLAN mapping                               */
typedef struct  EcfmLbLtVlanInfo
{
   tEcfmRBNodeEmbd  VlanGlobalNode;       /* Embedded node for VlanTable in 
                                           * GlobalInfo
                                           */
   tEcfmRBNodeEmbd  PrimaryVlanGlobalNode;
   UINT4            u4VidIsid;            /* Vlan ID as Index of VlanTable */
   UINT4            u4PrimaryVidIsid;     /* PrimaryVid associated with the 
        * VLAN ID
                                           */ 
   UINT1            u1RowStatus;          /* Row status*/
   UINT1            au1Pad[3];
} tEcfmLbLtVlanInfo;

/****************************************************************************/
/* Context Specific Information for LBLT task */
typedef struct EcfmLbLtContextInfo
{
   tEcfmRBTree          MepTableIndex;  /* RBTree for tEcfmLbLtMepInfo indexed 
                                         * by MdIndex, MaIndex, MepId
                                         */
   tEcfmRBTree          MepPortTable;  /* RBTree for tEcfmLbLtMepInfo indexed 
                                        * by PortNum, Mdlevel, VlanId and
                                        * Direction
                                        */
   tEcfmRBTree          MipTable;        /* RBTree for tEcfmLbLtMipInfo indexed 
                                          * by MDLevel and VID
                                          */
   tEcfmRBTree          LtrTable;       /* RBTree for tEcfmLbLtLtrInfo indexed 
                                         * by  MdIndex, MaIndex, MepId, 
                                         * LtrSeqNum, ReceiveOrder
                                         */
 
   tEcfmRBTree          LtmReplyList;  /* The RBTree maintained for the
                                         * LTM reply List indexed by
                                         * Meg Index, Me Index, MepId, LtmSeqNum.
                                         */
 
   tEcfmRBTree          LbmTable;       /* The RBTree maintained for the
                                         * LBM Initiation Table indexed by
                                         * Meg Index, Me Index, MepId, Trans Id,
                                         * Lbr SeqNum. 
                                         */
   tEcfmRBTree          RMepDbTable;    /* Remote MEP Database Table*/
   tEcfmRBTree          MipCcmDbTable;  /* MIP CCM Database Table*/
   tEcfmRBTree          FrmDelayBuffer; /* RBTree for Frame Delay rolling buffer 
                                         * indexed by MegIndex, MeIndex,MepId, 
                                         * TimeStamp
                                         */
   tEcfmRBTree          MaTable;        /* MA Table*/
   tEcfmRBTree          MdTable;        /* MD Table*/
   tEcfmRBTree          DefaultMdTable; /* Default MD Table */
   tEcfmRBTree          VlanTable;      /* Vlan Table */
   tEcfmRBTree          PrimaryVlanTable; /* Primary Vlan Table */
   tEcfmRBTree          MplsPathTree;   /* RB-Tree based MPLS-TP MEP table 
                                         * containing MEPs indexed by 4 tuple 
                                         * for LSP and VC-ID for PW.
                                         */
   tMemPoolId           LtrTableMemPool;   /* Memory pool for assigning memory 
                                            * to LTR table nodes 
                                            */
   tMemPoolId           LbrTableMemPool;   /* Memory pool for assigning memory 
                                            * to LBR table nodes 
                                            */
   tMemPoolId           FrmDelayBuffMemPool;  /* Memory pool for assigning memory 
                                               * to Frame Delay Buffer nodes 
                                               */
   tMemPoolId           MipCcmDbMemPool;     /* Memory pool for various 
                                              * MIP CCM DB nodes
                                              */    
#ifndef ECFM_ARRAY_TO_RBTREE_WANTED
  tEcfmLbLtPortInfo    *apPortInfo[ECFM_PORTS_PER_CONTEXT_MAX]; /* Array of pointers to 
                                                                  * PortInfo to store all 
                                                                  * the  port specific 
                                                                  * information required for 
                                                                  * the protocol functions
                                                                  */
#endif
   tEcfmLbLtDelayQueueInfo   DelayQueueInfo;            /* Delay Queue related 
                                                         * information
                                                         */
   tTmrBlk               LtrHoldTimer;  /* LTRHold timer for LTR Transmission 
                                         */
   tTmrBlk              LbrHoldTimer;  /* LBR Hold timer till which entries
                                         * will be maintained in the LBR table 
                                         */
   tEcfmLbLtStats       LbLtStats;      /* Per Context Stastitics */
   UINT4                u4ContextId;    /* Context Identifier*/
   UINT4                u4BridgeMode;    /* Context Identifier*/
   UINT4                u4RegModBmp; /* BitMap of Registered Modules. This
                                        bitmap is to store the Moules registerd
                                        with ECFM for a context for receiving 
                                        fault notification. */
#ifdef L2RED_WANTED
   tEcfmSysTime         TimeStamp;
   UINT4                u4RecvdLtrCacheHldTime; /* Remaining Time Value for 
                                                 * the Ltr Cache Hold Timer */
   UINT4                u4RecvdLbCacheHldTime; /* Remaining Time Value for 
                                                 * the Lb Cache Hold Timer */
#endif 
   UINT2                u2Y1731TrapControl;    /* Controls the enabling or
                                                * disabling of ECFM Traps 
                                                */
   UINT2                u2LtrCacheSize; /* configurable size of LTR mempool */
   UINT2                u2LtrCacheHoldTime;/* configurable time for which LTR
                                              entries are to be retained */
   UINT2                u2FrmDelayBuffSize; /* Number of entries that can be 
                                             * stored in frame delay Database
                                             */
   UINT2                u2LbrCacheSize;     /* configurable size of LBR Cache */
   UINT2                u2LbrCacheHoldTime; /* configurable time for which LBR
                                             * entries are to be retained
                                             */
   UINT1                u1LbrCacheStatus; /* Enable/Disable for storing LBRs 
                                           * received. It is User configurable
                                           */
   UINT1                u1LtrCacheStatus; /* Enable/Disable for storing LTRs 
                                           * received. It is User configurable
                                           */
   UINT1                u1DefaultMdDefIdPermission; /* Used to control the 
                                                     * SenderIdTlv 
                                                     * transmission, 
                                                     * when 
                                                     * DefaultMdEntry's 
                                                     * IdPermission 
                                                     * contains 
                                                     * SenderIdDefer
                                                     */
   UINT1                u1LbrCacheStatusBckup; /* This varible used 
                                                  * to store the current
                                                  * LBR Ststus
                                                  */
   tMemPoolId           LtrSenderTlvChassisIdPool;
   tMemPoolId           LtrSenderTlvMgmtDomainPool;
   tMemPoolId           LtrSenderTlvMgmtAddrPool;
   tMemPoolId           LtrOrgTlvValuePool;
   tMemPoolId           LtrEgressTlvEgressPortIdPool;
   tMemPoolId           LtrIngressTlvIngressPortIdPool;

} tEcfmLbLtContextInfo;
/****************************************************************************/
/* global information for LBLT task */
typedef struct EcfmLbLtGlobalInfo
{
   tOsixTaskId           TaskId;            /* Task ID for LBLT Task*/
   tOsixQId              CfgQId;            /* Configuration Message Queue ID 
                                             * for LBLT Task
                                             */
   tOsixQId              PktQId;            /* Packet Message Queue ID for LBLT 
                                             * Task
                                             */
   tTimerListId          TimerListId;       /* Timer list Id for the LBLT Task*/
   tTmrDesc              aTmrDesc[ECFM_LBLT_TMR_MAX_TYPES];
                                           /* description of all timers are 
                                            * stored in this array with TimerId 
                                            * as the array index. */

   tOsixSemId            SemId;             /* Semaphore ID for the LBLT Task*/
   tMemPoolId            LbmTableMemPool;   /* Memory pool for assigning memory 
                                             * to LBM table nodes 
                                             */
   tMemPoolId            MsgQMemPool;       /* Memory pool for Message Queues*/
   tMemPoolId            PortInfoMemPool;   /* Memory  pool for assigning memory 
                                             * to PortInfo nodes
                                             */
   tMemPoolId            MepTableMemPool;   /* Memory pool for assigning memory 
                                             * to MEP nodes
                                             */
   tMemPoolId            MipTableMemPool;   /* Memory pool for assigning memory 
                                             * to MIP nodes
                                             */
   tMemPoolId            StackTableMemPool; /* Memory pools for assigning 
                                             * memory to Stack Table nodes
                                             */
   tMemPoolId            LtmReplyListMemPool; /* Memory pools for assigning 
                                               * memory to LTM Reply list nodes
                                               */
   tMemPoolId            TmrMemPool;          /* Memory pools for assigning 
                                               * memory to Timer nodes
                                               */
   tMemPoolId            ContextMemPool;      /* Memory pool for various 
                                               * Contexts
                                               */    
   tMemPoolId            RMepDbMemPool;       /* Memory pool for various 
                                               * Remote MEP DB nodes
                                               */    
   tMemPoolId            MaMemPool;           /* Memory pool for various 
                                               * MA nodes
                                               */    
   tMemPoolId            MdMemPool;           /* Memory pool for various 
                                               * MD nodes
                                               */    
   tMemPoolId            DelayQueueMemPool;   /* Memory pool for various 
                                               * Delay Queue Nodes
                                               */    
   tMemPoolId            DefMdTableMemPool;   /* Memory pool for assigning 
                                               * memory to DefaultMD Tree
                                               * Nodes
                                               */
   tMemPoolId            AppRegMemPoolId;     /* Memory Pool for
                                               * Application registered 
                                               * with LBLT Task and 
                                               * Database
                                               */
   tMemPoolId            VlanMemPool;         /* Memory Pool for Vlan
                                               * information
                                               */
   tMemPoolId            PathInfoPool;        /* Memory pool for MPLS Path info
                                               */
   tMemPoolId            InParamsPool;        /* Memory pool to hold inparams */
   tMemPoolId            OutParamsPool;       /* Memory pool to hold outparams */
   tMemPoolId            LtrCacheIndicesPool; /*Memory Pool for Ltr Cache*/

   tMemPoolId            MepLbmPduMemPool;

   tMemPoolId            MepLbmDataTlvMemPool;

   UINT4                 u4MemoryFailureCount; /* Memory allocation failure
                                                * counter
                                                */
   UINT4                 u4BufferFailureCount; /* Buffer allocation failure
                                                * counter
                                                */
   UINT1                 *pu1LbLtPdu;       /* Global pointer used in formation 
                                             * of  CFM PDU  (LBM, LBR, LTM and 
                                             * LTR)
                                             */
   tEcfmLbLtContextInfo  *apContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];/* Array of Pointer 
                                                              * to various 
                                                              * context sepcific 
                                                              * informations
                                                              */
   tEcfmRegParams        *apAppRegParams[ECFM_MAX_APPS];  /* Y.1731 Information
                                                             specific to 
                                                             applications 
                                                             registering with
                                                             ECFM */
   tEcfmDll               LbmDllList;   /* A DLL list maintained to hold the
                                         * Sent LBMs; each Sent LBM is added 
                                         * to the tail of this list.
                                         */
   UINT1                 au1OUI[ECFM_OUI_LENGTH];  /* Array of Length 3 for 
                                                    * storing the 
                                                    * Oraganisation Unique 
                                                    * Identifier.
                                                    */
   UINT1                 au1Pad[1];
} tEcfmLbLtGlobalInfo;
#endif /* _CFM_LB_TDF_H */

/****************************************************************************
  End of File cfmlbtdf.h
 ****************************************************************************/
