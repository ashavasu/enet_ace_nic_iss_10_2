/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlbdef.h,v 1.17 2016/02/18 08:32:27 siva Exp $
 * 
 * Description: This file contains the constant definations for LBLT
 *              Task.
 *********************************************************************/
#ifndef _CFM_LB_DEF_H
#define _CFM_LB_DEF_H
/*****************************************************************************/
/* Events for DM Initiator */
#define ECFM_LBLT_DM_INTERVAL_EXPIRY     2 /* DM While Timer expiry event*/
#define ECFM_LBLT_DM_DEADLINE_EXPIRY 3 /* DM Deadline Timer expiry event*/
/*****************************************************************************/
/* Events for LBM Initiator */
#define ECFM_LBLT_LBI_INTERVAL_EXPIRY    2 /* LBI Interval Timer expiry event*/
#define ECFM_LBLT_LBI_DEADLINE_EXPIRY    3 /* LBI Deadline Timer expiry event*/
#define ECFM_LBLT_LBR_TIMEOUT_EXPIRY     4 /* LBR Timeout Timer expiry event*/

/* Events for TST Initiator */
#define ECFM_LBLT_TST_INTERVAL_EXPIRY    2 /* TST Interval Timer expiry event*/
#define ECFM_LBLT_TST_DEADLINE_EXPIRY    3 /* TST Deadline Timer expiry event*/
/*****************************************************************************/
/* Events for Linktrace Inititator State Machine */
#define ECFM_SM_EV_LTI_TX_LTM           2   /* Event received by this state 
                                             * machine, to initiate transmission 
                                             * of  LTM.
                                             */
#define ECFM_SM_EV_LTI_TX_LTM_TIMESOUT  3   /* Event received by this state 
                                             * machine to indicate LtmTx timer 
                                             * expiry.
                                             */
#define ECFM_SM_LTI_TX_MAX_EVENTS       4   /* Maximum number of events received 
                                             * by this state machine.
                                             */

/*****************************************************************************/
/* Events for Linktrace Inititator receive State Machine */
#define ECFM_SM_EV_LTI_RX_LTR_RCVD      2   /* Event received by this  machine 
                                             * to indicate the reception of LTR.
                                             */
#define ECFM_SM_LTI_RX_MAX_EVENTS       3   /* Maximum number of events received 
                                             * by this state machine.
                                             */

/*****************************************************************************/
/* Events for LTM Receiver State Machine */
#define ECFM_SM_EV_LTM_RX_LTM_RCVD      2   /* Event received by this state 
                                             * machine to indicate the reception 
                                             * of LTM.
                                             */
#define ECFM_SM_LTM_RX_MAX_EVENTS       3   /* Maximum number of events received 
                                             * by this  state machine
                                             */

/*****************************************************************************/
/* Events for TH Initiator */
#define ECFM_EV_LB_BURST_COMPLETE        2   /* Event received when LB Burst Comlete 
                                             */
#define ECFM_EV_TST_BURST_COMPLETE       3   /* Event received when TST Burst Comlete 
                                             */
#define ECFM_LBLT_TH_GET_TSTINFO_EXPIRY  4   /* GET TST INFO Timer expiry event*/ 
#define ECFM_LBLT_TH_DEADLINE_EXPIRY     5   /* TH DeadlineTimer expiry event*/
#define ECFM_LBLT_TH_VSR_TIMEOUT_EXPIRY  6   /* TH Vsr Timeout Timer expiry event*/

/*****************************************************************************/
/* States for Linktrace Inititator State Machine */
#define ECFM_LTI_STATE_DEFAULT          0   /* When a MEP is configured, state 
                                             * machine is initialized to this 
                                             * state 
                                             */
#define ECFM_LTI_STATE_IDLE             1   /* When state machine receives an 
                                             * event  �BEGIN�  
                                             */
#define ECFM_LTI_STATE_TRANSMIT         2   /* When state machine receives an 
                                             * event to initiate transmission of 
                                             * LTM .
                                             */
#define ECFM_LTI_MAX_STATES             3   /* Maximum number of possible States 
                                             * of this state machine.
                                             */

/*****************************************************************************/
/* States for Linktrace Inititator receive State Machine */
#define ECFM_LTI_RX_STATE_DEFAULT       0   /* When a MEP is configured, this 
                                             * state machine is initialized to 
                                             * this state.
                                             */
#define ECFM_LTI_RX_STATE_IDLE          1   /* When state machine receives an 
                                             * event  �BEGIN�
                                             */
#define ECFM_LTI_RX_STATE_RESPOND       2   /* When state machine receives event 
                                             * for reception of LTR.
                                             */
#define ECFM_LTI_RX_MAX_STATES          3   /* Maximum number of possible states 
                                             * of  this  state machine.
                                             */


/*****************************************************************************/
/* States for LTM Receiver State Machine */
#define ECFM_LTM_RX_STATE_DEFAULT   0   /* When ECFM module gets enabled  this 
                                         * state machine is initialized to 
                                         * �Default� state.
                                         */
#define ECFM_LTM_RX_STATE_IDLE      1   /* When state machine receives an event 
                                         * �BEGIN�
                                         */
#define ECFM_LTM_RX_STATE_RESPOND   2   /* When state machine receives an event 
                                         * for LTM reception
                                         */
#define ECFM_LTM_RX_MAX_STATES      3   /* Maximum number of states of  this 
                                         * state machine
                                         */
/*****************************************************************************/
/* States for Throughput SM */
#define ECFM_TH_STATE_DEFAULT                 0   /* When a MEP is configured, state 
                                                   * machine is initialized to this 
                                                   * state 
                                                   */
#define ECFM_TH_STATE_WAITING_LBR_TIMEOUT     1   /* When waiting for Lbr Timer Timeout 
                                                   */
#define ECFM_TH_STATE_WAITING_GET_TSTINFO     2   /* When waiting to get Tst Counter Info  
                                                   */
#define ECFM_TH_STATE_WAITING_TST_TIMEOUT     3   /* When waiting for Tst Timer Timeout 
                                                   */
#define ECFM_TH_STATE_WAITING_VSR_TIMEOUT     4   /* When waiting for Vsr Timer Timeout 
                                                   */
#define ECFM_TH_MAX_STATES                    5   /* Maximum number of possible States 
                                                   * of this state machine.
                                                   */
/*****************************************************************************/


/*****************************************************************************/
/* Timer Intervals */
#define ECFM_DELAY_QUEUE_INTERVAL 1   /* Interval value in sec for the Delay 
                                       * Queue timer.
                                       */
#define ECFM_LTM_TX_INTERVAL    5   /* Interval value in sec for the LtmTx timer 
                                     */
/*****************************************************************************/
/* LB Interval Timer Types*/
#define ECFM_LBLT_LB_INTERVAL_SEC   3
#define ECFM_LBLT_LB_INTERVAL_MSEC  2
#define ECFM_LBLT_LB_INTERVAL_USEC  1
/*****************************************************************************/
/* Free-Entry-Function Related */
#define ECFM_LBLT_MEP_ENTRY                 1  /* Entry type for FreeEntry 
                                             * function of RBTreeDestroy() 
                                             * used for MepTableIndex in 
                                             * global info.
                                             */
#define ECFM_LBLT_LTM_REPLY_LIST_ENTRY 2  /* Entry type for FreeEntry 
                                                  * function of RBTreeDestroy() 
                                                  * used for LTM Reply List 
                                                  * per MEP.
                                                  */
#define ECFM_LBLT_LTR_ENTRY                   3  /* Entry Type for FreeEntry 
                                                  * function of RBTreeDestroy 
                                                  * used for LTR Entry in Global
                                                  */
#define ECFM_LBLT_STACK_ENTRY                 4   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for Up/Down MP Entry
                                                   * in Stack.
                                                   */
#define ECFM_LBLT_MIP_ENTRY                   5   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for MIP Entry
                                                   * in Global Info.
                                                   */
#define ECFM_LBLT_LBM_ENTRY                   6   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for Lb Init
                                                   * in context Info.
                                                   */
#define ECFM_LBLT_FD_BUFFER_ENTRY             7   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for FD Buffer
                                                   * in context Info.
                                                   */
#define ECFM_LBLT_MIP_CCM_DB_ENTRY            8   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for MIP CCM DB
                                                   * in context Info.
                                                   */
#define ECFM_LBLT_RMEP_DB_ENTRY               9   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for RMEP DB
                                                   * in context Info.
                                                   */
#define ECFM_LBLT_MA_ENTRY                   10   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for MA
                                                   * in context Info.
                                                   */
#define ECFM_LBLT_MD_ENTRY                   11   /* Entry Type for FreeEntry 
                                                   * function of RBTreeDestroy
                                                   * used for MD
                                                   * in context Info.
                                                   */
#define ECFM_LBLT_DEF_MD_ENTRY               12
#define ECFM_LBLT_PORT_MEP_ENTRY             13

#define ECFM_LBLT_STACK_ENTRY_IN_PORT        14
#define ECFM_LBLT_VLAN_ENTRY                 15
/*****************************************************************************/
/* LBM TLV Types*/
#define ECFM_LBLT_LBM_WITHOUT_TLV                 0
#define ECFM_LBLT_LBM_WITH_DATA_TLV               1
#define ECFM_LBLT_LBM_WITH_TEST_TLV               2
/*****************************************************************************/
/* TST Interval Timer Types*/
#define ECFM_LBLT_TST_INTERVAL_SEC   3
#define ECFM_LBLT_TST_INTERVAL_MSEC  2
#define ECFM_LBLT_TST_INTERVAL_USEC  1
/*****************************************************************************/
/* TEST-TLV Value Types*/
#define ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITHOUT_CRC 0
#define ECFM_LBLT_TEST_TLV_NULL_SIGNAL_WITH_CRC    1
#define ECFM_LBLT_TEST_TLV_PRBS_WITHOUT_CRC        2
#define ECFM_LBLT_TEST_TLV_PRBS_WITH_CRC           3
/*****************************************************************************/

#define ECFM_LBLT_LBM_WITH_PATTERN                0
#define ECFM_LBLT_LBM_WITH_PATTERN_CRC            1
#define ECFM_LBLT_LBM_WITHOUT_PATTERN             2
/*****************************************************************************/
/* Size of Pseudo random byte sequence */
#define ECFM_LBLT_PRBS_SIZE                       4
/*****************************************************************************/
/* LBR Errors Types*/
#define ECFM_LBLT_LBR_WITH_BAD_MSDU               1
#define ECFM_LBLT_LBR_WITH_BIT_ERR                2
/*****************************************************************************/
/* Various possible type of DM transaction*/
#define ECFM_LBLT_DM_TYPE_1DM                     1 /* One way delay measurement*/
#define ECFM_LBLT_DM_TYPE_DMM                     2 /* Two way delay measurement*/
/* Various possible type of TH transaction*/
#define ECFM_LBLT_TH_TYPE_1TH                     1 /* One way Through measurement*/
#define ECFM_LBLT_TH_TYPE_2TH                     2 /* Two way Through measurement*/
#define ECFM_LBLT_TH_FS_MIN                       64    /* Minimum Frame Size Supported */
#define ECFM_LBLT_TH_PKT_DEF_SIZE                 1500  /*  Pkt Size */
#define ECFM_LBLT_TH_BURST_MESSAGES_DEF_VAL       10   /*  No of Messages*/
#define ECFM_LBLT_TH_BURST_DEADLINE_DEF_VAL       100   /*  100 milliseconds*/
#define ECFM_LBLT_TH_TYPE_DEF_VAL                 2   /* 2 way*/
#define ECFM_LBLT_TH_BURSTTYPE_DEF_VAL            1   /* Burst Type Messages*/
#define ECFM_LBLT_TH_MIN                          0       /* Minimum Throughput Supported in bps*/
#define Y1731_VSR_TIMEOUT_DEF_VAL                 5       /* Default Vsr Timeout Value  */
#define ECFM_BITS_PER_BYTE                        8       /* Bits per Byte  */
#define ECFM_LBLT_MIN_LBM_SIZE                    31      /* Minimum LBM PKT SIze  */
#define ECFM_LBLT_MIN_TST_SIZE                    31      /* Minimum TST PKT SIze  */
#define ECFM_LBLT_TH_MBPS_CONV_FACTOR             1000000.0

#define ECFM_LBLT_TH_VSM_SUBOPCODE                1
#define ECFM_LBLT_TH_REQCODE_TST_CLEAR            1
#define ECFM_LBLT_TH_REQCODE_TST_FETCH            2
#define ECFM_TH_TST_FETCH_RX_CNT_TMR              2
/*****************************************************************************/
/* Default transaction ID for 1Dm receiver side */
#define ECFM_LBLT_1DM_DEF_TRANS_ID                1
/*****************************************************************************/
/* Various possible type of DM interval*/
#define ECFM_LBLT_DM_INTERVAL_INVALID             -1 /* One way delay measurement*/
#define ECFM_LBLT_DM_INTERVAL_1                   1  /* Two way delay measurement*/
#define ECFM_LBLT_DM_INTERVAL_1000                1000 /* Two way delay measurement*/
#define ECFM_LBLT_1DM_TRANS_INTERVAL_MIN          0    /* 1Dm Transaction min interval */
#define ECFM_LBLT_1DM_TRANS_INTERVAL_MAX          1500 /* 1Dm Transaction max interval */
/*****************************************************************************/

/* System specific Constatnts */
#define ECFM_LBLT_CFG_QUEUE_NAME        "LBCQ"
/* maximum number of configuration messages = maximum ports, we have only SNMP
 * configuration message posted on LBLT task*/
#define ECFM_LBLT_CFG_QUEUE_DEPTH       (ECFM_MAX_PORTS_IN_SYSTEM)
#define ECFM_LBLT_PKT_QUEUE_NAME        "LBPQ"
/* maximum number of received packtes = maximum ports * (LBM + LBR + LTM + LTR +
 * DMM + DMR + 1DM + TST*/
#define ECFM_LBLT_PKT_QUEUE_DEPTH       (ECFM_MAX_PORTS_IN_SYSTEM*8)
#define ECFM_LBLT_TASK_NAME             "LBLT"
#define ECFM_LBLT_MSG_QUEUE_DEPTH           (ECFM_LBLT_CFG_QUEUE_DEPTH+\
                                             ECFM_LBLT_PKT_QUEUE_DEPTH)
/*****************************************************************************/
/* Failure Codes for LTR Reply list addition */                                             
#define ECFM_LBLT_LTR_FAILURE_1   1
#define ECFM_LBLT_LTR_FAILURE_2   2
#define ECFM_LBLT_LTR_FAILURE_3   3
#define ECFM_LBLT_LTR_FAILURE_4   4
#define ECFM_LBLT_LTR_FAILURE_5   5
#define ECFM_LBLT_LTR_FAILURE_6   6
#define ECFM_LBLT_LTR_FAILURE_7   7
/*****************************************************************************/
#endif /*_CFM_LB_DEF_H*/
/****************************************************************************
  End of File cfmlbdef.h
 ****************************************************************************/
