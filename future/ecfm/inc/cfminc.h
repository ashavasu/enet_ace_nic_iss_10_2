/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfminc.h,v 1.24 2015/07/30 05:45:44 siva Exp $
 * 
 * Description: This file contains the header files included in 
 *              this module.
 *********************************************************************/

#ifndef _CFMINC_H
#define _CFMINC_H

#include "lr.h"
#include "cfa.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "snmputil.h"
#include "trace.h"
#include "cli.h"
#include "fsvlan.h"
#include "pbb.h"
#include "mstp.h"
#include "rstp.h"
#include "pvrst.h"
#include "l2iwf.h"
#include "vcm.h"
#include "bridge.h"
#include "ecfm.h"
#include "erps.h"
#include "lldp.h"
#include "iss.h"
#include "utilipvx.h"
#ifdef L2RED_WANTED
#include "snp.h"
#include "elm.h"
#endif
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ecfmnpwr.h"
#ifndef NP_BACKWD_COMPATIBILITY
#include "ecfmminp.h"
#include "cfmoffnp.h"
#else
#include "cfmminpwr.h"
#include "ecfmnp.h"
#endif
#ifdef PBB_WANTED
#include "pbbnp.h"
#endif
#endif
#include "osxstd.h"
#include "utldll.h"
#include "utlmacro.h"
#include "utilcli.h"
#include "ecfmcli.h"
#include "redblack.h"
#include "fssnmp.h"
#include "cfmdef.h"
#include "cfmccdef.h"
#include "cfmlbdef.h"
#include "cfmtdfs.h"
#include "cfmcctdf.h"
#include "cfmlbtdf.h"
#include "cfmport.h"
#include "cfmprot.h"
#include "cfmccprt.h"
#include "cfmlbprt.h"
#include "cfmccapi.h"
#include "cfmlbapi.h"
#include "cfmtrap.h"
#include "cfmmptp.h"  
#include "fssyslog.h"

#ifdef L2RED_WANTED
#include "cfmred.h"
#else
#include "cfmrdstb.h"
#endif
#ifdef ICCH_WANTED
#include "icch.h"
#endif
#ifdef LA_WANTED
#include "la.h"
#endif
#include "cfmext.h"
#include "cfmccext.h"
#include "cfmlbext.h"
#include "stdecfwr.h"
#include "cfmv2ewr.h"
#include "fsecfmwr.h"
#include "fscfmmwr.h"
#include "stdecflw.h"
#include "cfmv2elw.h"
#include "fsecfmlw.h"
#include "fscfmmlw.h"
/* ECFM-PBB-Extention MIB */
#include "fscfmewr.h"
#include "fscfmelw.h"
/*Y.1731 MIB*/
#include "fsmiy1lw.h"
#include "fsmiy1wr.h"

#include "cfmmacs.h"
#include "cfmtrc.h"

#include "utilcli.h"
#include "cfmsz.h"
#include "isspi.h"
#endif /* _CFMINC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cfminc.h                       */
/*-----------------------------------------------------------------------*/
