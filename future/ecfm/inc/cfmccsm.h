/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmccsm.h,v 1.8 2011/11/21 13:24:02 siva Exp $
 * 
 * Description: This file contains the functionality of the
 *              CC state machine.
 *********************************************************************/
#ifndef __CFM_CC_SM_H
#define __CFM_CC_SM_H

/* routines that handles the occurence of the events */

/* CCI State machine */
PRIVATE INT4 EcfmCciSmSetStateIdle PROTO ((tEcfmCcPduSmInfo * ));
PRIVATE INT4 EcfmCciSmSetStateDefault PROTO ((tEcfmCcPduSmInfo * ));
PRIVATE INT4 EcfmCciSmSetStateWaiting PROTO ((tEcfmCcPduSmInfo *));
PRIVATE INT4 EcfmCciSmEvtImpossible PROTO ((tEcfmCcPduSmInfo *));
PRIVATE INT4 EcfmCciSmXmitCCM PROTO ((tEcfmCcPduSmInfo * ));
PUBLIC VOID
EcfmCciSmFormatCcmPduHdr PROTO((tEcfmCcMepInfo *, UINT1 **));
PUBLIC VOID
EcfmCciSmPutCCMTlv PROTO((tEcfmCcPduSmInfo *, UINT1 ** ));
PUBLIC VOID
EcfmCciSmPutPortStatusTlv PROTO((tEcfmCcPduSmInfo * , UINT1 **));
PUBLIC VOID
EcfmCciSmPutInterfaceStatusTlv PROTO((tEcfmCcMepInfo * , UINT1 ** ));

PUBLIC VOID
EcfmCcNotEnableRmepIndication PROTO((tEcfmCcPduSmInfo * ));

/* Enums for the rotuines to be retrieve from the the matrix*/
enum
{
  CCI0,               /* EcfmCciSmSetStateIdle */
  CCI1,               /* EcfmCciSmSetStateDefault */
  CCI2,               /* EcfmCciSmSetStateWaiting */
  CCI3,               /* EcfmCciSmEvtImpossible */
  ECFM_MAX_CCI_FN_PTRS
};

/* function pointers of  CCI State machine */
INT4 (*gaEcfmCCActionProc[ECFM_MAX_CCI_FN_PTRS]) (tEcfmCcPduSmInfo *) =
{
  EcfmCciSmSetStateIdle,
  EcfmCciSmSetStateDefault,
  EcfmCciSmSetStateWaiting,
  EcfmCciSmEvtImpossible
};

/* State Event Table of CCM TX */
const UINT1 gau1EcfmCCmSem[ECFM_SM_CCI_MAX_EVENTS][ECFM_CCI_MAX_STATES] =
 /* DEFAULT IDLE WAITING  States */
{                         /* Events */
    { CCI0,   CCI3,  CCI3},    /* ECFM_SM_EV_BEGIN */
    { CCI3,   CCI1,  CCI1},    /* ECFM_SM_EV_MEP_NOT_ACTIVE */
    { CCI3,   CCI2,  CCI3},    /* ECFM_SM_EV_CCI_ENABLE */
    { CCI3,   CCI3,  CCI0},    /* ECFM_SM_EV_CCI_DISABLE */
    { CCI3,   CCI3,  CCI2},    /* ECFM_SM_EV_CCI_TIMER_TIMEOUT */
};

#endif /* __CFM_CC_SM_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmccsm.h                        */
/*-----------------------------------------------------------------------*/
