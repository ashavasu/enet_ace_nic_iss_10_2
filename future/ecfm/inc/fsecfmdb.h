/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsecfmdb.h,v 1.11 2013/01/05 11:44:22 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSECFMDB_H
#define _FSECFMDB_H

UINT1 FsEcfmPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEcfmMipTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEcfmMipCcmDbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsEcfmDynMipPreventionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEcfmRemoteMepDbExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEcfmLtmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEcfmMepExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEcfmMdExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEcfmMaExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsecfm [] ={1,3,6,1,4,1,2076,150};
tSNMP_OID_TYPE fsecfmOID = {8, fsecfm};


UINT4 FsEcfmSystemControl [ ] ={1,3,6,1,4,1,2076,150,1,1};
UINT4 FsEcfmModuleStatus [ ] ={1,3,6,1,4,1,2076,150,1,2};
UINT4 FsEcfmOui [ ] ={1,3,6,1,4,1,2076,150,1,3};
UINT4 FsEcfmTraceOption [ ] ={1,3,6,1,4,1,2076,150,1,4};
UINT4 FsEcfmLtrCacheStatus [ ] ={1,3,6,1,4,1,2076,150,1,5};
UINT4 FsEcfmLtrCacheClear [ ] ={1,3,6,1,4,1,2076,150,1,6};
UINT4 FsEcfmLtrCacheHoldTime [ ] ={1,3,6,1,4,1,2076,150,1,7};
UINT4 FsEcfmLtrCacheSize [ ] ={1,3,6,1,4,1,2076,150,1,8};
UINT4 FsEcfmPortIndex [ ] ={1,3,6,1,4,1,2076,150,1,9,1,1};
UINT4 FsEcfmPortLLCEncapStatus [ ] ={1,3,6,1,4,1,2076,150,1,9,1,2};
UINT4 FsEcfmPortModuleStatus [ ] ={1,3,6,1,4,1,2076,150,1,9,1,3};
UINT4 FsEcfmPortTxCfmPduCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,4};
UINT4 FsEcfmPortTxCcmCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,5};
UINT4 FsEcfmPortTxLbmCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,6};
UINT4 FsEcfmPortTxLbrCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,7};
UINT4 FsEcfmPortTxLtmCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,8};
UINT4 FsEcfmPortTxLtrCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,9};
UINT4 FsEcfmPortTxFailedCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,10};
UINT4 FsEcfmPortRxCfmPduCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,11};
UINT4 FsEcfmPortRxCcmCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,12};
UINT4 FsEcfmPortRxLbmCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,13};
UINT4 FsEcfmPortRxLbrCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,14};
UINT4 FsEcfmPortRxLtmCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,15};
UINT4 FsEcfmPortRxLtrCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,16};
UINT4 FsEcfmPortRxBadCfmPduCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,17};
UINT4 FsEcfmPortFrwdCfmPduCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,18};
UINT4 FsEcfmPortDsrdCfmPduCount [ ] ={1,3,6,1,4,1,2076,150,1,9,1,19};
UINT4 FsEcfmMipCcmDbStatus [ ] ={1,3,6,1,4,1,2076,150,1,10};
UINT4 FsEcfmMipCcmDbClear [ ] ={1,3,6,1,4,1,2076,150,1,11};
UINT4 FsEcfmMipCcmDbSize [ ] ={1,3,6,1,4,1,2076,150,1,12};
UINT4 FsEcfmMipCcmDbHoldTime [ ] ={1,3,6,1,4,1,2076,150,1,13};
UINT4 FsEcfmMemoryFailureCount [ ] ={1,3,6,1,4,1,2076,150,1,14};
UINT4 FsEcfmBufferFailureCount [ ] ={1,3,6,1,4,1,2076,150,1,15};
UINT4 FsEcfmUpCount [ ] ={1,3,6,1,4,1,2076,150,1,16};
UINT4 FsEcfmDownCount [ ] ={1,3,6,1,4,1,2076,150,1,17};
UINT4 FsEcfmNoDftCount [ ] ={1,3,6,1,4,1,2076,150,1,18};
UINT4 FsEcfmRdiDftCount [ ] ={1,3,6,1,4,1,2076,150,1,19};
UINT4 FsEcfmMacStatusDftCount [ ] ={1,3,6,1,4,1,2076,150,1,20};
UINT4 FsEcfmRemoteCcmDftCount [ ] ={1,3,6,1,4,1,2076,150,1,21};
UINT4 FsEcfmErrorCcmDftCount [ ] ={1,3,6,1,4,1,2076,150,1,22};
UINT4 FsEcfmXconDftCount [ ] ={1,3,6,1,4,1,2076,150,1,23};
UINT4 FsEcfmCrosscheckDelay [ ] ={1,3,6,1,4,1,2076,150,1,24};
UINT4 FsEcfmMipDynamicEvaluationStatus [ ] ={1,3,6,1,4,1,2076,150,1,25};
UINT4 FsEcfmMipIfIndex [ ] ={1,3,6,1,4,1,2076,150,1,26,1,1};
UINT4 FsEcfmMipMdLevel [ ] ={1,3,6,1,4,1,2076,150,1,26,1,2};
UINT4 FsEcfmMipVid [ ] ={1,3,6,1,4,1,2076,150,1,26,1,3};
UINT4 FsEcfmMipActive [ ] ={1,3,6,1,4,1,2076,150,1,26,1,4};
UINT4 FsEcfmMipRowStatus [ ] ={1,3,6,1,4,1,2076,150,1,26,1,5};
UINT4 FsEcfmMipCcmFid [ ] ={1,3,6,1,4,1,2076,150,1,27,1,1};
UINT4 FsEcfmMipCcmSrcAddr [ ] ={1,3,6,1,4,1,2076,150,1,27,1,2};
UINT4 FsEcfmMipCcmIfIndex [ ] ={1,3,6,1,4,1,2076,150,1,27,1,3};
UINT4 FsEcfmGlobalCcmOffload [ ] ={1,3,6,1,4,1,2076,150,1,28};
UINT4 FsEcfmDynMipPreventionRowStatus [ ] ={1,3,6,1,4,1,2076,150,1,29,1,1};
UINT4 FsEcfmMdIndex [ ] ={1,3,6,1,4,1,2076,150,2,1,1,1};
UINT4 FsEcfmMaIndex [ ] ={1,3,6,1,4,1,2076,150,2,1,1,2};
UINT4 FsEcfmMepIdentifier [ ] ={1,3,6,1,4,1,2076,150,2,1,1,3};
UINT4 FsEcfmRMepIdentifier [ ] ={1,3,6,1,4,1,2076,150,2,1,1,4};
UINT4 FsEcfmRMepCcmSequenceNum [ ] ={1,3,6,1,4,1,2076,150,2,1,1,5};
UINT4 FsEcfmRMepPortStatusDefect [ ] ={1,3,6,1,4,1,2076,150,2,1,1,6};
UINT4 FsEcfmRMepInterfaceStatusDefect [ ] ={1,3,6,1,4,1,2076,150,2,1,1,7};
UINT4 FsEcfmRMepCcmDefect [ ] ={1,3,6,1,4,1,2076,150,2,1,1,8};
UINT4 FsEcfmRMepRDIDefect [ ] ={1,3,6,1,4,1,2076,150,2,1,1,9};
UINT4 FsEcfmRMepMacAddress [ ] ={1,3,6,1,4,1,2076,150,2,1,1,10};
UINT4 FsEcfmRMepRdi [ ] ={1,3,6,1,4,1,2076,150,2,1,1,11};
UINT4 FsEcfmRMepPortStatusTlv [ ] ={1,3,6,1,4,1,2076,150,2,1,1,12};
UINT4 FsEcfmRMepInterfaceStatusTlv [ ] ={1,3,6,1,4,1,2076,150,2,1,1,13};
UINT4 FsEcfmRMepChassisIdSubtype [ ] ={1,3,6,1,4,1,2076,150,2,1,1,14};
UINT4 FsEcfmMepDbChassisId [ ] ={1,3,6,1,4,1,2076,150,2,1,1,15};
UINT4 FsEcfmRMepManAddressDomain [ ] ={1,3,6,1,4,1,2076,150,2,1,1,16};
UINT4 FsEcfmRMepManAddress [ ] ={1,3,6,1,4,1,2076,150,2,1,1,17};
UINT4 FsEcfmLtmSeqNumber [ ] ={1,3,6,1,4,1,2076,150,2,2,1,1};
UINT4 FsEcfmLtmTargetMacAddress [ ] ={1,3,6,1,4,1,2076,150,2,2,1,2};
UINT4 FsEcfmLtmTtl [ ] ={1,3,6,1,4,1,2076,150,2,2,1,3};
UINT4 FsEcfmXconnRMepId [ ] ={1,3,6,1,4,1,2076,150,2,3,1,1};
UINT4 FsEcfmErrorRMepId [ ] ={1,3,6,1,4,1,2076,150,2,3,1,2};
UINT4 FsEcfmMepDefectRDICcm [ ] ={1,3,6,1,4,1,2076,150,2,3,1,3};
UINT4 FsEcfmMepDefectMacStatus [ ] ={1,3,6,1,4,1,2076,150,2,3,1,4};
UINT4 FsEcfmMepDefectRemoteCcm [ ] ={1,3,6,1,4,1,2076,150,2,3,1,5};
UINT4 FsEcfmMepDefectErrorCcm [ ] ={1,3,6,1,4,1,2076,150,2,3,1,6};
UINT4 FsEcfmMepDefectXconnCcm [ ] ={1,3,6,1,4,1,2076,150,2,3,1,7};
UINT4 FsEcfmMepCcmOffload [ ] ={1,3,6,1,4,1,2076,150,2,3,1,8};
UINT4 FsEcfmMepLbrIn [ ] ={1,3,6,1,4,1,2076,150,2,3,1,9};
UINT4 FsEcfmMepLbrInOutOfOrder [ ] ={1,3,6,1,4,1,2076,150,2,3,1,10};
UINT4 FsEcfmMepLbrBadMsdu [ ] ={1,3,6,1,4,1,2076,150,2,3,1,11};
UINT4 FsEcfmMepUnexpLtrIn [ ] ={1,3,6,1,4,1,2076,150,2,3,1,12};
UINT4 FsEcfmMepLbrOut [ ] ={1,3,6,1,4,1,2076,150,2,3,1,13};
UINT4 FsEcfmMepCcmSequenceErrors [ ] ={1,3,6,1,4,1,2076,150,2,3,1,14};
UINT4 FsEcfmMepCciSentCcms [ ] ={1,3,6,1,4,1,2076,150,2,3,1,15};
UINT4 FsEcfmMepArchiveHoldTime [ ] ={1,3,6,1,4,1,2076,150,2,4,1,1};
UINT4 FsEcfmMaCrosscheckStatus [ ] ={1,3,6,1,4,1,2076,150,2,5,1,1};
UINT4 FsEcfmTrapControl [ ] ={1,3,6,1,4,1,2076,150,3,1};
UINT4 FsEcfmTrapType [ ] ={1,3,6,1,4,1,2076,150,3,2};


tMbDbEntry fsecfmMibEntry[]= {

{{10,FsEcfmSystemControl}, NULL, FsEcfmSystemControlGet, FsEcfmSystemControlSet, FsEcfmSystemControlTest, FsEcfmSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmModuleStatus}, NULL, FsEcfmModuleStatusGet, FsEcfmModuleStatusSet, FsEcfmModuleStatusTest, FsEcfmModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsEcfmOui}, NULL, FsEcfmOuiGet, FsEcfmOuiSet, FsEcfmOuiTest, FsEcfmOuiDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmTraceOption}, NULL, FsEcfmTraceOptionGet, FsEcfmTraceOptionSet, FsEcfmTraceOptionTest, FsEcfmTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "262144"},

{{10,FsEcfmLtrCacheStatus}, NULL, FsEcfmLtrCacheStatusGet, FsEcfmLtrCacheStatusSet, FsEcfmLtrCacheStatusTest, FsEcfmLtrCacheStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsEcfmLtrCacheClear}, NULL, FsEcfmLtrCacheClearGet, FsEcfmLtrCacheClearSet, FsEcfmLtrCacheClearTest, FsEcfmLtrCacheClearDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsEcfmLtrCacheHoldTime}, NULL, FsEcfmLtrCacheHoldTimeGet, FsEcfmLtrCacheHoldTimeSet, FsEcfmLtrCacheHoldTimeTest, FsEcfmLtrCacheHoldTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},

{{10,FsEcfmLtrCacheSize}, NULL, FsEcfmLtrCacheSizeGet, FsEcfmLtrCacheSizeSet, FsEcfmLtrCacheSizeTest, FsEcfmLtrCacheSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},

{{12,FsEcfmPortIndex}, GetNextIndexFsEcfmPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortLLCEncapStatus}, GetNextIndexFsEcfmPortTable, FsEcfmPortLLCEncapStatusGet, FsEcfmPortLLCEncapStatusSet, FsEcfmPortLLCEncapStatusTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, "2"},

{{12,FsEcfmPortModuleStatus}, GetNextIndexFsEcfmPortTable, FsEcfmPortModuleStatusGet, FsEcfmPortModuleStatusSet, FsEcfmPortModuleStatusTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, "2"},

{{12,FsEcfmPortTxCfmPduCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxCfmPduCountGet, FsEcfmPortTxCfmPduCountSet, FsEcfmPortTxCfmPduCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortTxCcmCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxCcmCountGet, FsEcfmPortTxCcmCountSet, FsEcfmPortTxCcmCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortTxLbmCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxLbmCountGet, FsEcfmPortTxLbmCountSet, FsEcfmPortTxLbmCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortTxLbrCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxLbrCountGet, FsEcfmPortTxLbrCountSet, FsEcfmPortTxLbrCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortTxLtmCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxLtmCountGet, FsEcfmPortTxLtmCountSet, FsEcfmPortTxLtmCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortTxLtrCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxLtrCountGet, FsEcfmPortTxLtrCountSet, FsEcfmPortTxLtrCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortTxFailedCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortTxFailedCountGet, FsEcfmPortTxFailedCountSet, FsEcfmPortTxFailedCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxCfmPduCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxCfmPduCountGet, FsEcfmPortRxCfmPduCountSet, FsEcfmPortRxCfmPduCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxCcmCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxCcmCountGet, FsEcfmPortRxCcmCountSet, FsEcfmPortRxCcmCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxLbmCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxLbmCountGet, FsEcfmPortRxLbmCountSet, FsEcfmPortRxLbmCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxLbrCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxLbrCountGet, FsEcfmPortRxLbrCountSet, FsEcfmPortRxLbrCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxLtmCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxLtmCountGet, FsEcfmPortRxLtmCountSet, FsEcfmPortRxLtmCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxLtrCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxLtrCountGet, FsEcfmPortRxLtrCountSet, FsEcfmPortRxLtrCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortRxBadCfmPduCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortRxBadCfmPduCountGet, FsEcfmPortRxBadCfmPduCountSet, FsEcfmPortRxBadCfmPduCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortFrwdCfmPduCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortFrwdCfmPduCountGet, FsEcfmPortFrwdCfmPduCountSet, FsEcfmPortFrwdCfmPduCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{12,FsEcfmPortDsrdCfmPduCount}, GetNextIndexFsEcfmPortTable, FsEcfmPortDsrdCfmPduCountGet, FsEcfmPortDsrdCfmPduCountSet, FsEcfmPortDsrdCfmPduCountTest, FsEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmPortTableINDEX, 1, 0, 0, NULL},

{{10,FsEcfmMipCcmDbStatus}, NULL, FsEcfmMipCcmDbStatusGet, FsEcfmMipCcmDbStatusSet, FsEcfmMipCcmDbStatusTest, FsEcfmMipCcmDbStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsEcfmMipCcmDbClear}, NULL, FsEcfmMipCcmDbClearGet, FsEcfmMipCcmDbClearSet, FsEcfmMipCcmDbClearTest, FsEcfmMipCcmDbClearDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsEcfmMipCcmDbSize}, NULL, FsEcfmMipCcmDbSizeGet, FsEcfmMipCcmDbSizeSet, FsEcfmMipCcmDbSizeTest, FsEcfmMipCcmDbSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{10,FsEcfmMipCcmDbHoldTime}, NULL, FsEcfmMipCcmDbHoldTimeGet, FsEcfmMipCcmDbHoldTimeSet, FsEcfmMipCcmDbHoldTimeTest, FsEcfmMipCcmDbHoldTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "24"},

{{10,FsEcfmMemoryFailureCount}, NULL, FsEcfmMemoryFailureCountGet, FsEcfmMemoryFailureCountSet, FsEcfmMemoryFailureCountTest, FsEcfmMemoryFailureCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmBufferFailureCount}, NULL, FsEcfmBufferFailureCountGet, FsEcfmBufferFailureCountSet, FsEcfmBufferFailureCountTest, FsEcfmBufferFailureCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmUpCount}, NULL, FsEcfmUpCountGet, FsEcfmUpCountSet, FsEcfmUpCountTest, FsEcfmUpCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmDownCount}, NULL, FsEcfmDownCountGet, FsEcfmDownCountSet, FsEcfmDownCountTest, FsEcfmDownCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmNoDftCount}, NULL, FsEcfmNoDftCountGet, FsEcfmNoDftCountSet, FsEcfmNoDftCountTest, FsEcfmNoDftCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmRdiDftCount}, NULL, FsEcfmRdiDftCountGet, FsEcfmRdiDftCountSet, FsEcfmRdiDftCountTest, FsEcfmRdiDftCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmMacStatusDftCount}, NULL, FsEcfmMacStatusDftCountGet, FsEcfmMacStatusDftCountSet, FsEcfmMacStatusDftCountTest, FsEcfmMacStatusDftCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmRemoteCcmDftCount}, NULL, FsEcfmRemoteCcmDftCountGet, FsEcfmRemoteCcmDftCountSet, FsEcfmRemoteCcmDftCountTest, FsEcfmRemoteCcmDftCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmErrorCcmDftCount}, NULL, FsEcfmErrorCcmDftCountGet, FsEcfmErrorCcmDftCountSet, FsEcfmErrorCcmDftCountTest, FsEcfmErrorCcmDftCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmXconDftCount}, NULL, FsEcfmXconDftCountGet, FsEcfmXconDftCountSet, FsEcfmXconDftCountTest, FsEcfmXconDftCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmCrosscheckDelay}, NULL, FsEcfmCrosscheckDelayGet, FsEcfmCrosscheckDelaySet, FsEcfmCrosscheckDelayTest, FsEcfmCrosscheckDelayDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,FsEcfmMipDynamicEvaluationStatus}, NULL, FsEcfmMipDynamicEvaluationStatusGet, FsEcfmMipDynamicEvaluationStatusSet, FsEcfmMipDynamicEvaluationStatusTest, FsEcfmMipDynamicEvaluationStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsEcfmMipIfIndex}, GetNextIndexFsEcfmMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEcfmMipTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMipMdLevel}, GetNextIndexFsEcfmMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsEcfmMipTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMipVid}, GetNextIndexFsEcfmMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEcfmMipTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMipActive}, GetNextIndexFsEcfmMipTable, FsEcfmMipActiveGet, FsEcfmMipActiveSet, FsEcfmMipActiveTest, FsEcfmMipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMipTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMipRowStatus}, GetNextIndexFsEcfmMipTable, FsEcfmMipRowStatusGet, FsEcfmMipRowStatusSet, FsEcfmMipRowStatusTest, FsEcfmMipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMipTableINDEX, 3, 0, 1, NULL},

{{12,FsEcfmMipCcmFid}, GetNextIndexFsEcfmMipCcmDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmMipCcmDbTableINDEX, 2, 0, 0, NULL},

{{12,FsEcfmMipCcmSrcAddr}, GetNextIndexFsEcfmMipCcmDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsEcfmMipCcmDbTableINDEX, 2, 0, 0, NULL},

{{12,FsEcfmMipCcmIfIndex}, GetNextIndexFsEcfmMipCcmDbTable, FsEcfmMipCcmIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsEcfmMipCcmDbTableINDEX, 2, 0, 0, NULL},

{{10,FsEcfmGlobalCcmOffload}, NULL, FsEcfmGlobalCcmOffloadGet, FsEcfmGlobalCcmOffloadSet, FsEcfmGlobalCcmOffloadTest, FsEcfmGlobalCcmOffloadDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsEcfmDynMipPreventionRowStatus}, GetNextIndexFsEcfmDynMipPreventionTable, FsEcfmDynMipPreventionRowStatusGet, FsEcfmDynMipPreventionRowStatusSet, FsEcfmDynMipPreventionRowStatusTest, FsEcfmDynMipPreventionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmDynMipPreventionTableINDEX, 3, 0, 1, NULL},

{{12,FsEcfmMdIndex}, GetNextIndexFsEcfmRemoteMepDbExTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmMaIndex}, GetNextIndexFsEcfmRemoteMepDbExTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmMepIdentifier}, GetNextIndexFsEcfmRemoteMepDbExTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepIdentifier}, GetNextIndexFsEcfmRemoteMepDbExTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepCcmSequenceNum}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepCcmSequenceNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepPortStatusDefect}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepPortStatusDefectGet, FsEcfmRMepPortStatusDefectSet, FsEcfmRMepPortStatusDefectTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepInterfaceStatusDefect}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepInterfaceStatusDefectGet, FsEcfmRMepInterfaceStatusDefectSet, FsEcfmRMepInterfaceStatusDefectTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepCcmDefect}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepCcmDefectGet, FsEcfmRMepCcmDefectSet, FsEcfmRMepCcmDefectTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepRDIDefect}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepRDIDefectGet, FsEcfmRMepRDIDefectSet, FsEcfmRMepRDIDefectTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepMacAddress}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepMacAddressGet, FsEcfmRMepMacAddressSet, FsEcfmRMepMacAddressTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepRdi}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepRdiGet, FsEcfmRMepRdiSet, FsEcfmRMepRdiTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepPortStatusTlv}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepPortStatusTlvGet, FsEcfmRMepPortStatusTlvSet, FsEcfmRMepPortStatusTlvTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepInterfaceStatusTlv}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepInterfaceStatusTlvGet, FsEcfmRMepInterfaceStatusTlvSet, FsEcfmRMepInterfaceStatusTlvTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepChassisIdSubtype}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepChassisIdSubtypeGet, FsEcfmRMepChassisIdSubtypeSet, FsEcfmRMepChassisIdSubtypeTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, "4"},

{{12,FsEcfmMepDbChassisId}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmMepDbChassisIdGet, FsEcfmMepDbChassisIdSet, FsEcfmMepDbChassisIdTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepManAddressDomain}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepManAddressDomainGet, FsEcfmRMepManAddressDomainSet, FsEcfmRMepManAddressDomainTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmRMepManAddress}, GetNextIndexFsEcfmRemoteMepDbExTable, FsEcfmRMepManAddressGet, FsEcfmRMepManAddressSet, FsEcfmRMepManAddressTest, FsEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEcfmRemoteMepDbExTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmLtmSeqNumber}, GetNextIndexFsEcfmLtmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEcfmLtmTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmLtmTargetMacAddress}, GetNextIndexFsEcfmLtmTable, FsEcfmLtmTargetMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsEcfmLtmTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmLtmTtl}, GetNextIndexFsEcfmLtmTable, FsEcfmLtmTtlGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsEcfmLtmTableINDEX, 4, 0, 0, NULL},

{{12,FsEcfmXconnRMepId}, GetNextIndexFsEcfmMepExTable, FsEcfmXconnRMepIdGet, FsEcfmXconnRMepIdSet, FsEcfmXconnRMepIdTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmErrorRMepId}, GetNextIndexFsEcfmMepExTable, FsEcfmErrorRMepIdGet, FsEcfmErrorRMepIdSet, FsEcfmErrorRMepIdTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepDefectRDICcm}, GetNextIndexFsEcfmMepExTable, FsEcfmMepDefectRDICcmGet, FsEcfmMepDefectRDICcmSet, FsEcfmMepDefectRDICcmTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepDefectMacStatus}, GetNextIndexFsEcfmMepExTable, FsEcfmMepDefectMacStatusGet, FsEcfmMepDefectMacStatusSet, FsEcfmMepDefectMacStatusTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepDefectRemoteCcm}, GetNextIndexFsEcfmMepExTable, FsEcfmMepDefectRemoteCcmGet, FsEcfmMepDefectRemoteCcmSet, FsEcfmMepDefectRemoteCcmTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepDefectErrorCcm}, GetNextIndexFsEcfmMepExTable, FsEcfmMepDefectErrorCcmGet, FsEcfmMepDefectErrorCcmSet, FsEcfmMepDefectErrorCcmTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepDefectXconnCcm}, GetNextIndexFsEcfmMepExTable, FsEcfmMepDefectXconnCcmGet, FsEcfmMepDefectXconnCcmSet, FsEcfmMepDefectXconnCcmTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepCcmOffload}, GetNextIndexFsEcfmMepExTable, FsEcfmMepCcmOffloadGet, FsEcfmMepCcmOffloadSet, FsEcfmMepCcmOffloadTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, "2"},

{{12,FsEcfmMepLbrIn}, GetNextIndexFsEcfmMepExTable, FsEcfmMepLbrInGet, FsEcfmMepLbrInSet, FsEcfmMepLbrInTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepLbrInOutOfOrder}, GetNextIndexFsEcfmMepExTable, FsEcfmMepLbrInOutOfOrderGet, FsEcfmMepLbrInOutOfOrderSet, FsEcfmMepLbrInOutOfOrderTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepLbrBadMsdu}, GetNextIndexFsEcfmMepExTable, FsEcfmMepLbrBadMsduGet, FsEcfmMepLbrBadMsduSet, FsEcfmMepLbrBadMsduTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepUnexpLtrIn}, GetNextIndexFsEcfmMepExTable, FsEcfmMepUnexpLtrInGet, FsEcfmMepUnexpLtrInSet, FsEcfmMepUnexpLtrInTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepLbrOut}, GetNextIndexFsEcfmMepExTable, FsEcfmMepLbrOutGet, FsEcfmMepLbrOutSet, FsEcfmMepLbrOutTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepCcmSequenceErrors}, GetNextIndexFsEcfmMepExTable, FsEcfmMepCcmSequenceErrorsGet, FsEcfmMepCcmSequenceErrorsSet, FsEcfmMepCcmSequenceErrorsTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepCciSentCcms}, GetNextIndexFsEcfmMepExTable, FsEcfmMepCciSentCcmsGet, FsEcfmMepCciSentCcmsSet, FsEcfmMepCciSentCcmsTest, FsEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEcfmMepExTableINDEX, 3, 0, 0, NULL},

{{12,FsEcfmMepArchiveHoldTime}, GetNextIndexFsEcfmMdExTable, FsEcfmMepArchiveHoldTimeGet, FsEcfmMepArchiveHoldTimeSet, FsEcfmMepArchiveHoldTimeTest, FsEcfmMdExTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsEcfmMdExTableINDEX, 1, 0, 0, "100"},

{{12,FsEcfmMaCrosscheckStatus}, GetNextIndexFsEcfmMaExTable, FsEcfmMaCrosscheckStatusGet, FsEcfmMaCrosscheckStatusSet, FsEcfmMaCrosscheckStatusTest, FsEcfmMaExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEcfmMaExTableINDEX, 2, 0, 0, "1"},

{{10,FsEcfmTrapControl}, NULL, FsEcfmTrapControlGet, FsEcfmTrapControlSet, FsEcfmTrapControlTest, FsEcfmTrapControlDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcfmTrapType}, NULL, FsEcfmTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsecfmEntry = { 92, fsecfmMibEntry };
#endif /* _FSECFMDB_H */

