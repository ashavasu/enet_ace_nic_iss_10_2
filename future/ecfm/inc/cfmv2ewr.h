/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmv2ewr.h,v 1.2 2011/12/27 11:53:40 siva Exp $
 *
 * Description: Protocol Low Level Routines prototypes.  
 *******************************************************************/

#ifndef _CFMV2EWR_H
#define _CFMV2EWR_H
INT4 GetNextIndexIeee8021CfmStackTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterCFMV2E(VOID);

VOID UnRegisterCFMV2E(VOID);
INT4 Ieee8021CfmStackMdIndexGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmStackMaIndexGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmStackMepIdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmStackMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021CfmVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021CfmVlanPrimarySelectorGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmVlanRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmVlanPrimarySelectorSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmVlanRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmVlanPrimarySelectorTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmVlanRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021CfmDefaultMdTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021CfmDefaultMdStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdLevelGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdMhfCreationGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdIdPermissionGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdLevelSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdMhfCreationSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdIdPermissionSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdMhfCreationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdIdPermissionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmDefaultMdTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021CfmConfigErrorListTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021CfmConfigErrorListErrorTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021CfmMaCompTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021CfmMaCompPrimarySelectorTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompPrimarySelectorOrNoneGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompMhfCreationGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompIdPermissionGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompNumberOfVidsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompPrimarySelectorTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompPrimarySelectorOrNoneSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompMhfCreationSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompIdPermissionSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompNumberOfVidsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompPrimarySelectorTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompPrimarySelectorOrNoneTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompMhfCreationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompIdPermissionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompNumberOfVidsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021CfmMaCompTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _CFMV2EWR_H */
