/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmelw.h,v 1.2 2009/09/24 14:30:33 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIEcfmExtStackTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmExtStackTable ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmExtStackTable  */

INT1
nmhGetFirstIndexFsMIEcfmExtStackTable ARG_LIST((INT4 * , INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmExtStackTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmExtStackMdIndex ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtStackMaIndex ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtStackMepId ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtStackMacAddress ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,tMacAddr * ));

/* Proto Validate Index Instance for FsMIEcfmExtDefaultMdTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmExtDefaultMdTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmExtDefaultMdTable  */

INT1
nmhGetFirstIndexFsMIEcfmExtDefaultMdTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmExtDefaultMdTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmExtDefaultMdStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtDefaultMdLevel ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtDefaultMdMhfCreation ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtDefaultMdIdPermission ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmExtDefaultMdLevel ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtDefaultMdMhfCreation ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtDefaultMdIdPermission ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmExtDefaultMdLevel ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtDefaultMdMhfCreation ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtDefaultMdIdPermission ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmExtDefaultMdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmExtConfigErrorListTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmExtConfigErrorListTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmExtConfigErrorListTable  */

INT1
nmhGetFirstIndexFsMIEcfmExtConfigErrorListTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmExtConfigErrorListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmExtConfigErrorListErrorType ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIEcfmExtMaTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmExtMaTable ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmExtMaTable  */

INT1
nmhGetFirstIndexFsMIEcfmExtMaTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmExtMaTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmExtMaFormat ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMaName ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMaMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMaIdPermission ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMaCcmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMaNumberOfVids ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMaRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMaCrosscheckStatus ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmExtMaFormat ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMaName ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmExtMaMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMaIdPermission ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMaCcmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMaRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMaCrosscheckStatus ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmExtMaFormat ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMaName ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmExtMaMhfCreation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMaIdPermission ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMaCcmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMaRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMaCrosscheckStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmExtMaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmExtMipTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmExtMipTable ARG_LIST((INT4  , INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmExtMipTable  */

INT1
nmhGetFirstIndexFsMIEcfmExtMipTable ARG_LIST((INT4 * , INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmExtMipTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmExtMipActive ARG_LIST((INT4  , INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMipRowStatus ARG_LIST((INT4  , INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmExtMipActive ARG_LIST((INT4  , INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMipRowStatus ARG_LIST((INT4  , INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmExtMipActive ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMipRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmExtMipTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmExtMepTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmExtMepTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmExtMepTable  */

INT1
nmhGetFirstIndexFsMIEcfmExtMepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmExtMepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmExtMepIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepPrimaryVidOrIsid ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepActive ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepFngState ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepCcmLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmExtMepLowPrDef ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepFngAlarmTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepFngResetTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepHighestPrDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepDefects ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMepErrorCcmLastFailure ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMepXconCcmLastFailure ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepNextLbmTransId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepLtmNextSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmDataTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLbmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmResult ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmExtMepTransmitLtmEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmExtMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmExtMepCcmOffload ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmExtMepIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepPrimaryVidOrIsid ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepActive ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepCcmLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepLowPrDef ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepFngAlarmTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepFngResetTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmDataTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmExtMepTransmitLtmEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmExtMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmExtMepCcmOffload ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmExtMepIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepDirection ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepPrimaryVidOrIsid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepActive ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepCciEnabled ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepCcmLtmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepLowPrDef ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepFngAlarmTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepFngResetTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepCcmSequenceErrors ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepCciSentCcms ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepLbrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepLbrInOutOfOrder ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepLbrBadMsdu ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepUnexpLtrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepLbrOut ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmDataTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmFlags ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTargetMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTargetIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmTtl ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmExtMepTransmitLtmEgressIdentifier ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmExtMepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmExtMepCcmOffload ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmExtMepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
