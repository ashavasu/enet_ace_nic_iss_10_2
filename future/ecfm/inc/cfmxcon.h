/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmxcon.h,v 1.3 2007/11/19 07:21:10 iss Exp $
 * 
 * Description: This file contains the functionality of the
 *              XCON state machine.
 *********************************************************************/
#ifndef __CFM_XCON_H
#define __CFM_XCON_H

/* routines that handles the occurence of the events */

/* XCON State machine */
PRIVATE INT4
EcfmXconSmSetStateDefault PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmXconSmSetStateNoDefect PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmXconSmSetStateDefect PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmXconSmSetStateDefFrmDef PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmXconSmEvtImpossible PROTO((tEcfmCcPduSmInfo * ));

/* Enums for the routines to be retrieve from the the matrix*/
/* Enums for the routines to be retrieve from the the matrix*/
enum
{
   XCON0,               /* EcfmXconSmSetStateDefault */
   XCON1,               /* EcfmXconSmSetStateDefect */
   XCON2,               /* EcfmXconSmSetStateDefFrmDef */
   XCON3,               /* EcfmXconSmSetStateNoDefect */
   XCON4,               /* EcfmXconSmEvtImpossible */
   ECFM_MEP_XCON_FN_PTRS
};

/* function pointers of  Cross Connect MEP State machine */
INT4 (*gaEcfmLmepXconActionProc[ECFM_MEP_XCON_FN_PTRS]) (tEcfmCcPduSmInfo *) =
{
   EcfmXconSmSetStateDefault,
   EcfmXconSmSetStateDefect,
   EcfmXconSmSetStateDefFrmDef,
   EcfmXconSmSetStateNoDefect,
   EcfmXconSmEvtImpossible
};

/* State Event Table of XCON RX */
const UINT1
gau1EcfmLmepXconSem[ECFM_SM_XCON_MAX_EVENTS][ECFM_MEP_XCON_MAX_STATES] =
/* DEFAULT  NODEFECT  DEFECT    States */
{                               /* Events */
 { XCON3,   XCON4,   XCON4},    /* ECFM_SM_EV_BEGIN */
 { XCON4,   XCON0,   XCON0},    /* ECFM_MS_EV_MEP_NOT_ACTIVE */
 { XCON4,   XCON1,   XCON2},    /* ECFM_SM_EV_XCON_CCM_RCVD */
 { XCON4,   XCON4,   XCON3},    /* ECFM_SM_EV_XCON_TIMEOUT */
};
#endif /* __CFM_XCON_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmxcon.h                      */
/*-----------------------------------------------------------------------*/

