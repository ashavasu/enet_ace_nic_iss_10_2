/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlbglb.h,v 1.8 2012/01/20 13:18:35 siva Exp $
 * 
 * Description: This file contains various global variables for 
 *              LBLTTask.
 *********************************************************************/

#ifndef _CFM_LBLT_GLOB_H
#define _CFM_LBLT_GLOB_H

tEcfmLbLtGlobalInfo gEcfmLbLtGlobalInfo;
tEcfmLbLtContextInfo *gpEcfmLbLtContextInfo=NULL;
UINT1 gu1EcfmLbLtInitialised = ECFM_FALSE;
tEcfmLbLtCliEvInfo  gaEcfmLbLtCliEventInfo[ECFM_CLI_MAX_SESSIONS+1];
tEcfmLbLtCliEvInfo *gapEcfmLbLtCliEventInfo[ECFM_CLI_MAX_SESSIONS+1]={NULL};
tEcfmLbLtMepInfo   *gpEcfmLbLtMepNode = NULL;
UINT1               gau1EcfmLbLtPdu [ECFM_MAX_JUMBO_PDU_SIZE];
UINT1               gau1EcfmLbltTstPdu [ECFM_MAX_JUMBO_PDU_SIZE];
#endif /* _CFM_LBLT_GLOB_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmlbglob.h                     */
/*-----------------------------------------------------------------------*/
