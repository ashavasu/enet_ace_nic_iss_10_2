/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmred.h,v 1.22 2016/05/19 10:55:39 siva Exp $
 * 
 * Description: This file contains the constant definations for both 
 *              Redunancy manager for ECFM.
 *********************************************************************/

#ifndef _CFM_RED_H
#define _CFM_RED_H


typedef struct EcfmRedHwAuditInfo {
    UINT4               u4CmdEvnt; /*Command/Event currently
                                     handled in Active node*/
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4MdId;
    UINT4               u4MaId;
    UINT4               u4MepId;
    tVlanId             VlanId;
    BOOL1               bMdCmd;
    BOOL1               bMaCmd;
    BOOL1               bError;
    UINT1               au1Pad[3];
}tEcfmRedHwAuditInfo;
typedef struct EcfmRedGlobalInfo {
    tEcfmRedHwAuditInfo  HwAuditInfo;
    tOsixTaskId          EcfmAuditTaskId;
    UINT4                u4BulkUpdNextContext;
    UINT2                u2BulkUpdNextPort;
    /* Indicates number of standby nodes that are up. */
    UINT1                u1NumPeersUp;
    UINT1                u1AuditFlag;
    /* Indicates which CC Task Info needs to be updated */
    BOOL1                b1BulkReqRcvd;  /* To check whether bulk request
                                          * rcvd from standby before
                                          * RM_STANDBY_UP event. */
   
    /* HITLESS RESTART */
    /* To check whether steady state request received from RM*/
    UINT1                u1StdyStReqRcvd;
      UINT1                au1Pad[2];
}tEcfmRedGlobalInfo;

/* Contants used  for ECFM Redundancy */
#define ECFM_RM_GET_NODE_STATUS()  EcfmRedGetNodeStateFromRm ()
#define ECFM_NODE_STATUS()         gEcfmNodeStatus
#define ECFM_BULK_REQ_RECD()         gEcfmRedGlobalInfo.b1BulkReqRcvd

#define ECFM_RED_TYPE_FIELD_SIZE       1
#define ECFM_RED_LEN_FIELD_SIZE        2 /* Number of bytes occupied by sync
                                          * mesg length field */
#define ECFM_RED_TYPE_LEN_FIELD_SIZE   ECFM_RED_TYPE_FIELD_SIZE +\
                                       ECFM_RED_LEN_FIELD_SIZE 

#define ECFM_PORT_ID_SIZE              4
#define ECFM_PORT_STATUS_SIZE          1
#define ECFM_CONTEXT_ID_SIZE           4

#define ECFM_RED_MAX_MSG_SIZE          1500
#define ECFM_RED_SYNC_SEQ_NO_INTERVAL  100
#define ECFM_RM_FRAME                  (ECFM_MAX_EVENT+1)

/*SubUpdate for port*/
#define ECFM_RED_NO_OF_PORTS_PER_SUB_UPDATE 120
#define ECFM_RED_SYNC_LB_TST_TRAN_SIZE 11
/* Message Type */
#define ECFM_PORT_OPER_STATUS_MSG      4
#define ECFM_BULK_REQ_MSG              RM_BULK_UPDT_REQ_MSG
#define ECFM_BULK_UPD_TAIL_MSG         RM_BULK_UPDT_TAIL_MSG
#define ECFM_RED_SYNC_UPD_MSG          5
#define ECFM_RED_MIP_DB_HLD_TMR        6
#define ECFM_RED_SYNC_LBLT_PDU         7
#define ECFM_RED_LTR_CACHE_HLD_TMR     8
#define ECFM_RED_DELAY_QUEUE_TIMER_EXPIRY 9
#define ECFM_RED_LTT_TIMER_EXPIRY      10
#define ECFM_RED_LBI_TIMER_EXPIRY      11
#define ECFM_RED_LBR_CACHE_HLD_TMR     12
#define ECFM_RED_SYNC_LB_TST_TRAN_TYPE 13
#define ECFM_RED_SYNC_STOP_LB_TRAN     14
#define ECFM_RED_SYNC_STOP_TST_TRAN    15
#define ECFM_RED_LBI_STOP_TX           16
#define ECFM_RED_TST_TIMER_EXPIRY      17
#define ECFM_RED_TST_STOP_TX           18
#define ECFM_RED_PROTO_INDI_SYNC_TYPE  19
#define ECFM_RED_RDI_PERIOD_TIMER_EXPIRY  20 
#define ECFM_RED_ERROR_LOG_ENTRY          21 
#define ECFM_RED_LCK_CONDITION            22 
#define ECFM_RED_LCK_STATUS               23
#define ECFM_RED_LCK_PERIOD_TIMER_EXPIRY  24 
#define ECFM_RED_LCK_RXWHILE_TIMER_EXPIRY 25 
#define ECFM_RED_AIS_CONDITION            26 
#define ECFM_RED_AIS_PERIOD_TIMER_EXPIRY  27
#define ECFM_RED_AIS_RXWHILE_TIMER_EXPIRY 28 
#define ECFM_RED_LM_STOP_TRANSACTION      29 
#define ECFM_RED_LM_DEADLINE_TIMER_EXPIRY 30 
#define ECFM_RED_DM_DEADLINE_TIMER_EXPIRY 31 
#define ECFM_RED_DM_STOP_TRANSACTION      32 
#define ECFM_RED_1DM_TRANS_INT_EXPIRY     33 
#define ECFM_RED_LBLT_SYNC_UPD_MSG        34
#define ECFM_RED_SYNC_LM_TRANS_STS        35 
#define ECFM_RED_LBLT_SYNC_TRANS_STS      36 
#define ECFM_RED_SYNC_ERR_LOG_MSG         37
#define ECFM_RED_LBR_CACHE_BACKUP_MSG     38
#define ECFM_RED_STOP_THROUGHPUT_TRANS    39
#define ECFM_IMMEDIATE_SYNCH_MSG          40
#define ECFM_STATE_MACHINE_DATA_MSG       41
#define ECFM_RED_SYNCH_RMEP_CNTR_MSG      42
#define ECFM_RED_SYNC_TMR                 43
#define ECFM_RED_SYNCH_TX_SEQ_NO_MSG      44
#define ECFM_RED_SYNCH_HW_ADT_INFO        45
#define ECFM_RED_SYNCH_TX_FILTER_ID       46
#define ECFM_RED_SYNCH_RX_FILTER_ID       47
#define ECFM_RED_SYNCH_HW_RX_HANDLER      48
#define ECFM_RED_SYNCH_HW_TX_HANDLER      49
#define ECFM_RED_SYNC_AVLBLTY_INFO        50


/* TLV Value */
#define ECFM_RED_MEP_INFO           1
#define ECFM_RED_RMEP_INFO          2
#define ECFM_RED_MIP_DB_INFO        3
#define ECFM_RED_LM_BUFF_INFO       4
#define ECFM_RED_DM_BUFF_INFO       5
#define ECFM_RED_LB_BUFF_INFO       6
#define ECFM_RED_ERR_LOG_INFO       7


#define ECFM_RED_ERR_LOG_INFO_SIZE     26 

#define ECFM_RED_MEP_INFO_SIZE         98 

#define ECFM_RED_MEP_Y1731_INFO_SIZE   164

#define ECFM_RED_MIP_DB_INFO_SIZE      16 
#define ECFM_RED_RMEP_INFO_SIZE        46 

#define ECFM_RED_LM_COMMON_INFO_SIZE     28
#define ECFM_RED_LM_BUFF_INFO_SIZE       21 
#define ECFM_RED_LM_BUFF_FULL_INFO_SIZE   (ECFM_RED_LM_COMMON_INFO_SIZE + \
                                           ECFM_RED_LM_BUFF_INFO_SIZE) 

#define ECFM_RED_DM_COMMON_INFO_SIZE     28
#define ECFM_RED_DM_BUFF_INFO_SIZE       43 
#define ECFM_RED_DM_BUFF_FULL_INFO_SIZE   (ECFM_RED_DM_COMMON_INFO_SIZE + \
                                           ECFM_RED_DM_BUFF_INFO_SIZE) 

#define ECFM_RED_LBM_BUFF_INFO_SIZE   48 

#define ECFM_RED_LBR_BUFF_INFO_SIZE   13 

#define ECFM_BULK_REQ_MSG_SIZE         3

#define ECFM_PORT_OPER_STATUS_MSG_SIZE            ECFM_PORT_ID_SIZE +\
                                                  ECFM_PORT_STATUS_SIZE

#define ECFM_LBR_CACHE_STATUS_BCKUP_MSG_SIZE      5

#define ECFM_MIB_DB_HLD_TMR_MSG_SIZE    8
#define ECFM_LTR_CACHE_HLD_TMR_MSG_SIZE 8 
#define ECFM_LBR_CACHE_HLD_TMR_MSG_SIZE 8 
#define ECFM_DELAY_QUEUE_EXPIRY_MSG_SIZE  4
#define  ECFM_LBLT_MEP_EXPIRY_MSG_SIZE  16
#define ECFM_STOP_TRANS_MSG_SIZE        16
#define ECFM_RED_PROTO_INDI_SYNC_SIZE  17
#define ECFM_RDI_PERIOD_EXPIRY_MSG_SIZE  14
#define ECFM_ERROR_LOG_ENTRY_MSG_SIZE    26
#define ECFM_LCK_STATUS_MSG_SIZE         14
#define ECFM_STATE_MACHINE_DATA_MSG_SIZE 69
#define ECFM_IMMEDIATE_SYNCH_MSG_SIZE    31
#define ECFM_LCK_CONDITION_MSG_SIZE      19
#define ECFM_LCK_PERIOD_EXPIRY_MSG_SIZE  14
#define ECFM_LCK_RXWHILE_EXPIRY_MSG_SIZE 14
#define ECFM_AIS_CONDITION_MSG_SIZE      19
#define ECFM_RED_SYNCH_TMR_SIZE          20
#define ECFM_AIS_PERIOD_EXPIRY_MSG_SIZE  14
#define ECFM_RED_SYNCH_RMEP_CNTR_MSG_SIZE  17
#define ECFM_RED_SYNCH_TX_SEQ_NO_MSG_SIZE  18
#define ECFM_AIS_RXWHILE_EXPIRY_MSG_SIZE 14
#define ECFM_LM_DEADLINE_EXPIRY_MSG_SIZE 14
#define ECFM_LM_STOP_TRANSACTION_MSG_SIZE 14
#define ECFM_DM_STOP_TRANSACTION_MSG_SIZE 14
#define ECFM_DM_DEADLINE_EXPIRY_MSG_SIZE 14
#define ECFM_1DM_TRANS_INT_EXPIRY_MSG_SIZE 14
#define ECFM_SYNC_LM_TRANS_MSG_SIZE        15
#define ECFM_SYNC_LBLT_TRANS_MSG_SIZE      18
#define ECFM_RED_SYNCH_HW_ADT_INFO_MSG_SIZE 27
#define ECFM_RED_SYNCH_TX_FILTER_ID_MSG_SIZE 20
#define ECFM_RED_SYNCH_RX_FILTER_ID_MSG_SIZE 24
#define ECFM_RED_SYNCH_HW_TX_HANDLER_MSG_SIZE (16 + ECFM_HW_MEP_HANDLER_SIZE)
#define ECFM_RED_SYNCH_HW_RX_HANDLER_MSG_SIZE (20 + ECFM_HW_MEP_HANDLER_SIZE)
#define ECFM_RED_SYNC_START_TMR            1
#define ECFM_RED_SYNC_EXPIRY_TMR           2
#define ECFM_RED_SYNC_STOP_TMR             3
#define ECFM_AVLBLTY_INFO_MSG_SIZE         51

/*Macros related to Hardware Audit*/
#define ECFM_RED_MD_ROW_STS_CMD            ECFM_MAX_EVENT + 1
#define ECFM_RED_MA_ROW_STS_CMD            ECFM_MAX_EVENT + 2
#define ECFM_RED_MEP_ROW_STS_CMD           ECFM_MAX_EVENT + 3
#define ECFM_RED_SHUTDOWN_CMD              ECFM_MAX_EVENT + 4
#define ECFM_RED_MOD_STS_CHNG_CMD          ECFM_MAX_EVENT + 5
#define ECFM_RED_PORT_LLC_CMD              ECFM_MAX_EVENT + 6
#define ECFM_RED_PORT_MOD_STS_CMD          ECFM_MAX_EVENT + 7
#define ECFM_RED_GLOBAL_OFF_CMD            ECFM_MAX_EVENT + 8
#define ECFM_RED_Y1731_MOD_STS_CMD         ECFM_MAX_EVENT + 9
#define ECFM_RED_OUI_CHANGE_CMD            ECFM_MAX_EVENT + 10
#define ECFM_RED_LB_NPAPI                  ECFM_MAX_EVENT + 11
#define ECFM_RED_TST_NPAPI                 ECFM_MAX_EVENT + 12
#define ECFM_RED_AUDIT_NOT_STARTED         1
#define ECFM_RED_AUDIT_STARTED             2
#define ECFM_RED_AUDIT_COMPLETED           3
#define ECFM_RED_AUDIT_STOPPED             4
#define ECFM_RED_AUDIT_START_EVENT         5

#define ECFM_RED_AUDIT_FLAG() (gEcfmRedGlobalInfo.u1AuditFlag)
#define ECFM_AUDIT_TASK_ID    gEcfmRedGlobalInfo.EcfmAuditTaskId
#define ECFM_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define ECFM_AUDIT_TASK            ((UINT1*)"ECAU")

#define ECFM_SPAWN_TASK                OsixTskCrt
#define ECFM_DELETE_TASK               OsixTskDel

#define ECFM_IS_STANDBY_UP() \
        ((gEcfmRedGlobalInfo.u1NumPeersUp > 0) ? ECFM_TRUE : ECFM_FALSE)

#define ECFM_NUM_STANDBY_NODES() gEcfmRedGlobalInfo.u1NumPeersUp
#define ECFM_INIT_NUM_PEERS() gEcfmRedGlobalInfo.u1NumPeersUp =0;

#define ECFM_RM_GET_NUM_PEERS_UP() \
      gEcfmRedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#ifdef RM_WANTED
#define  ECFM_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? ECFM_TRUE: ECFM_FALSE)
#else
#define  ECFM_IS_NP_PROGRAMMING_ALLOWED() ECFM_TRUE
#endif

/* HITLESS RESTART */
#define ECFM_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ
#define ECFM_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define ECFM_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL
#define ECFM_HR_STATUS()               EcfmRedGetHRFlag()
#define ECFM_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define ECFM_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                           * protocols can start writing. */
#define ECFM_HR_STDY_ST_REQ_RCVD()     gEcfmRedGlobalInfo.u1StdyStReqRcvd

/* Macros to write in to RM buffer. */
#define ECFM_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), (u4MesgType)); \
        *(pu4Offset) += 4;\
}while (0)

#define ECFM_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), (u2MesgType)); \
        *(pu4Offset) += 2;\
}while (0)

#define ECFM_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), (u1MesgType)); \
        *(pu4Offset) += 1;\
}while (0)

#define ECFM_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET((pdest), (psrc), *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define ECFM_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ECFM_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ECFM_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ECFM_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)
/*****************************************************************************/
#endif /*_CFM_RED*/
/******************************************************************************
  End of File cfmred.h
 *****************************************************************************/
