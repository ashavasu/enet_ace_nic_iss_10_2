/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmext.h,v 1.18 2016/02/18 08:32:27 siva Exp $
 * 
 * Description: This file contains various common extern variables 
 *              declared used in CFM.
 *********************************************************************/
#ifndef _CFM_EXTN_H
#define _CFM_EXTN_H
/******************************************************************************/
#ifdef L2RED_WANTED
PUBLIC tEcfmRedGlobalInfo gEcfmRedGlobalInfo;
#endif
PUBLIC tEcfmNodeStatus    gEcfmNodeStatus; 
extern UINT1 gau1EcfmSystemStatus[SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT1 gau1EcfmModuleStatus[SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT1 gau1EcfmY1731Status[SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT4 gau4EcfmTraceOption[SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT4 gu4EcfmGlobalTrace;
extern UINT4 gaau4TransportDomain[ECFM_MAX_TRANSPORT_DOMAIN][ECFM_MAX_TRANSPORT_DOMAIN_LEN];
extern tEcfmMepPromptInfo  gaEcfmMepPromptInfo[ECFM_CLI_MAX_SESSIONS];
extern UINT4 gu4CfmCrcTable [256];
extern UINT2 gu2CfmCrcTable [256];
extern BOOLEAN gbEcfmMbsmEvtHndl;
extern UINT1 gaau1EcfmBitListScanMask[256][8];
extern tFsModSizingParams gFsEcfmSizingParams [];
extern tFsModSizingInfo gFsEcfmSizingInfo;
#endif /* _CFM_EXTN_H*/
/*******************************************************************************
                            End of File cfmext.h                              
 ******************************************************************************/

