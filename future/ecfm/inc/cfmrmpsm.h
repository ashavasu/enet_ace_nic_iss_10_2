/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmrmpsm.h,v 1.4 2008/05/14 07:05:03 prabuc-iss Exp $
 *
 * Description: This file contains state-machine for Remote MEP.
 *************************************************************************/
#ifndef __CFM_RM_SM_H
#define __CFM_RM_SM_H

/* routines that handles the occurence of the events */

/* RMEP State machine */
PRIVATE INT4
EcfmRmepSmSetStateDefault PROTO((tEcfmCcPduSmInfo * ));

PRIVATE INT4
EcfmRmepSmSetStateStart PROTO((tEcfmCcPduSmInfo * ));

PRIVATE INT4
EcfmRmepSmSetStateOk PROTO((tEcfmCcPduSmInfo * ));

PRIVATE INT4
EcfmRmepSmSetStateFailed PROTO((tEcfmCcPduSmInfo * ));

PRIVATE INT4
EcfmRmepSmEvtImpossible PROTO((tEcfmCcPduSmInfo * ));

/* Enums for the routines to be retrieve from the the matrix*/
enum
{
  RMEP0,    /* EcfmRmepSmSetStateStart */
  RMEP1,    /* EcfmRmepSmSetStateDefault */
  RMEP2,    /* EcfmRMepSmSetStateOk */
  RMEP3,    /* EcfmRMepSmSetStateFailed */
  RMEP4,    /* EcfmRMepSmStateEvtImpossible */
 ECFM_RMEP_MAX_FN_PTRS
};

/* function pointers of  RMEP State machine */
INT4 (*gaEcfmRmepActionProc[ECFM_RMEP_MAX_FN_PTRS]) (tEcfmCcPduSmInfo *) =
{
 EcfmRmepSmSetStateStart,
 EcfmRmepSmSetStateDefault,
 EcfmRmepSmSetStateOk,
 EcfmRmepSmSetStateFailed,
 EcfmRmepSmEvtImpossible
};

/* State Event Table of RMEP RX */
const UINT1 gau1EcfmRmepSem[ECFM_SM_RMEP_MAX_EVENTS][ECFM_RMEP_MAX_STATES] =
/* DEFAULT  START  FAILED  OK       States */
{                                    /* Events */
   { RMEP0, RMEP4, RMEP4, RMEP4},    /* ECFM_SM_EV_BEGIN */
   { RMEP4, RMEP1, RMEP1, RMEP1},    /* ECFM_SM_EV_MEP_NOT_ACTIVE */
   { RMEP4, RMEP2, RMEP2, RMEP2},    /* ECFM_SM_EV_RMEP_CCM_RCVD */
   { RMEP4, RMEP3, RMEP3, RMEP3},    /* ECFM_SM_EV_RMEP_TIMEOUT */
   { RMEP4, RMEP0, RMEP0, RMEP0},    /* ECFM_SM_EV_RMEP_NO_DEFECT */
};
#endif /* __CFM_RM_SM_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmrmsm.h                        */
/*-----------------------------------------------------------------------*/

