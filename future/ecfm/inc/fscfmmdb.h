/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmmdb.h,v 1.14 2013/11/09 10:23:19 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSCFMMDB_H
#define _FSCFMMDB_H

UINT1 FsMIEcfmContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmStackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmDefaultMdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmConfigErrorListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmMdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMaTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMaMepListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMepTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmLtrTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMepDbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMipTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmDynMipPreventionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmMipCcmDbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIEcfmRemoteMepDbExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmLtmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMepExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMdExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmMaExTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fscfmm [] ={1,3,6,1,4,1,2076,160};
tSNMP_OID_TYPE fscfmmOID = {8, fscfmm};


UINT4 FsMIEcfmGlobalTrace [ ] ={1,3,6,1,4,1,2076,160,1,1,1};
UINT4 FsMIEcfmOui [ ] ={1,3,6,1,4,1,2076,160,1,1,2};
UINT4 FsMIEcfmContextId [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,1};
UINT4 FsMIEcfmSystemControl [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,2};
UINT4 FsMIEcfmModuleStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,3};
UINT4 FsMIEcfmDefaultMdDefLevel [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,4};
UINT4 FsMIEcfmDefaultMdDefMhfCreation [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,5};
UINT4 FsMIEcfmDefaultMdDefIdPermission [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,6};
UINT4 FsMIEcfmMdTableNextIndex [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,7};
UINT4 FsMIEcfmLtrCacheStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,8};
UINT4 FsMIEcfmLtrCacheClear [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,9};
UINT4 FsMIEcfmLtrCacheHoldTime [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,10};
UINT4 FsMIEcfmLtrCacheSize [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,11};
UINT4 FsMIEcfmMipCcmDbStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,12};
UINT4 FsMIEcfmMipCcmDbClear [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,13};
UINT4 FsMIEcfmMipCcmDbSize [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,14};
UINT4 FsMIEcfmMipCcmDbHoldTime [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,15};
UINT4 FsMIEcfmMemoryFailureCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,16};
UINT4 FsMIEcfmBufferFailureCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,17};
UINT4 FsMIEcfmUpCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,18};
UINT4 FsMIEcfmDownCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,19};
UINT4 FsMIEcfmNoDftCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,20};
UINT4 FsMIEcfmRdiDftCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,21};
UINT4 FsMIEcfmMacStatusDftCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,22};
UINT4 FsMIEcfmRemoteCcmDftCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,23};
UINT4 FsMIEcfmErrorCcmDftCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,24};
UINT4 FsMIEcfmXconDftCount [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,25};
UINT4 FsMIEcfmCrosscheckDelay [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,26};
UINT4 FsMIEcfmMipDynamicEvaluationStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,27};
UINT4 FsMIEcfmContextName [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,28};
UINT4 FsMIEcfmTrapControl [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,29};
UINT4 FsMIEcfmTrapType [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,30};
UINT4 FsMIEcfmTraceOption [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,31};
UINT4 FsMIEcfmGlobalCcmOffload [ ] ={1,3,6,1,4,1,2076,160,1,0,1,1,32};
UINT4 FsMIEcfmPortIfIndex [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,1};
UINT4 FsMIEcfmPortLLCEncapStatus [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,2};
UINT4 FsMIEcfmPortModuleStatus [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,3};
UINT4 FsMIEcfmPortTxCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,4};
UINT4 FsMIEcfmPortTxCcmCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,5};
UINT4 FsMIEcfmPortTxLbmCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,6};
UINT4 FsMIEcfmPortTxLbrCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,7};
UINT4 FsMIEcfmPortTxLtmCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,8};
UINT4 FsMIEcfmPortTxLtrCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,9};
UINT4 FsMIEcfmPortTxFailedCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,10};
UINT4 FsMIEcfmPortRxCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,11};
UINT4 FsMIEcfmPortRxCcmCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,12};
UINT4 FsMIEcfmPortRxLbmCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,13};
UINT4 FsMIEcfmPortRxLbrCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,14};
UINT4 FsMIEcfmPortRxLtmCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,15};
UINT4 FsMIEcfmPortRxLtrCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,16};
UINT4 FsMIEcfmPortRxBadCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,17};
UINT4 FsMIEcfmPortFrwdCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,18};
UINT4 FsMIEcfmPortDsrdCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,1,3,1,19};
UINT4 FsMIEcfmStackIfIndex [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,1};
UINT4 FsMIEcfmStackVlanIdOrNone [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,2};
UINT4 FsMIEcfmStackMdLevel [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,3};
UINT4 FsMIEcfmStackDirection [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,4};
UINT4 FsMIEcfmStackMdIndex [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,5};
UINT4 FsMIEcfmStackMaIndex [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,6};
UINT4 FsMIEcfmStackMepId [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,7};
UINT4 FsMIEcfmStackMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,1,4,1,8};
UINT4 FsMIEcfmVlanVid [ ] ={1,3,6,1,4,1,2076,160,1,0,2,1,1};
UINT4 FsMIEcfmVlanPrimaryVid [ ] ={1,3,6,1,4,1,2076,160,1,0,2,1,2};
UINT4 FsMIEcfmVlanRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,2,1,3};
UINT4 FsMIEcfmDefaultMdPrimaryVid [ ] ={1,3,6,1,4,1,2076,160,1,0,3,1,1};
UINT4 FsMIEcfmDefaultMdStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,3,1,2};
UINT4 FsMIEcfmDefaultMdLevel [ ] ={1,3,6,1,4,1,2076,160,1,0,3,1,3};
UINT4 FsMIEcfmDefaultMdMhfCreation [ ] ={1,3,6,1,4,1,2076,160,1,0,3,1,4};
UINT4 FsMIEcfmDefaultMdIdPermission [ ] ={1,3,6,1,4,1,2076,160,1,0,3,1,5};
UINT4 FsMIEcfmConfigErrorListVid [ ] ={1,3,6,1,4,1,2076,160,1,1,5,1,1};
UINT4 FsMIEcfmConfigErrorListIfIndex [ ] ={1,3,6,1,4,1,2076,160,1,1,5,1,2};
UINT4 FsMIEcfmConfigErrorListErrorType [ ] ={1,3,6,1,4,1,2076,160,1,1,5,1,3};
UINT4 FsMIEcfmMdIndex [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,1};
UINT4 FsMIEcfmMdFormat [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,2};
UINT4 FsMIEcfmMdName [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,3};
UINT4 FsMIEcfmMdMdLevel [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,4};
UINT4 FsMIEcfmMdMhfCreation [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,5};
UINT4 FsMIEcfmMdMhfIdPermission [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,6};
UINT4 FsMIEcfmMdMaTableNextIndex [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,7};
UINT4 FsMIEcfmMdRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,4,1,8};
UINT4 FsMIEcfmMaIndex [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,1};
UINT4 FsMIEcfmMaPrimaryVlanId [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,2};
UINT4 FsMIEcfmMaFormat [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,3};
UINT4 FsMIEcfmMaName [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,4};
UINT4 FsMIEcfmMaMhfCreation [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,5};
UINT4 FsMIEcfmMaIdPermission [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,6};
UINT4 FsMIEcfmMaCcmInterval [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,7};
UINT4 FsMIEcfmMaNumberOfVids [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,8};
UINT4 FsMIEcfmMaRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,5,1,9};
UINT4 FsMIEcfmMaMepListIdentifier [ ] ={1,3,6,1,4,1,2076,160,1,0,6,1,1};
UINT4 FsMIEcfmMaMepListRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,6,1,2};
UINT4 FsMIEcfmMepIdentifier [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,1};
UINT4 FsMIEcfmMepIfIndex [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,2};
UINT4 FsMIEcfmMepDirection [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,3};
UINT4 FsMIEcfmMepPrimaryVid [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,4};
UINT4 FsMIEcfmMepActive [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,5};
UINT4 FsMIEcfmMepFngState [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,6};
UINT4 FsMIEcfmMepCciEnabled [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,7};
UINT4 FsMIEcfmMepCcmLtmPriority [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,8};
UINT4 FsMIEcfmMepMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,9};
UINT4 FsMIEcfmMepLowPrDef [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,10};
UINT4 FsMIEcfmMepFngAlarmTime [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,11};
UINT4 FsMIEcfmMepFngResetTime [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,12};
UINT4 FsMIEcfmMepHighestPrDefect [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,13};
UINT4 FsMIEcfmMepDefects [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,14};
UINT4 FsMIEcfmMepErrorCcmLastFailure [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,15};
UINT4 FsMIEcfmMepXconCcmLastFailure [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,16};
UINT4 FsMIEcfmMepCcmSequenceErrors [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,17};
UINT4 FsMIEcfmMepCciSentCcms [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,18};
UINT4 FsMIEcfmMepNextLbmTransId [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,19};
UINT4 FsMIEcfmMepLbrIn [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,20};
UINT4 FsMIEcfmMepLbrInOutOfOrder [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,21};
UINT4 FsMIEcfmMepLbrBadMsdu [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,22};
UINT4 FsMIEcfmMepLtmNextSeqNumber [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,23};
UINT4 FsMIEcfmMepUnexpLtrIn [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,24};
UINT4 FsMIEcfmMepLbrOut [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,25};
UINT4 FsMIEcfmMepTransmitLbmStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,26};
UINT4 FsMIEcfmMepTransmitLbmDestMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,27};
UINT4 FsMIEcfmMepTransmitLbmDestMepId [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,28};
UINT4 FsMIEcfmMepTransmitLbmDestIsMepId [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,29};
UINT4 FsMIEcfmMepTransmitLbmMessages [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,30};
UINT4 FsMIEcfmMepTransmitLbmDataTlv [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,31};
UINT4 FsMIEcfmMepTransmitLbmVlanPriority [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,32};
UINT4 FsMIEcfmMepTransmitLbmVlanDropEnable [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,33};
UINT4 FsMIEcfmMepTransmitLbmResultOK [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,34};
UINT4 FsMIEcfmMepTransmitLbmSeqNumber [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,35};
UINT4 FsMIEcfmMepTransmitLtmStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,36};
UINT4 FsMIEcfmMepTransmitLtmFlags [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,37};
UINT4 FsMIEcfmMepTransmitLtmTargetMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,38};
UINT4 FsMIEcfmMepTransmitLtmTargetMepId [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,39};
UINT4 FsMIEcfmMepTransmitLtmTargetIsMepId [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,40};
UINT4 FsMIEcfmMepTransmitLtmTtl [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,41};
UINT4 FsMIEcfmMepTransmitLtmResult [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,42};
UINT4 FsMIEcfmMepTransmitLtmSeqNumber [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,43};
UINT4 FsMIEcfmMepTransmitLtmEgressIdentifier [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,44};
UINT4 FsMIEcfmMepRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,45};
UINT4 FsMIEcfmMepCcmOffload [ ] ={1,3,6,1,4,1,2076,160,1,0,7,1,46};
UINT4 FsMIEcfmLtrSeqNumber [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,1};
UINT4 FsMIEcfmLtrReceiveOrder [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,2};
UINT4 FsMIEcfmLtrTtl [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,3};
UINT4 FsMIEcfmLtrForwarded [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,4};
UINT4 FsMIEcfmLtrTerminalMep [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,5};
UINT4 FsMIEcfmLtrLastEgressIdentifier [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,6};
UINT4 FsMIEcfmLtrNextEgressIdentifier [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,7};
UINT4 FsMIEcfmLtrRelay [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,8};
UINT4 FsMIEcfmLtrChassisIdSubtype [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,9};
UINT4 FsMIEcfmLtrChassisId [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,10};
UINT4 FsMIEcfmLtrManAddressDomain [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,11};
UINT4 FsMIEcfmLtrManAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,12};
UINT4 FsMIEcfmLtrIngress [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,13};
UINT4 FsMIEcfmLtrIngressMac [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,14};
UINT4 FsMIEcfmLtrIngressPortIdSubtype [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,15};
UINT4 FsMIEcfmLtrIngressPortId [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,16};
UINT4 FsMIEcfmLtrEgress [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,17};
UINT4 FsMIEcfmLtrEgressMac [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,18};
UINT4 FsMIEcfmLtrEgressPortIdSubtype [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,19};
UINT4 FsMIEcfmLtrEgressPortId [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,20};
UINT4 FsMIEcfmLtrOrganizationSpecificTlv [ ] ={1,3,6,1,4,1,2076,160,1,0,8,1,21};
UINT4 FsMIEcfmMepDbRMepIdentifier [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,1};
UINT4 FsMIEcfmMepDbRMepState [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,2};
UINT4 FsMIEcfmMepDbRMepFailedOkTime [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,3};
UINT4 FsMIEcfmMepDbMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,4};
UINT4 FsMIEcfmMepDbRdi [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,5};
UINT4 FsMIEcfmMepDbPortStatusTlv [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,6};
UINT4 FsMIEcfmMepDbInterfaceStatusTlv [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,7};
UINT4 FsMIEcfmMepDbChassisIdSubtype [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,8};
UINT4 FsMIEcfmMepDbChassisId [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,9};
UINT4 FsMIEcfmMepDbManAddressDomain [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,10};
UINT4 FsMIEcfmMepDbManAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,9,1,11};
UINT4 FsMIEcfmMipIfIndex [ ] ={1,3,6,1,4,1,2076,160,1,1,6,1,1};
UINT4 FsMIEcfmMipMdLevel [ ] ={1,3,6,1,4,1,2076,160,1,1,6,1,2};
UINT4 FsMIEcfmMipVid [ ] ={1,3,6,1,4,1,2076,160,1,1,6,1,3};
UINT4 FsMIEcfmMipActive [ ] ={1,3,6,1,4,1,2076,160,1,1,6,1,4};
UINT4 FsMIEcfmMipRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,1,6,1,5};
UINT4 FsMIEcfmDynMipPreventionRowStatus [ ] ={1,3,6,1,4,1,2076,160,1,1,7,1,1};
UINT4 FsMIEcfmMipCcmFid [ ] ={1,3,6,1,4,1,2076,160,1,0,10,1,1};
UINT4 FsMIEcfmMipCcmSrcAddr [ ] ={1,3,6,1,4,1,2076,160,1,0,10,1,2};
UINT4 FsMIEcfmMipCcmIfIndex [ ] ={1,3,6,1,4,1,2076,160,1,0,10,1,3};
UINT4 FsMIEcfmRMepCcmSequenceNum [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,1};
UINT4 FsMIEcfmRMepPortStatusDefect [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,2};
UINT4 FsMIEcfmRMepInterfaceStatusDefect [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,3};
UINT4 FsMIEcfmRMepCcmDefect [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,4};
UINT4 FsMIEcfmRMepRDIDefect [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,5};
UINT4 FsMIEcfmRMepMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,6};
UINT4 FsMIEcfmRMepRdi [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,7};
UINT4 FsMIEcfmRMepPortStatusTlv [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,8};
UINT4 FsMIEcfmRMepInterfaceStatusTlv [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,9};
UINT4 FsMIEcfmRMepChassisIdSubtype [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,10};
UINT4 FsMIEcfmRMepDbChassisId [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,11};
UINT4 FsMIEcfmRMepManAddressDomain [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,12};
UINT4 FsMIEcfmRMepManAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,11,1,13};
UINT4 FsMIEcfmLtmSeqNumber [ ] ={1,3,6,1,4,1,2076,160,1,0,12,1,1};
UINT4 FsMIEcfmLtmTargetMacAddress [ ] ={1,3,6,1,4,1,2076,160,1,0,12,1,2};
UINT4 FsMIEcfmLtmTtl [ ] ={1,3,6,1,4,1,2076,160,1,0,12,1,3};
UINT4 FsMIEcfmXconnRMepId [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,1};
UINT4 FsMIEcfmErrorRMepId [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,2};
UINT4 FsMIEcfmMepDefectRDICcm [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,3};
UINT4 FsMIEcfmMepDefectMacStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,4};
UINT4 FsMIEcfmMepDefectRemoteCcm [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,5};
UINT4 FsMIEcfmMepDefectErrorCcm [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,6};
UINT4 FsMIEcfmMepDefectXconnCcm [ ] ={1,3,6,1,4,1,2076,160,1,0,13,1,7};
UINT4 FsMIEcfmMepArchiveHoldTime [ ] ={1,3,6,1,4,1,2076,160,1,0,14,1,1};
UINT4 FsMIEcfmMaCrosscheckStatus [ ] ={1,3,6,1,4,1,2076,160,1,0,15,1,1};
UINT4 FsMIEcfmTxCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,1};
UINT4 FsMIEcfmTxCcmCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,2};
UINT4 FsMIEcfmTxLbmCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,3};
UINT4 FsMIEcfmTxLbrCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,4};
UINT4 FsMIEcfmTxLtmCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,5};
UINT4 FsMIEcfmTxLtrCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,6};
UINT4 FsMIEcfmTxFailedCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,7};
UINT4 FsMIEcfmRxCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,8};
UINT4 FsMIEcfmRxCcmCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,9};
UINT4 FsMIEcfmRxLbmCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,10};
UINT4 FsMIEcfmRxLbrCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,11};
UINT4 FsMIEcfmRxLtmCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,12};
UINT4 FsMIEcfmRxLtrCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,13};
UINT4 FsMIEcfmRxBadCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,14};
UINT4 FsMIEcfmFrwdCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,15};
UINT4 FsMIEcfmDsrdCfmPduCount [ ] ={1,3,6,1,4,1,2076,160,1,0,16,1,16};




tMbDbEntry fscfmmMibEntry[]= {

{{13,FsMIEcfmContextId}, GetNextIndexFsMIEcfmContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmSystemControl}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmSystemControlGet, FsMIEcfmSystemControlSet, FsMIEcfmSystemControlTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmModuleStatus}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmModuleStatusGet, FsMIEcfmModuleStatusSet, FsMIEcfmModuleStatusTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmDefaultMdDefLevel}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmDefaultMdDefLevelGet, FsMIEcfmDefaultMdDefLevelSet, FsMIEcfmDefaultMdDefLevelTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "0"},

{{13,FsMIEcfmDefaultMdDefMhfCreation}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmDefaultMdDefMhfCreationGet, FsMIEcfmDefaultMdDefMhfCreationSet, FsMIEcfmDefaultMdDefMhfCreationTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIEcfmDefaultMdDefIdPermission}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmDefaultMdDefIdPermissionGet, FsMIEcfmDefaultMdDefIdPermissionSet, FsMIEcfmDefaultMdDefIdPermissionTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIEcfmMdTableNextIndex}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMdTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmLtrCacheStatus}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmLtrCacheStatusGet, FsMIEcfmLtrCacheStatusSet, FsMIEcfmLtrCacheStatusTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmLtrCacheClear}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmLtrCacheClearGet, FsMIEcfmLtrCacheClearSet, FsMIEcfmLtrCacheClearTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmLtrCacheHoldTime}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmLtrCacheHoldTimeGet, FsMIEcfmLtrCacheHoldTimeSet, FsMIEcfmLtrCacheHoldTimeTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "100"},

{{13,FsMIEcfmLtrCacheSize}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmLtrCacheSizeGet, FsMIEcfmLtrCacheSizeSet, FsMIEcfmLtrCacheSizeTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "100"},

{{13,FsMIEcfmMipCcmDbStatus}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMipCcmDbStatusGet, FsMIEcfmMipCcmDbStatusSet, FsMIEcfmMipCcmDbStatusTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmMipCcmDbClear}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMipCcmDbClearGet, FsMIEcfmMipCcmDbClearSet, FsMIEcfmMipCcmDbClearTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmMipCcmDbSize}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMipCcmDbSizeGet, FsMIEcfmMipCcmDbSizeSet, FsMIEcfmMipCcmDbSizeTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "1000"},

{{13,FsMIEcfmMipCcmDbHoldTime}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMipCcmDbHoldTimeGet, FsMIEcfmMipCcmDbHoldTimeSet, FsMIEcfmMipCcmDbHoldTimeTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "24"},

{{13,FsMIEcfmMemoryFailureCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMemoryFailureCountGet, FsMIEcfmMemoryFailureCountSet, FsMIEcfmMemoryFailureCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmBufferFailureCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmBufferFailureCountGet, FsMIEcfmBufferFailureCountSet, FsMIEcfmBufferFailureCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmUpCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmUpCountGet, FsMIEcfmUpCountSet, FsMIEcfmUpCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmDownCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmDownCountGet, FsMIEcfmDownCountSet, FsMIEcfmDownCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmNoDftCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmNoDftCountGet, FsMIEcfmNoDftCountSet, FsMIEcfmNoDftCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRdiDftCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmRdiDftCountGet, FsMIEcfmRdiDftCountSet, FsMIEcfmRdiDftCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmMacStatusDftCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMacStatusDftCountGet, FsMIEcfmMacStatusDftCountSet, FsMIEcfmMacStatusDftCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRemoteCcmDftCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmRemoteCcmDftCountGet, FsMIEcfmRemoteCcmDftCountSet, FsMIEcfmRemoteCcmDftCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmErrorCcmDftCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmErrorCcmDftCountGet, FsMIEcfmErrorCcmDftCountSet, FsMIEcfmErrorCcmDftCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmXconDftCount}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmXconDftCountGet, FsMIEcfmXconDftCountSet, FsMIEcfmXconDftCountTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmCrosscheckDelay}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmCrosscheckDelayGet, FsMIEcfmCrosscheckDelaySet, FsMIEcfmCrosscheckDelayTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "0"},

{{13,FsMIEcfmMipDynamicEvaluationStatus}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmMipDynamicEvaluationStatusGet, FsMIEcfmMipDynamicEvaluationStatusSet, FsMIEcfmMipDynamicEvaluationStatusTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmContextName}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTrapControl}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmTrapControlGet, FsMIEcfmTrapControlSet, FsMIEcfmTrapControlTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTrapType}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTraceOption}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmTraceOptionGet, FsMIEcfmTraceOptionSet, FsMIEcfmTraceOptionTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "262144"},

{{13,FsMIEcfmGlobalCcmOffload}, GetNextIndexFsMIEcfmContextTable, FsMIEcfmGlobalCcmOffloadGet, FsMIEcfmGlobalCcmOffloadSet, FsMIEcfmGlobalCcmOffloadTest, FsMIEcfmContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmVlanVid}, GetNextIndexFsMIEcfmVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmVlanTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmVlanPrimaryVid}, GetNextIndexFsMIEcfmVlanTable, FsMIEcfmVlanPrimaryVidGet, FsMIEcfmVlanPrimaryVidSet, FsMIEcfmVlanPrimaryVidTest, FsMIEcfmVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmVlanTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmVlanRowStatus}, GetNextIndexFsMIEcfmVlanTable, FsMIEcfmVlanRowStatusGet, FsMIEcfmVlanRowStatusSet, FsMIEcfmVlanRowStatusTest, FsMIEcfmVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmVlanTableINDEX, 2, 0, 1, NULL},

{{13,FsMIEcfmDefaultMdPrimaryVid}, GetNextIndexFsMIEcfmDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmDefaultMdTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmDefaultMdStatus}, GetNextIndexFsMIEcfmDefaultMdTable, FsMIEcfmDefaultMdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmDefaultMdTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmDefaultMdLevel}, GetNextIndexFsMIEcfmDefaultMdTable, FsMIEcfmDefaultMdLevelGet, FsMIEcfmDefaultMdLevelSet, FsMIEcfmDefaultMdLevelTest, FsMIEcfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmDefaultMdTableINDEX, 2, 0, 0, "-1"},

{{13,FsMIEcfmDefaultMdMhfCreation}, GetNextIndexFsMIEcfmDefaultMdTable, FsMIEcfmDefaultMdMhfCreationGet, FsMIEcfmDefaultMdMhfCreationSet, FsMIEcfmDefaultMdMhfCreationTest, FsMIEcfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmDefaultMdTableINDEX, 2, 0, 0, "4"},

{{13,FsMIEcfmDefaultMdIdPermission}, GetNextIndexFsMIEcfmDefaultMdTable, FsMIEcfmDefaultMdIdPermissionGet, FsMIEcfmDefaultMdIdPermissionSet, FsMIEcfmDefaultMdIdPermissionTest, FsMIEcfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmDefaultMdTableINDEX, 2, 0, 0, "5"},

{{13,FsMIEcfmMdIndex}, GetNextIndexFsMIEcfmMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmMdTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmMdFormat}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdFormatGet, FsMIEcfmMdFormatSet, FsMIEcfmMdFormatTest, FsMIEcfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMdTableINDEX, 2, 0, 0, "4"},

{{13,FsMIEcfmMdName}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdNameGet, FsMIEcfmMdNameSet, FsMIEcfmMdNameTest, FsMIEcfmMdTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmMdTableINDEX, 2, 0, 0, "DEFAULT"},

{{13,FsMIEcfmMdMdLevel}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdMdLevelGet, FsMIEcfmMdMdLevelSet, FsMIEcfmMdMdLevelTest, FsMIEcfmMdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmMdTableINDEX, 2, 0, 0, "0"},

{{13,FsMIEcfmMdMhfCreation}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdMhfCreationGet, FsMIEcfmMdMhfCreationSet, FsMIEcfmMdMhfCreationTest, FsMIEcfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMdTableINDEX, 2, 0, 0, "1"},

{{13,FsMIEcfmMdMhfIdPermission}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdMhfIdPermissionGet, FsMIEcfmMdMhfIdPermissionSet, FsMIEcfmMdMhfIdPermissionTest, FsMIEcfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMdTableINDEX, 2, 0, 0, "1"},

{{13,FsMIEcfmMdMaTableNextIndex}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdMaTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmMdTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmMdRowStatus}, GetNextIndexFsMIEcfmMdTable, FsMIEcfmMdRowStatusGet, FsMIEcfmMdRowStatusSet, FsMIEcfmMdRowStatusTest, FsMIEcfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMdTableINDEX, 2, 0, 1, NULL},

{{13,FsMIEcfmMaIndex}, GetNextIndexFsMIEcfmMaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmMaTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMaPrimaryVlanId}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaPrimaryVlanIdGet, FsMIEcfmMaPrimaryVlanIdSet, FsMIEcfmMaPrimaryVlanIdTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMaFormat}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaFormatGet, FsMIEcfmMaFormatSet, FsMIEcfmMaFormatTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMaName}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaNameGet, FsMIEcfmMaNameSet, FsMIEcfmMaNameTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMaMhfCreation}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaMhfCreationGet, FsMIEcfmMaMhfCreationSet, FsMIEcfmMaMhfCreationTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 0, "4"},

{{13,FsMIEcfmMaIdPermission}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaIdPermissionGet, FsMIEcfmMaIdPermissionSet, FsMIEcfmMaIdPermissionTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 0, "5"},

{{13,FsMIEcfmMaCcmInterval}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaCcmIntervalGet, FsMIEcfmMaCcmIntervalSet, FsMIEcfmMaCcmIntervalTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 0, "4"},

{{13,FsMIEcfmMaNumberOfVids}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaNumberOfVidsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmMaTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMaRowStatus}, GetNextIndexFsMIEcfmMaTable, FsMIEcfmMaRowStatusGet, FsMIEcfmMaRowStatusSet, FsMIEcfmMaRowStatusTest, FsMIEcfmMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaTableINDEX, 3, 0, 1, NULL},

{{13,FsMIEcfmMaMepListIdentifier}, GetNextIndexFsMIEcfmMaMepListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmMaMepListTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMaMepListRowStatus}, GetNextIndexFsMIEcfmMaMepListTable, FsMIEcfmMaMepListRowStatusGet, FsMIEcfmMaMepListRowStatusSet, FsMIEcfmMaMepListRowStatusTest, FsMIEcfmMaMepListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaMepListTableINDEX, 4, 0, 1, NULL},

{{13,FsMIEcfmMepIdentifier}, GetNextIndexFsMIEcfmMepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepIfIndex}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepIfIndexGet, FsMIEcfmMepIfIndexSet, FsMIEcfmMepIfIndexTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDirection}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepDirectionGet, FsMIEcfmMepDirectionSet, FsMIEcfmMepDirectionTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepPrimaryVid}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepPrimaryVidGet, FsMIEcfmMepPrimaryVidSet, FsMIEcfmMepPrimaryVidTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "0"},

{{13,FsMIEcfmMepActive}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepActiveGet, FsMIEcfmMepActiveSet, FsMIEcfmMepActiveTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMIEcfmMepFngState}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepFngStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMIEcfmMepCciEnabled}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepCciEnabledGet, FsMIEcfmMepCciEnabledSet, FsMIEcfmMepCciEnabledTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMIEcfmMepCcmLtmPriority}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepCcmLtmPriorityGet, FsMIEcfmMepCcmLtmPrioritySet, FsMIEcfmMepCcmLtmPriorityTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepMacAddress}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepLowPrDef}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepLowPrDefGet, FsMIEcfmMepLowPrDefSet, FsMIEcfmMepLowPrDefTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMIEcfmMepFngAlarmTime}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepFngAlarmTimeGet, FsMIEcfmMepFngAlarmTimeSet, FsMIEcfmMepFngAlarmTimeTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "250"},

{{13,FsMIEcfmMepFngResetTime}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepFngResetTimeGet, FsMIEcfmMepFngResetTimeSet, FsMIEcfmMepFngResetTimeTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "1000"},

{{13,FsMIEcfmMepHighestPrDefect}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepHighestPrDefectGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDefects}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepDefectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepErrorCcmLastFailure}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepErrorCcmLastFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepXconCcmLastFailure}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepXconCcmLastFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepCcmSequenceErrors}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepCcmSequenceErrorsGet, FsMIEcfmMepCcmSequenceErrorsSet, FsMIEcfmMepCcmSequenceErrorsTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepCciSentCcms}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepCciSentCcmsGet, FsMIEcfmMepCciSentCcmsSet, FsMIEcfmMepCciSentCcmsTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepNextLbmTransId}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepNextLbmTransIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepLbrIn}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepLbrInGet, FsMIEcfmMepLbrInSet, FsMIEcfmMepLbrInTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepLbrInOutOfOrder}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepLbrInOutOfOrderGet, FsMIEcfmMepLbrInOutOfOrderSet, FsMIEcfmMepLbrInOutOfOrderTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepLbrBadMsdu}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepLbrBadMsduGet, FsMIEcfmMepLbrBadMsduSet, FsMIEcfmMepLbrBadMsduTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepLtmNextSeqNumber}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepLtmNextSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepUnexpLtrIn}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepUnexpLtrInGet, FsMIEcfmMepUnexpLtrInSet, FsMIEcfmMepUnexpLtrInTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepLbrOut}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepLbrOutGet, FsMIEcfmMepLbrOutSet, FsMIEcfmMepLbrOutTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLbmStatus}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmStatusGet, FsMIEcfmMepTransmitLbmStatusSet, FsMIEcfmMepTransmitLbmStatusTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "0"},

{{13,FsMIEcfmMepTransmitLbmDestMacAddress}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmDestMacAddressGet, FsMIEcfmMepTransmitLbmDestMacAddressSet, FsMIEcfmMepTransmitLbmDestMacAddressTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLbmDestMepId}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmDestMepIdGet, FsMIEcfmMepTransmitLbmDestMepIdSet, FsMIEcfmMepTransmitLbmDestMepIdTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLbmDestIsMepId}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmDestIsMepIdGet, FsMIEcfmMepTransmitLbmDestIsMepIdSet, FsMIEcfmMepTransmitLbmDestIsMepIdTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLbmMessages}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmMessagesGet, FsMIEcfmMepTransmitLbmMessagesSet, FsMIEcfmMepTransmitLbmMessagesTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMIEcfmMepTransmitLbmDataTlv}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmDataTlvGet, FsMIEcfmMepTransmitLbmDataTlvSet, FsMIEcfmMepTransmitLbmDataTlvTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLbmVlanPriority}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmVlanPriorityGet, FsMIEcfmMepTransmitLbmVlanPrioritySet, FsMIEcfmMepTransmitLbmVlanPriorityTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLbmVlanDropEnable}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmVlanDropEnableGet, FsMIEcfmMepTransmitLbmVlanDropEnableSet, FsMIEcfmMepTransmitLbmVlanDropEnableTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMIEcfmMepTransmitLbmResultOK}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMIEcfmMepTransmitLbmSeqNumber}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLbmSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLtmStatus}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmStatusGet, FsMIEcfmMepTransmitLtmStatusSet, FsMIEcfmMepTransmitLtmStatusTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "0"},

{{13,FsMIEcfmMepTransmitLtmFlags}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmFlagsGet, FsMIEcfmMepTransmitLtmFlagsSet, FsMIEcfmMepTransmitLtmFlagsTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "0"},

{{13,FsMIEcfmMepTransmitLtmTargetMacAddress}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmTargetMacAddressGet, FsMIEcfmMepTransmitLtmTargetMacAddressSet, FsMIEcfmMepTransmitLtmTargetMacAddressTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLtmTargetMepId}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmTargetMepIdGet, FsMIEcfmMepTransmitLtmTargetMepIdSet, FsMIEcfmMepTransmitLtmTargetMepIdTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLtmTargetIsMepId}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmTargetIsMepIdGet, FsMIEcfmMepTransmitLtmTargetIsMepIdSet, FsMIEcfmMepTransmitLtmTargetIsMepIdTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLtmTtl}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmTtlGet, FsMIEcfmMepTransmitLtmTtlSet, FsMIEcfmMepTransmitLtmTtlTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "64"},

{{13,FsMIEcfmMepTransmitLtmResult}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMIEcfmMepTransmitLtmSeqNumber}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepTransmitLtmEgressIdentifier}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepTransmitLtmEgressIdentifierGet, FsMIEcfmMepTransmitLtmEgressIdentifierSet, FsMIEcfmMepTransmitLtmEgressIdentifierTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepRowStatus}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepRowStatusGet, FsMIEcfmMepRowStatusSet, FsMIEcfmMepRowStatusTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 1, NULL},

{{13,FsMIEcfmMepCcmOffload}, GetNextIndexFsMIEcfmMepTable, FsMIEcfmMepCcmOffloadGet, FsMIEcfmMepCcmOffloadSet, FsMIEcfmMepCcmOffloadTest, FsMIEcfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMIEcfmLtrSeqNumber}, GetNextIndexFsMIEcfmLtrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrReceiveOrder}, GetNextIndexFsMIEcfmLtrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrTtl}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrTtlGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrForwarded}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrForwardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrTerminalMep}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrTerminalMepGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrLastEgressIdentifier}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrLastEgressIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrNextEgressIdentifier}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrNextEgressIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrRelay}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrRelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrChassisIdSubtype}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrChassisId}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrManAddressDomain}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrManAddressDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrManAddress}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrManAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrIngress}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrIngressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrIngressMac}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrIngressMacGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrIngressPortIdSubtype}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrIngressPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrIngressPortId}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrIngressPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrEgress}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrEgressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrEgressMac}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrEgressMacGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrEgressPortIdSubtype}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrEgressPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrEgressPortId}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrEgressPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmLtrOrganizationSpecificTlv}, GetNextIndexFsMIEcfmLtrTable, FsMIEcfmLtrOrganizationSpecificTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmLtrTableINDEX, 6, 0, 0, NULL},

{{13,FsMIEcfmMepDbRMepIdentifier}, GetNextIndexFsMIEcfmMepDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbRMepState}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbRMepStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbRMepFailedOkTime}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbRMepFailedOkTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbMacAddress}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbRdi}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbRdiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbPortStatusTlv}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbPortStatusTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, "0"},

{{13,FsMIEcfmMepDbInterfaceStatusTlv}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbInterfaceStatusTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, "0"},

{{13,FsMIEcfmMepDbChassisIdSubtype}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, "4"},

{{13,FsMIEcfmMepDbChassisId}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbManAddressDomain}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbManAddressDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMepDbManAddress}, GetNextIndexFsMIEcfmMepDbTable, FsMIEcfmMepDbManAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmMepDbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmMipCcmFid}, GetNextIndexFsMIEcfmMipCcmDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmMipCcmDbTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMipCcmSrcAddr}, GetNextIndexFsMIEcfmMipCcmDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIEcfmMipCcmDbTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMipCcmIfIndex}, GetNextIndexFsMIEcfmMipCcmDbTable, FsMIEcfmMipCcmIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmMipCcmDbTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmRMepCcmSequenceNum}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepCcmSequenceNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepPortStatusDefect}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepPortStatusDefectGet, FsMIEcfmRMepPortStatusDefectSet, FsMIEcfmRMepPortStatusDefectTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepInterfaceStatusDefect}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepInterfaceStatusDefectGet, FsMIEcfmRMepInterfaceStatusDefectSet, FsMIEcfmRMepInterfaceStatusDefectTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepCcmDefect}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepCcmDefectGet, FsMIEcfmRMepCcmDefectSet, FsMIEcfmRMepCcmDefectTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepRDIDefect}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepRDIDefectGet, FsMIEcfmRMepRDIDefectSet, FsMIEcfmRMepRDIDefectTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepMacAddress}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepMacAddressGet, FsMIEcfmRMepMacAddressSet, FsMIEcfmRMepMacAddressTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepRdi}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepRdiGet, FsMIEcfmRMepRdiSet, FsMIEcfmRMepRdiTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepPortStatusTlv}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepPortStatusTlvGet, FsMIEcfmRMepPortStatusTlvSet, FsMIEcfmRMepPortStatusTlvTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, "0"},

{{13,FsMIEcfmRMepInterfaceStatusTlv}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepInterfaceStatusTlvGet, FsMIEcfmRMepInterfaceStatusTlvSet, FsMIEcfmRMepInterfaceStatusTlvTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, "0"},

{{13,FsMIEcfmRMepChassisIdSubtype}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepChassisIdSubtypeGet, FsMIEcfmRMepChassisIdSubtypeSet, FsMIEcfmRMepChassisIdSubtypeTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, "4"},

{{13,FsMIEcfmRMepDbChassisId}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepDbChassisIdGet, FsMIEcfmRMepDbChassisIdSet, FsMIEcfmRMepDbChassisIdTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepManAddressDomain}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepManAddressDomainGet, FsMIEcfmRMepManAddressDomainSet, FsMIEcfmRMepManAddressDomainTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmRMepManAddress}, GetNextIndexFsMIEcfmRemoteMepDbExTable, FsMIEcfmRMepManAddressGet, FsMIEcfmRMepManAddressSet, FsMIEcfmRMepManAddressTest, FsMIEcfmRemoteMepDbExTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmRemoteMepDbExTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmLtmSeqNumber}, GetNextIndexFsMIEcfmLtmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmLtmTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmLtmTargetMacAddress}, GetNextIndexFsMIEcfmLtmTable, FsMIEcfmLtmTargetMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmLtmTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmLtmTtl}, GetNextIndexFsMIEcfmLtmTable, FsMIEcfmLtmTtlGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmLtmTableINDEX, 5, 0, 0, NULL},

{{13,FsMIEcfmXconnRMepId}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmXconnRMepIdGet, FsMIEcfmXconnRMepIdSet, FsMIEcfmXconnRMepIdTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmErrorRMepId}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmErrorRMepIdGet, FsMIEcfmErrorRMepIdSet, FsMIEcfmErrorRMepIdTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDefectRDICcm}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmMepDefectRDICcmGet, FsMIEcfmMepDefectRDICcmSet, FsMIEcfmMepDefectRDICcmTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDefectMacStatus}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmMepDefectMacStatusGet, FsMIEcfmMepDefectMacStatusSet, FsMIEcfmMepDefectMacStatusTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDefectRemoteCcm}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmMepDefectRemoteCcmGet, FsMIEcfmMepDefectRemoteCcmSet, FsMIEcfmMepDefectRemoteCcmTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDefectErrorCcm}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmMepDefectErrorCcmGet, FsMIEcfmMepDefectErrorCcmSet, FsMIEcfmMepDefectErrorCcmTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepDefectXconnCcm}, GetNextIndexFsMIEcfmMepExTable, FsMIEcfmMepDefectXconnCcmGet, FsMIEcfmMepDefectXconnCcmSet, FsMIEcfmMepDefectXconnCcmTest, FsMIEcfmMepExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMepExTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmMepArchiveHoldTime}, GetNextIndexFsMIEcfmMdExTable, FsMIEcfmMepArchiveHoldTimeGet, FsMIEcfmMepArchiveHoldTimeSet, FsMIEcfmMepArchiveHoldTimeTest, FsMIEcfmMdExTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmMdExTableINDEX, 2, 0, 0, "100"},

{{13,FsMIEcfmMaCrosscheckStatus}, GetNextIndexFsMIEcfmMaExTable, FsMIEcfmMaCrosscheckStatusGet, FsMIEcfmMaCrosscheckStatusSet, FsMIEcfmMaCrosscheckStatusTest, FsMIEcfmMaExTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMaExTableINDEX, 3, 0, 0, "1"},

{{13,FsMIEcfmTxCfmPduCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxCfmPduCountGet, FsMIEcfmTxCfmPduCountSet, FsMIEcfmTxCfmPduCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTxCcmCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxCcmCountGet, FsMIEcfmTxCcmCountSet, FsMIEcfmTxCcmCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTxLbmCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxLbmCountGet, FsMIEcfmTxLbmCountSet, FsMIEcfmTxLbmCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTxLbrCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxLbrCountGet, FsMIEcfmTxLbrCountSet, FsMIEcfmTxLbrCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTxLtmCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxLtmCountGet, FsMIEcfmTxLtmCountSet, FsMIEcfmTxLtmCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTxLtrCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxLtrCountGet, FsMIEcfmTxLtrCountSet, FsMIEcfmTxLtrCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmTxFailedCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmTxFailedCountGet, FsMIEcfmTxFailedCountSet, FsMIEcfmTxFailedCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxCfmPduCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxCfmPduCountGet, FsMIEcfmRxCfmPduCountSet, FsMIEcfmRxCfmPduCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxCcmCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxCcmCountGet, FsMIEcfmRxCcmCountSet, FsMIEcfmRxCcmCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxLbmCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxLbmCountGet, FsMIEcfmRxLbmCountSet, FsMIEcfmRxLbmCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxLbrCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxLbrCountGet, FsMIEcfmRxLbrCountSet, FsMIEcfmRxLbrCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxLtmCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxLtmCountGet, FsMIEcfmRxLtmCountSet, FsMIEcfmRxLtmCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxLtrCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxLtrCountGet, FsMIEcfmRxLtrCountSet, FsMIEcfmRxLtrCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmRxBadCfmPduCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmRxBadCfmPduCountGet, FsMIEcfmRxBadCfmPduCountSet, FsMIEcfmRxBadCfmPduCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmFrwdCfmPduCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmFrwdCfmPduCountGet, FsMIEcfmFrwdCfmPduCountSet, FsMIEcfmFrwdCfmPduCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmDsrdCfmPduCount}, GetNextIndexFsMIEcfmStatsTable, FsMIEcfmDsrdCfmPduCountGet, FsMIEcfmDsrdCfmPduCountSet, FsMIEcfmDsrdCfmPduCountTest, FsMIEcfmStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmStatsTableINDEX, 1, 0, 0, NULL},

{{11,FsMIEcfmGlobalTrace}, NULL, FsMIEcfmGlobalTraceGet, FsMIEcfmGlobalTraceSet, FsMIEcfmGlobalTraceTest, FsMIEcfmGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIEcfmOui}, NULL, FsMIEcfmOuiGet, FsMIEcfmOuiSet, FsMIEcfmOuiTest, FsMIEcfmOuiDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsMIEcfmPortIfIndex}, GetNextIndexFsMIEcfmPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortLLCEncapStatus}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortLLCEncapStatusGet, FsMIEcfmPortLLCEncapStatusSet, FsMIEcfmPortLLCEncapStatusTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmPortModuleStatus}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortModuleStatusGet, FsMIEcfmPortModuleStatusSet, FsMIEcfmPortModuleStatusTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, "2"},

{{13,FsMIEcfmPortTxCfmPduCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxCfmPduCountGet, FsMIEcfmPortTxCfmPduCountSet, FsMIEcfmPortTxCfmPduCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortTxCcmCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxCcmCountGet, FsMIEcfmPortTxCcmCountSet, FsMIEcfmPortTxCcmCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortTxLbmCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxLbmCountGet, FsMIEcfmPortTxLbmCountSet, FsMIEcfmPortTxLbmCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortTxLbrCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxLbrCountGet, FsMIEcfmPortTxLbrCountSet, FsMIEcfmPortTxLbrCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortTxLtmCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxLtmCountGet, FsMIEcfmPortTxLtmCountSet, FsMIEcfmPortTxLtmCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortTxLtrCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxLtrCountGet, FsMIEcfmPortTxLtrCountSet, FsMIEcfmPortTxLtrCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortTxFailedCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortTxFailedCountGet, FsMIEcfmPortTxFailedCountSet, FsMIEcfmPortTxFailedCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxCfmPduCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxCfmPduCountGet, FsMIEcfmPortRxCfmPduCountSet, FsMIEcfmPortRxCfmPduCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxCcmCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxCcmCountGet, FsMIEcfmPortRxCcmCountSet, FsMIEcfmPortRxCcmCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxLbmCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxLbmCountGet, FsMIEcfmPortRxLbmCountSet, FsMIEcfmPortRxLbmCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxLbrCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxLbrCountGet, FsMIEcfmPortRxLbrCountSet, FsMIEcfmPortRxLbrCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxLtmCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxLtmCountGet, FsMIEcfmPortRxLtmCountSet, FsMIEcfmPortRxLtmCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxLtrCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxLtrCountGet, FsMIEcfmPortRxLtrCountSet, FsMIEcfmPortRxLtrCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortRxBadCfmPduCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortRxBadCfmPduCountGet, FsMIEcfmPortRxBadCfmPduCountSet, FsMIEcfmPortRxBadCfmPduCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortFrwdCfmPduCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortFrwdCfmPduCountGet, FsMIEcfmPortFrwdCfmPduCountSet, FsMIEcfmPortFrwdCfmPduCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmPortDsrdCfmPduCount}, GetNextIndexFsMIEcfmPortTable, FsMIEcfmPortDsrdCfmPduCountGet, FsMIEcfmPortDsrdCfmPduCountSet, FsMIEcfmPortDsrdCfmPduCountTest, FsMIEcfmPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmPortTableINDEX, 1, 0, 0, NULL},

{{13,FsMIEcfmStackIfIndex}, GetNextIndexFsMIEcfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackVlanIdOrNone}, GetNextIndexFsMIEcfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackMdLevel}, GetNextIndexFsMIEcfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackDirection}, GetNextIndexFsMIEcfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackMdIndex}, GetNextIndexFsMIEcfmStackTable, FsMIEcfmStackMdIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackMaIndex}, GetNextIndexFsMIEcfmStackTable, FsMIEcfmStackMaIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackMepId}, GetNextIndexFsMIEcfmStackTable, FsMIEcfmStackMepIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmStackMacAddress}, GetNextIndexFsMIEcfmStackTable, FsMIEcfmStackMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmStackTableINDEX, 4, 0, 0, NULL},

{{13,FsMIEcfmConfigErrorListVid}, GetNextIndexFsMIEcfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmConfigErrorListTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmConfigErrorListIfIndex}, GetNextIndexFsMIEcfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmConfigErrorListTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmConfigErrorListErrorType}, GetNextIndexFsMIEcfmConfigErrorListTable, FsMIEcfmConfigErrorListErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmConfigErrorListTableINDEX, 2, 0, 0, NULL},

{{13,FsMIEcfmMipIfIndex}, GetNextIndexFsMIEcfmMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmMipTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMipMdLevel}, GetNextIndexFsMIEcfmMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIEcfmMipTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMipVid}, GetNextIndexFsMIEcfmMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmMipTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMipActive}, GetNextIndexFsMIEcfmMipTable, FsMIEcfmMipActiveGet, FsMIEcfmMipActiveSet, FsMIEcfmMipActiveTest, FsMIEcfmMipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMipTableINDEX, 3, 0, 0, NULL},

{{13,FsMIEcfmMipRowStatus}, GetNextIndexFsMIEcfmMipTable, FsMIEcfmMipRowStatusGet, FsMIEcfmMipRowStatusSet, FsMIEcfmMipRowStatusTest, FsMIEcfmMipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmMipTableINDEX, 3, 0, 1, NULL},

{{13,FsMIEcfmDynMipPreventionRowStatus}, GetNextIndexFsMIEcfmDynMipPreventionTable, FsMIEcfmDynMipPreventionRowStatusGet, FsMIEcfmDynMipPreventionRowStatusSet, FsMIEcfmDynMipPreventionRowStatusTest, FsMIEcfmDynMipPreventionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmDynMipPreventionTableINDEX, 3, 0, 1, NULL},
};
tMibData fscfmmEntry = { 219, fscfmmMibEntry };

#endif /* _FSCFMMDB_H */

