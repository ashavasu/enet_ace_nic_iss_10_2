/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmltism.h,v 1.4 2008/05/14 07:05:03 prabuc-iss Exp $
 * 
 * Description: This file contains the functionality of the
 *              LT initiator State Machine.
 *********************************************************************/
#ifndef __CFM_LTISM_H
#define __CFM_LTISM_H


/* Private Routines for the LT Initiator SM */
/* Routines for handling the received events */
PRIVATE INT4 EcfmLtInitSmSetStateIdle PROTO ((tEcfmLbLtPduSmInfo * ));
PRIVATE INT4 EcfmLtInitSmSetStateDefault PROTO ((tEcfmLbLtPduSmInfo * ));
PRIVATE INT4 EcfmLtInitSmSetStateTransmit PROTO ((tEcfmLbLtPduSmInfo * ));
PRIVATE INT4 EcfmLtInitSmEvtTimerTimeout PROTO ((tEcfmLbLtPduSmInfo * ));
PRIVATE INT4 EcfmLtInitSmEvtImpossible PROTO ((tEcfmLbLtPduSmInfo * ));

/* Routines for formatting and transmitting LTM, required by SM routines */
PRIVATE INT4 EcfmLtInitSmXmitLtm PROTO ((tEcfmLbLtPduSmInfo * ));
PRIVATE VOID EcfmLtInitSmFormatLtmPduHdr PROTO((tEcfmLbLtMepInfo * ,UINT1 **)); 
PRIVATE INT4 EcfmLtInitSmPutLtmInfo PROTO((tEcfmLbLtMepInfo *, UINT1 **));
PRIVATE VOID EcfmLtInitSmPutLtmTlv PROTO((tEcfmLbLtMepInfo * , UINT1 **));

PRIVATE INT4 EcfmLtInitSmAddLtmReplyListEntry PROTO ((tEcfmLbLtLTInfo *,                    
                                                      tEcfmLbLtMepInfo *)); 

/* Enums for the rotuines to be retrieve from the the matrix*/

enum
{
  LTISM0,                     /* EcfmLtInitSmSetStateIdle*/
  LTISM1,                     /* EcfmLtInitSmSetStateDefault */
  LTISM2,                     /* EcfmLtInitSmSetStateTransmit */
  LTISM3,                     /* EcfmLtInitSmEvtTimerTimeout*/
  LTISM4,                     /* EcfmLtInitSmEvtImpossible */
  ECFM_MAX_LTI_FN_PTRS
};

/* function pointers of  LT Initiator State machine */
INT4 (*gaEcfmLtInitActionProc[ECFM_MAX_LTI_FN_PTRS]) (tEcfmLbLtPduSmInfo *) =
{
    EcfmLtInitSmSetStateIdle,
    EcfmLtInitSmSetStateDefault , 
    EcfmLtInitSmSetStateTransmit ,
    EcfmLtInitSmEvtTimerTimeout,
    EcfmLtInitSmEvtImpossible ,
};

/* State Event Table of LT Initiator State machine */
const UINT1 gau1EcfmLtInitSm[ECFM_SM_LTI_TX_MAX_EVENTS][ECFM_LTI_MAX_STATES] 
=
/* DEFAULT      IDLE      TRANSMIT */  /* STATES */ 
{                                       /* Events */
    {LTISM0,    LTISM4,    LTISM4},     /* ECFM_SM_EV_BEGIN */
    {LTISM4,    LTISM1,    LTISM1},     /* ECFM_SM_EV_MEP_NOT_ACTIVE */
    {LTISM4,    LTISM2,    LTISM4},     /* ECFM_SM_EV_LTI_TX_LTM */
	{LTISM4,    LTISM4,    LTISM3}      /* ECFM_SM_EV_LTI_TX_LTM_TIMESOUT */
};

#endif /* __CFM_LTISM_H*/

/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmltism.h                      */
/*-----------------------------------------------------------------------*/
