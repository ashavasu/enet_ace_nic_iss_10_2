/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmedb.h,v 1.2 2009/09/24 14:30:33 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSCFMEDB_H
#define _FSCFMEDB_H

UINT1 FsMIEcfmExtStackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmExtDefaultMdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmExtConfigErrorListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIEcfmExtMaTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmExtMipTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEcfmExtMepTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fscfme [] ={1,3,6,1,4,1,29601,2,13};
tSNMP_OID_TYPE fscfmeOID = {9, fscfme};


UINT4 FsMIEcfmExtStackIfIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,1};
UINT4 FsMIEcfmExtStackServiceSelectorType [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,2};
UINT4 FsMIEcfmExtStackServiceSelectorOrNone [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,3};
UINT4 FsMIEcfmExtStackMdLevel [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,4};
UINT4 FsMIEcfmExtStackDirection [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,5};
UINT4 FsMIEcfmExtStackMdIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,6};
UINT4 FsMIEcfmExtStackMaIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,7};
UINT4 FsMIEcfmExtStackMepId [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,8};
UINT4 FsMIEcfmExtStackMacAddress [ ] ={1,3,6,1,4,1,29601,2,13,1,1,1,1,9};
UINT4 FsMIEcfmExtDefaultMdPrimarySelectorType [ ] ={1,3,6,1,4,1,29601,2,13,1,2,1,1,1};
UINT4 FsMIEcfmExtDefaultMdPrimarySelector [ ] ={1,3,6,1,4,1,29601,2,13,1,2,1,1,2};
UINT4 FsMIEcfmExtDefaultMdStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,2,1,1,3};
UINT4 FsMIEcfmExtDefaultMdLevel [ ] ={1,3,6,1,4,1,29601,2,13,1,2,1,1,4};
UINT4 FsMIEcfmExtDefaultMdMhfCreation [ ] ={1,3,6,1,4,1,29601,2,13,1,2,1,1,5};
UINT4 FsMIEcfmExtDefaultMdIdPermission [ ] ={1,3,6,1,4,1,29601,2,13,1,2,1,1,6};
UINT4 FsMIEcfmExtConfigErrorListSelectorType [ ] ={1,3,6,1,4,1,29601,2,13,1,1,2,1,1};
UINT4 FsMIEcfmExtConfigErrorListSelector [ ] ={1,3,6,1,4,1,29601,2,13,1,1,2,1,2};
UINT4 FsMIEcfmExtConfigErrorListIfIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,1,2,1,3};
UINT4 FsMIEcfmExtConfigErrorListErrorType [ ] ={1,3,6,1,4,1,29601,2,13,1,1,2,1,4};
UINT4 FsMIEcfmExtMaIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,1};
UINT4 FsMIEcfmExtMaPrimarySelectorType [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,2};
UINT4 FsMIEcfmExtMaPrimarySelectorOrNone [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,3};
UINT4 FsMIEcfmExtMaFormat [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,4};
UINT4 FsMIEcfmExtMaName [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,5};
UINT4 FsMIEcfmExtMaMhfCreation [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,6};
UINT4 FsMIEcfmExtMaIdPermission [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,7};
UINT4 FsMIEcfmExtMaCcmInterval [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,8};
UINT4 FsMIEcfmExtMaNumberOfVids [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,9};
UINT4 FsMIEcfmExtMaRowStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,10};
UINT4 FsMIEcfmExtMaCrosscheckStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,2,2,1,11};
UINT4 FsMIEcfmExtMipIfIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,1,3,1,1};
UINT4 FsMIEcfmExtMipMdLevel [ ] ={1,3,6,1,4,1,29601,2,13,1,1,3,1,2};
UINT4 FsMIEcfmExtMipSelectorType [ ] ={1,3,6,1,4,1,29601,2,13,1,1,3,1,3};
UINT4 FsMIEcfmExtMipPrimarySelector [ ] ={1,3,6,1,4,1,29601,2,13,1,1,3,1,4};
UINT4 FsMIEcfmExtMipActive [ ] ={1,3,6,1,4,1,29601,2,13,1,1,3,1,5};
UINT4 FsMIEcfmExtMipRowStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,1,3,1,6};
UINT4 FsMIEcfmExtMepIdentifier [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,1};
UINT4 FsMIEcfmExtMepIfIndex [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,2};
UINT4 FsMIEcfmExtMepDirection [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,3};
UINT4 FsMIEcfmExtMepPrimaryVidOrIsid [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,4};
UINT4 FsMIEcfmExtMepActive [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,5};
UINT4 FsMIEcfmExtMepFngState [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,6};
UINT4 FsMIEcfmExtMepCciEnabled [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,7};
UINT4 FsMIEcfmExtMepCcmLtmPriority [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,8};
UINT4 FsMIEcfmExtMepMacAddress [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,9};
UINT4 FsMIEcfmExtMepLowPrDef [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,10};
UINT4 FsMIEcfmExtMepFngAlarmTime [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,11};
UINT4 FsMIEcfmExtMepFngResetTime [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,12};
UINT4 FsMIEcfmExtMepHighestPrDefect [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,13};
UINT4 FsMIEcfmExtMepDefects [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,14};
UINT4 FsMIEcfmExtMepErrorCcmLastFailure [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,15};
UINT4 FsMIEcfmExtMepXconCcmLastFailure [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,16};
UINT4 FsMIEcfmExtMepCcmSequenceErrors [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,17};
UINT4 FsMIEcfmExtMepCciSentCcms [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,18};
UINT4 FsMIEcfmExtMepNextLbmTransId [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,19};
UINT4 FsMIEcfmExtMepLbrIn [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,20};
UINT4 FsMIEcfmExtMepLbrInOutOfOrder [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,21};
UINT4 FsMIEcfmExtMepLbrBadMsdu [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,22};
UINT4 FsMIEcfmExtMepLtmNextSeqNumber [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,23};
UINT4 FsMIEcfmExtMepUnexpLtrIn [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,24};
UINT4 FsMIEcfmExtMepLbrOut [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,25};
UINT4 FsMIEcfmExtMepTransmitLbmStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,26};
UINT4 FsMIEcfmExtMepTransmitLbmDestMacAddress [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,27};
UINT4 FsMIEcfmExtMepTransmitLbmDestMepId [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,28};
UINT4 FsMIEcfmExtMepTransmitLbmDestIsMepId [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,29};
UINT4 FsMIEcfmExtMepTransmitLbmMessages [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,30};
UINT4 FsMIEcfmExtMepTransmitLbmDataTlv [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,31};
UINT4 FsMIEcfmExtMepTransmitLbmVlanIsidPriority [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,32};
UINT4 FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,33};
UINT4 FsMIEcfmExtMepTransmitLbmResultOK [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,34};
UINT4 FsMIEcfmExtMepTransmitLbmSeqNumber [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,35};
UINT4 FsMIEcfmExtMepTransmitLtmStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,36};
UINT4 FsMIEcfmExtMepTransmitLtmFlags [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,37};
UINT4 FsMIEcfmExtMepTransmitLtmTargetMacAddress [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,38};
UINT4 FsMIEcfmExtMepTransmitLtmTargetMepId [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,39};
UINT4 FsMIEcfmExtMepTransmitLtmTargetIsMepId [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,40};
UINT4 FsMIEcfmExtMepTransmitLtmTtl [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,41};
UINT4 FsMIEcfmExtMepTransmitLtmResult [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,42};
UINT4 FsMIEcfmExtMepTransmitLtmSeqNumber [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,43};
UINT4 FsMIEcfmExtMepTransmitLtmEgressIdentifier [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,44};
UINT4 FsMIEcfmExtMepRowStatus [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,45};
UINT4 FsMIEcfmExtMepCcmOffload [ ] ={1,3,6,1,4,1,29601,2,13,1,2,3,1,46};


tMbDbEntry fscfmeMibEntry[]= {

{{14,FsMIEcfmExtStackIfIndex}, GetNextIndexFsMIEcfmExtStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackServiceSelectorType}, GetNextIndexFsMIEcfmExtStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackServiceSelectorOrNone}, GetNextIndexFsMIEcfmExtStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackMdLevel}, GetNextIndexFsMIEcfmExtStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackDirection}, GetNextIndexFsMIEcfmExtStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackMdIndex}, GetNextIndexFsMIEcfmExtStackTable, FsMIEcfmExtStackMdIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackMaIndex}, GetNextIndexFsMIEcfmExtStackTable, FsMIEcfmExtStackMaIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackMepId}, GetNextIndexFsMIEcfmExtStackTable, FsMIEcfmExtStackMepIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtStackMacAddress}, GetNextIndexFsMIEcfmExtStackTable, FsMIEcfmExtStackMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmExtStackTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtConfigErrorListSelectorType}, GetNextIndexFsMIEcfmExtConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtConfigErrorListSelector}, GetNextIndexFsMIEcfmExtConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtConfigErrorListIfIndex}, GetNextIndexFsMIEcfmExtConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtConfigErrorListErrorType}, GetNextIndexFsMIEcfmExtConfigErrorListTable, FsMIEcfmExtConfigErrorListErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmExtConfigErrorListTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtMipIfIndex}, GetNextIndexFsMIEcfmExtMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtMipTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMipMdLevel}, GetNextIndexFsMIEcfmExtMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIEcfmExtMipTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMipSelectorType}, GetNextIndexFsMIEcfmExtMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtMipTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMipPrimarySelector}, GetNextIndexFsMIEcfmExtMipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtMipTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMipActive}, GetNextIndexFsMIEcfmExtMipTable, FsMIEcfmExtMipActiveGet, FsMIEcfmExtMipActiveSet, FsMIEcfmExtMipActiveTest, FsMIEcfmExtMipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMipTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMipRowStatus}, GetNextIndexFsMIEcfmExtMipTable, FsMIEcfmExtMipRowStatusGet, FsMIEcfmExtMipRowStatusSet, FsMIEcfmExtMipRowStatusTest, FsMIEcfmExtMipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMipTableINDEX, 4, 0, 1, NULL},

{{14,FsMIEcfmExtDefaultMdPrimarySelectorType}, GetNextIndexFsMIEcfmExtDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtDefaultMdTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtDefaultMdPrimarySelector}, GetNextIndexFsMIEcfmExtDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtDefaultMdTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtDefaultMdStatus}, GetNextIndexFsMIEcfmExtDefaultMdTable, FsMIEcfmExtDefaultMdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmExtDefaultMdTableINDEX, 3, 0, 0, NULL},

{{14,FsMIEcfmExtDefaultMdLevel}, GetNextIndexFsMIEcfmExtDefaultMdTable, FsMIEcfmExtDefaultMdLevelGet, FsMIEcfmExtDefaultMdLevelSet, FsMIEcfmExtDefaultMdLevelTest, FsMIEcfmExtDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmExtDefaultMdTableINDEX, 3, 0, 0, "-1"},

{{14,FsMIEcfmExtDefaultMdMhfCreation}, GetNextIndexFsMIEcfmExtDefaultMdTable, FsMIEcfmExtDefaultMdMhfCreationGet, FsMIEcfmExtDefaultMdMhfCreationSet, FsMIEcfmExtDefaultMdMhfCreationTest, FsMIEcfmExtDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtDefaultMdTableINDEX, 3, 0, 0, "4"},

{{14,FsMIEcfmExtDefaultMdIdPermission}, GetNextIndexFsMIEcfmExtDefaultMdTable, FsMIEcfmExtDefaultMdIdPermissionGet, FsMIEcfmExtDefaultMdIdPermissionSet, FsMIEcfmExtDefaultMdIdPermissionTest, FsMIEcfmExtDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtDefaultMdTableINDEX, 3, 0, 0, "5"},

{{14,FsMIEcfmExtMaIndex}, GetNextIndexFsMIEcfmExtMaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtMaTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtMaPrimarySelectorType}, GetNextIndexFsMIEcfmExtMaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIEcfmExtMaTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtMaPrimarySelectorOrNone}, GetNextIndexFsMIEcfmExtMaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtMaTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtMaFormat}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaFormatGet, FsMIEcfmExtMaFormatSet, FsMIEcfmExtMaFormatTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtMaName}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaNameGet, FsMIEcfmExtMaNameSet, FsMIEcfmExtMaNameTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtMaMhfCreation}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaMhfCreationGet, FsMIEcfmExtMaMhfCreationSet, FsMIEcfmExtMaMhfCreationTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 0, "4"},

{{14,FsMIEcfmExtMaIdPermission}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaIdPermissionGet, FsMIEcfmExtMaIdPermissionSet, FsMIEcfmExtMaIdPermissionTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 0, "5"},

{{14,FsMIEcfmExtMaCcmInterval}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaCcmIntervalGet, FsMIEcfmExtMaCcmIntervalSet, FsMIEcfmExtMaCcmIntervalTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 0, "4"},

{{14,FsMIEcfmExtMaNumberOfVids}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaNumberOfVidsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtMaTableINDEX, 5, 0, 0, NULL},

{{14,FsMIEcfmExtMaRowStatus}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaRowStatusGet, FsMIEcfmExtMaRowStatusSet, FsMIEcfmExtMaRowStatusTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 1, NULL},

{{14,FsMIEcfmExtMaCrosscheckStatus}, GetNextIndexFsMIEcfmExtMaTable, FsMIEcfmExtMaCrosscheckStatusGet, FsMIEcfmExtMaCrosscheckStatusSet, FsMIEcfmExtMaCrosscheckStatusTest, FsMIEcfmExtMaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMaTableINDEX, 5, 0, 0, "1"},

{{14,FsMIEcfmExtMepIdentifier}, GetNextIndexFsMIEcfmExtMepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepIfIndex}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepIfIndexGet, FsMIEcfmExtMepIfIndexSet, FsMIEcfmExtMepIfIndexTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepDirection}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepDirectionGet, FsMIEcfmExtMepDirectionSet, FsMIEcfmExtMepDirectionTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepPrimaryVidOrIsid}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepPrimaryVidOrIsidGet, FsMIEcfmExtMepPrimaryVidOrIsidSet, FsMIEcfmExtMepPrimaryVidOrIsidTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "0"},

{{14,FsMIEcfmExtMepActive}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepActiveGet, FsMIEcfmExtMepActiveSet, FsMIEcfmExtMepActiveTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "2"},

{{14,FsMIEcfmExtMepFngState}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepFngStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "1"},

{{14,FsMIEcfmExtMepCciEnabled}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepCciEnabledGet, FsMIEcfmExtMepCciEnabledSet, FsMIEcfmExtMepCciEnabledTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "2"},

{{14,FsMIEcfmExtMepCcmLtmPriority}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepCcmLtmPriorityGet, FsMIEcfmExtMepCcmLtmPrioritySet, FsMIEcfmExtMepCcmLtmPriorityTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepMacAddress}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepLowPrDef}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepLowPrDefGet, FsMIEcfmExtMepLowPrDefSet, FsMIEcfmExtMepLowPrDefTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "2"},

{{14,FsMIEcfmExtMepFngAlarmTime}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepFngAlarmTimeGet, FsMIEcfmExtMepFngAlarmTimeSet, FsMIEcfmExtMepFngAlarmTimeTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "250"},

{{14,FsMIEcfmExtMepFngResetTime}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepFngResetTimeGet, FsMIEcfmExtMepFngResetTimeSet, FsMIEcfmExtMepFngResetTimeTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "1000"},

{{14,FsMIEcfmExtMepHighestPrDefect}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepHighestPrDefectGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepDefects}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepDefectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepErrorCcmLastFailure}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepErrorCcmLastFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepXconCcmLastFailure}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepXconCcmLastFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepCcmSequenceErrors}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepCcmSequenceErrorsGet, FsMIEcfmExtMepCcmSequenceErrorsSet, FsMIEcfmExtMepCcmSequenceErrorsTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepCciSentCcms}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepCciSentCcmsGet, FsMIEcfmExtMepCciSentCcmsSet, FsMIEcfmExtMepCciSentCcmsTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepNextLbmTransId}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepNextLbmTransIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepLbrIn}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepLbrInGet, FsMIEcfmExtMepLbrInSet, FsMIEcfmExtMepLbrInTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepLbrInOutOfOrder}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepLbrInOutOfOrderGet, FsMIEcfmExtMepLbrInOutOfOrderSet, FsMIEcfmExtMepLbrInOutOfOrderTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepLbrBadMsdu}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepLbrBadMsduGet, FsMIEcfmExtMepLbrBadMsduSet, FsMIEcfmExtMepLbrBadMsduTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepLtmNextSeqNumber}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepLtmNextSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepUnexpLtrIn}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepUnexpLtrInGet, FsMIEcfmExtMepUnexpLtrInSet, FsMIEcfmExtMepUnexpLtrInTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepLbrOut}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepLbrOutGet, FsMIEcfmExtMepLbrOutSet, FsMIEcfmExtMepLbrOutTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLbmStatus}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmStatusGet, FsMIEcfmExtMepTransmitLbmStatusSet, FsMIEcfmExtMepTransmitLbmStatusTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "0"},

{{14,FsMIEcfmExtMepTransmitLbmDestMacAddress}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmDestMacAddressGet, FsMIEcfmExtMepTransmitLbmDestMacAddressSet, FsMIEcfmExtMepTransmitLbmDestMacAddressTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLbmDestMepId}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmDestMepIdGet, FsMIEcfmExtMepTransmitLbmDestMepIdSet, FsMIEcfmExtMepTransmitLbmDestMepIdTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLbmDestIsMepId}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmDestIsMepIdGet, FsMIEcfmExtMepTransmitLbmDestIsMepIdSet, FsMIEcfmExtMepTransmitLbmDestIsMepIdTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLbmMessages}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmMessagesGet, FsMIEcfmExtMepTransmitLbmMessagesSet, FsMIEcfmExtMepTransmitLbmMessagesTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "1"},

{{14,FsMIEcfmExtMepTransmitLbmDataTlv}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmDataTlvGet, FsMIEcfmExtMepTransmitLbmDataTlvSet, FsMIEcfmExtMepTransmitLbmDataTlvTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLbmVlanIsidPriority}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmVlanIsidPriorityGet, FsMIEcfmExtMepTransmitLbmVlanIsidPrioritySet, FsMIEcfmExtMepTransmitLbmVlanIsidPriorityTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableGet, FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableSet, FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "1"},

{{14,FsMIEcfmExtMepTransmitLbmResultOK}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "1"},

{{14,FsMIEcfmExtMepTransmitLbmSeqNumber}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLbmSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLtmStatus}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmStatusGet, FsMIEcfmExtMepTransmitLtmStatusSet, FsMIEcfmExtMepTransmitLtmStatusTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "0"},

{{14,FsMIEcfmExtMepTransmitLtmFlags}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmFlagsGet, FsMIEcfmExtMepTransmitLtmFlagsSet, FsMIEcfmExtMepTransmitLtmFlagsTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "0"},

{{14,FsMIEcfmExtMepTransmitLtmTargetMacAddress}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmTargetMacAddressGet, FsMIEcfmExtMepTransmitLtmTargetMacAddressSet, FsMIEcfmExtMepTransmitLtmTargetMacAddressTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLtmTargetMepId}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmTargetMepIdGet, FsMIEcfmExtMepTransmitLtmTargetMepIdSet, FsMIEcfmExtMepTransmitLtmTargetMepIdTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLtmTargetIsMepId}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmTargetIsMepIdGet, FsMIEcfmExtMepTransmitLtmTargetIsMepIdSet, FsMIEcfmExtMepTransmitLtmTargetIsMepIdTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLtmTtl}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmTtlGet, FsMIEcfmExtMepTransmitLtmTtlSet, FsMIEcfmExtMepTransmitLtmTtlTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "64"},

{{14,FsMIEcfmExtMepTransmitLtmResult}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "1"},

{{14,FsMIEcfmExtMepTransmitLtmSeqNumber}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepTransmitLtmEgressIdentifier}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepTransmitLtmEgressIdentifierGet, FsMIEcfmExtMepTransmitLtmEgressIdentifierSet, FsMIEcfmExtMepTransmitLtmEgressIdentifierTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, NULL},

{{14,FsMIEcfmExtMepRowStatus}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepRowStatusGet, FsMIEcfmExtMepRowStatusSet, FsMIEcfmExtMepRowStatusTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 1, NULL},

{{14,FsMIEcfmExtMepCcmOffload}, GetNextIndexFsMIEcfmExtMepTable, FsMIEcfmExtMepCcmOffloadGet, FsMIEcfmExtMepCcmOffloadSet, FsMIEcfmExtMepCcmOffloadTest, FsMIEcfmExtMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEcfmExtMepTableINDEX, 4, 0, 0, "2"},
};
tMibData fscfmeEntry = { 82, fscfmeMibEntry };
#endif /* _FSCFMEDB_H */

