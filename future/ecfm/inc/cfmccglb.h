/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmccglb.h,v 1.14 2012/02/20 12:52:57 siva Exp $
 * 
 * Description: This file contains various global variables on cc task.
 *********************************************************************/
#ifndef _CFM_CC_GLOB_H
#define _CFM_CC_GLOB_H
/*****************************************************************************/
tEcfmCcGlobalInfo  gEcfmCcGlobalInfo;
tEcfmCcContextInfo *gpEcfmCcContextInfo=NULL;
UINT1              gau1EcfmCCPdu[ECFM_MAX_PDU_SIZE];
/*array that stores the Transmission interval value in the millisec
corrosponding to the CCI_INETRVAL macro defined 
Time resolution is 10 ms */
const UINT4 au4CCTimer[ECFM_MAX_CCM_INTERVALS] =
  {
   0,/*CCM Interval field = 0 invalid interval*/ 
   3,    /* Interval field = 1 , tx interval 300 Hz(not supported currently)*/
   10 ,  /* Interval field = 2, Tx interval  = 10ms*/
   100,  /* Interval field = 3 ,Tx interval = 100ms*/
   1000,    /* Interval field = 4, tx interval = 1s(1000ms)*/ 
   10000,   /* Interval field = 5,Tx interval = 10s(10000ms) */
   60000,   /* Interval field = 6 , Tx interval = 1min(60000ms)*/
   600000   /* Interval field = 7, Tx interval = 10min(600000ms)*/
   };


/*array that stores the Transmission interval value in the millisec
corrosponding to the AIS and LCK intervals 
Time resolution is 10 ms */
const UINT4 au4AisLckTimer[ECFM_MAX_AIS_LCK_INTERVALS] =
  {
   0,     /* Interval field = 0 invalid interval*/ 
   1000,    /* Interval field = 1, tx interval = 1s(1000ms)*/ 
   60000,   /* Interval field = 2, Tx interval = 1min(60000ms)*/
   };

   
/*array that stores the Transmission interval value in the millisec
corrosponding to the  LMM intervals
Time resolution is 10 ms */
const UINT4 au4LmmTimer[ECFM_MAX_LMM_INTERVALS] =
  {
   0,       /* Interval field = 0, invalid interval*/ 
   100,     /* Interval field = 1, Tx interval = 100ms*/
   1000,    /* Interval field = 2, Tx interval = 1s(1000ms)*/ 
   10000,   /* Interval field = 3, Tx interval = 10s(10000ms)*/ 
   60000,   /* Interval field = 4, Tx interval = 1min(60000ms)*/
   600000   /* Interval field = 5, Tx interval = 10min(600000ms)*/
   };

/*array that stores the Transmission interval value in the millisec
corrosponding to the  LMM intervals for availability measurement
Time resolution is 10 ms */
const UINT4 au4AvlbltyTimer[ECFM_MAX_AVLBLTY_INTERVALS] =
  {
   0,       /* Interval field = 0, invalid interval*/ 
   100,     /* Interval field = 1, Tx interval = 100ms*/
   1000,    /* Interval field = 2, Tx interval = 1s(1000ms)*/ 
   10000,   /* Interval field = 3, Tx interval = 10s(10000ms)*/ 
   60000,   /* Interval field = 4, Tx interval = 1min(60000ms)*/
   600000,  /* Interval field = 5, Tx interval = 10min(600000ms)*/
   3600000, /* Interval field = 6, Tx interval = 1hour(3600000ms)*/
   86400000 /* Interval field = 7, Tx interval = 1day (86400000ms)*/
   };
UINT4                     gu4ModId;   /* Module Id that is used to identify
                                      * the modules that registered to
                                      * indicate the SF / SF clear
                                      * conditions. */

/*array that stores the Cfa Blocked Oper Status Type  */   
const UINT1 au1BlockedOperType[ECFM_MAX_INTF_OPER_TYPES] =
  {
   CFA_IF_DOWN,   /* CFA_IF_DOWN, value - 2*/ 
   CFA_IF_DEL,    /* CFA_IF_DEL, value - 3 */
   CFA_IF_DORM,   /* CFA_IF_DORM, value - 5 */ 
   CFA_IF_NP,     /* CFA_IF_NP, value - 6 */
   };

UINT1 gu1EcfmCcInitialised = ECFM_FALSE;
UINT1              *gpu1ActiveMdLevels = NULL;
tEcfmCcMaInfo      **gppActiveMaNodes = NULL;
tEcfmCcMepInfo     *gpEcfmCcMepNode = NULL;
#endif /* _CFM_CC_GLOB_H */
/*******************************************************************************
                            End of File cfmccglb.h                              
 ******************************************************************************/
