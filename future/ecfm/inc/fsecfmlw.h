/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsecfmlw.h,v 1.9 2009/09/24 14:30:33 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmOui ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEcfmTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmLtrCacheStatus ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmLtrCacheClear ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmLtrCacheHoldTime ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmLtrCacheSize ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmOui ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEcfmTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmLtrCacheStatus ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmLtrCacheClear ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmLtrCacheHoldTime ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmLtrCacheSize ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmOui ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEcfmTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmLtrCacheStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmLtrCacheClear ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmLtrCacheHoldTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmLtrCacheSize ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmOui ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmLtrCacheStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmLtrCacheClear ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmLtrCacheHoldTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmLtrCacheSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmPortTable. */
INT1
nmhValidateIndexInstanceFsEcfmPortTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmPortTable  */

INT1
nmhGetFirstIndexFsEcfmPortTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmPortTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmPortLLCEncapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsEcfmPortModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsEcfmPortTxCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortTxCcmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortTxLbmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortTxLbrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortTxLtmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortTxLtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortTxFailedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxCcmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxLbmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxLbrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxLtmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxLtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortRxBadCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortFrwdCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmPortDsrdCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmPortLLCEncapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsEcfmPortModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsEcfmPortTxCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortTxCcmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortTxLbmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortTxLbrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortTxLtmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortTxLtrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortTxFailedCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxCcmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxLbmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxLbrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxLtmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxLtrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortRxBadCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortFrwdCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmPortDsrdCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmPortLLCEncapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmPortModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmPortTxCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortTxCcmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortTxLbmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortTxLbrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortTxLtmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortTxLtrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortTxFailedCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxCcmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxLbmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxLbrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxLtmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxLtrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortRxBadCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortFrwdCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmPortDsrdCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmMipCcmDbStatus ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmMipCcmDbClear ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmMipCcmDbSize ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmMipCcmDbHoldTime ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmMemoryFailureCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmBufferFailureCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmUpCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmDownCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmNoDftCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmRdiDftCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmMacStatusDftCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmRemoteCcmDftCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmErrorCcmDftCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmXconDftCount ARG_LIST((UINT4 *));

INT1
nmhGetFsEcfmCrosscheckDelay ARG_LIST((INT4 *));

INT1
nmhGetFsEcfmMipDynamicEvaluationStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmMipCcmDbStatus ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmMipCcmDbClear ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmMipCcmDbSize ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmMipCcmDbHoldTime ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmMemoryFailureCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmBufferFailureCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmUpCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmDownCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmNoDftCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmRdiDftCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmMacStatusDftCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmRemoteCcmDftCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmErrorCcmDftCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmXconDftCount ARG_LIST((UINT4 ));

INT1
nmhSetFsEcfmCrosscheckDelay ARG_LIST((INT4 ));

INT1
nmhSetFsEcfmMipDynamicEvaluationStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmMipCcmDbStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmMipCcmDbClear ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmMipCcmDbSize ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmMipCcmDbHoldTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmMemoryFailureCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmBufferFailureCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmUpCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmDownCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmNoDftCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmRdiDftCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmMacStatusDftCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmRemoteCcmDftCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmErrorCcmDftCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmXconDftCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcfmCrosscheckDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEcfmMipDynamicEvaluationStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmMipCcmDbStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmMipCcmDbClear ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmMipCcmDbSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmMipCcmDbHoldTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmMemoryFailureCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmBufferFailureCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmUpCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmDownCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmNoDftCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmRdiDftCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmMacStatusDftCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmRemoteCcmDftCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmErrorCcmDftCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmXconDftCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmCrosscheckDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcfmMipDynamicEvaluationStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmMipTable. */
INT1
nmhValidateIndexInstanceFsEcfmMipTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmMipTable  */

INT1
nmhGetFirstIndexFsEcfmMipTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmMipTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmMipActive ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEcfmMipRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmMipActive ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEcfmMipRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmMipActive ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMipRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmMipTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmMipCcmDbTable. */
INT1
nmhValidateIndexInstanceFsEcfmMipCcmDbTable ARG_LIST((UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmMipCcmDbTable  */

INT1
nmhGetFirstIndexFsEcfmMipCcmDbTable ARG_LIST((UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmMipCcmDbTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmMipCcmIfIndex ARG_LIST((UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmGlobalCcmOffload ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmGlobalCcmOffload ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmGlobalCcmOffload ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmGlobalCcmOffload ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmDynMipPreventionTable. */
INT1
nmhValidateIndexInstanceFsEcfmDynMipPreventionTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmDynMipPreventionTable  */

INT1
nmhGetFirstIndexFsEcfmDynMipPreventionTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmDynMipPreventionTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmDynMipPreventionRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmDynMipPreventionRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmDynMipPreventionRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmDynMipPreventionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmRemoteMepDbExTable. */
INT1
nmhValidateIndexInstanceFsEcfmRemoteMepDbExTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmRemoteMepDbExTable  */

INT1
nmhGetFirstIndexFsEcfmRemoteMepDbExTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmRemoteMepDbExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmRMepCcmSequenceNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmRMepPortStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepInterfaceStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepCcmDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepRDIDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsEcfmRMepRdi ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepPortStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepInterfaceStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmRMepChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepDbChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEcfmRMepManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsEcfmRMepManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmRMepPortStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepInterfaceStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepCcmDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepRDIDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsEcfmRMepRdi ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepPortStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepInterfaceStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmRMepChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepDbChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEcfmRMepManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsEcfmRMepManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmRMepPortStatusDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepInterfaceStatusDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepCcmDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepRDIDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsEcfmRMepRdi ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepPortStatusTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepInterfaceStatusTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmRMepChassisIdSubtype ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepDbChassisId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEcfmRMepManAddressDomain ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsEcfmRMepManAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmRemoteMepDbExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmLtmTable. */
INT1
nmhValidateIndexInstanceFsEcfmLtmTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmLtmTable  */

INT1
nmhGetFirstIndexFsEcfmLtmTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmLtmTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsEcfmLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsEcfmMepExTable. */
INT1
nmhValidateIndexInstanceFsEcfmMepExTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmMepExTable  */

INT1
nmhGetFirstIndexFsEcfmMepExTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmMepExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmXconnRMepId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmErrorRMepId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepDefectRDICcm ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepDefectMacStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepDefectRemoteCcm ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepDefectErrorCcm ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepDefectXconnCcm ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepCcmOffload ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsEcfmMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsEcfmMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmXconnRMepId ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmErrorRMepId ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepDefectRDICcm ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepDefectMacStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepDefectRemoteCcm ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepDefectErrorCcm ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepDefectXconnCcm ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepCcmOffload ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsEcfmMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsEcfmMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmXconnRMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmErrorRMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepDefectRDICcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepDefectMacStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepDefectRemoteCcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepDefectErrorCcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepDefectXconnCcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepCcmOffload ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsEcfmMepLbrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepLbrInOutOfOrder ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepLbrBadMsdu ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepUnexpLtrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepLbrOut ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepCcmSequenceErrors ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsEcfmMepCciSentCcms ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmMepExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmMdExTable. */
INT1
nmhValidateIndexInstanceFsEcfmMdExTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmMdExTable  */

INT1
nmhGetFirstIndexFsEcfmMdExTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmMdExTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmMepArchiveHoldTime ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmMepArchiveHoldTime ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmMepArchiveHoldTime ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmMdExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEcfmMaExTable. */
INT1
nmhValidateIndexInstanceFsEcfmMaExTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEcfmMaExTable  */

INT1
nmhGetFirstIndexFsEcfmMaExTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEcfmMaExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmMaCrosscheckStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmMaCrosscheckStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmMaCrosscheckStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmMaExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEcfmTrapControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEcfmTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEcfmTrapControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEcfmTrapControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEcfmTrapControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
