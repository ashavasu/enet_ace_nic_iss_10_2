/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmccapi.h,v 1.5 2008/10/21 06:30:57 prabuc-iss Exp $
 * 
 * Description: This file contains the Prototypes of the routines
 *              exported by CC Task.
 *********************************************************************/

#ifndef _CFM_CC_API_H
#define _CFM_CC_API_H
/******************************************************************************/
/* Prototypes for Routines exported by CC Task */
#endif /* _CFM_CC_API_H */
/*******************************************************************************
 *                   End of file cfmccapi.h
 ******************************************************************************/
