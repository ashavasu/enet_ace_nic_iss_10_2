/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmdef.h,v 1.87 2016/07/25 07:25:10 siva Exp $
 * 
 * Description: This file contains the constant definations for both 
 *              CC and LBLT Task
 *********************************************************************/

#ifndef _CFM_DEF_H
#define _CFM_DEF_H
/*****************************************************************************/
/*Maximum values for various typedefs*/
#define     ECFM_UINT1_MAX  0xFF
#define     ECFM_UINT1_MIN  0x1
#define     ECFM_UINT2_MAX  0xFFFF
#define     ECFM_UINT4_MAX  0xFFFFFFFF
#define     ECFM_WORD_SIZE  4
/*****************************************************************************/
/* ECFM Module Indications */
#define ECFM_IND_MODULE_ENABLE              0   /* Indications of Module is 
                                                 * enable 
                                                 */
#define ECFM_IND_MODULE_DISABLE             1   /* Indications that module is 
                                                 * disabled
                                                 */
#define ECFM_IND_MEP_ACTIVE                 4   /* Indications that MEP is 
                                                 * enabled
                                                 */
#define ECFM_IND_MEP_INACTIVE               5   /* Indications that MEP is not 
                                                 * active
                                                 */
#define ECFM_IND_IF_DELETE                  6   /* Indications that interface is 
                                                 * deleted
                                                 */
/*****************************************************************************/
#define ECFM_TRANSPORT_ADDR_LENGTH      6   /* Length of the transport mac 
                                             * address
                                             */
#define ECFM_CHASSIS_ID_LENGTH          255 /* Chassis id length */
#define ECFM_PORT_ID_LENGTH             255 /* Port Id length */
#define ECFM_MAN_ADDRESS_LENGTH         255 /* Management address length */
#define ECFM_MAC_ADDR_LENGTH            6   /* Mac Address Size*/
#define ECFM_EGRESS_ID_LENGTH           8
#define ECFM_OUI_LENGTH                 3   /* OUI Value size */
#define ECFM_ETH_SRC_DST_MAC_ADDR_LEN   12

#define ECFM_DEF_MSG_LEN                 OSIX_DEF_MSG_LEN
/*****************************************************************************/
/* CLI related Events for command handler */
#define ECFM_EV_CLI_MD_CREATE           11  /* CLI Command for the MD Creation*/
#define ECFM_EV_CLI_MA_CREATE           12  /* CLI Command for the MA Creation*/
#define ECFM_EV_CLI_MEP_CREATE          13  /* CLI Command for the MEP 
                                             * Creation
                                             */
#define ECFM_EV_CLI_CFM_ENABLE          14  /* CLI Command for the CFM Enable */
#define ECFM_EV_CLI_TRANSMIT_CCM        15  /* CLI Command for the transmitting 
                                             * the CCM.
                                             */
#define ECFM_EV_CLI_CFM_DISABLE         16  /* CLI Command for the CFM Disable*/ 
#define ECFM_EV_CLI_CCM_DISABLE         17  /* CLI Command for the CFM Disable*/
/*****************************************************************************/
/*Following events are common for all the state machines running in context of  
  CC and LBLT*/
#define ECFM_EV_MEP_BEGIN                  0
#define ECFM_EV_MEP_NOT_ACTIVE             1
/*****************************************************************************/
/*Constants Related to VLAN information in the received packet*/
#define ECFM_VLAN_TAGGED                      VLAN_TAGGED
#define ECFM_VLAN_UNTAGGED                    VLAN_UNTAGGED

/* Bit map to store the ids of vlans supported by the device */
#define ECFM_CVLAN_LIST_SIZE                  VLAN_DEV_VLAN_LIST_SIZE_EXT
/*****************************************************************************/
/*Pdu Size*/
#define ECFM_MAX_CCM_PDU_SIZE   128   /* Maximum size of the CCM PDU */
#define ECFM_MAX_JUMBO_PDU_SIZE  9000                /* Maximum size for the
                                                        Jumbo packets 
                                                      */
#define ECFM_MAX_LBM_PDU_SIZE   (ECFM_MAX_JUMBO_PDU_SIZE)  /* Maximum size of the LBM 
                                                            * PDU 
                                                            */
#define ECFM_MAX_LBR_PDU_SIZE   (ECFM_MAX_JUMBO_PDU_SIZE)  /* Maximum size of the LBR 
                                                            * PDU 
                                                            */
#define ECFM_MAX_LTM_PDU_SIZE   (ECFM_MAX_PDU_SIZE)  /* Maximum size of the LTM 
                                                      * PDU 
                                                      */
#define ECFM_MAX_LTR_PDU_SIZE   (ECFM_MAX_PDU_SIZE)  /* Maximum size of the LTR 
                                                      * PDU 
                                                      */
#define ECFM_MAX_DMM_PDU_SIZE   (ECFM_MAX_PDU_SIZE)  /* Maximum size of the DMM
                                                      * PDU
                                                      */
#define ECFM_MAX_AIS_PDU_SIZE   (32)                 /* Maximum size of the AIS
                                                      * PDU
                                                      */
#define ECFM_MAX_LCK_PDU_SIZE   (32)                 /* Maximum size of the LCK
                                                      * PDU
                                                      */
#define ECFM_MAX_1DM_PDU_SIZE   (ECFM_MAX_PDU_SIZE) /* Maximum size of the 1DM
                                                     * PDU
                                                     */
#define ECFM_MAX_LMM_PDU_SIZE   (ECFM_MAX_PDU_SIZE) /* Maximum size of the LMM 
                                                     * PDU
                                                     */
#define ECFM_MAX_TST_PDU_SIZE   (ECFM_MAX_JUMBO_PDU_SIZE) /* Maximum size of the TST
                                                           * PDU
                                                           */
#define ECFM_MAX_TH_VSM_PDU_SIZE   (ECFM_MAX_PDU_SIZE) /* Maximum size of the TH VSM
                                                     * PDU
                                                     */

#define ECFM_APS_PDU_SIZE 4  /* Size of APS data */

#define ECFM_Y1564_PDU_SIZE ECFM_MAX_PDU_SIZE
#define ECFM_RFC2544_PDU_SIZE ECFM_MAX_JUMBO_PDU_SIZE


#define ECFM_MPLSTP_SERVICE_PTR_SIZE  (sizeof(tServicePtr))
#define ECFM_MPLSTP_OUTPARAMS_SIZE    (sizeof(tMplsApiOutInfo))
#define ECFM_MPLSTP_INPARAMS_SIZE     (sizeof(tMplsApiInInfo))
#define ECFM_MPLSTP_PATHINFO_SIZE     (sizeof(tEcfmMplsTpPathInfo))
#define ECFM_MPTP_LBLT_OUTPARAMS_SIZE (sizeof(tMplsApiOutInfo))
#define ECFM_MPTP_LBLT_INPARAMS_SIZE  (sizeof(tMplsApiInInfo))
#define ECFM_MPTP_LBLT_PATHINFO_SIZE  (sizeof(tEcfmMplsTpPathInfo))
#define ECFM_LTR_CACHE_STRUCT_SIZE    (sizeof(tEcfmLtrCacheIndices)*\
                                       ECFM_LTR_CACHE_DEF_SIZE)
#define ECFM_MPLSTP_DEF_LEVEL          7
#define ECFM_AVLBLTY_INFO_SIZE         (sizeof(tEcfmCcAvlbltyInfo))
#define ECFM_MAX_AVLBLTY_SESSION       10

/*****************************************************************************/
/*Possible Opcode values for CFM-PDU*/
#define ECFM_OPCODE_CCM         1 /* OpCode for CCM PDUs */
#define ECFM_OPCODE_LBR         2 /* OpCode for LBR PDUs */
#define ECFM_OPCODE_LBM         3 /* OpCode for LBM PDUs */
#define ECFM_OPCODE_LTR         4 /* OpCode for LTR PDUs */
#define ECFM_OPCODE_LTM         5 /* OpCode for LTM PDUs */
#define ECFM_OPCODE_AIS         33/* OpCode for AIS PDUs */
#define ECFM_OPCODE_LCK         35/* OpCode for LCK PDUs */
#define ECFM_OPCODE_TST         37/* OpCode for TST PDUs */
#define ECFM_OPCODE_APS         39/* OpCode for APS PDUs */
#define ECFM_OPCODE_RAPS        40/* OpCode for RAPS PDUs */
#define ECFM_OPCODE_MCC         41/* OpCode for MCC PDUs */
#define ECFM_OPCODE_LMM         43/* OpCode for LMM PDUs */
#define ECFM_OPCODE_LMR         42/* OpCode for LMR PDUs */
#define ECFM_OPCODE_1DM         45/* OpCode for 1DM PDUs */
#define ECFM_OPCODE_DMM         47/* OpCode for DMM PDUs */
#define ECFM_OPCODE_DMR         46/* OpCode for DMR PDUs */
#define ECFM_OPCODE_EXM         49/* OpCode for EXM PDUs */
#define ECFM_OPCODE_EXR         48/* OpCode for EXR PDUs */
#define ECFM_OPCODE_VSM         51/* OpCode for VSM PDUs */
#define ECFM_OPCODE_VSR         50/* OpCode for VSR PDUs */
/*****************************************************************************/
/* Encoding Constants*/
/* Various possible TLV types*/
#define ECFM_END_TLV_TYPE                   0 /* End TLV Type transmitted in 
                                               * the PDUs
                                               */
#define ECFM_SENDER_ID_TLV_TYPE             1 /* Type of Sender ID TLV 
                                               * transmitted in the PDUs
                                               */
#define ECFM_PORT_STATUS_TLV_TYPE           2 /* Type of the Port Status 
                                               * TLV transmitted in the PDUs
                                               */
#define ECFM_INTERFACE_STATUS_TLV_TYPE      4 /* Type of the interface 
                                               * status TLV transmitted in the 
                                               * PDUs
                                               */
#define ECFM_DATA_TLV_TYPE                  3 /* Type of Data TLV 
                                               * transmitted in the PDUs
                                               */
#define ECFM_REPLY_INGRESS_TLV_TYPE         5 /* Reply Ingress TLV Type 
                                               * transmitted in the PDUs
                                               */
#define ECFM_REPLY_EGRESS_TLV_TYPE          6 /* Reply Egress TLV Type
                                               * transmitted 
                                               * in the PDUs
                                               */
#define ECFM_LTM_EGRESS_ID_TLV_TYPE         7 /* Egress ID TLV Type
                                               * transmitted in 
                                               * the LTM PDUs
                                               */
#define ECFM_LTR_EGRESS_ID_TLV_TYPE         8 /* Egress id TLV Type
                                               * transmitted in 
                                               * the LTR PDUs
                                               */
#define ECFM_TEST_TLV_TYPE                  32 /* Test-Tlv Type to be transmitted in 
                                                * LBM/LBR and TST
                                                */
#define ECFM_ORG_SPEC_TLV_TYPE              31/* Organisation Specific 
                                               * TLV Type transmitted in the 
                                               * PDUs
                                               */
/*****************************************************************************/
/* Encoding Constants*/
/* CCM Specific TLV Encoding*/
/* CCM TlV Sizes*/
#define ECFM_PORT_STATUS_VALUE_SIZE         1      /* Port status tlv size */
#define ECFM_INTERFACE_STATUS_VALUE_SIZE    1      /* Size of the interface 
                                                    * status tlv 
                                                    */
#define ECFM_MAID_FIELD_SIZE                48 /* Max length of the MAID */
#define ECFM_MEGID_FIELD_SIZE               ECFM_MAID_FIELD_SIZE
#define ECFM_CCM_MEGID_PADDING_SIZE         32
#define ECFM_ITU_RESERVE_FIELD_SIZE         16 /* Length of the Reserved 
                                                * field 
                                                */

#define ECFM_CCM_RESERVE_FIELD_SIZE         4 /* Size of CCM's Reserive Field
                                               */
/* Constants to get specific fields from MEG-ID */
#define ECFM_MEGID_RESERVED_VALUE           ECFM_DOMAIN_NAME_TYPE_NONE 
#define ECFM_MEGID_FORMAT_TYPE              ECFM_ASSOC_NAME_ICC  
#define ECFM_CARRIER_CODE_SIZE              6
#define ECFM_CARRIER_CODE_ARRAY_SIZE        (ECFM_CARRIER_CODE_SIZE)+1
/* UMC max code size is 12.*/
#define ECFM_UMC_CODE_SIZE                  12
#define ECFM_UMC_CODE_ARRAY_SIZE            (ECFM_UMC_CODE_SIZE)+1
#define ECFM_MEG_ID_LEN                     13  /* MA Name Length for ICC Format
                                                 */
#define ECFM_MD_NAME_FRMT_FIELD_SIZE        1
#define ECFM_MD_NAME_LEN_FIELD_SIZE         1
#define ECFM_MA_NAME_FRMT_FIELD_SIZE        1
#define ECFM_MA_NAME_LEN_FIELD_SIZE         1
#define ECFM_MA_NAME_LENGTH_IF_MD_NONE      13 
#define ECFM_MIN_MA_NAME_LEN_OFFSET        (ECFM_MD_NAME_FRMT_FIELD_SIZE\
                                            +ECFM_MD_NAME_LEN_FIELD_SIZE\
                                            +ECFM_MA_NAME_FRMT_FIELD_SIZE)
#define ECFM_CCM_SEQ_NUM_FIELD_SIZE         4  /* CCM seq number length */
#define ECFM_MEP_ID_FIELD_SIZE              2  /* MEP id length */

/* LBM Specific TLV Encoding*/
#define ECFM_LBM_SEQ_NUM_FIELD_SIZE        4  /* Size of LBM Sequence Number
                                                 field 
                                               */
#define ECFM_LBLT_TEST_TLV_CRC_SIZE         4  /* Size of CRC field 
                                                */
#define ECFM_TST_PATTERN_TYPE_FIELD_SIZE    1  /* Size of Test TLV pattern type
                                                * field*/
#define ECFM_TST_SEQ_NUM_FIELD_SIZE        4  /* Size of TST Sequence Number
                                                 field 
                                               */
/* LBR Specific TLV Encoding*/

#define ECFM_LBR_TRANS_ID_FIELD_SIZE        4  /* Size of LBM Transaction 
                                                * Identifier field 
                                                */
/* LTM Specific TLV Encoding*/

#define ECFM_LTM_TRANS_ID_FIELD_SIZE        4  /* Size of LTM Transaction 
                                                * Identifier
                                                */
#define ECFM_LTM_TTL_FIELD_SIZE             1
#define ECFM_ORGINAL_MAC_ADDR_FIELD_SIZE    (ECFM_MAC_ADDR_LENGTH)
#define ECFM_TARGET_MAC_ADDR_FIELD_SIZE     (ECFM_MAC_ADDR_LENGTH)
#define ECFM_LTM_EGRESS_ID_VALUE_SIZE       (ECFM_EGRESS_ID_LENGTH)  

/* LTR Specific TLV Encoding*/

#define ECFM_LAST_EGRESS_ID_FIELD_SIZE      (ECFM_EGRESS_ID_LENGTH)  
#define ECFM_NEXT_EGRESS_ID_FIELD_SIZE      (ECFM_EGRESS_ID_LENGTH)  
#define ECFM_LTR_EGRESS_ID_VALUE_SIZE       (ECFM_LAST_EGRESS_ID_FIELD_SIZE+\
                                             ECFM_NEXT_EGRESS_ID_FIELD_SIZE)

#define ECFM_INGRESS_ACTION_FIELD_SIZE      1
#define ECFM_EGRESS_ACTION_FIELD_SIZE       1

#define ECFM_INGRESS_MAC_ADDR_FIELD_SIZE    (ECFM_MAC_ADDR_LENGTH)
#define ECFM_EGRESS_MAC_ADDR_FIELD_SIZE     (ECFM_MAC_ADDR_LENGTH)

#define ECFM_INGRESS_PORTID_LEN_FIELD_SIZE  1
#define ECFM_EGRESS_PORTID_LEN_FIELD_SIZE   1

#define ECFM_INGRESS_PORTID_SUBTYPE_FIELD_SIZE   1 
#define ECFM_EGRESS_PORTID_SUBTYPE_FIELD_SIZE    1 

#define ECFM_LTR_TRANS_ID_FIELD_SIZE        4  /* Size of LTR Transaction 
                                                * Identifier
                                                */
#define ECFM_REPLY_TTL_FIELD_SIZE           1

#define ECFM_RELAY_ACTION_FIELD_SIZE        1  /* Size of Chassis-Id SubType 
                                                * filed in the SenderId TLV
                                                */
#define ECFM_CHASSIS_COMPONENT_CHASSIS_ID        1
#define ECFM_INTERFACE_ALIAS_CHASSIS_ID          2
#define ECFM_PORT_COMPONENT_CHASSIS_ID           3
#define ECFM_MAC_ADDRESS_CHASSIS_ID              4
#define ECFM_NETWORK_ADDRESS_CHASSIS_ID          5
#define ECFM_INTERFACE_NAME_CHASSIS_ID           6
#define ECFM_LOCAL_CHASSIS_ID                    7

/* Constants defining the RelayAction transmitted in LTR */
#define ECFM_LTR_RLY_HIT                    1
#define ECFM_LTR_RLY_FDB                    2
#define ECFM_LTR_RLY_MPDB                   3

/* Constatans defining the Ingress Action values to be transmitted in LTR */
#define ECFM_LBLT_PORT_FILTERING_ACTION_ING_NOTLV   0
#define ECFM_LBLT_PORT_FILTERING_ACTION_ING_OK      1
#define ECFM_LBLT_PORT_FILTERING_ACTION_ING_DOWN    2
#define ECFM_LBLT_PORT_FILTERING_ACTION_ING_BLOCKED 3
#define ECFM_LBLT_PORT_FILTERING_ACTION_ING_VID     4

/* Constatans defining the Egress Action values to be transmitted in LTR */
#define ECFM_LBLT_PORT_FILTERING_ACTION_EGR_NOTLV   0
#define ECFM_LBLT_PORT_FILTERING_ACTION_EGR_OK      1
#define ECFM_LBLT_PORT_FILTERING_ACTION_EGR_DOWN    2
#define ECFM_LBLT_PORT_FILTERING_ACTION_EGR_BLOCKED 3
#define ECFM_LBLT_PORT_FILTERING_ACTION_EGR_VID     4

/* Common Encoding*/
#define ECFM_CHASSIS_ID_SUB_TYPE_FIELD_SIZE     1 /* Size of Chassis-Id SubType 
                                                   * filed in the SenderId TLV
                                                   */
#define ECFM_CHASSIS_ID_LENGTH_FIELD_SIZE       1 /* Size of Chassis-id length  
                                                   * field in senderId Tlv
                                                   */
#define ECFM_MGMT_ADDR_DOMAIN_LENGTH_FIELD_SIZE 1 /* Size of Management address 
                                                   * domain length field in 
                                                   * senderId Tlv
                                                   */
#define ECFM_MGMT_ADDR_LENGTH_FIELD_SIZE        1 /* Size of Management address
                                                   * length  field in senderId 
                                                   * Tlv
                                                   */
#define ECFM_ORG_SPEC_SUB_TYPE_FIELD_SIZE       1 /* Size of SubType filed in 
                                                   * the organizational sepcific 
                                                   * TLV
                                                   */

#define ECFM_OUI_FIELD_SIZE                 (ECFM_OUI_LENGTH)/* OUI Value size 
                                                              */
#define ECFM_TLV_LENGTH_FIELD_SIZE          2 /* Size of Length field in every 
                                               * CFM PDU TLV
                                               */
#define ECFM_TLV_TYPE_FIELD_SIZE            1 /* Size of type field in every 
                                               * CFM PDU TLV
                                               */
#define ECFM_TLV_TYPE_LEN_FIELD_SIZE        (ECFM_TLV_TYPE_FIELD_SIZE + \
                                             ECFM_TLV_LENGTH_FIELD_SIZE)
#define ECFM_ORG_SPEC_MIN_LENGTH            4
#define ECFM_ORG_SPEC_SUBTYPE_VALUE         0

/* Constants for 1DM and DMM PDU*/
#define ECFM_DMM_RESERVED_SIZE             24 /* Size of reserved bytes to be filled 
                                               * in DMM frame
                                               */
#define ECFM_TIMESTAMP_FIELD_SIZE          8  /* Size of the TimeStamp field used in 
                                                 1DM and DMM*/
#define ECFM_1DM_RESERVED_SIZE             8 /* Size of reserved bytes to be filled
                                              * in 1DM frame
                                              */
#define ECFM_1DM_RCV_CAPABILITY_ENABLED     1
#define ECFM_1DM_RCV_CAPABILITY_DISABLED    2
/*CFM-PDU Header Fileds sizes*/
#define ECFM_MDLEVEL_VER_FIELD_SIZE         1
#define ECFM_OPCODE_FIELD_SIZE              1
#define ECFM_FLAGS_FIELD_SIZE               1
#define ECFM_FIRST_TLV_OFFSET_FIELD_SIZE    1
#define ECFM_DATA_COUNTER_FIELD_SIZE        4

/* CFM-PDUs First TLVOffset values */
#define ECFM_CCM_FIRST_TLV_OFFSET           70 /* Value of  first tlv offset 
                                                * for CCM PDU
                                                */
#define ECFM_LBR_FIRST_TLV_OFFSET           4  /* Value of  first tlv offset 
                                                * for LBR PDU
                                                */
#define ECFM_LBM_FIRST_TLV_OFFSET           4  /* Value of  first tlv offset 
                                                * for LBM PDU
                                                */
#define ECFM_LTR_FIRST_TLV_OFFSET           6  /* Value of  first tlv offset 
                                                * for LTR PDU
                                                */
#define ECFM_LTM_FIRST_TLV_OFFSET           17 /* Value of  first tlv offset 
                                                * for LTM PDU
                                                */
#define ECFM_DMM_FIRST_TLV_OFFSET           32 /* Value for first tlv offset 
                                                * for DMM PDU
                                                */
#define ECFM_DMR_FIRST_TLV_OFFSET           32 /* Value for first tlv offset 
                                                * for DMR PDU
                                                */

#define ECFM_FIRST_TLV_OFFSET_OFFSET        3  /* Offset used to get the First 
                                                * TLV offset value from the CFM 
                                                * header
                                                */
#define ECFM_FIRST_TLV_OFFSET_SIZE          1  /* Size of the FirstTLVOffset 
                                                * field of the CFM PDU
                                                */
#define ECFM_1DM_FIRST_TLV_OFFSET           16 /* Value for first tlv offset
                                                * for 1DM PDU
                                                */
#define ECFM_LMM_FIRST_TLV_OFFSET           12 /* Value for first TLV offset */

#define ECFM_LMR_FIRST_TLV_OFFSET           ECFM_LMM_FIRST_TLV_OFFSET
#define ECFM_TH_VSM_FIRST_TLV_OFFSET        0 /* Value for first TLV offset */
#define ECFM_TH_VSR_FIRST_TLV_OFFSET        0 /* Value for first TLV offset */
#define ECFM_TST_FIRST_TLV_OFFSET           4  /* Value of  first tlv offset 
                                                * for TST PDU
                                                */
#define ECFM_CREATE             1
#define ECFM_DELETE             2
/*****************************************************************************/
/* Possible Maintenance Point Direction */
#define ECFM_MP_DIR_DOWN        1   /* MP Direction */
#define ECFM_MP_DIR_UP          2   /* MP Direction */
/*****************************************************************************/
/* Header Length */
#define ECFM_ETH_HDR_SIZE       14  /* Maximum size of the Ethernet headers to  
                                     * be attached with PDU 
                                     */
#define ECFM_VLAN_HDR_SIZE      4   /* Maximum size of the VLAN headers to be 
                                     * attached with PDU
                                     */
#define ECFM_ISID_TAG_SIZE      6   /* Maximum size of the ISID tags*/
#define ECFM_ISID_HDR_SIZE      18   /* Maximum size of the ISID headers to be 
                                     * attached with PDU
                                     */
          
#define ECFM_LLC_SNAP_HDR_SIZE  8   /* Maximum size of the LLC SNAP 
                                     * headers to be attached with 
                                     * PDU
                                     */
#define ECFM_CFM_HDR_SIZE       4   /* Maximum size of the CFM headers to be 
                                     * attached with PDU
                                     */

/* The Max of ECFM_ETHER_TYPE_LEN (2B), ECFM_LLC_SNAP_HDR_SIZE (8B) is
 * assigned to the below macro.
 */
#define ECFM_MAX_OF_ETH_OR_LLC_LEN  ECFM_LLC_SNAP_HDR_SIZE

#define ECFM_CTRL_PKT_OFFSET ( ECFM_ETH_SRC_DST_MAC_ADDR_LEN + \
                               ECFM_MAX_OF_ETH_OR_LLC_LEN + \
                               (2 * ECFM_VLAN_HDR_SIZE) )

/* Type and length size in the  Eth or VLAN or in the LLC headers */
#define ECFM_ETH_TYPE_LEN_FIELD_SIZE  2  
#define ECFM_VLAN_TYPE_LEN_FIELD_SIZE 2  
#define ECFM_LLC_TYPE_LEN_FIELD_SIZE  2  
#define ECFM_CFM_PDU_TYPE_FIELD_SIZE    2
/*****************************************************************************/
/*ECFM Protocol Version*/
#define ECFM_PROTOCOL_VERSION           0
/*****************************************************************************/
/* Protocol and Packet Types*/
#define ECFM_DST_LSAP_IN_LLC_SNAP_HDR       0xAA/* Destination address in LLC 
                                                 * snap hdr
                                                 */
#define ECFM_SRC_LSAP_IN_LLC_SNAP_HDR       0xAA/*  Source addressLLC snap hdr*/
#define ECFM_CONTROL_BYTE_IN_LLC_SNAP_HDR   0x03/* Control byte in LLC snap     
                                                 * hdr
                                                 */
/* define ECFM_LLC_CFM_TYPE                 0x01*/ /* CFM PKT type */ 
#define ECFM_LLC_CFM_TYPE                   0x8902 /* CFM PKT type */

#define ECFM_LLC_CFM_TYPE_OFFSET_1          12
#define ECFM_LLC_CFM_TYPE_OFFSET_2          16
#define ECFM_LLC_CFM_TYPE_OFFSET_3          30 
#define ECFM_LLC_CFM_TYPE_OFFSET_4          34
#define ECFM_LLC_CFM_TYPE_OFFSET_5          38 
/*****************************************************************************/
#define ECFM_MAX_LOG_STR_LEN            256     /* used for traces */
/*****************************************************************************/
/*Various Possible Events*/
#define ECFM_EV_CFM_PDU_IN_QUEUE        0x01
#define ECFM_EV_CFG_MSG_IN_QUEUE        0x02
#define ECFM_EV_TMR_EXP                 0x04
#define ECFM_EV_RED_BULK_UPD            0x08
#define ECFM_EV_INT_PDU_IN_QUEUE         0x10
#define ECFM_EV_INT_QUEUE_OVERFLOW       0x20
#define ECFM_EV_ALL                     (ECFM_EV_CFM_PDU_IN_QUEUE|\
                                         ECFM_EV_CFG_MSG_IN_QUEUE|\
                                         ECFM_EV_INT_PDU_IN_QUEUE|\
                                         ECFM_EV_INT_QUEUE_OVERFLOW|\
                                         ECFM_EV_TMR_EXP|\
                                         ECFM_EV_RED_BULK_UPD)
/*****************************************************************************/
/*Various Possible Message Related to "ECFM_EV_MSG_IN_QUEUE"*/
#define ECFM_CREATE_PORT_MSG            0
#define ECFM_DELETE_PORT_MSG            1
#define ECFM_OPER_STATUS_CHG_MSG        2
#define ECFM_PDU_RCV_FRM_CFA            3
#define ECFM_MAP_PORT_MSG               4
#define ECFM_UNMAP_PORT_MSG             5
#define ECFM_CREATE_CONTEXT_MSG         6
#define ECFM_DELETE_CONTEXT_MSG         7
#define ECFM_CC_START_TRANSACTION       8
#define ECFM_LB_START_TRANSACTION       9
#define ECFM_LB_STOP_TRANSACTION        10
#define ECFM_DM_START_TRANSACTION       11 
#define ECFM_DM_STOP_TRANSACTION        12
#define ECFM_LT_START_TRANSACTION       13
#define ECFM_MCC_TRANSMIT               14
#define ECFM_EXM_TRANSMIT               15
#define ECFM_EXR_TRANSMIT               16
#define ECFM_VSM_TRANSMIT               17
#define ECFM_VSR_TRANSMIT               18
#define ECFM_APS_TRANSMIT               19
#define ECFM_LM_START_TRANSACTION       20
#define ECFM_LM_STOP_TRANSACTION        21
#define ECFM_AIS_START_TRANSACTION      22
#define ECFM_AIS_STOP_TRANSACTION       23
#define ECFM_LCK_START_TRANSACTION      24
#define ECFM_LCK_STOP_TRANSACTION       25
#define ECFM_TST_START_TRANSACTION      26
#define ECFM_TST_STOP_TRANSACTION       27
#define ECFM_CALL_BACK                  28
#define ECFM_CREATE_VLAN                29
#define ECFM_MSTP_ENABLE                31
#define ECFM_MSTP_DISABLE               32
#define ECFM_VLAN_MEMBER_CHANGE         33
#define ECFM_DELETE_VLAN                30
#define ECFM_PORT_STATE_CHANGE          34
#define ECFM_TX_FAILURE_CALL_BACK       35
/* sender-id*/
#define ECFM_UPDATE_LOC_SYS_INFO        36
#define ECFM_IP4_MAN_ADDR_CHG_MSG       37
#define ECFM_IP6_MAN_ADDR_CHG_MSG       38
/* TH related*/
#define ECFM_TH_STOP_TRANSACTION        39   /* Stop TH Transaction event*/
#define ECFM_TH_START_TRANSACTION       40   /* Start TH Transaction event*/
#define ECFM_CREATE_ISID                41
#define ECFM_DELETE_ISID                42
#define ECFM_INTF_TYPE_CHANGE           43
#define ECFM_ADD_TO_PORT_CHANNEL        44
#define ECFM_REMOVE_FROM_PORT_CHANNEL   45
#define ECFM_VLAN_UPDATE_DEI_BIT        46
#define ECFM_VLAN_ETHER_TYPE_CHANGE     47
#define ECFM_MAX_EVENT                  48

#define ECFM_MPLS_PATH_STATUS_CHG       49
#define ECFM_EV_MPLSTP_CC_PDU_IN_QUEUE  50
#define ECFM_AIS_COND_ENTRY             51
#define ECFM_AIS_COND_EXIT              52
#define ECFM_MPLS_BULK_PATH_STATUS_IND  53
#define ECFM_EV_MPLSTP_LBLT_PDU_IN_QUE  54
#define ECFM_AVLBLTY_START_TRANSACTION  55 
#define ECFM_AVLBLTY_STOP_TRANSACTION   56
#define ECFM_RX_CALL_BACK_FROM_HW       57
#define ECFM_TX_FAILURE_CALL_BACK_FROM_HW 58
#define ECFM_LT_STOP_TRANSACTION        59
#define ECFM_PB_PORT_STATE_CHANGE 60

/********************************************************************/
/* Default Datalengths */
#define ECFM_APS_DATA_LENGTH           4
#define ECFM_OFFSET_EX_API_LENGTH      9

#define ECFM_OFFSET_FOR_FLAG            2
#define ECFM_OFFSET_FOR_OUI             3
/* SNMP Constants */
/* 1DM/DMM Messeges constants*/
#define ECFM_DM_MESSAGE_MAX                 8192

/*TH Messages constants*/
#define ECFM_TH_MESSAGE_MIN            0
#define ECFM_TH_MESSAGE_MAX            8192
#define ECFM_TH_PPS_MIN                1
#define ECFM_TH_PPS_MAX                100000

#define ECFM_TH_DEADLINE_MIN           0
#define ECFM_TH_DEADLINE_MAX           172800
#define ECFM_TH_BURSTDEADLINE_MIN      10 /*Min Timer value in millisec supported*/
#define ECFM_TH_BURSTDEADLINE_MAX      172800000
#define ECFM_LBLT_TH_BURST_TYPE_MSGS   1 /* Burst Type Messages*/
#define ECFM_LBLT_TH_BURST_TYPE_DL     2 /* Burst Type Deadline*/

/*TST Messages constants*/
#define ECFM_TST_MESSAGE_MIN                0
#define ECFM_TST_MESSAGE_MAX                8192
#define ECFM_TST_DEADLINE_MIN               0
#define ECFM_TST_DEADLINE_MAX               172800
/* LBM Messeges constants*/
#define ECFM_LBM_MESSAGE_MIN                1
#define ECFM_LBM_MESSAGE_MAX                1024
#define Y1731_LBM_MESSAGE_MAX               8192
/* Ports Validation constants*/
#define ECFM_BRG_PLUS_LOG_PORTS_MIN         1
#define ECFM_BRG_PLUS_LOG_PORTS_MAX         BRG_MAX_PHY_PLUS_LOG_PORTS
#define ECFM_INTERNAL_IFACES_MIN            CFA_MIN_INTERNAL_IF_INDEX
#define ECFM_INTERNAL_IFACES_MAX            CFA_MAX_SISP_IF_INDEX
/* Per context Ports Validation constants*/
#define ECFM_PORTS_PER_CONTEXT_MIN          1
#define ECFM_PORTS_PER_CONTEXT_MAX          ECFM_MAX_PORTS_PER_CONTEXT
/* MD Index Validation constants*/
#define ECFM_MD_INDEX_MAX                   (ECFM_UINT4_MAX)
#define ECFM_MD_INDEX_MIN                   1               
/* MA Index Validation constants*/
#define ECFM_MA_INDEX_MAX                   (ECFM_UINT4_MAX)
#define ECFM_MA_INDEX_MIN                   1
/* MEP ID Validation constants*/
#define ECFM_MEPID_MAX                      8191 
#define ECFM_MEPID_MIN                      1
/*MD Level Validation constants*/
#define ECFM_MD_LEVEL_MAX                   7
#define ECFM_MD_LEVEL_MIN                   0
#define ECFM_MD_LEVEL_NOT_CONFIGURE         -1
/* Implicit MIP creation related */
#define ECFM_MIP_ACTIVE_MD_LEVELS_MAX       9
/* MEP VlanPriority Validation constants*/
#define ECFM_VLAN_PRIORITY_MIN              0 
#define ECFM_VLAN_PRIORITY_MAX              (ECFM_DEF_VLAN_PRIORITY) 
/* MEP FngAlarmTime Validation constants*/
#define ECFM_FNG_ALARM_TIME_MIN             250
#define ECFM_FNG_ALARM_TIME_MAX             1000
/* MEP FngResetTime Validation constants*/
#define ECFM_FNG_RESET_TIME_MIN             (ECFM_FNG_ALARM_TIME_MIN)
#define ECFM_FNG_RESET_TIME_MAX             (ECFM_FNG_ALARM_TIME_MAX)

/* MEP LtmTTL Validation constants*/
#define ECFM_LTM_TTL_MAX                    255
#define ECFM_LTM_TTL_MIN                    1
/*Constants for VLAN configuration in ECFM*/
#define ECFM_VLANID_MAX                     4094 
#define ECFM_VLANID_MIN                     1
#define ECFM_ISID_MAX                       16777214
#define ECFM_ISID_MIN                       255
/* The number of secondary vlans for a single primary vlan is limited to 32*/
#define ECFM_MAX_SEC_VLAN_PER_PVLAN        32 
/* constants for ECFM internal isid values  this values are for 
 * storing the ISID values into RB trees etc. 
 * */
#define ECFM_INTERNAL_ISID_MAX  (ECFM_VLANID_MAX+ECFM_ISID_MAX)
#define ECFM_INTERNAL_ISID_MIN  (ECFM_VLANID_MAX+1)
#define ECFM_TWO_PORT_BRIDGE       2
#define ECFM_RECEIVED_FROM_PORT             2
/*Constants for Primary VLAN configuration for MEP*/
#define ECFM_MEP_PRIMARY_VLANID_MAX         ECFM_INTERNAL_ISID_MAX  
/* MEP CCM SeqNumber Validation constants*/
#define ECFM_CCM_SEQ_NUM_MAX                (ECFM_UINT4_MAX)
#define ECFM_CCM_SEQ_NUM_MIN                1
/* MEP CCM SeqNumber Validation constants*/
#define ECFM_LTR_SEQ_NUM_MAX                (ECFM_UINT4_MAX)
#define ECFM_LTR_SEQ_NUM_MIN                1
/* MEP LtrRcvOrder Validation constants*/
#define ECFM_LTR_RECV_ORDER_MAX             (ECFM_UINT4_MAX)
#define ECFM_LTR_RECV_ORDER_MIN             1

/*LBM Data TLV Length Validation constants*/
#define ECFM_LBM_DATA_TLV_LEN_MAX           8973
#define ECFM_LBM_DATA_TLV_LEN_MIN           1

#define ECFM_MIP_CCM_DB_SIZE_MIN            1000
#define ECFM_MIP_CCM_DB_SIZE_MAX            10000
#define ECFM_MIP_CCM_DB_HOLD_TIME_MIN       24
#define ECFM_MIP_CCM_DB_HOLD_TIME_MAX       48
/* LTR cache related constants */
#define ECFM_LTR_CACHE_SIZE_MIN             1
#define ECFM_LTR_CACHE_SIZE_MAX             4095
#define ECFM_LTR_CACHE_HOLD_TIME_MIN        1
#define ECFM_LTR_HOLD_TIME_MIN              1
#define ECFM_LTR_HOLD_TIME_MAX              10080
/* LBR cache related constants*/
#define ECFM_LBR_CACHE_SIZE_MIN             1
#define ECFM_LBR_CACHE_SIZE_MAX             4096
#define ECFM_LBR_HOLD_TIME_MIN              1
#define ECFM_LBR_HOLD_TIME_MAX              2880
/* Frame delay buffer related constants*/
#define ECFM_FD_BUFFER_SIZE_MIN             1
#define ECFM_FD_BUFFER_SIZE_MAX             4096
/* LTR Receive Time*/
#define ECFM_LTR_TIMEOUT_MIN                1
#define ECFM_LTR_TIMEOUT_MAX                1000
/* LBM Test/Data Pattern Size */
#define ECFM_LBM_DATA_PATTERN_SIZE_MAX      8970
#define ECFM_LBM_TEST_PATTERN_SIZE_MAX      8969
#define ECFM_LBM_DATA_PATTERN_SIZE_MIN      34
#define ECFM_LBM_TEST_PATTERN_SIZE_MIN      33

/* Loopback Related constants*/
#define ECFM_LBR_TIMEOUT_MIN                1
#define ECFM_LBR_TIMEOUT_MAX                1000
#define ECFM_LBLT_LB_BURST_TYPE             1
#define ECFM_LBLT_LB_REQ_RES_TYPE           2


/* Delay Mesaurement Related constants*/
#define ECFM_DM_DEADLINE_MAX                172800
/* Frame Delay Bufferet Related constants*/
#define ECFM_FD_BUFF_THRESHOLD_MIN          10
#define ECFM_FD_BUFF_THRESHOLD_MAX          ECFM_UINT4_MAX
/*LBM Transmission Interval*/
#define ECFM_LB_INTERVAL_MIN                1
#define ECFM_LB_INTERVAL_MAX                1000
#define ECFM_LB_INTERVAL_SEC_MIN            1
#define ECFM_LB_INTERVAL_SEC_MAX            600
#define ECFM_LB_INTERVAL_IN_USEC_MIN        10
#define ECFM_LB_INTERVAL_IN_USEC_MAX        1000
#define ECFM_LB_DEADLINE_MAX                172800

/* TST Transmission Interval*/
#define ECFM_TST_INTERVAL_MIN                1
#define ECFM_TST_INTERVAL_MAX                1000
#define ECFM_TST_INTERVAL_SEC_MIN            1
#define ECFM_TST_INTERVAL_SEC_MAX            600
#define ECFM_TST_INTERVAL_IN_USEC_MIN        10
#define ECFM_TST_INTERVAL_IN_USEC_MAX        1000

/*Error Log*/
#define ECFM_ERROR_LOG_SIZE_MIN             1
#define ECFM_ERROR_LOG_SIZE_MAX             4096

/* Frame Loss */
#define ECFM_FL_BUFF_THRESHOLD_MIN          10
#define ECFM_FL_BUFF_THRESHOLD_MAX          ECFM_UINT4_MAX
#define ECFM_LMM_RESERVED_SIZE              8 /* Size of the reserved fields
                                               * to be filled in LMM PDU
                                               */
#define ECFM_CC_MAX_FRM_LOSS                ECFM_UINT4_MAX

/*Destination types */
#define ECFM_TX_DEST_TYPE_UNICAST           0
#define ECFM_TX_DEST_TYPE_MEPID             1
#define ECFM_TX_DEST_TYPE_MULTICAST         2
#define ECFM_TX_DEST_TYPE_MIPID             3 /* Currently used for MPLS-TP */
/* Transaction Related Constats*/
#define ECFM_TX_STATUS_READY                0
#define ECFM_TX_STATUS_NOT_READY            1
#define ECFM_TX_STATUS_START                2
#define ECFM_TX_STATUS_STOP                 3

/*Default Values*/
#define ECFM_XCHK_DELAY_DEF_VAL            3.5
#define ECFM_Y1731_LOC_EXIT_MIN            3
#define ECFM_DEF_MD_LEVEL_DEF_VAL          -1
#define ECFM_MIP_CCM_DB_DEF_HOLD_TIME      24
#define ECFM_MIP_CCM_DB_DEF_SIZE           1000
#define ECFM_LTM_TTL_DEF_VAL               64
#define ECFM_CCI_SENT_DEF_VAL              1
#define ECFM_MEP_ARCHIVE_DEF_HOLD_TIME     100
#define ECFM_XCHK_DELAY_MIN_VAL            0
#define ECFM_XCHK_DELAY_MAX_VAL            100
#define ECFM_LTR_CACHE_DEF_SIZE            100
#define ECFM_LTR_CACHE_DEF_HOLD_TIME       100
#define ECFM_LBR_CACHE_DEF_HOLD_TIME       1440
#define ECFM_LBR_CACHE_DEF_SIZE            1024
#define ECFM_FD_BUFFER_DEF_SIZE            1024
#define ECFM_LB_DEF_INTERVAL               1
#define ECFM_ERROR_LOG_DEF_SIZE            1024
#define ECFM_FL_BUFFER_DEF_SIZE            1024
#define ECFM_LM_INTERVAL_DEF_VAL           100
#define ECFM_LB_MESG_DEF_VAL               1
#define Y1731_LB_MESG_DEF_VAL              0
#define ECFM_FD_BUFFER_DEF_THRESHOLD       200
/* LBM Default All-1 pattern */            
#define ECFM_LBM_DEF_DATA_PATTERN          1
#define ECFM_LTR_TIMEOUT_DEF_VAL           500
#define ECFM_LB_INTERVAL_DEF_VAL           10
#define ECFM_LB_BURST_INTERVAL_DEF_VAL     100
#define Y1731_LB_INTERVAL_DEF_VAL          1  /*sec*/
#define Y1731_LB_DEADLINE_DEF_VAL          0  /*infinite*/
#define ECFM_LBR_TIMEOUT_DEF_VAL           5 * ECFM_NUM_OF_TIME_UNITS_IN_A_SEC  
#define ECFM_DM_INTERVAL_DEF_VAL           100
#define ECFM_LBM_SEQ_NUM_INIT_VAL          0
#define ECFM_DMM_SEQ_NUM_INIT_VAL          0
#define ECFM_LTM_SEQ_NUM_INIT_VAL          0
#define ECFM_TST_DEADLINE_DEF_VAL          0
#define ECFM_TST_INTERVAL_DEF_VAL          1
#define ECFM_TST_SEQ_NUM_INIT_VAL          0
#define ECFM_TST_MESG_DEF_VAL              0
#define ECFM_CC_LCK_DEFAULT_DELAY          1
#define ECFM_DEFAULT_VLAN_PRIORITY         7
#define ECFM_TH_DEADLINE_DEF_VAL           0  /*infinite*/
#define ECFM_TH_MESG_DEF_VAL               0
#define ECFM_TH_PPS_DEF_VAL                100
/*****************************************************************************/
/* RDI related  */
#define ECFM_CC_RDI_PERIOD_MIN          10
#define ECFM_CC_RDI_PERIOD_MAX          172800
/*****************************************************************************/
/* AIS related  */
#define ECFM_CC_AIS_PERIOD_MIN          10
#define ECFM_CC_AIS_PERIOD_MAX          172800
/*****************************************************************************/
/* LCK related  */
#define ECFM_CC_LCK_PERIOD_MIN          10
#define ECFM_CC_LCK_PERIOD_MAX          172800
#define ECFM_CC_LCK_DELAY_MIN           1
#define ECFM_CC_LCK_DELAY_MAX           10
/*****************************************************************************/
/* Frame Loss buffer related constants */
#define ECFM_FL_BUFFER_SIZE_MIN             1
#define ECFM_FL_BUFFER_SIZE_MAX             4096
/*****************************************************************************/
/* Frame Loss Mesaurement Related constants */
#define ECFM_LM_DEADLINE_MAX                172800
#define ECFM_LM_MESSAGE_MIN                 0 
#define ECFM_LM_MESSAGE_MAX                 8192 
/*****************************************************************************/
/* Availability Mesaurement Related constants */
#define ECFM_AVLBLTY_DEADLINE_MAX           409968000   /* Max Possible year 13 */
#define ECFM_AVLBLTY_WINDOW_MAX             0xF45C2700U /* Max Value of Integer 
                                                           Wich can be mapped to 
                                                           rounded up to 13 year 
                                                           deadline */
#define ECFM_AVLBLTY_DEADLINE_DEFAULT       3600 /* One Hour */
#define ECFM_AVLBLTY_INTERVAL_DEFAULT       ECFM_CC_AVLBLTY_INTERVAL_1_MIN
#define ECFM_AVLBLTY_WINDOW_DEFAULT         5
#define ECFM_AVLBLTY_RATIO_LENGTH           10 
/*****************************************************************************/
/* CC ERROR LOG related  */
#define ECFM_CC_ERROR_LOG_SIZE_MIN          1024
#define ECFM_CC_ERROR_LOG_SIZE_MAX          4096
/*****************************************************************************/
/*Various Possible ROW STATUS*/
#define ECFM_ROW_STATUS_ACTIVE              ACTIVE
#define ECFM_ROW_STATUS_NOT_IN_SERVICE      NOT_IN_SERVICE
#define ECFM_ROW_STATUS_NOT_READY           NOT_READY
#define ECFM_ROW_STATUS_CREATE_AND_GO       CREATE_AND_GO
#define ECFM_ROW_STATUS_CREATE_AND_WAIT     CREATE_AND_WAIT
#define ECFM_ROW_STATUS_DESTROY             DESTROY
/*****************************************************************************/
/* General Table manipulation macros */
#define ECFM_ADD                            1
#define ECFM_DEL                            2
/*****************************************************************************/

/* System Specific Constants */
#define ECFM_RCVD_ORDER_INIT_VAL        1   /* First PDU received value */ 
#define ECFM_DEF_VLAN_PRIORITY          7   /* Default priority to be filled in 
                                             * the VLAN Header
                                             */
#define ECFM_RESERVE_VALUE              0   /* Reserve value */
#define ECFM_INVALID_VALUE              -1  /* Invalid value */
#define ECFM_NULL_CHAR                 '\0' /* NULL character */
#define ECFM_INIT_VAL                   0   /* Initialization value */
#define ECFM_INCR_VAL                   1   /* Increment value */
#define ECFM_DECR_VAL                   1   /* Decrement value */
#define ECFM_Y1731_DEF_RX_COUNT         3   /* Default count value for receiving the CCM's in Y1731*/
#define ECFM_DEFAULT_CONTEXT            L2IWF_DEFAULT_CONTEXT
#define ECFM_MPLS_DUMMY_PORTS_PER_CONTEXT  1
/* One additional port is added per context for MPLSTP OAM and
 * it is reserved to be the last port in the context
 */

/* This macro is added for pseudo wire visibility to ECFM.
 * Where this is specific to ECFM module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_PSW_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_PSW_IFACES.
 */
#define ECFM_MAX_L2_PW_IFACES       SYS_DEF_MAX_L2_PSW_IFACES

/* This macro is added for Attachment Circuit interface in ECFM.
 * Where this is specific to ECFM module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_AC_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_AC_IFACES.
 */
#define ECFM_MAX_L2_AC_IFACES       SYS_DEF_MAX_L2_AC_IFACES

#define ECFM_MAX_PORTS_PER_CONTEXT      (L2IWF_MAX_PORTS_PER_CONTEXT + \
                                         ECFM_MAX_L2_PW_IFACES + \
                                         ECFM_MAX_L2_AC_IFACES + \
                                         ECFM_MPLS_DUMMY_PORTS_PER_CONTEXT)
#define LBLT_MAX_CONTEXTS               (FsECFMSizingParams[MAX_ECFM_LBLT_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits)
#define ECFM_MAX_CONTEXTS               (FsECFMSizingParams[MAX_ECFM_CC_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits)
#define ECFM_INVALID_CONTEXT            (ECFM_MAX_CONTEXTS +1)
#define ECFM_PROT_BPDU                  CFA_PROT_BPDU
#define ECFM_ENCAP_NONE                 CFA_ENCAP_NONE
#define ECFM_NUM_OF_TIME_UNITS_IN_A_SEC SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT (1000/ECFM_NUM_OF_TIME_UNITS_IN_A_SEC)
#define ECFM_NUM_OF_NSEC_IN_A_USEC       1000
#define ECFM_NUM_OF_USEC_IN_A_MSEC       1000
#define ECFM_NUM_OF_MSEC_IN_A_SEC        1000
#define ECFM_NUM_OF_USEC_IN_A_SEC        1000000
#define ECFM_NUM_OF_NSEC_IN_A_SEC        1000000000
#define ECFM_NUM_OF_SEC_IN_A_MIN         60
#define ECFM_NUM_OF_MIN_IN_A_HOUR        60
#define ECFM_NUM_OF_TICKS_IN_A_MSEC      10
#define ECFM_PORT_LIST_SIZE              CONTEXT_PORT_LIST_SIZE_EXT
#define ECFM_SIGINT                      2 /*SIGINT*/
#define ECFM_VLAN_TAG_OFFSET             12
#define ECFM_VLAN_TYPE_OR_LEN_SIZE       2
#define ECFM_VLAN_TAGGED_HEADER_SIZE     16
#define ECFM_VLAN_TAG_PRIORITY_SHIFT 13
#define ECFM_VLAN_ID_MASK                VLAN_ID_MASK
#define ECFM_PORTID_SUB_IF_ALIAS         LLDP_PORT_ID_SUB_IF_ALIAS
#define ECFM_PORTID_SUB_IF_LOCAL         LLDP_PORT_ID_SUB_LOCAL
#define ECFM_MPLSTP_PORTID_SUB_IF_ALIAS         0 
#define ECFM_CHASSISID_SUB_MAC_ADDR      LLDP_CHASS_ID_SUB_MAC_ADDR
#define ECFM_MAX_PORTID_LEN              LLDP_MAX_LEN_PORTID
#define ECFM_MAX_CHASSISID_LEN           LLDP_MAX_LEN_CHASSISID
#define ECFM_MAX_MAN_ADDR_LEN            31
#define ECFM_MAX_TRANSPORT_DOMAIN        16
#define ECFM_MAX_TRANSPORT_DOMAIN_LEN    9
#define ECFM_L2_MEMBER_PORT              L2_MEMBER_PORT

/* Bit map to store the ids of vlans supported by VLAN module */
#define ECFM_VLAN_LIST_SIZE              VLAN_LIST_SIZE_EXT
/*****************************************************************************/
/* Total Mep defects */
#define ECFM_NUM_OF_MEP_DEFECTS          11
/*****************************************************************************/
/* CLI Constants */
#define MDLEVEL_LIST_SIZE                 8
#define ECFM_CLI_MAX_MAC_STRING_SIZE      21
#define ECFM_CLI_MAX_SESSIONS             CLI_MAX_SESSIONS
#define ECFM_CLI_INVALID_CONTEXT          0xFFFFFFFF
#define ECFM_SWITCH_ALIAS_LEN             L2IWF_CONTEXT_ALIAS_LEN 

/*****************************************************************************/
/* Trace type  definitions */
#define  ECFM_TRC_BUF_SIZE                500
#define ECFM_MIN_TRC_VAL       ECFM_INIT_SHUT_TRC
#define ECFM_DEF_TRC_OPT       ECFM_CRITICAL_TRC
#define ECFM_MAX_TRC_VAL       ECFM_ALL_TRC
#define ECFM_CLR_TRC_OPT       0x0000

/*****************************************************************************/
/* Scalabilty and performance related constants */
#ifdef MI_WANTED
#define ECFM_MAX_RMEP_IN_MA             4
#else
#define ECFM_MAX_RMEP_IN_MA             2
#endif
/*constants for ECFM isid/vlan values  */
#define ECFM_MAX_ISID                   (SYS_DEF_MAX_VIP_IFACES)
#define ECFM_MAX_DELAY_QUEUE            128
#define ECFM_MAX_CONFIG_ERR_PER_PORT    2
#define ECFM_MAX_STACK                  (ECFM_MAX_MEP +\
                                         (ECFM_MAX_MIP*2))/* Maximum number 
                                                           * of MHFs and MEPs
                                                           * per bridge
                                                           */
#define ECFM_MAX_MA_MEP_LIST               ((ECFM_MAX_MA * \
                                             ECFM_MAX_RMEP_IN_MA) \
                                         +ECFM_MAX_MEP)/* Maximum number of local and 
                                                        * remote MEP per bridge
                                                        */
#define ECFM_MAX_MA_MEP_LIST_IN_SYSTEM     ((ECFM_MAX_MA_IN_SYSTEM * \
                                             ECFM_MAX_RMEP_IN_MA) \
                                             + ECFM_MAX_MEP_IN_SYSTEM)
/* NOTE: only 2 local MEPs can be
 * created per VID for a MA.
 */ 
#define ECFM_MAX_RMEP_DB                   (ECFM_MAX_MA_IN_SYSTEM * \
                                            ECFM_MAX_RMEP_IN_MA)
#define ECFM_MAX_CONFIG_ERR_LIST           (ECFM_MAX_PORTS_IN_SYSTEM * \
                                            ECFM_MAX_CONFIG_ERR_PER_PORT)
#define ECFM_MAX_LTR_CACHE_INDEX                1

#define ECFM_TRUE                       TRUE
#define ECFM_FALSE                      FALSE
#define ECFM_MAX_LTM_TX_PER_MEP         10 
#define ECFM_MAX_LTM                    (ECFM_MAX_LTM_TX_PER_MEP * ECFM_MAX_MEP)
#define ECFM_MAX_LBM_TX_PER_MEP         10
#define ECFM_MAX_LBM                    (ECFM_MAX_LBM_TX_PER_MEP * ECFM_MAX_MEP)
#define ECFM_REPLY_INGRESS_VALUE_SIZE   7
#define ECFM_REPLY_EGRESS_VALUE_SIZE    7
#define ECFM_TRAP_OID_LEN               15                                      

#define ECFM_OBJ_NAME_LEN               257

#define ECFM_VLAN_TAG_PRIORITY_DEI_SHIFT    12
#define ECFM_VLAN_CUSTOMER_PROTOCOL_ID      0x8100
#define ECFM_VLAN_PROVIDER_PROTOCOL_ID      0x88a8

#define ECFM_CUSTOMER_BRIDGE_MODE      1
#define ECFM_PROVIDER_BRIDGE_MODE      2
#define ECFM_PROVIDER_EDGE_BRIDGE_MODE 3
#define ECFM_PROVIDER_CORE_BRIDGE_MODE 4
#define ECFM_PBB_ICOMPONENT_BRIDGE_MODE 5
#define ECFM_PBB_BCOMPONENT_BRIDGE_MODE 6
#define ECFM_INVALID_BRIDGE_MODE       7

#define ECFM_CLI_MAC_STRING_LENGTH            18   
/* Configuration errors */
#define ECFM_CONFIG_ERR_CFM_LEAK                0
#define ECFM_CONFIG_ERR_CONFLICT_VIDS           1
#define ECFM_CONFIG_ERR_EXCESS_LEVELS           2
#define ECFM_CONFIG_ERR_OVERLAPPED_LEVELS       3


/*  High Availability related */
#define ECFM_NODE_IDLE                    RM_INIT
#define ECFM_NODE_ACTIVE                  RM_ACTIVE
#define ECFM_NODE_STANDBY                 RM_STANDBY
#define ECFM_RED_MD_ROW_STS_CMD            ECFM_MAX_EVENT + 1
#define ECFM_RED_MA_ROW_STS_CMD            ECFM_MAX_EVENT + 2
#define ECFM_RED_MEP_ROW_STS_CMD           ECFM_MAX_EVENT + 3
#define ECFM_RED_SHUTDOWN_CMD              ECFM_MAX_EVENT + 4
#define ECFM_RED_MOD_STS_CHNG_CMD          ECFM_MAX_EVENT + 5
#define ECFM_RED_PORT_LLC_CMD              ECFM_MAX_EVENT + 6
#define ECFM_RED_PORT_MOD_STS_CMD          ECFM_MAX_EVENT + 7
#define ECFM_RED_GLOBAL_OFF_CMD            ECFM_MAX_EVENT + 8
#define ECFM_RED_Y1731_MOD_STS_CMD         ECFM_MAX_EVENT + 9
#define ECFM_RED_OUI_CHANGE_CMD            ECFM_MAX_EVENT + 10
#define ECFM_RED_LB_NPAPI                  ECFM_MAX_EVENT + 11
#define ECFM_RED_TST_NPAPI                 ECFM_MAX_EVENT + 12

#define ECFM_PROVIDER_NETWORK_PORT        CFA_PROVIDER_NETWORK_PORT
#define ECFM_CNP_CTAGGED_PORT             CFA_CNP_CTAGGED_PORT
#define ECFM_CNP_PORTBASED_PORT           CFA_CNP_PORTBASED_PORT
#define ECFM_CNP_STAGGED_PORT             CFA_CNP_STAGGED_PORT
#define ECFM_CUSTOMER_EDGE_PORT           CFA_CUSTOMER_EDGE_PORT
#define ECFM_PROP_CUSTOMER_EDGE_PORT      CFA_PROP_CUSTOMER_EDGE_PORT
#define ECFM_PROP_CUSTOMER_NETWORK_PORT   CFA_PROP_CUSTOMER_NETWORK_PORT
#define ECFM_PROP_PROVIDER_NETWORK_PORT   CFA_PROP_PROVIDER_NETWORK_PORT
#define ECFM_CUSTOMER_BRIDGE_PORT         CFA_CUSTOMER_BRIDGE_PORT
#define ECFM_PROVIDER_EDGE_PORT           CFA_PROVIDER_EDGE_PORT
#define ECFM_PROVIDER_INSTANCE_PORT       CFA_PROVIDER_INSTANCE_PORT
#define ECFM_VIRTUAL_INSTANCE_PORT        CFA_VIRTUAL_INSTANCE_PORT
#define ECFM_CUSTOMER_BACKBONE_PORT       CFA_CUSTOMER_BACKBONE_PORT
#define ECFM_INVALID_PROVIDER_PORT        CFA_INVALID_BRIDGE_PORT
/* Filter Actions */
#define ECFM_COPY_TO_CPU             3
#define ECFM_SWITCH                  2
#define ECFM_DROP                    1
/* Array indexes */
#define ECFM_INDEX_ZERO        0
#define ECFM_INDEX_ONE         1
#define ECFM_INDEX_TWO         2
#define ECFM_INDEX_THREE       3
#define ECFM_INDEX_FOUR        4
#define ECFM_INDEX_FIVE        5
#define ECFM_INDEX_SIX         6
#define ECFM_INDEX_SEVEN       7
#define ECFM_INDEX_EIGHT       8
#define ECFM_INDEX_NINE        9
#define ECFM_INDEX_TEN         10
#define ECFM_INDEX_ELEVEN      11
#define ECFM_INDEX_TWELVE      12
#define ECFM_INDEX_THIRTEEN    13
#define ECFM_INDEX_FOURTEEN    14
#define ECFM_INDEX_FIFTEEN     15
#define ECFM_INDEX_SIXTEEN     16
#define ECFM_INDEX_SEVENTEEN   17
#define ECFM_INDEX_EIGHTEEN    18
#define ECFM_INDEX_NINETEEN    19
#define ECFM_INDEX_TWENTY      20
#define ECFM_INDEX_32          32

/* Incremental MSR related */
#define ECFM_TABLE_INDICES_COUNT_ZERO          0
#define ECFM_TABLE_INDICES_COUNT_ONE           1
#define ECFM_TABLE_INDICES_COUNT_TWO           2
#define ECFM_TABLE_INDICES_COUNT_THREE         3
#define ECFM_TABLE_INDICES_COUNT_FOUR          4
#define ECFM_TABLE_INDICES_COUNT_FIVE          5
/*Macros*/
#define ECFM_MSB_FLAG                                 0x80
#define ECFM_NUM_OF_BITS_IN_CRC                       32
#define ECFM_OPERATE_WITH_VAL_TWO                     2
#define ECFM_ARRAY_SIZE_256                           256
#define ECFM_ARRAY_SIZE_257                           257
#define ECFM_ARRAY_SIZE_32                            32
#define ECFM_ARRAY_SIZE_64                            64
#define ECFM_ARRAY_SIZE_5                             5
#define ECFM_ARRAY_SIZE_2                             2
#define ECFM_ARRAY_SIZE_8                             8
#define ECFM_VAL_0                                    0
#define ECFM_VAL_1                                    1
#define ECFM_VAL_2                                    2
#define ECFM_VAL_3                                    3
#define ECFM_VAL_4                                    4
#define ECFM_VAL_5                                    5
#define ECFM_VAL_6                                    6
#define ECFM_VAL_7                                    7
#define ECFM_VAL_8                                    8
#define ECFM_VAL_10                                   10
#define ECFM_VAL_12                                   12
#define ECFM_VAL_14                                   14
#define ECFM_VAL_15                                   15
#define ECFM_VAL_16                                   16
#define ECFM_VAL_18                                   18
#define ECFM_VAL_21                                   21
#define ECFM_VAL_26                                   26
#define ECFM_VAL_27                                   27
#define ECFM_VAL_28                                   28
#define ECFM_VAL_30                                   30
#define ECFM_VAL_31                                   31
#define ECFM_VAL_32                                   32
#define ECFM_VAL_33                                   33
#define ECFM_VAL_34                                   34
#define ECFM_VAL_40                                   40
#define ECFM_VAL_48                                   48
#define ECFM_VAL_50                                   50
#define ECFM_VAL_58                                   58
#define ECFM_VAL_64                                   64
#define ECFM_FLT_VAL_100                              100.0
#define ECFM_VAL_127                                  127
#define ECFM_4BIT_MAX                                 0xf
#define ECFM_SHIFT_1BIT                               1
#define ECFM_SHIFT_3BITS                              3
#define ECFM_SHIFT_4BITS                              4
#define ECFM_SHIFT_5BITS                              5
#define ECFM_SHIFT_7BITS                              7
#define ECFM_SHIFT_8BITS                              8
#define ECFM_SHIFT_16BITS                             16
#define ECFM_SHIFT_24BITS                             24
#define ECFM_MASK_WITH_VAL_128                        0x80
#define ECFM_MASK_WITH_VAL_4026531840                 0xf0000000
#define ECFM_MASK_WITH_VAL_266338304                  0x0fe00000
#define ECFM_MASK_WITH_VAL_2043904                    0x001f3000
#define ECFM_MASK_WITH_VAL_16256                      0x00003f80
#define ECFM_MASK_WITH_VAL_127                        0x0000007f
#define ECFM_MASK_WITH_VAL_255                        0x00ff
#define ECFM_MASK_WITH_VAL_4095                       0x0fff
#define ECFM_CC_RDI_BIT                               7
#define ECFM_MEP_DEFECTS_LENGTH                       2
#define ECFM_DEF_MAC_ADDR                             0xff
#define ECFM_CLI_MAX_LINE_LENGTH                      80
#define ECFM_100_PERCENT                              100
#define ECFM_BITS_IN_ONE_BYTE                         8

#define ECFM_TX_UNIQUE_HANDLE_VAL                     54
#define ECFM_TX_HANDLE_NAME_LENGTH                    56
#define ECFM_COMPARE_WITH_MAC_FIVE_VALUE              5
#define ECFM_MAC_ADD_STRING_LENGTH                    17
#define ECFM_MAC_ADD_STRING_AND_OCTET_MIN_LENGTH      19 
                                                     /* xx:xx:xx:xx:xx:xx:y */
#define ECFM_MAC_ADD_STRING_AND_OCTET_MAX_LENGTH      23 
                                                 /*xx:xx:xx:xx:xx:xx:656535 */

/* For Handling Multiple Line Cards Insertion/Removal in System */
#define ECFM_MBSM_LC_STATUS                           0x00

/* TRAPS OID */
#define Y1731_TRAPS_OID       "1.3.6.1.4.1.29601.2.7"
#define ECFM_STD_TRAPS_OID    "1.3.111.2.802.1.1.8"
#define ECFM_PROP_TRAPS_OID   "1.3.6.1.4.1.2076.160"
#define ECFM_FS_TRAPS_OID     "1.3.6.1.4.1.2076.150.4"

#define ECFM_FAULT_TRAP_NOTIFICATION                  1
#define ECFM_DEFECT_TRAP_NOTIFICATION                 2
#define ECFM_SI_TRAP_NOTIFICATION                     1

/* MPLS-TP LBM/LBR related MACROS */
#define ECFM_TLV_SUB_TYPE_SIZE                        1
#define ECFM_TLV_NODE_ID_SIZE                         4
#define ECFM_TLV_IFNUM_SIZE                           4
/* Target MEP/MIP ID TLV related MACROS */
#define ECFM_TARGET_MPID_TLV_TYPE                     0x21
#define ECFM_TARGET_MPID_TLV_LENGTH                   25
#define ECFM_TARGET_MEPID_TLV_SUB_TYPE                0x02
#define ECFM_TARGET_MIPID_TLV_SUB_TYPE                0x03
/* Replying MEP ID TLV related MACROS */
#define ECFM_REPLY_MPID_TLV_TYPE                      0x22
#define ECFM_REPLY_MPID_TLV_LENGTH                    25
#define ECFM_REPLY_MEPID_TLV_SUB_TYPE                 0x02
#define ECFM_REPLY_MIPID_TLV_SUB_TYPE                 0x03
#define ECFM_REPLY_MIP_ID_FIELD_SIZE         ECFM_TLV_SUB_TYPE_SIZE + \
                                             ECFM_CARRIER_CODE_SIZE + \
                                             ECFM_TLV_NODE_ID_SIZE + \
                                             ECFM_TLV_IFNUM_SIZE 
/* Requesting MEP ID TLV related MACROS */
#define ECFM_REQ_MEPID_TLV_TYPE                       0x23
#define ECFM_REQ_MEPID_TLV_LENGTH                     53
#define ECFM_REQ_MEPID_TLV_LBM_INDICATION             0x00
#define ECFM_REQ_MEPID_TLV_LBR_INDICATION             0x01
#define ECFM_REQ_MEPID_TLV_LB_IND_SIZE                1
#define ECFM_REQ_MEPID_TLV_RSRVD_VALUE                0x0000
#define ECFM_REQ_MEPID_TLV_RESV_FIELD_SIZE            2
/* Selector Type either LSP or PW */
#define ECFM_IS_SELECTOR_TYPE_MPLS_TP(u1SelectorType)\
            ((u1SelectorType == ECFM_SERVICE_SELECTION_LSP) || \
             (u1SelectorType == ECFM_SERVICE_SELECTION_PW))

#define ECFM_MPTP_CC_TTL_DEF                          255
/* LBM Data TTL Validation constants */
#define ECFM_LBM_TTL_MIN                              1
#define ECFM_LBM_TTL_DEF                              255
#define ECFM_LBM_TTL_MAX                              255

#define  ECFM_MAX_MGMT_DOMAIN_LEN      255
#define  ECFM_MAX_ORG_TLV_VALUE_LEN    1500 /* As per the MIB */
#define  ECFM_MAX_EGRESS_PORT_ID_LEN   255 
#define  ECFM_MAX_INGRESS_PORT_ID_LEN  255

#define ECFM_CC_MEP_ERR_CCM_SIZE       256
#define ECFM_CC_MEP_XCON_CCM_SIZE       256

#define ECFM_MAX_CC_MEP_ERRORS             (ECFM_MAX_PORTS_IN_SYSTEM * \
                                            ECFM_MAX_CONFIG_ERR_PER_PORT) 
#define ECFM_MAX_CC_MEP_XCONS              (ECFM_MAX_PORTS_IN_SYSTEM * \
                                            ECFM_MAX_CONFIG_ERR_PER_PORT)
#define ECFM_LBR_RCVD_INFO_SIZE        (sizeof(tEcfmLbrRcvdInfo))
#define ECFM_MAX_LBR_RCVD_INFO          50

#endif /*_CFM_DEF_H*/
/******************************************************************************
  End of File cfmdef.h
 *****************************************************************************/
