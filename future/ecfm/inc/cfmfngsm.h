/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmfngsm.h,v 1.4 2008/11/05 04:26:40 premap-iss Exp $
 * 
 * Description: This file contains the functionality of the
 *              FNG state machine.
 *********************************************************************/
#ifndef __CFMFNGSM_H_
#define __CFMFNGSM_H_

/* routines that handles the occurence of the events */

/* FNG State machine */
PRIVATE VOID
EcfmFngSmSetStateReset PROTO((tEcfmCcPduSmInfo * ));

PRIVATE VOID
EcfmFngSmSetStateDefect PROTO((tEcfmCcPduSmInfo * ));

PRIVATE VOID
EcfmFngSmSetStateRptDft PROTO ((tEcfmCcPduSmInfo * ));

PRIVATE VOID
EcfmFngSmSetStateRptDftFrmRptd PROTO ((tEcfmCcPduSmInfo *));

PRIVATE VOID
EcfmFngSmSetStateDftRptdFrmClr PROTO((tEcfmCcPduSmInfo * ));

PRIVATE VOID
EcfmFngSmSetStateDftClearing PROTO((tEcfmCcPduSmInfo * ));

PRIVATE VOID
EcfmFngSmEvtImpossible PROTO((tEcfmCcPduSmInfo * ));
/* Enums for the rotuines to be retrieve from the the matrix*/
enum
{
   FNG0,               /* EcfmFngSmSetStateReset */
   FNG1,               /* EcfmFngSmSetStateDefect */
   FNG2,               /* EcfmFngSmSetStateRptDft */
   FNG3,               /* EcfmFngSmSetStateRptDftFrmRptd */
   FNG4,               /* EcfmFngSmSetStateDftRptdFrmClr */
   FNG5,               /* EcfmFngSmSetStateDftClearing */
   FNG6,               /* EcfmFngSmEvtImpossible */
   ECFM_MEP_FNG_MAX_FN_PTRS
};

/* function pointers of  MEP FNG State machine */
VOID (*gaEcfmFngActionProc[ECFM_MEP_FNG_MAX_FN_PTRS]) (tEcfmCcPduSmInfo *) =
{
   EcfmFngSmSetStateReset,
   EcfmFngSmSetStateDefect,
   EcfmFngSmSetStateRptDft,
   EcfmFngSmSetStateRptDftFrmRptd,
   EcfmFngSmSetStateDftRptdFrmClr,
   EcfmFngSmSetStateDftClearing,
   EcfmFngSmEvtImpossible
};

/* State Event Table of FNG RX */
const UINT1
gau1EcfmFngSem[ECFM_SM_FNG_MAX_EVENTS][ECFM_MEP_FNG_MAX_STATES] =
/*   RESET   DFCT  DFCTRPRT DFCTCLR     States */
{                                            /* Events */
   { FNG0,  FNG6,  FNG6,    FNG6},/*ECFM_SM_EV_BEGIN */
   { FNG0,  FNG0,  FNG0,    FNG0},/*ECFM_SM_EV_MEP_NOT_ACTIVE */
   { FNG1,  FNG6,  FNG3,    FNG4},/*ECFM_SM_EV_FNG_MA_MEP_DEFECT */
   { FNG6,  FNG0,  FNG5,    FNG6},/*ECFM_SM_EV_FNG_NO_MA_MEP_DEFECT */
   { FNG6,  FNG2,  FNG6,    FNG6},/*ECFM_SM_EV_FNG_TIMEOUT */
   { FNG6,  FNG6,  FNG6,    FNG0},/*ECFM_SM_EV_FNG_RESET_TIMEOUT */
};
#endif /* _CFMFNGSM_H_ */


/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmfngsm.h                        */
/*-----------------------------------------------------------------------*/


