/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: cfmv2db.h,v 1.2 2011/12/27 11:53:40 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _CFMV2DB_H
#define _CFMV2DB_H

UINT1 Dot1agCfmStackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1agCfmVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1agCfmDefaultMdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1agCfmConfigErrorListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1agCfmMdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1agCfmMaNetTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1agCfmMaCompTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1agCfmMaMepListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1agCfmMepTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1agCfmLtrTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1agCfmMepDbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 cfmv2 [] ={1,3,111,2,802,1,1,8};
tSNMP_OID_TYPE cfmv2OID = {8, cfmv2};


/* Generated OID's for tables */
UINT4 Dot1agCfmStackTable [] ={1,3,111,2,802,1,1,8,1,1,1};
tSNMP_OID_TYPE Dot1agCfmStackTableOID = {11, Dot1agCfmStackTable};


UINT4 Dot1agCfmVlanTable [] ={1,3,111,2,802,1,1,8,1,3,1};
tSNMP_OID_TYPE Dot1agCfmVlanTableOID = {11, Dot1agCfmVlanTable};


UINT4 Dot1agCfmDefaultMdTable [] ={1,3,111,2,802,1,1,8,1,2,4};
tSNMP_OID_TYPE Dot1agCfmDefaultMdTableOID = {11, Dot1agCfmDefaultMdTable};


UINT4 Dot1agCfmConfigErrorListTable [] ={1,3,111,2,802,1,1,8,1,4,1};
tSNMP_OID_TYPE Dot1agCfmConfigErrorListTableOID = {11, Dot1agCfmConfigErrorListTable};


UINT4 Dot1agCfmMdTable [] ={1,3,111,2,802,1,1,8,1,5,2};
tSNMP_OID_TYPE Dot1agCfmMdTableOID = {11, Dot1agCfmMdTable};


UINT4 Dot1agCfmMaNetTable [] ={1,3,111,2,802,1,1,8,1,6,1};
tSNMP_OID_TYPE Dot1agCfmMaNetTableOID = {11, Dot1agCfmMaNetTable};


UINT4 Dot1agCfmMaCompTable [] ={1,3,111,2,802,1,1,8,1,6,2};
tSNMP_OID_TYPE Dot1agCfmMaCompTableOID = {11, Dot1agCfmMaCompTable};


UINT4 Dot1agCfmMaMepListTable [] ={1,3,111,2,802,1,1,8,1,6,3};
tSNMP_OID_TYPE Dot1agCfmMaMepListTableOID = {11, Dot1agCfmMaMepListTable};


UINT4 Dot1agCfmMepTable [] ={1,3,111,2,802,1,1,8,1,7,1};
tSNMP_OID_TYPE Dot1agCfmMepTableOID = {11, Dot1agCfmMepTable};


UINT4 Dot1agCfmLtrTable [] ={1,3,111,2,802,1,1,8,1,7,2};
tSNMP_OID_TYPE Dot1agCfmLtrTableOID = {11, Dot1agCfmLtrTable};


UINT4 Dot1agCfmMepDbTable [] ={1,3,111,2,802,1,1,8,1,7,3};
tSNMP_OID_TYPE Dot1agCfmMepDbTableOID = {11, Dot1agCfmMepDbTable};




UINT4 Dot1agCfmStackifIndex [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,1};
UINT4 Dot1agCfmStackVlanIdOrNone [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,2};
UINT4 Dot1agCfmStackMdLevel [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,3};
UINT4 Dot1agCfmStackDirection [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,4};
UINT4 Dot1agCfmStackMdIndex [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,5};
UINT4 Dot1agCfmStackMaIndex [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,6};
UINT4 Dot1agCfmStackMepId [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,7};
UINT4 Dot1agCfmStackMacAddress [ ] ={1,3,111,2,802,1,1,8,1,1,1,1,8};
UINT4 Dot1agCfmVlanComponentId [ ] ={1,3,111,2,802,1,1,8,1,3,1,1,1};
UINT4 Dot1agCfmVlanVid [ ] ={1,3,111,2,802,1,1,8,1,3,1,1,2};
UINT4 Dot1agCfmVlanPrimaryVid [ ] ={1,3,111,2,802,1,1,8,1,3,1,1,3};
UINT4 Dot1agCfmVlanRowStatus [ ] ={1,3,111,2,802,1,1,8,1,3,1,1,4};
UINT4 Dot1agCfmDefaultMdDefLevel [ ] ={1,3,111,2,802,1,1,8,1,2,1};
UINT4 Dot1agCfmDefaultMdDefMhfCreation [ ] ={1,3,111,2,802,1,1,8,1,2,2};
UINT4 Dot1agCfmDefaultMdDefIdPermission [ ] ={1,3,111,2,802,1,1,8,1,2,3};
UINT4 Dot1agCfmDefaultMdComponentId [ ] ={1,3,111,2,802,1,1,8,1,2,4,1,1};
UINT4 Dot1agCfmDefaultMdPrimaryVid [ ] ={1,3,111,2,802,1,1,8,1,2,4,1,2};
UINT4 Dot1agCfmDefaultMdStatus [ ] ={1,3,111,2,802,1,1,8,1,2,4,1,3};
UINT4 Dot1agCfmDefaultMdLevel [ ] ={1,3,111,2,802,1,1,8,1,2,4,1,4};
UINT4 Dot1agCfmDefaultMdMhfCreation [ ] ={1,3,111,2,802,1,1,8,1,2,4,1,5};
UINT4 Dot1agCfmDefaultMdIdPermission [ ] ={1,3,111,2,802,1,1,8,1,2,4,1,6};
UINT4 Dot1agCfmConfigErrorListVid [ ] ={1,3,111,2,802,1,1,8,1,4,1,1,1};
UINT4 Dot1agCfmConfigErrorListIfIndex [ ] ={1,3,111,2,802,1,1,8,1,4,1,1,2};
UINT4 Dot1agCfmConfigErrorListErrorType [ ] ={1,3,111,2,802,1,1,8,1,4,1,1,3};
UINT4 Dot1agCfmMdTableNextIndex [ ] ={1,3,111,2,802,1,1,8,1,5,1};
UINT4 Dot1agCfmMdIndex [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,1};
UINT4 Dot1agCfmMdFormat [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,2};
UINT4 Dot1agCfmMdName [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,3};
UINT4 Dot1agCfmMdMdLevel [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,4};
UINT4 Dot1agCfmMdMhfCreation [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,5};
UINT4 Dot1agCfmMdMhfIdPermission [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,6};
UINT4 Dot1agCfmMdMaNextIndex [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,7};
UINT4 Dot1agCfmMdRowStatus [ ] ={1,3,111,2,802,1,1,8,1,5,2,1,8};
UINT4 Dot1agCfmMaIndex [ ] ={1,3,111,2,802,1,1,8,1,6,1,1,1};
UINT4 Dot1agCfmMaNetFormat [ ] ={1,3,111,2,802,1,1,8,1,6,1,1,2};
UINT4 Dot1agCfmMaNetName [ ] ={1,3,111,2,802,1,1,8,1,6,1,1,3};
UINT4 Dot1agCfmMaNetCcmInterval [ ] ={1,3,111,2,802,1,1,8,1,6,1,1,4};
UINT4 Dot1agCfmMaNetRowStatus [ ] ={1,3,111,2,802,1,1,8,1,6,1,1,5};
UINT4 Dot1agCfmMaComponentId [ ] ={1,3,111,2,802,1,1,8,1,6,2,1,1};
UINT4 Dot1agCfmMaCompPrimaryVlanId [ ] ={1,3,111,2,802,1,1,8,1,6,2,1,2};
UINT4 Dot1agCfmMaCompMhfCreation [ ] ={1,3,111,2,802,1,1,8,1,6,2,1,3};
UINT4 Dot1agCfmMaCompIdPermission [ ] ={1,3,111,2,802,1,1,8,1,6,2,1,4};
UINT4 Dot1agCfmMaCompNumberOfVids [ ] ={1,3,111,2,802,1,1,8,1,6,2,1,5};
UINT4 Dot1agCfmMaCompRowStatus [ ] ={1,3,111,2,802,1,1,8,1,6,2,1,6};
UINT4 Dot1agCfmMaMepListIdentifier [ ] ={1,3,111,2,802,1,1,8,1,6,3,1,1};
UINT4 Dot1agCfmMaMepListRowStatus [ ] ={1,3,111,2,802,1,1,8,1,6,3,1,2};
UINT4 Dot1agCfmMepIdentifier [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,1};
UINT4 Dot1agCfmMepIfIndex [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,2};
UINT4 Dot1agCfmMepDirection [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,3};
UINT4 Dot1agCfmMepPrimaryVid [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,4};
UINT4 Dot1agCfmMepActive [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,5};
UINT4 Dot1agCfmMepFngState [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,6};
UINT4 Dot1agCfmMepCciEnabled [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,7};
UINT4 Dot1agCfmMepCcmLtmPriority [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,8};
UINT4 Dot1agCfmMepMacAddress [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,9};
UINT4 Dot1agCfmMepLowPrDef [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,10};
UINT4 Dot1agCfmMepFngAlarmTime [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,11};
UINT4 Dot1agCfmMepFngResetTime [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,12};
UINT4 Dot1agCfmMepHighestPrDefect [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,13};
UINT4 Dot1agCfmMepDefects [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,14};
UINT4 Dot1agCfmMepErrorCcmLastFailure [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,15};
UINT4 Dot1agCfmMepXconCcmLastFailure [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,16};
UINT4 Dot1agCfmMepCcmSequenceErrors [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,17};
UINT4 Dot1agCfmMepCciSentCcms [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,18};
UINT4 Dot1agCfmMepNextLbmTransId [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,19};
UINT4 Dot1agCfmMepLbrIn [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,20};
UINT4 Dot1agCfmMepLbrInOutOfOrder [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,21};
UINT4 Dot1agCfmMepLbrBadMsdu [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,22};
UINT4 Dot1agCfmMepLtmNextSeqNumber [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,23};
UINT4 Dot1agCfmMepUnexpLtrIn [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,24};
UINT4 Dot1agCfmMepLbrOut [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,25};
UINT4 Dot1agCfmMepTransmitLbmStatus [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,26};
UINT4 Dot1agCfmMepTransmitLbmDestMacAddress [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,27};
UINT4 Dot1agCfmMepTransmitLbmDestMepId [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,28};
UINT4 Dot1agCfmMepTransmitLbmDestIsMepId [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,29};
UINT4 Dot1agCfmMepTransmitLbmMessages [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,30};
UINT4 Dot1agCfmMepTransmitLbmDataTlv [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,31};
UINT4 Dot1agCfmMepTransmitLbmVlanPriority [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,32};
UINT4 Dot1agCfmMepTransmitLbmVlanDropEnable [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,33};
UINT4 Dot1agCfmMepTransmitLbmResultOK [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,34};
UINT4 Dot1agCfmMepTransmitLbmSeqNumber [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,35};
UINT4 Dot1agCfmMepTransmitLtmStatus [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,36};
UINT4 Dot1agCfmMepTransmitLtmFlags [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,37};
UINT4 Dot1agCfmMepTransmitLtmTargetMacAddress [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,38};
UINT4 Dot1agCfmMepTransmitLtmTargetMepId [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,39};
UINT4 Dot1agCfmMepTransmitLtmTargetIsMepId [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,40};
UINT4 Dot1agCfmMepTransmitLtmTtl [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,41};
UINT4 Dot1agCfmMepTransmitLtmResult [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,42};
UINT4 Dot1agCfmMepTransmitLtmSeqNumber [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,43};
UINT4 Dot1agCfmMepTransmitLtmEgressIdentifier [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,44};
UINT4 Dot1agCfmMepRowStatus [ ] ={1,3,111,2,802,1,1,8,1,7,1,1,45};
UINT4 Dot1agCfmLtrSeqNumber [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,1};
UINT4 Dot1agCfmLtrReceiveOrder [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,2};
UINT4 Dot1agCfmLtrTtl [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,3};
UINT4 Dot1agCfmLtrForwarded [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,4};
UINT4 Dot1agCfmLtrTerminalMep [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,5};
UINT4 Dot1agCfmLtrLastEgressIdentifier [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,6};
UINT4 Dot1agCfmLtrNextEgressIdentifier [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,7};
UINT4 Dot1agCfmLtrRelay [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,8};
UINT4 Dot1agCfmLtrChassisIdSubtype [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,9};
UINT4 Dot1agCfmLtrChassisId [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,10};
UINT4 Dot1agCfmLtrManAddressDomain [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,11};
UINT4 Dot1agCfmLtrManAddress [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,12};
UINT4 Dot1agCfmLtrIngress [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,13};
UINT4 Dot1agCfmLtrIngressMac [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,14};
UINT4 Dot1agCfmLtrIngressPortIdSubtype [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,15};
UINT4 Dot1agCfmLtrIngressPortId [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,16};
UINT4 Dot1agCfmLtrEgress [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,17};
UINT4 Dot1agCfmLtrEgressMac [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,18};
UINT4 Dot1agCfmLtrEgressPortIdSubtype [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,19};
UINT4 Dot1agCfmLtrEgressPortId [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,20};
UINT4 Dot1agCfmLtrOrganizationSpecificTlv [ ] ={1,3,111,2,802,1,1,8,1,7,2,1,21};
UINT4 Dot1agCfmMepDbRMepIdentifier [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,1};
UINT4 Dot1agCfmMepDbRMepState [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,2};
UINT4 Dot1agCfmMepDbRMepFailedOkTime [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,3};
UINT4 Dot1agCfmMepDbMacAddress [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,4};
UINT4 Dot1agCfmMepDbRdi [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,5};
UINT4 Dot1agCfmMepDbPortStatusTlv [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,6};
UINT4 Dot1agCfmMepDbInterfaceStatusTlv [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,7};
UINT4 Dot1agCfmMepDbChassisIdSubtype [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,8};
UINT4 Dot1agCfmMepDbChassisId [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,9};
UINT4 Dot1agCfmMepDbManAddressDomain [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,10};
UINT4 Dot1agCfmMepDbManAddress [ ] ={1,3,111,2,802,1,1,8,1,7,3,1,11};


tSNMP_OID_TYPE Dot1agCfmDefaultMdDefLevelOID = {11, Dot1agCfmDefaultMdDefLevel};


tSNMP_OID_TYPE Dot1agCfmDefaultMdDefMhfCreationOID = {11, Dot1agCfmDefaultMdDefMhfCreation};


tSNMP_OID_TYPE Dot1agCfmDefaultMdDefIdPermissionOID = {11, Dot1agCfmDefaultMdDefIdPermission};


tSNMP_OID_TYPE Dot1agCfmMdTableNextIndexOID = {11, Dot1agCfmMdTableNextIndex};




tMbDbEntry Dot1agCfmStackTableMibEntry[]= {

{{13,Dot1agCfmStackifIndex}, GetNextIndexDot1agCfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackVlanIdOrNone}, GetNextIndexDot1agCfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackMdLevel}, GetNextIndexDot1agCfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackDirection}, GetNextIndexDot1agCfmStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackMdIndex}, GetNextIndexDot1agCfmStackTable, Dot1agCfmStackMdIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackMaIndex}, GetNextIndexDot1agCfmStackTable, Dot1agCfmStackMaIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackMepId}, GetNextIndexDot1agCfmStackTable, Dot1agCfmStackMepIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},

{{13,Dot1agCfmStackMacAddress}, GetNextIndexDot1agCfmStackTable, Dot1agCfmStackMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1agCfmStackTableINDEX, 4, 1, 0, NULL},
};
tMibData Dot1agCfmStackTableEntry = { 8, Dot1agCfmStackTableMibEntry };

tMbDbEntry Dot1agCfmDefaultMdDefLevelMibEntry[]= {

{{11,Dot1agCfmDefaultMdDefLevel}, NULL, Dot1agCfmDefaultMdDefLevelGet, Dot1agCfmDefaultMdDefLevelSet, Dot1agCfmDefaultMdDefLevelTest, Dot1agCfmDefaultMdDefLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData Dot1agCfmDefaultMdDefLevelEntry = { 1, Dot1agCfmDefaultMdDefLevelMibEntry };

tMbDbEntry Dot1agCfmDefaultMdDefMhfCreationMibEntry[]= {

{{11,Dot1agCfmDefaultMdDefMhfCreation}, NULL, Dot1agCfmDefaultMdDefMhfCreationGet, Dot1agCfmDefaultMdDefMhfCreationSet, Dot1agCfmDefaultMdDefMhfCreationTest, Dot1agCfmDefaultMdDefMhfCreationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData Dot1agCfmDefaultMdDefMhfCreationEntry = { 1, Dot1agCfmDefaultMdDefMhfCreationMibEntry };

tMbDbEntry Dot1agCfmDefaultMdDefIdPermissionMibEntry[]= {

{{11,Dot1agCfmDefaultMdDefIdPermission}, NULL, Dot1agCfmDefaultMdDefIdPermissionGet, Dot1agCfmDefaultMdDefIdPermissionSet, Dot1agCfmDefaultMdDefIdPermissionTest, Dot1agCfmDefaultMdDefIdPermissionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData Dot1agCfmDefaultMdDefIdPermissionEntry = { 1, Dot1agCfmDefaultMdDefIdPermissionMibEntry };

tMbDbEntry Dot1agCfmDefaultMdTableMibEntry[]= {

{{13,Dot1agCfmDefaultMdComponentId}, GetNextIndexDot1agCfmDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmDefaultMdTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmDefaultMdPrimaryVid}, GetNextIndexDot1agCfmDefaultMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1agCfmDefaultMdTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmDefaultMdStatus}, GetNextIndexDot1agCfmDefaultMdTable, Dot1agCfmDefaultMdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmDefaultMdTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmDefaultMdLevel}, GetNextIndexDot1agCfmDefaultMdTable, Dot1agCfmDefaultMdLevelGet, Dot1agCfmDefaultMdLevelSet, Dot1agCfmDefaultMdLevelTest, Dot1agCfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmDefaultMdTableINDEX, 2, 1, 0, "-1"},

{{13,Dot1agCfmDefaultMdMhfCreation}, GetNextIndexDot1agCfmDefaultMdTable, Dot1agCfmDefaultMdMhfCreationGet, Dot1agCfmDefaultMdMhfCreationSet, Dot1agCfmDefaultMdMhfCreationTest, Dot1agCfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmDefaultMdTableINDEX, 2, 1, 0, "4"},

{{13,Dot1agCfmDefaultMdIdPermission}, GetNextIndexDot1agCfmDefaultMdTable, Dot1agCfmDefaultMdIdPermissionGet, Dot1agCfmDefaultMdIdPermissionSet, Dot1agCfmDefaultMdIdPermissionTest, Dot1agCfmDefaultMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmDefaultMdTableINDEX, 2, 1, 0, "5"},
};
tMibData Dot1agCfmDefaultMdTableEntry = { 6, Dot1agCfmDefaultMdTableMibEntry };

tMbDbEntry Dot1agCfmVlanTableMibEntry[]= {

{{13,Dot1agCfmVlanComponentId}, GetNextIndexDot1agCfmVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmVlanTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmVlanVid}, GetNextIndexDot1agCfmVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1agCfmVlanTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmVlanPrimaryVid}, GetNextIndexDot1agCfmVlanTable, Dot1agCfmVlanPrimaryVidGet, Dot1agCfmVlanPrimaryVidSet, Dot1agCfmVlanPrimaryVidTest, Dot1agCfmVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmVlanTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmVlanRowStatus}, GetNextIndexDot1agCfmVlanTable, Dot1agCfmVlanRowStatusGet, Dot1agCfmVlanRowStatusSet, Dot1agCfmVlanRowStatusTest, Dot1agCfmVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmVlanTableINDEX, 2, 1, 1, NULL},
};
tMibData Dot1agCfmVlanTableEntry = { 4, Dot1agCfmVlanTableMibEntry };

tMbDbEntry Dot1agCfmConfigErrorListTableMibEntry[]= {

{{13,Dot1agCfmConfigErrorListVid}, GetNextIndexDot1agCfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1agCfmConfigErrorListTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmConfigErrorListIfIndex}, GetNextIndexDot1agCfmConfigErrorListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1agCfmConfigErrorListTableINDEX, 2, 1, 0, NULL},

{{13,Dot1agCfmConfigErrorListErrorType}, GetNextIndexDot1agCfmConfigErrorListTable, Dot1agCfmConfigErrorListErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmConfigErrorListTableINDEX, 2, 1, 0, NULL},
};
tMibData Dot1agCfmConfigErrorListTableEntry = { 3, Dot1agCfmConfigErrorListTableMibEntry };

tMbDbEntry Dot1agCfmMdTableNextIndexMibEntry[]= {

{{11,Dot1agCfmMdTableNextIndex}, NULL, Dot1agCfmMdTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1agCfmMdTableNextIndexEntry = { 1, Dot1agCfmMdTableNextIndexMibEntry };

tMbDbEntry Dot1agCfmMdTableMibEntry[]= {

{{13,Dot1agCfmMdIndex}, GetNextIndexDot1agCfmMdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmMdTableINDEX, 1, 0, 0, NULL},

{{13,Dot1agCfmMdFormat}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdFormatGet, Dot1agCfmMdFormatSet, Dot1agCfmMdFormatTest, Dot1agCfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMdTableINDEX, 1, 0, 0, "4"},

{{13,Dot1agCfmMdName}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdNameGet, Dot1agCfmMdNameSet, Dot1agCfmMdNameTest, Dot1agCfmMdTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1agCfmMdTableINDEX, 1, 0, 0, "DEFAULT"},

{{13,Dot1agCfmMdMdLevel}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdMdLevelGet, Dot1agCfmMdMdLevelSet, Dot1agCfmMdMdLevelTest, Dot1agCfmMdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1agCfmMdTableINDEX, 1, 0, 0, "0"},

{{13,Dot1agCfmMdMhfCreation}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdMhfCreationGet, Dot1agCfmMdMhfCreationSet, Dot1agCfmMdMhfCreationTest, Dot1agCfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMdTableINDEX, 1, 0, 0, "1"},

{{13,Dot1agCfmMdMhfIdPermission}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdMhfIdPermissionGet, Dot1agCfmMdMhfIdPermissionSet, Dot1agCfmMdMhfIdPermissionTest, Dot1agCfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMdTableINDEX, 1, 0, 0, "1"},

{{13,Dot1agCfmMdMaNextIndex}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdMaNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmMdTableINDEX, 1, 0, 0, NULL},

{{13,Dot1agCfmMdRowStatus}, GetNextIndexDot1agCfmMdTable, Dot1agCfmMdRowStatusGet, Dot1agCfmMdRowStatusSet, Dot1agCfmMdRowStatusTest, Dot1agCfmMdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMdTableINDEX, 1, 0, 1, NULL},
};
tMibData Dot1agCfmMdTableEntry = { 8, Dot1agCfmMdTableMibEntry };

tMbDbEntry Dot1agCfmMaNetTableMibEntry[]= {

{{13,Dot1agCfmMaIndex}, GetNextIndexDot1agCfmMaNetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmMaNetTableINDEX, 2, 0, 0, NULL},

{{13,Dot1agCfmMaNetFormat}, GetNextIndexDot1agCfmMaNetTable, Dot1agCfmMaNetFormatGet, Dot1agCfmMaNetFormatSet, Dot1agCfmMaNetFormatTest, Dot1agCfmMaNetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaNetTableINDEX, 2, 0, 0, NULL},

{{13,Dot1agCfmMaNetName}, GetNextIndexDot1agCfmMaNetTable, Dot1agCfmMaNetNameGet, Dot1agCfmMaNetNameSet, Dot1agCfmMaNetNameTest, Dot1agCfmMaNetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1agCfmMaNetTableINDEX, 2, 0, 0, NULL},

{{13,Dot1agCfmMaNetCcmInterval}, GetNextIndexDot1agCfmMaNetTable, Dot1agCfmMaNetCcmIntervalGet, Dot1agCfmMaNetCcmIntervalSet, Dot1agCfmMaNetCcmIntervalTest, Dot1agCfmMaNetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaNetTableINDEX, 2, 0, 0, "4"},

{{13,Dot1agCfmMaNetRowStatus}, GetNextIndexDot1agCfmMaNetTable, Dot1agCfmMaNetRowStatusGet, Dot1agCfmMaNetRowStatusSet, Dot1agCfmMaNetRowStatusTest, Dot1agCfmMaNetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaNetTableINDEX, 2, 0, 1, NULL},
};
tMibData Dot1agCfmMaNetTableEntry = { 5, Dot1agCfmMaNetTableMibEntry };

tMbDbEntry Dot1agCfmMaCompTableMibEntry[]= {

{{13,Dot1agCfmMaComponentId}, GetNextIndexDot1agCfmMaCompTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmMaCompTableINDEX, 3, 1, 0, NULL},

{{13,Dot1agCfmMaCompPrimaryVlanId}, GetNextIndexDot1agCfmMaCompTable, Dot1agCfmMaCompPrimaryVlanIdGet, Dot1agCfmMaCompPrimaryVlanIdSet, Dot1agCfmMaCompPrimaryVlanIdTest, Dot1agCfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1agCfmMaCompTableINDEX, 3, 1, 0, NULL},

{{13,Dot1agCfmMaCompMhfCreation}, GetNextIndexDot1agCfmMaCompTable, Dot1agCfmMaCompMhfCreationGet, Dot1agCfmMaCompMhfCreationSet, Dot1agCfmMaCompMhfCreationTest, Dot1agCfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaCompTableINDEX, 3, 1, 0, "4"},

{{13,Dot1agCfmMaCompIdPermission}, GetNextIndexDot1agCfmMaCompTable, Dot1agCfmMaCompIdPermissionGet, Dot1agCfmMaCompIdPermissionSet, Dot1agCfmMaCompIdPermissionTest, Dot1agCfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaCompTableINDEX, 3, 1, 0, "5"},

{{13,Dot1agCfmMaCompNumberOfVids}, GetNextIndexDot1agCfmMaCompTable, Dot1agCfmMaCompNumberOfVidsGet, Dot1agCfmMaCompNumberOfVidsSet, Dot1agCfmMaCompNumberOfVidsTest, Dot1agCfmMaCompTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1agCfmMaCompTableINDEX, 3, 1, 0, NULL},

{{13,Dot1agCfmMaCompRowStatus}, GetNextIndexDot1agCfmMaCompTable, Dot1agCfmMaCompRowStatusGet, Dot1agCfmMaCompRowStatusSet, Dot1agCfmMaCompRowStatusTest, Dot1agCfmMaCompTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaCompTableINDEX, 3, 1, 1, NULL},
};
tMibData Dot1agCfmMaCompTableEntry = { 6, Dot1agCfmMaCompTableMibEntry };

tMbDbEntry Dot1agCfmMaMepListTableMibEntry[]= {

{{13,Dot1agCfmMaMepListIdentifier}, GetNextIndexDot1agCfmMaMepListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmMaMepListTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMaMepListRowStatus}, GetNextIndexDot1agCfmMaMepListTable, Dot1agCfmMaMepListRowStatusGet, Dot1agCfmMaMepListRowStatusSet, Dot1agCfmMaMepListRowStatusTest, Dot1agCfmMaMepListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMaMepListTableINDEX, 3, 0, 1, NULL},
};
tMibData Dot1agCfmMaMepListTableEntry = { 2, Dot1agCfmMaMepListTableMibEntry };

tMbDbEntry Dot1agCfmMepTableMibEntry[]= {

{{13,Dot1agCfmMepIdentifier}, GetNextIndexDot1agCfmMepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepIfIndex}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepIfIndexGet, Dot1agCfmMepIfIndexSet, Dot1agCfmMepIfIndexTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepDirection}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepDirectionGet, Dot1agCfmMepDirectionSet, Dot1agCfmMepDirectionTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepPrimaryVid}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepPrimaryVidGet, Dot1agCfmMepPrimaryVidSet, Dot1agCfmMepPrimaryVidTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "0"},

{{13,Dot1agCfmMepActive}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepActiveGet, Dot1agCfmMepActiveSet, Dot1agCfmMepActiveTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "2"},

{{13,Dot1agCfmMepFngState}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepFngStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, "1"},

{{13,Dot1agCfmMepCciEnabled}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepCciEnabledGet, Dot1agCfmMepCciEnabledSet, Dot1agCfmMepCciEnabledTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "2"},

{{13,Dot1agCfmMepCcmLtmPriority}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepCcmLtmPriorityGet, Dot1agCfmMepCcmLtmPrioritySet, Dot1agCfmMepCcmLtmPriorityTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepMacAddress}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepLowPrDef}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepLowPrDefGet, Dot1agCfmMepLowPrDefSet, Dot1agCfmMepLowPrDefTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "2"},

{{13,Dot1agCfmMepFngAlarmTime}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepFngAlarmTimeGet, Dot1agCfmMepFngAlarmTimeSet, Dot1agCfmMepFngAlarmTimeTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "250"},

{{13,Dot1agCfmMepFngResetTime}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepFngResetTimeGet, Dot1agCfmMepFngResetTimeSet, Dot1agCfmMepFngResetTimeTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "1000"},

{{13,Dot1agCfmMepHighestPrDefect}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepHighestPrDefectGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepDefects}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepDefectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepErrorCcmLastFailure}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepErrorCcmLastFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepXconCcmLastFailure}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepXconCcmLastFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepCcmSequenceErrors}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepCcmSequenceErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepCciSentCcms}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepCciSentCcmsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepNextLbmTransId}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepNextLbmTransIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepLbrIn}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepLbrInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepLbrInOutOfOrder}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepLbrInOutOfOrderGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepLbrBadMsdu}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepLbrBadMsduGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepLtmNextSeqNumber}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepLtmNextSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepUnexpLtrIn}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepUnexpLtrInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepLbrOut}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepLbrOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLbmStatus}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmStatusGet, Dot1agCfmMepTransmitLbmStatusSet, Dot1agCfmMepTransmitLbmStatusTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "2"},

{{13,Dot1agCfmMepTransmitLbmDestMacAddress}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmDestMacAddressGet, Dot1agCfmMepTransmitLbmDestMacAddressSet, Dot1agCfmMepTransmitLbmDestMacAddressTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLbmDestMepId}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmDestMepIdGet, Dot1agCfmMepTransmitLbmDestMepIdSet, Dot1agCfmMepTransmitLbmDestMepIdTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLbmDestIsMepId}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmDestIsMepIdGet, Dot1agCfmMepTransmitLbmDestIsMepIdSet, Dot1agCfmMepTransmitLbmDestIsMepIdTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLbmMessages}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmMessagesGet, Dot1agCfmMepTransmitLbmMessagesSet, Dot1agCfmMepTransmitLbmMessagesTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "1"},

{{13,Dot1agCfmMepTransmitLbmDataTlv}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmDataTlvGet, Dot1agCfmMepTransmitLbmDataTlvSet, Dot1agCfmMepTransmitLbmDataTlvTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLbmVlanPriority}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmVlanPriorityGet, Dot1agCfmMepTransmitLbmVlanPrioritySet, Dot1agCfmMepTransmitLbmVlanPriorityTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLbmVlanDropEnable}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmVlanDropEnableGet, Dot1agCfmMepTransmitLbmVlanDropEnableSet, Dot1agCfmMepTransmitLbmVlanDropEnableTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "1"},

{{13,Dot1agCfmMepTransmitLbmResultOK}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, "1"},

{{13,Dot1agCfmMepTransmitLbmSeqNumber}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLbmSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLtmStatus}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmStatusGet, Dot1agCfmMepTransmitLtmStatusSet, Dot1agCfmMepTransmitLtmStatusTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "1"},

{{13,Dot1agCfmMepTransmitLtmFlags}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmFlagsGet, Dot1agCfmMepTransmitLtmFlagsSet, Dot1agCfmMepTransmitLtmFlagsTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLtmTargetMacAddress}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmTargetMacAddressGet, Dot1agCfmMepTransmitLtmTargetMacAddressSet, Dot1agCfmMepTransmitLtmTargetMacAddressTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLtmTargetMepId}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmTargetMepIdGet, Dot1agCfmMepTransmitLtmTargetMepIdSet, Dot1agCfmMepTransmitLtmTargetMepIdTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLtmTargetIsMepId}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmTargetIsMepIdGet, Dot1agCfmMepTransmitLtmTargetIsMepIdSet, Dot1agCfmMepTransmitLtmTargetIsMepIdTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLtmTtl}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmTtlGet, Dot1agCfmMepTransmitLtmTtlSet, Dot1agCfmMepTransmitLtmTtlTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, "64"},

{{13,Dot1agCfmMepTransmitLtmResult}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, "1"},

{{13,Dot1agCfmMepTransmitLtmSeqNumber}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepTransmitLtmEgressIdentifier}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepTransmitLtmEgressIdentifierGet, Dot1agCfmMepTransmitLtmEgressIdentifierSet, Dot1agCfmMepTransmitLtmEgressIdentifierTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 0, NULL},

{{13,Dot1agCfmMepRowStatus}, GetNextIndexDot1agCfmMepTable, Dot1agCfmMepRowStatusGet, Dot1agCfmMepRowStatusSet, Dot1agCfmMepRowStatusTest, Dot1agCfmMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1agCfmMepTableINDEX, 3, 0, 1, NULL},
};
tMibData Dot1agCfmMepTableEntry = { 45, Dot1agCfmMepTableMibEntry };

tMbDbEntry Dot1agCfmLtrTableMibEntry[]= {

{{13,Dot1agCfmLtrSeqNumber}, GetNextIndexDot1agCfmLtrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrReceiveOrder}, GetNextIndexDot1agCfmLtrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrTtl}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrTtlGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrForwarded}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrForwardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrTerminalMep}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrTerminalMepGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrLastEgressIdentifier}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrLastEgressIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrNextEgressIdentifier}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrNextEgressIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrRelay}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrRelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrChassisIdSubtype}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrChassisId}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrManAddressDomain}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrManAddressDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrManAddress}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrManAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrIngress}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrIngressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrIngressMac}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrIngressMacGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrIngressPortIdSubtype}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrIngressPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrIngressPortId}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrIngressPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrEgress}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrEgressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrEgressMac}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrEgressMacGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrEgressPortIdSubtype}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrEgressPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrEgressPortId}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrEgressPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},

{{13,Dot1agCfmLtrOrganizationSpecificTlv}, GetNextIndexDot1agCfmLtrTable, Dot1agCfmLtrOrganizationSpecificTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmLtrTableINDEX, 5, 0, 0, NULL},
};
tMibData Dot1agCfmLtrTableEntry = { 21, Dot1agCfmLtrTableMibEntry };

tMbDbEntry Dot1agCfmMepDbTableMibEntry[]= {

{{13,Dot1agCfmMepDbRMepIdentifier}, GetNextIndexDot1agCfmMepDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbRMepState}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbRMepStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbRMepFailedOkTime}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbRMepFailedOkTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbMacAddress}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbRdi}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbRdiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbPortStatusTlv}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbPortStatusTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, "0"},

{{13,Dot1agCfmMepDbInterfaceStatusTlv}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbInterfaceStatusTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, "0"},

{{13,Dot1agCfmMepDbChassisIdSubtype}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbChassisId}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbManAddressDomain}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbManAddressDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},

{{13,Dot1agCfmMepDbManAddress}, GetNextIndexDot1agCfmMepDbTable, Dot1agCfmMepDbManAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1agCfmMepDbTableINDEX, 4, 0, 0, NULL},
};
tMibData Dot1agCfmMepDbTableEntry = { 11, Dot1agCfmMepDbTableMibEntry };

#endif /* _CFMV2DB_H */

