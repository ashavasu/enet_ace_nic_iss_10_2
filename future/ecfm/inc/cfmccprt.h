/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmccprt.h,v 1.53 2014/11/14 12:16:56 siva Exp $
 * 
 * Description: This file contains the prototypes for all the 
 *              PUBLIC procedures.
 *********************************************************************/

#ifndef _CFM_CC_PRT_H
#define _CFM_CC_PRT_H

PUBLIC INT4 EcfmHandleCcAndLbLtStartModule PROTO ((VOID));
PUBLIC VOID EcfmCcPktQueueHandler PROTO ((tEcfmCcMsg *));
PUBLIC VOID EcfmCcCfgQueueHandler PROTO ((tEcfmCcMsg *));
PUBLIC VOID EcfmCcInterruptQueueHandler PROTO ((tEcfmCcMsg *));
PUBLIC VOID EcfmCcTmrExpHandler PROTO((VOID));

PUBLIC INT4 EcfmCcCfgQueMsg PROTO ((tEcfmCcMsg * ));
PUBLIC INT4 EcfmCcInterruptQueMsg PROTO ((tEcfmCcMsg * ));
PUBLIC INT4 EcfmCcIfHandleCreatePort PROTO ((UINT4, UINT4, UINT2));

PUBLIC INT4 EcfmCreateCcMplsPort PROTO ((UINT4, UINT4, UINT2));
PUBLIC VOID EcfmCcIfHandleDeletePort PROTO ((UINT2));
PUBLIC INT4 EcfmCcIfHandlePortOperChg PROTO ((UINT2, UINT1));
PUBLIC INT4 EcfmHandlePortStateChg PROTO ((UINT2, UINT4, UINT1));
PUBLIC INT4 EcfmHandlePbPortStateChg PROTO ((UINT4, UINT4, UINT1));
PUBLIC VOID EcfmCcIfCreateAllPorts PROTO ((VOID));
PUBLIC VOID EcfmDeleteRMepEntry PROTO ((tEcfmCcMepInfo *, tEcfmCcRMepDbInfo *));

/* Timer routine*/
PUBLIC INT4 EcfmCcTmrStartTimer PROTO ((UINT1, tEcfmCcPduSmInfo *,UINT4));
PUBLIC INT4 EcfmCcTmrStopTimer PROTO ((UINT1, tEcfmCcPduSmInfo * ));
PUBLIC VOID EcfmCcHandleMepHldTmrExpiry PROTO ((tEcfmCcMdInfo *));

PUBLIC VOID EcfmCcDeleteAllPortsInfo PROTO((VOID));

/* entry point to the ECFM module*/
PUBLIC INT4 EcfmCcModuleStart PROTO ((VOID));
PUBLIC VOID EcfmCcModuleShutDown PROTO ((VOID));
PUBLIC VOID EcfmCcModuleEnable PROTO ((VOID));
PUBLIC VOID EcfmCcModuleDisable PROTO ((VOID));
PUBLIC VOID EcfmCcY1731Enable PROTO ((VOID));
PUBLIC VOID EcfmCcY1731Disable PROTO ((VOID));
PUBLIC VOID EcfmCcModuleEnableForAPort PROTO((UINT2));
PUBLIC VOID EcfmCcModuleDisableForAPort PROTO((UINT2));
PUBLIC VOID EcfmCcY1731EnableForAPort PROTO ((UINT2 ));
PUBLIC VOID EcfmCcY1731DisableForAPort PROTO ((UINT2 ));


PUBLIC INT4 EcfmCcInitGlobalInfo PROTO ((VOID));
PUBLIC VOID EcfmCcDeInitGlobalInfo PROTO ((VOID));
PUBLIC VOID EcfmCcDeInitMempool PROTO ((VOID));
PUBLIC VOID EcfmCcProcessQueue PROTO ((UINT4 , BOOL1 ));

/* CCM state machine */
PUBLIC INT4 EcfmCcClntCciSm PROTO ((tEcfmCcPduSmInfo * , UINT1 ));
PUBLIC INT4 EcfmCcClntProcessEqCcm PROTO ((tEcfmCcPduSmInfo * ));
PUBLIC INT4 EcfmCcClntProcessLowCcm PROTO ((tEcfmCcPduSmInfo *));
PUBLIC INT4 EcfmCcClntMhfProcessCcm PROTO ((tEcfmCcPduSmInfo *));
PUBLIC VOID EcfmCcClntNotifyRmep PROTO((tEcfmCcPduSmInfo * , UINT1 ));
PUBLIC INT4 EcfmCcClntParseCcm PROTO((tEcfmCcPduSmInfo *,UINT1* ));
PUBLIC VOID EcfmCcClntFngSm PROTO((tEcfmCcPduSmInfo * , UINT1 ));
PUBLIC INT4 EcfmCcClntRmepSm PROTO((tEcfmCcPduSmInfo * , UINT1 ));
PUBLIC INT4 EcfmCcClntRmepErrSm PROTO((tEcfmCcPduSmInfo * , UINT1 ));
PUBLIC INT4 EcfmCcClntXconSm PROTO((tEcfmCcPduSmInfo * , UINT1 ));
PUBLIC VOID EcfmCcFngNoMaDefectIndication PROTO ((tEcfmCcPduSmInfo * ));
PUBLIC VOID EcfmCcFngMaDefectIndication PROTO ((tEcfmCcPduSmInfo *,UINT1));
PUBLIC VOID EcfmCcFngCalulateRemoteDefects PROTO ((tEcfmCcPduSmInfo *, BOOL1));

/* cfmccutils.c */
PUBLIC  tEcfmCcStackInfo* EcfmCcUtilGetMp PROTO ((UINT2, UINT1,
                                                  UINT4, UINT1 ));
PUBLIC tEcfmCcRMepDbInfo* EcfmUtilGetRmepInfo PROTO ((tEcfmCcMepInfo *,UINT2));
PUBLIC VOID EcfmCcUtilNotifySm PROTO ((tEcfmCcMepInfo * , UINT1 ));
PUBLIC VOID EcfmCcUtilNotifyY1731 PROTO ((tEcfmCcMepInfo * , UINT1 ));
PUBLIC INT4 EcfmCcUtilFreeEntryFn PROTO ((tRBElem * , UINT4 ));
PUBLIC tEcfmCcMepInfo * EcfmCcUtilGetMepEntryFrmGlob PROTO ((UINT4 , UINT4 , UINT2 ));
PUBLIC tEcfmCcMepInfo * EcfmCcUtilGetMepEntryFrmPort PROTO ((UINT1, UINT4, UINT2,UINT1));
PUBLIC tEcfmCcMipInfo * EcfmCcUtilGetMipEntry PROTO ((UINT2 , UINT1 , UINT4));
PUBLIC tEcfmCcMipPreventInfo *
EcfmCcUtilGetMipPreventEntry PROTO ((UINT2, UINT1, UINT4));
PUBLIC INT4
EcfmCcUtilAddMipPreventEntry PROTO ((tEcfmCcMipPreventInfo * ));
PUBLIC VOID
EcfmCcUtilDelMipPreventEntry PROTO ((tEcfmCcMipPreventInfo *));
PUBLIC INT4 EcfmCcADUtilChkPortFiltering PROTO ((UINT2, UINT4));
PUBLIC tEcfmCcMaInfo * EcfmCcUtilGetMaAssocWithVid PROTO ((UINT4 , UINT1));
PUBLIC INT4 EcfmCcUtilAddMipEntry PROTO (( tEcfmCcMipInfo *, tEcfmCcMaInfo *));
PUBLIC VOID EcfmCcUtilDeleteMipEntry PROTO ((tEcfmCcMipInfo *));
PUBLIC VOID EcfmCcUtilEvaluateAndCreateMip PROTO ((INT2, INT4, BOOL1));
PUBLIC VOID EcfmDeleteImplicitlyCreatedMips PROTO ((INT2, INT4));
PUBLIC INT4 EcfmCcUtilGetHighestMpMdLevel PROTO ((UINT2 , UINT4 , UINT1 *, BOOL1 *));
PUBLIC INT4 EcfmCcUtilAddConfigErrEntry PROTO ((UINT2 , UINT4 , UINT1));
PUBLIC BOOL1 EcfmCcUtilChkIfCfgCfmLeakErr PROTO ((tEcfmCcMaInfo *, UINT2 , UINT2
));
PUBLIC VOID EcfmCcUtilClearCfmLeakErr PROTO ((tEcfmCcMaInfo *, UINT2 , UINT2));
PUBLIC BOOL1 EcfmCcUtilChkIfCfgConfictVidErr PROTO ((tEcfmCcMaInfo *, UINT2 , UINT2));
PUBLIC VOID EcfmCcUtilDeleteConfigErrEntry PROTO ((UINT2 , UINT4, UINT1));
PUBLIC BOOL1 EcfmIsMepAssocWithMa PROTO ((UINT4, UINT4, INT2, INT1));
PUBLIC INT4  EcfmCcGetLocDefectFromRmep PROTO((tEcfmCcMepInfo *, UINT2));
PUBLIC tEcfmBufChainHeader * EcfmCcUtilDupCruBuf PROTO ((tEcfmBufChainHeader *));
/* Routine used to forward the CFM-PDU to the port */
PUBLIC INT4 EcfmCtrlTxFwdToPort PROTO ((tEcfmBufChainHeader *, UINT4));
/* cfmcctx.c*/
PUBLIC INT4 EcfmCcADCtrlTxFwdToPort PROTO ((tEcfmBufChainHeader *, UINT2, 
                                          tEcfmVlanTag*));
PUBLIC INT4 EcfmCcADCtrlTxFwdToFf PROTO ((tEcfmCcPduSmInfo *));

PUBLIC INT4 EcfmCcCtrlTxTransmitPkt PROTO ((tEcfmBufChainHeader *, UINT2, 
                                            UINT4, UINT1, UINT1, UINT1, UINT1, 
                                            tEcfmVlanTag *, tEcfmPbbTag *));
/* Receiver routine in cfmccrx.c file */
/* Receiver routine receives PDU  from the Lower layers or Frame Filtering*/
PUBLIC INT4 EcfmCcCtrlRxPkt PROTO ((tEcfmBufChainHeader *, UINT4, UINT2));
/* Free CRU Buffer memory */
PUBLIC VOID EcfmCcCtrlRxPktFree PROTO ((tEcfmBufChainHeader *));
PUBLIC INT4 EcfmCcCtrlRxLevelDeMux PROTO ((tEcfmCcPduSmInfo *));

PUBLIC INT4 EcfmOffloadHandleOUIChange(VOID);
PUBLIC INT4 EcfmHandleDeleteVlan (UINT4 u4ContextId, UINT2 u2VlanId);
/* MI*/
PUBLIC INT4 EcfmCcHandleCreateContext PROTO ((UINT4));
PUBLIC INT4 EcfmCcHandleDeleteContext PROTO ((UINT4));
PUBLIC INT4 EcfmCcSelectContext PROTO ((UINT4));
PUBLIC INT4 EcfmCcReleaseContext PROTO ((VOID));
PUBLIC INT4 EcfmCcGetFirstActiveContext PROTO ((UINT4 *));
PUBLIC INT4 EcfmCcGetNextActiveContext PROTO ((UINT4 , UINT4 *));
PUBLIC BOOL1 EcfmCcIsContextExist PROTO ((UINT4 ));
#ifdef NPAPI_WANTED
PUBLIC INT4  EcfmInitWithMBSMandRM PROTO ((UINT4));
#endif
/* MIP */
PUBLIC tEcfmCcMipCcmDbInfo * EcfmCcUtilGetMipCcmDbEntry PROTO ((UINT2 , 
                                                                UINT1 *));
PUBLIC VOID EcfmCcAgeoutMipDbEntry PROTO ((VOID));
PUBLIC INT4 EcfmMipCcmDbCmp PROTO ((tRBElem *,tRBElem *));

/* CCM Offload related */
PUBLIC INT4 EcfmCcmOffLoadUpdateLlc(UINT4 u4FsEcfmPortIndex);
PUBLIC INT4 EcfmUpdatePortStatusForDownMep (tEcfmCcMepInfo * pMepInfo);
PUBLIC INT4 EcfmUpdatePortStatusForAllUpMep (UINT4 u4ContextId,
                                             UINT4 u4IfIndex,
                                             tEcfmCcPortInfo * pPortInfo,
                                             UINT1 u1PortState);
PUBLIC VOID EcfmCcmOffLoadUpdateTxRxStats(UINT4 u4FsEcfmPortIndex);
PUBLIC INT4 EcfmCcmOffStatus (INT4 i4EcfmOffloadStatus);
PUBLIC INT4 EcfmCcmOffCreateTxRxForMep (tEcfmCcMepInfo *pMepNode);
PUBLIC VOID EcfmCcmOffDeleteTxRxForMep (tEcfmCcMepInfo *pMepNode);
PUBLIC VOID EcfmCcmOffHwRegisterCallBack(UINT4 u4ContextId);
PUBLIC INT4 EcfmCcmOffHandleCallBackEvt (UINT4 u4ContextId, UINT2 u2RxHandle,
                                         UINT4 u4IfIndex);
PUBLIC INT4 EcfmCcmHwHandleRxCallBackEvt (UINT4 u4ContextId, UINT1* pau1HwHandle,
                                          UINT4 u4IfIndex);
PUBLIC INT4 EcfmCcmHwHandleTxFailureEvt (UINT4 u4ContextId, UINT1 *pau1HwHandle);
PUBLIC INT4 FsEcfmOffloadSetRDI(tEcfmCcMepInfo *pMepInfo);
PUBLIC INT4 EcfmCcmOffHandleTxFailureEvt (UINT4 u4ContextId, UINT2 u2TxHandle);
PUBLIC INT4 EcfmHandleUpdateDeiBit (UINT4 u4IfIndex);
PUBLIC INT4 EcfmHandlePortStateChangeIndication PROTO ((UINT2,UINT4, UINT1));
PUBLIC INT4 EcfmCcmOffCreateTxMep (tEcfmCcMepInfo * pMepNode);
PUBLIC INT4 EcfmCcmOffCreateRxRMep (tEcfmCcMepInfo * pMepNode,
                                     tEcfmCcRMepDbInfo * pRMepNode);
PUBLIC INT4 EcfmCcmOffCreateRxForAllRMep (tEcfmCcMepInfo * pMepNode);
PUBLIC INT4 EcfmCcmOffDeleteRxRMep (tEcfmCcMepInfo * pMepNode,
                                     tEcfmCcRMepDbInfo * pRMepNode);
PUBLIC VOID EcfmCcmOffDeleteRxForAllRMep (tEcfmCcMepInfo * pMepNode);
PUBLIC INT4 EcfmCcmOffDeleteTxMep (tEcfmCcMepInfo * pMepNode);
PUBLIC VOID EcfmCcmOffGenerateMAID (tEcfmCcMepInfo * pMepInfo, UINT1 *pu1MAID);
PUBLIC INT4 EcfmCcCtrlTxTransmitPktToOffLoad (tEcfmBufChainHeader * pBuf,
     tEcfmCcMepInfo * pCcMepInfo, tEcfmVlanTagInfo * pVlanInfo, 
     tEcfmCcMepCcInfo * pCcMepCcInfo);
PUBLIC INT4        EcfmCcCtrlTxFwdToHwForDown (tEcfmBufChainHeader * pBuf,
                                                tEcfmCcMepInfo * pCcMepInfo,
                                                tEcfmCcMepCcInfo *
                                                pCcMepCcInfo);
PUBLIC INT4        EcfmCcCtrlTxFwdToHdForUp (tEcfmCcPduSmInfo * pPduSmInfo);
PUBLIC VOID        EcfmCcmOffFormatEthHdr (tEcfmCcMepInfo * pMepInfo,
                                            UINT1 **pu1CcmPdu, UINT1 u1Length,
                                            tVlanTagInfo VlanInfo);
PUBLIC VOID        EcfmFormVlanTagFrame (UINT2 *pu2VlanTagId,
                                          tVlanTagInfo VlanInfo,
                                          UINT1 u1DeiBitValue);
PUBLIC INT4
 
 
 
 EcfmCcCtrlTxTransmitIsidPktToOffLoad (tEcfmBufChainHeader * pBuf,
                                       tEcfmCcMepInfo * pCcMepInfo,
                                       tEcfmIsidTagInfo * pIsidInfo,
                                       tEcfmCcMepCcInfo * pCcMepCcInfo);

PUBLIC VOID         EcfmCciSmPutInfo (tEcfmCcPduSmInfo * pPduSmInfo,
                                      UINT1 **pu1CcmPdu);

PUBLIC VOID         EcfmCciCcmOffFormatCcmPduHdr (tEcfmCcMepInfo * pMepInfo,
                                                  UINT1 **pu1CcmPdu);
PUBLIC VOID         EcfmCcOffCtrlTxParsePduHdrs (tEcfmCcPduSmInfo * pPduSmInfo);

PUBLIC INT4         EcfmCcmOffCreateRemoteMepRx (UINT4 u4IfIndex, UINT2 u2MepId,
                                                 tEcfmCcOffRxInfo *
                                                 pEcfmCcOffRxInfo,
                                                 UINT2 u2OffloadRMepRxHandle,
                                                tEcfmCcMepInfo *pMepInfo, 
                                                tEcfmCcRMepDbInfo *pRmepInfo);

PUBLIC INT4         EcfmCcmOffCreateMepTxOnPortList (tPortListExt IfFwdPortList,
                                                     tPortListExt
                                                     UntagIfFwdPortList,
                                                     UINT1 *pu1Buf,
                                                     UINT2 u2PduLength,
                                                     UINT4 u4CcmInterval,
                                                     UINT4 u4ContextId,
                                                     UINT2
                                                     u2OffloadMepTxHandle,
                                                     tEcfmCcMepInfo *pMepInfo, 
                                                     tEcfmCcRMepDbInfo *pRmepInfo);

PUBLIC INT4         EcfmCcmOffCreateMepTx (UINT4 u4IfIndex, UINT1 *pu1Buf,
                                           UINT2 u2PduSize, UINT4 u4CcmInterval,
                                           UINT2 u2OffloadMepTxHandle,
                                           tEcfmCcMepInfo *pMepInfo);

PUBLIC INT4         EcfmGetCcmOffTxStatistics (UINT4 u4ContextId,
                                               UINT2 u2OffloadMepTxHandle,
                                               tEcfmCcOffMepTxStats *
                                               pEcfmCcOffMepTxStats);

PUBLIC VOID         EcfmCcmOffDeleteMepTx (UINT4 u4ContextId,
                                           UINT2 u2OffloadMepTxHandle,
                                           tEcfmCcMepInfo *pMepInfo);

PUBLIC INT4         EcfmGetCcmOffRxStatistics (UINT4 u4ContextId,
                                               UINT2 u2OffloadRMepRxHandle,
                                               tEcfmCcOffMepRxStats *
                                               pEcfmCcOffMepRxStats);

PUBLIC INT4         EcfmStartCcmOffCreateMepRxOnPortList (tPortListExt
                                                          IfFwdActualPortList,
                                                          tPortListExt
                                                          UntagIfFwdActualPortList,
                                                          UINT2 u2MEPID,
                                                          tEcfmCcOffRxInfo *
                                                          pEcfmCcOffRxInfo,
                                                          UINT4 u4ContextId,
                                                          UINT2
                                                          u2OffloadRMepRxHandle,
                                                          tEcfmCcMepInfo *pMepNode, 
                                                          tEcfmCcRMepDbInfo *pRmep);

PUBLIC VOID         EcfmCcmOffDeleteMepRx (UINT4 u4ContextId,
                                           UINT2 u2OffloadMepTxHandle,
                                           tEcfmCcMepInfo *pMepNode,
                                           tEcfmCcRMepDbInfo *pRMepNode);
PUBLIC VOID        EcfmCcmoffUpdateRxStats (tEcfmCcPortInfo * pPortInfo,
                                             tEcfmCcMepInfo * pMepNode);
PUBLIC INT4         EcfmGetPortCcStats (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        tEcfmCcOffPortStats *
                                        pEcfmCcOffPortStats);
PUBLIC VOID EcfmHwUpdateMaMepParams (tEcfmHwParams *, tEcfmCcMepInfo *,tEcfmCcRMepDbInfo *);
/* MI */
PUBLIC INT4 EcfmCcTmrInit PROTO ((VOID));
PUBLIC VOID  EcfmCcTmrDeInit PROTO((VOID));

/* High Availability related */
PUBLIC INT4 EcfmRedRegisterWithRM PROTO ((VOID));
PUBLIC INT4 EcfmRedDeRegisterWithRM PROTO ((VOID));
PUBLIC INT4 EcfmRedSyncUpPortOperStatus PROTO ((UINT4,UINT1));
PUBLIC INT4 EcfmRedSendMsgToRm PROTO ((tRmMsg *, UINT2));
PUBLIC VOID EcfmCcRedHandleRmEvents PROTO ((tEcfmCcMsg *));
PUBLIC VOID EcfmRedGetNodeStateFromRm PROTO ((VOID));
PUBLIC VOID EcfmRedHandleBulkUpdateEvent PROTO ((VOID));
PUBLIC VOID EcfmRedSyncMipDbHldTmr PROTO ((UINT4));
PUBLIC VOID EcfmRedSyncErrorLogEntry  PROTO ((tEcfmCcErrLogInfo *));
PUBLIC VOID EcfmRedSyncTmr PROTO ((UINT1 ,UINT1 ,tEcfmCcPduSmInfo *,UINT4));
PUBLIC VOID EcfmRedSyncLckCondition PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncLckStatus PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncLckPeriodTimeout PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncLckRxWhileTimeout PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncAisCondition PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncAisPeriodTimeout PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncLmCache PROTO ((VOID));
PUBLIC VOID EcfmRedSyncLmDeadlineTmrExp PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmRedSyncLmStopTransaction PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmRedSyncSmData PROTO ((tEcfmCcPduSmInfo * ));
PUBLIC VOID EcfmRedCheckAndSyncSmData PROTO ((UINT1*, UINT1,tEcfmCcPduSmInfo *));
PUBLIC VOID
EcfmRedSynchHwAuditCmdEvtInfo PROTO ((UINT4,UINT4,UINT4,UINT4,UINT4,UINT4,tVlanId,BOOL1));
PUBLIC VOID EcfmRedSynchRmepCounter PROTO ((tEcfmCcPduSmInfo *, UINT1 ));
PUBLIC VOID EcfmRedSyncCcSeqCounter PROTO ((tEcfmCcPduSmInfo *));
PUBLIC INT4 EcfmRedSynchXConWhile PROTO ((tEcfmCcMepInfo *,UINT4 ));
PUBLIC INT4 EcfmRedSynchErrWhile PROTO ((tEcfmCcMepInfo *pMepInfo,UINT4 u4CcmInterval));
PUBLIC INT4 EcfmRedSyncCCDataOnChng PROTO ((UINT4 ,UINT4 ,UINT4 ,UINT2 ,UINT2 ));
PUBLIC VOID EcfmRedSynchOffTxFilterId PROTO ((UINT4,UINT4,UINT4,UINT4,UINT4));
PUBLIC VOID EcfmRedSynchOffRxFilterId PROTO ((UINT4,UINT4,UINT4,UINT4,UINT4,UINT4));
PUBLIC VOID EcfmRedSynchHwTxHandler PROTO ((UINT4,UINT4,UINT4,UINT4,UINT1 *));
PUBLIC VOID EcfmRedSynchHwRxHandler PROTO ((UINT4,UINT4,UINT4,UINT4,UINT4,UINT1 *));

/* HITLESS RESTART */
VOID EcfmRedHRProcStdyStPktReq (VOID);
INT1 EcfmRedHRSendStdyStTailMsg (VOID);
UINT1 EcfmRedGetHRFlag (VOID);
INT1
EcfmRedHRSendStdyStPkt (UINT1 *pu1LinBuf, UINT4 u4PktLen, UINT2 u2Port,UINT4 u4TimeOut);


/* Y_1731 related */
PUBLIC INT4 EcfmCcRdiPeriodTimeout PROTO ((tEcfmCcMepInfo *));
PUBLIC tEcfmCcErrLogInfo *
EcfmCcAddErrorLogEntry PROTO ((tEcfmCcPduSmInfo * ,UINT2 ));
PUBLIC INT4 EcfmCcUtilPostTransaction PROTO ((tEcfmCcMepInfo *, UINT1));
/* Loss Measurement */
PUBLIC UINT4 EcfmCcClntLmInitiator PROTO((tEcfmCcPduSmInfo *, UINT1));
PUBLIC  VOID EcfmLmInitStopLmTransaction PROTO ((tEcfmCcPduSmInfo *));
PUBLIC tEcfmCcFrmLossBuff * EcfmLmInitAddLmEntry PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcClntParseLmr PROTO ((tEcfmCcPduSmInfo *, UINT1 *));
PUBLIC INT4 EcfmCcClntProcessLmr PROTO ((tEcfmCcPduSmInfo *, BOOL1 *));
PUBLIC INT4 EcfmCcClntProcessLmm PROTO ((tEcfmCcPduSmInfo *,UINT1 *,BOOL1 *));
PUBLIC UINT4 EcfmCcClntCalcFrameLoss PROTO ((tEcfmCcPduSmInfo *,UINT1));
/* AIS related*/
PUBLIC UINT4 EcfmCcClntAisInitiator PROTO ((tEcfmCcMepInfo *, UINT1));
PUBLIC VOID EcfmCcInitStopAisTx PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmCcAisPeriodConfiguration PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmCcAisCapabilityConfiguration PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcAisPeriodTimeout PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmCcClntProcessAis PROTO ((tEcfmCcPduSmInfo * , BOOL1 *));
PUBLIC VOID EcfmCcClearAisCondition PROTO ((tEcfmCcMepInfo *));

/* LCK related*/
PUBLIC UINT4 EcfmCcClntLckInitiator PROTO ((tEcfmCcMepInfo *, UINT1));
PUBLIC VOID EcfmCcInitStopLckTx PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmCcLckPeriodConfiguration PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcLckPeriodTimeout PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmCcClntProcessLck PROTO ((tEcfmCcPduSmInfo * , BOOL1 *));
PUBLIC VOID EcfmCcClearLckCondition PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcUpdateLckStatus PROTO ((tEcfmCcMepInfo * ));
PUBLIC UINT1 EcfmCcUtilGetMepSenderIdPerm PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID
EcfmCcmOffFormatIsidEthHdr PROTO ((tEcfmCcMepInfo * pMepInfo, UINT1 **pu1CcmPdu, UINT1
                        u1Length, tIsidTagInfo IsidInfo));
PUBLIC INT4
EcfmCcAHCtrlTxFwdToFf PROTO ((tEcfmCcPduSmInfo * pPduSmInfo));
PUBLIC INT4
EcfmCcAHCtrlTxFwdToPort PROTO ((tEcfmBufChainHeader *,UINT2, tEcfmVlanTag *,  tEcfmPbbTag  *));
PUBLIC INT4
EcfmAHCheckNValidateMepCreation PROTO ((UINT2, UINT4, UINT1));
PUBLIC VOID
EcfmConvertPortListToArray (tPortListExt LocalPortList,
                            UINT4 *pu4IfPortArray, UINT2 *u2NumPorts);
PUBLIC VOID EcfmCcIfInterfaceTypeChg PROTO ((UINT1));
PUBLIC VOID EcfmCcUtilGetDefaultMdNode PROTO ((UINT4,tEcfmCcDefaultMdTableInfo **));
PUBLIC VOID EcfmCcHandleMacStatusChange PROTO ((tEcfmCcMepInfo *pMepNode, 
                                               UINT4 u4Event));
PUBLIC VOID EcfmCcUtilUpdatePortState PROTO ((tEcfmCcMepInfo * pMepInfo,
                                              UINT1 u1PortState));
PUBLIC INT4 EcfmSendAisLckOnAllCVlan PROTO ((tEcfmCcPduSmInfo *));
PUBLIC VOID EcfmCcUtilRelinquishLoop PROTO ((UINT4 ));
PUBLIC VOID EcfmCcUtilY1731EnableMep PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcUtilY1731DisableMep PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcUtilModuleEnableForAMep PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmCcUtilModuleDisableForAMep PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmCcmOffHandleIntQFailure PROTO ((UINT2 ));
PUBLIC INT4 EcfmClearCcContextStats PROTO ((UINT4, UINT1));
PUBLIC INT4 EcfmGetCcContextStats   PROTO ((UINT4, UINT4 *, UINT1));
PUBLIC INT4 EcfmTestCcContextStats  PROTO ((UINT4 *, UINT4, UINT4));
/* cfmccsm.c */
PUBLIC VOID EcfmUtilPutMAID PROTO((tEcfmCcMepInfo *, UINT1 *));
/* Stack Overflow */
PUBLIC VOID EcfmNotifyProtocolShutdownStatus PROTO ((INT4));
PUBLIC INT4 EcfmSendForAllISIDFromCBP PROTO ((tEcfmBufChainHeader *, 
                       UINT2, tEcfmVlanTag *, tEcfmPbbTag *, UINT1));
PUBLIC INT4        EcfmCcAHSendForAllVLAN PROTO ((tEcfmBufChainHeader *, 
                                UINT2, tEcfmVlanTag *, tEcfmPbbTag *, UINT1));
PUBLIC INT4 EcfmCcADSendForAllVLAN PROTO ((tEcfmBufChainHeader *, UINT2,
                                            tEcfmVlanTag *, UINT1));
PUBLIC VOID EcfmCcCtrlTxParsePduHdrs PROTO ((tEcfmCcPduSmInfo *));
PUBLIC BOOL1 EcfmCcTxCheckLckCondition PROTO ((UINT1, UINT2, UINT4, UINT1));
PUBLIC INT4 EcfmCcADCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, 
                        UINT2 u2LocalPort, tEcfmVlanTag * pVlanInfo,
                        UINT1 u1MpDirection, UINT1 u1OpCode);
PUBLIC INT4 EcfmCcAHCtrlTxTransmitPkt (tEcfmBufChainHeader * pBuf, 
                        UINT2 u2LocalPort, tEcfmVlanTag * pVlanTag,
                        tEcfmPbbTag * pPbbTag, UINT1 u1MpDirection,
                        UINT1 u1OpCode);
PUBLIC INT4 EcfmCcSendForVlanUnAwareMa PROTO ((tEcfmCcPduSmInfo * pPduSmInfo));
PUBLIC INT4 EcfmCcSendForVlanAwareMa PROTO ((tEcfmCcPduSmInfo * pPduSmInfo));

/* Availability Red Syncup */
PUBLIC VOID EcfmRedSyncAvlbltyInfo PROTO(( tEcfmCcMepInfo     *pMepInfo));

#endif /* _CFM_CC_PRT_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmccprt.h                      */
/*-----------------------------------------------------------------------*/
