/********************************************************************
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021CfmStackTable. */
INT1
nmhValidateIndexInstanceIeee8021CfmStackTable ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CfmStackTable  */

INT1
nmhGetFirstIndexIeee8021CfmStackTable ARG_LIST((INT4 * , INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CfmStackTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CfmStackMdIndex ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021CfmStackMaIndex ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021CfmStackMepId ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021CfmStackMacAddress ARG_LIST((INT4  , INT4  , UINT4  , INT4  , INT4 ,tMacAddr * ));

/* Proto Validate Index Instance for Ieee8021CfmVlanTable. */
INT1
nmhValidateIndexInstanceIeee8021CfmVlanTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CfmVlanTable  */

INT1
nmhGetFirstIndexIeee8021CfmVlanTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CfmVlanTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CfmVlanPrimarySelector ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CfmVlanRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CfmVlanPrimarySelector ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CfmVlanRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CfmVlanPrimarySelector ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CfmVlanRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CfmVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CfmDefaultMdTable. */
INT1
nmhValidateIndexInstanceIeee8021CfmDefaultMdTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CfmDefaultMdTable  */

INT1
nmhGetFirstIndexIeee8021CfmDefaultMdTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CfmDefaultMdTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CfmDefaultMdStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CfmDefaultMdLevel ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CfmDefaultMdMhfCreation ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CfmDefaultMdIdPermission ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CfmDefaultMdLevel ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CfmDefaultMdMhfCreation ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CfmDefaultMdIdPermission ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CfmDefaultMdLevel ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CfmDefaultMdMhfCreation ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CfmDefaultMdIdPermission ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CfmDefaultMdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CfmConfigErrorListTable. */
INT1
nmhValidateIndexInstanceIeee8021CfmConfigErrorListTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CfmConfigErrorListTable  */

INT1
nmhGetFirstIndexIeee8021CfmConfigErrorListTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CfmConfigErrorListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CfmConfigErrorListErrorType ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Ieee8021CfmMaCompTable. */
INT1
nmhValidateIndexInstanceIeee8021CfmMaCompTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CfmMaCompTable  */

INT1
nmhGetFirstIndexIeee8021CfmMaCompTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CfmMaCompTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CfmMaCompPrimarySelectorType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CfmMaCompPrimarySelectorOrNone ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CfmMaCompMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CfmMaCompIdPermission ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CfmMaCompNumberOfVids ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CfmMaCompRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CfmMaCompPrimarySelectorType ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CfmMaCompPrimarySelectorOrNone ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CfmMaCompMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CfmMaCompIdPermission ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CfmMaCompNumberOfVids ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CfmMaCompRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CfmMaCompPrimarySelectorType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CfmMaCompPrimarySelectorOrNone ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CfmMaCompMhfCreation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CfmMaCompIdPermission ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CfmMaCompNumberOfVids ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CfmMaCompRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CfmMaCompTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
