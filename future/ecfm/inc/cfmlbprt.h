/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlbprt.h,v 1.26 2015/10/27 11:27:46 siva Exp $
 * 
 * Description: This file contains the prototypes for all the 
 *              PUBLIC procedures in LBLT Task. 
 *********************************************************************/
#ifndef _CFM_LB_PRT_H
#define _CFM_LBP_RT_H

/* Global Initiator */
PUBLIC INT4 EcfmLbLtTaskInit PROTO ((VOID));
PUBLIC VOID EcfmLbLtHandleTaskInitFailure PROTO ((VOID));
PUBLIC INT4 EcfmLbLtInitGlobalInfo PROTO ((VOID));
PUBLIC VOID EcfmLbLtDeInitGlobalInfo PROTO ((VOID));
PUBLIC VOID EcfmLbLtDeInitMempool PROTO ((VOID));

/* Y1564 and RFC 2544 Initiator */
PUBLIC VOID
EcfmLbLtHandleY1564andRFC2544Events PROTO ((tEcfmLbLtMsg *));

/* LBM Initiator */
PUBLIC INT4
EcfmLbLtClntLbInitiator PROTO ((tEcfmLbLtMepInfo *, UINT1));

PUBLIC VOID 
EcfmLbiTxStopXmitLbmPdu PROTO ((tEcfmLbLtMepInfo *));

/* LBR Receiver */
PUBLIC INT4
EcfmLbiRxProcessLbr PROTO((tEcfmLbLtPduSmInfo * ));

/* LoopBack Reply Parser  */
PUBLIC INT4 EcfmLbLtClntParseLbr PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));

/* LBM Responder */
PUBLIC INT4
EcfmLbResProcessLbm PROTO ((tEcfmLbLtPduSmInfo *, BOOL1 *));

PUBLIC INT4 
EcfmLtInitRxSmProcessLtr PROTO((tEcfmLbLtPduSmInfo * ));
PUBLIC INT4
EcfmLtmRxSmProcessLtm PROTO((tEcfmLbLtPduSmInfo * ));

PUBLIC INT4 EcfmLbLtClntParseLtm PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));
PUBLIC INT4 EcfmLbLtClntLtInitSm PROTO ((tEcfmLbLtPduSmInfo *, UINT1));
PUBLIC VOID EcfmLbLtUtilHandleLtNodeAddFail PROTO ((tEcfmLbLtLtmReplyListInfo * ));
PUBLIC INT4 EcfmLbLtClntLtInitRxSm PROTO ((tEcfmLbLtPduSmInfo *,UINT1));
PUBLIC VOID EcfmSendLtmEventToCli PROTO ((tEcfmLbLtMepInfo *, UINT1));
/* Queue Handler */
PUBLIC VOID EcfmLbLtPktQueueHandler PROTO ((tEcfmLbLtMsg *));
PUBLIC VOID EcfmLbLtCfgQueueHandler PROTO ((tEcfmLbLtMsg *));
PUBLIC INT4 EcfmLbLtCfgQueMsg PROTO ((tEcfmLbLtMsg *));
/* */
PUBLIC INT4 EcfmLbLtModuleStart PROTO ((VOID));
PUBLIC VOID EcfmLbLtModuleShutDown PROTO ((VOID));
PUBLIC VOID EcfmLbLtModuleEnable PROTO ((VOID));
PUBLIC VOID EcfmLbLtModuleDisable PROTO ((VOID));
PUBLIC VOID EcfmLbLtModuleEnableForAPort PROTO((UINT2));
PUBLIC VOID EcfmLbLtModuleDisableForAPort PROTO((UINT2));
PUBLIC VOID EcfmLbLtY1731Enable PROTO ((VOID));
PUBLIC VOID EcfmLbLtY1731Disable PROTO ((VOID));
PUBLIC VOID EcfmLbLtY1731EnableForAPort PROTO ((UINT2 ));
PUBLIC VOID EcfmLbLtY1731DisableForAPort PROTO ((UINT2 ));

PUBLIC VOID EcfmLbLtIfCreateAllPorts PROTO((VOID));
PUBLIC INT4 EcfmLbLtIfHandleCreatePort PROTO ((UINT4, UINT4, UINT2));
PUBLIC INT4 EcfmLbLtIfCreateMplsPort PROTO ((UINT4, UINT4, UINT2));
PUBLIC VOID EcfmLbLtIfHandleDeletePort PROTO ((UINT2));
PUBLIC INT4 EcfmLbLtIfHandlePortOperChg PROTO ((UINT2 , UINT1));
PUBLIC  tEcfmLbLtStackInfo* EcfmLbLtUtilGetMp PROTO ((UINT2 ,UINT1, 
                                                            UINT4, UINT1));
PUBLIC tEcfmLbLtMepInfo* EcfmLbLtUtilGetMepEntryFrmGlob PROTO ((UINT4 , UINT4 , 
                                                         UINT2 ));
PUBLIC INT4 EcfmLbLtUtilFreeEntryFn PROTO ((tRBElem * , UINT4 ));
tEcfmLbLtMepInfo * EcfmLbLtUtilGetMepEntryFrmPort PROTO ((UINT1, UINT4, UINT2, 
                                                  UINT1));  
                                            
PUBLIC  tEcfmLbLtMipInfo * EcfmLbLtUtilGetMipEntry PROTO ((UINT2 , UINT1 , UINT4 ));
PUBLIC VOID EcfmLbLtUtilNotifySm PROTO ((tEcfmLbLtMepInfo * , UINT1 ));
PUBLIC VOID EcfmLbLtUtilNotifyY1731 PROTO ((tEcfmLbLtMepInfo * , UINT1 ));
PUBLIC INT4 EcfmLbLtLtrCmp PROTO ((tRBElem *,tRBElem *));

/* LBLT Task Timer Routines */
PUBLIC VOID EcfmLbLtTmrExpHandler PROTO ((VOID));

PUBLIC INT4 EcfmLbLtTmrStartTimer PROTO ((UINT1, tEcfmLbLtMepInfo *, UINT4));
PUBLIC INT4 EcfmLbLtTmrStopTimer PROTO ((UINT1, tEcfmLbLtMepInfo *));

/* cfmlbtx.c*/
PUBLIC INT4 EcfmLbLtADCtrlTxFwdToPort PROTO ((tEcfmBufChainHeader *, UINT2, 
                                            tEcfmVlanTag*));
PUBLIC INT4 EcfmLbLtADCtrlTxFwdToFf PROTO ((tEcfmLbLtPduSmInfo *));
PUBLIC INT4 EcfmLbLtCtrlTxTransmitPkt PROTO((tEcfmBufChainHeader *,  UINT2, 
                                             UINT4, UINT1, UINT1, UINT1, UINT1, 
                                             tEcfmVlanTag *, tEcfmPbbTag *));
/* Receiver Routine */
PUBLIC INT4 EcfmLbLtCtrlRxPkt PROTO ((tEcfmBufChainHeader *, UINT4, UINT2));
PUBLIC VOID EcfmLbLtCtrlRxPktFree PROTO ((tEcfmBufChainHeader *));
PUBLIC INT4 EcfmLbLtCtrlRxLevelDeMux PROTO ((tEcfmLbLtPduSmInfo *));
PUBLIC INT4 EcfmLbLtCtrlRxOpCodeDeMux PROTO ((tEcfmLbLtPduSmInfo *));
/* LTR Parser Routine */
PUBLIC INT4 EcfmLbLtClntParseLtr PROTO ((tEcfmLbLtPduSmInfo *, UINT1 * ));


PUBLIC INT4 EcfmLbLtUtilChkPortFiltering PROTO ((UINT2, UINT4));
PUBLIC VOID EcfmSnmpLwDelMepLtmReplyList PROTO ((VOID));

/*MI*/
PUBLIC INT4 EcfmLbLtHandleCreateContext PROTO ((UINT4));
PUBLIC INT4 EcfmLbLtHandleDeleteContext PROTO ((UINT4));
PUBLIC INT4 EcfmLbLtSelectContext PROTO ((UINT4));
PUBLIC INT4 EcfmLbLtReleaseContext PROTO ((VOID));
PUBLIC INT4 EcfmLbLtGetNextActiveContext PROTO ((UINT4 , UINT4 *));

/* MI */  
PUBLIC VOID EcfmLbLtDeleteAllPorts PROTO ((VOID));
PUBLIC INT4 EcfmLbLtTmrInit PROTO ((VOID));
PUBLIC VOID EcfmLbLtTmrDeInit PROTO ((VOID));
/* High Availability related */
PUBLIC VOID EcfmLbLtRedHandleRmEvents PROTO ((tEcfmLbLtMsg *));
PUBLIC VOID EcfmRedSyncLbLtPdu PROTO ((tEcfmBufChainHeader *, UINT4));
PUBLIC VOID EcfmRedSyncLtrCacheHldTmr PROTO ((UINT4));
PUBLIC VOID EcfmRedSyncDelayQueueTmrExpiry PROTO ((VOID));
PUBLIC VOID EcfmRedSyncLbLtMepTmrExp PROTO ((UINT1, UINT4, UINT4, UINT4));
PUBLIC VOID EcfmRedSyncStopLbTstTran PROTO ((UINT1 ,tEcfmLbLtMsg *));
PUBLIC VOID EcfmRedSyncTransactionStopEvent PROTO ((UINT1,UINT4,
                                                    UINT4,UINT4));
PUBLIC VOID EcfmRedSyncLbmTstStopTx (UINT1 u1TransType,
                      UINT4 u4MdIndex, UINT4 u4MaIndex, UINT4 u4MepIndex);
PUBLIC VOID EcfmRedSyncDmDeadlineTimerExp PROTO ((tEcfmLbLtMepInfo *));
PUBLIC VOID EcfmRedStartMepLbLtTimers PROTO ((UINT4, UINT4 ));
PUBLIC VOID EcfmRedFillLbLtEcfmMepInfo PROTO ((tRmMsg *, UINT2 *, UINT4, UINT4,
                                           UINT2, UINT4));
PUBLIC VOID EcfmRedFillLbLtY1731MepInfo PROTO ((tRmMsg *, UINT2 *, UINT4, UINT4,
                                           UINT2, UINT4));
PUBLIC VOID EcfmRedSyncDmCache PROTO ((VOID));
PUBLIC VOID EcfmRedSyncDmTransIntrvalExp PROTO ((tEcfmLbLtMepInfo *));
PUBLIC VOID EcfmRedLbLtUpdateEcfmMepInfo PROTO ((tRmMsg *, UINT2 *, UINT4, UINT4, 
                                      UINT2, UINT4));
PUBLIC VOID EcfmRedLbLtUpdateY1731MepInfo PROTO ((tRmMsg *, UINT2 *, UINT4, UINT4, 
                                      UINT2, UINT4));
PUBLIC VOID EcfmRedSyncLbCache  PROTO ((VOID));
PUBLIC VOID EcfmRedSyncDmStopTransaction PROTO ((tEcfmLbLtMepInfo *));
PUBLIC VOID EcfmRedUpdateDmBuffInfo PROTO ((tRmMsg *, UINT2 *, UINT2));
PUBLIC VOID EcfmRedUpdateLbBuffInfo PROTO ((tRmMsg *, UINT2 *, UINT2));
PUBLIC VOID EcfmRedSyncLbrCacheHldTmr PROTO ((UINT4));
PUBLIC VOID EcfmRedStartLBLTTimersOnActiveEvent PROTO ((VOID));
PUBLIC VOID EcfmRedClearDmSyncFlag PROTO ((VOID));
PUBLIC VOID EcfmRedClearLbSyncFlag PROTO ((VOID));
PUBLIC VOID EcfmRedSyncLbLtTransStatus PROTO ((UINT4, UINT4, UINT4, 
                                               UINT2, UINT1, UINT1,
                                               UINT1,UINT1));
/* TH Initiator*/
PUBLIC UINT4 EcfmLbLtClntThInitiator PROTO ((tEcfmLbLtPduSmInfo *, UINT1));
PUBLIC VOID EcfmThInitStopThTransaction PROTO ((tEcfmLbLtMepInfo *));
PUBLIC INT4 EcfmLbLtClntThParseVsr PROTO ((tEcfmLbLtPduSmInfo * , UINT1 *));
PUBLIC INT4 EcfmLbltClntThProcessVsr PROTO ((tEcfmLbLtPduSmInfo * ));

/* TH VSM Responder*/
PUBLIC INT4 EcfmLbLtClntThParseVsm PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));
PUBLIC INT4 EcfmLbltClntThProcessVsm PROTO ((tEcfmLbLtPduSmInfo *, BOOL1*));

/* DM Initiator*/
PUBLIC UINT4 EcfmLbLtClntDmInitiator PROTO ((tEcfmLbLtMepInfo *, UINT1));
PUBLIC VOID EcfmDmInitStopDmTransaction PROTO ((tEcfmLbLtMepInfo *));

/* TST Initiator*/
PUBLIC INT4 EcfmLbLtClntTstInitiator PROTO ((tEcfmLbLtMepInfo *, UINT1));
PUBLIC INT4 EcfmLbltClntProcessTst PROTO ((tEcfmLbLtPduSmInfo *, BOOL1 *));
PUBLIC INT4 EcfmLbLtClntParseTst PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));

/* APS, MCC, EXM, EXR, VSM, VSR related */
PUBLIC INT4 EcfmLbLtClntExApiInitiator PROTO((tEcfmLbLtMsg *));
PUBLIC INT4 EcfmLbLtClntProcessExApi PROTO ((tEcfmLbLtPduSmInfo *, BOOL1 *));

/*APS MCC EXM EXR VSM VSR responders */
PUBLIC VOID EcfmLbLtClntFwdExPdu PROTO ((tEcfmLbLtPduSmInfo * , UINT1 *));
PUBLIC INT4 EcfmLbLtClntXmitExPdu PROTO ((tExternalPduMsg *, UINT1));
/* Get current time */
PUBLIC VOID EcfmLbLtUtilDmGetCurrentTime PROTO ((tEcfmTimeRepresentation *));

/* Frame delay related */
PUBLIC tEcfmLbLtFrmDelayBuff *EcfmDmInitAddDmEntry PROTO ((tEcfmLbLtMepInfo *));
/* DM Responder*/
PUBLIC INT4 EcfmLbLtClntParseDmm PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));
PUBLIC INT4 EcfmLbltClntProcessDmm PROTO ((tEcfmLbLtPduSmInfo *, BOOL1*));
/* DM Initator Receiver*/
PUBLIC INT4 EcfmLbLtClntParseDmr PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));
PUBLIC INT4 EcfmLbltClntProcessDmr PROTO ((tEcfmLbLtPduSmInfo *));
PUBLIC INT4 EcfmLbLtUtilPostTransaction PROTO ((tEcfmLbLtMepInfo *, UINT1));
/* 1DM Initiator */
PUBLIC INT4 EcfmLbLtClntParse1Dm PROTO ((tEcfmLbLtPduSmInfo *, UINT1 *));
PUBLIC INT4 EcfmLbltClntProcess1Dm PROTO ((tEcfmLbLtPduSmInfo *, BOOL1*));
/* Calculate CRC 32*/
PUBLIC UINT4 EcfmLbLtUtilCalculateCrc32 PROTO ((UINT1 *, UINT4 ));
/* Delete LB node */
PUBLIC VOID
EcfmLbLtUtilRemoveLbEntry PROTO ((tEcfmLbLtLbmInfo * ));
/* Frame Dealy*/
PUBLIC INT4
EcfmLbLtFrmDelayBuffCmp PROTO ((tRBElem * , tRBElem * ));
PUBLIC tEcfmLbLtMipCcmDbInfo * EcfmLbLtUtilGetMipCcmDbEntry PROTO ((UINT2, UINT1 *));
PUBLIC tEcfmLbLtRMepDbInfo * EcfmLbLtUtilGetRMepDbEntry PROTO ((UINT4, UINT4, UINT2,UINT2));
PUBLIC INT4 EcfmLbLtUtilGetRMepMacAddr PROTO ((UINT4, UINT4,UINT2, UINT2, UINT1 *));
PUBLIC tEcfmLbLtMdInfo * EcfmLbLtUtilGetMdEntry PROTO ((UINT4));
PUBLIC tEcfmLbLtMaInfo * EcfmLbLtUtilGetMaEntry PROTO ((UINT4, UINT4));
PUBLIC UINT1 EcfmLbLtUtilGetMhfSenderIdPerm PROTO ((tEcfmLbLtStackInfo *));
PUBLIC UINT1 EcfmLbLtUtilGetMepSenderIdPerm PROTO ((tEcfmLbLtMepInfo *));
/*Delay Queue Routines*/
/* routine for adding response CFM PDU to queue*/
PUBLIC VOID EcfmLbLtAddCfmPduToDelayQueue PROTO ((tEcfmLbLtDelayQueueNode *));

/* routine for Delay Queue CFM PDU Transmission*/
PUBLIC VOID EcfmLbLtDelayQueueTimerTimeOut PROTO ((VOID));

/* routine for Innitialize the Timer Pending Delay Queue CFM PDUs */
PUBLIC VOID EcfmLbLtInitDelayQueue PROTO((VOID));

/* routine for clearing Pending Delay Queue CFM PDUs */
PUBLIC VOID EcfmLbLtDeInitDelayQueue PROTO((VOID));
PUBLIC INT4
EcfmLbLtAHCtrlTxFwdToPort PROTO ((tEcfmBufChainHeader *, UINT2, tEcfmVlanTag *,  tEcfmPbbTag*));
PUBLIC INT4
EcfmLbLtAHCtrlTxFwdToFf PROTO ((tEcfmLbLtPduSmInfo *));

PUBLIC VOID
EcfmLbLtIfHandleIntfTypeChg PROTO ((UINT1));
PUBLIC VOID EcfmLbLtUtilGetDefaultMdNode PROTO ((UINT4, tEcfmLbLtDefaultMdTableInfo **));
PUBLIC VOID EcfmLbLtUtilY1731EnableMep PROTO ((tEcfmLbLtMepInfo *));
PUBLIC VOID EcfmLbLtUtilY1731DisableMep PROTO ((tEcfmLbLtMepInfo *));
PUBLIC VOID EcfmLbLtUtilModuleEnableForAMep PROTO ((tEcfmLbLtMepInfo *));
PUBLIC VOID EcfmLbLtUtilModuleDisableForAMep PROTO ((tEcfmLbLtMepInfo *));
PUBLIC INT4 EcfmClearLbLtContextStats  PROTO ((UINT4, UINT1));
PUBLIC INT4 EcfmGetLbLtContextStats    PROTO ((UINT4, UINT4 *, UINT1));
PUBLIC INT4 EcfmTestLbLtContextStats   PROTO ((UINT4 *, UINT4, UINT4));

PUBLIC INT4 EcfmLbltCreateMemPoolsForLtrTlvInfo PROTO ((INT4 i4LtrCacheSize));
PUBLIC VOID EcfmLbltDeleteMemPoolsForLtrTlvInfo PROTO ((VOID));

#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
/*Global LBLT LocalPort RBTREE*/
INT4
EcfmLbLtLocalPortTableGlobalCmp (tRBElem * pRBElem1,
                                 tRBElem *pRBElem2);
INT4
EcfmAddLbLtLocalPortEntry (tEcfmLbLtPortInfo *pPortEntry);
INT4
EcfmDelLbLtLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId);
tEcfmLbLtPortInfo*
EcfmGetLbLtLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId);
#endif
#endif /* _CFM_LB_PRT_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file cfmlbprt.h                      */
/*-----------------------------------------------------------------------*/
