/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlbapi.h,v 1.14 2016/02/18 08:32:27 siva Exp $
 * 
 * Description: This file contains the Prototypes of the routines
 *              exported by LBLT Task.
 *********************************************************************/
#ifndef _CFM_LBLT_API_H
#define _CFM_LBLT_API_H
/******************************************************************************/
/* Prototypes for Routines exported by LBLT Task */
PUBLIC VOID EcfmLbLtDeleteMepEntry PROTO ((UINT4, UINT4, UINT2, UINT4));
PUBLIC VOID EcfmLbLtConfMepCcmLtmPriority PROTO ((UINT4, UINT4, UINT2, UINT1,
                                                  UINT4 ));
PUBLIC VOID EcfmLbLtConfMepActive PROTO ((UINT4, UINT4, UINT2, BOOL1, UINT4));
PUBLIC VOID EcfmLbLtConfMepRowStatus PROTO ((UINT4, UINT4, UINT2, UINT1, UINT4));
PUBLIC INT4 EcfmLbLtAddMepInStack PROTO ((UINT4, UINT4, UINT2, UINT4));
PUBLIC INT4 EcfmLbLtAddMepInAPort PROTO ((tEcfmCcMepInfo * ));  
PUBLIC INT4 EcfmLbLtAddMepEntry PROTO ((tEcfmCcMepInfo *, UINT4 ));
PUBLIC INT4 EcfmLbLtUpdateMepToMptpMepTbl PROTO ((tEcfmCcMepInfo *, UINT1));
PUBLIC VOID EcfmLbLtNotifySM PROTO ((UINT4, UINT4, UINT2,UINT4,UINT1));
PUBLIC VOID EcfmLbLtConfPortLLCEncapStatus PROTO((UINT4 , UINT2, BOOL1));
PUBLIC BOOL1 EcfmLbLtPortCreated PROTO((UINT2, UINT4));
PUBLIC VOID EcfmLbLtConfOui PROTO ((UINT1 * ));
PUBLIC VOID EcfmLbLtConfTrapCtrl PROTO ((UINT2));
PUBLIC VOID EcfmLbLtSetPortEcfmStatus PROTO ((UINT4, UINT2, UINT1));
PUBLIC INT4 EcfmLbLtAddMipEntry PROTO ((tEcfmCcMipInfo *,UINT4));
PUBLIC VOID EcfmLbLtDeleteMipEntry PROTO ((UINT2 , UINT1 , UINT4, UINT4 ));
PUBLIC VOID EcfmLbLtConfMipRowStatus PROTO ((UINT2 , UINT1 , UINT4 , UINT1,
                                             UINT4));
PUBLIC VOID EcfmLbLtConfMipActive PROTO ((UINT2 , UINT1 , UINT4 , BOOL1, UINT4));
PUBLIC VOID EcfmLbLtConfY1731Defaults PROTO ((UINT4, UINT4, UINT2, UINT4));
PUBLIC VOID EcfmLbLtConfUnawreMepPresent PROTO ((BOOL1, UINT2));
PUBLIC VOID EcfmLbLtConfDropEnable PROTO ((UINT4, BOOL1));
PUBLIC VOID EcfmLbLtConfVlanPriority PROTO ((UINT4, UINT1));
PUBLIC VOID EcfmLbLtConfLCKStatus PROTO ((UINT4, UINT4, UINT2, UINT4, BOOL1));
PUBLIC INT4 EcfmLbLtEnableMipCcmDb PROTO ((UINT4, UINT2));
PUBLIC VOID EcfmLbLtDisableMipCcmDb PROTO ((UINT4));
PUBLIC VOID EcfmLbLtDrainMipCcmDb PROTO ((UINT4));
PUBLIC INT4 EcfmLbLtAddMipCcmDbEntry PROTO ((UINT4, tEcfmCcMipCcmDbInfo *));
PUBLIC VOID EcfmLbLtRemoveMipCcmDbEntry PROTO ((UINT4, tEcfmCcMipCcmDbInfo *));
PUBLIC INT4 EcfmLbLtAddRMepDbEntry PROTO ((UINT4, tEcfmCcRMepDbInfo *));
PUBLIC VOID EcfmLbLtRemoveRMepDbEntry PROTO ((UINT4, tEcfmCcRMepDbInfo *));
PUBLIC VOID EcfmLbLtUpdateDefaultMdEntry PROTO ((UINT4,tEcfmCcDefaultMdTableInfo *));
PUBLIC VOID EcfmLbLtUpdateDefDefaultIdPerm PROTO ((UINT4, UINT1));
PUBLIC INT4 EcfmLbLtAddMaEntry PROTO ((UINT4, tEcfmCcMaInfo *));
PUBLIC VOID EcfmLbLtRemoveMaEntry PROTO ((UINT4, tEcfmCcMaInfo *));
PUBLIC INT4 EcfmLbLtAddMdEntry PROTO ((UINT4, tEcfmCcMdInfo *));
PUBLIC VOID EcfmLbLtRemoveMdEntry PROTO ((UINT4, tEcfmCcMdInfo *));
PUBLIC VOID EcfmLbLtSetTransmitFailOpcode PROTO ((UINT4, UINT2, UINT1));
PUBLIC INT4 EcfmLbLtAddDefMdEntry PROTO ((UINT4 , tEcfmCcDefaultMdTableInfo *));
PUBLIC VOID EcfmLbLtRemoveDefMdEntry PROTO ((UINT4, tEcfmCcDefaultMdTableInfo *));
PUBLIC INT4 EcfmLbLtHandleAddToChannel (UINT2 ,UINT4 );
PUBLIC INT4 EcfmLbLtHandleRemoveFromChannel (UINT2 ,UINT4 );
PUBLIC VOID EcfmLbLtAddVlanEntry PROTO ((UINT4 u4ContextId,
      tEcfmCcVlanInfo * pCcVlanInfo));
PUBLIC VOID EcfmLbLtRemoveVlanEntry PROTO ((UINT4 u4ContextId,
         tEcfmCcVlanInfo * pCcVlanInfo));
#endif /* _CFM_LBLT_API_H */
/*******************************************************************************
 *                   End of file cfmlbltapi.h
 ******************************************************************************/
