/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmminpwr.h,v 1.1 2008/01/17 12:46:58 iss Exp $ 
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _ECFMMINP_H
#define _ECFMMINP_H

INT4 FsMiEcfmHwInit PROTO ((UINT4 u4ContextId));
INT4 FsMiEcfmHwDeInit PROTO ((UINT4 u4ContextId));

#endif
