/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmmlw.h,v 1.9 2011/06/22 09:04:06 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsMIEcfmOui ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsMIEcfmOui ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIEcfmOui ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIEcfmOui ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmContextTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmContextTable  */

INT1
nmhGetFirstIndexFsMIEcfmContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmDefaultMdDefLevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmDefaultMdDefMhfCreation ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmDefaultMdDefIdPermission ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMdTableNextIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmLtrCacheStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrCacheClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrCacheHoldTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrCacheSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMipCcmDbStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMipCcmDbClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMipCcmDbSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMipCcmDbHoldTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMemoryFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmBufferFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmUpCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmDownCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmNoDftCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRdiDftCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMacStatusDftCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRemoteCcmDftCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmErrorCcmDftCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmXconDftCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmCrosscheckDelay ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMipDynamicEvaluationStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmContextName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmTrapControl ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmTrapType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmTraceOption ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmGlobalCcmOffload ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmDefaultMdDefLevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmDefaultMdDefMhfCreation ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmDefaultMdDefIdPermission ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmLtrCacheStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmLtrCacheClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmLtrCacheHoldTime ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmLtrCacheSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMipCcmDbStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMipCcmDbClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMipCcmDbSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMipCcmDbHoldTime ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMemoryFailureCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmBufferFailureCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmUpCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmDownCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmNoDftCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRdiDftCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMacStatusDftCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRemoteCcmDftCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmErrorCcmDftCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmXconDftCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmCrosscheckDelay ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMipDynamicEvaluationStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmTrapControl ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmTraceOption ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmGlobalCcmOffload ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmDefaultMdDefLevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmDefaultMdDefMhfCreation ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmDefaultMdDefIdPermission ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmLtrCacheStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmLtrCacheClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmLtrCacheHoldTime ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmLtrCacheSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMipCcmDbStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMipCcmDbClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMipCcmDbSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMipCcmDbHoldTime ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMemoryFailureCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmBufferFailureCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmUpCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmDownCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmNoDftCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRdiDftCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMacStatusDftCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRemoteCcmDftCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmErrorCcmDftCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmXconDftCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmCrosscheckDelay ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMipDynamicEvaluationStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmTrapControl ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmTraceOption ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmGlobalCcmOffload ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmPortTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmPortTable  */

INT1
nmhGetFirstIndexFsMIEcfmPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmPortLLCEncapStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmPortModuleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmPortTxCfmPduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortTxCcmCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortTxLbmCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortTxLbrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortTxLtmCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortTxLtrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortTxFailedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxCfmPduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxCcmCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxLbmCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxLbrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxLtmCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxLtrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortRxBadCfmPduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortFrwdCfmPduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmPortDsrdCfmPduCount ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmPortLLCEncapStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEcfmPortModuleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEcfmPortTxCfmPduCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortTxCcmCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortTxLbmCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortTxLbrCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortTxLtmCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortTxLtrCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortTxFailedCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxCfmPduCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxCcmCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxLbmCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxLbrCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxLtmCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxLtrCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortRxBadCfmPduCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortFrwdCfmPduCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmPortDsrdCfmPduCount ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmPortLLCEncapStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmPortModuleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmPortTxCfmPduCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortTxCcmCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortTxLbmCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortTxLbrCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortTxLtmCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortTxLtrCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortTxFailedCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxCfmPduCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxCcmCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxLbmCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxLbrCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxLtmCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxLtrCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortRxBadCfmPduCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortFrwdCfmPduCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmPortDsrdCfmPduCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmStackTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmStackTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmStackTable  */

INT1
nmhGetFirstIndexFsMIEcfmStackTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmStackTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmStackMdIndex ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmStackMaIndex ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmStackMepId ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmStackMacAddress ARG_LIST((INT4  , INT4  , INT4  , INT4 ,tMacAddr * ));

/* Proto Validate Index Instance for FsMIEcfmVlanTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmVlanTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmVlanTable  */

INT1
nmhGetFirstIndexFsMIEcfmVlanTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmVlanTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmVlanPrimaryVid ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmVlanRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmVlanPrimaryVid ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIEcfmVlanRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmVlanPrimaryVid ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmVlanRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmDefaultMdTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmDefaultMdTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmDefaultMdTable  */

INT1
nmhGetFirstIndexFsMIEcfmDefaultMdTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmDefaultMdTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmDefaultMdStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmDefaultMdLevel ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmDefaultMdMhfCreation ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmDefaultMdIdPermission ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmDefaultMdLevel ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIEcfmDefaultMdMhfCreation ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIEcfmDefaultMdIdPermission ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmDefaultMdLevel ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmDefaultMdMhfCreation ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmDefaultMdIdPermission ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmDefaultMdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmConfigErrorListTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmConfigErrorListTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmConfigErrorListTable  */

INT1
nmhGetFirstIndexFsMIEcfmConfigErrorListTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmConfigErrorListTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmConfigErrorListErrorType ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIEcfmMdTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMdTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMdTable  */

INT1
nmhGetFirstIndexFsMIEcfmMdTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMdTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMdFormat ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMdName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMdMdLevel ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMdMhfCreation ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMdMhfIdPermission ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMdMaTableNextIndex ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMdRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMdFormat ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMdName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmMdMdLevel ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMdMhfCreation ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMdMhfIdPermission ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMdRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMdFormat ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMdName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmMdMdLevel ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMdMhfCreation ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMdMhfIdPermission ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMdRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmMaTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMaTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMaTable  */

INT1
nmhGetFirstIndexFsMIEcfmMaTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMaTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMaPrimaryVlanId ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMaFormat ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMaName ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMaMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMaIdPermission ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMaCcmInterval ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMaNumberOfVids ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMaRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMaPrimaryVlanId ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMaFormat ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMaName ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmMaMhfCreation ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMaIdPermission ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMaCcmInterval ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMaRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMaPrimaryVlanId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMaFormat ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMaName ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmMaMhfCreation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMaIdPermission ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMaCcmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMaRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmMaMepListTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMaMepListTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMaMepListTable  */

INT1
nmhGetFirstIndexFsMIEcfmMaMepListTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMaMepListTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMaMepListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMaMepListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMaMepListRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMaMepListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmMepTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMepTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMepTable  */

INT1
nmhGetFirstIndexFsMIEcfmMepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMepIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepPrimaryVid ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepActive ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepFngState ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepCcmLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmMepLowPrDef ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepFngAlarmTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepFngResetTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepHighestPrDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDefects ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepErrorCcmLastFailure ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepXconCcmLastFailure ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepNextLbmTransId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepLtmNextSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmMepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmDataTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepTransmitLbmVlanPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmVlanDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLbmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmMepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmResult ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepTransmitLtmEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepCcmOffload ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMepIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepPrimaryVid ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepActive ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepCcmLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepLowPrDef ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepFngAlarmTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepFngResetTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepCcmSequenceErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepCciSentCcms ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepLbrInOutOfOrder ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepLbrBadMsdu ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepUnexpLtrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepLbrOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIEcfmMepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLbmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLbmDataTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmMepTransmitLbmVlanPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLbmVlanDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmMepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIEcfmMepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepTransmitLtmEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepCcmOffload ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMepIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepDirection ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepPrimaryVid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepActive ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepCciEnabled ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepCcmLtmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepLowPrDef ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepFngAlarmTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepFngResetTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepCcmSequenceErrors ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepCciSentCcms ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepLbrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepLbrInOutOfOrder ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepLbrBadMsdu ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepUnexpLtrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepLbrOut ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmDataTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmVlanPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLbmVlanDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmFlags ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmTargetMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmTargetMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmTargetIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmTtl ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepTransmitLtmEgressIdentifier ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmMepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepCcmOffload ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmLtrTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmLtrTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmLtrTable  */

INT1
nmhGetFirstIndexFsMIEcfmLtrTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmLtrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmLtrTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmLtrForwarded ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrTerminalMep ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrLastEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmLtrNextEgressIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmLtrRelay ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmLtrManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIEcfmLtrManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmLtrIngress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrIngressMac ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmLtrIngressPortIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrIngressPortId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmLtrEgress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrEgressMac ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmLtrEgressPortIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmLtrEgressPortId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmLtrOrganizationSpecificTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIEcfmMepDbTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMepDbTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMepDbTable  */

INT1
nmhGetFirstIndexFsMIEcfmMepDbTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMepDbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMepDbRMepState ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDbRMepFailedOkTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepDbMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmMepDbRdi ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDbPortStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDbInterfaceStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDbChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDbChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmMepDbManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIEcfmMepDbManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIEcfmMipTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMipTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMipTable  */

INT1
nmhGetFirstIndexFsMIEcfmMipTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMipTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMipActive ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMipRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMipActive ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMipRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMipActive ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMipRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMipTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmDynMipPreventionTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmDynMipPreventionTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmDynMipPreventionTable  */

INT1
nmhGetFirstIndexFsMIEcfmDynMipPreventionTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmDynMipPreventionTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmDynMipPreventionRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmDynMipPreventionRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmDynMipPreventionRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmDynMipPreventionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmMipCcmDbTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMipCcmDbTable ARG_LIST((UINT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMipCcmDbTable  */

INT1
nmhGetFirstIndexFsMIEcfmMipCcmDbTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMipCcmDbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMipCcmIfIndex ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsMIEcfmRemoteMepDbExTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmRemoteMepDbExTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmRemoteMepDbExTable  */

INT1
nmhGetFirstIndexFsMIEcfmRemoteMepDbExTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmRemoteMepDbExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmRMepCcmSequenceNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRMepPortStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepInterfaceStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepCcmDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepRDIDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmRMepRdi ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepPortStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepInterfaceStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmRMepDbChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIEcfmRMepManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIEcfmRMepManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmRMepPortStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepInterfaceStatusDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepCcmDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepRDIDefect ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIEcfmRMepRdi ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepPortStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepInterfaceStatusTlv ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepChassisIdSubtype ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmRMepDbChassisId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIEcfmRMepManAddressDomain ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMIEcfmRMepManAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmRMepPortStatusDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepInterfaceStatusDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepCcmDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepRDIDefect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIEcfmRMepRdi ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepPortStatusTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepInterfaceStatusTlv ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepChassisIdSubtype ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmRMepDbChassisId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIEcfmRMepManAddressDomain ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMIEcfmRMepManAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmRemoteMepDbExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmLtmTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmLtmTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmLtmTable  */

INT1
nmhGetFirstIndexFsMIEcfmLtmTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmLtmTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIEcfmLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIEcfmMepExTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMepExTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMepExTable  */

INT1
nmhGetFirstIndexFsMIEcfmMepExTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMepExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmXconnRMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmErrorRMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmMepDefectRDICcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDefectMacStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDefectRemoteCcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDefectErrorCcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEcfmMepDefectXconnCcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmXconnRMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmErrorRMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmMepDefectRDICcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepDefectMacStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepDefectRemoteCcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepDefectErrorCcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEcfmMepDefectXconnCcm ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmXconnRMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmErrorRMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmMepDefectRDICcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepDefectMacStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepDefectRemoteCcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepDefectErrorCcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEcfmMepDefectXconnCcm ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMepExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmMdExTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMdExTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMdExTable  */

INT1
nmhGetFirstIndexFsMIEcfmMdExTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMdExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMepArchiveHoldTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMepArchiveHoldTime ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMepArchiveHoldTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMdExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmMaExTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmMaExTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmMaExTable  */

INT1
nmhGetFirstIndexFsMIEcfmMaExTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmMaExTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmMaCrosscheckStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmMaCrosscheckStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmMaCrosscheckStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmMaExTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEcfmStatsTable. */
INT1
nmhValidateIndexInstanceFsMIEcfmStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEcfmStatsTable  */

INT1
nmhGetFirstIndexFsMIEcfmStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEcfmStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEcfmTxCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmTxCcmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmTxLbmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmTxLbrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmTxLtmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmTxLtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmTxFailedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxCcmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxLbmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxLbrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxLtmCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxLtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmRxBadCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmFrwdCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEcfmDsrdCfmPduCount ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEcfmTxCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmTxCcmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmTxLbmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmTxLbrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmTxLtmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmTxLtrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmTxFailedCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxCcmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxLbmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxLbrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxLtmCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxLtrCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmRxBadCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmFrwdCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIEcfmDsrdCfmPduCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEcfmTxCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmTxCcmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmTxLbmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmTxLbrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmTxLtmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmTxLtrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmTxFailedCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxCcmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxLbmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxLbrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxLtmCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxLtrCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmRxBadCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmFrwdCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEcfmDsrdCfmPduCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEcfmStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
