/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiy1lw.h,v 1.17 2017/08/24 12:00:14 siva Exp $
* 
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIY1731ContextTable. */
INT1
nmhValidateIndexInstanceFsMIY1731ContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731ContextTable  */

INT1
nmhGetFirstIndexFsMIY1731ContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731ContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731ContextName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FrameLossBufferClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731FrameDelayBufferClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrCacheClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731ErrorLogClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731FrameLossBufferSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731FrameDelayBufferSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrCacheSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrCacheHoldTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731TrapControl ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731ErrorLogStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731ErrorLogSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731OperStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrCacheStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731FrameLossBufferClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731FrameDelayBufferClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731LbrCacheClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731ErrorLogClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731FrameLossBufferSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731FrameDelayBufferSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731LbrCacheSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731LbrCacheHoldTime ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731TrapControl ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731ErrorLogStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731ErrorLogSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731OperStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731LbrCacheStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731FrameLossBufferClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731FrameDelayBufferClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731LbrCacheClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731ErrorLogClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731FrameLossBufferSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731FrameDelayBufferSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731LbrCacheSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731LbrCacheHoldTime ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731TrapControl ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731ErrorLogStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731ErrorLogSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731OperStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731LbrCacheStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731ContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731MegTable. */
INT1
nmhValidateIndexInstanceFsMIY1731MegTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731MegTable  */

INT1
nmhGetFirstIndexFsMIY1731MegTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731MegTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731MegClientMEGLevel ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MegVlanPriority ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MegDropEnable ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MegRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731MegClientMEGLevel ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MegVlanPriority ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MegDropEnable ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MegRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731MegClientMEGLevel ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MegVlanPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MegDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MegRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731MegTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731MeTable. */
INT1
nmhValidateIndexInstanceFsMIY1731MeTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731MeTable  */

INT1
nmhGetFirstIndexFsMIY1731MeTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731MeTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731MeCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MeCcmApplication ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MeMegIdIcc ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MeMegIdUmc ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MeRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731MeCciEnabled ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MeCcmApplication ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MeMegIdIcc ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MeMegIdUmc ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MeRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731MeCciEnabled ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MeCcmApplication ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MeMegIdIcc ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MeMegIdUmc ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MeRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731MeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731MepTable. */
INT1
nmhValidateIndexInstanceFsMIY1731MepTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731MepTable  */

INT1
nmhGetFirstIndexFsMIY1731MepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731MepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731MepOutOfService ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepRdiCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepRdiPeriod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepCcmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepCcmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepMulticastLbmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLoopbackCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLbmCurrentTransId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmDestType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmIntervalType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmVariableBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmTlvType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmDataPattern ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepTransmitLbmDataPatternSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmTestPatternType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmTestPatternSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepBitErroredLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLtmTimeout ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepMulticastTstRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstPatternType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstVariableBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstPatternSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstDestType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepTransmitTstDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstSeqNumber ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstIntervalType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitTstDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepBitErroredTstIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepValidTstIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTstOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepTransmitLmmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitLmmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTxFCf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepRxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepFarEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmit1DmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmit1DmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitDmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepDmrOptionalFields ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731Mep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepFrameDelayThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAisCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAisCondition ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAisInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAisPeriod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAisPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAisDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAisDestIsMulticast ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAisClientMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepLckDestIsMulticast ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLckClientMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepLckCondition ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLckInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLckPeriod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepLckPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepLckDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLckDelay ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepDefectConditions ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepUnicastCcmMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepRxFCf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731Mep1DmTransInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepTransmitThDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitThDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThPps ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitThDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitThType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThFrameSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitThBurstMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThBurstDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitThBurstType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThTestPatternType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepThVerifiedBps ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepThUnVerifiedBps ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmMode ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLbmOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepTransmitLbmTimeout ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTstCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepTransmitThResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepThVerifiedFrameSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAisOffload ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLbmTTL ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepLbmIcc ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepLbmNodeId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepLbmIfNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepLoopbackStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepLmCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731MepOutOfService ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepRdiCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepRdiPeriod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepCcmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepCcmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepMulticastLbmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLoopbackCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepTransmitLbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmDestType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmIntervalType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmVariableBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmTlvType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmDataPattern ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MepTransmitLbmDataPatternSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmTestPatternType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmTestPatternSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepBitErroredLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MepTransmitLtmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepTransmitLtmTargetMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmTargetIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmTtl ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLtmTimeout ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepMulticastTstRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstPatternType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstVariableBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstPatternSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstDestType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepTransmitTstDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstIntervalType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitTstDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepBitErroredTstIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepValidTstIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTstOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepTransmitLmmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLmmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepFarEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmit1DmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmit1DmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitDmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepDmrOptionalFields ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731Mep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepFrameDelayThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAisCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAisInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAisPeriod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAisPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAisDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAisDestIsMulticast ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAisClientMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepLckDestIsMulticast ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLckClientMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepLckInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLckPeriod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepLckPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepLckDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLckDelay ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepUnicastCcmMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731Mep1DmTransInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepTransmitThDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitThDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThPps ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitThDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitThType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThFrameSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitThBurstMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThBurstDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitThBurstType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitThTestPatternType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmMode ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLbmOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepTransmitLbmTimeout ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepTstCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAisOffload ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLbmTTL ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepLbmIcc ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MepLbmNodeId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepLbmIfNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepLoopbackStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepLmCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731MepOutOfService ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepRdiCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepRdiPeriod ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepCcmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepCcmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepMulticastLbmRecvCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLoopbackCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDestType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmIntervalType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmVariableBytes ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmTlvType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDataPattern ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MepTransmitLbmDataPatternSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmTestPatternType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmTestPatternSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepBitErroredLbrIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmFlags ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MepTransmitLtmTargetMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmTargetMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmTargetIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmTtl ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLtmTimeout ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepMulticastTstRecvCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstPatternType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstVariableBytes ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstPatternSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstDestType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepTransmitTstDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstIntervalType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitTstDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepBitErroredTstIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepValidTstIn ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTstOut ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLmmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepNearEndFrameLossThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepFarEndFrameLossThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmit1DmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmit1DmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepTransmitDmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitDmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepDmrOptionalFields ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731Mep1DmRecvCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepFrameDelayThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAisCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAisInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAisPeriod ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAisPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAisDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAisDestIsMulticast ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAisClientMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepLckDestIsMulticast ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLckClientMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepLckInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLckPeriod ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepLckPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepLckDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLckDelay ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepUnicastCcmMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731Mep1DmTransInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepTransmitThDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThPps ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThFrameSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThBurstMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThBurstDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThBurstType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitThTestPatternType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLbmOut ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepTransmitLbmTimeout ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepTstCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAisOffload ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLbmTTL ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepLbmIcc ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MepLbmNodeId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepLbmIfNum ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepLoopbackStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepLmCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731MepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731ErrorLogTable. */
INT1
nmhValidateIndexInstanceFsMIY1731ErrorLogTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731ErrorLogTable  */

INT1
nmhGetFirstIndexFsMIY1731ErrorLogTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731ErrorLogTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731ErrorLogTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ErrorLogType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731ErrorLogRMepIdentifier ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIY1731LtrTable. */
INT1
nmhValidateIndexInstanceFsMIY1731LtrTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731LtrTable  */

INT1
nmhGetFirstIndexFsMIY1731LtrTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731LtrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731LtrReceiveTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIY1731LbmTable. */
INT1
nmhValidateIndexInstanceFsMIY1731LbmTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731LbmTable  */

INT1
nmhGetFirstIndexFsMIY1731LbmTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731LbmTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731LbmBytesSent ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmTargetMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731LbmUnexptedLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmDuplicatedLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmNumOfResponders ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmDestType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmIcc ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , 
                                         UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731LbmNodeId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmIfNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbmTTL ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIY1731LbrTable. */
INT1
nmhValidateIndexInstanceFsMIY1731LbrTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731LbrTable  */

INT1
nmhGetFirstIndexFsMIY1731LbrTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731LbrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731LbrResponderMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731LbrReceiveTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrErrorType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrDestType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbrDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbrICC ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731LbrNodeId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbrIfNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIY1731LbStatsTable. */
INT1
nmhValidateIndexInstanceFsMIY1731LbStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731LbStatsTable  */

INT1
nmhGetFirstIndexFsMIY1731LbStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731LbStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731LbStatsLbmOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbStatsLbrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbStatsLbrTimeAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbStatsLbrTimeMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbStatsLbrTimeMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731LbStatsTotalResponders ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LbStatsAvgLbrsPerResponder ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIY1731FdTable. */
INT1
nmhValidateIndexInstanceFsMIY1731FdTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731FdTable  */

INT1
nmhGetFirstIndexFsMIY1731FdTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731FdTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731FdTxTimeStampf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdMeasurementTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FdPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731FdIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731FdDelayValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdIFDV ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdFDV ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdMeasurementType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIY1731FdStatsTable. */
INT1
nmhValidateIndexInstanceFsMIY1731FdStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731FdStatsTable  */

INT1
nmhGetFirstIndexFsMIY1731FdStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731FdStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731FdStatsTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FdStatsDmmOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FdStatsDmrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FdStatsDelayAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdStatsFDVAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdStatsIFDVAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdStatsDelayMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731FdStatsDelayMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIY1731FlTable. */
INT1
nmhValidateIndexInstanceFsMIY1731FlTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731FlTable  */

INT1
nmhGetFirstIndexFsMIY1731FlTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731FlTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731FlMeasurementTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731FlIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731FlFarEndLoss ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlNearEndLoss ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlMeasurementTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIY1731FlStatsTable. */
INT1
nmhValidateIndexInstanceFsMIY1731FlStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731FlStatsTable  */

INT1
nmhGetFirstIndexFsMIY1731FlStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731FlStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731FlStatsTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsMessagesOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsMessagesIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsFarEndLossAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsNearEndLossAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsMeasurementType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731FlStatsFarEndLossMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsFarEndLossMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsNearEndLossMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731FlStatsNearEndLossMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIY1731PortTable. */
INT1
nmhValidateIndexInstanceFsMIY1731PortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731PortTable  */

INT1
nmhGetFirstIndexFsMIY1731PortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731PortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731PortAisOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortAisIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortLckOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortLckIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortTstOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortTstIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortLmmOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortLmmIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortLmrOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortLmrIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731Port1DmOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731Port1DmIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortDmmOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortDmmIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortDmrOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortDmrIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortApsOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortApsIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortMccOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortMccIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortVsmOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortVsmIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortVsrOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortVsrIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortExmOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortExmIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortExrOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortExrIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIY1731PortOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIY1731LastTxFailOpcode ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731PortAisOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortAisIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortLckOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortLckIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortTstOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortTstIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortLmmOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortLmmIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortLmrOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortLmrIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731Port1DmOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731Port1DmIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortDmmOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortDmmIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortDmrOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortDmrIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortApsOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortApsIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortMccOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortMccIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortVsmOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortVsmIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortVsrOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortVsrIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortExmOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortExmIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortExrOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortExrIn ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIY1731PortOperStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731PortAisOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortAisIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortLckOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortLckIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortTstOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortTstIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortLmmOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortLmmIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortLmrOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortLmrIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731Port1DmOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731Port1DmIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortDmmOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortDmmIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortDmrOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortDmrIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortApsOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortApsIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortMccOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortMccIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortVsmOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortVsmIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortVsrOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortVsrIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortExmOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortExmIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortExrOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortExrIn ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731PortOperStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731PortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731MplstpExtRemoteMepTable. */
INT1
nmhValidateIndexInstanceFsMIY1731MplstpExtRemoteMepTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731MplstpExtRemoteMepTable  */

INT1
nmhGetFirstIndexFsMIY1731MplstpExtRemoteMepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731MplstpExtRemoteMepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731MplstpExtRMepServicePointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIY1731MplstpExtRMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731MplstpExtRMepServicePointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMIY1731MplstpExtRMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731MplstpExtRMepServicePointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMIY1731MplstpExtRMepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731MplstpExtRemoteMepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731StatsTable. */
INT1
nmhValidateIndexInstanceFsMIY1731StatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731StatsTable  */

INT1
nmhGetFirstIndexFsMIY1731StatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731StatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731AisOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731AisIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LckOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LckIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731TstOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731TstIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LmmOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LmmIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LmrOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731LmrIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY17311DmOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY17311DmIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731DmmOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731DmmIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731DmrOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731DmrIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ApsOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ApsIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MccOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MccIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731VsmOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731VsmIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731VsrOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731VsrIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ExmOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ExmIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ExrOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731ExrIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731TxFailOpcode ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731AisOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731AisIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731LckOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731LckIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731TstOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731TstIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731LmmOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731LmmIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731LmrOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731LmrIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY17311DmOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY17311DmIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731DmmOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731DmmIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731DmrOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731DmrIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731ApsOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731ApsIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MccOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MccIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731VsmOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731VsmIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731VsrOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731VsrIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731ExmOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731ExmIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731ExrOut ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731ExrIn ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731TxFailOpcode ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731AisOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731AisIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731LckOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731LckIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731TstOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731TstIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731LmmOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731LmmIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731LmrOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731LmrIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY17311DmOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY17311DmIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731DmmOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731DmmIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731DmrOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731DmrIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731ApsOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731ApsIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MccOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MccIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731VsmOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731VsmIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731VsrOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731VsrIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731ExmOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731ExmIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731ExrOut ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731ExrIn ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731TxFailOpcode ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731StatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIY1731MepAvailabilityTable. */
INT1
nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIY1731MepAvailabilityTable  */

INT1
nmhGetFirstIndexFsMIY1731MepAvailabilityTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIY1731MepAvailabilityTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIY1731MepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMIY1731MepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIY1731MepAvailabilityPercentage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIY1731MepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIY1731MepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIY1731MepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMIY1731MepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIY1731MepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIY1731MepAvailabilityStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityLowerThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MepAvailabilityUpperThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIY1731MepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityWindowSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMIY1731MepAvailabilityDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilitySchldDownInitTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilitySchldDownEndTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIY1731MepAvailabilityRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIY1731MepAvailabilityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

