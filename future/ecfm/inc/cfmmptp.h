/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmmptp.h,v 1.13 2015/07/23 11:12:09 siva Exp $
 *
 * Description: This file contains the MPLS-TP specific definitions 
 *******************************************************************/

#ifndef _CFMMPTP_H_
#define _CFMMPTP_H_

#include "mplsapi.h"
#include "mpls.h"

#define CFM_Y1731_CHANNEL_TYPE 0x8902

#define CFM_GAL_LABEL 13

/* VCCV Control Channel Types  - Bits 3 to 7: Reserved */
#define CFM_L2VPN_VCCV_CC_RAL       0x40 /* Bit 1: Type 2: MPLS Router Alert Label */
#define CFM_L2VPN_VCCV_CC_TTL_EXP   0x20 /* Bit 2: Type 3: MPLS PW Label with TTL ==1 */

#define ECFM_MPLSTP_SERVICE_PTR_POOLID  gEcfmCcGlobalInfo.ServicePtrPool
#define ECFM_MPLSTP_PATHINFO_POOLID     gEcfmCcGlobalInfo.PathInfoPool
#define ECFM_MPLSTP_INPARAMS_POOLID     gEcfmCcGlobalInfo.InParamsPool
#define ECFM_MPLSTP_OUTPARAMS_POOLID    gEcfmCcGlobalInfo.OutParamsPool
#define ECFM_MPTP_LBLT_PATHINFO_POOLID  gEcfmLbLtGlobalInfo.PathInfoPool
#define ECFM_MPTP_LBLT_INPARAMS_POOLID  gEcfmLbLtGlobalInfo.InParamsPool
#define ECFM_MPTP_LBLT_OUTPARAMS_POOLID gEcfmLbLtGlobalInfo.OutParamsPool

/* Structure containing service pointer */
typedef struct
{
    UINT4 au4ServicePtr[256];
    UINT4 u4OidLength;
}tServicePtr;

/* Structure containing complete MPLS-TP path information
 * fetched from MPLS module */
typedef struct
{
    tTeTnlApiInfo MplsTeTnlApiInfo;
    tNonTeApiInfo MplsNonTeApiInfo;
    tPwApiInfo    MplsPwApiInfo;
    UINT2         u2PathFilled;   /* Bitmap indicating filled paths */
    UINT1         u1PathStatus;
    UINT1         au1Pad[1];
#define CFM_CHECK_PATH_TE_AVAIL    0x1
#define CFM_CHECK_PATH_PW_AVAIL    0x2
#define CFM_CHECK_PATH_NONTE_AVAIL 0x4
}tEcfmMplsTpPathInfo;

/********************************************************/
PUBLIC INT4
EcfmMplsTpCcTxPacket PROTO ((tEcfmBufChainHeader *, tEcfmCcMepInfo *, UINT1));

PUBLIC INT4
EcfmMplsTpLbLtTxPacket PROTO ((tEcfmBufChainHeader *, tEcfmLbLtMepInfo *, UINT1));

PUBLIC INT4
EcfmCcContructOffloadMplsTpTxPkt PROTO 
                  ((tEcfmBufChainHeader *, tEcfmCcMepInfo *, 
                    UINT1 * , UINT4 * ));

PUBLIC INT4
EcfmGetMplsPathInfo PROTO ((UINT4, tEcfmMplsParams *,
                            tEcfmMplsTpPathInfo *));

PUBLIC INT4
EcfmMplsGetPwIndex PROTO ((UINT4 u4PswId, UINT4 *pu4PwIndex));

PUBLIC INT4
EcfmMplsGetPwId PROTO ((UINT4 u4PwIndex, UINT4 *pu4PswId));

PUBLIC INT4
EcfmMplsTpConstructPacket PROTO ((tEcfmMepProcInfo *, tEcfmMplsTpPathInfo *, 
                                  UINT4 *, UINT1 *, tCRU_BUF_CHAIN_HEADER *));

PUBLIC INT4 EcfmMpTpMepMplsPathCmp PROTO ((tEcfmMplsParams *, 
                                           tEcfmMplsParams *));

PUBLIC INT4
EcfmSendPktToMpls PROTO ((UINT4, UINT4, UINT1 *, tCRU_BUF_CHAIN_HEADER *, UINT1));

PUBLIC INT4
EcfmMplsRegCallBack PROTO ((VOID));

PUBLIC INT4
EcfmRegMplsTpPath PROTO ((BOOL1, tEcfmCcMepInfo *));

PUBLIC INT4
EcfmMplsGetBaseServiceOID PROTO ((UINT4, UINT1, tServicePtr *));

PUBLIC INT4
EcfmMplsValidateServiceOID PROTO ((UINT4, UINT1 , tServicePtr *));

PUBLIC INT4
EcfmMplsGetOidFromPathId PROTO ((UINT4, tEcfmMplsParams *,
                                 tServicePtr *));
PUBLIC INT4
EcfmRxProcessMplsHeaders PROTO ((tEcfmBufChainHeader *, UINT4, UINT4,
     tEcfmMplsParams *));

PUBLIC VOID
EcfmRxGetMplsHdr PROTO ((tEcfmBufChainHeader *, tMplsHdr *));

PUBLIC INT4 
EcfmMptpHandleBulkPathStatusInd PROTO ((tEcfmBufChainHeader *, UINT4));

PUBLIC INT4
EcfmGetMplsPktCnt PROTO((tEcfmCcMepInfo *pMepInfo,
                         UINT4 *pu4TxFCf,
                         UINT4 *pu4RxFCf));
PUBLIC INT4
EcfmIncrOutMplsPktCnt PROTO((UINT4 u1MplsPathType, 
                             UINT4 u4PswId, 
                             UINT4 u4ContextId));

PUBLIC INT4
EcfmUtilGetMcLagStatus PROTO ((VOID));

PUBLIC INT4
EcfmUtilCheckIfIcclInterface PROTO ((UINT4 u4Index));

PUBLIC INT4
EcfmUtilCheckIfMcLagInterface PROTO ((UINT4 u4Index));

/********************************************************/
#endif /* _CFMMPTP_H_ */

