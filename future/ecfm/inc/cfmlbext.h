/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmlbext.h,v 1.8 2012/01/20 13:18:35 siva Exp $
 * 
 * Description: This file contains extern variables used for ECFM
 *              modules LBLT Task.
 *********************************************************************/


#ifndef _CFM_LBLT_EXTN_H
#define _CFM_LBLT_EXTN_H
extern tEcfmLbLtGlobalInfo gEcfmLbLtGlobalInfo;
extern tEcfmLbLtContextInfo *gpEcfmLbLtContextInfo;
extern UINT1 gu1EcfmLbLtInitialised;
extern tEcfmLbLtCliEvInfo gaEcfmLbLtCliEventInfo[ECFM_CLI_MAX_SESSIONS+1];
extern tEcfmLbLtCliEvInfo *gapEcfmLbLtCliEventInfo[ECFM_CLI_MAX_SESSIONS+1];
extern tEcfmLbLtMepInfo   *gpEcfmLbLtMepNode;
extern UINT1               gau1EcfmLbLtPdu [ECFM_MAX_JUMBO_PDU_SIZE];
extern UINT1               gau1EcfmLbltTstPdu [ECFM_MAX_JUMBO_PDU_SIZE];
#endif /* _CFMLBLTEXTN_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmlbextn.h                      */
/*-----------------------------------------------------------------------*/
