/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmcctdf.h,v 1.71 2017/08/24 12:00:14 siva Exp $
 * 
 * Description: This file contains data structures defined for CC Task.
 *********************************************************************/
#ifndef _CFM_CC_TDF_H
#define _CFM_CC_TDF_H

/****************************************************************************/
/*Used for storing information received in CCM*/
typedef struct EcfmCcRxCcmPduInfo
{
   tEcfmSenderId    RecvdSenderId;     /* Sender Id*/
   tEcfmOrgSpecific RecvdOrgSpecifc;      /* Organization Specific Data*/
   UINT4            u4SeqNumber;          /* Sequence no. in the  received CCM*/
   UINT4            u4TxFCf;          /* Carries the value of the counter of
                                       * in-profile data frames transmitted
                                       * by the MEP towards its peer MEP, at
                                       * the time of CCM frame transmission.
                                       */
   UINT4            u4RxFCb;          /* Carries the value of the counter of
                                       * in-profile data frames received by the
                                       * MEP from its peer MEP, at the time of
                                       * receiving the last CCM frame from that
                                       * peer MEP.
                                       */
   UINT4            u4TxFCb;          /* Carries the value of the TxFCf field in
                                       * the last CCM frame received by the MEP
                                       * from its peer MEP.
                                       */
   UINT2            u2MepId;              /* MepIdentifier field of received 
                                           * CCM
                                           */

   UINT1            u1RecvdPortStatus;     /* PortStatus received in CCM*/
   UINT1            u1RecvdIfStatus;       /* InterfaceStatus received in CCM
                                            */
   UINT1            au1MAID[ECFM_MAID_FIELD_SIZE]; /* MAID received in CCM*/
} tEcfmCcRxCcmPduInfo;
/****************************************************************************/
/* CC specific information maintained per Mep*/
typedef struct EcfmCcMepCcInfo
{
   tTmrBlk          CciWhileTimer; /* TImer used to transmit CCMs after its 
                                    * expiration. Default is 1 sec.
                                    */
   tTmrBlk          ErrCCMWhileTimer;  /* Timer for timing out invalid CCMs*/
   tTmrBlk          XconCCMWhileTimer; /* Timer for timing out Cross 
                                        * Connected CCMs
                                        */
   tTmrBlk          RdiPeriodTimer;  /* Timer for timing out invalid CCMs*/

   tEcfmString      ErrorCcmLastFailure; /* Last received CCM, that 
                                              * triggered DefErrorCCM
                                              */
   tEcfmString      XconCcmLastFailure;  /* Last received CCM, that 
                                              * triggered DefXconCCM
                                              */
#ifdef L2RED_WANTED
   tEcfmSysTime     XConTimeStamp;   /*Expiry timestamp of Cross connect timer*/
   tEcfmSysTime     ErrTimeStamp;    /*Expiry timestamp of Error PDU timer*/
   tEcfmSysTime     CciTimeStamp;    /*Expiry timestamp of CCI timer*/
   tEcfmSysTime     RdiTimeStamp;    /*Expiry timestamp of RDI timer*/
#endif

   UINT4            u4CciSentCcms;   /* Counter to maintain the number of the 
                                      * CCMs transmitted by this MEP
                                      */
   UINT4            u4CciCcmCount;    /*Counter exclusively added to maintain the number
                                        of CCMs transmitted in statistics clearing and display*/
   UINT4            u4CcmSeqErrors;  /* Total number of Out-of-sequence CCMs 
                                      * received.
                                      */
   UINT4            u4RdiCapPeriod;   /* Time period for which
                                       * RDI transmission Capabilty
                                       * is enabled.
                                       */
   UINT2            u2ErrorRMepId;   /* MEP-Id of the CCM which caused the 
                                      * Error CCM Defect*/
   UINT2            u2XconnRMepId;   /* MEP-Id of the CCM which caused the 
                                      * Xconn CCM Defect*/
#ifdef NPAPI_WANTED
   UINT2            u2InvalidPrioRmepId; /* MEP-Id of the CCM which caused the
                                          * invalid priority defect. */
#endif
   BOOL1            b1UnExpectedMepDefect;/* MEPID received is not present in configured
                                           * list of MEPIDs in MEG, or Received MEPID is same as
                                           * the MEPID of the receiving MEP. 
                                           */
   BOOL1            b1UnExpectedPeriodDefect;/* It is set if the period in received
                                              * CCM is not same as receiving MEP
                                              * CC Interval.
                                              */
   BOOL1            b1MisMergeDefect;/* It is set if the MEGID in received
                                      * CCM is not same as receiving MEP
                                      * MEGID.
                                      */
   BOOL1            b1UnExpectedLevelDefect;  /* It is set if the MEG Level of the
                                               * received CCM is less then receiving
                                               * MEP's MEG Level 
                                               */
   BOOL1            b1LocalLinkFailure;       /* It is set when there is a link
                                               *  failure
                                               */
   BOOL1            b1InternalHwFailure;      /* It is set when there is a H/w
                                               * Failure 
                                               */
   BOOL1            b1InternalSwFailure;      /* It is set when there is a S/w
                                               * Failure 
                                               */
   BOOL1            b1CcmDropEligible;  /* The Drop eligibility bit value to be used in
                                         * VLAN tag.
                                         */
   BOOL1            b1MacStatusChanged;  /* Denotes there is a change in the 
                                          * MAC Status i.e. either PortStatus 
                                          * TLV or the InterfaceStatus TLV is 
                                          * changed and the CCIWhile timer value 
                                          * is less than 10 seconds. This 
                                          * variable is set when the above 
                                          * listed conditions are true.
                                          */
   BOOL1            b1ErrorCcmDefect;   /* True if Error CCM is Received 
                                         * Set by Remote Error SM
                                         */
   BOOL1            b1XconCcmDefect;    /* True if Xconn CCM is Received 
                                         * Set by Xconn SM
                                         */
#ifdef NPAPI_WANTED
   BOOL1            b1InvalidPrioDefect; /* Invalid priority detected by VOE
                                          * as per G.8021. This will be reported
                                          * by VOE through interrupt Q.
                                          */
   BOOL1            b1CcmZeroInterval; /* Defect - CCM received with zero
                                        * interval */

#endif
   BOOL1            b1CciEnabled;    /* For transmitting CCMs this variable 
                                      * needs to be SET. 
                                      */
   BOOL1            b1UnicastCcmEnabled; /* To check CCM unicast enabled */
   UINT1            u1CcmPriority;   /* The 3-bit value to be included in Vlan 
                                      * tag for Ethernet MEPs or in EXP bits in 
                                      * the MPLS header for MPLS-TP MEPs.
                                      */ 
   UINT1            u1RdiCapability; /* RDI Capbility*/
   UINT1            u1PortState;     /* Inidcates the Port Status on which the 
                                      * MEP is configured.
                                      */
   UINT1            u1IfStatus;      /* Inidcates the Interface Status on which 
                                      * the MEP is configured.
                                      */
   UINT1            u1CciState;      /* Current State of the Continuity Check  
                                      * Initiator State Machine
                                      */
   UINT1            u1MepXconState;  /* Current State of MEP Cross Connect 
                                      * state machine
                                      */
   UINT1            u1RMepErrState;  /* Current State of Remote MEP Error state 
                                      * machine
                                      */
   tEcfmMacAddr     UnicastCcmMacAddr;   /* Unicast CCM Destination MacAddress */
   UINT1            au1Pad[2];
} tEcfmCcMepCcInfo;

/****************************************************************************/
/* Ais specific information maintained per Mep*/
typedef struct EcfmCcAisInfo
{
   tTmrBlk             AisInterval;      /* Timer used to transmit AIS after 
                                          * its expiration 
                                          */
   tTmrBlk             AisPeriod;        /* Timer used to stop sending AIS after
                                          * its expiration 
                                          */
   tTmrBlk             AisRxWhile;         /* Timer used to clear the AIS condition
                                            */
   
   tEcfmString         SavedAisPdu;        /* To maintain the pointer to the Buffer
                                            */
#ifdef L2RED_WANTED
   tEcfmSysTime            AisPrdTimeStamp;       /* Expiry Timestamp of AIS period Timer */
   tEcfmSysTime            AisIntervalTimeStamp;      /* Expiry Timestamp of AIS Rx While Timer */
   UINT4                   u4AisRcvdRxWhileValue;      /* Interval received in AIS PDU*/
#endif

   UINT4               u4AisPeriod;                /* ETH-AIS transmission period 
                                                    * (time for which AIS capability is enabled)
                                                    */
   UINT1               u1AisPriority;              /* Denotes the priority value  for AIS
                                                    *  frame transmitted by the MEP 
                                                    */
   UINT1               u1AisInterval;              /* Timer interval between two consecutive 
                                                    * AIS transmission
                                                    ECFM_AIS_INTERVAL_ONE_SEC =1
                                                    ECFM_AIS_INTERVAL_ONE_MIN=2 
                                                    */
   UINT1               u1AisCapability;            /* Denoted the AIS capability on MEP
                                                    */
   BOOL1               b1AisDropEnable;            /* Denotes Drop eligibility value to be
                                                    * use in VLAN tag
                                                    */
   BOOL1               b1AisCondition;             /* Denotes the AIS condition on MEP */
   BOOL1               b1AisDestIsMulticast;       /* Denotes whether the AIS frame should be 
                                                    * unicast or multicast 
                                                    */
   BOOL1               b1AisTsmting;
   tEcfmMacAddr        AisClientUnicastMacAddr;    /* Denotes the Unicast destination 
                                                    * MAC address (Client MEP)
                                                    */
   UINT1               au1Pad[3];                  /* padding to keep the structure 4 byte aligned */
}tEcfmCcAisInfo;
/****************************************************************************/
/* Lck specific information maintained per Mep*/
typedef struct EcfmCcLckInfo
{
   tTmrBlk            LckInterval;      /* Timer used to transmit LCK after 
                                         * its expiration 
                                         */
   tTmrBlk            LckPeriod;        /* Timer used to stop sending LCK 
                                         * after its expiration
                                         */
   tTmrBlk            LckRxWhile;       /*Timer used to clear the LCK condition 
                                         */
   tTmrBlk            LckDelay;         /*Timer used to stop Data packets after
                                         * its expiration 
                                         */
   tEcfmString         SavedLckPdu;        /* To maintain the pointer to the Buffer
                                            */
#ifdef L2RED_WANTED
   tEcfmSysTime     LckPrdTimeStamp;      /*Expiry timestamp of Lck Period timer*/
   tEcfmSysTime     LckIntervalTimeStamp; /*Expiry timestamp of Lck Interval timer*/
   UINT4            u4LckRcvdRxWhileValue;/*Receive while value received in LCK PDU*/
#endif

   UINT4               u4LckPeriod;                /* Denotes the time for which LCK messages 
                                                    * are to be transmitted inf MEP is in
                                                    * LCK contdition
                                                    */
   UINT1               u1LckPriority;              /* Denotes priority value for LCK frame 
                                                    * transmitted by the MEP 
                                                    */
   UINT1               u1LckDelay;                /*When a MEP is set as locked (for out-of-service operations),
                                                   * data traffic through that MEP will be blocked after this delay.
                                                   */
   UINT1               u1LckInterval;              /*  Timer interval between two consecutive  
                                                    *  LCK transmission
                                                    ECFM_LCK_INTERVAL_ONE_SEC =1
                                                    ECFM_LCK_INTERVAL_ONE_MIN=2 
                                                    */

   BOOL1               b1OutOfService;             /* Incicates the administrative service that is being or to be
                                                    * performed.
                                                    * true indicates operation needs to be perfomed Out-of-service
                                                    * and false indicates operation needs to be performed In-service.
                                                    */
   BOOL1               b1LckDropEnable;            /* Denotes Drop eligibility of LCK Pdu 
                                                    */
   BOOL1               b1LckCondition;             /* Denotes LCK condition on MEP 
                                                    */
   BOOL1               b1LckDestIsMulticast;       /* Denotes whether the LCK frame should
                                                    * be unicast of Multicast */
   BOOL1               b1LckDelayExp;              /* Denotes whether the LCK delay timer
                                                    * is expired or not */
   tEcfmMacAddr        LckClientUnicastMacAddr;    /* Denotes the unicast destination of
                                                    * MAC address (client MEP)
                                                    */
   UINT1               au1Pad[2];                  /* padding to keep the structure 4 byte aligned */
} tEcfmCcLckInfo;

/****************************************************************************/
/* Fault Notification Generator specific information maintained per Mep*/
typedef struct EcfmCcFngInfo
{
   tTmrBlk          FngAlarmWhileTimer; /* Timer used by Fault Notification 
                                         * Generator state machine to wait for 
                                         * defects to stabilize.
                                         */
   tTmrBlk          FngResetWhileTimer; /* Timer used by Fault Notification 
                                         * Generator state machine to wait for 
                                         * defects to disappear.
                                         */
#ifdef L2RED_WANTED
   tEcfmSysTime      FngWhileTimeStamp; /* Expiry timestamp of Fng Alarm Timer */
   tEcfmSysTime      FngRstWhileTimeStamp; /* Expiry Timestamp of Fng Reset Timer*/

#endif
   UINT2            u2FngAlarmTime;  /* Time for which defects must be present 
                                      * before a fault alarm is raised. Its 
                                      * default value is 250.
                                      */
   UINT2            u2FngResetTime;  /* Time for which no defect is raised 
                                      * before raising another fault alarm. Its 
                                      * default value is 1000.
                                      */
   BOOL1            b1SomeRMepCcmDefect; /* True is any Remote MEP is indicating
                                          * CCM Defect
                                          */
   BOOL1            b1SomeMacStatusDefect; /* Mac Status Defect*/
   BOOL1            b1SomeRdiDefect;       /* RDI Defect by some Remote MEP*/
   UINT1            u1HighestDefect;     /* Identifies highest priority defects. 
                                          * Highest priority defect that has 
                                          * been present since MEPs FNG state 
                                          * machine was last in FNG_RESET state.
                                          */
   UINT1            u1HighestDefectPri;  /* Identiies the Priority of the
                                          * highest defect*/
   UINT1            u1FngPriority;       /* Identifies the priority of the last
                                          * reported defect
                                          */
   UINT1            u1FngDefect;         /* Identifies the last reported defect
                                          */
   UINT1            u1FngState;      /* Current state of MEP Fault Notification 
                                      * Generator
                                      */
} tEcfmCcFngInfo;

/****************************************************************************/
/* Node info for Md Table */
typedef struct EcfmCcMdInfo
{
   tTmrBlk          MepArchiveHoldTimer;
   tEcfmRBNodeEmbd  MdTableGlobalNode; /* Embedded RBTree node for MD table 
                                        * indexed by MDIndex in GlobalInfo 
                                        * structure.
                                        */
   tEcfmRBTree      MaTable;           /* RB Tree of type tEcfmCcMaInfo to 
                                        * maintain the MAs in this 
                                        * MD.
                                        */
   UINT4            u4MaTableNextIndex;/* Used to find next available index for 
                                        * MA row creation in this MD
                                        */
   UINT4            u4MdIndex;         /* Index of the MD Table*/
   UINT4            u4ContextId;       /* Context Identifier*/
   UINT1            au1Name[ECFM_MD_NAME_ARRAY_SIZE]; /* Maintenance Domain 
                                                       * Name
                                                       */  
   UINT2            u2MepArchiveHoldTime; /* MEP Archive Hold Time*/
   BOOL1            b1CfmDropEligible; /* Drop Eligible parameter to be applied 
                                        * to all the CFM Packets
                                        */
   UINT1            u1Level;           /* MD Level, default value is 0*/
   UINT1            u1NameLength;      /* MD name Length */
   INT1             i1ClientLevel;    /* MD Level of immediate higher layer*/
   UINT1            u1CfmVlanPriority; /* Vlan Priority to be applied to all 
                                        * the CFM Packtes
                                        */
   UINT1            u1NameFormat;      /* Format of the MD Name, Used as field 
                                        * in CCM PDU, Default value is 
                                        * Character string
                                        */
   UINT1            u1RowStatus;       /* Row status of this MD row created. */
   UINT1            u1MhfCreation;     /* Indicates whether management entity 
                                        * can create MHF  for this MD
                                        */
   UINT1            u1IdPermission;    /* Indicates if anything is to be 
                                        * included in sender ID TLV transmitted 
                                        * by the MPs configured in this MD.
                                        */
   UINT1           u1Pad;
} tEcfmCcMdInfo;
/****************************************************************************/
/*Node info for Ma Table*/
typedef struct EcfmCcMaInfo
{
   tEcfmRBNodeEmbd  MaTableMdNode ;    /* DLL Node for MA Table in   MD Table*/
   tEcfmRBNodeEmbd  MaTableGlobalNode; /* Ma Table node for MA tree in 
                                        * GlobalInfo, indexed by MdIndex, 
                                        * MaIndex
                                        */
   tEcfmDll         MepTable;          /* Doubly Linklist of type tEcfmCcMepInfo 
                                        * of MEPs that are  associated with this 
                                        * MA.
                                        */
   tEcfmCcMdInfo    *pMdInfo;          /* Back Pointer to the MD Table Entry 
                                        * used to get the MD with which this MA 
                                        * is associated.
                                        */
   UINT4            u4MdIndex;         /* MD index of the associated MD*/
   UINT4            u4MaIndex;         /* MA index of the MA*/
   UINT4            u4TransId;         /* Transaction Id for Performance
                                        * Monitoring
                                        */
 
   UINT4            u4SeqNum;          /* Seq Num for LM */
   UINT4            u4PrimaryVidIsid;  /* Primary VID assigned to this MA.*/
   UINT4            u4EcfmMelFP;       /* FP entry ID of packets matching Ethertype
             * 8902 and ME Level*/
   INT4             i4PrimarySelType;  /* Primary Selector Type */

   UINT2            u2NumberOfVids;    /* Number of VLAN Ids associated with 
                                        * this MA
                                        */
   BOOL1            b1CciEnabled;     /* This Variable controls the CCM transmission 
                                       * by all the MEPs in this MA.
                                       */
   UINT1            u1SelectorType;
   UINT1            u1CcRole;          /* CCM Application Can be one of
                                        * FM/PM/PS
                                        */
   UINT1            u1NameFormat;      /* Format of the MA Name, sent in the CCM 
                                        * Message, default vaue is Character 
                                        * string
                                        */
   UINT1            u1NameLength;      /* MA name Length */
   UINT1            u1IccCodeLength;   /* ICC code Length */
   UINT1            u1UmcCodeLength;   /* UMC code Length */
   UINT1            u1CcmInterval;     /* Interval for all MEPs in a MA for 
                                        * transmitting CCMs
                                        */
   UINT1            u1MhfCreation;     /* Indicates whether management entity 
                                        * can create MHF for this MA.
                                        */
   UINT1            u1IdPermission;    /* Indicates if anything is to be 
                                        * included in sender ID TLV transmitted 
                                        * by the MP configured in this MA.
                                        */
   UINT1            u1RowStatus;       /* Row status of this table*/
   UINT1            u1CrossCheckStatus; /* Cross Check Enabled/Disabled*/
   UINT1            au1Name[ECFM_MA_NAME_ARRAY_SIZE]; /* 21 byte array denoting 
                                                       * the MA Name.
                                                       */
   UINT1            au1IccCode[ECFM_CARRIER_CODE_ARRAY_SIZE]; /* ITU Carrier Code 
                                                               */
   UINT1            au1UmcCode[ECFM_UMC_CODE_ARRAY_SIZE]; /* Unique MEG-ID Code 
                                                           */
   UINT1            au1HwMaHandler [ECFM_HW_MA_HANDLER_SIZE];
   UINT1            u1CompRowStatus;    /* Row status of Component table*/
   UINT1            au1Pad[3];
} tEcfmCcMaInfo;
/****************************************************************************/
/* Information regarding each port */
typedef struct EcfmCcPortInfo
{
   tEcfmRBTree      StackInfoTree;     /*RBTree for tEcfmCcStackInfo */
   tEcfmRBNodeEmbd  PortTableGlobalNode; /* RB Node for port table*/
#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
   tEcfmRBNodeEmbd  LocalPortTableNode; /* RB Node for port table*/
#endif
   UINT4            u4IfIndex;         /* Physical Interface index of the Port*/
   UINT4            u4ContextId;       /* Context Identifier*/
   UINT4            u4TxCfmPduCount;   /* Number of CFM-PDU tranmitted CC Task
                                        */
   UINT4            u4TxCcmCount;      /* Number of CCMs transmitted on CC Task
                                        */
   UINT4            u4TxFailedCount;   /* Number of failed transmissions on CC
                                        * Task
                                        */
   UINT4            u4RxCfmPduCount;   /* Number of CFM-PDUs received on CC Task
                                        */
   UINT4            u4RxCcmCount;      /* Number of CCMs received on CC Task
                                        */
   UINT4            u4RxBadCfmPduCount;/* Number of bad CFM-PDUs received on CC
                                        * Task
                                        */
   UINT4            u4FrwdCfmPduCount; /* Number of CFM-PDUs forwarded 
                                        */
   UINT4            u4DsrdCfmPduCount; /* Number of CFM-PDus discarded*/
   UINT4            u4TxLmmPduCount;      /* Number of LMM PDUs sent */
   UINT4            u4TxLmrPduCount;      /* Number of LMR PDUs sent */
   UINT4            u4RxLmmPduCount;      /* Number of LMM PDUs received */
   UINT4            u4RxLmrPduCount;      /* Number of LMR PDUs received */
   UINT4            u4TxAisCount;      /* Number of AIS-PDus Transmitted
                                        */
   UINT4            u4RxAisCount;      /* Number of AIS-PDus Received
                                        */
   UINT4            u4TxLckCount;      /* Number of LCK-PDus Transmitted
                                        */
   UINT4            u4RxLckCount;      /* Number of LCK-PDus Received
                                        */
   UINT4            u4PhyPortNum;      /* Associated Physical port number for 
                         * SISP logical interfaces 
                                        */ 
   UINT2            u2PortNum;         /* Logical Port Number*/
   UINT2            u2ChannelPortNum;  /* logical port number for port channel 
                                        * if applicable
                                        */
#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
   UINT2            u2LocalPortMask;    /*Mask for specifying Port Property
                                         * ECFM_PORT_PROPERTY_DEMUX
                                         * Other Mask are reserved for future
                                         */
#endif
   BOOL1            b1LLCEncapStatus;  /* If true, then LLC SNAP header is to 
                                        * added with the PDU transmitted by the 
                                        * MPs configured on this port.
                                        */
   UINT1            u1IfOperStatus;    /* Operational Status of the Port 
                                        * (Up/Down)
                                        */
   UINT1            u1PortState;       /* Spanning tree/VLAN state of the port
                                          (Blocked/UP) */
   UINT1            u1IfType;          /* Identifies the Port Type i.e either 
                                        * Ethernet port or Aggregated Port.
                                        */
   UINT1   u1PortType;
   UINT1            u1PortEcfmStatus;  /* To enable/disable the ECFM module for 
                                        * this port only.
                                        */
   UINT1            u1PortY1731Status;  /* To enable/disable the Y.1731
                                         *  functionality for 
                                         * this port only.
                                         */
   BOOL1            b1UnawareMepPresent;/* boolean to indicate whether a unware MEP is 
                                         * present on the port
                                         */
   UINT1            u1PortIdSubType;  /* Port-Indentifier type*/
   UINT1            au1PortId[ECFM_MAX_PORTID_LEN]; /* Port-Identifier*/
#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
   UINT1            au1pad[2];
#endif
} tEcfmCcPortInfo;

typedef struct EcfmCcLmInfo
{
   tTmrBlk                  LmmInitWhileTimer; /* Timer node for LMM While Timer */
   tTmrBlk                  LmInitDeadlineTimer;/* Timer node for LM Deadline Timer */
#ifdef L2RED_WANTED
   UINT4                    u4RemainingDeadlineTimer; /* Remaining Timer Value
                                                       * for Deadline Timer */
   UINT4                    u4RemainingInitWhileTimer;/* Remaining Value for
                                                       * Init While Timer */
#endif
   UINT4                    u4PreTxFCf;/* Value of TxFCf received in the previous LM frame */
   UINT4                    u4PreRxFCf;/* Value of RxFCf received in the previous LM frame */
   UINT4                    u4PreTxFCb;/* Value of TxFCb received in the previous LM frame */
   UINT4                    u4PreRxFCl;/* Value of RxFCl at the time of reception 
                                        * of last LMR frame 
                                        */
   UINT4                    u4PreRxFCb; /* Value of RxFCb received in last LM frame*/
   UINT4                    u4RxFCf;/* Value of RcFCl at the time of LMM frame transmission */
   UINT4                    u4NearEndFrmLossThreshold;/* Threshold value for Near end loss */
   UINT4                    u4FarEndFrmLossThreshold;/* Threshold value for Far End Loss */
   UINT4                    u4TxLmmDeadline; /* Specify  a  timeout, in seconds, before 
                                              * Loss Measurement operation times out.
                                              * LM exits, if configured number of messages are sent 
                                              * before timeout
                                              */
   UINT4                    u4MplsTxCount;  /* MPLS Pdu transmitted count */
   UINT4                    u4MplsRxCount;  /* MPLS Pdu received count */
   UINT2                    u2TxLmmInterval;/* Interval between 2 LMMs */
   UINT2                    u2TxLmDestMepId;/* MEP ID of the MEP with which LM is taking place */
   UINT2                    u2TxLmmMessages;/* Max number of LMMs that can be sent */
   UINT2                    u2NoOfLmrIn;    /* Number of LMRs received */
   UINT1                    u1TxLmmStatus;/* Transmit status of LMMs */
   UINT1                    u1TxLmmPriority;/* Priority of LMM frames */
   UINT1                    u1LossMeasurementType; /* Type of Loss measurement*/
   BOOL1                    b1LmmDropEnable;/* Drop eligibility of LMM frames */
   BOOL1                    b1ResultOk;/* Indicates the result of the operation:
                                        * True - The LM message will be(or has been) sent 
                                        * False - The LM Message will not be sent
                                        */
   BOOL1                    b1TxLmIsDestMepId;/* Indicates Destination MP is
                                               * identified through MEPID
                                               */
   BOOL1                    b1TxLmByAvlbility;/* Indicates LM is initiated
                                               * by Availabilkity Measurement
                                               */
   tEcfmMacAddr             TxLmmDestMacAddr;/* Target MAC address with which the LM 
                                              * will take place
                                              */
   UINT1                    u1LmCapability;   /*capability of MEP to initiate
                                             and process Y.1731 LMM/LMR frames*/
   UINT1                    auPad[2];
}tEcfmCcLmInfo;

typedef struct EcfmCcAvlbltyStaticInfo 
{
    UINT4 u4CurWinAvailableCnt;
    UINT4 u4AvailabilityIndicator;
    UINT4 u4PreAvailabilityIndicator;
    UINT4 u4SchdldDownInitWindow;
    UINT4 u4SchdldDownEndWindow;
    UINT4 u4SchdldDownWindowCnt;
    UINT4 u4CurWinIntervalCnt;
    UINT4 u4WindowCount;
}tEcfmCcAvlbltyStaticInfo;

typedef struct EcfmCcAvlbltySlideInfo
{
    UINT4 u4WindowContinuity;
    UINT4 u4CurIntCount;
    UINT4 u4SchdldDownInitIntCnt;
    UINT4 u4SchdldDownEndIntCnt;
    UINT4 u4SchdldDownIntTotal;
    UINT4 u4AvlbltyCount;
    BOOL1 b1AvlbltyState;
    BOOL1 b1IntAvlbltyState;
    BOOL1 b1PreIntAvlbltyState;
    UINT1 u1Pad;
}tEcfmCcAvlbltySlideInfo;

typedef struct EcfmCcAvlbltyInfo
{

   union
   {
       tEcfmCcAvlbltyStaticInfo  StaticInfo;  /* Static Type Info */
       tEcfmCcAvlbltySlideInfo   SlideInfo;   /* Sliding Type Info */
   }uAvlbltyOperInfo;                         /* This union contains the 
                                               * structures of Static and 
                                               * Sliding Algorithm related
                                               * Data
                                               */
   FLT4                     f4AvlbltyPercentage; /* Availability Percentage 
                                                 */
   FLT4                     f4FrameLossUpperThreshold;/* Upper Threshold value for 
                                                       * Frame Loss Ratio 
                                                       */
   FLT4                     f4FrameLossLowerThreshold;/* Lower Threshold value for 
                                                       * Frame Loss Ratio  
                                                       */
   FLT4                     f4FrameLossThreshold;/* Normalized Threshold used in algorithms 
                                                  */
   UINT4                    u4TxAvlbltyDeadline; /* Specify  a  timeout, in seconds, before 
                                                 * Availability Measurement operation times out.
                                                 */
   UINT4                    u4TxAvlbltyWindowSize; /* Availability Measurement 
                                                    * Window Size.
                                                    */
   UINT4                    u4TxAvlbltySchldDownInitTime; /* the time at which  
                                                           * schduled down time begins
                                                           */
   UINT4                    u4TxAvlbltySchldDownEndTime; /* the time at which  
                                                           * schduled down time ends
                                                           */
#if defined (Y1564_WANTED) && defined (NPAPI_WANTED)
   UINT4                    u4UnAvailCnt; /* Unavailability Count */
#endif
   UINT2                    u2TxAvlbltyInterval; /* Interval between 2 LMMs 
                                                 */
   UINT2                    u2TxAvlbltyDestMepId;/* MEP ID of the MEP with which 
                                                 * Availability Measurement
                                                 * is taking place 
                                                 */
   UINT1                    u1TxAvlbltyStatus;/* Operational status of 
                                              * Availability Measurement 
                                              */
   UINT1                    u1TxAvlbltyPriority;/* Priority of LMM frames 
                                                */
   UINT1                    u1AvlbltyType; /* Type of Availability measurement
                                            */
   UINT1                    u1RowStatus;  /* Row status of row created for this MEP 
                                           * in Mep Availability table
                                           */
   BOOL1                    b1AvlbltyDropEnable;/* Drop eligibility of LMM frames 
                                                */
   BOOL1                    b1ResultOk;/* Indicates the result of the operation:
                                        * True - Availability initiated successfully 
                                        * False -Availability failed in transmitting LMM
                                        */
   BOOL1                    b1TxAvlbltyIsDestMepId;/* Indicates Destination MP is
                                                   * identified through MEPID
                                                   */
   BOOL1                    bTxAvlbltyModestType;/* Area Between Upper and Lower threshold
                                                   * True  - Considered as Available 
                                                   * False -  Considered as Unavailable
                                                   */
   tEcfmMacAddr             TxAvlbltyDestMacAddr;/* Target MAC address with which the LM 
                                                 * will take place
                                                 */
   UINT1                    au1Pad[2];
}tEcfmCcAvlbltyInfo;




typedef struct EcfmCcFrmLossBuff 
{
   tEcfmRBNodeEmbd          FrmLossGlobalNode;/* Frame Loss Tree Node for 
                                               * table in context info
                                               */
   tEcfmSysTime             RxTimeStamp;/* Time at which CCM/LMR is received*/
   UINT4                    u4TransId;/* Transaction ID for LM */
   UINT4                    u4MdIndex;/* MD Index of MEP. Also used as index
                                       * for Frame Loss Buffer Table
                                       */
   UINT4                    u4MaIndex;/* MA Index of MEP. Also used as the 
                                       * index of Frame Loss Buffer table
                                       */
   UINT4                    u4MeasurementInterval;/* Duration in which LM took place.
                                                   * LM happens between 2 LMRs/CCMs. 
                                                   * This is the time difference 
                                                   * between reception of 2 LMRs/CCMs
                                                   */
   UINT4                    u4NearEndLoss;/* Loss Calculated for Ingress Frames */
   UINT4                    u4SeqNum;/* Sequence Number unique to a transaction.
                                      * This acts as an index for Frame Loss
                                      * Buffer Table
                                      */    
   UINT4                    u4FarEndLoss;/* Loss Calculated for Egress Frames */
   FLT4                     f4FarEndFrmLossRatio; /* Far end Frame Loss Ration */
   UINT2                    u2MepId; /* MEP ID of remote MEP. This acts as an 
                                      * index to Frame Loss Buffer Table
                                      */
   UINT2                    u2NoOfLMMSent;     /* Number of LMMs sent.*/
   UINT2                    u2NoOfLMRRcvd;     /* Number of LMRs received.*/
   UINT1                    u1LossMeasurementType;/* Type of Loss measurement */
#ifdef L2RED_WANTED
   BOOL1                    b1Synched;  /* TRUE - indicates Entry is synched at
                                        * STANDBY node */
   tEcfmMacAddr             PeerMepMacAddress;/* MAC address of the MEP with which
                                               * LM takes place
                                               */ 
   UINT1                    au1Pad[2]; 
#else
   tEcfmMacAddr             PeerMepMacAddress;/* MAC address of the MEP with which
                                               * LM takes place
                                               */ 
   UINT1                    au1Pad[3];
#endif
}tEcfmCcFrmLossBuff;

typedef struct EcfmCcRxLmmPduInfo
{
   UINT4           u4TxFCf; /* Value of TxFCf field in the received LMM Frame */
   UINT4           u4RxFCf; /* Value of the counter of in-profile data frames
                             * received by MEP from its peer MEP, at the time 
                             * of receiving last LMM frame.
                             */
}tEcfmCcRxLmmPduInfo;

typedef struct EcfmCcRxLmrPduInfo
{
   UINT4          u4TxFCf;/* Value of TxFCf sent in the LMM frame */
   UINT4          u4TxFCb;/* Value of TxFCl of remote MEP */
   UINT4          u4RxFCf;/* Value of RxFCl of remote MEP */
}tEcfmCcRxLmrPduInfo;

/****************************************************************************/
/* Per Context Statistics for CC Task*/
typedef struct EcfmCcStats
{
   UINT4            u4TxAisCount;     /* Number of AIS-PDUs Transmitted */ 
   UINT4            u4RxAisCount;     /* Number of AIS-PDUs Received    */ 
   UINT4            u4TxLckCount;     /* Number of LCK-PDUs Transmitted */ 
   UINT4            u4RxLckCount;     /* Number of LCK-PDUs Received    */ 
   UINT4            u4TxLmmPduCount;  /* Number of LMM-PDUs Transmitted */ 
   UINT4            u4RxLmmPduCount;  /* Number of LMM-PDUs Received    */ 
   UINT4            u4TxLmrPduCount;  /* Number of LMR-PDUs Transmitted */ 
   UINT4            u4RxLmrPduCount;  /* Number of LMR-PDUs Received    */ 
   UINT4            u4TxCfmPduCount;  /* Number of CFM-PDUs Transmitted 
                                       * in CC Task
                                       */
   UINT4            u4TxCcmCount;     /* Number of CCMs Transmitted 
                                       * in CC Task
                                       */
   UINT4            u4TxFailedCount;  /* Number of failed transmissions 
                                       * in CC Task
                                       */
   UINT4            u4RxCfmPduCount;  /* Number of CFM-PDUs received
                                       * in CC Task
                                       */
   UINT4            u4RxCcmCount;     /* Number of CCMs received 
                                       * in CC Task
                                       */
   UINT4            u4RxBadCfmPduCount; /* Number of bad CFM-PDUs 
                                         * received in CC Task
                                         */
   UINT4            u4FrwdCfmPduCount;  /* Number of CFM-PDUs forwarded */ 
   UINT4            u4DsrdCfmPduCount;  /* Number of CFM-PDUs discarded */
   UINT4            u1LastCfmTxFailOpcode;/* Opcode of last Transmitted CFM
                                           * PDU which was failed.
                                           */ 
} tEcfmCcStats;

/****************************************************************************/
/* Node for MEP Table */
typedef struct EcfmCcMepInfo
{
   tEcfmDllNode          MepTableDllNode;   /* DLL Node maintained in MA  for MEP 
                                             * Table.
                                             */
   tEcfmDll              RMepDb;            /* DLL Node for tEcfmCcRMepDbInfo 
                                             */
   tEcfmRBNodeEmbd       MepGlobalIndexNode;/* MEP Table tree nodes for table in 
                                             * GlobalInfo for MIB walk.
                                             */
   tEcfmRBNodeEmbd       MepPortTableNode; /*Embedded MepContextTable tree node for tree 
                                            * in Global Info
                                            */
   tEcfmRBNodeEmbd       OffloadMepNode; /* Embedded OffloadedRMepPortTable tree node 
                                          * for tree in Global Info
                                          */
   tEcfmRBNodeEmbd  HwMepNode;
   tEcfmRBNodeEmbd       MepTableMplsNode;  /* RB-Tree Node pointer of the MPLS-TP
                                             * MEP node in the MPLS-TP MEP Table
                                             * (RB Tree based). 
                                             */
   tEcfmCcMaInfo         *pMaInfo;          /* Backward pointer to MA with which this 
                                             * MEP is associated.
                                             */
   tEcfmCcPortInfo       *pPortInfo;        /* Backward pointer to PortInfo entry on 
                                             * which this MEP is configured.
                                             */
   tEcfmCcMepCcInfo      CcInfo;            /* Continuity Cehck functionality related 
                                             * information maintained for this MEP
                                             */
   tEcfmCcLmInfo         LmInfo;            /* Loss Measurement Functionality for 
                                             * this MEP 
                                             */
   tEcfmCcAvlbltyInfo    *pAvlbltyInfo;       /* Availability Measurement Functionality
                                              * for this MEP 
                                              */
   tEcfmCcAisInfo        AisInfo;          /* Alarm Indication Signal(AIS) related 
                                             * information maintained for this MEP
                                             */
   tEcfmCcLckInfo        LckInfo;          /* Locked Signal(LCK) related 
                                             * information maintained for this MEP
                                             */

   tEcfmCcFngInfo        FngInfo;           /* Fault notification Specific 
                                             * information maintained for this MEP
                                             */
   UINT4                 u4IfIndex;         /* Physical Interface index of the Port*/
   UINT4                 u4ContextId;       /* Used for offloading RB-Tree */
   UINT4                 u4MdIndex;         /* Md Index of the MD to which this MEP 
                                             * belongs 
                                             */
   UINT4                 u4MaIndex;         /* Ma Index of the MA with which this MEP 
                                             * is associated
                                             */
   UINT4                 u4PrimaryVidIsid;      /* Primary VID assigned to this MEP. Its 
                                             * value can be either the Primary VID of 
                                             * the MA it is associated or any Vlan ID 
                                             * associated to the Primary VID of its 
                                             * MA. Also used as index for MEP RBTree 
                                             * in portInfo
                                             */
   UINT4                 u4ModId;          /* Module Id that is used to identify 
                                              the modules that registered to 
                                              indicate the SF / SF clear 
                                              conditions that the MEP is received */
   UINT2                 u2MepId;           /* MEP Identifier, identifying the MEP in 
                                             * a MA.Also an index for the MEP Table.
                                             */  
   UINT2                 u2PortNum;          /* Local Port Number of the IfIndex */
   UINT2                 u2OffloadMepTxHandle; /* Maintains the transmission handle for
                                                * each MEP */
   UINT1            au1HwMepHandler [ECFM_HW_MEP_HANDLER_SIZE];
   BOOL1                 b1Active;          /* Identifies the MEP status. When SET, 
                                             * CC related state machines of this MEP 
                                             * are intialised, on RESET MEP stops 
                                             * functioning (Reset all the state 
                                             * machines of this MEP).
                                             */
   BOOL1                 b1SavedActiveValue;
   BOOL1                 b1EnableRmepDefect;
   BOOL1                 b1MaDefectIndication;
   BOOL1                 b1AllRMepsDead;
   BOOL1                 b1PresentRdi;
   BOOL1                 b1CcmOffloadLastRdi;
   UINT1                 u1LowestAlarmPri; /* Identifies the priority above which the
                                            * faults needs to be generated
                                            */
   UINT1                 u1MdLevel;         /* MDLevel at which MEP is configured, 
                                             * also used as an index for MEP RBTree 
                                             * in PortInfo
                                             */
   UINT1                 u1RowStatus;       /* Row status of row created for this MEP 
                                             * in this table
                                             */
   UINT1                 u1Direction;       /* Indicates the direction of the MEP 
                                             * (Up/Down)
                                             */
   UINT1                 u1CcmLtmPriority;  /* Priority of the CCM and LTM Pdu, 
                                             * Default value is 7
                                             */ 
   UINT1                 u1PepPortState;    /* In Provider Edge Bridge and in case of
                                             * UP MEP, PEP Port state has to filled
                                             * in Port Status TLV and this state has                                                          * to be maintained per UP MEP                                                                    */
   BOOL1                 b1MepCcmOffloadStatus;
   BOOL1                 b1MepCcmOffloadHwStatus;
   BOOL1                 b1MepAisOffloadStatus; /* Denotes the offloading status */
   BOOL1                b1MepInUse; /*Represents whether MEP is used by u4ModId*/
   UINT1                 au1Pad[1];
   tEcfmMplsParams      *pEcfmMplsParams;  /* Pointer to the MPLS Parameters. If MEP 
                                            * is MPLS-TP based, then this pointer is
                                            * valid, otherwise this pointer is set 
                                            * to NULL. The MPLS paramter block 
                                            * pointed by this field is shared 
                                            * between CC and LBLT tasks.
                                            */
   tEcfmSlaParams        EcfmSlaParams; /* Struct to RFC2544 and Y1564 Parameters.
                                            If the RFC2544 or Y1564 is enabled then
                                            this pointer is valid otherwise this
                                            pointer is set to NULL.
                                            The parameters block pointed is
                                            shared between CC and LBLT tasks */
   UINT4                 au4MpTpServicePointer[256]; /* Array to hold the service 
                                                        pointer in case of MPLS_TP
                                                        _OAM */
   UINT4                 u4MpTpServicePointerLen; /* To store the length of 
                                              * LSP/PW service pointer Length.
                                              */


   UINT4                 u4LmTxStatsHwId;       /* Used to store Egress Id of the
                                              * Egress Filter*/

   UINT4                 u4LmRxStatsHwId;       /* Used to store Ingress Id of
                                              * the Ingress Filter */

   INT4                  i4LoopbackStatus;   /* Loopback status of the the MEP */

} tEcfmCcMepInfo;
/****************************************************************************/
/*Node info for RMepDb Table*/
typedef struct EcfmCcRMepDbInfo
{
   tEcfmDllNode     MepDbDllNode;    /* Embedded MepDb DLL node for Node in 
                                      * MepInfo
                                      */
   tEcfmRBNodeEmbd  MepDbGlobalNode; /* Embedded MepDbTable tree node for tree 
                                      * in Global Info
                                      */
   tEcfmRBNodeEmbd  OffloadRMepDbNode; /* Embedded OffloadedRMepDbTable tree node 
                                        * for tree in Global Info
                                        */
   tEcfmRBNodeEmbd  HwRMepNode;
   tEcfmSenderId    LastSenderId;    /* SenderId, this structure includes 
                                      * chassis ID, Management Address 
                                      * Domain  and management address.
                                      */
   tTmrBlk          RMepWhileTimer; /* Timer after which the defects for thr 
                                        loss of three CCM is generated 
                                      */
#ifdef L2RED_WANTED
   tEcfmSysTime     RMepTimeStamp;    /*Expiry timestamp of RMEP timer*/
#endif

   tEcfmCcMepInfo   *pMepInfo;       /* Back Pointer to MEP info to which this 
                                      * Remote MEP DB is realted
                                      */
   UINT4            u4MdIndex;       /* Md index of the MD this MEP belongs 
                                      * to.
                                      */
   UINT4            u4MaIndex;       /* Ma index of the MA with which this 
                                      * MEP is associated to.
                                      */
   UINT4            u4SeqNum;        /* Sequence Number receive in CCM PDU. 
                                      * Used to update the number of 
                                      * Out-of-Sequence CCMs received.
                                      */
   UINT4            u4FailedOkTime;  /* In Time ticks, time at which Remote 
                                      * Mep state machine last entered 
                                      * either RMEP_FAILED or RMEP_OK
                                      */
   UINT2            u2MepId;         /* Mep id of this MEP*/
   UINT2            u2RMepId;        /* Remote Mep Identifier*/
   UINT2            u2OffloadRMepRxHandle; /* Maintains reception handle for each
                                            * remote MEP */
   UINT1            au1HwRMepHandler [ECFM_HW_MEP_HANDLER_SIZE];
   tEcfmMacAddr     RMepMacAddr;     /* Remote Mep's Mac address*/
   BOOL1            b1LastRdi;       /* State of RDI bit in last recieved 
                                      * CCM
                                      */
   BOOL1            b1RMepPortStatusDefect;     /* True if a Port Status Defect 
                                                 *is reported by a remote MEP*/
   BOOL1            b1RMepInterfaceStatusDefect;/* True if a Interface Status Defect
                                                 * is reported by remote MEP
                                                 */
   BOOL1            b1RMepCcmDefect; /* True if no CCM is received from the MEP
                                      * for the last 3.5* CCmInterval
                                      */
   UINT1            u1State;         /* Operational state of Remote Mep*/
   UINT1            u1LastPortStatus;/* Value of Port status TLV of last received 
                                      * CCM, or the default value indicates 
                                      * that either no CCM has been received 
                                      * or no port status TLV was present in 
                                      * the last CCM received
                                      */
   UINT1            u1LastInterfaceStatus; /* Value of Interface status TLV of last 
                                            * received CCM,or the default 
                                            * value indicates that either no 
                                            * CCM has been received or no 
                                            * interface status TLV was 
                                            * present in the last CCM 
                                            * received
                                            */
   UINT1            u1MepCcmLCStatus;  /* Gives the information whether Line 
                                        * cards belonging to this MEP  
                                        * Tx/Rx CCM PDUs from Remote MEP or 
                                        * not. */
   UINT1            u1Y1731RemCCMRxCount;
   UINT1            u1RowStatus; /* To store the Y1731-MPLSTP-RMep RowStatus,
                                  * Assigned to zero when not configured.
                                  */
   UINT1            au1Pad[2];
 
} tEcfmCcRMepDbInfo;
/****************************************************************************/
/* Node for MIP Table*/
typedef struct EcfmCcMipInfo
{
   tEcfmRBNodeEmbd  MipTableGlobalNode; /* RB Tree node for global MIP table*/
   UINT4            u4ContextId;        /* Context Identifier*/
   UINT4            u4VlanIdIsid;          /* Vlan Identifier*/
   UINT4            u4MdIndex;
   UINT4            u4MaIndex;
   UINT2            u2PortNum;         /* Local Port Num of the IfIndex */
   UINT1            u1MdLevel;         /* MD-Level of MIP*/
   UINT1            u1RowStatus;       /* Row Status*/
   BOOL1            b1Active;          /* MIP Active/In-Active*/
   BOOL1            b1ImplicitlyCreated; /* Flag indicates whether MIP is
                                            implictily configured by implicit 
                                            MIP dynamic evaluation or not */
   UINT1           au1Pad[2];   
} tEcfmCcMipInfo;
/****************************************************************************/
/* Node for Automatic MIP creation prevention Table*/
typedef struct EcfmCcMipPreventInfo
{
   tEcfmRBNodeEmbd  MipPreventTblGblNode; 
                                   /* RB Tree node for global MIP table*/
   UINT4            u4ContextId;   /* Context Identifier*/
   UINT4            u4IfIndex;         
                                   /* Global Interface Index */
   UINT4            u4VlanIdIsid; 
                                   /* Vlan Identifier */
   UINT1            u1MdLevel;         
                                   /* MD-Level of MIP */
   UINT1            u1RowStatus;       
                                   /* Row Status */
   UINT1            au1Pad[2];
} tEcfmCcMipPreventInfo;
/****************************************************************************/
/* Node for Stack Table, per interface index and in Global info also*/
typedef struct EcfmCcStackInfo
{
   tEcfmRBNodeEmbd  StackTableGlobalNode; /* RB tree node for global stack table
                                           */
   tEcfmRBNodeEmbd  StackPortInfoNode;   /* MEP Info node for tree in PortInfo 
                                          * indexed by MDLevel, VID and 
                                          * direction.
                                          */
   UINT4            u4ContextId;        /* Context Identifier*/
   tEcfmCcMepInfo  *pMepInfo;           /* Pointer to MepInfo */
   tEcfmCcMipInfo  *pMipInfo;           /* Pointer to MipInfo */  
   UINT4            u4IfIndex;          /* Interface of Stack Node*/
   UINT4            u4VlanIdIsid;        /* VlanId of the MP or zero, if it  is 
                                         * not attached to any vlan 
                                         */
   UINT2            u2PortNum;          /* Local Port Number */
   UINT1            u1MdLevel;          /* Md Level at which this MP 
                                         * configured
                                         */
   UINT1            u1Direction;        /* Direction of MP*/
} tEcfmCcStackInfo;
/****************************************************************************/
/*Used for storing information from CFMPDU */
typedef struct EcfmCcPduSmInfo
{
   union
   {
       tEcfmCcRxCcmPduInfo   Ccm;   /* Received CCM Info */
       tEcfmCcRxLmmPduInfo   Lmm;   /* Received LMM Info */
       tEcfmCcRxLmrPduInfo   Lmr;   /* Received LMR Info */
   }uPduInfo;      

   tEcfmCcStackInfo      *pStackInfo; /* Pointer to the Stack Info*/
   tEcfmCcMdInfo         *pMdInfo;   /* Pointer to MD info*/
   tEcfmCcMepInfo        *pMepInfo;   /* Pointer to MEP info*/
   tEcfmCcMipInfo        *pMipInfo;   /* Pointer to MIP info*/
   tEcfmCcRMepDbInfo     *pRMepInfo;  /* Pointer to the Remote MEP info*/
   tEcfmBufChainHeader   *pBuf       ;/* Pointer to the CRU Buffer chain 
                                       * containing the CFM PDU.
                                       */
   tEcfmBufChainHeader   *pHeaderBuf; /* Pointer to the header of the CRU buffer
                                       * containing the CFM PDU.
                                       */
   tEcfmCcPortInfo       *pPortInfo;  /* Pointer to Port info table*/
   tEcfmVlanTag          VlanClassificationInfo;/* Vlan Classification 
                                                 * Information of the 
                                                 * Received PDU
                                                 */
   tEcfmPbbTag    PbbClassificationInfo;
   tEcfmTagType    EcfmTagType;  /* indicates the  valid calsification type
                                    vlan / isid */ 
   UINT4                 u4ByteCount;     /* Length of the PDU */
   UINT4                 u4RxVlanIdIsId;  /* ISID recieved in CFM PDU*/
   UINT4                 u4ITagOffset;    /* offset from where next tag to 
                                           * be deocoded */
   UINT4                 u4PduOffset;     /* CFM PDU offset in the recvd buf*/
   UINT4                 u4InterfaceId;   /* Interface on which CCM PDU is 
                                           * received from CFA */
   UINT1                 u1NoFwd;         /* ECFM_INTR_GENERATED*/ 
   UINT1                 u1RxPbbPortType;  /* Port type of incomming port
                                            * CNP/PIP/CBP/PNP */
   UINT1                 u1RxDirection;  /* Direction of the MP*/
   UINT1                 u1RxMdLevel;    /* MD Level received in CFM PDU*/
   UINT1                 u1RxOpcode;       /* Opcode of the received CFM-PDU*/
   UINT1                 u1RxFlags;      /* Flags Field in the received CFM 
                                          * PDU
                                          */
   UINT1                 u1RxFirstTlvOffset; /* First TLV Offset in the PDU, 
                                              * its value is according to 
                                              * OpCode
                                              */
   UINT1                 u1CfmPduOffset;   /* CFM PDU offset*/
   UINT1                 u1RxVersion;      /* CFM Version of the CFMPDU received 
                                            */
   BOOL1                 b1PortInChannel;  /* boolean to indicate whether this port 
                                            * is part of a port channel
                                            */
   UINT1                 au1Pad[2];        /* Padding to keep the structure byte
                                            * aligned 
                                            */ 
   tEcfmMacAddr          RxDestMacAddr;  /* Destination address received in 
                                          * CFM PD
                                          */
   tEcfmMacAddr          RxSrcMacAddr;   /* Source address received in CFM 
                                            * PDU
                                            */
} tEcfmCcPduSmInfo;
/****************************************************************************/
/* Message to be queued in the message queue*/
typedef struct EcfmCcMsg
{
   union    
   {
#ifdef MBSM_WANTED
      struct MbsmIndication {
          tMbsmProtoMsg *pMbsmProtoMsg;
      }MbsmCardUpdate;
#endif
#ifdef L2RED_WANTED
       tEcfmRedRmFrame   RmFrame;
#endif
       struct
       {
           UINT4                u4VidIsid;      /* VLAN ID of the MEP*/
           /* In MPLSTP-OAM, MEPs is not added to the ECFM_CC_PORT_MEP_TABLE 
            * as the MPLSTP MEPs are not associated to port.
            * Inorder to the get the information about the MPLSTP MEPs from
            * ECFM_CC_MEP_TABLE, the MEP related information is added here.
            */
           UINT4                u4MdIndex;      /* MdIndex of the MEP */
           UINT4                u4MaIndex;      /* MaIndex of the MEP */ 
           UINT2                u2MepId;        /* MEP ID */
           UINT1                u1Direction;    /* Direction of the MEP*/
           UINT1                u1MdLevel;      /* MD Level of the MEP*/
           UINT1                u1SelectorType; /* Selector Type */
           UINT1                au1Pad[3];
       }Mep;
       /* structure for chassis information update*/
       struct
       {
           UINT2 u2IdLen;
           UINT1 u1SubType;
           UINT1 au1Id[ECFM_MAX_CHASSISID_LEN];
           UINT1 au1Pad[2];
       }ChassisId;
       /* structure for port information update*/
       struct
       {
           UINT2 u2IdLen;
           UINT1 u1SubType;
           UINT1 au1Id[ECFM_MAX_PORTID_LEN];
           UINT1 au1Pad[2];
       }PortId;
       /* structure for IPv4 interface change updated*/
       struct
       {
           tNetIpv4IfInfo  NetIpIfInfo;
           UINT4           u4BitMap;
       }Ip4ManAddr;
       struct 
       {
           UINT1           au1HwHandler [ECFM_HW_MEP_HANDLER_SIZE];
#ifdef NPAPI_WANTED
           tEcfmHwEvents   EcfmHwEvents;
#endif
       }HwCalBackParam;
       /* structure for IPv6 interface change updated*/
       tNetIpv6AddrChange Ip6ManAddr;
       struct
       {
           UINT4            u4VlanIdIsid;
           UINT2            u2CcmOffloadHandle;
           UINT2            u2MstInst;
           UINT2            u2EtherTypeValue; /* VLAN Ether Type Value */
           UINT1            u1EtherType;      /* VLAN Ingress or Egress Ether
                                               * Type 
                                               */
           UINT1            u1PortState;
       }Indications;
       tEcfmBufChainHeader   *pEcfmPdu;    /* ECFMPDU received from CFA*/
   
       tEcfmMplsParams       EcfmMplsParams; /* Structure Containing MPLS
                                              * specific parameters
                                              */
       tEcfmMplstpAisOffParams MplstpAisOffParams; /* MPLSTP AIS Offad 
                                                    * details
                                                    */
       UINT1                 u1PortOperState;
       UINT1                 u1IntfType; 
   }uMsg;
   UINT4            u4IfIndex;     /* Interface Index of the port*/
   UINT4            u4ContextId;   /* Context Identifier*/
   tEcfmMsgType     MsgType;       /* Message type specifying Creation / 
                                    * deletion / updation or PDU received.
                                    */
   tEcfmMsgType     MsgSubType;    /* Message type specifying sub message 
                                    */
   UINT2            u2PortNum;
   UINT1            u1MdLevel;
   UINT1            u1MplsTnlPwOperState;  /* Specifies the port state
                                            * Up/Down of the port. Port
                                            * operational state indication
                                            * is provided by the CFA.
                                            */
   tEcfmMacAddr     RxSrcMacAddr;
   UINT1            u1Pad[2];
} tEcfmCcMsg;
/****************************************************************************/
/* Node info for MEPListTable*/
typedef struct EcfmCcMepListInfo
{
   tEcfmCcMaInfo   *pMaInfo;     /* Back pointer to the MA with which this MEP 
                                  * is associated
                                  */
   tEcfmRBNodeEmbd  MepListGlobalNode;  /* Embedded RB Tree node for MepList 
                                         * Table in GlobalInfo indexed by 
                                         * MdIndex, MaIndex, MepId.
                                         */
   UINT4            u4MdIndex;          /* Index of MD to which this MEP 
                                         * belongs.
                                         */
   UINT4            u4MaIndex;          /* Index of MA with which this MEP is 
                                         * belongs.
                                         */
   UINT2            u2MepId;            /* MepId identifying the MEP in a MA. */
   UINT1            u1RowStatus;        /* Row status of the Row created. */
   UINT1            u1Pad;
} tEcfmCcMepListInfo;
/******************************************************************************/
/* Info related to MIP CCM Database */
typedef struct EcfmCcMipCcmDb
{
   tEcfmRBNodeEmbd  MipCcmDbGlobalNode; /* Embedded node for MIP CCM Db Table*/
   tEcfmSysTime     TimeStamp;          /* Stores the current time */
   UINT2            u2Fid;              /* Filtetring identifier used as an 
                                         * index of this table
                                         */
   UINT2            u2PortNum;          /* Local Port Num */
#ifdef L2RED_WANTED
   BOOL1             b1Changed;          /* Set to TRUE whenever an entry is
                                          * updated */
   tEcfmMacAddr     SrcMacAddr;         /* Mac address received in CCM PDU for 
                                         * this VID
                                         */
   UINT1             u1Pad;
#else 
   tEcfmMacAddr     SrcMacAddr;         /* Mac address received in CCM PDU for 
                                         * this VID
                                         */
   UINT1             au1Pad[2];
#endif
} tEcfmCcMipCcmDbInfo;
/****************************************************************************/
/* Node info for DefaultMdTable*/
typedef struct EcfmCcDefaultMdTableInfo
{
   tEcfmRBNodeEmbd  DefaultMdGlobalNode;

   UINT4            u4PrimaryVidIsid;   /* Primary VID of VLAN, used as an 
                                         * index
                                         */
   BOOL1            b1Status;           /* True if, there is a MA at same 
                                         * MdLevel associated with same vlanId 
                                         * and an upMep defined with the same MA
                                         */
   INT1             i1MdLevel;          /* Level at which MHFs are to be 
                                         * created, for entry indexed by the 
                                         * VLAN, -1 indicates that Mdlevel for 
                                         * MHF creation is to be controlled by 
                                         * DefMdDefLevel
                                         */
   UINT1            u1MhfCreation;      /* Indicating if mgmt entity can create 
                                         * MHFs at MdLevel for VlD
                                         */
   UINT1            u1IdPermission;     /* Indicating if anything is to be 
                                         * included in SenderID TLV, transmitted 
                                         * by the MHFs created by default 
                                         * maintenance domain
                                         */
} tEcfmCcDefaultMdTableInfo;
/****************************************************************************/
/*Node for Configuration Error List Table*/
typedef struct EcfmCcConfigErrInfo
{   
   tEcfmRBNodeEmbd  ConfigErrTableGlobalNode; /* RB Tree node for global
                                               * config error table 
                                               */
   UINT4            u4ContextId;              /* Virtual context identifier */
   UINT4            u4VidIsid;                /* VlanId*/
   UINT4            u4IfIndex;            /* Global Interface Index*/
   UINT1            u1ErrorType;          /* Error type,
                                           * 0 - CFMleak
                                           * 1 - conflictingVids
                                           * 2 - ExcessiveLevels
                                           * 3 - OverlappedLevels 
                                           */
   UINT1          au1Pad[3];
} tEcfmCcConfigErrInfo;
/****************************************************************************/
/*Node for Vlan Table*/
typedef struct  EcfmCcVlanInfo
{
   tEcfmRBNodeEmbd  VlanGlobalNode;       /* Embedded node for VlanTable in 
                                           * GlobalInfo
                                           */
   tEcfmRBNodeEmbd  PrimaryVlanGlobalNode;
   UINT4            u4VidIsid;            /* Vlan ID as Index of VlanTable*/
   UINT4            u4PrimaryVidIsid;     /* PrimaryVid associated with the VLAN 
                                           * ID
                                           */ 
   UINT1            u1RowStatus;          /* Row status*/
   UINT1            au1Pad[3];
} tEcfmCcVlanInfo;
/****************************************************************************/
/* Node for Ma Hash Table maintained for dyanamic MIP evaluation */
typedef struct EcfmCcMaHashInfo
{
   tTMO_HASH_NODE *pNext;
   tEcfmCcMaInfo *pMaInfo;
} tEcfmCcMaHashInfo;
/****************************************************************************/
/* Ccm Error Log Specific Information for CC Task */
typedef struct EcfmCcErrLogInfo
{
   tEcfmRBNodeEmbd       ErrLogGlobalNode;  /* The MepError Table tree node
                                               for table in Context Info.*/
   UINT4                 u4MdIndex; /* The MD Index of MEP, also used as an
                                       index for MepError Table in Global Info
                                       for MIB walk.*/
   UINT4                 u4MaIndex;  /* The ME Index of MEP, also used as an index
                                        for MepError Table in Global Info for
                                        MIB walk.*/
   UINT4                 u4SeqNum;  /* Locally maintained sequence number of the
                                       error log entry
                                     */
   tEcfmSysTime          TimeStamp; /* Time Stamp at which the error occured/cleared */   
   UINT2                 u2RmepId;    /* The MepId of MEP, with which defect is
                                         encountered/cleared.*/
   UINT2                 u2LogType;  /* Type of fault that occurs at the receiving
                                      * Mep 
                                      */
   UINT2                 u2MepId;    /* The MepId of MEP, used as an index for
                                        Mep Error table in Global Info for MIB Walk. */
   UINT1                 au1Pad[2];

}tEcfmCcErrLogInfo;
/****************************************************************************/
/* Context Specific Information for CC task */
typedef struct EcfmCcContextInfo
{
   tEcfmRBTree               MepPortTable; /* RBTree for tEcfmCcMepInfo indexed 
                                            * by portNum, VlanId, Mdlevel and 
                                            * direction
                                            */
   tEcfmRBTree               MdTableIndex;  /* RBTree for tEcfmCcMdInfo indexed 
                                             * by MdIndex
                                             */
   tEcfmRBTree               MaTableIndex ; /* RBTree for tEcfmCcMaInfo indexed 
                                             * by  MdIndex and MaIndex
                                             */
   tEcfmRBTree               MepTableIndex; /* RBTree for tEcfmCcMepInfo indexed 
                                             * by MdIndex, MaIndex, MepId.
                                             */
   tEcfmRBTree               RMepDbTable;   /* RBTree for tEcfmCcRMepDbInfo 
                                             * table indexed by  MdIndex, 
                                             * MaIndex, MepId, RMepId.
                                             */
   tEcfmRBTree               MaMepListTable;/* RBTree for the tEcfmCcMepListInfo 
                                             * indexed by MdIndex,MaIndex and 
                                             * MepId for maintaining the list of 
                                             * MEPs in a single MA.
                                             */
   tEcfmRBTree               VlanTable;     /* RBTree for tEcfmCcVlanInfo 
                                             * indexed by VLAN ID
                                             */
   tEcfmRBTree               PrimaryVlanTable;
   tEcfmRBTree               MipCcmDbTable; /* RBTree for tEcfmCcMipCcmDbInfo 
                                             * indexed by FID (Filtering 
                                             * Identifier).
                                             */
   tEcfmRBTree               ErrLogTable; /* RBTree for tEcfmCcErrLog*/
   tEcfmRBTree               FrmLossBuffer;/* RBTree for Frame Loss Rolling Buffer
                                            * indexed by MegIndex, MeIndex,MepId, 
                                            * transaction id, sequence number
                                            */
   tEcfmRBTree               DefaultMdTable;     
   

   tEcfmRBTree               MplsPathTree; /* RB Tree containing MA(ME) info
                                            * indexed by
                                            * either 4 tuple or VC ID.
                                            * */
  
   tMemPoolId                FrmLossPool;         /* Memory pool for Cc Frame Loss
                                                   * table
                                                   */
   tMemPoolId                MipCcmDbInfoPool;   /* Memory pool for assigning 
                                                  * memory to MIP DB tree Nodes.
                                                  */
   tMemPoolId                ErrLogPool;         /* Memory pool for Cc Error Log
                                                  * table
                                                  */

   tTmrBlk                   MipDbHoldTimer; /* MIP CCM DB timer for holding CCM 
                                              * entries in the DB.
                                              */
#ifndef ECFM_ARRAY_TO_RBTREE_WANTED 
   tEcfmCcPortInfo           *apPortInfo[ECFM_PORTS_PER_CONTEXT_MAX]; /* Array of pointers 
                                                                       * to PortInfo to 
                                                                       * store all the port 
                                                                       * specific 
                                                                       * information 
                                                                       * required for the 
                                                                       * protocol 
                                                                       * functionality.
                                                                       */
#endif
   tEcfmCcStats              CcStats;            /* Per Context Stastitics */
#ifdef L2RED_WANTED
   tEcfmSysTime              TimeStamp;
   UINT4                     u4RecvdMipDbHldTime; /* Remaining Time Value for 
                                                   * the MipDbHold Timer 
                                                   */
#endif 
   UINT4                     u4ContextId;        /* Context Identifier*/ 

   UINT4                     u4EcfmUpCount;      /* Number of times ECFM module
                                                  * was enabled 
                                                  */
   UINT4                     u4EcfmDownCount;    /* Number of times ECFM module
                                                  * was disabled
                                                  */
   UINT4                     u4NoDefectCount;    /* Identifies the number of
                                                  * times no defect occured in any MEP
                                                  */
   UINT4                     u4RdiDefectCount;   /* Identifies the number of
                                                  * times RDI defect occured in any MEP
                                                  */
   UINT4                     u4MacStatusDefectCount; /* Identifies the number of
                                                      * times MAC Status defect occured in any MEP
                                                      */
   UINT4                     u4RemoteCcmDefectCount; /* Identifies the number of
                                                      * times Remote CCM defect occured in any MEP
                                                      */
   UINT4                     u4ErrorCcmDefectCount;/* Identifies the number of
                                                    * times Error CCM defect occured in any MEP
                                                    */
   UINT4                     u4XconnDefectCount; /* Identifies the number of
                                                  * times Xconn defect occured in any MEP
                                                  */
   UINT4                     u4ErrorLogSeqNum;   /* SeqNumber of the Error Log Entry */ 
   UINT4                     u4BridgeMode;  /* Bridge mode BCB/BEB/PEB/PCB etc 
                                             * this values is get from l2iwf on 
                                             * prot cretion and stored in 
                                             * context */
   UINT4                     u4RegModBmp; /* BitMap of Registered Modules.
                                             This bitmap is to store the
                                             Modules registered with ECFM for 
                                             receiving fault notification */
   UINT2                     u2ErrLogSize;       /* The Maximum number of entries for the 
                                                  * Continuity Check Error Log, only if 
                                                  * fsMIY1731CcErrorLogStatus is enabled.
                                                  */
   UINT2                     u2MipCcmDbHoldSize; /* configurable size of
                                                  * MipCcmDb mempool */                        
   UINT2                     u2CrossCheckDelay;  /* Delay in number of CCMs */
   UINT2                     u2Y1731TrapControl; /* Controls the enabling or
                                                  * disabling of ECFM Traps 
                                                  */
   UINT2                     u2FrmLossBuffSize; /* Number of entries that can be 
                                                 * stored in Frame Loss Buffer
                                                 */

   UINT1                     u1DefaultMdDefLevel;/* MdLevel at which MHFs are to 
                                                  * created, used when 
                                                  * DefaultMdEntry's MdLevel 
                                                  * contains -1
                                                  */
   UINT1                     u1DefaultMdDefMhfCreation;/* Used to control the 
                                                        * MHF creation, when 
                                                        * DefaultMdEntry's 
                                                        * MhfCreation 
                                                        * contains defMHFdefer
                                                        */
   UINT1                     u1DefaultMdDefIdPermission; /* Used to control the 
                                                          * SenderIdTlv 
                                                          * transmission, 
                                                          * when 
                                                          * DefaultMdEntry's 
                                                          * IdPermission 
                                                          * contains 
                                                          * SenderIdDefer
                                                          */
   UINT1                     u1MipCcmDbStatus; /* Enable/Disable for storing
                                                *  entries of MipCcmDb.
                                                *  It is User configurable
                                                */
   UINT1                     u1MipCcmDbHoldTime; /* configurable time in hours for which
                                                  * MipCcmDb entries are to be 
                                                  * retained */
   UINT1                     u1TrapControl;      /* Controls the enabling or
                                                  * disabling of ECFM Traps 
                                                  */
   UINT1                     u1ErrLogStatus;     /* The administrative status of 
                                                  * Continuity Check Error Log.
                                                  * This enables or disables the logging 
                                                  * of Continuity Check errors 
                                                  * detected by MEP.
                                                  */
   BOOL1                     b1MipDynamicEvaluationStatus; /* True or false for
                                                              for controlling the 
                                                              implicit evaluation
                                                              and creation of MIP */
   BOOL1                     b1GlobalCcmOffload;
#ifndef ECFM_ARRAY_TO_RBTREE_WANTED
   tLocalPortListExt            DeMuxPortList;   
                             /* This conatins the list of ports for a context
                              * on which de-mux for MEP has to be done. This 
                              * will contain ports which have UP-MEP and ports
                              * which have up/down MEP as locked.
                              * */
#endif
   UINT1                     u1Pad;
} tEcfmCcContextInfo;

/****************************************************************************/
/* global information for CC task */
typedef struct EcfmCcGlobalInfo
{
   tMemPoolId            ActiveMaLevelMemPool;   /* Memory pool for assigning 
                                                  *  memory for Active MA Nodes
                                                  */
   tMemPoolId            ActiveMdLevelMemPool;   /* Memory pool for assigning 
                                                  *  memory for Active MD Nodes
                                                  */
   tMemPoolId            MdTableMemPool;         /* Memory pool for assigning 
                                                  * memory to MD Tree Nodes
                                                  */
   tMemPoolId            DefMdTableMemPool;     /* Memory pool for assigning 
                                                  * memory to DefaultMD Tree Nodes
                                                  */
   tMemPoolId            MaTableMemPool;         /* Memory pool for assigning 
                                                  * memory to MA Tree Nodes
                                                  */
   tMemPoolId            MepTableMemPool;        /* Memory pool for assigning 
                                                  * memory to MEP Tree Nodes
                                                  */
   tMemPoolId            MepMpTpParamsMemPool;   /* Memory pool for assigning 
                                                  * memory to MEP MPLS-TP Params
                                                  * blocks
                                                  */
   tMemPoolId            MipTableMemPool;        /* Memory pool for assigning 
                                                  * memory to MIP Tree Nodes
                                                  */
   tMemPoolId            MipPreventTblMemPool;   /* Memory pool for assigning 
                                                  * memory to MIP prevent Tree 
                                                  * Nodes
                                                  */
   tMemPoolId            MepDbTableMemPool;      /* Memory pool for assigning 
                                                  * memory to MEP DB Tree Nodes
                                                  */
   tMemPoolId            MaMepListTableMemPool;  /* Memory pool for assigning 
                                                  * memory to MaMepList Tree 
                                                  * Nodes
                                                  */
   tMemPoolId            StackTableMemPool;      /* Memory pool for assigning 
                                                  * memory to Stack Tree Nodes
                                                  */ 
   tMemPoolId            ConfigErrListTableMemPool; /* Memory pool for 
                                                     * assigning memory to 
                                                     * Configure Error 
                                                     * List Tree Nodes
                                                     */
   tMemPoolId            VlanTableMemPool;       /* Memory pool for assigning 
                                                  * memory to VlanTree Nodes
                                                  */
   tMemPoolId            PortInfoMemPool;        /* Memory pool for assigning 
                                                  * memory to Port Info Nodes
                                                  */
   tMemPoolId            MsgQMemPool;            /* Memory pool for Message 
                                                  * Queue 
                                                  */
   tMemPoolId            TmrMemPool;             /* Memory pool for Timer List*/
   tMemPoolId            ContextMemPool;         /* Memory pool for various 
                                                  * Contexts
                                                  */    
   tMemPoolId            HwPortListPool;         /* Memory Pool for array containing 
                                                  * Port List to forward CCM in 
                                                  * case CCM if offloaded */
   tMemPoolId            AppRegMemPoolId;        /* Memory Pool for Application
                                                    registered with CC Task
                                                    and Database */
   tMemPoolId            AisPduPool;             /* Memory pool for AIS-PDU
                                                  */
   tMemPoolId            LckPduPool;             /* Memory pool for LCK-PDU */

   tMemPoolId            PathInfoPool;           /* Memory pool for MPLS Path info 
                                                  */
   tMemPoolId            InParamsPool;           /* Memory pool to hold inparams */
   tMemPoolId            OutParamsPool;          /* Memory pool to hold outparams */
   tMemPoolId            ServicePtrPool;      /* Memory pool to hold Service */

   tMemPoolId            RmepChassiIdPool;

   tMemPoolId            RmepMgmtAddrDomPool;

   tMemPoolId            RmepMgmtAddrPool;
   tMemPoolId            AvlbltyPool;            /* Availability Info
                                                  * Mempool */ 
   tMemPoolId            MepErrCcmMemPool;
   tMemPoolId            MepXconCcmMemPool;

   tMemPoolId            LbrRcvdInfoPool;
   
   tMemPoolId            AcInfoPool; /*Mem pool for MPLS TP AC Info*/
   
   tEcfmRBTree           GlobalPortTable;        /* Global Port Table*/
#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
   tEcfmRBTree           GlobalCcLocalPortTable; /*Global Local Port Table for CC*/
   tEcfmRBTree           GlobalLbltLocalPortTable; /*Global Local Port Table for LBLT*/
#endif
   tEcfmRBTree           GlobalStackTable;       /* Global Stack Table*/
   tEcfmRBTree           GlobalConfigErrTable;   /* Global configuration error table*/
   tEcfmRBTree           GlobalMipTable;         /* Global MIP table*/
   tEcfmRBTree           GlobalMipPreventTbl;    /* Global MIP Prevent table*/
   tEcfmRBTree           GlobalOffloadRMepDbTbl; /* Global Offload RMepDb table*/
   tEcfmRBTree           GlobalOffloadMepTbl;    /* Global Offload Mep table*/
   tEcfmRBTree           GlobalHwMepTbl;         /* Global Hw Mep table*/
   tEcfmRBTree           GlobalHwRMepTbl;        /* Global Hw RMep table*/
   tOsixTaskId           TaskId;                 /* Task Identifier for the CC 
                                                  * task
                                                  */        
   tOsixQId              CfgQId;                 /* Configuration Message Queue 
                                                  * Identifier for CC Task
                                                  */    
   tOsixQId              PktQId;                 /* Packet Message Queue 
                                                  * Identifier for CCTask
                                                  */   
   tOsixQId              IntQId;                 /* Interrupt Message Queue 
                                                  * Identifier for CCTask
                                                  */    

   tOsixSemId            SemId;                  /* Semaphore Identifier for the 
                                                  * CC Task
                                                  */ 
   tTimerListId          TimerListId;            /* Timer List Identifier for CC 
                                                  * Task
                                                  */
   tTmrDesc              aTmrDesc[ECFM_CC_TMR_MAX_TYPES];
                          /* description of all timers are stored in this
                           * array with TimerId as the array index. */

   UINT4                 u4MemoryFailureCount;   /* Memory allocation failure
                                                  * counter
                                                  */
   UINT4                 u4BufferFailureCount;   /* Buffer allocation failure
                                                  * counter
                                                  */
   UINT4                 u4SysLogId;              /* Syslog Identifier */
   UINT4                 u4MgmtAddressLen;        /* management address length */
   UINT4                 u4StaggeringDelta;        /* Staggering Delta */
   UINT4                 u4StaggeredCtxId;         /* Staggering Delta */
   UINT4                 u4ElpsFilter;             /* Elps Filter Id */
   INT4                  i4Ipv4L3IfIndex;
   INT4                  i4Ipv6L3IfIndex;
   UINT1                 *pu1CCPdu;              /* Global Pointer used for 
                                                  * formatting CCM PDU
                                                  */
   tEcfmCcContextInfo    *apContextInfo[SYS_DEF_MAX_NUM_CONTEXTS]; /* Array of Pointer 
                                                             * to various 
                                                             * context sepcific 
                                                             * informations
                                                             */
    tEcfmRegParams        *apAppRegParams[ECFM_MAX_APPS];  
                                                  /* Y.1731 Information
                                                   * specific to applications
                                                   * registering with ECFM */
   UINT1                 au1OUI[ECFM_OUI_LENGTH]; /* Array of Length 3 for 
                                                   * storing the 
                                                   * Oraganisation Unique 
                                                   * Identifier.
                                                   */
   UINT1                 u1ChassisIdLen;          /* Chassis-id length*/
   UINT1                 au1ChassisId[ECFM_MAX_CHASSISID_LEN];/* Array to store
                                                               * the chassis-id
                                                               */
   UINT1                 u1ChassisIdSubType;      /*
                                                   * Chassis-id subtype.
                                                   */
   /* Management Address*/
   UINT1                 au1MgmtAddress[ECFM_MAX_MAN_ADDR_LEN];
   UINT1                 u1MgmtAddressDomain;
} tEcfmCcGlobalInfo;

#endif /* _CFM_CC_TDF_H */
/****************************************************************************
  End of File cfmcctdfs.h
 ****************************************************************************/
