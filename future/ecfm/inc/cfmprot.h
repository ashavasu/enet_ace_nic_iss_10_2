/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmprot.h,v 1.55 2016/02/18 08:32:27 siva Exp $
 * 
 * Description: This file contains the prototypes for all the 
 *              PUBLIC procedures.
 *********************************************************************/

#ifndef _CFM_PRT_H
#define _CFM_PRT_H
PUBLIC INT4 EcfmLtmRxSmGetEgressPort PROTO ((tEcfmLbLtPduSmInfo *, UINT2 *, 
                                              BOOL1 *));
PUBLIC INT2 EcfmGetFwdPortCount PROTO ((tLocalPortListExt *));
PUBLIC BOOL1 EcfmSnmpLwTestLbmTxParam PROTO ((UINT4, UINT4, UINT2));
PUBLIC VOID EcfmSnmpLwUpdateDefaultMdStatus PROTO ((UINT1, UINT4, BOOL1));
PUBLIC tEcfmCcVlanInfo * EcfmSnmpLwGetVlanEntry PROTO ((UINT4));
PUBLIC tEcfmCcStackInfo* EcfmSnmpLwGetStackEntry PROTO ((UINT4, UINT4, UINT1,
                                                         UINT1 ));
PUBLIC tEcfmCcMdInfo * EcfmSnmpLwGetMdEntry PROTO ((UINT4 ));
PUBLIC tEcfmCcDefaultMdTableInfo * EcfmSnmpLwGetDefMdEntry PROTO ((UINT4 ));
PUBLIC INT4 EcfmSnmpLwIsInfoConfiguredForMd PROTO ((tEcfmCcMdInfo *));
PUBLIC tEcfmCcMaInfo * EcfmSnmpLwGetMaEntry PROTO ((UINT4 , UINT4 ));
PUBLIC tEcfmCcRMepDbInfo* EcfmSnmpLwGetRMepEntry PROTO ((UINT4, UINT4, UINT2, 
                                                         UINT2 ));
PUBLIC tEcfmCcMepListInfo* EcfmSnmpLwGetMaMepListEntry PROTO ((UINT4, UINT4,  
                                                               UINT2 ));
PUBLIC tEcfmCcConfigErrInfo* EcfmSnmpLwGetConfigErrEntry PROTO ((UINT4 ,UINT2 ));
PUBLIC INT4 EcfmSnmpLwAddMaMepListEntry PROTO ((tEcfmCcMepListInfo *));
PUBLIC INT4 EcfmSnmpLwAddMipEntry PROTO (( tEcfmCcMipInfo *));
PUBLIC tEcfmCcRMepDbInfo *EcfmSnmpLwGetY1731MplsTpRMepCfgEntry PROTO ((UINT4, 
                                            UINT4, UINT2, UINT2));
PUBLIC tEcfmCcRMepDbInfo *EcfmSnmpLwGetY1731MplsTpRMepAutoEntry PROTO ((UINT4, 
                                            UINT4, UINT2, UINT2));
PUBLIC VOID EcfmSnmpLwDeleteMipEntry PROTO ((tEcfmCcMipInfo *));
PUBLIC INT4 EcfmSnmpLwIsInfoConfiguredForMip PROTO ((UINT4 , UINT1 , UINT4, UINT4 *));
PUBLIC INT4 EcfmSnmpLwDeleteMaMepListEntry PROTO ((tEcfmCcMepListInfo *));
PUBLIC VOID EcfmSnmpLwUpdateAllMepRowStatus PROTO ((tEcfmCcMaInfo *));
PUBLIC VOID EcfmSnmpLwUpdateAllMaRowStatus PROTO ((tEcfmCcMdInfo *));
PUBLIC INT4 EcfmSnmpLwIsInfoConfiguredForMa PROTO ((tEcfmCcMaInfo *));
PUBLIC BOOL1 EcfmSnmpLwDefaultMaConfigAllowed PROTO ((tEcfmCcMdInfo *));
PUBLIC INT4 EcfmSnmpLwAddMaEntry PROTO ((tEcfmCcMaInfo *));
PUBLIC VOID EcfmSnmpLwDeleteMaEntry PROTO ((tEcfmCcMaInfo *));
PUBLIC INT4 EcfmSnmpLwAddMepEntry PROTO (( tEcfmCcMepInfo *));
PUBLIC INT4 EcfmSnmpLwAddMepInAPort PROTO ((tEcfmCcMepInfo *));
PUBLIC INT4 EcfmSnmpLwAddMepInStack PROTO ((tEcfmCcMepInfo*  ));
PUBLIC VOID EcfmSnmpLwDeleteMepEntry PROTO (( tEcfmCcMepInfo *));
PUBLIC INT4 EcfmSnmpLwIsInfoConfiguredForMep PROTO ((tEcfmCcMepInfo *));
PUBLIC VOID EcfmSnmpLwHandleMepAddFailure PROTO ((tEcfmCcMepInfo *));
PUBLIC UINT2 EcfmSnmpLwGetNoOfEntriesForPVid PROTO ((UINT2 ));
PUBLIC INT4 EcfmMepCmpForMepListInMa PROTO ((tRBElem *,tRBElem *));
PUBLIC INT4 EcfmMaCmpForMaTableInMd PROTO ((tRBElem *,tRBElem *));
PUBLIC INT4 EcfmCmpForRMepDbInMep PROTO ((tRBElem *,tRBElem *));
PUBLIC tEcfmLbLtLtrInfo* EcfmSnmpLwGetLtrEntry PROTO ((UINT4, UINT4, UINT2, 
                                                       UINT4 , UINT4 ));
PUBLIC INT4 EcfmSnmpLwInitiateLbmTx PROTO ((UINT4 , UINT4 , UINT2 ));
PUBLIC INT4 EcfmSnmpLwInitiateLtmTx PROTO ((UINT4 , UINT4 , UINT2 ));
PUBLIC INT4 EcfmSnmpLwCreateRMepInAllMeps PROTO ((tEcfmCcMepListInfo *));
PUBLIC INT4 EcfmSnmpLwDeleteRMepInAllMeps PROTO ((tEcfmCcMepListInfo *));
PUBLIC VOID EcfmSnmpLwUpdateRMepDbEntries PROTO ((tEcfmCcMepInfo *));
PUBLIC BOOL1 EcfmSnmpLwTestDmTxParam PROTO ((UINT4, UINT4, UINT2));
PUBLIC BOOL1 EcfmSnmpLwTestTstTxParam PROTO ((UINT4, UINT4, UINT2));
PUBLIC BOOL1 EcfmSnmpLwTestThTxParam PROTO ((UINT4, UINT4, UINT2));
PUBLIC tEcfmLbLtLbmInfo * EcfmSnmpLwGetLbTransEntry PROTO ((UINT4, UINT4, UINT2, UINT4));
PUBLIC VOID EcfmCcSetAisCondition PROTO ((tEcfmCcMepInfo *, tEcfmMacAddr,
                                                                UINT1));
PUBLIC VOID EcfmSnmpLwGetLbTransactionStats PROTO ((tEcfmLbLtLbmInfo *,
                                                    UINT4 *,
                                                    INT4 *,
                                                    INT4 *,
                                                    INT4 *,
                                                    UINT4 *,
                                                    UINT4 *));
PUBLIC INT4 EcfmSnmpLwGetNextIndexLbStatsTbl PROTO ((UINT4 u4MdIndex,
                                                       UINT4 *,
                                                       UINT4 ,
                                                       UINT4 *,
                                                       UINT4 ,
                                                       UINT4 *,
                                                       UINT4 ,
                                                       UINT4 *));
PUBLIC INT4 EcfmSnmpLwGetFirstIndexLbStatsTbl PROTO ((UINT4 *,
                                                        UINT4 *,
                                                        UINT4 *,
                                                        UINT4 *));
PUBLIC tEcfmLbLtFrmDelayBuff * EcfmSnmpLwGetFrmDelayTransEntry PROTO ((UINT4, UINT4, UINT2, UINT4 ));
PUBLIC VOID EcfmSnmpLwGetDmTransactionStats PROTO ((tEcfmLbLtFrmDelayBuff *,
                                                    UINT4 *pu4DmrIn,
                                                    tSNMP_OCTET_STRING_TYPE *,
                                                    tSNMP_OCTET_STRING_TYPE *,
                                                    tSNMP_OCTET_STRING_TYPE *,
                                                    tSNMP_OCTET_STRING_TYPE *,
                                                    tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT4 EcfmSnmpLwGetNextIndexFdStatsTbl PROTO ((UINT4 u4MdIndex,
                                                       UINT4 *,
                                                       UINT4 ,
                                                       UINT4 *,
                                                       UINT4 ,
                                                       UINT4 *,
                                                       UINT4 ,
                                                       UINT4 *));
PUBLIC INT4 EcfmSnmpLwGetFirstIndexFdStatsTbl PROTO ((UINT4 *,
                                                        UINT4 *,
                                                        UINT4 *,
                                                        UINT4 *));
PUBLIC tEcfmCcErrLogInfo * EcfmSnmpLwGetErrorLogEntry PROTO ((UINT4, UINT4, UINT2, UINT4));
PUBLIC tEcfmLbLtLbmInfo * EcfmSnmpLwGetLbmEntry PROTO ((UINT4, UINT4, UINT2, UINT4, UINT4));
PUBLIC tEcfmLbLtLbrInfo * EcfmSnmpLwGetLbrEntry PROTO ((UINT4, UINT4, UINT2, UINT4, UINT4, UINT4));
PUBLIC tEcfmLbLtLbmInfo * EcfmGetNextLbmNodeForATrans PROTO ((tEcfmLbLtLbmInfo *));
PUBLIC tEcfmLbLtFrmDelayBuff *EcfmGetNextFrmDelayNodeForATrans PROTO ((tEcfmLbLtFrmDelayBuff *));
PUBLIC tEcfmLbLtFrmDelayBuff * EcfmSnmpLwGetFrmDelayEntry PROTO ((UINT4, UINT4, UINT2,UINT4, UINT4));
PUBLIC tEcfmCcFrmLossBuff * EcfmSnmpLwGetFrmLossEntry PROTO ((UINT4, UINT4,UINT2,UINT4, UINT4));

/*cfmutil.c*/
UINT1 EcfmUtilQueryBitListTable PROTO ((UINT1 u1Value, UINT1 u1Position));
PUBLIC VOID  EcfmUtilEpochToDate PROTO ((UINT4, UINT1 *));
PUBLIC UINT4 EcfmUtilGetTimeFromEpoch PROTO ((VOID));
PUBLIC INT4 EcfmUtilModuleStart PROTO ((VOID));
PUBLIC VOID EcfmUtilModuleShutDown PROTO ((VOID));
PUBLIC INT4 EcfmUtilModuleEnable PROTO ((VOID));
PUBLIC VOID EcfmUtilModuleDisable PROTO ((VOID));
PUBLIC INT4 EcfmUtilY1731Enable PROTO ((VOID));
PUBLIC VOID EcfmUtilY1731Disable PROTO ((VOID));
PUBLIC INT4 EcfmLbLtUtilValidateRMepMacAddress PROTO ((tEcfmCcMepInfo *,tMacAddr ));
PUBLIC tEcfmCcFrmLossBuff * EcfmGetNextFrmLossNodeForATrans PROTO ((tEcfmCcFrmLossBuff *));
PUBLIC tEcfmCcFrmLossBuff * EcfmSnmpLwGetFrmLossTransEntry (UINT4 , UINT4 , UINT2 ,UINT4 );
PUBLIC INT4 EcfmSnmpLwGetNextIndexFlStatsTbl (UINT4 ,
                                    UINT4 *,
                                    UINT4 ,
                                    UINT4 *,
                                    UINT4 ,
                                    UINT4 *,
                                    UINT4 ,
                                    UINT4 *);
PUBLIC INT4 EcfmSnmpLwGetFirstIndexFlStatsTbl (UINT4 *,
                                    UINT4 *,
                                    UINT4 *,
                                    UINT4 *);
PUBLIC INT4 EcfmUtilY1731EnableForAPort PROTO ((tEcfmCcPortInfo * ));
PUBLIC VOID EcfmUtilY1731DisableForAPort PROTO ((tEcfmCcPortInfo * ));
PUBLIC INT4 EcfmUtilModuleEnableForAPort PROTO ((tEcfmCcPortInfo * ));
PUBLIC INT4 EcfmUtilModuleDisableForAPort PROTO ((tEcfmCcPortInfo * ));
PUBLIC INT4 EcfmUtilGetIfInfo PROTO ((UINT4, tCfaIfInfo *));
PUBLIC BOOL1 EcfmSnmpLwTestLmTxParam (UINT4 , UINT4 , UINT2 );
PUBLIC VOID EcfmSnmpLwGetLmTransactionStats PROTO ((tEcfmCcFrmLossBuff *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 *,
                                                    UINT4 * ));
PUBLIC VOID EcfmSnmpLwConfDropEnable PROTO ((UINT4 , BOOL1 ));
PUBLIC VOID EcfmSnmpLwConfVlanPriority PROTO ((UINT4 , UINT1 ));
#ifdef LLDP_WANTED
PUBLIC INT4 EcfmLldpRedRcvPktFromRm PROTO ((UINT1, tRmMsg *, UINT2));
#endif
PUBLIC INT4 EcfmGetRmepMacAddr (UINT4 , UINT4 ,UINT2 , UINT2 , UINT1 *);
/* Registration with ECFM */
PUBLIC VOID EcfmNotifyProtocols PROTO((tEcfmEventNotification *, UINT1));
/* Stubs for testing the APIs and External PDU formation */
PUBLIC VOID EcfmInitiateStubsForApiTest PROTO((UINT1));
PUBLIC VOID EcfmInitiateStubsForPduTest PROTO((UINT1));
PUBLIC VOID EcfmRegDereg PROTO((UINT1));
PUBLIC VOID EcfmUtilSetSenderIdTlv PROTO ((UINT1, UINT1**));
PUBLIC VOID EcfmUtilDecodeAsn1BER PROTO ((UINT1 *, UINT4, UINT4, UINT4 *, UINT4*));
PUBLIC VOID EcfmUtilEncodeAsn1BER PROTO ((UINT4 *, UINT4, UINT1 *, UINT1*));
PUBLIC VOID EcfmUtilPrepareNotification PROTO ((UINT4 u4EventId, tEcfmMepInfoParams * pMepInfo,
                                                tMacAddr * pMacAddr, UINT4 u4Var, UINT1 u1Flag,
                                                UINT1 u1Offset, tEcfmBufChainHeader * pu1DataPtr,
                                                UINT1 u1CfmPduOffset, UINT1 *pu1OuiPtr,
                                                UINT1 u1SubOpcodeVal, UINT1 u1TaskId));
PUBLIC VOID EcfmFormatCcTaskPduEthHdr PROTO ((tEcfmCcMepInfo *, UINT1 **, UINT2 , UINT1));
PUBLIC VOID EcfmFormatLbLtTaskPduEthHdr PROTO ((tEcfmLbLtMepInfo *, UINT1 **,
                                                UINT2 , UINT1 ));
PUBLIC tRBTree EcfmRBTreeCreateEmbedded PROTO ((UINT4, tRBCompareFn));

PUBLIC VOID
EcfmUtilIndicateProtocol PROTO ((tEcfmEventNotification * 
     pEcfmEventNotification,
     UINT4 u4EntId, UINT4 u4Event));

PUBLIC INT4
EcfmUtilGetMepInfoFromMaVlans PROTO ((tEcfmLbLtPduSmInfo * pPduSmInfo));

PUBLIC VOID EcfmCcUtilRestartAisTimer PROTO ((tEcfmCcMdInfo *));

PUBLIC INT4
EcfmUtilCompareMEGID PROTO ((UINT1 *, tEcfmCcMepInfo *));

#ifdef MBSM_WANTED
PUBLIC INT4 EcfmMbsmIsPortPresent PROTO ((UINT4, UINT4, UINT1));
PUBLIC VOID EcfmMbsmHandleLcStatusChg PROTO ((tMbsmProtoMsg *, UINT4 ));
#endif /*MBSM_WANTED*/
PUBLIC INT4 EcfmAHUtilChkPortFiltering PROTO ((UINT4, UINT1, UINT4, UINT1, tPbbTag *, UINT1));
PUBLIC tEcfmCcDefaultMdTableInfo *EcfmSnmpLwSetDefaultMdNode PROTO ((UINT4));
PUBLIC VOID EcfmSnmpLwProcessDefaultMdNode PROTO ((UINT4));
PUBLIC tEcfmCcDefaultMdTableInfo *EcfmCcSnmpLwGetDefaultMdEntry PROTO ((UINT4));
PUBLIC INT1 EcfmSnmpLwGetFirstIndexFsY1731MplstpExtRemoteMepTable PROTO ((
                                   UINT4 *, UINT4 *, UINT4 *, UINT4 *));
PUBLIC INT1 EcfmSnmpLwGetNextIndexFsY1731MplstpExtRemoteMepTable PROTO ((
                             UINT4, UINT4 *, UINT4, UINT4 *, 
                             UINT4, UINT4 *, UINT4, UINT4 *));
PUBLIC tEcfmLbLtDefaultMdTableInfo *EcfmLbLtSnmpLwGetDefaultMdEntry PROTO ((UINT4));
PUBLIC INT4 EcfmValidateMaNameStrFormat PROTO ((UINT1 *, UINT4 ));
PUBLIC VOID EcfmUtilMaNameOctetStrToStr PROTO ((tSNMP_OCTET_STRING_TYPE *,
                                                   INT4, UINT1 *));
PUBLIC VOID EcfmUtilMaNameOctetStrToMegId PROTO ((UINT1 *, UINT1, UINT1, 
                                                   UINT1 *, UINT1 *));
PUBLIC VOID EcfmUtilMdNameOctetStrToStr PROTO ((tSNMP_OCTET_STRING_TYPE *,
                                         INT4, UINT1 *));
/************************* cfmmptp.c **********************************/
PUBLIC INT4
EcfmMplsTpTxPacket PROTO ((tEcfmBufChainHeader *, tEcfmCcMepInfo *));

PUBLIC INT4 EcfmMpTpCcMepMplsPathCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 EcfmMpTpLbLtMepMplsPathCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 EcfmMpTpDelMaEntry PROTO ((tEcfmCcMepInfo *));

PUBLIC INT4 EcfmMpTpAddMaEntry PROTO ((tEcfmCcMepInfo *));

PUBLIC INT4 EcfmCcProcessMplsPathDown PROTO ((tEcfmCcMepInfo *));

PUBLIC INT4
EcfmCcProcessMplsPathStatusChg PROTO ((tEcfmMplsParams *, UINT1));

PUBLIC INT4
EcfmMpTpCcProcessPdu PROTO ((tEcfmBufChainHeader *, tEcfmMplsParams *));

PUBLIC INT4
EcfmProcessMpTpPdu PROTO ((tEcfmBufChainHeader *, UINT4));

PUBLIC INT4
EcfmMpTpLbLtProcessPdu PROTO ((tEcfmBufChainHeader *, tEcfmMplsParams *));

PUBLIC INT4
EcfmMplstpCcOffCreateTxMep PROTO ((tEcfmMplstpCcOffTxInfo *));

PUBLIC INT4
EcfmMplstpCcOffDeleteTxMep PROTO ((tEcfmMplstpCcOffTxInfo *));

PUBLIC INT4
EcfmMplstpCcOffCreateRxMep PROTO ((tEcfmMplstpCcOffRxInfo *));

PUBLIC INT4
EcfmMplstpCcOffDeleteRxMep PROTO ((tEcfmMplstpCcOffRxInfo *));


PUBLIC INT4
EcfmMpTpLbValidateLbm PROTO ((tEcfmLbLtPduSmInfo * pPduSmInfo));

PUBLIC INT4
EcfmMpTpLbValidateLbr PROTO ((tEcfmLbLtPduSmInfo *pPduSmInfo));

PUBLIC VOID
EcfmMpTpLbPutTrgtMepIdTlv PROTO ((tEcfmLbLtMepInfo *pMepInfo,
                                  UINT1 **ppu1LbPdu));
PUBLIC VOID
EcfmMpTpLbPutTrgtMipIdTlv PROTO ((tEcfmLbLtMepInfo *pMepInfo,
                                  UINT1 **ppu1LbPdu));
PUBLIC VOID
EcfmMpTpLbPutRqstMepIdTlv PROTO ((tEcfmLbLtMepInfo *pMepInfo,
                                  UINT1 **ppu1LbPdu));
PUBLIC VOID
EcfmMptpUtilGetY1731HdrOffSet PROTO ((tEcfmBufChainHeader * pBuf,
                                      UINT4 * pu4Offset));

PUBLIC VOID EcfmUtilPutMEGID PROTO ((tEcfmCcMepInfo *, UINT1 *));

PUBLIC VOID DummyBreakForLbLtReleaseContext PROTO ((VOID));
PUBLIC VOID DummyBreakForLbLtSelectContext PROTO ((VOID));
PUBLIC VOID DummyBreakForCcReleaseContext PROTO ((VOID));
PUBLIC VOID DummyBreakForCcSelectContext PROTO ((VOID));

/********************** cfmmdutil.c **************************************/
 
PUBLIC INT1 EcfmMdUtlGetAgMdTableNextIndex PROTO((UINT4 *));
PUBLIC INT1 EcfmMdUtlValAgMdTable PROTO((UINT4 ));
PUBLIC INT1 EcfmMdUtlGetNextIndexAgMdTable PROTO((UINT4 , UINT4 *));
PUBLIC INT1 EcfmMdUtlGetAgMdFormat PROTO((UINT4 ,INT4 *));
PUBLIC INT1 EcfmMdUtlGetAgMdName PROTO((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMdUtlGetAgMdMdLevel PROTO((UINT4 ,INT4 *));
PUBLIC INT1 EcfmMdUtlGetAgMdMhfCreation PROTO((UINT4 ,INT4 *));
PUBLIC INT1 EcfmMdUtlGetAgMdMhfIdPermission PROTO((UINT4 ,INT4 *));
PUBLIC INT1 EcfmMdUtlGetAgMdMaTableNextIndex PROTO((UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMdUtlGetAgMdRowStatus PROTO((UINT4 ,INT4 *));
PUBLIC INT1 EcfmMdUtlSetAgMdFormat PROTO((UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlSetAgMdName PROTO((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMdUtlSetAgMdMdLevel PROTO((UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlSetAgMdMhfCreation PROTO((UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlSetAgMdMhfIdPermission PROTO((UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlSetAgMdRowStatus PROTO((UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlTestv2AgMdFormat PROTO((UINT4 *  ,UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlTestv2AgMdName PROTO((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMdUtlTestv2AgMdMdLevel PROTO((UINT4 *  ,UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlTestv2AgMdMhfCreation PROTO((UINT4 *  ,UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlTestAgMdMhfIdPermission PROTO((UINT4 *  ,UINT4  ,INT4 ));
PUBLIC INT1 EcfmMdUtlTestv2AgMdRowStatus PROTO((UINT4 *  ,UINT4  ,INT4 ));
 
/******************** cfmmeputil.c ***************************************/
 
PUBLIC INT1 EcfmMepUtlValAgMepTable PROTO((UINT4  , UINT4  , UINT4 ));
PUBLIC INT1 EcfmMepUtlGetNextIndexAgMepTable PROTO((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepIfIndex PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDirection PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepPrimaryVid PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepActive PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepFngState PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepCciEnabled PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepCcmLtmPriority PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepMacAddress PROTO((UINT4  , UINT4  , UINT4 ,tMacAddr * ));
PUBLIC INT1 EcfmMepUtlGetAgMepLowPrDef PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepFngAlarmTime PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepFngResetTime PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepHighestPrDef PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDefects PROTO((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepErrCcmLastFail PROTO((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepXconCcmLastFail PROTO((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepCcmSeqErrors PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepCciSentCcms PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepNextLbmTransId PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepLbrIn PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepLbrInOutOfOrder PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepLbrBadMsdu PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepLtmNextSeqNo PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepUnexpLtrIn PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepLbrOut PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmStatus PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmDstMacAddr PROTO((UINT4  , UINT4  , UINT4 ,tMacAddr * ));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmDestMepId PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmDstIsMepId PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmMessages PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmDataTlv PROTO((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmVlanPri PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmVlanDropEna PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmResultOK PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLbmSeqNumber PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmStatus PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmFlags PROTO((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmTgtMacAddr PROTO((UINT4  , UINT4  , UINT4 ,tMacAddr * ));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmTgtMepId PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmTgtIsMepId PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmTtl PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmResult PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmSeqNumber PROTO((UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepTxLtmEgrId PROTO((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepRowStatus PROTO((UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlSetAgMepIfIndex PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepDirection PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepPrimaryVid PROTO((UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepActive PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepCciEnabled PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepCcmLtmPriority PROTO((UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepLowPrDef PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepFngAlarmTime PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepFngResetTime PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmStatus PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmDstMacAddr PROTO((UINT4  , UINT4  , UINT4  ,tMacAddr ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmDestMepId PROTO((UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmDstIsMepId PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmMessages PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmDataTlv PROTO((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmVlanPri PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLbmVlanDropEna PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmStatus PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmFlags PROTO((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmTgtMacAddr PROTO((UINT4  , UINT4  , UINT4  ,tMacAddr ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmTgtMepId PROTO((UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmTgtIsMepId PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmTtl PROTO((UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlSetAgMepTxLtmEgrId PROTO((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMepUtlSetAgMepRowStatus PROTO((UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepIfIndex PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepDirection PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepPrimaryVid PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepActive PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepCciEnabled PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepCcmLtmPri PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepLowPrDef PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepFngAlmTime PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepFngRstTime PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepTxLbmStatus PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmDstMacAddr PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tMacAddr ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmDestMepId PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmDstIsMepId PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmMessages PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmDataTlv PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmVlanPri PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLbmVlanDropEna PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepTxLtmStatus PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepTxLtmFlags PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLtmTgtMacAddr PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tMacAddr ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLtmTgtMepId PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLtmTgtIsMepId PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlTestv2AgMepTxLtmTtl PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));
PUBLIC INT1 EcfmMepUtlTstAgMepTxLtmEgrId PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 EcfmMepUtlTestv2AgMepRowStatus PROTO((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
PUBLIC INT1 EcfmMepUtlValAgLtrTable PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));
PUBLIC INT1 EcfmMepUtlGetNextIndexAgLtrTable PROTO((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrTtl PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrForwarded PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrTerminalMep PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrLastEgrId PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrNextEgrId PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrRelay PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrChassisIdSubtype PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrChassisId PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrManAddressDomain PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrManAddress PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrIngress PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrIngressMac PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrIngPortIdSubtype PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrIngressPortId PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrEgress PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrEgressMac PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrEgrPortIdSubtype PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgLtrEgressPortId PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgLtrOrgSpecificTlv PROTO((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlValAgMepDbTable PROTO((UINT4  , UINT4  , UINT4  , UINT4 ));
PUBLIC INT1 EcfmMepUtlGetNxtIdxAgMepDbTable PROTO((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbRMepState PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbRMepFailOkTime PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbMacAddress PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));
PUBLIC INT1 EcfmMepUtlGetAgMepDbRdi PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbPortStatTlv PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbInterfaceStatTlv PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbChassisIdSubtype PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
PUBLIC INT1 EcfmMepUtlGetAgMepDbChassisId PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepDbManAddrDom PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));
PUBLIC INT1 EcfmMepUtlGetAgMepDbManAddress PROTO((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
/********************** cfmmautl.c **************************************/
PUBLIC INT1 
EcfmUtlValdateMaMepListTable PROTO ((UINT4, UINT4, UINT4));
PUBLIC INT1
EcfmUtlGetNxtIdxMaMepListTable PROTO ((UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *));
PUBLIC INT1
EcfmUtlGetMaMepListRowStatus PROTO ((UINT4, UINT4, UINT4, INT4 *));
PUBLIC INT1
EcfmUtlSetMaMepListRowStatus PROTO ((UINT4, UINT4, UINT4, INT4));
PUBLIC INT1
EcfmUtlTestMaMepListRowStatus PROTO ((UINT4 *, UINT4, UINT4, UINT4, INT4));

PUBLIC INT1 
EcfmUtlValIndexInstMaNetTable PROTO ((UINT4, UINT4));
PUBLIC INT1 
EcfmUtlGetNextIndexMaNetTable PROTO ((UINT4, UINT4 *, UINT4, UINT4 *));
PUBLIC INT1 
EcfmUtlGetMaNetFormat PROTO ((UINT4, UINT4, INT4 *));
PUBLIC INT1 
EcfmUtlGetMaNetName PROTO ((UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 
EcfmUtlGetMaNetRowStatus PROTO ((UINT4, UINT4, INT4 *));
PUBLIC INT1 
EcfmUtlSetMaNetFormat PROTO ((UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlSetMaNetName PROTO ((UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 
EcfmUtlSetMaNetCcmInterval PROTO ((UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlSetMaNetRowStatus PROTO ((UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTestv2CfmMaNetFormat PROTO ((UINT4 *, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTestv2MaNetName PROTO ((UINT4 *, UINT4, UINT4, 
                               tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 
EcfmUtlTestv2MaNetCcmInterval PROTO ((UINT4 *, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTestv2MaNetRowStatus PROTO ((UINT4 *, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlValIndexInstMaCompTable PROTO ((UINT4, UINT4, UINT4));
PUBLIC INT1 
EcfmUtlGetNextIndexMaCompTable PROTO ((UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *));
PUBLIC INT1 
EcfmUtlGetMaCompPriSelType PROTO ((UINT4, UINT4, UINT4, INT4 *));
PUBLIC INT1 
EcfmUtlGetMaCompPriSelectOrNone PROTO ((UINT4, UINT4, UINT4, UINT4 *));
PUBLIC INT1 
EcfmUtlGetMaCompMhfCreation PROTO ((UINT4, UINT4, UINT4, INT4 *));
PUBLIC INT1 
EcfmUtlGetMaCompIdPermission PROTO ((UINT4, UINT4, UINT4, INT4 *));
PUBLIC INT1 
EcfmUtlGetMaCompNumberOfVids PROTO ((UINT4, UINT4, UINT4, UINT4 *));
PUBLIC INT1 
EcfmUtlGetMaCompRowStatus PROTO ((UINT4, UINT4, UINT4, INT4 *));
PUBLIC INT1
EcfmUtlSetMaCompPriSelType PROTO ((UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlSetMaCompPriSelorNone PROTO ((UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlSetMaCompMhfCreation PROTO ((UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlSetMaCompIdPermission PROTO ((UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlSetMaCompNumberOfVids PROTO ((UINT4, UINT4, UINT4, UINT4));
PUBLIC INT1 
EcfmUtlSetMaCompRowStatus PROTO ((UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTestv2MaCompNumberOfVids PROTO ((UINT4 *, UINT4, UINT4, UINT4, UINT4));
PUBLIC INT1
EcfmUtlTestMaCompPriSelType PROTO ((UINT4 *,UINT4 ,UINT4 ,UINT4 ,INT4 ));
PUBLIC INT1 
EcfmUtlTestv2MaCompPriSelOrNone PROTO ((UINT4 *, UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTstv2CfmMaCompMhfCreation PROTO ((UINT4 *, UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTestv2MaCompIdPermission PROTO ((UINT4 *, UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlTestv2MaCompRowStatus PROTO ((UINT4 *, UINT4, UINT4, UINT4, INT4));
PUBLIC INT1 
EcfmUtlGetMaNetCcmInterval PROTO ((UINT4, UINT4, INT4 *));

/* cfmavlinit.c */
PUBLIC UINT4
EcfmCcClntAvlbltyInitiator PROTO ((tEcfmCcPduSmInfo * pPduSmInfo, UINT1 u1Event));
PUBLIC VOID
EcfmCcAvlbltyStopTransaction PROTO (( tEcfmCcPduSmInfo *pPduSmInfo));
PUBLIC VOID
EcfmCcMeasureAvlblty PROTO (( tEcfmCcPduSmInfo *pPduSmInfo,
                              FLT4              f4FrmLossRatio));
PUBLIC VOID
EcfmCcAvlbltyResetAlgoInfo PROTO (( tEcfmCcAvlbltyInfo *pAvlbltyNewNode));

INT4  EcfmIsPWInterface (INT4 i4PortId);
INT4  EcfmCreatePwInterface (INT4 i4PortId);
INT4  EcfmDeletePwInterface (INT4 i4PortId);
VOID EcfmCcAssignMempoolIds PROTO ((VOID));
VOID EcfmLbltAssignMempoolIds PROTO ((VOID));

#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
/*RBTree Utilities*/
/*Global CC LocalPort RBTREE*/
INT4
EcfmCcLocalPortTableGlobalCmp (tRBElem * pRBElem1,
                                 tRBElem *pRBElem2);
INT4
EcfmAddCcLocalPortEntry (tEcfmCcPortInfo *pPortEntry);
INT4
EcfmDelCcLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId);
tEcfmCcPortInfo*
EcfmGetCcLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId);
tEcfmCcPortInfo*
EcfmGetNextCcLocalPortEntry (UINT4 u4ContextId, UINT2 u2PortId);
INT4
EcfmAddSinglePortInDB (UINT4 u4ContextId, UINT2 u2PortNum, UINT2 u2Flag);
INT4
EcfmResetSinglePortInDB (UINT4 u4ContextId, UINT2 u2PortNum, UINT2 u2Flag);
VOID
EcfmGetPortsFromDB (UINT4 u4ContextId, UINT2 u2Flag, tLocalPortListExt LocalPortList);
#endif

INT4
EcfmCreateHwLoopback (tEcfmCcMepInfo * pEcfmCcMepInfo);

INT4
EcfmDeleteHwLoopback (tEcfmCcMepInfo * pEcfmCcMepInfo);

#endif /* _CFM_PRT_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmprot.h                      */
/*-----------------------------------------------------------------------*/
