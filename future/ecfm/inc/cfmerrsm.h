/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmerrsm.h,v 1.3 2007/11/19 07:21:10 iss Exp $
 * 
 * Description: This file contains the functionality of the
 *              MEP Error state machine.
 *********************************************************************/
#ifndef __CFMERRSM_H_
#define __CFMERRSM_H_

/* routines that handles the occurence of the events */

/* XCON State machine */
PRIVATE INT4
EcfmRmepErrSmSetStateDefault PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmRmepErrSmSetStateNoDefect PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmRmepErrSmSetStateDefect PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmRmepErrSmSetStateDefFrmDef PROTO((tEcfmCcPduSmInfo * ));
PRIVATE INT4
EcfmRmepErrSmEvtImpossible PROTO((tEcfmCcPduSmInfo * ));

/* Enums for the routines to be retrieve from the the matrix*/
enum
{
   ERR0,               /* EcfmRmepErrSmSetStateDefault */
   ERR1,               /* EcfmRmepErrSmSetStateDefect */
   ERR2,               /* EcfmRmepErrSmSetStateDefFrmDef */
   ERR3,               /* EcfmRmepErrSmSetStateNoDefect */
   ERR4,               /* EcfmRmepErrSmEvtImpossible */
   ECFM_RMEP_ERR_MAX_FN_PTRS
};

/* function pointers of  RMEP ERROR State machine */
INT4 (*gaEcfmRmepErrActionProc[ECFM_RMEP_ERR_MAX_FN_PTRS]) (tEcfmCcPduSmInfo *) =
{
   EcfmRmepErrSmSetStateDefault,
   EcfmRmepErrSmSetStateDefect,
   EcfmRmepErrSmSetStateDefFrmDef,
   EcfmRmepErrSmSetStateNoDefect,
   EcfmRmepErrSmEvtImpossible
};

/* State Event Table of RMEP ERROR RX */
const UINT1
gau1EcfmRmepErrSem[ECFM_SM_RMEP_ERR_MAX_EVENTS][ECFM_RMEP_ERR_MAX_STATES] =
/*  DEFAULT NODEFECT DEFECT       States    */
{                                /* Events */
  {    ERR3,ERR4, ERR4},    /* ECFM_SM_EV_BEGIN */
  {    ERR4,ERR0, ERR0},    /*ECFM_SM_EV_MEP_NOT_ACTIVE */
  {    ERR4,ERR1, ERR2},    /*ECFM_SM_EV_RMEP_ERR_CCM_RCVD */
  {    ERR4,ERR4, ERR3},    /*ECFM_SM_EV_RMEP_ERR_TIMEOUT */
};
#endif /* _CFMERRSM_H_ */


/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmerrsm.h                      */
/*-----------------------------------------------------------------------*/

