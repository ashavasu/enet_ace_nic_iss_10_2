/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: cfmv2ext.h,v 1.2 2011/12/28 12:58:42 siva Exp $
*
* Description: Protocol Mib Tanslation Table
*********************************************************************/

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_ext_table[] = {
"ccitt",        "0",
"iso",        "1",
"lldpExtensions",        "1.0.8802.1.1.2.1.5",
"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000",
"org",        "1.3",
"dod",        "1.3.6",
"internet",        "1.3.6.1",
"directory",        "1.3.6.1.1",
"mgmt",        "1.3.6.1.2",
"mib-2",        "1.3.6.1.2.1",
"ip",        "1.3.6.1.2.1.4",
"transmission",        "1.3.6.1.2.1.10",
"mplsStdMIB",        "1.3.6.1.2.1.10.166",
"rmon",        "1.3.6.1.2.1.16",
"statistics",        "1.3.6.1.2.1.16.1",
"history",        "1.3.6.1.2.1.16.2",
"hosts",        "1.3.6.1.2.1.16.4",
"matrix",        "1.3.6.1.2.1.16.6",
"filter",        "1.3.6.1.2.1.16.7",
"tokenRing",        "1.3.6.1.2.1.16.10",
"dot1dBridge",        "1.3.6.1.2.1.17",
"dot1dStp",        "1.3.6.1.2.1.17.2",
"dot1dTp",        "1.3.6.1.2.1.17.4",
"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1",
"dns",        "1.3.6.1.2.1.32",
"experimental",        "1.3.6.1.3",
"private",        "1.3.6.1.4",
"enterprises",        "1.3.6.1.4.1",
"ARICENT-MPLS-TP-MIB",        "1.3.6.1.4.1.2076.13.4",
"issExt",        "1.3.6.1.4.1.2076.81.8",
"fsDot1dBridge",        "1.3.6.1.4.1.2076.116",
"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2",
"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4",
"security",        "1.3.6.1.5",
"snmpV2",        "1.3.6.1.6",
"snmpDomains",        "1.3.6.1.6.1",
"snmpProxys",        "1.3.6.1.6.2",
"snmpModules",        "1.3.6.1.6.3",
"snmpTraps",        "1.3.6.1.6.3.1.1.5",
"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1",
"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2",
"ieee802dot1mibs",        "1.3.111.2.802.1.1",
"ieee8021CfmV2Mib",        "1.3.111.2.802.1.1.8",
"dot1agNotifications",        "1.3.111.2.802.1.1.8.0",
"dot1agMIBObjects",        "1.3.111.2.802.1.1.8.1",
"dot1agCfmStack",        "1.3.111.2.802.1.1.8.1.1",
"ieee8021CfmStackTable",        "1.3.111.2.802.1.1.8.1.1.2",
"ieee8021CfmStackEntry",        "1.3.111.2.802.1.1.8.1.1.2.1",
"ieee8021CfmStackifIndex",        "1.3.111.2.802.1.1.8.1.1.2.1.1",
"ieee8021CfmStackServiceSelectorType",        "1.3.111.2.802.1.1.8.1.1.2.1.2",
"ieee8021CfmStackServiceSelectorOrNone",        "1.3.111.2.802.1.1.8.1.1.2.1.3",
"ieee8021CfmStackMdLevel",        "1.3.111.2.802.1.1.8.1.1.2.1.4",
"ieee8021CfmStackDirection",        "1.3.111.2.802.1.1.8.1.1.2.1.5",
"ieee8021CfmStackMdIndex",        "1.3.111.2.802.1.1.8.1.1.2.1.6",
"ieee8021CfmStackMaIndex",        "1.3.111.2.802.1.1.8.1.1.2.1.7",
"ieee8021CfmStackMepId",        "1.3.111.2.802.1.1.8.1.1.2.1.8",
"ieee8021CfmStackMacAddress",        "1.3.111.2.802.1.1.8.1.1.2.1.9",
"dot1agCfmDefaultMd",        "1.3.111.2.802.1.1.8.1.2",
"ieee8021CfmDefaultMdTable",        "1.3.111.2.802.1.1.8.1.2.5",
"ieee8021CfmDefaultMdEntry",        "1.3.111.2.802.1.1.8.1.2.5.1",
"ieee8021CfmDefaultMdComponentId",        "1.3.111.2.802.1.1.8.1.2.5.1.1",
"ieee8021CfmDefaultMdPrimarySelectorType",        "1.3.111.2.802.1.1.8.1.2.5.1.2",
"ieee8021CfmDefaultMdPrimarySelector",        "1.3.111.2.802.1.1.8.1.2.5.1.3",
"ieee8021CfmDefaultMdStatus",        "1.3.111.2.802.1.1.8.1.2.5.1.4",
"ieee8021CfmDefaultMdLevel",        "1.3.111.2.802.1.1.8.1.2.5.1.5",
"ieee8021CfmDefaultMdMhfCreation",        "1.3.111.2.802.1.1.8.1.2.5.1.6",
"ieee8021CfmDefaultMdIdPermission",        "1.3.111.2.802.1.1.8.1.2.5.1.7",
"dot1agCfmVlan",        "1.3.111.2.802.1.1.8.1.3",
"ieee8021CfmVlanTable",        "1.3.111.2.802.1.1.8.1.3.2",
"ieee8021CfmVlanEntry",        "1.3.111.2.802.1.1.8.1.3.2.1",
"ieee8021CfmVlanComponentId",        "1.3.111.2.802.1.1.8.1.3.2.1.1",
"ieee8021CfmVlanSelector",        "1.3.111.2.802.1.1.8.1.3.2.1.3",
"ieee8021CfmVlanPrimarySelector",        "1.3.111.2.802.1.1.8.1.3.2.1.5",
"ieee8021CfmVlanRowStatus",        "1.3.111.2.802.1.1.8.1.3.2.1.6",
"dot1agCfmConfigErrorList",        "1.3.111.2.802.1.1.8.1.4",
"ieee8021CfmConfigErrorListTable",        "1.3.111.2.802.1.1.8.1.4.2",
"ieee8021CfmConfigErrorListEntry",        "1.3.111.2.802.1.1.8.1.4.2.1",
"ieee8021CfmConfigErrorListSelectorType",        "1.3.111.2.802.1.1.8.1.4.2.1.1",
"ieee8021CfmConfigErrorListSelector",        "1.3.111.2.802.1.1.8.1.4.2.1.2",
"ieee8021CfmConfigErrorListIfIndex",        "1.3.111.2.802.1.1.8.1.4.2.1.3",
"ieee8021CfmConfigErrorListErrorType",        "1.3.111.2.802.1.1.8.1.4.2.1.4",
"dot1agCfmMd",        "1.3.111.2.802.1.1.8.1.5",
"dot1agCfmMa",        "1.3.111.2.802.1.1.8.1.6",
"ieee8021CfmMaCompTable",        "1.3.111.2.802.1.1.8.1.6.4",
"ieee8021CfmMaCompEntry",        "1.3.111.2.802.1.1.8.1.6.4.1",
"ieee8021CfmMaComponentId",        "1.3.111.2.802.1.1.8.1.6.4.1.1",
"ieee8021CfmMaCompPrimarySelectorType",        "1.3.111.2.802.1.1.8.1.6.4.1.2",
"ieee8021CfmMaCompPrimarySelectorOrNone",        "1.3.111.2.802.1.1.8.1.6.4.1.3",
"ieee8021CfmMaCompMhfCreation",        "1.3.111.2.802.1.1.8.1.6.4.1.4",
"ieee8021CfmMaCompIdPermission",        "1.3.111.2.802.1.1.8.1.6.4.1.5",
"ieee8021CfmMaCompNumberOfVids",        "1.3.111.2.802.1.1.8.1.6.4.1.6",
"ieee8021CfmMaCompRowStatus",        "1.3.111.2.802.1.1.8.1.6.4.1.7",
"dot1agCfmMep",        "1.3.111.2.802.1.1.8.1.7",
"dot1agCfmConformance",        "1.3.111.2.802.1.1.8.2",
"dot1agCfmCompliances",        "1.3.111.2.802.1.1.8.2.1",
"dot1agCfmGroups",        "1.3.111.2.802.1.1.8.2.2",
"joint-iso-ccitt",        "2",
0,0
};

#endif /* _SNMP_MIB_H */
