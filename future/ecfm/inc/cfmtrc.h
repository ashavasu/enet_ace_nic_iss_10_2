/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmtrc.h,v 1.12 2014/12/09 12:47:21 siva Exp $
 *
 * Description: This file contains macros for tracing and debugging.
 *************************************************************************/
#ifndef _ECFM_TRC_H
#define _ECFM_TRC_H
PUBLIC VOID EcfmGlobalTrace PROTO ((UINT4, UINT4, const char *, ...));
/* Trace and debug flags */
#define   ECFM_CC_TRC_FLAG           (ECFM_CC_TRACE_OPTION)
#define   ECFM_LBLT_TRC_FLAG         (ECFM_LBLT_TRACE_OPTION)
#define   ECFM_GLB_TRC_FLAG          (ECFM_GLB_TRC_OPTION) 
/* Module names */
#define   ECFM_CC_MOD_NAME           ((const char *)"ECFM-CC  ")
#define   ECFM_LBLT_MOD_NAME         ((const char *)"ECFM-LBLT")
#define   ECFM_MOD_NAME              ((const char *)"ECFM     ")

#ifdef MI_WANTED
#define ECFM_CC_TRC_CONTEXT(x) "Context-%u:" x, ECFM_CC_CURR_CONTEXT_ID()
#define ECFM_LBLT_TRC_CONTEXT(x) "Context-%u:" x, ECFM_LBLT_CURR_CONTEXT_ID()
#else
#define ECFM_CC_TRC_CONTEXT(x)  x
#define ECFM_LBLT_TRC_CONTEXT(x)  x
#endif

extern UINT4 gu4EcfmTrcLvl; 
#define ECFM_TRC_LVL        gu4EcfmTrcLvl

/* Trace definitions */
#ifdef  TRACE_WANTED

#ifdef __GNUC__
/* Function Entry/Exit Trace for CC Task*/
#define  ECFM_CC_TRC_FN_ENTRY()  \
        if (ECFM_CC_TRC_FLAG & ECFM_FN_ENTRY_TRC) \
        {\
            MOD_FN_ENTRY (ECFM_CC_TRC_FLAG, ECFM_FN_ENTRY_TRC, \
                                                     ECFM_CC_MOD_NAME);\
        }

#define  ECFM_CC_TRC_FN_EXIT()   \
        if (ECFM_CC_TRC_FLAG & ECFM_FN_EXIT_TRC) \
        {\
            MOD_FN_EXIT (ECFM_CC_TRC_FLAG, ECFM_FN_EXIT_TRC, \
                                                     ECFM_CC_MOD_NAME);\
        }
/* Function Entry/Exit Trace for LBLT Task*/
#define  ECFM_LBLT_TRC_FN_ENTRY()  \
    if (ECFM_LBLT_TRC_FLAG & ECFM_FN_ENTRY_TRC) \
        {\
            MOD_FN_ENTRY (ECFM_LBLT_TRC_FLAG, ECFM_FN_ENTRY_TRC, \
                                                     ECFM_LBLT_MOD_NAME);\
        }
#define  ECFM_LBLT_TRC_FN_EXIT()   \
    if (ECFM_LBLT_TRC_FLAG & ECFM_FN_EXIT_TRC) \
        {\
            MOD_FN_EXIT (ECFM_LBLT_TRC_FLAG, ECFM_FN_EXIT_TRC, \
                                                     ECFM_LBLT_MOD_NAME);\
        }
#else
/* Function Entry/Exit Trace for CC Task*/
#define  ECFM_CC_TRC_FN_ENTRY()  \
        if (ECFM_CC_TRC_FLAG & ECFM_FN_ENTRY_TRC) \
        {\
         printf ("ECFM-CC  : %s Entered at line %d\n", __FILE__, __LINE__);\
        }

#define  ECFM_CC_TRC_FN_EXIT()   \
        if (ECFM_CC_TRC_FLAG & ECFM_FN_EXIT_TRC) \
        {\
         printf ("ECFM-CC  : %s Exiting at %d\n", __FILE__,  __LINE__);\
        }
/* Function Entry/Exit Trace for LBLT Task*/
#define  ECFM_LBLT_TRC_FN_ENTRY()  \
    if (ECFM_LBLT_TRC_FLAG & ECFM_FN_ENTRY_TRC) \
        {\
         printf ("ECFM-LBLT: %s Entered at line %d\n", __FILE__,  __LINE__);\
        }

#define  ECFM_LBLT_TRC_FN_EXIT()   \
    if (ECFM_LBLT_TRC_FLAG & ECFM_FN_EXIT_TRC) \
        {\
         printf ("ECFM-LBLT: %s Exiting at %d\n", __FILE__,  __LINE__);\
        }
#endif  /* __GNUC__ */

/* Common trace for both CC and LBLT task*/
#define ECFM_GLB_TRC  EcfmGlobalTrace
#define ECFM_GLB_PKT_DUMP(TraceType, pBuf, Length, Str)\
        MOD_PKT_DUMP(ECFM_GLB_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME, pBuf, Length,\
        Str)
/* Trace for CC Task*/
#define ECFM_CC_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(ECFM_CC_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME, pBuf, Length,\
        ECFM_CC_TRC_CONTEXT(Str))

#define ECFM_CC_TRC(TraceType, Str)                                              \
        MOD_TRC(ECFM_CC_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME, ECFM_CC_TRC_CONTEXT(Str))

#define ECFM_CC_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(ECFM_CC_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME,\
        ECFM_CC_TRC_CONTEXT(Str),Arg1)

#define ECFM_CC_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(ECFM_CC_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME, \
        ECFM_CC_TRC_CONTEXT(Str),Arg1, Arg2)

#define ECFM_CC_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(ECFM_CC_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME,\
        ECFM_CC_TRC_CONTEXT(Str), Arg1, Arg2, Arg3)
/* MPLS_TP_OAM_CHANGE_BEGIN */
#define ECFM_CC_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)\
        MOD_TRC_ARG4(ECFM_CC_TRC_FLAG, TraceType, ECFM_CC_MOD_NAME,\
        ECFM_CC_TRC_CONTEXT(Str), Arg1, Arg2, Arg3, Arg4)
/* MPLS_TP_OAM_CHANGE_END */

#define   ECFM_CC_DEBUG(x) {x}
/* Trace for LBLT Task*/

#define ECFM_LBLT_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(ECFM_LBLT_TRC_FLAG, TraceType, ECFM_LBLT_MOD_NAME, pBuf, Length,\
        ECFM_LBLT_TRC_CONTEXT(Str))

#define ECFM_LBLT_TRC(TraceType, Str)                                              \
        MOD_TRC(ECFM_LBLT_TRC_FLAG, TraceType, ECFM_LBLT_MOD_NAME, ECFM_LBLT_TRC_CONTEXT(Str))

#define ECFM_LBLT_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(ECFM_LBLT_TRC_FLAG, TraceType, ECFM_LBLT_MOD_NAME,\
        ECFM_LBLT_TRC_CONTEXT(Str),Arg1)

#define ECFM_LBLT_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(ECFM_LBLT_TRC_FLAG, TraceType, ECFM_LBLT_MOD_NAME, \
        ECFM_LBLT_TRC_CONTEXT(Str),Arg1, Arg2)

#define ECFM_LBLT_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(ECFM_LBLT_TRC_FLAG, TraceType, ECFM_LBLT_MOD_NAME,\
        ECFM_LBLT_TRC_CONTEXT(Str), Arg1, Arg2, Arg3)

#define   ECFM_LBLT_DEBUG(x) {x}
#else  /* TRACE_WANTED */

/* Trace Macros used in CC Task*/
#define ECFM_GLB_TRC  EcfmGlobalTrace
#define ECFM_CC_TRC_FN_ENTRY()
#define ECFM_CC_TRC_FN_EXIT()
#define ECFM_CC_PKT_DUMP(TraceType, pBuf, Length, Str)
#define ECFM_CC_TRC(TraceType, Str)
#define ECFM_CC_TRC_ARG1(TraceType, Str, Arg1)
#define ECFM_CC_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define ECFM_CC_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define ECFM_CC_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)
#define ECFM_CC_DEBUG(x)
/* Trace Macros used in LBLT Task*/
#define ECFM_LBLT_TRC_FN_ENTRY()
#define ECFM_LBLT_TRC_FN_EXIT()
#define ECFM_LBLT_PKT_DUMP(TraceType, pBuf, Length, Str)
#define ECFM_LBLT_TRC(TraceType, Str)
#define ECFM_LBLT_TRC_ARG1(TraceType, Str, Arg1)
#define ECFM_LBLT_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define ECFM_LBLT_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define ECFM_GLB_PKT_DUMP(TraceType, pBuf, Length, Str)
#define ECFM_LBLT_DEBUG(x)


#endif /* TRACE_WANTED */

#endif/* _ECFM_TRC_H */

/*******************************************************************************
                            End of File cfmtrc.h                              
 ******************************************************************************/

