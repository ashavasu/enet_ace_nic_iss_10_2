/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmport.h,v 1.33 2016/02/12 10:47:45 siva Exp $
 * 
 * Description: This file contains the prototypes for all the 
 *              PUBLIC procedures.
 *********************************************************************/

#ifndef _CFM_PORT_H
#define _CFM_PORT_H
/* MPLS_TP_OAM_CHANGE_BEGIN */
#include "mplsapi.h"
/* MPLS_TP_OAM_CHANGE_END */
PUBLIC INT4 EcfmVcmGetAliasName PROTO ((UINT4, UINT1 *));
PUBLIC INT4 EcfmVcmGetContextInfoFromIfIndex PROTO ((UINT4, UINT4 *,UINT2 *));
#ifdef L2RED_WANTED
#ifdef ELMI_WANTED
PUBLIC INT4 EcfmElmRedRcvPktFromRm PROTO ((UINT1, tRmMsg *, UINT2));
#endif
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
PUBLIC INT4 EcfmSnoopRedRcvPktFromRm PROTO ((UINT1, tRmMsg *, UINT2));
#endif

PUBLIC UINT4 EcfmRmEnqMsgToRmFromAppl PROTO ((tRmMsg *, UINT2, UINT4,UINT4));
PUBLIC UINT4 EcfmRmGetNodeState PROTO ((VOID));
PUBLIC UINT1 EcfmRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
PUBLIC UINT4 EcfmRmRegisterProtocols PROTO ((tRmRegParams *));
PUBLIC UINT4 EcfmRmDeRegisterProtocols PROTO ((VOID));
PUBLIC UINT4 EcfmRmReleaseMemoryForMsg PROTO ((UINT1 *));
PUBLIC INT4 EcfmRmSetBulkUpdatesStatus PROTO ((UINT4));
#endif

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
#include "y1564.h"
PUBLIC INT4 EcfmPortHandleExtInteraction PROTO ((tEcfmLbLtMsg *));
#endif

PUBLIC INT4 EcfmVlanGetFwdPortList PROTO ((UINT4, UINT2, tMacAddr,tMacAddr, tVlanId, tLocalPortListExt));
PUBLIC VOID EcfmSetPortLockStatus PROTO ((UINT4, UINT4, BOOL1));
PUBLIC INT4 EcfmVlanTransmitCfmFrame PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4,UINT2, tVlanTag, UINT1));
PUBLIC INT4
EcfmVlanIsMemberPort (UINT4 u4ContextId, UINT2 u2Port,tVlanTag VlanTagInfo);
PUBLIC INT4 EcfmL2IwfHandleOutgoingPktOnPort PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT2, UINT4, UINT2, UINT1));
PUBLIC BOOL1 EcfmL2IwfMiIsVlanMemberPort PROTO ((UINT4, tVlanId, UINT4));
PUBLIC INT4 EcfmL2IwfIsPortInPortChannel PROTO ((UINT4));
PUBLIC INT4 EcfmL2IwfGetPortChannelForPort PROTO ((UINT2, UINT2 *));
PUBLIC UINT1 EcfmL2IwfGetVlanPortState PROTO ((UINT4, UINT4));
PUBLIC INT4 EcfmL2IwfGetNextValidPortForContext PROTO ((UINT4, UINT2,UINT2 *, UINT4 *));
PUBLIC INT4 EcfmAstIsMstEnabledInContext PROTO ((UINT4));
PUBLIC INT4 EcfmVcmGetSystemMode PROTO ((UINT2));
PUBLIC INT4 EcfmVcmGetSystemModeExt PROTO ((UINT2));
PUBLIC INT4 EcfmGenerateRandPseudoBytes PROTO ((UINT1 *));
PUBLIC VOID EcfmCfaGetSysMacAddress PROTO ((tMacAddr));
PUBLIC VOID EcfmCfaGetContextMacAddr PROTO ((UINT4, tMacAddr ));
PUBLIC INT4 EcfmVlanGetVlanInfoFromFrame PROTO ((UINT4, UINT2,tCRU_BUF_CHAIN_DESC *, tVlanTag *,UINT1 *, UINT4 *));
PUBLIC INT4 EcfmVlanGetTagLenInFrame PROTO ((tCRU_BUF_CHAIN_DESC *, UINT4, UINT4 *));
PUBLIC VOID EcfmGetPacketCounters PROTO((UINT4, UINT4,UINT4 *,UINT4 *));
PUBLIC INT4 EcfmCfaGetIfInfo PROTO ((UINT4, tCfaIfInfo *));
PUBLIC INT4 EcfmCfaCliConfGetIfName PROTO ((UINT4, INT1 *));
PUBLIC BOOL1 EcfmL2IwfMiIsVlanActive PROTO ((UINT4,tVlanId));
PUBLIC INT4 EcfmL2IwfGetBridgeMode PROTO ((UINT4 , UINT4 *));
PUBLIC INT4 EcfmL2IwfMiIsVlanUntagMemberPort PROTO((UINT4 ,UINT2,UINT4));
PUBLIC INT4 EcfmL2IwfMiGetVlanEgressPorts PROTO ((UINT4 ,UINT2,tPortListExt));
PUBLIC INT4 EcfmValidateMdNameFormat PROTO ((UINT1 *,INT4,UINT1,UINT4 *));
PUBLIC INT4 EcfmValidateMaNameFormat PROTO ((UINT1 *,INT4,UINT1, tEcfmCcMdInfo
*, UINT4 *));
PUBLIC UINT4 EcfmCfaGetInterfaceNameFromIndex PROTO ((UINT4, UINT1 *));
PUBLIC INT4 EcfmPortRegisterWithIp PROTO ((VOID));
PUBLIC INT4 EcfmPortDeRegisterWithIp PROTO ((VOID));
PUBLIC INT4 EcfmL2IwfGetTagInfoFromFrame PROTO ((UINT4, UINT4,
                                            tCRU_BUF_CHAIN_DESC *, 
                                            tVlanTag *,tPbbTag *,UINT1 *, 
                                            UINT4 *, UINT4 *, UINT1, UINT1 *));
PUBLIC INT4 EcfmL2IwfGetInterfaceType PROTO ((UINT4, UINT1 *));
PUBLIC BOOL1 EcfmVlanIsDeiBitSet PROTO ((UINT4 ));
PUBLIC VOID EcfmPbbGetBCompBDA PROTO ((UINT4, UINT2, UINT4, tMacAddr, UINT1, BOOL1));
PUBLIC INT4 EcfmL2IwfGetVlanPortPvid PROTO ((UINT4, tVlanId *));
PUBLIC INT4 EcfmVlanProcessPktForCep PROTO ((UINT4, UINT2, tCRU_BUF_CHAIN_DESC *, tVlanTag *));
PUBLIC INT4 EcfmVcmGetIfIndexFromLocalPort PROTO ((UINT4, UINT2, UINT4 *));
PUBLIC INT1 EcfmPbbGetVipIsidWithPortList PROTO ((UINT4, UINT2, tLocalPortListExt, UINT2 *, UINT2 *, UINT4 *, tMacAddr *));
PUBLIC INT4 EcfmPbbGetMemberPortsForIsid PROTO (( UINT4,UINT4,tLocalPortListExt));
PUBLIC INT4 EcfmPbbGetCbpFwdPortListForIsid PROTO (( UINT4, UINT2, UINT4, tLocalPortListExt));
PUBLIC INT4 EcfmPbbGetPipWithPortList PROTO (( UINT4 , UINT2 , tLocalPortListExt , tLocalPortListExt ));
PUBLIC INT4 EcfmPbbGetBvidForIsid PROTO (( UINT4 , UINT2 , UINT4  , tVlanId *));
PUBLIC INT4 EcfmL2IwfTransmitCfmFrame PROTO ((tCRU_BUF_CHAIN_HEADER * , UINT4 , tVlanTag ,
                                              tEcfmPbbTag  , UINT1 ));
PUBLIC BOOL1 EcfmPbbMiIsIsidMemberPort PROTO ((UINT4, UINT4, UINT2 ));
PUBLIC INT1 EcfmPbbGetPipVipWithIsid PROTO ((UINT4, UINT2, UINT4, UINT2 *, UINT2 *));
PUBLIC INT4 EcfmPbbGetCnpMemberPortsForIsid PROTO ((UINT4, UINT4, tLocalPortListExt));
PUBLIC INT1 EcfmPbbGetfirstIsidForCBP PROTO ((UINT4, UINT2, UINT4 *));
PUBLIC INT1 EcfmPbbGetNextIsidForCBP PROTO ((UINT4, UINT2, UINT4, UINT4 *));
PUBLIC INT4 EcfmL2IwfPbbTxFrameToIComp PROTO ((UINT4, UINT4, tCRU_BUF_CHAIN_DESC *, tVlanTag *,
                                               tPbbTag *, UINT4 *, UINT1, UINT2));
PUBLIC INT4 EcfmL2IwfTransmitFrameOnVip PROTO ((UINT4, UINT4,tCRU_BUF_CHAIN_DESC *,
                                                tVlanTag *, tPbbTag *, UINT1));
INT4 
EcfmVcmSispGetPhysicalPortOfSispPort (UINT4 u4IfIndex, UINT4 * pu4PhyIfIndex);

INT4
EcfmVcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
                                           UINT1 u1RetLocalPorts,
                                           VOID *paSispPorts,
        UINT4 *pu4PortCount);
PUBLIC INT4 EcfmIsStpEnabledInContext PROTO ((UINT4));
PUBLIC VOID EcfmVlanGetPortEtherType PROTO ((UINT4, UINT2 *));
PUBLIC INT4 EcfmVlanGetCVlanIdList PROTO ((UINT4, tVlanId, tVlanListExt));
PUBLIC INT4 EcfmL2IwfGetPortVlanList PROTO ((UINT4, tSNMP_OCTET_STRING_TYPE *,
                                             UINT2, UINT1));
INT4 EcfmVlanApiForwardOnPortList PROTO ((UINT4                  u4ContextId, 
                                          UINT4                  u4InPort,
                                          tVlanTag              *pVlanTag,
                                          tCRU_BUF_CHAIN_HEADER *pFrame,
                                          tLocalPortListExt PortList,
                                          tMacAddr  DestAddr));
/* MPLS_TP_OAM related functions  */
INT4 EcfmMplsHandleExtInteraction PROTO ((UINT4 u4ReqType,
                                          tMplsApiInInfo * pInMplsApiInfo,
                                          tMplsApiOutInfo * pOutMplsApiInfo));
INT4 EcfmErpsApiIsErpsStartedInContext PROTO ((UINT4 u4ContextId));
#endif /* _CFM_PROT_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cfmport.h                     */
/*-----------------------------------------------------------------------*/
